@echo off
@pushd "%~dp0"
if NOT exist "packages\ja" (md packages\ja)
nuget pack C1.Excel.ja.nuspec -OutputDirectory "packages\ja"
nuget pack C1.Document.ja.nuspec -OutputDirectory "packages\ja"
nuget pack C1.FlexReport.ja.nuspec -OutputDirectory "packages\ja"
nuget pack C1.DataEngine.ja.nuspec -OutputDirectory "packages\ja"

nuget pack C1.Excel.452.ja.nuspec -OutputDirectory "packages\ja"
nuget pack C1.Document.452.ja.nuspec -OutputDirectory "packages\ja"
nuget pack C1.FlexReport.452.ja.nuspec -OutputDirectory "packages\ja"
nuget pack C1.DataEngine.452.ja.nuspec -OutputDirectory "packages\ja"
@popd