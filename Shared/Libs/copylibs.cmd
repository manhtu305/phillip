set jppath=E:\C1\dlls\win\C1WinForms.4_4.0.20202.449_JPN\bin\v4.0
set enpath=E:\C1\dlls\win\C1WinForms.4_4.0.20202.449\bin\v4.0
set jppath2=E:\C1\dlls\win\C1WinForms.2_2.0.20202.449_JPN\bin\v2.0
set enpath2=E:\C1\dlls\win\C1WinForms.2_2.0.20202.449\bin\v2.0

set jppath3=E:\C1\dlls\win\C1WinForms-452_4.5.20202.449_JPN\bin\v4.5.2
set enpath3=E:\C1\dlls\win\C1WinForms-452_4.5.20202.449\bin\v4.5.2

rem copy GrapeCity libs

copy %jppath%\c1.C1Report.4.dll GrapeCity\
copy %jppath%\C1.C1Report.CustomFields.4.dll GrapeCity\
copy %jppath%\c1.C1Zip.4.dll GrapeCity\
copy %jppath%\c1.Win.4.dll GrapeCity\
copy %jppath%\c1.Win.BarCode.4.dll GrapeCity\
copy %jppath%\c1.Win.C1Chart.4.dll GrapeCity\

copy %jppath%\C1.C1Excel.4.xml GrapeCity\
copy %jppath%\C1.C1Pdf.4.xml GrapeCity\
copy %jppath%\C1.C1Word.4.xml GrapeCity\
copy %jppath%\C1.C1Zip.4.xml GrapeCity\
copy %jppath%\C1.DataEngine.4.xml GrapeCity\
copy %jppath%\C1.Win.BarCode.4.xml GrapeCity\
copy %jppath%\C1.Win.Bitmap.4.xml GrapeCity\
copy %jppath%\C1.Win.C1Chart.4.xml GrapeCity\
copy %jppath%\C1.Win.C1Chart3D.4.xml GrapeCity\
copy %jppath%\C1.Win.C1Document.4.xml GrapeCity\
copy %jppath%\C1.Win.C1DX.4.xml GrapeCity\
copy %jppath%\C1.Win.C1SuperTooltip.4.xml GrapeCity\
copy %jppath%\C1.Win.FlexChart.4.xml GrapeCity\
copy %jppath%\C1.Win.FlexReport.4.xml GrapeCity\
copy %jppath%\C1.Win.FlexReport.CustomFields.4.xml GrapeCity\
copy %jppath%\C1.Win.ImportServices.4.xml GrapeCity\

rem copy release libs

copy %enpath%\C1.C1Excel.4.dll Release\
copy %enpath%\C1.C1Excel.4.xml Release\
copy %enpath%\C1.C1Pdf.4.dll Release\
copy %enpath%\C1.C1Pdf.4.xml Release\
copy %enpath%\C1.C1Report.4.dll Release\
copy %enpath%\C1.C1Report.CustomFields.4.dll Release\
copy %enpath%\C1.C1Word.4.dll Release\
copy %enpath%\C1.C1Word.4.xml Release\
copy %enpath%\C1.C1Zip.4.dll Release\
copy %enpath%\C1.C1Zip.4.xml Release\
copy %enpath%\C1.DataEngine.4.dll Release\
copy %enpath%\C1.DataEngine.4.xml Release\
copy %enpath%\C1.Win.4.dll Release\
copy %enpath%\C1.Win.BarCode.4.dll Release\
copy %enpath%\C1.Win.BarCode.4.xml Release\
copy %enpath%\C1.Win.Bitmap.4.dll Release\
copy %enpath%\C1.Win.Bitmap.4.xml Release\
copy %enpath%\C1.Win.C1Chart.4.dll Release\
copy %enpath%\C1.Win.C1Chart.4.xml Release\
copy %enpath%\C1.Win.C1Chart3D.4.dll Release\
copy %enpath%\C1.Win.C1Chart3D.4.xml Release\
copy %enpath%\C1.Win.C1Document.4.dll Release\
copy %enpath%\C1.Win.C1Document.4.xml Release\
copy %enpath%\C1.Win.C1DX.4.dll Release\
copy %enpath%\C1.Win.C1DX.4.xml Release\
copy %enpath%\C1.Win.C1SuperTooltip.4.dll Release\
copy %enpath%\C1.Win.C1SuperTooltip.4.xml Release\
copy %enpath%\C1.Win.FlexChart.4.dll Release\
copy %enpath%\C1.Win.FlexChart.4.xml Release\
copy %enpath%\C1.Win.FlexReport.4.dll Release\
copy %enpath%\C1.Win.FlexReport.4.xml Release\
copy %enpath%\C1.Win.FlexReport.CustomFields.4.dll Release\
copy %enpath%\C1.Win.FlexReport.CustomFields.4.xml Release\
copy %enpath%\C1.Win.ImportServices.4.dll Release\
copy %enpath%\C1.Win.ImportServices.4.xml Release\

rem copy c1.C1Report.2.dll\
copy %jppath2%\c1.C1Report.2.dll GrapeCity\
copy %enpath2%\c1.C1Report.2.dll Release\

rem copy GrapeCity452 libs

copy %jppath3%\c1.C1Report.4.5.2.dll GrapeCity452\
copy %jppath3%\C1.C1Report.CustomFields.4.5.2.dll GrapeCity452\
copy %jppath3%\c1.C1Zip.4.5.2.dll GrapeCity452\
copy %jppath3%\c1.Win.4.5.2.dll GrapeCity452\
copy %jppath3%\c1.Win.BarCode.4.5.2.dll GrapeCity452\
copy %jppath3%\c1.Win.C1Chart.4.5.2.dll GrapeCity452\

copy %jppath3%\C1.C1Excel.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.C1Pdf.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.C1Word.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.C1Zip.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.DataEngine.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.BarCode.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.Bitmap.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.C1Chart.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.C1Chart3D.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.C1Document.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.C1DX.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.C1SuperTooltip.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.FlexChart.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.FlexReport.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.FlexReport.CustomFields.4.5.2.xml GrapeCity452\
copy %jppath3%\C1.Win.ImportServices.4.5.2.xml GrapeCity452\

rem copy release452 libs

copy %enpath3%\C1.C1Excel.4.5.2.dll Release452\
copy %enpath3%\C1.C1Excel.4.5.2.xml Release452\
copy %enpath3%\C1.C1Pdf.4.5.2.dll Release452\
copy %enpath3%\C1.C1Pdf.4.5.2.xml Release452\
copy %enpath3%\C1.C1Report.4.5.2.dll Release452\
copy %enpath3%\C1.C1Report.CustomFields.4.5.2.dll Release452\
copy %enpath3%\C1.C1Word.4.5.2.dll Release452\
copy %enpath3%\C1.C1Word.4.5.2.xml Release452\
copy %enpath3%\C1.C1Zip.4.5.2.dll Release452\
copy %enpath3%\C1.C1Zip.4.5.2.xml Release452\
copy %enpath3%\C1.DataEngine.4.5.2.dll Release452\
copy %enpath3%\C1.DataEngine.4.5.2.xml Release452\
copy %enpath3%\C1.Win.4.5.2.dll Release452\
copy %enpath3%\C1.Win.BarCode.4.5.2.dll Release452\
copy %enpath3%\C1.Win.BarCode.4.5.2.xml Release452\
copy %enpath3%\C1.Win.Bitmap.4.5.2.dll Release452\
copy %enpath3%\C1.Win.Bitmap.4.5.2.xml Release452\
copy %enpath3%\C1.Win.C1Chart.4.5.2.dll Release452\
copy %enpath3%\C1.Win.C1Chart.4.5.2.xml Release452\
copy %enpath3%\C1.Win.C1Chart3D.4.5.2.dll Release452\
copy %enpath3%\C1.Win.C1Chart3D.4.5.2.xml Release452\
copy %enpath3%\C1.Win.C1Document.4.5.2.dll Release452\
copy %enpath3%\C1.Win.C1Document.4.5.2.xml Release452\
copy %enpath3%\C1.Win.C1DX.4.5.2.dll Release452\
copy %enpath3%\C1.Win.C1DX.4.5.2.xml Release452\
copy %enpath3%\C1.Win.C1SuperTooltip.4.5.2.dll Release452\
copy %enpath3%\C1.Win.C1SuperTooltip.4.5.2.xml Release452\
copy %enpath3%\C1.Win.FlexChart.4.5.2.dll Release452\
copy %enpath3%\C1.Win.FlexChart.4.5.2.xml Release452\
copy %enpath3%\C1.Win.FlexReport.4.5.2.dll Release452\
copy %enpath3%\C1.Win.FlexReport.4.5.2.xml Release452\
copy %enpath3%\C1.Win.FlexReport.CustomFields.4.5.2.dll Release452\
copy %enpath3%\C1.Win.FlexReport.CustomFields.4.5.2.xml Release452\
copy %enpath3%\C1.Win.ImportServices.4.5.2.dll Release452\
copy %enpath3%\C1.Win.ImportServices.4.5.2.xml Release452\

rem copy c1.C1Report.2.dll\
copy %jppath2%\c1.C1Report.2.dll GrapeCity452\
copy %enpath2%\c1.C1Report.2.dll Release452\
pause