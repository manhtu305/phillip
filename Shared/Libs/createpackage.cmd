@echo off
@pushd "%~dp0"
nuget pack C1.Excel.nuspec -OutputDirectory "packages"
nuget pack C1.Document.nuspec -OutputDirectory "packages"
nuget pack C1.FlexReport.nuspec -OutputDirectory "packages"
nuget pack C1.DataEngine.nuspec -OutputDirectory "packages"

nuget pack C1.Excel.452.nuspec -OutputDirectory "packages"
nuget pack C1.Document.452.nuspec -OutputDirectory "packages"
nuget pack C1.FlexReport.452.nuspec -OutputDirectory "packages"
nuget pack C1.DataEngine.452.nuspec -OutputDirectory "packages"
@popd