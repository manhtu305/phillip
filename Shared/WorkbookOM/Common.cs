﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
#if !FLEXSHEET
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
#else
using C1.JsonNet;
#endif
//
// The Excel common exchange format model definitions
// ==================================================

namespace
#if !FLEXSHEET
    C1.Web.Api.Excel
#else
 C1.Web.Mvc.Sheet
#endif
{
    /// <summary>
    /// The Excel workbook.
    /// </summary>
    public class Workbook
    {
#if !FLEXSHEET
        internal static JsonSerializerSettings JsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            DateFormatString = "yyyy/MM/dd HH:mm:ss",
        };

#endif
        private const string DefaultCreator = "C1";

        private const int DefaultActiveWorksheet = 0;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        public Workbook()
        {
            Sheets = new List<Worksheet>();
            Styles = new List<Style>();
            DefinedNames = new List<DefinedName>();
            Creator = DefaultCreator;
            ActiveWorksheet = DefaultActiveWorksheet;
        }

        /// <summary>
        /// Gets or sets the name of application that generated the file that appears in the file properties.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Application { get; set; }

        /// <summary>
        /// Gets or sets the name of company that generated the file that appears in the file properties.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets the name of the creator.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Creator { get; set; }

        /// <summary>
        /// Gets or sets the created time.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets or sets the name of the person who made the latest modification.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the modified time.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public DateTime Modified { get; set; }

        /// <summary>
        /// Gets or sets the current worksheet.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultActiveWorksheet)]
        public int ActiveWorksheet { get; set; }

        /// <summary>
        /// Gets the worksheet collection of the workbook.
        /// </summary>
        public List<Worksheet> Sheets { get; protected set; }

        /// <summary>
        /// Gets the styles table of the workbook.
        /// </summary>
        public List<Style> Styles { get; protected set; }

        /// <summary>
        /// Gets the defined name items of the workbook.
        /// </summary>
        public List<DefinedName> DefinedNames { get; protected set; }

        /// <summary>
        /// Gets or sets the color of the workbook themes.
        /// </summary>
        public string[] ColorThemes { get; set; }
    }

    /// <summary>
    /// The Excel worksheet.
    /// </summary>
    public class Worksheet
    {
        /// <summary>
        /// Create a new instance.
        /// </summary>
        public Worksheet()
        {
            Columns = new List<Column>();
            Rows = new List<Row>();
            Tables = new List<WorkbookTable>();
            Visible = true;
        }

        /// <summary>
        /// Gets or sets the FrozenPane of the worksheet.
        /// </summary>
        public FrozenPane FrozenPane { get; set; }

        /// <summary>
        /// Gets or sets the name of the worksheet.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Name { get; set; }

#if !FLEXSHEET
        /// <summary>
        /// The column definitions in the worksheet.
        /// </summary>
        [JsonIgnore]
        [Obsolete("This property has been deprecated. Use the Columns property instead.")]
        public List<Column> Cols
        {
            get
            {
                return Columns;
            }
        }
#endif

        /// <summary>
        /// Gets the column definitions in the worksheet.
        /// </summary>
#if FLEXSHEET
        [Json(false)]
#endif
        public List<Column> Columns { get; protected set; }

        /// <summary>
        /// Gets the row data in the worksheet.
        /// </summary>
#if FLEXSHEET
        [Json(false)]
#endif
        public List<Row> Rows { get; protected set; }

#if !FLEXSHEET
        //TODO: Should use DefaultValueAttribute here. But currently, this property is never 
        // transferred from client and it should be true when exporting FlexGrid, and its 
        // default value is false at client when importing.
        /// <summary>
        /// Gets or sets whether the summary of the group is above.
        /// </summary>
        [JsonIgnore]
        [XmlIgnore]
        [Obsolete("This property has been deprecated. Use the SummaryBelow property instead.")]
        public bool IsGroupHeaderAbove
        {
            get
            {
                return !SummaryBelow;
            }
            set
            {
                SummaryBelow = !value;
            }
        }
#endif
        /// <summary>
        /// Gets or sets the worksheet visibility.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(true)]
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether summary rows appear below or above detail rows.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool SummaryBelow { get; set; }

        /// <summary>
        /// Gets the tables in this worksheet.
        /// </summary>
        public List<WorkbookTable> Tables { get; private set; }
    }

    /// <summary>
    /// Represents the Table definition.
    /// </summary>
    public class WorkbookTable
    {
        /// <summary>
        /// Creates a WorkbookTable instance.
        /// </summary>
        public WorkbookTable()
        {
        }

        /// <summary>
        /// Creates a WorkbookTable instance.
        /// </summary>
        /// <param name="name">The name of the table.</param>
        /// <param name="range">The range of the table.</param>
        /// <param name="style">The table style to use with the table.</param>
        /// <param name="columns">The columns of the table.</param>
        public WorkbookTable(string name, string range, WorkbookTableStyle style=null, List<WorkbookTableColumn> columns = null)
        {
            Name = name;
            Range = range;
            Style = style;
            Columns = columns ?? new List<WorkbookTableColumn>();
        }

        /// <summary>
        /// Gets or sets the name of the table.
        /// </summary>
        /// <remarks>
        /// It is used to reference the table programmatically.
        /// </remarks>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets range on the relevant sheet that the table occupies expressed using A1 style referencing. i.e. "A1:D4".
        /// </summary>
        /// <remarks>
        /// The reference shall include the totals row if it is shown.
        /// </remarks>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Range { get; set; }

        /// <summary>
        /// Indicating whether show the header row for the table.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool ShowHeaderRow { get; set; }

        /// <summary>
        /// Indicating whether show the total row for the table.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool ShowTotalRow { get; set; }

        /// <summary>
        /// Indicating whether banded column formatting is applied.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool ShowBandedColumns { get; set; }

        /// <summary>
        /// Indicating whether banded row formatting is applied.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool ShowBandedRows { get; set; }

        /// <summary>
        /// Indicating whether the first column in the table should have the style applied.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool AlterFirstColumn { get; set; }

        /// <summary>
        /// Indicating whether the last column in the table should have the style applied.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool AlterLastColumn { get; set; }

        /// <summary>
        /// Gets or sets table style to use with the table.
        /// </summary>
        /// <remarks>
        /// The default style is the 'TableStyleMedium9' built-in table style, if the style is omitted.
        /// </remarks>
        public WorkbookTableStyle Style { get; set; }

        /// <summary>
        /// Gets the columns of the table.
        /// </summary>
        public List<WorkbookTableColumn> Columns { get; private set; }
    }

    /// <summary>
    /// Represents the Table Style definition.
    /// </summary>
    public class WorkbookTableStyle
    {
        /// <summary>
        /// Gets or sets the name of the table style.
        /// </summary>
        /// <remarks>
        /// Workbook supports built-in names: "TableStyleLight1" to "TableStyleLight21", "TableStyleMedium1" to "TableStyleMedium28", "TableStyleDark1" to "TableStyleDark11".
        /// </remarks>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the whole table style.
        /// </summary>
        public WorkbookTableCommonStyle WholeTableStyle { get; set; }

        /// <summary>
        /// Gets or sets the first column stripe style.
        /// </summary>
        public WorkbookTableBandedStyle FirstBandedColumnStyle { get; set; }

        /// <summary>
        /// Gets or sets the second column stripe style.
        /// </summary>
        public WorkbookTableBandedStyle SecondBandedColumnStyle { get; set; }

        /// <summary>
        /// Gets or sets the first row stripe style.
        /// </summary>
        public WorkbookTableBandedStyle FirstBandedRowStyle { get; set; }

        /// <summary>
        /// Gets or sets the second row stripe style.
        /// </summary>
        public WorkbookTableBandedStyle SecondBandedRowStyle { get; set; }

        /// <summary>
        /// Gets or sets the first column style.
        /// </summary>
        public WorkbookTableCommonStyle FirstColumnStyle { get; set; }

        /// <summary>
        /// Gets or sets the last column style.
        /// </summary>
        public WorkbookTableCommonStyle LastColumnStyle { get; set; }

        /// <summary>
        /// Gets or sets the header row style.
        /// </summary>
        public WorkbookTableCommonStyle HeaderRowStyle { get; set; }

        /// <summary>
        /// Gets or sets the total row style.
        /// </summary>
        public WorkbookTableCommonStyle TotalRowStyle { get; set; }

        /// <summary>
        /// Gets or sets the first cell style in the header row.
        /// </summary>
        public WorkbookTableCommonStyle FirstHeaderCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the last cell style in the header row.
        /// </summary>
        public WorkbookTableCommonStyle LastHeaderCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the first cell style in the total row.
        /// </summary>
        public WorkbookTableCommonStyle FirstTotalCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the last cell style in the total row.
        /// </summary>
        public WorkbookTableCommonStyle LastTotalCellStyle { get; set; }
    }

    /// <summary>
    /// Represents the Table Common Style definition.
    /// </summary>
    public class WorkbookTableCommonStyle : Style
    {
        /// <summary>
        /// Gets or sets the table borders setting.
        /// </summary>
        public WorkbookTableBorder Borders { get; set; }
    }

    /// <summary>
    /// Represents the Table Stripe Style definition.
    /// </summary>
    public class WorkbookTableBandedStyle : WorkbookTableCommonStyle
    {
        /// <summary>
        /// Gets or sets the number of rows or columns in a single band of striping.
        /// </summary>

#if !FLEXSHEET
        [XmlIgnore]
#endif
        public float? Size { get; set; }

#if !FLEXSHEET
        /// <summary>
        /// Gets or sets the <see cref="Size"/> for xml serialization.
        /// </summary>
        [XmlAttribute("Size")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public float SizeValue
        {
            get { return Size.HasValue ? Size.Value : 0f; }
            set { Size = value; }
        }
        /// <summary>
        /// Determines whether to serialize the <see cref="SizeValue"/> property.
        /// </summary>
        /// <returns>true if should serialize the <see cref="SizeValue"/> property; otherwise, false.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeSizeValue()
        {
            return Size.HasValue;
        }
#endif
    }

    /// <summary>
    /// Represents the Table border definition.
    /// </summary>
    public class WorkbookTableBorder
    {
        /// <summary>
        /// Gets or sets the vertical border setting.
        /// </summary>
        public Border Vertical { get; set; }

        /// <summary>
        /// Gets or set the horizontal border setting.
        /// </summary>
        public Border Horizontal { get; set; }
    }

    /// <summary>
    /// Represents the Table Column definition.
    /// </summary>
    public class WorkbookTableColumn
    {
        /// <summary>
        /// The name of the table column.
        /// </summary>
        /// <remarks>
        /// It is referenced through functions.
        /// </remarks>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the string to show in the totals row cell for the column.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string TotalRowLabel { get; set; }

        /// <summary>
        /// Gets or sets the function to show in the totals row cell for this column.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string TotalRowFunction { get; set; }

        /// <summary>
        /// Indicating whether show filter button for the column.
        /// </summary>

#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool ShowFilterButton { get; set; }
    }

    /// <summary>
    /// The FrozenPane of worksheet
    /// </summary>
    public class FrozenPane
    {
        private const int DefaultRows = 0;
        private const int DefaultColumns = 0;

        /// <summary>
        /// Create a FrozenPane instance.
        /// </summary>
        public FrozenPane()
        {
            Rows = DefaultRows;
            Columns = DefaultColumns;
        }

        /// <summary>
        /// Gets or sets the frozen row index.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultRows)]
        public int Rows { get; set; }

        /// <summary>
        /// Gets or sets the frozen column index.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultColumns)]
        public int Columns { get; set; }
    }

    /// <summary>
    /// The Excel cell.
    /// </summary>
    public class Cell
    {
        private const string DefaultFormula = "";
        private const int DefaultColSpan = 1;
        private const int DefaultRowSpan = 1;
        private const bool DefaultIsDate = false;

        /// <summary>
        /// Create a Cell instance.
        /// </summary>
        public Cell()
        {
            Formula = DefaultFormula;
            ColSpan = DefaultColSpan;
            RowSpan = DefaultRowSpan;
            IsDate = DefaultIsDate;
            TextRuns = new List<TextRun>();
        }

        /// <summary>
        /// Gets or sets the value of the cell.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the value of the cell is date or not.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultIsDate)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool IsDate { get; set; }

        /// <summary>
        /// Gets or sets the formula of the cell.
        /// </summary>
        [DefaultValue(DefaultFormula)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Formula { get; set; }

        /// <summary>
        /// Gets or sets the style of the cell.
        /// </summary>
        public Style Style { get; set; }

        /// <summary>
        /// Gets or sets the number of columns the cell occupies.
        /// </summary>

        [DefaultValue(DefaultColSpan)]
#if FLEXSHEET
        [Json(false)] //There are some wrong behavior if "ColSpan" or "RowSpan" doesn't have value.
#else
        [XmlAttribute]
#endif
        public int ColSpan { get; set; }

        /// <summary>
        /// Gets or sets the number of rows the cell occupies.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultRowSpan)]
#if FLEXSHEET
        [Json(false)]
#endif
        public int RowSpan { get; set; }

        /// <summary>
        /// Gets or sets the hyperlink of cell.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Link { get; set; }

        /// <summary>
        ///  Gets the text runs represent the rich text of cell.
        /// </summary>
        public List<TextRun> TextRuns { get; private set; }
    }

    /// <summary>
    /// Represents the text run definition.
    /// </summary>
    public class TextRun
    {
        /// <summary>
        /// Gets or sets the font of the text run.
        /// </summary>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the text of the text run.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Text { get; set; }
    }

    /// <summary>
    /// The coordinates of a cell in a worksheet.
    /// </summary>
    public class CellPosition
    {
        /// <summary>
        /// Create a CellPosition by an address.
        /// </summary>
        /// <param name="address">The cell address.</param>
        public CellPosition(string address)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create a CellPosition by coordinate components.
        /// </summary>
        /// <param name="col">The column number.</param>
        /// <param name="row">The row number.</param>
        public CellPosition(int col, int row)
        {
            Col = col;
            Row = row;
        }

        /// <summary>
        /// Gets or sets the vertical coordinate.
        /// </summary>
        public int Row { get; set; }

        /// <summary>
        /// Gets or sets the horizontal coordinate.
        /// </summary>
        public int Col { get; set; }
    }

    /// <summary>
    /// A rectangular area in a worksheet denominated by a top-left cell and a bottom-right cell.
    /// </summary>
    public class Range
    {
        /// <summary>
        /// Create a Range by address.
        /// </summary>
        /// <param name="address">The range address.</param>
        public Range(string address)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create a Range by coordinates and spans.
        /// </summary>
        /// <param name="leftCol">The starting column number.</param>
        /// <param name="topRow">The starting row number.</param>
        /// <param name="cols">The number of columns.</param>
        /// <param name="rows">The number of rows.</param>
        public Range(int leftCol, int topRow, int cols, int rows)
        {
            From = new CellPosition(leftCol, topRow);
            To = new CellPosition(leftCol + cols - 1, topRow + rows - 1);
        }

        /// <summary>
        /// Gets or sets the top-left cell position.
        /// </summary>
        public CellPosition From { get; set; }

        /// <summary>
        /// Gets or sets the bottom-right cell position.
        /// </summary>
        public CellPosition To { get; set; }
    }

    /// <summary>
    /// A column in a worksheet.
    /// </summary>
    public class Column
    {
        private const bool DefaultVisible = true;
        private const string DefaultWidth = "";
        private const bool DefaultAutoWidth = false;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        public Column()
        {
            Visible = DefaultVisible;
            Width = DefaultWidth;
            AutoWidth = DefaultAutoWidth;
        }

        /// <summary>
        /// Gets or sets the width of the column (pixels or characters width 'ch' suffix).
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultWidth)]
        public string Width { get; set; }

        /// <summary>
        /// Gets or sets the visibility of the column.
        /// </summary>
        [DefaultValue(DefaultVisible)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether auto adjust the column width.
        /// </summary>
        [DefaultValue(DefaultAutoWidth)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool AutoWidth { get; set; }

        /// <summary>
        /// Gets or sets the style of the column.
        /// </summary>
        public Style Style { get; set; }
    }

    /// <summary>
    /// A row in a worksheet.
    /// </summary>
    public class Row
    {
        private const bool DefaultVisible = true;
        private const int DefaultHeight = -1;
        private const int DefaultGroupLevel = 0;
        private const bool DefaultCollapsed = false;
        /// <summary>
        /// Create a new instance.
        /// </summary>
        public Row()
        {
            Visible = DefaultVisible;
            Height = DefaultHeight;
            GroupLevel = DefaultGroupLevel;
            Collapsed = DefaultCollapsed;
            Cells = new List<Cell>();
        }

        /// <summary>
        /// Create a new Row with cells data.
        /// </summary>
        /// <param name="values">The cells data.</param>
        public Row(params object[] values)
            : this()
        {
            foreach (var value in values)
            {
                Cells.Add(new Cell { Value = value });
            }
        }

        /// <summary>
        /// Gets or sets the height of the row (points).
        /// </summary>
#if !FLEXSHEET
        [JsonConverter(typeof(FloatRoundConverter))]
        [XmlAttribute]
#endif
        [DefaultValue(DefaultHeight)]
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the visibility of the row.
        /// </summary>
        [DefaultValue(DefaultVisible)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the level of the row when in grouping.
        /// </summary>
        [DefaultValue(DefaultGroupLevel)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public int GroupLevel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating if the rows 1 level of outlining deeper than the current row are in the collapsed outline state.
        /// </summary>
        [DefaultValue(DefaultCollapsed)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Collapsed { get; set; }

        /// <summary>
        /// Gets or sets the style of the row.
        /// </summary>
        public Style Style { get; set; }

        /// <summary>
        /// Gets the cells of the row.
        /// </summary>
#if FLEXSHEET
        [Json(false)]
#endif
        public List<Cell> Cells { get; protected set; }
    }

    /// <summary>
    /// The appearance attributes of certain cells.
    /// </summary>
    public class Style
    {
        private const string DefaultFormatExcel = "";
        private const HAlignType DefaultHAlign = HAlignType.General;
        private const VAlignType DefaultVAlign = VAlignType.Top;
        private const int DefaultIndent = 0;
        private const int DefaultTextRotation = 0;
        private const TextDirectionType DefaultTextDirection = TextDirectionType.Context;
        private const bool DefaultWrapText = false;
        private const bool DefaultShrinkToFit = false;
        private const bool DefaultLocked = true;

        /// <summary>
        /// Create a Style instance.
        /// </summary>
        public Style()
        {
            Format = DefaultFormatExcel;
            HAlign = DefaultHAlign;
            VAlign = DefaultVAlign;
            Indent = DefaultIndent;
            TextRotation = DefaultTextRotation;
            TextDirection = DefaultTextDirection;
            WrapText = DefaultWrapText;
            ShrinkToFit = DefaultShrinkToFit;
            Locked = DefaultLocked;
        }

        /// <summary>
        /// Gets or sets the excel format string.
        /// </summary>
        [DefaultValue(DefaultFormatExcel)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public string Format { get; set; }

#if !FLEXSHEET
        /// <summary>
        /// The format string.
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        [Obsolete("This property has been deprecated. Use the Format property instead.")]
        public string FormatCode
        {
            get
            {
                return Format;
            }
            set
            {
                Format = value;
            }
        }
#endif

        /// <summary>
        /// Gets or sets the the base style that this style inherits from.
        /// </summary>
        public Style BaseOn { get; set; }

        /// <summary>
        /// Gets or sets the font of text.
        /// </summary>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the horizontal alignment of cell content.
        /// </summary>
        [DefaultValue(DefaultHAlign)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public HAlignType HAlign { get; set; }

        /// <summary>
        /// Gets or sets the vertical alignment of cell content.
        /// </summary>
        [DefaultValue(DefaultVAlign)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public VAlignType VAlign { get; set; }

        /// <summary>
        /// Gets or sets the text indent.
        /// </summary>
        [DefaultValue(DefaultIndent)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public int Indent { get; set; }

        /// <summary>
        /// Gets or sets the rotation angle of text.
        /// </summary>
        [DefaultValue(DefaultTextRotation)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public int TextRotation { get; set; }

        /// <summary>
        /// Gets or sets the direction of text flow.
        /// </summary>
        [DefaultValue(DefaultTextDirection)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public TextDirectionType TextDirection { get; set; }

        /// <summary>
        /// Gets or sets the cell fill.
        /// </summary>
        public Fill Fill { get; set; }

        /// <summary>
        /// Gets or sets the cell border.
        /// </summary>
        public BorderCollection Border { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow text wrap.
        /// </summary>
        [DefaultValue(DefaultWrapText)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool WrapText { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow text shrink to fit cell size.
        /// </summary>
        [DefaultValue(DefaultShrinkToFit)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool ShrinkToFit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to lock the cells.
        /// </summary>
        [DefaultValue(DefaultLocked)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Locked { get; set; }
    }

    /// <summary>
    /// Font styles.
    /// </summary>
    public class Font
    {
        private const string DefaultFamily = "";
        private const int DefaultSize = 15;
        private const bool DefaultBold = false;
        private const bool DefaultItalic = false;
        private const bool DefaultUnderline = false;
        private const bool DefaultStrikethrough = false;
        private const bool DefaultSuperscript = false;
        private const bool DefaultSubscript = false;
        private const string DefaultColor = "";

        /// <summary>
        /// Create a Font instance.
        /// </summary>
        public Font()
        {
            Family = DefaultFamily;
            Size = DefaultSize;
            Bold = DefaultBold;
            Italic = DefaultItalic;
            Underline = DefaultUnderline;
            Strikethrough = DefaultStrikethrough;
            Superscript = DefaultSuperscript;
            Subscript = DefaultSubscript;
            Color = DefaultColor;
        }

        /// <summary>
        /// Gets or sets the typeface name.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultFamily)]
        public string Family { get; set; }

        /// <summary>
        /// Gets or sets the font size.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultSize)]
        public float Size { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to make the text bold.
        /// </summary>
        [DefaultValue(DefaultBold)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Bold { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to make the text italic.
        /// </summary>
        [DefaultValue(DefaultItalic)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Italic { get; set; }

        /// <summary>
        /// Gets or sets underscore setting.
        /// </summary>
        [DefaultValue(DefaultUnderline)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Underline { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show strikethrough.
        /// </summary>
        [DefaultValue(DefaultStrikethrough)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Strikethrough { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to make the text superscript.
        /// </summary>
        [DefaultValue(DefaultSuperscript)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Superscript { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to make the text subscript.
        /// </summary>
        [DefaultValue(DefaultSubscript)]
#if !FLEXSHEET
        [XmlAttribute]
#endif
        public bool Subscript { get; set; }

        /// <summary>
        /// Gets or sets the text color.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultColor)]
        public string Color { get; set; }
    }

    /// <summary>
    /// All borders of a cell.
    /// </summary>
    public class BorderCollection
    {
        /// <summary>
        /// Gets or sets the top border.
        /// </summary>
        public Border Top { get; set; }

        /// <summary>
        /// Gets or sets the bottom border.
        /// </summary>
        public Border Bottom { get; set; }

        /// <summary>
        /// Gets or sets the left border.
        /// </summary>
        public Border Left { get; set; }

        /// <summary>
        /// Gets or sets the right border.
        /// </summary>
        public Border Right { get; set; }

        /// <summary>
        /// Gets or sets the diagonal border.
        /// </summary>
        public Border Diagonal { get; set; }

        /// <summary>
        /// Gets or sets the create borders.
        /// </summary>
        public BorderCollection()
        {
        }

        /// <summary>
        /// Create uniform solid borders using a color.
        /// </summary>
        /// <param name="color">The border color.</param>
        public BorderCollection(string color)
            : this()
        {
            Top = new Border(color);
            Bottom = new Border(color);
            Left = new Border(color);
            Right = new Border(color);
            Diagonal = new Border(color);
        }
    }

    /// <summary>
    /// Border styles.
    /// </summary>
    public class Border
    {
        private const string DefaultColor = "";
        private const BorderLinetype DefaultType = BorderLinetype.None;

        /// <summary>
        /// Create a border.
        /// </summary>
        public Border()
            : this(DefaultColor)
        {
        }

        /// <summary>
        /// Create a solid border using a color.
        /// </summary>
        /// <param name="color">The border color.</param>
        public Border(string color)
        {
            Color = color;
            Type = string.IsNullOrEmpty(color) ? DefaultType : BorderLinetype.Thin;
        }

        /// <summary>
        /// Gets or sets the border color.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultColor)]
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets the border linetype.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultType)]
        public BorderLinetype Type { get; set; }
    }

    /// <summary>
    /// Cell fill styles.
    /// </summary>
    public class Fill
    {
        private const string DefaultColor = "";
        private const PatternStyle DefaultPattern = PatternStyle.None;

        /// <summary>
        /// Create a Fill instance.
        /// </summary>
        public Fill()
        {
            Color = DefaultColor;
            Pattern = DefaultPattern;
        }

        /// <summary>
        /// Gets or sets the fill color.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultColor)]
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets the fill pattern.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultPattern)]
        public PatternStyle Pattern { get; set; }
    }

    /// <summary>
    /// Represents the Workbook Object Model Defined Name item definition.
    /// </summary>
    public class DefinedName
    {
        private const string DefaultName = "";
        private const string DefaultValue = "";
        private const string DefaultSheetName = "";

        /// <summary>
        /// Creates a defined name.
        /// </summary>
        public DefinedName()
        {
            Name = DefaultName;
            Value = DefaultValue;
            SheetName = DefaultSheetName;
        }

        /// <summary>
        /// The name of the defined name item.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultName)]
        public string Name { get; set; }

        /// <summary>
        /// The value of the defined name item.
        /// The value could be a formula, a string constant or a cell range.
        /// For e.g. "Sum(1, 2, 3)", "1.2", "\"test\"" or "sheet1!A1:B2".
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultValue)]
        public string Value { get; set; }

        /// <summary>
        /// Indicates the defined name item works in which sheet.
        /// If omitted, the defined name item works in workbook.
        /// </summary>
#if !FLEXSHEET
        [XmlAttribute]
#endif
        [DefaultValue(DefaultSheetName)]
        public string SheetName { get; set; }
    }

    /// <summary>
    /// Horizontal alignment options.
    /// </summary>
#if !FLEXSHEET
    [JsonConverter(typeof(StringEnumConverter))]
#endif
    public enum HAlignType
    {
        /// <summary>
        /// General Horizontal Alignment.
        /// </summary>
        General = 0,

        /// <summary>
        /// Left Horizontal Alignment.
        /// </summary>
        Left = 1,

        /// <summary>
        /// Centered Horizontal Alignment.
        /// </summary>
        Center = 2,

        /// <summary>
        /// Right Horizontal Alignment.
        /// </summary>
        Right = 3,

        /// <summary>
        /// Fill.
        /// </summary>
        Fill = 4,

        /// <summary>
        /// Justify.
        /// </summary>
        Justify = 5
    }

    /// <summary>
    /// Vertical alignment options.
    /// </summary>
#if !FLEXSHEET
    [JsonConverter(typeof(StringEnumConverter))]
#endif
    public enum VAlignType
    {
        /// <summary>
        /// Align Top.
        /// </summary>
        Top = 0,

        /// <summary>
        /// Centered Vertical Alignment.
        /// </summary>
        Center = 1,

        /// <summary>
        /// Aligned To Bottom.
        /// </summary>
        Bottom = 2,

        /// <summary>
        /// 
        /// </summary>
        Justify = 3
    }

    /// <summary>
    /// The direction of text flow.
    /// </summary>
#if !FLEXSHEET
    [JsonConverter(typeof(StringEnumConverter))]
#endif
    public enum TextDirectionType
    {
        /// <summary>
        /// Reading order is determined by scanning the text for the first
        /// non-whitespace character: if it is a strong right-to-left character,
        /// the reading order is right-to-left; otherwise, the reading order left-to-right.
        /// </summary>
        Context = 0,

        /// <summary>
        /// Reading order is left-to-right in the cell, as in English.
        /// </summary>
        LeftToRight = 1,

        /// <summary>
        /// Reading order is right-to-left in the cell, as in Hebrew.
        /// </summary>
        RightToLeft = 2
    }

    /// <summary>
    /// The style of the cell borders.
    /// </summary>
#if !FLEXSHEET
    [JsonConverter(typeof(StringEnumConverter))]
#endif
    public enum BorderLinetype
    {
        /// <summary>
        /// The line style of a border is none (no border visible).
        /// </summary>
        None = 0,

        /// <summary>
        /// The line style of a border is thin. 
        /// </summary>
        Thin = 1,

        /// <summary>
        /// The line style of a border is medium.
        /// </summary>
        Medium = 2,

        /// <summary>
        /// The line style of a border is dashed.
        /// </summary>
        Dashed = 3,

        /// <summary>
        /// The line style of a border is dotted.
        /// </summary>
        Dotted = 4,

        /// <summary>
        /// The line style of a border is 'thick'.
        /// </summary>
        Thick = 5,

        /// <summary>
        /// The line style of a border is double line.
        /// </summary>
        Double = 6,

        /// <summary>
        /// The line style of a border is hairline.
        /// </summary>
        Hair = 7,

        /// <summary>
        /// The line style of a border is medium dashed.
        /// </summary>
        MediumDashed = 8,

        /// <summary>
        /// The line style of a border is thin dash-dot.
        /// </summary>
        ThinDashDotted = 9,

        /// <summary>
        /// The line style of a border is medium dash-dot.
        /// </summary>
        MediumDashDotted = 10,

        /// <summary>
        /// The line style of a border is thin dash-dot-dot.
        /// </summary>
        ThinDashDotDotted = 11,

        /// <summary>
        /// The line style of a border is medium dash-dot-dot.
        /// </summary>
        MediumDashDotDotted = 12,

        /// <summary>
        /// The line style of a border is slanted dash-dot.
        /// </summary>
        SlantedMediumDashDotted = 13
    }

    /// <summary>
    /// The pattern type of the cell fills.
    /// </summary>
#if !FLEXSHEET
    [JsonConverter(typeof(StringEnumConverter))]
#endif
    public enum PatternStyle
    {
        /// <summary>
        /// No pattern (transparent).
        /// </summary>
        None = 0,

        /// <summary>
        /// Solid background.
        /// </summary>
        Solid = 1,

        /// <summary>
        /// 50% dotted pattern.
        /// </summary>
        Gray50 = 2,

        /// <summary>
        /// 75% dotted pattern.
        /// </summary>
        Gray75 = 3,

        /// <summary>
        /// 25% dotted pattern.
        /// </summary>
        Gray25 = 4,

        /// <summary>
        /// Horizontal stripe pattern.
        /// </summary>
        HorizontalStripe = 5,

        /// <summary>
        /// Vertical stripe pattern.
        /// </summary>
        VerticalStripe = 6,

        /// <summary>
        /// Reverse diagonal stripe pattern (\).
        /// </summary>
        ReverseDiagonalStripe = 7,

        /// <summary>
        /// Diagonal stripe pattern (/).
        /// </summary>
        DiagonalStripe = 8,

        /// <summary>
        /// Diagonal crosshatch pattern.
        /// </summary>
        DiagonalCrosshatch = 9,

        /// <summary>
        /// Thick diagonal crosshatch pattern.
        /// </summary>
        ThickDiagonalCrosshatch = 10,

        /// <summary>
        /// Thin horizontal stripe pattern.
        /// </summary>
        ThinHorizontalStripe = 11,

        /// <summary>
        /// Thin vertical stripe pattern.
        /// </summary>
        ThinVerticalStripe = 12,

        /// <summary>
        /// Thin reverse diagonal stripe pattern (\).
        /// </summary>
        ThinReverseDiagonalStripe = 13,

        /// <summary>
        /// Thin diagonal stripe pattern (/).
        /// </summary>
        ThinDiagonalStripe = 14,

        /// <summary>
        /// Thin horizontal crosshatch pattern (squares).
        /// </summary>
        ThinHorizontalCrosshatch = 15,

        /// <summary>
        /// Thin diagonal crosshatch pattern (diamonds).
        /// </summary>
        ThinDiagonalCrosshatch = 16,

        /// <summary>
        /// 12% dotted pattern (less dots).
        /// </summary>
        Gray12 = 17,

        /// <summary>
        /// 6% dotted pattern (least dots).
        /// </summary>
        Gray06 = 18
    }
#if !FLEXSHEET
    /// <summary>
    /// The json converter for rounding the float value.
    /// </summary>
    public class FloatRoundConverter : JsonConverter
    {
        /// <summary>
        /// Check if the specified type can be converted.
        /// </summary>
        /// <param name="objectType">The object type</param>
        /// <returns>A boolean value indicates if the specified type can be converted</returns>
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>
        /// The object value.
        /// </returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return null;
            }
            string valueString = reader.Value.ToString();
            double valueDouble;
            double.TryParse(valueString, out valueDouble);
            return (int)Math.Round(valueDouble);
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter"/> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }
    }

    /// <summary>
    /// Converts an <see cref="T:System.Enum"/> to and from its name string value with camel style.
    /// </summary>
    public class CamelStringEnumConverter : StringEnumConverter
    {
        /// <summary>
        /// Create a CamelStringEnumConverter.
        /// </summary>
        public CamelStringEnumConverter()
        {
            CamelCaseText = true;
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>
        /// The object value.
        /// </returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            try
            {
                return base.ReadJson(reader, objectType, existingValue, serializer);
            }
            // Some invalid values may be sent from client side, like: empty string, 
            // start(HAlign) and so on
            catch (Exception)
            {
                return existingValue;
            }
        }
    }
#endif
}