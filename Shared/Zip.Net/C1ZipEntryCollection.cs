//----------------------------------------------------------------------------
// C1\C1Zip\C1ZipEntryCollection.cs
//
// C1ZipEntryCollection is a collection of C1ZipEntry objects that
// represents entries contained in a C1ZipFile object.
//
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status           Date            By                      Comments
//----------------------------------------------------------------------------
// Created          Nov, 2001       Rodrigo             -
//----------------------------------------------------------------------------

using System;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace C1.C1Zip
{
#if EXPOSE_ZIP
    /// <summary>
	/// A collection of <see cref="C1ZipEntry"/> objects that represent the contents of
	/// a zip file.
	/// </summary>
	/// <remarks>
	/// Use the <b>C1ZipEntryCollection</b> to programmatically manage the contents of zip
	/// files. You can add, remove, extract, or insert items into the <b>C1ZipEntryCollection</b>.
	/// </remarks>
#if false && !COMPACT_FRAMEWORK
	[
	ClassInterface(ClassInterfaceType.AutoDual),
	Guid("A1FBE3E2-E49B-4c21-ADF9-3BE160EED2D6")
	]
#endif
    public class C1ZipEntryCollection : IEnumerable<C1ZipEntry>
#else
    #if WIJMOCONTROLS
    [SmartAssembly.Attributes.DoNotObfuscateType]
    #endif
    internal class C1ZipEntryCollection : IEnumerable<C1ZipEntry>
#endif
    {
		//--------------------------------------------------------------------------------
		#region ** fields

		C1ZipFile _owner;

		#endregion

		//--------------------------------------------------------------------------------
		#region ** ctor

        internal C1ZipEntryCollection(C1ZipFile owner)
        {
            _owner = owner;
        }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** IEnumerable

        IEnumerator<C1ZipEntry> IEnumerable<C1ZipEntry>.GetEnumerator()
        {
            return _owner._headers.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _owner._headers.GetEnumerator();
        }

        #endregion

		//--------------------------------------------------------------------------------
		#region ** public

		/// <summary>
		/// Gets the number of entries in the current zip file.
		/// </summary>
        public int Count
        {
            get { return _owner._headers.Count; }
        }
		/// <summary>
		/// Gets the <see cref="C1ZipEntry"/> at the specified index.
		/// </summary>
        public C1ZipEntry this [int index]
        {
            get { return _owner._headers[index]; }
        }
		/// <summary>
		/// Gets the <see cref="C1ZipEntry"/> with the given name (returns null if the entry cannot be found).
		/// </summary>
		public C1ZipEntry this [string name]
        {
            get 
            {
                int index = IndexOf(name);
                return index < 0 ? null : this[index];
            }
        }
		/// <summary>
		/// Determines whether the collection contains an entry with a given name.
		/// </summary>
		/// <param name="name">Name of the entry to look for.</param>
		/// <returns>True if the collection contains an entry with the given name, false otherwise.</returns>
		public bool Contains(string name)
		{
			return IndexOf(name) < 0 ? false : true;
		}
        /// <summary>
        /// Determines whether the collection contains an entry.
        /// </summary>
        /// <param name="entry">Entry of the entry to look for.</param>
        /// <returns>True if the collection contains the entry, false otherwise.</returns>
        public bool Contains(C1ZipEntry entry) // <<B56>>
        {
            return IndexOf(entry) < 0 ? false : true;
        }
        /// <summary>
		/// Gets the index of the entry with the specified name.
		/// </summary>
		/// <param name="name">Name of the entry to look for.</param>
		/// <returns>The index of the entry in the collection, or -1 if the entry was not found.</returns>
		public int IndexOf(string name)
		{
			// look for full match, e.g: "123.exe" finds "123.exe"
			for (int i = 0; i < Count; i++)
			{
				string entryName = this[i].FileName;
				if (string.Equals(name, entryName, StringComparison.OrdinalIgnoreCase))
					return i;
			}

			// =========================================================================
			// commented out because when we add a new entry with the same file name
			// of an existing entry but with different path, we don't want to delete
			// the existing entry.
			// e.g: if we have an entry called "myfolder\123.exe" and we add a new entry
			// called "123.exe", we want them both in the zip. If we keep the code below,
			// first the entry "myfolder\123.exe" is deleted and then "123.exe" is added.
			// -------------------------------------------------------------------------
			// // look for file match ignoring dir, e.g: "123.exe" finds "bin/123.exe"
			// for (int i = 0; i < Count; i++)
			// {
			//      string entryName = Path.GetFileName(this[i].FileName);
			//      if (string.Compare(name, entryName, true, CultureInfo.CurrentCulture) == 0)
			//          return i;
			// }
			// =========================================================================

			// not found
			return -1;
		}
        /// <summary>
        /// Gets the index of an entry in the collection.
        /// </summary>
        /// <param name="entry"><see cref="C1ZipEntry"/> to look for.</param>
        /// <returns>The index of the entry in the collection, or -1 if the entry was not found.</returns>
        public int IndexOf(C1ZipEntry entry) // <<B56>>
        {
            return _owner._headers.IndexOf(entry);
        }

		#region ** add entries to a zip file

		/// <summary>
		/// Adds an entry to the current zip file.
		/// </summary>
		/// <param name="fileName">Name of the file to add to the zip file.</param>
		/// <remarks>
		/// By default, the directory name is not stored in the zip file. To store a specific
		/// part of the directory name in the zip file, use the overloaded version of the 
		/// <b>Add</b> method with a <b>pathLevels</b> parameter.
		/// </remarks>
        public void Add(string fileName)
        {
            Add(fileName, 0);
        }
		/// <summary>
		/// Adds a list of entries to the current zip file.
		/// </summary>
		/// <param name="fileNames">Array containing the file names of the entries to be added to the zip file.</param>
        public void Add(string[] fileNames)
        {
			foreach (string fileName in fileNames)
				Add(fileName, 0);
		}
		/// <summary>
		/// Adds an entry to the current zip file.
		/// </summary>
		/// <param name="fileName">Name of the file to add to the zip file.</param>
		/// <param name="pathLevels">The number of path levels to be stored as the entry name.</param>
		/// <remarks>
		/// <para>By default, path names are not stored in the zip file. For example, adding
		/// the file "c:\temp\samples\readme.txt" to the zip file will create an entry called
		/// "readme.txt".</para>
		/// <para>The <paramref name="pathLevels"/> parameter allows you to store one or more levels of the path in the entry name.
		/// For example, adding the file "c:\temp\samples\readme.txt" to the zip file with <paramref name="pathLevels"/>=1 
		/// will create an entry called "samples\readme.txt".</para>
		/// </remarks>
        public void Add(string fileName, int pathLevels)
        {
            string entryName = TrimPath(fileName, pathLevels);
            Add(fileName, entryName);
        }
		/// <summary>
		/// Adds an entry to the current zip file.
		/// </summary>
		/// <param name="fileName">Name of the file to add to the zip file.</param>
		/// <param name="entryName">Name of the new entry as stored in the zip file.</param>
		/// <remarks>
		/// By default, entries in the zip file have the same name as the original (uncompressed)
		/// file. This method allows you to specify a different name, including a path for example.
		/// </remarks>
        public void Add(string fileName, string entryName)
        {
            var dateTime = fileName != null && (File.Exists(fileName) || Directory.Exists(fileName))
                ? File.GetLastWriteTime(fileName)
                : DateTime.Now;
            Add(fileName, entryName, dateTime);
        }
        /// <summary>
        /// Adds an entry to the current zip file.
        /// </summary>
        /// <param name="fileName">Name of the file to add to the zip file.</param>
        /// <param name="entryName">Name of the new entry as stored in the zip file.</param>
        /// <param name="dateTime">The date and time when the file was last modified.</param>
        /// <remarks>
        /// By default, entries in the zip file have the same name as the original (uncompressed)
        /// file. This method allows you to specify a different name, including a path for example.
        /// </remarks>
        public void Add(string fileName, string entryName, DateTime dateTime)
        {
			// refuse to add a zip file to itself
			if (string.Equals(fileName, _owner.FileName, StringComparison.OrdinalIgnoreCase))
			{
				string msg = StringTables.GetString("Can't add a zip file to itself.");
				throw new ArgumentException(msg);
			}

			// make sure this is a valid file or folder
			bool bFile = File.Exists(fileName);
			bool bFolder = !bFile && Directory.Exists(fileName);
			if (!bFile && !bFolder)
			{
				string msg = StringTables.GetString("File not found: '{0}'.");
				msg = string.Format(msg, fileName);
				throw new FileNotFoundException(msg);
			}

            // add to zip file
            _owner.Add(fileName, entryName, dateTime);
		}
		/// <summary>
		/// Adds a stream to the current zip file.
		/// </summary>
		/// <param name="stream">Stream that contains data for the new entry.</param>
		/// <param name="entryName">Name to be used for the new entry.</param>
        public void Add(Stream stream, string entryName)
        {
            // get date from stream if possible
            var dateTime = DateTime.Now;
#if !SILVERLIGHT
            var fs = stream as FileStream;
            if (fs != null)
            {
                try
                {
                    var fn = fs.Name;
                    if (fn != null && (File.Exists(fn) || Directory.Exists(fn)))
                    {
                        dateTime = File.GetLastWriteTime(fn);
                    }
                }
                catch 
                {
                    // not always allowed...
                }
            }
#endif
            // go add it
            Add(stream, entryName, dateTime);
        }
        /// <summary>
        /// Adds a stream to the current zip file.
        /// </summary>
        /// <param name="stream">Stream that contains data for the new entry.</param>
        /// <param name="entryName">Name to be used for the new entry.</param>
        /// <param name="dateTime">The date and time when the file was last modified.</param>
        public void Add(Stream stream, string entryName, DateTime dateTime)
        {
            _owner.Add(stream, entryName, dateTime);
        }


		#endregion

#if !SILVERLIGHT
		#region ** add/extract folders (paths) to a zip file

		/// <summary>
		/// Adds the content of a folder to the current zip file.
		/// </summary>
		/// <param name="path">The full path of the folder to be added to the zip file.</param>
		/// <remarks>
		/// This method adds all files and sub folders to the zip file.
		/// </remarks>
		public void AddFolder(string path)
		{
			AddFolder(path, "*.*", true, 0);
		}
		/// <summary>
		/// Adds the content of a folder to the current zip file.
		/// </summary>
		/// <param name="path">The full path of the folder to be added to the zip file.</param>
		/// <param name="searchPattern">A mask that specifies which files to add.</param>
		/// <remarks>
		/// If the folder contains sub folders, those are also added to the zip file.
		/// </remarks>
		public void AddFolder(string path, string searchPattern)
		{
			AddFolder(path, searchPattern, true, 0);
		}
		/// <summary>
		/// Adds the content of a folder to the current zip file.
		/// </summary>
		/// <param name="path">The full path of the folder to be added to the zip file.</param>
		/// <param name="searchPattern">A mask that specifies which files to add.</param>
		/// <param name="includeSubfolders">True to include sub folders, false to include only files at the root level.</param>
		public void AddFolder(string path, string searchPattern, bool includeSubfolders)
		{
			AddFolder(path, searchPattern, includeSubfolders, 0);
		}
		void AddFolder(string path, string searchPattern, bool includeSubfolders, int level)
		{
            // get files and sub-folders
            string[] files = Directory.GetFiles(path, searchPattern);
            string[] dirs = Directory.GetDirectories(path);

            // if there are no files or sub-folders, add empty folder <<B38>>
            if (files.Length == 0 && dirs.Length == 0 && level > 0)
            {
                this.Add(path, level - 1);
            }

			// add files
			foreach (string file in files)
			{
				this.Add(file, level);
			}

			// add includeSubfolders
			if (includeSubfolders)
			{
				foreach (string dir in dirs)
				{
					AddFolder(dir, searchPattern, true, level + 1);
				}
			}
		}
		/// <summary>
		/// Extracts the contents of the zip file into a specified path.
		/// </summary>
		/// <param name="path">Destination path for the unzipped files.</param>
		/// <remarks>
		/// If the zip file contains compressed folders, new folders will be created
		/// under the destination path to preserve the hierarchical structure of the
		/// archive.
		/// </remarks>
		public void ExtractFolder(string path)
		{
			foreach (C1ZipEntry ze in this)
			{
				string fn = ze.FileName;
				string filePath = Path.Combine(path, Path.GetDirectoryName(fn));
				string fileName = Path.GetFileName(fn);

				if (!Directory.Exists(filePath))
				{
					Directory.CreateDirectory(filePath);
				}
				this.Extract(ze.FileName, Path.Combine(filePath, fileName));
			}
		}
		#endregion
#endif
		/// <summary>
		/// Removes an entry from the current zip file.
		/// </summary>
		/// <param name="index">Index of the entry to remove.</param>
        public void Remove(int index)
        {
            _owner.Remove(index);
        }
		/// <summary>
		/// Removes an entry from the current zip file.
		/// </summary>
		/// <param name="fileName">Name of the entry to remove (case-insensitive).</param>
        public void Remove(string fileName)
        {
            Remove(IndexOf(fileName));
        }
		/// <summary>
		/// Removes several entries from the current zip file.
		/// </summary>
		/// <param name="indices">Array containing the indices of the entries to remove.</param>
        public void Remove(int[] indices)
        {
            _owner.Remove(indices);
        }
		/// <summary>
		/// Removes several entries from the current zip file.
		/// </summary>
		/// <param name="entryNames">Array containing the names of the entries to remove.</param>
		public void Remove(string[] entryNames)
        {
            int[] indices = new int[entryNames.Length];
            for (int i = 0; i < entryNames.Length; i++)
            {
                indices[i] = IndexOf(entryNames[i]);
            }
            Remove(indices);
        }
		/// <summary>
		/// Extracts a file from the current zip file. 
		/// </summary>
		/// <param name="index">Index of the entry to extract.</param>
		/// <param name="destFileName">Name and location of the extracted file.</param>
        public void Extract(int index, string destFileName)
        {
			// refuse to extract a zip file over itself <<B27>>
			if (string.Equals(destFileName, _owner.FileName, StringComparison.OrdinalIgnoreCase))
			{
				string msg = StringTables.GetString("Can't extract entry over parent zip file.");
				throw new ArgumentException(msg, destFileName);
			}
            
			// check index
			if (index < 0 || index >= Count)
			{
				string msg = StringTables.GetString("Invalid index or entry name.");
				throw new IndexOutOfRangeException(msg);
			}

            // call base implementation
            _owner.Extract(index, destFileName);
        }
		/// <summary>
		/// Extracts a file from the current zip file. 
		/// </summary>
		/// <param name="entryName">Name of the entry to extract.</param>
		/// <param name="destFileName">Name and location of the extracted file.</param>
		public void Extract(string entryName, string destFileName)
        {
            Extract(IndexOf(entryName), destFileName);
        }
		/// <summary>
		/// Extracts a file from the current zip file. 
		/// </summary>
		/// <param name="index">Index of the entry to extract.</param>
		/// <remarks>
		/// The entry is extracted to a file in the same folder as the current zip
		/// file, with the same name as the entry.
		/// </remarks>
		public void Extract(int index)
        {
            // get default name for the destination file
            string dstFileName = _owner.FileName.Length > 0 
                ? Path.GetDirectoryName(_owner.FileName) 
                : string.Empty;
			if (dstFileName.Length > 0)
				dstFileName += Path.DirectorySeparatorChar;
			dstFileName += Path.GetFileName(_owner.Entries[index].FileName);

            // go extract it
            Extract(index, dstFileName);
        }
		/// <summary>
		/// Extracts a file from the current zip file. 
		/// </summary>
		/// <param name="entryName">Name of the entry to extract.</param>
		/// <remarks>
		/// The entry is extracted to a file in the same folder as the current zip
		/// file, with the same name as the entry.
		/// </remarks>
		public void Extract(string entryName)
        {
            Extract(IndexOf(entryName));
        }
		/// <summary>
		/// Opens a stream for writing an entry into the zip file.
		/// </summary>
		/// <param name="entryName">The name of the new entry.</param>
		/// <param name="useMemory">Whether to use a memory stream or temporary file.</param>
		/// <returns>
		/// A stream that can be used to write data into the zip file. The entry
		/// is not added until the stream is closed.
		/// </returns>
        public Stream OpenWriter(string entryName, bool useMemory)
        {
            return _owner.OpenWriter(entryName, DateTime.Now, useMemory);
        }
        /// <summary>
        /// Opens a stream for writing an entry into the zip file.
        /// </summary>
        /// <param name="entryName">The name of the new entry.</param>
        /// <param name="dateTime">The date and time when the file was last modified.</param>
        /// <param name="useMemory">Whether to use a memory stream or temporary file.</param>
        /// <returns>
        /// A stream that can be used to write data into the zip file. The entry
        /// is not added until the stream is closed.
        /// </returns>
        public Stream OpenWriter(string entryName, DateTime dateTime, bool useMemory)
        {
            return _owner.OpenWriter(entryName, dateTime, useMemory);
        }
        #endregion

		//--------------------------------------------------------------------------------
		#region ** internal

        // get the file name with 'pathLevels' levels of path
        internal static string TrimPath(string fileName, int pathLevels)
        {
            string trimmedPath = Path.GetFileName(fileName);
            DirectoryInfo dir = new DirectoryInfo(fileName);    
            for (int i = 0; i < pathLevels; i++)
            {
                // add the directory name to the file name
                trimmedPath = string.Format("{0}{1}{2}",
                    dir.Parent.Name, Path.DirectorySeparatorChar, trimmedPath);
                dir = dir.Parent;                                   
            }

            // done
            return trimmedPath;
        }
		#endregion
    }
}
