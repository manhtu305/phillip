//----------------------------------------------------------------------------
// C1\C1Zip\ZipEntryStreamWriter.cs
//
// ZipEntryStreamWriter is a class that allows you to add streams into a Zip file.
//
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status           Date            By                      Comments
//----------------------------------------------------------------------------
// Created          Jan, 2002       Rodrigo/Bernardo        -
//----------------------------------------------------------------------------
using System;
using System.IO;

namespace C1.C1Zip
{
	/// <summary>
    /// ZipEntryStreamWriter
    /// Extends C1ZStreamWriter to provide the following:
    /// 1) Creates a memory or temporary storage stream.
    /// 2) Keeps track of the parent zip file.
    /// 3) Overrides Close to add the compressed stream to the zip file.
	/// </summary>
    internal class ZipEntryStreamWriter : C1ZStreamWriter
    {
        C1ZipFile   _owner;
		string      _entryName;
		string      _tempFile;
		string      _fileName;
		bool        _cancel;
        DateTime    _dateTime;

        // initializes a new instance of a ZipEntryStreamWriter
		internal ZipEntryStreamWriter(C1ZipFile zipFile, string fileName, string entryName, DateTime dateTime, bool memory)
		{
			// create data stream
			Stream baseStream;
			if (memory)
			{
				_tempFile = null;
				baseStream = new MemoryStream();
			}
			else
			{
				_tempFile = zipFile._tempFileName;
                if (_tempFile == null || _tempFile.Length == 0)
                {
                    _tempFile = Path.GetTempFileName();
                }
				baseStream = new FileStream(_tempFile, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
			}

			// initialize
			_owner     = zipFile;
			_entryName = entryName;
			_fileName  = fileName;
            _dateTime  = dateTime;
			_cancel    = false;
			InitStream(baseStream, zipFile.CompressionLevel, false, true); // <<B26>>
			OwnsBaseStream = false;
		}

        // cancel the operation (causes Close not to add the stream to the base zip file) <<B4>>
        public void Cancel()
        {
            _cancel = true;
        }

        // close the stream, optionally add to zip file
        override public void Close()
        {
            // close compressed stream
            base.Close();

			// try adding compressed stream to zip file
			try
			{
				// add compressed stream to zip file <<B4>>
				if (!_cancel)
				{
					BaseStream.Position = 0;
					AddCompressed();
				}
			}
			finally // always close/delete temp file <<B35>>
			{
				// close base stream (memory/temp file)
				BaseStream.Close();

				// delete temporary file
				Stream fs = BaseStream as FileStream;
                if (fs != null)
                {
                    File.Delete(_tempFile);
                }
			}
        }
        
        // adds a stream that is already compressed to the zip file
        void AddCompressed()
        {
            // create a new entry, set its name
            var ze = new C1ZipEntry(_owner);
            ze.SetNameAndFlags(_fileName, _entryName, _dateTime);

            // if this entry already exists in the zip file, remove it
            var index = _owner.Entries.IndexOf(ze.FileName);
            if (index >= 0)
            {
                _owner.Remove(index);
            }

            // add the new entry to the zip file
            Stream zipStream = null;
            try
            {
                // open zip file
                zipStream = _owner.OpenInternal(_owner.FileName);

                // add the entry to the entry collection
                _owner._headers.Add(ze);

                // new entry goes where central dir used to be
                ze._offset = _owner._bytesBeforeZip + _owner._offset;

                // remove central dir by truncating the zip file just above the central dir
                zipStream.SetLength(ze._offset);

                // update sizes so the local header is written correctly
                ze._szComp = SizeCompressedLong + (ze.IsEncrypted ? C1CryptStream.ENCR_HEADER_LEN : 0);
                ze._szOriginal = SizeUncompressedLong;

                // write local header for the new entry
                zipStream.Position = ze._offset;
                ze.WriteLocalHeader(zipStream);
                
                // write compressed data into the zip file
                Stream data = BaseStream;
                data.Position = 0;

				// if entry is to be encrypted, get an encryptor 
				if (ze.IsEncrypted)
				{
					// build an encryptor
					C1CryptStream cryptStream = new C1CryptStream(ze, zipStream);

					// create the encrypted header
					byte[] buf = new byte[C1CryptStream.ENCR_HEADER_LEN];
					long crc = (long)(ze._modTime << 16);
					cryptStream.CryptCryptHeader(crc, buf);

					// write crypt header to the zip stream
					zipStream.Write(buf, 0, buf.Length);

					// write data
					C1ZipFile.StreamCopy(cryptStream, data, data.Length);

					// and write size
					// including size of encrypted header <<B8>>
                    ze._szComp = SizeCompressedLong + C1CryptStream.ENCR_HEADER_LEN;
				}
				else
				{
					C1ZipFile.StreamCopy(zipStream, data, data.Length);
                    ze._szComp = SizeCompressedLong;
				}

                // finish filling out entry information
                ze._szOriginal = SizeUncompressedLong;
                ze._crc32 = Checksum;

                // write the entry's local header again (this time with all information)
                zipStream.Position = ze._offset;
                ze.WriteLocalHeader(zipStream);

                // update zip's offset (the position where the central dir is located)
                zipStream.Position = zipStream.Length;
                _owner._offset = zipStream.Position;

                // and write central dir back into the zip file
                _owner.WriteCentralDir(zipStream);
            }
            finally // always close the zip file
            {
				_owner.CloseInternal(zipStream);
            }
        }
    }
}
