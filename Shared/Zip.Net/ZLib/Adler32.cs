using System;

namespace C1.C1Zip.ZLib
{
    internal class Adler32 : IChecksum
    {
        private long        _adler = 1L;
        private const int   BASE = 65521; // largest prime smaller than 65536
        private const int   NMAX = 5552;  // NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1
    
        public long checkSum(long value, byte[] buf, int index, int len)
        {
            if (value == 1 || buf == null) reset();
            if (buf != null) update(buf, index, len);
            return getValue();
        }
        private void reset() 
        {
            _adler = 1L;
        }
        private long getValue()
        {
            return _adler;
        }
        private void update(byte[] b, int off, int len) 
        {
			if (b == null || off < 0 || len < 0 || off + len > b.Length) 
			{
				string msg = StringTables.GetString("Bad parameters in call to Adler.update.");
				throw new ArgumentException(msg);
			}
        
            long s1 = _adler & 0xffff;
            long s2 = (_adler >> 16) & 0xffff;
        
            while (len > 0) 
            {
                int k = (len < NMAX) ? (len) : (NMAX);
                len -= k;
                while (k >= 16) 
                {
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    s2 += s1 += b[off] & 0xff; off++;
                    k -= 16;
                }
                while (k != 0) 
                {
                    s2 += s1 += b[off] & 0xff; off++;
                    k--;
                }
                s1 %= BASE;
                s2 %= BASE;
            }
            _adler = (s2 << 16) | s1;
        }
    }
}
