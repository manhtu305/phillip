using System;

namespace C1.C1Zip.ZLib
{
    /// <summary>
    /// Interface ICheckSum
    /// implemented by the Adler32 and CRC32 objects.
    /// Adler32 is a faster checksum used by the native ZLib
    /// CRC32   is the default checksum used in ZIP files
    /// </summary>
    internal interface IChecksum
    {
        long checkSum(long value, byte[] buf, int index, int len);
    }
}
