using System;
#if !COMPACT_FRAMEWORK
using System.Runtime.Serialization;
#endif

namespace C1.C1Zip.ZLib
{
#if EXPOSE_ZIP
	/// <summary>
	/// <b>ZStream</b> is the most flexible and hardest to use class in the <b>C1.C1Zip</b> assembly.
	/// It contains a C# implementation of ZLIB's zstream object, which is a general purpose 
	/// compressor and decompressor.
	/// </summary>
	/// <remarks>
	/// <para>In most cases, you should be able to use the <see cref="C1ZStreamReader"/> 
	/// and <see cref="C1ZStreamWriter"/> classes instead of <b>ZStream</b>. 
	/// These classes provide friendly and easy-to-use wrappers that hide the ZLIB complexity.</para>
	/// <para>
	/// Use <b>ZStream</b> only if you are familiar with ZLIB and need control over 
	/// the low-level aspects of the data compression or decompression process 
	/// (e.g., to provide your own buffers or compression dictionaries).</para>
	/// <para>
	/// If you choose to use <b>ZStream</b> directly and need technical support, 
	/// please check out the detailed documentation, sample, and articles available 
	/// at http://www.info-zip.org/ or http://www.gzip.org/, the official zlib sites.</para>
	/// <para>
	/// ZLIB is an open-source, patent-free library created by Jean-Loup Gailly and Mark Adler.</para>
	/// </remarks>
	public class ZStream
#else
    internal class ZStream
#endif
    {
        //--------------------------------------------------------------------------------
        #region ** internal constants

        // private stuff
        internal const int MAX_WBITS = 15;        // 32K LZ77 window
        internal const int DEF_WBITS = MAX_WBITS;
        //static int MAX_MEM_LEVEL = 9;

        // compression levels
        internal const int Z_NO_COMPRESSION = 0;
        internal const int Z_BEST_SPEED = 1;
        internal const int Z_BEST_COMPRESSION = 9;
        internal const int Z_DEFAULT_COMPRESSION = 6;

        // compression strategy
        internal const int Z_FILTERED = 1;
        internal const int Z_HUFFMAN_ONLY = 2;
        internal const int Z_DEFAULT_STRATEGY = 0;

        // deflate commands
        internal const int Z_NO_FLUSH = 0;
        internal const int Z_PARTIAL_FLUSH = 1;
        internal const int Z_SYNC_FLUSH = 2;
        internal const int Z_FULL_FLUSH = 3;
        internal const int Z_FINISH = 4;
        #endregion

        //--------------------------------------------------------------------------------
        #region ** public constants

        // error codes

        /// <summary>
        /// No error.
        /// </summary>
        public const int Z_OK = 0;
        /// <summary>
        /// End of stream detected.
        /// </summary>
        public const int Z_STREAM_END = 1;
        /// <summary>
        /// A preset dictionary is needed at this point.
        /// </summary>
        public const int Z_NEED_DICT = 2;
        /// <summary>
        /// File error.
        /// </summary>
        public const int Z_ERRNO = -1;
        /// <summary>
        /// Stream structure is inconsistent (input/output buffers are null for example).
        /// </summary>
        public const int Z_STREAM_ERROR = -2;
        /// <summary>
        /// Input data is corrupted (wrong format or checksum).
        /// </summary>
        public const int Z_DATA_ERROR = -3;
        /// <summary>
        /// Not enough memory.
        /// </summary>
        public const int Z_MEM_ERROR = -4;
        /// <summary>
        /// No progress possible or no room in output buffer.
        /// </summary>
        public const int Z_BUF_ERROR = -5;
        /// <summary>
        /// Incompatible ZLIB version.
        /// </summary>
        public const int Z_VERSION_ERROR = -6;
        #endregion

        //--------------------------------------------------------------------------------
        #region ** internal fields
        internal Deflate dstate;
        internal Inflate istate;
        internal int data_type;         // best guess about the data type: ascii or binary
        internal IChecksum _adler;		// checksum object: Adler32(for ZLIB) or CRC32(for zip)
        internal bool _crc32;			// sames as: _adler is CRC32
        #endregion

        //--------------------------------------------------------------------------------
        #region ** public fields
        /// <summary>
        /// Input buffer.
        /// </summary>
        public byte[] next_in;
        /// <summary>
        /// Position of cursor into input buffer.
        /// </summary>
        public int next_in_index;
        /// <summary>
        /// Number of bytes available in the input buffer.
        /// </summary>
        public int avail_in;
        /// <summary>
        /// Total number of input bytes read so far.
        /// </summary>
        public long total_in;
        /// <summary>
        /// Output buffer.
        /// </summary>
        public byte[] next_out;
        /// <summary>
        /// Position of cursor into the output buffer.
        /// </summary>
        public int next_out_index;
        /// <summary>
        /// Number of free bytes remaining in output buffer.
        /// </summary>
        public int avail_out;
        /// <summary>
        /// Total number of bytes output so far.
        /// </summary>
        public long total_out;
        /// <summary>
        /// Description of the last error (null if no errors).
        /// </summary>
        public string msg;
        /// <summary>
        /// Current checksum value (Adler or CRC32).
        /// </summary>
        public long adler;
        #endregion

        //--------------------------------------------------------------------------------
        #region ** ctors
        /// <summary>
        /// Initializes a new instance of the <b>ZStream</b> class using an Adler checksum.
        /// </summary>
        public ZStream()
        {
            _adler = new Adler32();
        }
        /// <summary>
        /// Initializes a new instance of the <b>ZStream</b> class.
        /// </summary>
        /// <param name="crc32"><b>True</b> to use a CRC32 checksum, <b>False</b> to use an Adler checksum.</param>
        /// <remarks>
        /// CRC32 checksums are the standard used in zip files. Adler checksums are the default
        /// used by ZLIB. Adler checksums are faster to calculate, but are not compatible with the zip format.
        /// </remarks>
        public ZStream(bool crc32)
        {
            // uses ZIP-compatible CRC32 or faster, ZLIB-compatible Adler32 checksum
            _crc32 = crc32;
            if (crc32)
                _adler = new CRC32();
            else
                _adler = new Adler32();
        }
        #endregion

        //--------------------------------------------------------------------------------
        #region ** public methods

        // inflate members

        /// <summary>
        /// Initializes the internal stream state for decompression.
        /// </summary>
        /// <returns>Zero on success, an error code on failure.</returns>
        /// <remarks>
        /// <para>The fields <see cref="next_in"/> and <see cref="avail_in"/> must be 
        /// initialized before by the caller.</para>
        /// <para><b>inflateInit</b> does not perform any decompression apart from reading the 
        /// zlib header if present: data decompression is done by the <see cref="inflate"/> 
        /// method. Therefore, the <b>next_in</b> and <b>avail_in</b> may be modified, but 
        /// <b>next_out</b> and <b>avail_out</b> are unchanged.</para>
        /// </remarks>
        public int inflateInit()
        {
            istate = new Inflate();
            return istate.inflateInit(this, DEF_WBITS);
        }
        /// <summary>
        /// Initializes the internal stream state for decompression.
        /// </summary>
        /// <param name="bits">Size of the LZ77 sliding compression window in bits (the default value is 15 bits).</param>
        /// <returns>Zero on success, an error code on failure.</returns>
        public int inflateInit(int bits)
        {
            istate = new Inflate();
            return istate.inflateInit(this, bits);
        }
        /// <summary>
        /// Decompresses as much data as possible until the input buffer is exhausted or 
        /// the output buffer is full.
        /// </summary>
        /// <param name="flush">How to flush data into the output buffer (default value is 2).</param>
        /// <returns>
        /// <b>Z_OK</b> if some progress has been made (more input processed or more output produced), 
        /// <b>Z_STREAM_END</b> if the end of the compressed data has been reached and all 
        /// uncompressed output has been produced, 
        /// <b>Z_NEED_DICT</b> if a preset dictionary is needed at this point, 
        /// <b>Z_DATA_ERROR</b> if the input data was corrupted (input stream not conforming to 
        /// the zlib format or incorrect checksum), 
        /// <b>Z_STREAM_ERROR</b> if the stream structure was inconsistent (for example if 
        /// <b>next_in</b> or <b>next_out</b> was null), 
        /// <b>Z_MEM_ERROR</b> if there was not enough memory,
        /// <b>Z_BUF_ERROR</b> if no progress is possible or if there was not enough room in the 
        /// output buffer when <b>Z_FINISH</b> is used.
        /// </returns>
        /// <remarks>
        /// <para><b>inflate</b> performs one or both of the following actions:</para>
        /// 
        /// <para>1. Decompress more input starting at <b>next_in</b> and update <b>next_in</b>
        /// and <b>avail_in</b> accordingly. If not all input can be processed (because there 
        /// is not enough room in the output buffer), <b>next_in</b> is updated and processing 
        /// will resume at this point for the next call to inflate.</para>
        /// 
        /// <para>2. Provide more output starting at <b>next_out</b> and update <b>next_out</b>
        /// and <b>avail_out</b> accordingly. <b>inflate</b> provides as much output as 
        /// possible, until there is no more input data or no more space in the output buffer.</para>
        /// 
        /// <para>Before the call to inflate, the application should ensure that at least one of the 
        /// actions is possible, by providing more input and/or consuming more output, and 
        /// updating the <b>next_*</b> and <b>avail_*</b> values accordingly.</para>
        /// 
        /// <para>If <b>inflate</b> returns Zero and <b>avail_out</b> == 0, it must be called again 
        /// after making room in the output buffer because there might be more output pending.</para>
        /// 
        /// <para>The application can consume the uncompressed output when it wants, for example when 
        /// the output buffer is full (<b>avail_out</b> == 0), or after each call of <b>inflate</b>.</para>
        /// 
        /// <para>This method may introduce some output latency (reading input without producing 
        /// any output) except when forced to flush.</para>
        /// </remarks>
        public int inflate(int flush)
        {
            if (istate == null) return Z_STREAM_ERROR;
            return Inflate.inflate(this, flush);
        }
        /// <summary>
        /// Frees all dynamically allocated data structures for this stream, 
        /// discards any unprocessed input, and does not flush any pending output.
        /// </summary>
        /// <returns>Zero on success, an error code on failure.</returns>
        public int inflateEnd()
        {
            if (istate == null) return Z_STREAM_ERROR;
            int ret = istate.inflateEnd(this);
            istate = null;
            return ret;
        }
        /// <summary>
        /// Skips invalid compressed data until a full flush point is found, 
        /// or until all available input is skipped. No output is provided.
        /// </summary>
        /// <returns>Zero on success, an error code on failure.</returns>
        public int inflateSync()
        {
            if (istate == null) return Z_STREAM_ERROR;
            return istate.inflateSync(this);
        }
        /// <summary>
        /// Initializes the decompression dictionary from the given uncompressed byte sequence.
        /// </summary>
        /// <param name="dictionary">Data in the dictionary.</param>
        /// <param name="dictLength">Number of bytes in the dictionary.</param>
        /// <returns>Zero on success, an error code on failure.</returns>
        /// <remarks>
        /// <para>This method must be called immediately after a call of <see cref="inflate"/> 
        /// if this call returned <b>Z_NEED_DICT</b>. The dictionary chosen by the compressor 
        /// can be determined from the <b>Adler32</b> value returned by this call to <b>inflate</b>.</para>
        /// <para>The compressor and decompressor must use exactly the same dictionary 
        /// (see the <see cref="deflateSetDictionary"/> method).</para>
        /// </remarks>
        public int inflateSetDictionary(byte[] dictionary, int dictLength)
        {
            if (istate == null) return Z_STREAM_ERROR;
            return Inflate.inflateSetDictionary(this, dictionary, dictLength);
        }

        // ** deflate members

        /// <summary>
        /// Initializes the internal stream state for compression.
        /// </summary>
        /// <param name="level">Compression level between zero and nine (0-9).</param>
        /// <returns>Zero on success, an error code on failure.</returns>
        /// <remarks>
        /// <para>Compression level 1 gives best speed, 9 gives best compression.</para>
        /// <para>Compression level zero gives no compression at all (the input data is simply copied a block at a time).</para>
        /// <para>The default compression level is 6, which provides a compromise between speed and compression.</para>
        /// </remarks>
        public int deflateInit(int level)
        {
            dstate = new Deflate();
            return dstate.deflateInit(this, level, MAX_WBITS);
        }
        /// <summary>
        /// Initializes the internal stream state for compression.
        /// </summary>
        /// <param name="level">Compression level between zero and nine (0-9).</param>
        /// <param name="bits">Size of the LZ77 sliding compression window in bits (the default value is 15 bits).</param>
        /// <returns>Zero on success, an error code on failure.</returns>
        /// <remarks>
        /// <para>Compression level 1 gives best speed, 9 gives best compression.</para>
        /// <para>Compression level zero gives no compression at all (the input data is simply copied a block at a time).</para>
        /// <para>The default compression level is 6, which provides a compromise between speed and compression.</para>
        /// </remarks>
        public int deflateInit(int level, int bits)
        {
            dstate = new Deflate();
            return dstate.deflateInit(this, level, bits);
        }
        /// <summary>
        /// Compresses as much data as possible, and stops when the input buffer becomes empty or the 
        /// output buffer becomes full.
        /// </summary>
        /// <param name="flush">Non-zero to force some data to be flushed into the output buffer.</param>
        /// <returns>Zero on success, an error code on failure.</returns>
        /// <remarks>
        /// <para><b>deflate</b> performs one or both of the following actions:</para>
        /// <para>1. Compress more input starting at <b>next_in</b> and update <b>next_in</b> and 
        /// <b>avail_in</b> accordingly. If not all input can be processed (because there is not 
        /// enough room in the output buffer), <b>next_in</b> and <b>avail_in</b> are updated and 
        /// processing will resume at this point for the next call to <b>deflate</b>.</para>
        /// <para>2. Provide more output starting at <b>next_out</b> and update <b>next_out</b> and 
        /// <b>avail_out</b> accordingly. This action is forced if the parameter <paramref name="flush"/> is 
        /// non zero. Forcing flush frequently degrades the compression ratio, so this parameter 
        /// should be set only when necessary (in interactive applications). Some output may be 
        /// provided even if flush is not set</para>
        /// <para>This method may introduce some output latency (reading input without producing 
        /// any output) except when forced to flush.</para>
        /// <para>If <b>deflate</b> returns with <b>avail_out</b> == 0, this method must be called 
        /// again with the same value of the <paramref name="flush"/> parameter and more output space 
        /// until the flush is complete (<b>deflate</b> returns with <b>avail_out</b> != 0).</para>
        /// </remarks>
        public int deflate(int flush)
        {
            if (dstate == null) return Z_STREAM_ERROR;
            return dstate.deflate(this, flush);
        }
        /// <summary>
        /// Frees all dynamically allocated data structures for this stream, 
        /// discards any unprocessed input, and does not flush any pending output.
        /// </summary>
        /// <returns>Zero on success, an error code on failure.</returns>
        public int deflateEnd()
        {
            if (dstate == null) return Z_STREAM_ERROR;
            int ret = dstate.deflateEnd();
            dstate = null;
            return ret;
        }
        /// <summary>
        /// Dynamically updates the compression level and compression strategy.
        /// </summary>
        /// <param name="level">Compression level between zero and nine (0-9).</param>
        /// <param name="strategy">Compression strategy (0-2).</param>
        /// <returns></returns>
        public int deflateParams(int level, int strategy)
        {
            if (dstate == null) return Z_STREAM_ERROR;
            return dstate.deflateParams(this, level, strategy);
        }
        /// <summary>
        /// Initializes the compression dictionary from the given byte sequence without 
        /// producing any compressed output.
        /// </summary>
        /// <param name="dictionary">Data in the dictionary.</param>
        /// <param name="dictLength">Number of bytes in the dictionary.</param>
        /// <returns>Zero on success, an error code on failure.</returns>
        /// <remarks>
        /// <para>This method must be called immediately after <see cref="deflateInit(int)"/>,
        /// before any call to <see cref="deflate"/>.</para>
        /// <para>The compressor and decompressor must use exactly the same dictionary 
        /// (see <see cref="inflateSetDictionary"/>).</para>
        /// </remarks>
        public int deflateSetDictionary(byte[] dictionary, int dictLength)
        {
            if (dstate == null) return Z_STREAM_ERROR;
            return dstate.deflateSetDictionary(this, dictionary, dictLength);
        }
        //		public void free()
        //		{
        //			next_in  = null;
        //			next_out = null;
        //			msg      = null;
        //			_adler   = null;
        //		}
        #endregion

        //--------------------------------------------------------------------------------
        #region ** internal methods

        // Flush as much pending output as possible. All deflate() output goes
        // through this function so some applications may wish to modify it
        // to avoid allocating a large strm->next_out buffer and copying into it.
        // (See also read_buf()).
        internal void flush_pending()
        {
            int len = dstate.pending;

            if (len > avail_out) len = avail_out;
            if (len == 0) return;

            System.Array.Copy(dstate.pending_buf, dstate.pending_out, next_out, next_out_index, len);

            next_out_index += len;
            dstate.pending_out += len;
            total_out += len;
            avail_out -= len;
            dstate.pending -= len;
            if (dstate.pending == 0) dstate.pending_out = 0;
        }

        // Read a new buffer from the current input stream, update the adler32
        // and total number of bytes read.  All deflate() input goes through
        // this function so some applications may wish to modify it to avoid
        // allocating a large strm->next_in buffer and copying from it.
        // (See also flush_pending()).
        internal int read_buf(byte[] buf, int start, int size)
        {
            int len = avail_in;

            if (len > size) len = size;
            if (len == 0) return 0;

            avail_in -= len;

            // compute checksum
            if (dstate.noheader == 0 || _crc32)
                adler = _adler.checkSum(adler, next_in, next_in_index, len);

            System.Array.Copy(next_in, next_in_index, buf, start, len);
            next_in_index += len;
            total_in += len;
            return len;
        }
        #endregion
    }

    /// <summary>
    /// The exception that is thrown when reading or writing to a compressed stream fails.
    /// </summary>
#if COMPACT_FRAMEWORK
	public class ZStreamException : Exception 
	{
        /// <summary>
        /// Initializes a new instance of a <see cref="ZStreamException"/>.
        /// </summary>
		public	ZStreamException() {}
        /// <summary>
        /// Initializes a new instance of a <see cref="ZStreamException"/>.
        /// </summary>
        /// <param name="msg">Message that describes the exception.</param>
		public	ZStreamException(string msg) : base(msg) {}
        /// <summary>
        /// Initializes a new instance of a <see cref="ZStreamException"/>.
        /// </summary>
        /// <param name="msg">Message that describes the exception.</param>
        /// <param name="x">Inner exception.</param>
		public	ZStreamException(string msg, Exception x) : base(msg, x) {}
	}
#else
#if EXPOSE_ZIP
	[Serializable()] // <<fxcop>>
	public class ZStreamException : ApplicationException
#else
    [Serializable()] // <<fxcop>>
    internal class ZStreamException : ApplicationException
#endif
    {
        internal ZStreamException() { }
        internal ZStreamException(string msg) : base(msg) { }
        internal ZStreamException(string msg, Exception x) : base(msg, x) { }
        internal ZStreamException(SerializationInfo si, StreamingContext sc) : base(si, sc) { }
    }
#endif
}
