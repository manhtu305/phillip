//----------------------------------------------------------------------------
// C1\C1Zip\C1ZipFile.cs
//
// C1ZipFile is a class that allows you to create and manage zip files.
//
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status           Date            By                 Comments
//----------------------------------------------------------------------------
// Created          Nov 2001        Rodrigo             -
// Updated          Dec 2011        Bernardo            zip64 extensions
//----------------------------------------------------------------------------
using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Globalization;
using System.Runtime.InteropServices;

namespace C1.C1Zip
{
	using C1.C1Zip.ZLib;

#if EXPOSE_ZIP
	/// <summary>
	/// Used for creating, opening, and managing zip files.
	/// </summary>
	/// <remarks>
	/// <para>Use the <see cref="Open(string)"/> or <see cref="Create(string)"/> methods to 
	/// associate the C1Zip file object with a zip file on disk. Then use the 
	/// <see cref="Entries"/> property to add, remove, retrieve, or inspect 
	/// individual entries in the zip file.</para>
	/// <para><see cref="C1ZipFile"/> can only be used with standard zip files. The component
	/// does not support other similar formats such as gzip, zip2, tar, or rar.</para>
	/// <para>The standard zip file imposes some limitations on the size of each entry. 
	/// You cannot use it to compress files larger than 4 gigabytes (uint.MaxValue).</para>
	/// </remarks>
	/// <example>
	/// The code below creates a zip file called <b>sources.zip</b> and adds all 
	/// files with a "cs" extension to the zip file:
	/// <code>
	/// // get path for zip file and files to compress
	/// string path = Application.ExecutablePath;
	/// int pos = path.IndexOf(@"\bin");
	/// path = path.Substring(0, pos + 1);
	/// 
	/// // create a zip file
	/// C1ZipFile zip = new C1ZipFile();
	/// zip.Create(path + "source.zip");
	/// 
	/// // add all files with extension cs to the zip file
	/// foreach (string fileName in Directory.GetFiles(path, "*.cs"))
	///		zip.Entries.Add(fileName);
	///		
	/// // show result
	/// foreach (C1ZipEntry ze in zip.Entries)
	/// {
	///		Console.WriteLine("{0} {1:#,##0} {2:#,##0}", 
	///			ze.FileName, ze.SizeUncompressed, ze.SizeCompressed);
	/// }
	/// </code>
    /// </example>
#if false && !COMPACT_FRAMEWORK
	[
	ClassInterface(ClassInterfaceType.AutoDual),
	Guid("C5AE6DB9-02C1-4240-9B2D-D0F76A23E32B")
	]
#endif
    public class C1ZipFile : IDisposable
#else
	#if WIJMOCONTROLS
	[SmartAssembly.Attributes.DoNotObfuscate]
	#endif
    internal class C1ZipFile : IDisposable
#endif
    {
		//--------------------------------------------------------------------------------
		#region ** constants

        internal static byte[] CENTRAL_DIR_SIGN    = new byte[] { 0x50, 0x4b, 0x05, 0x06 };
        internal static byte[] CENTRAL_DIR64_SIGN  = new byte[] { 0x50, 0x4b, 0x06, 0x06 };
        internal static byte[] CENTRAL_DIR64L_SIGN = new byte[] { 0x50, 0x4b, 0x06, 0x07 };
        internal const int     BUFFERSIZE          = 256 * 1024;   // 256k buffer// 32 * 1024; 32k buffer
		internal const  int    ZIPFILEHEADERSIZE   = 22;           // stored at the end of the zip file
		internal const  char   DIRSEPCHAR          = '/';          // used when entry names include a path
															        // ('/' for Windows Commander compatibility)
        internal const string  ERR_TOOLARGE_UNCOMPRESSED =
            "Size is too large to be represented as an Int32, use SizeUncompressedLong instead.";
        internal const string  ERR_TOOLARGE_COMPRESSED =
            "Size is too large to be represented as an Int32, use SizeCompressedLong instead.";
        internal const string  ERR_TOOLARGE_LENGTH =
            "Length is too large to be represented as an Int32, use LengthLong instead.";
        internal const string  ERR_TOOLARGE_POSITION =
            "Position is too large to be represented as an Int32, use PositionLong instead.";

		#endregion

		//--------------------------------------------------------------------------------
		#region ** fields

		internal uint                   _size;              // size of the central directory
		internal long                   _offset;            // offset to the central directory
		internal uint                   _bytesBeforeZip;    // padding on the top of the zip file (usually zero)
		internal string                 _fileName;          // name of the zip file
		internal string                 _comment;           // comment attached to the zip file
		internal string                 _password;          // used for encryption/decryption
		internal List<C1ZipEntry>       _headers;           // list of entries in the zip file
		internal bool			        _owReadOnly;        // overwrite when extracting
        internal bool                   _owHidden;	        // overwrite when extracting
        internal bool                   _owSystem;	        // overwrite when extracting
		internal CompressionLevelEnum   _level;             // compression level to use when adding new files
		internal C1ZipEntryCollection   _entries;

		// work buffer (not static to be thread-safe) <<B34>>
		internal byte[] _buf = new byte[BUFFERSIZE];               

		// new features
		internal Stream			_batchStream;		// batch stream, remains open between OpenBatch/CloseBatch
		internal int			_memoryThreshold;	// largest stream to be compressed using memory
		internal string			_tempFileName;		// name of temp file to use while compressing (null for default)

#if EXPOSE_ZIP
		/// <summary>
		/// Fired while data is being read from or written into a zip file.
		/// </summary>
		/// <remarks>
		/// This event is typically used to update the application UI during lengthy
		/// operations. It can also be used to cancel the operations.
		///</remarks>
		///<example>
		/// The code below writes messages to the output window while the application
		/// compresses files.
		/// <code>
		/// void Compress()
		/// {
		///		// create zip file
		///		C1Zip zip = new C1Zip();
		///		zip.Create(zipFileName);
		/// 
		///		// connect event handler
		///		zip.Progress += new ZipProgressEventHandler(zip_Progress);
		/// 
		///		// add files
		///		foreach (string fileName in Directory.GetFiles(path, "*.*"))
		///			zip.Entries.Add(fileName);
		///	}
		/// 
		/// // print messages while files are compressed
		/// void zip_Progress(object sender, ZipProgressEventArgs e)
		/// {
		///		Console.WriteLine("Compressing {0}, {1:p0} done",
		///			e.FileName, e.Position/(float)e.FileLength);
		///	}
		/// </code>
		///</example>
		public event ZipProgressEventHandler Progress;
#endif

		#endregion

		//--------------------------------------------------------------------------------
		#region ** ctors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZipFile"/> class.
		/// </summary>
		public C1ZipFile()
		{
			_level    = CompressionLevelEnum.DefaultCompression;
			_fileName = string.Empty;
			_comment  = string.Empty;
			_password = string.Empty;
            _headers = new List<C1ZipEntry>();
			_entries = new C1ZipEntryCollection(this);

			_memoryThreshold = 512 * 1024;
			_tempFileName = null;
			_batchStream  = null;
		}
        /// <summary>
        /// Initializes a new instance of the <see cref="C1ZipFile"/> class and creates or opens
        /// a zip file associated with this new instance.
        /// </summary>
        /// <param name="fileName">The name of the zip file to open or create.</param>
        /// <param name="create">True to create a new zip file, false to open an existing file.</param>
        /// <remarks>
        /// <para>If <paramref name="create"/> is true and the zip file already exists, it is overwritten
        /// with a new empty file.</para>
        /// <para>If <paramref name="create"/> is false and the zip file already exists, the existing file 
        /// is opened.</para>
        /// </remarks>
        public C1ZipFile(string fileName, bool create)
            : this()
        {
            if (create)
            {
                this.Create(fileName);
            }
            else
            {
                this.Open(fileName);
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="C1ZipFile"/> class and opens a zip file associated 
        /// with this new instance.
        /// </summary>
        /// <param name="fileName">The name of the zip file to open.</param>
        /// <remarks>
        /// If the file does not exist, a new empty file is created.
        /// </remarks>
        public C1ZipFile(string fileName) : this(fileName, false) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="C1ZipFile"/> class and opens a zip stream associated 
        /// with this new instance.
        /// </summary>
        /// <param name="stream"><see cref="Stream"/> that contains the zip data.</param>
        /// <param name="create">Whether to initialize the stream with an empty zip header or open an existing
        /// zip file in the stream.</param>
        public C1ZipFile(Stream stream, bool create)
            : this()
        {
            if (create)
            {
                this.Create(stream);
            }
            else
            {
                this.Open(stream);
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="C1ZipFile"/> class and opens a zip stream associated 
        /// with this new instance.
        /// </summary>
        /// <param name="stream"><see cref="Stream"/> that contains the zip data.</param>
        public C1ZipFile(Stream stream) : this(stream, false) { }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** IDisposable

		void IDisposable.Dispose() // <<B32>>
		{
			CloseBatch();
		}

		#endregion

		//--------------------------------------------------------------------------------
		#region ** public stuff

		/// <summary>
		/// Opens an existing zip file.
		/// </summary>
		/// <param name="fileName">The name of an existing zip file, including the path.</param>
		/// <remarks>
		/// <para>This method checks that the zip file exists and is a valid zip file, then
		/// reads the zip file directory into the <see cref="Entries"/> collection.
		/// The zip file is then closed, and can be used by other applications. There is no
		/// need to close the zip file explicitly.</para>
		/// </remarks>
		public void Open(string fileName)
		{
			// if the file doesn't exist, create a new one <<B18>>
			if (!File.Exists(fileName))
			{
				Create(fileName);
				return;
			}

			// it exists, so open it
			Stream fs = null;
			try
			{
				fs = OpenInternal(fileName, true, true);
			}
			finally // always close the file
			{
				CloseInternal(fs);
			}
		}
        /// <summary>
        /// Creates an empty zip file on disk.
        /// </summary>
        /// <param name="fileName">The name of the zip file to create, including the path.</param>
        /// <remarks>
        /// If a file by the same name already exists, it is deleted before the new one
        /// is created.
        /// </remarks>
        public void Create(string fileName)
        {
            // reset all data members
            Close();

            // write an empty zip file on disk
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                WriteCentralDir(fs);
            }

            // save file name
            _fileName = fileName;
        }
        /// <summary>
        /// Opens an existing zip file stored in a stream.
        /// </summary>
        /// <param name="stream"><see cref="Stream"/> that contains a zip file.</param>
        /// <remarks>
        /// <para>This method allows you to open and work with a zip file stored in a stream
        /// instead of in an actual file.</para>
        /// <para>Typical usage scenarios for this are zip files stored as application resources 
        /// or in binary database fields.</para>
        /// </remarks>
        /// <example>
        /// <para>The example below loads information from a zip file stored in an embedded resource. 
        /// To embed a zip file in an application, follow these steps:</para>
        /// <para>1) Right-click the project node in Visual Studio, select the <b>Add | Add Existing Item...</b> menu option.</para>
        /// <para>2) Select a zip file to add to the project as an embedded resource.</para>
        /// <para>3) Select the newly added file and make sure the <b>Build Action</b> property is set to "Embedded Resource".</para>
        /// <code>
        /// // get Stream from application resources
        /// System.Reflection.Assembly a = this.GetType().Assembly;
        /// using (Stream stream = a.GetManifestResourceStream("MyApp.test.zip"))
        /// {
        ///   // open C1ZipFile on the stream
        ///   zip.Open(stream);
        /// 
        ///   // enumerate the entries in the zip file,
        ///   foreach (C1ZipEntry ze in zip.Entries)
        ///   {
        ///     // show entries that have a 'txt' extension.
        ///     if (ze.FileName.ToLower().EndsWith(".txt"))
        ///     {
        ///       using (var sr = new StreamReader(ze.OpenReader()))
        ///       {
        ///         MessageBox.Show(sr.ReadToEnd(), ze.FileName);
        ///       }
        ///     }
        ///   }
        /// }
        /// </code>
        /// </example>
        public void Open(Stream stream)
        {
            Close();
            CloseBatch();
            OpenInternal(stream, true);
            _batchStream = stream;
        }
        /// <summary>
        /// Creates a new zip file in a stream.
        /// </summary>
        /// <param name="stream"><see cref="Stream"/> that will contain the new zip file.</param>
        /// <example>
        /// The code below creates a new <see cref="C1ZipFile"/> on a memory stream, then adds
        /// several files to it. Finally, the code gets the zipped data out as an array of bytes,
        /// which could be stored in a database for example.
        /// <code>
        /// // create zip on a stream
        /// MemoryStream msZip = new MemoryStream();
        /// C1ZipFile zip = new C1ZipFile(msZip, true);
        /// 
        /// // add some entries to it
        /// foreach (string f in Directory.GetFiles(@"c:\WINDOWS\Web\Wallpaper"))
        /// {
        ///   zip.Entries.Add(f);
        /// }
        /// 
        /// // get zipped data out as a byte array
        /// byte[] zipData = msZip.ToArray();
        /// </code>
        /// </example>
        public void Create(Stream stream)
        {
            Close();
            CloseBatch();
            WriteCentralDir(stream);
            _batchStream = stream;
        }
        /// <summary>
        /// Resets all data members of the <see cref="C1ZipFile"/> object.
		/// </summary>
		/// <remarks>
		/// Disk files are automatically closed by <b>C1Zip</b>. You only need to use this
        /// method if you want to break the connection between a <see cref="C1ZipFile"/> class 
        /// and a physical zip file.
		/// </remarks>
		public void Close()
		{
			_size             = 0;
			_offset           = 0;
			_bytesBeforeZip   = 0;
			_fileName         = string.Empty;
			_comment          = string.Empty;
			_password         = string.Empty;
			_headers.Clear();
		}
		/// <summary>
		/// Refreshes all data members by re-opening the current zip file.
		/// </summary>
		/// <remarks>
		/// This method is useful in instances where other applications may have changed
		/// the zip file and you want to make sure the information in the <see cref="Entries"/>
		/// collection is up to date.
		/// </remarks>
		public void Refresh()
		{
			Open(_fileName);
		}
		/// <summary>
		/// Gets the name of the current zip file.
		/// </summary>
		public string FileName
		{
			get { return _fileName; }
		}
		/// <summary>
		/// Gets or sets the password to use when adding or retrieving entries from the 
		/// zip file.
		/// </summary>
		/// <remarks>
        /// <para>If the <see cref="Password"/> property is set to a non-empty string, any entries 
        /// added to the zip file will be encrypted and protected by the password. To extract these 
        /// entries later, the same password must be used.</para>
		/// <para>The password applies to all entries from the moment it is set. If you set the 
		/// password to a non-empty string and then add several entries to the zip file, all
		/// entries will use the same password.</para>
        /// <para>Although <b>C1Zip</b> supports Unicode characters in passwords, several popular zip 
		/// utilities do not. To ensure your encrypted zip files can be opened with third-party
		/// utilities, use passwords that consist of ASCII characters only.</para>
		/// </remarks>
		public string Password
		{
			get { return _password;  }
			set { _password = (value != null)? value: string.Empty; }
		}
		/// <summary>
		/// Gets or sets a comment associated with the current zip file.
		/// </summary>
		public string Comment
		{
			get { return _comment; }
			set 
			{
				Stream fs = null;
				try
				{
					// open zip file
					fs = OpenInternal(FileName);

					// remove central dir by truncating the zip file just above the central dir
					fs.SetLength(_bytesBeforeZip + _offset);

					// set comment
					if (value == null) value = string.Empty;
					_comment = value;

					// write central dir back into the zip file
					WriteCentralDir(fs);
				}
				finally // always close the file
				{
					CloseInternal(fs);
				}
			}
		}
		/// <summary>
		/// Gets or sets the compression level to use when adding entries to the zip file.
		/// </summary>
		/// <remarks>
		/// Higher compression settings create smaller files, but take longer to process. The
        /// default setting (<see cref="CompressionLevelEnum.DefaultCompression"/>) provides 
        /// a good trade-off between compression and speed.
		/// </remarks>
		public CompressionLevelEnum CompressionLevel
		{
            get { return _level; }
			set { _level = value; }
		}
		/// <summary>
        /// Gets a <see cref="C1ZipEntryCollection"/> that contains the entries in the zip file.
		/// </summary>
		/// <remarks>
        /// The <see cref="Entries"/> collection is used to enumerate the entries in the zip file,
		/// and also to add, remove, and expand entries.
		/// </remarks>
#if WIJMOCONTROLS
		[SmartAssembly.Attributes.DoNotObfuscate]
#endif
		public C1ZipEntryCollection Entries
		{
#if WIJMOCONTROLS
			[SmartAssembly.Attributes.DoNotObfuscate]
#endif
			get { return _entries; }
		}
		/// <summary>
		/// Tests whether a file is a valid zip file.
		/// </summary>
		/// <param name="fileName">Name of the file to test.</param>
		/// <returns>True if the file exists and is a valid zip file, false otherwise.</returns>
		public static bool IsZipFile(string fileName)
		{
			if (!File.Exists(fileName))
			{
				return false;
			}
			try
			{
                var test = new C1ZipFile();
                test.Open(fileName);
                return true;
			}
			catch 
			{
				return false;
			}
		}
        /// <summary>
        /// Tests whether a stream contains a valid zip file.
        /// </summary>
        /// <param name="stream"><see cref="Stream"/> to test.</param>
        /// <returns>True if <paramref name="stream"/> contains a valid zip file, false otherwise.</returns>
        public static bool IsZipFile(Stream stream)
        {
            // trivial cases
            if (stream == null || stream.Length < ZIPFILEHEADERSIZE || !stream.CanSeek)
            {
                return false;
            }

            // try opening it...
            var streamPosition = stream.Position;
            try
            {
                var test = new C1ZipFile();
                test.Open(stream);
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                stream.Position = streamPosition;
            }
        }
        /// <summary>
		/// Determines whether the component should overwrite read-only files when extracting 
		/// entries from the zip file.
		/// </summary>
		public bool OverwriteReadOnly
		{
			get { return _owReadOnly; }
			set { _owReadOnly = value; }
		}
		/// <summary>
		/// Determines whether the component should overwrite hidden files when extracting 
		/// entries from the zip file.
		/// </summary>
		public bool OverwriteHidden
		{
			get { return _owHidden; }
			set { _owHidden = value; }
		}
		/// <summary>
		/// Determines whether the component should overwrite system files when extracting 
		/// entries from the zip file.
		/// </summary>
		public bool OverwriteSystem
		{
			get { return _owSystem; }
			set { _owSystem = value; }
		}
        /// <summary>
        /// Determines whether file names and comments should be stored in UTF8 format.
        /// </summary>
        /// <remarks>
        /// <para>The Zip specification does not specify what character encoding to be used for the 
        /// embedded file names and comments. The original IBM PC character encoding set, 
        /// commonly referred to as IBM Code Page 437, is supposed to be the only encoding supported.
        /// Because of this, some zip utilities will not allow storing file names or comments with 
        /// Unicode characters. Others do allow it, but those file names and comments may not be
        /// read correctly by other utilities.</para>
        /// <para>Setting the <b>UseUtf8Encoding</b> to true causes C1Zip to store and retrieve
        /// file names and comments using the Utf8 encoding, which allows Unicode characters. The
        /// caveat is that Unicode file names may not be read correctly by other zip utilities.</para>
        /// </remarks>
        public bool UseUtf8Encoding { get; set; }

        // gets the Encoding used to encode file names and comments
        internal Encoding Encoding
        {
            get { return UseUtf8Encoding ? Encoding.UTF8 : ZipEncoding.Encoding; }
        }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** new features in 2005

		/// <summary>
		/// Opens the zip file for multiple operations.
		/// </summary>
		/// <remarks>
		/// <para>By default, <see cref="C1ZipFile"/> opens and closes the zip file 
		/// automatically whenever entries are added or removed.</para>
		/// <para>This can cause delays in systems that have certain types of anti-virus
		/// software installed, or in situations where you want to add a large number of 
		/// relatively small entries. In these cases, use the <see cref="OpenBatch"/> and 
		/// <see cref="CloseBatch"/> methods to keep the zip file open until the entire 
		/// operation is concluded.</para>
		/// <para>Use a finally clause to ensure that the <see cref="CloseBatch"/> 
		/// method is called even if an exception occurs.</para>
		/// </remarks>
		/// <example>
		/// The code below opens a zip file, adds several entries to it, then closes 
		/// the file:
		/// <code>
		/// C1ZipFile zip = new C1ZipFile();
		/// zip.Open(myzipfile);
		/// try
		/// {
		///		zip.OpenBatch();
		///		foreach (string fileName in Directory.GetFiles(path, "*.*"))
		///			zip.Entries.Add(fileName);
		/// }
		/// finally
		/// {
		///		zip.CloseBatch();
		/// }
		/// </code>
		/// </example>
		public void OpenBatch()
		{
			CloseBatch();
			_batchStream = OpenInternal(FileName);
		}
		/// <summary>
		/// Closes a zip file after it was opened with a call to the 
		/// <see cref="OpenBatch"/> method.
		/// </summary>
		/// <remarks>
		/// See the <see cref="OpenBatch"/> method for a complete description 
		/// and a sample.
		/// </remarks>
		public void CloseBatch()
		{
            if (_batchStream != null)
            {
                _batchStream.Close();
                _batchStream = null;
            }
		}
		/// <summary>
		/// Gets or sets the size of the largest stream to be compressed in memory.
		/// </summary>
		/// <remarks>
		/// <para><see cref="C1ZipFile"/> compresses entries into temporary streams before
		/// adding them to the zip file.</para>
		/// <para>Entries with fewer than <see cref="MemoryThreshold"/> bytes are compressed
		/// using a temporary memory stream.</para>
		/// <para>Entries with more than <see cref="MemoryThreshold"/> bytes are compressed
		/// using a temporary file. You can control the location of the temporary file using
		/// the <see cref="TempFileName"/> property.</para>
		/// </remarks>
		public int MemoryThreshold
		{
			get { return _memoryThreshold; }
			set { _memoryThreshold = value; }
		}
		/// <summary>
		/// Gets or sets the name of the temporary file to use when adding entries to the zip file.
		/// </summary>
		/// <remarks>
		/// <para><see cref="C1ZipFile"/> creates temporary streams while adding entries to a zip file.
		/// These temporary streams can be memory-based or disk-based, depending on the size of the
		/// entry and on the setting of the <see cref="MemoryThreshold"/> property.</para>
		/// If a temporary file is used, <see cref="C1ZipFile"/> you can control its location by 
		/// setting the <see cref="TempFileName"/> property. If you don't select a path for the 
		/// temporary file, <see cref="C1ZipFile"/> will create one automatically using the 
		/// <see cref="Path.GetTempFileName"/> method.
		/// </remarks>
		public string TempFileName
		{
			get { return _tempFileName; }
			set { _tempFileName = value; }
		}

		#endregion

		//--------------------------------------------------------------------------------
		#region ** internal stuff

		// add new file/folder to the Zip file
        internal void Add(string fileName, string entryName, DateTime dateTime)
		{
			if (File.Exists(fileName)) // add file entry to Zip file
			{
				using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					Add(fs, entryName, dateTime);
				}
			}
			else if (Directory.Exists(fileName)) // add folder entry to zip file
			{
                var dstStream = OpenWriter(fileName, entryName, dateTime, true);
				dstStream.Close();
			}
			else 
			{
				var msg = StringTables.GetString("File not found: '{0}'.");
				msg = string.Format(msg, fileName);
				throw new FileNotFoundException(msg);
			}
		}

		// add a new stream to the Zip file
        internal void Add(Stream srcStream, string entryName, DateTime dateTime)
		{
            // make sure zip file has been opened or created
            if (FileName == null || FileName.Length == 0)
            {
                if (_batchStream == null)
                {
                    var msg = StringTables.GetString("Zip file is not open.");
                    throw new ArgumentException(msg);
                }
            }

			// get file name from stream if we can
			string fileName = null;
#if !SILVERLIGHT
			var fs = srcStream as FileStream;
            if (fs != null)
            {
                try
                {
                    fileName = fs.Name;
                }
                catch
                {
                    // not always allowed...
                }
            }

            // use memory or disk buffer, depending on the amount of data
            var memory = srcStream.Length < _memoryThreshold;
#else
            // always use memory buffers in Silverlight
            var memory = true;
#endif
            // create a stream writer
            using (var dstStream = OpenWriter(fileName, entryName, dateTime, memory))
            {
                // read source, write into destination stream
                // the source stream could be compressed, so set len to the maximum 
                // possible value to ensure the whole stream will be read <<B94>> TFS 23671
                var len = long.MaxValue; //srcStream.Length - srcStream.Position;
                var ok = StreamCopy(dstStream, srcStream, len, entryName);

                // canceled? don't add temp stream to zip file
                if (!ok)
                {
                    var zsw = dstStream as ZipEntryStreamWriter;
                    zsw.Cancel();
                }
            }
        }

        // remove an entry from the zip file
        internal void Remove(C1ZipEntry entry)
        {
            Remove( _entries.IndexOf(entry));
        }

		// remove an entry from the zip file
		internal void Remove(int index)
		{
            // sanity checks
            if (FileName == null || FileName.Length == 0)
            {
                if (_batchStream == null)
                {
                    var msg = StringTables.GetString("Zip file is not open.");
                    throw new ArgumentException(msg);
                }
            }
            if (index < 0 || index >= Entries.Count)
            {
                var msg = StringTables.GetString("Invalid index or entry name.");
                throw new IndexOutOfRangeException(msg);
            }
            
            Stream fs = null;
			try
			{
				// open zip file
				fs = OpenInternal(FileName);

				// remove central dir by truncating the zip file just above the central dir
				fs.SetLength(_bytesBeforeZip + _offset);

				// remove the packed file from the zip file
				var sizedRemoved = RemovePackedFile(fs, index);

				// remove the entry from the entry collection
				_headers.RemoveAt(index);

				// update offsets for the remaining entries
				var e = Entries;
				for (int i = index; i < e.Count; i++)
				{
					e[i]._offset -= sizedRemoved;
				}

				// write central dir back into the zip file
				WriteCentralDir(fs);
			}
			finally // always close the file
			{
				CloseInternal(fs);
			}
		}

		// remove a bunch of entries from the zip file
		// (if we have a bunch of entries to remove, it's more efficient to
		// call this method once than its overload for each entry,
		// because this way we only write the central dir once).
		internal void Remove(int[] indices)
		{
			// sort the array of indices
			var sortedIndices = (int[])indices.Clone();
			Array.Sort(sortedIndices);

			// remove entries
			Stream fs = null;
			try
			{
				// open zip file
				fs = OpenInternal(FileName);

				// remove central dir by truncating the zip file just above the central dir
				fs.SetLength(_bytesBeforeZip + _offset);

				// remove each entry (always from bottom up)
				var lastIndex = -1;
				var e = Entries;
				for (var i = sortedIndices.Length - 1; i >= 0; i--)
				{
					var index = sortedIndices[i];
					if (index < 0) continue;
					if (index >= e.Count) continue;

					// skip repeated indices <<B30>>
					if (index == lastIndex) continue;
					lastIndex = index;

					// remove the packed file from the zip file
					var sizedRemoved = RemovePackedFile(fs, index);

					// remove the entry from the entry collection
					_headers.RemoveAt(index);

					// update offsets for the remaining entries
					for (var j = index; j < e.Count; j++)
					{
						e[j]._offset -= sizedRemoved;
					}
				}

				// write central dir back into the zip file
				WriteCentralDir(fs);
			}
			finally // always close the file
			{
				CloseInternal(fs);
			}
		}

		// extract a packed file from the zip file
        internal void Extract(int index, string dstFileName)
        {
            Extract(Entries[index], dstFileName);
        }

        // extract a packed file from the zip file <<B56>>
#if WIJMOCONTROLS
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        internal void Extract(C1ZipEntry ze, string dstFileName)
		{
            // check destination folder
			var dstFolder = Path.GetDirectoryName(dstFileName);
			if (dstFolder.Length > 0 && !Directory.Exists(dstFolder))
			{
				var msg = StringTables.GetString("Invalid destination folder: ") + dstFolder;
				throw new DirectoryNotFoundException(msg);
			}

            // do extract folders, why not?
			// don't extract folder names, but do extract empty files <<B27>>
			//if ((ze._externalAttr & (int)FileAttributes.Directory) != 0)
			//{
			//	return;
			//}

			// don't extract empty entries (such as folder names)
			//if (ze._uncomprSize == 0)
			//    return;

			// get the destination file name
			var dstName = Path.GetFileName(dstFileName);

			// if the file name was not provided, use the original (entry) file name
			if (dstName.Length == 0)
			{
                dstFileName = Path.Combine(dstFolder, Path.GetFileName(ze.FileName));
			}
                
			// handle read-only, hidden, system files <<B27>>
			if (File.Exists(dstFileName))
			{
				// get attributes
                var fi = new FileInfo(dstFileName);
                var fa = fi.Attributes;

				// get attributes to honor Overwrite* properties <<B33>>
                var isRdOnly = (fa & FileAttributes.ReadOnly) != 0;
                var isHidden = (fa & FileAttributes.Hidden) != 0;
                var isSystem = (fa & FileAttributes.System) != 0;

				// prevent illegal operations 
				if (isRdOnly && !OverwriteReadOnly)
				{
                    var msg = StringTables.GetString("Cannot overwrite read-only file.");
					throw new UnauthorizedAccessException(msg);
				}
				if (isHidden && !OverwriteHidden)
				{
                    var msg = StringTables.GetString("Cannot overwrite hidden file.");
					throw new UnauthorizedAccessException(msg);
				}
				if (isSystem && !OverwriteSystem)
				{
                    var msg = StringTables.GetString("Cannot overwrite system file.");
					throw new UnauthorizedAccessException(msg);
				}

				// change attributes to overwrite as requested (will restore later)
				if (isRdOnly) fa &= ~FileAttributes.ReadOnly;
				if (isHidden) fa &= ~FileAttributes.Hidden;
				if (isSystem) fa &= ~FileAttributes.System;

				// apply changed attributes
				if (fi.Attributes != fa)
				{
					try
					{
						fi.Attributes = fa;
					}
					catch {}
				}
			}

            // is this a folder?
            if (((FileAttributes)ze._attExternal & FileAttributes.Directory) != 0)
            {
                if (!Directory.Exists(dstFileName))
                {
                    Directory.CreateDirectory(dstFileName);
                }
            }
            else
            {
                // initialize file stream
                Stream dstStream = null;
                Stream srcStream = null;
                try
                {
                    // open streams
                    dstStream = new FileStream(dstFileName, FileMode.Create, FileAccess.Write);
                    srcStream = OpenReader(ze);

                    // read source stream, copy into destination stream <<B18>>
                    var ok = StreamCopy(dstStream, srcStream, ze.SizeUncompressedLong, dstFileName);

                    // canceled? delete extracted file
                    if (!ok)
                    {
                        dstStream.Close();
                        File.Delete(dstFileName);
                    }
                }
    			finally // close streams
                {
                    if (srcStream != null) srcStream.Close();
                    if (dstStream != null) dstStream.Close();
                }
            }

			// fix date and attributes on extracted file
			if (File.Exists(dstFileName))
			{
				// fix dates before setting attributes, in case the file is read-only <<B26>>
                var fi = new FileInfo(dstFileName);

#if !COMPACT_FRAMEWORK
                // set last write time (not supported in CF)
                if (ze.Date != DateTime.MinValue)
                {
                    try
                    {
                        fi.LastWriteTime = ze.Date;
                    }
                    catch { }
                }
#endif
				// set attributes
				if (fi.Attributes != ze.Attributes)
				{
					try
					{
						fi.Attributes = ze.Attributes;
					}
					catch {}
				}
			}
		}

		// set a comment for an entry in the zip file
		internal void SetEntryComment(string entryName, string comment)
		{
			// make sure we have a comment
            if (comment == null)
            {
                comment = string.Empty;
            }

			// set the new comment to the specified entry
			Stream zipStream = null;
			try
			{
				// open zip file
				zipStream = OpenInternal(FileName);

				// get the entry with the specified file name
				C1ZipEntry ze = Entries[entryName];
				if (ze == null)
				{
                    var msg = StringTables.GetString("Cannot find entry '{0}'.");
					msg = string.Format(msg, entryName);
					throw new IndexOutOfRangeException(msg);
				}

				// set the new comment
				ze._comment = comment;

				// remove central dir by truncating the zip file just above the central dir
				zipStream.SetLength(_bytesBeforeZip + _offset);

				// write central dir back into the zip file
				WriteCentralDir(zipStream);
			}
			finally // always close the file
			{
				CloseInternal(zipStream);
			}
		}

		// build a stream writer object to create a new entry
		internal Stream OpenWriter(string entryName, DateTime dateTime, bool memory)
		{
			return OpenWriter(null, entryName, dateTime, memory);
		}
		internal Stream OpenWriter(string fileName, string entryName, DateTime dateTime, bool memory)
		{
			return new ZipEntryStreamWriter(this, fileName, entryName, dateTime, memory);
		}

		// look for a match in two byte buffers
		internal static bool Match(byte[] buf, byte[] find, int offset)
		{
			for (int i = 0; i < find.Length; i++)
			{
                if (buf[offset + i] != find[i])
                {
                    return false;
                }
			}
			return true;
		}

		// open the zip file
		internal Stream OpenInternal(string fileName)
		{
			return OpenInternal(fileName, false, true);
		}

        // open the zip file, optionally reading all entries
		internal Stream OpenInternal(string fileName, bool readOnly, bool readEntries)
		{
            // if open in batch mode, keep open
            if (_batchStream != null)
            {
                return _batchStream;
            }

            // open the zip file in read-write mode
            Stream fs = null;
            if (!readOnly)
            {
                try
                {
                    fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
                catch { }
            }

            // open the zip file in read-only mode on request or failure <<B15>>
            if (fs == null)
            {
                try
                {
                    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                }
                catch
                {
                    // if that failed, try again allowing others to read-write <<B91>>
                    fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                }
            }

            // open the stream
            try
            {
                OpenInternal(fs, readEntries);
            }
            catch
            {
                // doesn't look like a zip file... close the stream <<B69>>
                fs.Close();
                throw;
            }

            // save file name
            _fileName = fileName;

            // return file stream
            return fs;
        }

        // open the zip file contained in the stream, optionally reading all entries
        internal Stream OpenInternal(Stream fs, bool readEntries)
        {
            // find central dir end record
            var centralDirPos = FindCentralDir(fs);

            // move to the beginning of the central dir end record
            fs.Position = centralDirPos;

            // check end of central dir signature
            var br = new BinaryReader(fs);
            var sig = br.ReadBytes(CENTRAL_DIR_SIGN.Length);
            if (sig.Length != CENTRAL_DIR_SIGN.Length || !Match(sig, CENTRAL_DIR_SIGN, 0))
            {
                var msg = StringTables.GetString("This is not a Zip file (wrong signature).");
                throw new ZipFileException(msg, FileName);
            }

            // read central dir
            var thisDisk = br.ReadUInt16();
            var diskWithCD = br.ReadUInt16();
            var diskEntryCount = br.ReadUInt16();
            var entryCount = br.ReadUInt16();
            _size = br.ReadUInt32();
            _offset = br.ReadUInt32();

            // read comment
            _comment = string.Empty;
            var commentSize = br.ReadInt16();
            if (commentSize > 0)
            {
                var buf = br.ReadBytes(commentSize);
                if (buf.Length != commentSize)
                {
                    string msg = StringTables.GetString("Zip file is corrupted (wrong comment size).");
                    throw new ZipFileException(msg, FileName);
                }
                _comment = Encoding.GetString(buf, 0, buf.Length);
            }

            // we don't handle archives that span disks (yet, anyway)
            if (thisDisk != 0 || diskWithCD != 0 || diskEntryCount != entryCount)
            {
                var msg = StringTables.GetString("Disk spanning not supported.");
                throw new NotSupportedException(msg);
            }

            // if we have entries, we must have a minimum entry dir size
            if (entryCount != 0xffff && _size < entryCount * C1ZipEntry.HEADERSIZE)
            {
                var msg = StringTables.GetString("Zip file is corrupted (wrong header size).");
                throw new ZipFileException(msg, FileName);
            }

            // REVIEW: not sure what _bytesBeforeZip does
            // but it seems to screw things up when it is not zero
            //_bytesBeforeZip = (uint)(centralDirPos - _size - _offset);
            _bytesBeforeZip = 0;

            // if offset is negative, read zip64 central dir
            if (_offset == 0xffffffff)
            {
                // move back to Zip64 end of central directory locator
                fs.Position = centralDirPos - 20;

                // check signature
                sig = br.ReadBytes(CENTRAL_DIR64L_SIGN.Length);
                if (sig.Length != CENTRAL_DIR64L_SIGN.Length || !Match(sig, CENTRAL_DIR64L_SIGN, 0))
                {
                    var msg = StringTables.GetString("Central dir 64 not found.");
                    throw new ZipFileException(msg);
                }
       
                // get offset to start of zip64 central dir
                var disk = br.ReadInt32();
                var offset = br.ReadInt64();

                // read zip64 central dir signature
                fs.Position = offset;
                sig = br.ReadBytes(CENTRAL_DIR64_SIGN.Length);
                if (sig.Length != CENTRAL_DIR64_SIGN.Length || !Match(sig, CENTRAL_DIR64_SIGN, 0))
                {
                    var msg = StringTables.GetString("Central dir 64 not found.");
                    throw new ZipFileException(msg);
                }

                // skip central dir extra info (see item E: Zip64 end of central directory record)
                fs.Position += 44; // sz, made, needed, #disk, #startDisk, #entries, #totEntries
                _offset = br.ReadInt64();
            }

            // read entries (this changes the Entries collection)
            if (readEntries)
            {
                ReadEntryHeaders(fs);
            }

            // done
            return fs;
        }
        
        // close the zip file (if not in batch mode)
		internal void CloseInternal(Stream s)
		{
			if (_batchStream == null && s != null)
			{
				s.Close();
			}
		}

		// write CentralDir
		internal void WriteCentralDir(Stream fs)
		{
            // write central directory: item D
			WriteEntryHeaders(fs);

            // write zip 64 end and locator: items E,F
            if (IsZip64())
            {
                WriteCentralDir64(fs);
            }

            // write end of central dir: item G
			WriteCentralDirEnd(fs);
		}

        // check whether this zip file requires zip 64 extensions
        internal bool IsZip64()
        {
            // big file?
            if (_offset >= int.MaxValue)
            {
                return true;
            }

            // big entries?
            foreach (var ze in this.Entries)
            {
                if (ze.IsZip64())
                {
                    return true;
                }
            }

            // lots of entries?
            if (this.Entries.Count >= ushort.MaxValue)
            {
                return true;
            }

            // no need for zip64...
            return false;
        }

		// read srcStream and copy its contents into dstStream
		internal bool StreamCopy(Stream dstStream, Stream srcStream, long len, string streamName)
		{
#if EXPOSE_ZIP
			// to fire progress event <<B4>>
            ZipProgressEventArgs e = null;
            if (streamName != null && Progress != null)
            {
                // create event args
                // note that 'len' is always long.MaxValue (TFS 41367)
                // not really (TFS 48361)
                var l = len;
                if (len == long.MaxValue)
                {
                    l = srcStream.Length - srcStream.Position;
                }
                e = new ZipProgressEventArgs(streamName, l);

                // fire progress event one time even if file length is zero <<B4>>
                Progress(this, e);
                if (e.Cancel)
                {
                    return false;
                }
            }

#endif
			// copy stream
			while (len > 0)
			{
				// copy one buffer length
				var toRead = (int)Math.Min(_buf.Length, len);
                var read = srcStream.Read(_buf, 0, toRead);
				if (read == 0) break;
				dstStream.Write(_buf, 0, read);
				len -= (uint)read;

#if EXPOSE_ZIP
				// fire progress event, return false on cancel <<B4>>
				if (e != null)
				{
					e._position += read;
					Progress(this, e);
                    if (e.Cancel)
                    {
                        return false;
                    }
				}
#endif
            }
            dstStream.Flush();

			// done
			return true;
		}

        // read srcStream and copy its contents into dstStream
		internal static void StreamCopy(Stream dstStream, Stream srcStream, long len)
		{
            var buf = new byte[C1ZipFile.BUFFERSIZE];
			while (len > 0)
			{
                var toRead = (int)Math.Min(buf.Length, len);
                var read = srcStream.Read(buf, 0, toRead);
				if (read == 0) break;
				dstStream.Write(buf, 0, read);
				len -= (uint)read;
			}
			dstStream.Flush();
		}

		// build a stream reader object for a given entry
		internal Stream OpenReader(C1ZipEntry ze)
		{
			// open a compressed stream reader on the selected entry
			C1ZStreamReader zr = null;
			Stream zipStream = null;
			try
			{
				// open zip file
				zipStream = OpenInternal(FileName, true, false);

                // read local header to position reader
                ze.ReadLocalHeader(zipStream);
                
				// build stream reader
				// (will close underlying stream when it's closed)
				if (ze.IsEncrypted)
				{
					// make sure we have a password to decrypt data
					if (_password == null || _password.Length == 0)
					{
                        var msg = StringTables.GetString("Password not set: can't decrypt data.");
						throw new ZipFileException(msg);
					}
                    
					// build new encrypted stream object to read the entry
					C1CryptStream cryptStream = new C1CryptStream(ze, zipStream);
					if (!cryptStream.InitKeys())
					{
                        var msg = StringTables.GetString("Invalid password: can't decrypt data.");
						throw new ZipFileException(msg);
					}

					// attach the compressed stream reader on a decryptor stream
					// adjust the stream length by the encrypted header size <<B8>>
					long len = ze.SizeCompressedLong - C1CryptStream.ENCR_HEADER_LEN;

					// and don't forget to pass the compression method <<B31>>
					zr = new C1ZStreamReader(cryptStream, true, len, ze._method);
				}
				else
				{
					// create reader/decompressor <<B3>>
					zr = new C1ZStreamReader(zipStream, true, ze.SizeCompressedLong, ze._method);
				}
			}
			catch (Exception x) // close the file if there's any errors
			{
				CloseInternal(zipStream);
				if (zr != null) zr.Close();
                var msg = StringTables.GetString("Error extracting entry from Zip file.");
				throw new ZipFileException(msg, ze.FileName, x);
			}

			// when the user closes this stream, it should close the base stream as well
			// (unless we're open in batch mode)
			if (zr != null && _batchStream == null)
			{
				zr.OwnsBaseStream = true;
			}

			// return the stream reader
			// note: the zipStream remains open until the user closes the
			// stream we are returning.
			return zr;
		}

        // checks whether the current password is valid for a given entry
        internal bool CheckPassword(C1ZipEntry ze)
        {
            // not encrypted? any password will do
            if (!ze.IsEncrypted)
            {
                return true;
            }

            // assume password is OK
            bool retVal = true;

            // open a compressed stream reader on the selected entry
            C1ZStreamReader zr = null;
            Stream zipStream = null;
            try
            {
                // open zip file
                zipStream = OpenInternal(FileName, true, false);

                // read local header to position reader
                ze.ReadLocalHeader(zipStream);

                // check password
                var cryptStream = new C1CryptStream(ze, zipStream);
                retVal = cryptStream.InitKeys();
            }
            finally // always close the file
            {
                CloseInternal(zipStream);
                if (zr != null) zr.Close();
            }

            // done
            return retVal;
        }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** private stuff

		// read headers for each entry in the zip file, from the central dir
		void ReadEntryHeaders(Stream fs)
		{
			// clear existing info, if any
			_headers.Clear();

            // read all entries until we can't read anymore
            fs.Position = _offset;
            for (; ; )
            {
                var ze = new C1ZipEntry(this);
                if (ze.ReadCentralHeader(fs))
                {
                    _headers.Add(ze);
                }
                else
                {
                    break;
                }
            }
		}

		// write entry headers in the zip file
		void WriteEntryHeaders(Stream fs)
		{
            var e = Entries;
			_offset = fs.Position - _bytesBeforeZip;
            _size = 0;
            
			// loop through the entries
			for (int i = 0; i < e.Count; i++)
			{
				// write entry header, accumulate total size of entry headers
				_size += e[i].WriteCentralHeader(fs);
			}
		}

        // write zip 64 end of central dir and locator (items E,F)
        void WriteCentralDir64(Stream fs)
        {
            // position of the central dir 64
            var offset64 = fs.Position;

            // write Zip64 end of central directory record (item E)
            var bw = new BinaryWriter(fs);
            bw.Write(CENTRAL_DIR64_SIGN);               // signature
            bw.Write((long)44);                         // size of zip64 end of central directory record 8 bytes
            bw.Write((ushort)C1ZipEntry.ZIPVERSION64);  // version made by
            bw.Write((ushort)C1ZipEntry.ZIPVERSION64);  // version needed to extract
            bw.Write((uint)0);                          // number of this disk, 4 bytes
            bw.Write((uint)0);                          // number of the disk with the start of the central directory, 4 bytes
            bw.Write((long)Entries.Count);              // total number of entries in the central directory on this disk, 8 bytes
            bw.Write((long)Entries.Count);              // total number of entries in the central directory, 8 bytes
            bw.Write((long)_size);                      // size of the central directory, 8 bytes
            bw.Write((long)_offset);                    // offset of start of central directory with respect to the starting disk number, 8 bytes
            // zip64 extensible data sector (variable size, RESERVED BY PKWARE)

            // write Zip64 end of central directory locator (item F)
            bw.Write(CENTRAL_DIR64L_SIGN);
            bw.Write((uint)0);                          // number of the disk with the start of the zip64 end of central directory, 4 bytes
            bw.Write((long)offset64);                   // relative offset of the zip64 end of central directory record, 8 bytes
            bw.Write((uint)1);                          // total number of disks, 4 bytes
        }

		// write end of central dir (item G)
		void WriteCentralDirEnd(Stream fs)
		{
            // get number of entries (limit to ushort.MaxValue: 65535)
            int count = Math.Min(_entries.Count, ushort.MaxValue);

            // get offset (limit to uint.MaxValue: xxx)
            long offset = Math.Min(_offset, uint.MaxValue);

            // get comments
            var comment = Encoding.GetBytes(_comment);

			// write end of central dir record
            var bw = new BinaryWriter(fs);
            bw.Write(CENTRAL_DIR_SIGN);         // signature
            bw.Write((ushort)0);                // thisDisk
            bw.Write((ushort)0);                // diskWithCD
            bw.Write((ushort)count);            // number of entries
            bw.Write((ushort)count);            // number of entries
            bw.Write((uint)_size);              // size of central dir
            bw.Write((uint)offset);             // offset to central dir
            bw.Write((ushort)comment.Length);   // comment length
            bw.Write(comment);                  // comment
		}
        
		// remove a packed file from the zip file
		long RemovePackedFile(Stream fs, int index)
		{
			// if this is the last packed file, simply truncate the zip file
			// just above the packed file
            var e = Entries;
			if (index == e.Count - 1)
			{
				long len = e[index]._offset + _bytesBeforeZip;
				fs.SetLength(len);
				return 0;
			}

			// get offset range to be removed from the zip file
			long uStartOffset = e[index]._offset + _bytesBeforeZip;
			long uEndOffset   = e[index + 1]._offset + _bytesBeforeZip;
            
			// initialize counters
			long uBytesToCopy  = fs.Length - uEndOffset;
			long uTotalToWrite = uBytesToCopy;
			long uTotalWritten = 0;
			int  size_read     = 0;

			// initialize buffer
            var buf = _buf;
            if (uBytesToCopy > buf.Length)
            {
                uBytesToCopy = buf.Length;
            }

			// fill up idle space in zip file by moving up all remaining data
			do
			{
                fs.Position = uEndOffset + uTotalWritten;
				size_read = fs.Read(buf, 0, (int)uBytesToCopy);
				if (size_read > 0)
				{
					fs.Position = uStartOffset + uTotalWritten;
					fs.Write(buf, 0, (int)size_read);
				}
				uTotalWritten += size_read;
			}
			while (size_read == uBytesToCopy);

			// sanity check
			if (uTotalToWrite != uTotalWritten)
			{
                var msg = StringTables.GetString("Error writing data to Zip file (file may be corrupted).");
				throw new IOException(msg);
			}

			// adjust file size
			long uRemoved = uEndOffset - uStartOffset;
			fs.SetLength(fs.Length - uRemoved);
			return uRemoved;
		}

		// get the offset of the central dir in the zip file <<B29>>
		static long FindCentralDir(Stream fs)
		{
			// maximum size of end of central dir record
			long maxRecordSize = 0xffff + ZIPFILEHEADERSIZE;
			long size = fs.Length;
			if (maxRecordSize > size)
			{
				maxRecordSize = size;
			}

			long position = 0;
			long totalRead = 0;
            
			// backward reading
            var buf = new byte[C1ZipFile.BUFFERSIZE];
			while (position < maxRecordSize)
			{
				position = totalRead + BUFFERSIZE;
				if (position > maxRecordSize)
				{
					position = maxRecordSize;
				}

                var toRead = (int)(position - totalRead);
				fs.Seek(-position, SeekOrigin.End);
                var read = fs.Read(buf, 0, toRead);
				if (read != toRead)
				{
					string msg1 = StringTables.GetString("Error reading data from Zip file (file may be corrupted).");
					throw new IOException(msg1);
				}

				// search from the end to find the last signature in the stream
                // (important in case there are other packed files in the stream)
				for (int i = toRead - 4; i >=0; i--)
				{
					if (Match(buf, CENTRAL_DIR_SIGN, i))
					{
						return size - (position - i);
					}
				}
				totalRead += toRead - 3;
			}
    
			// central dir not found
            var msg2 = StringTables.GetString("Central dir not found.");
			throw new ZipFileException(msg2);
		}

#if DEBUG
        /// <summary>
        /// Dump the contents of the zip file (for debugging only).
        /// </summary>
        public void Dump()
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                DumpLine("** {0}", Path.GetFileName(FileName));

                using (var fs = new FileStream(FileName, FileMode.Open))
                using (var br = new BinaryReader(fs))
                {
                    var entriesLocal = 0;
                    var entriesCentral = 0;

                    // dump entries
                    while (DumpLocalEntry(br, entriesLocal))
                    {
                        entriesLocal++;
                    }

                    // dump central directory entries
                    while (DumpCentralEntry(br, entriesCentral))
                    {
                        entriesCentral++;
                    }

                    // finish central dir
                    DumpCentralDir64(br);
                    DumpCentralDirEnd(br);
                }
            }
        }
        bool DumpLocalEntry(BinaryReader br, int entryID)
        {
            var position = br.BaseStream.Position;

            var sig = br.ReadBytes(4);
            if (!C1ZipFile.Match(sig, C1ZipEntry.LOCAL_SIGN, 0))
            {
                br.BaseStream.Position -= 4;
                return false;
            }

            // easy stuff
            var versionNeeded = br.ReadInt16();
            var flags = br.ReadInt16();
            var method = br.ReadInt16();
            var modTime = br.ReadInt16();
            var modDate = br.ReadInt16();
            var crc32 = br.ReadInt32(); // << invalid when HasDataDescriptor == true!!!
            long comprSize = br.ReadUInt32(); // << invalid when HasDataDescriptor == true!!!
            long uncomprSize = br.ReadUInt32(); // << invalid when HasDataDescriptor == true!!!
            var fileNameSize = br.ReadInt16();
            var extraFieldSize = br.ReadInt16();
            var buf = br.ReadBytes(fileNameSize);
            var fileName = Encoding.GetString(buf, 0, buf.Length);

            // extra stuff (zip64 entries etc)
            if (extraFieldSize > 0)
            {
                var xfInfo = br.ReadBytes(extraFieldSize);
                long offset = 0;
                DumpExtraFieldInfo(xfInfo, ref comprSize, ref uncomprSize, ref offset);
            }

            // skip content
            br.BaseStream.Position += comprSize;
            
            // data descriptor (block C)
            if ((flags & 8) != 0)
            {
                crc32 = br.ReadInt32();
                if (versionNeeded == C1ZipEntry.ZIPVERSION64)
                {
                    // For Zip64 format archives, the compressed and 
                    // uncompressed sizes are 8 bytes each.
                    comprSize = br.ReadInt64();
                    uncomprSize = br.ReadInt64();
                }
                else
                {
                    comprSize = br.ReadUInt32();
                    uncomprSize = br.ReadUInt32();
                }
            }

            // dump
            if (entryID == 0)
            {
                DumpLine("------------------\r\nLocal Entries");
                DumpLine("{0,-15} {1,15} {2,15} {3,10} {4,10} {5,15}",
                    "fileName", "szComp", "szUncomp", "needed", "xtra", "offset");
            }
            DumpLine("{0,-15} {1,15:n0} {2,15:n0} {3,10} {4,10} {5,15:n0}",
                fileName, comprSize, uncomprSize,
                versionNeeded, extraFieldSize, position);

            // done
            return true;
        }
        bool DumpCentralEntry(BinaryReader br, int entryID)
        {
            var sig = br.ReadBytes(4);
            if (!C1ZipFile.Match(sig, C1ZipEntry.CENTRAL_SIGN, 0))
            {
                br.BaseStream.Position -= 4;
                return false;
            }

            // read in info
            var versionMadeBy = br.ReadInt16();
            var versionNeeded = br.ReadInt16();
            var flags = br.ReadInt16();
            var method = br.ReadInt16();
            var modTime = br.ReadInt16();
            var modDate = br.ReadInt16();
            var crc32 = br.ReadInt32();
            long comprSize = br.ReadUInt32();
            long uncomprSize = br.ReadUInt32();
            var fileNameSize = br.ReadInt16();
            var extraFieldSize = br.ReadInt16();
            var commentSize = br.ReadInt16();
            var diskStart = br.ReadInt16();
            var internalAttr = br.ReadInt16();
            var externalAttr = br.ReadInt32();
            long offset = br.ReadUInt32();

            // read file name
            var buf = br.ReadBytes(fileNameSize);
            var fileName = Encoding.GetString(buf, 0, buf.Length);

            // read extra field information (zip64 sizes)
            if (extraFieldSize > 0)
            {
                var xInfo = br.ReadBytes(extraFieldSize);
                DumpExtraFieldInfo(xInfo, ref comprSize, ref uncomprSize, ref offset);
            }

            // read comment
            if (commentSize > 0)
            {
                buf = br.ReadBytes(commentSize);
                var comment = Encoding.GetString(buf, 0, buf.Length);
            }

            // dump
            if (entryID == 0)
            {
                DumpLine("------------------\r\nCentral Entries");
                DumpLine("{0,-15} {1,15} {2,15} {3,10} {4,10} {5,15:n0}",
                    "fileName", "szComp", "szUncomp", "needed", "xtra", "offset");
            }
            DumpLine("{0,-15} {1,15:n0} {2,15:n0} {3,10} {4,10} {5,15:n0}",
            fileName, comprSize, uncomprSize,
                versionNeeded, extraFieldSize, offset);

            // done
            return true;
        }
        bool DumpExtraFieldInfo(byte[] xInfo, ref long comprSize, ref long uncomprSize, ref long offset)
        {
            if (xInfo == null || xInfo.Length <= 4)
            {
                return false;
            }

            using (var ms = new MemoryStream(xInfo))
            using (var br = new BinaryReader(ms))
            {
                for (; br.PeekChar() > -1; )
                {
                    ushort hdrId = br.ReadUInt16();
                    ushort size = br.ReadUInt16();
                    if (hdrId == 1) //HDR_ZIP64
                    {
                        // check record size
                        if (size > 28)
                        {
                            string msg = StringTables.GetString("Inconsistent datasize for ZIP64 extra field.");
                            throw new ZipFileException(msg, FileName);
                        }

                        // uncompressed size
                        if (uncomprSize == 0xffffffff)
                        {
                            uncomprSize = br.ReadInt64();
                            size -= 8;
                        }

                        // compressed size
                        if (comprSize == 0xffffffff)
                        {
                            comprSize = br.ReadInt64();
                            size -= 8;
                        }

                        // offset
                        if (offset == 0xffffffff)
                        {
                            offset = br.ReadInt64();
                            size -= 8;
                        }
                    }

                    // eat left over info if any
                    ms.Position += size;
                }
            }
            return true;
        }
        bool DumpCentralDir64(BinaryReader br)
        {
            var sig = br.ReadBytes(4);
            if (!Match(sig, CENTRAL_DIR64_SIGN, 0))
            {
                br.BaseStream.Position -= 4;
                return false;
            }

            // E: Zip64 end of central directory record
            var size = br.ReadUInt64();
            var versionMadeBy = br.ReadUInt16();
            var versionNeeded = br.ReadUInt16();
            var diskNo = br.ReadUInt32();
            var diskStart = br.ReadUInt32();
            var entryCountDisk = br.ReadUInt64();
            var entryCount = br.ReadUInt64();
            var sizeCentral = br.ReadUInt64();
            var offset = br.ReadUInt64();

            // F: Zip64 end of central directory locator
            sig = br.ReadBytes(4);
            if (!Match(sig, CENTRAL_DIR64L_SIGN, 0))
            {
                throw new Exception("No end of Zip64 directory?");
            }
            var diskCentral = br.ReadInt32();
            var offset64 = br.ReadInt64();
            var diskCount = br.ReadInt32();

            // done
            return true;
        }
        bool DumpCentralDirEnd(BinaryReader br)
        {
            // G: End of central directory record:
            var sig = br.ReadBytes(4);
            if (!Match(sig, CENTRAL_DIR_SIGN, 0))
            {
                throw new Exception("No end of central directory?");
            }

            var diskNo = br.ReadUInt16();
            var diskDir = br.ReadUInt16();
            var entriesDisk = br.ReadUInt16();
            var entries = br.ReadUInt16();
            var sizeDir = br.ReadUInt32();
            var commentSize = br.ReadInt16();
            if (commentSize > 0)
            {
                var buf = br.ReadBytes(commentSize);
                var comment = Encoding.GetString(buf, 0, buf.Length);
            }

            // done
            return true;
        }
        void DumpLine(string fmt, params object[] args)
        {
            var str = string.Format(fmt, args);
            System.Diagnostics.Debug.WriteLine(str);
        }
#endif
		#endregion
	}

#if EXPOSE_ZIP
	/// <summary>
	/// Represents the method that will handle the <see cref="C1.C1Zip.C1ZipFile.Progress"/> event of a 
	/// <see cref="C1ZipFile"/>.
	/// </summary>
	public delegate void ZipProgressEventHandler(object sender, ZipProgressEventArgs e);
	/// <summary>
	/// Provides data for the <see cref="C1.C1Zip.C1ZipFile.Progress"/> event of a 
	/// <see cref="C1ZipFile"/>.
	/// </summary>
	public class ZipProgressEventArgs : EventArgs
	{
		//--------------------------------------------------------------------------------
		#region ** fields

		internal long   _position;
        long            _fileLength;
        string          _fileName;
        bool            _cancel;

		#endregion

		//--------------------------------------------------------------------------------
		#region ** ctor

		internal ZipProgressEventArgs(string fileName, long fileLength)
		{
			_fileName   = fileName;
            _fileLength = fileLength;
        }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** public

		/// <summary>
		/// Gets the name of the file being compressed or expanded.
		/// </summary>
        public string FileName
        {
            get { return _fileName; }
        }
        /// <summary>
        /// Gets the length of the file being compressed or expanded.
        /// </summary>
        public int FileLength
        {
            get
            {
                if (_fileLength > int.MaxValue)
                {
                    string msg = StringTables.GetString(C1ZipFile.ERR_TOOLARGE_LENGTH);
                    throw new ZipFileException(msg, FileName);
                }
                return (int)_fileLength;
            }
        }
        /// <summary>
        /// Gets the length of the file being compressed or expanded.
        /// </summary>
        public long FileLengthLong
        {
            get { return _fileLength; }
        }
        /// <summary>
		/// Set to true to cancel the current operation.
		/// </summary>
        public bool Cancel
        {
            get { return _cancel; }
            set { _cancel = value; }
        }
        /// <summary>
        /// Gets the current position into the stream.
        /// </summary>
        public int Position
        {
            get 
            {
                if (_position > int.MaxValue)
                {
                    string msg = StringTables.GetString(C1ZipFile.ERR_TOOLARGE_POSITION);
                    throw new ZipFileException(msg, FileName);
                }
                return (int)_position; 
            }
        }
        /// <summary>
        /// Gets the current position into the stream.
        /// </summary>
        public long PositionLong
        {
            get { return _position; }
        }
        #endregion
	}
#endif

    /// <summary>
	/// Encoding used for entry names and comments
	/// 
	/// NOTE: this is culture-dependent, which is a really bad idea, but most zip packers
	/// use the default OEM code page to encode file names, so we have to go along with it...
	/// 
	/// Note that Encoding.Default seems like a logical choice but doesn't really work for
	/// international locales. Instead, we need to create an encoding using the current
	/// OEMCodePage. That allows accents and international characters to be used in file
	/// names (like the zip built into Windows, allows names such as "�land.txt", "�iti.txt", 
	/// "W�rth.txt", etc.).
	/// 
	/// This has nothing to do with compression, it's just used to encode and decode entry 
	/// names and comments.
	/// </summary>
	internal class ZipEncoding // <<B36>>
	{
		static Encoding _enc;

        private ZipEncoding() { }
		
        internal static Encoding Encoding
		{
			get 
            {
                if (_enc == null)
                {
#if SILVERLIGHT || WINDOWS_PHONE
                    _enc = Encoding.UTF8;
#else
                    _enc = Encoding.Default;
#endif
#if !COMPACT_FRAMEWORK
                    try
                    {
                        // use current culture to interpret Japanese and other non-ASCII pages
                        // NOTE: the encoding to be used is not specified by the Zip format, but
                        // using the current culture works with zip files created with Japanese versions of Windows.
                        //_enc = System.Text.Encoding.GetEncoding("IBM437"); // TFS 30097
                        var codePage = CultureInfo.CurrentUICulture.TextInfo.OEMCodePage;
                        _enc = Encoding.GetEncoding(codePage);
                    }
                    catch { }
#endif
                }
                return _enc; 
            }
		}
	}

	/// <summary>
    /// Exception thrown when trying to open an invalid Zip file.
	/// </summary>
#if EXPOSE_ZIP
	public class ZipFileException : IOException
#else
    internal class ZipFileException : IOException
#endif
	{
        /// <summary>
        /// Initializes a new instance of a <see cref="ZipFileException"/>.
        /// </summary>
        /// <param name="msg">Message that describes the exception.</param>
		public ZipFileException(string msg) : base(msg) {}
        /// <summary>
        /// Initializes a new instance of a <see cref="ZipFileException"/>.
        /// </summary>
        /// <param name="msg">Message that describes the exception.</param>
        /// <param name="filename">Name of the file that caused the exception.</param>
		public ZipFileException(string msg, string filename) : base(msg) {}
        /// <summary>
        /// Initializes a new instance of a <see cref="ZipFileException"/>.
        /// </summary>
        /// <param name="msg">Message that describes the exception.</param>
        /// <param name="filename">Name of the file that caused the exception.</param>
        /// <param name="innerException">Inner exception.</param>
        public ZipFileException(string msg, string filename, Exception innerException) : base(msg, innerException) { }	
	}

#if SILVERLIGHT || WPF || WINDOWS_PHONE
    /// <summary>
    /// No error message localization in Silverlight/WPF.
    /// </summary>
    internal static class StringTables
    {
        public static string GetString(string key)
        {
            return key;
        }
    }
#endif
}
