//----------------------------------------------------------------------------
// C1\C1Zip\C1ZStreamWriter.cs
//
// C1ZStreamWriter implements a compressed output Stream.
//
// To use C1ZStreamWriter, open a stream of any type (file, memory, etc), 
// then create a C1ZStreamWriter and pass the stream you created as an 
// argument in the constructor. Anything you write to the C1ZStreamWriter 
// object will be compressed into the underlying stream.
//
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status           Date            By                      Comments
//----------------------------------------------------------------------------
// Created          Nov, 2001       Bernardo                -
//----------------------------------------------------------------------------

using System;
using System.IO;
using System.Diagnostics;

namespace C1.C1Zip
{
	using C1.C1Zip.ZLib;

#if EXPOSE_ZIP
	/// <summary>
	/// Specifies the level of compression to be applied when adding entries to a <see cref="C1ZipFile"/>.
	/// </summary>
	public enum CompressionLevelEnum
#else
	internal enum CompressionLevelEnum
#endif
	{
		/// <summary>
		/// No Compression.
		/// </summary>
        NoCompression = 0,
		/// <summary>
		/// Low compression, highest speed.
		/// </summary>
        BestSpeed = 1,
		/// <summary>
		/// Highest compression, low speed.
		/// </summary>
        BestCompression = 9,
		/// <summary>
		/// High compression, high speed.
		/// </summary>
        DefaultCompression = -1
    }

#if EXPOSE_ZIP
	/// <summary>
	/// Compresses data into .NET Streams.
	/// </summary>
	/// <remarks>
	/// <para>To compress data into a stream, create a <see cref="C1ZStreamWriter"/> object 
	/// passing the stream to the <see cref="C1ZStreamWriter"/> constructor.</para>
	/// <para>Then write the data into the <see cref="C1ZStreamWriter"/> using the <see cref="Write"/>
	/// method, or create a <see cref="StreamWriter"/> on the <see cref="C1ZStreamWriter"/>. 
	/// The second option is indicated when you want to write formatted data.</para>
	/// <para>When you are done writing the data, call the <see cref="Close"/> method to
	/// flush the data and close the underlying stream.</para>
	/// </remarks>
	/// <example>
	/// The code below compresses a string into a memory stream:
	/// <code>
	/// public byte[] CompressString(string str)
	/// {
	/// 	// open memory stream
	/// 	var ms = new MemoryStream();
	/// 	
	/// 	// attach compressor stream to memory stream
	/// 	var sw = new C1ZStreamWriter(ms);
	/// 	
	/// 	// write data into compressor stream
	/// 	var writer = new StreamWriter(sw);
	/// 	writer.Write(str);
	/// 	
	/// 	// flush any pending data
	/// 	writer.Flush();
	/// 	
	/// 	// return the memory buffer
	/// 	return ms.ToArray();
	/// }
	/// </code>
	/// </example>
	public class C1ZStreamWriter : Stream
#else
	internal class C1ZStreamWriter : Stream
#endif
    {
		//--------------------------------------------------------------------------------
		#region ** fields

        Stream  _baseStream;	// base Stream (uncompressed data store)
        ZStream _z;				// base ZStream (compressor)
        byte[]  _buf;			// work buffer
        bool	_finished;		// stream is finished (can't write any more data)
        bool	_ownsBase;		// close base stream when we close
        long    _flushed;       // flush automatically at each 1/2 meg of output

		#endregion ** fields

		//--------------------------------------------------------------------------------
		#region ** ctors
		internal C1ZStreamWriter() {}	// for inheritors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamWriter"/> class.
		/// </summary>
		/// <param name="baseStream">Output stream that will contain the compressed data.</param>
		public C1ZStreamWriter(Stream baseStream)
			: this(baseStream, CompressionLevelEnum.DefaultCompression, true, false) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamWriter"/> class.
		/// </summary>
		/// <param name="baseStream">Output stream that will contain the compressed data.</param>
		/// <param name="zip">Specifies whether the compressed stream should be compatible with zip files.</param>
		/// <remarks>
		/// <para>Streams in zip files are different from regular zlib streams in two aspects:</para>
		/// <para>(1) zip streams do not contain any local header information (the information is stored in the zip file headers instead) and</para>
		/// <para>(2) zip streams use a CRC32 checksum instead of the adler32 checksum used by zlib streams.</para>
		/// </remarks>
        public C1ZStreamWriter(Stream baseStream, bool zip)
			: this(baseStream, CompressionLevelEnum.DefaultCompression, !zip, zip) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamWriter"/> class.
		/// </summary>
		/// <param name="baseStream">Output stream that will contain the compressed data.</param>
		/// <param name="header">Include header information in compressed stream (should be False for streams in zip files).</param>
		/// <param name="crc32">Include CRC32 checksum in compressed stream (should be True for streams in zip files).</param>
		public C1ZStreamWriter(Stream baseStream, bool header, bool crc32)
			: this(baseStream, CompressionLevelEnum.DefaultCompression, header, crc32) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamWriter"/> class.
		/// </summary>
		/// <param name="baseStream">Output stream that will contain the compressed data.</param>
		/// <param name="level">Compression level to use when compressing data.</param>
		public C1ZStreamWriter(Stream baseStream, CompressionLevelEnum level)
			: this(baseStream, level, true, false) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamWriter"/> class.
		/// </summary>
		/// <param name="baseStream">Output stream that will contain the compressed data.</param>
		/// <param name="level">Compression level to use when compressing data.</param>
		/// <param name="zip">Specifies whether the compressed stream should be compatible with zip files.</param>
		public C1ZStreamWriter(Stream baseStream, CompressionLevelEnum level, bool zip)
			: this(baseStream, level, !zip, zip) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamWriter"/> class.
		/// </summary>
		/// <param name="baseStream">Output stream that will contain the compressed data.</param>
		/// <param name="level">Compression level to use when compressing data.</param>
		/// <param name="header">Include header information in compressed stream (should be False for streams in zip files).</param>
		/// <param name="crc32">Include CRC32 checksum in compressed stream (should be True for streams in zip files).</param>
		public C1ZStreamWriter(Stream baseStream, CompressionLevelEnum level, bool header, bool crc32)
		{
			InitStream(baseStream, level, header, crc32);
		}

		// ctor utility
		internal void InitStream(Stream baseStream, CompressionLevelEnum level, bool header, bool crc32)
		{
			// baseStream must be writable to receive the compressed data
			if (baseStream == null || !baseStream.CanWrite)
			{
				string msg = StringTables.GetString("baseStream must be writable stream.");
				throw new ArgumentException(msg);
			}

			// initialize members
			_buf = new byte[1024*16];
			_baseStream = baseStream;
			_ownsBase = true;

			// initialize ZStream
			_z = new ZStream(crc32);
			_z.next_out = _buf;
			_z.avail_out = _buf.Length;
			_z.next_out_index = 0;
			int err = (header)
				? _z.deflateInit((int)level)
				: _z.deflateInit((int)level, -ZStream.DEF_WBITS);

			// sanity
			if (err != ZStream.Z_OK)
			{
				string msg = StringTables.GetString("Failed to initialize compressed stream.");
				throw new ZStreamException(msg);
			}
		}
		#endregion

		//--------------------------------------------------------------------------------
		#region ** public

		// ** provide access to _baseStream

		/// <summary>
		/// Gets the underlying stream that receives the compressed data.
		/// </summary>
        public Stream BaseStream
        {
            get { return _baseStream; }
        }
		/// <summary>
		/// Gets or sets whether calling the <see cref="Close"/> method will also
		/// close the underlying stream (see <see cref="BaseStream"/>).
		/// </summary>
        public bool OwnsBaseStream
        {
            get { return _ownsBase; }
            set { _ownsBase = value; }
        }

		/// <summary>
		/// Gets the checksum value used to check the integrity of the stream.
		/// </summary>
		/// <remarks>
		/// The checksum used may be an Adler or crc32 value depending on how
		/// the <see cref="C1ZStreamWriter"/> was created.
		/// </remarks>
        public int Checksum
        {
            get { return (int)_z.adler; }
        }

		// ** provide access to uncompressed and compressed byte counts

		/// <summary>
		/// Gets the number of bytes in the stream (compressed bytes).
		/// </summary>
		public int SizeCompressed
		{
			get 
            {
                if (_z.total_out > int.MaxValue)
                {
                    string msg = StringTables.GetString(C1ZipFile.ERR_TOOLARGE_COMPRESSED);
                    throw new ZipFileException(msg);
                }
                return (int)_z.total_out; 
            }
		}
		/// <summary>
		/// Gets the number of bytes that were compressed into the stream (uncompressed bytes).
		/// </summary>
		public int SizeUncompressed
		{
			get 
            {
                if (_z.total_in > int.MaxValue)
                {
                    string msg = StringTables.GetString(C1ZipFile.ERR_TOOLARGE_UNCOMPRESSED);
                    throw new ZipFileException(msg);
                }
                return (int)_z.total_in; 
            }
		}
        /// <summary>
        /// Gets the number of bytes in the stream (compressed bytes).
        /// </summary>
        public long SizeCompressedLong
        {
            get { return _z.total_out; }
        }
        /// <summary>
        /// Gets the number of bytes that were compressed into the stream (uncompressed bytes).
        /// </summary>
        public long SizeUncompressedLong
        {
            get { return _z.total_in; }
        }

		// ** provide access to underlying ZStream (for advanced users)
        
		/// <summary>
		/// Gets the ZStream instance wrapped by this <see cref="C1ZStreamWriter"/>.
		/// </summary>
		/// <remarks>
		/// This property is useful only in advanced applications that need to customize 
		/// the low-level behavior of the compressor. It is not needed in common applications.
		/// </remarks>
		public ZStream ZStream
		{
			get { return this._z; }
		}
		
		// ** implementation of Stream abstract members

		/// <summary>
		/// Always returns False.
		/// </summary>
        override public bool CanRead
        {
            get { return false; }
        }
		/// <summary>
		/// Always returns True.
		/// </summary>
		override public bool CanWrite
        {
            get { return true; }
        }
		/// <summary>
		/// Always returns False.
		/// </summary>
		override public bool CanSeek
        {
            get { return false; }
        }
		/// <summary>
		/// Returns the length of the underlying stream, in bytes.
		/// </summary>
		override public long Length
        {
            get { return _baseStream.Length; }
        }
		/// <summary>
		/// Gets the position within the stream (read-only).
		/// </summary>
        override public long Position
        {
            get { return _baseStream.Position; }
			set 
			{ 
				string msg = StringTables.GetString("Seek not supported."); 
				throw new NotSupportedException(msg); 
			}
        }
		/// <summary>
		/// Not supported.
		/// </summary>
        override public long Seek(long offset, SeekOrigin origin)
        {
			string msg = StringTables.GetString("Seek not supported."); 
			throw new NotSupportedException(msg); 
        }
		/// <summary>
		/// Not supported.
		/// </summary>
		override public void SetLength(long value)
        {
			string msg = StringTables.GetString("SetLength not supported.");
			throw new NotSupportedException(msg);
		}
		/// <summary>
		/// Not supported.
		/// </summary>
		override public int Read(byte[] buf, int offset, int count)
        {
			string msg = StringTables.GetString("Read not supported.");
			throw new NotSupportedException(msg);
		}
		/// <summary>
		/// Writes a sequence of bytes to the current stream and advances the current 
		/// position within this stream by the number of bytes written.
		/// </summary>
		/// <param name="buf">An array of bytes. This method copies <paramref name="count"/> bytes from
		/// <paramref name="buf"/> to the current stream.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buf"/> at which to begin copying bytes to the current stream.</param>
		/// <param name="count">The number of bytes to be written to the current stream.</param>
		/// <remarks>
		/// The data is compressed as it is written into the stream. Writing <paramref name="count"/> bytes
		/// into the stream will usually advance the position by a number smaller than <paramref name="count"/>.
		/// </remarks>
        override public void Write(byte[] buf, int offset, int count)
        {
            // write in chunks of 500 bytes for safety, no flush!! (TFS 15568, 18671)
            var blockSize = 500;
            for (int start = offset; start < offset + count; start += blockSize)
            {
                var cnt = Math.Min(blockSize, offset + count - start);
                write(buf, start, cnt);
            }

            // original, write it all at once
            //write(buf, offset, count);
        }
		/// <summary>
		/// Clears all buffers for this stream and causes any buffered data to be written to the underlying 
		/// stream.
		/// </summary>
        override public void Flush()
        {
			flush();
			_baseStream.Flush();
        }
		/// <summary>
		/// <para>Closes the current stream compressor and flushed any pending data into the base stream.</para>
		/// <para>If the <see cref="OwnsBaseStream"/> property is set to True (the default value), 
		/// then this method also closes the base stream and releases any resources (such as sockets 
		/// and file handles) associated with it.</para>
		/// </summary>
		override public void Close()
		{
			finish();
            if (_ownsBase)
                _baseStream.Close();
		}

        // The default implementation on Stream creates a new single-byte array 
        // and then calls Read. While this is formally correct, it is inefficient. 
        // Any stream with an internal buffer should override this method and 
        // provide a much more efficient version that reads the buffer directly, 
        // avoiding the extra array allocation on every call.
        byte[] _wb = new byte[1];

		/// <summary>
		/// Writes a byte to the current position in the stream and advances the position within the stream by one byte.
		/// </summary>
		/// <param name="value">Value to be written to the stream.</param>
        override public void WriteByte(byte value)
        {
            _wb[0] = value;
            write(_wb, 0, 1);
        }
		#endregion

		//--------------------------------------------------------------------------------
		#region ** internal

		internal void write(byte[] b, int off, int len)
		{
			// no work?
			if (len == 0) return;

			// can't write after the compressor is finished
			if (_finished)
			{
				string msg = StringTables.GetString("Can't write data after call to Finish().");
				throw new ZStreamException(msg);
			}

			// initialize ZStream
			_z.next_in = b;
			_z.next_in_index = off;
			_z.avail_in = len;

			// write bytes to underlying stream
			do 
			{
                // deflate data
                //int err = _z.deflate(ZStream.Z_NO_FLUSH);

                // apply synch flush after each 1/2 meg, for safety <<B53>>, VNZIP000051
                int flush = ZStream.Z_NO_FLUSH;
                if (_z.total_in - _flushed > 500 * 1024)
                {
                    flush = ZStream.Z_SYNC_FLUSH;
                    _flushed = _z.total_in;
                }

                // deflate data
                int err = _z.deflate(flush);

				// check result
				if (err != ZStream.Z_OK)
				{
					string msg = StringTables.GetString("Error deflating: ");
					throw new ZStreamException(msg + _z.msg);
				}

				// if there's anything available, write to underlying stream
				int cnt = _buf.Length - _z.avail_out;
				if (cnt > 0)
				{
					_baseStream.Write(_buf, 0, cnt);
					_z.avail_out = _buf.Length;
					_z.next_out = _buf;
					_z.next_out_index = 0;
				}
			}
			while (_z.avail_in > 0);
		}
		internal void flush()
		{
			// nothing to do if we're finished
			if (_finished) return;

			// flush all pending output
			while (true)
			{
				int err = _z.deflate(ZStream.Z_SYNC_FLUSH);
				if (err == ZStream.Z_STREAM_END) break;
				if (err != ZStream.Z_OK) 
				{
					string msg = StringTables.GetString("Error deflating: ");
					throw new ZStreamException(msg + _z.msg);
				}
				int cnt = _buf.Length - _z.avail_out;
				if (cnt > 0)
				{
					_baseStream.Write(_buf, 0, cnt);
					_z.avail_out = _buf.Length;
					_z.next_out = _buf;
					_z.next_out_index = 0;
				}
			}
		}
		internal void finish()
		{
			// only finish once
			if (_finished) return;

			// flush all pending output
			int err, cnt;
			while (true)
			{
				err = _z.deflate(ZStream.Z_FINISH);
				if (err == ZStream.Z_STREAM_END) break;
				if (err != ZStream.Z_OK) 
				{
					string msg = StringTables.GetString("Error deflating: ");
					throw new ZStreamException(msg + _z.msg);
				}
				cnt = _buf.Length - _z.avail_out;
				if (cnt > 0)
				{
					_baseStream.Write(_buf, 0, cnt);
					_z.avail_out = _buf.Length;
					_z.next_out = _buf;
					_z.next_out_index = 0;
				}
			}

			// finish deflating
			err = _z.deflateEnd();      
			if (err != ZStream.Z_OK) 
			{
				string msg = StringTables.GetString("Error deflating: ");
				throw new ZStreamException(msg + _z.msg);
			}
			cnt = _buf.Length - _z.avail_out;
			if (cnt > 0)
				_baseStream.Write(_buf, 0, cnt);

			// done compressing this stream
			_finished = true;
		}
		#endregion
	}
}
