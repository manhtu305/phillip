//----------------------------------------------------------------------------
// C1\C1Zip\C1ZipEntry.cs
//
// C1CryptStream reads and writes encrypted streams. It is used as a filter
// to read and write compressed zip entries.
//
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status           Date            By                      Comments
//----------------------------------------------------------------------------
// Created          Nov, 2001       Rodrigo             -
//----------------------------------------------------------------------------

using System;
using System.IO;
using C1.C1Zip.ZLib;

namespace C1.C1Zip
{
    /// <summary>
    /// C1CryptStream
    /// reads and decrypts data from an encrypted zip base stream, or
    /// encrypts and writes data to an encrypted zip base stream
    /// </summary>
    internal class C1CryptStream : Stream
    {
        Stream      _baseStream;
        C1ZipFile   _zip;
        C1ZipEntry  _ze;
        string      _password;
        uint[]      _keys;
        long        _crcValue;
        IChecksum   _crc;

        internal C1CryptStream(C1ZipEntry ze, Stream baseStream) 
        {
            _keys = new uint[3];
            _baseStream = baseStream;
            _ze = ze;
            _zip = ze._owner;
            _password = _zip._password;
            _crc = new CRC32();
        }

        // ** implementation of Stream abstract members

        override public bool CanRead    { get { return _baseStream.CanRead;  }}
        override public bool CanWrite   { get { return _baseStream.CanWrite; }}
        override public bool CanSeek    { get { return _baseStream.CanSeek;  }}
        override public long Length     { get { return _baseStream.Length;   }}
        override public long Position
        {
            get { return _baseStream.Position; }
            set { _baseStream.Position = value; }
        }
        override public long Seek(long offset, SeekOrigin origin)
        {
            return _baseStream.Seek(offset, origin);
        }
        override public void SetLength(long value)
        {
            _baseStream.SetLength(value);
        }
        override public void Flush()
        {
            _baseStream.Flush();
        }
        override public void Close()
        {
            _baseStream.Close();
        }

        // read data into buffer, decrypt buffer, return result
        override public int Read(byte[] buf, int offset, int count)
        {
            count = _baseStream.Read(buf, offset, count);
            CryptDecodeBuffer(buf, count);
            return count;
        }

        // encrypt buffer, write it out to base stream
        override public void Write(byte[] buf, int offset, int count)
        {
            CryptEncodeBuffer(buf, offset, count);
            _crcValue = _crc.checkSum(_crcValue, buf, offset, count);
            _baseStream.Write(buf, offset, count);
        }

        // ** encryption/decryption methods

        internal const int ENCR_HEADER_LEN = 12;
        internal bool InitKeys()
        {
            // initialize crypt keys
            CryptInitKeys();

            // check header and update crypt keys
            return CryptCheck();
        }
        internal void CryptDecodeBuffer(byte[] buf, int count)
        {
            for (int i = 0; i < count; i++)
            {
                buf[i] = CryptDecode(buf[i]);
            }
        }
        internal byte CryptEncode(byte b)
        {
            byte t = CryptDecryptByte();
            CryptUpdateKeys((char)b);
            b ^= t;
            return b;
        }
        internal void CryptEncodeBuffer(byte[] buf, int offset, int count)
        {
            for (int i = 0; i < count; i++)
            {
                buf[offset + i] = CryptEncode(buf[offset + i]);
            }
        }
        internal void CryptCryptHeader(long crc, byte[] buf)
        {
            // initialize crypt keys
            CryptInitKeys();
            
            // generate pseudo-random sequence and encrypt it
            byte c;
            Random r = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < ENCR_HEADER_LEN - 2; i++)
            {
                int rand = r.Next(); 
                c = (byte)(rand >> 6);
                if (c == 0) c = (byte)rand;
                buf[i] = CryptEncode(c);
            }

            // calculate last two bytes based on crc, and encrypt them too
            c = (byte)((crc >> 16) & 0xff);
            buf[ENCR_HEADER_LEN - 2] = CryptEncode(c);
            c = (byte)((crc >> 24) & 0xff);
            buf[ENCR_HEADER_LEN - 1] = CryptEncode(c);
        }
        void CryptInitKeys()
        {
            _keys[0] = 305419896;
            _keys[1] = 591751049;
            _keys[2] = 878082192;
			for (int i = 0; i < _password.Length; i++)
			{
				CryptUpdateKeys(_password[i]);
			}
        }
        void CryptUpdateKeys(char c)
        {
            _keys[0] = CryptCRC32(_keys[0], c);
            _keys[1] += _keys[0] & 0xff;
            _keys[1] = _keys[1] * 134775813 + 1;
            c = (char)(_keys[1] >> 24);
            _keys[2] = CryptCRC32(_keys[2], c);
        }
		static uint CryptCRC32(uint l, char c)
		{
			return CRC32.crc_table[(l ^ c) & 0xff] ^ (l >> 8);
		}
		bool CryptCheck()
        {
            // read and decode encrypted header
            byte b = 0;
			byte[] buf = new byte[C1ZipFile.BUFFERSIZE];
			_baseStream.Read(buf, 0, ENCR_HEADER_LEN);
            for (int i = 0; i < ENCR_HEADER_LEN; i++)
            {
                b = CryptDecode(buf[i]);
            }

            // check last byte to validate
            return _ze.HasDataDescriptor
                ? (byte)(_ze._modTime >> 8)  == b
                : (byte)(_ze._crc32   >> 24) == b;
        }
        byte CryptDecryptByte()
        {
            uint temp = (_keys[2] & 0xffff) | 2;
            return (byte)(((temp * (temp ^ 1)) >> 8) & 0xff);
        }
        byte CryptDecode(byte b)
        {
            b ^= CryptDecryptByte();
            CryptUpdateKeys((char)b);
            return b;
        }
    }
}
