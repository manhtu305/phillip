//----------------------------------------------------------------------------
// C1.Util.Localization.StringTables
//----------------------------------------------------------------------------
//
// Populates localized string tables used by the Localizer class.
//
// This file is project-specific (except for licensing-related resources
// such as About Box).
//
//----------------------------------------------------------------------------
// Copyright (C) 2001-2005 ComponentOne LLC
//----------------------------------------------------------------------------
// Status	Date		By			Comments
//----------------------------------------------------------------------------
// Created	May 2002	Bernardo	-
// Modified Jan 2005	Bernardo	Added Japanese strings
//----------------------------------------------------------------------------
using System;
using System.Globalization;
using System.Collections;

namespace C1.C1Zip
{
	/// <summary>
	/// Provide localization for error messages in Japanese.
	/// </summary>
	internal class StringTables
	{
        // as per CA1812:prevent compiler from generating a default ctor
        private StringTables() { }

#if EXPOSE_ZIP
		private static Hashtable _htGetStr = null;

		internal static string GetString(string key)
		{
			if (_htGetStr == null) InitTable();
			string desc = _htGetStr[key] as string;
			return (desc != null)? desc: key;
		}
		internal static void InitTable()
		{
			_htGetStr = new Hashtable();
			string locale = CultureInfo.CurrentCulture.Name;
			if (locale.StartsWith("ja"))
			{
				_htGetStr.Add("Bad parameters in call to Adler.update.", "Adler.update 呼び出しにおける不正なパラメータです。");
				_htGetStr.Add("Bad parameters in call to CRC32.update.", "CRC32.update 呼び出しにおける不正なパラメータです。");
				_htGetStr.Add("baseStream must be writable stream.", "baseStream は書き込み可能なストリームでなければなりません。");
				_htGetStr.Add("C1ZStreamReader needs a readable stream.", "C1ZStreamReader には読み込み可能なストリームが必要です。");
				_htGetStr.Add("Cannot find entry '{0}'.", "エントリ'{0}'を見つけることができません。");
				_htGetStr.Add("Cannot overwrite hidden file.", "隠しファイルに上書きできません。");
				_htGetStr.Add("Cannot overwrite read-only file.", "読み取り専用のファイルに上書きできません。");
				_htGetStr.Add("Cannot overwrite system file.", "システムファイルに上書きできません。");
				_htGetStr.Add("Can't add a zip file to itself.", "Zip ファイルにそれ自身を追加できません。");
				_htGetStr.Add("Can't extract entry over parent zip file.", "親の Zip ファイル外にエントリを抽出できません。");
				_htGetStr.Add("Can't write data after call to Finish().", "Finish() 呼び出しの後にデータを書き込むことはできません。");
				_htGetStr.Add("Central dir not found.", "中央のディレクトリが見つかりません。");
				_htGetStr.Add("Compression method not supported by C1Zip.", "C1Zip がサポートしない圧縮方法です。");
				_htGetStr.Add("Disk spanning not supported.", "ディスク分割機能はサポートしていません。");
				_htGetStr.Add("Error deflating: ", "deflate 中のエラーです。");
				_htGetStr.Add("Error extracting entry from Zip file.", "Zip ファイルからのエントリの抽出エラーです。");
				_htGetStr.Add("Error inflating: ", "inflate 中のエラーです。");
				_htGetStr.Add("Error reading data from Zip file (file may be corrupted).", "Zip ファイルからのデータ読み込みエラーです（ファイルが壊れています）。");
				_htGetStr.Add("Error writing data to Zip file (file may be corrupted).", "Zip ファイルへのデータ書き込みエラーです（ファイルが壊れています）。");
				_htGetStr.Add("Failed to initialize compressed stream.", "圧縮されたストリームの初期化に失敗しました。");
				_htGetStr.Add("File not found: '{0}'.", "ファイルが見つかりません: '{0}'");
				_htGetStr.Add("Invalid destination folder: ", "展開先フォルダが不正です。");
				_htGetStr.Add("Invalid index or entry name.", "インデックスまたはエントリ名が不正です。");
				_htGetStr.Add("Invalid password: can't decrypt data.", "パスワードが不正です：データを復号化できません。");
				_htGetStr.Add("Password not set: can't decrypt data.", "パスワードが設定されていません：データを復号化できません。");
				_htGetStr.Add("Read not supported.", "読み込みはサポートされていません。");
				_htGetStr.Add("Seek not supported. ", "シークはサポートされていません。");
				_htGetStr.Add("SetLength not supported.", "SetLength はサポートされていません。");
				_htGetStr.Add("This is not a Zip file (wrong signature).", "これは Zip ファイルではありません（不正な署名）。");
				_htGetStr.Add("Write not supported.", "書き込みはサポートされていません。");
				_htGetStr.Add("Zip file format unrecognized (bytesBeforeZip != 0).", "Zip ファイルの書式が認識できません（bytesBeforeZip が０ではありません）");
				_htGetStr.Add("Zip file is corrupted (wrong comment size).", "Zip ファイルが壊れています（不正なコメントサイズ）。");
				_htGetStr.Add("Zip file is corrupted (wrong entry comment size).", "Zip ファイルが壊れています（不正なエントリコメントのサイズ）。");
				_htGetStr.Add("Zip file is corrupted (wrong entry headers).", "Zip ファイルが壊れています（不正なエントリヘッダ）。");
				_htGetStr.Add("Zip file is corrupted (wrong entry name size).", "Zip ファイルが壊れています（不正なエントリ名のサイズ）。");
				_htGetStr.Add("Zip file is corrupted (wrong header size).", "Zip ファイルが壊れています（不正なヘッダサイズ）。");
                //_htGetStr.Add("Number of entries exceeds the 64K Zip limit.", "?");
                //_htGetStr.Add("Entry '{0}' exceeds the 4G Zip size limit.", "?");
            }
		}
#else
		internal static string GetString(string key)
		{
			return key;
		}
#endif
	}
}
