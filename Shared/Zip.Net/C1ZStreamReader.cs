//----------------------------------------------------------------------------
// C1\C1Zip\C1ZStreamReader.cs
//
// C1ZStreamReader implements a regular input Stream that retrieves 
// decompressed data from a compressed Stream.
//
// To use C1ZStreamReader, open a compressed stream of any type (file, 
// memory, etc), then create a C1ZStreamReader and pass the compressed stream 
// as an argument in the constructor. You can then read the decompressed data 
// from the C1ZStreamReader.
//
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status           Date            By                      Comments
//----------------------------------------------------------------------------
// Created          Nov, 2001       Bernardo                -
//----------------------------------------------------------------------------

using System;
using System.IO;
using System.Diagnostics;

namespace C1.C1Zip
{
	using C1.C1Zip.ZLib;
	
#if EXPOSE_ZIP
	/// <summary>
	/// Decompresses data from .NET streams.
	/// </summary>
	/// <remarks>
	/// <para>To decompress data from a compressed stream, create a <see cref="C1ZStreamReader"/> object 
	/// passing the compressed stream to the <see cref="C1ZStreamReader"/> constructor.</para>
	/// <para>Then read the data from the <see cref="C1ZStreamReader"/> using the <see cref="Read"/>
	/// method, or create a <see cref="StreamReader"/> on the <see cref="C1ZStreamReader"/>. 
	/// The second option is indicated when you want to read formatted data.</para>
	/// </remarks>
	/// <example>
	/// The code below decompresses a string that was stored into a memory stream object:
	/// <code>
	/// public string ExpandString(byte[] buffer)
	/// {
	///		// turn buffer into a memory stream
	///		var ms = new MemoryStream(buffer);
	///		
	///		// attach decompressor stream to memory stream
	///		var sr = new C1ZStreamReader(ms);
	///		
	///		// read uncompressed data
	///		var reader = new StreamReader(sr);
	///		return reader.ReadToEnd();
	///	}
	/// </code>
	/// </example>
	public class C1ZStreamReader : Stream
#else
	internal class C1ZStreamReader : Stream
#endif
    {
		//--------------------------------------------------------------------------------
		#region ** constants

		const int BUFFERSIZE = 32 * 1024;

		#endregion

		//--------------------------------------------------------------------------------
		#region ** fields

        ZStream _z;                 // base ZStream (decompressor)
        Stream  _baseStream;        // base Stream (compressed data store)
        byte[]  _buf;               // work buffer (raw compressed data)
        byte[]  _rb = new byte[1];  // used only in ReadByte
        bool    _noMoreInput;       // done reading input buffer
        long    _length;            // # of compressed bytes (-1 means unknown)
        long    _start;             // starting position within base stream
        bool    _ownsBase;          // close baseStream when we close
		bool    _stored;            // stored file (not compressed)

		#endregion

		//--------------------------------------------------------------------------------
		#region ** ctors

		internal C1ZStreamReader() {} // for inheritors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamReader"/> class.
		/// </summary>
		/// <param name="baseStream">Input stream that contains the compressed data.</param>
		public C1ZStreamReader(Stream baseStream) 
        {
            Init(baseStream, true, false);
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamReader"/> class.
		/// </summary>
		/// <param name="baseStream">Input stream that contains the compressed data.</param>
		/// <param name="zip">Specifies whether the compressed stream was created in zip format.</param>
		/// <param name="sizeCompressed">Specifies the number of compressed bytes to read from the stream.</param>
		/// <remarks>
		/// The <paramref name="sizeCompressed"/> parameter is needed only when a single stream contains
		/// several compressed streams (in zip files for example). If this parameter is not 
		/// specified, it is assumed that the stream contains a single stream of compressed data.
		/// </remarks>
        public C1ZStreamReader(Stream baseStream, bool zip, int sizeCompressed)
        {
            bool header = !zip;
            bool crc32 = zip;
            Init(baseStream, header, crc32);
            _length = sizeCompressed;
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamReader"/> class.
		/// </summary>
		/// <param name="baseStream">Input stream that contains the compressed data.</param>
		/// <param name="zip">Specifies whether the compressed stream was created in zip format.</param>
		public C1ZStreamReader(Stream baseStream, bool zip)
        {
            bool header = !zip;
            bool crc32 = zip;
            Init(baseStream, header, crc32);
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamReader"/> class.
		/// </summary>
		/// <param name="baseStream">Input stream that contains the compressed data.</param>
		/// <param name="header">Specifies whether the compressed stream contains header information (should be False for streams in zip files).</param>
		/// <param name="crc32">Specifies whether the compressed stream contains a CRC32 checksum (should be True for streams in zip files).</param>
		public C1ZStreamReader(Stream baseStream, bool header, bool crc32)
        {
            Init(baseStream, header, crc32);
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamReader"/> class.
		/// </summary>
		/// <param name="baseStream">Input stream that contains the compressed data.</param>
		/// <param name="zip">Specifies whether the compressed stream was created in zip format.</param>
		/// <param name="sizeCompressed">Specifies the number of compressed bytes to read from the stream.</param>
		/// <param name="method">Specifies the method that was used to compress the stream.</param>
        public C1ZStreamReader(Stream baseStream, bool zip, int sizeCompressed, int method)
            : this(baseStream, zip, (long)sizeCompressed, method)
        {

        }
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ZStreamReader"/> class.
		/// </summary>
		/// <param name="baseStream">Input stream that contains the compressed data.</param>
		/// <param name="zip">Specifies whether the compressed stream was created in zip format.</param>
		/// <param name="sizeCompressed">Specifies the number of compressed bytes to read from the stream.</param>
		/// <param name="method">Specifies the method that was used to compress the stream.</param>
		public C1ZStreamReader(Stream baseStream, bool zip, long sizeCompressed, int method)
		{
			bool header = !zip;
			bool crc32 = zip;
			Init(baseStream, header, crc32);
			_length = sizeCompressed;
			_stored = (method == 0);

			// we only support two methods: uncompressed and deflated <<B27>>
			if (method != 0 && method != 8)
			{
				string msg = StringTables.GetString("Compression method not supported by C1Zip.");
                throw new ZStreamException(msg);
			}
		}

		// ctor utility
		internal void Init(Stream baseStream, bool header, bool crc32)
        {
            // baseStream must be readable to provide the source (compressed) data
			if (baseStream == null || !baseStream.CanRead)
			{
				string msg = StringTables.GetString("C1ZStreamReader needs a readable stream.");
				throw new ArgumentException(msg);
			}

            // initialize members
            _noMoreInput = false;
            _buf		 = new byte[BUFFERSIZE];
            _baseStream  = baseStream;
            _start		 = 0;
			_length		 = -1; // unknown
			_stored      = false;
            
            // try getting start position from baseStream
			// (if we can't, just use zero)
			try
			{
				_start = baseStream.Position;
			} 
			catch {}

            // initialize ZStream buffer
            _z = new ZStream(crc32);
            _z.next_in_index = 0;
            _z.avail_in = 0;
            _z.next_in = _buf;
            int err = _z.inflateInit(header? ZStream.DEF_WBITS: -ZStream.DEF_WBITS);

            // sanity
			if (err != ZStream.Z_OK)
			{
				string msg = StringTables.GetString("Failed to initialize compressed stream.");
				throw new ZStreamException(msg);
			}
        }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** public

		/// <summary>
		/// Gets the underlying stream that contains the compressed data.
		/// </summary>
		public Stream BaseStream
        {
            get { return _baseStream; }
        }
		/// <summary>
		/// Gets or sets whether calling the <see cref="Close"/> method will also
		/// close the underlying stream (see <see cref="BaseStream"/>).
		/// </summary>
		public bool OwnsBaseStream
        {
            get { return _ownsBase; }
            set { _ownsBase = value; }
        }

        // ** provide access to uncompressed and compressed byte counts

		/// <summary>
		/// Gets the number of bytes in the stream (compressed bytes).
		/// </summary>
		public int SizeCompressed
        {
			get 
            {
                if (_z.total_in > int.MaxValue)
                {
                    string msg = StringTables.GetString(C1ZipFile.ERR_TOOLARGE_COMPRESSED);
                    throw new ZipFileException(msg);
                }
                return (int)_z.total_in; 
            }
        }
		/// <summary>
		/// Gets the number of bytes that were compressed into the stream (uncompressed bytes).
		/// </summary>
		public int SizeUncompressed
        {
            get 
            {
                if (_z.total_out > int.MaxValue)
                {
                    string msg = StringTables.GetString(C1ZipFile.ERR_TOOLARGE_UNCOMPRESSED);
                    throw new ZipFileException(msg);
                }
                return (int)_z.total_out; 
            }
        }
        /// <summary>
        /// Gets the number of bytes in the stream (compressed bytes).
        /// </summary>
        public long SizeCompressedLong
        {
            get { return _z.total_in; }
        }
        /// <summary>
        /// Gets the number of bytes that were compressed into the stream (uncompressed bytes).
        /// </summary>
        public long SizeUncompressedLong
        {
            get { return _z.total_out; }
        }

        // ** provide access to underlying ZStream (for advanced users)
        
#if EXPOSE_ZIP
		/// <summary>
		/// Gets the ZStream instance wrapped by this <see cref="C1ZStreamWriter"/>.
		/// </summary>
		/// <remarks>
		/// This property is useful only in advanced applications that need to customize 
		/// the low-level behavior of the compressor. It is not needed in common applications.
		/// </remarks>
		public ZStream ZStream
#else
		internal ZStream ZStream
#endif
		{
            get { return this._z; }
        }
        
        // ** implementation of Stream abstract members

		/// <summary>
		/// Always returns True.
		/// </summary>
		override public bool CanRead
        {
            get { return true; }
        }
		/// <summary>
		/// Always returns False.
		/// </summary>
		override public bool CanWrite
        {
            get { return false; }
        }
		/// <summary>
		/// Always returns False.
		/// </summary>
		override public bool CanSeek
        {
            get { return false; }
        }
		/// <summary>
		/// Gets the length of the compressed stream if it is known (or -1 if the
		/// length of the compressed stream is unknown).
		/// </summary>
		override public long Length
        {
            get { return _length < 0 ? 0 : _length; }
        }
		/// <summary>
		/// Gets the position within the stream (read-only).
		/// </summary>
		override public long Position
        {
            get { return _baseStream.Position - _start; }
            set
			{
				string msg = StringTables.GetString("Seek not supported."); 
				throw new NotSupportedException(msg); 
			}
        }
		/// <summary>
		/// Not supported.
		/// </summary>
		override public long Seek(long offset, SeekOrigin origin)
        {
			string msg = StringTables.GetString("Seek not supported."); 
			throw new NotSupportedException(msg); 
        }
		/// <summary>
		/// Sets the number of compressed bytes to read from the underlying stream.
		/// </summary>
		override public void SetLength(long value)
        {
            _length = value;
        }
		/// <summary>
		/// Reads a sequence of bytes from the underlying compressed stream, decompressing them into
		/// a buffer, then advances the position within the stream by the number of bytes read.
		/// </summary>
		/// <param name="buf">An array of bytes. When this method returns, <paramref name="buf"/> contains the specified byte array with the values between <paramref name="offset"/>and (<paramref name="offset"/> + <paramref name="count"/>) replaced by the uncompressed data read from the stream.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buf"/> at which to begin storing the data read from the current stream.</param>
		/// <param name="count">The maximum number of (decompressed) bytes to be read from the current stream.</param>
		/// <returns>The total number of bytes read into the buffer. This may be less than the number of bytes 
		/// requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.
		/// </returns>
        override public int Read(byte[] buf, int offset, int count)
        {
            return read(buf, offset, count);
        }
        /// <summary>
        /// Reads a byte from the stream and advances the position within the stream 
        /// by one byte, or returns -1 if at the end of the stream.
        /// </summary>
        /// <returns>The unsigned byte cast to an <see cref="Int32"/>, or -1 if at 
        /// the end of the stream.
        /// </returns>
        override public int ReadByte()
        {
            return read(_rb, 0, 1) == 0 ? -1 : (int)_rb[0];
        }
        /// <summary>
		/// This method is overridden and is not supported by the <see cref="C1ZStreamReader"/> class.
		/// </summary>
		override public void Write(byte[] buf, int offset, int count)
        {
			string msg = StringTables.GetString("Write not supported.");
            throw new NotSupportedException(msg);
        }
		/// <summary>
		/// Clears all buffers for this stream and causes any buffered data to be written to the underlying 
		/// stream.
		/// </summary>
		override public void Flush()
        {
            _baseStream.Flush();
        }
		/// <summary>
		/// <para>Closes the current stream compressor and flushed any pending data into the base stream.</para>
		/// <para>If the <see cref="OwnsBaseStream"/> property is set to True (the default value), 
		/// then this method also closes the base stream and releases any resources (such as sockets 
		/// and file handles) associated with it.</para>
		/// </summary>
		override public void Close()
        {
            if (_ownsBase)
            {
                _baseStream.Close();
            }
		}

		#endregion

		//--------------------------------------------------------------------------------
		#region ** private

        // For efficiency, as recommended in MS docs:
        // The default implementation on Stream creates a new single-byte array 
        // and then calls Read. While this is formally correct, it is inefficient. 
        // Any stream with an internal buffer should override this method and 
        // provide a much more efficient version that reads the buffer directly, 
        // avoiding the extra array allocation on every call.
        byte[] _xb = new byte[BUFFERSIZE];
        int _xpos = 0;
        int _xlen = 0;

        // read exactly len bytes as long as there's input
        int read(byte[] b, int offset, int len)
        {
            int totalRead = 0;
            while (totalRead < len)
            {
                // read from internal buffer
                for (int i = _xpos; i < _xlen && totalRead < len; i++)
                {
                    b[offset++] = _xb[_xpos++];
                    totalRead++;
                }

                // need more? refill buffer
                if (totalRead < len)
                {
                    _xpos = 0;
                    _xlen = readInternal(_xb, 0, _xb.Length);
                    if (_xlen == 0)
                    {
                        break; // no more input
                    }
                }
            }
            return totalRead;
        }

        // read between 1 and len bytes as long as there's input
        // return the number of bytes read
        int readInternal(byte[] b, int off, int len)
        {
            // no work?
            if (len == 0) return 0;

            // initialize ZStream
            _z.next_out = b;
            _z.next_out_index = off;
            _z.avail_out = len;

            // read bytes from base stream
            for (;;)
            {
                // if input buffer is empty and more input is available from the stream, refill buffer
                if (_z.avail_in == 0 && _noMoreInput == false)
                {
                    // calculate number of bytes to read
                    int toRead = _buf.Length;

                    // if the stream has a length, honor it
                    if (_length >= 0)
                    {
                        toRead = (int)Math.Min(toRead, _length - Position);
                    }

                    // read input
                    if (toRead > 0)
                    {
                        _z.next_in_index = 0;
                        _z.avail_in = _baseStream.Read(_buf, 0, toRead);
                        _noMoreInput = _z.avail_in < toRead;
                    }
                    else
                    {
                        _noMoreInput = true;
                    }
                }

                // expand or copy data from input to output buffer
                if (!_stored)
                {
                    // expand compressed data
                    int err = _z.inflate(ZStream.Z_NO_FLUSH);

                    // stream-end and out-of-input buffer errors are OK
                    switch (err)
                    {
                        case ZStream.Z_STREAM_END:
                            err = ZStream.Z_OK;
                            _noMoreInput = true; // << review: necessary in some cases, but 100% safe?
                            break;
                        case ZStream.Z_BUF_ERROR:
                            if (_noMoreInput)
                                err = ZStream.Z_OK;
                            break;
                    }

                    // throw on error
                    if (err != ZStream.Z_OK)
                    {
                        string msg = StringTables.GetString("Error inflating: ");
                        throw new ZStreamException(msg + _z.msg);
                    }
                }
                else
                {
                    // copy stored data from input stream
                    int cnt = Math.Min(_z.avail_in, _z.avail_out);
                    for (int i = 0; i < cnt; i++)
                    {
                        //_z.next_out[_z.next_out_index + i] = _buf[i + off]; // <<B48>>
                        _z.next_out[_z.next_out_index + i] = _z.next_in[_z.next_in_index + i]; // <<B44>>
                    }
                    _z.next_out_index += cnt;
                    _z.next_in_index += cnt;
                    _z.avail_in -= cnt;
                    _z.avail_out -= cnt;
                }

                // stop when we get at least one byte or when we run out of input
                int read = len - _z.avail_out;
                if (read > 0 || _noMoreInput)
                {
                    return read;
                }
            }

			// never gets here:
            // return number of bytes read
            //return len - _z.avail_out;
        }

#if false 

        // ** unbuffered old version, about 5x slower!!
        // read exactly len bytes as long as there's input
        int read(byte[] b, int offset, int len)
        {
            int totalRead = 0;
            while (len > 0)
            {
                int read = readInternal(b, offset, len);
                if (read == 0) break;
                totalRead += read;
                offset += read;
                len -= read;
            }
            return totalRead;
        }

        // ** old version, same thing but more complicated
        // read between 1 and len bytes as long as there's input
        // return the number of bytes read
        int readInternal(byte[] b, int off, int len)
        {
            // no work?
            if (len == 0) return 0;

            // initialize ZStream
            _z.next_out = b;
            _z.next_out_index = off;
            _z.avail_out = len;

            // read bytes from base stream
            do
            {
                // if buffer is empty and more input is available, refill it
                if (_z.avail_in == 0 && !_noMoreInput)
                {
                    // calculate number of bytes to read
                    int toRead = Math.Min(len, _buf.Length);

                    // if the stream has a length, honor it
                    if (_length >= 0)
                        toRead = (int)Math.Min(toRead, _length - Position);

                    // read input
                    _z.next_in_index = 0;
                    _z.avail_in = _baseStream.Read(_buf, 0, toRead);
                    //if (_z.avail_in == 0) <<B16>>
                    if (_z.avail_in == 0 || _z.avail_in < toRead)
                        _noMoreInput = true;
                }

                // inflate data from compressed stream
                // or just read it from stored stream <<B3>>
                int err = 0;
                if (_stored)
                {
                    int cnt = Math.Min(_z.avail_in, _z.avail_out);
                    for (int i = 0; i < cnt; i++)
                        _z.next_out[_z.next_out_index + i] = _buf[i];
                    _z.avail_in -= cnt;
                    _z.avail_out -= cnt;
                }
                else
                {
                    err = _z.inflate(ZStream.Z_NO_FLUSH);
                }

                // check result
                if (_noMoreInput && err == ZStream.Z_BUF_ERROR)
                    break;
                if (err != ZStream.Z_OK && err != ZStream.Z_STREAM_END)
                {
                    string msg = StringTables.GetString("Error inflating: ");
                    throw new ZStreamException(msg + _z.msg);
                }
                if (_noMoreInput && _z.avail_out == len)
                    break;
            }
            while (_z.avail_out == len); // repeat until we get at least one byte

            // return number of bytes read
            return len - _z.avail_out;
        }
#endif
        #endregion
    }
}
