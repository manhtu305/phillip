//----------------------------------------------------------------------------
// C1\C1Zip\C1ZipEntry.cs
//
// C1ZipEntry represents an individual entry in a C1ZipFile object.
//
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status           Date            By                  Comments
//----------------------------------------------------------------------------
// Created          Nov, 2001       Rodrigo             -
// Updated          Dec 2011        Bernardo            zip64 extensions
//----------------------------------------------------------------------------

using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace C1.C1Zip
{
	using C1.C1Zip.ZLib;

#if EXPOSE_ZIP
    /// <summary>
    /// Represents an entry in a zip file, and exposes information including the entry name,
    /// compressed and uncompressed sizes, date, checksum, etc.
    /// </summary>
#if false && !COMPACT_FRAMEWORK
	[
	ClassInterface(ClassInterfaceType.AutoDual),
	Guid("90E72D84-CBA8-4731-80C6-DB37D449CF3E")
	]
#endif
	public class C1ZipEntry
#else
#if WIJMOCONTROLS
    [SmartAssembly.Attributes.DoNotObfuscate]
#endif
    internal class C1ZipEntry
#endif
    {
		//--------------------------------------------------------------------------------
		#region ** constants

        internal static byte[]  CENTRAL_SIGN     = new byte[] {0x50, 0x4b, 0x01, 0x02};
        internal static byte[]  LOCAL_SIGN       = new byte[] {0x50, 0x4b, 0x03, 0x04};
        internal const int      HEADERSIZE       = 46;   // the entry header is stored in the zip's central dir
        internal const int      LOCALHEADERSIZE  = 30;   // the local header is stored just before the packed file
        internal const ushort   HDR_ZIP64        = 0x01; // header id for ZIP64 extra info
        internal const ushort   ZIPVERSION = 20;         // original (32-bit) zip format
        internal const ushort   ZIPVERSION64 = 45;       // zip64 format

        #endregion

        //--------------------------------------------------------------------------------
		#region ** fields

		internal C1ZipFile      _owner;         // owner zip file
        internal short          _flags;         // encrypted, data descriptor, etc
        internal short          _modTime;       // last modified time
        internal short          _modDate;       // last modified date
        internal int            _crc32;         // entry crc
        internal short          _method;        // method used to compress
        internal long           _szOriginal;    // original entry size
        internal long           _szComp;        // compressed entry size
        internal short          _attInternal;   // 1 for text, 0 for binary (unused)
        internal int            _attExternal;   // regular file attributes
        internal long           _offset;        // offset from zip start
        internal string         _fileName;      // entry file name
        internal string         _comment;       // entry comment

		#endregion

		//--------------------------------------------------------------------------------
		#region ** ctor

		internal C1ZipEntry(C1ZipFile owner)
        {
            _owner = owner;
        }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** public stuff

		/// <summary>
		/// Gets the entry name. This is usually a file name, optionally including a path.
		/// </summary>
        public string FileName
        {
            get { return _fileName; }
        }
		/// <summary>
		/// Gets the original (uncompressed) size of the entry, in bytes.
		/// </summary>
        public int SizeUncompressed
        {
            get 
            {
                if (_szOriginal > int.MaxValue)
                {
                    string msg = StringTables.GetString(C1ZipFile.ERR_TOOLARGE_UNCOMPRESSED);
                    throw new ZipFileException(msg, FileName);
                }
                return (int)_szOriginal; 
            }
        }
        /// <summary>
        /// Gets the compressed size of the entry, in bytes.
        /// </summary>
        public int SizeCompressed
        {
            get
            {
                if (_szComp > int.MaxValue)
                {
                    string msg = StringTables.GetString(C1ZipFile.ERR_TOOLARGE_COMPRESSED);
                    throw new ZipFileException(msg, FileName);
                }
                return (int)_szComp;
            }
        }
        /// <summary>
        /// Gets the original (uncompressed) size of the entry, in bytes, as a long integer.
        /// </summary>
        public long SizeUncompressedLong
        {
            get { return _szOriginal;  }
        }
        /// <summary>
		/// Gets the compressed size of the entry, in bytes, as a long integer.
		/// </summary>
		public long SizeCompressedLong
        {
            get { return _szComp; }
        }
		/// <summary>
		/// Gets the checksum calculated when the entry was compressed.
		/// </summary>
		/// <remarks>
		/// This value can be used to check the integrity of the entry when it is decompressed.
		/// </remarks>
		public int CRC32
        {
            get { return _crc32; }
        }
		/// <summary>
		/// Gets the date and time when the file used to create the entry was last modified.
		/// </summary>
		/// <remarks>
		/// This value can be used to check whether an entry needs to be updated because the
		/// source file was modified since it was last compressed.
		/// </remarks>
        public DateTime Date
        {
            get { return ToDateTime(_modDate, _modTime); }
        }
		/// <summary>
		/// Gets or sets a comment associated with the entry.
		/// </summary>
		public string Comment
        {
            get {  return _comment; }
            set {  _owner.SetEntryComment(FileName, value); }
        }
		/// <summary>
		/// Gets the file attributes associated with the entry.
		/// </summary>
        public FileAttributes Attributes
        {
            get { return (FileAttributes)_attExternal; }
        }
		/// <summary>
		/// Gets a value that determines whether the entry is encrypted.
		/// </summary>
		/// <remarks>
		/// Encrypted entries can only be extracted if the <see cref="C1.C1Zip.C1ZipFile.Password"/> property
        /// on the containing <see cref="C1ZipFile"/> object is set to the password that was used
		/// when the file was added to the zip file.
		/// </remarks>
        public bool IsEncrypted
        {
            get { return (_flags & 1) != 0; }
        }
		/// <summary>
		/// Calculates a checksum value for the entry and compares it to the checksum that
		/// was stored when the entry was created.
		/// </summary>
		/// <returns>True if the checksum values match, false otherwise.</returns>
		/// <remarks>
		/// This method is used to check the integrity of the entries in the zip file. If the
		/// calculated checksum does not match the stored checksum, then either the zip file is
		/// corrupted or the program used to create the zip file is incompatible with 
        /// <b>C1Zip</b>.
		/// </remarks>
        public bool CheckCRC32()
        {
            // read out of compressed stream and calculate CRC32
            long crc32val = 0;
            Stream cmpStream = null;
            try
            {
                cmpStream = OpenReader();
                var crc32 = new CRC32();
                var len = _szOriginal;
				var buf  = new byte[C1ZipFile.BUFFERSIZE];
				while (len > 0)
                {
                    int toRead = (int)Math.Min(buf.Length, len);
                    int read   = cmpStream.Read(buf, 0, toRead);
					if (read == 0) break; // <<B34>> extra safety
                    len -= (uint)read;
                    crc32val = crc32.checkSum(crc32val, buf, 0, read);
				}
            }
            catch // something bad happened, make sure we return an error
            {
                crc32val = _crc32 + 1;
                throw;
            }
            finally // always close files
            {
                if (cmpStream != null) cmpStream.Close();
            }

            // done
            return (int)crc32val == _crc32;
        }
        /// <summary>
        /// Checks whether this <see cref="C1ZipEntry"/> can be read with the currently set password.
        /// </summary>
        /// <returns>True if the entry can be read with the current password, or if the entry is not
        /// encrypted.</returns>
        /// <remarks>
        /// This method is more efficient than using a try/catch block and trying to open the entry
        /// to determine whether the current password is valid for the entry.
        /// </remarks>
        public bool CheckPassword()
        {
            return _owner.CheckPassword(this);
        }
		/// <summary>
		/// Returns a <see cref="Stream"/> that can be used to read the content of the entry without
		/// extracting it to a disk file.
		/// </summary>
        /// <returns>A <see cref="Stream"/> that can be used to read the data in the entry.</returns>
		/// <remarks>
        /// The <see cref="Stream"/> returned is a <see cref="C1ZStreamReader"/>, which 
        /// decompresses the data as it is read from the entry.
		/// </remarks>
        public Stream OpenReader()
        {
            return _owner.OpenReader(this);
        }
		/// <summary>
		/// Extracts this entry to a file.
		/// </summary>
		/// <param name="destFileName">Name and location of the extracted file.</param>
        public void Extract(string destFileName) // <<B56>>
        {
            _owner.Extract(this, destFileName);
        }
        /// <summary>
        /// Removes this entry from the zip file.
        /// </summary>
        public void Remove() // <<B56>>
        {
            _owner.Remove(this);
        }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** internal stuff

        // ** Zip64 Extended Information Extra Field (0x0001):
        // Value                 Size       Description
        // 0x0001                2 bytes    Tag for this "extra" block type
        // Size                  2 bytes    Size of this "extra" block
        // Original Size         8 bytes    Original uncompressed file size
        // Compressed Size       8 bytes    Size of compressed data
        // Rel Header Offset     8 bytes    Offset of local header record
        // Disk Start Number     4 bytes    Number of the disk on which this file starts 
        // ** NOTE: This entry in the Local header must include BOTH original
        //    and compressed file size fields.

        // process extra field info
        void ProcessExtraFieldInfo(byte[] xInfo)
        {
            if (xInfo != null && xInfo.Length > 4)
            {
                using (var ms = new MemoryStream(xInfo))
                using (var br = new BinaryReader(ms))
                {
                    for (; br.PeekChar() > -1; )
                    {
                        ushort id = br.ReadUInt16();
                        ushort sz = br.ReadUInt16();
                        if (id == HDR_ZIP64)
                        {
                            // check record size
                            if (sz > 28)
                            {
                                var msg = StringTables.GetString("Inconsistent datasize for ZIP64 extra field.");
                                throw new ZipFileException(msg, FileName);
                            }
                            
                            // uncompressed size
                            if (this._szOriginal == 0xffffffff)
                            {
                                CheckDouble(sz);
                                _szOriginal = br.ReadInt64();
                                sz -= 8;
                            }

                            // compressed size
                            if (this._szComp == 0xffffffff)
                            {
                                CheckDouble(sz);
                                _szComp = br.ReadInt64();
                                sz -= 8;
                            }

                            // offset
                            if (this._offset == 0xffffffff)
                            {
                                CheckDouble(sz);
                                _offset = br.ReadInt64();
                                sz -= 8;
                            }
                        }

                        // skip any extra info
                        ms.Position += sz;
                    }
                }
            }
        }
        void CheckDouble(int size)
        {
            if (size < 8)
            {
                string msg = StringTables.GetString("Missing data for ZIP64 extra field");
                throw new ZipFileException(msg, FileName);
            }
        }

        // get extra field info
        internal byte[] GetExtraFieldInfo(bool central, out bool size64, out bool offset64)
        {
            // see if we have to save the size and/or offset as 64-bit values
            offset64 = _offset >= uint.MaxValue;
            size64 = offset64 || SizeUncompressedLong >= uint.MaxValue || SizeCompressedLong >= uint.MaxValue;

            // create extra field info
            if (size64 || offset64)
            {
                // update
                using (var ms = new MemoryStream())
                using (var bw = new BinaryWriter(ms))
                {
                    // header
                    bw.Write(HDR_ZIP64);

                    // ** important:
                    var hdrSize = 0;
                    if (size64) hdrSize += 16;
                    if (offset64 && central) hdrSize += 8;
                    bw.Write((short)hdrSize);

                    // payload
                    if (size64)
                    {
                        bw.Write(_szOriginal);
                        bw.Write(_szComp);
                    }
                    if (offset64 && central)
                    {
                        bw.Write(_offset);
                    }
                    bw.Flush();

                    // done
                    return ms.ToArray();
                }
            }

            // no 64 bit data
            return null;
        }

        // check whether this entry requires zip64 extensions
        internal bool IsZip64()
        {
            if (SizeUncompressedLong >= uint.MaxValue || SizeCompressedLong >= uint.MaxValue)
                return true;

            if (_offset >= uint.MaxValue)
                return true;

            return false;
        }

        // gets the number of bytes in a string using the current encoding
        short GetStringLength(string str)
        {
            return string.IsNullOrEmpty(str)
                ? (short)0
                : (short)Encoding.GetByteCount(str);
        }

        // read entry header from the central dir
        internal bool ReadCentralHeader(Stream fs)
        {
            // check entry's signature
            var br = new BinaryReader(fs);
            var sig = br.ReadBytes(CENTRAL_SIGN.Length);
            if (sig.Length != CENTRAL_SIGN.Length || !C1ZipFile.Match(sig, CENTRAL_SIGN, 0))
            {
                return false;
            }

            // read in info
            var madeBy = br.ReadInt16();
            var needed = br.ReadInt16();
            _flags = br.ReadInt16();
            _method = br.ReadInt16();
            _modTime = br.ReadInt16();
            _modDate = br.ReadInt16();
            _crc32 = br.ReadInt32();
            _szComp = br.ReadUInt32();
            _szOriginal = br.ReadUInt32();
            var szFileName = br.ReadInt16();
            var szExtraInfo = br.ReadInt16();
            var szComment = br.ReadInt16();
            var diskStart = br.ReadInt16();
            _attInternal = br.ReadInt16();
            _attExternal = br.ReadInt32();
            _offset = br.ReadUInt32();

            // read file name
            var buf = br.ReadBytes(szFileName);
            if (buf.Length != szFileName)
            {
                var msg = StringTables.GetString("Zip file is corrupted (wrong entry name size).");
                throw new ZipFileException(msg, FileName);
            }
            _fileName = Encoding.GetString(buf, 0, buf.Length);

            // read extra field information (zip64 sizes)
            if (szExtraInfo > 0)
            {
                var xInfo = br.ReadBytes(szExtraInfo);
                ProcessExtraFieldInfo(xInfo);
            }

            // read comment
            _comment = string.Empty;
            if (szComment > 0)
            {
                buf = br.ReadBytes(szComment);
                if (buf.Length != szComment)
                {
                    string msg = StringTables.GetString("Zip file is corrupted (wrong entry comment size).");
                    throw new ZipFileException(msg, FileName);
                }
                _comment = Encoding.GetString(buf, 0, buf.Length);
            }

            // done
            return true;
        }

        // write entry header in the central dir
        internal uint WriteCentralHeader(Stream fs)
        {
            var pos = fs.Position;

            // update zip64 field information
            bool size64, offset64;
            var xInfo = GetExtraFieldInfo(true, out size64, out offset64);

            // save fixed members
            var bw = new BinaryWriter(fs);
            bw.Write(CENTRAL_SIGN);
            bw.Write(ZIPVERSION64);
            bw.Write(xInfo != null ? ZIPVERSION64 : ZIPVERSION);
            bw.Write(_flags);
            bw.Write(_method);
            bw.Write(_modTime);
            bw.Write(_modDate);
            bw.Write(_crc32);
            bw.Write(GetUIntValue(_szComp, size64));
            bw.Write(GetUIntValue(_szOriginal, size64));
            bw.Write(GetStringLength(FileName));
            bw.Write((short)(xInfo != null ? xInfo.Length : 0));
            bw.Write(GetStringLength(Comment));
            bw.Write((short)0);         // diskStart
            bw.Write(_attInternal);     // 1 for text, 0 for binary (unused)
            bw.Write(_attExternal);     // file attributes
            bw.Write(GetUIntValue(_offset, offset64));
            if (!string.IsNullOrEmpty(FileName))
            {
                bw.Write(Encoding.GetBytes(FileName));
            }
            if (xInfo != null)
            {
                bw.Write(xInfo);
            }
            if (!string.IsNullOrEmpty(Comment))
            {
                bw.Write(Encoding.GetBytes(Comment));
            }

            // return number of bytes written
            var size = HEADERSIZE + GetStringLength(FileName) + GetStringLength(Comment);
            if (xInfo != null) size += xInfo.Length;
            return (uint)size;
        }

        // read entry header just before the packed file
        internal bool ReadLocalHeader(Stream fs)
        {
            // position on local header
            fs.Position =
                _owner._bytesBeforeZip +    // padding on the top of the zip file (usually zero)
                _offset;

            // check entry's signature
            var br = new BinaryReader(fs);
            var sig = br.ReadBytes(LOCAL_SIGN.Length);
            if (sig.Length != LOCAL_SIGN.Length || !C1ZipFile.Match(sig, LOCAL_SIGN, 0))
            {
                return false;
            }

            // read in info
            var needed = br.ReadInt16();
            _flags = br.ReadInt16();
            _method = br.ReadInt16();
            _modTime = br.ReadInt16();
            _modDate = br.ReadInt16();
            var crc32 = br.ReadInt32();
            var szComp = br.ReadUInt32();
            var szOriginal = br.ReadUInt32();
            var szFileName = br.ReadInt16();
            var szExtraInfo = br.ReadInt16();

            // save CRCs and sizes if they are valid
            if (!HasDataDescriptor)
            {
                if (crc32 != 0)
                {
                    _crc32 = crc32;
                }
                if ((int)szComp != -1)
                {
                    _szComp = szComp;
                }
                if ((int)szOriginal != -1)
                {
                    _szOriginal = szOriginal;
                }
            }

            // skip over file name
            br.ReadBytes(szFileName);

            // read extra field information (zip64 entries)
            if (szExtraInfo > 0)
            {
                var xInfo = br.ReadBytes(szExtraInfo);
                ProcessExtraFieldInfo(xInfo);
            }

            // done
            return true;
        }

        // write local header just before the packed file
        internal int WriteLocalHeader(Stream fs)
        {
            // update zip64 field information
            bool size64, offset64;
            var xInfo = GetExtraFieldInfo(false, out size64, out offset64);

            // save fixed members
            var bw = new BinaryWriter(fs);
            bw.Write(LOCAL_SIGN);
            bw.Write(xInfo != null ? ZIPVERSION64 : ZIPVERSION);
            bw.Write(_flags);
            bw.Write(_method);
            bw.Write(_modTime);
            bw.Write(_modDate);
            bw.Write(_crc32);
            bw.Write(GetUIntValue(_szComp, size64));
            bw.Write(GetUIntValue(_szOriginal, size64));
            bw.Write(GetStringLength(FileName));
            bw.Write((short)(xInfo != null ? xInfo.Length : 0));
            if (!string.IsNullOrEmpty(FileName))
            {
                bw.Write(Encoding.GetBytes(FileName));
            }
            if (xInfo != null)
            {
                bw.Write(xInfo);
            }

            // return number of bytes written
            var size = LOCALHEADERSIZE + GetStringLength(FileName);
            if (xInfo != null) size += xInfo.Length;
            return size;
        }

        // gets a uint value from a long, or -1 if the entry is Zip64
        // (if the entry is zip64, this info goes in an extended record)
        static uint GetUIntValue(long value, bool zip64)
        {
            return (uint)(zip64 ? -1 : value);
        }

        // initialize entry for a new file to be compressed
        internal void SetNameAndFlags(string fileName, string entryName, DateTime dateTime)
        {
            // use the appropriate directory separator char
            entryName = AdjustDirSepChar(entryName);

            // set file/stream name
			_fileName = entryName;

            // set constants
            _comment = string.Empty;
            _method = (short)Deflate.Z_DEFLATED;
            SetFlags();
            
            // set file date/time
            SetTime(dateTime);
            _attExternal = (int)FileAttributes.Archive;

            // if the file exists (not a stream), then copy file attributes
            // if the file doesn't exist (we're adding a stream), then use default values
            if (fileName != null && (File.Exists(fileName) || Directory.Exists(fileName)))
            {
                //SetTime(File.GetLastWriteTime(fileName)); // already set (TFS 10242)
                var fi = new FileInfo(fileName);
                _attExternal = (int)fi.Attributes;
            }
        }

        // checks whether an entry has a data descriptor (non-seek, rare)
        internal bool HasDataDescriptor
        {
            get { return (_flags & 8) != 0; }
        }

        // gets the Encoding used to encode file names and comments
        Encoding Encoding
        {
            get { return this._owner.Encoding; }
        }

		#endregion

		//--------------------------------------------------------------------------------
		#region ** private stuff

        // convert DOS date/time (in the zip file) into DateTime value
        static DateTime ToDateTime(int wDosDate, int wDosTime)
        {
            ushort tm = (ushort)wDosTime;
            ushort dt = (ushort)wDosDate;

            int sec  = (tm & 0x1f) << 1;
            int min  = (tm >>  5) & 0x3f;
            int hour = (tm >> 11) & 0x1f;

            int day  = dt & 0x1f;
            int mon  = (dt >> 5) & 0x0f;
            int year = ((dt >> 9) & 0x7f) + 1980;

            // assume MinValue in case dos info is invalid <<B45>>
            DateTime d = DateTime.MinValue;
            if (mon > 0 && day > 0)
            {
                try
                {
                    d = new DateTime(year, mon, day, hour, min, sec);
                }
                catch { }
            }
            return d;
        }
        
        // convert DateTime value into DOS date/time
        void SetTime(DateTime time)
        {
            int year = Math.Max(time.Year - 1980, 0);
            _modDate = (short)(time.Day + (time.Month << 5) + (year << 9));
            _modTime = (short)((time.Second >> 1) + (time.Minute << 5) + (time.Hour << 11));
        }
        
        // set flags
        //
        //   Bit 0: indicates that the file is encrypted (we don't handle this!)
        //
        // compression method = 6 (Imploding, we don't handle this!)
        //   Bit 1: indicates an 8/4K sliding dictionary
        //   Bit 2: indicates 3/2 Shannon-Fano trees
        //
        // compression method = 8 (Deflating)
        //   Bits 1,2: 0 indicates normal compression 
        //   Bits 1,2: 2 indicates maximum compression 
        //   Bits 1,2: 4 indicates fast compression 
        //   Bits 1,2: 6 indicates super-fast compression 
        //
        //   Bit 3: crc-32, compressed, uncompressed sizes follow the header
        //   Bit 4: reserved for use with method 8, for enhanced deflating. 
        //   Bit 5: indicates that the file is compressed patched data.
        //
        // others: unused/reserved by PKWare
		//
		// Note: We only handle deflated/non-encrypted files, so we're only
		//       setting bits 1 and 2. They don't seem to be used by any
		//       decompressors anyway, so this is not a big deal.
        void SetFlags()
        {
            _flags = 0; // << normal compression
            switch (_owner.CompressionLevel)
            {
                case CompressionLevelEnum.BestCompression: _flags = 2; break;
                case CompressionLevelEnum.BestSpeed:       _flags = 4; break;
                case CompressionLevelEnum.NoCompression:   _flags = 6; break;
            }

            // encryption?
            bool bEncryption = (_owner.Password.Length > 0);
            if (bEncryption)
                _flags = (short)((ushort)_flags | 9);
        }

        // adjust the directory separator char
        static string AdjustDirSepChar(string str)
        {
            str = str.Replace(Path.DirectorySeparatorChar,     C1ZipFile.DIRSEPCHAR);
            return str.Replace(Path.AltDirectorySeparatorChar, C1ZipFile.DIRSEPCHAR);
        }
		#endregion
    }
}
