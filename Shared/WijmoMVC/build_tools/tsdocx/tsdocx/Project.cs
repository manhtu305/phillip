﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Linq;

namespace tsdocx
{
    /// <summary>
    /// Represents a tsdocx project.
    /// </summary>
    public partial class Project
    {
        List<ProjectItem> _items = new List<ProjectItem>();
        List<string> _errors = new List<string>();

        /// <summary>
        /// Gets or sets the name of the file that contains the project.
        /// </summary>
        public string ProjectFile { get; set; }
        /// <summary>
        /// Gets or sets the root directory for input files.
        /// </summary>
        public string InputPath { get; set; }
        /// <summary>
        /// Gets or sets the root directory for output files.
        /// </summary>
        public string OutputPath { get; set; }
        /// <summary>
        /// Gets the root directory for xml documentation files.
        /// </summary>
        public string XmlDocumentationPath { get; set; }
        /// <summary>
        /// Gets or sets whether to create a minified version of the output.
        /// </summary>
        public bool Minify { get; set; }
        /// <summary>
        /// Gets or sets whether to create documentation for the project.
        /// </summary>
        public bool Document { get; set; }
        /// <summary>
        /// Gets or sets a string to be pre-pended as a header to all minified files.
        /// </summary>
        public string FileHeader { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether source TS files should be included in the build.
        /// </summary>
        public bool IncludeSources { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether unminified versions of JS/CSS files should be included in the build.
        /// </summary>
        public string IncludeUnminified { get; set; }
        /// <summary>
        /// Gets or sets a list of conditional compilation symbols delimited by semicolon.
        /// </summary>
        public string Define { get; set; }
        /// <summary>
        /// Gets the list of output items in this project.
        /// </summary>
        public List<ProjectItem> Items { get { return _items; } }
        /// <summary>
        /// Gets a list of errors detected during the build.
        /// </summary>
        public List<string> Errors { get { return _errors; } }
        /// <summary>
        /// Gets the version of the target product.
        /// </summary>
        public string ProductVersion { get; set; }
        /// <summary>
        /// Actual build configuration used by the tool.
        /// </summary>
        public string Configuration { get; set; }

        /// <summary>
        /// Loads the project from an xml file.
        /// </summary>
        /// <param name="fileName">Name of the xml file that contains the project data.</param>
        public void Load(string fileName, string configName)
        {
            // save filename and config
            ProjectFile = fileName;
            Configuration = configName;

            // load document
            var doc = new XmlDocument();
            try
            {
                doc.Load(fileName);
            }
            catch (Exception x)
            {
                Errors.Add("Can't load project file: " + x.Message);
                return;
            }

            // load project
            var ndPrj = doc.SelectSingleNode("Project");
            if (ndPrj == null)
            {
                Errors.Add("Invalid project file (missing 'Project' node)");
                return;
            }

            // get project attributes
            Minify = GetAttributeBool(ndPrj, "Minify", true);
            Document = GetAttributeBool(ndPrj, "Document", true);

            // get input and output paths
            InputPath = Path.GetDirectoryName(ProjectFile);
            var att = ndPrj.Attributes["InputPath"];
            if (att != null)
            {
                InputPath = Path.IsPathRooted(att.Value)
                    ? att.Value
                    : Path.Combine(InputPath, att.Value);
            }
            OutputPath = InputPath;
            att = ndPrj.Attributes["OutputPath"];
            if (att != null) 
            {
                OutputPath = Path.IsPathRooted(att.Value)
                    ? att.Value
                    : Path.Combine(OutputPath, att.Value);
            }
            XmlDocumentationPath = InputPath;
            att = ndPrj.Attributes["XmlDocumentationPath"];
            if (att != null)
            {
                XmlDocumentationPath = Path.IsPathRooted(att.Value)
                    ? att.Value
                    : Path.Combine(XmlDocumentationPath, att.Value);
            }

            // find and apply config
            bool isExplicitConfig = !string.IsNullOrWhiteSpace(configName);
            XmlNodeList configNodes = ndPrj.SelectNodes("Configurations/Configuration");
            if (configNodes.Count > 0)
            {
                XmlNode ndCfg = null;
                // find a config
                foreach (XmlNode curCfg in configNodes)
                {
                    XmlAttribute nameAttr = curCfg.Attributes["Name"];
                    if (nameAttr == null)
                    {
                        Errors.Add("Configuration.Name is not specified.");
                        return;
                    }
                    if ((isExplicitConfig && nameAttr.Value.Split(',', ';').FirstOrDefault(c => string.Compare(c.Trim(), configName, true) == 0) != null)
                        ||
                        (!isExplicitConfig && GetAttributeBool(curCfg, "Default", false)))
                    {
                        ndCfg = curCfg;
                        break;
                    }
                }
                if (ndCfg == null)
                {
                    if (isExplicitConfig)
                    {
                        Errors.Add(string.Format("Configuration name '{0}' not found", configName));
                        return;
                    };
                    ndCfg = configNodes[0];
                }
                Define = GetAttributeString(ndCfg, "Define", Define);
                IncludeSources = GetAttributeBool(ndCfg, "IncludeSources", IncludeSources);
                IncludeUnminified = GetAttributeString(ndCfg, "IncludeUnminified", IncludeUnminified);
            }
            else if (isExplicitConfig)
            {
                Errors.Add(string.Format("Configuration name '{0}' not found", configName));
                return;
            };

            // get project items
            var ndItems = ndPrj.SelectSingleNode("Items");
            if (ndItems == null)
            {
                Errors.Add("Invalid project file (missing 'Items' node)");
                return;
            }
            //foreach (XmlNode ndItem in ndItems.ChildNodes)
            foreach (XmlNode ndItem in ndItems.SelectNodes("Item"))
            {
                // get output file
                att = ndItem.Attributes["File"];
                if (att == null)
                {
                    Errors.Add("Invalid project file (missing 'File' attribute in 'Item' node)");
                    return;
                }
                var item = new ProjectItem(this, att.Value);
                if (item.FileType == FileType.Unknown)
                {
                    Errors.Add(string.Format("Unknown file type '{0}'", item.FileName));
                    return;
                }
                item.IncludeInConfig = GetAttributeString(ndItem, "IncludeInConfig", "");
                item.MinifyLocalRename = GetAttributeBool(ndItem, "MinifyLocalRename", item.MinifyLocalRename);
                item.MinifyKeepLineBreaks = GetAttributeBool(ndItem, "MinifyKeepLineBreaks", item.MinifyKeepLineBreaks);
                item.IncludeUnminified = GetAttributeNullableBool(ndItem, "IncludeUnminified");
                item.CheckModuleName = GetAttributeBool(ndItem, "CheckModuleName", item.CheckModuleName);
                item.SeparateOutput = GetAttributeString(ndItem, "SeparateOutput", "");
                item.MergedOutput = GetAttributeString(ndItem, "MergedOutput", "");
                Items.Add(item);

                // get input files
                foreach (XmlNode ndInput in ndItem.SelectNodes("Input"))
                {
                    att = ndInput.Attributes["File"];
                    if (att == null)
                    {
                        Errors.Add("Invalid project file (missing 'File' attribute in 'Input' node)");
                        return;
                    }
                    var input = new ProjectInput(item, att.Value);
                    if (input.FileType == FileType.Unknown)
                    {
                        Errors.Add(string.Format("Unknown file type '{0}'", input.FileName));
                        return;
                    }
                    if (item.FileType != input.FileType)
                    {
                        Errors.Add(string.Format("Item and Input file type mismatch: Item = '{0}'; Input = '{1}'", item.FileName, input.FileName));
                        return;
                    }
                    input.CheckCase = GetAttributeBool(ndInput, "CheckCase", input.CheckCase);
                    input.IncludeInDocumentation = GetAttributeBool(ndInput, "Document", input.IncludeInDocumentation);
                    item.Inputs.Add(input);
                    string fn;
#if false  // base input file might not exist
                    if (!string.IsNullOrEmpty(input.BaseExtension))
                    {
                        //fn = input.GetFullPath("ts");
                        fn = input.GetFullPath(input.BaseExtension);
                        if (!File.Exists(fn))
                        {
                            Errors.Add("Missing input file: " + fn);
                            return;
                        }
                    }
#endif
                    //fn = input.GetFullPath("js");
                    fn = input.GetFullPath(input.Extension);
                    if (!File.Exists(fn))
                    {
                        Errors.Add("Missing input file: " + fn);
                        return;
                    }
                }
            }

            // get product version
            foreach (var item in Items)
            {
                foreach (var input in item.Inputs)
                {
                    if (input.FileType == FileType.JS)
                    {
                        //using (var sr = new StreamReader(input.GetFullPath("ts")))
                        using (var sr = new StreamReader(input.GetFullPath(input.BaseExtension)))
                        {
                            var src = sr.ReadToEnd();
                            var m = Regex.Match(src, @"_VERSION\s*=\s*(\'|\"")([0-9\.]*)(\'|\"")");
                            if (m.Success)
                            {
                                ProductVersion = m.Groups[2].Value;
                                break;
                            }
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(ProductVersion))
            {
                Errors.Add("Version not found in source files (looking for _VERSION = 'x.y.z.w')");
            }

            // get header for minified files
            var ndHdr = ndPrj.SelectSingleNode("FileHeader");
            if (ndHdr != null)
            {
                FileHeader = ndHdr.InnerText.Trim();
                FileHeader = FileHeader.Replace("@version", ProductVersion);
            }
        }

        private bool GetAttributeBool(XmlNode nd, string attName, bool defVal)
        {
            bool? ret = GetAttributeNullableBool(nd, attName);
            return ret.HasValue ? ret.Value : defVal;
        }

        private bool? GetAttributeNullableBool(XmlNode nd, string attName)
        {
            XmlAttribute attr = nd.Attributes[attName];
            if (attr != null)
            {
                bool ret;
                if (!bool.TryParse(attr.Value, out ret))
                {
                    Errors.Add(string.Format("'{0}' is a wrong value for the MinifyLocalRename attribute", attr.Value));
                    return null;
                }
                return ret;
            }

            return null;
        }

        static string GetAttributeString(XmlNode nd, string attName, string defVal)
        {
            var att = nd.Attributes[attName];
            return att != null ? att.Value : defVal;
        }

    }
}
