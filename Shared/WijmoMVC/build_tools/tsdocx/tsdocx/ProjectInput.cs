﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace tsdocx
{
    /// <summary>
    /// Represents an input item (TypeScript file) for a tsdocx project.
    /// </summary>
    public class ProjectInput
    {
        static Regex decorRx = new Regex(@"\G\s*@\w+\s*\(", RegexOptions.Compiled);

        List<Member> _members = new List<Member>();

        /// <summary>
        /// Initializes a new instance of a <see cref="ProjectInput"/>.
        /// </summary>
        /// <param name="item">Output item that owns this input item.</param>
        /// <param name="fileName">Name of the TypeScript file that represents this input item.</param>
        public ProjectInput(ProjectItem item, string fileName)
        {
            Item = item;
            FileName = fileName;
            FileType = ProjectItem.GetFileType(fileName);
            CheckCase = true;
            IncludeInDocumentation = true;
        }
        /// <summary>
        /// Gets the name of the TypeScript file that represents this input item.
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Gets the output item that owns this input item.
        /// </summary>
        public ProjectItem Item { get; set; }
        /// <summary>
        /// Gets the file type.
        /// </summary>
        public FileType FileType { get; private set; }
        // Gets or sets a value indicating whether to check identifiers casing.
        // Default is true.
        public bool CheckCase { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether to include this file to the documentation. 
        /// Represents the "Document" attribute of the "Input" element in wijmo.tsdocx.xml.
        /// </summary>
        public bool IncludeInDocumentation { get; set; }
        /// <summary>
        /// Gets file extension.
        /// </summary>
        public string Extension
        {
            get { return Item.Extension; }
        }
        //Gets extension of a file that this file was generated from, e.g. "ts" for "js" file.
        public string BaseExtension
        {
            get { return Item.BaseExtension; }
        }
        /// <summary>
        /// Gets the full (rooted) path for this input item.
        /// </summary>
        /// <param name="extension">The file extension to use when generating the path.</param>
        /// <returns>The full (rooted) path for this input item.</returns>
        public string GetFullPath(string extension)
        {
            var fn = Path.ChangeExtension(this.FileName, extension);
            if (!Path.IsPathRooted(fn))
            {
                fn = Path.Combine(this.Item.Project.InputPath, fn);
            }
            return fn;
        }

        /// <summary>
        /// Gets a list of Members containing documentation for this input item.
        /// </summary>
        public List<Member> Members { get { return _members; } }
        /// <summary>
        /// Generates the Members that contain documentation for this input item.
        /// </summary>
        public void Document()
        {
            // we are documenting js files
            if (FileType != FileType.JS)
            {
                return;
            }

            Members.Clear();

            //using (var sr = new StreamReader(this.GetFullPath("ts")))
            using (var sr = new StreamReader(this.GetFullPath(BaseExtension)))
            {
                var module = string.Empty;
                var className = string.Empty;
                var classEnd = -1;
                var comment = string.Empty;

                var src = sr.ReadToEnd();

                var tok = new Tokenizer(src);

                for (var t = tok.GetToken(); t != null; t = tok.GetToken())
                {
                    // handle modules with no comments
                    if (t == "module")
                    {
                        module = tok.GetToken();
                        comment = string.Empty;
                        continue;
                    }

                    // handle enum without commented
                    if (t == "enum") {
                        comment = string.Empty;
                        string def = t + " " + tok.GetToken("}");

                        // create new member
                        var m = new Member(this, module, className, def, comment);
                        className = null;
                        classEnd = tok.EndBlock('{', '}');
                        Members.Add(m);
                    }

                    // handle commented items
                    else if (t.StartsWith("/**"))
                    {
                        comment = tok.GetToken("*/");

                        // Skip leading @Decorators
                        Match match;
                        while ((match = decorRx.Match(src, tok.Position)).Success)
                        {
                            tok.Position = match.Index + match.Length;
                            tok.EndBlock('(', ')', true);
                        }
                        string def = tok.GetToken("{", ";", "}", "<amd-module ");

                        if (def.IndexOf("enum") > -1)
                        {
                            def += tok.GetToken("}");
                            // find @link token
                            while (def.LastIndexOf("*/") < def.LastIndexOf("/**"))
                            {
                                def += tok.GetToken("}");
                            }
                        }
                        else if (def.IndexOf("///") > -1)
                        {
                            def = "<amd-module " + tok.GetToken("/>");
                        }

                        // create new member
                        var m = new Member(this, module, className, def, comment);

                        // update state
                        switch (m.Kind)
                        {
                            case MemberType.Module:
                                className = null;
                                module = m.Name;
                                break;
                            case MemberType.Enum:
                                className = null;
                                classEnd = tok.EndBlock('{', '}');
                                break;
                            case MemberType.Class:
                            case MemberType.Interface:
                                className = m.Name;
                                classEnd = tok.EndBlock('{', '}');
                                break;
                        }
                        // add new member to list
                        if (!m.IsOrphanMember()) Members.Add(m);

                    }
                    // Check if class definition start with /*
                    else if (t == "/*")
                    {
                        comment = tok.GetToken("*/");
                        // Skip leading @Decorators
                        Match match;
                        while ((match = decorRx.Match(src, tok.Position)).Success)
                        {
                            tok.Position = match.Index + match.Length;
                            tok.EndBlock('(', ')', true);
                        }
                        string def = tok.GetToken("{", ";", "<amd-module ");
                        if (def != null)
                        {
                            var mReg = Regex.Match(def, @"^\s*(export\s+)?class\s+(\w+)");
                            if (mReg.Success)
                            {
                                // create new member
                                var m = new Member(this, module, className, def, comment);

                                // update state
                                switch (m.Kind)
                                {
                                    case MemberType.Module:
                                        className = null;
                                        module = m.Name;
                                        break;
                                    case MemberType.Enum:
                                        className = null;
                                        classEnd = tok.EndBlock('{', '}');
                                        break;
                                    case MemberType.Class:
                                    case MemberType.Interface:
                                        className = m.Name;
                                        classEnd = tok.EndBlock('{', '}');
                                        break;
                                }

                                // add new member to list
                                Members.Add(m);
                            }
                        }
                    }

                    // skip multi-line non-document comment.
                    else if (t.StartsWith("/*") && !t.EndsWith("*/"))
                    {
                        string s = tok.GetToken("*/");
                    }

                    // skip single-line comment.
                    else if (t.StartsWith("//"))
                    {
                        string s = tok.GetToken("\r", "\n", "\0");
                    }

                    if (classEnd > -1 && tok.Position > classEnd)
                    {
                        className = string.Empty;
                        classEnd = -1;
                    }
                }
            }
        }
    }
}
