﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

namespace tsdocx
{
  class Program
  {
    const int ERROR = 1;
    private static readonly Regex _paramRegex = new Regex(@"^/(?<name>.+?)(:(?<val>.+))$");

    /// <summary>
    /// Arguments:
    /// file_name - a path to wijmo.tsdocx.xml file.
    /// /cfg:config_name - a config name, if omitted then the default config will be used.
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
      var start = DateTime.Now;

      Console.WriteLine("-----------------------------------------------------");
      Console.WriteLine("tsdocx - TypeScript merger/minifier/documenter");
      Console.WriteLine("Copyright (c) ComponentOne");
      Console.WriteLine("-----------------------------------------------------");
      //args = new string[] { "C:\\TFS\\RhinoMain\\Development\\Wijmo5MVC\\C1.Web.Mvc\\Client\\c1.tsdocx.xml", "/cfg:release" };
      // check command line parameters
      if (args.Length < 1)
      {
        Console.WriteLine();
        Console.WriteLine("command line parameters:");
        Console.WriteLine(">tsdocx projectfile[.tsdocx]");
        Environment.ExitCode = ERROR;
        return;
      }

      // process arguments
      string fileName = null;
      string configName = null;
      foreach (string curArg in args)
      {
        Match m = _paramRegex.Match(curArg);
        if (m.Success)
        {
          string parVal = m.Groups["val"].Value;
          switch (m.Groups["name"].Value.ToLower())
          {
            case "cfg":
              if (configName != null)
              {
                Console.WriteLine("Multiple /cfg parameters specified.");
                Environment.ExitCode = ERROR;
                return;
              }
              configName = parVal;
              break;
            default:
              Console.WriteLine(string.Format("Unknown parameter /'{0}'.", m.Groups["name"].Value));
              Environment.ExitCode = ERROR;
              return;
          }
        }
        else
        {
          if (fileName == null)
            fileName = curArg;
          else
          {
            Console.WriteLine("Multiple project file names passed as tsdocx arguments.");
            Environment.ExitCode = ERROR;
            return;
          }
        }
      }
      if (fileName == null)
      {
        Console.WriteLine("Project file is not specified.");
        Environment.ExitCode = ERROR;
        return;
      }

      // read project file
      if (string.IsNullOrEmpty(Path.GetExtension(fileName)))
      {
        fileName = Path.ChangeExtension(fileName, "tsdocx");
      }
      if (!File.Exists(fileName))
      {
        Console.WriteLine();
        Console.WriteLine("cannot find project file.");
        Environment.ExitCode = ERROR;
        return;
      }
      var prj = new Project();
      prj.Load(fileName, configName);

      // execute selected project commands
      if (prj.Errors.Count == 0)
      {
        // clear all errors
        prj.Errors.Clear();

        // generate output files
        foreach (var item in prj.Items)
        {
          item.GenerateOutput();
        }

        foreach (var item in prj.Items)
        {
          item.GenerateOutput();
        }

        // Intellisense
        prj.IntellisenseJs();

#if false
                // merge
                Console.WriteLine("  merging");
                foreach (var item in prj.Items)
                {
                    //if (item.IsUptodate("js"))
                    if (item.IsUptodate(item.Extension))
                    {
                        Console.WriteLine("    " + item.FileName + " is up-do-date");
                    }
                    else
                    {
                        Console.WriteLine("    " + item.FileName + "...");
                        item.Merge();
                    }
                    if (item.FileType == FileType.JS)
                    {
                        string dtsFileName = Path.ChangeExtension(item.FileName, "d.ts");
                        if (item.IsUptodate("d.ts"))
                        {
                            Console.WriteLine("    " + dtsFileName + " is up-do-date");
                        }
                        else
                        {
                            Console.WriteLine("    " + dtsFileName + "...");
                            item.Merge("d.ts");
                        }
                    }
                }

                // minify
                if (prj.Minify)
                {
                    Console.WriteLine("  minifying");
                    foreach (var item in prj.Items)
                    {
                        //if (item.IsUptodate("min.js"))
                        if (item.IsUptodate(item.MinExtension))
                        {
                            Console.WriteLine("    " + item.FileName + " is up-do-date");
                        }
                        else
                        {
                            Console.WriteLine("    " + item.FileName + "...");
                            item.Minify();
                        }
                    }
                }

                // document
                if (prj.Document) 
                {
                    Console.WriteLine("  documenting");
                    foreach (var item in prj.Items)
                    {
                        if (item.FileType == FileType.JS)
                        {
                            if (item.IsUptodate("xml"))
                            {
                                Console.WriteLine("    " + item.FileName + " is up-do-date");
                            }
                            else
                            {
                                Console.WriteLine("    " + item.FileName + "...");
                                item.Document();
                            }
                        }
                    }
                }
#endif
      }

      // report errors
      if (prj.Errors.Count > 0)
      {
        Environment.ExitCode = ERROR;
        Console.WriteLine();
        Console.WriteLine("** Finished with error(s):");
        foreach (var err in prj.Errors)
        {
          Console.WriteLine("  " + err);
        }
      }

      // done
      Console.WriteLine();
      Console.WriteLine("tsdocx done in {0:n0} ms.", DateTime.Now.Subtract(start).TotalMilliseconds);
    }
  }
}
