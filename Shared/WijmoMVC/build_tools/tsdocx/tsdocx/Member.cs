﻿using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System;

namespace tsdocx
{
    /// <summary>
    /// Represents the possible member types in a TypeScript file.
    /// </summary>
    public enum MemberType
    {
        Unknown,
        Module,
        Class,
        Enum,
        Interface,
        Field,
        Property,
        Method,
        Event,
        Type
    }
    /// <summary>
    /// Represents a class, interface, field, property, method, event, or enum in a TypeScript file.
    /// </summary>
    public class Member
    {
        List<Parameter> _parms = new List<Parameter>();
        List<string> _see = new List<string>();
        List<string> _implements = new List<string>();

        /// <summary>
        /// Initializes a new instance of a <see cref="Member"/>.
        /// </summary>
        /// <param name="pi"><see cref="ProjectInput"/> that defines this member.</param>
        /// <param name="module">Name of the module that defines this member (namespace).</param>
        /// <param name="className">Name of the class that contains this member.</param>
        /// <param name="def">TypeScript definition for this member.</param>
        /// <param name="comment">Comment associated with this member.</param>
        public Member(ProjectInput pi, string module, string className, string def, string comment)
        {
            ProjectInput = pi;
            Module = module;
            Class = className;
            Def = def;
            ParseDef(def);
            ParseComments(comment);
            if (string.IsNullOrEmpty(Name) && Parameters.Count > 0) Name = Class;
            // sanity
            if (string.IsNullOrEmpty(this.Name))
            {
                var err = ProjectInput.Item.Project.Errors;
                err.Add(string.Format("parsing error, can't figure this out: '{0}'.", def));
            }

            // validate name
            if (pi.CheckCase && !string.IsNullOrEmpty(this.Name))
            {
                // names starting with underscores are for private stuff
                //if (this.Name.StartsWith("_"))
                //{
                //    var err = ProjectInput.Item.Project.Errors;
                //    err.Add(string.Format("{0} should be private and not documented.", this.Name));
                //}

                // class, interface, and Enum names should start with uppercase
                var c = this.Name[0];
                switch (this.Kind)
                {
                    case MemberType.Class:
                    case MemberType.Enum:
                    case MemberType.Interface:
                        if (!char.IsUpper(c) && c != '_')
                        {
                            var err = ProjectInput.Item.Project.Errors;
                            err.Add(string.Format("bad {0} name: '{1}' (should start with uppercase)", this.Kind, this.Name));
                        }
                        break;
                    case MemberType.Module:
                    case MemberType.Event:
                    case MemberType.Method:
                    case MemberType.Property:
                    case MemberType.Type:
                    case MemberType.Field:
                        if (!char.IsLower(c) && c != '_' && Name != Class)
                        {
                            var err = ProjectInput.Item.Project.Errors;
                            err.Add(string.Format("bad {0} name: '{1}' (should start with lowercase)", this.Kind, this.Name));
                        }
                        break;
                }
            }

            // check for orphans (members with no/uncommented class)
            if (string.IsNullOrEmpty(className))
            {
                var orphan = false;
                switch (this.Kind)
                {
                    case MemberType.Property:
                        orphan = def.IndexOf("export") < 0 || def.IndexOf("var") < 0;
                        break;
                    case MemberType.Method:
                        orphan = def.IndexOf("export") < 0 || def.IndexOf("function") < 0;
                        break;
                    case MemberType.Event:
                        orphan = true;
                        break;
                }
                if (orphan)
                {
                    //var err = ProjectInput.Item.Project.Errors;
                    //err.Add(string.Format("Member '{0}' in module {1} doesn't belong to a public/commented class", this.Name, module));
                }
            }

        }
        /**
         * Detect that this member belong to public/commented class or not.
         */
        public bool IsOrphanMember()
        {
            if (string.IsNullOrEmpty(Class))
            {
                var orphan = false;
                switch (this.Kind)
                {
                    case MemberType.Property:
                        orphan = Def.IndexOf("export") < 0 || Def.IndexOf("var") < 0;
                        break;
                    case MemberType.Method:
                        orphan = Def.IndexOf("export") < 0 || Def.IndexOf("function") < 0;
                        break;
                    case MemberType.Event:
                        orphan = true;
                        break;
                }
                return orphan;
            }
            return false;
        }

        /// <summary>
        /// Gets or sets definition for this member.
        /// </summary>
        public string Def { get; set; }

        /// <summary>
        /// <see cref="ProjectInput"/> that defines this member.
        /// </summary>
        public ProjectInput ProjectInput { get; set; }
        /// <summary>
        /// Gets or sets the name of the module that defines this member.
        /// </summary>
        public string Module { get; set; }
        /// <summary>
        /// Gets or sets the name of the class that defines this member.
        /// </summary>
        public string Class { get; set; }
        /// <summary>
        /// Gets or sets the name of this member.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets whether this member is static.
        /// </summary>
        public bool Static { get; set; }
        /// <summary>
        /// Gets or sets the name of base class (if this member is a derived class).
        /// </summary>
        public string BaseClass { get; set; }
        /// <summary>
        /// Gets or sets the type of this member (class, property, event, method, etc).
        /// </summary>
        public MemberType Kind { get; set; }
        /// <summary>
        /// Gets or sets the type that this member represents (property type, method return type).
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Gets or sets whether the member is optional.
        /// </summary>
        public bool Optional { get; set; }
        /// <summary>
        /// Gets the list of parameters for this member (if this member is a method).
        /// </summary>
        public List<Parameter> Parameters { get { return _parms; } }
        /// <summary>
        /// Gets or sets a string containing the comments associated with this member.
        /// </summary>
        public string Comments { get; set; }
        /// <summary>
        /// Gets or sets a string describing the return value for this member.
        /// </summary>
        public string ReturnComments { get; set; }
        /// <summary>
        /// Gets a list of "see also" items.
        /// </summary>
        public List<string> See { get { return _see; } }
        /// <summary>
        /// Gets a list of the interfaces supported by this member.
        /// </summary>
        public List<string> Implements { get { return _implements; } }
        /// <summary>
        /// Gets the full name of this member, using the .NET XML docs style.
        /// <para>
        /// The full name includes the member type, module, class, and actual member name.
        /// </para>
        /// </summary>
        /// <returns>The full name of this member, using the .NET XML docs style.</returns>
        public string GetFullName()
        {
            var arr = new string[] {
                GetMemberCode(),
                Module,
                Class,
                Name
            };
            return string.Join(":", arr);
        }

        // get the char code for a member type
        string GetMemberCode()
        {
            switch (this.Kind)
            {
                case tsdocx.MemberType.Class:
                case tsdocx.MemberType.Enum:
                    return "T";
                case tsdocx.MemberType.Interface:
                    return "I";
                case tsdocx.MemberType.Event:
                    return "E";
                case tsdocx.MemberType.Field:
                    return "F";
                case tsdocx.MemberType.Method:
                    return "M";
                case tsdocx.MemberType.Property:
                    return "P";
            }
            return "?";
        }

        // parse member definition
        void ParseDef(string def)
        {
            // module
            var m = Regex.Match(def, @"^\s*module\s+(\S+)");
            if (m.Success)
            {
                Kind = MemberType.Module;
                Module = Name = m.Groups[1].Value;
                return;
            }
            //external module: <amd-module name='wijmo/wijmo.angular2.input'/>
            m = Regex.Match(def, @"^<amd-module\s+name\s*=\s*(?<quot>[""'])(?<name>.*?)\k<quot>");
            if (m.Success)
            {
                Kind = MemberType.Module;
                Module = Name = m.Groups["name"].Value;
                return;
            }

            // class
            m = Regex.Match(def, @"^\s*(export\s+)?class\s+(\w+)(\<(.*)\>)?");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Class;
                Class = Name = m.Groups[2].Value;
                m = Regex.Match(def, @"extends\s+([\w\.]+)");
                if (m.Success)
                {
                    BaseClass = m.Groups[1].Value;
                }
                m = Regex.Match(def, @"implements\s+([\w\.\s,]+)"); // IList, IEnumerable, ICollectionView, etc... {
                if (m.Success)
                {
                    foreach (var i in m.Groups[1].Value.TrimEnd('{', ' ').Split(','))
                    {
                        Implements.Add(i.Trim());
                    }
                }
                return;
            }

            // interface
            m = Regex.Match(def, @"^\s*(export\s+)?interface\s+(\w+)(\<(.*)\>)?");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Interface;
                Class = Name = m.Groups[2].Value;
                m = Regex.Match(def, @"extends\s+([\w\.]+)");
                if (m.Success)
                {
                    foreach (var i in m.Groups[1].Value.TrimEnd('{', ' ').Split(','))
                    {
                        Implements.Add(i.Trim());
                    }
                }
                return;
            }

            // constructor
            m = Regex.Match(def, @"^\s*constructor\s*\(([\s\S]+)\)");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Method;
                Name = "constructor";
                Type = Class;
                ParseParameters(m.Groups[1].Value);
                return;
            }

            // enum
            m = Regex.Match(def, @"^\s*((export)?\s+)?enum\s+(\w+)");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Enum;
                Class = Name = m.Groups[3].Value;
                var pos = def.IndexOf('{');
                if (pos > -1)
                {
                    m = Regex.Match(def.Substring(pos + 1),
                        @"(/\*\*(.*?)\*/)?\s*" + // comment
                        @"(\w+)\s*" + // symbol
                        @"(\=\s*([\w\| ]+))?" + // value
                        @"(\s*,?\s*//[^\r]*\r)?", // non-doc comment
                        RegexOptions.Singleline);
                    for (; m.Success; m = m.NextMatch())
                    {
                        var p = new Parameter();
                        p.Name = m.Groups[3].Value;
                        p.Comment = CleanComment(m.Groups[2].Value);
                        p.Value = m.Groups[5].Value;
                        if (string.IsNullOrEmpty(p.Value))
                        {
                            p.Value = Parameters.Count.ToString();
                        }
                        Parameters.Add(p);
                    }
                }
                return;
            }

            // module function
            m = Regex.Match(def, @"^\s*(export\s+)?function\s+(\w+)\s*\((.*)\)");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Method;
                Name = m.Groups[2].Value;
                ParseParameters(m.Groups[3].Value);
                m = Regex.Match(def, @"\)\s*:\s*([\w\.]+(\[\])*)");
                if (m.Success)
                {
                    Type = m.Groups[1].Value;
                }
                return;
            }

            // property
            //m = Regex.Match(def, @"^\s*(private\s+|public\s+)?(static\s+)?(get|set)\s+(\w+)\s*\(.*\)");
            m = Regex.Match(def, @"^\s*(private\s+|public\s+)?(static\s+)?(get|set|readonly)\s+(\w+)\s*[\:\(].*\)?");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Property;
                Name = m.Groups[4].Value;
                Static = !string.IsNullOrEmpty(m.Groups[2].Value);
                m = Regex.Match(def, @".*:\s*([\w\.]+(\[\])*)");
                if (m.Success)
                {
                    Type = m.Groups[1].Value;
                }
                return;
            }

            // type
            m = Regex.Match(def, @"^\s*(export\s+)?type\s+(\w+)(\<(.*)\>)?");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Type;
                Class = Name = m.Groups[2].Value;
                m = Regex.Match(def, @"extends\s+([\w\.]+)");
                if (m.Success)
                {
                    foreach (var i in m.Groups[1].Value.TrimEnd('{', ' ').Split(','))
                    {
                        Implements.Add(i.Trim());
                    }
                }
                return;
            }
            
            // event
            m = Regex.Match(def, @"^\s*(private\s+|public\s+|readonly\s+)?(static\s+)?(\w+)\s*=\s*new\s+(wijmo\.)?Event\s*");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Event;
                Name = m.Groups[3].Value;
                Static = !string.IsNullOrEmpty(m.Groups[2].Value);
                return;
            }
            
            // method
            // note: avoid matching "foo = new Event()"
            m = Regex.Match(def, @"^\s*(private\s+|public\s+)?(static\s+)?(\w+)\s*\((.*)\)");
            if (m.Success && !Regex.Match(def, @"\s*=\s*new\s+").Success)
            {
                Kind = tsdocx.MemberType.Method;
                Name = m.Groups[3].Value;
                Static = !string.IsNullOrEmpty(m.Groups[2].Value);
                ParseParameters(m.Groups[4].Value);
                m = Regex.Match(def, @"\)\s*:\s*([\w\.]+(\[\])*)");
                if (m.Success)
                {
                    Type = m.Groups[1].Value;
                }
                return;
            }
            
            // field
            //m = Regex.Match(def, @"^\s*(private\s+|public\s+)?(static\s+)?(\w+)\s*(:|=)\s*([\w\.]+)?");
            m = Regex.Match(def, @"^\s*(private\s+|public\s+)?(?<stat>static\s+)?(?<name>\w+)?(\((.*)\))?(?<opt>\?)?\s*(?<delim>:|=)\s*(?<new>new\s+)?(?<te>[\w\<\,\s\>\.]+(?<arr>\[)?)?");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Field;
                Name = m.Groups["name"].Value;
                if (Name == "")
                    ParseParameters(m.Groups[3].Value);
                Static = !string.IsNullOrEmpty(m.Groups["stat"].Value);
                Optional = m.Groups["opt"].Length > 0;
                string v = m.Groups["te"].Value;
                if (m.Groups["arr"].Length > 0)
                {
                    v += "]";
                }
                if (m.Groups["delim"].Value == ":")
                {
                    Type = v;
                }
                else if (m.Groups["delim"].Value == "=")
                {
                    // REVIEW: default field value is hard to parse, try to get type at least
                    Type = "any";
                    if (v.Length > 0)
                    {
                        var c = v[0];
                        if (m.Groups["new"].Length > 0)
                        {
                            Type = v;
                        }
                        else if (char.IsDigit(c))
                        {
                            Type = "number";
                        }
                        else if (c == '\'' || c == '"')
                        {
                            Type = "string";
                        }
                        else if (v == "true" || v == "false")
                        {
                            Type = "boolean";
                        }
                        //else if (v == "new")
                        //{
                        //    Type = m.Groups[6].Value;
                        //}
                    }
                }

                return;
            }
            
            // module var
            m = Regex.Match(def, @"^\s*(export\s+)?var\s+(\w+)");
            if (m.Success)
            {
                Kind = tsdocx.MemberType.Property;
                Name = m.Groups[2].Value;
                m = Regex.Match(def, @":\s*([\w\.]+(\[\])*)");
                if (m.Success)
                {
                    Type = m.Groups[1].Value;
                }
                return;
            }
            
            System.Diagnostics.Debug.Assert(false, def + "should not get here...");
        }

        // parse member parameters
        void ParseParameters(string parms)
        {
            string[] splittedParams = parms.Contains("<") 
                ? Regex.Split(parms, ",(?=[^>]*$)") 
                : Regex.Split(parms, ",");
            foreach (var parm in splittedParams)
            {
                if (string.IsNullOrEmpty(parm)) continue;
                var parmDef = parm;

                // define parameter, add to member
                var p = new Parameter();
                Parameters.Add(p);

                // get default value
                var pos = parmDef.IndexOf('=');
                if (pos > -1)
                {
                    int i;
                    p.Value = parmDef.Substring(pos + 1).Trim();
                    p.Optional = true;
                    if (p.Value == "true" || p.Value == "false")
                    {
                        p.Type = "boolean";
                    }
                    else if (p.Value.StartsWith("'") || p.Value.StartsWith("\""))
                    {
                        p.Type = "string";
                    }
                    else if (int.TryParse(p.Value, out i))
                    {
                        p.Type = "number";
                    }
                    else
                    {
                        foreach (Parameter pp in Parameters)
                        {
                            if (pp != p && p.Value.IndexOf(pp.Name) > -1)
                            {
                                p.Type = pp.Type;
                                break;
                            }
                        }
                    }
                    parmDef = parmDef.Substring(0, pos);
                }

                // get type
                pos = parmDef.IndexOf(':');
                if (pos > -1)
                {
                    p.Type = parmDef.Substring(pos + 1).Trim();
                    parmDef = parmDef.Substring(0, pos);
                }

                // save parameter name
                p.Name = parmDef.Trim();

                // handle optional parameters
                if (p.Name.EndsWith("?"))
                {
                    p.Optional = true;
                    p.Name = p.Name.Substring(0, p.Name.Length - 1);
                }
            }
        }

        // parse member comments
        void ParseComments(string comment)
        {
            // uncomment the comments
            comment = CleanComment(comment);

            // parse @directives
            var directives = new List<string>();
            for (var i = comment.Length - 1; i >= 0; i--)
            {
                if (comment[i] == '@')
                {
                    if (i - 1 >= 0)
                    {
                        if (comment[i - 1] == '\n')
                        {
                            var ps = comment.IndexOf(' ', i); // (ignore @see:* directives here)
                            var pc = comment.IndexOf(':', i);
                            if (pc < 0 || (ps > -1 && ps < pc))
                            {
                                directives.Insert(0, comment.Substring(i));
                                comment = comment.Substring(0, i).Trim();
                                i = comment.Length;
                            }
                        }
                    }
                    else
                    {
                        var ps = comment.IndexOf(' ', i); // (ignore @see:* directives here)
                        var pc = comment.IndexOf(':', i);
                        if (pc < 0 || (ps > -1 && ps < pc))
                        {
                            directives.Insert(0, comment.Substring(i));
                            comment = comment.Substring(0, i).Trim();
                            i = comment.Length;
                        }
                    }
                }
            }

            // handle @directives
            foreach (var d in directives)
            {
                ParseDirective(d);
            }

            // done
            this.Comments = comment;
        }

        // clean comments by removing /** */ and whitespace
        static char[] _rn = "\r\n".ToCharArray();
        static char[] _cmt = " \t/*\r\n".ToCharArray();
        static string CleanComment(string comment)
        {
            var lines = comment.Split(_rn, System.StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < lines.Length; i++)
            {
                lines[i] = Regex.Replace(lines[i], @"^\s*\/?\*+\s?", string.Empty); // remove '/**' or '*' at the start of the string
                lines[i] = Regex.Replace(lines[i], @"\s*\*+\/\s*$", string.Empty);  // remove '*/' at the end of the string
            }
            return string.Join("\r\n", lines).Trim(_cmt);
        }

        // Parse comment directives
        // http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html
        void ParseDirective(string d)
        {
            // match directive
            var err = ProjectInput.Item.Project.Errors;
            var m = Regex.Match(d, @"(@\S+)\s+(\S+)?\s*(.*)");
            if (!m.Success)
            {
                err.Add(string.Format("Unknown comment directive '{0}' (in {1})", d, GetFullName()));
            }

            // process directive
            var dctv = m.Groups[1].Value;
            switch (dctv)
            {
                case "@param":
                    var pName = m.Groups[2].Value;
                    foreach (var p in this.Parameters)
                    {
                        if (p.Name == pName)
                        {
                            p.Comment = d.Substring(m.Groups[3].Index);
                            return;
                        }
                    }
                    // Because wijmo ver637 has some missing Parameters, so comment this line to make tsdocx can run without any error.
                    // Should uncomment this line when have new version of wijmo and check.
                    //err.Add(string.Format("Parameter '{0}' not found (in {1})", pName, GetFullName()));
                    break;
                case "@return":
                    this.ReturnComments = d.Substring(m.Groups[2].Index);
                    break;
                case "@returns":
                    this.ReturnComments = d.Substring(m.Groups[2].Index);
                    break;
                case "@see":
                    var seeList = d.Substring(m.Groups[2].Index).Split(',');
                    foreach (var see in seeList)
                    {
                        this.See.Add(see.Trim());
                    }
                    break;
                default:
                    if (!d.StartsWith("@see:"))
                    {
                        err.Add(string.Format("Unknown comment directive '{0}' (in {1})", d, GetFullName()));
                    }
                    break;
            }
        }
    }
    /// <summary>
    /// Represents a parameter in a method call.
    /// </summary>
    public class Parameter
    {
        /// <summary>
        /// Gets or sets the name of the parameter.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the parameter type.
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Gets or sets whether the parameter is optional.
        /// </summary>
        public bool Optional { get; set; }
        /// <summary>
        /// Gets or sets the default value for the parameter.
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Gets or sets a string describing the parameter.
        /// </summary>
        public string Comment { get; set; }
    }
}
