﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace tsdocx
{
	partial class Project
	{
		/// <summary>
		/// Generates a js file with definitions for VS Intellisense
		/// </summary>
		public void IntellisenseJs()
		{
			// make sure that all ProjectInput(s) has Members
			foreach (ProjectItem item in Items)
			{
				foreach (ProjectInput input in item.Inputs)
				{
					if (input.Members.Count == 0 && input.IncludeInDocumentation)
						input.Document();
				}
			}

			// make sure output directory exists
			string dir = Path.Combine(Path.GetDirectoryName(XmlDocumentationPath), "intellisense");
			if (!Directory.Exists(dir))
			{
				Directory.CreateDirectory(dir);
			}
			// the name of the generated file that will contain intellisense related definitions
			string file = Path.Combine(dir, "wijmo.intellisense.js");
			//string file = "d:\\wijmo.intellisense.js";

			// Create class/member definitions by analyzing each Items[i].Inputs[j].Members[k]
			JsIntellisense.Generate(file, this);
		}
	}

	public static class JsIntellisense
	{
		private static string JsPrefix = "_wj";
		private static string JsEnumMarker = JsPrefix + "Enum";
		private static string JsModuleMarker = JsPrefix + "Module";
		private static string JsClassMarker = JsPrefix + "Class";
		private static string JsClassName = JsPrefix + "ClassName";
		private static string JsEventsPropsDict = JsPrefix + "Dict";

		private static string JsMergeFn = JsPrefix + "Merge";
		private static string JsGetPropFn = JsPrefix + "GetProp";
		private static string JsReownEventsFn = JsPrefix + "ReownEvents";
		private static string JsUpdateEventHandlerSignatureHintFn = JsPrefix + "UpdateEventHandlerSignatureHint";

		private interface _IJsBuilderContext
		{
			StringBuilder Builder { get; }

			/// <summary>
			/// Registers a given namespace.
			/// </summary>
			/// <param name="ns"></param>
			void RegisterNS(string ns);

			/// <summary>
			/// Returns the fully qualified name of a given type including its namespace ("FlexGrid" => "wijmo.grid.FlexGrid").
			/// </summary>
			/// <param name="type"></param>
			/// <returns></returns>
			string ResolveTypeNameNS(string type);
		}

		private class _JsBuilder : _IJsBuilderContext
		{
			private HashSet<string> _nsCache = new HashSet<string>();
			private Dictionary<string, string> _customTypesNS = new Dictionary<string, string>();
			private StringBuilder _builder;

			public _JsBuilder()
			{
				_builder = new StringBuilder();
			}

			public void Write(IList<ProjectItem> items)
			{
				CollectCustomTypesNS(items);

				WriteJsMergeFn();

				foreach (ProjectInput input in SelectJsInputs(items))
				{
					IList<Member> members = input.Members;

					for (var i = 0; i < members.Count; i++)
					{
						Member member = members[i];
						_MemberBuilder writer = null;

						switch (member.Kind)
						{
							case MemberType.Class:
								if (member.Module == "wijmo" && member.Name == "Event")
								{
									// handle this class in a special way to provide a design-time typing for the events handlers
									writer = new _WijmoEventClassBuilder(this);
								}
								else
								{
									writer = new _ClassBuilder(this);
								}
								break;

							case MemberType.Enum:
								writer = new _EnumBuilder(this);
								break;

							case MemberType.Interface:
								writer = new _InterfaceBuilder(this);
								break;

							case MemberType.Method: // module function
								writer = new _MethodBuilder(this);
								break;

							case MemberType.Property: // module variable
								writer = new _StaticFieldBuilder(this);
								break;

							default:
								break;
						}
						
						if (writer != null)
						{
							i = writer.Build(members, i);
						}
					}
				}

				WriteJsExtension();
			}

			private void CollectCustomTypesNS(IList<ProjectItem> items)
			{
				foreach (ProjectInput input in SelectJsInputs(items))
				{
					foreach (Member member in input.Members)
					{
						switch (member.Kind)
						{
							case MemberType.Class:
							case MemberType.Enum:
							case MemberType.Interface:
								if ((member.Name.IndexOf('.') < 0) && !string.IsNullOrEmpty(member.Module) && !_customTypesNS.ContainsKey(member.Name))
								{
									// Suppose that different namespaces can't have members with the same name.
									_customTypesNS.Add(member.Name, string.Format("{0}.{1}", member.Module, member.Name));
								}
								break;
						}
					}
				}
			}

			private IEnumerable<ProjectInput> SelectJsInputs(IList<ProjectItem> items)
			{
				return items
					.Select(prjItem => prjItem)
					.Where(prjItem => !prjItem.FileName.StartsWith("interop") && prjItem.FileType == FileType.JS)
					.SelectMany(prjInput => prjInput.Inputs)
					.Where(prjInput => prjInput.FileType == FileType.JS);
			}

			private void WriteJsMergeFn()
			{
				_builder.AppendFormat("function {0}(f, t) {{\n", JsMergeFn);
				_builder.AppendLine("   if (f && t) for (var k in f) t[k] = f[k];");
				_builder.AppendLine("   return t;");
				_builder.AppendLine("}");
			}

			private void WriteJsExtension()
			{
				_builder.AppendFormat("\nfunction {0}(name) {{\n", JsGetPropFn);
				_builder.AppendLine("   if (!name || (typeof (name) !== 'string')) return;");
				_builder.AppendLine("   name = name.split('.');");
				_builder.AppendLine("   var cur;");
				_builder.AppendLine("   for (var i = 0; i < name.length; i++) {");
				_builder.AppendLine("      if (i && !cur) break;");
				_builder.AppendLine("      cur = !i ? window[name[i]] : cur[name[i]];");
				_builder.AppendLine("   }");
				_builder.AppendLine("   return cur;");
				_builder.AppendLine("}\n");

				_builder.AppendFormat("function {0}(ctrl) {{\n", JsReownEventsFn);
				_builder.AppendFormat("   var foo;\n");
				_builder.AppendFormat("   if ((foo = {0}(ctrl.{1})) && (foo = foo.{2})) {{\n", JsGetPropFn, JsClassName, JsEventsPropsDict);
				_builder.AppendFormat("      for (var k in foo) {{\n");
				_builder.AppendFormat("         if (foo[k] === 1) ctrl[k].{0} = ctrl;\n", _WijmoEventClassBuilder.JsOwner); // an event
				_builder.AppendFormat("      }}\n");
				_builder.AppendFormat("   }}\n");
				_builder.AppendFormat("}}\n");
				_builder.AppendFormat("\n");

				_builder.AppendFormat("function {0}(po, fh) {{\n", JsUpdateEventHandlerSignatureHintFn);
				_builder.AppendFormat("   if (po && fh && (po.{0} === 'wijmo.Event') && (fh.functionName === 'addHandler' || fh.functionName === 'removeHandler')) {{\n", JsClassName);
				_builder.AppendFormat("      var owner, sender, arg;\n");
				_builder.AppendFormat("      if ((owner = po.{0}) && (sender = owner.{1}) && (arg = po.{2})) {{\n", _WijmoEventClassBuilder.JsOwner, JsClassName, _WijmoEventClassBuilder.JsArgType);
				_builder.AppendFormat("         fh.signatures[0].params[0].type = 'Function(' + sender + ' sender, ' + arg + ' args)';\n"); // customize typing information
				_builder.AppendFormat("      }}\n");
				_builder.AppendFormat("   }}\n");
				_builder.AppendFormat("}}\n");
				_builder.AppendFormat("\n");

				// statementcompletion
				_builder.AppendFormat("intellisense.addEventListener('statementcompletion', function (e) {{\n");
				_builder.AppendFormat("   e.items = e.items.filter(function (item) {{\n");
				_builder.AppendFormat("      var hide = !!(item.name && (item.name.indexOf('{0}') === 0));\n", JsPrefix); // hide internal members started with "_wj".
				_builder.AppendFormat("\n");
				_builder.AppendFormat("      if (!hide) {{\n");
				_builder.AppendFormat("         var po = item.parentObject,\n");
				_builder.AppendFormat("            value = item.value,\n");
				_builder.AppendFormat("            obj = new Object(),\n");
				_builder.AppendFormat("            fn = function() {{}},\n");
				_builder.AppendFormat("            isObjMember = function(name) {{ return name in obj; }},\n");
				_builder.AppendFormat("            isFnMember = function(name) {{ return name in fn; }};\n");
				_builder.AppendFormat("\n");                
				_builder.AppendFormat("         if (value.{0} === true) {{\n", JsModuleMarker);
				_builder.AppendFormat("            item.glyph = 'vs:GlyphGroupNamespace';\n");
				_builder.AppendFormat("         }} else if (value.{0} === true && item.name !== 'constructor') {{\n", JsClassMarker);
				_builder.AppendFormat("            item.glyph = 'vs:GlyphGroupClass';\n");
				_builder.AppendFormat("         }} else if (value.{0} === true) {{\n", JsEnumMarker);
				_builder.AppendFormat("            item.glyph = 'vs:GlyphGroupEnum';\n");
				_builder.AppendFormat("         }} else if (po.{0} === true) {{\n", JsModuleMarker);
				_builder.AppendFormat("            hide = isObjMember(item.name);\n"); // hide Object.prototype members.
				_builder.AppendFormat("         }} else if (po.{0} === true) {{\n", JsClassMarker);
				_builder.AppendFormat("            hide = isFnMember(item.name);\n"); // hide Function.prototype members.
				_builder.AppendFormat("         }} else if (po.{0} === true) {{\n", JsEnumMarker);
				_builder.AppendFormat("            item.glyph = 'vs:GlyphGroupEnumMember';\n");
				_builder.AppendFormat("            hide = isObjMember(item.name);\n"); // leave only enumeration values.
				_builder.AppendFormat("         }} else {{\n"); // instance member?
				_builder.AppendFormat("            if (po.constructor && po.constructor.{0} === true) {{\n", JsClassMarker); // make sure that non-wijmo classes will not be affected
				_builder.AppendFormat("               hide = isObjMember(item.name);\n"); // hide Object.prototype members.
				_builder.AppendFormat("               var foo;\n");
				_builder.AppendFormat("               if (!hide && po.{0} && (foo = {1}(po.{0})) && (foo = foo.{2})) {{\n", JsClassName, JsGetPropFn, JsEventsPropsDict); // if event or property, not a field (default).
				_builder.AppendFormat("                  if (foo[item.name] === 1)\n"); // Event
				_builder.AppendFormat("                     item.glyph = 'vs:GlyphGroupEvent';\n");
				_builder.AppendFormat("                  else if (foo[item.name] === 2)\n"); // Property, 
				_builder.AppendFormat("                     item.glyph = 'vs:GlyphGroupProperty';\n");
				_builder.AppendFormat("               }}\n");
				_builder.AppendFormat("            }}\n");
				_builder.AppendFormat("         }}\n");
				_builder.AppendFormat("      }}\n");
				_builder.AppendFormat("\n");
				_builder.AppendFormat("      return !hide;\n");
				_builder.AppendFormat("   }});\n");
				_builder.AppendFormat("}});\n");
				_builder.AppendFormat("\n");

				// signaturehelp
				_builder.AppendFormat("intellisense.addEventListener('signaturehelp', function (e) {{\n");
				_builder.AppendFormat("   {0}(e.parentObject, e.functionHelp);\n", JsUpdateEventHandlerSignatureHintFn);
				_builder.AppendFormat("}});\n");
				_builder.AppendFormat("\n");

				// statementcompletionhint
				_builder.AppendFormat("intellisense.addEventListener('statementcompletionhint', function (e) {{\n");
				_builder.AppendFormat("   if (e.completionItem.value != null) {{\n");
				_builder.AppendFormat("      if (e.completionItem.value.{0} === true) {{\n", JsEnumMarker);
				_builder.AppendFormat("         e.symbolHelp.symbolDisplayType = 'Enum';\n");
				_builder.AppendFormat("      }}\n");
				_builder.AppendFormat("   }}\n");
				_builder.AppendFormat("\n");
				_builder.AppendFormat("   {0}(e.completionItem.parentObject, e.symbolHelp.functionHelp);\n", JsUpdateEventHandlerSignatureHintFn);
				_builder.AppendFormat("}});\n");
			}

			#region _IJsWriterContext

			public StringBuilder Builder
			{
				get { return _builder; }
			}

			void _IJsBuilderContext.RegisterNS(string ns)
			{
				if (string.IsNullOrEmpty(ns) || _nsCache.Contains(ns))
				{
					return;
				}

				int subNs = 0;
				for (int pnt = 0; (pnt = ns.IndexOf('.', pnt + 1)) >= 0; subNs++)
				{
					string nsPart = ns.Substring(0, pnt);

					if (!_nsCache.Contains(nsPart))
					{
						_nsCache.Add(nsPart);
						_builder.AppendLine(string.Format("{0}{1} = {1} || {{ {2}: true }};", subNs == 0 ? "var " : "", nsPart, JsModuleMarker));
					}
				}

				_nsCache.Add(ns);
				_builder.AppendLine(string.Format("{0}{1} = {1} || {{ {2}: true }};", subNs == 0 ? "var " : "", ns, JsModuleMarker));
			}

			string _IJsBuilderContext.ResolveTypeNameNS(string type)
			{
				if (string.IsNullOrEmpty(type))
				{
					return "Object";
				}

				type = Regex.Replace(type, @"\s*([\w\.]+)\s*(.*)", match =>
				{
					string t = match.Groups[1].Value;
					  
					if (t == "any")
					{
						t = "Object";
					}

					if (_customTypesNS.ContainsKey(t))
					{
						t = _customTypesNS[t]; // FlexGrid -> wijmo.grid.FlexGrid
					}
					else
					{
						// Process fully or partially qualified type name (grid.FlexGrid -> wijmo.grid.FlexGrid).
						if (t.IndexOf('.') > 0)
						{
							string tt = t.Substring(t.LastIndexOf('.') + 1);
							if (_customTypesNS.ContainsKey(tt))
							{
								t = _customTypesNS[tt];
							}
						}
						else
						{
							// Capitalize the first letter, so IntelliSense will work for built-in types.
							if (Regex.IsMatch(t, "^number|boolean|string$", RegexOptions.IgnoreCase))
							{
								t = t[0].ToString().ToUpper() + t.Substring(1).ToLower();
							}
						}
					}

					return t + match.Groups[2];
				});

				return type;
			}

			#endregion
		}

		private class _MemberBuilder
		{
			private _IJsBuilderContext _ctx;

			public _MemberBuilder(_IJsBuilderContext context)
			{
				_ctx = context;
			}

			public _IJsBuilderContext Ctx
			{
				get { return _ctx; }
			}

			internal virtual int Build(IList<Member> members, int startFrom)
			{
				throw new NotImplementedException();
			}

			internal void Build(Member member)
			{
				Build(new Member[] { member }, 0);
			}

			protected string[] SplitComment(string comment)
			{
				if (string.IsNullOrEmpty(comment))
				{
					return new string[0];
				}

				string[] comments = comment.Split('\n');

				for (var i = 0; i < comments.Length; i++)
				{
					comments[i] = comments[i].TrimEnd();
				}

				return comments;
			}

			protected void WritePlainComment(string comment)
			{
				comment = ValidateXml(comment);

				foreach (string line in SplitComment(comment))
				{
					Ctx.Builder.AppendLine("// " + line);
				}
			}

			protected void WriteXmlComment(string comment)
			{
				comment = ValidateXml(comment);

				string[] comments = SplitComment(comment);

				int len = comments.Length;
				for (var i = 0; i < len; i++)
				{
					Ctx.Builder.AppendFormat("{0}{1}{2}",
						i > 0 ? "/// " : string.Empty,
						comments[i],
						i < len - 1 ? "\n" : string.Empty);
				}
			}

			protected void WriteXmlElement(string name, Dictionary<string, string> attributes, string content)
			{
				Ctx.Builder.AppendFormat("/// <{0}", name);

				if (attributes != null)
				{
					int i = 0;
					int cnt = attributes.Count;
					foreach (KeyValuePair<string, string> attr in attributes)
					{
						Ctx.Builder.AppendFormat("{0}{1}=\"{2}\"{3}",
							i == 0 ? " " : string.Empty,
							attr.Key,
							attr.Value,
							i < cnt - 1 ? " " : string.Empty);

						i++;
					}
				}

				Ctx.Builder.Append(">");

				WriteXmlComment(content);

				Ctx.Builder.AppendFormat("</{0}>\n", name);
			}

			/// <summary>
			/// Validates the XML comment.
			/// </summary>
			/// <remarks>
			/// Any members declared below the XML comment that is not well-formed will be hidden from the IntelliSense completion list.
			/// </remarks>
			/// <param name="comment"></param>
			/// <returns></returns>
			private string ValidateXml(string comment)
			{
				// Check whether tags are balanced.
				if (!string.IsNullOrEmpty(comment))
				{
					int parity = 0,
						len = comment.Length;
                    
					for (var i = 0; i < len; i++)
					{
						if (comment[i] == '<')
						{
                            if (i == len - 1 || (comment[i + 1] != '/' && comment[i + 1] != ' '))
							{
								parity++;
							}

							if (i < len - 1)
							{
								for (int j = i + 1; j < len && comment[j] != '>'; j++)
								{
									if (comment[j] == '/' && (comment[j - 1] == '<' || comment[j + 1] == '>'))
									{
										i = j;
										parity--;
									}
								}
							}

						}
					}

					if (parity != 0)
					{
						throw new InvalidOperationException(string.Format("Unbalanced tags has been found: \"{0}\"", comment));
					}

					// Because of XML nature of the comment an ampersand character must be escaped.
					// It seems that only the following named character entities are supported: "amp", "apos", "gt", "lt", "quot", so any other named entities must be removed or replaced with an appropriate numerical entity.
					comment = Regex.Replace(comment, @"(&[a-z]*;?)", match =>
					{
						string str = match.Value;

						if (str.Length > 2 && str.EndsWith(";")) // character entity?
						{
							if (!Regex.IsMatch(str, @"&(amp|apos|gt|lt|quot|#\d+|#x[0-9A-F]+);"))
							{
								switch (str)
								{
									case "&nbsp;":
										str = "&#160;";
										break;
									case "&reg;":
										str = "&#174;";
										break;
									default: // remove unknown entity
										str = string.Empty;
										break;
								}
							}
						}
						else // just an ampesdand character.
						{
							str = str.Replace("&", "&amp;");
						}

						return str;
					});
				}

				return comment;
			}
		}

		private class _EnumBuilder : _MemberBuilder
		{
			public _EnumBuilder(_IJsBuilderContext context)
				: base(context)
			{
			}

			internal override int Build(IList<Member> members, int startFrom)
			{
				Member member = members[startFrom];

				Ctx.RegisterNS(member.Module);

				// begin
				Ctx.Builder.AppendFormat("{0}.{1} = {{\n", member.Module, member.Name);

				// write enumeration values *
				for (var i = 0; i < member.Parameters.Count; i++)
				{
					Parameter parameter = member.Parameters[i];

					if (!string.IsNullOrEmpty(parameter.Comment))
					{
						WritePlainComment(parameter.Comment);
					}

					Ctx.Builder.AppendFormat("{0}: {1},\n", parameter.Name, parameter.Value);
				}

				Ctx.Builder.AppendFormat("{0}: true\n", JsEnumMarker);

				// end
				Ctx.Builder.AppendLine("};\n");

				// annotate 
				if (!string.IsNullOrEmpty(member.Comments))
				{
					Ctx.Builder.AppendFormat("intellisense.annotate({0}, {{\n", member.Module);
					WritePlainComment(member.Comments);
					Ctx.Builder.AppendFormat("{0}: undefined\n", member.Name);
					Ctx.Builder.AppendLine("});\n");
				}

				return startFrom;
			}
		}
		
		private class _WriteMethodBodyEventArgs : EventArgs
		{
			private Member _method;

			public _WriteMethodBodyEventArgs(Member method)
			{
				_method = method;
			}

			public Member Method
			{
				get { return _method; }
			}
		}

		private class _MethodBuilder : _MemberBuilder
		{
			private bool _close;

			public _MethodBuilder(_IJsBuilderContext context, bool close = true)
				: base(context)
			{
				_close = close;
			}

			public event EventHandler<_WriteMethodBodyEventArgs> WriteBody;

			internal override int Build(IList<Member> members, int startFrom)
			{
				Member method = members[startFrom];

				Ctx.RegisterNS(method.Module); 

				if (method.Name == "constructor")
				{
					if (string.IsNullOrEmpty(method.Class))
					{
						//return startFrom; 
						throw new InvalidOperationException(string.Format("Module can't have a constructor. File: '{0}'.", method.ProjectInput.FileName)); 
					}

					Ctx.Builder.AppendFormat("{0}.{1} = function", method.Module, method.Class);
				}
				else
				{
					bool instanceMethod = !string.IsNullOrEmpty(method.Class);

					Ctx.Builder.AppendFormat("{0}{1}{2}.{3} = function",
						method.Module,
						!instanceMethod ? string.Empty : "." + method.Class,
						method.Static || !instanceMethod ? string.Empty : ".prototype",
						method.Name);
				}

				// signature
				Ctx.Builder.Append("(");
				for (int i = 0; i < method.Parameters.Count; i++)
				{
					Ctx.Builder.Append(HandleParameterName(method.Parameters[i].Name) + (i < method.Parameters.Count - 1 ? ", " : ""));
				}
				Ctx.Builder.AppendLine(") {");

				// description
				WriteXmlElement("summary", null, method.Comments);

				// parameters
				foreach (Parameter param in method.Parameters)
				{
					WriteXmlElement("param", new Dictionary<string, string>() {
						{ "name", HandleParameterName(param.Name) },
						{ "type", Ctx.ResolveTypeNameNS(param.Type) },
						{ "optional", param.Optional == true ? "true" : "false" } // lowercase
					}, param.Comment);
				}

				// returns
				if (!string.IsNullOrEmpty(method.Type) && method.Type != "void")
				{
					WriteXmlElement("returns", new Dictionary<string, string>() {
						{ "type", Ctx.ResolveTypeNameNS(method.Type) }
					}, method.ReturnComments);
				}

				OnWriteBody(new _WriteMethodBodyEventArgs(method));

				if (_close)
				{
					Ctx.Builder.AppendLine("}");
				}

				return startFrom;
			}

			private string HandleParameterName(string value)
			{
				return value.Replace("...", string.Empty); // remove spread operator.
			}

			private void OnWriteBody(_WriteMethodBodyEventArgs args)
			{
				if (WriteBody != null)
				{
					WriteBody(this, args);
				}
			}

		}

		private class _InterfaceBuilder : _MemberBuilder
		{
			private Member _ctor = null;
			private List<Member> _fieldsEventsProps = new List<Member>(); // instance mebers only
			private List<Member> _eventsProps = new List<Member>();
			private List<Member> _statFields = new List<Member>();
			private List<Member> _methods = new List<Member>(); // contains both instance and static methods
			private Dictionary<string, string> _eventArgTypes = new Dictionary<string, string>(); // <eventName>:<eventArgType>

			public _InterfaceBuilder(_IJsBuilderContext context)
				: base(context)
			{
			}

			protected Member Ctor
			{
				get { return _ctor; }
			}

			protected List<Member> EventsProps
			{
				get { return _eventsProps; }
			}

			protected List<Member> FieldsEventsProps
			{
				get { return _fieldsEventsProps; }
			}

			protected List<Member> StaticFields
			{
				get { return _statFields; }
			}

			protected List<Member> Methods
			{
				get { return _methods; }
			}

			internal override int Build(IList<Member> members, int startFrom)
			{
				Member iface = members[startFrom];

				// Actually we don't generate any code here. Just fill the collections.
				for (startFrom++; startFrom < members.Count; startFrom++)
				{
					Member member = members[startFrom];
					MemberType kind = member.Kind;

					if ((kind == MemberType.Event || kind == MemberType.Field || kind == MemberType.Method || kind == MemberType.Property) && // class member?
						(member.Class == iface.Name))
					{
                        switch (member.Kind)
						{
							case MemberType.Field:
							case MemberType.Property:
							case MemberType.Event:
								if (member.Static)
								{
									_statFields.Add(member);
								}
								else
								{
									_fieldsEventsProps.Add(member);

									if (member.Kind == MemberType.Event || member.Kind == MemberType.Property)
									{
										_eventsProps.Add(member);
									}
								}
								break;

							case MemberType.Method:
								if (member.Name == "constructor")
								{
									_ctor = member;
								}
								else
								{
									_methods.Add(member);
                                    
                                    if (!member.Static)
									{
                                        // TFS 440388
                                        // wijmo 680 wrote "onСloningItem" with 'С' is unicode char 1057 instead of 'C' 67
                                        // temporary change it to 'C' 67 here 
                                        if (member.Name == "onСloningItem")
                                        {
                                            member.Name = "onCloningItem";
                                        }
										Match match = Regex.Match(member.Name, "^on([A-Z].*)"); // An event-raising method? 
										if (match.Success)
										{
											string eventName = match.Groups[1].Value[0].ToString().ToLower() + match.Groups[1].Value.Substring(1);
											_eventArgTypes.Add(eventName, ExtractEventArgType(member));
										}
									}
								}
								break;

							default:
								// should never get here
								throw new InvalidOperationException("Unexpected member.");
						}
					}
					else
					{
						startFrom--;
						break; // break on next block (another class, enum etc).
					}
				}

				return startFrom;
			}

			protected string GetEventArgType(string eventName)
			{
				if (_eventArgTypes.ContainsKey(eventName))
				{
					return _eventArgTypes[eventName];
				}

				return string.Empty;
			}

			private string ExtractEventArgType(Member method)
			{
				string res = string.Empty;

				if (method.Parameters.Count > 0)
				{
					Parameter param = method.Parameters[0];

					if (string.IsNullOrEmpty(res = param.Type))
					{
						if (!string.IsNullOrEmpty(res = param.Value)) // the event-raising method is described as onMyEvent(args = MyEventArgs.empty)
						{
							// MyEventArgs.empty => MyEventArgs
							int dot = res.LastIndexOf('.'); 
							if (dot >= 0)
							{
								res = res.Substring(0, dot); 
							}
						}
					}
				}

				if (string.IsNullOrEmpty(res))
				{
					res = "EventArgs"; // default
				}

				return Ctx.ResolveTypeNameNS(res);
			}
		}

		private class _ClassBuilder : _InterfaceBuilder
		{
			public _ClassBuilder(_IJsBuilderContext context)
				: base(context)
			{
			}

			internal override int Build(IList<Member> members, int startFrom)
			{
				Member clazz = members[startFrom];

				startFrom = base.Build(members, startFrom); // fill collections

				Member ctor = Ctor != null
					? Ctor
					: new Member(clazz.ProjectInput, clazz.Module, clazz.Name, "constructor() {", clazz.Comments); // create constructor with an empty signature using jsdoc of the parent class itself.

				Ctx.RegisterNS(clazz.Module);

				WriteInternals(clazz, ctor); // generate class description, fields, body.

				// inheritance
				if (!string.IsNullOrEmpty(clazz.BaseClass))
				{
					Ctx.Builder.AppendFormat("{0}.{1}.prototype = new {2}();\n", clazz.Module, clazz.Name, Ctx.ResolveTypeNameNS(clazz.BaseClass));
				}

				WriteMethods();

				WriteStaticFields(); 

				// IntelliSense
				WriteEventsPropsDict(clazz);
				Ctx.Builder.AppendFormat("{0}.{1}.{2} = true;\n", clazz.Module, clazz.Name, JsClassMarker);

				return startFrom;
			}

			private void WriteInternals(Member clazz, Member ctor)
			{
				new _MethodBuilder(Ctx, false).Build(ctor);

				// ** fields
				foreach (Member prop in FieldsEventsProps)
				{
					string propType = prop.Kind == MemberType.Event ? "wijmo.Event" : Ctx.ResolveTypeNameNS(prop.Type);

					WriteXmlElement("field", new Dictionary<string, string>() {
						{ "name", prop.Name },
						{ "type", propType }
					}, prop.Comments);
				}

				WriteBody(clazz);

				Ctx.Builder.AppendLine("}");
			}

			protected virtual void WriteMethods(EventHandler<_WriteMethodBodyEventArgs> onWriteMethodBody = null)
			{
				foreach (Member method in Methods)
				{
					_MethodBuilder builder = new _MethodBuilder(Ctx);

					if (onWriteMethodBody != null)
					{
						builder.WriteBody += onWriteMethodBody;
					}

					builder.Build(method);
				}
			}

			protected virtual void WriteBody(Member clazz)
			{
				Ctx.Builder.AppendFormat("this.{0} = '{1}.{2}';\n", JsClassName, clazz.Module, clazz.Name);

				// * design-time typing support for the event handler's arguments *
				foreach (Member member in FieldsEventsProps)
				{
					if (member.Kind == MemberType.Event)
					{
                        string eventArgType = GetEventArgType(member.Name);
                        
						if (string.IsNullOrEmpty(eventArgType)
                            // wijmo 663/wijmo.chart.animation provides event 'ended' without 'onEnded'
                            // temporary removed check eventArgType here
                            && member.Class != "ChartAnimation" && member.Name != "ended"
                            )
						{
							throw new InvalidOperationException(string.Format("Unable to determine the type of the 'args' parameter of the '{0}.{1}' event.", member.Class, member.Name));
						}

						Ctx.Builder.AppendFormat("this.{0} = new wijmo.Event('{1}');\n", member.Name, eventArgType);
					}
				}

				Ctx.Builder.AppendFormat("{0}(this);\n", JsReownEventsFn); // so the inherited events handlers will use the current type as the type of the "sender" parameter.
			}

			private void WriteStaticFields()
			{
				foreach (Member property in StaticFields)
				{
					new _StaticFieldBuilder(Ctx).Build(property);
				}
			}

			private void WriteEventsPropsDict(Member clazz)
			{
				Ctx.Builder.AppendFormat("{0}.{1}.{2} = {3}({4}, {{",
					clazz.Module,
					clazz.Name,
					JsEventsPropsDict,
					JsMergeFn, 
					string.IsNullOrEmpty(clazz.BaseClass)
						? "{}"
						: Ctx.ResolveTypeNameNS(clazz.BaseClass) + "." + JsEventsPropsDict);

				for (int i = 0; i < EventsProps.Count; i++)
				{
					Member member = EventsProps[i];
					Ctx.Builder.AppendFormat("{0}:{1}{2}",
						member.Name,
						member.Kind == MemberType.Event ? 1 : 2, // event = 1, prop = 2
						i < EventsProps.Count - 1 ? "," : string.Empty);
				}

				Ctx.Builder.AppendLine("});");
			}
		}

		private sealed class _WijmoEventClassBuilder : _ClassBuilder
		{
			public static string JsOwner = JsPrefix + "Owner";
			public static string JsArgType = JsPrefix + "ArgType";
			private static string JsArgInst = JsPrefix + "Arg";

			public _WijmoEventClassBuilder(_IJsBuilderContext context)
				: base(context)
			{
			}

			protected override void WriteBody(Member clazz)
			{
				Ctx.Builder.AppendLine("if (arguments[0]) {"); // EventArg
				Ctx.Builder.AppendFormat("   this.{0} = arguments[0];\n", JsArgType);
				Ctx.Builder.AppendFormat("   this.{0} = new ({1}(this.{2}))();\n", JsArgInst, JsGetPropFn, JsArgType);
				Ctx.Builder.AppendLine("}");

				base.WriteBody(clazz);
			}

			protected override void WriteMethods(EventHandler<_WriteMethodBodyEventArgs> onWriteMethodBody = null)
			{
				foreach (Member method in Methods)
				{
					if (method.Name == "addHandler" || method.Name == "removeHandler")
					{
						// Use a more descriptive moniker instead of native "wijmo.IEventHandler".
						method.Parameters[0].Type = "Function(sender, args)";
					}
				}

				base.WriteMethods((object sender, _WriteMethodBodyEventArgs args) =>
				{
					var ctx = ((_MethodBuilder)sender).Ctx;

					// design-time dynamic typing support for the event handler arguments
					if (args.Method.Name == "addHandler") // (handler, self?: any)
					{
						string handler = args.Method.Parameters[0].Name; // handler
						string self = args.Method.Parameters[1].Name; // self, optional

						ctx.Builder.AppendFormat("if (typeof {0} === 'function') {0}.call({1} || this.{2}, this.{2}, this.{3});\n", handler, self, JsOwner, JsArgInst);
					}
				});
			}
		}

		private class _StaticFieldBuilder : _MemberBuilder
		{
			public _StaticFieldBuilder(_IJsBuilderContext context)
				: base(context)
			{
			}

			internal override int Build(IList<Member> members, int startFrom)
			{
				Member property = members[startFrom];
				string owner = string.IsNullOrEmpty(property.Class)
					? property.Module
					: Ctx.ResolveTypeNameNS(property.Class);

				Ctx.RegisterNS(property.Module);

				Ctx.Builder.AppendFormat("{0}.{1} = undefined;\n", owner, property.Name);

				Ctx.Builder.AppendFormat("intellisense.annotate({0}, {{\n", owner);

				// Write plain comments
				if (!string.IsNullOrEmpty(property.Comments))
				{
					WritePlainComment(property.Comments);
					Ctx.Builder.AppendFormat("{0}: undefined\n", property.Name);
				}

				Ctx.Builder.AppendLine("});");

				return startFrom;
			}
		}

		/// <summary>
		/// Generates a JavaScript IntelliSense file for a given project (except the interop modules).
		/// </summary>
		/// <param name="file"></param>
		/// <param name="project"></param>
		public static void Generate(string file, Project project) {
			try
			{
				_JsBuilder writer = new _JsBuilder();
				writer.Write(project.Items);
				File.WriteAllText(file, writer.Builder.ToString());
			}
			catch (Exception ex)
			{
				project.Errors.Add("[JsIntellisense] " + ex.Message); 
			}
		}
	}
}