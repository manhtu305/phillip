﻿--------------------------------------------------------------------------------
tsdocx readme
--------------------------------------------------------------------------------

tsdocx is a utility that loads an XML project file with the following structure:

<Project InputPath="..."  OutputPath="..."  Minify="true" Document="true">
	<Items>
		<Item File="wijmo.js" >
			<Input File="Util.js" />
			<Input File="Globalize.js" />
			...
	    </Item>        
		<Item File="wijmo.grid.js" >
			<Input File="grid\FlexGrid.js" />
			...
	    </Item>        
	</Items>
	<FileHeader>
/*
 * Wijmo 5 - @version
 * http://wijmo.com/
 *
 * Copyright(c) ComponentOne, Inc.  All rights reserved.
 *
 */
	</FileHeader>
</Project>

For each item specified as an "input", there should two files, one with a "js"
and one with a "ts" extension. The "js" files are used in merging and obfuscating
the code, and the "ts" files are used for extracting the documentation.

tsdocx performs the following tasks:

1) Merges the js files speficied as "inputs" into js files specified as "items".

2) Obfuscates the merged js files and optionally prefixes them with the string 
   specified as a "FileHeader". 
   If the file header contains the string "@version", that string is replaced 
   with the product version as found in the input files. The product version
   must be a constant defined in one of the TypeScript inputs as 
   "_VERSION = 'major.yearQtr.sequential'".

3) Scans the TypeScript files specified as "inputs" and generates XML 
   documentation files similar to the ones created by the C# compiler.
   The scanner generates warnings for several common errors, including:
   - wrong capitalization
   - extra or missing parameters
   - broken links/references

The xml files created in the last step can be localized and later turned
into html files by the xml2html utility.