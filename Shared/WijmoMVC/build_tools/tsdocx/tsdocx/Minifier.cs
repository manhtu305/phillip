﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
#if !GOOGLE_CLOSURE
using Microsoft.Ajax.Utilities;
#endif
using System;
using System.Linq;

namespace tsdocx
{
    public static class Minifier
    {
//#if GOOGLE_CLOSURE
        // minifier (Google's)
        // https://developers.google.com/closure/compiler/docs/gettingstarted_api?csw=1
        // https://developers.google.com/closure/compiler/docs/api-ref
        public static string MinifyJs(string code)
        {
            // compilation parameters
            var values = new NameValueCollection();
            values.Add("js_code", code);
            //values.Add("compilation_level", "ADVANCED_OPTIMIZATIONS");
            values.Add("compilation_level", "SIMPLE_OPTIMIZATIONS");
            //values.Add("compilation_level", "WHITESPACE_ONLY");
            values.Add("output_format", "text");
            values.Add("output_info", "compiled_code");
            values.Add("", "compiled_code");

            // call google closure compiler
            var wc = new WebClient();
            wc.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            var result = wc.UploadValues("http://closure-compiler.appspot.com/compile", "POST", values);

            // parse results
            return result.Length > 1 ? Encoding.UTF8.GetString(result) : null;
        }
//#else //minify using Microsoft Ajax Minifier (http://ajaxmin.codeplex.com/)
        public static string Minify(string code, FileType fileType, out List<string> errors, bool localRename, bool keepLineBreaks)
        {
            Microsoft.Ajax.Utilities.Minifier minifier = new Microsoft.Ajax.Utilities.Minifier();
            string ret = "";
            CodeSettings settings = new CodeSettings();
            if (!localRename)
                settings.KillSwitch |= (long)TreeModifications.LocalRenaming;
            settings.KillSwitch |= (long)TreeModifications.MinifyStringLiterals;
            if (keepLineBreaks)
            {
                settings.OutputMode = OutputMode.MultipleLines;
                settings.IndentSize = 0;
            }
            //settings.AlwaysEscapeNonAscii = true;
            switch (fileType)
            {
                case FileType.JS:
                    ret = minifier.MinifyJavaScript(code, settings);
                    break;
                case FileType.CSS:
                    ret = minifier.MinifyStyleSheet(code);
                    break;
            }
            errors = minifier.ErrorList.Select(e => e.Message).ToList();

            return ret;
        }
//#endif
    }
}
