﻿using System.Diagnostics;
using System;

namespace tsdocx
{
    /// <summary>
    /// Class that reads tokens from an input string.
    /// </summary>
    public class Tokenizer
    {
        string _text;
        int _idx = 0;

        /// <summary>
        /// Initializes a new instance of a <see cref="Tokenizer"/>.
        /// </summary>
        /// <param name="text"></param>
        public Tokenizer(string text)
        {
            _text = text;
        }
        /// <summary>
        /// Gets the next token.
        /// </summary>
        public string GetToken()
        {
            // EOF
            if (_idx >= _text.Length)
            {
                return null;
            }

            // skip whitespace
            for (; _idx < _text.Length; _idx++)
            {
                if (!char.IsWhiteSpace(_text[_idx]))
                {
                    break;
                }
            }

            // get token
            var start = _idx;
            var end = start;
            for (; end < _text.Length - 1; end++)
            {
                if (char.IsWhiteSpace(_text[end]))
                {
                    break;
                }
            }
            _idx = end + 1;

            // return token
            return _text.Substring(start, end - start);
        }
        /// <summary>
        /// Gets the next token ending with a given string.
        /// </summary>
        public string GetToken(params string[] endArr)
        {
            var pos = -1;
            var end = string.Empty;
            foreach (var e in endArr)
            {
                var p = (e == "\0")  // \0 means EOF allowed
                    ? _text.Length - 1
                    : _text.IndexOf(e, _idx);
                if (p > -1)
                {
                    if (pos < 0 || p < pos)
                    {
                        pos = p;
                        end = e;
                    }
                }
            }
            if (pos > -1)
            {
                var start = _idx;
                _idx = pos + end.Length;
                return _text.Substring(start, pos - start + end.Length);
            }

            Debug.WriteLine("end delimiter not found");
            return null;
        }
        /// <summary>
        /// Finds the end of a block delimited by the specified brackets. <paramref name="Position"/> should point to a char
        /// right after the opening bracket.
        /// </summary>
        /// <param name="lbr">Block opening bracket.</param>
        /// <param name="rbr">Block closing bracket.</param>
        /// <param name="advance">Indicates whether <paramref name="Position"/> should be advanced to after the closing bracket.</param>
        /// <returns>Closing bracket position.</returns>
        public int EndBlock(char lbr, char rbr, bool advance = false)
        {
            int ret = Tokenizer.FindEndOfBlock(_text, _idx, lbr, rbr);
            if (advance && ret > -1)
                _idx = ret + 1;
            return ret;
        }
        public static int FindEndOfBlock(string text, int startPos, char lbr, char rbr)
        {
            // find closing bracket
            var nest = 1;
            var quoting = (char)0;
            for (var pos = startPos; pos < text.Length; pos++)
            {
                var c = text[pos];

                // handle quotes
                if (c == '\'' || c == '\"')
                {
                    if (quoting == c)
                    {
                        quoting = (char)0;
                    }
                    else if (quoting == (char)0)
                    {
                        quoting = c;
                    }
                    continue;
                }
                if (c == '\r')
                {
                    quoting = (char)0;
                }

                // handle strings
                if (quoting != (char)0)
                {
                    continue;
                }

                // handle comments
                if (c == '/' && pos < text.Length - 1)
                {
                    // /* comments */
                    if (text[pos + 1] == '*')
                    {
                        pos = text.IndexOf("*/", pos + 2);
                        if (pos < 0)
                        {
                            return -1;
                        }
                        pos++;
                        continue;
                    }
                    // // comments (but look out for regexes, e.g.
                    // if (navigator.userAgent.match(/ MSIE | Trident\/| Edge\//)) {
                    // here we make sure the match is "//" and not "\//", but this solution doesn't seem perfect
                    // maybe it would be better to match "//" and not "//)"?
                    else if (text[pos + 1] == '/' && (pos == 0 || text[pos - 1] != '\\'))
                    {
                        pos = text.IndexOf("\r", pos);
                        if (pos < 0)
                        {
                            pos = text.Length - 1;
                        }
                        continue;
                    }
                }

                // handle nesting
                if (c == /*'{'*/lbr)
                {
                    nest++;
                    continue;
                }
                else if (c == /*'}'*/rbr)
                {
                    nest--;
                    if (nest == 0)
                    {
                        return pos;
                    }
                }
            }

            // if we got here we didn't find the end of the block...
            return -1;
        }
        /// <summary>
        /// Gets the current input position.
        /// </summary>
        public int Position
        {
            get { return _idx; }
            set { _idx = value; }
        }
    }
}
