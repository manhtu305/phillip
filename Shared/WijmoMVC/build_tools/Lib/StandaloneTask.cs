using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime;
using System.Linq;

namespace GeneratorUtils
{
    /// <summary>
    /// Reperesents MSBuild Task that is capable to work without MSBuild environment.
    /// </summary>
    public abstract class StandaloneTask : Task
    {
        public override sealed bool Execute()
        {
            if (BuildEngine == null)
                BuildEngine = new MockBuildEngine();
            return ExecuteImpl();
        }

        /// <summary>
        /// Gets a value indicating whether the specified <paramref name="taskItem"/> has the specified metadata <paramref name="name"/>.
        /// </summary>
        /// <param name="taskItem"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool HasMetadata(ITaskItem taskItem, string name)
        {
            return taskItem.MetadataNames.Cast<string>().Contains(name);
        }

        public abstract bool ExecuteImpl();
    }


    internal class MockBuildEngine : IBuildEngine
    {
        private List<CustomBuildEventArgs> customEvents = new List<CustomBuildEventArgs>();
        private List<BuildErrorEventArgs> errors = new List<BuildErrorEventArgs>();
        private List<BuildMessageEventArgs> messages = new List<BuildMessageEventArgs>();
        private List<BuildWarningEventArgs> warnings = new List<BuildWarningEventArgs>();

        public bool BuildProjectFile(string projectFileName, string[] targetNames, IDictionary globalProperties, IDictionary targetOutputs)
        {
            throw new NotImplementedException();
        }

        public virtual void LogCustomEvent(CustomBuildEventArgs eventArgs)
        {
            this.customEvents.Add(eventArgs);
            OutputMessage(eventArgs);
        }

        public virtual void LogErrorEvent(BuildErrorEventArgs eventArgs)
        {
            this.errors.Add(eventArgs);
            OutputMessage(eventArgs);
        }

        public virtual void LogMessageEvent(BuildMessageEventArgs eventArgs)
        {
            this.messages.Add(eventArgs);
            OutputMessage(eventArgs);
        }

        public virtual void LogWarningEvent(BuildWarningEventArgs eventArgs)
        {
            this.warnings.Add(eventArgs);
            OutputMessage(eventArgs);
        }

        public int ColumnNumberOfTaskNode
        {
            get
            {
                return 0;
            }
        }

        public bool ContinueOnError
        {
            get
            {
                return false;
            }
        }

        internal ICollection<CustomBuildEventArgs> CustomEvents
        {
            get
            {
                return this.customEvents;
            }
        }

        internal ICollection<BuildErrorEventArgs> Errors
        {
            get
            {
                return this.errors;
            }
        }

        public int LineNumberOfTaskNode
        {
            get
            {
                return 0;
            }
        }

        internal ICollection<BuildMessageEventArgs> Messages
        {
            get
            {
                return this.messages;
            }
        }

        public string ProjectFileOfTaskNode
        {
            get
            {
                return string.Empty;
            }
        }

        internal ICollection<BuildWarningEventArgs> Warnings
        {
            get
            {
                return this.warnings;
            }
        }

        private void OutputMessage(LazyFormattedBuildEventArgs eventArgs)
        {
            Console.WriteLine(eventArgs.Message);
        }
    }
}
