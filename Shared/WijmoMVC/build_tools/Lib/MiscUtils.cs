﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneratorUtils
{
    static class MiscUtils
    {
        static public string ReplaceString(string str, string oldValue, string newValue,
            StringComparison comparison)
        {
            bool isChanged;
            return ReplaceString(str, oldValue, newValue, comparison, out isChanged);
        }
        static public string ReplaceString(string str, string oldValue, string newValue, 
            StringComparison comparison, out bool isChanged) 
        {
            isChanged = false;
            StringBuilder sb = new StringBuilder(); 
            int previousIndex = 0; 
            int index = str.IndexOf(oldValue, comparison); 
            while (index != -1) 
            {
                isChanged = true;
                sb.Append(str.Substring(previousIndex, index - previousIndex)); 
                sb.Append(newValue); 
                index += oldValue.Length; 
                previousIndex = index; 
                index = str.IndexOf(oldValue, index, comparison); 
            } 
            sb.Append(str.Substring(previousIndex)); 
            return sb.ToString(); 
        }

        public static string RemoveLineBreaks(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Replace("\r", "").Replace("\n", "");
        }
    }
}
