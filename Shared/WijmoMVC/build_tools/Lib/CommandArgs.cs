﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GeneratorUtils
{
    /// <summary>
    /// Helper class for command line arguments processing.
    /// </summary>
    class CommandArgs
    {
        private static readonly Regex _paramRegex = new Regex(@"^/(?<name>.+?)(:(?<val>.+))?$");

        private readonly Dictionary<string, string> _dictionary = new Dictionary<string, string>();

        public CommandArgs(string[] args)
        {
            foreach (string par in args)
            {
                string trimmedPar = par.Trim();
                if (trimmedPar.StartsWith("/"))
                {
                    Match match = _paramRegex.Match(trimmedPar);
                    if (!match.Success)
                    {
                        Console.WriteLine(string.Format("Wrong parameter '{0}'", par));
                        throw new ArgumentException();
                    }
                    string parName = match.Groups["name"].Value;
                    Group valGroup = match.Groups["val"];
                    _dictionary.Add(parName, valGroup.Success ? valGroup.Value : "");
                }
                else
                    _dictionary.Add("", trimmedPar);
            }
        }

        public Dictionary<string, string> Dictionary
        {
            get { return _dictionary; }
        }

        /// <summary>
        /// Returns null only if optional parameter is not specified.
        /// </summary>
        /// <param name="paramsDict"></param>
        /// <param name="paramName"></param>
        /// <param name="required"></param>
        /// <param name="requiresValue"></param>
        /// <returns></returns>
        public string GetParam(string paramName, bool required, bool requiresValue)
        {
            string ret;
            if (!_dictionary.TryGetValue(paramName, out ret))
            {
                if (required)
                {
                    Console.WriteLine(string.Format("Parameter '{0}' is not specified", paramName));
                    throw new ArgumentException();
                }
                return null;
            }
            if (requiresValue && string.IsNullOrEmpty(ret))
            {
                Console.WriteLine(string.Format("Parameter '{0}' requires a value", paramName));
                throw new ArgumentException();
            }
            return ret ?? "";
        }
    }
}
