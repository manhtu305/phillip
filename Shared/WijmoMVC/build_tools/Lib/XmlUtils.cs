﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Text.RegularExpressions;
using System.Net;


namespace GeneratorUtils
{
    static class XmlUtils
    {
        const string ClrNsDefPrefix = @"clr-namespace:";
        const string ClrNsDefAssemblyPrefix = @"assembly=";

        public static XmlDocument GetLoadedXmlDocument(string xmlFileAbsPath, string defaultXmlNsPrefix,
            out XmlNamespaceManager nsmgr)
        {
            return GetLoadedXmlDocumentImpl(new StringReader(File.ReadAllText(xmlFileAbsPath)), defaultXmlNsPrefix, out nsmgr);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="defaultXmlNsPrefix">
        /// The xmlns prefix name that will be mapped to a default xmlns defined in a document's 
        /// root element.
        /// </param>
        /// <returns></returns>
        public static XmlDocument GetLoadedXmlDocument(TextReader reader, string defaultXmlNsPrefix,
            out XmlNamespaceManager nsmgr)
        {
            return GetLoadedXmlDocumentImpl(reader, defaultXmlNsPrefix, out nsmgr);
#if false
            XmlDocument xDoc = new XmlDocument();
            xDoc.PreserveWhitespace = true;
            xDoc.Load(reader);
            nsmgr = new XmlNamespaceManager(
                xDoc.NameTable);
            if (!string.IsNullOrEmpty(defaultXmlNsPrefix) && xDoc.DocumentElement != null &&
                !string.IsNullOrEmpty(xDoc.DocumentElement.NamespaceURI))
            {
                nsmgr.AddNamespace(defaultXmlNsPrefix, xDoc.DocumentElement.NamespaceURI);
            }

            return xDoc;
#endif
        }

        public static XmlDocument GetLoadedXmlDocument(XmlReader reader, string defaultXmlNsPrefix,
            out XmlNamespaceManager nsmgr)
        {
            return GetLoadedXmlDocumentImpl(reader, defaultXmlNsPrefix, out nsmgr);
        }

        private static XmlDocument GetLoadedXmlDocumentImpl(object reader, string defaultXmlNsPrefix,
            out XmlNamespaceManager nsmgr)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.PreserveWhitespace = true;
            if (reader is TextReader) 
                xDoc.Load((TextReader)reader);
            else
                xDoc.Load((XmlReader)reader);
            nsmgr = CreateDefaultNamespaceManager(xDoc, defaultXmlNsPrefix);
            //nsmgr = new XmlNamespaceManager(
            //    xDoc.NameTable);
            //if (!string.IsNullOrEmpty(defaultXmlNsPrefix) && xDoc.DocumentElement != null &&
            //    !string.IsNullOrEmpty(xDoc.DocumentElement.NamespaceURI))
            //{
            //    nsmgr.AddNamespace(defaultXmlNsPrefix, xDoc.DocumentElement.NamespaceURI);
            //}

            return xDoc;
        }

        public static XmlNamespaceManager CreateDefaultNamespaceManager(XmlDocument xDoc, string defaultXmlNsPrefix)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(
                xDoc.NameTable);
            if (!string.IsNullOrEmpty(defaultXmlNsPrefix) && xDoc.DocumentElement != null &&
                !string.IsNullOrEmpty(xDoc.DocumentElement.NamespaceURI))
            {
                nsmgr.AddNamespace(defaultXmlNsPrefix, xDoc.DocumentElement.NamespaceURI);
            }
            return nsmgr;
        }

        public static List<XmlNode>[] GetXmlDocumentValues(string xmlFileAbsPath, string defaultXmlNsPrefix,
            params string[] valueXPaths)
        {
            return GetXmlDocumentValues(new StringReader(File.ReadAllText(xmlFileAbsPath)), defaultXmlNsPrefix, valueXPaths);
        }

        public static List<XmlNode>[] GetXmlDocumentValues(TextReader reader, string defaultXmlNsPrefix,
            params string[] valueXPaths)
        {
            XmlNamespaceManager nsmgr;
            XmlDocument xDoc = XmlUtils.GetLoadedXmlDocument(reader, defaultXmlNsPrefix, out nsmgr);
            if (valueXPaths == null || valueXPaths.Length == 0)
                return new List<XmlNode>[0];
            List<XmlNode>[] ret = new List<XmlNode>[valueXPaths.Length];
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = CloneNodeList(xDoc.SelectNodes(valueXPaths[i], nsmgr));
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentElement"></param>
        /// <param name="nsmgr"></param>
        /// <param name="defaultXmlNsPrefix"></param>
        /// <param name="elementNames"></param>
        /// <param name="attributeValues">
        /// If attribute name (dict key) starts with '~' then the attribute is not used in a search operation.
        /// </param>
        /// <returns></returns>
        public static XmlElement EnsureElementChain(XmlElement parentElement, XmlNamespaceManager nsmgr,
            string defaultXmlNsPrefix, IList<string> elementNames,
            IList<Dictionary<string, string>> attributeValues)
        {
            return EnsureElementChainImpl(parentElement, nsmgr, defaultXmlNsPrefix, elementNames, attributeValues, 0);
        }

        private static XmlElement EnsureElementChainImpl(XmlElement parentElement, XmlNamespaceManager nsmgr,
            string defaultXmlNsPrefix, IList<string> elementNames,
            IList<Dictionary<string, string>> attributeValues, int elementIdx)
        {
            string elemName = elementNames[elementIdx];
            string queryString = (string.IsNullOrEmpty(defaultXmlNsPrefix) ? "" : defaultXmlNsPrefix + ":") + elemName;
            Dictionary<string, string> attrValuesDict = attributeValues != null && elementIdx < attributeValues.Count ? 
                attributeValues[elementIdx] : null;
            if (attrValuesDict != null && attrValuesDict.Count > 0)
            {
                StringBuilder attrFilterSb = new StringBuilder();
                bool notFirst = false;
                foreach (KeyValuePair<string, string> pairs in attrValuesDict)
                {
                    if (!pairs.Key.StartsWith("~"))
                    {
                        if (notFirst)
                            attrFilterSb.Append(" and ");
                        attrFilterSb.Append("@");
                        attrFilterSb.Append(pairs.Key);
                        attrFilterSb.Append("='");
                        attrFilterSb.Append(pairs.Value);
                        attrFilterSb.Append("'");

                        notFirst = true;
                    }
                }

                if (notFirst)
                    queryString += "[" + attrFilterSb.ToString() + "]";
            }

            XmlElement child = (XmlElement)parentElement.SelectSingleNode(queryString, nsmgr);
            if (child == null)
            {
                child = (XmlElement)parentElement.AppendChild(parentElement.OwnerDocument.CreateElement(elemName));
                if (attrValuesDict != null && attrValuesDict.Count > 0)
                {
                    foreach (KeyValuePair<string, string> pairs in attrValuesDict)
                    {
                        child.SetAttribute(pairs.Key.TrimStart('~'), pairs.Value);
                    }
                }
            }

            elementIdx++;
            if (elementIdx < elementNames.Count)
            {
                return EnsureElementChainImpl(child, nsmgr, defaultXmlNsPrefix, elementNames, attributeValues,
                    elementIdx);
            }
            else
                return child;

        }

        public static List<XmlNode> CloneNodeList(XmlNodeList nodeList)
        {
            List<XmlNode> ret = new List<XmlNode>();
            if (nodeList != null)
            {
                foreach (XmlNode curNode in nodeList)
                    ret.Add(curNode);
            }

            return ret;
        }


        /// <summary>
        /// If <paramref name="strGuid"/> is wrapped with braces "{}" then the returning value is wrapped
        /// with braces too.
        /// </summary>
        /// <param name="strGuid"></param>
        /// <param name="increment"></param>
        /// <returns></returns>
        public static string IncrementGuid(string strGuid, int increment)
        {
            strGuid = strGuid.Trim();
            bool isStripped = strGuid.StartsWith("{");
            if (isStripped)
                strGuid = strGuid.TrimStart('{').TrimEnd('}');
            string strGuidStart = strGuid.Substring(0, 8);
            UInt32 guidStart = UInt32.Parse(strGuidStart,
                System.Globalization.NumberStyles.HexNumber,
                System.Globalization.CultureInfo.InvariantCulture.NumberFormat);

            string originalStart = guidStart.ToString("X8");

            guidStart = (uint)((int)guidStart + increment);
            string newStart = guidStart.ToString("X8");

            strGuid = strGuid.Replace(originalStart, newStart);
            if (isStripped)
                strGuid = "{" + strGuid + "}";

            return strGuid;
        }

        public static bool ParseClrNsDef(string nsUri, out string clrNs, out string assembly)
        {
            clrNs = null;
            assembly = null;
            if (string.IsNullOrEmpty(nsUri))
                return false;
            int nsDefIdx = nsUri.IndexOf(ClrNsDefPrefix);
            if (nsDefIdx < 0)
                return false;
            int semiColIdx = nsUri.IndexOf(';');
            int nsIdx = nsDefIdx + ClrNsDefPrefix.Length;
            int nsLength = semiColIdx < 0 || semiColIdx < nsDefIdx ? nsUri.Length - nsIdx : semiColIdx - nsIdx;
            clrNs = nsUri.Substring(nsIdx, nsLength);
            if (semiColIdx < 0)
                return true;
            int assmDefIdx = nsUri.IndexOf(ClrNsDefAssemblyPrefix);
            if (assmDefIdx < 0)
                return true;
            int assmIdx = assmDefIdx + ClrNsDefAssemblyPrefix.Length;
            int assmLength = semiColIdx < assmIdx ? nsUri.Length - assmIdx : semiColIdx - assmIdx;
            assembly = nsUri.Substring(assmIdx, assmLength);
            return true;
        }

        public static string MakeClrNsDef(string clrNs, string assembly)
        {
            if (string.IsNullOrEmpty(clrNs))
                throw new ArgumentException("XmlUtils.MakeClrNsDef - the 'clrNs' can't be empty.");
            StringBuilder sb = new StringBuilder();
            sb.Append(ClrNsDefPrefix);
            sb.Append(clrNs);
            if (!string.IsNullOrEmpty(assembly))
            {
                sb.Append(";");
                sb.Append(ClrNsDefAssemblyPrefix);
                sb.Append(assembly);
            }

            return sb.ToString();
        }

    }

    internal class PackUri
    {
        private static readonly Regex _rx = new Regex(@"^\s*(?<prefix>pack:[\\/][\\/]application:,,,)?([\\/]?(?<assembly>.+?)(;(?<asmVer>.+?))?(;(?<asmKey>.+?))?;component(?=[\\/]))?(?<path>[^;]+?)\s*$", RegexOptions.IgnoreCase);
        
        public bool HasSchemaAuthorityPrefix { get; set; }
        public string AssemblyName { get; set; }
        public string AssemblyVersion { get; set; }
        public string AssemblyKey { get; set; }
        public string Path { get; set; }
        public bool IsValid { get; set; }

        public PackUri(string uri)
        {
            Parse(uri);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (HasSchemaAuthorityPrefix)
                sb.Append(@"pack://application:,,,");
            if (!string.IsNullOrEmpty(AssemblyName))
            {
                sb.Append('/');
                sb.Append(AssemblyName);
                AddAsmPart(sb, AssemblyVersion);
                AddAsmPart(sb, AssemblyKey);
                AddAsmPart(sb, "component");
            }
            sb.Append(Path);

            return sb.ToString();
        }
        
        private void Parse(string uri)
        {
            IsValid = true;
            if (string.IsNullOrEmpty(uri))
                return;
            Match m = _rx.Match(uri);
            if (m.Success)
            {
                HasSchemaAuthorityPrefix = !string.IsNullOrEmpty(m.Groups["prefix"].Value);
                AssemblyName = m.Groups["assembly"].Value;
                AssemblyVersion = m.Groups["asmVer"].Value;
                AssemblyKey = m.Groups["asmKey"].Value;
                Path = m.Groups["path"].Value;
            }
            IsValid = m.Success;
        }

        private void AddAsmPart(StringBuilder sb, string part)
        {
            if (!string.IsNullOrEmpty(part))
            {
                sb.Append(';');
                sb.Append(part);
            }
        }
    }

    //TBD: doen't work
    internal class ParsedAspx
    {
        private const string LeftBracket = "<%";
        private const string RightBracket = "%>";
        private const string LeftBracketSubstitute = "~~~~~~~~~%";
        private const string RightBracketSubstitute = "%#########";
        
        private readonly XmlDocument _xmlDocument;
        private readonly string _nonXmlHeader;
        private readonly XmlNamespaceManager _namespaceManager;

        private class AspxUriResolver : XmlResolver
        {
            private ICredentials _credentials = null;
            
            public override ICredentials Credentials
            {
                set
                {
                    this._credentials = value;
                }
            }
            
            public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
            {
                if (absoluteUri.Equals(@"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"))
                {
                    return File.Open(@"Y:\Silverlight\Branches\SL5Converter\Utils\Shared\DTD\xhtml1-transitional.dtd",
                        FileMode.Open, FileAccess.Read);
                }
                else
                {
                    return new MemoryStream();
                }
            }

            public override Uri ResolveUri(Uri baseUri, string relativeUri)
            {
                Uri ret;
                if (string.Compare(relativeUri.ToString(), @"-//W3C//DTD XHTML 1.0 Transitional//EN", true) == 0)
                    ret = new Uri(@"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd", UriKind.Absolute);
                else
                    ret = base.ResolveUri(baseUri, relativeUri);
                return ret;
            }
        }
        
        private ParsedAspx(string content, string searchXmlNsPrefix)
        {
            string xmlBody;
            PreprocessParsing(content, out _nonXmlHeader, out xmlBody);
            using (StringReader sr = new StringReader(xmlBody))
            {
                XmlReaderSettings xrSettings = new XmlReaderSettings();
                xrSettings.DtdProcessing = DtdProcessing.Parse;
                xrSettings.XmlResolver = new AspxUriResolver();
                xrSettings.CheckCharacters = false;
                using (XmlReader xr = XmlReader.Create(sr, xrSettings))
                {
                    _xmlDocument = XmlUtils.GetLoadedXmlDocument(xr, searchXmlNsPrefix, out _namespaceManager);
                }
            }
        }

        public static ParsedAspx FromContent(string content, string searchXmlNsPrefix)
        {
            return new ParsedAspx(content, searchXmlNsPrefix);
        }

        public XmlDocument XmlDocument
        {
            get { return _xmlDocument; }
        }

        public string NonXmlHeader
        {
            get { return _nonXmlHeader; }
        }

        public XmlNamespaceManager NamespaceManager
        {
            get { return _namespaceManager; }
        }

        public string SaveToString()
        {
            string ret;
            using (StringWriter swr = new StringWriter())
            {
                XmlWriterSettings xwSettings = new XmlWriterSettings();
                xwSettings.OmitXmlDeclaration = true;
                using (XmlWriter xwr = XmlWriter.Create(swr, xwSettings))
                {
                    XmlDocument.Save(xwr);
                    ret = swr.ToString();
                }
            }
            ret = NormalizeForSave(NonXmlHeader + ret);
            return ret;
        }

        private void PreprocessParsing(string content, out string nonXmlHeader, out string xmlBody)
        {
            xmlBody = "";
            nonXmlHeader = NormalizeForParsing(content);
            //int tagIdx = nonXmlHeader.IndexOf('<');
            int tagIdx = nonXmlHeader.IndexOf("<body", StringComparison.OrdinalIgnoreCase);
            if (tagIdx >= 0)
            {
                if (nonXmlHeader.Substring(tagIdx).StartsWith("<!DOCTYPE", StringComparison.OrdinalIgnoreCase))
                    tagIdx = nonXmlHeader.IndexOf("<", tagIdx + 1);
                xmlBody = nonXmlHeader.Substring(tagIdx);
                nonXmlHeader = nonXmlHeader.Substring(0, tagIdx);
            }
        }

        private string NormalizeForParsing(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Replace(LeftBracket, LeftBracketSubstitute).Replace(RightBracket, RightBracketSubstitute);
        }

        private string NormalizeForSave(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Replace(LeftBracketSubstitute, LeftBracket).Replace(RightBracketSubstitute, RightBracket);
        }
    }
}
