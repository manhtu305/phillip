﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Linq;


namespace GeneratorUtils
{
    public class Solution
    {
        private enum SectionTypes
        {
            Global,
            Project
        }
        
        //Well known solution section names and phases
        internal static readonly string Section_TeamFoundationVersionControl = "TeamFoundationVersionControl";
        internal static readonly string Section_ProjectConfigurationPlatforms = "ProjectConfigurationPlatforms";
        internal static readonly string SectionPhase_ProjectConfigurationPlatforms = "postSolution";
        internal static readonly string Section_NestedProjects = "NestedProjects";
        internal static readonly string SectionPhase_NestedProjects = "preSolution";
        internal static readonly string Section_SolutionConfigurationPlatforms = "SolutionConfigurationPlatforms";
        internal static readonly string SectionPhase_SolutionConfigurationPlatforms = "preSolution";
        internal static readonly string Section_SolutionProperties = "SolutionProperties";
        internal static readonly string SectionPhase_SolutionProperties = "preSolution";

        //Well known solution/project guids
        internal const string SlnVirtualFolderGuid = "{2150E333-8FDC-42A3-9474-1A3956D46DE8}";
        internal const string SlnProjectGuid = "{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}";
        internal const string AspProjectGuid = "{349c5851-65df-11da-9384-00065b846f21}";
        internal const string VbWebMvcProjectGuid = @"{F85E285F-A4E0-4152-9332-AB1D724D3325}";
        internal const string VbProjectGuid = @"{F184B08F-C81C-45f6-A57F-5ABD9991F28F}";
        
        private readonly SolutionProjects _projects = new SolutionProjects();
        private readonly SolutionSections _sections = new SolutionSections();
        private string _formatVersion = "11.00";
        private string _studioName = "2010";

        /// <summary>
        /// Possible values - usual studio name association:
        /// "10.00" - VS2008
        /// "11.00" - VS2010
        /// "12.00" - VS2012
        /// </summary>
        public string FormatVersion
        {
            get { return _formatVersion; }
            set
            {
                /*   There is no strict association
                switch (value)
                {
                    case "10.00":
                        _studioName = "2008";
                        break;
                    case "11.00":
                        _studioName = "2010";
                        break;
                    case "12.00":
                        _studioName = "2012";
                        break;
                    default:
                        throw new ArgumentException(string.Format("'{0}' is an unknown Visual Studio format version", value));
                }
                 */
                
                _formatVersion = value;
            }
        }

        public string StudioName
        {
            get { return _studioName; }
            set { _studioName = value; }
        }
        
        internal SolutionProjects Projects
        {
            get { return _projects; }
        }

        internal SolutionSections Sections
        {
            get { return _sections; }
        }

        public void LoadFromString(string content)
        {
            Parse(content);
        }

        public string SaveToString(params string[] excludeSections)
        {
            return Generate(excludeSections);
        }

        /// <summary>
        /// Updates in Global and Project sections.
        /// </summary>
        /// <param name="oldGuid"></param>
        /// <param name="newGuid"></param>
        internal void UpdateProjectGuidInSections(string oldGuid, string newGuid)
        {
            UpdateProjectGuidInSections(oldGuid, newGuid, Sections);
            foreach (SolutionProjectRef projRef in Projects)
            {
                UpdateProjectGuidInSections(oldGuid, newGuid, projRef.ProjectSections);
            }
        }

        internal void UpdateProjectGuidInSections(string oldGuid, string newGuid, SolutionSections sections)
        {
            foreach (SolutionSection sect in sections)
            {
                foreach (SectionItem item in sect.Items)
                {
                    if (item.Name.StartsWith(oldGuid, StringComparison.OrdinalIgnoreCase))
                        item.Name = newGuid + item.Name.Remove(0, oldGuid.Length);
                    if (item.Value.StartsWith(oldGuid, StringComparison.OrdinalIgnoreCase))
                        item.Value = newGuid + item.Value.Remove(0, oldGuid.Length);
                }
            }
        }

        /// <summary>
        /// Fully removes the specified project from the solution (from Projects and all sections).
        /// </summary>
        /// <param name="projRef"></param>
        internal void RemoveProject(SolutionProjectRef projRef)
        {
            RemoveProjectGuidInSections(projRef.ProjectGuid);
            Projects.Remove(projRef);
        }

        /// <summary>
        /// Removes in Global and Project sections.
        /// </summary>
        /// <param name="guid"></param>
        internal void RemoveProjectGuidInSections(string guid)
        {
            RemoveProjectGuidInSections(guid, Sections);
            foreach (SolutionProjectRef projRef in Projects)
            {
                RemoveProjectGuidInSections(guid, projRef.ProjectSections);
            }
        }

        internal void RemoveProjectGuidInSections(string guid, SolutionSections sections)
        {
            foreach (SolutionSection sect in sections)
            {
                for (int i = sect.Items.Count - 1; i >= 0; i--)
                {
                    if (sect.Items[i].Name.StartsWith(guid, StringComparison.OrdinalIgnoreCase))
                        sect.Items.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// See GetVirtualFolder(string projectGuid, out string virtualFolderGuid) override.
        /// </summary>
        /// <param name="projectGuid"></param>
        /// <returns></returns>
        internal string GetVirtualFolder(string projectGuid)
        {
            string virtualFolderGuid;
            return GetVirtualFolder(projectGuid, out virtualFolderGuid);
        }
        
        /// <summary>
        /// Returns a name of virtual folder containing the project with the specified GUID, or a null
        /// value if project is not in virtual folder or absent.
        /// </summary>
        /// <param name="projectGuid"></param>
        /// <param name="virtualFolderGuid"></param>
        /// <returns></returns>
        internal string GetVirtualFolder(string projectGuid, out string virtualFolderGuid)
        {
            virtualFolderGuid = null;
            SolutionSection nestedSect = Sections.FindByName(Section_NestedProjects);
            if (nestedSect == null)
                return null;
            SectionItem mapItem = nestedSect.Items.FirstOrDefault(
                item => item.Name.StartsWith(projectGuid, StringComparison.OrdinalIgnoreCase));
            if (mapItem == null)
                return null;
            SolutionProjectRef vfProjRef = Projects.FindByProjectGuid(mapItem.Value);
            if (vfProjRef == null)
                return null;
            virtualFolderGuid = vfProjRef.ProjectGuid;
            return vfProjRef.Name;
        }
        
        private void Parse(string content)
        {
            _sections.Clear();
            _projects.Clear();
            Regex headerRegex = new Regex(@"^\s*Microsoft Visual Studio Solution File, Format Version\s+(?<formatVersion>.*?)\s*?\r?\n#\s*Visual Studio\s+(?<vsName>.*?)\s*?\r?\n",
                RegexOptions.None);
            Regex projectRegex = new Regex(@"(?<projFirstLine>(?<=\r?\n)Project\(""(?<projTypeGuid>\{.+?\})""\)\s*=\s*""(?<projName>.+?)""\s*,\s*""(?<projPath>.+?)""\s*,\s*""(?<projGuid>.+?)""[\s-[\r\n]]*)(?<innerText>(.|\n)*?)\bEndProject\s*[\r\n]+",
                RegexOptions.None);

            Match match;
            if ((match = headerRegex.Match(content)).Success)
            {
                FormatVersion = match.Groups["formatVersion"].Value;
                StudioName = match.Groups["vsName"].Value;
            }
            int sta = 0;
            // Parse project references
            while ((match = projectRegex.Match(content, sta)).Success)
            {
                sta = match.Index + match.Length;
                SolutionProjectRef projRef = new SolutionProjectRef() 
                { 
                    TypeGuid = match.Groups["projTypeGuid"].Value,
                    Name = match.Groups["projName"].Value,
                    Path = match.Groups["projPath"].Value,
                    ProjectGuid = match.Groups["projGuid"].Value,
                    //InnerText = match.Groups["innerText"].Value.Trim('\r', '\n')
                };
                string innerText = match.Groups["innerText"].Value.Trim('\r', '\n') + "\n";
                int idx = 0;
                ParseSections(SectionTypes.Project, innerText, ref idx, projRef.ProjectSections);
                _projects.Add(projRef);
            }

            // Parse sections
            ParseSections(SectionTypes.Global, content, ref sta, Sections);
#if false // moved to ParseSections
            Regex sectionRegex = new Regex(@"\s*GlobalSection\s*\(\s*(?<sectionName>.+?)\s*\)\s*=\s*(?<sectionPhaze>\w+)\s*[\r\n]+");
            Regex eosRegex = new Regex(@"\s*EndGlobalSection\s*[\r\n]+");
            while ((match = sectionRegex.Match(content, sta)).Success)
            {
                sta = match.Index + match.Length;
                SolutionSection section = new SolutionSection()
                {
                    Name = match.Groups["sectionName"].Value,
                    Phase = match.Groups["sectionPhaze"].Value
                };
                Sections.Add(section);
                Match eosMatch = eosRegex.Match(content, sta);
                if (eosMatch.Success)
                {
                    string[] secItems = content.Substring(sta, eosMatch.Index - sta).Split(
                        new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string curItem in secItems)
                    {
                        string[] pair = curItem.Split('=');
                        if (pair.Length == 2)
                        {
                            section.Items.Add(new SectionItem()
                                {
                                    Name = pair[0].Trim(),
                                    Value = pair[1].Trim()
                                });
                        }
                    }
                }
                else
                    throw new Exception("EndGlobalSection is not found.");
            }
#endif
        }

        private static void ParseSections(SectionTypes sectionType, string content, ref int sta, SolutionSections toSections)
        {
            if (string.IsNullOrEmpty(content) || content.Trim() == "")
                return;

            string startBracket, endBracket;
            GetSectionBrackets(sectionType, out startBracket, out endBracket);

            Regex sectionRegex = new Regex(@"\s*" + startBracket + @"\s*\(\s*(?<sectionName>.+?)\s*\)\s*=\s*(?<sectionPhaze>\w+)\s*[\r\n]+");
            Regex eosRegex = new Regex(@"\s*" + endBracket + @"\s*[\r\n]+");
            Match match; 
            while ((match = sectionRegex.Match(content, sta)).Success)
            {
                sta = match.Index + match.Length;
                SolutionSection section = new SolutionSection()
                {
                    Name = match.Groups["sectionName"].Value,
                    Phase = match.Groups["sectionPhaze"].Value
                };
                toSections.Add(section);
                Match eosMatch = eosRegex.Match(content, sta);
                if (eosMatch.Success)
                {
                    string[] secItems = content.Substring(sta, eosMatch.Index - sta).Split(
                        new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string curItem in secItems)
                    {
                        string[] pair = curItem.Split('=');
                        if (pair.Length == 2)
                        {
                            section.Items.Add(new SectionItem()
                            {
                                Name = pair[0].Trim(),
                                Value = pair[1].Trim()
                            });
                        }
                    }
                }
                else
                    throw new Exception(endBracket + " is not found.");
            }
        }

        private string Generate(params string[] excludeSections)
        {
            StringBuilder sb = new StringBuilder();

            // Header
            sb.AppendLine();
            sb.AppendLine(string.Format("Microsoft Visual Studio Solution File, Format Version {0}", FormatVersion));
            sb.AppendLine(string.Format("# Visual Studio {0}", StudioName));

            // Projects
            foreach (SolutionProjectRef proj in Projects)
            {
                sb.AppendLine(string.Format(@"Project(""{0}"") = ""{1}"", ""{2}"", ""{3}""",
                    proj.TypeGuid, proj.Name, proj.Path, proj.ProjectGuid));
                //if (!string.IsNullOrEmpty(proj.InnerText))
                //    sb.AppendLine(proj.InnerText);
                GenerateSections(SectionTypes.Project, proj.ProjectSections, sb, 1);
                sb.AppendLine("EndProject");
            }

            // Sections
            sb.AppendLine("Global");
            GenerateSections(SectionTypes.Global, Sections, sb, 1, excludeSections);
#if false // moved to GenerateSections
            foreach (SolutionSection sect in Sections)
            {
                if (excludeSections == null || excludeSections.Length == 0 ||
                    !excludeSections.Contains(sect.Name, StringComparer.OrdinalIgnoreCase))
                {
                    sb.AppendLine(string.Format("\tGlobalSection({0}) = {1}", sect.Name, sect.Phase));
                    foreach (SectionItem item in sect.Items)
                    {
                        sb.AppendLine(string.Format("\t\t{0} = {1}", item.Name, item.Value));
                    }
                    sb.AppendLine("\tEndGlobalSection");
                }
            }
#endif
            sb.AppendLine("EndGlobal");

            return sb.ToString();
        }

        private static void GenerateSections(SectionTypes sectionType, SolutionSections sections, StringBuilder sb,
            int indent, params string[] excludeSections)
        {
            if (sections.Count == 0)
                return;
            string startBracket, endBracket;
            GetSectionBrackets(sectionType, out startBracket, out endBracket);

            string endSectionLine = new string('\t', indent) + endBracket;

            foreach (SolutionSection sect in sections)
            {
                if (excludeSections == null || excludeSections.Length == 0 ||
                    !excludeSections.Contains(sect.Name, StringComparer.OrdinalIgnoreCase))
                {
                    sb.AppendLine(string.Format("{0}{1}({2}) = {3}", 
                        new string('\t', indent), startBracket, sect.Name, sect.Phase));
                    foreach (SectionItem item in sect.Items)
                    {
                        sb.AppendLine(string.Format("{0}{1} = {2}", new string('\t', indent + 1), item.Name, item.Value));
                    }
                    sb.AppendLine(endSectionLine);
                }
            }
        }

        private static void GetSectionBrackets(SectionTypes sectionType, out string startBracket, out string endBracket)
        {
            switch (sectionType)
            {
                case SectionTypes.Global:
                    startBracket = "GlobalSection";
                    endBracket = "EndGlobalSection";
                    break;
                case SectionTypes.Project:
                    startBracket = "ProjectSection";
                    endBracket = "EndProjectSection";
                    break;
                default:
                    throw new NotSupportedException("Unknown section type " + sectionType.ToString());
            }
        }
    }

    class SolutionProjectRef
    {
        private readonly SolutionSections _projectSections = new SolutionSections();

        internal string TypeGuid { get; set; }
        internal string Name { get; set; }
        internal string Path { get; set; }
        internal string ProjectGuid { get; set; }
        //internal string InnerText { get; set; }
        internal SolutionSections ProjectSections
        {
            get { return _projectSections; }
        }

        public override string ToString()
        {
            return Path ?? "";
        }
    }

    class SolutionProjects : Collection<SolutionProjectRef>
    {
        internal SolutionProjectRef FindByProjectGuid(string projGuid)
        {
            return this.FirstOrDefault(pRef => string.Compare(pRef.ProjectGuid, projGuid, true) == 0);
        }
    }

    class SolutionSection
    {
        private readonly SectionItems _items = new SectionItems();
        
        internal SectionItems Items
        {
            get { return _items; }
        }
        internal string Name { get; set; }
        internal string Phase { get; set; }
    }

    class SolutionSections: Collection<SolutionSection>
    {
        internal SolutionSection FindByName(string sectionName)
        {
            int idx = IndexOfName(sectionName);
            return idx >= 0 ? this[idx] : null;
        }

        internal int IndexOfName(string sectionName)
        {
            for (int i = 0; i < Count; i++)
            {
                if (sectionName == this[i].Name)
                    return i;
            }

            return -1;
        }

        internal SolutionSection EnsureSection(string name, string phase)
        {
            SolutionSection ret = FindByName(name);
            if (ret == null)
            {
                ret = new SolutionSection()
                {
                    Name = name,
                    Phase = phase
                };
                Add(ret);
            }

            return ret;
        }
    }

    class SectionItem
    {
        internal string Name { get; set; }
        internal string Value { get; set; }
    }

    class SectionItems : Collection<SectionItem>
    {
    }
}
