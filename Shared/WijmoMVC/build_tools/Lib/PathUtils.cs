﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace GeneratorUtils
{
    static class PathUtils
    {
        public class PathEqualityComparer : EqualityComparer<string>
        {
            public static readonly PathEqualityComparer Default = new PathEqualityComparer();
            
            public override bool Equals(string x, string y)
            {
                return PathsEqual(x, y);
            }

            public override int GetHashCode(string obj)
            {
                return MakeWinDirPath(obj).ToUpper().GetHashCode();
            }
        }
        
        public static string GetAbsWinPath(string absPath, string relPath)
        {
            return (new Uri(new Uri(MakeWinDirPath(absPath)), MakeWinDirPath(relPath))).OriginalString;
        }

        public static string GetAbsWinPath(string relPath)
        {
            return GetAbsWinPath(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), relPath);
        }

        public static string GetAbsWinFilePath(string absPath, string relPath)
        {
            Uri relUri = new Uri(relPath, UriKind.RelativeOrAbsolute);
            string ret = relUri.IsAbsoluteUri ? relPath :
                (new Uri(new Uri(MakeWinDirPath(absPath)), NormalizeWinPath(relPath))).OriginalString;
            return ret;
        }

        public static string GetAbsWinFilePath(string relPath)
        {
            return GetAbsWinFilePath(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), relPath);
        }

        /// <summary>
        /// Returns pathToRelate relative to baseDir
        /// </summary>
        /// <param name="baseDir"></param>
        /// <param name="pathToRelate"></param>
        /// <returns></returns>
        public static string GetRelativeFilePath(string baseDir, string pathToRelate)
        {
            baseDir = MakeWinDirPath(baseDir);
            pathToRelate = NormalizeWinPath(pathToRelate);
            if (PathEqualityComparer.Default.Equals(baseDir, pathToRelate))
                return ".";
            Uri pathToRelUri = new Uri(pathToRelate);
            Uri retUri = (new Uri(baseDir)).MakeRelativeUri(pathToRelUri);
            string ret = retUri.OriginalString;
            ret = Uri.UnescapeDataString(ret); // MakeRelativeUri returns escaped result
            return ret;
            //return (new Uri(baseDir)).MakeRelativeUri(new Uri(pathToRelate)).OriginalString;
        }


        public static string NormalizeWinPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return path;
            return path.Replace("/", @"\");
        }

        public static string NormalizeWebPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return path;
            return path.Replace(@"\", "/");
        }

        public static string MakeWinDirPath(string path)
        {
            const string pathDelimiter = @"\";
            if (string.IsNullOrEmpty(path))
                return "";
            path = NormalizeWinPath(path);
            if (!path.EndsWith(pathDelimiter))
                path += pathDelimiter;
            return path;
        }

        public static string EnsurePathRooted(string path)
        {
            if (string.IsNullOrEmpty(path))
                return "";
            if (!Path.IsPathRooted(path))
                return @"\" + NormalizeWinPath(path);
            return path;
        }

        public static bool PathsEqual(string path1, string path2)
        {
            if (path1 == null || path2 == null)
                return string.IsNullOrEmpty(path1) == string.IsNullOrEmpty(path2);
            return string.Compare(MakeWinDirPath(path1), MakeWinDirPath(path2), true) == 0;
        }

        public static string ChangeExtension(string fileName, string newExtension)
        {
            if (string.IsNullOrEmpty(fileName))
                return fileName;
            string ext = Path.GetExtension(fileName);
            string woExt = ext.Length > 0 ? fileName.Substring(0, fileName.Length - ext.Length) : fileName;
            return woExt + "." + newExtension;
        }

        /// <summary>
        /// Copies directories, possibly recursively. Overwrites target files.
        /// Basic implementation is obtained here: http://msdn.microsoft.com/en-us/library/bb762914.aspx
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        /// <param name="copySubDirs"></param>
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        public static bool RemoveReadonly(string filePath)
        {
            //FileInfo fi = new FileInfo(filePath);
            //if (fi.Exists)
            //    fi.Attributes = fi.Attributes & ~FileAttributes.ReadOnly;
            return ChangeFileAttribute(filePath, FileAttributes.ReadOnly, false);

        }

        /// <summary>
        /// For the specified file sets or clears (as specified by the <paramref name="set"/> parameter) the specified attribute(s).
        /// Returns a value indicating whether any attribute was actually changed.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="attr"></param>
        /// <param name="set"></param>
        /// <returns></returns>
        public static bool ChangeFileAttribute(string filePath, FileAttributes attr, bool set)
        {
            FileInfo fi = new FileInfo(filePath);
            if (!fi.Exists)
                return false;
            FileAttributes oldAttributes = fi.Attributes;
            if (set)
                fi.Attributes |= attr;
            else
                fi.Attributes &= ~attr;
            return oldAttributes != fi.Attributes;
        }

        /// <summary>
        /// Saves the content to the specified file even the file is read-only, keeping the read-only status.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="content"></param>
        /// <param name="encoding"></param>
        public static void SaveFileSafe(string file, string content, Encoding encoding)
        {
            bool isReadonly = PathUtils.ChangeFileAttribute(file, FileAttributes.ReadOnly, false);
            File.WriteAllText(file, content, encoding);
            if (isReadonly)
                PathUtils.ChangeFileAttribute(file, FileAttributes.ReadOnly, true);
        }
    }
}
