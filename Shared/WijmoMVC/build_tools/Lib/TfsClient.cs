﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace GeneratorUtils
{
    class TfsClient
    {
        private const int StreamBufferSize = 4096;
        
        public string _tfexe = ""; //turned off by default
        string _lastError = null;
        private bool _doDeletes = false;

        public bool AllowTfs
        {
            get { return _tfexe != ""; }
            set
            {
                if (value == AllowTfs)
                    return;
                if (value)
                {
                    if (string.IsNullOrEmpty(_tfexe))
                        _tfexe = null;
                }
                else
                    _tfexe = "";
            }
        }

        public bool RunTfAdd(string fpath)
        {
            return RunTfExe(new string[] { "add", fpath });
        }

        public bool RunTfDelete(string fpath)
        {
            return RunTfExe(new string[] { "delete", fpath });
        }

        public bool RunTfCheckout(string fpath)
        {
            return RunTfExe(new string[] { "checkout", fpath });
        }

        public bool RunTfExe(string[] args)
        {
            Init();
            if (_tfexe == null || _tfexe.Length == 0)
            {
                if (_doDeletes && args != null && args.Length >= 2 && args[0] == "delete")
                {
                    for (int ia = 1; ia < args.Length; ia++)
                    {
                        FileInfo fi = new FileInfo(args[ia]);
                        if (fi.Exists)
                        {
                            if (fi.IsReadOnly)
                            {
                                FileAttributes fa = fi.Attributes;
                                fa &= ~FileAttributes.ReadOnly;
                                fi.Attributes = fa;
                            }
                            fi.Delete();
                        }
                    }
                }

                return false;
            }

            StringBuilder sb = new StringBuilder();
            foreach (string arg in args)
            {
                if (sb.Length > 0)
                    sb.Append(' ');
                sb.Append('"');
                sb.Append(arg);
                sb.Append('"');
            }

            Process proc = new Process();
            ProcessStartInfo psi = new ProcessStartInfo(_tfexe, sb.ToString());
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = true;
            proc.StartInfo = psi;
            proc.Start();

            Console.Write(proc.StandardOutput.ReadToEnd());

            proc.WaitForExit();
            return proc.ExitCode == 0;
        }

        public void SaveFileSmart(string path, string content)
        {
            SaveFileSmart(path, content, false);
        }

        public void SaveFileSmart(string path, string content, bool forceTfsUpdate)
        {
            SaveFileSmart(path, content, forceTfsUpdate, Encoding.UTF8);
        }

        public void SaveFileSmart(string path, string content, bool forceTfsUpdate, Encoding encoding)
        {
            bool fileExists = File.Exists(path);
            if (fileExists && !forceTfsUpdate)
            {
                string curContent = File.ReadAllText(path);
                if (curContent == content)
                    return;
            }
            Init();
            if (fileExists)
            {
                if (!RunTfCheckout(path))
                    RunTfAdd(path);
            }
            File.WriteAllText(path, content, encoding);
            if (!fileExists)
                RunTfAdd(path);
        }

        public void SaveStreamSmart(string path, Stream stream, bool forceTfsUpdate)
        {
            bool fileExists = File.Exists(path);
            {
                if (fileExists && !forceTfsUpdate)
                {
                    using (FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read))
                    {
                        if (StreamsEqual(stream, fs))
                            return;
                    }
                }
                Init();
                if (fileExists)
                {
                    if (!RunTfCheckout(path))
                        RunTfAdd(path);
                }
                using (FileStream fs = File.Open(path, FileMode.Create, FileAccess.Write))
                {
                    CopyStreams(stream, fs);
                }
            }
            if (!fileExists)
                RunTfAdd(path);
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int memcmp(byte[] b1, byte[] b2, UIntPtr count);

        private static bool StreamsEqual(Stream stream1, Stream stream2)
        {
            if (stream1.Length != stream2.Length)
                return false;
            const int bufferSize = StreamBufferSize;
            var buffer1 = new byte[bufferSize];
            var buffer2 = new byte[bufferSize];
            long pos1 = stream1.Position;
            long pos2 = stream2.Position;
            stream1.Position = 0;
            stream2.Position = 0;

            try
            {
                while (true)
                {
                    int count1 = stream1.Read(buffer1, 0, bufferSize);
                    int count2 = stream2.Read(buffer2, 0, bufferSize);

                    if (count1 != count2)
                    {
                        return false;
                    }

                    if (count1 == 0)
                    {
                        return true;
                    }

                    if (memcmp(buffer1, buffer2, new UIntPtr((uint)count1)) != 0)
                        return false;
                    //int iterations = (int)Math.Ceiling((double)count1 / sizeof(int));
                    //for (int i = 0; i < iterations; i++)
                    //{
                    //    if (BitConverter.ToInt32(buffer1, i * sizeof(int)) != BitConverter.ToInt32(buffer2, i * sizeof(int)))
                    //    {
                    //        return false;
                    //    }
                    //}
                }
            }
            finally
            {
                stream1.Position = pos1;
                stream2.Position = pos2;
            }
        }

        private void CopyStreams(Stream sourceStream, Stream targetStream)
        {
            const int bufferSize = StreamBufferSize;
            byte[] buffer = new byte[bufferSize];
            sourceStream.Position = 0;
            targetStream.SetLength(0);
            targetStream.Position = 0;
            while (true)
            {
                int count = sourceStream.Read(buffer, 0, bufferSize);
                if (count <= 0)
                    return;
                targetStream.Write(buffer, 0, count);
            }
        }


        private void Init()
        {
            _lastError = null;

            if (_tfexe == null)
            {
                string[] possibleKeyList = new string[] 
                {
                    @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\10.0",
                    @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\11.0",
                    @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\VisualStudio\12.0"
                };
                foreach (string curKey in possibleKeyList)
                {
                    string key = curKey;
                    if (IntPtr.Size == 8) key = key.Replace(@"SOFTWARE", @"SOFTWARE\Wow6432Node");

                    _tfexe = Microsoft.Win32.Registry.GetValue(key, "InstallDir", "") as string;
                    if (!string.IsNullOrEmpty(_tfexe))
                        break;
            
                }

                if (_tfexe != null && _tfexe.Length > 0)
                {
                    _tfexe = Path.Combine(_tfexe, "tf.exe");
                    if (_tfexe == null || _tfexe.Length == 0 || !File.Exists(_tfexe))
                        _tfexe = "";
                }

                if (_tfexe == null || _tfexe.Length == 0)
                {
                    _tfexe = "";
                    _lastError = "TF.EXE cannot be found.  TFS operations will not be performed";
                    Console.WriteLine(_lastError);
                    Console.WriteLine("-- Press Enter to Continue, Ctrl-C to terminate. --");
                    Console.ReadLine();

                    Console.WriteLine();
                }
            }

        }

    }
}
