﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace GeneratorUtils
{
    /// <summary>
    /// Represents a conditional compilation preprocessor for JS/TS code.
    /// Syntax:
    /// ///#IF [!]SYMBOL
    /// // code
    /// [///#ELSEIF [!]SYMBOL]
    /// // code
    /// [///#ELSE]
    /// // code
    /// ///#ENDIF
    /// , where [] means an optional constructions, SYMBOL means a define symbol and ! means logical NOT operation.
    /// 
    /// #IF directives can be nested.
    /// 
    /// All directive lines, as well as code lines of branches evaluated to false are removed from code. 
    /// For code lines of a branch evaluated to true the first two leading backslashes (and only the ones) are removed from code, so
    /// a line of code should look as
    /// //var n = 1;
    /// and a line with a line comment may look as
    /// //// This is a comment (4 backslashes, 2 ones will be removed).
    /// 
    /// Directive names and symbols are case insensitive.
    /// 
    /// Directive line may contain ending single-line comment, e.g.:
    /// ///#IF EVAL // evaluation only block
    /// alert('Evaluation');
    /// ///#ENDIF // end of evaluation only block
    /// </summary>
    public class ConditionalPreprocessor
    {
        private enum Directive
        {
            If,
            Else,
            ElseIf,
            Endif
        }

        private static readonly Regex _directiveRx = new Regex(@"^\s*///\s*#(?<dir>\w+)\s*(?<cond>.*)$",
            RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static readonly Regex _conditionRx = new Regex(@"^(?<not>!)?\s*(?<def>.+)$",
            RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static readonly Regex _lineCommentRx = new Regex(@"^\s*(?<comment>//)?",
            RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private readonly Stack<PreprocessorContext> _context = new Stack<PreprocessorContext>();
        private readonly HashSet<string> _defines = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        public string Define { get; set; }

        public string Process(string content)
        {
            // Cleanup
            _context.Clear();
            _defines.Clear();

            if (string.IsNullOrEmpty(content))
                return content;
            if (!string.IsNullOrWhiteSpace(Define))
            {
                string[] defArr = Define.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string curDef in defArr)
                {
                    string stripDef = curDef.Trim();
                    if (stripDef.Length > 0)
                        _defines.Add(stripDef);
                }
            }
            bool changed = false;
            StringBuilder sb = new StringBuilder(content.Length);
            using (StringReader sr = new StringReader(content))
            {
                string curLine;
                while ((curLine = sr.ReadLine()) != null)
                {
                    string outputLine = ProcessLine(curLine);
                    if (outputLine != null)
                        sb.AppendLine(outputLine);
                    else
                        changed = true;
                }
            }
            if (_context.Count > 0)
                StopOnError("#ENDIF not found");

            return changed ? sb.ToString() : content;
        }

        private string ProcessLine(string line)
        {
            bool outOfDirective = _context.Count == 0;
            bool isOutput = outOfDirective || _context.Peek().IsOutput;
            Directive directive; 
            string condition;
            if (CheckDirective(line, out directive, out condition))
            {
                if (directive == Directive.If)
                {
                    PreprocessorContext newCtx = new PreprocessorContext(isOutput);
                    _context.Push(newCtx);
                    if (isOutput)
                        newCtx.IsOutput = EvalCondition(condition);
                    // process directive, check sematics, evaluate, change context IsOutput
                }
                else
                {
                    if (outOfDirective)
                        StopOnError("#If not found");
                    PreprocessorContext curCtx = _context.Peek();
                    switch (directive)
                    {
                        case Directive.ElseIf:
                            curCtx.IsOutput = curCtx.CanOutput && !curCtx.HadOutput && EvalCondition(condition);
                            break;
                        case Directive.Else:
                            if (curCtx.MetElse)
                                StopOnError("Multiple #ELSE clauses.");
                            curCtx.MetElse = true;
                            curCtx.IsOutput = curCtx.CanOutput && !curCtx.HadOutput;
                            break;
                        case Directive.Endif:
                            _context.Pop();
                            break;
                        default:
                            throw new NotSupportedException();

                    }
                }

                return null;
            }
            else
            {
                if (outOfDirective)
                    return line;
                else
                    return isOutput ? StripDirectiveCode(line) : null;
            }


        }

        private bool CheckDirective(string line, out Directive directive, out string condition)
        {
            directive = Directive.If;
            condition = null;
            Match m = _directiveRx.Match(line);
            if (!m.Success)
                return false;
            condition = m.Groups["cond"].Value.Trim();
            int commIdx = condition.IndexOf("//");
            if (commIdx >= 0)
                condition = condition.Remove(commIdx).Trim();
            switch (m.Groups["dir"].Value.ToUpper())
            {
                case "IF":
                    directive = Directive.If;
                    if (condition.Length == 0)
                        StopOnError("#IF clause requires a condition.");
                    break;
                case "ELSE":
                    directive = Directive.Else;
                    if (condition.Length > 0)
                        StopOnError("Unknown text in #ELSE clause.");
                    break;
                case "ELSEIF":
                    directive = Directive.ElseIf;
                    if (condition.Length == 0)
                        StopOnError("#ELSEIF clause requires a condition.");
                    break;
                case "ENDIF":
                    directive = Directive.Endif;
                    if (condition.Length > 0)
                        StopOnError("Unknown text in #ENDIF clause.");
                    break;
                default:
                    StopOnError(string.Format("Unknown conditional compilation clause #{0}", m.Groups["dir"].Value));
                    break;
            }

            return true;
        }

        private bool EvalCondition(string condition)
        {
            Match m = _conditionRx.Match(condition);
            if (!m.Success)
                StopOnError(string.Format("Unrecognized conditional directive expression: '{0}'", condition));
            bool isTrue = _defines.Contains(m.Groups["def"].Value);
            if (m.Groups["not"].Success)
                isTrue = !isTrue;
            return isTrue;
        }

        private string StripDirectiveCode(string line)
        {
            Match m = _lineCommentRx.Match(line);
            if (m.Success)
            {
                Group g = m.Groups["comment"];
                if (g.Success)
                    return line.Remove(g.Index, g.Length);
            }

            return line;
        }

        private void StopOnError(string message)
        {
            Console.WriteLine(message);
            throw new Exception(message);
        }
    }

    class PreprocessorContext
    {
        private bool _isOutput = false;
        public PreprocessorContext(bool parentIsOutput)
        {
            CanOutput = parentIsOutput;
        }

        public bool CanOutput { get; private set; }
        /// <summary>
        /// Indicates whether current condition is true.
        /// </summary>
        public bool IsOutput 
        {
            get { return _isOutput; }
            set
            {
                _isOutput = value;
                HadOutput |= value;
            }
        }
        public bool MetElse  { get; set; }
        public bool HadOutput { get; private set; }
    }

}
