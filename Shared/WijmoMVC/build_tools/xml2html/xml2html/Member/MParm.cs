﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

namespace xml2html
{
    public class MParm : Member
    {
        public MParm(Module module, XmlNode node)
            : base(module, node)
        {
            Type = GetAttValue(node, "type");
            DefVal = GetAttValue(node, "defVal");
            Optional = GetAttValue(node, "optional").ToLower() == "true";

            // try getting the parameter type from DefVal
            if (string.IsNullOrEmpty(Type) && !string.IsNullOrEmpty(DefVal))
            {
                var index = DefVal.LastIndexOf('.');
                if (index > -1)
                {
                    Type = DefVal.Substring(0, index);
                }
            }
        }
        public string DefVal { get; set; }
        public override string Url
        {
            get { return null; }
        }
    }
}
