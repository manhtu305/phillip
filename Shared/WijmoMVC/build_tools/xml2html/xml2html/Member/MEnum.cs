﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

namespace xml2html
{
    public class MEnum : Member
    {
        public MEnum(Module module, XmlNode node)
            : base(module, node)
        {
            // values
        }

        public void CreateDocs(string outDir)
        {
            var fn = Path.Combine(outDir, Url);
            using (var sw = Program.OpenStreamWriter(fn))
            {
                OutputHeaderPanel(sw);
                sw.WriteLine("<div class=\"member-panel enum\">");
                OutputEnumTable(sw);
                sw.WriteLine("</div>");
            }
        }
        public void OutputEnumTable(StreamWriter sw)
        {
            // heading
            sw.WriteLine("<h3>{0}</h3>", Localization.GetString("Members"));

            // start table
            sw.WriteLine("<table class=\"table table-condensed\">" +
                "<thead>" +
                "  <tr>" +
                "    <th>{0}</th>" +
                "    <th>{1}</th>" +
                "    <th>{2}</th>" +
                "  </tr>" +
                "</thead>" +
                "<tbody>",
                Localization.GetString("Name"),
                Localization.GetString("Value"),
                Localization.GetString("Description"));

            // table body
            foreach (MParm p in Params)
            {
                sw.WriteLine("<tr>" +
                    "  <td><span class=\"identifier\">{0}</span></td>" +
                    "  <td>{1}</td>" +
                    "  <td>{2}</td>" +
                    "<tr>",
                    p.Name,
                    p.DefVal,
                    ResolveComment(p.Comment)
                );
            }

            // end table
            sw.WriteLine("</tbody></table>");
        }
    }
}
