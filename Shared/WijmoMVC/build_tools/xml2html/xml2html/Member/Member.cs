﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace xml2html
{
    /// <summary>
    /// Base class for Classes, Enums, Interfaces, Methods, etc.
    /// </summary>
    public class Member : IComparable
    {
        List<MParm> _parms = new List<MParm>();

        public Member(Module module, XmlNode node)
        {
            Module = module;
            DefinitionNode = node;
            Name = GetAttValue(node, "name");
            Class = GetAttValue(node, "class");
            Kind = GetAttValue(node, "kind");
            Type = GetAttValue(node, "type");
            Optional = GetAttValue(node, "optional").ToLower() == "true";
            FileName = GetAttValue(node, "file");
            Static = GetAttValue(node, "static").ToLower() == "true";
            Comment = Module.FormatComment(GetNodeValue(node, "comment"));
            foreach (XmlNode ndParm in node.SelectNodes("parms/parm"))
            {
                Params.Add(new MParm(module, ndParm));
            }
        }
        public Member(Member owner, XmlNode node)
            : this(owner.Module, node)
        {
            OwnerMember = owner;
        }

        public Module Module { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Comment { get; set; }
        public string Kind { get; set; }
        public string Type { get; set; }
        public bool Optional { get; set; }
        public string FileName { get; set; }
        public bool Static { get; set; }
        public virtual bool EventRaiser { get; }
        public XmlNode DefinitionNode { get; set; }
        public Member OwnerMember { get; set; }
        public List<MParm> Params { get { return _parms; } }

        public string GetNodeValue(XmlNode node, string name)
        {
            var nd = node.SelectSingleNode(name);
            return nd != null
                ? nd.InnerText
                : GetAttValue(node, name);
        }
        public string GetAttValue(XmlNode node, string name)
        {
            var att = node.Attributes[name];
            return att != null ? att.Value : string.Empty;
        }
        public string FullName
        {
            get { return string.Format("{0}.{1}", Module.Name, Name); }
        }
        public virtual string Url
        {
            get { return string.Format("{0}.{1}.{2}.html", Module.EscapeForUrl(Module.Name), Name, Kind); }
        }
        public string ResolveString(string text)
        {
            return Module.ResolveString(text, this);
        }
        public string ResolveString(IEnumerable<MClass> classes)
        {
            var list = new List<string>();
            foreach (var c in classes)
            {
                list.Add(ResolveString(c.FullName));
            }
            return string.Join(", ", list.ToArray());
        }
        public string ResolveComment(string comment)
        {
            return Module.ResolveComment(comment, this);
        }
        public int CompareTo(object obj)
        {
            return Name.CompareTo(((Member)obj).Name);
        }

        public virtual void OutputHeaderPanel(StreamWriter sw)
        {
            // header
            sw.WriteLine("<div class=\"header-panel member\">");
            sw.WriteLine("  <h2><span class=\"identifier\">{0}</span> {1}</h2>",
                Name,
                Localization.GetString(Kind));
            sw.WriteLine("  <dl class=\"dl-horizontal\">");
            sw.WriteLine("    <dt>{0}</dt><dd class=\"filename\">{1}</dd>",
                Localization.GetString("File"),
                FileName);
            sw.WriteLine("    <dt>{0}</dt><dd>{1}</dd>", 
                Localization.GetString("Module"), 
                ResolveString(Module.Name));
            sw.WriteLine("  </dl>");
            sw.WriteLine("  <div class=\"comment\">{0}</div>", 
                ResolveComment(Comment));
            sw.WriteLine("</div>");
        }
        public bool OpenMemberList(StreamWriter sw, string title, IEnumerable<Member> list)
        {
            return Module.OpenMemberList(sw, title, this, list);
        }
        public void OutputMemberTable(StreamWriter sw, string title, IEnumerable<Member> items)
        {
            Module.OutputMemberTable(sw, title, this, items);
        }
        public void OutputMember(StreamWriter sw, Member m)
        {
            Module.OutputMember(sw, this, m);
        }
        public virtual void OutputDocs(StreamWriter sw, Member owner)
        {
            throw new Exception("OutputDocs should be called only for properties, events, or methods.");
        }
        public override string ToString()
        {
            return GetType().Name + " " + Name;
        }
    }
}
