﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

namespace xml2html
{
    public class MClass : Member
    {
        List<MMethod> _methods = new List<MMethod>();
        List<MProperty> _props = new List<MProperty>();
        List<MEvent> _evts = new List<MEvent>();
        List<string> _interfaces = new List<string>();
        MClass _baseClass = null;
        bool _checkedForBaseClass = false;

        public MClass(Module module, XmlNode node)
            : base(module, node)
        {
            // get base class
            BaseClass = GetAttValue(node, "base");

            // get implemented interfaces
            foreach (XmlNode nd in node.SelectNodes("implements/interface"))
            {
                _interfaces.Add(GetAttValue(nd, "name"));
            }

            // get properties/events/methods
            var props = Properties;
            var methods = Methods;
            var events = Events;
            var path = string.Format("product/members/member[@module='{0}' and @class='{1}']",
                node.Attributes["module"].Value,
                node.Attributes["class"].Value);
            foreach (XmlNode nd in node.OwnerDocument.SelectNodes(path))
            {
                var t = nd.Attributes["kind"];
                if (t != null)
                {
                    switch (t.Value)
                    {
                        case "Property":
                        case "Field":
                            props.Add(new MProperty(this, nd));
                            break;
                        case "Event":
                            events.Add(new MEvent(this, nd));
                            break;
                        case "Method":
                            methods.Add(new MMethod(this, nd));
                            break;
                        default:
                            Debug.Assert(
                                GetAttValue(nd, "name") == GetAttValue(nd, "class") &&
                                (GetAttValue(nd, "kind") == "Class" || GetAttValue(nd, "kind") == "Interface"));
                            break;
                    }
                }
            }

            // add inherited members
            for (var bc = getBaseClass(); bc != null; bc = bc.getBaseClass())
            {
                foreach (var p in bc.Properties)
                {
                    if (props.Find(p.Name) == null)
                        props.Add(new MProperty(this, p.DefinitionNode));
                }
                foreach (var m in bc.Methods)
                {
                    if (methods.Find(m.Name) == null)
                        methods.Add(new MMethod(this, m.DefinitionNode));
                }
                foreach (var e in bc.Events)
                {
                    if (events.Find(e.Name) == null)
                        events.Add(new MEvent(this, e.DefinitionNode));
                }
            }

            // sort members
            props.Sort();
            events.Sort();
            methods.Sort();
        }

        public string BaseClass { get; set; }
        public List<MProperty> Properties { get { return _props; } }
        public List<MMethod> Methods { get { return _methods; } }
        public List<MEvent> Events { get { return _evts; } }
        public MClass getBaseClass()
        {
            if (!_checkedForBaseClass && !string.IsNullOrEmpty(BaseClass))
            {
                _baseClass = Module.FindMember(BaseClass, true) as MClass;
            }
            _checkedForBaseClass = true;
            return _baseClass;
        }
        public List<MClass> getDerivedClasses()
        {
            var derived = new List<MClass>();
            foreach (var m in Program._modules.Values)
            {
                foreach (var c in m.Classes)
                {
                    if (c.getBaseClass() == this)
                    {
                        derived.Add(c);
                    }
                }
            }
            return derived;
        }
        public List<MClass> getInterfaces()
        {
            var interfaces = new List<MClass>();
            foreach (var s in _interfaces)
            {
                var i = Module.FindMember(s, true) as MClass;
                if (i != null)
                {
                    interfaces.Add(i);
                }
            }
            return interfaces;
        }

        // create documentation for this class
        public void CreateDocs(string outDir)
        {
            var fn = Path.Combine(outDir, Url);
            using (var sw = Program.OpenStreamWriter(fn))
            {
                // global member info
                // Name, File, Child Modules, comment
                OutputHeaderPanel(sw);

                // links to members
                sw.WriteLine("<div class=\"member-panel class members\" ng-class=\"{ 'show-inherited' : ctx.showInherited, 'show-raisers' : ctx.showRaisers }\">");
                MMethod ctor = Methods.Find("constructor") as MMethod;
                if (ctor != null)
                {
                    OutputMemberTable(sw, "Constructor", new List<MMethod> { ctor });
                    Methods.Remove(ctor);
                }
                OutputMemberTable(sw, "Properties", Properties);
                OutputMemberTable(sw, "Methods", Methods);
                OutputMemberTable(sw, "Events", Events);
                sw.WriteLine("</div>");

                // member details
                if (ctor != null)
                {
                    sw.WriteLine("<h3>{0}</h3>", Localization.GetString("Constructor"));
                    OutputMember(sw, ctor);
                }
                if (OpenMemberList(sw, "Properties", Properties))
                {
                    foreach (var m in Properties)
                    {
                        OutputMember(sw, m);
                    }
                    sw.WriteLine("</div>");
                }
                if (OpenMemberList(sw, "Methods", Methods))
                {
                    foreach (var m in Methods)
                    {
                        OutputMember(sw, m);
                    }
                    sw.WriteLine("</div>");
                }
                if (OpenMemberList(sw, "Events", Events))
                {
                    foreach (var m in Events)
                    {
                        OutputMember(sw, m);
                    }
                    sw.WriteLine("</div>");
                }
            }
        }

        // add base/derived class information to header panel
        public override void OutputHeaderPanel(StreamWriter sw)
        {
            sw.WriteLine("<div class=\"header-panel member\">");
            sw.WriteLine("<h2><span class=\"identifier\">{0}</span> {1}</h2>", 
                Name, 
                Localization.GetString(Kind));

            sw.WriteLine("<dl class=\"dl-horizontal\">");

            // header
            sw.WriteLine("<dt>{0}</dt><dd class=\"filename\">{1}</dd>",
                Localization.GetString("File"),
                FileName);
            sw.WriteLine("<dt>{0}</dt><dd>{1}</dd>",
                Localization.GetString("Module"), 
                ResolveString(Module.Name));

            // base/derived classes/interfaces
            var derived = getDerivedClasses();
            var interfaces = getInterfaces();
            if (this.Module.FindMember(BaseClass, this) != null) //!string.IsNullOrEmpty(BaseClass))
            {
                sw.WriteLine("<dt>{0}</dt><dd>{1}</dd>", 
                    Localization.GetString("Base Class"), 
                    ResolveString(BaseClass));
            }
            if (derived.Count > 0)
            {
                sw.WriteLine("<dt>{0}</dt><dd>{1}</dd>", 
                    Localization.GetString("Derived Classes"), 
                    ResolveString(derived));
            }
            if (interfaces.Count > 0)
            {
                sw.WriteLine("<dt>{0}</dt><dd>{1}</dd>", 
                    Localization.GetString("Implements"),
                    ResolveString(interfaces));
            }
            if (!string.IsNullOrEmpty(BaseClass))
            {
                sw.WriteLine("<dt>{0}</dt>",
                    Localization.GetString("Show"));
                sw.WriteLine("<dd>");
                sw.WriteLine("<label><input type=\"checkbox\" ng-model=\"ctx.showInherited\"> {0}</label>&nbsp;&nbsp;&nbsp;",
                    Localization.GetString("Inherited Members"));
                sw.WriteLine("<label><input type=\"checkbox\" ng-model=\"ctx.showRaisers\"> {0}</label>",
                    Localization.GetString("Event Raisers"));
                sw.WriteLine("</dd>");
            }
            sw.WriteLine("</dl>");

            // documentation body
            sw.WriteLine("<div class=\"comment\">{0}</div>", 
                ResolveComment(Comment));
            sw.WriteLine("</div>");
        }
    }
}
