﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

namespace xml2html
{
    public class MProperty : Member
    {
        public MProperty(Module module, XmlNode node)
            : base(module, node)
        {
        }
        public MProperty(Member owner, XmlNode node)
            : base(owner.Module, node)
        {
            OwnerMember = owner;
        }
        public override void OutputDocs(StreamWriter sw, Member owner)
        {
            sw.WriteLine("<div class=\"comment\">{0}</div>", 
                ResolveComment(Comment));
            sw.WriteLine("<dl class=\"dl-horizontal\">");
            if (owner != null && owner.Class != Class)
            {
                sw.WriteLine("  <dt>{0}</dt><dd>{1}</dd>", 
                    Localization.GetString("Inherited From"), 
                    ResolveString(Class));
            }
            sw.WriteLine("  <dt>{0}</dt><dd>{1}</dd>", 
                Localization.GetString("Type"),
                ResolveString(Type));
            sw.WriteLine("</dl>");
        }
        public override string Url
        {
            get
            {
                var url = OwnerMember != null ? OwnerMember.Url : Module.Url;
                return url + "/#" + Name;
            }
        }
    }
}
