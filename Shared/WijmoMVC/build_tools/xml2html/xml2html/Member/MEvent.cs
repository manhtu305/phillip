﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

namespace xml2html
{
    public class MEvent : Member
    {
        public MEvent(Module module, XmlNode node)
            : base(module, node)
        {
        }
        public MEvent(Member owner, XmlNode node)
            : base(owner.Module, node)
        {
            OwnerMember = owner;
        }
        public override void OutputDocs(StreamWriter sw, Member owner)
        {
            sw.WriteLine("<div class=\"comment\">{0}</div>", 
                ResolveComment(Comment));
            sw.WriteLine("<dl class=\"dl-horizontal\">");

            // inherited from
            if (owner != null && owner.Class != Class)
            {
                sw.WriteLine("  <dt>{0}</dt><dd>{1}</dd>",
                    Localization.GetString("Inherited From"),
                    ResolveString(Class));
            }

            // parameter types
            var raiser = this.Module.FindMember("on" + char.ToUpper(this.Name[0]) + this.Name.Substring(1), OwnerMember);
            var argType = raiser != null && raiser.Params.Count == 1
                ? raiser.Params[0].Type
                : "EventArgs";
            sw.WriteLine("  <dt>{0}</dt><dd>{1}</dd>", 
                Localization.GetString("Arguments"), 
                ResolveString(argType));

            sw.WriteLine("</dl>");
        }
        public override string Url
        {
            get
            {
                var url = OwnerMember != null ? OwnerMember.Url : Module.Url;
                return url + "/#" + Name;
            }
        }
    }
}
