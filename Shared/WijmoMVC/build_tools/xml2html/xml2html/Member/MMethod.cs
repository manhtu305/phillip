﻿using System.IO;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;

namespace xml2html
{
    public class MMethod : Member
    {
        public MMethod(Module module, XmlNode node)
            : base(module, node)
        {
            ReturnValue = GetNodeValue(node, "return");
            if (string.IsNullOrEmpty(Type))
            {
                Type = "void";
            }
        }
        public MMethod(Member owner, XmlNode node)
            : this(owner.Module, node)
        {
            OwnerMember = owner;
        }
        public string ReturnValue { get; set; }
        public override bool EventRaiser
        {
            get
            {
                if (Regex.IsMatch(Name, "^on[A-Z]") && Params.Count == 1)
                {
                    return Params[0].Type.EndsWith("EventArgs") ;
                }
                return base.EventRaiser;
            }
        }
        public string Signature
        {
            get
            {
                var sb = new StringBuilder();

                sb.AppendFormat("{0}(", Name);
                foreach (var p in Params)
                {
                    if (p != Params[0]) sb.Append(", ");
                    sb.AppendFormat("{0}{1}", p.Name, p.Optional ? "?" : string.Empty);
                    if (!string.IsNullOrEmpty(p.Type))
                    {
                        sb.AppendFormat(": {0}", ResolveString(p.Type));
                    }
                }
                sb.Append(")");
                if (!string.IsNullOrEmpty(Type))
                {
                    sb.AppendFormat(": {0}", ResolveString(Type));
                }
                return "<pre>" + sb.ToString() + "</pre>";
            }
        }
        public override void OutputDocs(StreamWriter sw, Member owner)
        {
            // event signature (with links)
            sw.WriteLine(Signature);

            // comments
            sw.WriteLine("<div class=\"comment\">{0}</div>", 
                ResolveComment(Comment));

            // parameters
            if (this.Params.Count > 0)
            {
                sw.WriteLine("<dl class=\"dl-horizontal\">");
                sw.WriteLine("  <dt>{0}</dt><dd></dd>", 
                    Localization.GetString("Parameters"));
                sw.WriteLine("</dl>");
                sw.WriteLine("<ul>");
                foreach (var p in Params)
                {
                    sw.Write("<li><b>{0}</b>: {1}{2}<br/>{3}</li>",
                        p.Name,
                        ResolveString(p.Type),
                        p.Optional ? " <span class=\"badge\">Optional</span>" : string.Empty,
                        ResolveComment(p.Comment));
                }
                sw.WriteLine("</ul>");
            }

            // inherited/return type
            sw.WriteLine("<dl class=\"dl-horizontal\">");
            if (OwnerMember != null && OwnerMember.Class != Class)
            {
                sw.WriteLine("  <dt>{0}</dt><dd>{1}</dd>",
                    Localization.GetString("Inherited From"),
                    ResolveString(Class));
            }
            sw.WriteLine("  <dt>{0}</dt><dd>{1}</dd>",
                Localization.GetString("Returns"),
                ResolveString(Type));
            sw.WriteLine("</dl>");
        }
        public override string Url
        {
            get
            {
                var url = OwnerMember != null ? OwnerMember.Url : Module.Url;
                return url + "/#" + Name;
            }
        }
    }
}
