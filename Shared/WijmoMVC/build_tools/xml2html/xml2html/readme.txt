﻿Wijmo Help 2.0

** Background
Wijmo's current on-line help has some limitations that make it hard to use and
less functional than comparable tools such as TypeDoc.

We decided to address that for our V2.5/2016 release.

** Goals
- Improve TOC-based navigation.
- Get rid of the massive and hard-to-read tables we use for class members.
- Improve topic formatting, expand use of CSS for more flexibility.
- Enable local links (links to properties/events/methods).
- Add syntax highlighting to sample code snippets.
- Embed fiddles to make samples more accessible (instead of using buttons).
- Improve diagnostics/validation of HTML comments/links.
- Keep the current URL's to leverage the current indexing, SEO, etc.
- Keep the ability to export the documentation to PDF.

** Status
- Finished changes to xml2html tool and WinHelp app
- Improved the help system substantially
- Goals listed above accomplished:
	Simpler TOC has modules only (and is manually maintained, with info about enterprise modules)
	No more tables, all divs
	Topics have more room and are formatted with CSS
	Links work with modules, classes/enums/interfaces, events/properties/methods.
	Using highlightjs to highlight code in <pre> blocks
	Replaced fiddle buttons with collapsible/embedded panels
	Fixed lots of issues in the tool and in the documentation comments
	No changes to URL's (no impact on indexing/SEO)
	** Need to check the export to PDF capability

** Next steps:
- Document syntax for @see:XXX links
- Keep working on the content. Many topics need to be revised/expanded
- Add more fiddles


-------
- revise toc: modules only? => no grid, no wijmo
- switch bootstrap template: http://getbootstrap.com/examples/dashboard
- test test test

> ready to show!!!
