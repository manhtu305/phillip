﻿using GeneratorUtils;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;

namespace xml2html
{
    class Program
    {
        const int ERROR = 1;
        public static Dictionary<string, Module> _modules = new Dictionary<string, Module>();
        public static string _version = string.Empty;

        static void Main(string[] args)
        {
            // benchmark
            var start = DateTime.Now;
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("xml2html - xml docs to html");
            Console.WriteLine("Copyright (c) ComponentOne");
            Console.WriteLine("-----------------------------------------------------");
            //args = new string[] { "C:\\TFS\\RhinoMain\\Main\\Wijmo5Mvc\\C1.Web.Mvc\\Client\\c1.tsdocx.xml", "/cfg:debug", "/culture:ja" };
            CommandArgs commandArgs = new CommandArgs(args);

            if (args.Length == 0 || commandArgs.GetParam("?", false, false) != null)
            {
                Console.WriteLine("command line parameters:");
                Console.WriteLine(">xml2html <path to wijmo.tsdocx.xml> \r\n          [/xml:<path to source xml dir>] \r\n          [/html:<path to output html dir>] \r\n          [/culture:<culture code>]");
                Console.WriteLine();
                Console.WriteLine("parameter descriptions:");
                Console.WriteLine("<path to wijmo.tsdocx.xml> - absolute or relative to the folder where xml2html.exe runs from.");
                Console.WriteLine();
                Console.WriteLine("/xml:<path to source xml dir> - optional, overrides XML files location defined in wijmo.tsdocx.xml; absolute or relative to the folder where wijmo.tsdocx.xml is located");
                Console.WriteLine();
                Console.WriteLine("/html:<path to output html dir> - optional, overrides HTML files location defined in wijmo.tsdocx.xml; absolute or relative to the folder where wijmo.tsdocx.xml is located");
                Console.WriteLine();
                Console.WriteLine("/culture:<culture code> - optional, specify the culture code, e.g. ja or ja-JP");
                return;
            }

            string prjFile = commandArgs.GetParam("", false, true);
            string xmlPath = commandArgs.GetParam("xml", false, true);
            string htmlPath = commandArgs.GetParam("html", false, true);
            if (string.IsNullOrWhiteSpace(prjFile))
            {
                Console.WriteLine();
                Console.WriteLine("Configuration file is not specified.");
                Environment.ExitCode = ERROR;
                return;
            }

            string culture = commandArgs.GetParam("culture", false, true);
            if (!string.IsNullOrEmpty(culture))
            {
                Localization.Culture = CultureInfo.GetCultureInfo(culture);
            }

            //-----------------------------------------------------------------------
            // get input/output paths

            IList<string> xmlFiles;
            if (!Path.IsPathRooted(prjFile))
            {
                prjFile = Path.Combine(Directory.GetCurrentDirectory(), prjFile);
            }
            if (!File.Exists(prjFile))
            {
                Console.WriteLine();
                Console.WriteLine("cannot find source file '" + args[0] + "'.");
                Environment.ExitCode = ERROR;
                return;
            }

            // load tsdocx file
            var tsdocx = new XmlDocument();
            tsdocx.Load(prjFile);

            var ndPrj = tsdocx.SelectSingleNode("Project");
            var inputPath = Path.GetDirectoryName(prjFile);
            var att = ndPrj.Attributes["InputPath"];
            if (att != null)
            {
                inputPath = Path.IsPathRooted(att.Value)
                    ? att.Value
                    : Path.Combine(inputPath, att.Value);
            }

            // /xml: command line option overrides XmlDocumentationPath setting in tsdocx.xml
            if (string.IsNullOrWhiteSpace(xmlPath))
            {
                att = ndPrj.Attributes["XmlDocumentationPath"];
                if (att != null)
                    xmlPath = att.Value;
            }
            if (!string.IsNullOrWhiteSpace(xmlPath))
            {
                xmlPath = Path.IsPathRooted(xmlPath)
                    ? xmlPath
                    : Path.Combine(inputPath, xmlPath);
            }

            // /html: command line option overrides DocumentationPath setting in tsdocx.xml
            if (string.IsNullOrWhiteSpace(htmlPath))
            {
                att = ndPrj.Attributes["DocumentationPath"];
                if (att != null)
                    htmlPath = att.Value;
            }
            if (!string.IsNullOrWhiteSpace(htmlPath))
            {
                htmlPath = Path.IsPathRooted(htmlPath)
                    ? htmlPath
                    : Path.Combine(inputPath, htmlPath);
            }

            // collect xml files
            xmlFiles = new List<string>();
            foreach (XmlNode node in tsdocx.SelectNodes("Project/Items/Item"))
            {
                var f = node.Attributes["File"].Value;
                if (Path.GetExtension(f).ToLower() == ".js")
                {
                    f = Path.ChangeExtension(f, ".xml");
                    f = Path.Combine(xmlPath, f);
                    xmlFiles.Add(f);
                }
            }

            //-----------------------------------------------------------------------
            // parse modules

            // parse modules
            foreach (string f in xmlFiles)
            {
                ParseModules(f);
            }

            //-----------------------------------------------------------------------
            // create topics

            if (!Directory.Exists(htmlPath))
            {
                Directory.CreateDirectory(htmlPath);
            }

            // create topics directory
            var topicsDir = Path.Combine(htmlPath, "topics");
            if (Directory.Exists(topicsDir))
            {
                try
                {
                    Directory.Delete(topicsDir, true);
                    while (Directory.Exists(topicsDir)) ;

                }
                catch
                {
                }
            }
            if (Directory.Exists(topicsDir))
            {
                throw new InvalidOperationException();
            }
            Directory.CreateDirectory(topicsDir);
            if (!Directory.Exists(topicsDir))
            {
                throw new InvalidOperationException();
            }

            // write out documentation
            foreach (Module m in _modules.Values)
            {
                m.CreateDocs(topicsDir);

                // write a topic for each class in the module
                m.Classes.Sort();
                foreach (var c in m.Classes)
                {
                    c.CreateDocs(topicsDir);
                }

                // write a topic for each interface in the module
                m.Interfaces.Sort();
                foreach (var i in m.Interfaces)
                {
                    i.CreateDocs(topicsDir);
                }

                // write a topic for each enum in the module
                m.Enums.Sort();
                foreach (var e in m.Enums)
                {
                    e.CreateDocs(topicsDir);
                }
            }

            //-----------------------------------------------------------------------
            // write out templates

            // create template directory
            var tplDir = Path.Combine(htmlPath, "templates");
            if (Directory.Exists(tplDir))
            {
                try
                {
                    Directory.Delete(tplDir, true);
                    while (Directory.Exists(tplDir)) ;
                }
                catch
                {
                }
            }
            Directory.CreateDirectory(tplDir);
            while (!Directory.Exists(tplDir)) ;

            // save version template
            Program._version = string.Empty;
            var fn = Path.Combine(tplDir, "version.html");
            foreach (var m in _modules.Values)
            {
                if (!string.IsNullOrEmpty(m.ProductVersion))
                {
                    if (!string.IsNullOrEmpty(Program._version) && Program._version != m.ProductVersion)
                    {
                        Console.WriteLine("Modules have different versions!");
                    }
                    Program._version = m.ProductVersion;
                }
            }
            if (!string.IsNullOrEmpty(Program._version))
            {
                using (var sw = OpenStreamWriter(fn))
                {
                    sw.WriteLine(Program._version);
                }
            }

            //-----------------------------------------------------------------------
            // write single output file (for PDF conversion)
            // TODO: - check why dl-horizontal is not working
            // var fnHtml = CreateMergedFile(htmlPath);
            //System.Diagnostics.Process.Start(fnHtml);

            //-----------------------------------------------------------------------
            // done
            Console.WriteLine();
            Console.WriteLine("xml2html done in {0:n0} ms.", DateTime.Now.Subtract(start).TotalMilliseconds);
            // Console.ReadKey();
        }

        static void ParseModules(string f)
        {
            // read xml file
            if (string.IsNullOrEmpty(Path.GetExtension(f)))
            {
                f = Path.ChangeExtension(f, "xml");
            }
            if (!File.Exists(f))
            {
                Console.WriteLine();
                Console.WriteLine("cannot find source file '" + f + "'.");
                return;
            }
            var doc = new XmlDocument();
            doc.Load(f);

            // get product version
            var ndVersion = doc.SelectSingleNode("product/version");
            var version = ndVersion != null
                ? ndVersion.Attributes["value"].Value
                : string.Empty;

            // create hierarchical structure of modules
            Module first = null;
            foreach (XmlNode nd in doc.SelectNodes("product/members/member"))
            {
                var module = nd.Attributes["module"];
                if (module != null && !_modules.ContainsKey(module.Value))
                {
                    var m = new Module(nd);
                    m.ProductVersion = version;
                    _modules[m.Name] = m;
                    first = m;
                }
            }
        }
        public static StreamWriter OpenStreamWriter(string path)
        {
            // make sure the file is not read-only
            if (File.Exists(path))
            {
                File.SetAttributes(path, FileAttributes.Normal);
            }
            return new StreamWriter(path);
        }

        // create a single HTML output file for conversion to PDF
        static string CreateMergedFile(string htmlPath)
        {
            // initialize html output
            //var helpPath = htmlPath.Substring(0, htmlPath.IndexOf("partials"));
            var helpPath = htmlPath;
            var topicsDir = Path.Combine(htmlPath, "topics");
            var fnHtml = Path.Combine(helpPath, "Wijmo5.html");
            using (var sw = new StreamWriter(fnHtml))
            {
                sw.WriteLine("<!DOCTYPE html>\r\n" +
                    "<html lang = \"en\" xmlns = \"http://www.w3.org/1999/xhtml\">\r\n" +
                    "<head>\r\n" +
                    "  <meta charset=\"utf-8\"/>\r\n" +
                    "    <title>Wijmo 5</title>\r\n" +
                    "    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\"/>\r\n" +
                    "    <link href=\"styles/app.css\" rel=\"stylesheet\"/>\r\n" +
                    "    <link href=\"styles/print.css\" rel=\"stylesheet\"/>\r\n" +
                    "  </head>\r\n" +
                    "  <body>");

                // Title page
                sw.WriteLine("<div class=\"doc-title\">\r\n" +
                    "  <h1>\r\n" +
                    "    <img src=\"images/wijmo.png\"/>\r\n" +
                    "    Wijmo 5</h1>\r\n" +
                    "  <p>\r\n" +
                    "    version " + Program._version + "</p>\r\n" +
                    "</div>");

                // TOC
                var fn = Path.Combine(helpPath, "static/toc.html");
                var toc = string.Empty;
                using (var srToc = new StreamReader(fn))
                {
                    toc = srToc.ReadToEnd();
                    toc = toc.Replace("class=\"node collapsed", "class=\"node");
                }
                sw.WriteLine("<div class=\"topic\">\r\n" +
                    "<h2>\r\n" +
                    "  Table of Contents</h2>\r\n" +
                    "<p></p>\r\n" +
                    "<div class=\"toc-c3\">\r\n" +
                    ResolveLinks(toc, null) +
                    "</div>");

                // static topics
                foreach (var line in toc.Split('\r', '\n'))
                {
                    var match = Regex.Match(line, "<a href=\"(static/.*)\"");
                    if (match.Success)
                    {
                        fn = Path.Combine(helpPath, match.Groups[1].Value);
                        sw.WriteLine(GetMergedTopic(fn));
                    }
                }

                // output reference docs
                foreach (var m in Program._modules.Values)
                {
                    // write module topic
                    fn = Path.Combine(topicsDir, m.Url);
                    sw.WriteLine(GetMergedTopic(fn));

                    // write class topics
                    foreach (var c in m.Classes)
                    {
                        fn = Path.Combine(topicsDir, c.Url);
                        sw.WriteLine(GetMergedTopic(fn));
                    }
                    foreach (var c in m.Interfaces)
                    {
                        fn = Path.Combine(topicsDir, c.Url);
                        sw.WriteLine(GetMergedTopic(fn));
                    }
                    foreach (var c in m.Enums)
                    {
                        fn = Path.Combine(topicsDir, c.Url);
                        sw.WriteLine(GetMergedTopic(fn));
                    }
                }

                // close HTML file
                sw.Write("</body>\r\n</html>");
                sw.Close();
            }
            return fnHtml;
        }
        static string GetMergedTopic(string fn)
        {
            // read the topic
            string topic;
            using (var sr = new StreamReader(fn))
            {
                topic = sr.ReadToEnd();
            }

            // remove show inherited/raiser checkboxes
            topic = Regex.Replace(topic, @"<dt>Show</dt>\s*<dd>.*?</dd>", string.Empty, RegexOptions.Singleline);

            // resolve links and anchors
            fn = Path.GetFileName(fn);
            topic = ResolveLinks(topic, fn);

            // replace embedded fiddles
            topic = Regex.Replace(topic, @"<script async src=""http://jsfiddle.net/Wijmo5/([^/]+)/embed/result,html,js,css""></script>", (Match m) =>
            {
                var a =
                    "<a href=\"http://jsfiddle.net/Wijmo5/" + m.Groups[1].Value + 
                        "\" target=\"_blank\" class=\"btn btn-info\">" +
                        "<img src=\"images/fiddle.png\"/>" +
                        " Show me" +
                    "</a>";
                return a;
            });

            // wrap the whole thing in a div with an ID
            topic = string.Format("<div id=\"{0}\" class=\"topic\">\r\n{1}\r\n</div>", fn, topic);

            // return the result
            return topic;
        }
        static string ResolveLinks(string topic, string fn)
        {
            // resolve internal links
            topic = Regex.Replace(topic, @"<a\s[^>]*href=""([^""]*)""[^>]*>(.*?)</a>", (Match m) =>
            {
                var target = m.Groups[1].Value;
                if (target.IndexOf('#') > -1 && fn != null)
                {
                    target = string.Format("#{0}_{1}", fn, target.Substring(target.IndexOf('#') + 1));
                }
                else if (target.StartsWith("static/") || target.StartsWith("topic/"))
                {
                    target = string.Format("#{0}", Path.GetFileName(m.Groups[1].Value));
                }
                else if (target.IndexOf("fiddle") < 0) // remove external links, they look bad in PDF
                {
                    return "<b>" + m.Groups[2].Value.Trim() + "</b>";
                }
                return m.Groups[0].Value.Replace(m.Groups[1].Value, target);
            });

            // resolve internal anchors
            topic = Regex.Replace(topic, @"<div\s[^>]*id=""([^""]*)""[^>]*>", (Match m) =>
            {
                var target = m.Groups[1].Value;
                if (fn != null)
                {
                    target = string.Format("{0}_{1}", fn, Path.GetFileName(m.Groups[1].Value));
                }
                return m.Groups[0].Value.Replace(m.Groups[1].Value, target);
            });

            // expand any collapsed nodes
            topic = topic.Replace("class=\"node collapsed\"", "class=\"node\"");

            // done
            return topic;
        }
    }
}
