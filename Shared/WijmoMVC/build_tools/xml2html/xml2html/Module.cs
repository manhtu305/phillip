﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Text.RegularExpressions;

namespace xml2html
{
    public class Module
    {
        List<MClass> _classes = new List<MClass>();
        List<MEnum> _enums = new List<MEnum>();
        List<MInterface> _interfaces = new List<MInterface>();
        List<MProperty> _properties = new List<MProperty>();
        List<MMethod> _methods = new List<MMethod>();
        Dictionary<string, Member> _memberCache = new Dictionary<string, Member>();

        public Module(XmlNode node)
        {
            // save module name
            Name = node.Attributes["module"].Value;
            FileName = node.Attributes["file"].Value;

            // get module classes
            var path = string.Format("product/members/member[@module='{0}']", Name);
            foreach (XmlNode nd in node.OwnerDocument.SelectNodes(path))
            {
                var t = nd.Attributes["kind"];
                if (t != null)
                {
                    switch (t.Value)
                    {
                        case "Module":
                            var ndc = nd.SelectSingleNode("comment");
                            if (ndc != null)
                            {
                                Comment = FormatComment(ndc.InnerText);
                            }
                            break;
                        case "Class":
                            Classes.Add(new MClass(this, nd));
                            break;
                        case "Interface":
                            Interfaces.Add(new MInterface(this, nd));
                            break;
                        case "Enum":
                            Enums.Add(new MEnum(this, nd));
                            break;
                        case "Property":
                            if (nd.Attributes["class"] == null)
                            {
                                Properties.Add(new MProperty(this, nd));
                            }
                            break;
                        case "Field":
                            if (nd.Attributes["class"] == null)
                            {
                                Properties.Add(new MProperty(this, nd));
                            }
                            break;
                        case "Method":
                            if (nd.Attributes["class"] == null)
                            {
                                Methods.Add(new MMethod(this, nd));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            // sort members
            Classes.Sort();
            Interfaces.Sort();
            Enums.Sort();
            Properties.Sort();
            Methods.Sort();
        }

        public string Name { get; set; }
        public string FileName { get; set; }
        public string ProductVersion { get; set; }
        public string Comment { get; set; }
        public List<MEnum> Enums { get { return _enums; } }
        public List<MClass> Classes { get { return _classes; } }
        public List<MInterface> Interfaces { get { return _interfaces; } }
        public List<MMethod> Methods { get { return _methods; } }
        public List<MProperty> Properties { get { return _properties; } }
        public string Url
        {
            get { return string.Format("{0}.Module.html", Module.EscapeForUrl(Name)); }
        }

        // format comments for inclusion in docs
        public static string FormatComment(string comment)
        {
            // break text into paragraphs
            var s = string.Empty;
            var inpara = false;
            var inpre = false;
            for (var i = 0; i < comment.Length; i++)
            {
                var c = comment[i];
                if (!inpre && c == '\r' && MatchAt(comment, i, "\r\n\r\n"))
                {
                    if (inpara) s += "</p>\r\n";
                    s += "<p>";
                    inpara = true;
                    i += 3;
                }
                else if (c == '<' && MatchAt(comment, i, "<pre>"))
                {
                    if (inpara) s += "</p>\r\n";
                    s += "\r\n<pre>";
                    inpara = false;
                    inpre = true;
                    i += 4;
                }
                else if (c == '<' && MatchAt(comment, i, "</pre>"))
                {
                    s += "</pre>\r\n";
                    inpara = false;
                    inpre = false;
                    i += 5;
                }
                else
                {
                    if (!inpara && !inpre)
                    {
                        s += "<p>";
                        inpara = true;
                    }
                    s += c;
                }
            }
            if (inpara) s += "</p>";

            // remove empty paragraphs
            s = Regex.Replace(s, @"<p>\s*</p>", string.Empty);

            // clean up pre blocks
            s = Regex.Replace(s, @"<pre>\s+", "<pre>");
            s = Regex.Replace(s, @"\s+</pre>", "</pre>");
            s = Regex.Replace(s, @"(\r\n)+(\s*//)", "\r\n\r\n$2"); // empty line before comments
            s = Regex.Replace(s, @"(\s*//.*\r\n)\r\n(\s*//)", "$1$2"); // but not within comment blocks

            // check for unbalanced tags
            foreach (string tag in _matchTags)
            {
                if (Regex.Matches(s, "<" + tag + @"(\s+[^>]*)?>", RegexOptions.Singleline).Count != Regex.Matches(s, "</" + tag + ">").Count)
                {
                    Console.WriteLine("Warning: Unbalanced '{0}' tags in comment: {1}.", tag, s);
                }
            }

            // check for lt/gt within script blocks
            foreach (Match m in Regex.Matches(s, "<pre>(.*?)</pre>", RegexOptions.Singleline))
            {
                if (m.Groups[1].Value.IndexOfAny(_gtlt) > -1)
                {
                    Console.WriteLine("Warning: pre blocks should not contain '<' or '>' characters: {0}", s);
                }
            }

            // all done
            return s;
        }
        static string[] _matchTags = "pre,b,i,a,ul,ol,table,p".Split(',');
        static char[] _gtlt = "<>".ToCharArray();
        static bool MatchAt(string text, int index, string match)
        {
            var len = match.Length;
            return index + len < text.Length &&
                text.Substring(index, len) == match;
        }

        // create docs for this module
        public void CreateDocs(string outDir)
        {
            var fn = Path.Combine(outDir, Url);
            using (var sw = Program.OpenStreamWriter(fn))
            {
                // global module info
                // Name, File, Child Modules, comment
                OutputHeaderPanel(sw);

                // member tables
                // Classes, Interfaces, Enums, Properties, Methods
                sw.WriteLine("<div class=\"member-panel module members\">");
                OutputMemberTable(sw, "Classes", null, Classes);
                OutputMemberTable(sw, "Interfaces", null, Interfaces);
                OutputMemberTable(sw, "Enums", null, Enums);
                OutputMemberTable(sw, "Properties", null, Properties);
                OutputMemberTable(sw, "Methods", null, Methods);
                sw.WriteLine("</div>");

                // member lists
                if (OpenMemberList(sw, "Properties", null, Properties))
                {
                    foreach (var p in Properties)
                    {
                        OutputMember(sw, null, p);
                    }
                    sw.WriteLine("</div>");
                }
                if (OpenMemberList(sw, "Methods", null, Methods))
                {
                    foreach (var m in Methods)
                    {
                        OutputMember(sw, null, m);
                    }
                    sw.WriteLine("</div>");
                }
            }
        }

        // create member table
        public void OutputHeaderPanel(StreamWriter sw)
        {
            sw.WriteLine("<div class=\"header-panel module\">");

            // header
            sw.WriteLine("<h2><span class=\"identifier\">{0}</span> {1}</h2>",
                Name,
                Localization.GetString("Module"));
            sw.WriteLine("<dl class=\"dl-horizontal\">");
            sw.WriteLine("<dt>{0}</dt><dd class=\"filename\">{1}</dd>",
                Localization.GetString("File"),
                FileName);
            sw.WriteLine("<dt>{0}</dt><dd><span class=\"identifier\">{1}</span></dd>",
                Localization.GetString("Module"),
                Name);
            sw.WriteLine("</dl>");

            // body
            sw.WriteLine("<div class=\"comment\">{0}</div>",
                ResolveComment(Comment, null));
            sw.WriteLine("</div>");
        }

        // create member table
        public static void OutputMemberTable(StreamWriter sw, string title, Member owner, IEnumerable<Member> items)
        {
            if (!items.IsEmpty())
            {
                var header = Localization.GetString(title);
                sw.WriteLine("<h3 class=\"node\">{0}</h3>",
                    header);
                sw.WriteLine("<ul>");
                foreach (var i in items)
                {
                    var clsList = new List<string>();
                    clsList.Add(i.GetType().Name.ToLower());
                    if (owner != null && i.Class != owner.Class)
                    {
                        clsList.Add("inherited");
                    }
                    if (i.EventRaiser)
                    {
                        clsList.Add("raiser");
                    }
                    sw.WriteLine(
                        "<li class=\"{0}\"><a href=\"{1}\">{2}</a></li>",
                        string.Join(" ", clsList.ToArray()),
                        string.IsNullOrEmpty(i.Url) ? "#" + i.Name : "topic/" + i.Url,
                        i.Name);
                }
                sw.WriteLine("</ul>");
            }
        }

        // open member list
        public static bool OpenMemberList(StreamWriter sw, string title, Member owner, IEnumerable<Member> list)
        {
            if (!list.IsEmpty())
            {
                sw.WriteLine("<h3 class=\"node\">{0}</h3>",
                    Localization.GetString(title));
                sw.WriteLine("<div class=\"member-list\" " +
                    "ng-class=\"{ 'show-inherited' : ctx.showInherited, 'show-raisers' : ctx.showRaisers }\">");
                return true;
            }
            return false;
        }

        // output member into list
        public static void OutputMember(StreamWriter sw, Member owner, Member m)
        {
            var clsList = new List<string>();
            clsList.Add("member-panel");
            clsList.Add(m.GetType().Name.ToLower());
            if (owner != null && m.Class != owner.Class)
            {
                clsList.Add("inherited");
            }
            if (m.EventRaiser)
            {
                clsList.Add("raiser");
            }

            // start panel
            sw.WriteLine("<div id=\"{0}\" class=\"{1}\">",
                m.Name,
                string.Join(" ", clsList.ToArray()));
            sw.WriteLine("<h3>{0}{1}</h3>",
                m.Static ? "<span class=\"badge\">Static</span> " : string.Empty,
                m.Name);

            // content
            m.OutputDocs(sw, owner);

            // end panel
            sw.WriteLine("</div>");
        }

        // find members
        public Member FindMember(string name, Member owner)
        {
            Member m = null;

            // look in owner class first
            if (!string.IsNullOrEmpty(name) && char.IsLower(name[0]) && owner is MClass)
            {
                var oc = owner as MClass;
                m = oc.Properties.Find(name);
                if (m == null) m = oc.Methods.Find(name);
                if (m == null) m = oc.Events.Find(name);
            }

            // look in module
            if (m == null)
            {
                m = FindMember(name, true);
            }

            // done
            return m;
        }
        public Member FindMember(string name, bool allModules)
        {
            // doh!
            if (string.IsNullOrEmpty(name)) return null;

            // look in cache first
            Member m;
            if (_memberCache.TryGetValue(name, out m))
            {
                return m;
            }

            // look for classes/interfaces/enums
            m = Classes.Find(name);
            if (m == null) m = Interfaces.Find(name);
            if (m == null) m = Enums.Find(name);
            if (m == null) m = Properties.Find(name);
            if (m == null) m = Methods.Find(name);

            // found here, store in cache
            if (m != null) 
            {
                _memberCache[name] = m;
            }

            // not found here, look in other modules
            if (m == null && allModules)
            {
                foreach (var module in Program._modules.Values)
                {
                    if (module != this)
                    {
                        m = module.FindMember(name, false);
                        if (m != null) return m;
                    }
                }
            }

            // done searching
            return m;
        }

        // resolve strings into links
        public string ResolveString(string text, Member owner)
        {
            // resolve references to modules
            if (text == this.Name)
            {
                text = string.Format("<span class=\"identifier\">{0}</span>", text);
                return string.Format("<a href=\"topic/{0}\">{1}</a>", this.Url, text);
            }

            // empty string...
            var member = text;
            if (string.IsNullOrEmpty(member))
            {
                return string.Empty;
            }

            // remove array brackets
            var brackets = string.Empty;
            var pos = member.IndexOf('[');
            if (pos > -1)
            {
                brackets = member.Substring(pos);
                member = member.Substring(0, pos);
            }

            // External links
            if (ExternalLinks.ContainsKey(member))
            {
                var url = ExternalLinks[member];
                member = string.Format("<span class=\"identifier\">{0}</span>", member);
                if (!string.IsNullOrEmpty(url))
                {
                    member = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url, member);
                }
                return member + brackets;
            }

            // get the member owner (should be class, interface, or enum)
            if (owner is MProperty || owner is MEvent || owner is MMethod)
            {
                owner = owner.OwnerMember;
            }

            // get the member
            Member href = FindMember(member, owner);

            // remove namespace from text (it's in the href)
            pos = member.LastIndexOf('.');
            if (pos > -1)
            {
                // extract member text and namespace/owner class
                var cls = member.Substring(0, pos);
                member = member.Substring(pos + 1);

                // if we didn't get it, try with Class.member or Enum.member notation
                // try again with class.member notation
                if (href == null)
                {
                    owner = FindMember(cls, true);
                    if (owner is MEnum)
                    {
                        href = owner; // link to enum
                    }
                    else if (owner != null)
                    {
                        href = FindMember(member, owner); // link to member
                    }
                }
            }

            // report broken references
            if (href == null)
            {
                Console.WriteLine("Warning: can't resolve reference to '{0}'.", text);
                ExternalLinks[text] = null; // don't report this again (cleaner output)
            }

            // done
            member = string.Format("<span class=\"identifier\">{0}</span>", member);
            if (href != null)
            {
                member = string.Format("<a href=\"topic/{0}\">{1}</a>", href.Url, member);
            }
            return member + brackets;
        }

        // resolve links in comments
        public string ResolveComment(string comment, Member owner)
        {   
            // Resolve null or empty comment
            if (String.IsNullOrEmpty(comment)) return "";

            // resolve @see links (allow dots and slashes as separators)
            comment = Regex.Replace(comment, @"{@link\s([\w\.\/]+)}", (Match m) =>
            {
                return ResolveString(m.Groups[1].Value, owner);
            });

            // resolve @see links (allow dots and slashes as separators)
            comment = Regex.Replace(comment, @"@see:([\w\.\/]+\w+)", (Match m) =>
            {
                return ResolveString(m.Groups[1].Value, owner);
            });

            // resolve @fiddle links
            comment = Regex.Replace(comment, @"@fiddle:(\w+)", (Match m) =>
            {
                if (true) // embed fiddles
                {
                    return string.Format("<div class=\"member-panel example\">" +
                          "<h3 class=\"node collapsed\">{0}</h3>" +
                          "<div class=\"fiddle\">" +
                            "<script async src=\"http://jsfiddle.net/Wijmo5/{1}/embed/result,html,js,css\"></script>" +
                          "</div>" +
                        "</div>",
                        Localization.GetString("Example"),
                        m.Groups[1].Value);
                }
                else // add fiddle buttons
                { 
                    return string.Format("<a href=\"http://jsfiddle.net/Wijmo5/{0}/\" target=\"_blank\" class=\"btn btn-primary\">\r\n" +
                        "  <img src=\"images/fiddle.png\"/>\r\n" +
                        "  {1}\r\n" +
                        "</a>\r\n",
                        m.Groups[1].Value,
                        Localization.GetString("Example"));
                }
            });

            // done
            return comment;
        }

        // for debugging
        public override string ToString()
        {
            return "Module " + Name;
        }

        // Escapes name to be a correct part of an Url
        public static string EscapeForUrl(string name)
        {
            return string.IsNullOrEmpty(name) ? name : name.Replace('\\', '-').Replace('/', '-');
        }

        // External links to useful content
        const string moz = "https://developer.mozilla.org/en-US/docs/Web/";
        public static Dictionary<string, string> ExternalLinks = new Dictionary<string, string>
        {
            { "Array",              moz + "JavaScript/Reference/Global_Objects/Array" },
            { "Blob",               moz + "API/Blob" },
            { "Date",               moz + "JavaScript/Reference/Global_Objects/Date" },
            { "DOMRect",            moz + "API/DOMRect" },
            { "Element",            moz + "API/Element" },
            { "EventTarget",        moz + "API/EventTarget" },
            { "Function",           moz + "JavaScript/Reference/Global_Objects/Function" },
            { "HTMLElement",        moz + "API/HTMLElement" },
            { "HTMLInputElement",   moz + "API/HTMLInputElement" },
            { "Node",               moz + "API/Node" },
            { "Map",                moz + "JavaScript/Reference/Global_Objects/Map" },
            { "Object",             moz + "JavaScript/Reference/Global_Objects/Object" },
            { "SVGGElement",        moz + "API/SVGGElement" },
            { "Uint8Array",         moz + "JavaScript/Reference/Global_Objects/Uint8Array" },
            { "XMLHttpRequest",     moz + "API/XMLHttpRequest" },
            { "boolean",            moz + "JavaScript/Reference/Global_Objects/Boolean" },
            { "number",             moz + "JavaScript/Reference/Global_Objects/Number" },
            { "string",             moz + "JavaScript/Reference/Global_Objects/String" },
            { "any",                null },
            { "void",               null },
        };
    }
}
