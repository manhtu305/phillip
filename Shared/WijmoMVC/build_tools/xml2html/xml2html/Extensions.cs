﻿using System.Collections.Generic;

namespace xml2html
{
    // extensions for IEnumerable<Member>
    public static class Extensions
    {
        public static bool IsEmpty(this IEnumerable<Member> list)
        {
            foreach (var item in list)
            {
                return false;
            }
            return true;
        }
        public static Member Find(this IEnumerable<Member> list, string name)
        {
            // don't waste time
            if (string.IsNullOrEmpty(name) || 
                Module.ExternalLinks.ContainsKey(name))
            {
                return null;
            }

            // look by name of full name
            foreach (var item in list)
            {
                if (item.Name == name || item.FullName == name)
                {
                    return item;
                }
            }

            // look for partials (e.g. "grid.FlexGrid")
            if (name.IndexOf('.') > -1 && !name.StartsWith("wijmo"))
            {
                foreach (var item in list)
                {
                    if (item.FullName.EndsWith(name) && item.FullName[item.FullName.Length - name.Length - 1] == '.')
                    {
                        return item;
                    }
                }
            }

            // not found
            return null;
        }
    }
}
