﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace xml2html
{
    /// <summary>
    /// Provides strings localization. 
    /// </summary>
    /// <remarks>
    /// Set the <see cref="Culture"/> property to the wanted culture.
    /// Use the <see cref="GetString"/> method to get a localized version of the string, passing the English phrase as a parameter.
    /// The localized versions of strings are defined in the <see cref="FillDictionary"/> method.
    /// </remarks>
    static class Localization
    {
        private static CultureInfo _culture = CultureInfo.GetCultureInfo("en-US");
        private static Dictionary<string, string> _stringDict = null;

        /// <summary>
        /// Gets or sets the culture to localize to.
        /// </summary>
        public static CultureInfo Culture
        {
            get { return _culture; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                if (value != _culture)
                {
                    _stringDict = null;
                    _culture = value;
                }
            }
        }

        /// <summary>
        /// Returns the localized string for the passed English string. If there is no translation for the passed string then
        /// this string itself is returned by this method.
        /// </summary>
        /// <param name="stringKey"></param>
        /// <returns></returns>
        public static string GetString(string stringKey)
        {
            EnsureDictionary();
            string ret;
            if (_stringDict.TryGetValue(stringKey, out ret))
            {
                return ret;
            }
            if (_stringDict.Count > 0)
            {
                Console.WriteLine("Warning: Missing localized version of '{0}' string.", stringKey);
            }
            return stringKey;
        }

        private static void EnsureDictionary()
        {
            if (_stringDict == null)
            {
                _stringDict = new Dictionary<string, string>();
                FillDictionary(_stringDict);
            }
        }

        /// <summary>
        /// Fills the localized strings dictionary.
        /// </summary>
        /// <param name="strDict"></param>
        private static void FillDictionary(Dictionary<string, string> strDict)
        {
            switch (Culture.TwoLetterISOLanguageName)
            {
                // keep keys in alphabetic order, for the convenience
                case "ja":
                    strDict.Add("Arguments", "引数");
                    strDict.Add("Base Class", "基本クラス");
                    strDict.Add("Class", "クラス");
                    strDict.Add("Classes", "クラス");
                    strDict.Add("Constructor", " コンストラクタ");
                    strDict.Add("default value", "既定値");
                    strDict.Add("Derived Classes", "派生クラス");
                    strDict.Add("Description", "説明");
                    strDict.Add("Enum", "列挙体");
                    strDict.Add("Enumerations", "列挙体");
                    strDict.Add("Events", "イベント");
                    strDict.Add("Field", "フィールド");
                    strDict.Add("File", "ファイル");
                    strDict.Add("Implements", "インターフェイス");
                    strDict.Add("inherited from", "継承元：");
                    strDict.Add("Inherited Members", "継承されたメンバー");
                    strDict.Add("Interfaces", "インターフェイス");
                    strDict.Add("Introduction", "はじめに");
                    strDict.Add("Example", "サンプル");
                    strDict.Add("Methods", "メソッド");
                    strDict.Add("Module", "モジュール");
                    strDict.Add("Name", "名前");
                    strDict.Add("Parameters", "パラメーター");
                    strDict.Add("Parameters/Return Type", "パラメーター／戻り値の型");
                    strDict.Add("Properties", "プロパティ");
                    strDict.Add("Returns", "戻り値");
                    strDict.Add("Static", "静的メンバー");
                    strDict.Add("Show", "表示");
                    strDict.Add("Type", "型");
                    strDict.Add("Values", "メンバー");
                    strDict.Add("Enums", "列挙型");
                    strDict.Add("Event Raisers", "イベント発生元");
                    strDict.Add("Inherited From", "継承元");
                    strDict.Add("Interface", "インターフェイス");
                    strDict.Add("Members", "メンバー");
                    strDict.Add("Value", "値");
                    break;
                case "zh":
                    strDict.Add("Arguments", "参数");
                    strDict.Add("Base Class", "基类");
                    strDict.Add("Class", "类");
                    strDict.Add("Classes", "类");
                    strDict.Add("Constructor", " 构造函数");
                    strDict.Add("default value", "默认值");
                    strDict.Add("Derived Classes", "派生类");
                    strDict.Add("Description", "描述");
                    strDict.Add("Enum", "枚举");
                    strDict.Add("Enumerations", "枚举");
                    strDict.Add("Events", "事件");
                    strDict.Add("Field", "字段");
                    strDict.Add("File", "文件");
                    strDict.Add("Implements", "实现");
                    strDict.Add("inherited from", "继承自");
                    strDict.Add("Inherited Members", "继承成员");
                    strDict.Add("Interfaces", "接口");
                    strDict.Add("Introduction", "介绍");
                    strDict.Add("Example", "在线示例");
                    strDict.Add("Methods", "方法");
                    strDict.Add("Module", "模块");
                    strDict.Add("Name", "名称");
                    strDict.Add("Parameters", "参数");
                    strDict.Add("Parameters/Return Type", "参数/返回类型");
                    strDict.Add("Properties", "属性");
                    strDict.Add("Returns", "返回值");
                    strDict.Add("Static", "静态");
                    strDict.Add("Show", "表示");
                    strDict.Add("Type", "类型");
                    strDict.Add("Values", "值");
                    break;
            }
        }
    }
}
