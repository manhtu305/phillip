'use strict';
const clean = require('gulp-clean');
const gulp = require('gulp');
const fsExtra = require('fs-extra')
const execSync = require('child_process').execSync;
const rename = require('gulp-rename');
const pipeline = require('readable-stream').pipeline;
const uglifyjs = require('uglify-es');
const composer = require('gulp-uglify/composer');
const minify = composer(uglifyjs, console);
const uglifycss = require('gulp-uglifycss');

/**Get configuration from json file */
var config = JSON.parse(fsExtra.readFileSync('config.json'));

const USER = 'liampham';
const PASS = 'Coder121101';
const REPO = 'bitbucket.org/grapecity/wijmo';
const BRANCH = "wijmomvc";
const git = require('simple-git/promise');
const remote = `https://${USER}:${PASS}@${REPO}`;


/**Clean wijmo folder */
gulp.task('clean-wijmo', (done) => {
    if (!fsExtra.existsSync('wijmo')) {
        return done();
    }
    return gulp.src('wijmo', {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});
/** Clone wijmo from wijmomvc branch, get latest version*/
gulp.task('clone-wijmo', gulp.series("clean-wijmo", (done) => {
    git().silent(true)
        .clone(remote, "wijmo", ["--branch", BRANCH])
        .then(() => {
            done();
        })
        .catch((err) => console.error('failed: ', err));
}));


/**Clean dist folder */
gulp.task('clean-mvc', (done) => {
    if (!fsExtra.existsSync(config.wijmo_dist_folder)) {
        return done();
    }
    return gulp.src(config.wijmo_dist_folder, {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});


/**Clean all generated folder */
gulp.task('clean-all', gulp.series("clean-mvc", "clean-wijmo", async (done) => {
    done();
}));


/**Copy wijmo control files*/
gulp.task("copy-wijmo-controls", async (done) => {
    return gulp.src([`${config.wijmo_purejs_compiled_folder}/*.js`, `${config.wijmo_purejs_compiled_folder}/*.d.ts`, `${config.wijmo_ts_folder}/*.ts`]).pipe(gulp.dest(`${config.wijmo_dist_folder}/controls`));
});

/**Copy wijmo culture files */
gulp.task("copy-wijmo-cultures", async (done) => {
    return gulp.src([`${config.wijmo_npm_folder}/wijmo.cultures/*.js`, `${config.wijmo_npm_folder}/wijmo.cultures/*.d.ts`,`${config.wijmo_packages_folder}/wijmo.cultures/*.ts` ]).pipe(gulp.dest(`${config.wijmo_dist_folder}/controls/cultures`));
});

/**Copy wijmo style files */
gulp.task("copy-wijmo-styles", async (done) => {

    await fsExtra.copy(`${config.wijmo_npm_folder}/wijmo.styles`, `${config.wijmo_dist_folder}/styles`, {
        overwrite: true
    });

    await fsExtra.remove(`${config.wijmo_dist_folder}/styles/package.json`);

    done();
});

/**install wijmo packages*/
gulp.task("install-wijmo-dependencies", async (done) => {
    await execSync('npm install', {
        cwd: './wijmo'
    }, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
        done();
    });
});

/**install packages for 'wijmo packages'*/
gulp.task("install-wijmo-packages-dependencies", async (done) => {
    await execSync('npm install', {
        cwd: './wijmo/packages'
    }, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
        done();
    });

});

/**Complie wijmo source to purejs*/
gulp.task("compile-wijmo", async (done) => {
    await execSync('npm run build-globals-purejs', {
        cwd: './wijmo/packages'
    }, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
        done();
    });
});


/**Clean minify */
gulp.task('clean-minify', (done) => {
    return gulp.src(`${config.wijmo_dist_folder}/**/*.min.*`, {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});


/**Minify source code */
gulp.task("minify-js", (done) => {
    var options = {};
    return pipeline(
        gulp.src([`${config.wijmo_dist_folder}/**/*.js`]),
        minify(options),
        rename({
            suffix: '.min'
        }),
        gulp.dest(function (file) {
            return file.base;
        }));
});

gulp.task('minify-css', function () {
    return gulp.src([`${config.wijmo_dist_folder}/**/*.css`])
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(rename({
            suffix: '.min'
        }), )
        .pipe(gulp.dest(function (file) {
            return file.base;
        }));
});

/**build all source code. first build wijmo and then build mvc after*/
gulp.task("minify", gulp.series("clean-minify", "minify-js", "minify-css", async (done) => {
    done();
}));


/**default gulp task*/
gulp.task("default", gulp.series("clean-mvc", "copy-wijmo-controls", "copy-wijmo-cultures", "copy-wijmo-styles", async (done) => {
    done();
}));

/**Build mvc only*/
gulp.task("build-mvc", gulp.series("clean-mvc", "copy-wijmo-controls", "copy-wijmo-cultures", "copy-wijmo-styles", "minify", async (done) => {
    done();
}));

/**Build wijmo only*/
gulp.task("build-wijmo", gulp.series("clone-wijmo", "install-wijmo-dependencies", "install-wijmo-packages-dependencies", "compile-wijmo", async (done) => {
    done();
}));

/**Build wijmo only without re-clone*/
gulp.task("build-wijmo-withoutclone", gulp.series("install-wijmo-dependencies", "install-wijmo-packages-dependencies", "compile-wijmo", "build-mvc", async (done) => {
    done();
}));

/**build all source code. first build wijmo and then build mvc after*/
gulp.task("build-all", gulp.series("build-wijmo", "build-mvc", async (done) => {
    done();
}));