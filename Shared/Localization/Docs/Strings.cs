﻿// TBD: add xml comments?
#pragma warning disable 1591
//#define LOCALIZE_TO_CURRENT_CULTURE
//#define LOCTEST

using System;
using System.Resources;
using System.Globalization;
using System.Reflection;
using C1.Win.Localization;

namespace C1.C1Sample // <-- TODO: REPLACE WITH YOUR PROJECT'S NAMESPACE
{
    /// <summary>
    /// Static class containing UI strings used by the designer.
    /// </summary>
    public static class Strings
    {
        #region Infrastructure
        private static ResourceManager s_ResourceManager;

        public static ResourceManager ResourceManager
        {
            get { return s_ResourceManager; }
            set { s_ResourceManager = value; }
        }

        public static CultureInfo UICulture
        {
#if LOCTEST
			get { return new CultureInfo("ja"); }
#else
#if DEBUG && GRAPECITY
            get { return new CultureInfo("ja"); }
#else
            // 2012/10/26
            // As result of C1 developers discussion now CurrentUICulture used
            // instead of CurrentCulture to get culture for localized strings
#if !LOCALIZE_TO_CURRENT_CULTURE
            get { return CultureInfo.CurrentUICulture; }
#else
            get { return CultureInfo.CurrentCulture; }
#endif
#endif
#endif
        }
        #endregion

        public static class SampleNestedClass
        {
            public static string SampleString
            {
                get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "Sample nested string."); }
            }
        }

        public static string SampleString
        {
            get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "Sample string."); }
        }
    }
}
