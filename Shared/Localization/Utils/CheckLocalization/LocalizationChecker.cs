﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Resources;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using C1.Win.Localization.Design;

namespace CheckLocalization
{
    public enum LocalizationCheckerOptions
    {
        None,
        BuildDefaultResources,
    }

    public class LocalizationCheckerResult
    {
        private int _notFoundInResourcesCount;
        private int _notUsedResourceStringCount;
        private StringBuilder _messages = new StringBuilder();

        #region Public
        public void AddMessage(
            string mask,
            params object[] args)
        {
            _messages.Append(string.Format(mask, args));
            _messages.Append("\r\n");
        }
        #endregion

        #region Public properties
        public StringBuilder Messages
        {
            get { return _messages; }
        }

        public int NotFoundInResourcesCount
        {
            get { return _notFoundInResourcesCount; }
            set { _notFoundInResourcesCount++; }
        }

        public int NotUsedResourceStringCount
        {
            get { return _notUsedResourceStringCount; }
            set { _notUsedResourceStringCount = value; }
        }
        #endregion
    }

    public class LocalizationChecker
    {
        private LocalizationCheckerResult _result;
        private List<Assembly> _assemblies;
        private List<string> _cultures;
        private List<Type> _stringsTypes;
        private Dictionary<string, string> _licKeys;
        private LocalizationCheckerOptions _options;
        private List<Type> _types;
        private AttributesStringsCache _attributesStringsCache = new AttributesStringsCache();
        private StringsCache _stringsCache = new StringsCache();
        private Dictionary<Type, object> _objectsCache = new Dictionary<Type, object>();

        #region Private static
        private static FieldInfo GetFieldInfo(
            Type type,
            string fieldName,
            BindingFlags bindingFlags)
        {
            while (type != null && type != typeof(object))
            {
                FieldInfo result = type.GetField(fieldName, bindingFlags);
                if (result != null)
                    return result;
                type = type.BaseType;
            }
            return null;
        }

        public static Type FindType(
            Type[] types,
            string typeName)
        {
            foreach (Type type in types)
                if (type.Name == typeName)
                    return type;
            return null;
        }

        public static Type FindTypeByFullName(
            List<Type> types,
            string typeFullName)
        {
            foreach (Type type in types)
                if (type.FullName == typeFullName)
                    return type;
            return null;
        }

        private static CultureInfo GetCulture(
            string cultureName)
        {
            try
            {
                return new CultureInfo(cultureName);
            }
            catch
            {
                return null;
            }
        }

        private static string FindResource(
            Assembly assembly,
            string resourceSuffix)
        {
            string[] resources = assembly.GetManifestResourceNames();
            foreach (string s in resources)
                if (s.EndsWith(resourceSuffix))
                    return s;
            return null;
        }
        #endregion

        #region Private
        private object CreateObject(
            Type type,
            out Exception exception)
        {
            exception = null;
            if (type.IsAbstract)
                return null;
            object result;
            if (_objectsCache.TryGetValue(type, out result))
                return result;

            if (type.Name == "FlagsEnumEditorControl")
            {
                result = type.GetConstructor(new Type[] { typeof(IWindowsFormsEditorService) }).Invoke(new object[] { null });
            }
            else
            {
                result = null;
                ConstructorInfo ci = type.GetConstructor(new Type[] { typeof(string) });
                if (ci != null)
                {
                    string licKey;
                    if (_licKeys.TryGetValue(type.FullName, out licKey))
                    {
                        try
                        {
                            result = ci.Invoke(new object[] { licKey });
                        }
                        catch (Exception e)
                        {
                            exception = e;
                            result = null;
                        }
                    }
                }

                if (result == null)
                {
                    // create a control without license
                    ci = type.GetConstructor(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, new Type[0], null);
                    if (ci != null)
                    {
                        try
                        {
                            result = ci.Invoke(null);
                        }
                        catch (Exception e)
                        {
                            exception = e;
                            result = null;
                        }
                    }
                }
            }

            _objectsCache.Add(type, result);
            return result;
        }

        private bool GetSatelliteResourceStream(
            Type stringsType,
            string cn,
            out Stream resourceStream,
            out string resourceName)
        {
            resourceName = null;
            resourceStream = null;

            //
            CultureInfo ci = GetCulture(cn);
            if (ci == null)
                return true;

            // get satellite assembly
            Assembly sa = null;
            try
            {
                sa = stringsType.Assembly.GetSatelliteAssembly(ci);
            }
            catch
            {
                // can not get satellite assembly simple return null
                // possible resources for the type are in main assembly
                return true;
            }

            // search resource stream in satellite assembly
            resourceName = FindResource(sa, stringsType.FullName + "." + ci.ToString() + ".resources");
            if (resourceName == null)
                // satellite assembly HAS NO resources for specified type
                // simple return null possible they are in main assembly
                return true;

            // Open resources
            resourceStream = sa.GetManifestResourceStream(resourceName);
            if (resourceStream == null)
            {
                _result.AddMessage("Can't create resource stream for resource [{0}], assembly [{1}].", resourceName, sa.FullName);
                return false;
            }

            return true;
        }

        private bool GetMainResourceStream(
            Type stringsType,
            string cn,
            out Stream resourceStream,
            out string resourceName)
        {
            //
            resourceStream = null;

            // search resource stream in satellite assembly
            resourceName = FindResource(stringsType.Assembly, cn + "." + stringsType.FullName + ".resources");
            if (resourceName == null)
                // main assembly HAS NO resources for specified type
                // simple return null possible they are in satellite assembly
                return true;

            // Open resources
            resourceStream = stringsType.Assembly.GetManifestResourceStream(resourceName);
            if (resourceStream == null)
            {
                _result.AddMessage("Can't create resource stream for resource [{0}], assembly [{1}].", resourceName, stringsType.Assembly.FullName);
                return false;
            }

            return true;
        }

        private void ProcessAttributes(
            object[] attrs,
            Type type,
            MemberInfo memberInfo)
        {
            string memberInfoName = memberInfo == null ? "(NONE)" : memberInfo.Name;
            foreach (object attr in attrs)
            {
                Type attrType = attr.GetType();
                if (attrType.Name == "C1DescriptionAttribute")
                {
                    FieldInfo fiKey = GetFieldInfo(attrType, "_key", BindingFlags.NonPublic | BindingFlags.Instance);
                    PropertyInfo piDescription = attrType.GetProperty("DescriptionValue", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo fiStringsType = GetFieldInfo(attrType, "_stringsType", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (fiKey != null && fiKey.FieldType == typeof(string) && fiStringsType != null && fiStringsType.FieldType == typeof(Type) && piDescription != null && piDescription.PropertyType == typeof(string))
                    {
                        Type stringsType = fiStringsType.GetValue(attr) as Type;
                        string key = "Attributes.Description." + (string)fiKey.GetValue(attr);
                        string description = (string)piDescription.GetValue(attr, null);
                        string oldDefaultValue = _attributesStringsCache.FindOrAdd(stringsType, key, description);
                        if (oldDefaultValue != null && oldDefaultValue != description)
                            _result.AddMessage("Different default value specified for attribute string [{0}].", key);
                    }
                    else
                    {
                        // do not generate error here because it can be OLD-style
                        // C1DescriptionAttribute from old stringtable-base localization
                        //_result.AddMessage("Invalid C1Description attribute type [{0}], member [{1}], assembly [{2}].", type.FullName, memberInfoName, type.Assembly.FullName);
                    }
                }
                else if (attrType.Name == "C1CategoryAttribute")
                {
                    FieldInfo fiKey = GetFieldInfo(attrType, "categoryValue", BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo fiStringsType = GetFieldInfo(attrType, "_stringsType", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (fiKey != null && fiKey.FieldType == typeof(string) && fiStringsType != null && fiStringsType.FieldType == typeof(Type))
                    {
                        Type stringsType = fiStringsType.GetValue(attr) as Type;
                        string key = fiKey.GetValue(attr) as string;
                        string oldDefaultValue = _attributesStringsCache.FindOrAdd(stringsType, "Attributes.Category." + key, key);
                        if (oldDefaultValue != null && oldDefaultValue != key)
                            _result.AddMessage("Different default value specified for attribute string [{0}].", "Attributes.Category." + key);
                    }
                    else
                    {
                        // do not generate error here because it can be OLD-style
                        // C1CategoryAttribute from old stringtable-base localization
                        //_result.AddMessage("Invalid C1Category attribute type [{0}], member [{1}], assembly [{2}].", type.FullName, memberInfoName, type.Assembly.FullName);
                    }
                }
                else if (attrType.Name == "EndUserLocalizeOptionsAttribute")
                {
                    // process only attributes that have specified _stringsType
                    FieldInfo fiKey = GetFieldInfo(attrType, "_key", BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo fiDescription = GetFieldInfo(attrType, "_description", BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo fiStringsType = GetFieldInfo(attrType, "_stringsType", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (fiKey != null && fiKey.FieldType == typeof(string) && fiStringsType != null && fiStringsType.FieldType == typeof(Type) && fiDescription != null && fiDescription.FieldType == typeof(string))
                    {
                        Type stringsType = fiStringsType.GetValue(attr) as Type;
                        if (stringsType != null)
                        {
                            string key = "Attributes.EndUserLocalizeOptions." + fiKey.GetValue(attr) as string;
                            string description = fiDescription.GetValue(attr) as string;
                            string oldDefaultValue = _attributesStringsCache.FindOrAdd(stringsType, key, description);
                            if (oldDefaultValue != null && oldDefaultValue != key)
                                _result.AddMessage("Different default value specified for attribute string [{0}].", key);
                        }
                    }
                    else
                        _result.AddMessage("Invalid EndUserLocalizeOptions attribute type [{0}], member [{1}], assembly [{2}].", type.FullName, memberInfoName, type.Assembly.FullName);
                }
            }
        }

        private string GetControlStringDefaultValue(
            string[] parts,
            Type type)
        {
            object obj = null;
            if (type.IsGenericTypeDefinition)
            {
                // string linked with generic type
                // for example such types exist in C1Shedule
                // we can not create an instance of this class
                // so assume that string is ok
                return "!!!CAN NOT CHECK STRING DEFINED IN GENERIC CLASS!!!";
            }
            else
            {
                Exception e;
                obj = CreateObject(type, out e);
                if (obj == null)
                {
                    if (e != null)
                        _result.AddMessage("Can not create instance of type [{0}], exception: [{1}].", type.FullName, e.Message);
                    return null;
                }
            }

            try
            {
                object parentObj = obj;
                for (int i = 2; i < parts.Length - 1; i++)
                {
                    FieldInfo fi = type.GetField(parts[i], BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    if (fi == null)
                        fi = type.GetField("m_" + parts[i], BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    if (fi == null)
                        return null;
                    type = fi.FieldType;
                    obj = fi.GetValue(obj);
                }

                PropertyInfo pi = type.GetProperty(parts[parts.Length - 1], BindingFlags.Public | BindingFlags.Instance);
                if (pi == null)
                    return null;


                string result;
                try
                {
                    result = pi.GetValue(obj, null) as string;
                }
                catch
                {
                    // property exists but we can not read value of property, this string should be 
                    result = "!!!CAN NOT GET DEFAULT VALUE FOR STRING!!!";
                }

                return result;
            }
            finally
            {
            }
        }

        private string GetControlStringDefaultValue(
            string s)
        {
            string[] parts = s.Split('.');
            if (parts.Length <= 2)
            {
                _result.AddMessage("Invalid Control string [{0}].", s);
                return null;
            }

            foreach (Type t in _types)
            {
                if (t == null)
                    continue;
                if (!typeof(Control).IsAssignableFrom(t))
                    continue;

                string typeName;
                if (t.IsGenericType)
                {
                    int p = t.Name.IndexOf('`');
                    if (p == -1)
                        continue;
                    typeName = t.Name.Substring(0, p);
                }
                else
                    typeName = t.Name;
                if (typeName != parts[1])
                    continue;

                // t - is a type that should be analysed
                string result = GetControlStringDefaultValue(parts, t);
                if (result != null)
                    return result;
            }
            return null;
        }

        private string GetStringsStringDefaultValue(
            Type stringsType,
            string s)
        {
            string[] parts = s.Split('.');
            Type type = stringsType;
            for (int i = 0; i < parts.Length - 1; i++)
            {
                Type[] types = type.GetNestedTypes(BindingFlags.Public);
                type = FindType(types, parts[i]);
                if (type == null)
                    return null;
            }

            PropertyInfo pi = type.GetProperty(parts[parts.Length - 1], BindingFlags.Public | BindingFlags.Static);
            if (pi == null || pi.PropertyType != typeof(string) || !pi.CanRead)
                return null;

            return pi.GetValue(null, null) as string;
        }

        private void ProcessResourceStream(
            Stream resourceStream,
            string resourceName,
            string cn,
            Type stringsType)
        {
            using (ResourceReader resourceReader = new ResourceReader(resourceStream))
            {
                foreach (DictionaryEntry de in resourceReader)
                {
                    string key = de.Key as string;
                    if (key == null)
                    {
                        _result.AddMessage("Not a string: Resources stream should contain only strings, the object with type [{0}] was found.", key.GetType());
                        continue;
                    }
                    string value = de.Value as string;

                    //
                    ResourceString rs = _stringsCache.FindOrAddString(stringsType, key);
                    rs.AddTranslation(cn, value);

                    if (key.StartsWith("Forms."))
                    {
                        string s = GetControlStringDefaultValue(key);
                        if (s == null)
                        {
                            _result.AddMessage("Not used: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                            _result.NotUsedResourceStringCount++;
                            continue;
                        }
                        else
                            rs.DefaultValue = s;
                    }
                    else if (key.StartsWith("Attributes."))
                    {
                        string s = _attributesStringsCache.GetString(stringsType, key);
                        if (s == null)
                        {
                            _result.AddMessage("Not used in attributes: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                            _result.NotUsedResourceStringCount++;
                        }
                        else
                            rs.DefaultValue = s;
                    }
                    else
                    {
                        //if ((options & ResCheckOptionFlags.IgnoreUnusedStrings) != ResCheckOptionFlags.None)
                        //    continue;

                        // search this string in the Strings class
                        string s = GetStringsStringDefaultValue(stringsType, key);
                        if (s == null)
                        {
                            _result.AddMessage("Not used in Strings class: String [{0}] with text [{1}].", key, value, cn);
                            _result.NotUsedResourceStringCount++;
                            continue;
                        }
                        else
                            rs.DefaultValue = s;
                    }
                }
            }
        }

        private void CheckStringsStrings(
            Type stringsClass,
            string parentPrefix,
            Type classToCheck,
            bool topParent)
        {
            PropertyInfo[] properties = classToCheck.GetProperties(BindingFlags.Static | BindingFlags.Public);
            string s = topParent ? "" : parentPrefix + classToCheck.Name + ".";
            foreach (PropertyInfo pi in properties)
            {
                if (pi.PropertyType != typeof(string))
                    continue;

                ResourceString rs =_stringsCache.GetString(stringsClass, s + pi.Name);
                if (rs == null)
                {
                    rs = _stringsCache.FindOrAddString(stringsClass, s + pi.Name);
                    rs.DefaultValue = pi.GetValue(null, null) as string;
                    _result.AddMessage("Strings type [{0}], translation for [{1}] string is not found.", stringsClass.FullName, s + pi.Name);
                }
                else
                {
                    foreach (string cn in _cultures)
                        if (rs.GetTranslation(cn) == null)
                            _result.AddMessage("Strings type [{0}], translation for [{1}], culture [{2}] is not found.", stringsClass.FullName, s + pi.Name, cn);
                }
            }

            Type[] types = classToCheck.GetNestedTypes(BindingFlags.Public | BindingFlags.NonPublic);
            foreach (Type type in types)
                CheckStringsStrings(
                    stringsClass,
                    s,
                    type,
                    false);
        }

        private void ProcessStringsType(
            Type stringsType)
        {
            // 1. Check structure of Strings class
            if (stringsType.Name == "Strings")
            {
                PropertyInfo pi = stringsType.GetProperty("ResourceManager", BindingFlags.Public | BindingFlags.Static);
                if (pi == null)
                    _result.AddMessage("Strings type [{0}] has no ResourceManager property", stringsType.FullName);
                else
                {
                    if (!pi.CanRead || !pi.CanWrite || pi.PropertyType != typeof(ResourceManager))
                        _result.AddMessage("Strings type [{0}] has invalid ResourceManager property", stringsType.FullName);
                }

                pi = stringsType.GetProperty("UICulture", BindingFlags.Public | BindingFlags.Static);
                if (pi == null)
                    _result.AddMessage("Strings type [{0}] has no UICulture property", stringsType.FullName);
                else
                {
                    if (!pi.CanRead || pi.PropertyType != typeof(CultureInfo))
                        _result.AddMessage("Strings type [{0}] has invalid UICulture property", stringsType.FullName);
                }
            }

            // 2. Get strings from resources
            foreach (string cn in _cultures)
            {
                string satelliteResourceName;
                Stream satelliteResourceStream;
                if (!GetSatelliteResourceStream(stringsType, cn, out satelliteResourceStream, out satelliteResourceName))
                    continue;
                string mainResourceName;
                Stream mainResourceStream;
                if (!GetMainResourceStream(stringsType, cn, out mainResourceStream, out mainResourceName))
                    return;

                //
                if (satelliteResourceStream == null && mainResourceStream == null)
                {
                    _result.AddMessage("Resources for type [{0}] and culture [{1}] is not found.", stringsType.FullName, cn);
                    continue;
                }

                if (satelliteResourceStream != null)
                    ProcessResourceStream(satelliteResourceStream, satelliteResourceName, cn, stringsType);

                if (mainResourceStream != null)
                    ProcessResourceStream(mainResourceStream, mainResourceName, cn, stringsType);
            }

            // 3. Get strings from Strings class, they all should be in _stringsCache
            CheckStringsStrings(stringsType, "", stringsType, true);
        }

        private bool CheckString(
            Type stringsType,
            string stringKey,
            string cn,
            bool addWarning)
        {
            ResourceString rs = _stringsCache.GetString(stringsType, stringKey);
            if (rs == null)
            {
                if (addWarning)
                {
                    _result.AddMessage("String not exist in resources: Strings type = [{0}], string key = [{1}]", stringsType.FullName, stringKey);
                    _result.NotFoundInResourcesCount++;
                }
                return false;
            }

            if (rs.GetTranslation(cn) == null)
            {
                if (addWarning)
                {
                    _result.AddMessage("String translation not exist in resources: Strings type = [{0}], string key = [{1}], culture = [{2}]", stringsType.FullName, stringKey, cn);
                    _result.NotFoundInResourcesCount++;
                }
                return false;
            }

            return true;
        }

        private void ProcessControl(
            object control,
            object endUserLocalizeOptions,
            string prefix,
            List<object> processedControls)
        {
            if (processedControls.Contains(control))
                return;
            processedControls.Add(control);

            //
            Type controlType = control.GetType();
            if (endUserLocalizeOptions == null)
            {
                // EndUserLocalizeOptions attribute NOT specified
                // exit if controlType not derived from Control
                if (!typeof(Control).IsAssignableFrom(controlType))
                    return;
            }
            else
            {
                // specified EndUserLocalizeOptions with Exclude == true
                if (!Utils.GetExclude(endUserLocalizeOptions))
                    return;
            }

            string[] properties = ControlLocalizeRules.GetLocalizedProperties(controlType, endUserLocalizeOptions);
            if (properties != null && properties.Length > 0)
            {
                foreach (string property in properties)
                {
                    // try to get the default property value
                    PropertyInfo pi = controlType.GetProperty(property, BindingFlags.Instance | BindingFlags.Public);
                    if (pi != null && pi.CanRead && pi.PropertyType == typeof(string))
                    {
                        string defaultValue = pi.GetValue(control, null) as string;
                        if (defaultValue != null && defaultValue.Length > 0)
                        {
                            // should be in resources
                            string stringKey = prefix + pi.Name;
                            ResourceString rs = null;
                            Type stringsType = null;
                            foreach (Type type in _stringsTypes)
                            {
                                rs = _stringsCache.GetString(type, stringKey);
                                if (rs != null)
                                {
                                    stringsType = type;
                                    break;
                                }
                            }
                            if (rs == null)
                            {
                                // add string to _strigsCache if only one Strings type is specified
                                // this string will be written to resx file if buildresx is true
                                if (_stringsTypes.Count == 1)
                                {
                                    rs = _stringsCache.FindOrAddString(_stringsTypes[0], stringKey);
                                    rs.DefaultValue = defaultValue;
                                }
                                _result.AddMessage("String not exist in resources: string key = [{0}].", stringKey);
                                _result.NotFoundInResourcesCount++;
                                continue;
                            }

                            foreach (string cn in _cultures)
                            {
                                if (rs.GetTranslation(cn) == null)
                                {
                                    _result.AddMessage("String translation not exist in resources: Strings type = [{0}], string key = [{0}], culture = [{2}].", stringsType.FullName, stringKey, cn);
                                    _result.NotFoundInResourcesCount++;
                                }
                            }
                        }
                    }
                }
            }

            // process child controls only for root object or for "C1" types, they should be defined in assembly
            // with name started with "C1"
            FieldInfo[] fields = controlType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
            foreach (FieldInfo fi in fields)
            {
                // by default we localize only protected members
                //if (!fi.IsFamily && !fi.IsPublic)
                //    continue;

                object fieldEndUserLocalizeOptions = Utils.GetEndUserLocalizeOptionsAttribute(fi.GetCustomAttributes(false));
                if (fieldEndUserLocalizeOptions == null)
                    fieldEndUserLocalizeOptions = Utils.GetEndUserLocalizeOptionsAttribute(fi.FieldType.GetCustomAttributes(false));

                string fieldName = fi.Name.StartsWith("m_") ? fi.Name.Substring(2) : fi.Name;
                object childControl = fi.GetValue(control);
                if (childControl != null)
                    ProcessControl(childControl, fieldEndUserLocalizeOptions, prefix + fieldName + ".", processedControls);
            }
        }

        private void ProcessControl(
            Type controlType)
        {
            object endUserLocalizeOptions = Utils.GetEndUserLocalizeOptionsAttribute(controlType.GetCustomAttributes(false));
            if (endUserLocalizeOptions == null)
            {
                // EndUserLocalizeOptions attribute NOT specified
                // exit if controlType not derived from Control
                if (!typeof(Control).IsAssignableFrom(controlType))
                    return;
            }
            else
            {
                // specified EndUserLocalizeOptions with Exclude == true
                if (!Utils.GetExclude(endUserLocalizeOptions))
                    return;
            }

            // try to use constructor with license key
            Exception e;
            object control = CreateObject(controlType, out e);
            if (control == null)
            {
                if (e != null)
                    _result.AddMessage("Can not create object of type [{0}], exception [{1}].", controlType, e);
                return;
            }

            try
            {
                List<object> processedControls = new List<object>();
                ProcessControl(control, endUserLocalizeOptions, "Forms." + controlType.Name + ".", processedControls);
            }
            finally
            {
            }
        }

        private void ProcessTypeAttributes(
            Type type)
        {
            ProcessAttributes(type.GetCustomAttributes(false), type, null);
            PropertyInfo[] properties;
            try
            {
                properties = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            }
            catch (Exception)
            {
                properties = null;
            }

            if (properties != null)
                foreach (PropertyInfo propertyInfo in properties)
                {
                    try
                    {
                        object[] attrs = propertyInfo.GetCustomAttributes(false);
                        ProcessAttributes(attrs, type, propertyInfo);
                    }
                    catch
                    {
                    }
                }

            EventInfo[] events;
            try
            {
                events = type.GetEvents(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            }
            catch
            {
                events = null;
            }

            if (events != null)
                foreach (EventInfo eventInfo in events)
                {
                    try
                    {
                        object[] attrs = eventInfo.GetCustomAttributes(false);
                        ProcessAttributes(attrs, type, eventInfo);
                    }
                    catch
                    {
                    }
                }

            Type[] nestedTypes = type.GetNestedTypes(BindingFlags.Public);
            if (nestedTypes != null)
                foreach (Type nt in nestedTypes)
                    ProcessTypeAttributes(nt);
        }
        #endregion

        #region Public
        public LocalizationCheckerResult ProcessResources(
            List<Assembly> assemblies,
            List<Type> types,
            List<string> cultures,
            List<Type> stringsTypes,
            Dictionary<string, string> licKeys,
            LocalizationCheckerOptions options,
            bool processControls)
        {
            if (cultures == null || cultures.Count <= 0)
                throw new ArgumentException("cultures");
            if (types == null || types.Count <= 0)
                throw new ArgumentException("types");
            if (stringsTypes == null || stringsTypes.Count <= 0)
                throw new ArgumentException("stringsTypes");

            // 1. 
            _result = new LocalizationCheckerResult();
            _assemblies = assemblies;
            _types = types;
            _cultures = cultures;
            _stringsTypes = stringsTypes;
            _licKeys = licKeys;
            _options = options;

            // 2. Build list of strings used in C1Description and C1Category attributes
            foreach (Type type in _types)
            {
                if (type == null || !type.IsPublic)
                    continue;

                ProcessTypeAttributes(type);
            }

            // 3. Process strings types and build cache of localized strings
            foreach (Type type in _stringsTypes)
                ProcessStringsType(type);

            // 4. Check all strings in _attributesStringsCache they should be in _stringsCache
            foreach (string cn in _cultures)
            {
                foreach (KeyValuePair<Type, Dictionary<string, string>> p in _attributesStringsCache.Data)
                {
                    foreach (KeyValuePair<string, string> str in p.Value)
                    {
                        if (!CheckString(p.Key, str.Key, cn, true))
                        {
                            // attributes string not found or translation not found
                            // add string to _stringsCache otherwise
                            ResourceString rs = _stringsCache.FindOrAddString(p.Key, str.Key);
                            rs.DefaultValue = str.Value;
                        }
                    }
                }
            }

            // 5.Check controls
            if (processControls)
            {
                foreach (Type type in _types)
                {
                    if (type == null)
                        continue;

                    ProcessControl(type);
                }
            }

            //
            return _result;
        }
        #endregion

        #region Public properties
        public StringsCache StringsCache
        {
            get { return _stringsCache; }
        }
        #endregion

        #region Nested types
        private class AttributesStringsCache
        {
            public Dictionary<Type, Dictionary<string, string>> Data = new Dictionary<Type, Dictionary<string, string>>();

            #region Public
            public string FindOrAdd(
                Type stringsType,
                string key,
                string defaultValue)
            {
                Dictionary<string, string> strs;
                if (!Data.TryGetValue(stringsType, out strs))
                {
                    strs = new Dictionary<string, string>();
                    Data.Add(stringsType, strs);
                }
                string result;
                if (strs.TryGetValue(key, out result))
                    return result;

                strs.Add(key, defaultValue);
                return null;
            }

            public string GetString(
                Type stringsType,
                string stringKey)
            {
                Dictionary<string, string> strs;
                if (!Data.TryGetValue(stringsType, out strs))
                    return null;

                string result;
                if (!strs.TryGetValue(stringKey, out result))
                    return null;

                return result;
            }
            #endregion
        }
        #endregion
    }
}
