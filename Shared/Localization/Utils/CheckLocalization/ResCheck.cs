using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Globalization;
using System.Resources;
using System.Windows.Forms;
using System.ComponentModel;
using System.IO;

using C1.C1Preview.Design.Localization;

namespace CheckLocalization
{
    [Flags]
    public enum ResCheckResultFlags
    {
        NoError = 0x00,
        HasNoStringsManagerClass = 0x01,
        HasNoStringsClass = 0x02,

        StringsHasNoResourceManager = 0x04,
        StringsResourceManagerInvalid = 0x08,
        StringsHasNoUICulture = 0x10,
        StringsUICultureInvalid = 0x20,
        StringsHasNoStringsResourcesName = 0x40,
        StringsStringsResourcesNameInvalid = 0x80,

        StringsInvalid = StringsHasNoResourceManager |
            StringsResourceManagerInvalid |
            StringsHasNoUICulture |
            StringsUICultureInvalid |
            StringsHasNoStringsResourcesName |
            StringsStringsResourcesNameInvalid,

        ResourcesHasInvalidObject = 0x100,
        ResourcesHasInvalidString = 0x200,
        StringNotFoundInStrings = 0x400,
        StringNotFoundInForm = 0x800,

        StringNotFoundInC1Description = 0x1000,
        StringNotFoundInC1Category = 0x2000,

        StringNotFoundInResources = 0x4000,

        CantGetAssemblyTypes = 0x8000,

        ResourcesNotFound = 0x10000,
        CantCreateResourceStream = 0x20000,
        CantGetSatellliteAssembly = 0x40000,
    }

    [Flags]
    public enum ResCheckOptionFlags
    {
        None = 0,
        /// <summary>
        /// Ignore unused strings
        /// </summary>
        IgnoreUnusedStrings = 1,
    }

    public class ResCheckResult
    {
        public ResCheckResultFlags Flags;
        public StringBuilder Messages;
        public int InvalidStringCount;
        public int NotUsedResourceStringCount;
        public int NotFoundInResourcesCount;

        #region Constructors

        public ResCheckResult()
        {
            Flags = ResCheckResultFlags.NoError;
            Messages = new StringBuilder();
        }

        #endregion

        #region Public

        public void AddMessage(string mask, params object[] parameters)
        {
            Messages.Append(string.Format(mask, parameters));
            Messages.Append("\r\n");
        }

        #endregion
    }

    public static class ResCheck
    {
        #region Private static
        private static Type FindType(Type[] types, string typeName)
        {
            foreach (Type type in types)
                if (type.Name == typeName)
                    return type;
            return null;
        }

        private static PropertyInfo GetStringsString(Type type, string s)
        {
            string[] parts = s.Split('.');
            for (int i = 0; i < parts.Length - 1; i++)
            {
                Type[] types = type.GetNestedTypes(BindingFlags.Public);
                type = FindType(types, parts[i]);
                if (type == null)
                    return null;
            }

            PropertyInfo pi = type.GetProperty(parts[parts.Length - 1], BindingFlags.Public | BindingFlags.Static);
            if (pi == null || pi.PropertyType != typeof(string) || !pi.CanRead)
                return null;

            return pi;
        }

        private static PropertyInfo GetFormsString(Type[] types, string s)
        {
            string[] parts = s.Split('.');
            if (parts.Length <= 1)
                return null;

            Type type = null;
            foreach (Type t in types)
                if (t.Name == parts[0] && typeof(Control).IsAssignableFrom(t))
                {
                    type = t;
                    break;
                }
            if (type == null)
                return null; // root type not found

            for (int i = 1; i < parts.Length - 1; i++)
            {
                FieldInfo fi = type.GetField(parts[i], BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (fi == null)
                    fi = type.GetField("m_" + parts[i], BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (fi == null)
                    return null;
                type = fi.FieldType;
            }

            return type.GetProperty(parts[parts.Length - 1], BindingFlags.Public | BindingFlags.Instance);
        }

        private static void CheckAttributes(object[] attrs, AttributesStrings attributesStrings)
        {
            foreach (object attr in attrs)
            {
                Type attrType = attr.GetType();
                if (attrType.Name == "C1DescriptionAttribute")
                {
                    FieldInfo fiKey = attrType.GetField("_key", BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo fiStringsType = attrType.GetField("_stringsType", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (fiKey != null && fiKey.FieldType == typeof(string) && fiStringsType != null && fiStringsType.FieldType == typeof(Type))
                        attributesStrings.AddDecriptionString((Type)fiStringsType.GetValue(attr), (string)fiKey.GetValue(attr));
                }
                else if (attrType.Name == "C1CategoryAttribute")
                {
                    FieldInfo fiKey = attrType.BaseType.GetField("categoryValue", BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo fiStringsType = attrType.GetField("_stringsType", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (fiKey != null && fiKey.FieldType == typeof(string) && fiStringsType != null && fiStringsType.FieldType == typeof(Type))
                        attributesStrings.AddCategoryString((Type)fiStringsType.GetValue(attr), (string)fiKey.GetValue(attr));
                }
            }
        }

        private static void CheckCultures(
            Type stringsClass,
            List<CultureInfo> allowedCultures,
            StringsCache stringsCache,
            ResCheckResult result,
            string stringKey,
            string stringCategory)
        {
            foreach (CultureInfo ci in allowedCultures)
                if (!stringsCache.HasString(stringsClass, ci, stringKey))
                {
                    result.NotFoundInResourcesCount++;
                    result.AddMessage("String not exist in resources: culture = [{0}], key = [{1}], category = [{2}]", ci.Name, stringKey, stringCategory);
                    result.Flags |= ResCheckResultFlags.StringNotFoundInResources;
                }
        }

        private static void CheckStringsStrings(
            Type stringsClass,
            string parentPrefix,
            Type classToCheck,
            List<CultureInfo> allowedCultures,
            StringsCache stringsCache,
            ResCheckResult result,
            bool topParent)
        {
            PropertyInfo[] properties = classToCheck.GetProperties(BindingFlags.Static | BindingFlags.Public);
            string s = topParent ? "" : parentPrefix + classToCheck.Name + ".";
            foreach (PropertyInfo pi in properties)
            {
                if (pi.PropertyType != typeof(string))
                    continue;

#if skip_dima // this can be used to generate missing string resources
                var qq = pi.GetValue(null, null);
                System.Diagnostics.Debug.WriteLine(
                    string.Format(
                    "<data name=\"ja.{0}\" xml:space=\"preserve\">\r\n  <value>{1}</value>\r\n</data>",
                    s + pi.Name,
                    qq));
#endif

                CheckCultures(
                    stringsClass,
                    allowedCultures,
                    stringsCache,
                    result,
                    s + pi.Name,
                    "Property of Strings class");
            }

            Type[] types = classToCheck.GetNestedTypes(BindingFlags.Public | BindingFlags.NonPublic);
            foreach (Type type in types)
                CheckStringsStrings(
                    stringsClass,
                    s,
                    type,
                    allowedCultures,
                    stringsCache,
                    result,
                    false);
        }

        private static void CheckProperties(object control, Type controlType, List<Type> stringsClasses, object endUserLocalizeOptions, ResCheckResult result, List<CultureInfo> allowedCultures, StringsCache cache, string prefix)
        {
            string[] properties = ControlLocalizeRules.GetLocalizedProperties(controlType, endUserLocalizeOptions);
            if (properties == null || properties.Length == 0)
                return;

            foreach (string property in properties)
            {
                // try to get the default property value
                PropertyInfo pi = controlType.GetProperty(property, BindingFlags.Instance | BindingFlags.Public);
                if (pi != null && pi.CanRead && pi.PropertyType == typeof(string))
                {
                    string defaultValue = pi.GetValue(control, null) as string;
                    if (defaultValue != null && defaultValue.Length > 0)
                    {
                        // should be in resources
                        string stringKey = prefix + "." + pi.Name;
                        foreach (CultureInfo ci in allowedCultures)
                        {
                            bool exists = false;
                            foreach (Type t in stringsClasses)
                                if (cache.HasString(t, ci, stringKey))
                                {
                                    exists = true;
                                    break;
                                }
                            if (!exists)
                            {
                                result.NotFoundInResourcesCount++;
                                result.AddMessage("String not exist in resources: culture = [{2}] key = [{0}], category = [{1}]", stringKey, "Property on end user localizable form", ci.Name);
                                result.Flags |= ResCheckResultFlags.StringNotFoundInResources;
                            }
                        }
                    }
                }
            }
        }

        private static void CheckControlStrings(
            Type controlType,
            List<Type> stringsClasses,
            ResCheckResult result,
            List<CultureInfo> allowedCultures,
            StringsCache cache,
            Dictionary<string, string> licKeys)
        {
            object endUserLocalizeOptions = Utils.GetEndUserLocalizeOptionsAttribute(controlType.GetCustomAttributes(false));
            if (endUserLocalizeOptions == null || Utils.GetExclude(endUserLocalizeOptions))
                return; // excluded from localization

            // try to use constructor with license key
            Control control = null;
            ConstructorInfo ci = controlType.GetConstructor(new Type[] { typeof(string) });
            if (ci != null)
            {
                string licKey;
                if (licKeys.TryGetValue(controlType.FullName, out licKey))
                {
                    try
                    {
                        control = ci.Invoke(new object[] { licKeys }) as Control;
                    }
                    catch
                    {
                        control = null;
                    }
                }
            }

            if (control == null)
            {
                // create a control without license
                ci = controlType.GetConstructor(new Type[0]);
                if (ci != null)
                {
                    try
                    {
                        control = ci.Invoke(null) as Control;
                    }
                    catch
                    {
                        control = null;
                    }
                }
            }

            if (control == null)
                return;

            try
            {
                CheckProperties(control, controlType, stringsClasses, endUserLocalizeOptions, result, allowedCultures, cache, "Forms." + controlType.Name);

                FieldInfo[] fields = controlType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
                foreach (FieldInfo fi in fields)
                {
                    // by default we localize only protected members
                    if (!fi.IsFamily && !fi.IsPublic)
                        continue;

                    string fieldName = fi.Name.StartsWith("m_") ? fi.Name.Substring(2) : fi.Name;
                    CheckProperties(
                        fi.GetValue(control),
                        fi.FieldType,
                        stringsClasses,
                        Utils.GetEndUserLocalizeOptionsAttribute(fi.GetCustomAttributes(false)),
                        result,
                        allowedCultures,
                        cache,
                        "Forms." + controlType.Name + "." + fieldName);
                }

            }
            finally
            {
                control.Dispose();
            }
        }

        private static void CheckResourceStream(
            Stream resourceStream,
            string resourceName,
            CultureInfo ci,
            Type[] types,
            ResCheckOptionFlags options,
            Type stringsClass,
            StringsCache stringsCache,
            AttributesStrings attributesStrings,
            ResCheckResult result)
        {
            using (ResourceReader resourceReader = new ResourceReader(resourceStream))
            {
                foreach (DictionaryEntry de in resourceReader)
                {
                    string key = de.Key as string;
                    if (key == null)
                    {
                        result.Flags |= ResCheckResultFlags.ResourcesHasInvalidObject;
                        result.AddMessage("Not a string: Resources stream should contain only strings, the object with type [{0}] was found.", key.GetType());
                        continue;
                    }
                    string value = de.Value as string;

                    //
                    stringsCache.AddString(stringsClass, ci, key);

                    if (key.StartsWith("Forms."))
                    {
                        if (GetFormsString(types, key.Substring(6)) == null)
                        {
                            result.Flags |= ResCheckResultFlags.StringNotFoundInForm;
                            result.AddMessage("Not used: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                            result.NotUsedResourceStringCount++;
                            continue;
                        }
                    }
                    else if (key.StartsWith("Attributes."))
                    {
                        string s = key.Substring(11);
                        if (s.StartsWith("EndUserLocalization"))
                        {
                        }
                        else if (s.StartsWith("Description."))
                        {
                            if (!attributesStrings.ContainsDescription(stringsClass, s.Substring(12)))
                            {
                                result.Flags |= ResCheckResultFlags.StringNotFoundInC1Description;
                                result.AddMessage("Not used in C1Description: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                                result.NotUsedResourceStringCount++;
                                continue;
                            }
                        }
                        else if (s.StartsWith("Category."))
                        {
                            if (!attributesStrings.ContainsCategory(stringsClass, s.Substring(9)))
                            {
                                result.Flags |= ResCheckResultFlags.StringNotFoundInC1Category;
                                result.AddMessage("Not used in C1Category: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                                result.NotUsedResourceStringCount++;
                                continue;
                            }
                        }
                        else
                        {
                            result.Flags |= ResCheckResultFlags.ResourcesHasInvalidString;
                            result.AddMessage("Unknown attribute: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                            result.InvalidStringCount++;
                            continue;
                        }
                    }
                    else
                    {
                        if ((options & ResCheckOptionFlags.IgnoreUnusedStrings) != ResCheckOptionFlags.None)
                            continue;

                        // search this string in the Strings class
                        if (GetStringsString(stringsClass, key) == null)
                        {
                            result.Flags |= ResCheckResultFlags.StringNotFoundInStrings;
                            result.AddMessage("Not used in Strings class: String [{0}] with text [{1}].", key, value, ci);
                            result.NotUsedResourceStringCount++;
                            continue;
                        }
                    }
                }
            }
        }

        private static Stream GetSatelliteResourceStream(
            Assembly assembly,
            Type stringsClass,
            CultureInfo ci,
            ResCheckResult result,
            out string resourceName)
        {
            // get satellite assembly
            resourceName = null;
            Assembly sa = null;
            try
            {
                sa = assembly.GetSatelliteAssembly(ci);
            }
            catch
            {
                // can not get satellite assembly simple return null
                // possible resources for the type are in main assembly
                return null;
            }

            // search resource stream in satellite assembly
            string[] resources = sa.GetManifestResourceNames();
            string resKey = stringsClass.FullName + "." + ci.ToString() + ".resources";
            foreach (string s in resources)
                if (s.EndsWith(resKey))
                {
                    resourceName = s;
                    break;
                }
            if (resourceName == null)
                // satellite assembly HAS NO resources for specified type
                // simple return null possible they are in main assembly
                return null;

            // Open resources
            Stream resourceStream = sa.GetManifestResourceStream(resourceName);
            if (resourceStream == null)
            {
                result.Flags |= ResCheckResultFlags.CantCreateResourceStream;
                result.AddMessage("Can't create resource stream for resource [{0}], assembly [{1}].", resourceName, sa.FullName);
                return null;
            }

            return resourceStream;
        }

        private static Stream GetMainResourceStream(
            Assembly assembly,
            Type stringsClass,
            CultureInfo ci,
            ResCheckResult result,
            out string resourceName)
        {
            // search resource stream in satellite assembly
            string[] resources = assembly.GetManifestResourceNames();
            resourceName = null;
            string resKey = ci.ToString() + "." + stringsClass.FullName + ".resources";
            foreach (string s in resources)
                if (s.EndsWith(resKey))
                {
                    resourceName = s;
                    break;
                }
            if (resourceName == null)
                // main assembly HAS NO resources for specified type
                // simple return null possible they are in satellite assembly
                return null;

            // Open resources
            Stream resourceStream = assembly.GetManifestResourceStream(resourceName);
            if (resourceStream == null)
            {
                result.Flags |= ResCheckResultFlags.CantCreateResourceStream;
                result.AddMessage("Can't create resource stream for resource [{0}], assembly [{1}].", resourceName, assembly.FullName);
                return null;
            }

            return resourceStream;
        }

        private static void CheckStringsClass(
            Assembly assembly,
            Type[] types,
            List<CultureInfo> allowedCultures,
            ResCheckOptionFlags options,
            Type stringsClass,
            StringsCache stringsCache,
            AttributesStrings attributesStrings,
            ResCheckResult result)
        {
            // 1. Check the structure of Strings class
            if (stringsClass.Name == "Strings")
            {
                PropertyInfo pi = stringsClass.GetProperty("ResourceManager", BindingFlags.Public | BindingFlags.Static);
                if (pi == null)
                    result.Flags |= ResCheckResultFlags.StringsHasNoResourceManager;
                else
                {
                    if (!pi.CanRead || !pi.CanWrite || pi.PropertyType != typeof(ResourceManager))
                        result.Flags |= ResCheckResultFlags.StringsResourceManagerInvalid;
                }

                pi = stringsClass.GetProperty("UICulture", BindingFlags.Public | BindingFlags.Static);
                if (pi == null)
                    result.Flags |= ResCheckResultFlags.StringsHasNoUICulture;
                else
                {
                    if (!pi.CanRead || pi.PropertyType != typeof(CultureInfo))
                        result.Flags |= ResCheckResultFlags.StringsUICultureInvalid;
                }

                if ((result.Flags & ResCheckResultFlags.StringsInvalid) != 0)
                    return;
            }

            // 2. Resources stored in satellite assemblies, cycle other cultures
            foreach (CultureInfo ci in allowedCultures)
            {
                string satelliteResourceName;
                Stream satelliteResourceStream = GetSatelliteResourceStream(assembly, stringsClass, ci, result, out satelliteResourceName);
                if ((result.Flags & ResCheckResultFlags.CantCreateResourceStream) != 0)
                    return;
                string mainResourceName;
                Stream mainResourceStream = GetMainResourceStream(assembly, stringsClass, ci, result, out mainResourceName);
                if ((result.Flags & ResCheckResultFlags.CantCreateResourceStream) != 0)
                    return;

                //
                if (satelliteResourceStream == null && mainResourceStream == null)
                {
                    result.Flags |= ResCheckResultFlags.ResourcesNotFound;
                    result.AddMessage("Resources for type [{0}] and culture [{1}] is not found.", stringsClass.FullName, ci.ToString());
                    return;
                }

                if (satelliteResourceStream != null)
                    CheckResourceStream(satelliteResourceStream, satelliteResourceName, ci, types, options, stringsClass, stringsCache, attributesStrings, result);

                if (mainResourceStream != null)
                    CheckResourceStream(mainResourceStream, mainResourceName, ci, types, options, stringsClass, stringsCache, attributesStrings, result);
            }

            CheckStringsStrings(stringsClass, "", stringsClass, allowedCultures, stringsCache, result, true);
        }

        /*private static void CheckStringsClass(
            Assembly assembly,
            Type[] types,
            List<CultureInfo> allowedCultures,
            ResCheckOptionFlags options,
            Type stringsClass,
            StringsCache stringsCache,
            AttributesStrings attributesStrings, 
            ResCheckResult result)
        {
            // 1. Check the structure of Strings class
            if (stringsClass.Name == "Strings")
            {
                PropertyInfo pi = stringsClass.GetProperty("ResourceManager", BindingFlags.Public | BindingFlags.Static);
                if (pi == null)
                    result.Flags |= ResCheckResultFlags.StringsHasNoResourceManager;
                else
                {
                    if (!pi.CanRead || !pi.CanWrite || pi.PropertyType != typeof(ResourceManager))
                        result.Flags |= ResCheckResultFlags.StringsResourceManagerInvalid;
                }

                pi = stringsClass.GetProperty("UICulture", BindingFlags.Public | BindingFlags.Static);
                if (pi == null)
                    result.Flags |= ResCheckResultFlags.StringsHasNoUICulture;
                else
                {
                    if (!pi.CanRead || pi.PropertyType != typeof(CultureInfo))
                        result.Flags |= ResCheckResultFlags.StringsUICultureInvalid;
                }

                if ((result.Flags & ResCheckResultFlags.StringsInvalid) != 0)
                    return;
            }

            // 2. Get resources for stringsClass
            string[] resources = assembly.GetManifestResourceNames();
            string resourceName = null;
            string resKey = stringsClass.FullName + ".resources";
            foreach (string s in resources)
                if (s.EndsWith(resKey))
                {
                    resourceName = s;
                    break;
                }

            if (resourceName == null)
            {
                result.Flags |= ResCheckResultFlags.ResourcesNotFound;
                result.AddMessage("Resources for type [{0}] is not found.", stringsClass.FullName);
                return;
            }

            // 3. Open resources
            Stream resourceStream = assembly.GetManifestResourceStream(resourceName);
            if (resourceStream == null)
            {
                result.Flags |= ResCheckResultFlags.CantCreateResourceStream;
                result.AddMessage("Can't create resource stream for resource [{0}].", resourceName);
                return;
            }

            try
            {
                // go over all strings in resources and check
                using (ResourceReader resourceReader = new ResourceReader(resourceStream))
                {
                    foreach (DictionaryEntry de in resourceReader)
                    {
                        string key = de.Key as string;
                        if (key == null)
                        {
                            result.Flags |= ResCheckResultFlags.ResourcesHasInvalidObject;
                            result.AddMessage("Not a string: Resources stream should contain only strings, the object with type [{0}] was found.", key.GetType());
                            continue;
                        }
                        string value = de.Value as string;

                        // check the culture
                        int p = key.IndexOf('.');
                        if (p <= 0)
                        {
                            result.Flags |= ResCheckResultFlags.ResourcesHasInvalidString;
                            result.AddMessage("Has no culture prefix: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                            result.InvalidStringCount++;
                            continue;
                        }
                        string cultureName = key.Substring(0, p);
                        CultureInfo ci;
                        try
                        {
                            ci = CultureInfo.GetCultureInfo(cultureName);
                        }
                        catch
                        {
                            ci = null;
                        }
                        if (ci == null)
                        {
                            result.Flags |= ResCheckResultFlags.ResourcesHasInvalidString;
                            result.AddMessage("Has unknown culture [{2}]: Resource [{3}] String [{0}] with text [{1}].", key, value, cultureName, resourceName);
                            result.InvalidStringCount++;
                            continue;
                        }

                        int i;
                        for (i = 0; i < allowedCultures.Count; i++)
                            if (allowedCultures[i].Name == ci.Name)
                                break;
                        if (i >= allowedCultures.Count)
                        {
                            result.Flags |= ResCheckResultFlags.ResourcesHasInvalidString;
                            result.AddMessage("Has not allowed culture [{2}]: Resource [{3}] String [{0}] with text [{1}].", key, value, cultureName, resourceName);
                            result.InvalidStringCount++;
                            continue;
                        }

                        string keyNoCulture = key.Substring(p + 1);

                        stringsCache.AddString(stringsClass, ci, keyNoCulture);

                        if (keyNoCulture.StartsWith("Forms."))
                        {
                            if (GetFormsString(types, keyNoCulture.Substring(6)) == null)
                            {
                                result.Flags |= ResCheckResultFlags.StringNotFoundInForm;
                                result.AddMessage("Not used: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                                result.NotUsedResourceStringCount++;
                                continue;
                            }
                        }
                        else if (keyNoCulture.StartsWith("Attributes."))
                        {
                            string s = keyNoCulture.Substring(11);
                            if (s.StartsWith("EndUserLocalization"))
                            {
                            }
                            else if (s.StartsWith("Description."))
                            {
                                if (!attributesStrings.ContainsDescription(stringsClass, s.Substring(12)))
                                {
                                    result.Flags |= ResCheckResultFlags.StringNotFoundInC1Description;
                                    result.AddMessage("Not used in C1Description: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                                    result.NotUsedResourceStringCount++;
                                    continue;
                                }
                            }
                            else if (s.StartsWith("Category."))
                            {
                                if (!attributesStrings.ContainsCategory(stringsClass, s.Substring(9)))
                                {
                                    result.Flags |= ResCheckResultFlags.StringNotFoundInC1Category;
                                    result.AddMessage("Not used in C1Category: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                                    result.NotUsedResourceStringCount++;
                                    continue;
                                }
                            }
                            else
                            {
                                result.Flags |= ResCheckResultFlags.ResourcesHasInvalidString;
                                result.AddMessage("Unknown attribute: Resource [{2}] String [{0}] with text [{1}].", key, value, resourceName);
                                result.InvalidStringCount++;
                                continue;
                            }
                        }
                        else
                        {
                            if ((options & ResCheckOptionFlags.IgnoreUnusedStrings) != ResCheckOptionFlags.None)
                                continue;
                            // search this string in the Strings class
                            if (GetStringsString(stringsClass, keyNoCulture) == null)
                            {
                                result.Flags |= ResCheckResultFlags.StringNotFoundInStrings;
                                result.AddMessage("Not used in Strings class: String [{0}] with text [{1}].", key, value, cultureName);
                                result.NotUsedResourceStringCount++;
                                continue;
                            }
                        }
                    }
                }

                //
                CheckStringsStrings(stringsClass, "", stringsClass, allowedCultures, stringsCache, result, true);
            }
            finally
            {
                resourceStream.Dispose();
            }
        }*/
        #endregion

        #region Public static
        public static ResCheckResult CheckResources(
            Assembly assembly,
            List<CultureInfo> allowedCultures,
            List<Type> stringsClasses,
            Dictionary<string, string> licKeys,
            ResCheckOptionFlags options)
        {
            if (allowedCultures == null || allowedCultures.Count == 0)
                throw new ArgumentException("The list of allowed cultures should be not empty.", "allowedCultures");
            foreach (CultureInfo ci in allowedCultures)
                if (ci.Name == CultureInfo.InvariantCulture.Name)
                    throw new ArgumentException("The list of allowed cultures can't contain the InvariantCulture.", "allowedCultures");
            if (stringsClasses == null || stringsClasses.Count == 0)
                throw new ArgumentException("The list of classes to check is empty");

            ResCheckResult result = new ResCheckResult();


            // 1. Get list of types in assembly
            Type[] types;
            try
            {
                types = assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                // Note: to get listed by GetTypes, some types require full trust.
                // E.g. this applies to our types derived from UITypeEditor. So,
                // in partial trust scenarios, GetTypes fails but for all practical
                // purposes getting the loaded types from the exception works as well.
                // --dima.
                types = ex.Types;
            }
            catch
            {
                result.Flags |= ResCheckResultFlags.CantGetAssemblyTypes;
                return result;
            }


            // 2. Build list of strings used in C1Description and C1Category attributes
            AttributesStrings attributesStrings = new AttributesStrings();
            foreach (Type type in types)
            {
                if (type == null)
                    continue;

                CheckAttributes(type.GetCustomAttributes(false), attributesStrings);
                PropertyInfo[] properties;
                try
                {
                    properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                }
                catch (Exception)
                {
                    properties = null;
                }

                if (properties != null)
                    foreach (PropertyInfo propertyInfo in properties)
                    {
                        try
                        {
                            object[] attrs = propertyInfo.GetCustomAttributes(false);
                            CheckAttributes(attrs, attributesStrings);
                        }
                        catch
                        {
                        }
                    }

                EventInfo[] events;
                try
                {
                    events = type.GetEvents(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                }
                catch
                {
                    events = null;
                }

                if (events != null)
                    foreach (EventInfo eventInfo in events)
                    {
                        try
                        {
                            object[] attrs = eventInfo.GetCustomAttributes(false);
                            CheckAttributes(attrs, attributesStrings);
                        }
                        catch
                        {
                        }
                    }
            }


            // 3. Check all strings classes
            StringsCache stringsCache = new StringsCache();
            for (int i = 0; i < stringsClasses.Count; i++)
                CheckStringsClass(assembly, types, allowedCultures, options, stringsClasses[i], stringsCache, attributesStrings, result);

            // 4. Check atributes strings, they should exists in resources
            foreach (KeyValuePair<Type, CategoryDescriptionStrings> p in attributesStrings.Data)
            {
                foreach (string s in p.Value.CategoryStrings)
                    CheckCultures(p.Key, allowedCultures, stringsCache, result, "Attributes.Category." + s, "C1Description strings");

                foreach (string s in p.Value.DescriptionStrings)
                    CheckCultures(p.Key, allowedCultures, stringsCache, result, "Attributes.Description." + s, "C1Description strings");
            }

            // 5. Check the forms and controls which have the end user localization
            foreach (Type type in types)
            {
                if (!typeof(Control).IsAssignableFrom(type))
                    continue;

                CheckControlStrings(type, stringsClasses, result, allowedCultures, stringsCache, licKeys);
            }

            return result;
        }
        #endregion

        #region Nested classes
        private class AttributesStrings
        {
            public Dictionary<Type, CategoryDescriptionStrings> Data = new Dictionary<Type, CategoryDescriptionStrings>();

            #region Public

            public void AddCategoryString(Type stringsType, string categoryString)
            {
                CategoryDescriptionStrings cds;
                if (!Data.TryGetValue(stringsType, out cds))
                {
                    cds = new CategoryDescriptionStrings();
                    Data.Add(stringsType, cds);
                }
                if (!cds.CategoryStrings.Contains(categoryString))
                    cds.CategoryStrings.Add(categoryString);
            }

            public void AddDecriptionString(Type stringsType, string descriptionString)
            {
                CategoryDescriptionStrings cds;
                if (!Data.TryGetValue(stringsType, out cds))
                {
                    cds = new CategoryDescriptionStrings();
                    Data.Add(stringsType, cds);
                }
                if (!cds.DescriptionStrings.Contains(descriptionString))
                    cds.DescriptionStrings.Add(descriptionString);
            }

            public bool ContainsCategory(Type stringsType, string categoryString)
            {
                CategoryDescriptionStrings cds;
                if (Data.TryGetValue(stringsType, out cds))
                    return cds.CategoryStrings.Contains(categoryString);
                return false;
            }

            public bool ContainsDescription(Type stringsType, string descriptionString)
            {
                CategoryDescriptionStrings cds;
                if (Data.TryGetValue(stringsType, out cds))
                    return cds.DescriptionStrings.Contains(descriptionString);
                return false;
            }

            #endregion
        }

        private class CategoryDescriptionStrings
        {
            public List<string> CategoryStrings = new List<string>();
            public List<string> DescriptionStrings = new List<string>();
        }

        private class StringsCache
        {
            public Dictionary<Type, Dictionary<CultureInfo, List<string>>> Data = new Dictionary<Type,Dictionary<CultureInfo,List<string>>>();

            #region Public

            public void AddString(Type stringsClass, CultureInfo cultureInfo, string stringKey)
            {
                Dictionary<CultureInfo, List<string>> cultures;
                if (!Data.TryGetValue(stringsClass, out cultures))
                {
                    cultures = new Dictionary<CultureInfo, List<string>>();
                    Data.Add(stringsClass, cultures);
                }

                List<string> strings;
                if (!cultures.TryGetValue(cultureInfo, out strings))
                {
                    strings = new List<string>();
                    cultures.Add(cultureInfo, strings);
                }

                strings.Add(stringKey);
            }

            public bool HasString(Type stringsClass, CultureInfo cultureInfo, string stringKey)
            {
                Dictionary<CultureInfo, List<string>> cultures;
                if (Data.TryGetValue(stringsClass, out cultures))
                {
                    List<string> strings;
                    if (cultures.TryGetValue(cultureInfo, out strings))
                        return strings.Contains(stringKey);
                }
                return false;
            }

            #endregion
        }

        #endregion
    }
}
