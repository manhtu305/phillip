﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Globalization;
using System.Resources;
using System.Windows.Forms;
using System.ComponentModel;
using System.IO;

using C1.C1Preview.Design.Localization;

namespace CheckLocalization
{
    public class ResBuildResult
    {
        public StringBuilder Messages;

        #region Public
        public void AddMessage(
            string mask,
            params object[] parameters)
        {
            Messages.Append(string.Format(mask, parameters));
            Messages.Append("\r\n");
        }
        #endregion
    }

    public class ResBuild
    {
        private Assembly _assembly;
        private List<Type> _stringsClasses;
        private Dictionary<string, string> _licKeys;
        private ResBuildResult _result;
        private Type[] _types;
        private Dictionary<Type, ResXResourceWriter> _writers;

        #region Private
        private ResXResourceWriter GetWriter(
            Type stringsType)
        {
            return null;
        }

        private void WriteAttrStrings(
            object[] attrs)
        {
            foreach (object attr in attrs)
            {
                Type attrType = attr.GetType();
                if (attrType.Name == "C1DescriptionAttribute")
                {
                    FieldInfo fiKey = attrType.GetField("_key", BindingFlags.NonPublic | BindingFlags.Instance);
                    PropertyInfo piDescription = attrType.GetProperty("DescriptionValue", BindingFlags.Public);
                    FieldInfo fiStringsType = attrType.GetField("_stringsType", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (fiKey == null || fiKey.FieldType != typeof(string) || fiStringsType == null || fiStringsType.FieldType != typeof(Type) || piDescription == null || piDescription.PropertyType != typeof(string))
                        _result.AddMessage("Invalid C1Description attribute in assembly [{0}].", _assembly.FullName);
                    else
                    {
                        string key = fiKey.GetValue(attr) as string;
                        string description = string.Empty;
                        if (piDescription != null)
                            description = piDescription.GetValue(attr, null) as string;
                        Type stringsType = fiStringsType.GetValue(attr) as Type;
                        GetWriter(stringsType).AddResource("Attributes.Description." + key, description);
                    }
                }
                else if (attrType.Name == "C1CategoryAttribute")
                {
                    FieldInfo fiKey = attrType.BaseType.GetField("categoryValue", BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo fiStringsType = attrType.GetField("_stringsType", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (fiKey == null || fiKey.FieldType != typeof(string) || fiStringsType == null || fiStringsType.FieldType != typeof(Type))
                        _result.AddMessage("Invalid C1Category attribute in assembly [{0}].", _assembly.FullName);
                    else
                    {
                        Type stringsType = fiStringsType.GetValue(attr) as Type;
                        string key = fiKey.GetValue(attr) as string;
                        GetWriter(stringsType).AddResource("Attributes.Category." + key, key);
                    }
                }
            }
        }

        private void WriteStringsType(
            Type stringsType,
            Type type,
            string prefix)
        {
            PropertyInfo[] props = type.GetProperties(BindingFlags.Static | BindingFlags.Public);
            foreach (PropertyInfo pi in props)
            {
                if (pi.PropertyType != typeof(string))
                    continue;

                string value = pi.GetValue(null, null) as string;
                GetWriter(stringsType).AddResource(prefix + pi.Name, value); // ???
            }

            Type[] types = type.GetNestedTypes(BindingFlags.Public | BindingFlags.NonPublic);
            foreach (Type t in types)
                WriteStringsType(stringsType, type, prefix + type.Name + ".");
        }
        #endregion

        #region Public
        public ResBuildResult Build(
            Assembly assembly,
            List<Type> stringsClasses,
            Dictionary<string, string> licKeys)
        {
            if (stringsClasses == null || stringsClasses.Count <= 0)
                throw new ArgumentException("stringsClasses");

            // 0.
            _assembly = assembly;
            _stringsClasses = stringsClasses;
            _licKeys = licKeys;
            _result = new ResBuildResult();

            // 1. Get list of types in assembly
            try
            {
                _types = _assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                // Note: to get listed by GetTypes, some types require full trust.
                // E.g. this applies to our types derived from UITypeEditor. So,
                // in partial trust scenarios, GetTypes fails but for all practical
                // purposes getting the loaded types from the exception works as well.
                // --dima.
                _types = ex.Types;
            }
            catch
            {
                _result.AddMessage("Can't get list of types from assembly [{0}].", _assembly.FullName);
                return _result;
            }

            // 2. Get strings from attributes
            foreach (Type type in _types)
            {
                if (type == null)
                    continue;

                WriteAttrStrings(type.GetCustomAttributes(false));
                PropertyInfo[] properties;
                try
                {
                    properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                }
                catch (Exception)
                {
                    properties = null;
                }

                if (properties != null)
                    foreach (PropertyInfo propertyInfo in properties)
                    {
                        try
                        {
                            object[] attrs = propertyInfo.GetCustomAttributes(false);
                            WriteAttrStrings(attrs);
                        }
                        catch
                        {
                        }
                    }

                EventInfo[] events;
                try
                {
                    events = type.GetEvents(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                }
                catch
                {
                    events = null;
                }

                if (events != null)
                    foreach (EventInfo eventInfo in events)
                    {
                        try
                        {
                            object[] attrs = eventInfo.GetCustomAttributes(false);
                            WriteAttrStrings(attrs);
                        }
                        catch
                        {
                        }
                    }
            }

            // 3. Go over Strings types
            foreach (Type stringsType in _stringsClasses)
                WriteStringsType(stringsType, stringsType, string.Empty);

            // 4. Go over controls
            foreach (Type type in _types)
            {
                if (!typeof(Control).IsAssignableFrom(type))
                    continue;
            }

            //
            return _result;
        }
        #endregion

        #region Public properties
        #endregion
    }
}
