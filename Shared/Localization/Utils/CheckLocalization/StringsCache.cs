﻿using System;
using System.Collections.Generic;

namespace CheckLocalization
{
    public class ResourceString
    {
        private string _defaultValue;
        private Dictionary<string, string> _localizations = new Dictionary<string, string>();

        #region Public
        public string GetTranslation(
            string cultureName)
        {
            string result;
            if (_localizations.TryGetValue(cultureName, out result))
                return result;
            return null;
        }

        public void AddTranslation(
            string cultureName,
            string translation)
        {
            _localizations.Add(cultureName, translation);
        }
        #endregion

        #region Public properties
        public string DefaultValue
        {
            get { return _defaultValue; }
            set { _defaultValue = value; }
        }
        #endregion
    }

    public class StringsCache
    {
        private Dictionary<Type, Dictionary<string, ResourceString>> _data = new Dictionary<Type,Dictionary<string,ResourceString>>();

        #region Public
        public ResourceString FindOrAddString(
            Type stringsType,
            string stringKey)
        {
            Dictionary<string, ResourceString> strs;
            if (!_data.TryGetValue(stringsType, out strs))
            {
                strs = new Dictionary<string, ResourceString>();
                _data.Add(stringsType, strs);
            }

            ResourceString result;
            if (!strs.TryGetValue(stringKey, out result))
            {
                result = new ResourceString();
                strs.Add(stringKey, result);
            }

            return result;
        }

        public ResourceString GetString(
            Type stringsType,
            string stringKey)
        {
            Dictionary<string, ResourceString> strs;
            if (!_data.TryGetValue(stringsType, out strs))
                return null;

            ResourceString result;
            if (!strs.TryGetValue(stringKey, out result))
                return null;

            return result;
        }
        #endregion

        #region Public properties
        public Dictionary<Type, Dictionary<string, ResourceString>> Data
        {
            get { return _data; }
        }
        #endregion
    }
}
