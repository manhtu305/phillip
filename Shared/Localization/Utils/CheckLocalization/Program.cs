using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;
using System.Reflection;
using System.Resources;

using C1.Win.Localization.Design;

namespace CheckLocalization
{
    public enum ExitCodeEnum : int
    {
        Success = 0,
        InvalidParameters = 1,
        AssemblyFileNameNotFound = 2,
        ResxFileNameNotFound = 3,
        AllowedCulturesInvalid = 4,
        CantLoadAssembly = 5,
        ErrorsExists = 6,
        StringsClassNotFound = 7,
        CantGetTypes = 8,
    }

    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            string curDir = Environment.CurrentDirectory;

            if (args.Length < 3 || args.Length > 5)
            {
                Console.WriteLine(@"Usage: CheckLocalization assemblies cultures stringClasses [/buildresx]");
                Console.WriteLine(@"  assemblies - the list of assemblies files delimited with ','");
                Console.WriteLine(@"  cultures  - comma-delimited list of allowed cultures");
                Console.WriteLine(@"  stringClasses - comma-delimited list of Strings classes");
                Console.WriteLine(@"  /buildresx - if specified, resx files with default (english) values will be generated");
                Console.WriteLine(@"               in current directory");
                Console.WriteLine(@"");
                Console.WriteLine(@"Example:");
                Console.WriteLine(@"  CheckLocalization d:\temp\C1.C1Preview.2.dll ru,ja C1.C1Preview.Strings,C1.C1Preview.Design.Localization.DesignLocalizationStrings");
                Console.WriteLine(@"  CheckLocalization d:\temp\C1.C1Preview.2.dll ru C1.C1Preview.Strings /buildresx");

                Environment.ExitCode = (int)ExitCodeEnum.InvalidParameters;
                return;
            }

            //
            bool buildresx = false;
            bool buildstrings = false;
            bool buildattrstrings = false;
            bool buildformstrings = false;
            for (int i = 0; i < args.Length; i++)
            {
                buildresx = buildresx || string.Compare(args[i], "/buildresx", true) == 0;
                buildstrings = buildstrings || string.Compare(args[i], "/buildstrings", true) == 0;
                buildattrstrings = buildattrstrings || string.Compare(args[i], "/buildattrstrings", true) == 0;
                buildformstrings = buildformstrings || string.Compare(args[i], "/buildformstrings", true) == 0;
            }
            buildstrings = buildresx || buildstrings;
            buildattrstrings = buildresx || buildattrstrings;
            buildformstrings = buildresx || buildformstrings;

            //
            Console.WriteLine("Analyse assembly localization...");
            if (buildresx)
                Console.WriteLine("Build default resources...");
            Console.WriteLine("");

            //
            string assemblyNames = args[0].Trim();
            string culturesNames = args[1].Trim();
            string stringsTypesNames = args[2].Trim();

            // 
            if (string.IsNullOrEmpty(culturesNames))
            {
                Console.WriteLine("Cultures not specified");
                Environment.ExitCode = (int)ExitCodeEnum.AllowedCulturesInvalid;
                return;
            }

            // parse list of cultures
            List<string> cultures = new List<string>();
            string[] parts = culturesNames.Split(',');
            foreach (string s in parts)
                cultures.Add(s.Trim());

            string[] files = assemblyNames.Split(',');
            List<Assembly> assemblies = new List<Assembly>();
            List<Type> types = new List<Type>();
            for (int i = 0; i < files.Length; i++)
            {
                //
                if (!File.Exists(files[i]))
                {
                    Console.WriteLine(string.Format("File [{0}] does not exist", files[i]));
                    Environment.ExitCode = (int)ExitCodeEnum.AssemblyFileNameNotFound;
                    return;
                }

                // load assembly
                Assembly assembly;
                string curDirectory = Path.GetDirectoryName(files[i]);
                if (!string.IsNullOrEmpty(curDirectory))
                    Directory.SetCurrentDirectory(curDirectory);
                try
                {
                    assembly = Assembly.UnsafeLoadFrom(files[i]);
                    assemblies.Add(assembly);
                }
                catch (Exception e)
                {
                    assembly = null;
                    Console.WriteLine(string.Format("Error during loading assembly:\r\n{0}\r\nError message:\r\n{1}", files[i], e.Message));
                    Environment.ExitCode = (int)ExitCodeEnum.CantLoadAssembly;
                    return;
                }

                // get types from assembly
                Type[] tps;
                try
                {
                    tps = assembly.GetTypes();
                }
                catch (ReflectionTypeLoadException ex)
                {
                    tps = ex.Types;
                }
                catch
                {
                    Console.WriteLine(string.Format("Can not get list of types from assembly [{0}].", files[i]));
                    Environment.ExitCode = (int)ExitCodeEnum.CantGetTypes;
                    return;
                }
                types.AddRange(tps);
            }

            // search the string types in assembly
            string[] typeNames = stringsTypesNames.Split(',');
            List<Type> stringsTypes = new List<Type>();
            foreach (string s in typeNames)
            {
                // search a type
                Type type = LocalizationChecker.FindTypeByFullName(types, s);
                if (type == null)
                {
                    Console.WriteLine(string.Format("Type [{0}] is not found.", s));
                    Environment.ExitCode = (int)ExitCodeEnum.StringsClassNotFound;
                    return;
                }
                stringsTypes.Add(type);
            }

            //
            Console.WriteLine("Assembly        : " + files[0]);
            for (int i = 1; i < files.Length; i++)
                Console.WriteLine("                  " + files[i]);
            Console.WriteLine("Allowed cultures: " + culturesNames);
            Console.WriteLine("Types           : " + typeNames[0]);
            for (int i = 1; i < typeNames.Length; i++)
                Console.WriteLine("                  " + typeNames[i]);
            Console.WriteLine("");

            // loading license keys for types
            Dictionary<string, string> licKeys = new Dictionary<string, string>();
            string keysFile = System.Windows.Forms.Application.ExecutablePath + ".keys";
            if (File.Exists(keysFile))
            {
                using (FileStream fs = new FileStream(keysFile, FileMode.Open, FileAccess.Read))
                using (StreamReader sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        string l = sr.ReadLine();
                        if (string.IsNullOrEmpty(l))
                            continue;

                        string[] ss = l.Split(' ');
                        if (ss.Length != 2)
                            continue;

                        licKeys.Add(ss[0].Trim(), ss[1].Trim());
                    }
                }
            }

            //
            LocalizationChecker lc = new LocalizationChecker();
            LocalizationCheckerResult result = lc.ProcessResources(
                assemblies,
                types,
                cultures,
                stringsTypes,
                licKeys,
                LocalizationCheckerOptions.None,
                buildresx);

            //
            if (buildformstrings | buildattrstrings | buildstrings)
            {
                // write resources with default values
                foreach (KeyValuePair<Type, Dictionary<string, ResourceString>> p in lc.StringsCache.Data)
                {
                    string fileName = Path.Combine(curDir, p.Key.FullName + ".resx");
                    Console.WriteLine(string.Format("Writing {0}", fileName));
                    using (ResXResourceWriter rw = new ResXResourceWriter(fileName))
                    {
                        // sort items in the p.Value
                        List<string> sortedKeys = new List<string>();
                        foreach (KeyValuePair<string, ResourceString> str in p.Value)
                        {
                            if (str.Key.StartsWith("Forms."))
                            {
                                if (buildformstrings)
                                    sortedKeys.Add(str.Key);
                            }
                            else if (str.Key.StartsWith("Attributes."))
                            {
                                if (buildattrstrings)
                                    sortedKeys.Add(str.Key);
                            }
                            else
                            {
                                if (buildstrings)
                                    sortedKeys.Add(str.Key);
                            }
                        }
                        sortedKeys.Sort();
                        foreach (string key in sortedKeys)
                        {
                            ResXDataNode dn = new ResXDataNode(key, p.Value[key].DefaultValue);
                            dn.Comment = p.Value[key].DefaultValue;
                            rw.AddResource(dn);
                        }
                    }
                }
            }

            //
            if (result.Messages.Length > 0)
            {
                Console.WriteLine("Errors found during checking:");
                Console.WriteLine("");
                Console.WriteLine(string.Format("Count of resources strings which are not used in assembly: {0}", result.NotUsedResourceStringCount));
                Console.WriteLine(string.Format("Count of strings not found in resources: {0}", result.NotFoundInResourcesCount));
                if (result.Messages.Length > 0)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Messages:");
                    Console.WriteLine("");
                    Console.WriteLine(result.Messages.ToString());
                }
                Console.ReadLine();
                Environment.ExitCode = (int)ExitCodeEnum.ErrorsExists;
                return;
            }

            //
            Environment.ExitCode = (int)ExitCodeEnum.Success;
        }
    }
}
