This application can be used to check the localizaiton
of compiled assemblies. It performs:

1. Check all strings in Strings class - they should
   have a strings in the resx file.
   
2. Check all strings in the Forms section of resx file - 
   the assembly should have controls or forms with these strings.
   
3. Check all strings in the [Attributes] section of resx file -
   the assembly should have the attributes referencing these
   strings.

4. Check all forms and controls within assembly -
   they should have the strings in the resources.

Expected parameters:
1. Names of the assemblies to check delimited with ','
2. List of cultures delimited with ',' (e.g. "ja,ko")
3. Comma-separated (without spaces) list of namespaces to check, e.g. "C1.C1Report.Strings,C1.C1Preview.Strings"
4 (optional): /buildresx - if specified, resx files with default english langugage
  will be generated in the current directory.

Note: for assemblies built for a specific target, use corresponding
checklocalization version:
- "Any CPU": checklocalization.exe
- "x86": checklocalization32.exe
- "x64": checklocalization64.exe
