using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Globalization;
using System.Resources;

namespace C1.Win.Localization.Design
{
    public static class Utils
    {
        public const string c_EndUserLocalizeOptionsAttributeTypeName = "EndUserLocalizeOptionsAttribute";

        #region Public static

        public static object GetEndUserLocalizeOptionsAttribute(object[] attributes)
        {
            foreach (object attr in attributes)
                if (attr.GetType().Name == c_EndUserLocalizeOptionsAttributeTypeName)
                    return attr;
            return null;
        }

        public static bool GetExclude(object endUserLocalizeOptionsAttribute)
        {
            return (bool)endUserLocalizeOptionsAttribute.GetType().GetProperty("Exclude").GetValue(endUserLocalizeOptionsAttribute, null);
        }

        public static string[] GetProperties(object endUserLocalizeOptionsAttribute)
        {
            return (string[])endUserLocalizeOptionsAttribute.GetType().GetProperty("Properties").GetValue(endUserLocalizeOptionsAttribute, null);
        }

        public static bool ParseCultures(string culturesNames, bool dumpOnConsole, out List<CultureInfo> cultures)
        {
            cultures = new List<CultureInfo>();
            string[] parts = culturesNames.Split(',');
            foreach (string s in parts)
            {
                CultureInfo ci = CultureInfo.GetCultureInfo(s);
                if (ci == null)
                {
                    if (dumpOnConsole)
                        Console.WriteLine(string.Format("Culture [{0}] is not found", s));
                }
                else
                    cultures.Add(ci);
            }
            return cultures.Count == parts.Length;
        }

        #endregion
    }
}
