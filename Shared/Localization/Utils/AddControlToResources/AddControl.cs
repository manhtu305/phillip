using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Windows.Forms;
using System.Reflection;

using C1.C1Preview.Design.Localization;

namespace AddControlToResources
{
    public static class AddControl
    {
        #region Private

        private static void AddProperties(object control, Type controlType, ResXResourceWriter w, List<CultureInfo> allowedCultures, string prefix)
        {
            string[] properties = ControlLocalizeRules.GetLocalizedProperties(controlType, null);
            if (properties == null || properties.Length == 0)
                return;

            foreach (string property in properties)
            {
                // try to get the default property value
                PropertyInfo pi = controlType.GetProperty(property, BindingFlags.Instance | BindingFlags.Public);
                if (pi != null && pi.CanRead && pi.PropertyType == typeof(string))
                {
                    string defaultValue = pi.GetValue(control, null) as string;
                    if (defaultValue != null && defaultValue.Length > 0)
                    {
                        foreach (CultureInfo ci in allowedCultures)
                            w.AddResource(string.Format("{0}.{1}.{2}", ci.Name, prefix, pi.Name), defaultValue);
                    }
                }
            }
        }

        #endregion

        #region Public static

        public static void AddControlType(Type controlType, ResXResourceWriter w, List<CultureInfo> allowedCultures)
        {
            // try to create a control
            ConstructorInfo ci = controlType.GetConstructor(new Type[0]);
            if (ci == null)
                return;

            using (Control control = (Control)ci.Invoke(null))
            {
                // get the control localizable properties
                AddProperties(control, controlType, w, allowedCultures, "Forms." + controlType.Name);

                FieldInfo[] fields = controlType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
                foreach (FieldInfo fi in fields)
                {
                    // by default we localize only protected members
                    if (!fi.IsFamily && !fi.IsPublic)
                        continue;

                    string fieldName = fi.Name.StartsWith("m_") ? fi.Name.Substring(2) : fi.Name;
                    AddProperties(fi.GetValue(control), fi.FieldType, w, allowedCultures, "Forms." + controlType.Name + "." + fieldName);
                }
            }
        }

        #endregion
    }
}
