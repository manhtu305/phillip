﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Windows.Forms;

using C1.C1Preview.Design.Localization;

namespace AddControlToResources
{
    public enum ExitCodeEnum
    {
        Success = 0,
        InvalidParameters = 1,
        AssemblyFileNameNotFound = 2,
        ResxFileNameNotFound = 3,
        AllowedCulturesInvalid = 4,
        CantLoadAssembly = 5,
        CantFindControlType = 6,
        InvalidControlType = 7,
    }

    class Program
    {
        static int Main(string[] args)
        {
            Console.WriteLine("AddControlToResources");
            Console.WriteLine("------------------");
            Console.WriteLine("Adds the form or control of assembly to the StringsRes.resx file");
            Console.WriteLine("");
            if (args.Length != 3 && args.Length != 4)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine("AddControlToResources AssemblyFileName ResourcesFileName ControlTypeFullNam [AllowedCultures]");
                return (int)ExitCodeEnum.InvalidParameters;
            }

            string assemblyFileName = args[0];
            string resxFileName = args[1];
            string controlTypeName = args[2];
            string allowedCulturesNames = args.Length > 3 ? args[3] : "ja";

            if (!File.Exists(assemblyFileName))
            {
                Console.WriteLine(string.Format("File [{0}] does not exist", assemblyFileName));
                return (int)ExitCodeEnum.AssemblyFileNameNotFound;
            }

            if (!File.Exists(resxFileName))
            {
                Console.WriteLine(string.Format("File [{0}] does not exist", resxFileName));
                return (int)ExitCodeEnum.ResxFileNameNotFound;
            }

            Assembly assembly;
            //Directory.SetCurrentDirectory(Path.GetDirectoryName(assemblyFileName));
            try
            {
                assembly = Assembly.LoadFrom(assemblyFileName);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Error during loading assembly:\r\n{0}\r\nError message:\r\n{1}", assemblyFileName, e.Message));
                return (int)ExitCodeEnum.CantLoadAssembly;
            }

            // parse list of cultures
            List<CultureInfo> allowedCultures = new List<CultureInfo>();
            if (!Utils.ParseCultures(allowedCulturesNames, true, out allowedCultures))
                return (int)ExitCodeEnum.AllowedCulturesInvalid;

            Type controlType = assembly.GetType(controlTypeName);
            if (controlType == null)
            {
                Console.WriteLine(string.Format("The type [{0}] does not exists in assembly.", controlTypeName));
                return (int)ExitCodeEnum.CantFindControlType;
            }

            if (!typeof(Control).IsAssignableFrom(controlType))
            {
                Console.WriteLine(string.Format("The type [{0}] does not derived from System.Windows.Forms.Control.", controlTypeName));
                return (int)ExitCodeEnum.InvalidControlType;
            }

            //
            Console.WriteLine("Assembly        : " + assemblyFileName);
            Console.WriteLine("ResX file       : " + resxFileName);
            Console.WriteLine("Control type    : " + controlTypeName);
            Console.WriteLine("Allowed cultures: " + allowedCulturesNames);
            Console.WriteLine("");

            string bakResFileName = resxFileName + ".bak";
            if (File.Exists(bakResFileName))
                File.Delete(bakResFileName);
            File.Move(resxFileName, bakResFileName);
            using (ResXResourceWriter w = new ResXResourceWriter(resxFileName))
            {
                using (ResXResourceReader r = new ResXResourceReader(bakResFileName))
                {
                    foreach (DictionaryEntry de in r)
                        w.AddResource(de.Key as string, de.Value);
                }
                // copy existing file
                AddControl.AddControlType(controlType, w, allowedCultures);
            }

            return (int)ExitCodeEnum.Success;
        }
    }
}
