using System;
using System.Resources;
using System.Globalization;
using System.Reflection;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    /// <summary>
    /// Contains localizable design-time strings.
    /// </summary>
	[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class DesignLocalizationStrings
	{
		#region Strings used in design-time by the localizer
        /// <summary>
        /// Gets the "Warning" string.
        /// </summary>
		public static string Warning
		{
			get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "Warning"); }
		}

        /// <summary>
        /// Gets the "Question" string.
        /// </summary>
        public static string Question
		{
			get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "Question"); }
		}

        /// <summary>
        /// Gets the "None" string.
        /// </summary>
        public static string None
		{
			get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "None"); }
		}

        /// <summary>
        /// Gets the "Error" string.
        /// </summary>
        public static string Error
		{
			get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "Error"); }
		}

        /// <summary>
        /// Contains localizable design-time strings for the root group.
        /// </summary>
        public static class RootResourceGroup
        {
            /// <summary>
            /// Gets the "overwrite a readonly file?" format string.
            /// </summary>
            public static string ReadOnlyFile
            {
                get
                {
                    return StringsManager.GetString(MethodBase.GetCurrentMethod(),
                        "File [{0}] is readonly, overwrite?");
                }
            }

            /// <summary>
            /// Contains error strings.
            /// </summary>
            public static class Errors
            {
                /// <summary>
                /// Gets the "errors while saving resources" format string.
                /// </summary>
                public static string SaveToFile
                {
                    get
                    {
                        return StringsManager.GetString(MethodBase.GetCurrentMethod(),
                            "Error occurred when saving resources to file:\r[{0}]\rException message:\r[{1}]");
                    }
                }
            }
        }

        /// <summary>
        /// Contains strings designer strings.
        /// </summary>
		public static class StringsDesigner
		{
            /// <summary>
            /// Gets the "All" string.
            /// </summary>
			public static string AllStringsCaption
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(), "All");
				}
			}

            /// <summary>
            /// Contains error strings.
            /// </summary>
			public static class Errors
			{
                /// <summary>
                /// Gets the "Localizer can be used under MS Visual Studio only." string.
                /// </summary>
				public static string CantInitializeDte
				{
					get
					{
						return StringsManager.GetString(MethodBase.GetCurrentMethod(), "Localizer can be used under MS Visual Studio only.");
					}
				}
			}
		}

        /// <summary>
        /// Contains localizer strings.
        /// </summary>
		public static class Localizer
		{
            /// <summary>
            /// Gets the "Localize..." string.
            /// </summary>
			public static string DesignActionCaption
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"Localize...");
				}
			}

            /// <summary>
            /// Gets the "Culture [{0}] already added to the current translation." format string.
            /// </summary>
			public static string CultureAlreadyExists
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"Culture [{0}] already added to the current translation.");
				}
			}

            /// <summary>
            /// Gets the "Exception occurs during loading strings from resource file:\r{0}\rException message:\r{1}" format string.
            /// </summary>
            public static string LoadResXStringsException
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"Exception occurs during loading strings from resource file:\r{0}\rException message:\r{1}");
				}
			}

            /// <summary>
            /// Gets the "The translation has been changed, save?" string.
            /// </summary>
            public static string SaveQuestion
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"The translation has been changed, save?");
				}
			}

            /// <summary>
            /// Gets the "Select product to translate" string.
            /// </summary>
            public static string NewTranslation
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"Select product to translate");
				}
			}

            /// <summary>
            /// Gets the "Select project to store translation" string.
            /// </summary>
            public static string SaveAsDialogCaption
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"Select project to store translation");
				}
			}

            /// <summary>
            /// Gets the "Resource image with name [{0}] is not found." format string.
            /// </summary>
            public static string ImageResourceNotFound
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"Resource image with name [{0}] is not found.");
				}
			}

            /// <summary>
            /// Gets the "Invariant culture" string.
            /// </summary>
            public static string InvariantCultureDescription
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"Invariant culture");
				}
			}

            /// <summary>
            /// Gets the "You have selected {0} culture to delete, there are {1} strings of this culture. Are you sure?" format string.
            /// </summary>
            public static string DeleteCultureWarning
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"You have selected {0} culture to delete, there are {1} strings of this culture. Are you sure?");
				}
			}

            /// <summary>
            /// Gets the "You have selected {0} cultures to delete, there are {1} strings of these cultures. Are you sure?" format string.
            /// </summary>
            public static string DeleteCulturesWarning
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"You have selected {0} cultures to delete, there are {1} strings of these cultures. Are you sure?");
				}
			}

            /// <summary>
            /// Gets the "Can't create the [{0}] folder in the project.\rError message:\r{1}" format string.
            /// </summary>
            public static string CantCreateLocalizedFolder
			{
				get
				{
					return StringsManager.GetString(MethodBase.GetCurrentMethod(),
						"Can't create the [{0}] folder in the project.\rError message:\r{1}");
				}
			}

            /// <summary>
            /// Contains the open translation dialog strings.
            /// </summary>
            public static class OpenTranslationDialog
			{
                /// <summary>
                /// Gets the "Solution '{0}'" format string.
                /// </summary>
                public static string SolutionMask
				{
					get
					{
						return StringsManager.GetString(MethodBase.GetCurrentMethod(),
							"Solution '{0}'");
					}
				}

                /// <summary>
                /// Gets the "Can't build list of cultures" string.
                /// </summary>
                public static string NoCultures
				{
					get
					{
						return StringsManager.GetString(MethodBase.GetCurrentMethod(),
							"Can't build list of cultures");
					}
				}
			}
		}
		#endregion
	}
}
