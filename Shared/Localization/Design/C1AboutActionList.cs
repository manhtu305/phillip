using System;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;

using C1.Win.Localization;
using C1.Util.Licensing;

namespace C1.Util.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class C1AboutActionList : DesignerActionList
    {
        private object _c1product;
        // for display, "" or null means use default
        private string _display;
        // for description (tooltip), "" means do not show, null means use default.
        private string _description;

        public C1AboutActionList(IComponent component, object c1product)
            : base(component)
        {
            _c1product = c1product;
        }

        public C1AboutActionList(IComponent component, object c1product, string display, string description)
            : base(component)
        {
            _c1product = c1product;
            _display = display;
            _description = description;
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            if (_c1product == null)
                return null;

            string display = string.IsNullOrEmpty(_display) ? C1AboutActionListStrings.About : _display;
            string description = _description == null ?
                string.Format(C1AboutActionListStrings.Description,
                ((AssemblyProductAttribute)Attribute.GetCustomAttribute(Assembly.GetExecutingAssembly(),
                typeof(AssemblyProductAttribute))).Product) :
                _description;

            DesignerActionItemCollection items = new DesignerActionItemCollection();
            items.Add(new DesignerActionMethodItem(this, "ShowAboutBox", display, "About", description, true));
            return items;
        }

        public void ShowAboutBoxEventHandler(object sender, EventArgs e)
        {
            ShowAboutBox();
        }

        public void ShowAboutBox()
        {
            C1.Util.Licensing.ProviderInfo.ShowAboutBox(_c1product);
        }
    }

    public static class C1AboutActionListStrings
    {
        public static string About
        {
            get
            {
                return StringsManager.GetString(MethodBase.GetCurrentMethod(),
                    "About...");
            }
        }

        public static string Description
        {
            get
            {
                return StringsManager.GetString(MethodBase.GetCurrentMethod(),
                    "About {0}...");
            }
        }
    }
}
