#if DEBUG
// #define DBG_DESIGN
#endif

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Reflection;
using System.IO;
using System.Globalization;
using System.Resources;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class StringsDesigner : Form//, IDisposable
    {
#if DBG_DESIGN
		private static string cDebugFileNameMask;
#endif
        private const string c_PropsStoreKey = "C1Localizer\\StringsDesigner";
        private static MethodInfo s_GetService;

        private DTE _dte;
        private int _lockCount;
        private ComponentDesigner _componentDesigner;
        private ProjectRefCollection _projects = new ProjectRefCollection();
        private ProductCollection _products = new ProductCollection();
        private RootResourceGroup _root = new RootResourceGroup();
        private List<ResourceString> _listStrings = new List<ResourceString>();
        private ResourceGroup _lastGroup;
        private ProjectRef _currentProject;
        private Product _currentProduct;
        private bool _changed = false;
        private CultureInfo _currentCulture;
        private bool _valueWasChanged;
        private bool _showSelectedGroupOnly = false;
        private bool _syncTreeWithGrid = false;
        private bool _showNamesInGrid = true;

        // this domain is used to load assemblies to it
        private AppDomain _domain;
        private string _domainAssembliesPath;

        public StringsDesigner()
        {
#if DBG_DESIGN
            s_FileNumber++;
#endif
            InitializeComponent();
            StringsManager.LocalizeControl(typeof(DesignLocalizationStrings), this, GetType());

            _domain = AppDomain.CreateDomain("TemporaryDomain");
            // create directory
            _domainAssembliesPath = Path.GetTempFileName();
            File.Delete(_domainAssembliesPath);
            Directory.CreateDirectory(_domainAssembliesPath);
        }

        static StringsDesigner()
        {
            s_GetService = typeof(ComponentDesigner).GetMethod("GetService", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            System.Diagnostics.Debug.Assert(s_GetService != null);

#if DBG_DESIGN
			cDebugFileNameMask = string.Format("c:\\C1PreviewDesign_{0}_{{0}}.log", Guid.NewGuid().ToString());
#endif
        }

        #region Public static

        public static void DoOpenDesigner(ComponentDesigner componentDesigner)
        {
            using (StringsDesigner f = new StringsDesigner())
                f.OpenDesigner(componentDesigner);
        }

        #endregion

        #region Protected

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = !CheckSaved();
            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            RegistryPropsStore rps = new RegistryPropsStore(c_PropsStoreKey);
            rps.SaveFormState(this);
            rps.SetInt("SplitterDistance", m_splitPanel1.Width);
            rps.SetBoolNoResolution("SyncTreeWithGrid", SyncTreeWithGrid);
            rps.SetBoolNoResolution("ShowNamesInGrid", ShowNamesInGrid);
            rps.SetBoolNoResolution("ShowSelectedGroupOnly", ShowSelectedGroupOnly);
            for (int i = 0; i < m_dgvList.Columns.Count; i++)
                rps.SetInt("ColumnWidth" + i.ToString(), m_dgvList.Columns[i].Width);

            if (_products != null)
            {
                foreach (Product product in _products)
                    product.Dispose();
                _products = null;
            }
            if (_projects != null)
            {
                foreach (ProjectRef pr in _projects)
                    pr.Dispose();
                _projects = null;
            }
            base.OnClosed(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            RegistryPropsStore rps = new RegistryPropsStore(c_PropsStoreKey);
            rps.RestoreFormState(this);
            m_splitPanel1.Width = rps.GetInt("SplitterDistance", m_splitPanel1.Width);
            SyncTreeWithGrid = rps.GetBoolNoResolution("SyncTreeWithGrid", SyncTreeWithGrid);
            ShowNamesInGrid = rps.GetBoolNoResolution("ShowNamesInGrid", ShowNamesInGrid);
            ShowSelectedGroupOnly = rps.GetBoolNoResolution("ShowSelectedGroupOnly", ShowSelectedGroupOnly);
            for (int i = 0; i < m_dgvList.Columns.Count; i++)
                m_dgvList.Columns[i].Width = rps.GetInt("ColumnWidth" + i.ToString(), m_dgvList.Columns[i].Width);

            BeginUpdate();
            m_tsmiSyncTreeWithGrid.Checked = _syncTreeWithGrid;
            m_tsmiShowNamesInGrid.Checked = _showNamesInGrid;
            m_tsmiShowSelectedGroupOnly.Checked = _showSelectedGroupOnly;
            EndUpdate();

            base.OnLoad(e);
        }

        #endregion

        #region Private

        private void OnOpenDropDownClick(object sender, EventArgs e)
        {
            OpenDropDownItem oddi = ((ToolStripMenuItem)sender).Tag as OpenDropDownItem;
            if (oddi == null)
                return;
            if (!CheckSaved())
                return;
            Open(oddi.Product, oddi.Project);
        }

        private void OnValueChanged(object sender, EventArgs e)
        {
            Changed = true;
        }

        private void BeginUpdate()
        {
            _lockCount++;
        }

        private void EndUpdate()
        {
            _lockCount--;
        }

        private new object GetService(Type type)
        {
            return s_GetService.Invoke(_componentDesigner, new object[] { type });
        }

#if DBG_DESIGN
		private static int s_FileNumber;

        private static void DebugEnterMethod(string methodName)
        {
            DebugLine("^^Enter - " + methodName);
        }

        private static void DebugExitMethod(string methodName)
        {
            DebugLine("^^Exit - " + methodName);
        }

        private static void DebugLine(string mask, params object[] args)
		{
			string s = string.Format(cDebugFileNameMask, s_FileNumber);
            using (FileStream fs = new FileStream(s, FileMode.Append))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(DateTime.Now.ToString() + "\t" + string.Format(mask, args));
					sw.Flush();
				}
            }
		}

		private static void DebugStartLine(string lineText)
		{
			DebugLine(">> " + lineText);
		}

		private static void DebugValue(string valueName, object value)
		{
			if (value == null)
				DebugLine(string.Format("   [{0}]   returns null", valueName));
			else
			{
				string typeName = string.Empty;
				string stringValue = string.Empty;
				try
				{
					typeName = value.GetType().Name;
				}
				catch (Exception e)
				{
					typeName = string.Format("Can't get type, exception: {0}", e.Message);
				}
				finally
				{
				}

				try
				{
					stringValue = value.ToString();
				}
				catch (Exception e)
				{
					stringValue = string.Format("Can't get value as string, exception: {0}", e.Message);
				}
				finally
				{
				}

				DebugLine(string.Format("   [{0}]   Type: {1} Value: {2}", valueName, typeName, stringValue));
			}
		}
#endif

        private void BuildProductsAndProjects()
        {
#if DBG_DESIGN
            DebugEnterMethod("BuildProductsAndProjects");
#endif

#if DBG_DESIGN
            DebugStartLine("Array startUpProjects = _dte.Solution.SolutionBuild.StartupProjects as Array;");
			DebugValue("_dte", _dte);
			DebugValue("_dte.Solution", _dte.Solution);
			DebugValue("_dte.Solution.SolutionBuild", _dte.Solution.SolutionBuild);
			DebugValue("_dte.Solution.SolutionBuild.StartupProjects", _dte.Solution.SolutionBuild.StartupProjects);
#endif
			Array startUpProjects = _dte.Solution.SolutionBuild.StartupProjects as Array;

#if DBG_DESIGN
            DebugStartLine("int projectCount = _dte.Solution.Projects.Count;");
			DebugValue("_dte.Solution.Projects", _dte.Solution.Projects);
			DebugValue("_dte.Solution.Projects.Count", _dte.Solution.Projects.Count);
#endif
			Projects projects = _dte.Solution.Projects;
			int projectCount = projects.Count;
            List<string> loadedAssemblies = new List<string>();
            for (int i = 1; i <= projectCount; i++)
            {
				Project project = projects[i];
#if DBG_DESIGN
                DebugStartLine("if (project == null || project.Object == null)");
                DebugValue("project.Object", project.Object);
#endif
                if (!project.IsValid || project.Object == null || !project.ProjectItems.IsValid)
                    continue;

                string projectUniqueName = project.UniqueName;
                if (projectUniqueName == null || projectUniqueName.Length <= 0)
                    continue;

                // determine is project StartUp or not
                bool startUp = false;
                if (startUpProjects != null)
                    foreach (string pun in startUpProjects)
                    {
                        if (projectUniqueName == pun)
                        {
                            startUp = true;
                            break;
                        }
                    }

#if DBG_DESIGN
                DebugStartLine("ProjectRef pr = new ProjectRef(project, startUp);");
				DebugValue("project", project);
#endif
				ProjectRef pr = new ProjectRef(project, startUp);
                _projects.Add(pr);

                // iterate all project references
#if DBG_DESIGN
                DebugStartLine("if (project.IsLangProject)");
				DebugValue("project.IsLangProject", project.IsLangProject);
#endif
				if (project.IsLangProject)
                {
#if DBG_DESIGN
                    DebugStartLine("int referenceCount = project.References.Count;");
					DebugValue("project.References", project.References);
					DebugValue("project.References.Count", project.References.Count);
#endif
					References references = project.References;
					int referenceCount = references.Count;
                    for (int j = 1; j <= referenceCount; j++)
                    {
						Reference reference = references[j];
#if DBG_DESIGN
                        DebugStartLine("if (!reference.Type.Equals(DTE.prjReferenceTypeAssembly))");
						DebugValue("reference", reference);
						DebugValue("reference.Type", reference.Type);
#endif
						if (!reference.IsValid || !reference.Type.Equals(DTE.prjReferenceTypeAssembly))
                            continue;
#if DBG_DESIGN
                        DebugStartLine("if (!File.Exists(reference.Path))");
						DebugValue("reference.Path", reference.Path);
#endif
						if (!File.Exists(reference.Path))
                            continue;

                        // assemblt name should start with "c1."
                        if (!Path.GetFileName(reference.Path).ToLower().StartsWith("c1."))
                            continue;

                        //
                        bool alreadyLoaded = false;
                        foreach (string s in loadedAssemblies)
                            if (string.Compare(s, reference.Path) == 0)
                            {
                                alreadyLoaded = true;
                                break;
                            }
                        if (alreadyLoaded)
                            continue;
                        loadedAssemblies.Add(reference.Path);

                        // load an assembly
                        Assembly assembly;
						try
						{
                            assembly = _domain.Load(AssemblyName.GetAssemblyName(reference.Path));
#if DBG_DESIGN
                            System.Diagnostics.Debug.WriteLine("LOADED: " + reference.Path);
#endif
						}
#if DBG_DESIGN
						catch (Exception e)
#else
                        catch (Exception)
#endif
						{
#if DBG_DESIGN
							DebugLine("**Exception while load assembly: {0}", e.Message);
#endif
							assembly = null;
						}
                        if (assembly == null)
                            continue;

                        // now we should check is assembly - OUR C1 assembly or not
                        // also this assembly should contain the Strings class
                        Type[] types;
                        try
                        {
                            types = assembly.GetTypes();
                        }
                        catch (ReflectionTypeLoadException e)
                        {
                            types = e.Types;
                        }
#if DBG_DESIGN
                        catch (Exception e)
#else
                        catch (Exception)
#endif
                        {
#if DBG_DESIGN
							DebugLine("**Exception when assembly.GetTypes(): {0}", e.Message);
#endif
							types = null;
                        }
						if (types == null)
							continue;

#if DBG_DESIGN
						try
						{
#endif
                        Type stringsClassType = null;
                        foreach (Type type in types)
                        {
                            if (type == null || !type.IsClass || type.Name != Utils.c_StringsClassName)
                                continue;
                            // this class should contain static ResourceManager property
                            PropertyInfo pi = type.GetProperty(Utils.c_ResourceManagerPropertyName, BindingFlags.Public | BindingFlags.Static);
                            if (pi == null || !pi.CanRead || !pi.CanWrite || pi.PropertyType != typeof(ResourceManager))
                                continue;

                            stringsClassType = type;
                            break;
                        }

						if (stringsClassType == null)
						{
#if DBG_DESIGN
							DebugLine("--Strings type is NOT found in assembly: {0}", assembly.GetName().Name);
#endif
							continue;
						}

#if DBG_DESIGN
						DebugLine("++Strings type {0} is found in assembly: {1}", stringsClassType.FullName, assembly.GetName().Name);
#endif

                        AssemblyName assemblyName = assembly.GetName();
                        Product product = _products.FindByAssemblyName(assemblyName.Name);
                        if (product != null)
                        {
                            // the specified assembly already exists, compare the AssemblyVersion
                            // and select the newer
                            if (product.Assembly.GetName().Version < assemblyName.Version)
                            {
                                product.Assembly = assembly;
                                product.StringsClassType = stringsClassType;
                            }
                        }
                        else
                        {
                            // initialize the Product object
                            product = new Product(reference, assembly, stringsClassType);
                            _products.Add(product);
                        }
                        if (!product.Projects.Contains(pr))
                            product.Projects.Add(pr);
                        if (!pr.Products.Contains(product))
                            pr.Products.Add(product);
#if DBG_DESIGN
					}
						catch (Exception e)
						{
							DebugLine("**Exception when search Strings type: {0}", e.Message);
						}
#endif
					}
				}
            }

#if DBG_DESIGN
            DebugExitMethod("BuildProductsAndProjects");
#endif
        }

        private void UpdateStatusBarCurrentProjectProduct()
        {
#if DBG_DESIGN
            DebugEnterMethod("UpdateStatusBarCurrentProjectProduct");
#endif
            m_tsslCurrentProject.Text = _currentProject == null ? DesignLocalizationStrings.None : _currentProject.Project.UniqueName;
			m_tsslCurrentProduct.Text = _currentProduct == null ? DesignLocalizationStrings.None : _currentProduct.Assembly.GetName().Name;
#if DBG_DESIGN
            DebugExitMethod("UpdateStatusBarCurrentProjectProduct");
#endif
        }

        private TreeNode AddResourceNode(TreeNodeCollection nodes, ResourceItem ri)
        {
            string nodeImageKey;
            string nodeText;
            ri.GetNodeParams(m_imageList, out nodeText, out nodeImageKey);

            object tag;
            ControlResourceGroup crg = ri as ControlResourceGroup;
            if (crg != null && crg.Children.Count == 1)
            {
                string tempNodeImageKey;
                string childNodeText;
                crg.Children[0].GetNodeParams(m_imageList, out childNodeText, out tempNodeImageKey);

                nodeText += "." + childNodeText;
                tag = crg.Children[0];
            }
            else
                tag = ri;

            TreeNode result = new TreeNode(nodeText);
            result.ImageKey = nodeImageKey;
            result.SelectedImageKey = nodeImageKey;
            result.Tag = tag;
            nodes.Add(result);

            return result;
        }

        private void AddResourceItems(TreeNodeCollection nodes, ResourceItemCollection items)
        {
            foreach (ResourceItem ri in items)
            {
                TreeNode node = AddResourceNode(nodes, ri);
                ResourceGroup rg = ri as ResourceGroup;
                if (rg != null && !(rg is ControlResourceGroup && rg.Children.Count == 1))
                    AddResourceItems(node.Nodes, rg.Children);
            }
        }

        private void UpdateStrings()
        {
#if DBG_DESIGN
            DebugEnterMethod("UpdateString");
#endif
            m_tvStrings.BeginUpdate();
            m_tvStrings.Nodes.Clear();

            TreeNode node = AddResourceNode(m_tvStrings.Nodes, _root);
            AddResourceItems(node.Nodes, _root.Children);

            m_tvStrings.SelectedNode = node;
            node.Expand();

            m_tvStrings.EndUpdate();
#if DBG_DESIGN
            DebugExitMethod("UpdateString");
#endif
        }

        private void UpdateList(TreeNode node)
        {
            ResourceGroup rg = null;
            if (!_showSelectedGroupOnly)
                rg = _root;
            else if (node != null)
            {
                rg = node.Tag as ResourceGroup;
                if (rg == null)
                {
                    ResourceItem ri = node.Tag as ResourceItem;
                    if (ri != null)
                    {
                        rg = ri.Parent as ResourceGroup;
                        if (rg is ControlResourceGroup && rg.Children.Count == 1)
                            rg = rg.Parent as ResourceGroup;
                    }
                }
            }
            if (_lastGroup == rg)
                return;
            _lastGroup = rg;
            _listStrings.Clear();
            if (rg == null)
                m_lblList.Text = string.Empty;
            else
            {
                m_lblList.Text = rg.Name;
                rg.GetStrings(_listStrings);
            }

            m_dgvList.Rows.Clear();
            if (_listStrings.Count > 0)
                m_dgvList.Rows.Add(_listStrings.Count);
        }

        private void UpdateToolStrip()
        {
#if DBG_DESIGN
            DebugEnterMethod("UpdateToolStrip");
#endif
            m_tsbSave.Enabled = Changed;
            m_tsbSaveAs.Enabled = _root.Children.Count > 0;
            m_tsbDeleteCulture.Enabled = _root.Children.Count > 0 && _root.Cultures.Count > 1;
            m_tsbAddCulture.Enabled = _root.Children.Count > 0;
#if DBG_DESIGN
            DebugExitMethod("UpdateToolStrip");
#endif
        }

        private void UpdateCultures()
        {
#if DBG_DESIGN
            DebugEnterMethod("UpdateCultures");
#endif
            BeginUpdate();
            m_tscbCurrentCulture.Items.Clear();
            foreach (AdvCultureInfo aci in _root.Cultures)
                m_tscbCurrentCulture.Items.Add(aci);
            EndUpdate();
#if DBG_DESIGN
            DebugExitMethod("UpdateCultures");
#endif
        }

        private void OpenDesigner(ComponentDesigner componentDesigner)
        {
#if DBG_DESIGN
            DebugEnterMethod("OpenDesigner");
#endif
            // 0. Get IDE interfaces
            _dte = new DTE();
            if (!DTE.Initialize(componentDesigner, ref _dte))
            {
				MessageBox.Show(DesignLocalizationStrings.StringsDesigner.Errors.CantInitializeDte, DesignLocalizationStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Dispose();
                return;
            }

            _componentDesigner = componentDesigner;

            // 1. Build list of projects and products
            BuildProductsAndProjects();

            // 2. Determine the initial product
            _currentProduct = _products.FindByAssemblyName(_componentDesigner.Component.GetType().Assembly.GetName().Name);

            // 3. Determine the current project,
            // We should select project that already contains the 
            // resources for selected product.
            // Otherwise (if such project does not exist)
            // search the first startUp project
            _currentProject = null;
            foreach (ProjectRef project in _projects)
                if (project.GetResXFilesForProduct(_currentProduct) != null)
                {
                    _currentProject = project;
                    break;
                }

            if (_currentProject == null)
            {
                foreach (ProjectRef project in _projects)
                    if (project.StartUp && project.IsLangProject)
                    {
                        _currentProject = project;
                        break;
                    }
                if (_currentProject == null)
                    foreach (ProjectRef project in _projects)
                        if (project.IsLangProject)
                        {
                            _currentProject = project;
                            break;
                        }
            }

            if (_currentProduct != null)
                _root.Load(_currentProduct, _currentProject);
            if (_root.Cultures.Count <= 0)
                _root.AddCulture(CultureInfo.InvariantCulture);

            UpdateStrings();
            UpdateCultures();
            UpdateStatusBarCurrentProjectProduct();
            UpdateToolStrip();
            CurrentCulture = _root.Cultures[0].CultureInfo;

            ShowDialog();
#if DBG_DESIGN
            DebugExitMethod("OpenDesigner");
#endif
        }

        private bool CheckSaved()
        {
            if (!Changed)
                return true;

			switch (MessageBox.Show(DesignLocalizationStrings.Localizer.SaveQuestion, DesignLocalizationStrings.Question, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning))
            {
                case DialogResult.Cancel:
                    return false;
                case DialogResult.Yes:
                    Save();
                    break;
            }
            return true;
        }

        /// <summary>
        /// Create the new translation.
        /// </summary>
        private void New()
        {
            if (!CheckSaved())
                return;

			Product product = SelectProductDialog.DoSelectProduct(this, DesignLocalizationStrings.Localizer.NewTranslation, _products, _currentProduct);
            if (product == null)
                return;

            _currentProduct = product;
            _currentProject = null;
            _root.Load(_currentProduct, _currentProject);
            if (_root.Cultures.Count <= 0)
                _root.AddCulture(CultureInfo.InvariantCulture);

            UpdateStrings();
            UpdateCultures();
            UpdateStatusBarCurrentProjectProduct();
            UpdateToolStrip();
            CurrentCulture = _root.Cultures[0].CultureInfo;
            Changed = false;
        }

        private void Save()
        {
            if (_currentProject == null)
                SaveAs();
            else
            {
                if (_root.Save(_currentProduct, _currentProject))
                    Changed = false;
            }
        }

        private void SaveAs()
        {
			ProjectRef project = SelectProjectDialog.DoSelectProject(this, DesignLocalizationStrings.Localizer.SaveAsDialogCaption, _projects, _currentProject);
            if (project == null)
                return;

            _currentProject = project;
            UpdateStatusBarCurrentProjectProduct();
            if (_root.Save(_currentProduct, _currentProject))
                Changed = false;
        }

        private void Open(Product product, ProjectRef project)
        {
            _currentProduct = product;
            _currentProject = project;
            _root.Load(_currentProduct, _currentProject);

            UpdateStrings();
            UpdateCultures();
            UpdateStatusBarCurrentProjectProduct();
            UpdateToolStrip();
            CurrentCulture = _root.Cultures[0].CultureInfo;
        }

        private void Open()
        {
            if (!CheckSaved())
                return;

            Product product;
            ProjectRef project;
            if (!OpenTranslationDialog.DoOpen(this, _dte, _products, _projects, out product, out project))
                return;

            // add selected items to the drop-down list
            OpenDropDownItem oddi = new OpenDropDownItem(product, project);
            ToolStripMenuItem tsmi = new ToolStripMenuItem();
            tsmi.Text = oddi.ToString();
            tsmi.Tag = oddi;
            tsmi.Click += OnOpenDropDownClick;
            m_tssbOpen.DropDownItems.Add(tsmi);

            Open(product, project);
            Changed = false;
        }

        private void AddCulture()
        {
            CultureInfo newCulture = SelectCultureDialog.DoSelectCulture(this, null);
            if (newCulture == null)
                return;

            // check if culture already exists
            if (_root.Cultures.IndexByCultureInfo(newCulture) != -1)
            {
				MessageBox.Show(string.Format(DesignLocalizationStrings.Localizer.CultureAlreadyExists, newCulture.EnglishName), DesignLocalizationStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _root.AddCulture(newCulture);
            UpdateCultures();
            UpdateToolStrip();
            CurrentCulture = newCulture;
            Changed = true;
        }

        private void DeleteCulture()
        {
            if (_root.Cultures.Count <= 1)
                return;

            List<AdvCultureInfo> cultures = DeleteCultureDialog.DoSelectCultures(_root.Cultures);
            if (cultures == null)
                return;

            int n = 0;
            foreach (AdvCultureInfo aci in cultures)
                n += aci.TranslatedStringCount;

            if (n > 0)
            {
                string s = n == 1 ?
					string.Format(DesignLocalizationStrings.Localizer.DeleteCultureWarning, cultures[0].CultureInfo.EnglishName, n) :
					string.Format(DesignLocalizationStrings.Localizer.DeleteCulturesWarning, cultures.Count, n);
				if (MessageBox.Show(s, DesignLocalizationStrings.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return;
            }

            // delete cultures
            foreach (AdvCultureInfo aci in cultures)
                _root.DeleteCulture(aci.CultureInfo);
            UpdateCultures();
            UpdateToolStrip();
            CurrentCulture = _root.Cultures[0].CultureInfo;
            Changed = true;
        }

        private void SyncTree(ResourceString rs)
        {
            if (rs == null)
                return;

            TreeNode node = Utils.FindNodeByTag(m_tvStrings.Nodes, rs);
            if (node == null)
                return;

            m_tvStrings.SelectedNode = node;
            m_tvStrings.SelectedNode.EnsureVisible();
        }

        private void SyncGrid(TreeNode node)
        {
            ResourceString rs = node.Tag as ResourceString;
            if (rs == null)
            {
                ResourceGroup rg = node.Tag as ResourceGroup;
                if (rg == null)
                    rs = null;
                else
                {
                    List<ResourceString> strings = new List<ResourceString>();
                    rg.Children.GetStrings(strings);
                    rs = strings.Count > 0 ? strings[0] : null;
                }
            }

            int ci;
            if (m_dgvList.CurrentCell == null)
            {
                // select first visible column index
                for (ci = 0; ci < m_dgvList.Columns.Count; ci++)
                    if (m_dgvList.Columns[ci].Visible)
                        break;
                System.Diagnostics.Debug.Assert(ci < m_dgvList.Columns.Count);
            }
            else
                ci = m_dgvList.CurrentCell.ColumnIndex;

            if (m_dgvList.Rows.Count > 0)
            {
                int i = _listStrings.IndexOf(rs);
                if (i < 0)
                    m_dgvList.CurrentCell = m_dgvList.Rows[0].Cells[ci];
                else
                    m_dgvList.CurrentCell = m_dgvList.Rows[i].Cells[ci];
            }
        }

        private void SyncValue()
        {
            if (m_dgvList.CurrentRow == null || m_dgvList.CurrentRow.Index < 0 || m_dgvList.CurrentRow.Index >= _listStrings.Count)
                return;
            SyncValue(_listStrings[m_dgvList.CurrentRow.Index]);
        }

        private void SyncValue(ResourceString rs)
        {
            if (rs == null)
            {
                m_lblStringName.Text = string.Empty;
                m_lblStringName.Enabled = false;
                m_tbValue.Text = string.Empty;
                m_tbValue.Enabled = false;
                m_lblDescription.Text = string.Empty;
                m_lblDescription.Enabled = false;
            }
            else
            {
                m_lblStringName.Text = rs.FullName;
                m_lblStringName.Enabled = true;
                ResourceStringValue rsv = rs.Values.FindByCultureInfo(_currentCulture);
                if (rsv == null)
                    m_tbValue.Text = string.Empty;
                else
                    m_tbValue.Text = rsv.Value;
                m_tbValue.Enabled = true;
                m_lblDescription.Text = rs.Description;
                m_lblDescription.Enabled = true;
            }
        }

        private void SetValue(ResourceString rs, string s)
        {
            BeginUpdate();
            ResourceStringValue rsv = rs.Values.FindByCultureInfo(_currentCulture);
            if (rsv == null)
            {
                rsv = new ResourceStringValue(_root.Cultures.FindByCultureInfo(_currentCulture));
                rs.Values.Add(rsv);
            }
            rsv.Value = s;
            m_tbValue.Text = rsv.Value;
            Changed = true;
            EndUpdate();
        }

        private void SaveValue()
        {
            if (m_dgvList.CurrentRow == null || m_dgvList.CurrentRow.Index < 0 || m_dgvList.CurrentRow.Index >= _listStrings.Count)
                return;

            SaveValue(_listStrings[m_dgvList.CurrentRow.Index]);
        }

        private void SaveValue(ResourceString rs)
        {
            if (!_valueWasChanged)
                return;

            _valueWasChanged = false;
            SetValue(rs, m_tbValue.Text);
            m_dgvList.Refresh();
        }

        #endregion

        #region Private properties

        private bool UpdateLocked
        {
            get { return _lockCount > 0; }
        }

        private bool Changed
        {
            get { return _changed; }
            set
            {
                if (_changed == value)
                    return;
                _changed = value;
                UpdateToolStrip();
            }
        }

        private bool ShowSelectedGroupOnly
        {
            get { return _showSelectedGroupOnly; }
            set
            {
                if (_showSelectedGroupOnly == value)
                    return;
                _showSelectedGroupOnly = value;
                SaveValue();
                BeginUpdate();
                m_tsmiShowSelectedGroupOnly.Checked = _showSelectedGroupOnly;
                EndUpdate();
                UpdateList(m_tvStrings.SelectedNode);
                SyncGrid(m_tvStrings.SelectedNode);
            }
        }

        private bool SyncTreeWithGrid
        {
            get { return _syncTreeWithGrid; }
            set
            {
                if (_syncTreeWithGrid == value)
                    return;
                _syncTreeWithGrid = value;
                BeginUpdate();
                m_tsmiSyncTreeWithGrid.Checked = _syncTreeWithGrid;
                EndUpdate();
                if (_syncTreeWithGrid && m_dgvList.CurrentRow != null && m_dgvList.CurrentRow.Index >= 0 && m_dgvList.CurrentRow.Index < _listStrings.Count)
                    SyncTree(_listStrings[m_dgvList.CurrentRow.Index]);
            }
        }

        private bool ShowNamesInGrid
        {
            get { return _showNamesInGrid; }
            set
            {
                if (_showNamesInGrid == value)
                    return;
                _showNamesInGrid = value;
                BeginUpdate();
                m_tsmiShowNamesInGrid.Checked = _showNamesInGrid;
                EndUpdate();
                m_dgvcName.Visible = _showNamesInGrid;
            }
        }

        private CultureInfo CurrentCulture
        {
            get { return _currentCulture; }
            set
            {
                if (_currentCulture == value)
                    return;
                _currentCulture = value;
                // update m_dgvList
                m_dgvcValue.HeaderText = _currentCulture.DisplayName;
                m_dgvList.Refresh();

                BeginUpdate();
                // update toolstrip
                m_tscbCurrentCulture.SelectedIndex = _root.Cultures.IndexByCultureInfo(_currentCulture);
                // update value
                if (m_dgvList.CurrentRow != null && m_dgvList.CurrentRow.Index < _listStrings.Count)
                {
                    ResourceStringValue rsv = _listStrings[m_dgvList.CurrentRow.Index].Values.FindByCultureInfo(_currentCulture);
                    m_tbValue.Text = rsv == null ? string.Empty : rsv.Value;
                }
                else
                    m_tbValue.Text = string.Empty;
                EndUpdate();
            }
        }

        #endregion

        #region Nested types

        private class OpenDropDownItem
        {
            public Product Product;
            public ProjectRef Project;

            #region Constructors

            public OpenDropDownItem(Product product, ProjectRef project)
            {
                Product = product;
                Project = project;
            }

            #endregion

            #region Public

            public override string ToString()
            {
                return string.Format("{0}; {1}", Project.ToString(), Product.ToString());
            }

            #endregion
        }

        #endregion

        #region IDisposable implementation

        /*void IDisposable.Dispose()
        {
        }*/

        #endregion

        private void tvStrings_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // refresh list of strings
            if (UpdateLocked)
                return;

            BeginUpdate();
            UpdateList(e.Node);
            SyncGrid(e.Node);
            SyncValue();
            EndUpdate();
        }

        private void tsbNew_Click(object sender, EventArgs e)
        {
            New();
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void tsbSaveAs_Click(object sender, EventArgs e)
        {
            SaveAs();
        }

        private void tssbOpen_ButtonClick(object sender, EventArgs e)
        {
            Open();
        }

        private void tsbAddCulture_Click(object sender, EventArgs e)
        {
            AddCulture();
        }

        private void tssbDeleteCulture_ButtonClick(object sender, EventArgs e)
        {
            DeleteCulture();
        }

        private void m_dgvList_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.RowIndex >= _listStrings.Count)
                e.Value = string.Empty;
            else
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        e.Value = _listStrings[e.RowIndex].FullName;
                        break;
                    case 1:
                        e.Value = _listStrings[e.RowIndex].DefaultValue;
                        break;
                    case 2:
                        ResourceStringValue rsv = _listStrings[e.RowIndex].Values.FindByCultureInfo(_currentCulture);
                        if (rsv == null)
                            e.Value = string.Empty;
                        else
                            e.Value = rsv.Value;
                        break;
                    default:
                        System.Diagnostics.Debug.Assert(false);
                        e.Value = string.Empty;
                        break;
                }
            }
        }

        private void m_dgvList_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
        {
            System.Diagnostics.Debug.Assert(e.RowIndex < _listStrings.Count);
            switch (e.ColumnIndex)
            {
                case 0:
                case 1:
                    System.Diagnostics.Debug.Assert(false);
                    break;
                case 2:
                    SetValue(_listStrings[e.RowIndex], (string)e.Value);
                    break;
            }
        }

        private void m_dgvList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (UpdateLocked)
                return;

            BeginUpdate();
            SyncValue(_listStrings[e.RowIndex]);
            if (_syncTreeWithGrid)
                SyncTree(_listStrings[e.RowIndex]);
            EndUpdate();
        }

        private void m_dgvList_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (UpdateLocked)
                return;

            if (m_dgvList.Focused)
            {
                BeginUpdate();
                SyncValue(null);
                EndUpdate();
            }
        }

        private void m_tbValue_TextChanged(object sender, EventArgs e)
        {
            if (UpdateLocked)
                return;
            _valueWasChanged = true;
        }

        private void m_tbValue_Leave(object sender, EventArgs e)
        {
            if (UpdateLocked)
                return;
            SaveValue();
        }

        private void m_tscbCurrentCulture_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UpdateLocked)
                return;
            CurrentCulture = ((AdvCultureInfo)m_tscbCurrentCulture.Items[m_tscbCurrentCulture.SelectedIndex]).CultureInfo;
            ActiveControl = m_dgvList;
        }

        private void m_statusStrip_Resize(object sender, EventArgs e)
        {
            // 
            int w = m_statusStrip.ClientSize.Width - m_tsslProject.Width - m_tsslProduct.Width;
            m_tsslCurrentProject.Width = (int)(w * 0.6);
            m_tsslCurrentProduct.Width = w - m_tsslCurrentProject.Width - 25;
        }

        private void m_tsmiSyncTreeWithGrid_Click(object sender, EventArgs e)
        {
            if (UpdateLocked)
                return;
            SyncTreeWithGrid = !SyncTreeWithGrid;
        }

        private void m_tsmiShowNamesInGrid_Click(object sender, EventArgs e)
        {
            if (UpdateLocked)
                return;
            ShowNamesInGrid = !ShowNamesInGrid;
        }

        private void m_tsmiShowSelectedGroupOnly_Click(object sender, EventArgs e)
        {
            if (UpdateLocked)
                return;
            ShowSelectedGroupOnly = !ShowSelectedGroupOnly;
        }
    }
}