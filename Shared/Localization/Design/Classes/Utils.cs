using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Reflection;
using System.IO;
using System.Globalization;
using System.Resources;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    internal static class Utils
    {
        public const int c_GroupImageIndex = 0;
        public const int c_StringImageIndex = 1;
        public const int c_SolutionImageIndex = 2;
        public const int c_VSProjectImageIndex = 3;
        public const int c_VBProjectImageIndex = 4;
        public const int c_VJProjectImageIndex = 5;
        public const int c_UnknownProjectImageIndex = 6;
        public const int c_ProductDefaultImageImageIndex = 7;
        public const int c_FormsImageIndex = 8;
        public const int c_FormImageIndex = 9;

        public const string c_StringsClassName = "Strings";
        public const string c_ResourceManagerPropertyName = "ResourceManager";
        public const string c_EndUserLocalizeOptionsAttributeTypeName = "EndUserLocalizeOptionsAttribute";

        private static ImageList s_Images = new ImageList();
        private static Dictionary<string, CultureInfo> s_Cultures = new Dictionary<string, CultureInfo>();

        #region Constructors

        static Utils()
        {
            // file s_Images with resource images
            s_Images.TransparentColor = Color.Magenta;
            s_Images.ImageSize = new Size(16, 16);
            s_Images.ColorDepth = ColorDepth.Depth32Bit;
            s_Images.Images.Add(GetImage("Group.bmp"));
            s_Images.Images.Add(GetImage("String.bmp"));
            s_Images.Images.Add(GetImage("Solution.bmp"));
            s_Images.Images.Add(GetImage("VSProject.bmp"));
            s_Images.Images.Add(GetImage("VBProject.bmp"));
            s_Images.Images.Add(GetImage("VJProject.bmp"));
            s_Images.Images.Add(GetImage("UnknownProject.bmp"));
            s_Images.Images.Add(GetImage("ProductDefaultImage.bmp"));
            s_Images.Images.Add(GetImage("Forms.bmp"));
            s_Images.Images.Add(GetImage("Form.bmp"));

            // Cache cultures
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
                s_Cultures[ci.Name] = ci;
        }

        #endregion

        #region Private static

   /*     private static Image GetImage(string resourceName) <<IP>> // can't make this method working
		// a.GetName().Name always ends with ".2" but resources names don't contain this stuff
		// so, static ctor always falls
        {
			Assembly a = Assembly.GetExecutingAssembly();
			Stream stream = a.GetManifestResourceStream(string.Format(@"{0}.Design.Localization.Resources.{1}", a.GetName().Name, resourceName));
            if (stream == null)
                throw new Exception(string.Format(DesignLocalizationStrings.Localizer.ImageResourceNotFound, resourceName));
            return Image.FromStream(stream);
        }*/

		/// <summary>
		/// Loads an image from the entry assembly. 
		/// </summary>
		/// <param name="name">The name of the requested image.</param>
		/// <returns>The <see cref="Image"/> if it is found; null otherwise.</returns>
		private static Image GetImage(string name)
		{
			Image img = null;
			Assembly a = Assembly.GetExecutingAssembly();
			if (a != null)
			{
				img = GetImage(a, name);
			}
			if (img == null)
			{
				img = GetImage(Assembly.GetExecutingAssembly(), name);
			}
			return img;
		}
		/// <summary>
		/// Loads an image from the specified assembly. 
		/// </summary>
		/// <param name="a">An <see cref="Assembly"/> to load image from.</param>
		/// <param name="name">The name of the requested image.</param>
		/// <returns>The <see cref="Image"/> if it is found; null otherwise.</returns>
		public static Image GetImage(Assembly a, string name)
		{
			Image img = null;
			foreach (string n in a.GetManifestResourceNames())
			{
				if (n.EndsWith(name))
				{
					img = Image.FromStream(a.GetManifestResourceStream(n));
					break;
				}
			}
			return img;
		}
        #endregion

        #region Public static

        public static TreeNode FindNodeByTag(TreeNodeCollection nodes, object tag)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Tag == tag)
                    return node;
                TreeNode node2 = FindNodeByTag(node.Nodes, tag);
                if (node2 != null)
                    return node2;
            }
            return null;
        }


        public static object GetEndUserLocalizeOptionsAttribute(object[] attributes)
        {
            foreach (object attr in attributes)
                if (attr.GetType().Name == c_EndUserLocalizeOptionsAttributeTypeName)
                    return attr;
            return null;
        }

        public static bool GetExclude(object endUserLocalizeOptionsAttribute)
        {
            return (bool)endUserLocalizeOptionsAttribute.GetType().GetProperty("Exclude").GetValue(endUserLocalizeOptionsAttribute, null);
        }

        public static string GetDescription(
            object endUserLocalizeOptionsAttribute)
        {
            Type t = endUserLocalizeOptionsAttribute.GetType();
            Type stringsType = (Type)t.GetProperty("StringsType").GetValue(endUserLocalizeOptionsAttribute, null);
            string key = (string)t.GetProperty("Key").GetValue(endUserLocalizeOptionsAttribute, null);
            string description = (string)t.GetProperty("Description").GetValue(endUserLocalizeOptionsAttribute, null);
            if (t == null || string.IsNullOrEmpty(key))
                return description;

            string result = StringsManager.GetCustomString(stringsType, "Attributes.EndUserLocalizeOptions." + key, description);
            return string.IsNullOrEmpty(result) ? key : result;
            //return (string)endUserLocalizeOptionsAttribute.GetType().GetProperty("Description").GetValue(endUserLocalizeOptionsAttribute, null);
        }

        public static string[] GetProperties(object endUserLocalizeOptionsAttribute)
        {
            return (string[])endUserLocalizeOptionsAttribute.GetType().GetProperty("Properties").GetValue(endUserLocalizeOptionsAttribute, null);
        }

        public static CultureInfo GetLocale(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            string[] parts = fileName.Split('.');
            if (parts.Length < 3)
                return CultureInfo.InvariantCulture;
            else
            {
                string cn = parts[parts.Length - 2];
                if (s_Cultures.ContainsKey(cn))
                    return s_Cultures[cn];
                else
                    return CultureInfo.InvariantCulture;
            }
        }

        public static CultureInfo GetCultureInfo(string cultureInfoName)
        {
            if (s_Cultures.ContainsKey(cultureInfoName))
                return s_Cultures[cultureInfoName];
            else
                return null;
        }

        public static bool ParseResXFileName(string fileName, out string productName, out CultureInfo cultureInfo)
        {
            productName = null;
            cultureInfo = null;
            fileName = Path.GetFileName(fileName);

            string[] items = fileName.Split('.');
            if (items.Length < 2)
                return false;

            if (items[items.Length - 1].ToLower() != "resx")
                return false;

            cultureInfo = GetCultureInfo(items[items.Length - 2]);
            if (cultureInfo == null)
            {
                cultureInfo = CultureInfo.InvariantCulture;
                productName = fileName.Substring(0, fileName.Length - 1 - items[items.Length - 1].Length);
            }
            else
            {
                productName = fileName.Substring(0, fileName.Length - 2 - items[items.Length - 1].Length - items[items.Length - 2].Length);
            }
            return true;
        }

        public static string GetCultureDescription(CultureInfo cultureInfo)
        {
            if (cultureInfo == CultureInfo.InvariantCulture)
                return DesignLocalizationStrings.Localizer.InvariantCultureDescription;
            else
                return string.Format("{0} ({1})", cultureInfo.EnglishName, cultureInfo.Name);
        }

        public static void MoveDirectory()
        {
        }

        #endregion

        #region Public static properties

        public static ImageList Images
        {
            get { return s_Images; }
        }

        public static Dictionary<string, CultureInfo> Cultures
        {
            get { return s_Cultures; }
        }

        public static IComparer<CultureInfo> CultureComparer
        {
            get { return CultureComparerClass.Instance; }
        }

        #endregion

        #region Sub classes

        private class CultureComparerClass : IComparer<CultureInfo>
        {
            private static CultureComparerClass s_Instance = new CultureComparerClass();

            #region Public static properties

            public static CultureComparerClass Instance
            {
                get { return s_Instance; }
            }

            #endregion

            #region IComparer<CultureInfo> implementation

            int IComparer<CultureInfo>.Compare(CultureInfo x, CultureInfo y)
            {
                if (x == y)
                    return 0;
                if (x.Name == null || x.Name.Length == 0)
                    return -1;
                if (y.Name == null || y.Name.Length == 0)
                    return 1;
                return x.EnglishName.CompareTo(y.EnglishName);
            }

            #endregion
        }

        #endregion
    }
}