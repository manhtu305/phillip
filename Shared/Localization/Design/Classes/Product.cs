using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Reflection;
using System.IO;
using System.Globalization;
using System.Resources;

namespace C1.Win.Localization.Design
{
    internal class Product : IDisposable
    {
        private Reference _reference;
        private Assembly _assembly;
        private Type _stringsClassType;
        private ProjectRefCollection _projects = new ProjectRefCollection();

        #region Constructors

        public Product(Reference reference, Assembly assembly, Type stringsClassType)
        {
            _reference = reference;
            _assembly = assembly;
            _stringsClassType = stringsClassType;
        }

        #endregion

        #region Public

        public void Dispose()
        {
            _reference.Dispose();
            _assembly = null;
            _stringsClassType = null;
            _projects = null;
        }

        public override string ToString()
        {
            return Assembly.GetName().Name;
        }

        public string GetResXFileNamePrefix()
        {
            //return _assembly.GetName().Name.Replace('.', '_');
            return _assembly.GetName().Name;
        }

        public string GetResXFileName(CultureInfo cultureInfo)
        {
            string s = GetResXFileNamePrefix();
            if (cultureInfo == null || cultureInfo == CultureInfo.InvariantCulture)
                return s + ".resx";
            else
                return s + "." + cultureInfo.Name + ".resx";
        }

        public bool ParseResXFileName(string fileName, out CultureInfo cultureInfo)
        {
            string productName;
            if (!Utils.ParseResXFileName(fileName, out productName, out cultureInfo))
                return false;
            return productName.ToLower() == GetResXFileNamePrefix().ToLower();
        }

        #endregion

        #region Public properties

        public Reference Reference
        {
            get { return _reference; }
        }

        public Assembly Assembly
        {
            get { return _assembly; }
            set { _assembly = value; }
        }

        public Type StringsClassType
        {
            get { return _stringsClassType; }
            set { _stringsClassType = value; }
        }

        /// <summary>
        /// Gets the list of projects containing this product.
        /// </summary>
        public ProjectRefCollection Projects
        {
            get { return _projects; }
        }

        public Image Image
        {
            get { return Utils.Images.Images[Utils.c_ProductDefaultImageImageIndex]; }
        }

        #endregion
    }

    internal class ProductCollection : List<Product>
    {
        #region Public

        public Product FindByAssemblyName(string assemblyName)
        {
            foreach (Product product in this)
                if (product.Assembly.GetName().Name == assemblyName)
                    return product;
            return null;
        }

        public bool ParseResXFileName(string fileName, out Product product, out CultureInfo cultureInfo)
        {
            product = null;
            string productName;
            if (!Utils.ParseResXFileName(fileName, out productName, out cultureInfo))
                return false;

            productName = productName.ToLower();
            foreach (Product pr in this)
                if (pr.GetResXFileNamePrefix().ToLower() == productName)
                {
                    product = pr;
                    break;
                }
            return product != null;
        }

        #endregion
    }
}
