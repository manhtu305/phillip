using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Reflection;
using System.IO;
using System.Globalization;
using System.Resources;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    internal abstract class ResourceItem
    {
        public ResourceItem Parent;
        public string Name;
        public string Description;

        #region Constructors
        public ResourceItem(ResourceItem parent, string name)
        {
            Parent = parent;
            Name = name;
        }

        public ResourceItem(ResourceItem parent, Type stringsType, string name, string description)
            : this(parent, name)
        {
            Description = description;
            /*StringBuilder sb = new StringBuilder("StringsDescriptions.");
            sb.Append(name);
            Type declaringType = stringsType.DeclaringType;
            while (declaringType != null)
            {
                sb.Insert(0, '.');
                sb.Insert(0, stringsType.Name);
                stringsType = declaringType;
                declaringType = declaringType.DeclaringType;
            }
            Description = StringsManager.GetCustomString(stringsType, sb.ToString(), string.Empty);
            if (string.IsNullOrEmpty(Description))
                Description = description;*/
            /*Description = StringsManager.GetCustomString(stringsType, "StringsDescriptions." + name, string.Empty);
            if (string.IsNullOrEmpty(Description))
                Description = description;*/
        }
        #endregion

        #region Public

        public virtual void GetNodeParams(ImageList imageList, out string nodeText, out string nodeImageKey)
        {
            nodeText = Name;
            nodeImageKey = GetType().Name;
            if (!imageList.Images.ContainsKey(nodeImageKey))
                imageList.Images.Add(nodeImageKey, Image);
        }

        public abstract void GetStrings(List<ResourceString> strings);

        #endregion

        #region Public properties

        public string FullName
        {
            get
            {
                StringBuilder result = new StringBuilder(Name);
                for (ResourceItem p = Parent; p != null && !(p is RootResourceGroup); p = p.Parent)
                {
                    result.Insert(0, '.');
                    result.Insert(0, p.Name);
                }
                return result.ToString();
            }
        }

        public abstract bool Changed { get; }

        public abstract Image Image { get; }

        #endregion
    }

    /// <summary>
    /// Collection of ResourceItem objects.
    /// </summary>
    internal class ResourceItemCollection : List<ResourceItem>
    {
        #region Private
        private void AddProperties(
            object control,
            Type controlType,
            Type stringsType,
            object endUserLocalizeOptions,
            ResourceGroup crg)
        {
			if (control == null)
				return;

            string[] properties = ControlLocalizeRules.GetLocalizedProperties(controlType, endUserLocalizeOptions);
            if (properties == null || properties.Length == 0)
                return;

            foreach (string property in properties)
            {
                // try to get the default property value
                PropertyInfo pi = controlType.GetProperty(property, BindingFlags.Instance | BindingFlags.Public);
                if (pi != null && pi.CanRead && pi.PropertyType == typeof(string))
                {
                    string defaultValue = pi.GetValue(control, null) as string;
                    if (defaultValue != null && defaultValue.Length > 0)
                        crg.Children.Add(new ResourceString(crg, stringsType, property, defaultValue, string.Empty));
                }
            }
        }
        #endregion

        #region Internal
        internal void AddControlStrings(
            ResourceGroup parent,
            ResourceGroup controlGroup,
            object control,
            Type stringsType,
            object endUserLocalizeOptions)
        {
            Type controlType = control.GetType();
            // get the control localizable properties
            AddProperties(control, controlType, stringsType, endUserLocalizeOptions, controlGroup);

            FieldInfo[] fields = controlType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
            foreach (FieldInfo fi in fields)
            {
                // by default we localize only protected members
                // if (!fi.IsFamily && !fi.IsPublic) <<IP>>
                //  continue;

                string fieldName = fi.Name.StartsWith("m_") ? fi.Name.Substring(2) : fi.Name;
                object fieldEndUserLocalizeOptions = Utils.GetEndUserLocalizeOptionsAttribute(fi.GetCustomAttributes(false));
                object fieldControl = fi.GetValue(control);

                //
                ControlResourceGroup crg = new ControlResourceGroup(controlGroup, fieldName, fi.FieldType);

                //
                if (fieldEndUserLocalizeOptions == null)
                {
                    // EndUserLocalizeOptions attribute not specified for the field
                    // so simple check and add localizable properties on the base of s_Rules
                    AddProperties(fieldControl, fi.FieldType, stringsType, fieldEndUserLocalizeOptions, crg);
                    if (crg.Children.Count > 0)
                        controlGroup.Children.Add(crg);
                }
                else
                {
                    // possible object has a nested localizable objects (fieldControl is a UserControl for example)
                    // we check it only if EndUserLocalizeOptions attribute specified
                    if (!Utils.GetExclude(fieldEndUserLocalizeOptions))
                        controlGroup.Children.AddControlStrings(controlGroup, crg, fieldControl, stringsType, fieldEndUserLocalizeOptions);
                }
            }

            // <<IP>> Add Text property of the control (i.e. Not only Text of buttons, but the Text of Form as well)
            PropertyInfo text = controlType.GetProperty("Text");
            if (text != null)
                AddProperties(control, control.GetType(), stringsType, null, controlGroup);

            if (controlGroup.Children.Count > 0)
                this.Add(controlGroup);
        }

        internal void AddControlStrings(
            ControlsResourceGroup parent,
            Type controlType,
            Type stringsType)
        {
            object endUserLocalizeOptions = Utils.GetEndUserLocalizeOptionsAttribute(controlType.GetCustomAttributes(false));
            if (endUserLocalizeOptions == null || Utils.GetExclude(endUserLocalizeOptions))
                return; // excluded from localization

            // try to create a control
		    //	ConstructorInfo ci = controlType.GetConstructor(new Type[0]); // <<IP>>
			ConstructorInfo ci = controlType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[0], null);
            if (ci == null)
                return;

            using (Control control = (Control)ci.Invoke(null))
            {
                ControlResourceGroup controlGroup = new ControlResourceGroup(parent, controlType.Name, controlType);
                AddControlStrings(parent, controlGroup, control, stringsType, endUserLocalizeOptions);
            }
        }

        internal void AddStringsType(ResourceGroup parent, Type type)
        {
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Static | BindingFlags.Public);
            if (properties != null)
            {
                foreach (PropertyInfo property in properties)
                {
                    if (!property.CanRead || property.CanWrite)
                        continue;
                    if (property.PropertyType != typeof(string))
                        continue;
                    object endUserLocalizeOptions = Utils.GetEndUserLocalizeOptionsAttribute(property.GetCustomAttributes(false));
                    if (endUserLocalizeOptions == null || Utils.GetExclude(endUserLocalizeOptions))
                        continue; // excluded from localize

                    // get the default value of string
                    string description = endUserLocalizeOptions == null ? string.Empty : Utils.GetDescription(endUserLocalizeOptions);
                    string defaultValue = property.GetValue(null, null) as string;
                    ResourceString rs = new ResourceString(
                        parent,
                        type,
                        property.Name,
                        defaultValue,
                        description);
                    this.Add(rs);
                }
            }

            Type[] types = type.GetNestedTypes(BindingFlags.Static | BindingFlags.Public);
            if (types != null)
            {
                foreach (Type subType in types)
                {
                    ResourceGroup rg = new ResourceGroup(parent, subType.Name);
                    rg.Children.AddStringsType(rg, subType);
                    if (rg.Children.Count > 0)
                        this.Add(rg);
                }
            }
        }
        #endregion

        #region Public

        public ResourceItem FindItemByFullPath(string path)
        {
            string[] parts = path.Split('.');
            ResourceItemCollection items = this;
            for (int i = 0; i < parts.Length - 1; i++)
            {
                ResourceGroup rg = items.FindItemByName(parts[i]) as ResourceGroup;
                if (rg == null)
                    return null;
                items = rg.Children;
            }
            return items.FindItemByName(parts[parts.Length - 1]);
        }

        public ResourceItem FindItemByName(string itemName)
        {
            foreach (ResourceItem ri in this)
                if (ri.Name == itemName)
                    return ri;
            return null;
        }

        /// <summary>
        /// Fills a list with all ResourceString objects containing
        /// in this collection and all its nested collections.
        /// </summary>
        public void GetStrings(List<ResourceString> strings)
        {
            foreach (ResourceItem ri in this)
                ri.GetStrings(strings);
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Returns true if collection or any of its children
        /// has changed items.
        /// </summary>
        public bool HasChanged
        {
            get
            {
                foreach (ResourceItem ri in this)
                    if (ri.Changed)
                        return true;
                return false;
            }
        }

        #endregion
    }

    /// <summary>
    /// Represents the resource string for all cultures.
    /// </summary>
    internal class ResourceString : ResourceItem
    {
        private string _defaultValue;
        private ResourceStringValues _values = new ResourceStringValues();

        #region Constructors

        public ResourceString(ResourceItem parent, Type stringsType, string name, string defaultValue, string description)
            : base(parent, stringsType, name, description)
        {
            _defaultValue = defaultValue;
        }

        #endregion

        #region Public

        public override void GetStrings(List<ResourceString> strings)
        {
            strings.Add(this);
        }

        #endregion

        #region Public properties

        public ResourceStringValues Values
        {
            get { return _values; }
        }

        public string DefaultValue
        {
            get { return _defaultValue == null ? string.Empty : _defaultValue; }
        }

        public override bool Changed
        {
            get
            {
                foreach (ResourceStringValue rsv in _values)
                    if ((rsv.State & ResourceStringValueStateEnum.Changed) != 0)
                        return true;
                return false;
            }
        }

        public override Image Image
        {
            get { return Utils.Images.Images[Utils.c_StringImageIndex]; }
        }

        #endregion
    }

    internal class ResourceStringValue
    {
        private AdvCultureInfo _cultureInfo;
        private string _value;
        private ResourceStringValueStateEnum _state;

        #region Constructors

        public ResourceStringValue(AdvCultureInfo cultureInfo, string s)
        {
            _cultureInfo = cultureInfo;
            _value = s;
            _state = ResourceStringValueStateEnum.None;
        }

        public ResourceStringValue(AdvCultureInfo cultureInfo)
        {
            _cultureInfo = cultureInfo;
            _state = ResourceStringValueStateEnum.New;
        }

        #endregion

        #region Public properties

        public AdvCultureInfo CultureInfo
        {
            get { return _cultureInfo; }
        }

        public string Value
        {
            get { return _value == null ? string.Empty : _value; }
            set
            {
                if (_value == value)
                    return;
                _value = value;
                if (_state == ResourceStringValueStateEnum.New)
                    _cultureInfo.TranslatedStringCount++;
                State |= ResourceStringValueStateEnum.Changed;
            }
        }

        public ResourceStringValueStateEnum State
        {
            get { return _state; }
            set
            {
                if (_state == value)
                    return;

                if ((_state & ResourceStringValueStateEnum.Changed) != (value & ResourceStringValueStateEnum.Changed))
                {
                    if ((_state & ResourceStringValueStateEnum.Changed) != 0)
                    {
                        // changed to unchanged
                        _cultureInfo.ModifiedStringCount--;
                    }
                    else
                    {
                        // unchanged to changed
                        _cultureInfo.ModifiedStringCount++;
                    }
                }
                _state = value;
            }
        }

        #endregion
    }

    [Flags]
    internal enum ResourceStringValueStateEnum
    {
        None = 0x00,
        New = 0x01,
        Changed = 0x02,
    }

    /// <summary>
    /// Contains values for each culture.
    /// </summary>
    internal class ResourceStringValues : List<ResourceStringValue>
    {
        #region Public

        public int IndexByCultureInfo(CultureInfo cultureInfo)
        {
            for (int i = 0; i < this.Count; i++)
                if (this[i].CultureInfo.CultureInfo == cultureInfo)
                    return i;
            return -1;
        }

        public ResourceStringValue FindByCultureInfo(CultureInfo cultureInfo)
        {
            int i = IndexByCultureInfo(cultureInfo);
            return i == -1 ? null : this[i];
        }

        public void RemoveByCultureInfo(CultureInfo cultureInfo)
        {
            int i = IndexByCultureInfo(cultureInfo);
            if (i != -1)
                RemoveAt(i);
        }

        public new void Add(ResourceStringValue rsv)
        {
            System.Diagnostics.Debug.Assert(IndexByCultureInfo(rsv.CultureInfo.CultureInfo) == -1);
            base.Add(rsv);
            if ((rsv.State & ResourceStringValueStateEnum.Changed) != 0)
                rsv.CultureInfo.ModifiedStringCount++;
            if (rsv.State != ResourceStringValueStateEnum.New)
                rsv.CultureInfo.TranslatedStringCount++;
        }

        #endregion
    }

    /// <summary>
    /// Represents the group of resource strings.
    /// </summary>
    internal class ResourceGroup : ResourceItem
    {
        private ResourceItemCollection _children = new ResourceItemCollection();

        #region Constructors

        public ResourceGroup(ResourceItem parent, string name)
            : base(parent, name)
        {
        }

        #endregion

        #region Public

        public override void GetStrings(List<ResourceString> strings)
        {
            _children.GetStrings(strings);
        }

        #endregion

        #region Public properties

        public ResourceItemCollection Children
        {
            get { return _children; }
        }

        public override bool Changed
        {
            get
            {
                foreach (ResourceItem ri in _children)
                    if (ri.Changed)
                        return true;
                return false;
            }
        }

        public override Image Image
        {
            get { return Utils.Images.Images[Utils.c_GroupImageIndex]; }
        }

        #endregion
    }

    /// <summary>
    /// Represents the group of resources containing the 
    /// strings for localizing the visual controls 
    /// (forms, user controls, controls on forms, etc).
    /// </summary>
    internal class ControlsResourceGroup : ResourceGroup
    {
        #region Constructors

        public ControlsResourceGroup(ResourceItem parent, string name)
            : base(parent, name)
        {
        }

        #endregion

        #region Public properties

        public override Image Image
        {
            get { return Utils.Images.Images[Utils.c_FormsImageIndex]; }
        }

        #endregion
    }

    /// <summary>
    /// Represents the group of resource string containing the
    /// strings for localizing the separate control.
    /// </summary>
    internal class ControlResourceGroup : ResourceGroup
    {
        private Type _controlType;

        #region Constructors

        public ControlResourceGroup(ResourceItem parent, string name, Type controlType)
            : base(parent, name)
        {
            _controlType = controlType;
        }

        #endregion

        #region Public properties

        public Type ControlType
        {
            get { return _controlType; }
        }

        public override Image Image
        {
            get
            {
                Type type;
                Stream stream = null;
                for (
                    type = _controlType;
                    stream == null && type != typeof(object);
                    stream = type.Assembly.GetManifestResourceStream(string.Format(@"{0}.bmp", type.FullName)),
                    type = type.BaseType) ;

                if (stream == null)
                    return Utils.Images.Images[Utils.c_ProductDefaultImageImageIndex];
                else
                    return Image.FromStream(stream);
            }
        }

        #endregion

        #region Public

        public override void GetNodeParams(ImageList imageList, out string nodeText, out string nodeImageKey)
        {
            nodeText = Name;
            nodeImageKey = _controlType.FullName;
            if (!imageList.Images.ContainsKey(nodeImageKey))
                imageList.Images.Add(nodeImageKey, Image);
        }

        #endregion
    }

    /// <summary>
    /// Represents the root resource group containing all other resources.
    /// </summary>
    internal class RootResourceGroup : ResourceGroup
    {
        private AdvCultureInfoList _cultures = new AdvCultureInfoList();

        #region Constructors

        public RootResourceGroup()
			: base(null, DesignLocalizationStrings.StringsDesigner.AllStringsCaption)
        {
        }

        #endregion

        #region Private

        private void LoadResX(string fileName)
        {
            try
            {
                // determine the file culture
                CultureInfo cultureInfo = Utils.GetLocale(fileName);
                AdvCultureInfo advCultureInfo = new AdvCultureInfo(cultureInfo, 0, 0);
                _cultures.Add(advCultureInfo);

                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                using (ResXResourceReader r = new ResXResourceReader(fs))
                {
                    foreach (DictionaryEntry de in r)
                    {
                        string value = de.Value as string;
                        string key = de.Key as string;
                        if (value != null && key != null)
                        {
                            // search the ResourceString
                            ResourceString rs = Children.FindItemByFullPath(key) as ResourceString;
                            if (rs != null)
                                rs.Values.Add(new ResourceStringValue(advCultureInfo, value));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(
					string.Format(DesignLocalizationStrings.Localizer.LoadResXStringsException, fileName, e.Message),
					DesignLocalizationStrings.Error,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Public

        public bool Save(Product product, ProjectRef project)
        {
            // 1. get all ResourceString objects
            List<ResourceString> strings = new List<ResourceString>();
            Children.GetStrings(strings);
            if (strings.Count == 0)
                return true; // nothing to save

            // 2. Write strings to resx files
            ProjectItem localizeFolder = new ProjectItem();
            project.GetLocalizeFolder(true, ref localizeFolder);
            string localizeFolderName = localizeFolder.FileName;
            Dictionary<CultureInfo, ResWriter> resWriters = new Dictionary<CultureInfo, ResWriter>();
            bool successfullySaved = true;
            try
            {
                // build list of resource writers
                foreach (ResourceString rs in strings)
                {
                    foreach (ResourceStringValue rsv in rs.Values)
                    {
                        if (rsv.State == ResourceStringValueStateEnum.New)
                            continue;

                        CultureInfo ci = rsv.CultureInfo.CultureInfo;

                        string resFileName = product.GetResXFileName(ci);

                        // select resource writer
                        if (!resWriters.ContainsKey(ci))
                        {
                            // create writer
                            // search the project item
                            ProjectItem resItem = new ProjectItem();
                            int itemCount = localizeFolder.ProjectItems.Count;
                            for (int i = 1; i <= itemCount; i++)
                            {
                                ProjectItem pi = localizeFolder.ProjectItems[i];
                                if (Path.GetFileName(pi.FileName).ToLower() == resFileName.ToLower())
                                {
                                    resItem = pi;
                                    break;
                                }
                            }
                            string fileName = localizeFolderName + resFileName;
                            resWriters[ci] = new ResWriter(fileName, resItem);
                        }
                        ResWriter rw = resWriters[ci];
                        rw.WriteString(rs.FullName, rsv.Value);
                        if ((rsv.State &= ResourceStringValueStateEnum.Changed) != 0)
                        {
                            rsv.State &= ~ResourceStringValueStateEnum.Changed;
                            rsv.State &= ~ResourceStringValueStateEnum.New;
                        }
                    }
                }

                // save res writers to files and add project items
                foreach (ResWriter rw in resWriters.Values)
                {
                    rw.Flush();

                    try
                    {
                        // posssible file is write protected
                        FileAttributes fa = FileAttributes.Normal;
                        if (File.Exists(rw.FileName))
                        {
                            fa = File.GetAttributes(rw.FileName);
                            if ((fa & FileAttributes.ReadOnly) != 0)
                            {
                                // reset attribute
                                if (MessageBox.Show(string.Format(DesignLocalizationStrings.RootResourceGroup.ReadOnlyFile, rw.FileName), DesignLocalizationStrings.Question, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    File.SetAttributes(rw.FileName, fa & ~FileAttributes.ReadOnly);
                                else
                                {
                                    successfullySaved = false;
                                    continue;
                                }
                            }
                        }

                        rw.SaveToFile();

                        if ((fa & FileAttributes.ReadOnly) != 0)
                            File.SetAttributes(rw.FileName, fa);
                    }
                    catch (Exception e)
                    {
                        successfullySaved = false;
                        MessageBox.Show(string.Format(DesignLocalizationStrings.RootResourceGroup.Errors.SaveToFile, rw.FileName, e.Message), DesignLocalizationStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        continue;
                    }

                    if (rw.ProjectItem.Initialized)
                        continue;

                    // add project item
                    localizeFolder.ProjectItems.AddFromFileCopy(rw.FileName);
                }

                // delete project items which are not needed (its culture was deleted)
                for (int i = 1; i <= localizeFolder.ProjectItems.Count; )
                {
                    ProjectItem pi = localizeFolder.ProjectItems[i];
                    string piFileName = pi.FileName;
                    if (piFileName == null)
                        continue;

                    CultureInfo ci;
                    if (product.ParseResXFileName(piFileName, out ci))
                    {
                        // search the resource writer 
                        if (!resWriters.ContainsKey(ci))
                        {
                            // this project item should be deleted
                            pi.Remove();
                            continue;
                        }
                    }
                    i++;
                }

                if (successfullySaved)
                    project.Project.Save();

                return successfullySaved;
            }
            finally
            {
                foreach (CultureInfo ci in resWriters.Keys)
                    resWriters[ci].Dispose();
            }
        }

        public void Load(Product product, ProjectRef project)
        {
            Children.Clear();
            _cultures.Clear();

            // select all strings from Strings class using reflection
            Children.AddStringsType(this, product.StringsClassType);

            // select all strings from forms which should be localized
			ControlsResourceGroup crg = new ControlsResourceGroup(this, "Forms");
            // select all forms classes
            Type[] types;
            try
            {
                types = product.Assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                types = e.Types;
            }
            foreach (Type type in types)
            {
                if (!typeof(Control).IsAssignableFrom(type))
                    continue;

                crg.Children.AddControlStrings(crg, type, product.StringsClassType);
            }
            if (crg.Children.Count > 0)
                Children.Add(crg);

            // load strings' translated values
            if (project != null)
            {
                List<string> files = project.GetResXFilesForProduct(product);
                if (files != null)
                    foreach (string fileName in files)
                        LoadResX(fileName);
            }
        }

        public void AddCulture(CultureInfo cultureInfo)
        {
            if (_cultures.IndexByCultureInfo(cultureInfo) != -1)
                return; // already added

            AdvCultureInfo advCultureInfo = new AdvCultureInfo(cultureInfo, 0, 0);
            _cultures.Add(advCultureInfo);
            List<ResourceString> strings = new List<ResourceString>();
            GetStrings(strings);
            foreach (ResourceString rs in strings)
                rs.Values.Add(new ResourceStringValue(advCultureInfo));
        }

        public void DeleteCulture(CultureInfo cultureInfo)
        {
            int i = _cultures.IndexByCultureInfo(cultureInfo);
            if (i == -1)
                return;

            _cultures.RemoveAt(i);
            List<ResourceString> strings = new List<ResourceString>();
            GetStrings(strings);
            foreach (ResourceString rs in strings)
                rs.Values.RemoveByCultureInfo(cultureInfo);
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Gets the list of all cultures in the resources.
        /// </summary>
        public AdvCultureInfoList Cultures
        {
            get { return _cultures; }
        }

        #endregion

        #region Nested types

        private class ResWriter : IDisposable
        {
            private ProjectItem _projectItem;
            private MemoryStream _stream;
            private ResXResourceWriter _writer;
            private string _fileName;

            #region Constructors

            public ResWriter(string fileName, ProjectItem projectItem)
            {
                _fileName = fileName;
                _projectItem = projectItem;
                _stream = new MemoryStream();
                _writer = new ResXResourceWriter(_stream);
            }

            #endregion

            #region Public

            public void Flush()
            {
                _writer.Generate();
            }

            public void Dispose()
            {
                _projectItem.Dispose();
                if (_writer != null)
                {
                    _writer.Dispose();
                    _writer = null;
                }

                if (_stream != null)
                {
                    _stream.Dispose();
                    _stream = null;
                }
            }

            public void WriteString(string name, string value)
            {
                _writer.AddResource(name, value);
            }

            public void SaveToFile()
            {
                _stream.Seek(0, SeekOrigin.Begin);
                using (FileStream fs = new FileStream(_fileName, FileMode.Create, FileAccess.Write))
                    CopyStream(_stream, fs);
            }

            #region Internal static
            [SmartAssembly.Attributes.DoNotObfuscate]
            private static void CopyStream(Stream source, Stream dest)
            {
                byte[] buffer = new byte[16 * 1024];
                int bytesRead;
                while ((bytesRead = source.Read(buffer, 0, buffer.Length)) != 0)
                    dest.Write(buffer, 0, bytesRead);
            }
            #endregion

            #endregion

            #region Public properties

            public string FileName
            {
                get { return _fileName; }
            }

            public ProjectItem ProjectItem
            {
                get { return _projectItem; }
            }

            #endregion
        }

        #endregion
    }
}