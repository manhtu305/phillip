#pragma warning disable 1591
using System;
using System.Collections;
using System.Reflection;
using System.ComponentModel.Design;

namespace C1.Win.Localization.Design
{
    public struct DTE
    {
        public static Assembly EnvDTE;
        public static Assembly EnvDTE80;
        public static Assembly VSLangProj;
        public static Assembly VSLangProj2;
        public static Assembly VSLangProj80;

        private static Type s_Type;
        private static Type s_prjReferenceType;
        private static PropertyInfo s_SolutionPI;

        private object _obj;

        #region Constructors

        public DTE(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Private static

        private static Assembly LoadAssembly(string name)
        {
            Assembly result;
            try
            {
                //result = Assembly.LoadWithPartialName(name);
                result = Assembly.Load(name);
            }
            catch
            {
                result = null;
            }
            return result;
        }

        #endregion

        #region Public static

        public static bool Initialize(ComponentDesigner componentDesigner, ref DTE dte)
        {
            EnvDTE = LoadAssembly("EnvDTE");
            if (EnvDTE == null)
                return false;
            EnvDTE80 = LoadAssembly("EnvDTE80");
            if (EnvDTE80 == null)
                return false;
            VSLangProj = LoadAssembly("VSLangProj");
            if (VSLangProj == null)
                return false;
            VSLangProj2 = LoadAssembly("VSLangProj2");
            if (VSLangProj2 == null)
                return false;
            VSLangProj80 = LoadAssembly("VSLangProj80");
            if (VSLangProj80 == null)
                return false;

            s_Type = GetType(EnvDTE, "EnvDTE._DTE");
            if (s_Type == null)
                return false;
            s_prjReferenceType = GetType(VSLangProj, "VSLangProj.prjReferenceType");
            if (s_prjReferenceType == null)
                return false;
            //PropertyInfo[] p = s_Type.GetProperties();
            s_SolutionPI = GetPropertyInfo(s_Type, "Solution");
            if (s_SolutionPI == null)
                return false;
            if (!Project.Initialize())
                return false;
            if (!Solution.Initialize())
                return false;
            if (!ProjectItems.Initialize())
                return false;
            if (!ProjectItem.Initialize())
                return false;
            if (!Reference.Initialize())
                return false;
            if (!PrjKind.Initialize())
                return false;
            if (!PrjKind2.Initialize())
                return false;
            if (!Projects.Initialize())
                return false;
            if (!References.Initialize())
                return false;
            if (!SolutionBuild.Initialize())
                return false;

            MethodInfo mi = componentDesigner.GetType().GetMethod("GetService", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (mi == null)
                return false;
            dte._obj = mi.Invoke(componentDesigner, new object[] { s_Type });
            if (dte._obj == null)
                return false;
            return true;
        }

        public static Type GetType(Assembly assembly, string typeName)
        {
            Type result = assembly.GetType(typeName, false);
            System.Diagnostics.Debug.Assert(result != null, string.Format("Can't get type [{0}] in assembly [{1}].", typeName, assembly.GetName().Name));
            return result;
        }

        public static PropertyInfo GetPropertyInfo(Type type, string propertyName)
        {
            PropertyInfo result = type.GetProperty(propertyName);
            System.Diagnostics.Debug.Assert(result != null, string.Format("Can't get property [{0}] of type [{1}].", propertyName, type.Name));
            return result;
        }

        public static FieldInfo GetFieldInfo(Type type, string fieldName)
        {
            FieldInfo result = type.GetField(fieldName);
            System.Diagnostics.Debug.Assert(result != null, string.Format("Can't get field [{0}] of type [{1}].", fieldName, type.Name));
            return result;
        }

        public static MethodInfo GetMethodInfo(Type type, string methodName)
        {
            MethodInfo result = type.GetMethod(methodName);
            System.Diagnostics.Debug.Assert(result != null, string.Format("Can't get method [{0}] of type [{1}].", methodName, type.Name));
            return result;
        }

        #endregion

        #region Public static properties

        public static object prjReferenceTypeAssembly
        {
            get { return Enum.Parse(s_prjReferenceType, "prjReferenceTypeAssembly"); }
        }

        #endregion

        #region Public properties

        public Solution Solution
        {
            get { return new Solution(s_SolutionPI.GetValue(_obj, null)); }

        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

        #endregion
    }

    public struct Solution
    {
        private static Type s_Type;
        private static PropertyInfo s_FullNamePI;
        private static PropertyInfo s_SolutionBuildPI;
        private static PropertyInfo s_ProjectsPI;

        private object _obj;

        #region Constructors

        public Solution(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.EnvDTE, "EnvDTE._Solution");
            if (s_Type == null)
                return false;
            s_FullNamePI = DTE.GetPropertyInfo(s_Type, "FullName");
            if (s_FullNamePI == null)
                return false;
            s_SolutionBuildPI = DTE.GetPropertyInfo(s_Type, "SolutionBuild");
            if (s_SolutionBuildPI == null)
                return false;
            s_ProjectsPI = DTE.GetPropertyInfo(s_Type, "Projects");
            if (s_ProjectsPI == null)
                return false;
            return true;
        }

        #endregion

        #region Public properties

        public string FullName
        {
            get { return s_FullNamePI.GetValue(_obj, null) as string; }
        }

        public SolutionBuild SolutionBuild
        {
            get { return new SolutionBuild(s_SolutionBuildPI.GetValue(_obj, null)); }
        }

        public Projects Projects
        {
            get { return new Projects(s_ProjectsPI.GetValue(_obj, null)); }
        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

        #endregion
    }

    public struct SolutionBuild
    {
        private static Type s_Type;
        private static PropertyInfo s_StartupProjectsPI;

        private object _obj;

        #region Constructors

        public SolutionBuild(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.EnvDTE, "EnvDTE.SolutionBuild");
            if (s_Type == null)
                return false;
            s_StartupProjectsPI = DTE.GetPropertyInfo(s_Type, "StartupProjects");
            if (s_StartupProjectsPI == null)
                return false;
            return true;
        }

        #endregion

        #region Public properties

        public object StartupProjects
        {
            get { return s_StartupProjectsPI.GetValue(_obj, null); }
        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

		#endregion
    }

    public struct Project
    {
        private static Type s_Type;
        private static Type s_VSProject2;
        private static PropertyInfo s_ProjectItemsPI;
        private static PropertyInfo s_UniqueNamePI;
        private static PropertyInfo s_ObjectPI;
        private static PropertyInfo s_Kind;
        private static PropertyInfo s_ReferencesPI;
        private static PropertyInfo s_FileNamePI;
        private static MethodInfo s_SaveMI;

        private object _obj;

        #region Constructors

        public Project(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.EnvDTE, "EnvDTE.Project");
            if (s_Type == null)
                return false;
            s_ProjectItemsPI = DTE.GetPropertyInfo(s_Type, "ProjectItems");
            if (s_ProjectItemsPI == null)
                return false;
            s_UniqueNamePI = DTE.GetPropertyInfo(s_Type, "UniqueName");
            if (s_UniqueNamePI == null)
                return false;
            s_ObjectPI = DTE.GetPropertyInfo(s_Type, "Object");
            if (s_ObjectPI == null)
                return false;
            s_VSProject2 = DTE.GetType(DTE.VSLangProj80, "VSLangProj80.VSProject2");
            if (s_VSProject2 == null)
                return false;
            s_Kind = DTE.GetPropertyInfo(s_Type, "Kind");
            if (s_Kind == null)
                return false;
            s_ReferencesPI = DTE.GetPropertyInfo(s_VSProject2, "References");
            if (s_ReferencesPI == null)
                return false;
            s_FileNamePI = DTE.GetPropertyInfo(s_Type, "FileName");
            if (s_FileNamePI == null)
                return false;
            s_SaveMI = DTE.GetMethodInfo(s_Type, "Save");
            if (s_SaveMI == null)
                return false;
            return true;
        }

        #endregion

        #region Public

        public void Dispose()
        {
            _obj = null;
        }

        public void Save()
        {
            s_SaveMI.Invoke(_obj, new object[] { null });
        }

        #endregion

        #region Public properties

        public ProjectItems ProjectItems
        {
            get { return new ProjectItems(s_ProjectItemsPI.GetValue(_obj, null)); }
        }

        public string UniqueName
        {
            get
            {
                try
                {
                    return (string)s_UniqueNamePI.GetValue(_obj, null);
                }
                catch
                {
                    return null;
                }
            }
        }

        public object Object
        {
            get { return s_ObjectPI.GetValue(_obj, null); }
        }

        public bool IsLangProject
        {
            get
            {
                string kind = Kind;
                return
                    kind == PrjKind.prjKindCSharpProject ||
                    kind == PrjKind.prjKindVBProject ||
                    kind == PrjKind2.prjKindVJSharpProject;
            }
        }

        public string Kind
        {
            get { return (string)s_Kind.GetValue(_obj, null); }
        }

        public References References
        {
            get { return new References(s_ReferencesPI.GetValue(Object, null)); }
        }

        public string FileName
        {
            get { return s_FileNamePI.GetValue(_obj, null) as string; }
        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

		#endregion
    }

    public struct ProjectItems
    {
        private static Type s_Type;
        private static PropertyInfo s_CountPI;
        private static MethodInfo s_ItemMI;
        private static MethodInfo s_AddFolderMI;
        private static MethodInfo s_AddFromFileCopyMI;

        private object _obj;

        #region Constructors

        public ProjectItems(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.EnvDTE, "EnvDTE.ProjectItems");
            if (s_Type == null)
                return false;
            s_CountPI = DTE.GetPropertyInfo(s_Type, "Count");
            if (s_CountPI == null)
                return false;
            s_ItemMI = DTE.GetMethodInfo(s_Type, "Item");
            if (s_ItemMI == null)
                return false;
            s_AddFolderMI = DTE.GetMethodInfo(s_Type, "AddFolder");
            if (s_AddFolderMI == null)
                return false;
            s_AddFromFileCopyMI = DTE.GetMethodInfo(s_Type, "AddFromFileCopy");
            if (s_AddFromFileCopyMI == null)
                return false;
            return true;
        }

        #endregion

        #region Public properties

        public int Count
        {
            get { return (int)s_CountPI.GetValue(_obj, null); }
        }

        public ProjectItem this[int index]
        {
            get { return new ProjectItem(s_ItemMI.Invoke(_obj, new object[] { index })); }
        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

		#endregion

        #region Public

        public ProjectItem AddFolder(string name, string kind)
        {
            return new ProjectItem(s_AddFolderMI.Invoke(_obj, new object[] { name, kind }));
        }

        public ProjectItem AddFromFileCopy(string fileName)
        {
            return new ProjectItem(s_AddFromFileCopyMI.Invoke(_obj, new object[] { fileName }));
        }

        #endregion
    }

    public struct Projects
    {
        private static Type s_Type;
        private static PropertyInfo s_CountPI;
        private static MethodInfo s_ItemMI;

        private object _obj;

        #region Constructors

        public Projects(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.EnvDTE, "EnvDTE.Projects");
            if (s_Type == null)
                return false;
            s_CountPI = DTE.GetPropertyInfo(s_Type, "Count");
            if (s_CountPI == null)
                return false;
            s_ItemMI = DTE.GetMethodInfo(s_Type, "Item");
            if (s_ItemMI == null)
                return false;
            return true;
        }

        #endregion

        #region Public properties

        public int Count
        {
            get { return (int)s_CountPI.GetValue(_obj, null); }
        }

        public Project this[int index]
        {
            get { return new Project(s_ItemMI.Invoke(_obj, new object[] { index })); }
        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

		#endregion
    }

    public struct ProjectItem
    {
        private static Type s_Type;
        private static PropertyInfo s_ProjectItemsPI;
        private static PropertyInfo s_NamePI;
        private static PropertyInfo s_FileCountPI;
        private static MethodInfo s_getFileNamesMI;
        private static MethodInfo s_RemoveMI;

        private object _obj;

        #region Constructors

        public ProjectItem(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.EnvDTE, "EnvDTE.ProjectItem");
            if (s_Type == null)
                return false;
            s_ProjectItemsPI = DTE.GetPropertyInfo(s_Type, "ProjectItems");
            if (s_ProjectItemsPI == null)
                return false;
            s_NamePI = DTE.GetPropertyInfo(s_Type, "Name");
            if (s_NamePI == null)
                return false;
            s_FileCountPI = DTE.GetPropertyInfo(s_Type, "FileCount");
            if (s_FileCountPI == null)
                return false;
            s_getFileNamesMI = DTE.GetMethodInfo(s_Type, "get_FileNames");
            if (s_getFileNamesMI == null)
                return false;
            s_RemoveMI = DTE.GetMethodInfo(s_Type, "Remove");
            if (s_RemoveMI == null)
                return false;
            return true;
        }

        #endregion

        #region Public

        public void Dispose()
        {
            _obj = null;
        }

        public void Remove()
        {
            s_RemoveMI.Invoke(_obj, null);
            _obj = null;
        }

        #endregion

        #region Public properties

        public bool Initialized
        {
            get { return _obj != null; }
        }

        public ProjectItems ProjectItems
        {
            get { return new ProjectItems(s_ProjectItemsPI.GetValue(_obj, null)); }
        }

        public string Name
        {
            get { return (string)s_NamePI.GetValue(_obj, null); }
        }

        public int FileCount
        {
            get { return (Int16)s_FileCountPI.GetValue(_obj, null); }
        }

        public string FileName
        {
            get
            {
                int fileCount = FileCount;
                if (fileCount <= 0)
                    return null;
                return (string)s_getFileNamesMI.Invoke(_obj, new object[] { (Int16)0 });
            }
        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

		#endregion
    }

    public struct Reference
    {
        private static Type s_Type;
        private static PropertyInfo s_TypePI;
        private static PropertyInfo s_PathPI;

        private object _obj;

        #region Constructors

        public Reference(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.VSLangProj, "VSLangProj.Reference");
            if (s_Type == null)
                return false;
            s_TypePI = DTE.GetPropertyInfo(s_Type, "Type");
            if (s_TypePI == null)
                return false;
            s_PathPI = DTE.GetPropertyInfo(s_Type, "Path");
            if (s_PathPI == null)
                return false;
            return true;
        }

        #endregion

        #region Public

        public void Dispose()
        {
            _obj = null;
        }

        #endregion

        #region Public properties

        public object Type
        {
            get { return s_TypePI.GetValue(_obj, null); }
        }

        public string Path
        {
            get { return (string)s_PathPI.GetValue(_obj, null); }
        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

		#endregion
    }

    public struct PrjKind
    {
        private static Type s_Type;
        private static FieldInfo s_prjKindCSharpProjectFI;
        private static FieldInfo s_prjKindVBProjectFI;
        private static FieldInfo s_prjKindVSAProjectFI;

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.VSLangProj, "VSLangProj.PrjKind");
            if (s_Type == null)
                return false;
            s_prjKindCSharpProjectFI = DTE.GetFieldInfo(s_Type, "prjKindCSharpProject");
            if (s_prjKindCSharpProjectFI == null)
                return false;
            s_prjKindVBProjectFI = DTE.GetFieldInfo(s_Type, "prjKindVBProject");
            if (s_prjKindVBProjectFI == null)
                return false;
            s_prjKindVSAProjectFI = DTE.GetFieldInfo(s_Type, "prjKindVSAProject");
            if (s_prjKindVSAProjectFI == null)
                return false;
            return true;
        }

        #endregion

        #region Public static properties

        public static string prjKindCSharpProject
        {
            get { return (string)s_prjKindCSharpProjectFI.GetValue(null); }
        }

        public static string prjKindVBProject
        {
            get { return (string)s_prjKindVBProjectFI.GetValue(null); }
        }

        public static string prjKindVSAProject
        {
            get { return (string)s_prjKindVSAProjectFI.GetValue(null); }
        }

        #endregion
    }

    public struct PrjKind2
    {
        private static Type s_Type;
        private static FieldInfo s_prjKindVJSharpProjectFI;

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.VSLangProj2, "VSLangProj2.PrjKind2");
            if (s_Type == null)
                return false;
            s_prjKindVJSharpProjectFI = DTE.GetFieldInfo(s_Type, "prjKindVJSharpProject");
            if (s_prjKindVJSharpProjectFI == null)
                return false;
            return true;
        }

        #endregion

        #region Public static properties

        public static string prjKindVJSharpProject
        {
            get { return (string)s_prjKindVJSharpProjectFI.GetValue(null); }
        }

        #endregion
    }

    public struct References
    {
        private static Type s_Type;
        private static PropertyInfo s_CountPI;
        private static MethodInfo s_ItemMI;

        private object _obj;

        #region Constructors

        public References(object obj)
        {
            _obj = obj;
        }

        #endregion

        #region Public static

        public static bool Initialize()
        {
            s_Type = DTE.GetType(DTE.VSLangProj, "VSLangProj.References");
            if (s_Type == null)
                return false;
            s_CountPI = DTE.GetPropertyInfo(s_Type, "Count");
            if (s_CountPI == null)
                return false;
            s_ItemMI = DTE.GetMethodInfo(s_Type, "Item");
            if (s_ItemMI == null)
                return false;
            return true;
        }

        #endregion

        #region Public properties

        public int Count
        {
            get { return (int)s_CountPI.GetValue(_obj, null); }
        }

        public Reference this[int index]
        {
            get { return new Reference(s_ItemMI.Invoke(_obj, new object[] { index })); }
        }

		public bool IsValid
		{
			get { return _obj != null; }
		}

		#endregion
    }
}
