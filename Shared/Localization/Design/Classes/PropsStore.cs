using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Globalization;

namespace C1.Win.Localization.Design
{
    /// <summary>
    /// Base class for store properties of visual controls, position of form for example.
    /// </summary>
    internal abstract class PropsStoreBase
    {
        private const string c_CountSuffix = "_Count";
        private const string c_ItemNameMask = "{0}_{1}";
        private string _path;

        #region Constructors

        public PropsStoreBase(string path)
        {
            _path = path;
        }

        #endregion

        #region Protected

        protected abstract object GetValue(string propName);

        protected abstract void SetValue(string propName, object value);

        protected abstract object GetValueNoResolution(string propName);

        protected abstract void SetValueNoResolution(string propName, object value);

        #endregion

        #region Public

        public abstract bool HasValue(string propName);

        public abstract bool HasValueNoResolution(string propName);

        public object GetEnumNoResolution(string propName, object defaultValue, Type enumType)
        {
            string s = GetValueNoResolution(propName) as string;
            return s == null ? defaultValue : Enum.Parse(enumType, s);
        }

        public void SetEnumNoResolution(string propName, object value, Type enumType)
        {
            SetValueNoResolution(propName, Enum.GetName(enumType, value));
        }

        public string GetStringNoResolution(string propName, string defaultValue)
        {
            object v = GetValueNoResolution(propName);
            return v == null ? defaultValue : (string)v;
        }

        public void SetStringNoResolution(string propName, string value)
        {
            SetValueNoResolution(propName, value);
        }

        public int GetIntNoResolution(string propName, int defaultValue)
        {
            object v = GetValueNoResolution(propName);
            return v == null ? defaultValue : (int)v;
        }

        public void SetIntNoResolution(string propName, int value)
        {
            SetValueNoResolution(propName, value);
        }

        public void GetStringsNoResolution(string propName, IList strings)
        {
            strings.Clear();
            int count = GetIntNoResolution(propName + c_CountSuffix, 0);
            if (count == 0)
                return;

            for (int i = 0; i < count; i++)
            {
                string s = GetStringNoResolution(string.Format(c_ItemNameMask, propName, i), null);
                if (s != null)
                    strings.Add(s);
            }
        }

        public void SetStringsNoResolution(string propName, IList strings)
        {
            int count = 0;
            for (int i = 0; i < strings.Count; i++)
            {
                string s = strings[i] as string;
                if (s != null)
                {
                    SetStringNoResolution(string.Format(c_ItemNameMask, propName, i), s);
                    count++;
                }
            }
            SetIntNoResolution(propName + c_CountSuffix, count);
        }

        public int GetInt(string propName, int defaultValue)
        {
            object v = GetValue(propName);
            return v == null ? defaultValue : (int)v;
        }

        public void SetInt(string propName, int value)
        {
            SetValue(propName, value);
        }

        public bool GetBool(string propName, bool defaultValue)
        {
            object v = GetValue(propName);
            return v == null ? defaultValue : ((int)v == 1);
        }

        public void SetBool(string propName, bool value)
        {
            SetValue(propName, value ? 1 : 0);
        }

        public bool GetBoolNoResolution(string propName, bool defaultValue)
        {
            object v = GetValueNoResolution(propName);
            return v == null ? defaultValue : ((int)v == 1);
        }

        public void SetBoolNoResolution(string propName, bool value)
        {
            SetValueNoResolution(propName, value ? 1 : 0);
        }

        public object GetEnum(string propName, object defaultValue, Type enumType)
        {
            string s = GetValue(propName) as string;
            return s == null ? defaultValue : Enum.Parse(enumType, s);
        }

        public void SetEnum(string propName, object value, Type enumType)
        {
            SetValue(propName, Enum.GetName(enumType, value));
        }

        public Rectangle GetRectangle(string propName, Rectangle defaultValue)
        {
            string s = GetValue(propName) as string;
            if (s == null)
                return defaultValue;
            Rectangle result;
            if (!Converter.RectangleFromString(s, out result))
                return defaultValue;
            else
                return result;
        }

        public void SetRectangle(string propName, Rectangle value)
        {
            SetValue(propName, Converter.RectangleToString(value));
        }

        public void SaveFormState(Form form)
        {
            SetEnum("WindowState", form.WindowState, typeof(FormWindowState));
            if (form.WindowState == FormWindowState.Normal)
                SetRectangle("Bounds", form.Bounds);
        }

        public void RestoreFormState(Form form)
        {
            form.Bounds = GetRectangle("Bounds", form.Bounds);
            form.WindowState = (FormWindowState)GetEnum("WindowState", form.WindowState, typeof(FormWindowState));
        }

        #endregion

        #region Public properties

        public string Path
        {
            get { return _path; }
        }

        public Size CurrentResolution
        {
            get { return Screen.PrimaryScreen.Bounds.Size; }
        }

        public string CurrentResolutionStr
        {
            get { return string.Format("{0}x{1}", CurrentResolution.Width, CurrentResolution.Height); }
        }

        #endregion

        #region Sub classes

        private class Converter
        {
            public static NumberFormatInfo InvariantFormatInfo;

            #region Constructors

            static Converter()
            {
                InvariantFormatInfo = new NumberFormatInfo();
                InvariantFormatInfo.NumberDecimalSeparator = ".";
                InvariantFormatInfo.CurrencyDecimalSeparator = ".";
            }

            #endregion

            #region Public static

            public static string RectangleToString(Rectangle r)
            {
                return string.Format(InvariantFormatInfo, "{0},{1},{2},{3}", r.X, r.Y, r.Width, r.Height);
            }

            public static bool RectangleFromString(string s, out Rectangle r)
            {
                r = Rectangle.Empty;
                string[] parts = s.Split(',');
                if (parts.Length != 4)
                    return false;
                int x, y, w, h;
                if (!int.TryParse(parts[0], NumberStyles.Any, InvariantFormatInfo, out x) ||
                    !int.TryParse(parts[1], NumberStyles.Any, InvariantFormatInfo, out y) ||
                    !int.TryParse(parts[2], NumberStyles.Any, InvariantFormatInfo, out w) ||
                    !int.TryParse(parts[3], NumberStyles.Any, InvariantFormatInfo, out h))
                    return false;
                r.X = x;
                r.Y = y;
                r.Width = w;
                r.Height = h;
                return true;
            }

            public static string SizeToString(Size sz)
            {
                return string.Format(InvariantFormatInfo, "{0},{1}", sz.Width, sz.Height);
            }

            public static bool SizeFromString(string s, out Size sz)
            {
                sz = Size.Empty;
                string[] parts = s.Split(',');
                if (parts.Length != 2)
                    return false;
                int w, h;
                if (!int.TryParse(parts[0], NumberStyles.Any, InvariantFormatInfo, out w) ||
                    !int.TryParse(parts[1], NumberStyles.Any, InvariantFormatInfo, out h))
                    return false;
                sz.Width = w;
                sz.Height = h;
                return true;
            }

            #endregion
        }

        #endregion
    }

    internal class RegistryPropsStore : PropsStoreBase
    {
        #region Constructors

        public RegistryPropsStore(string path)
            : base(path)
        {
        }

        #endregion

        #region Protected

        protected override object GetValueNoResolution(string propName)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(string.Format("Software\\ComponentOne\\{0}", Path));
                if (key == null)
                    return null;
                return key.GetValue(propName, null);
            }
            catch
            {
                return null;
            }
        }

        protected override void SetValueNoResolution(string propName, object value)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(string.Format("Software\\ComponentOne\\{0}", Path));
                if (key == null)
                    return;
                key.SetValue(propName, value);
            }
            catch
            {
            }
        }

        protected override object GetValue(string propName)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(string.Format("Software\\ComponentOne\\{0}\\{1}", Path, CurrentResolutionStr));
                if (key == null)
                    return null;
                return key.GetValue(propName, null);
            }
            catch
            {
                return null;
            }
        }

        protected override void SetValue(string propName, object value)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(string.Format("Software\\ComponentOne\\{0}\\{1}", Path, CurrentResolutionStr));
                if (key == null)
                    return;
                key.SetValue(propName, value);
            }
            catch
            {
            }
        }

        #endregion

        #region Public

        public override bool HasValue(string propName)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(string.Format("Software\\ComponentOne\\{0}\\{1}", Path, CurrentResolutionStr));
            if (key == null)
                return false;
            return key.GetValue(propName, null) != null;
        }

        public override bool HasValueNoResolution(string propName)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(string.Format("Software\\ComponentOne\\{0}", Path));
            if (key == null)
                return false;
            return key.GetValue(propName, null) != null;
        }

        #endregion
    }
}
