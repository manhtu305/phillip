using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Reflection;
using System.IO;
using System.Globalization;
using System.Resources;

namespace C1.Win.Localization.Design
{
    internal class AdvCultureInfo
    {
        public CultureInfo CultureInfo;
        public int ModifiedStringCount;
        public int TranslatedStringCount;

        #region Constructors

        public AdvCultureInfo(CultureInfo cultureInfo, int modifiedStringCount, int translatedStringCount)
        {
            CultureInfo = cultureInfo;
            ModifiedStringCount = modifiedStringCount;
            TranslatedStringCount = translatedStringCount;
        }

        #endregion

        #region Public

        public override string ToString()
        {
            return CultureInfo.DisplayName;
        }

        #endregion
    }

    internal class AdvCultureInfoList : List<AdvCultureInfo>
    {
        #region Public

        public AdvCultureInfo FindByCultureInfo(CultureInfo cultureInfo)
        {
            int i = IndexByCultureInfo(cultureInfo);
            return i == -1 ? null : this[i];
        }

        public int IndexByCultureInfo(CultureInfo cultureInfo)
        {
            for (int i = 0; i < Count; i++)
                if (this[i].CultureInfo == cultureInfo)
                    return i;
            return -1;
        }

        #endregion
    }
}
