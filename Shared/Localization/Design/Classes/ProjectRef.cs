using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Reflection;
using System.IO;
using System.Globalization;
using System.Resources;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    internal class ProjectRef : IDisposable
    {
        private Project _project;
        private bool _startUp;
        private ProductCollection _products = new ProductCollection();

        #region Constructors

        public ProjectRef(Project project, bool startUp)
        {
            _project = project;
            _startUp = startUp;
        }

        #endregion

        #region Public

        public void Dispose()
        {
            _project.Dispose();
            _products = null;
        }

        public bool GetLocalizeFolder(bool createIfNotExists, ref ProjectItem pi)
        {
            int itemCount = Project.ProjectItems.Count;
            for (int i = 1; i <= itemCount; i++)
            {
                pi = Project.ProjectItems[i];
                if (pi.Name == StringsManager.c_LocalizeFolderName)
                    return true;
            }

            if (!createIfNotExists)
                return false;

            // unfornunely we can receive an exception
            // if folder c_LocalizeFolderName already exists in the project's directory
            // even if we delete this folder (before calling of AddFolder) the IDE
            // still throw an exception. It looks like a bug in VS.IDE.
            //
            // So the commented code does not work.
            try
            {
                pi = Project.ProjectItems.AddFolder(StringsManager.c_LocalizeFolderName, null);
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format(DesignLocalizationStrings.Localizer.CantCreateLocalizedFolder, StringsManager.c_LocalizeFolderName, e.InnerException == null ? e.Message : e.InnerException.Message), DesignLocalizationStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            /*// we should check does the directory with name c_LocalizeFolderName
            // already exists or not
            // and if exists save its content, delete it, add folder item and
            // copy directory content back
            // we do it because AddFolder method raises an exception
            // if directory already exists

            string tempFolderName = null;
            string tempLocalizedFolderName = null;
            string localizedFolderName = Path.GetDirectoryName(Project.FileName) + "\\" + Utils.c_LocalizeFolderName;
            if (Directory.Exists(localizedFolderName))
            {
                try
                {
                    // move content of directory to another place
                    tempFolderName = Path.GetTempFileName();
                    if (File.Exists(tempFolderName))
                        File.Delete(tempFolderName);
                    Directory.CreateDirectory(tempFolderName);
                    tempLocalizedFolderName = tempFolderName + "\\" + Utils.c_LocalizeFolderName;
                    Directory.Move(localizedFolderName, tempLocalizedFolderName);
                }
                catch
                {
                    return false;
                }
            }

            //
            pi = Project.ProjectItems.AddFolder(Utils.c_LocalizeFolderName, null);

            try
            {
                if (tempLocalizedFolderName != null)
                {
                    // move content of tempLocalizedFolderName back
                    string s = pi.FileName;
                    int tempLocalizedFolderNameLength = tempLocalizedFolderName.Length;
                    string[] files = Directory.GetFiles(tempLocalizedFolderName);
                    foreach (string file in files)
                        File.Move(file, s + file.Substring(tempLocalizedFolderNameLength));
                    string[] directories = Directory.GetDirectories(tempLocalizedFolderName);
                    foreach (string directory in directories)
                        Directory.Move(directory, s + directory.Substring(tempLocalizedFolderNameLength));

                    Directory.Delete(tempFolderName);
                }
            }
            catch
            {
                // simple eat this exception
            }*/
        }

        public List<string> GetResXFilesForProduct(Product product)
        {
            ProjectItem localizeFolder = new ProjectItem();
            if (!GetLocalizeFolder(false, ref localizeFolder) || localizeFolder.ProjectItems.Count == 0)
                return null;

            List<string> result = new List<string>();
            for (int i = 1; i <= localizeFolder.ProjectItems.Count; i++)
            {
                ProjectItem pi = localizeFolder.ProjectItems[i];
                if (pi.FileName == null)
                    continue;
                CultureInfo ci;
                if (product.ParseResXFileName(pi.Name, out ci))
                    result.Add(pi.FileName);
            }
            return result;
        }

        public override string ToString()
        {
            return _project.UniqueName;
        }

        #endregion

        #region Public properties

        public Project Project
        {
            get { return _project; }
        }

        public bool StartUp
        {
            get { return _startUp; }
        }

        public bool IsLangProject
        {
            get { return _project.IsLangProject; }
        }

        public int ImageIndex
        {
            get
            {
                string kind = _project.Kind;
                if (kind == PrjKind.prjKindCSharpProject)
                    return Utils.c_VSProjectImageIndex;
                else if (kind == PrjKind.prjKindVBProject)
                    return Utils.c_VBProjectImageIndex;
                else if (kind == PrjKind2.prjKindVJSharpProject)
                    return Utils.c_VJProjectImageIndex;
                else
                    return Utils.c_UnknownProjectImageIndex; 
            }
        }

        public Image Image
        {
            get
            {
                int imageIndex = ImageIndex;
                return imageIndex == -1 ? null : Utils.Images.Images[ImageIndex];
            }
        }

        public ProductCollection Products
        {
            get { return _products; }
        }

        #endregion

        #region IDisposable implementation

        void IDisposable.Dispose()
        {
            _project.Dispose();
        }

        #endregion
    }

    internal class ProjectRefCollection : List<ProjectRef>
    {
    }
}