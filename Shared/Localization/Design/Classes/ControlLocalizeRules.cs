using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;

namespace C1.Win.Localization.Design
{
    /// <summary>
    /// Defines the default rules of enduser localization.
    /// </summary>
    internal static class ControlLocalizeRules
    {
        private static ControlLocalizeRule[] s_Rules = new ControlLocalizeRule[] {
            new ControlLocalizeRule(typeof(ToolStripControlHost), null),
            new ControlLocalizeRule(typeof(ToolStripSeparator), null),
            new ControlLocalizeRule(typeof(ToolStripItem), "Text", "ToolTipText"),

            new ControlLocalizeRule(typeof(ColumnHeader), "Text"),

            new ControlLocalizeRule(typeof(NumericUpDown), null),
            new ControlLocalizeRule(typeof(TextBox), null),
            new ControlLocalizeRule(typeof(ComboBox), null),
            new ControlLocalizeRule(typeof(DateTimePicker), null),
            new ControlLocalizeRule(typeof(Control), "Text") };

        #region Public static

        /// <summary>
        /// Returns the list of properties that should be localized
        /// for specified type of control.
        /// </summary>
        /// <param name="controlType">Type of control.</param>
        /// <param name="endUserLocalizeOptions">The EndUserLocalizeOptionsAttribute specified for type.</param>
        /// <returns>Returns the list of properties' names or null if control should no localized.</returns>
        public static string[] GetLocalizedProperties(Type controlType, object endUserLocalizeOptions)
        {
            if (endUserLocalizeOptions != null)
            {
                if (Utils.GetExclude(endUserLocalizeOptions))
                    return null;
                return Utils.GetProperties(endUserLocalizeOptions);
            }

            // go other s_Rules
            List<string> result = new List<string>();
            foreach (ControlLocalizeRule rule in s_Rules)
            {
                if (rule.Type.IsAssignableFrom(controlType))
                {
                    if (rule.Properties == null || rule.Properties.Length == 0)
                        return null;
                    foreach (string s in rule.Properties)
                        if (!result.Contains(s))
                            result.Add(s);
                }
            }
            return result.Count == 0 ? null : result.ToArray();
        }

        #endregion
    }

    internal class ControlLocalizeRule
    {
        public Type Type;
        public string[] Properties;

        #region Constructors

        public ControlLocalizeRule(Type type, params string[] properties)
        {
            Type = type;
            Properties = properties;
        }

        #endregion
    }
}
