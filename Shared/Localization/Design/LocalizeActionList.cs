using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;

namespace C1.Win.Localization.Design
{
    /// <summary>
    /// Represents a localization <see cref="DesignerActionList"/>.
    /// </summary>
    public class LocalizeActionList : DesignerActionList
    {
        private ComponentDesigner _designer;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizeActionList"/> class.
        /// </summary>
        /// <param name="component">The designed component.</param>
        /// <param name="designer">The component designer.</param>
        public LocalizeActionList(IComponent component, ComponentDesigner designer)
            : base(component)
        {
            _designer = designer;
        }
        #endregion

        #region Public
        /// <summary>
        /// Returns the list of associated action items.
        /// </summary>
        /// <returns>The list containing the "Localize" item.</returns>
        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection items = new DesignerActionItemCollection();
            items.Add(new DesignerActionMethodItem(this, "Localize", DesignLocalizationStrings.Localizer.DesignActionCaption, true));
            return items;
        }

        /// <summary>
        /// Performs localizaion.
        /// </summary>
        public void Localize()
        {
            StringsDesigner.DoOpenDesigner(_designer);
        }

        /// <summary>
        /// Performs localizaion.
        /// </summary>
        /// <param name="designer">The designer to localize.</param>
        public static void Localize(ComponentDesigner designer)
        {
            StringsDesigner.DoOpenDesigner(designer);
        }
        #endregion
    }
}
