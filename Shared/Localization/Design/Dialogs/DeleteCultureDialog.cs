﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Reflection;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class DeleteCultureDialog : Form
    {
        public DeleteCultureDialog()
        {
            InitializeComponent();
			StringsManager.LocalizeControl(typeof(DesignLocalizationStrings), this, GetType());
        }

        #region Public static

        public static List<AdvCultureInfo> DoSelectCultures(AdvCultureInfoList availableCultures)
        {
            using (DeleteCultureDialog f = new DeleteCultureDialog())
                return f.SelectCultures(availableCultures);
        }

        #endregion

        #region Public

        public List<AdvCultureInfo> SelectCultures(AdvCultureInfoList availableCultures)
        {
            // fill lvCultures
            foreach (AdvCultureInfo ci in availableCultures)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = ci.CultureInfo.EnglishName;
                lvi.SubItems.Add(ci.CultureInfo.Name);
                lvi.SubItems.Add(ci.TranslatedStringCount.ToString());
                lvi.Tag = ci;
                m_lvCultures.Items.Add(lvi);
            }
            ActiveControl = m_lvCultures;

            if (ShowDialog() == DialogResult.OK)
            {
                // build list of selected cultures
                List<AdvCultureInfo> result = new List<AdvCultureInfo>();
                foreach (ListViewItem lvi in m_lvCultures.SelectedItems)
                    result.Add((AdvCultureInfo)lvi.Tag);
                return result;
            }
            return null;
        }

        #endregion

        private void lvCultures_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_btnOk.Enabled = m_lvCultures.SelectedItems.Count > 0 && m_lvCultures.SelectedItems.Count < m_lvCultures.Items.Count;
        }
    }
}