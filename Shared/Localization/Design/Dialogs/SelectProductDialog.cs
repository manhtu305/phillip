using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class SelectProductDialog : Form
    {
        public SelectProductDialog()
        {
            InitializeComponent();
            StringsManager.LocalizeControl(typeof(DesignLocalizationStrings), this, GetType());
        }

        #region Public static

        public static Product DoSelectProduct(IWin32Window owner, string formCaption, ProductCollection products, Product initialValue)
        {
            using (SelectProductDialog f = new SelectProductDialog())
                return f.SelectProduct(owner, formCaption, products, initialValue);
        }

        #endregion

        #region Public

        public Product SelectProduct(IWin32Window owner, string formCaption, ProductCollection products, Product initialValue)
        {
            foreach (Product product in products)
                cbProducts.Items.Add(product);
            cbProducts.SelectedItem = initialValue;
            if (cbProducts.SelectedIndex == -1)
                cbProducts.SelectedIndex = 0;
            Text = formCaption;
            ActiveControl = cbProducts;

            if (ShowDialog(owner) == DialogResult.OK)
                return cbProducts.SelectedItem as Product;
            else
                return null;
        }

        #endregion
    }
}