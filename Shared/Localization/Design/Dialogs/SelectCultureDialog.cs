using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Reflection;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class SelectCultureDialog : Form
    {
        public SelectCultureDialog()
        {
            InitializeComponent();
			StringsManager.LocalizeControl(typeof(DesignLocalizationStrings), this, GetType());
        }

        #region Public static

        public static CultureInfo DoSelectCulture(IWin32Window owner, CultureInfo initialValue)
        {
            using (SelectCultureDialog f = new SelectCultureDialog())
                return f.SelectCulture(owner, initialValue);
        }

        #endregion

        #region Private

        private ListViewItem FindItemByTag(object tag)
        {
            foreach (ListViewItem lvi in lvCultures.Items)
                if (lvi.Tag == tag)
                    return lvi;
            return null;
        }

        private bool CheckText(string text, string property)
        {
            return text.Length == 0 || property.ToLower().Contains(text.ToLower());
        }

        private bool IsCultureVisible(CultureInfo cultureInfo)
        {
            if (m_cbShowOnlyNeutralCultures.Checked && !cultureInfo.IsNeutralCulture)
                return false;
            return
                CheckText(m_tbEnglishName.Text, cultureInfo.EnglishName) &&
                CheckText(m_tbDisplayName.Text, cultureInfo.DisplayName) &&
                CheckText(m_tbCode.Text, cultureInfo.Name);
        }

        private void AddCulture(CultureInfo cultureInfo)
        {
            ListViewItem lvi = new ListViewItem();
            lvi.Text = cultureInfo.EnglishName;
            lvi.SubItems.Add(cultureInfo.DisplayName);
            lvi.SubItems.Add(cultureInfo.Name);
            lvi.Tag = cultureInfo;
            lvCultures.Items.Add(lvi);
        }

        private void UpdateCultures()
        {
            object selectedObject = lvCultures.SelectedItems.Count > 0 ? lvCultures.SelectedItems[0].Tag : null;
            lvCultures.BeginUpdate();
            lvCultures.Items.Clear();

            // Add invariant culture always!!!
            AddCulture(CultureInfo.InvariantCulture);
            // add all other
            foreach (CultureInfo ci in Utils.Cultures.Values)
                if (ci != CultureInfo.InvariantCulture && 
                    ci.Name != null &&
                    ci.Name.Length > 0 &&
                    IsCultureVisible(ci))
                    AddCulture(ci);

            if (lvCultures.Items.Count > 0)
            {
                ListViewItem lvi = selectedObject == null ? null : FindItemByTag(selectedObject);
                if (lvi == null)
                    lvCultures.FocusedItem = lvCultures.Items[0];
                else
                    lvCultures.FocusedItem = lvi;
                if (lvCultures.FocusedItem != null)
                {
                    lvCultures.FocusedItem.Selected = true;
                    lvCultures.FocusedItem.EnsureVisible();
                }
            }
            lvCultures.EndUpdate();
        }

        private void UpdateButtons()
        {
            m_btnOk.Enabled = SelectedCulture != null;
        }

        #endregion

        #region Public

        public CultureInfo SelectCulture(IWin32Window owner, CultureInfo initialValue)
        {
            // force create a handle
            IntPtr handle = lvCultures.Handle;
            UpdateCultures();
            UpdateButtons();
            ActiveControl = lvCultures;

            if (ShowDialog() == DialogResult.OK)
                return SelectedCulture;
            return null;
        }

        #endregion

        #region Public properties

        public CultureInfo SelectedCulture
        {
            get
            {
                if (lvCultures.SelectedItems.Count != 1)
                    return null;
                return lvCultures.SelectedItems[0].Tag as CultureInfo;
            }
        }

        #endregion

        private void lvCultures_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateButtons();
        }

        private void cbShowOnlyNeutralCultures_CheckedChanged(object sender, EventArgs e)
        {
            UpdateCultures();
            UpdateButtons();
        }

        private void tbEnglishName_TextChanged(object sender, EventArgs e)
        {
            UpdateCultures();
            UpdateButtons();
        }

        private void lvCultures_DoubleClick(object sender, EventArgs e)
        {
            if (SelectedCulture != null)
                DialogResult = DialogResult.OK;
        }
    }
}