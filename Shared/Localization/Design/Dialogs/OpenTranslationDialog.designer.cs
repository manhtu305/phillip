namespace C1.Win.Localization.Design
{
    partial class OpenTranslationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_tvTranslations = new System.Windows.Forms.TreeView();
            this.cmsTranslations = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiViewByProject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiViewByProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.m_btnCancel = new System.Windows.Forms.Button();
            this.m_btnOk = new System.Windows.Forms.Button();
            this.m_lblAvailableCultures = new System.Windows.Forms.Label();
            this.m_lblCultures = new System.Windows.Forms.Label();
            this.cmsTranslations.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_tvTranslations
            // 
            this.m_tvTranslations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tvTranslations.ContextMenuStrip = this.cmsTranslations;
            this.m_tvTranslations.HideSelection = false;
            this.m_tvTranslations.ImageIndex = 0;
            this.m_tvTranslations.ImageList = this.imageList;
            this.m_tvTranslations.Location = new System.Drawing.Point(12, 12);
            this.m_tvTranslations.Name = "m_tvTranslations";
            this.m_tvTranslations.SelectedImageIndex = 0;
            this.m_tvTranslations.ShowRootLines = false;
            this.m_tvTranslations.Size = new System.Drawing.Size(257, 216);
            this.m_tvTranslations.TabIndex = 0;
            this.m_tvTranslations.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvTranslations_AfterSelect);
            // 
            // cmsTranslations
            // 
            this.cmsTranslations.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiViewByProject,
            this.tsmiViewByProduct});
            this.cmsTranslations.Name = "cmsTranslations";
            this.cmsTranslations.Size = new System.Drawing.Size(163, 48);
            // 
            // tsmiViewByProject
            // 
            this.tsmiViewByProject.Checked = true;
            this.tsmiViewByProject.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiViewByProject.Name = "tsmiViewByProject";
            this.tsmiViewByProject.Size = new System.Drawing.Size(162, 22);
            this.tsmiViewByProject.Text = "View by project";
            this.tsmiViewByProject.Click += new System.EventHandler(this.tsmiViewByProject_Click);
            // 
            // tsmiViewByProduct
            // 
            this.tsmiViewByProduct.Name = "tsmiViewByProduct";
            this.tsmiViewByProduct.Size = new System.Drawing.Size(162, 22);
            this.tsmiViewByProduct.Text = "View by product";
            this.tsmiViewByProduct.Click += new System.EventHandler(this.tsmiViewByProject_Click);
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Magenta;
            // 
            // m_btnCancel
            // 
            this.m_btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_btnCancel.Location = new System.Drawing.Point(194, 329);
            this.m_btnCancel.Name = "m_btnCancel";
            this.m_btnCancel.Size = new System.Drawing.Size(75, 23);
            this.m_btnCancel.TabIndex = 2;
            this.m_btnCancel.Text = "Cancel";
            this.m_btnCancel.UseVisualStyleBackColor = true;
            // 
            // m_btnOk
            // 
            this.m_btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_btnOk.Location = new System.Drawing.Point(113, 329);
            this.m_btnOk.Name = "m_btnOk";
            this.m_btnOk.Size = new System.Drawing.Size(75, 23);
            this.m_btnOk.TabIndex = 1;
            this.m_btnOk.Text = "OK";
            this.m_btnOk.UseVisualStyleBackColor = true;
            // 
            // m_lblAvailableCultures
            // 
            this.m_lblAvailableCultures.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_lblAvailableCultures.AutoSize = true;
            this.m_lblAvailableCultures.Location = new System.Drawing.Point(12, 231);
            this.m_lblAvailableCultures.Name = "m_lblAvailableCultures";
            this.m_lblAvailableCultures.Size = new System.Drawing.Size(92, 18);
            this.m_lblAvailableCultures.TabIndex = 3;
            this.m_lblAvailableCultures.Text = "Available cultures:";
            this.m_lblAvailableCultures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_lblAvailableCultures.UseCompatibleTextRendering = true;
            // 
            // m_lblCultures
            // 
            this.m_lblCultures.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_lblCultures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_lblCultures.Location = new System.Drawing.Point(12, 248);
            this.m_lblCultures.Name = "m_lblCultures";
            this.m_lblCultures.Size = new System.Drawing.Size(257, 68);
            this.m_lblCultures.TabIndex = 4;
            this.m_lblCultures.UseCompatibleTextRendering = true;
            // 
            // OpenTranslationDialog
            // 
            this.AcceptButton = this.m_btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.m_btnCancel;
            this.ClientSize = new System.Drawing.Size(281, 359);
            this.Controls.Add(this.m_lblCultures);
            this.Controls.Add(this.m_lblAvailableCultures);
            this.Controls.Add(this.m_btnOk);
            this.Controls.Add(this.m_btnCancel);
            this.Controls.Add(this.m_tvTranslations);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OpenTranslationDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Open translation";
            this.cmsTranslations.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.TreeView m_tvTranslations;
        protected System.Windows.Forms.Button m_btnCancel;
        protected System.Windows.Forms.Button m_btnOk;
        protected System.Windows.Forms.ImageList imageList;
        protected System.Windows.Forms.Label m_lblAvailableCultures;
        protected System.Windows.Forms.Label m_lblCultures;
        protected System.Windows.Forms.ContextMenuStrip cmsTranslations;
        protected System.Windows.Forms.ToolStripMenuItem tsmiViewByProject;
        protected System.Windows.Forms.ToolStripMenuItem tsmiViewByProduct;
    }
}