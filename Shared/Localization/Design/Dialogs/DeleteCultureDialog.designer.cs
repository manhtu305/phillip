﻿namespace C1.Win.Localization.Design
{
    partial class DeleteCultureDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_lblAvailableCultures = new System.Windows.Forms.Label();
            this.m_lvCultures = new System.Windows.Forms.ListView();
            this.m_chEnglishName = new System.Windows.Forms.ColumnHeader();
            this.m_chCode = new System.Windows.Forms.ColumnHeader();
            this.m_chStringCount = new System.Windows.Forms.ColumnHeader();
            this.m_btnOk = new System.Windows.Forms.Button();
            this.m_btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_lblAvailableCultures
            // 
            this.m_lblAvailableCultures.AutoSize = true;
            this.m_lblAvailableCultures.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_lblAvailableCultures.Location = new System.Drawing.Point(12, 9);
            this.m_lblAvailableCultures.Name = "m_lblAvailableCultures";
            this.m_lblAvailableCultures.Size = new System.Drawing.Size(95, 16);
            this.m_lblAvailableCultures.TabIndex = 0;
            this.m_lblAvailableCultures.Text = "Available cultures:";
            this.m_lblAvailableCultures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_lblAvailableCultures.UseCompatibleTextRendering = true;
            // 
            // m_lvCultures
            // 
            this.m_lvCultures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_lvCultures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.m_chEnglishName,
            this.m_chCode,
            this.m_chStringCount});
            this.m_lvCultures.FullRowSelect = true;
            this.m_lvCultures.HideSelection = false;
            this.m_lvCultures.Location = new System.Drawing.Point(12, 28);
            this.m_lvCultures.Name = "m_lvCultures";
            this.m_lvCultures.Size = new System.Drawing.Size(339, 136);
            this.m_lvCultures.TabIndex = 1;
            this.m_lvCultures.UseCompatibleStateImageBehavior = false;
            this.m_lvCultures.View = System.Windows.Forms.View.Details;
            this.m_lvCultures.SelectedIndexChanged += new System.EventHandler(this.lvCultures_SelectedIndexChanged);
            // 
            // m_chEnglishName
            // 
            this.m_chEnglishName.Text = "Name";
            this.m_chEnglishName.Width = 163;
            // 
            // m_chCode
            // 
            this.m_chCode.Text = "Code";
            // 
            // m_chStringCount
            // 
            this.m_chStringCount.Text = "Count of strings";
            this.m_chStringCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_chStringCount.Width = 93;
            // 
            // m_btnOk
            // 
            this.m_btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_btnOk.Location = new System.Drawing.Point(195, 170);
            this.m_btnOk.Name = "m_btnOk";
            this.m_btnOk.Size = new System.Drawing.Size(75, 23);
            this.m_btnOk.TabIndex = 2;
            this.m_btnOk.Text = "OK";
            this.m_btnOk.UseVisualStyleBackColor = true;
            // 
            // m_btnCancel
            // 
            this.m_btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_btnCancel.Location = new System.Drawing.Point(276, 170);
            this.m_btnCancel.Name = "m_btnCancel";
            this.m_btnCancel.Size = new System.Drawing.Size(75, 23);
            this.m_btnCancel.TabIndex = 3;
            this.m_btnCancel.Text = "Cancel";
            this.m_btnCancel.UseVisualStyleBackColor = true;
            // 
            // DeleteCultureDialog
            // 
            this.AcceptButton = this.m_btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.m_btnCancel;
            this.ClientSize = new System.Drawing.Size(363, 201);
            this.Controls.Add(this.m_btnOk);
            this.Controls.Add(this.m_btnCancel);
            this.Controls.Add(this.m_lvCultures);
            this.Controls.Add(this.m_lblAvailableCultures);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeleteCultureDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select cultures to delete";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Label m_lblAvailableCultures;
        protected System.Windows.Forms.ListView m_lvCultures;
        protected System.Windows.Forms.ColumnHeader m_chEnglishName;
        protected System.Windows.Forms.ColumnHeader m_chCode;
        protected System.Windows.Forms.ColumnHeader m_chStringCount;
        protected System.Windows.Forms.Button m_btnOk;
        protected System.Windows.Forms.Button m_btnCancel;
    }
}