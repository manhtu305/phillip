using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Reflection;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class OpenTranslationDialog : Form
    {
        private const string c_PropsStoreKey = "C1Localizer\\OpenTranslationDialog";

        private DTE _dte;
        private Dictionary<object, int> _imageIndexes = new Dictionary<object, int>();
        private ProductCollection _products;
        private ProjectRefCollection _projects;
        private ViewModeEnum _viewMode = ViewModeEnum.ByProject;

        public OpenTranslationDialog()
        {
            InitializeComponent();
			StringsManager.LocalizeControl(typeof(DesignLocalizationStrings), this, GetType());
        }

        #region Public static

        public static bool DoOpen(
            IWin32Window owner,
            DTE dte,
            ProductCollection products,
            ProjectRefCollection projects,
            out Product product,
            out ProjectRef project)
        {
            using (OpenTranslationDialog f = new OpenTranslationDialog())
                return f.Open(owner, dte, products, projects, out product, out project);
        }

        #endregion

        #region Private

        private void FillByProduct(TreeNode parent)
        {
            foreach (Product product in _products)
            {
                // select projects where exists resources for this product
                List<ProjectRef> projects = new List<ProjectRef>();
                foreach (ProjectRef project in _projects)
                {
                    List<string> files = project.GetResXFilesForProduct(product);
                    if (files != null && files.Count > 0)
                        projects.Add(project);
                }
                if (projects.Count <= 0)
                    continue;

                TreeNode productNode = new TreeNode(product.ToString());
                productNode.ImageIndex = _imageIndexes.ContainsKey(product) ? _imageIndexes[product] : -1;
                productNode.SelectedImageIndex = productNode.ImageIndex;
                productNode.Tag = product;
                parent.Nodes.Add(productNode);

                foreach (ProjectRef project in projects)
                {
                    TreeNode projectNode = new TreeNode(project.ToString());
                    projectNode.ImageIndex = _imageIndexes.ContainsKey(project) ? _imageIndexes[project] : -1;
                    projectNode.SelectedImageIndex = projectNode.ImageIndex;
                    projectNode.Tag = project;
                    productNode.Nodes.Add(projectNode);
                }
            }
        }

        private void FillByProject(TreeNode parent)
        {
            foreach (ProjectRef project in _projects)
            {
                List<Product> products = new List<Product>();
                foreach (Product product in _products)
                {
                    List<string> files = project.GetResXFilesForProduct(product);
                    if (files != null && files.Count > 0)
                        products.Add(product);
                }
                if (products.Count <= 0)
                    continue;

                TreeNode projectNode = new TreeNode(project.ToString());
                projectNode.ImageIndex = _imageIndexes.ContainsKey(project) ? _imageIndexes[project] : -1;
                projectNode.SelectedImageIndex = projectNode.ImageIndex;
                projectNode.Tag = project;
                parent.Nodes.Add(projectNode);

                foreach (Product product in products)
                {
                    TreeNode productNode = new TreeNode(product.ToString());
                    productNode.ImageIndex = _imageIndexes.ContainsKey(product) ? _imageIndexes[product] : -1;
                    productNode.SelectedImageIndex = productNode.ImageIndex;
                    productNode.Tag = product;
                    projectNode.Nodes.Add(productNode);
                }
            }
        }

        private void UpdateTranslations()
        {
            object selectedObject = m_tvTranslations.SelectedNode == null ? null : m_tvTranslations.SelectedNode.Tag;
            m_tvTranslations.BeginUpdate();
            m_tvTranslations.Nodes.Clear();

            // add root node
            TreeNode root = new TreeNode(string.Format(DesignLocalizationStrings.Localizer.OpenTranslationDialog.SolutionMask, Path.GetFileNameWithoutExtension(_dte.Solution.FullName)), 0, 0);
            m_tvTranslations.Nodes.Add(root);

            switch (_viewMode)
            {
                case ViewModeEnum.ByProduct:
                    FillByProduct(root);
                    break;
                case ViewModeEnum.ByProject:
                    FillByProject(root);
                    break;
            }

            root.ExpandAll();

            m_tvTranslations.SelectedNode = Utils.FindNodeByTag(m_tvTranslations.Nodes, selectedObject);
            m_tvTranslations.EndUpdate();
        }

        private void UpdateButtons()
        {
            Product product;
            ProjectRef project;
            GetSelected(out product, out project);
            m_btnOk.Enabled = product != null && project != null;
        }

        #endregion

        #region Protected

        protected override void OnClosed(EventArgs e)
        {
            RegistryPropsStore rps = new RegistryPropsStore(c_PropsStoreKey);
            rps.SetEnumNoResolution("ViewMode", _viewMode, typeof(ViewModeEnum));
            base.OnClosed(e);
        }

        #endregion

        #region Public

        public bool Open(
            IWin32Window owner,
            DTE dte,
            ProductCollection products,
            ProjectRefCollection projects,
            out Product product,
            out ProjectRef project)
        {
            RegistryPropsStore rps = new RegistryPropsStore(c_PropsStoreKey);
            _viewMode = (ViewModeEnum)rps.GetEnumNoResolution("ViewMode", _viewMode, typeof(ViewModeEnum));
            tsmiViewByProduct.Checked = _viewMode == ViewModeEnum.ByProduct;
            tsmiViewByProject.Checked = _viewMode == ViewModeEnum.ByProject;

            _dte = dte;
            _products = products;
            _projects = projects;

            // prepare the list of images
            imageList.Images.Add(Utils.Images.Images[Utils.c_SolutionImageIndex]);
            foreach (Product p in _products)
            {
                if (p.Image == null)
                    continue;
                imageList.Images.Add(p.Image);
                _imageIndexes.Add(p, imageList.Images.Count - 1);
            }
            foreach (ProjectRef p in _projects)
            {
                if (p.Image == null)
                    continue;
                imageList.Images.Add(p.Image);
                _imageIndexes.Add(p, imageList.Images.Count - 1);
            }

            UpdateTranslations();
            UpdateButtons();

            ActiveControl = m_tvTranslations;

            if (ShowDialog(owner) == DialogResult.OK)
            {
                GetSelected(out product, out project);
                return true;
            }
            else
            {
                product = null;
                project = null;
                return false;
            }
        }

        public void GetSelected(out Product product, out ProjectRef project)
        {
            product = null;
            project = null;
            TreeNode node = m_tvTranslations.SelectedNode;
            if (node == null || node.Level != 2)
                return;

            switch (_viewMode)
            {
                case ViewModeEnum.ByProduct:
                    product = node.Parent.Tag as Product;
                    project = node.Tag as ProjectRef;
                    break;
                case ViewModeEnum.ByProject:
                    product = node.Tag as Product;
                    project = node.Parent.Tag as ProjectRef;
                    break;
            }
        }

        #endregion

        #region Public properties

        public Product SelectedProduct
        {
            get
            {
                Product product;
                ProjectRef project;
                GetSelected(out product, out project);
                return product;
            }
        }

        public ProjectRef SelectedProject
        {
            get
            {
                Product product;
                ProjectRef project;
                GetSelected(out product, out project);
                return project;
            }
        }

        #endregion

        #region Sub classes

        public enum ViewModeEnum
        {
            ByProject,
            ByProduct,
        }

        #endregion

        private void tvTranslations_AfterSelect(object sender, TreeViewEventArgs e)
        {
            UpdateButtons();
            Product product;
            ProjectRef project;
            GetSelected(out product, out project);
            if (product == null || project == null)
                m_lblCultures.Text = DesignLocalizationStrings.Localizer.OpenTranslationDialog.NoCultures;
            else
            {
                List<string> files = project.GetResXFilesForProduct(product);
                if (files == null || files.Count == 0)
                    m_lblCultures.Text = DesignLocalizationStrings.Localizer.OpenTranslationDialog.NoCultures;
                else
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (string fileName in files)
                    {
                        CultureInfo cultureInfo;
                        string productFileName;
                        if (!Utils.ParseResXFileName(fileName, out productFileName, out cultureInfo))
                            continue;
                        sb.Append(Utils.GetCultureDescription(cultureInfo));
                        sb.Append("\r\n");
                    }
                    m_lblCultures.Text = sb.ToString();
                }
            }
        }

        private void tsmiViewByProject_Click(object sender, EventArgs e)
        {
            if (sender == tsmiViewByProduct)
            {
                _viewMode = ViewModeEnum.ByProduct;
                tsmiViewByProject.Checked = false;
                tsmiViewByProduct.Checked = true;
            }
            else if (sender == tsmiViewByProject)
            {
                _viewMode = ViewModeEnum.ByProject;
                tsmiViewByProject.Checked = true;
                tsmiViewByProduct.Checked = false;
            }
            else
                return;
            UpdateTranslations();
        }
    }
}