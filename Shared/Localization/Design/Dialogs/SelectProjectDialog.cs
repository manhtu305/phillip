using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using C1.Win.Localization;

namespace C1.Win.Localization.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class SelectProjectDialog : Form
    {
        public SelectProjectDialog()
        {
            InitializeComponent();
			StringsManager.LocalizeControl(typeof(DesignLocalizationStrings), this, GetType());
        }

        #region Public static

        public static ProjectRef DoSelectProject(IWin32Window owner, string formCaption, ProjectRefCollection projects, ProjectRef initialValue)
        {
            using (SelectProjectDialog f = new SelectProjectDialog())
                return f.SelectProject(owner, formCaption, projects, initialValue);
        }

        #endregion

        #region Public

        public ProjectRef SelectProject(IWin32Window owner, string formCaption, ProjectRefCollection projects, ProjectRef initialValue)
        {
            foreach (ProjectRef project in projects)
                cbProjects.Items.Add(project);
            cbProjects.SelectedItem = initialValue;
            if (cbProjects.SelectedIndex == -1)
                cbProjects.SelectedIndex = 0;
            Text = formCaption;
            ActiveControl = cbProjects;

            if (ShowDialog(owner) == DialogResult.OK)
                return cbProjects.SelectedItem as ProjectRef;
            else
                return null;
        }

        #endregion
    }
}