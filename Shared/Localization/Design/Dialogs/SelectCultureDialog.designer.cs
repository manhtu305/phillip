namespace C1.Win.Localization.Design
{
    partial class SelectCultureDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_lblAvailableCultures = new System.Windows.Forms.Label();
            this.lvCultures = new System.Windows.Forms.ListView();
            this.m_chEnglishName = new System.Windows.Forms.ColumnHeader();
            this.m_chDisplayName = new System.Windows.Forms.ColumnHeader();
            this.m_chName = new System.Windows.Forms.ColumnHeader();
            this.m_gbFilter = new System.Windows.Forms.GroupBox();
            this.m_tbCode = new System.Windows.Forms.TextBox();
            this.m_tbDisplayName = new System.Windows.Forms.TextBox();
            this.m_tbEnglishName = new System.Windows.Forms.TextBox();
            this.m_lblCode = new System.Windows.Forms.Label();
            this.m_lblDisplayName = new System.Windows.Forms.Label();
            this.m_lblEnglishName = new System.Windows.Forms.Label();
            this.m_cbShowOnlyNeutralCultures = new System.Windows.Forms.CheckBox();
            this.m_btnOk = new System.Windows.Forms.Button();
            this.m_btnCancel = new System.Windows.Forms.Button();
            this.m_gbFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_lblAvailableCultures
            // 
            this.m_lblAvailableCultures.AutoSize = true;
            this.m_lblAvailableCultures.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_lblAvailableCultures.Location = new System.Drawing.Point(12, 9);
            this.m_lblAvailableCultures.Name = "m_lblAvailableCultures";
            this.m_lblAvailableCultures.Size = new System.Drawing.Size(95, 16);
            this.m_lblAvailableCultures.TabIndex = 0;
            this.m_lblAvailableCultures.Text = "Available cultures:";
            this.m_lblAvailableCultures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_lblAvailableCultures.UseCompatibleTextRendering = true;
            // 
            // lvCultures
            // 
            this.lvCultures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvCultures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.m_chEnglishName,
            this.m_chDisplayName,
            this.m_chName});
            this.lvCultures.FullRowSelect = true;
            this.lvCultures.HideSelection = false;
            this.lvCultures.Location = new System.Drawing.Point(12, 29);
            this.lvCultures.MultiSelect = false;
            this.lvCultures.Name = "lvCultures";
            this.lvCultures.ShowItemToolTips = true;
            this.lvCultures.Size = new System.Drawing.Size(362, 193);
            this.lvCultures.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvCultures.TabIndex = 1;
            this.lvCultures.UseCompatibleStateImageBehavior = false;
            this.lvCultures.View = System.Windows.Forms.View.Details;
            this.lvCultures.DoubleClick += new System.EventHandler(this.lvCultures_DoubleClick);
            this.lvCultures.SelectedIndexChanged += new System.EventHandler(this.lvCultures_SelectedIndexChanged);
            // 
            // m_chEnglishName
            // 
            this.m_chEnglishName.Name = "chEnglishName";
            this.m_chEnglishName.Text = "English name";
            this.m_chEnglishName.Width = 140;
            // 
            // m_chDisplayName
            // 
            this.m_chDisplayName.Name = "chDisplayName";
            this.m_chDisplayName.Text = "Display name";
            this.m_chDisplayName.Width = 140;
            // 
            // m_chName
            // 
            this.m_chName.Name = "chName";
            this.m_chName.Text = "Code";
            // 
            // m_gbFilter
            // 
            this.m_gbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_gbFilter.Controls.Add(this.m_tbCode);
            this.m_gbFilter.Controls.Add(this.m_tbDisplayName);
            this.m_gbFilter.Controls.Add(this.m_tbEnglishName);
            this.m_gbFilter.Controls.Add(this.m_lblCode);
            this.m_gbFilter.Controls.Add(this.m_lblDisplayName);
            this.m_gbFilter.Controls.Add(this.m_lblEnglishName);
            this.m_gbFilter.Controls.Add(this.m_cbShowOnlyNeutralCultures);
            this.m_gbFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_gbFilter.Location = new System.Drawing.Point(12, 228);
            this.m_gbFilter.Name = "m_gbFilter";
            this.m_gbFilter.Size = new System.Drawing.Size(362, 91);
            this.m_gbFilter.TabIndex = 2;
            this.m_gbFilter.TabStop = false;
            this.m_gbFilter.Text = " Filter ";
            // 
            // m_tbCode
            // 
            this.m_tbCode.Location = new System.Drawing.Point(272, 59);
            this.m_tbCode.Name = "m_tbCode";
            this.m_tbCode.Size = new System.Drawing.Size(84, 20);
            this.m_tbCode.TabIndex = 6;
            this.m_tbCode.TextChanged += new System.EventHandler(this.tbEnglishName_TextChanged);
            // 
            // m_tbDisplayName
            // 
            this.m_tbDisplayName.Location = new System.Drawing.Point(139, 59);
            this.m_tbDisplayName.Name = "m_tbDisplayName";
            this.m_tbDisplayName.Size = new System.Drawing.Size(127, 20);
            this.m_tbDisplayName.TabIndex = 5;
            this.m_tbDisplayName.TextChanged += new System.EventHandler(this.tbEnglishName_TextChanged);
            // 
            // m_tbEnglishName
            // 
            this.m_tbEnglishName.Location = new System.Drawing.Point(6, 59);
            this.m_tbEnglishName.Name = "m_tbEnglishName";
            this.m_tbEnglishName.Size = new System.Drawing.Size(127, 20);
            this.m_tbEnglishName.TabIndex = 4;
            this.m_tbEnglishName.TextChanged += new System.EventHandler(this.tbEnglishName_TextChanged);
            // 
            // m_lblCode
            // 
            this.m_lblCode.AutoSize = true;
            this.m_lblCode.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_lblCode.Location = new System.Drawing.Point(272, 39);
            this.m_lblCode.Name = "m_lblCode";
            this.m_lblCode.Size = new System.Drawing.Size(36, 16);
            this.m_lblCode.TabIndex = 3;
            this.m_lblCode.Text = "Code:";
            this.m_lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_lblCode.UseCompatibleTextRendering = true;
            // 
            // m_lblDisplayName
            // 
            this.m_lblDisplayName.AutoSize = true;
            this.m_lblDisplayName.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_lblDisplayName.Location = new System.Drawing.Point(139, 39);
            this.m_lblDisplayName.Name = "m_lblDisplayName";
            this.m_lblDisplayName.Size = new System.Drawing.Size(74, 16);
            this.m_lblDisplayName.TabIndex = 2;
            this.m_lblDisplayName.Text = "Display name:";
            this.m_lblDisplayName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_lblDisplayName.UseCompatibleTextRendering = true;
            // 
            // m_lblEnglishName
            // 
            this.m_lblEnglishName.AutoSize = true;
            this.m_lblEnglishName.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_lblEnglishName.Location = new System.Drawing.Point(6, 39);
            this.m_lblEnglishName.Name = "m_lblEnglishName";
            this.m_lblEnglishName.Size = new System.Drawing.Size(73, 16);
            this.m_lblEnglishName.TabIndex = 1;
            this.m_lblEnglishName.Text = "English name:";
            this.m_lblEnglishName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_lblEnglishName.UseCompatibleTextRendering = true;
            // 
            // m_cbShowOnlyNeutralCultures
            // 
            this.m_cbShowOnlyNeutralCultures.AutoSize = true;
            this.m_cbShowOnlyNeutralCultures.Checked = true;
            this.m_cbShowOnlyNeutralCultures.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_cbShowOnlyNeutralCultures.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_cbShowOnlyNeutralCultures.Location = new System.Drawing.Point(6, 19);
            this.m_cbShowOnlyNeutralCultures.Name = "m_cbShowOnlyNeutralCultures";
            this.m_cbShowOnlyNeutralCultures.Size = new System.Drawing.Size(159, 18);
            this.m_cbShowOnlyNeutralCultures.TabIndex = 0;
            this.m_cbShowOnlyNeutralCultures.Text = "Show only neutral cultures";
            this.m_cbShowOnlyNeutralCultures.UseVisualStyleBackColor = true;
            this.m_cbShowOnlyNeutralCultures.CheckedChanged += new System.EventHandler(this.cbShowOnlyNeutralCultures_CheckedChanged);
            // 
            // m_btnOk
            // 
            this.m_btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_btnOk.Location = new System.Drawing.Point(218, 329);
            this.m_btnOk.Name = "m_btnOk";
            this.m_btnOk.Size = new System.Drawing.Size(75, 23);
            this.m_btnOk.TabIndex = 3;
            this.m_btnOk.Text = "OK";
            this.m_btnOk.UseVisualStyleBackColor = true;
            // 
            // m_btnCancel
            // 
            this.m_btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.m_btnCancel.Location = new System.Drawing.Point(299, 329);
            this.m_btnCancel.Name = "m_btnCancel";
            this.m_btnCancel.Size = new System.Drawing.Size(75, 23);
            this.m_btnCancel.TabIndex = 4;
            this.m_btnCancel.Text = "Cancel";
            this.m_btnCancel.UseVisualStyleBackColor = true;
            // 
            // SelectCultureDialog
            // 
            this.AcceptButton = this.m_btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.m_btnCancel;
            this.ClientSize = new System.Drawing.Size(386, 357);
            this.Controls.Add(this.m_btnCancel);
            this.Controls.Add(this.m_btnOk);
            this.Controls.Add(this.m_gbFilter);
            this.Controls.Add(this.lvCultures);
            this.Controls.Add(this.m_lblAvailableCultures);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectCultureDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select culture";
            this.m_gbFilter.ResumeLayout(false);
            this.m_gbFilter.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Label m_lblAvailableCultures;
        protected System.Windows.Forms.ListView lvCultures;
        protected System.Windows.Forms.ColumnHeader m_chEnglishName;
        protected System.Windows.Forms.ColumnHeader m_chDisplayName;
        protected System.Windows.Forms.ColumnHeader m_chName;
        protected System.Windows.Forms.GroupBox m_gbFilter;
        protected System.Windows.Forms.CheckBox m_cbShowOnlyNeutralCultures;
        protected System.Windows.Forms.Label m_lblCode;
        protected System.Windows.Forms.Label m_lblDisplayName;
        protected System.Windows.Forms.Label m_lblEnglishName;
        protected System.Windows.Forms.TextBox m_tbCode;
        protected System.Windows.Forms.TextBox m_tbDisplayName;
        protected System.Windows.Forms.TextBox m_tbEnglishName;
        protected System.Windows.Forms.Button m_btnOk;
        protected System.Windows.Forms.Button m_btnCancel;
    }
}