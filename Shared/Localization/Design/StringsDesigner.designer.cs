﻿namespace C1.Win.Localization.Design
{
    partial class StringsDesigner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            if (_domain != null)
            {
                try
                {
                    System.AppDomain.Unload(_domain);
                }
                catch
                {
                }

                _domain = null;
                try
                {
                    if (!string.IsNullOrEmpty(_domainAssembliesPath) && System.IO.Directory.Exists(_domainAssembliesPath))
                        System.IO.Directory.Delete(_domainAssembliesPath);
                }
                catch
                {
                    _domainAssembliesPath = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StringsDesigner));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_toolStrip = new System.Windows.Forms.ToolStrip();
            this.m_tsbNew = new System.Windows.Forms.ToolStripButton();
            this.m_tssbOpen = new System.Windows.Forms.ToolStripSplitButton();
            this.m_tsbSave = new System.Windows.Forms.ToolStripButton();
            this.m_tsbSaveAs = new System.Windows.Forms.ToolStripButton();
            this.m_tss1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_tsbAddCulture = new System.Windows.Forms.ToolStripButton();
            this.m_tsbDeleteCulture = new System.Windows.Forms.ToolStripButton();
            this.m_tslCulture = new System.Windows.Forms.ToolStripLabel();
            this.m_tscbCurrentCulture = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_tsddbOptions = new System.Windows.Forms.ToolStripDropDownButton();
            this.m_tsmiSyncTreeWithGrid = new System.Windows.Forms.ToolStripMenuItem();
            this.m_tsmiShowNamesInGrid = new System.Windows.Forms.ToolStripMenuItem();
            this.m_tsmiShowSelectedGroupOnly = new System.Windows.Forms.ToolStripMenuItem();
            this.m_statusStrip = new System.Windows.Forms.StatusStrip();
            this.m_tsslProject = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_tsslCurrentProject = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_tsslProduct = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_tsslCurrentProduct = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_splitPanel1 = new System.Windows.Forms.Panel();
            this.m_splitPanel2 = new System.Windows.Forms.Panel();
            this.m_splitter = new System.Windows.Forms.Splitter();
            this.m_tvStrings = new System.Windows.Forms.TreeView();
            this.m_imageList = new System.Windows.Forms.ImageList(this.components);
            this.m_dgvList = new System.Windows.Forms.DataGridView();
            this.m_dgvcName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_dgvcDefaultValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_dgvcValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_lblList = new System.Windows.Forms.Label();
            this.m_pnlBottom = new System.Windows.Forms.Panel();
            this.m_lblStringName = new System.Windows.Forms.Label();
            this.m_lblDescription = new System.Windows.Forms.Label();
            this.m_tbValue = new System.Windows.Forms.TextBox();
            this.m_toolStrip.SuspendLayout();
            this.m_statusStrip.SuspendLayout();
            this.m_splitPanel1.SuspendLayout();
            this.m_splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvList)).BeginInit();
            this.m_pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_toolStrip
            // 
            this.m_toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.m_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_tsbNew,
            this.m_tssbOpen,
            this.m_tsbSave,
            this.m_tsbSaveAs,
            this.m_tss1,
            this.m_tsbAddCulture,
            this.m_tsbDeleteCulture,
            this.m_tslCulture,
            this.m_tscbCurrentCulture,
            this.toolStripSeparator1,
            this.m_tsddbOptions});
            this.m_toolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_toolStrip.Name = "m_toolStrip";
            this.m_toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.m_toolStrip.Size = new System.Drawing.Size(679, 25);
            this.m_toolStrip.TabIndex = 6;
            // 
            // m_tsbNew
            // 
            this.m_tsbNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsbNew.Image = ((System.Drawing.Image)(resources.GetObject("m_tsbNew.Image")));
            this.m_tsbNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsbNew.Name = "m_tsbNew";
            this.m_tsbNew.Size = new System.Drawing.Size(23, 22);
            this.m_tsbNew.Text = "New";
            this.m_tsbNew.ToolTipText = "Create new translation";
            this.m_tsbNew.Click += new System.EventHandler(this.tsbNew_Click);
            // 
            // m_tssbOpen
            // 
            this.m_tssbOpen.Image = ((System.Drawing.Image)(resources.GetObject("m_tssbOpen.Image")));
            this.m_tssbOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tssbOpen.Name = "m_tssbOpen";
            this.m_tssbOpen.Size = new System.Drawing.Size(77, 22);
            this.m_tssbOpen.Text = "Open...";
            this.m_tssbOpen.ToolTipText = "Open translation";
            this.m_tssbOpen.ButtonClick += new System.EventHandler(this.tssbOpen_ButtonClick);
            // 
            // m_tsbSave
            // 
            this.m_tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsbSave.Image = ((System.Drawing.Image)(resources.GetObject("m_tsbSave.Image")));
            this.m_tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsbSave.Name = "m_tsbSave";
            this.m_tsbSave.Size = new System.Drawing.Size(23, 22);
            this.m_tsbSave.Text = "Save";
            this.m_tsbSave.ToolTipText = "Save translation";
            this.m_tsbSave.Click += new System.EventHandler(this.tsbSave_Click);
            // 
            // m_tsbSaveAs
            // 
            this.m_tsbSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("m_tsbSaveAs.Image")));
            this.m_tsbSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsbSaveAs.Name = "m_tsbSaveAs";
            this.m_tsbSaveAs.Size = new System.Drawing.Size(77, 22);
            this.m_tsbSaveAs.Text = "Save as...";
            this.m_tsbSaveAs.ToolTipText = "Save translation to another project";
            this.m_tsbSaveAs.Click += new System.EventHandler(this.tsbSaveAs_Click);
            // 
            // m_tss1
            // 
            this.m_tss1.Name = "m_tss1";
            this.m_tss1.Size = new System.Drawing.Size(6, 25);
            // 
            // m_tsbAddCulture
            // 
            this.m_tsbAddCulture.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsbAddCulture.Image = ((System.Drawing.Image)(resources.GetObject("m_tsbAddCulture.Image")));
            this.m_tsbAddCulture.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsbAddCulture.Name = "m_tsbAddCulture";
            this.m_tsbAddCulture.Size = new System.Drawing.Size(23, 22);
            this.m_tsbAddCulture.Text = "Add culture";
            this.m_tsbAddCulture.Click += new System.EventHandler(this.tsbAddCulture_Click);
            // 
            // m_tsbDeleteCulture
            // 
            this.m_tsbDeleteCulture.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsbDeleteCulture.Image = ((System.Drawing.Image)(resources.GetObject("m_tsbDeleteCulture.Image")));
            this.m_tsbDeleteCulture.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsbDeleteCulture.Name = "m_tsbDeleteCulture";
            this.m_tsbDeleteCulture.Size = new System.Drawing.Size(23, 22);
            this.m_tsbDeleteCulture.Text = "Delete culture";
            this.m_tsbDeleteCulture.Click += new System.EventHandler(this.tssbDeleteCulture_ButtonClick);
            // 
            // m_tslCulture
            // 
            this.m_tslCulture.Name = "m_tslCulture";
            this.m_tslCulture.Size = new System.Drawing.Size(42, 22);
            this.m_tslCulture.Text = "Culture";
            // 
            // m_tscbCurrentCulture
            // 
            this.m_tscbCurrentCulture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_tscbCurrentCulture.Name = "m_tscbCurrentCulture";
            this.m_tscbCurrentCulture.Size = new System.Drawing.Size(210, 25);
            this.m_tscbCurrentCulture.SelectedIndexChanged += new System.EventHandler(this.m_tscbCurrentCulture_SelectedIndexChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // m_tsddbOptions
            // 
            this.m_tsddbOptions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_tsddbOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_tsmiSyncTreeWithGrid,
            this.m_tsmiShowNamesInGrid,
            this.m_tsmiShowSelectedGroupOnly});
            this.m_tsddbOptions.Image = ((System.Drawing.Image)(resources.GetObject("m_tsddbOptions.Image")));
            this.m_tsddbOptions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_tsddbOptions.Name = "m_tsddbOptions";
            this.m_tsddbOptions.Size = new System.Drawing.Size(29, 22);
            this.m_tsddbOptions.ToolTipText = "Options";
            // 
            // m_tsmiSyncTreeWithGrid
            // 
            this.m_tsmiSyncTreeWithGrid.Name = "m_tsmiSyncTreeWithGrid";
            this.m_tsmiSyncTreeWithGrid.Size = new System.Drawing.Size(197, 22);
            this.m_tsmiSyncTreeWithGrid.Text = "Sync tree with grid";
            this.m_tsmiSyncTreeWithGrid.Click += new System.EventHandler(this.m_tsmiSyncTreeWithGrid_Click);
            // 
            // m_tsmiShowNamesInGrid
            // 
            this.m_tsmiShowNamesInGrid.Name = "m_tsmiShowNamesInGrid";
            this.m_tsmiShowNamesInGrid.Size = new System.Drawing.Size(197, 22);
            this.m_tsmiShowNamesInGrid.Text = "Show names in grid";
            this.m_tsmiShowNamesInGrid.Click += new System.EventHandler(this.m_tsmiShowNamesInGrid_Click);
            // 
            // m_tsmiShowSelectedGroupOnly
            // 
            this.m_tsmiShowSelectedGroupOnly.Name = "m_tsmiShowSelectedGroupOnly";
            this.m_tsmiShowSelectedGroupOnly.Size = new System.Drawing.Size(197, 22);
            this.m_tsmiShowSelectedGroupOnly.Text = "Show selected group only";
            this.m_tsmiShowSelectedGroupOnly.Click += new System.EventHandler(this.m_tsmiShowSelectedGroupOnly_Click);
            // 
            // m_statusStrip
            // 
            this.m_statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_tsslProject,
            this.m_tsslCurrentProject,
            this.m_tsslProduct,
            this.m_tsslCurrentProduct});
            this.m_statusStrip.Location = new System.Drawing.Point(0, 351);
            this.m_statusStrip.Name = "m_statusStrip";
            this.m_statusStrip.Size = new System.Drawing.Size(679, 22);
            this.m_statusStrip.TabIndex = 8;
            this.m_statusStrip.Resize += new System.EventHandler(this.m_statusStrip_Resize);
            // 
            // m_tsslProject
            // 
            this.m_tsslProject.Name = "m_tsslProject";
            this.m_tsslProject.Size = new System.Drawing.Size(48, 17);
            this.m_tsslProject.Text = "Project: ";
            this.m_tsslProject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_tsslCurrentProject
            // 
            this.m_tsslCurrentProject.AutoSize = false;
            this.m_tsslCurrentProject.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_tsslCurrentProject.Name = "m_tsslCurrentProject";
            this.m_tsslCurrentProject.Size = new System.Drawing.Size(250, 17);
            this.m_tsslCurrentProject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_tsslProduct
            // 
            this.m_tsslProduct.Name = "m_tsslProduct";
            this.m_tsslProduct.Size = new System.Drawing.Size(51, 17);
            this.m_tsslProduct.Text = "Product: ";
            this.m_tsslProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_tsslCurrentProduct
            // 
            this.m_tsslCurrentProduct.AutoSize = false;
            this.m_tsslCurrentProduct.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_tsslCurrentProduct.Name = "m_tsslCurrentProduct";
            this.m_tsslCurrentProduct.Size = new System.Drawing.Size(200, 17);
            this.m_tsslCurrentProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_splitPanel1
            // 
            this.m_splitPanel1.Controls.Add(this.m_tvStrings);
            this.m_splitPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.m_splitPanel1.Location = new System.Drawing.Point(0, 25);
            this.m_splitPanel1.Name = "m_splitPanel1";
            this.m_splitPanel1.Size = new System.Drawing.Size(195, 326);
            this.m_splitPanel1.TabIndex = 7;
            // 
            // m_splitter
            // 
            this.m_splitter.Location = new System.Drawing.Point(195, 25);
            this.m_splitter.Name = "m_splitter";
            this.m_splitter.Size = new System.Drawing.Size(4, 326);
            this.m_splitter.TabIndex = 8;
            this.m_splitter.TabStop = false;
            // 
            // m_splitPanel2
            // 
            this.m_splitPanel2.Controls.Add(this.m_dgvList);
            this.m_splitPanel2.Controls.Add(this.m_lblList);
            this.m_splitPanel2.Controls.Add(this.m_pnlBottom);
            this.m_splitPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitPanel1.Location = new System.Drawing.Point(200, 25);
            this.m_splitPanel2.Name = "m_splitPanel2";
            this.m_splitPanel2.Size = new System.Drawing.Size(479, 326);
            this.m_splitPanel2.TabIndex = 9;
            // 
            // m_tvStrings
            // 
            this.m_tvStrings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_tvStrings.HideSelection = false;
            this.m_tvStrings.ImageIndex = 0;
            this.m_tvStrings.ImageList = this.m_imageList;
            this.m_tvStrings.Location = new System.Drawing.Point(0, 0);
            this.m_tvStrings.Name = "m_tvStrings";
            this.m_tvStrings.SelectedImageIndex = 0;
            this.m_tvStrings.ShowRootLines = false;
            this.m_tvStrings.Size = new System.Drawing.Size(195, 326);
            this.m_tvStrings.TabIndex = 6;
            this.m_tvStrings.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvStrings_AfterSelect);
            // 
            // m_imageList
            // 
            this.m_imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.m_imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.m_imageList.TransparentColor = System.Drawing.Color.Magenta;
            // 
            // m_dgvList
            // 
            this.m_dgvList.AllowUserToAddRows = false;
            this.m_dgvList.AllowUserToDeleteRows = false;
            this.m_dgvList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_dgvcName,
            this.m_dgvcDefaultValue,
            this.m_dgvcValue});
            this.m_dgvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_dgvList.Location = new System.Drawing.Point(0, 17);
            this.m_dgvList.Name = "m_dgvList";
            this.m_dgvList.RowHeadersVisible = false;
            this.m_dgvList.Size = new System.Drawing.Size(480, 195);
            this.m_dgvList.TabIndex = 1;
            this.m_dgvList.VirtualMode = true;
            this.m_dgvList.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_dgvList_RowEnter);
            this.m_dgvList.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.m_dgvList_CellValueNeeded);
            this.m_dgvList.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_dgvList_RowLeave);
            this.m_dgvList.CellValuePushed += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.m_dgvList_CellValuePushed);
            // 
            // m_dgvcName
            // 
            this.m_dgvcName.HeaderText = "Name";
            this.m_dgvcName.Name = "m_dgvcName";
            this.m_dgvcName.ReadOnly = true;
            this.m_dgvcName.Width = 150;
            // 
            // m_dgvcDefaultValue
            // 
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.m_dgvcDefaultValue.DefaultCellStyle = dataGridViewCellStyle1;
            this.m_dgvcDefaultValue.HeaderText = "Default value";
            this.m_dgvcDefaultValue.Name = "m_dgvcDefaultValue";
            this.m_dgvcDefaultValue.ReadOnly = true;
            this.m_dgvcDefaultValue.Width = 150;
            // 
            // m_dgvcValue
            // 
            this.m_dgvcValue.HeaderText = "Value";
            this.m_dgvcValue.Name = "m_dgvcValue";
            this.m_dgvcValue.Width = 150;
            // 
            // m_lblList
            // 
            this.m_lblList.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_lblList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_lblList.Location = new System.Drawing.Point(0, 2);
            this.m_lblList.Name = "m_lblList";
            this.m_lblList.Padding = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.m_lblList.Size = new System.Drawing.Size(51, 17);
            this.m_lblList.TabIndex = 3;
            this.m_lblList.Text = "m_lblList";
            this.m_lblList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_lblList.UseCompatibleTextRendering = true;
            // 
            // m_pnlBottom
            // 
            this.m_pnlBottom.Controls.Add(this.m_tbValue);
            this.m_pnlBottom.Controls.Add(this.m_lblStringName);
            this.m_pnlBottom.Controls.Add(this.m_lblDescription);
            this.m_pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_pnlBottom.Location = new System.Drawing.Point(0, 212);
            this.m_pnlBottom.Name = "m_pnlBottom";
            this.m_pnlBottom.Size = new System.Drawing.Size(480, 114);
            this.m_pnlBottom.TabIndex = 0;
            // 
            // m_lblStringName
            // 
            this.m_lblStringName.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_lblStringName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_lblStringName.Location = new System.Drawing.Point(3, 0);
            this.m_lblStringName.Name = "m_lblStringName";
            this.m_lblStringName.Padding = new System.Windows.Forms.Padding(0, 4, 0, 2);
            this.m_lblStringName.Size = new System.Drawing.Size(95, 17);
            this.m_lblStringName.TabIndex = 4;
            this.m_lblStringName.Text = "m_lblStringName";
            this.m_lblStringName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_lblStringName.UseCompatibleTextRendering = true;
            // 
            // m_lblDescription
            // 
            this.m_lblDescription.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_lblDescription.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_lblDescription.Location = new System.Drawing.Point(3, 74);
            this.m_lblDescription.Name = "m_lblDescription";
            this.m_lblDescription.Size = new System.Drawing.Size(474, 36);
            this.m_lblDescription.TabIndex = 3;
            this.m_lblDescription.UseCompatibleTextRendering = true;
            // 
            // m_tbValue
            // 
            this.m_tbValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_tbValue.Location = new System.Drawing.Point(3, 24);
            this.m_tbValue.Multiline = true;
            this.m_tbValue.Name = "m_tbValue";
            this.m_tbValue.Size = new System.Drawing.Size(474, 49);
            this.m_tbValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_tbValue.TabIndex = 2;
            this.m_tbValue.WordWrap = false;
            this.m_tbValue.TextChanged += new System.EventHandler(this.m_tbValue_TextChanged);
            this.m_tbValue.Leave += new System.EventHandler(this.m_tbValue_Leave);
            // 
            // StringsDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 403);
            this.Controls.Add(this.m_splitPanel2);
            this.Controls.Add(this.m_splitter);
            this.Controls.Add(this.m_splitPanel1);
            this.Controls.Add(this.m_statusStrip);
            this.Controls.Add(this.m_toolStrip);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "StringsDesigner";
            this.ShowInTaskbar = false;
            this.Text = "Localize";
            this.m_toolStrip.ResumeLayout(false);
            this.m_toolStrip.PerformLayout();
            this.m_statusStrip.ResumeLayout(false);
            this.m_statusStrip.PerformLayout();
            this.m_splitPanel1.ResumeLayout(false);
            this.m_splitPanel2.ResumeLayout(false);
            this.m_splitPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvList)).EndInit();
            this.m_pnlBottom.ResumeLayout(false);
            this.m_pnlBottom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.ToolStrip m_toolStrip;
        protected System.Windows.Forms.StatusStrip m_statusStrip;
        protected System.Windows.Forms.ToolStripStatusLabel m_tsslProject;
        protected System.Windows.Forms.ToolStripStatusLabel m_tsslCurrentProject;
        protected System.Windows.Forms.ToolStripStatusLabel m_tsslProduct;
        protected System.Windows.Forms.ToolStripStatusLabel m_tsslCurrentProduct;
        protected System.Windows.Forms.Panel m_splitPanel1;
        protected System.Windows.Forms.Panel m_splitPanel2;
        protected System.Windows.Forms.Splitter m_splitter;
        protected System.Windows.Forms.TreeView m_tvStrings;
        protected System.Windows.Forms.ToolStripSplitButton m_tssbOpen;
        protected System.Windows.Forms.ToolStripButton m_tsbNew;
        protected System.Windows.Forms.ToolStripButton m_tsbSave;
        protected System.Windows.Forms.ToolStripButton m_tsbSaveAs;
        protected System.Windows.Forms.ToolStripSeparator m_tss1;
        protected System.Windows.Forms.ToolStripButton m_tsbAddCulture;
        protected System.Windows.Forms.ToolStripButton m_tsbDeleteCulture;
        protected System.Windows.Forms.ImageList m_imageList;
        protected System.Windows.Forms.DataGridView m_dgvList;
        private System.Windows.Forms.Panel m_pnlBottom;
        protected System.Windows.Forms.Label m_lblStringName;
        protected System.Windows.Forms.Label m_lblList;
        protected System.Windows.Forms.TextBox m_tbValue;
        protected System.Windows.Forms.Label m_lblDescription;
        protected System.Windows.Forms.ToolStripLabel m_tslCulture;
        private System.Windows.Forms.ToolStripComboBox m_tscbCurrentCulture;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_dgvcName;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_dgvcDefaultValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_dgvcValue;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        protected System.Windows.Forms.ToolStripDropDownButton m_tsddbOptions;
        protected System.Windows.Forms.ToolStripMenuItem m_tsmiSyncTreeWithGrid;
        protected System.Windows.Forms.ToolStripMenuItem m_tsmiShowNamesInGrid;
        protected System.Windows.Forms.ToolStripMenuItem m_tsmiShowSelectedGroupOnly;
    }
}