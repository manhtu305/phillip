using System;
using System.Resources;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace C1.Win.Localization
{
    /// <summary>
    /// Allows to redefine default enduser localize options for
    /// property, field or class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Class | AttributeTargets.Property)]
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class EndUserLocalizeOptionsAttribute : Attribute
    {
        private string[] _properties;
        private bool _exclude;
        private string _description;
        private Type _stringsType;
        private string _key;

        #region Constructors
        public EndUserLocalizeOptionsAttribute()
        {
        }

        public EndUserLocalizeOptionsAttribute(
            params string[] properties)
        {
            if (properties == null || properties.Length == 0)
                throw new ArgumentException();
            _properties = properties;
        }

        public EndUserLocalizeOptionsAttribute(
            bool exclude)
        {
            _exclude = exclude;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Defines the list of properties that should be localized.
        /// </summary>
        public string[] Properties
        {
            get { return _properties; }
        }

        /// <summary>
        /// If true then class, property or field will be not displayed for 
        /// enduser localization.
        /// </summary>
        public bool Exclude
        {
            get { return _exclude; }
        }

        /// <summary>
        /// Gets the description of string that will be displayed in the
        /// end user localizer in design-time.
        /// </summary>
        public string Description
        {
            get { return _description == null ? string.Empty : _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Gets or sets type derived from Strings
        /// providing string used to localize <see cref="Description"/>.
        /// </summary>
        public Type StringsType
        {
            get { return _stringsType; }
            set { _stringsType = value; }
        }

        /// <summary>
        /// Gets or sets resource key containing localized string for <see cref="Description"/>.
        /// </summary>
        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
        #endregion
    }

    /// <summary>
    /// C1DescriptionAttribute replaces the DescriptionAttribute
    /// and uses the StringsManager and Strings classes to
    /// return the localized Attribute string.
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class C1DescriptionAttribute : DescriptionAttribute
    {
        private Type _stringsType;
        private string _key;

        #region Constructors
        public C1DescriptionAttribute(
            Type stringsType,
            string key,
            string description)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("key");
            if (stringsType == null)
                throw new ArgumentException("stringsType");
            _stringsType = stringsType;
            _key = key;
            base.DescriptionValue = description;
        }
        #endregion

        #region Public properties
        public override string Description
        {
            get
            {
                string result = StringsManager.GetCustomString(_stringsType, "Attributes.Description." + _key, base.DescriptionValue);
                return string.IsNullOrEmpty(result) ? _key : result;
            }
        }
        #endregion
    }

    /// <summary>
    /// C1CategoryAttribute replaces the CategoryAttribute
    /// and uses the StringsManager and Strings classes to
    /// return the localized Attribute string.
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class C1CategoryAttribute : CategoryAttribute
    {
        private Type _stringsType;
        private string _value;

        #region Constructors
        public C1CategoryAttribute(
            Type stringsType,
            string categoryName)
            : base(categoryName)
        {
            if (stringsType == null)
                throw new ArgumentException("stringsType");
            _stringsType = stringsType;
        }
        #endregion

        #region Protected
        protected override string GetLocalizedString(
            string value)
        {
            // if we have the value, return it
            if (_value != null)
                return _value;

            // look it up once
            _value = StringsManager.GetCustomString(_stringsType, "Attributes.Category." + value, null);
            if (_value == null)
                _value = value;

            // return what we got
            return _value;
        }
        #endregion
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class StringsManager
    {
        internal const string c_ResourceManagerPropertyName = "ResourceManager";
        internal const string c_UICulturePropertyName = "UICulture";
        internal const string c_LocalizeFolderName = "C1LocalizedResources";
        internal const string c_StringsClassName = "Strings";

        private static bool s_UseOldInternalGetSatelliteAssembly;
        private static MethodInfo s_InternalGetSatelliteAssemblyMI;
        private static Type s_StackCrawlMarkType;
        private static Type s_StackCrawlMarkTypeRef;
        private static MethodInfo s_GetSimpleNameMI;
        private static Dictionary<Type, Dictionary<string, ResourceSet>> s_ResourceSets;
        // contains ResourceSet for resources that can be in C1 satellite assemblies like:
        // C1.C1Preview.2.ja.resources.dll
        private static Dictionary<Type, Dictionary<string, ResourceSet>> s_C1LocalizableResourceSets;
        // contains ResourceSet for resources that provided ONLY by C1 for different
        // official versions like Japan, Chinese etc
        private static Dictionary<Type, Dictionary<string, ResourceSet>> s_C1BuiltinResourceSets;
        private static bool s_SkipUserResources;
        private static Type s_StringsType;
        private static PropertyInfo s_CultureInfoPI;
        private static PropertyInfo s_ResourceManagerPI;

        #region Constructors
        static StringsManager()
        {
#if (CLR40)
            BuildInternalSatelliteAssemblyMethodInfoExt();
            s_UseOldInternalGetSatelliteAssembly = false;
#else
            s_InternalGetSatelliteAssemblyMI = typeof(Assembly).GetMethod("InternalGetSatelliteAssembly", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(CultureInfo), typeof(Version), typeof(bool) }, null);
            if (s_InternalGetSatelliteAssemblyMI == null)
            {
                BuildInternalSatelliteAssemblyMethodInfoExt();
                s_UseOldInternalGetSatelliteAssembly = false;
            }
            else
                s_UseOldInternalGetSatelliteAssembly = true;
#endif

            // initialize cache of resource sets
            s_ResourceSets = new Dictionary<Type, Dictionary<string, ResourceSet>>();
            s_C1LocalizableResourceSets = new Dictionary<Type, Dictionary<string, ResourceSet>>();
            s_C1BuiltinResourceSets = new Dictionary<Type, Dictionary<string, ResourceSet>>();

            // we search for Strings type in assembly this type
            // will provide the default UICulture and ResourceManager properties
            // it they are not specified in other "strings classes".
            Assembly assembly = Assembly.GetExecutingAssembly();

            // Search for Strings type
            s_StringsType = null;
            Type[] types = null;
            try
            {
                types = assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                // Note: to get listed by GetTypes, some types require full trust.
                // E.g. this applies to our types derived from UITypeEditor. So,
                // in partial trust scenarios, GetTypes fails but for all practical
                // purposes getting the loaded types from the exception works as well.
                // --dima.
                types = ex.Types;
            }
            catch (Exception)
            {
                throw;
            }

            foreach (Type type in types)
                if (type.IsClass && type.Name == c_StringsClassName)
                {
                    // type should contais two properties:
                    // ResourceManager and UICulture
                    s_ResourceManagerPI = type.GetProperty(c_ResourceManagerPropertyName, BindingFlags.Public | BindingFlags.Static);
                    if (s_ResourceManagerPI == null || s_ResourceManagerPI.PropertyType != typeof(ResourceManager) || !s_ResourceManagerPI.CanRead)
                        continue;
                    s_CultureInfoPI = type.GetProperty(c_UICulturePropertyName, BindingFlags.Public | BindingFlags.Static);
                    if (s_CultureInfoPI == null || s_CultureInfoPI.PropertyType != typeof(CultureInfo) || !s_CultureInfoPI.CanRead)
                        continue;

                    s_StringsType = type;
                    break;
                }
        }
        #endregion

        #region Private static
        private static void BuildInternalSatelliteAssemblyMethodInfoExt()
        {
            s_StackCrawlMarkType = typeof(Assembly).Assembly.GetType("System.Threading.StackCrawlMark", false, true);
            if (s_StackCrawlMarkType != null)
                s_StackCrawlMarkTypeRef = s_StackCrawlMarkType.MakeByRefType();
            else
                s_StackCrawlMarkTypeRef = null;
            Type runtimeAssemblyType = typeof(Assembly).Assembly.GetType("System.Reflection.RuntimeAssembly", false);
            if (runtimeAssemblyType != null && s_StackCrawlMarkType != null && s_StackCrawlMarkTypeRef != null)
            {
                s_GetSimpleNameMI = runtimeAssemblyType.GetMethod(
                    "GetSimpleName",
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new Type[0],
                    null);
                ParameterModifier mods = new ParameterModifier(5);
                mods[4] = true;
                s_InternalGetSatelliteAssemblyMI = runtimeAssemblyType.GetMethod(
                    "InternalGetSatelliteAssembly",
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new Type[] { typeof(string), typeof(CultureInfo), typeof(Version), typeof(bool), s_StackCrawlMarkTypeRef },
                    new ParameterModifier[] { mods });
            }
            else
                s_InternalGetSatelliteAssemblyMI = null;
        }

        private static CultureInfo GetCulture(
            string cultureName)
        {
            try
            {
                return new CultureInfo(cultureName);
            }
            catch
            {
                return null;
            }
        }

        private static CultureInfo GetCurrentCulture(
            Type stringsType)
        {
            PropertyInfo pi = stringsType.GetProperty(c_UICulturePropertyName, BindingFlags.Public | BindingFlags.Static);
            if (pi != null && pi.PropertyType == typeof(CultureInfo) && pi.CanRead)
                return (CultureInfo)pi.GetValue(null, null);
            else if (s_CultureInfoPI != null)
                return (CultureInfo)s_CultureInfoPI.GetValue(null, null);
            else
                return null;
        }

        private static string FindResourceName(
            Assembly assembly,
            string resName)
        {
            string[] resources = assembly.GetManifestResourceNames();
            resName = resName + ".resources";
            foreach (string res in resources)
                if (res.EndsWith(resName))
                    return res.Substring(0, res.Length - 10);
            return null;
        }

        private static void GetInfo(
            Type stringsType,
            out ResourceManager userResourceManager/*,
            out ResourceManager internalResourceManager*/)
        {
            //
            PropertyInfo pi = stringsType.GetProperty(c_ResourceManagerPropertyName, BindingFlags.Public | BindingFlags.Static);
            if (pi != null && pi.PropertyType == typeof(ResourceManager) && pi.CanRead)
                userResourceManager = (ResourceManager)pi.GetValue(null, null);
            else if (s_ResourceManagerPI != null)
                userResourceManager = (ResourceManager)s_ResourceManagerPI.GetValue(null, null);
            else
                userResourceManager = null;

            /*// determine the default resource name on the base of name of stringsType.
            // The resource's name should be ended with name of strings class, for example:
            // type: C1.C1Preview.Strings
            // resources: AAA.ZZZ.C1.C1Preview.Strings.resources
            string resourceName = FindResourceName(stringsType.Assembly, stringsType.FullName);
            if (resourceName != null)
                internalResourceManager = new ResourceManager(resourceName, stringsType.Assembly);
            else
                internalResourceManager = null;*/
        }

        private static Assembly GetSatelliteAssembly(
            Assembly assembly,
            string cultureName)
        {
            CultureInfo culture = GetCulture(cultureName);
            if (culture == null)
                return null;

            if (s_InternalGetSatelliteAssemblyMI == null)
            {
                try
                {
                    return assembly.GetSatelliteAssembly(culture);
                }
                catch
                {
                    return null;
                }
            }
            if (s_UseOldInternalGetSatelliteAssembly)
            {
                try
                {
                    return (Assembly)s_InternalGetSatelliteAssemblyMI.Invoke(assembly, new object[] { culture, null, false });
                }
                catch
                {
                    return null; // TFS~~28141
                }
            }
            else
            {
            string simpleName = s_GetSimpleNameMI.Invoke(assembly, null) as string;
            if (string.IsNullOrEmpty(simpleName))
                return null;

            try
            {
                object o = Enum.GetValues(s_StackCrawlMarkType).GetValue(0);
                object[] args = new object[] { simpleName + ".resources", culture, null, false, o };
                return (Assembly)s_InternalGetSatelliteAssemblyMI.Invoke(assembly, args);
            }
            catch
            {
                return null; // TFS~~28141
            }
            }
        }

        /// <summary>
        /// Returns ResourceStream for "official" resources
        /// that provided only by C1, typicallly those resources contains
        /// design-time strings.
        /// Those resources can be only in main control assembly like C1.C1Preview.2.dll 
        /// and name of resource should have following format:
        /// CulturePrefix.StringsTypeName
        /// for example:
        /// ja.C1.C1Preview.Design.DesignStrings.resx
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="assembly"></param>
        /// <param name="cultureName"></param>
        /// <returns></returns>
        private static Stream GetC1BuiltinResourceStream(
            string resourceName,
            Assembly assembly,
            string cultureName)
        {
            string s = resourceName;
            if (!string.IsNullOrEmpty(cultureName))
                s = cultureName + "." + s;

            string resName = FindResourceName(assembly, s);
            if (resName == null)
                return null;

            return assembly.GetManifestResourceStream(resName + ".resources");
        }

        private static Stream GetResourceStream(
            string resourceName,
            Assembly assembly,
            string cultureName,
            bool searchInSatelliteOnly)
        {
            string s = resourceName;
            if (!string.IsNullOrEmpty(cultureName))
                s += "." + cultureName;
            string resName = null;
            if (!searchInSatelliteOnly)
                resName = FindResourceName(assembly, s);
            Stream result = null;
            if (resName != null)
                result = assembly.GetManifestResourceStream(resName + ".resources");
            if (result == null)
            {
                Assembly satelliteAssembly = GetSatelliteAssembly(assembly, cultureName);
                if (satelliteAssembly != null)
                {
                    resName = FindResourceName(satelliteAssembly, s);
                    if (resName != null)
                        result = satelliteAssembly.GetManifestResourceStream(resName + ".resources");
                }
            }
            return result;
        }

        private static ResourceSet GetC1BuiltinResourceSet(
            Type stringsType,
            string cultureName)
        {
            ResourceSet result;
            Dictionary<string, ResourceSet> cultureSets;
            lock (s_C1BuiltinResourceSets)
            {
                if (s_C1BuiltinResourceSets.TryGetValue(stringsType, out cultureSets))
                {
                    if (cultureSets.TryGetValue(cultureName, out result))
                        return result;
                }
                else
                {
                    cultureSets = new Dictionary<string, ResourceSet>();
                    s_C1BuiltinResourceSets.Add(stringsType, cultureSets);
                }

                Stream stream = GetC1BuiltinResourceStream(stringsType.FullName, stringsType.Assembly, cultureName);

                if (stream == null)
                {
                    cultureSets.Add(cultureName, null);
                    return null;
                }

                result = new ResourceSet(stream);
                cultureSets.Add(cultureName, result);
                return result;
            }
        }

        private static ResourceSet GetC1LocalizableResourceSet(
            Type stringsType,
            string cultureName)
        {
            ResourceSet result;
            Dictionary<string, ResourceSet> cultureSets;
            lock (s_C1LocalizableResourceSets)
            {
                if (s_C1LocalizableResourceSets.TryGetValue(stringsType, out cultureSets))
                {
                    if (cultureSets.TryGetValue(cultureName, out result))
                        return result;
                }
                else
                {
                    cultureSets = new Dictionary<string, ResourceSet>();
                    s_C1LocalizableResourceSets.Add(stringsType, cultureSets);
                }

                Stream stream = GetResourceStream(stringsType.FullName, stringsType.Assembly, cultureName, true);

                if (stream == null)
                {
                    cultureSets.Add(cultureName, null);
                    return null;
                }

                result = new ResourceSet(stream);
                cultureSets.Add(cultureName, result);
                return result;
            }
        }

        private static ResourceSet GetDefaultResourceSet(
            Type stringsType,
            string cultureName)
        {
            ResourceSet result;
            Dictionary<string, ResourceSet> cultureSets;
            lock (s_ResourceSets)
            {
                if (s_ResourceSets.TryGetValue(stringsType, out cultureSets))
                {
                    if (cultureSets.TryGetValue(cultureName, out result))
                        return result;
                }
                else
                {
                    cultureSets = new Dictionary<string, ResourceSet>();
                    s_ResourceSets.Add(stringsType, cultureSets);
                }

                Assembly entryAssembly = Assembly.GetEntryAssembly();
                if (entryAssembly == null)
                    return null;

                Stream stream = GetResourceStream(Assembly.GetExecutingAssembly().GetName().Name, entryAssembly, cultureName, false);
                if (stream == null)
                {
                    cultureSets.Add(cultureName, null);
                    return null;
                }

                result = new ResourceSet(stream);
                cultureSets.Add(cultureName, result);
                return result;
            }
        }

        /*/// <summary>
        /// Returns the parent of the CultureInfo passed in. Differs from ci.Parent
        /// in that while ci can be equal to ci.Parent, this method it never returns
        /// the passed ci itself, rather it would return null.
        /// </summary>
        /// <param name="ci">The culture info to get parent for.</param>
        /// <returns>The parent of the culture info, or null.</returns>
        private static CultureInfo GetParent(
            CultureInfo ci)
        {
            if (ci == null)
                return null;
            CultureInfo parent = ci.Parent;
            if (ci.Equals(parent))
                return null;
            return parent;
        }*/

        /// <summary>
        /// Returns the parent of the CultureInfo passed in. Differs from ci.Parent
        /// in that while ci can be equal to ci.Parent, this method it never returns
        /// the passed ci itself, rather it would return null.
        /// Also this function has special handling for chinese culture.
        /// Under NET2 it returns "zh" culture name for
        /// neutral "zh-Hans" and "zh-Hant" chinese cultures, under
        /// NET2 "zh" is not exists.
        /// </summary>
        /// <param name="cultureName">The culture name to get parent for.</param>
        /// <returns>The parent of the culture info, or null.</returns>
        private static string GetParentCulture(
            string cultureName)
        {
            if (cultureName == "zh")
                // special handling for "zh"
                return CultureInfo.InvariantCulture.Name;

            //
            CultureInfo ci = GetCulture(cultureName);
            if (ci == null)
                return null;

            CultureInfo cip = ci.Parent;
            if (ci.Equals(cip))
                return null;

            if (cip == CultureInfo.InvariantCulture && ci.Name.StartsWith("zh-"))
                return "zh";

            return cip.Name;
        }

        private static object GetNestedObject(
            object c,
            string memberName)
        {
            Type t = c.GetType();
            FieldInfo[] fields = t.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach (FieldInfo fi in fields)
            {
                string s = fi.Name;
                if (s.StartsWith("m_"))
                    s = s.Substring(2);
                if (s == memberName)
                    return fi.GetValue(c);
            }

            PropertyInfo[] properties = t.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                string s = pi.Name;
                if (s.StartsWith("m_"))
                    s = s.Substring(2);
                if (s == memberName)
                    return pi.GetValue(c, null);
            }

            return null;
        }

        private static void SetPropertyValue(
            object c,
            string memberName,
            object value)
        {
            PropertyInfo[] properties = c.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo pi in properties)
            {
                if (!pi.CanWrite || pi.Name != memberName)
                    continue;
                pi.SetValue(c, value, null);
                break;
            }
        }
        #endregion

        #region Public static properties
        public static bool SkipUserResources
        {
            get { return s_SkipUserResources; }
            set { s_SkipUserResources = value; }
        }
        #endregion

        #region Public static
        public static string GetCustomString(
            Type stringsType,
            string resourceName,
            string defaultValue)
        {
            return GetCustomString(stringsType, resourceName, defaultValue, GetCurrentCulture(stringsType));
        }

        public static string GetCustomString(
            Type stringsType,
            string resourceName,
            string defaultValue,
            CultureInfo culture)
        {
            string result = null;

            string cultureName;
            string currentCulture = culture == null ? null : culture.Name;
            //
            ResourceManager userResourceManager/*, internalResourceManager*/;
            GetInfo(stringsType, out userResourceManager/*, out internalResourceManager*/);

            if (!SkipUserResources)
            {
                // search string in the user-defined manager (Strings.ResourceManager property)
                if (userResourceManager != null)
                {
                    try
                    {
                        result = userResourceManager.GetString(resourceName, culture);
                    }
                    catch
                    {
                        result = null;
                    }
                    if (result != null)
                        return result;
                }

                cultureName = currentCulture;
                if (cultureName != null)
                {
                    // search string in the "default" resources created in the design-mode with localizer
                    while (cultureName != null)
                    {
                        ResourceSet rs = GetDefaultResourceSet(stringsType, cultureName);
                        if (rs != null)
                        {
                            result = rs.GetString(resourceName);
                            if (result != null)
                                return result;
                        }
                        cultureName = GetParentCulture(cultureName);
                    }
                }
            }

            // search string in C1 internal resources
            cultureName = currentCulture;
            while (result == null && cultureName != null && cultureName != string.Empty)
            {
                ResourceSet rs = GetC1LocalizableResourceSet(stringsType, cultureName);
                if (rs == null)
                    rs = GetC1BuiltinResourceSet(stringsType, cultureName);
                if (rs != null)
                    result = rs.GetString(resourceName);
                cultureName = GetParentCulture(cultureName);
            }
            if (result != null)
                return result;

            return defaultValue;
        }

        public static string GetString(
            Type type,
            string resourceName,
            string defaultValue,
            CultureInfo culture)
        {
            if (culture == null)
            {
                return GetCustomString(type, resourceName, defaultValue);
            }
            return GetCustomString(type, resourceName, defaultValue, culture);
        }

        public static string GetString(
            MethodBase mi,
            string defaultValue)
        {
            StringBuilder sb = new StringBuilder(mi.Name.Substring(4));
            Type type;
            for (type = mi.DeclaringType; type.DeclaringType != null; type = type.DeclaringType)
            {
                sb.Insert(0, ".");
                sb.Insert(0, type.Name);
            }
            return GetCustomString(type, sb.ToString(), defaultValue);
        }

        public static void LocalizeControl(
            Type stringsType,
            Control control,
            Type type,
            CultureInfo culture)
        {
            LocalizeControl(stringsType, control, "Forms." + type.Name, culture);
        }

        public static void LocalizeControl(
            Type stringsType,
            Control control,
            Type type)
        {
            LocalizeControl(stringsType, control, "Forms." + type.Name);
        }

        public static void LocalizeControl(
            Type stringsType,
            Control control,
            string resourceStringsPrefix,
            CultureInfo culture)
        {
            if (culture == null)
                return;

            ResourceManager userResourceManager/*, internalResourceManager*/;
            GetInfo(stringsType, out userResourceManager/*, out internalResourceManager*/);

            Dictionary<string, string> strings = new Dictionary<string, string>();
            string cultureName;
            resourceStringsPrefix += ".";

            if (!SkipUserResources)
            {
                // search all strings in the user-defined resourcemanager (Strings.ResourceManager property)
                ResourceManager resourceManager = userResourceManager;
                if (resourceManager != null)
                {
                    cultureName = culture.Name;
                    do
                    {
                        CultureInfo ci = GetCulture(cultureName);
                        if (ci != null)
                        {
                            ResourceSet rs = resourceManager.GetResourceSet(ci, true, false);
                            if (rs != null)
                            {
                                foreach (DictionaryEntry de in rs)
                                {
                                    string key = de.Key as string;
                                    if (key == null)
                                        continue;
                                    string value = de.Value as string;
                                    if (value == null)
                                        continue;
                                    if (key.StartsWith(resourceStringsPrefix) && !strings.ContainsKey(key))
                                        strings.Add(key, value);
                                }
                            }
                        }
                        cultureName = GetParentCulture(cultureName);
                    }
                    while (!string.IsNullOrEmpty(cultureName));
                }

                // select all strings in the "default" resources created in the design-mode with localizer
                cultureName = culture.Name;
                while (cultureName != null)
                {
                    ResourceSet rs = GetDefaultResourceSet(stringsType, cultureName);
                    if (rs != null)
                    {
                        foreach (DictionaryEntry de in rs)
                        {
                            string key = de.Key as string;
                            if (key == null)
                                continue;
                            string value = de.Value as string;
                            if (value == null)
                                continue;
                            if (key.StartsWith(resourceStringsPrefix) && !strings.ContainsKey(key))
                                strings.Add(key, value);
                        }
                    }
                    cultureName = GetParentCulture(cultureName);
                }
            }

            // select strings from internal C1 resources
            cultureName = culture.Name;
            while (!string.IsNullOrEmpty(cultureName))
            {
                ResourceSet rs = GetC1LocalizableResourceSet(stringsType, cultureName);
                if (rs == null)
                    rs = GetC1BuiltinResourceSet(stringsType, cultureName);
                if (rs != null)
                {
                    foreach (DictionaryEntry de in rs)
                    {
                        string key = de.Key as string;
                        if (key == null)
                            continue;
                        string value = de.Value as string;
                        if (value == null)
                            continue;
                        if (key.StartsWith(resourceStringsPrefix) && !strings.ContainsKey(key))
                            strings.Add(key, value);
                    }
                }
                cultureName = GetParentCulture(cultureName);
            }

            // strings contains all string used to localize this form
            int prefixLength = resourceStringsPrefix.Length;
            foreach (KeyValuePair<string, string> kvp in strings)
            {
                string key = kvp.Key.Substring(prefixLength);
                string value = kvp.Value;

                string[] parts = key.Split('.');
                if (parts.Length > 0)
                {
                    object c = control;
                    for (int i = 0; i < parts.Length - 1 && c != null; i++)
                        c = GetNestedObject(c, parts[i]);

                    if (c == null)
                        continue;

                    SetPropertyValue(c, parts[parts.Length - 1], value);
                }
            }
        }

        public static void LocalizeControl(
            Type stringsType,
            Control control,
            string resourceStringsPrefix)
        {
            LocalizeControl(stringsType, control, resourceStringsPrefix, GetCurrentCulture(stringsType));
        }
        #endregion
    }
}
