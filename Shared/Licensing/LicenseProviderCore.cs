#define USE_REFLECTION
using C1Licensing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using LicenseStatus = C1Licensing.LicenseHandler.LicenseStatus;
using System.IO;
using System.Xml.Linq;

namespace C1.Util.Licensing
{
    /// <summary>
    /// Attribute used to attach licensing/product information to assemblies.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class,
        Inherited = false, AllowMultiple = true)]
    internal class C1ProductInfoAttribute : Attribute
    {
        public C1ProductInfoAttribute(string productCode, string productGUID)
        {
            Debug.Assert(productCode.Length == 2 || productCode == null, "Invalid productCode");
            ProductCode = productCode;
            ProductGUID = productGUID;
        }
        public string ProductCode
        {
            get;
        }
        public string ProductGUID
        {
            get;
        }

        public static C1ProductInfoAttribute[] GetProductInfos(Assembly asm)
        {
            return asm.GetCustomAttributes(typeof(C1ProductInfoAttribute)) as C1ProductInfoAttribute[];
        }
    }

    internal class LicenseInfo
    {
        private readonly string _message;
        private readonly bool _needRenderEvalInfo;
        private readonly bool _availableLicense;

        public LicenseInfo(LicenseStatus status, int daysRemaining)
        {
            Status = status;
            switch (status)
            {
                case LicenseStatus.EvalValid:
                    _message = string.Format(LicenseResources.AppLicenseEvalValid, daysRemaining);
                    _needRenderEvalInfo = true;
                    _availableLicense = true;
                    break;
                case LicenseStatus.ExpiredEval:
                    _message = LicenseResources.AppLicenseExpiredEval;
                    break;
                case LicenseStatus.ExpiredFull:
                    _message = LicenseResources.AppLicenseExpiredFull;
                    break;
                case LicenseStatus.FullValid:
                    _message = string.Format(LicenseResources.AppLicenseFullValid, daysRemaining);
                    _availableLicense = true;
                    break;
                case LicenseStatus.InvalidCertificate:
                    _message = LicenseResources.AppLicenseInvalidCert;
                    break;
                case LicenseStatus.Unknown:
                default:
                    _message = LicenseResources.AppLicenseUnknown;
                    break;
            }
        }

        internal bool IsValid
        {
            get
            {
                if (!_availableLicense)
                {
                    throw new LicenseException(_message);
                }

                Debug.WriteLine(_message);
                return !_needRenderEvalInfo;
            }
        }

        public LicenseStatus Status
        {
            get;
            private set;
        }

        public int DaysRemaining
        {
            get;
            private set;
        }

        public bool NeedRenderEvalInfo()
        {
            if (!_availableLicense)
            {
                throw new LicenseException(_message);
            }

            Debug.WriteLine(_message);
            return _needRenderEvalInfo;
        }
    }

    internal class ProviderInfo
    {
        internal static LicenseInfo Validate(Type type)
        {
#if NETCORE
            var asm = type.Assembly();
#else
            var asm = type.Assembly;
#endif
#if !NOGCLICENSING && !GRAPECITY
            //GCLICENSING:
            //var gcLicProvider = new GrapeCity.Common.C1GCWebLicenseProvider();
            //var gclic = gcLicProvider.GetLicense(System.ComponentModel.LicenseManager.CurrentContext, type, null, false);
            //if (gclic != null && gclic.LicenseKey == "Product License, Activated")
            //{
            //    return new LicenseInfo( LicenseStatus.FullValid, 0);
            //}

            // core licensing installed for vs2019 uses buildTransitive\*.targets file.  These
            // target files create and add a resource (.gclicx) to the application.  For this
            // different logic must be used.
            string validProductCodes = "";
            {
                var productInfos = C1ProductInfoAttribute.GetProductInfos(asm);
                foreach(C1ProductInfoAttribute pia in productInfos)
                {
                    if (validProductCodes.Length != 0) validProductCodes += ",";
                    validProductCodes += pia.ProductCode;
                }
            }
            GrapeCity.Common.C1GCLicenseManager.ValidProductCodes = validProductCodes;
            var gclic = GrapeCity.Common.C1GCLicenseManager.GetRuntimeLicense();
            if (gclic != null)
            {
                if (gclic.LicenseText == "Product License, Activated")
                    return new LicenseInfo(LicenseStatus.FullValid, 365);//keep old behavior
                if (gclic.LicenseText.StartsWith("Trial License, Activated"))
                    return new LicenseInfo(LicenseStatus.FullValid, gclic.GetLeftDays());
            }

#endif
            //invalid, check old license:

            var licenseProvider = type.GetTypeInfo().GetCustomAttribute(typeof(BaseLicenseProviderAttribute), false) as BaseLicenseProviderAttribute;
            if (licenseProvider == null)
            {
                throw new LicenseException(LicenseResources.AppLicenseUnknown);
            }

            var keys = licenseProvider.RunTimeKeys.Where(k => !string.IsNullOrEmpty(k));
            if (!keys.Any())
            {
                throw new LicenseException(LicenseResources.AppLicenseNotSet);
            }

            Exception firstException = null;
            LicenseInfo firstLic = null;
            foreach (var key in keys)
            {
                try
                {
                    var lic = ValidateKey(asm, key);
                    if (lic.Status == LicenseStatus.FullValid)
                    {
                        return lic;
                    }

                    firstLic = firstLic ?? lic;
                }
                catch (Exception ex)
                {
                    firstException = firstException ?? ex;
                }
            }

            if (firstException != null)
            {
                throw firstException;
            }

            if (firstLic == null)
            {
                throw new LicenseException(LicenseResources.AppLicenseUnknown);
            }

            return firstLic;
        }

        private static LicenseInfo ValidateKey(Assembly productAssembly, string key)
        {
            var name = Assembly.GetEntryAssembly().GetName().Name;
            int daysRemaining;
            var status = LicenseStatus.Unknown;
            var keyParts = key.Split(',');

            // try to validate legacy standard license which is generated by
            // AspNet5LicenseGenerator(version <= 1.0.20153.3)
            var useLegacyStandard = keyParts.Length == 3;

            if (useLegacyStandard)
            {
                status = (LicenseStatus)LicenseHandler.VerifyLegacyStandardLicense(name, productAssembly, keyParts, out daysRemaining);
            }
            else
            {
                const string standLicPrefix = "c1v01";
                var useStandard = key.StartsWith(standLicPrefix);
                if (useStandard)
                {
                    // try to validate standard license which is generated by
                    // AspNet5LicenseGenerator(version > 1.0.20153.3)
                    status = (LicenseStatus)LicenseHandler.VerifyStandardLicense(name, productAssembly, key, out daysRemaining);
                }
                else
                {
                    // try to validate certificate license.
                    // The data length of certificate license is more than 1000.
                    var productInfos = C1ProductInfoAttribute.GetProductInfos(productAssembly).Select(p => p.ProductCode).ToArray();
                    status = (LicenseStatus)LicenseHandler.VerifyCertLicense(name, key, string.Join(",", productInfos), out daysRemaining);
                }
            }

            return new LicenseInfo(status, daysRemaining);
        }
    }

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    internal abstract class BaseLicenseProviderAttribute : Attribute
    {
        private readonly Lazy<IEnumerable<string>> _runTimeKeys;
        private static readonly Lazy<IEnumerable<string>> _embeddedKeys;

        public BaseLicenseProviderAttribute()
        {
            _runTimeKeys = new Lazy<IEnumerable<string>>(() =>
            {
                var runTimeKeys = new List<string>();
                if (!string.IsNullOrEmpty(RunTimeKey))
                {
                    runTimeKeys.Add(RunTimeKey);
                }

                if (EmbeddedKeys != null)
                {
                    runTimeKeys.AddRange(EmbeddedKeys);
                }

                return runTimeKeys;
            });
        }

        static BaseLicenseProviderAttribute()
        {
            _embeddedKeys = new Lazy<IEnumerable<string>>(()=>
            {
                var embeddedKeys = new List<string>();
                const string embeddedLicFile = "GCDTLicenses.xml";
                var app = Assembly.GetEntryAssembly();
                var name = app.GetManifestResourceNames().FirstOrDefault(n => n.EndsWith(embeddedLicFile, StringComparison.OrdinalIgnoreCase));
                if (!string.IsNullOrEmpty(name))
                {
                    embeddedKeys.AddRange(ReadEmbeddedKeys(app.GetManifestResourceStream(name)));
                }

                return embeddedKeys;
            });
        }

        /// <summary>
        /// Gets the run-time key which is set by user.
        /// </summary>
        public abstract string RunTimeKey { get; }

        /// <summary>
        /// Gets the run-time keys that includes the key which is set by user
        /// and the keys which are embedded in the application assembly.
        /// </summary>
        public IEnumerable<string> RunTimeKeys
        {
            get
            {
                return _runTimeKeys.Value;
            }
        }

        private static IEnumerable<string> EmbeddedKeys
        {
            get
            {
                return _embeddedKeys.Value;
            }
        }

        private static IEnumerable<string> ReadEmbeddedKeys(Stream stream)
        {
            try
            {
                var doc = XDocument.Load(stream);
                return doc.Elements().First().Elements().Select(x => x.Value);
            }
            catch
            {
                return new string[] { };
            }
        }
    }
}