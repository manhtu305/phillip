﻿using System;
using System.Text;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Linq;
using C1.Util.Licensing;
using System.IO;
using System.Diagnostics;
#if NETCORE
using System.Runtime.InteropServices;
#endif

#if !ASPNETCORE && !NETCORE
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(C1Licensing.C1LicenseChecker))]
#else
using X509Certificate = System.Security.Cryptography.X509Certificates.X509Certificate2;
#endif
namespace C1Licensing
{
#if !ASPNETCORE  && !NETCORE
    internal class C1LicenseChecker : IC1Licensing
    {
        static int daysRemaining = int.MinValue;
        static int licenseStatus = int.MinValue;
        static bool previousCheck = false;

        public int VerifyLicense(string licenseString)
        {
            if (licenseStatus == int.MinValue)
            {
				string appInfo = Application.Current.ToString();
				string validProductCodes = "XU,SU";  // build this from attributes if you can.
                licenseStatus = LicenseHandler.VerifyRuntimeLicense(appInfo, licenseString, validProductCodes, out daysRemaining);
            }
            else
            {
                previousCheck = true;
            }
            return licenseStatus;
        }
        public bool PreviousLicenseCheck()
        {
            return previousCheck;
        }
        public int GetDaysRemaining()
        {
            return daysRemaining;
        }

        public string GetStatusMessage()
        {
            string[] msgs =
            {
#if GRAPECITY
                "Xuni ライセンスキーが無効か見つかりません。ライセンスの詳細については http://c1.grapecity.com でご確認ください。",
                "Xuni ライセンスキーの有効期間が終了しました新しいキーでアプリケーションをリビルドしてください。",
                "Xuni のトライアル期間が終了しました。",
                "Xuni ライセンスは有効です。",
                "このアプリケーションは Xuni のトライアル版を使用してビルドされています。トライアル期間はあと {0} 日です。",
#else
                "The Xuni license key is invalid or missing. See http://www.componentone.com/xuni-license for more information.",
                "The Xuni license key has expired. Please rebuild the app with a new key.",
                "Your Xuni trial has expired.",
                "Your Xuni license is valid.",
                "This app was built using an evaluation copy of Xuni. You have {0} days remaining.",
#endif
            };

            int msgNumber = licenseStatus - 1;
            if (msgNumber < 0 || msgNumber > msgs.Length)
                msgNumber = 0;

            string msg = msgs[msgNumber];
            if(msg.IndexOf("{0}") > -1)
            {
                msg = string.Format(msg, daysRemaining);
            }
            return msg;
        }
    }
#endif

    internal class LicenseHandler
    {
        // Must use base class X509Certificate instead of X509Certificate2 because
        // Windows Phone does not support X509Certificate2 or even Key Exchanges
        // to allow verification.
        //
        // We can at least get some verification of correctness through the
        // packing/unpacking and object creation.

#if ASPNETCORE || NETCORE
        internal
#endif
        enum LicenseStatus
        {
            Unknown = 0,
            InvalidCertificate = 1,
            ExpiredFull = 2,
            ExpiredEval = 3,
            FullValid = 4,
            EvalValid = 5,
        };

        static bool VerifySigner(X509Certificate cert, string text, byte[] signature)
        {
            byte[] data = GetEncodedBytes(text);
            return VerifySigner(cert, data, signature);
        }
        static bool VerifySigner(X509Certificate cert, byte[] data, byte[] signature)
        {
            // Get its associated CSP and public key
            //RSACryptoServiceProvider csp = (RSACryptoServiceProvider)cert.PublicKey.Key;
#if NETCORE || NETSTANDARD
            var rsa = cert.GetRSAPublicKey();
#elif !WINDOWS_PHONE
            Oid oidkey = new Oid("1.2.840.113549.1.1.1");
            AsnEncodedData parms = new AsnEncodedData(cert.GetKeyAlgorithmParameters());
            AsnEncodedData keybs = new AsnEncodedData(cert.GetPublicKey());
            PublicKey pk = new PublicKey(oidkey, parms, keybs);
            RSACryptoServiceProvider csp = pk.Key as RSACryptoServiceProvider;
#else
            // can create the rsa provider, but have no way to recover the
            // public key to verify the signature.  creating the rsa provider
            // allows the code to continue.  Maybe later this will be supported.
            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
#endif

            // Hash the data
#if NETCORE || NETSTANDARD
            var sha = SHA1.Create();
#else
            SHA1Managed sha = new SHA1Managed();
#endif
            byte[] hash = sha.ComputeHash(data);

#if NETCORE || NETSTANDARD
            bool result = rsa.VerifyHash(hash, signature, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
#else
            // Verify the signature with the hash
            string oid = "1.3.14.3.2.26";  // CryptoConfig.MapNameToOID("SHA1");
            bool result = csp.VerifyHash(hash, oid, signature);
#endif

#if WINDOWS_PHONE
            result = true;  // X509 Services not available.
#endif
            return result;
        }

        // provide for uniform encoding and decoding of strings
        internal static string GetEncodedString(byte[] bytes)
        {
            return Encoding.Unicode.GetString(bytes, 0, bytes.Length);
        }
        internal static byte[] GetEncodedBytes(string str)
        {
            return Encoding.Unicode.GetBytes(str);
        }

        const int runtimeDataLen = 8;

        static bool UnpackRuntimeLicense(string licenseData, out byte[] appNameBytes, out byte[] appNameSignature,
            out byte[] pbkSignature, out byte[] progCertBytes, out byte[] pubCertBytes)
        {
            bool result = false;
            appNameBytes = appNameSignature = pbkSignature = progCertBytes = pubCertBytes = null;

            byte[] data = null;
            try { data = Convert.FromBase64String(licenseData); }
            catch { data = null; }

            if (data != null && data.Length > 20)
            {
                int startAppSig = (((int)data[0]) << 8) + data[1];
                int startPbkSig = (((int)data[2]) << 8) + data[3];
                int startPrgCert = (((int)data[4]) << 8) + data[5];
                int startPubCert = (((int)data[6]) << 8) + data[7];

                if (startAppSig < startPbkSig && startPbkSig < startPrgCert && startPrgCert < startPubCert &&
                    startPubCert < data.Length - 5)
                {
                    try
                    {
                        appNameBytes = new byte[startAppSig - runtimeDataLen];  // length of appName
                        appNameSignature = new byte[startPbkSig - startAppSig]; // length of appNameSignature
                        pbkSignature = new byte[startPrgCert - startPbkSig];    // length of the public cert signature
                        progCertBytes = new byte[startPubCert - startPrgCert];  // length of the program cert
                        pubCertBytes = new byte[data.Length - startPubCert];    // length of the public cert

                        Array.Copy(data, runtimeDataLen, appNameBytes, 0, appNameBytes.Length);
                        Array.Copy(data, startAppSig, appNameSignature, 0, appNameSignature.Length);
                        Array.Copy(data, startPbkSig, pbkSignature, 0, pbkSignature.Length);
                        Array.Copy(data, startPrgCert, progCertBytes, 0, progCertBytes.Length);
                        Array.Copy(data, startPubCert, pubCertBytes, 0, pubCertBytes.Length);
                        result = true;
                    }
                    catch
                    {
                        result = false;
                    }
                }
            }

            return result;
        }

        static bool validProductLicense(X509Certificate cert, string validCodes)
        {
            string certSubject = cert.Subject;
            if (string.CompareOrdinal(certSubject, cert.Issuer) == 0)
            {
                string[] allValidCodes = ("Xuni Internal Developer,0FFFFFFF,0EEEEEEE," + validCodes).Split(',');
                certSubject = certSubject.Substring("CN=GC-".Length);

                foreach (string code in allValidCodes)
                {
                    if (certSubject.StartsWith(code, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }

// TFS 401687 GetSerialNumberString return reservered order in .Net Core 3.0. 
#if ASPNETCORE || NETCORE
              // GC-Internal type license.
              if (cert.SerialNumber == "0EEEEEEE")
#else 
              // GC-Internal type license.
              if (cert.GetSerialNumberString() == "0EEEEEEE")
#endif
              return true;
            }
            return false;
        }

        // Verify legacy standard license data.
        // NOTE: NOT supported in DNX Core.
        public static int VerifyLegacyStandardLicense(string appName, Assembly asm, string[] licenseContent, out int daysRemaining)
        {
            // runtime key will be decrypted in VerifyStandardLicenseCore.
            const int runtimeKeyIndex = 1;
            var decryptedContent = licenseContent
                .Select((l, i) => i == runtimeKeyIndex ? l : ProductLicense.Decrypt(l))
                .ToArray();
            return VerifyStandardLicenseCore(appName, asm, decryptedContent, out daysRemaining);
        }

        // Verify standard license data.
        // NOTE: Supported in DNX Core.
        public static int VerifyStandardLicense(string appName, Assembly asm, string licenseData, out int daysRemaining)
        {
            const char licDataSeparator = ',';
            var licenseContent = ProductLicense.Decrypt(licenseData).Split(licDataSeparator);
            return VerifyStandardLicenseCore(appName, asm, licenseContent, out daysRemaining);
        }

        private static int VerifyStandardLicenseCore(string appName, Assembly asm, string[] licenseContent, out int daysRemaining)
        {
            var result = LicenseStatus.Unknown;
            daysRemaining = -1;
            const int licContentLength = 3;
            const int asmNameIndex = 0;
            const int runtimeKeyIndex = 1;
            const int appIdIndex = 2;

            var asmName = asm.GetName().Name.TrimEnd(".ja".ToCharArray());
            if (licenseContent.Length == licContentLength && asmName == licenseContent[asmNameIndex])
            {
                string authorizedAppId = licenseContent[appIdIndex];
                try
                {
                    if (appName.StartsWith(authorizedAppId, StringComparison.OrdinalIgnoreCase))
                    {
                        result = StandardLicenseProcessor.ValidateRunTimeKey(licenseContent[runtimeKeyIndex], asm.GetName().Version.ToString(), out daysRemaining);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }

            return (int)result;
        }

        public static int VerifyCertLicense(string appName, string licenseData, string validCodes, out int daysRemaining)
        {
            // License status is returned. See enum.
            int result = (int)LicenseStatus.InvalidCertificate;
            bool isEval = false;
            daysRemaining = -1;

            byte[] appNameBytes, appNameSignature, pbkSignature, progCertBytes, pubCertBytes;

            if (UnpackRuntimeLicense(licenseData, out appNameBytes, out appNameSignature, out pbkSignature,
                out progCertBytes, out pubCertBytes))
            {
                try
                {
                    // first, the appName must match
                    if (appName.StartsWith(GetEncodedString(appNameBytes), StringComparison.OrdinalIgnoreCase))
                    {
                        // verify the signature of the appName
                        X509Certificate appCert = new X509Certificate(pubCertBytes);
                        if (VerifySigner(appCert, appNameBytes, appNameSignature))
                        {
                            DateTime notAfter = DateTime.Parse(appCert.GetExpirationDateString());
                            // The date and time are formatted using the current culture and time zone.
                            notAfter = notAfter.ToUniversalTime();
                            DateTime expiry = DateTime.Today;

// TFS 401687 GetSerialNumberString return reservered order in .Net Core 3.0. 
#if ASPNETCORE || NETCORE
              if (appCert.SerialNumber == "0FFFFFFF")
#else
              if (appCert.GetSerialNumberString() == "0FFFFFFF")
#endif
              {
                // this is a trial serial number.  return false if the current date
                // is not within the limits of the certificate.
                isEval = true;
                                DateTime notBefore = DateTime.Parse(appCert.GetEffectiveDateString());
                                // The date and time are formatted using the current culture and time zone.
                                notBefore = notBefore.ToUniversalTime();

                                if (expiry > notAfter.AddDays(1) || expiry < notBefore.AddDays(-1))
                                    return (int)LicenseStatus.ExpiredEval;
                            }
                            else
                            {
                                if (!validProductLicense(appCert, validCodes))
                                    return (int)LicenseStatus.InvalidCertificate;
#if !ASPNETCORE && !NETCORE
                                Assembly asm = Assembly.GetExecutingAssembly();
#else
                                var asm = typeof(LicenseHandler).GetTypeInfo().Assembly;
#endif
                                AssemblyName asmn = asm.GetName();

                                int mo = asmn.Version.Build;
                                int year = mo / 10;
                                mo = (mo - 10 * year) * 4;

                                expiry = new DateTime(year, mo, (mo == 4) ? 30 : 31);
                                if (expiry > notAfter.AddDays(1))
                                {
                                    // The version expiration date is after the end of the certificate.
                                    // If the version starts before the end of the cert, allow it
                                    DateTime expiryStart;
                                    if (mo == 4)
                                        expiryStart = new DateTime(year - 1, 12, 31);
                                    else
                                        expiryStart = new DateTime(year, mo - 4, (mo == 8) ? 30 : 31);

                                    if (notAfter < expiryStart)
                                        return (int)LicenseStatus.ExpiredFull;

                                    // if not expired, then change notAfter for the daysRemaining calculation
                                    // such that it is days to the end of the trimester.
                                    notAfter = expiry;
                                }
                            }

                            daysRemaining = notAfter.Subtract(expiry).Days;
                            if (daysRemaining < 0) daysRemaining = 0;

                            // now verify the certificate itself
                            appCert = new X509Certificate(progCertBytes);
                            if (VerifySigner(appCert, pubCertBytes, pbkSignature))
                            {
                                string[] validCertData =
                                {
#if !GRAPECITY
                                    // GrapeCity builds do not honor non-GrapeCity certificates
                                    // Only non-GrapeCity builds honor non-GrapeCity certificates
                                    "CN=ComponentOne,9F12AAE32130A4FBCA4250EC0346471C5DAF5FC9",
#endif
                                    // GrapeCity licenses (certificates) are always honored
                                    "CN=GrapeCity,8D67D6615C6DB192851E7370BF3562F945D2C704",
                                };
                                bool validCert = appCert.Issuer.IndexOf("CN=Verisign", StringComparison.OrdinalIgnoreCase) > -1;
                                if (validCert)
                                {
                                    var isOSX = false;
#if NETCORE  || NETSTANDARD
                                    // 308926, In MAC + ASP.NET CORE 2.0 project, the T61String cannot be decoded.
                                    // in our certificate, the CN/O/OU part is encoded as T61String.
                                    isOSX = RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
#endif
                                    validCert = false;
                                    foreach (string vcert in validCertData)
                                    {
                                        string[] valcert = vcert.Split(',');
                                        validCert = (isOSX || appCert.Subject.IndexOf(valcert[0], StringComparison.OrdinalIgnoreCase) > -1) &&
                                            string.CompareOrdinal(appCert.GetCertHashString(), valcert[1]) == 0;
                                        if (validCert) break;
                                    }
                                }

                                result = (int)(validCert ? (isEval ? LicenseStatus.EvalValid : LicenseStatus.FullValid) :
                                    LicenseStatus.InvalidCertificate);
                            }
                        }
                    }
                }
#if DEBUG
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    result = (int)LicenseStatus.InvalidCertificate;
                }
#else
                catch { result = (int)LicenseStatus.InvalidCertificate; }
#endif
            }

            return result;
        }
    }
}
