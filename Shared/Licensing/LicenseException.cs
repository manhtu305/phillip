﻿using System;

namespace C1.Util.Licensing
{
    /// <summary>
    /// The exception for licensing.
    /// </summary>
    public class LicenseException : Exception
    {
        /// <summary>
        /// Create a LicenseException with message.
        /// </summary>
        /// <param name="message">The message.</param>
        public LicenseException(string message = null):base(message)
        {
        }
    }
}
