﻿using System;
using C1.Util.Licensing;
using Microsoft.Win32;
using System.Collections.Generic;

namespace C1Licensing
{
    internal class StandardLicenseProcessor
    {
        private const int EVAL_DURATION = 30;

#if ASPNETCORE_LIC_GEN

        internal static string GenerateRunTimeKey(ProductLicense productLicense)
        {
            return EncryptKey(productLicense.ProductCode, productLicense.Key);
        }

        internal static string GenerateEvalRuntimeKey()
        {
            DateTime utcnow = DateTime.UtcNow;
            return EncryptKey("EVAL", utcnow.Ticks.ToString());
        }

        private static string EncryptKey(string part0, string part1)
        {
            var rnd = new Random();
            var s = string.Format("{0}\t{1}\t{2}", part0, part1, rnd.Next(0xff));
            return ProductLicense.Encrypt(s, true);
        }

        internal static int GetEvaluationDaysLeft(List<Guid> productGuids, ref bool isTrialFound)
        {
            int daysElapsed = GetEvaluationDaysElapsed(productGuids, ref isTrialFound);
            return EVAL_DURATION - daysElapsed;
        }

        /// <summary>
        /// Extracted from C1.Util.Licensing.LicenseInfo
        /// </summary>
        /// <returns></returns>
        private static int GetEvaluationDaysElapsed(List<Guid> productGuids, ref bool isTrialFound)
        {
            DateTime toDay = DateTime.Today;
            DateTime installDate = DateTime.MinValue;
            bool remnants = false;
            string keyname = null;
            var getStop = false;

            if (productGuids != null && productGuids.Count > 0)
            {
                DateTime inDate = DateTime.MinValue;
                string kyname = null;

                foreach (Guid pguid in productGuids)
                {
                    string kn = pguid.ToString().ToUpper();
                    DateTime timeData;
                    if (DTStorage.GetUtcDateTime(kn, out timeData))
                    {  //find latest eval install date.
                        if (timeData > inDate)
                        {
                            inDate = timeData;
                            kyname = kn;
                        }
                    }
                    else
                    {
                        if (!remnants && DTStorage.GetUtcDateTime("today" + kn, out timeData))
                            remnants = true;
                    }
                }

                if (kyname != null)
                {
                    keyname = kyname;
                    installDate = inDate;
                    getStop = true;
                    isTrialFound = true;
                }

                // if there are remnants of an eval license, but no eval license, then hard stop.
                if (!getStop && remnants)
                {
                    getStop = true;
                }
            }

            if (getStop)
            {
                toDay = (keyname == null) ? DateTime.MaxValue :
              DTStorage.GetCurrentUtcDateTime("today" + keyname, DateTime.MaxValue);
            }

            // return number of evaluation days elapsed so far
            var evalDaysElapsed = (int)toDay.Subtract(installDate).TotalDays;
            return evalDaysElapsed;
        }
#else
        internal static LicenseHandler.LicenseStatus ValidateRunTimeKey(string key, string assemblyVersion, out int daysRemaining)
        {
            var runtimeKey = ProductLicense.Decrypt(key);
            daysRemaining = -1;

            if (runtimeKey != null)
            {
                string[] parts = runtimeKey.Split('\t');
                if (parts.Length == 3)
                {
                    if (parts[0].Length == 2)
                    {
                        var pl = new ProductLicense();
                        pl.Key = parts[1];
                        pl.CheckExpirationDate(assemblyVersion);

                        if (pl.IsExpired)
                        {
                            return LicenseHandler.LicenseStatus.ExpiredFull;
                        }

                        daysRemaining = 365;
                        return LicenseHandler.LicenseStatus.FullValid;
                    }
                    else if (parts[0] == "EVAL")
                    {
                        string compileDateStr = parts[1];
                        long utcTicks = 0;
                        if (long.TryParse(compileDateStr, out utcTicks))
                        {
                            DateTime utcNow = DateTime.UtcNow;
                            TimeSpan tsc = utcNow.Subtract(DateTime.FromBinary(utcTicks));
                            int evalDaysElapsed = tsc.Duration().Days;
                            daysRemaining = EVAL_DURATION - evalDaysElapsed;
                            return daysRemaining >= 0 ? LicenseHandler.LicenseStatus.EvalValid : LicenseHandler.LicenseStatus.ExpiredEval;
                        }
                    }

                }
            }
            return LicenseHandler.LicenseStatus.Unknown;
        }
#endif
    }
}
