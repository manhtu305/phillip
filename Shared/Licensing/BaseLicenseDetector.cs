﻿using System;
using System.ComponentModel;
#if ASPNETCORE || NETCORE
using LicenseStatus = C1Licensing.LicenseHandler.LicenseStatus;
#endif

namespace C1.Util.Licensing
{
    /// <summary>
    /// Define a class for detecting license.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class BaseLicenseDetector
    {
        private DateTime? _betaExpiredDate;

        /// <summary>
        /// The constructor.
        /// </summary>
        protected BaseLicenseDetector()
        {
            var type = GetType();
            if(IsBeta)
            {
                var remain = (BetaExpiredDate - DateTime.Now).Days;
#if !ASPNETCORE && !NETCORE
                if(remain < 0)
                {
                    throw new LicenseException(type);
                }

                LicenseInfo = new LicenseInfo(type, LicenseStatus.Valid);
#else
                if (remain < 0)
                {
                    throw new LicenseException(LicenseResources.AppLicenseExpiredFull);
                }

                LicenseInfo = new LicenseInfo(LicenseStatus.FullValid, 0);
#endif
                return;
            }

            LicenseInfo = Validate(type);

        }

        private LicenseInfo Validate(Type type)
        {
#if !ASPNETCORE && !NETCORE
            var licenseInfo = ProviderInfo.Validate(type, this, false);
            // Fix bug 215083
            // Below codes are copied from C1.Util.Licensing.ProviderInfo.Validate method.
            if (!licenseInfo.IsValid && licenseInfo.EvaluationDaysLeft < 0 && licenseInfo.GetStop)
            {
                throw new LicenseException(licenseInfo.Type);
            }

            return licenseInfo;
#else
            return ProviderInfo.Validate(type);
#endif
        }

        internal LicenseInfo LicenseInfo { get; private set; }

        internal DateTime BetaExpiredDate
        {
            get
            {
                if (!_betaExpiredDate.HasValue)
                {
                    // try to get release date of this assembly from version.
                    // For example, the third part in version No.: 4.0.20163.123
                    var dateStr = AssemblyInfo.Version.Split('.')[2];
                    var year = int.Parse(dateStr.Substring(0, 4));
                    var seasonNumber = int.Parse(dateStr.Substring(4));
                    const int seasons = 3;
                    const int monthEarlierThanSeasonEnd = 1;
                    const int monthsPerYear = 12;
                    const int middleDayOfMonth = 15;
                    var releaseMonth = monthsPerYear / seasons * seasonNumber - monthEarlierThanSeasonEnd;
                    var releaseDate = new DateTime(year, releaseMonth, middleDayOfMonth);

                    // Beta version duration is 6 months.
                    const int betaDurationMonths = 6;
                    _betaExpiredDate = releaseDate.AddMonths(betaDurationMonths);
                }

                return _betaExpiredDate.Value;
            }
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual bool IsBeta { get { return false; } }
    }
}