using System;
using System.Collections.Generic;

#if ASPNETCORE
#if !NETCORE3
using Microsoft.Extensions.PlatformAbstractions;
#endif
using Microsoft.AspNetCore.Http;
#elif !WEBAPI
using HttpContext = System.Web.HttpContextBase;
using HttpRequest = System.Web.HttpRequestBase;
#endif

#if NETCORE
using System.Linq;
using System.Security.Cryptography.X509Certificates;
#endif

namespace C1.Util.Licensing
{
    internal static partial class LicenseHelper
    {
        private static readonly object _lockObj = new object();
        private static readonly IDictionary<Type, LicenseInfo> LicenseInfos = new Dictionary<Type, LicenseInfo>();

        internal static bool NeedRenderEvalInfo(Type type
#if !WEBAPI
            , HttpContext context
#endif
            )
        {
            LicenseInfo licenseInfo = GetLicenseInfo(type);
#if GRAPECITY || WEBAPI
            return !licenseInfo.IsValid;
#else
            return !licenseInfo.IsValid && !IsLocalHost(context);
#endif

        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static bool IsValidLicense<T>() where T : BaseLicenseDetector, new()
        {
            LicenseInfo licenseInfo = GetLicenseInfo(typeof(T));
            return licenseInfo.IsValid
#if ASPNETCORE || NETCORE
                || licenseInfo.Status == C1Licensing.LicenseHandler.LicenseStatus.EvalValid
#else
                || licenseInfo.EvaluationDaysLeft > 0;
#endif
                ;
        }

        private static LicenseInfo GetLicenseInfo(Type type)
        {
            LicenseInfo licenseInfo;
            if (!LicenseInfos.TryGetValue(type, out licenseInfo))
            {
                lock (_lockObj)
                {
                    if (!LicenseInfos.TryGetValue(type, out licenseInfo))
                    {
                        var ld = Activator.CreateInstance(type) as BaseLicenseDetector;
                        licenseInfo = ld.LicenseInfo;
                        LicenseInfos.Add(type, licenseInfo);
                    }
                }
            }

            return licenseInfo;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static bool NeedRenderEvalInfo<T>(
#if !WEBAPI
            HttpContext context
#endif
            ) where T : BaseLicenseDetector, new()
        {
            return NeedRenderEvalInfo(typeof(T)
#if !WEBAPI
                , context
#endif
                );
        }

#if !WEBAPI
        private static bool IsLocalHost(HttpContext httpContext)
        {
            return httpContext != null &&
                   httpContext.Request.GetHost().StartsWith("localhost", StringComparison.OrdinalIgnoreCase);
        }

        private static string GetHost(this HttpRequest re)
        {
#if ASPNETCORE
            return re.Host.Value ?? string.Empty;
#else
            return re.Url != null ? re.Url.Host : string.Empty;
#endif
        }

#endif

#if NETCORE
        public static string GetExpirationDateString(this X509Certificate2 cert)
        {
            return cert.NotAfter.ToString();
        }

        public static string GetSerialNumberString(this X509Certificate2 cert)
        {
            return cert.SerialNumber;
        }

        public static string GetEffectiveDateString(this X509Certificate2 cert)
        {
            return cert.NotBefore.ToString();
        }

        public static string GetCertHashString(this X509Certificate2 cert)
        {
            return string.Join(string.Empty, cert.GetCertHash().Select(b => b.ToString("X2")));
        }
#endif

    }
}