namespace C1.Util.Licensing
{
    internal static partial class LicenseResources
    {

        internal static string AppLicenseEvalValid
        {
            get
            {
                return ResourceManager.GetString("AppLicenseEvalValid", Culture);
            }
        }

        internal static string AppLicenseExpiredEval
        {
            get
            {
                return ResourceManager.GetString("AppLicenseExpiredEval", Culture);
            }
        }

        internal static string AppLicenseExpiredFull
        {
            get
            {
                return ResourceManager.GetString("AppLicenseExpiredFull", Culture);
            }
        }

        internal static string AppLicenseFullValid
        {
            get
            {
                return ResourceManager.GetString("AppLicenseFullValid", Culture);
            }
        }

        internal static string AppLicenseInvalidCert
        {
            get
            {
                return ResourceManager.GetString("AppLicenseInvalidCert", Culture);
            }
        }

        internal static string AppLicenseUnknown
        {
            get
            {
                return ResourceManager.GetString("AppLicenseUnknown", Culture);
            }
        }

        internal static string AppLicenseNotSet
        {
            get
            {
                return ResourceManager.GetString("AppLicenseNotSet", Culture);
            }
        }

        internal static string AppLicenseNoAppName
        {
            get
            {
                return ResourceManager.GetString("AppLicenseNoAppName", Culture);
            }
        }
    }
}