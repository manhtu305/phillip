//#define TESTCRYPTO
#define USE_REFLECTION

using GrapeCity.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace GrapeCity.Common
{
    internal class C1Product : Product
    {
        public override Guid Guid => Guid.Empty;
        public override string Name => string.Empty;
#if UWP
        public override Version Version => typeof(C1Product).GetTypeInfo().Assembly.GetName().Version;
#else
        public override Version Version => System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
#endif
        public virtual string C1Code => null;
    }
#if TESTCRYPTO
    /****************************************************************************************
     * Following C1Product classes are for Test GCLM Testing 
     ****************************************************************************************/
    internal class C1WPFProduct : C1Product
    {
        public override Guid Guid => new Guid("3a18e60c-6e2b-413a-883f-d59f42b63929");
        public override string Name => "Component One for WPF";
        public override string C1Code => "S6";
    }
    internal class C1WinformProduct : C1Product
    {
        public override Guid Guid => new Guid("1b8597c5-0f4f-4cbb-bc34-d457a564eae8");
        public override string Name => "ComponentOne for Windows Forms";
        public override string C1Code => "S8";
    }
    internal class C1DesktopProduct : C1Product
    {
        public override Guid Guid => new Guid("76978edf-c4a2-4ab1-9baa-b90c79b62d44");
        public override string Name => "ComponentOne for Desktop";
        public override string C1Code => "SD";
    }
    internal class C1SEProduct : C1Product
    {
        public override Guid Guid => new Guid("338e8154-a14e-42ba-a433-7f4339af93f7");
        public override string Name => "ComponentOne Studio Enterprise";
        public override string C1Code => "SE,SU";
    }
    internal class C1UltimateProduct : C1Product
    {
        public override Guid Guid => new Guid("6db7542a-a9b8-477e-8651-3ddda4d3aacd");
        public override string Name => "ComponentOne Ultimate";
        public override string C1Code => "SU,SE";
    }
#else
    /****************************************************************************************
     * Following C1Product classes are for Production Licensing 
     ****************************************************************************************/
    // if GRAPECITY is defined, then only JP licenses are accepted.
    // if !GRAPECITY, then both JP and non-JP licenses are accepted.

#if !GRAPECITY
    internal class C1WPFProduct : C1Product
    {
        public override Guid Guid => new Guid("c02c28b7-1c24-4109-8eb3-f99c3905f3f1");
        public override string Name => "ComponentOne WPF Edition";
        public override string C1Code => "S6";
    }
    internal class C1WinformProduct : C1Product
    {
        public override Guid Guid => new Guid("da3d5d14-691f-4908-aa3c-fd3239734232");
        public override string Name => "ComponentOne WinForms Edition";
        public override string C1Code => "S8";
    }

    internal class C1SEProduct : C1Product
    {
        public override Guid Guid => new Guid("331cf6cd-b73c-429f-ba79-fa2f85eebd68");
        public override string Name => "ComponentOne Studio Enterprise";
        public override string C1Code => "SE,SU";
    }

    internal class C1IOSProduct : C1Product
    {
        public override Guid Guid => new Guid("c2e333e5-6dae-4aaf-8903-fa1dc779d32a");
        public override string Name => "ComponentOne Xamarin.iOS Edition";
        public override string C1Code => "XE";
    }

    internal class C1AndroidProduct : C1Product
    {
        public override Guid Guid => new Guid("18dbecd7-edc0-49d0-95d9-bdb9e5e4827f");
        public override string Name => "ComponentOne Xamarin.Android Edition";
        public override string C1Code => "XE";
    }

    internal class C1XamarinProduct : C1Product
    {
        public override Guid Guid => new Guid("de2b5824-e24d-4e7f-86d1-a87c1729993c");
        public override string Name => "ComponentOne Studio for Xamarin";
        public override string C1Code => "XE";
    }

    internal class C1BlazorProduct : C1Product
    {
        public override Guid Guid => new Guid("6631ee67-fec7-45b0-a771-4ec75cd748e3");
        public override string Name => "ComponentOne Blazor Edition";
        public override string C1Code => "XE";
    }

    internal class C1UWPProduct : C1Product
    {
        public override Guid Guid => new Guid("9afa522c-ea0b-47fe-ae14-7d9225612767");
        public override string Name => "ComponentOne UWP Edition";
        public override string C1Code => "AJ";
    }

    internal class C1AspNetProduct : C1Product
    {
        public override Guid Guid => new Guid("839e1737-f256-46ea-b391-50da451c13a4");
        public override string Name => "ComponentOne ASP.NET MVC Edition";
        public override string C1Code => "AH";
    }
#endif
    // ---------------------------------------------------------------------------------------------------
    internal class C1WPFProductJP : C1Product
    {
        public override Guid Guid => new Guid("4327EAF8-AA02-40A6-B9F6-3D007C039055");
        public override string Name => "ComponentOne WPF Edition JP";
        public override string C1Code => "S6";
    }
    internal class C1WinformProductJP : C1Product
    {
        public override Guid Guid => new Guid("1E2DD705-CD7C-42CE-8098-C4717DF794B1");
        public override string Name => "ComponentOne WinForms Edition JP";
        public override string C1Code => "S8";
    }

    internal class C1SEProductJP : C1Product
    {
        public override Guid Guid => new Guid("154B86E3-6B5B-4B2E-ACDC-91D24D249879");
        public override string Name => "ComponentOne Studio Enterprise JP";
        public override string C1Code => "SE,SU";
    }

    internal class C1IOSProductJP : C1Product
    {
        public override Guid Guid => new Guid("C617BC79-C041-4111-B472-AD2C5A5AD5F2");
        public override string Name => "ComponentOne Xamarin.iOS Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1AndroidProductJP : C1Product
    {
        public override Guid Guid => new Guid("326784EA-3999-4598-9A3A-BD36CA1142FE");
        public override string Name => "ComponentOne Xamarin.Android Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1XamarinProductJP : C1Product
    {
        public override Guid Guid => new Guid("EB58417C-27BB-4F62-ACFA-4D36582D2D0C");
        public override string Name => "ComponentOne Studio for Xamarin JP";
        public override string C1Code => "XE";
    }

    internal class C1BlazorProductJP : C1Product
    {
        public override Guid Guid => new Guid("24D9C311-5D6C-4BA1-81E7-7F86B9495596");
        public override string Name => "ComponentOne Blazor Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1UWPProductJP : C1Product
    {
        public override Guid Guid => new Guid("EAAD5427-BBD9-4D82-801F-6F37FB4AF9FE");
        public override string Name => "ComponentOne UWP Edition JP";
        public override string C1Code => "AJ";
    }

    internal class C1AspNetProductJP : C1Product
    {
        public override Guid Guid => new Guid("B78FAC6C-CC85-4A78-A08F-FF3556EDFA97");
        public override string Name => "ComponentOne ASP.NET MVC Edition JP";
        public override string C1Code => "AH";
    }
#endif

    //   Ultimate = "SU"
    //   Enterprise = "SE";
    //   WinForms = "S8";
    //   WPF = "S6";
    //   Desktop = "SD";
    //   UWP = "AJ";
    //   ActiveX = "S7";
    //   ASP.Net = "AH";
    //   Xamarin = "XE";


    internal static class C1GCLicenseManager
    {
        static internal string ValidProductCodes {get; set;}
        
        static private bool IsValidLicense(ILicenseData<C1Product> licenseData)
        {
            bool validLicense = false;
            if ((licenseData.State == ActivationState.ProductActivated || licenseData.State == ActivationState.TrialActivated) && !string.IsNullOrEmpty(licenseData.Edition))
            {
                string[] c1Prods = ValidProductCodes.ToUpperInvariant().Split(",;".ToCharArray());
                if (!c1Prods.Any(p => licenseData.Product.C1Code.Split(',').Contains(p)))
                    return false;

                string edition = licenseData.Edition;
                if (licenseData.Trial || edition.IndexOf("Trial", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    validLicense = true;
                }
                else if (edition.StartsWith("v", StringComparison.OrdinalIgnoreCase))
                {
                    int year = 0, ver = 0;

                    if (int.TryParse(edition.Substring(1, 4), out year) && int.TryParse(edition.Substring(6, 1), out ver))
                    {
                        int licBuild = licenseData.Product.Version.Build;
                        validLicense = (licBuild <= ((year + 1) * 10 + ver));
                    }
                }
            }
            return validLicense;
        }

#if USE_REFLECTION
        // These are built in C1Products and should always be checked last
        static Type[] studioProducts = new Type[]
        {
#if TESTCRYPTO
            typeof(C1WPFProduct),           // Test Licensing
            typeof(C1WinformProduct),
            typeof(C1DesktopProduct),
            typeof(C1SEProduct),
            typeof(C1UltimateProduct),      // Only Ultimate has trial and must be last.
#else
#if !GRAPECITY
            typeof(C1WPFProduct),               // Production Licensing
            typeof(C1WinformProduct),
            typeof(C1XamarinProduct),
            typeof(C1IOSProduct),
            typeof(C1AndroidProduct),
            typeof(C1UWPProduct),
            typeof(C1BlazorProduct),
            typeof(C1AspNetProduct),
            //typeof(C1SEProduct),                 // Only Enterprise has trial and must be last
#endif
            typeof(C1WPFProductJP),               // Production Licensing
            typeof(C1WinformProductJP),
            typeof(C1XamarinProductJP),
            typeof(C1IOSProductJP),
            typeof(C1AndroidProductJP),
            typeof(C1UWPProductJP),
            typeof(C1BlazorProductJP),
            typeof(C1AspNetProductJP),
            typeof(C1SEProductJP),               // Only Enterprise has trial and must be last
#if !GRAPECITY
            typeof(C1SEProduct),                 // Only Enterprise has trial and must be last
#endif
#endif
        };

        // The full list of C1Product types will be built in prodTypes
        static List<Type> prodTypes = null;

        static private MemberInfo[] GetLicenseManagerMemberInfo(string memberName)
        {
            if (prodTypes == null)
            {
                Assembly asm = null;
#if UWP
                asm = typeof(C1GCLicenseManager).GetTypeInfo().Assembly;
#else
                asm = Assembly.GetExecutingAssembly();
#endif
                prodTypes = new List<Type>(asm.GetTypes().
                    Where(p => (p.IsSubclassOf(typeof(C1Product)) && !studioProducts.Contains(p))));

                prodTypes.AddRange(studioProducts);

                string[] productCodes = ValidProductCodes.Split(",;".ToCharArray());
                prodTypes = prodTypes.Where(p =>
                    productCodes.Any(pc =>
                      (p.GetConstructor(new Type[0]).Invoke(null) as C1Product).C1Code.Split(',').Contains(pc))                    
                    ).ToList();
            }

            Type tylm = typeof(GcLicenseManager<>);
            MemberInfo[] mis = new MemberInfo[prodTypes.Count];

            for (int pt = 0; pt < prodTypes.Count; pt++)
            {
                Type ctylm = tylm.MakeGenericType(prodTypes[pt]);
                MemberInfo[] mi = ctylm.GetMember(memberName, BindingFlags.Static | BindingFlags.Public);
                mis[pt] = mi[0];
            }
            return mis;
        }

        static private ILicenseData<C1Product> GetValidLicenseData(string licensePropertyName)
        {
            var mis = GetLicenseManagerMemberInfo(licensePropertyName);
            foreach (PropertyInfo pi in mis)
            {
                var lic = pi.GetValue(null) as ILicenseData<C1Product>;
                if (IsValidLicense(lic)) return lic;
            }
            return null;
        }

        static internal ILicenseData<C1Product> GetRuntimeLicense()
        {
            return GetValidLicenseData("RunTimeLicense");
        }

#else
        private static int CurrentEdition
        {
            get { return 20202; }
        }
        public static ILicenseData<C1Product> GetRuntimeLicense()
        {
            return GetValidLicenseData(
#if TESTCRYPTO
                GcLicenseManager<C1WPFProduct>.RunTimeLicense,
                GcLicenseManager<C1DesktopProduct>.RunTimeLicense,
                GcLicenseManager<C1SEProduct>.RunTimeLicense,
                GcLicenseManager<C1UltimateProduct>.RunTimeLicense,
#else
#if !GRAPECITY
                GcLicenseManager<C1WPFProduct>.RunTimeLicense,
                GcLicenseManager<C1WinformProduct>.RunTimeLicense,
                GcLicenseManager<C1XamarinProduct>.RunTimeLicense,
                GcLicenseManager<C1IOSProduct>.RunTimeLicense,
                GcLicenseManager<C1AndroidProduct>.RunTimeLicense,
                GcLicenseManager<C1UWPProduct>.RunTimeLicense,
                GcLicenseManager<C1BlazorProduct>.RunTimeLicense,
                GcLicenseManager<C1AspNetProduct>.RunTimeLicense,
                GcLicenseManager<C1SEProduct>.RunTimeLicense,
#endif
                GcLicenseManager<C1WPFProductJP>.RunTimeLicense,
                GcLicenseManager<C1WinformProductJP>.RunTimeLicense,
                GcLicenseManager<C1XamarinProductJP>.RunTimeLicense,
                GcLicenseManager<C1IOSProductJP>.RunTimeLicense,
                GcLicenseManager<C1AndroidProductJP>.RunTimeLicense,
                GcLicenseManager<C1UWPProductJP>.RunTimeLicense,
                GcLicenseManager<C1BlazorProductJP>.RunTimeLicense,
                GcLicenseManager<C1AspNetProductJP>.RunTimeLicense,
                GcLicenseManager<C1SEProductJP>.RunTimeLicense,
#if !GRAPECITY
                GcLicenseManager<C1SEProduct>.RunTimeLicense,
#endif
#endif
            );
        }

        private static ILicenseData<C1Product> GetValidLicense(params ILicenseData<C1Product>[] licenseList)
        {
            var validLicense = licenseList.FirstOrDefault((license) => IsValidLicense(license));
            return validLicense ?? licenseList[0];
        }
#endif
    }
}