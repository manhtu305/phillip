﻿namespace GrapeCity.Common
{
    partial class LicenseDialog<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headerPanel = new System.Windows.Forms.Panel();
            this.headerText = new System.Windows.Forms.Label();
            this.headerIcon = new System.Windows.Forms.PictureBox();
            this.footerPanel = new System.Windows.Forms.Panel();
            this.aboutButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.bodyLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.bodyText = new System.Windows.Forms.Label();
            this.bodyWebLink = new System.Windows.Forms.LinkLabel();
            this.bodyActivateLink = new System.Windows.Forms.LinkLabel();
            this.bodyDetailHeader = new System.Windows.Forms.Label();
            this.bodyDetail = new System.Windows.Forms.TextBox();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.headerIcon)).BeginInit();
            this.footerPanel.SuspendLayout();
            this.bodyLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.AutoSize = true;
            this.headerPanel.Controls.Add(this.headerText);
            this.headerPanel.Controls.Add(this.headerIcon);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Padding = new System.Windows.Forms.Padding(2);
            this.headerPanel.Size = new System.Drawing.Size(400, 50);
            this.headerPanel.TabIndex = 0;
            // 
            // headerText
            // 
            this.headerText.AutoSize = true;
            this.headerText.Location = new System.Drawing.Point(48, 7);
            this.headerText.Margin = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.headerText.MaximumSize = new System.Drawing.Size(345, 0);
            this.headerText.MinimumSize = new System.Drawing.Size(345, 0);
            this.headerText.Name = "headerText";
            this.headerText.Size = new System.Drawing.Size(345, 13);
            this.headerText.TabIndex = 0;
            // 
            // headerIcon
            // 
            this.headerIcon.Location = new System.Drawing.Point(7, 7);
            this.headerIcon.Margin = new System.Windows.Forms.Padding(5);
            this.headerIcon.Name = "headerIcon";
            this.headerIcon.Size = new System.Drawing.Size(36, 36);
            this.headerIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.headerIcon.TabIndex = 0;
            this.headerIcon.TabStop = false;
            // 
            // footerPanel
            // 
            this.footerPanel.AutoSize = true;
            this.footerPanel.Controls.Add(this.aboutButton);
            this.footerPanel.Controls.Add(this.okButton);
            this.footerPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footerPanel.Location = new System.Drawing.Point(0, 286);
            this.footerPanel.Name = "footerPanel";
            this.footerPanel.Size = new System.Drawing.Size(400, 38);
            this.footerPanel.TabIndex = 2;
            // 
            // aboutButton
            // 
            this.aboutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.aboutButton.AutoSize = true;
            this.aboutButton.Location = new System.Drawing.Point(7, 7);
            this.aboutButton.Margin = new System.Windows.Forms.Padding(5);
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Size = new System.Drawing.Size(80, 24);
            this.aboutButton.TabIndex = 0;
            this.aboutButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.AutoSize = true;
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(313, 7);
            this.okButton.Margin = new System.Windows.Forms.Padding(5);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(80, 24);
            this.okButton.TabIndex = 1;
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.bodyLayoutPanel.AutoSize = true;
            this.bodyLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bodyLayoutPanel.Controls.Add(this.bodyText);
            this.bodyLayoutPanel.Controls.Add(this.bodyWebLink);
            this.bodyLayoutPanel.Controls.Add(this.bodyActivateLink);
            this.bodyLayoutPanel.Controls.Add(this.bodyDetailHeader);
            this.bodyLayoutPanel.Controls.Add(this.bodyDetail);
            this.bodyLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bodyLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.bodyLayoutPanel.Location = new System.Drawing.Point(0, 50);
            this.bodyLayoutPanel.Name = "flowLayoutPanel1";
            this.bodyLayoutPanel.Padding = new System.Windows.Forms.Padding(2);
            this.bodyLayoutPanel.Size = new System.Drawing.Size(400, 236);
            this.bodyLayoutPanel.TabIndex = 1;
            this.bodyLayoutPanel.WrapContents = false;
            // 
            // bodyText
            // 
            this.bodyText.AutoSize = true;
            this.bodyText.Location = new System.Drawing.Point(7, 7);
            this.bodyText.Margin = new System.Windows.Forms.Padding(5);
            this.bodyText.MaximumSize = new System.Drawing.Size(386, 0);
            this.bodyText.MinimumSize = new System.Drawing.Size(386, 70);
            this.bodyText.Name = "bodyText";
            this.bodyText.Size = new System.Drawing.Size(386, 70);
            this.bodyText.TabIndex = 0;
            // 
            // bodyWebLink
            // 
            this.bodyWebLink.AutoSize = true;
            this.bodyWebLink.Location = new System.Drawing.Point(12, 82);
            this.bodyWebLink.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.bodyWebLink.Name = "bodyWebLink";
            this.bodyWebLink.Size = new System.Drawing.Size(0, 13);
            this.bodyWebLink.TabIndex = 1;
            // 
            // bodyActivateLink
            // 
            this.bodyActivateLink.AutoSize = true;
            this.bodyActivateLink.Location = new System.Drawing.Point(12, 95);
            this.bodyActivateLink.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.bodyActivateLink.Name = "bodyActivateLink";
            this.bodyActivateLink.Size = new System.Drawing.Size(0, 13);
            this.bodyActivateLink.TabIndex = 2;
            // 
            // bodyDetailHeader
            // 
            this.bodyDetailHeader.AutoSize = true;
            this.bodyDetailHeader.Location = new System.Drawing.Point(7, 113);
            this.bodyDetailHeader.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.bodyDetailHeader.Name = "bodyDetailHeader";
            this.bodyDetailHeader.Size = new System.Drawing.Size(0, 13);
            this.bodyDetailHeader.TabIndex = 3;
            // 
            // bodyDetail
            // 
            this.bodyDetail.Location = new System.Drawing.Point(7, 131);
            this.bodyDetail.Margin = new System.Windows.Forms.Padding(5);
            this.bodyDetail.Multiline = true;
            this.bodyDetail.Name = "bodyDetail";
            this.bodyDetail.ReadOnly = true;
            this.bodyDetail.Size = new System.Drawing.Size(386, 100);
            this.bodyDetail.TabIndex = 4;
            this.bodyDetail.WordWrap = false;
            // 
            // LicenseDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(400, 324);
            this.Controls.Add(this.bodyLayoutPanel);
            this.Controls.Add(this.footerPanel);
            this.Controls.Add(this.headerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LicenseDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.headerIcon)).EndInit();
            this.footerPanel.ResumeLayout(false);
            this.footerPanel.PerformLayout();
            this.bodyLayoutPanel.ResumeLayout(false);
            this.bodyLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Panel headerPanel;
        protected System.Windows.Forms.Panel footerPanel;
        protected System.Windows.Forms.Button aboutButton;
        protected System.Windows.Forms.Button okButton;
        protected System.Windows.Forms.Label headerText;
        protected System.Windows.Forms.PictureBox headerIcon;
        protected System.Windows.Forms.FlowLayoutPanel bodyLayoutPanel;
        protected System.Windows.Forms.Label bodyText;
        protected System.Windows.Forms.LinkLabel bodyWebLink;
        protected System.Windows.Forms.LinkLabel bodyActivateLink;
        protected System.Windows.Forms.Label bodyDetailHeader;
        protected System.Windows.Forms.TextBox bodyDetail;

    }
}