﻿//==============================================================================
//  File Name   :   WinFormLicenseProvider.cs
//
//  Copyright (C) 2008 GrapeCity Inc. All rights reserved.
//
//  Distributable under grapecity code license.
//  See terms of license at www.grapecity.com.
//
//==============================================================================

// <fileinformation>
//   <summary>
//     This file defines the WinFormLicenseProvider class.
//   </summary>
//   <author name="Carl Chen" mail="Carl.Chen@grapecity.com"/>
//   <seealso ref=""/>
//   <remarks>
//     Implementation of the WinFormLicenseProvider class.
//   </remarks>
// </fileinformation>

// <history>
//   <record date="20081218" author="Carl Chen" revision="1.00.000">
//     Create the file and implement the class.
//   </record>
// </history>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security;
using System.Security.Permissions;
using System.Windows.Forms;
using System.Reflection;

namespace GrapeCity.Common
{
    /// <summary>
    ///   Represents a license provider of a Windows Forms PowerTools component.
    /// </summary>
    internal abstract class WinFormLicenseProvider<T> : GcNetFxLicenseProvider<T> where T : Product, new()
    {
        protected override void ShowLicenseDialogCore(T product, ActivationState state, ILicenseData<T> licenseData, object instance)
        {
            LicenseDialog<T> dialog = new LicenseDialog<T>(product, licenseData, this.ShowAboutBox);
            dialog.ShowDialog();
        }

        protected abstract void ShowAboutBox();
    }
}
