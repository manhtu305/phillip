﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;
using GrapeCity.Common.Resources;

namespace GrapeCity.Common
{
    internal partial class LicenseDialog<T> : Form where T : Product, new()
    {
        private T _product;
        private ILicenseData<T> _license;
        private Action _showAboutBox;

        public LicenseDialog(T product, ILicenseData<T> license, Action showAboutBox)
        {
            this._product = product;
            this._license = license;
            this._showAboutBox = showAboutBox;
            InitializeComponent();
            InitializeResource();
        }

        protected override void OnActivated(EventArgs e)
        {
            this.okButton.Focus();
            base.OnActivated(e);
        }

        protected virtual void InitializeResource()
        {
            var product = this._product;
            var isRuntime = this._license.IsRuntime;
            var state = this._license.State;
            var leftDays = this._license.GetLeftDays();

            LicenseDialogKind dialogKind = LicenseResource.GetDialogKind(this._license.State);
            switch (dialogKind)
            {
                case LicenseDialogKind.Info:
                    this.headerIcon.Image = new Bitmap(LicenseResource.InfoIcon);
                    this.headerPanel.BackColor = Color.Green;
                    this.headerText.ForeColor = Color.White;
                    break;
                case LicenseDialogKind.Warning:
                    this.headerIcon.Image = new Bitmap(LicenseResource.WarningIcon);
                    this.headerPanel.BackColor = Color.Orange;
                    this.headerText.ForeColor = Color.Black;
                    break;
                case LicenseDialogKind.Error:
                    this.headerIcon.Image = new Bitmap(LicenseResource.ErrorIcon);
                    this.headerPanel.BackColor = Color.Orange;
                    this.headerText.ForeColor = Color.Black;
                    break;
                default:
                    break;
            }

            this.Icon = new Icon(LicenseResource.GrapeCityIcon);
            this.Text = LicenseResource.GetDialogTitle();

            LicenseDialogWebLinkTarget linkTarget;
            this.headerText.Text = LicenseResource.GetDialogHeaderText(product, isRuntime, state, leftDays);
            this.headerText.Font = new Font(Control.DefaultFont.FontFamily, Control.DefaultFont.Size * 1.25f);
            this.bodyText.Text = LicenseResource.GetDialogBodyText(product, isRuntime, state, leftDays, out linkTarget);

            if (!isRuntime)
            {
                this.bodyDetailHeader.Visible = false;
                this.bodyDetail.Visible = false;

                this.bodyActivateLink.Visible = true;
                this.bodyActivateLink.Text = LicenseResource.GetDialogActivateLinkText();
                this.bodyActivateLink.LinkClicked += new LinkLabelLinkClickedEventHandler(RunActivateTool);

                if (state != ActivationState.NoLicense && state != ActivationState.InvalidLicense)
                {
                    this.bodyWebLink.Visible = true;
                    this.bodyWebLink.Text = LicenseResource.GetDialogWebLinkText(linkTarget);
                    switch (linkTarget)
                    {
                        case LicenseDialogWebLinkTarget.GotoWebFAQ:
                            this.bodyWebLink.LinkClicked += new LinkLabelLinkClickedEventHandler(GotoLink_FAQ);
                            break;
                        case LicenseDialogWebLinkTarget.GotoWebActivation:
                            this.bodyWebLink.LinkClicked += new LinkLabelLinkClickedEventHandler(GotoLink_Activation);
                            break;
                        case LicenseDialogWebLinkTarget.GotoWebShop:
                            this.bodyWebLink.LinkClicked += new LinkLabelLinkClickedEventHandler(GotoLink_Shop);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    this.bodyWebLink.Visible = false;
                }

                this.aboutButton.Text = LicenseResource.GetDialogAboutButtonText();
                if (this._showAboutBox == null)
                {
                    this.aboutButton.Visible = false;
                }
                else
                {
                    this.aboutButton.Visible = true;
                    this.aboutButton.Click += (sender, e)=> { this._showAboutBox(); };
                }
            }
            else
            {
                this.bodyDetailHeader.Visible = true;
                this.bodyDetail.Visible = true;
                this.bodyWebLink.Visible = false;
                this.bodyActivateLink.Visible = false;
                this.aboutButton.Visible = false;

                this.bodyDetailHeader.Text = LicenseResource.GetDialogDetailHeaderText();
                this.bodyDetail.Text = LicenseResource.GetDialogDetailInfo(product, isRuntime, state, this._license.BaseDate, this._license.MachineName, this._license.SerialKey);
            }
            this.okButton.Text = LicenseResource.GetDialogOKButtonText();
        }

        protected void RunActivateTool(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GcNetFxLicenseProvider<T>.RunActivateTool();
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        protected void GotoLink_FAQ(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                var psi = new ProcessStartInfo()
                {
#if GRAPECITY
                    FileName = "http://www.grapecity.com/japan/support/faq/",
#else
                    FileName = "http://www.grapecity.com/support/portal/",
#endif
                    UseShellExecute = true
                };
                Process.Start(psi);
            }
            catch
            {
            }
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        protected void GotoLink_Activation(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                var psi = new ProcessStartInfo()
                {
#if GRAPECITY
                    FileName = "http://www.grapecity.com/japan/activation/",
#else
                    FileName = "http://www.grapecity.com/componentone/",
#endif
                    UseShellExecute = true
                };
                Process.Start(psi);
            }
            catch
            {
            }
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        protected void GotoLink_Shop(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                var psi = new ProcessStartInfo()
                {
#if GRAPECITY
                    FileName = "http://shop.grapecity.com/",
#else
                    FileName = "http://www.grapecity.com/pricing/componentone/",
#endif
                    UseShellExecute = true
                };
                Process.Start(psi);
            }
            catch
            {
            }
        }
    }
}
