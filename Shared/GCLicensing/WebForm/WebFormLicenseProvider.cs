﻿//==============================================================================
//  File Name   :   WebFormLicenseProvider.cs
//
//  Copyright (C) 2008 GrapeCity Inc. All rights reserved.
//
//  Distributable under grapecity code license.
//  See terms of license at www.grapecity.com.
//
//==============================================================================

// <fileinformation>
//   <summary>
//     This file defines the WebFormLicenseProvider class.
//   </summary>
//   <author name="Carl Chen" mail="Carl.Chen@grapecity.com"/>
//   <seealso ref=""/>
//   <remarks>
//     Implementation of the WebFormLicenseProvider class.
//   </remarks>
// </fileinformation>

// <history>
//   <record date="20081226" author="Carl Chen" revision="1.00.000">
//     Create the file and implement the class.
//   </record>
// </history>

using System;
#if !WEBAPI &&  !ASPNETCORE
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
#endif
using System.Threading;
using System.Diagnostics;

namespace GrapeCity.Common
{
    /// <summary>
    ///   Represents a license provider of a Windows Forms PowerTools component.
    /// </summary>
    internal abstract class WebFormLicenseProvider<T> : GcNetFxLicenseProvider<T> where T : Product, new()
    {
        protected override bool VerifyDesignTimeEnvironment()
        {
#if WEBAPI || ASPNETCORE || SCAFFOLDER
            return false;
#else
            return HttpContext.Current == null;
#endif
        }
        public override void ShowLicenseDialog(T product, ActivationState state, ILicenseData<T> licenseData, object instance)
        {
            if (licenseData.IsRuntime)
            {
#if !WEB
                this.RenderLicenseOnWebPage(product, state, licenseData, instance);
#endif
            }
            else
            {
                this.ShowLicenseDialogForCurrentProcess(product, state, licenseData);
            }
        }

        private static EventWaitHandle _eventWaitHandle;

        private void ShowLicenseDialogForCurrentProcess(T product, ActivationState state, ILicenseData<T> licenseData)
        {
            //-- Make sure the dialog just be shown once in different appdomains
            if (_eventWaitHandle == null)
            {
                bool newEventCreated = false;
                EventWaitHandle eventWaitHandle = null;
                try
                {
                    string eventID = Process.GetCurrentProcess().Id + product.Guid.ToString();
                    eventWaitHandle = new EventWaitHandle(true,
                                                          EventResetMode.AutoReset,
                                                          eventID,
                                                          out newEventCreated);
                }
                catch
                {
                    return;
                }

                if (newEventCreated)
                {
#if WEB
                    Console.WriteLine("License invalid");

#else
                    //-- First time to show the license dialog
                    _eventWaitHandle = eventWaitHandle;

                    LicenseDialog<T> dialog = new LicenseDialog<T>(product, licenseData, this.ShowAboutBox);
                    dialog.ShowDialog();
#endif
                }
                else
                {
                    //-- The dialog has been shown
                    if (eventWaitHandle != null)
                        eventWaitHandle.Close();
                }
            }
        }
#if !WEB
        protected abstract void RenderLicenseOnWebPage(T product, ActivationState state, ILicenseData<T> licenseData, object instance);
#endif
#if !NO_ABOUTBOX
        protected abstract void ShowAboutBox();
#endif
    }
}
