//#define TESTCRYPTO
#define USE_REFLECTION
#if CLR45 || WPF
#define NET452
#endif

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

#if UWP
using Windows.UI.Xaml;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using System.Reflection;
#else
using System.Security.Cryptography;
#endif

namespace GrapeCity.Common
{
    internal abstract class Product
    {
        public abstract Guid Guid { get; }
        public abstract string Name { get; }
        public abstract Version Version { get; }
        public virtual bool AllowLicenseInLibrary
        {
            get { return false; }
        }
        public override bool Equals(object obj)
        {
            var product2 = obj as Product;
            if (product2 != null)
            {
                return this.Guid == product2.Guid;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return this.Guid.GetHashCode();
        }
    }
    internal interface ILicenseData<out T> where T : Product, new()
    {
        T Product { get; }
        ActivationState State { get; }
        string LicenseText { get; }
        bool Trial { get; }
        string Edition { get; }
        string Features { get; }
        string Domains { get; }
        string SerialKey { get; }
        string MachineName { get; }

        bool IsRuntime { get; }
        DateTime BaseDate { get; }

        int GetLeftDays();
        void VerifyLicense(out ActivationState state, out string licenseText);
    }
    internal class LicenseData<T> : ILicenseData<T>
        where T : Product, new()
    {
        private class LicenseDataInner
        {
            public Guid ProductCode;
            public string SerialKey;
            public string MachineName;
            public bool Unlimited;
            public int ActivationDate;
            public bool Trial;
            public int DesignTimeLeft;
            public int RunTimeLeft;
            public string Edition;
            public string Features;
            public string Domains;
        }

        public ActivationState State
        {
            get
            {
                ActivationState state;
                string dummy;
                this.VerifyLicense(out state, out dummy);
                return state;
            }
        }
        public string LicenseText
        {
            get
            {
                ActivationState dummy;
                string text;
                this.VerifyLicense(out dummy, out text);
                return text;
            }
        }

        public bool Trial { get { return this.InnerData.Trial; } }
        public string Edition { get { return this.InnerData == null ? string.Empty : this.InnerData.Edition; } }
        public string Features { get { return this.InnerData == null ? string.Empty : this.InnerData.Features; } }
        public string Domains { get { return this.InnerData == null ? string.Empty : this.InnerData.Domains; } }
        public string SerialKey { get { return this.InnerData == null ? string.Empty : this.InnerData.SerialKey; } }
        public string MachineName
        {
            get
            {
                var innerData = this.InnerData;
                if (innerData == null)
                {
                    return string.Empty;
                }
                if (this.InnerData.Unlimited)
                {
#if UWP
                    return null;
#else
                    return Environment.MachineName;
#endif
                }
                else
                {
                    return this.InnerData.MachineName;
                }
            }
        }

        public LicenseData(string native, bool isRuntime, DateTime baseDate)
        {
            this.NativeData = native;
            this.IsRuntime = isRuntime;
            this.BaseDate = baseDate;
        }

        public string NativeData { get; private set; }
        public bool IsRuntime { get; private set; }
        public DateTime BaseDate { get; private set; }
        private LicenseDataInner InnerData
        {
            get
            {
                LicenseDataInner inner = Parse(this.NativeData);
                if (inner == null)
                {
                    return null;
                }
                if (inner.ProductCode != this.Product.Guid)
                {
                    return null;
                }
                return inner;
            }
        }
        public T Product { get { return new T(); } }

        private static LicenseDataInner Parse(string licenseData)
        {
            if (string.IsNullOrEmpty(licenseData))
            {
                return null;
            }
            string[] contents = licenseData.Split(';');
            if (contents.Length != 2)
            {
                return null;
            }
            if (!Crypto.VerifySign(contents[0], contents[1]))
            {
                return null;
            }
            try
            {
                return ParseCore(contents[0]);
            }
            catch
            {
                return null;
            }
        }
        private static LicenseDataInner ParseCore(string licenseData)
        {
            if (string.IsNullOrEmpty(licenseData))
            {
                return null;
            }
            string[] contents = licenseData.Split(',');
            if (contents.Length != 11)
            {
                return null;
            }
            return new LicenseDataInner()
            {
                ProductCode = Guid.Parse(ConvertValue(contents[0])),
                SerialKey = ConvertValue(contents[1]),
                MachineName = ConvertValue(contents[2]),
                Unlimited = bool.Parse(ConvertValue(contents[3])),
                ActivationDate = int.Parse(ConvertValue(contents[4])),
                Trial = bool.Parse(ConvertValue(contents[5])),
                DesignTimeLeft = int.Parse(ConvertValue(contents[6])),
                RunTimeLeft = int.Parse(ConvertValue(contents[7])),
                Edition = ConvertValue(contents[8]),
                Features = ConvertValue(contents[9]),
                Domains = ConvertValue(contents[10]),
            };
        }
        private static string ConvertValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return Encoding.UTF8.GetString(Crypto.FromBase64(value));
        }

        private static int GetLeftDays(DateTime baseDate, int offsetDate)
        {
            if (baseDate > DateTime.Today.AddDays(1))
            {
                return -1;
            }

            if (offsetDate == 0)
            {
                return int.MaxValue;
            }
            else if (offsetDate > 0)
            {
                var date = ConvertDate(offsetDate);
                return (date - DateTime.Today).Days;
            }
            else // (runTimeDate < 0)
            {
                var date = baseDate + TimeSpan.FromDays(-offsetDate);
                return (date - DateTime.Today).Days;
            }
        }
        private static DateTime ConvertDate(int day)
        {
            return new DateTime(2000, 1, 1) + TimeSpan.FromDays(day);
        }
        public int GetLeftDays()
        {
            var inner = this.InnerData;
            if (inner == null)
            {
                return -1;
            }

            DateTime baseDate = this.IsRuntime ? this.GetRunTimeBaseDate(inner) : this.GetDesignTimeBaseDate(inner);
            return GetLeftDays(baseDate, this.IsRuntime ? inner.RunTimeLeft : inner.DesignTimeLeft);
        }
        private DateTime GetRunTimeBaseDate(LicenseDataInner innerData)
        {
            DateTime buildDate = this.BaseDate;
            DateTime lastDate = DateTime.MaxValue;
            if (!innerData.Unlimited)
            {
                if (innerData.DesignTimeLeft > 0)
                {
                    lastDate = ConvertDate(innerData.DesignTimeLeft);
                }
                else if (innerData.DesignTimeLeft < 0)
                {
                    var activationDate = ConvertDate(innerData.ActivationDate);
                    lastDate = activationDate + TimeSpan.FromDays(-innerData.DesignTimeLeft);
                }
            }
            return (lastDate > buildDate) ? buildDate : lastDate;
        }
        private DateTime GetDesignTimeBaseDate(LicenseDataInner innerData)
        {
            var activationDate = ConvertDate(innerData.ActivationDate);
            if (innerData.Unlimited)
            {
                if (this.BaseDate > activationDate && this.BaseDate.Year < 2050)
                {
                    return this.BaseDate;
                }
            }
            return activationDate;
        }

        public void VerifyLicense(out ActivationState state, out string licenseText)
        {
            Product p = new T();
            if (string.IsNullOrEmpty(this.NativeData))
            {
                state = ActivationState.NoLicense;
                licenseText = "No License";
                return;
            }
            var inner = this.InnerData;
            if (inner == null || inner.ProductCode != p.Guid)
            {
                state = ActivationState.InvalidLicense;
                licenseText = "Invalid License";
                return;
            }

            if (inner.ProductCode != p.Guid)
            {
                state = ActivationState.InvalidLicense;
                licenseText = "Invalid License";
            }
#if !UWP
            if (!this.IsRuntime && !inner.Unlimited)
            {
                if (inner.MachineName != Environment.MachineName)
                {
                    state = ActivationState.InvalidLicense;
                    licenseText = "Invalid License";
                }
            }
#endif
            int leftDays = this.GetLeftDays();
            var expired = leftDays < 0;

            string leftDaysText = leftDays == int.MaxValue ? "" : string.Format(" ({0} days left)", leftDays);
            if (inner.Trial)
            {
                state = expired ? ActivationState.TrialExpired : ActivationState.TrialActivated;
                licenseText = expired ? "Trial License, Expired" : ("Trial License, Activated" + leftDaysText);
            }
            else
            {
                state = expired ? ActivationState.ProductExpired : ActivationState.ProductActivated;
                licenseText = expired ? "Product License, Expired" : ("Product License, Activated" + leftDaysText);
            }
        }

    }
    internal static class Crypto
    {
        private static RSAParameters PublicKey
        {
            get
            {
                return new RSAParameters()
                {
#if TESTCRYPTO
                    Modulus = FromBase64("tgoYDy+InG+V+F4gU9ssbjuTHXzHaXwFzyF+SA85fe4AeN1N5jzxA2MzXT8VsArKZ9Ugz2rYPp9kOhpwiSq2QSfxE+axl8403O9JkcB9826e7Co3WjZYOMbfKWLrRJFTWatkEIRJvP2ocOEtYCLDOaET08OCAnSFAcO7fReSd00="),
#else
                    Modulus = FromBase64("+US+f7+816LsJEa/Gd0daNSWKehDM7uF72OpCHfSECbjX9WlrrOxIq8kOgIVxkDfJeWmP6OwZw9Xn+VJa8Sxze7jKUopRH1awfK1p+RiQnOcmqUpi4GTUHK+6F9nSc/Y0T5H7pgDNSk8CkB/LwfaaCj0FmrEXns8fguG2l3VrCU="),
#endif
                    Exponent = FromBase64("AQAB"),
                };
            }
        }

#if UWP
        class RSAParameters
        {
            public byte[] Modulus;
            public byte[] Exponent;
        }
        static byte[] GetBCryptBlobBytes(RSAParameters rsaparams)
        {
            List<byte> rsaPublicKeyBlob = new List<byte>(BitConverter.GetBytes(0x31415352));    // Magic
            rsaPublicKeyBlob.AddRange(BitConverter.GetBytes(rsaparams.Modulus.Length * 8));     // bitlength
            rsaPublicKeyBlob.AddRange(BitConverter.GetBytes(rsaparams.Exponent.Length));        // exponent byte len
            rsaPublicKeyBlob.AddRange(BitConverter.GetBytes(rsaparams.Modulus.Length));         // modulus byte len
            rsaPublicKeyBlob.AddRange(BitConverter.GetBytes((UInt64)0));                        // primaries (0) for public
            rsaPublicKeyBlob.AddRange(rsaparams.Exponent);                                      // exponent
            rsaPublicKeyBlob.AddRange(rsaparams.Modulus);                                       // modulus
            return rsaPublicKeyBlob.ToArray();
        }
        internal static bool VerifySign(string data, string sign)
        {
            byte[] rsaPublicKeyBlobBytes = GetBCryptBlobBytes(PublicKey);
            var provider = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithmNames.RsaSignPkcs1Sha256);
            var publicKey = provider.ImportPublicKey(CryptographicBuffer.CreateFromByteArray(rsaPublicKeyBlobBytes),
                CryptographicPublicKeyBlobType.BCryptPublicKey);

            var bufData = CryptographicBuffer.CreateFromByteArray(Encoding.UTF8.GetBytes(data));
            var bufSign = CryptographicBuffer.CreateFromByteArray(FromBase64(sign));
            var hash = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256).HashData(bufData);

            return CryptographicEngine.VerifySignatureWithHashInput(publicKey, hash, bufSign);
        }
#else

        public static bool VerifySign(string data, string sign)
        {
#if !NET452
            // For .NET Core, .NET 4.6 or later 
            using (RSA rsa = RSA.Create())
            {
                rsa.ImportParameters(PublicKey);
                return rsa.VerifyData(Encoding.UTF8.GetBytes(data), FromBase64(sign), HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            }
#else
            // If your target framework is less than .NET 4.6, using following code instead. (Windows Only)
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(PublicKey);
                return rsa.VerifyData(Encoding.UTF8.GetBytes(data), "SHA256", FromBase64(sign));
            }
#endif
        }
#endif

        public static string ToBase64(byte[] bytes)
        {
            string result = Convert.ToBase64String(bytes);
            return result.TrimEnd('=');
        }
        public static byte[] FromBase64(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return new byte[0];
            }
            if (data.Length % 4 != 0)
            {
                var mod = 4 - data.Length % 4;
                data = data + new string('=', mod);
            }
            return Convert.FromBase64String(data);
        }
    }

    internal enum ActivationState
    {
        NoLicense,
        InvalidLicense,
        ProductActivated,
        ProductExpired,
        TrialActivated,
        TrialExpired,
    }

    internal static class GcLicenseManager<T> where T: Product, new()
    {
#if !UWP
        private class ProductType: System.Reflection.TypeDelegator
        {
            public ProductType():
                base(typeof(T))
            { }
            public override string AssemblyQualifiedName
            {
                get
                {
                    return new T().Guid.ToString();
                }
            }
        }
        [ThreadStatic]
        private static LicenseData<T> _designTimeLicense;
#endif

        [ThreadStatic]
        private static LicenseData<T> _runTimeLicense;

#if !UWP
        [System.Reflection.Obfuscation(Exclude=true)]
        public static LicenseData<T> DesignTimeLicense
        {
            get
            {
                if (_designTimeLicense == null)
                {
                    _designTimeLicense = GetDesignTimeLicenseNative(new T());
                }
                return _designTimeLicense;
            }
        }
#endif
        [System.Reflection.Obfuscation(Exclude = true)]
        public static LicenseData<T> RunTimeLicense
        {
            get
            {
                if (_runTimeLicense == null)
                {
                    LicenseData<T> license = null;
#if !UWP
                    try
                    {
                        license = ReadLicenseContext(LicenseManager.CurrentContext);
                    }
                    catch // May fail if no permissions, e.g. Asp.NET with Medium Trust.
                    {}
#endif
                    if (license == null)
                    {
                        license = GetRunTimeLicenseNative(new T());
                    }
                    if (license == null)
                    {
                        license = new LicenseData<T>(null, true, DateTime.MinValue);
                    }
                    _runTimeLicense = license;
                }
                return _runTimeLicense;
            }
        }

#if !UWP
        private static LicenseData<T> GetDesignTimeLicenseNative(T product)
        {
            string licenseData = null;
            string productData = null;

            bool isWindows = true;
            int p = (int)Environment.OSVersion.Platform;
            if ((p == 4 /*Unix*/) || (p == 6 /*MaxOS X*/) || (p == 128 /*Mono*/))
            {
                isWindows = false;
            }
            var sysFolder = isWindows ? Environment.SpecialFolder.CommonApplicationData : Environment.SpecialFolder.LocalApplicationData;
            string root = Environment.GetFolderPath(sysFolder);
            string licenseFile = Path.Combine(root, "GrapeCity", product.Guid.ToString(), ".license");
            if (File.Exists(licenseFile))
            {
                licenseData = File.ReadAllText(licenseFile);
                if (!string.IsNullOrEmpty(licenseData))
                {
                    licenseData = licenseData.Trim();
                }
            }

            string dataFile = Path.Combine(root, "GrapeCity", product.Guid.ToString(), ".data");
            if (File.Exists(dataFile))
            {
                productData = File.ReadAllText(dataFile);
                if (!string.IsNullOrEmpty(productData))
                {
                    productData = productData.Trim();
                }
            }
            DateTime installDate = ParseProductData(product.Guid, productData);
            return new LicenseData<T>(licenseData, false, installDate);
        }
#endif
        private static DateTime ParseProductData(Guid guid, string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return DateTime.MinValue;
            }
            DateTime date = DateTime.MinValue;
            try
            {
                Guid guid1 = guid;
                Guid guid2 = Guid.Parse(data);
                byte[] bytes1 = guid1.ToByteArray();
                byte[] bytes2 = guid2.ToByteArray();
                long value = 0;
                for (int i = 0; i < 8; i++)
                {
                    long n = 0xff;
                    if ((bytes1[i * 2] ^ bytes2[i * 2]) == (bytes1[i * 2 + 1] ^ bytes2[i * 2 + 1]))
                    {
                        n = (byte)(bytes1[i * 2] ^ bytes2[i * 2]);
                    }
                    value |= (n & 0xff) << (8 * i);
                }
                date = DateTime.FromBinary(value);
            }
            catch
            { }
            return date;
        }

#if !UWP
        [System.Reflection.Obfuscation(Exclude = true)]
        public static LicenseContext WriteLicenseContextProp
        {
            // a set only property avoids obfuscation issues associated with the method.
            set { WriteLicenseContext(value); }
        }

        public static void WriteLicenseContext(LicenseContext context)
        {
            var nativeLicense = DesignTimeLicense.NativeData;
            if (string.IsNullOrEmpty(nativeLicense))
            {
                return;
            }

            List<string> line = new List<string>();
            line.Add(DateTime.Today.ToBinary().ToString());
            line.Add(nativeLicense);
            var data = string.Join(";", line);
            context.SetSavedLicenseKey(new ProductType(), data);
        }
        private static LicenseData<T> ReadLicenseContext(LicenseContext context)
        {
            string data = context.GetSavedLicenseKey(new ProductType(), null);
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }
            var elements = data.Split(';');
            if (elements.Length != 3)
            {
                return null;
            }
            var buildDate = DateTime.FromBinary(long.Parse(elements[0]));
            var licenseData = elements[1] + ';' + elements[2];
            return new LicenseData<T>(licenseData, true, buildDate);
        }
#endif
#if UWP
        private static LicenseData<T> GetRunTimeLicenseNative(T product)
        {
            var entry = Application.Current.GetType().GetTypeInfo().Assembly;
            if (entry != null)
            {
                var result = GetRunTimeLicenseNativeCore(product, entry, null);
                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }
#else
        private static LicenseData<T> GetRunTimeLicenseNative(T product)
        {
            var entry = System.Reflection.Assembly.GetEntryAssembly();
            if (entry != null)
            {
                var result = GetRunTimeLicenseNativeCore(product, entry, null);
                if (result != null)
                {
                    return result;
                }
                result = GetRunTimeLicenseNativeFromDomain(product, entry.GetName().Name + ".");
                if (result != null)
                {
                    return result;
                }

                result = GetRunTimeLicenseNativeFromDomain(product, null);
                if (result != null)
                {
                    if (product.AllowLicenseInLibrary)
                    {
                        return result;
                    }
                    var features = result.Features;
                    if (!string.IsNullOrEmpty(features) && features.Contains("[plugin-lic]"))
                    {
                        return result;
                    }
                }
                return null;
            }
            else
            {
                return GetRunTimeLicenseNativeFromDomain(product, null);
            }
        }

        private static LicenseData<T> GetRunTimeLicenseNativeFromDomain(T product, string targetPrefix)
        {
            var targets = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var t in targets)
            {
                var result = GetRunTimeLicenseNativeCore(product, t, targetPrefix);
                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }
#endif
        private static LicenseData<T> GetRunTimeLicenseNativeCore(T product, System.Reflection.Assembly target, string targetPrefix)
        {
            LicenseData<T> result = null;
            if (target.IsDynamic)
            {
                return result;
            }

            var streamNames = target.GetManifestResourceNames();
            foreach (var name in streamNames)
            {
                if (!name.EndsWith(".gclicx", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }
                try
                {
                    var stream = target.GetManifestResourceStream(name);
                    result = GetRunTimeLicenseNativeFromStream(product, target, targetPrefix, stream);
                }
                catch
                { }
                if (result != null)
                {
                    break;
                }
            }

            return result;
        }

        private static LicenseData<T> GetRunTimeLicenseNativeFromStream(T product, System.Reflection.Assembly target, string targetPrefix, Stream stream)
        {
            List<string> contents = new List<string>();
            using (StreamReader reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    contents.Add(reader.ReadLine());
                }
            }

            string guid = product.Guid.ToString();
            string licenseLine = null;
            foreach (var line in contents)
            {
                if (!string.IsNullOrEmpty(line) && line.StartsWith(guid, StringComparison.OrdinalIgnoreCase))
                {
                    licenseLine = line;
                    break;
                }
            }
            if (!string.IsNullOrEmpty(licenseLine))
            {
                return ParseNativeLicenseData(target, targetPrefix, licenseLine);
            }
            return null;
        }

        private static LicenseData<T> ParseNativeLicenseData(System.Reflection.Assembly target, string targetPrefix, string licenseLine)
        {
            var elements = licenseLine.Split(';');
            if (elements.Length != 5)
            {
                return null;
            }
            var buildDate = DateTime.FromBinary(long.Parse(elements[1]));
            var targetName = Encoding.UTF8.GetString(Convert.FromBase64String(elements[2]));
            var licenseData = elements[3] + ';' + elements[4];

            if (!string.IsNullOrEmpty(targetPrefix))
            {
                if (targetName.StartsWith(targetPrefix, StringComparison.OrdinalIgnoreCase))
                {
                    targetName = targetName.Substring(targetPrefix.Length);
                }
                else
                {
                    return null;
                }
            }
#if !UWP && !WEB
            bool matched = false;
            foreach (var m in target.GetLoadedModules())
            {
                if (m.Name == targetName)
                {
                    matched = true;
                    break;
                }
            }
            if (!matched)
            {
                return null;
            }
#endif
            return new LicenseData<T>(licenseData, true, buildDate);
        }
    }
}
