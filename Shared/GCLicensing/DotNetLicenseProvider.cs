﻿#if !ASPNETCORE
using GrapeCity.Common.Resources;
#endif
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace GrapeCity.Common
{
    internal class GcNetFxLicense : License
    {
        private string licenseKey;
        public GcNetFxLicense(string licenseKey)
        {
            this.licenseKey = licenseKey;
        }
        public override string LicenseKey => licenseKey;

        public override void Dispose()
        { }
    }

    internal class GcNetFxLicenseProvider<T> : LicenseProvider where T: Product, new ()
    {
        public sealed override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
        {
            ILicenseData<T> licenseData = null;

            try
            {
                licenseData = this.GetLicenseCore(context, type, instance);
            }
            catch (Exception e)
            {
                if (allowExceptions)
                {
                    if (e is IOException)
                    {
                        throw new IOException();
                    }
                    else if (e is UnauthorizedAccessException)
                    {
                        throw new UnauthorizedAccessException();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            if (licenseData == null)
            {
                if (allowExceptions)
                {
                    throw new LicenseException(type);
                }
                return null;
            }

            var product = licenseData.Product;
#if WEB
            ActivationState state;
            string licenseText;
            licenseData.VerifyLicense(out state, out licenseText);
#else
            licenseData.VerifyLicense(out ActivationState state, out string licenseText);
#endif
            // Save license to assembly resource.
            if (context.UsageMode == LicenseUsageMode.Designtime && (state == ActivationState.ProductActivated || state == ActivationState.TrialActivated))
            {
                this.WriteLicenseContext(context);
            }

            if (allowExceptions)
            {
                if (this.ShouldShowLicenseDialog(product, state, licenseData, instance))
                {
                    this.ShowLicenseDialog(product, state, licenseData, instance);
                }
                if (licenseData.IsRuntime && this.ShouldEndApplication(product, state, licenseData, instance))
                {
                    this.EndApplication(product, state, licenseData, instance);
                }
            }
            return new GcNetFxLicense(licenseText);
        }
        protected virtual ILicenseData<T> GetLicenseCore(LicenseContext context, Type type, object instance)
        {
            if (context.UsageMode == LicenseUsageMode.Designtime)
            {
                return this.GetDesignTimeLicenseCore();
            }
            else
            {
                var licenseData = this.GetRuntimeLicenseCore();
                //   In DesignTime, because of IDE bugs, some component are not created with DesigntimeLicenseContext. 
                //   For example, when drag a control from Win Forms DataSource tool window, or use a web control in a ASP.NET Template.
                //   This may cause the control do run-time license validation and found that there is no license.
                //   To resolve this problem, we will check the environment and decide whether to do design-time license validation again.
                if (licenseData == null || licenseData.State == ActivationState.NoLicense)
                {
                    try
                    {
                        new PermissionSet(PermissionState.Unrestricted).Demand(); // Designtime has full permission.
                        if (this.VerifyDesignTimeEnvironment())
                        {
                            return this.GetDesignTimeLicenseCore();
                        }
                    }
                    catch
                    {
                    }
                }
                return licenseData;
            }
        }

        protected virtual ILicenseData<T> GetDesignTimeLicenseCore()
        {
            return GcLicenseManager<T>.DesignTimeLicense;
        }

        protected virtual ILicenseData<T> GetRuntimeLicenseCore()
        {
            return GcLicenseManager<T>.RunTimeLicense;
        }

        protected virtual void WriteLicenseContext(LicenseContext context)
        {
            GcLicenseManager<T>.WriteLicenseContext(context);
        }

        public virtual void ShowLicenseDialog(T product, ActivationState state, ILicenseData<T> licenseData, object instance)
        {
            if (_showLicenseHelper == null)
            {
                _showLicenseHelper = new ShowLicenseHelper(this, product, state, licenseData, instance);
            }
        }
        private static ShowLicenseHelper _showLicenseHelper;
        private class ShowLicenseHelper
        {
            public ShowLicenseHelper(GcNetFxLicenseProvider<T> provider, T product, ActivationState state, ILicenseData<T> licenseData, object instance)
            {
                provider.ShowLicenseDialogCore(product, state, licenseData, instance);
            }
        }
        protected virtual bool ShouldShowLicenseDialog(T product, ActivationState state, ILicenseData<T> licenseData, object instance)
        {
            return state != ActivationState.ProductActivated;
        }
        protected virtual void ShowLicenseDialogCore(T product, ActivationState state, ILicenseData<T> licenseData, object instance)
        {
            // Show Dialog/MessageBox with the result of BuildDialogContents.
        }
#if !ASPNETCORE
        protected Tuple<string, LicenseDialogKind, string> BuildDialogContents(T product, ActivationState state, ILicenseData<T> licenseData)
        {
            var isRuntime = licenseData.IsRuntime;
            var leftDays = licenseData.GetLeftDays();

            string caption = LicenseResource.GetDialogTitle();
            LicenseDialogKind dialogKind = LicenseResource.GetDialogKind(state);

            string headerText = LicenseResource.GetDialogHeaderText(product, isRuntime, state, leftDays);
            LicenseDialogWebLinkTarget linkTarget;
            string bodyText = LicenseResource.GetDialogBodyText(product, isRuntime, state, leftDays, out linkTarget);
            string detailHeader = LicenseResource.GetDialogDetailHeaderText();
            string detailText = LicenseResource.GetDialogDetailInfo(product, isRuntime, state, licenseData.BaseDate, licenseData.MachineName, licenseData.SerialKey);
            string contents = string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "{0}{4}{4}{1}{4}{4}{2}{4}--------------------------------------------------------------{4}{3}",
                headerText, bodyText, detailHeader, detailText, Environment.NewLine);

            return new Tuple<string, LicenseDialogKind, string>(caption, dialogKind, contents);
        }

#endif
        protected virtual bool ShouldEndApplication(T product, ActivationState state, ILicenseData<T> licenseData, object instance)
        {
            return state != ActivationState.ProductActivated && state != ActivationState.TrialActivated;
        }
        protected virtual void EndApplication(T product, ActivationState state, ILicenseData<T> licenseData, object instance)
        {
            Environment.Exit(0);
        }

        /// <summary>
        ///   Verify the environment to get whether the component is in design-time. 
        /// </summary>
        /// <returns>
        ///   <c>true</c> if it is in design-time; otherwise <c>false</c>.
        /// </returns>
        /// <remarks>
        ///   In DesignTime, because of IDE bugs, some component are not created with DesigntimeLicenseContext. 
        ///   For example, when drag a control from Win Forms DataSource tool window, or use a web control in a ASP.NET Template.
        ///   This may cause the control do run-time license validation and found that there is no license.
        ///   To resolve this problem, we will check the environment and decide whether to do design-time license validation again.
        /// </remarks>
        protected virtual bool VerifyDesignTimeEnvironment()
        {
#if !CORE31&& !ASPNETCORE
            AppDomainManager domainManager = AppDomain.CurrentDomain.DomainManager;
            if (domainManager != null && string.CompareOrdinal(domainManager.GetType().Name, "VsAppDomainManager") == 0)
            {
                return true;
            }
#endif
            var entryAssembly = System.Reflection.Assembly.GetEntryAssembly();
            if (entryAssembly != null && entryAssembly.FullName.StartsWith("UserControlTestContainer", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        internal static void RunActivateTool()
        {
            var gclmPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "GrapeCity", "gclm", "gclm.exe");
            if (File.Exists(gclmPath))
            {
                var psi = new ProcessStartInfo()
                {
                    FileName = gclmPath,
                    UseShellExecute = true
                };
                Process.Start(psi);
            }
        }
    }
}
