﻿//#define TESTCRYPTO
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using C1.Util.Licensing;

namespace GrapeCity.Common
{
    internal class C1Product : Product
    {
        public override Guid Guid => Guid.Empty;
        public override string Name => string.Empty;
        public override Version Version => null;
        public virtual string C1Code => null;
    }
#if TESTCRYPTO
    /****************************************************************************************
     * Following C1Product classes are for Test GCLM Testing 
     ****************************************************************************************/
    internal class C1WPFProduct : C1Product
    {
        public override Guid Guid => new Guid("3a18e60c-6e2b-413a-883f-d59f42b63929");
        public override string Name => "Component One for WPF";
        public override string C1Code => "S6";
    }
    internal class C1WinformProduct : C1Product
    {
        public override Guid Guid => new Guid("1b8597c5-0f4f-4cbb-bc34-d457a564eae8");
        public override string Name => "ComponentOne for Windows Forms";
        public override string C1Code => "S8";
    }
    internal class C1DesktopProduct : C1Product
    {
        public override Guid Guid => new Guid("76978edf-c4a2-4ab1-9baa-b90c79b62d44");
        public override string Name => "ComponentOne for Desktop";
        public override string C1Code => "SD";
    }
    internal class C1SEProduct : C1Product
    {
        public override Guid Guid => new Guid("338e8154-a14e-42ba-a433-7f4339af93f7");
        public override string Name => "ComponentOne Studio Enterprise";
        public override string C1Code => "SE,SU";
    }
    internal class C1UltimateProduct : C1Product
    {
        public override Guid Guid => new Guid("6db7542a-a9b8-477e-8651-3ddda4d3aacd");
        public override string Name => "ComponentOne Ultimate";
        public override string C1Code => "SU,SE";
    }
#else
    /****************************************************************************************
     * Following C1Product classes are for Production Licensing 
     ****************************************************************************************/
    // if GRAPECITY is defined, then only JP licenses are accepted.
    // if !GRAPECITY, then both JP and non-JP licenses are accepted.

#if !GRAPECITY
    internal class C1WPFProduct : C1Product
    {
        public override Guid Guid => new Guid("c02c28b7-1c24-4109-8eb3-f99c3905f3f1");
        public override string Name => "ComponentOne WPF Edition";
        public override string C1Code => "S6";
    }
    internal class C1WinformProduct : C1Product
    {
        public override Guid Guid => new Guid("da3d5d14-691f-4908-aa3c-fd3239734232");
        public override string Name => "ComponentOne WinForms Edition";
        public override string C1Code => "S8";
    }

    internal class C1SEProduct : C1Product
    {
        public override Guid Guid => new Guid("331cf6cd-b73c-429f-ba79-fa2f85eebd68");
        public override string Name => "ComponentOne Studio Enterprise";
        public override string C1Code => "SE,SU";
    }

    internal class C1IOSProduct : C1Product
    {
        public override Guid Guid => new Guid("c2e333e5-6dae-4aaf-8903-fa1dc779d32a");
        public override string Name => "ComponentOne Xamarin.iOS Edition";
        public override string C1Code => "XE";
    }

    internal class C1AndroidProduct : C1Product
    {
        public override Guid Guid => new Guid("18dbecd7-edc0-49d0-95d9-bdb9e5e4827f");
        public override string Name => "ComponentOne Xamarin.Android Edition";
        public override string C1Code => "XE";
    }

    internal class C1XamarinProduct : C1Product
    {
        public override Guid Guid => new Guid("de2b5824-e24d-4e7f-86d1-a87c1729993c");
        public override string Name => "ComponentOne Studio for Xamarin";
        public override string C1Code => "XE";
    }

    internal class C1BlazorProduct : C1Product
    {
        public override Guid Guid => new Guid("6631ee67-fec7-45b0-a771-4ec75cd748e3");
        public override string Name => "ComponentOne Blazor Edition";
        public override string C1Code => "XE";
    }

    internal class C1UWPProduct : C1Product
    {
        public override Guid Guid => new Guid("9afa522c-ea0b-47fe-ae14-7d9225612767");
        public override string Name => "ComponentOne UWP Edition";
        public override string C1Code => "AJ";
    }

    internal class C1AspNetProduct : C1Product
    {
        public override Guid Guid => new Guid("839e1737-f256-46ea-b391-50da451c13a4");
        public override string Name => "ComponentOne ASP.NET MVC Edition";
        public override string C1Code => "AH";
    }
#endif
    // ---------------------------------------------------------------------------------------------------
    internal class C1WPFProductJP : C1Product
    {
        public override Guid Guid => new Guid("4327EAF8-AA02-40A6-B9F6-3D007C039055");
        public override string Name => "ComponentOne WPF Edition JP";
        public override string C1Code => "S6";
    }
    internal class C1WinformProductJP : C1Product
    {
        public override Guid Guid => new Guid("1E2DD705-CD7C-42CE-8098-C4717DF794B1");
        public override string Name => "ComponentOne WinForms Edition JP";
        public override string C1Code => "S8";
    }

    internal class C1SEProductJP : C1Product
    {
        public override Guid Guid => new Guid("154B86E3-6B5B-4B2E-ACDC-91D24D249879");
        public override string Name => "ComponentOne Studio Enterprise JP";
        public override string C1Code => "SE,SU";
    }

    internal class C1IOSProductJP : C1Product
    {
        public override Guid Guid => new Guid("C617BC79-C041-4111-B472-AD2C5A5AD5F2");
        public override string Name => "ComponentOne Xamarin.iOS Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1AndroidProductJP : C1Product
    {
        public override Guid Guid => new Guid("326784EA-3999-4598-9A3A-BD36CA1142FE");
        public override string Name => "ComponentOne Xamarin.Android Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1XamarinProductJP : C1Product
    {
        public override Guid Guid => new Guid("EB58417C-27BB-4F62-ACFA-4D36582D2D0C");
        public override string Name => "ComponentOne Studio for Xamarin JP";
        public override string C1Code => "XE";
    }

    internal class C1BlazorProductJP : C1Product
    {
        public override Guid Guid => new Guid("24D9C311-5D6C-4BA1-81E7-7F86B9495596");
        public override string Name => "ComponentOne Blazor Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1UWPProductJP : C1Product
    {
        public override Guid Guid => new Guid("EAAD5427-BBD9-4D82-801F-6F37FB4AF9FE");
        public override string Name => "ComponentOne UWP Edition JP";
        public override string C1Code => "AJ";
    }

    internal class C1AspNetProductJP : C1Product
    {
        public override Guid Guid => new Guid("B78FAC6C-CC85-4A78-A08F-FF3556EDFA97");
        public override string Name => "ComponentOne ASP.NET MVC Edition JP";
        public override string C1Code => "AH";
    }

#endif

    //   Ultimate = "SU"
    //   Enterprise = "SE";
    //   WinForms = "S8";
    //   WPF = "S6";
    //   Desktop = "SD";
    //   UWP = "AJ";
    //   ActiveX = "S7";
    //   ASP.Net = "AH";
    //   Xamarin = "XE";

    internal sealed class C1GcActivationTool
    {
        private List<ProductLicense> productLicenses = null;
        private ILicenseData<C1Product>[] gcDesignLicenseData = null;
        private List<string> validSerials = null;

#if GRAPECITY
        private bool subProducts = false;
#else
        private bool subProducts = true;
#endif
        private MemberInfo[] GetLicenseManagerMemberInfo(string memberName)
        {
            List<Type> prodTypes = null;
            if (prodTypes == null)
            {
                Assembly asm = null;
                asm = Assembly.GetExecutingAssembly();
                prodTypes = new List<Type>(asm.GetTypes().Where(p => (p.IsSubclassOf(typeof(C1Product)))));
            }

            Type tylm = typeof(GcLicenseManager<>);
            MemberInfo[] mis = new MemberInfo[prodTypes.Count];

            for (int pt = 0; pt < prodTypes.Count; pt++)
            {
                Type ctylm = tylm.MakeGenericType(prodTypes[pt]);
                MemberInfo[] mi = ctylm.GetMember(memberName, BindingFlags.Static | BindingFlags.Public);
                mis[pt] = mi[0];
            }
            return mis;
        }
        private void SetValidGCLicenseData()
        {
            if (gcDesignLicenseData == null)
            {
                List<ILicenseData<C1Product>> gcLicData = new List<ILicenseData<C1Product>>();

                var mis = GetLicenseManagerMemberInfo("DesignTimeLicense");
                foreach (PropertyInfo pi in mis)
                {
                    var lic = pi.GetValue(null) as ILicenseData<C1Product>;
                    string ed = lic.Edition;
                    if (lic.State == ActivationState.ProductActivated && !string.IsNullOrEmpty(ed) && ed.StartsWith("v"))
                    {
                        gcLicData.Add(lic);
                    }
                }

                gcDesignLicenseData = gcLicData.ToArray();
            }
        }
        private void GetValidGCSerials()
        {
            if (validSerials == null)
            {
                validSerials = new List<string>();

                SetValidGCLicenseData();
                foreach (var lic in gcDesignLicenseData)
                {
                    int year = 0, ver = 0;
                    string ed = lic.Edition;
                    if (int.TryParse(ed.Substring(1, 4), out year) && int.TryParse(ed.Substring(6, 1), out ver))
                    {
                        if (year > 2020 || (year == 2020 && ver >= 2))
                        {
                            validSerials.Add(lic.SerialKey.Substring(0, 6));
                        }
                    }
                }
            }
        }
        private bool canUpdateLicense(ProductLicense pl)
        {
            bool update = false;
            if (pl.IsInstalled)
            {
                var oldpl = ProductLicense.GetInstalledLicense(pl.ProductGuid);
                if (oldpl.OrderCode != "GC")
                    update = false;
                else
                    update = pl.Year > oldpl.Year || (pl.Year == oldpl.Year && pl.Quarter > oldpl.Quarter);
            }
            else
                update = true;

            return update;
        }

        internal bool InstallNewLicenses()
        {
            bool result = true;
            if (productLicenses == null)
                GetValidC1ProductSerialInfo();

            foreach(ProductLicense pl in productLicenses)
            {
                if(canUpdateLicense(pl))
                {
                    if(pl.IsInstalled)
                    {
                        var oldpl = ProductLicense.GetInstalledLicense(pl.ProductGuid);
                        oldpl.Uninstall(subProducts);
                    }
                    pl.Install(subProducts);
                }
            }
            return result;
        }
        internal void RemoveInvalidProductLicenses()
        {
            GetValidGCSerials();
            string[] installedKeys = C1LicenseActivation.QueryKeys.GetInstalledKeys();
            if (installedKeys.Length > 0)
            {
                foreach (string key in installedKeys)
                {
                    if (key.Substring(5, 2) == "GC" && !validSerials.Contains(key.Substring(11, 6)))
                    {
                        var pi = ProductInformation.GetProductByCode(key.Substring(0, 2), false);
                        if (pi != null)
                        {
                            var pl = ProductLicense.GetInstalledLicense(pi.Guid);
                            if (pl != null) pl.Uninstall(subProducts);
                        }
                    }
                }
            }
        }
        internal string GetValidC1ProductSerialInfo()
        {
            string result = string.Empty;

            if (productLicenses == null)
            {
                Dictionary<string, string> serialInfo = new Dictionary<string, string>();

                SetValidGCLicenseData();
                foreach (var lic in gcDesignLicenseData)
                {
                    int year = 0, ver = 0;
                    string ed = lic.Edition;
                    if (int.TryParse(ed.Substring(1, 4), out year) && int.TryParse(ed.Substring(6, 1), out ver))
                    {
                        bool isJP = lic.Product.Name.IndexOf("JP", StringComparison.OrdinalIgnoreCase) > -1;
                        string val = (year % 100).ToString() + "," + ver.ToString() + "," +
                                     lic.SerialKey.Substring(0, 6) + "," + lic.SerialKey.Substring(0, 8) +
                                     (isJP ? ",JP0" : ",066");

                        string[] codes = lic.Product.C1Code.Split(',');
                        foreach (string code in codes)
                        {
                            string cval = code + "," + val;
                            if (serialInfo.ContainsKey(code))
                            {
                                if (serialInfo[code].CompareTo(cval) < 0)
                                    serialInfo[code] = cval;
                            }
                            else
                            {
                                serialInfo.Add(code, cval);
                            }
                        }
                    }
                }

                productLicenses = new List<ProductLicense>();

                foreach (string s in serialInfo.Values)
                {
                    string[] snp = s.Split(',');

                    var prodInfo = ProductInformation.GetProductByCode(snp[0], false);
                    if (prodInfo != null)
                    {
                        ProductLicense pl = new ProductLicense()
                        {
                            ProductCode = snp[0],
                            Year = int.Parse(snp[1]),
                            Quarter = int.Parse(snp[2]),
                            Serial = int.Parse(snp[3]),
                            VendorCode = snp[5],
                            OrderCode = "GC",
                            UserName = Environment.UserName,
                            CompanyName = snp[4],
                            ProductGuid = prodInfo.Guid,
                        };

                        productLicenses.Add(pl);
                    }
                }
            }

            if(productLicenses != null && productLicenses.Count > 0)
            {
                foreach (var pl in productLicenses)
                {
                    if (canUpdateLicense(pl))
                    {
                        if (result.Length > 0)
                            result += ";";
                        result += pl.Key;
                    }
                }
            }

            return result;
        }
        static internal void ManageGCLicenses()
        {
            var gcTool = new C1GcActivationTool();
            gcTool.RemoveInvalidProductLicenses();
            gcTool.InstallNewLicenses();
        }
        static internal bool CheckForValidGcSerial(string value)
        {
            var gcTool = new C1GcActivationTool();
            gcTool.RemoveInvalidProductLicenses();
            if (!string.IsNullOrEmpty(value))
            {
                return gcTool.validSerials.Contains(value);
            }
            return false;
        }
    }
}
