﻿using System;
using System.Globalization;
using System.Resources;
using System.Reflection;
using System.Text;
using System.ComponentModel;
using System.Security;
using System.Diagnostics;
using System.Collections.Generic;

namespace GrapeCity.Common.Resources
{
    /// <summary>
    ///   Contains the resource for PowerToolsLicenseProvider
    /// </summary>
    internal static class LicenseResource
    {
        #region Setup
        private static CultureInfo resourceCulture;
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        internal static ResourceManager ResourceManager
        {
            get
            {
                if (resourceMan == null)
                {
                    resourceMan = new ResourceManager(typeof(LicenseResource));
                }
                return resourceMan;
            }
            set
            {
                if (resourceMan == null)
                {
                    resourceMan = value;
                }
            }
        }
        
        private static ResourceManager resourceMan;
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }
        #endregion

        #region Icons
        /// <summary>
        ///   Gets the Error icon for license dialog.
        /// </summary>
        internal static System.IO.MemoryStream ErrorIcon 
        {
            get 
            {
                object obj = ResourceManager.GetObject("error", resourceCulture);
                return ((System.IO.MemoryStream)(obj));
            }
        }
        /// <summary>
        ///   Gets the Info icon for license dialog.
        /// </summary>
        internal static System.IO.MemoryStream InfoIcon
        {
            get 
            {
                object obj = ResourceManager.GetObject("info", resourceCulture);
                return ((System.IO.MemoryStream)(obj));
            }
        }
        /// <summary>
        ///   Gets the Warning icon for license dialog.
        /// </summary>
        internal static System.IO.MemoryStream WarningIcon
        {
            get 
            {
                object obj = ResourceManager.GetObject("warning", resourceCulture);
                return ((System.IO.MemoryStream)(obj));
            }
        }
        /// <summary>
        ///   Gets the GrapeCity icon for license dialog.
        /// </summary>
        internal static System.IO.MemoryStream GrapeCityIcon
        {
            get
            {
                object obj = ResourceManager.GetObject("GrapeCity", resourceCulture);
                return ((System.IO.MemoryStream)(obj));
            }
        }
        #endregion

        #region Get String
        /// <summary>
        ///   Gets the localized string.
        /// </summary>
        /// <param name="name">
        ///   The name of the string. It could be a const value in <see cref="LicenseResource"/>.
        /// </param>
        /// <returns>
        ///   The localized string.
        /// </returns>
        private static string GetString(string name)
        {
            return ResourceManager.GetString(name, Culture);
        }
        /// <summary>
        ///   Gets the localized string.
        /// </summary>
        /// <param name="name">
        ///   The name of the string. It could be a const value in <see cref="LicenseResource"/>.
        /// </param>
        /// <param name="args">
        ///   The data used in the string.
        /// </param>
        /// <returns>
        ///   The localized string.
        /// </returns>
        private static string GetString(string name, params object[] args)
        {
            string format = ResourceManager.GetString(name, Culture);
            return string.Format(CultureInfo.CurrentCulture, format, args);
        }
        #endregion
        
        #region Consts
        // Resource for LicenseDialog.
		private const string DialogText = "DialogText";
        private const string OKButtonText = "OKButtonText";
        private const string AboutButtonText = "AboutButtonText";

        private const string DesignNotExistHeader = "DesignNotExistHeader";
        private const string DesignNotExistBody = "DesignNotExistBody";

        private const string DesignInvalidHeader = "DesignInvalidHeader";
        private const string DesignInvalidBody = "DesignInvalidBody";

        private const string DesignTrialNotExpiredHeader = "DesignTrialNotExpiredHeader";
        private const string DesignTrialNotExpiredBodyFAQ = "DesignTrialNotExpiredBodyFAQ";
        private const string DesignTrialNotExpiredBodyActivation = "DesignTrialNotExpiredBodyActivation";
        private const string DesignTrialNotExpiredBodyShop = "DesignTrialNotExpiredBodyShop";

        private const string DesignTrialExpiredHeader = "DesignTrialExpiredHeader";
        private const string DesignTrialExpiredBody = "DesignTrialExpiredBody";

        private const string DesignProductExpiredHeader = "DesignProductExpiredHeader";
        private const string DesignProductExpiredBody = "DesignProductExpiredBody";

        private const string DesignLinkRunActivateTool = "DesignLinkRunActivateTool";
        private const string DesignLinkFAQ = "DesignLinkFAQ";
        private const string DesignLinkActivation = "DesignLinkActivation";
        private const string DesignLinkShop = "DesignLinkShop";

        private const string RunNotExistHeader = "RunNotExistHeader";
        private const string RunNotExistBody = "RunNotExistBody";

        private const string RunInvalidHeader = "RunInvalidHeader";
        private const string RunInvalidBody = "RunInvalidBody";

        private const string RunTrialNotExpiredHeader = "RunTrialNotExpiredHeader";
        private const string RunTrialNotExpiredBody = "RunTrialNotExpiredBody";

        private const string RunTrialExpiredHeader = "RunTrialExpiredHeader";
        private const string RunTrialExpiredBody = "RunTrialExpiredBody";

        private const string RunProductExpiredHeader = "RunProductExpiredHeader";
        private const string RunProductExpiredBody = "RunProductExpiredBody";

        private const string RunBodyDetailHeader = "RunBodyDetailHeader";
        private const string RunBodyDetailAssembly = "RunBodyDetailAssembly";
        private const string RunBodyDetailAssemblyVersion = "RunBodyDetailAssemblyVersion";
        private const string RunBodyDetailBuildDate = "RunBodyDetailBuildDate";
        private const string RunBodyDetailBuildMachine = "RunBodyDetailBuildMachine";
        private const string RunBodyDetailProductKey = "RunBodyDetailProductKey";

        // Error Code for PowerToolsLicenseProvider.GetLicense.
        private const string ErrorCodeMessage = "ErrorCodeMessage";

        // Message for LicenseException
        private const string Exception_NoLicense = "Exception_NoLicense";
        private const string Exception_LicenseNotExist = "Exception_LicenseNotExist";
        private const string Exception_LicenseInvalid = "Exception_LicenseInvalid";
        private const string Exception_LicenseExpired = "Exception_LicenseExpired";

        #endregion   

        #region Dialog Resource
        public static LicenseDialogKind GetDialogKind(ActivationState state)
        {
            switch (state)
            {
                case ActivationState.NoLicense:
                case ActivationState.InvalidLicense:
                default:
                    return LicenseDialogKind.Error;
                case ActivationState.TrialExpired:
                case ActivationState.ProductExpired:
                    return LicenseDialogKind.Warning;
                case ActivationState.ProductActivated:
                case ActivationState.TrialActivated:
                    return LicenseDialogKind.Info;
            }
        }
        public static string GetDialogTitle()
        {
            return GetString(LicenseResource.DialogText);
        }
        public static string GetDialogOKButtonText()
        {
            return GetString(LicenseResource.OKButtonText);
        }
        public static string GetDialogAboutButtonText()
        {
            return GetDialogAboutButtonText('&');
        }
        public static string GetDialogAboutButtonText(char accessKeyPrefix)
        {
            return GetString(LicenseResource.AboutButtonText, accessKeyPrefix);
        }
        public static string GetDialogDetailHeaderText()
        {
            return GetString(LicenseResource.RunBodyDetailHeader);
        }

        public static string GetDialogHeaderText(Product product, bool isRuntime, ActivationState licenseState, int leftDays)
        {
            return GetDialogHeaderText(product, isRuntime, licenseState, leftDays, Environment.NewLine);
        }
        public static string GetDialogHeaderText(Product product, bool isRuntime, ActivationState licenseState, int leftDays, string newLine)
        {
            if (!isRuntime)
            {
                switch (licenseState)
                {
                    case ActivationState.NoLicense:
                        return LicenseResource.GetString(LicenseResource.DesignNotExistHeader, product.Name, newLine);
                    case ActivationState.InvalidLicense:
                    default:
                        return LicenseResource.GetString(LicenseResource.DesignInvalidHeader, product.Name, newLine);
                    case ActivationState.TrialExpired:
                        return LicenseResource.GetString(LicenseResource.DesignTrialExpiredHeader, product.Name, newLine);
                    case ActivationState.TrialActivated:
                        return LicenseResource.GetString(LicenseResource.DesignTrialNotExpiredHeader, product.Name, newLine, leftDays);
                    case ActivationState.ProductExpired:
                        return LicenseResource.GetString(LicenseResource.DesignProductExpiredHeader, product.Name);
                    case ActivationState.ProductActivated:
                        return "";
                }
            }
            else
            {
                switch (licenseState)
                {
                    case ActivationState.NoLicense:
                        return LicenseResource.GetString(LicenseResource.RunNotExistHeader, product.Name, newLine);
                    case ActivationState.InvalidLicense:
                    default:
                        return LicenseResource.GetString(LicenseResource.RunInvalidHeader, product.Name, newLine);
                    case ActivationState.TrialExpired:
                        return LicenseResource.GetString(LicenseResource.RunTrialExpiredHeader, newLine);
                    case ActivationState.TrialActivated:
                        return LicenseResource.GetString(LicenseResource.RunTrialNotExpiredHeader, newLine, leftDays);
                    case ActivationState.ProductExpired:
                        return LicenseResource.GetString(LicenseResource.RunProductExpiredHeader, product.Name);
                    case ActivationState.ProductActivated:
                        return "";
                }
            }
        }
        public static string GetDialogBodyText(Product product, bool isRuntime, ActivationState licenseState, int leftDays, out LicenseDialogWebLinkTarget target)
        {
            return GetDialogBodyText(product, isRuntime, licenseState, leftDays, Environment.NewLine, out target);
        }
        public static string GetDialogBodyText(Product product, bool isRuntime, ActivationState licenseState, int leftDays, string newLine, out LicenseDialogWebLinkTarget target)
        {
            target = LicenseDialogWebLinkTarget.None;

            if (!isRuntime)
            {
                switch (licenseState)
                {
                    case ActivationState.NoLicense:
                        return LicenseResource.GetString(LicenseResource.DesignNotExistBody);
                    case ActivationState.InvalidLicense:
                    default:
                        return LicenseResource.GetString(LicenseResource.DesignInvalidBody);
                    case ActivationState.TrialExpired:
                        target = LicenseDialogWebLinkTarget.GotoWebShop;
                        return LicenseResource.GetString(LicenseResource.DesignTrialExpiredBody, newLine, newLine, newLine);
                    case ActivationState.TrialActivated:
                        if (leftDays > 3)
                        {
                            Random rnd = new Random(DateTime.Now.Second);
                            switch (rnd.Next(0, 2))
                            {
                                case 0:
                                    target = LicenseDialogWebLinkTarget.GotoWebFAQ;
                                    return LicenseResource.GetString(LicenseResource.DesignTrialNotExpiredBodyFAQ);
                                case 1:
                                default:
                                    target = LicenseDialogWebLinkTarget.GotoWebActivation;
                                    return LicenseResource.GetString(LicenseResource.DesignTrialNotExpiredBodyActivation);
                            }
                        }
                        else
                        {
                            target = LicenseDialogWebLinkTarget.GotoWebShop;
                            return LicenseResource.GetString(LicenseResource.DesignTrialNotExpiredBodyShop);
                        }
                    case ActivationState.ProductExpired:
                        target = LicenseDialogWebLinkTarget.GotoWebActivation;
                        return LicenseResource.GetString(LicenseResource.DesignProductExpiredBody);
                    case ActivationState.ProductActivated:
                        return "";
                }
            }
            else
            {
                switch (licenseState)
                {
                    case ActivationState.NoLicense:
                        return LicenseResource.GetString(LicenseResource.RunNotExistBody, product.Name);
                    case ActivationState.InvalidLicense:
                    default:
                        return LicenseResource.GetString(LicenseResource.RunInvalidBody, product.Name);
                    case ActivationState.TrialExpired:
                        return LicenseResource.GetString(LicenseResource.RunTrialExpiredBody, product.Name);
                    case ActivationState.TrialActivated:
                        return LicenseResource.GetString(LicenseResource.RunTrialNotExpiredBody, product.Name);
                    case ActivationState.ProductExpired:
                        return LicenseResource.GetString(LicenseResource.RunProductExpiredBody);
                    case ActivationState.ProductActivated:
                        return "";
                }
            }
        }
        public static string GetDialogActivateLinkText()
        {
            return LicenseResource.GetString(LicenseResource.DesignLinkRunActivateTool);
        }
        public static string GetDialogWebLinkText(LicenseDialogWebLinkTarget target)
        {
            switch (target)
            {
                case LicenseDialogWebLinkTarget.GotoWebFAQ:
                    return LicenseResource.GetString(LicenseResource.DesignLinkFAQ);
                case LicenseDialogWebLinkTarget.GotoWebActivation:
                    return LicenseResource.GetString(LicenseResource.DesignLinkActivation);
                case LicenseDialogWebLinkTarget.GotoWebShop:
                    return LicenseResource.GetString(LicenseResource.DesignLinkShop);
                default:
                    throw new InvalidOperationException();
            }
        }

        public static string GetDialogDetailInfo(Product product, bool isRuntime, ActivationState licenseState, DateTime? buildDate, string machineName, string serialKey)
        {
            return GetDialogDetailInfo(product, isRuntime, licenseState, buildDate, machineName, serialKey, Environment.NewLine);
        }
        public static string GetDialogDetailInfo(Product product, bool isRuntime, ActivationState licenseState, DateTime? buildDate, string machineName, string serialKey, string newLine)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(LicenseResource.GetString(LicenseResource.RunBodyDetailAssembly, GetAssemblyName(product.GetType().Assembly)));
            sb.Append(newLine);
            sb.Append(LicenseResource.GetString(LicenseResource.RunBodyDetailAssemblyVersion, GetAssemblyFileVersion(product.GetType().Assembly)));
            sb.Append(newLine);

            if (licenseState != ActivationState.NoLicense && licenseState != ActivationState.InvalidLicense)
            {
                sb.Append(LicenseResource.GetString(LicenseResource.RunBodyDetailBuildMachine, machineName));
                sb.Append(newLine);

                if (buildDate.HasValue)
                {
                    sb.Append(LicenseResource.GetString(LicenseResource.RunBodyDetailBuildDate, buildDate.Value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
                    sb.Append(newLine);
                }

                if (licenseState == ActivationState.TrialActivated || licenseState == ActivationState.TrialExpired)
                {
                    sb.Append(LicenseResource.GetString(LicenseResource.RunBodyDetailProductKey, FormatSerialKey(serialKey)));
                    sb.Append(newLine);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        ///   Get the file version description of the specified assembly.
        /// </summary>
        /// <param name="assembly">
        ///   The <see cref="Assembly"/> whose file version description should be get.
        /// </param>
        /// <returns>
        ///   A <see cref="String"/> indicates the file version description of the specified assembly.
        /// </returns>
        internal static string GetAssemblyFileVersion(Assembly assembly)
        {
            if (assembly != null)
            {
                object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), true);

                if (attributes.Length > 0)
                {
                    AssemblyFileVersionAttribute fileVersionAttribute = attributes[0] as AssemblyFileVersionAttribute;
                    if (fileVersionAttribute != null)
                    {
                        return fileVersionAttribute.Version;
                    }
                }
                try
                {
                    return assembly.GetName().Version.ToString();
                }
                catch (SecurityException)
                {
                }
            }
            return string.Empty;
        }
        /// <summary>
        ///   Get the name of the specified assembly.
        /// </summary>
        /// <param name="assembly">
        ///   The <see cref="Assembly"/> whose name should be get.
        /// </param>
        /// <returns>
        ///   A <see cref="String"/> indicates the name of the specified assembly.
        /// </returns>
        /// <remarks>
        ///   Please use this method to get assembly name. It is because that the Assembly.GetName method need FileIO permission.
        /// </remarks>
        internal static string GetAssemblyName(Assembly assembly)
        {
            string fullName = assembly.FullName;
            return fullName.Split(',')[0];
        }
        /// <summary>
        ///   Format the product key as "xxxx-xxxx-xxxx-xxxx-xxxx-xxxx".
        /// </summary>
        /// <param name="productKey">
        ///   A <see cref="String"/> indicates the original product key which is a sequence of 24 digits.
        /// </param>
        /// <returns>
        ///   A readable text indicates the product key.
        /// </returns>
        internal static string FormatSerialKey(string productKey)
        {
            if (string.IsNullOrEmpty(productKey))
            {
                return string.Empty;
            }
            List<string> list = new List<string>();
            for (int i = 0; i < productKey.Length; i += 4)
            {
                list.Add(productKey.Substring(i, Math.Min(4, productKey.Length - i)));
            }
            return string.Join("-", list);
        }

        #endregion

        #region Error Code Message
        public static string GetErrorCodeMessage(int errorCode)
        {
            Debug.Assert(errorCode > 0);
            return LicenseResource.GetString(LicenseResource.ErrorCodeMessage, string.Format(CultureInfo.InvariantCulture,"{0:D2}-{1:D3}", 1, errorCode));
        }

        //public static string GetEndApplicationMessage(PowerToolsLicense license, Type componentType)
        //{
        //    string message = null;
        //    if (license.IsEmpty)
        //    {
        //        message = LicenseResource.GetString(LicenseResource.Exception_NoLicense, componentType.Name);
        //    }
        //    else if (!license.Exist)
        //    {
        //        message = LicenseResource.GetString(LicenseResource.Exception_LicenseNotExist, componentType.Name);
        //    }
        //    else if (!license.Valid)
        //    {
        //        message = LicenseResource.GetString(LicenseResource.Exception_LicenseInvalid, componentType.Name);
        //    }
        //    else if (license.Expired)
        //    {
        //        message = LicenseResource.GetString(LicenseResource.Exception_LicenseExpired);
        //    }
        //    return message;
        //}
        #endregion
    }

    #region LicenseDialogKind
    /// <summary>
    ///   Represents the kind of license dialog.
    /// </summary>
    internal enum LicenseDialogKind
    {
        /// <summary>
        ///   Info dialog. It is shown for a trial license.
        /// </summary>
        Info,
        /// <summary>
        ///   Warning dialog. It is shown for a trial license which is expired.
        /// </summary>
        Warning,
        /// <summary>
        ///   Error dialog. It is shown for an invalid license or other errors.
        /// </summary>
        Error,
    }
    #endregion

    #region LicenseDialogWebLinkTarget
    /// <summary>
    ///   Represents the target web site for the link in license dialog.
    /// </summary>
    internal enum LicenseDialogWebLinkTarget
    {
#if GRAPECITY
        /// <summary>
        ///   None target.
        /// </summary>
        None,
        /// <summary>
        ///   Goto web site http://www.grapecity.com/japan/support/faq/.
        /// </summary>
        GotoWebFAQ,
        /// <summary>
        ///   Goto web site http://www.grapecity.com/japan/activation/.
        /// </summary>
        GotoWebActivation,
        /// <summary>
        ///   Goto web site http://shop.grapecity.com/.
        /// </summary>
        GotoWebShop
#else
        /// <summary>
        ///   None target.
        /// </summary>
        None,
        /// <summary>
        ///   Goto web site http://www.grapecity.com/support/portal/.
        /// </summary>
        GotoWebFAQ,
        /// <summary>
        ///   Goto web site http://www.grapecity.com/componentone/.
        /// </summary>
        GotoWebActivation,
        /// <summary>
        ///   Goto web site http://www.grapecity.com/pricing/componentone/.
        /// </summary>
        GotoWebShop
#endif
    }
    #endregion
}
