﻿//==============================================================================
//  File Name   :   LicenseDialog.cs
//
//  Copyright (C) 2008 GrapeCity Inc. All rights reserved.
//
//  Distributable under grapecity code license.
//  See terms of license at www.grapecity.com.
//
//==============================================================================

// <fileinformation>
//   <summary>
//     This file defines the LicenseDialog class.
//   </summary>
//   <author name="Carl Chen" mail="Carl.Chen@grapecity.com"/>
//   <seealso ref=""/>
//   <remarks>
//     Implementation of the LicenseDialog class.
//   </remarks>
// </fileinformation>

// <history>
//   <record date="20081215" author="Carl Chen" revision="1.00.000">
//     Create the file and implement the class.
//   </record>
// </history>
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Reflection;
using System.Windows.Media.Imaging;
using GrapeCity.Common.Resources;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace GrapeCity.Common
{
    internal class WpfLicenseDialog<T> : Window where T : Product, new()
    {
        private T _product;
        private ILicenseData<T> _license;
        private Action _showAboutBox;

        public WpfLicenseDialog(T product, ILicenseData<T> license, Action showAboutBox)
        {
            this._product = product;
            this._license = license;
            this._showAboutBox = showAboutBox;

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.StreamSource = LicenseResource.GrapeCityIcon;
            bitmap.EndInit();
            this.Icon = bitmap;
            this.Title = LicenseResource.GetDialogTitle();

            this.ResizeMode = ResizeMode.NoResize;
            this.SizeToContent = SizeToContent.WidthAndHeight;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.SnapsToDevicePixels = true;

            this.Content = this.CreateContent();
        }

        private LicenseInfoPanel CreateContent()
        {
            var product = this._product;
            var isRuntime = this._license.IsRuntime;
            var state = this._license.State;
            var leftDays = this._license.GetLeftDays();

            LicenseInfoPanel panel = new LicenseInfoPanel();
            LicenseDialogKind dialogKind = LicenseResource.GetDialogKind(state);
            BitmapDecoder decoder;
            Brush background;
            Brush foreground;
            switch (dialogKind)
            {
                case LicenseDialogKind.Info:
                    decoder = BitmapDecoder.Create(LicenseResource.InfoIcon, BitmapCreateOptions.None, BitmapCacheOption.Default);
                    background = Brushes.Green;
                    foreground = Brushes.White;
                    break;
                case LicenseDialogKind.Warning:
                    decoder = BitmapDecoder.Create(LicenseResource.WarningIcon, BitmapCreateOptions.None, BitmapCacheOption.Default);
                    background = Brushes.Orange;
                    foreground = Brushes.Black;
                    break;
                case LicenseDialogKind.Error:
                    decoder = BitmapDecoder.Create(LicenseResource.ErrorIcon, BitmapCreateOptions.None, BitmapCacheOption.Default);
                    background = Brushes.Orange;
                    foreground = Brushes.Black;
                    break;
                default:
                    throw new InvalidOperationException();
            }
            panel.headerIcon.Source = decoder.Frames[0];
            panel.headerPanel.Background = background;
            panel.headerText.Foreground = foreground;

            LicenseDialogWebLinkTarget linkTarget;
            panel.headerText.Text = LicenseResource.GetDialogHeaderText(product, isRuntime, state, leftDays);
            panel.headerText.FontSize = SystemFonts.MessageFontSize + 2;
            panel.bodyText.Text = LicenseResource.GetDialogBodyText(product, isRuntime, state, leftDays, out linkTarget);

            if (!isRuntime)
            {
                panel.bodyDetailHeader.Visibility = Visibility.Collapsed;
                panel.bodyDetail.Visibility = Visibility.Collapsed;

                panel.bodyActivateLinkBlock.Visibility = Visibility.Visible;
                panel.bodyActivateLinkText.Text = LicenseResource.GetDialogActivateLinkText();
                panel.bodyActivateLink.Click += new RoutedEventHandler(RunActivateTool);

                if (state != ActivationState.NoLicense && state != ActivationState.InvalidLicense)
                {
                    panel.bodyWebLinkBlock.Visibility = Visibility.Visible;
                    panel.bodyWebLinkText.Text = LicenseResource.GetDialogWebLinkText(linkTarget);
                    panel.bodyWebLink.Click += new RoutedEventHandler(bodyWebLink_Click);
                    switch (linkTarget)
                    {
#if GRAPECITY
                        case LicenseDialogWebLinkTarget.GotoWebFAQ:
                            panel.bodyWebLink.NavigateUri = new Uri("http://www.grapecity.com/japan/support/faq/");
                            break;
                        case LicenseDialogWebLinkTarget.GotoWebActivation:
                            panel.bodyWebLink.NavigateUri = new Uri("http://www.grapecity.com/japan/activation/");
                            break;
                        case LicenseDialogWebLinkTarget.GotoWebShop:
                            panel.bodyWebLink.NavigateUri = new Uri("http://shop.grapecity.com/");
                            break;
#else
                        case LicenseDialogWebLinkTarget.GotoWebFAQ:
                            panel.bodyWebLink.NavigateUri = new Uri("http://www.grapecity.com/support/portal/");
                            break;
                        case LicenseDialogWebLinkTarget.GotoWebActivation:
                            panel.bodyWebLink.NavigateUri = new Uri("http://www.grapecity.com/componentone/");
                            break;
                        case LicenseDialogWebLinkTarget.GotoWebShop:
                            panel.bodyWebLink.NavigateUri = new Uri("https://www.grapecity.com/pricing/componentone");
                            break;
#endif
                        default:
                            break;
                    }
                }
                else
                {
                    panel.bodyWebLinkBlock.Visibility = Visibility.Collapsed;
                }

                panel.aboutButton.Content = CreateAccessText(LicenseResource.GetDialogAboutButtonText('_'));
                if (this._showAboutBox == null)
                {
                    panel.aboutButton.Visibility = Visibility.Collapsed;
                }
                else
                {
                    panel.aboutButton.Visibility = Visibility.Visible;
                    panel.aboutButton.Click += new RoutedEventHandler(ShowAboutBox);
                }
            }
            else
            {
                panel.bodyLinkPanel.Visibility = Visibility.Collapsed;
                panel.aboutButton.Visibility = Visibility.Collapsed;
                panel.bodyWebLinkBlock.Visibility = Visibility.Collapsed;

                panel.bodyDetailHeader.Visibility = Visibility.Visible;
                panel.bodyDetailHeader.Text = LicenseResource.GetDialogDetailHeaderText();
                panel.bodyDetail.Visibility = Visibility.Visible;
                panel.bodyDetail.Text = LicenseResource.GetDialogDetailInfo(product, isRuntime, state, this._license.BaseDate, this._license.MachineName, this._license.SerialKey);
            }
            panel.okButton.Content = CreateAccessText(LicenseResource.GetDialogOKButtonText());

            return panel;
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        void bodyWebLink_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string navigateUri = (e.Source as Hyperlink).NavigateUri.ToString();
                System.Diagnostics.Process.Start(navigateUri);
            }
            catch
            {
            }
        }

        private static AccessText CreateAccessText(string content)
        {
            AccessText accessText = new AccessText();
            accessText.Text = content;
            return accessText;
        }
        private void RunActivateTool(object sender, RoutedEventArgs e)
        {
            GcNetFxLicenseProvider<T>.RunActivateTool();
        }

        private void ShowAboutBox(object sender, RoutedEventArgs e)
        {
            if (this._showAboutBox != null)
            {
                this._showAboutBox();
            }
        }
    }
}