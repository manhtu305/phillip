﻿//==============================================================================
//  File Name   :   WPFLicenseProvider.cs
//
//  Copyright (C) 2008 GrapeCity Inc. All rights reserved.
//
//  Distributable under grapecity code license.
//  See terms of license at www.grapecity.com.
//
//==============================================================================

// <fileinformation>
//   <summary>
//     This file defines the WPFLicenseProvider class.
//   </summary>
//   <author name="Carl Chen" mail="Carl.Chen@grapecity.com"/>
//   <seealso ref=""/>
//   <remarks>
//     Implementation of the WPFLicenseProvider class.
//   </remarks>
// </fileinformation>

// <history>
//   <record date="20081218" author="Carl Chen" revision="1.00.000">
//     Create the file and implement the class.
//   </record>
// </history>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security;
using System.Security.Permissions;
using System.Windows;
using System.Reflection;
using System.Windows.Threading;
using System.IO;
using System.Text;
using GrapeCity.Common.Resources;

namespace GrapeCity.Common
{
    /// <summary>
    ///   Represents a license provider of a WPF PowerTools component.
    /// </summary>
    internal abstract class WPFLicenseProvider<T> : GcNetFxLicenseProvider<T> where T : Product, new()
    {
        protected override void ShowLicenseDialogCore(T product, ActivationState state, ILicenseData<T> licenseData, object instance)
        {
            if (!System.Windows.Interop.BrowserInteropHelper.IsBrowserHosted)
            {
                //-- WPF 
                WpfLicenseDialog<T> dialog = new WpfLicenseDialog<T>(product, licenseData, this.ShowAboutBox);
                if (licenseData.IsRuntime || IsLicenseCompiling())
                {
                    dialog.ShowDialog();
                }
                else
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(new Action(
                        () => { dialog.ShowDialog(); }
                    ), DispatcherPriority.Render);
                }
            }
            else //--XBAP
            {
                this.ShowMessageBox(product, state, licenseData);
            }
        }
        private static bool IsLicenseCompiling()
        {
            Assembly assembly = Assembly.GetEntryAssembly();

            if (assembly != null)
            {
                string assemblyName = assembly.FullName;
                if (assemblyName != null &&

                    assemblyName.StartsWith("lc", StringComparison.OrdinalIgnoreCase) &&
                    assemblyName.EndsWith("PublicKeyToken=b03f5f7f11d50a3a", StringComparison.OrdinalIgnoreCase))

                    return true;
            }
            return false;
        }

        private void ShowMessageBox(T product, ActivationState state, ILicenseData<T> licenseData)
        {
            var contents = BuildDialogContents(product, state, licenseData);
            MessageBoxImage image;
            switch (contents.Item2)
            {
                case LicenseDialogKind.Warning:
                    image = MessageBoxImage.Warning;
                    break;
                case LicenseDialogKind.Error:
                    image = MessageBoxImage.Error;
                    break;
                default:
                    image = MessageBoxImage.Information;
                    break;
            }

            MessageBox.Show(contents.Item3, contents.Item1, MessageBoxButton.OK, image);
        }

        protected abstract void ShowAboutBox();
    }
}
