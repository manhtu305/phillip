﻿//==============================================================================
//  File Name   :   LicenseInfoPanel.cs
//
//  Copyright (C) 2008 GrapeCity Inc. All rights reserved.
//
//  Distributable under grapecity code license.
//  See terms of license at www.grapecity.com.
//
//==============================================================================

// <fileinformation>
//   <summary>
//     This file defines the LicenseInfoPanel class.
//   </summary>
//   <author name="Carl Chen" mail="Carl.Chen@grapecity.com"/>
//   <seealso ref=""/>
//   <remarks>
//     Implementation of the LicenseInfoPanel class.
//   </remarks>
// </fileinformation>

// <history>
//   <record date="20081215" author="Carl Chen" revision="1.00.000">
//     Create the file and implement the class.
//   </record>
// </history>

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.ComponentModel;

namespace GrapeCity.Common
{
    /// <summary>
    /// Interaction logic for LicenseInfoPanel.xaml
    /// </summary>
    internal partial class LicenseInfoPanel : UserControl
    {
        public LicenseInfoPanel()
        {
            this.InheritanceBehavior = InheritanceBehavior.SkipToThemeNow;

            InitializeComponent();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.Close != null)
            {
                this.Close(this, EventArgs.Empty);
            }
        }

        public event EventHandler Close;
    }
}
