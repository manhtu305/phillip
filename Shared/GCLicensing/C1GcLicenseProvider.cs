﻿//#define TESTCRYPTO
#define USE_REFLECTION
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using C1.Util.Licensing;
using System.Globalization;
using System.Security.Permissions;
using System.Diagnostics;

namespace GrapeCity.Common
{
    internal class C1Product : Product
    {
        public override Guid Guid => Guid.Empty;
        public override string Name => string.Empty;
        public override Version Version => System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        public virtual string C1Code => null;
    }

#if TESTCRYPTO
    /****************************************************************************************
     * Following C1Product classes are for Test GCLM Testing 
     ****************************************************************************************/
    internal class C1WPFProduct : C1Product
    {
        public override Guid Guid => new Guid("3a18e60c-6e2b-413a-883f-d59f42b63929");
        public override string Name => "Component One for WPF";
        public override string C1Code => "S6";
    }
    internal class C1WinformProduct : C1Product
    {
        public override Guid Guid => new Guid("1b8597c5-0f4f-4cbb-bc34-d457a564eae8");
        public override string Name => "ComponentOne for Windows Forms";
        public override string C1Code => "S8";
    }
    internal class C1DesktopProduct : C1Product
    {
        public override Guid Guid => new Guid("76978edf-c4a2-4ab1-9baa-b90c79b62d44");
        public override string Name => "ComponentOne for Desktop";
        public override string C1Code => "SD";
    }
    internal class C1SEProduct : C1Product
    {
        public override Guid Guid => new Guid("338e8154-a14e-42ba-a433-7f4339af93f7");
        public override string Name => "ComponentOne Studio Enterprise";
        public override string C1Code => "SE,SU";
    }
    internal class C1UltimateProduct : C1Product
    {
        public override Guid Guid => new Guid("6db7542a-a9b8-477e-8651-3ddda4d3aacd");
        public override string Name => "ComponentOne Ultimate";
        public override string C1Code => "SU,SE";
    }
#else
    /****************************************************************************************
     * Following C1Product classes are for Production Licensing 
     ****************************************************************************************/
    // if GRAPECITY is defined, then only JP licenses are accepted.
    // if !GRAPECITY, then both JP and non-JP licenses are accepted.

#if !GRAPECITY
#if WPF || CLR40 || CLR45
    internal class C1WPFProduct : C1Product
    {
        public override Guid Guid => new Guid("c02c28b7-1c24-4109-8eb3-f99c3905f3f1");
        public override string Name => "ComponentOne WPF Edition";
        public override string C1Code => "S6";
    }
    internal class C1WinformProduct : C1Product
    {
        public override Guid Guid => new Guid("da3d5d14-691f-4908-aa3c-fd3239734232");
        public override string Name => "ComponentOne WinForms Edition";
        public override string C1Code => "S8";
    }

	internal class C1AspNetProduct : C1Product
	{
		public override Guid Guid => new Guid("839e1737-f256-46ea-b391-50da451c13a4");
		public override string Name => "ComponentOne ASP.NET MVC Edition";
		public override string C1Code => "AH";
	}

	internal class C1SEProduct : C1Product
	{
		public override Guid Guid => new Guid("331cf6cd-b73c-429f-ba79-fa2f85eebd68");
		public override string Name => "ComponentOne Studio Enterprise";
		public override string C1Code => "SE,SU";
	}
#else
    internal class C1IOSProduct : C1Product
    {
        public override Guid Guid => new Guid("c2e333e5-6dae-4aaf-8903-fa1dc779d32a");
        public override string Name => "ComponentOne Xamarin.iOS Edition";
        public override string C1Code => "XE";
    }

    internal class C1AndroidProduct : C1Product
    {
        public override Guid Guid => new Guid("18dbecd7-edc0-49d0-95d9-bdb9e5e4827f");
        public override string Name => "ComponentOne Xamarin.Android Edition";
        public override string C1Code => "XE";
    }

    internal class C1XamarinProduct : C1Product
    {
        public override Guid Guid => new Guid("de2b5824-e24d-4e7f-86d1-a87c1729993c");
        public override string Name => "ComponentOne Studio for Xamarin";
        public override string C1Code => "XE";
    }

    internal class C1BlazorProduct : C1Product
    {
        public override Guid Guid => new Guid("6631ee67-fec7-45b0-a771-4ec75cd748e3");
        public override string Name => "ComponentOne Blazor Edition";
        public override string C1Code => "XE";
    }

    internal class C1UWPProduct : C1Product
    {
        public override Guid Guid => new Guid("9afa522c-ea0b-47fe-ae14-7d9225612767");
        public override string Name => "ComponentOne UWP Edition";
        public override string C1Code => "AJ";
    }
#endif
#endif
    // ---------------------------------------------------------------------------------------------------
#if WPF || CLR40 || CLR45
    internal class C1WPFProductJP : C1Product
    {
        public override Guid Guid => new Guid("4327EAF8-AA02-40A6-B9F6-3D007C039055");
        public override string Name => "ComponentOne WPF Edition JP";
        public override string C1Code => "S6";
    }
    internal class C1WinformProductJP : C1Product
    {
        public override Guid Guid => new Guid("1E2DD705-CD7C-42CE-8098-C4717DF794B1");
        public override string Name => "ComponentOne WinForms Edition JP";
        public override string C1Code => "S8";
    }

	internal class C1AspNetProductJP : C1Product
	{
		public override Guid Guid => new Guid("B78FAC6C-CC85-4A78-A08F-FF3556EDFA97");
		public override string Name => "ComponentOne ASP.NET MVC Edition JP";
		public override string C1Code => "AH";
	}

	internal class C1SEProductJP : C1Product
	{
		public override Guid Guid => new Guid("154B86E3-6B5B-4B2E-ACDC-91D24D249879");
		public override string Name => "ComponentOne Studio Enterprise JP";
		public override string C1Code => "SE,SU";
	}
#else
    internal class C1IOSProductJP : C1Product
    {
        public override Guid Guid => new Guid("C617BC79-C041-4111-B472-AD2C5A5AD5F2");
        public override string Name => "ComponentOne Xamarin.iOS Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1AndroidProductJP : C1Product
    {
        public override Guid Guid => new Guid("326784EA-3999-4598-9A3A-BD36CA1142FE");
        public override string Name => "ComponentOne Xamarin.Android Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1XamarinProductJP : C1Product
    {
        public override Guid Guid => new Guid("EB58417C-27BB-4F62-ACFA-4D36582D2D0C");
        public override string Name => "ComponentOne Studio for Xamarin JP";
        public override string C1Code => "XE";
    }

    internal class C1BlazorProductJP : C1Product
    {
        public override Guid Guid => new Guid("24D9C311-5D6C-4BA1-81E7-7F86B9495596");
        public override string Name => "ComponentOne Blazor Edition JP";
        public override string C1Code => "XE";
    }

    internal class C1UWPProductJP : C1Product
    {
        public override Guid Guid => new Guid("EAAD5427-BBD9-4D82-801F-6F37FB4AF9FE");
        public override string Name => "ComponentOne UWP Edition JP";
        public override string C1Code => "AJ";
    }
#endif
#endif
    //   Ultimate = "SU"
    //   Enterprise = "SE";
    //   WinForms = "S8";
    //   WPF = "S6";
    //   Desktop = "SD";
    //   UWP = "AJ";
    //   ActiveX = "S7";
    //   ASP.Net = "AH";
    //   Xamarin = "XE";

#if WPF
    internal class C1GCWpfLicenseProvider : WPFLicenseProvider<C1Product>
#elif WEB
    internal sealed class C1GCWebLicenseProvider : WebFormLicenseProvider<C1Product>
#else
    internal sealed class C1GCWFLicenseProvider : WinFormLicenseProvider<C1Product>
#endif
    {
        ILicenseData<C1Product> _licenseData = null;
        static string resourceBase = null;

#if WPF
        public C1GCWpfLicenseProvider() : base()
#elif WEB
        public C1GCWebLicenseProvider(Type runtimeType) : base()
#else
        public C1GCWFLicenseProvider() : base()
#endif
        {
            this.runtimeType = runtimeType;
            if (resourceBase == null)
            {
                //if (Debugger.IsAttached)
                //    Debugger.Break();
                //else
                //    Debugger.Launch();

                resourceBase = string.Empty;

                var licRes = Assembly.GetExecutingAssembly().GetManifestResourceNames().
                    Where(p => p.EndsWith("GCLicensing.Resources.LicenseResource.resources",
                    StringComparison.OrdinalIgnoreCase));

                if (licRes != null && licRes.Count() > 0)
                {
                    resourceBase = licRes.ElementAt(0).Replace(".resources", "");

                    GrapeCity.Common.Resources.LicenseResource.ResourceManager =
                        new System.Resources.ResourceManager(resourceBase, Assembly.GetExecutingAssembly());
                }
            }
        }

        internal delegate void DisplayAboutBox();
        private DisplayAboutBox displayAboutBox;
        internal void SetAboutBox(DisplayAboutBox aboutBox)
        {
            displayAboutBox = aboutBox;
        }
#if !NO_ABOUTBOX
        protected override void ShowAboutBox()
        {
            if (displayAboutBox != null)
                displayAboutBox();
        }
#endif

        internal C1Product GetLicensedProduct()
        {
            // used for AboutBox info.
            return _licenseData?.Product;
        }

        internal int GetTrialDaysRemaining()
        {
            // used for Trial AboutBox info.
            return (_licenseData.State == ActivationState.TrialActivated) ? _licenseData.GetLeftDays() : 0;
        }

        // C1ProductAttributes are used to filter allowed licenses
        C1ProductInfoAttribute[] c1Prods = Assembly.GetExecutingAssembly().
            GetCustomAttributes(typeof(C1.Util.Licensing.C1ProductInfoAttribute)) as C1ProductInfoAttribute[];

        private Type runtimeType = null;

        private bool IsValidLicense(ILicenseData<C1Product> licenseData)
        {
            bool validLicense = false;
            if (!string.IsNullOrEmpty(licenseData.Edition))
            {
                ActivationState state = licenseData.State;
                if (state == ActivationState.ProductActivated || state == ActivationState.TrialActivated)
                {
                    if (runtimeType != null)
                    {
                        c1Prods = runtimeType.GetCustomAttributes(typeof(C1ProductInfoAttribute), false) as C1ProductInfoAttribute[];
                        if (c1Prods == null || c1Prods.Length == 0)
                            c1Prods = runtimeType.Assembly.GetCustomAttributes(typeof(C1ProductInfoAttribute), false) as C1ProductInfoAttribute[];
                    }
                    if (!c1Prods.Any(p => licenseData.Product.C1Code.Split(',').Contains(p.ProductCode)))
                        return false;

                    string edition = licenseData.Edition;
                    if (licenseData.Trial || edition.IndexOf("Trial", StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        validLicense = true;
                    }
                    else if (edition.StartsWith("v", StringComparison.OrdinalIgnoreCase))
                    {
                        int year = 0, ver = 0;

                        if (int.TryParse(edition.Substring(1, 4), out year) && int.TryParse(edition.Substring(6, 1), out ver))
                        {
                            int licBuild = licenseData.Product.Version.Build;
                            validLicense = (licBuild <= ((year + 1) * 10 + ver));
                        }
                    }
                }
            }
            return validLicense;
        }

#if USE_REFLECTION
        // These are built in C1Products and should always be checked last
        Type[] studioProducts = new Type[]
            {
#if TESTCRYPTO
                typeof(C1WPFProduct),           // Test Licensing
                typeof(C1WinformProduct),
                typeof(C1DesktopProduct),
                typeof(C1SEProduct),
                typeof(C1UltimateProduct),      // Only Ultimate has trial and must be last.
#else
#if !GRAPECITY
#if WPF || CLR40 || CLR45
                typeof(C1WPFProduct),               // Production Licensing
                typeof(C1WinformProduct),
				typeof(C1AspNetProduct),
				//typeof(C1SEProduct),                 // Only Enterprise has trial and must be last
#else
                typeof(C1XamarinProduct),
                typeof(C1IOSProduct),
                typeof(C1AndroidProduct),
                typeof(C1UWPProduct),
                typeof(C1BlazorProduct),
#endif
#endif
#if WPF || CLR40 || CLR45
                typeof(C1WPFProductJP),               // Production Licensing
                typeof(C1WinformProductJP),
				typeof(C1AspNetProductJP),
#else
                typeof(C1XamarinProductJP),
                typeof(C1IOSProductJP),
                typeof(C1AndroidProductJP),
                typeof(C1UWPProductJP),
				typeof(C1BlazorProductJP),
#endif
                typeof(C1SEProductJP),               // Only Enterprise has trial and must be last
#if !GRAPECITY
                typeof(C1SEProduct),                 // Only Enterprise has trial and must be last
#endif
#endif
            };

        // The full list of C1Product types will be built in prodTypes
        List<Type> prodTypes = null;

        private MemberInfo[] GetLicenseManagerMemberInfo(string memberName)
        {
            if (prodTypes == null)
            {
                prodTypes = new List<Type>(Assembly.GetExecutingAssembly().GetTypes().
                    Where(p => (p.IsSubclassOf(typeof(C1Product)) && !studioProducts.Contains(p))));

                prodTypes.AddRange(studioProducts);
            }

            Type tylm = typeof(GcLicenseManager<>);
            MemberInfo[] mis = new MemberInfo[prodTypes.Count];

            for (int pt = 0; pt < prodTypes.Count; pt++)
            {
                Type ctylm = tylm.MakeGenericType(prodTypes[pt]);
                MemberInfo[] mi = ctylm.GetMember(memberName, BindingFlags.Static | BindingFlags.Public);
                mis[pt] = mi[0];
            }
            return mis;
        }

        private ILicenseData<C1Product> GetValidLicenseData(string licensePropertyName)
        {
            ILicenseData<C1Product> licData = null;
            var mis = GetLicenseManagerMemberInfo(licensePropertyName);
            foreach (PropertyInfo pi in mis)
            {
                var lic = pi.GetValue(null) as ILicenseData<C1Product>;
                if (IsValidLicense(lic)) { licData = lic; break; } // return lic;

                // only last license checked can be trial.
                if (lic.State == ActivationState.TrialExpired) { licData = lic; break; } // return lic;
            }
            //return null;
            _licenseData = licData;
            return licData;
        }

        protected override ILicenseData<C1Product> GetDesignTimeLicenseCore()
        {
            return GetValidLicenseData("DesignTimeLicense");
        }

        protected override ILicenseData<C1Product> GetRuntimeLicenseCore()
        {
            return GetValidLicenseData("RunTimeLicense");
        }

        protected override void WriteLicenseContext(LicenseContext context)
        {
            // use a set only property to avoid obfuscation issues.
            MemberInfo[] mis = GetLicenseManagerMemberInfo("WriteLicenseContextProp");
            foreach (PropertyInfo mi in mis) mi.SetValue(null, context);

            //MemberInfo[] mis = GetLicenseManagerMemberInfo("WriteLicenseContext");
            //foreach (MethodInfo mi in mis)
            //{
            //    mi.Invoke(null, new object[] { context });
            //}
            //}
        }
#else
        protected override ILicenseData<C1Product> GetDesignTimeLicenseCore()
        {
            return GetValidLicenseData(
#if TESTCRYPTO
                GcLicenseManager<C1WPFProduct>.DesignTimeLicense,
                GcLicenseManager<C1DesktopProduct>.DesignTimeLicense,
                GcLicenseManager<C1SEProduct>.DesignTimeLicense,
                GcLicenseManager<C1UltimateProduct>.DesignTimeLicense,
#else
#if !GRAPECITY
#if WPF || CLR40 || CLR45
                GcLicenseManager<C1WPFProduct>.DesignTimeLicense,
                GcLicenseManager<C1WinformProduct>.DesignTimeLicense,
 			    GcLicenseManager<C1AspNetProduct>.DesignTimeLicense,
				//GcLicenseManager<C1SEProduct>.DesignTimeLicense,
#else
                GcLicenseManager<C1XamarinProduct>.DesignTimeLicense,
                GcLicenseManager<C1IOSProduct>.DesignTimeLicense,
                GcLicenseManager<C1AndroidProduct>.DesignTimeLicense,
                GcLicenseManager<C1UWPProduct>.DesignTimeLicense,
			    GcLicenseManager<C1BlazorProduct>.DesignTimeLicense,
#endif
#endif
#if WPF || CLR40 || CLR45
                GcLicenseManager<C1WPFProductJP>.DesignTimeLicense,
                GcLicenseManager<C1WinformProductJP>.DesignTimeLicense,
			    GcLicenseManager<C1AspNetProductJP>.DesignTimeLicense,
#else
                GcLicenseManager<C1XamarinProductJP>.DesignTimeLicense,
                GcLicenseManager<C1IOSProductJP>.DesignTimeLicense,
                GcLicenseManager<C1AndroidProductJP>.DesignTimeLicense,
                GcLicenseManager<C1UWPProductJP>.DesignTimeLicense,
                GcLicenseManager<C1BlazorProductJP>.DesignTimeLicense,
#endif
                GcLicenseManager<C1SEProductJP>.DesignTimeLicense,
#if !GRAPECITY
                GcLicenseManager<C1SEProduct>.DesignTimeLicense,
#endif
#endif
            );
        }

        protected override ILicenseData<C1Product> GetRuntimeLicenseCore()
        {
            return GetValidLicenseData(
#if TESTCRYPTO
                GcLicenseManager<C1WPFProduct>.RunTimeLicense,
                GcLicenseManager<C1WinformProduct>.RunTimeLicense,
                GcLicenseManager<C1DesktopProduct>.RunTimeLicense,
                GcLicenseManager<C1SEProduct>.RunTimeLicense,
                GcLicenseManager<C1UltimateProduct>.RunTimeLicense,
#else
#if !GRAPECITY
#if WPF || CLR40 || CLR45
                GcLicenseManager<C1WPFProduct>.RunTimeLicense,
                GcLicenseManager<C1WinformProduct>.RunTimeLicense,
			    GcLicenseManager<C1AspNetProduct>.RunTimeLicense,
			    //GcLicenseManager<C1SEProduct>.RunTimeLicense,
#else
                GcLicenseManager<C1XamarinProduct>.RunTimeLicense,
                GcLicenseManager<C1IOSProduct>.RunTimeLicense,
                GcLicenseManager<C1AndroidProduct>.RunTimeLicense,
                GcLicenseManager<C1UWPProduct>.RunTimeLicense,
                GcLicenseManager<C1BlazorProduct>.RunTimeLicense,
#endif
#endif
#if WPF || CLR40 || CLR45
                GcLicenseManager<C1WPFProductJP>.RunTimeLicense,
                GcLicenseManager<C1WinformProductJP>.RunTimeLicense,
			    GcLicenseManager<C1AspNetProductJP>.RunTimeLicense,
#else
                GcLicenseManager<C1XamarinProductJP>.RunTimeLicense,
                GcLicenseManager<C1IOSProductJP>.RunTimeLicense,
                GcLicenseManager<C1AndroidProductJP>.RunTimeLicense,
                GcLicenseManager<C1UWPProductJP>.RunTimeLicense,
                GcLicenseManager<C1BlazorProductJP>.RunTimeLicense,
#endif
                GcLicenseManager<C1SEProductJP>.RunTimeLicense,
#if !GRAPECITY
                GcLicenseManager<C1SEProduct>.RunTimeLicense,
#endif
#endif
            );
        }

        protected override void WriteLicenseContext(LicenseContext context)
        {
#if TESTCRYPTO
            GcLicenseManager<C1WPFProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1WinformProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1DesktopProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1SEProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1UltimateProduct>.WriteLicenseContext(context);
#else
#if !GRAPECITY
#if WPF || CLR40 || CLR45
            GcLicenseManager<C1WPFProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1WinformProduct>.WriteLicenseContext(context);
			GcLicenseManager<C1AspNetProduct>.WriteLicenseContext(context);
			//GcLicenseManager<C1SEProduct>.WriteLicenseContext(context);
#else
            GcLicenseManager<C1XamarinProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1IOSProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1AndroidProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1UWPProduct>.WriteLicenseContext(context);
            GcLicenseManager<C1BlazorProduct>.WriteLicenseContext(context);
#endif
#endif
#if WPF || CLR40 || CLR45
            GcLicenseManager<C1WPFProductJP>.WriteLicenseContext(context);
            GcLicenseManager<C1WinformProductJP>.WriteLicenseContext(context);
			GcLicenseManager<C1AspNetProductJP>.WriteLicenseContext(context);
#else
            GcLicenseManager<C1XamarinProductJP>.WriteLicenseContext(context);
            GcLicenseManager<C1IOSProductJP>.WriteLicenseContext(context);
            GcLicenseManager<C1AndroidProductJP>.WriteLicenseContext(context);
            GcLicenseManager<C1UWPProductJP>.WriteLicenseContext(context);
            GcLicenseManager<C1BlazorProductJP>.WriteLicenseContext(context);
#endif
            GcLicenseManager<C1SEProductJP>.WriteLicenseContext(context);
#if !GRAPECITY
            GcLicenseManager<C1SEProduct>.WriteLicenseContext(context);
#endif
#endif
        }

        private ILicenseData<C1Product> GetValidLicenseData(params ILicenseData<C1Product>[] licenseDatas)
        {
            foreach (ILicenseData<C1Product> licenseData in licenseDatas)
            {
                if (IsValidLicense(licenseData))
                {
                    _licenseData = licenseData;
                    return licenseData;
                }
            }
            return null;
        }
#endif
    }
}
