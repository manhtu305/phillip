﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace C1.Web.Wijmo
{
	interface ICustomOptionType
	{
		/// <summary>
		/// Serializes the value.
		/// </summary>
		/// <returns></returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		string SerializeValue();

		/// <summary>
		/// Gets a value indicating whether value must be enclosed in quotes.
		/// </summary>
		/// <value><c>true</c> if [include quotes]; otherwise, <c>false</c>.</value>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		bool IncludeQuotes { get;  }
	}
}
