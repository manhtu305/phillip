
---------- Upgrade template to VS2017 and support VS2019 ----------

- *.csproj : <MinimumVisualStudioVersion>15.0</MinimumVisualStudioVersion> is changed from 14 to 15 
- *.vsixmanifest : <InstallationTarget Version="[14.0,)"... /> - This means that a version of VS to install the project template should be equal or greater then 14 (VS2015). Thus the project templates will be installed for all subsequent version of VS.
- *.vsixmanifest : Mandatory <Prerequisites> section.