﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1Wizard
{
    internal class WizardContentViewModelBase : INotifyPropertyChanged
    {
        private Dictionary<string, string> _replacementsDictionary;

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        
        public Dictionary<string,string> ReplacementDictionary { get { return _replacementsDictionary; } }

        public WizardContentViewModelBase(Dictionary<string,string> src)
        {
            _replacementsDictionary = src;
        }
    }
}
