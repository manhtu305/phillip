﻿using Microsoft.VisualStudio.TemplateWizard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnvDTE;
using System.IO;
using System.Windows.Controls;

namespace C1Wizard
{
    public abstract class C1ProjectTemplateWizard : IWizard
    {
        public abstract void BeforeOpeningFile(ProjectItem projectItem);

        public void ProjectFinishedGenerating(Project project)
        {
            RunBeforeSaveSolution(project);
#if !WPF
            // save soluntion file
            var solutionPath = project.DTE.Solution.FullName;
            if (string.IsNullOrEmpty(solutionPath))
            {
                var projPath = Path.GetDirectoryName(project.FullName);
                var projName = Path.GetFileNameWithoutExtension(project.FullName);
                solutionPath = Path.Combine(Path.GetDirectoryName(projPath), projName + ".sln");
                project.DTE.Solution.SaveAs(solutionPath);
            }
#endif
            RunAfterSaveSolution(project);
        }

        public abstract void ProjectItemFinishedGenerating(ProjectItem projectItem);

        public abstract void RunFinished();

        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            RunBeforeWizardDialogCreate(replacementsDictionary);
            WizardDialog dialog = CreateDialog(replacementsDictionary);
            if (!dialog.ShowDialog().Value)
            {
                CleanUp(replacementsDictionary);
                throw new WizardBackoutException();
            }
            ApplyToProjectTemplate(replacementsDictionary);
        }

        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        public abstract void RunBeforeSaveSolution(Project project);
        public abstract void RunAfterSaveSolution(Project project);
        public abstract void RunBeforeWizardDialogCreate(Dictionary<string, string> replacementsDictionary);
        public abstract WizardDialog CreateDialog(Dictionary<string, string> replacementsDictionary);
        public abstract void ApplyToProjectTemplate(Dictionary<string, string> replacementsDictionary);

        string GetCleanUpPath(Dictionary<string, string> replacementsDictionary)
        {
            string projectName = replacementsDictionary["$projectname$"];
            return replacementsDictionary["$solutiondirectory$"].Contains(projectName) ? replacementsDictionary["$solutiondirectory$"] : replacementsDictionary["$destinationdirectory$"];
        }

        void CleanUp(Dictionary<string, string> replacementsDictionary)
        {
            string path = GetCleanUpPath(replacementsDictionary);
            DirectoryInfo tempFolder = new DirectoryInfo(path);
            tempFolder.Delete(true);
        }
    }
}
