﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace C1Wizard
{
    /// <summary>
    /// Interaction logic for WizardDialog.xaml
    /// </summary>
    public partial class WizardDialog : Window
    {
        public WizardDialog()
        {
            InitializeComponent();
            Icon = GetIcon();
#if WPF
            ApplyButton.Content = C1.WPF.Wizard.Localization.Resources.Common_OK;
            CancelButton.Content = C1.WPF.Wizard.Localization.Resources.Common_Cancel;
#elif WEB
            ApplyButton.Content = "OK";
            CancelButton.Content = "Cancel";
#endif
            ApplyButton.Focus();
        }

        public UserControl DialogContent
        {
            set
            {
                ContentGrid.Children.Add(value);
            }
        }

        private void OnCreateClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            this.Close();
        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Escape:
                    DialogResult = false;
                    this.Close();
                    break;
                case Key.Enter:
                    ApplyButton.ReleaseMouseCapture();
                    DialogResult = true;
                    this.Close();
                    break;
            }
        }

        ImageSource GetIcon()
        {
#if GRAPECITY
            var iconName = "C1_ja.png";
#else
            var iconName = "C1.png";
#endif
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resNames.Length; i++)
            {
                string s = resNames[i];
                int n = s.LastIndexOf('.');
                if (n >= 0)
                {
                    n = s.LastIndexOf('.', n - 1);
                }
                string fn = s.Substring(n + 1);

                if (string.Equals(fn, iconName, StringComparison.OrdinalIgnoreCase))
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.StreamSource = asm.GetManifestResourceStream(s);
                    bitmap.EndInit();
                    return bitmap;
                }
            }
            return null;
        }
    }
}
