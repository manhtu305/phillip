///////////////////////////////////////////////////////////////
//
// Helper class to implement Licensing for WPF classes.
// This should be used only in WPF projects.
//
///////////////////////////////////////////////////////////////
using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

namespace C1.Util.Licensing
{
    /// <summary>
    /// Helper class to implement Licensing for WPF classes.
    /// This should be used only in WPF projects.
    /// </summary>
#if WPF
    [SmartAssembly.Attributes.DoNotObfuscateType]
#endif
    internal static class ProviderInfoWPF
    {
        static bool _isLicensed = false;

        internal static LicenseInfo Validate(Type type, FrameworkElement instance)
        {
          return Validate(type, instance, true);
        }

        internal static void ValidateInternal(Type type, FrameworkElement instance)
        {
            if (_isLicensed && LicenseManager.UsageMode == LicenseUsageMode.Runtime)
            {
                // at runtime do nothing if already checked valid license
                return;
            }
            // call regular licensing and retain the result for possible nag after the load.
            LicenseInfo licInfo = null;
            try
            {
                licInfo = ProviderInfo.Validate(type, instance, false, true);
            }
            catch
            {
                licInfo = new LicenseInfo(type, LicenseStatus.Unlicensed);
            }

            // if at least one instance is licensed, assume we're licensed
            _isLicensed |= licInfo.IsValid;

            if (!_isLicensed)
            {
                // nag only after the element has been loaded
                instance.Loaded += (s, e) =>
                {
                    if (!_isLicensed)
                    {
                    // we're going to nag now, assume we're licensed after this
                    _isLicensed = true;

                    // check whether this is design time or not
                    var designTime = DesignerProperties.GetIsInDesignMode(instance);

                    // nag (once per day per control)
                    ProviderInfo.Nag(type, licInfo, designTime);
                    }
                };
            }
        }

        /// <summary>
        /// Perform license validation. Call this method from the licensed object's 
        /// constructor to save a license key at design time, validate it at runtime, 
        /// and display a nag dialog if a valid license is not found.
        /// <param name="type">Type of licensed object (use typeof() and not GetType()).</param>
        /// <param name="instance">Reference to the licensed object.</param>
        /// <param name="testDomain">Whether to test for an IdeDomainManager (ASP.NET design time that looks like runtime).</param>
        /// <param name="showNag">Whether to show nag screen or it will be handled from some other place.</param>
        /// <returns>A <see cref="LicenseInfo"/> object that contains information about the license.</returns>
        /// </summary>
        internal static LicenseInfo Validate(Type type, FrameworkElement instance, bool testDomain, bool showNag = true)
        {
            // call regular licensing and retain the result for possible nag after the load.
            LicenseInfo licInfo = null;
            try
            {
                licInfo = ProviderInfo.Validate(type, instance, false, testDomain);
            }
            catch
            {
                licInfo = new LicenseInfo(type, LicenseStatus.Unlicensed);
            }

            // if at least one instance is licensed, assume we're licensed
            _isLicensed |= licInfo.IsValid;

            // nag only after the element has been loaded
            instance.Loaded += (s, e) =>
            {
                if (!_isLicensed)
                {
                    // we're going to nag now, assume we're licensed after this
                    _isLicensed = true;

                    // check whether this is design time or not
                    var designTime = DesignerProperties.GetIsInDesignMode(instance);

                    if (showNag)
                    {
                        // nag (once per day per control)
                        ProviderInfo.Nag(type, licInfo, designTime);
                    }
                }
            };

            // done
            return licInfo;
        }
    }

#if WPF
    [SmartAssembly.Attributes.DoNotObfuscateType]
#endif
    internal static class ProviderInfoWPFEnt
    {
        // use this class when some control should not be licensed with WPF Studio and uses different product info attributes. 
        // It will make sure that this control won't share WPF key when used with other WPF controls
        static bool _isLicensed = false;

        internal static LicenseInfo Validate(Type type, FrameworkElement instance)
        {
            return Validate(type, instance, true);
        }

        /// <summary>
        /// Perform license validation. Call this method from the licensed object's 
        /// constructor to save a license key at design time, validate it at runtime, 
        /// and display a nag dialog if a valid license is not found.
        /// <param name="type">Type of licensed object (use typeof() and not GetType()).</param>
        /// <param name="instance">Reference to the licensed object.</param>
        /// <param name="testDomain">Whether to test for an IdeDomainManager (ASP.NET design time that looks like runtime).</param>
        /// <returns>A <see cref="LicenseInfo"/> object that contains information about the license.</returns>
        /// </summary>
        internal static LicenseInfo Validate(Type type, FrameworkElement instance, bool testDomain)
        {
            // call regular licensing and retain the result for possible nag after the load.
            LicenseInfo licInfo = ProviderInfoWPF.Validate(type, instance, testDomain, false);

            // if at least one instance is licensed, assume we're licensed
            _isLicensed |= licInfo.IsValid;

            // nag only after the element has been loaded
            instance.Loaded += (s, e) =>
            {
                if (!_isLicensed)
                {
                    // we're going to nag now, assume we're licensed after this
                    _isLicensed = true;

                    // check whether this is design time or not
                    var designTime = DesignerProperties.GetIsInDesignMode(instance);

                    // nag (once per day per control)
                    ProviderInfo.Nag(type, licInfo, designTime); 
                }
            };

            // done
            return licInfo;
        }
    }
}
