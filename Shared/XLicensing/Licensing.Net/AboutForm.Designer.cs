﻿#pragma warning disable CS0436

namespace C1.Util.Licensing
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this._btnOK = new System.Windows.Forms.Button();
            this._lblProductName = new System.Windows.Forms.Label();
            this._lblProductVersion = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this._lblCopyright = new System.Windows.Forms.Label();
            this._linkHome = new System.Windows.Forms.TextBox();
            this._lblContactUs = new System.Windows.Forms.Label();            
            this._lblSalesInfo = new System.Windows.Forms.Label();
            this._lblResources = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._panelLicenseInfo = new System.Windows.Forms.Panel();
            this._lblStateEval = new System.Windows.Forms.Label();
            this._lblStateEvalOver = new System.Windows.Forms.Label();
            this._lblStateExpired = new System.Windows.Forms.Label();
            this._lblStateValid = new System.Windows.Forms.Label();
            this._lblStateNag = new System.Windows.Forms.Label();
            this._panelResources = new System.Windows.Forms.Panel();
            this._panelGC = new System.Windows.Forms.Panel();
            this._panelFooter = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this._linkGC = new System.Windows.Forms.TextBox();
            this._panelLinks = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this._panelFooter.SuspendLayout();
            this._panelLicenseInfo.SuspendLayout();
            this._panelResources.SuspendLayout();
            this._panelGC.SuspendLayout();
            this._panelLinks.SuspendLayout();
            this.SuspendLayout();
            // 
            // _btnOK
            // 
            this._btnOK.BackColor = System.Drawing.Color.FromArgb(247, 248, 248);
            this._btnOK.ForeColor = System.Drawing.Color.Black;
            this._btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnOK.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this._btnOK.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btnOK.Location = new System.Drawing.Point(497, 17); //285);
            this._btnOK.Name = "_btnOK";
            this._btnOK.Size = new System.Drawing.Size(66, 36);
            this._btnOK.TabIndex = 0;
            this._btnOK.Text = "OK";
            this._btnOK.UseVisualStyleBackColor = false;
            this._btnOK.Click += new System.EventHandler(CloseOn_Click);
            // 
            // _lblProductName
            // 
            this._lblProductName.AutoSize = true;
            this._lblProductName.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblProductName.ForeColor = System.Drawing.Color.Black;
            this._lblProductName.Location = new System.Drawing.Point(12,91);    //204, 91);
            this._lblProductName.Name = "_lblProductName";
            this._lblProductName.Size = new System.Drawing.Size(85,20);         //45, 20);
            this._lblProductName.TabIndex = 1;
            this._lblProductName.Text = "{0}";
            // 
            // _lblProductVersion
            // 
            this._lblProductVersion.AutoSize = true;
            this._lblProductVersion.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblProductVersion.ForeColor = System.Drawing.Color.Black;
            this._lblProductVersion.Location = new System.Drawing.Point(12,114);    //204, 114);
            this._lblProductVersion.Name = "_lblProductVersion";
            this._lblProductVersion.Size = new System.Drawing.Size(81, 14);
            this._lblProductVersion.TabIndex = 2;
            this._lblProductVersion.Text = "version {0}";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(246,250,253);
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(1, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(585, 79);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click +=new System.EventHandler(CloseOn_Click);
            //
            // _panelFooter
            //
            this._panelFooter.BackColor = System.Drawing.Color.White;
            this._panelFooter.Controls.Add(this.pictureBox2);
            this._panelFooter.Controls.Add(this._lblCopyright);
            this._panelFooter.Controls.Add(this._btnOK);
            this._panelFooter.Location = new System.Drawing.Point(1, 285);
            this._panelFooter.Size = new System.Drawing.Size(583, 71);
            this._panelFooter.Name = "_panelFooter";
            //this._panelLinks.TabIndex = 21;
            this._panelLinks.TabStop = false;           
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Location = new System.Drawing.Point(15, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = pictureBox2.BackgroundImage.Size;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // _lblCopyright
            // 
            this._lblCopyright.AutoSize = false;
            this._lblCopyright.Font = new System.Drawing.Font("Verdana", 7F);
            this._lblCopyright.ForeColor = System.Drawing.Color.Black;
            this._lblCopyright.Location = new System.Drawing.Point(240, 1);
            this._lblCopyright.Name = "_lblCopyright";
            this._lblCopyright.Size = new System.Drawing.Size(250, 70);
            this._lblCopyright.TabIndex = 7;
            this._lblCopyright.Text = "Copyright (c) 2001-{0} GrapeCity, inc.\r\nAll rights reserved.";
            this._lblCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _linkHome
            // 
            this._linkHome.AutoSize = true;
            this._linkHome.Text = "www.grapecity.com/componentone";
            this._linkHome.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._linkHome.Location = new System.Drawing.Point(15, 3);
            this._linkHome.Name = "_linkHome";
            this._linkHome.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._linkHome.BackColor = System.Drawing.Color.FromArgb(246, 250, 253);
            this._linkHome.ForeColor = System.Drawing.Color.Blue;
            this._linkHome.Size = new System.Drawing.Size(430, 14);
            this._linkHome.TabStop = false;
            this._linkHome.Tag = "default.aspx";
            this._linkHome.ReadOnly = true;         // textbox version only
            this._lblContactUs.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblContactUs.Location = new System.Drawing.Point(15,20);
            this._lblContactUs.Name = "_lblContactUs";
            this._lblContactUs.Size = new System.Drawing.Size(250, 45);
            this._lblContactUs.Text = "GrapeCity, US";
            this._lblSalesInfo.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblSalesInfo.Location = new System.Drawing.Point(280, 20);
            this._lblSalesInfo.Name = "_lblSalesInfo";
            this._lblSalesInfo.Size = new System.Drawing.Size(170, 45);
            this._lblSalesInfo.Text = "\r\nPhone: 1.800.858.2739\r\nus.sales@grapecity.com";
            // 
            // _lblResources
            // 
            this._lblResources.Location = new System.Drawing.Point(15, 6);
            this._lblResources.Name = "_lblResources";
            this._lblResources.Size = new System.Drawing.Size(90, 12);
            this._lblResources.TabIndex = 7;
            this._lblResources.Text = "CONTACT US:";
            // 
            // _panelLicenseInfo
            // 
            this._panelLicenseInfo.BackColor = System.Drawing.Color.Transparent;
            this._panelLicenseInfo.Controls.Add(this._lblStateEval);
            this._panelLicenseInfo.Controls.Add(this._lblStateEvalOver);
            this._panelLicenseInfo.Controls.Add(this._lblStateExpired);
            this._panelLicenseInfo.Controls.Add(this._lblStateValid);
            this._panelLicenseInfo.Controls.Add(this._lblStateNag);
            this._panelLicenseInfo.Location = new System.Drawing.Point(12,141);     //164, 141);
            this._panelLicenseInfo.Name = "_panelLicenseInfo";
            this._panelLicenseInfo.Size = new System.Drawing.Size(575, 80);
            this._panelLicenseInfo.TabIndex = 23;
            // 
            // _lblStateEval
            // 
            this._lblStateEval.BackColor = System.Drawing.Color.Transparent;
            this._lblStateEval.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lblStateEval.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblStateEval.ForeColor = System.Drawing.Color.Red;
            this._lblStateEval.Location = new System.Drawing.Point(0, 0);
            this._lblStateEval.Name = "_lblStateEval";
            this._lblStateEval.Size = new System.Drawing.Size(357, 75);
            this._lblStateEval.TabIndex = 2;
            this._lblStateEval.Text = "Not licensed.\r\nYou can use this product for evaluation purposes for an additional {0} days.\r\nPlease visit our website or contact us to purchase a license.";
            // 
            // _lblStateEvalOver
            // 
            this._lblStateEvalOver.BackColor = System.Drawing.Color.Transparent;
            this._lblStateEvalOver.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lblStateEvalOver.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblStateEvalOver.ForeColor = System.Drawing.Color.Red;
            this._lblStateEvalOver.Location = new System.Drawing.Point(0, 0);
            this._lblStateEvalOver.Name = "_lblStateEvalOver";
            this._lblStateEvalOver.Size = new System.Drawing.Size(575, 75);
            this._lblStateEvalOver.TabIndex = 2;
            this._lblStateEvalOver.Text = "Your trial period has expired.\r\n" +
                "Please contact us or visit our site to purchase a license.\r\n" +
                "Visit https://www.grapecity.com/licensing/componentone to learn how to extend your trial another 30 days.";
            // 
            // _lblStateExpired
            // 
            this._lblStateExpired.BackColor = System.Drawing.Color.Transparent;
            this._lblStateExpired.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lblStateExpired.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblStateExpired.ForeColor = System.Drawing.Color.Red;
            this._lblStateExpired.Location = new System.Drawing.Point(0, 0);
            this._lblStateExpired.Name = "_lblStateExpired";
            this._lblStateExpired.Size = new System.Drawing.Size(357, 75);
            this._lblStateExpired.TabIndex = 1;
            this._lblStateExpired.Text = "Your license expired in {0:MMMM yyyy}.\r\nTo use this version, please renew your li" +
    "cense and rebuild the application.";
            // 
            // _lblStateValid
            // 
            this._lblStateValid.BackColor = System.Drawing.Color.Transparent;
            this._lblStateValid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lblStateValid.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblStateValid.Location = new System.Drawing.Point(0, 0);
            this._lblStateValid.Name = "_lblStateValid";
            this._lblStateValid.Size = new System.Drawing.Size(357, 75);
            this._lblStateValid.TabIndex = 0;
            this._lblStateValid.Text = "This product is licensed to:\r\n{0}\r\n";
            // 
            // _lblStateNag
            // 
            this._lblStateNag.BackColor = System.Drawing.Color.Transparent;
            this._lblStateNag.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lblStateNag.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblStateNag.ForeColor = System.Drawing.Color.Red;
            this._lblStateNag.Location = new System.Drawing.Point(0, 0);
            this._lblStateNag.Name = "_lblStateNag";
            this._lblStateNag.Size = new System.Drawing.Size(357, 75);
            this._lblStateNag.TabIndex = 0;
            this._lblStateNag.Text = "Evaluation version.\r\nThis dialog will not be shown if you rebuild your applicatio" +
    "n using a licensed version of the product.";
            // 
            // _panelResources
            // 
            this._panelResources.BackColor = System.Drawing.Color.FromArgb(246, 250, 253);
            this._panelResources.Controls.Add(this._lblResources);
            this._panelResources.Controls.Add(this._panelGC);
            this._panelResources.Controls.Add(this._panelLinks);
            this._panelResources.Location = new System.Drawing.Point(1, 210);
            this._panelResources.Name = "_panelResources";
            this._panelResources.Size = new System.Drawing.Size(585, 70);
            this._panelResources.TabIndex = 5;
            // 
            // _panelGC
            // 
            this._panelGC.Controls.Add(this.label1);
            this._panelGC.Controls.Add(this._linkGC);
            this._panelGC.Location = new System.Drawing.Point(144, 0);
            this._panelGC.Name = "_panelGC";
            this._panelGC.Size = new System.Drawing.Size(358, 69);
            this._panelGC.TabIndex = 3;
            this._panelGC.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "グレープシティ株式会社";
            // 
            // _linkGC
            // 
            this._linkGC.AutoSize = true;
            this._linkGC.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._linkGC.Location = new System.Drawing.Point(3, 21);
            this._linkGC.Name = "_linkGC";
            this._linkGC.Size = new System.Drawing.Size(430, 14);
            this._linkGC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._linkGC.BackColor = System.Drawing.Color.FromArgb(246, 250, 253);
            this._linkGC.ForeColor = System.Drawing.Color.Blue;
            this._linkGC.TabStop = false;
            this._linkGC.ReadOnly = true;
            this._linkGC.Text = "http://www.grapecity.co.jp/developer";
            // 
            // _panelLinks
            // 
            this._panelLinks.Controls.Add(this._linkHome);
            this._panelLinks.Controls.Add(this._lblContactUs);
            this._panelLinks.Controls.Add(this._lblSalesInfo);
            this._panelLinks.Location = new System.Drawing.Point(110, 2);
            this._panelLinks.Size = new System.Drawing.Size(460, 80);
            this._panelLinks.Name = "_panelLinks";
            this._panelLinks.TabIndex = 3;
            // 
            // AboutForm
            // 
            this.AcceptButton = this._btnOK;
            this.AutoSize = true;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.BackColor = System.Drawing.Color.White;
            this.ForeColor = System.Drawing.Color.FromArgb(44, 47, 79);
            this.CancelButton = this._btnOK;
            this.ClientSize = new System.Drawing.Size(585, 357);
            this.Controls.Add(this._lblProductName);
            this.Controls.Add(this._panelResources);
            this.Controls.Add(this._panelLicenseInfo);
            this.Controls.Add(this._panelFooter);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this._lblProductVersion);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.ShowInTaskbar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About {0}";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this._panelFooter.ResumeLayout(false);
            this._panelLicenseInfo.ResumeLayout(false);
            this._panelResources.ResumeLayout(false);
            this._panelResources.PerformLayout();
            this._panelGC.ResumeLayout(false);
            this._panelGC.PerformLayout();
            this._panelLinks.ResumeLayout(false);
            this._panelLinks.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button _btnOK;
        private System.Windows.Forms.Label _lblProductName;
        private System.Windows.Forms.Label _lblProductVersion;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label _lblCopyright;
        private System.Windows.Forms.TextBox _linkHome;
        private System.Windows.Forms.Label _lblContactUs;
        private System.Windows.Forms.Label _lblSalesInfo;
        private System.Windows.Forms.Label _lblResources;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel _panelLicenseInfo;
        private System.Windows.Forms.Panel _panelFooter;
        private System.Windows.Forms.Label _lblStateExpired;
        private System.Windows.Forms.Label _lblStateValid;
        private System.Windows.Forms.Label _lblStateNag;
        private System.Windows.Forms.Label _lblStateEval;
        private System.Windows.Forms.Label _lblStateEvalOver;
        private System.Windows.Forms.Panel _panelResources;
        private System.Windows.Forms.Panel _panelGC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _linkGC;
        private System.Windows.Forms.Panel _panelLinks;
    }
}
#pragma warning restore CS0436
