//----------------------------------------------------------------------------
// C1.Util.Localization.StringTables
//----------------------------------------------------------------------------
//
// Populates localized string trables used by the Localizer class.
//
//----------------------------------------------------------------------------
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status			Date			By						Comments
//----------------------------------------------------------------------------
// Created			May 20 2002	    Bernardo				-
// Revised          Feb 6 2008      Bernardo				general cleanup
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;

namespace C1.Util.Localization
{
	/// <summary>
	/// Contains a single static method called InitTables that populates the
	/// tables used by the C1Localizer class.
    /// 
    /// ** This file is project-specific **
    /// Do not include it in your project. Instead, copy it into your project
    /// and customize the copy with the strings you need.
	/// </summary>
	internal class StringTables
	{
		internal static void InitTables(Hashtable htDesc, Hashtable htCat, Hashtable htGetStr, Hashtable htForms, string locale)
		{
			// Japanese
			if (locale.StartsWith("ja"))
			{
				// C1Description table
				//
				htDesc.Add("C1Description text", "***");

				// C1Category table
				//
				htCat.Add("C1Category text", "***");

				// GetString table
				//
				htGetStr.Add("An argument of GetString() function", "***");

				// Localizable Form table
				//
				Hashtable htForm1 = new Hashtable();
				htForm1.Add("Item 1", "***");
				htForm1.Add("Item 2", "***");
				htForms.Add("Name of the Form to be localized", htForm1);
			}
		}
	}
}
