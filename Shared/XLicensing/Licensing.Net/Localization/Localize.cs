//----------------------------------------------------------------------------
// C1.Util.Localization
//----------------------------------------------------------------------------
//
// C1Localizer, C1DescriptionAttribute, C1CategoryAttribute
//
//----------------------------------------------------------------------------
// Copyright (C) 2001 ComponentOne LLC
//----------------------------------------------------------------------------
// Status			Date			By	Comments
//----------------------------------------------------------------------------
// Created			May 20, 2002	Bernardo
// Modified			Apr 11, 2005	GaryH - CompactFramework additions
// Modified			Aug 29, 2006	GaryH - Added Menu handling for dropdown items
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Windows.Forms;

namespace C1.Util.Localization
{
    //----------------------------------------------------------------------------------
    #region ** designer attributes

    /// <summary>
    /// C1DescriptionAttribute replaces the DescriptionAttribute
    /// and uses the C1Localizer class to return the localized Attribute string
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    internal class C1DescriptionAttribute : DescriptionAttribute
    {
        string _key;
        public C1DescriptionAttribute(string key)
        {
            _key = key;
            base.DescriptionValue = key;
        }
        public C1DescriptionAttribute(string key, string description)
        {
            _key = key;
            base.DescriptionValue = description;
        }
        override public string Description
        {
            get
            {
                // if we have a key, use it to retrieve the localized value
                if (_key != null)
                {
                    // get the localized value
                    string s = C1Localizer.GetStringDescription(_key);

                    // if we got it, save it
                    if (s != null) base.DescriptionValue = s;

                    // and don't look again
                    _key = null;
                }
                return base.Description;
            }
        }
    }

    /// <summary>
    /// C1CategoryAttribute replaces the CategoryAttribute
    /// and uses the C1Localizer class to return the localized Attribute string
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    internal class C1CategoryAttribute : CategoryAttribute
    {
        public C1CategoryAttribute(string name)
            : base(name)
        {
        }
        override protected string GetLocalizedString(string value)
        {
            string s = C1Localizer.GetStringCategory(value);
            return s != null
                ? s
                : value;
        }
    }

    #endregion

    //----------------------------------------------------------------------------------
    #region ** C1Localizer

    /// <summary>
    /// Localization tables and methods for looking up localized strings.
    /// </summary>
    internal partial class C1Localizer
    {
        // localization tables
        static Hashtable _htDesc = null;
        static Hashtable _htCat = null;
        static Hashtable _htGetStr = null;
        static Hashtable _htForms = null;

        // initialize localized string tables
        static void InitTables()
        {
#if !LOCALIZE_TO_CURRENT_CULTURE
            var culture = CultureInfo.CurrentUICulture;
#else
            var culture = CultureInfo.CurrentCulture;
#endif
            _htDesc = new Hashtable();
            _htCat = new Hashtable();
            _htGetStr = new Hashtable();
            _htForms = new Hashtable();

#if GRAPECITY
            StringTables.InitTables(_htDesc, _htCat, _htGetStr, _htForms, "ja");
#else
            StringTables.InitTables(_htDesc, _htCat, _htGetStr, _htForms, culture.Name);
#endif
        }

        // -- string localization

        internal static string GetStringDescription(string key)
        {
            if (_htDesc == null) InitTables();
            return (string)_htDesc[key]; // return null if not found
        }
        internal static string GetStringCategory(string key)
        {
            if (_htCat == null) InitTables();
            return (string)_htCat[key]; // return null if not found
        }
        internal static string GetString(string key)
        {
            if (_htGetStr == null) InitTables();
            var val = (string)_htGetStr[key];
            return val != null ? val : key; // return *key* if not found
        }

        //--------------------------------------------------------------------------
        // ** Forms localization

        // localize a form (controls and components) using data in
        // static form string table
        internal static void LocalizeForm(Control frm, IContainer components)
        {
            if (_htForms == null)
            {
                InitTables();
            }
            if (_htForms != null && _htForms.Contains(frm.Name))
            {
                Hashtable ht = (Hashtable)_htForms[frm.Name];
                LocalizeControl((Control)frm, components, ht);
            }
        }

        // localize a form (controls and components) using data in static form 
        // string tables, including tables of ancestor controls.
        //
        // miscObjects contains an additional list of objects that must be 
        // localized but can't be retrieved automatically as form control 
        // members (e.g. ContextMenu, IList, Listview.ColumnHeaderCollection).
        // These objects are localized using the LocalizeMiscObject method.
        //
        internal static void LocalizeInheritedForm(Control frm, IContainer components, object[] miscObjects)
        {
            if (_htForms == null) InitTables();
            if (_htForms != null)
            {
                Type baseCtrlType = frm.GetType();
                string curClassName = frm.Name;
                while (true)
                {
                    if (_htForms.Contains(curClassName))
                    {
                        Hashtable ht = (Hashtable)_htForms[curClassName];
                        LocalizeControl(frm, components, ht);
                        if (miscObjects != null)
                        {
                            foreach (object miscObj in miscObjects)
                                LocalizeMiscObject(miscObj, ht);
                        }
                    }
                    baseCtrlType = baseCtrlType.BaseType;
                    if (typeof(Control).IsAssignableFrom(baseCtrlType))
                    {
                        curClassName = baseCtrlType.FullName.Replace(baseCtrlType.Namespace + ".", string.Empty);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
        internal static void LocalizeInheritedForm(Control frm, IContainer components)
        {
            LocalizeInheritedForm(frm, components, null);
        }

        // localize controls
        internal static void LocalizeControl(Control ctl, IContainer components, Hashtable ht)
        {
            // localize the text for this control
            string s = Lookup(ctl.Text, ht);
            if (s != null) ctl.Text = s;

            // localize context menu items
#if !CORE31
            if (ctl.ContextMenu != null)
            {
                LocalizeContextMenuItems(ctl.ContextMenu.MenuItems, ht);
            }
#endif
            if (ctl.ContextMenuStrip != null)
            {
                LocalizeContextMenuStipItems(ctl.ContextMenuStrip.Items, ht);
            }

            // create a fake 'components' to handle UserControls with tooltips
            if (ctl is UserControl && components == null)
            {
                foreach (FieldInfo fi in ctl.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    if (fi.FieldType == typeof(ToolTip))
                    {
                        ToolTip tip = fi.GetValue(ctl) as ToolTip;
                        if (components == null)
                            components = new Container();
                        components.Add(tip);
                    }
                }
            }

            // localize any tooltips that reference this control
            if (components != null)
            {
                foreach (IComponent cmp in components.Components)
                {
                    ToolTip tip = cmp as ToolTip;
                    if (tip != null)
                    {
                        s = tip.GetToolTip(ctl);
                        s = Lookup(s, ht);
                        if (s != null) tip.SetToolTip(ctl, s);
                    }
                }
            }

            // custom treatment for linklabels
            // mark the LinkArea with pointy brackets (e.g. "click <here> to go")
            // or set the whole text as a link
            if (ctl is LinkLabel)
            {
                s = ctl.Text;
                LinkLabel ll = null;
                if (ctl is LinkLabel)
                    ll = (LinkLabel)ctl;

                int start = s.IndexOf('<');
                int end = s.IndexOf('>');
                if (start > -1 && end > -1 && end > start)
                {
                    s = s.Replace("<", "");
                    s = s.Replace(">", "");
                    ll.Text = s;
                    ll.LinkArea = new LinkArea(start, end - start - 1);
                }
                else
                {
                    ll.LinkArea = new LinkArea(0, s.Length);
                }
            }

            // custom treatment for toolbars
#if !CORE31
            if (ctl is ToolBar)
            {
                foreach (ToolBarButton tbb in ((ToolBar)ctl).Buttons)
                {
                    s = Lookup(tbb.Text, ht);
                    if (s != null) tbb.Text = s;
                    s = Lookup(tbb.ToolTipText, ht);
                    if (s != null) tbb.ToolTipText = s;

                    // handle dropdown toolbars
                    if (tbb.DropDownMenu != null && tbb.DropDownMenu.MenuItems != null)
                        LocalizeContextMenuItems(tbb.DropDownMenu.MenuItems, ht);
                }
            }
#endif
            if (ctl is ToolStrip)
            {
                foreach (ToolStripItem tbb in ((ToolStrip)ctl).Items)
                {
                    s = Lookup(tbb.Text, ht);
                    if (s != null) tbb.Text = s;
                    s = Lookup(tbb.ToolTipText, ht);
                    if (s != null) tbb.ToolTipText = s;

                    // handle dropdown toolbars
                    var subItems = (tbb as ToolStripDropDownItem)?.DropDownItems;
                    if (subItems != null)
                        LocalizeContextMenuStipItems(subItems, ht);
                }
            }

            // custom treatment for toolstrips
            if (ctl is ToolStrip)
                LocalizeToolStripItems(((ToolStrip)ctl).Items, ht);

            if (ctl is StatusStrip)
                LocalizeToolStripItems(((StatusStrip)ctl).Items, ht);

            // custom treatment for tab pages
            if (ctl is TabPage)
            {
                TabPage tp = (TabPage)ctl;
                s = Lookup(tp.ToolTipText, ht);
                if (s != null) tp.ToolTipText = s;
            }

            // localize Form's main menu
            Form f = ctl as Form;
#if !CORE31
            if (f != null && f.Menu != null)
                LocalizeContextMenuItems(f.Menu.MenuItems, ht);

#endif
            if (f != null && f.MainMenuStrip != null)
                LocalizeContextMenuStipItems(f.MainMenuStrip.Items, ht);

            // localize DataGrid 
#if !CORE31
            DataGrid grid = ctl as DataGrid;
            if (grid != null)
                LocalizeDataGridTableStyles(grid.TableStyles, ht);
#endif
            DataGridView gridv = ctl as DataGridView;
            if (gridv != null)
                LocalizeDataGridViewTableStyles(gridv, ht);

            // and scan all children
            foreach (Control c in ctl.Controls)
            {
                LocalizeControl(c, components, ht);
            }
        }

#if !CORE31
        internal static void LocalizeContextMenuItems(Menu.MenuItemCollection items, Hashtable ht)
        {
            foreach (MenuItem item in items)
            {
                string s = Lookup(item.Text, ht);
                if (s != null) item.Text = s;
                if (item.MenuItems != null)
                    LocalizeContextMenuItems(item.MenuItems, ht);
            }
        }
#endif
        internal static void LocalizeContextMenuStipItems(ToolStripItemCollection items, Hashtable ht)
        {
            foreach (ToolStripItem item in items)
            {
                string s = Lookup(item.Text, ht);
                if (s != null) item.Text = s;
                var subItems = (item as ToolStripDropDownItem)?.DropDownItems;
                if (subItems != null)
                    LocalizeContextMenuStipItems(subItems, ht);
            }
        }

        // Localize a specified object in case if this method has a knowledge how to do it.
        // Currently supported object types:
        //   ContextMenu
        //   IList with string items
        //   ListView.ColumnHeaderCollection 
        static void LocalizeMiscObject(object obj, Hashtable ht)
        {
#if !CORE31
            if (obj is ContextMenu)
            {
                LocalizeContextMenuItems(((ContextMenu)obj).MenuItems, ht);
            }
#endif
            if (obj is ContextMenuStrip)
            {
                LocalizeContextMenuStipItems(((ContextMenuStrip)obj).Items, ht);
            }
            else if (obj is ListView.ColumnHeaderCollection)
            {
                LocalizeListViewColumnHeaders((ListView.ColumnHeaderCollection)obj, ht);
            }
            else if (obj is FileDialog)
            {
                LocalizeFileDialog((FileDialog)obj, ht);
            }
            else if (obj is IList) // IMPORTANT: keep this at the end of the type determination sequence
            {
                LocalizeStringIList((IList)obj, ht);
            }
        }
        static void LocalizeListViewColumnHeaders(ListView.ColumnHeaderCollection headers, Hashtable ht)
        {
            if (headers != null)
            {
                foreach (ColumnHeader header in headers)
                {
                    string s = Lookup(header.Text, ht);
                    if (s != null)
                        header.Text = s;
                }
            }
        }
        static void LocalizeFileDialog(FileDialog dialog, Hashtable ht)
        {
            if (dialog != null)
            {
                string s = Lookup(dialog.Title, ht);
                if (s != null)
                    dialog.Title = s;
                s = Lookup(dialog.Filter, ht);
                if (s != null)
                    dialog.Filter = s;
            }
        }
#if !CORE31
        static void LocalizeDataGridTableStyles(GridTableStylesCollection styles, Hashtable ht)
        {
            if (styles != null)
            {
                foreach (DataGridTableStyle style in styles)
                    LocalizeDataGridColumnStyles(style.GridColumnStyles, ht);
            }
        }
        static void LocalizeDataGridColumnStyles(GridColumnStylesCollection styles, Hashtable ht)
        {
            if (styles != null)
            {
                foreach (DataGridColumnStyle style in styles)
                {
                    string s = Lookup(style.HeaderText, ht);
                    if (s != null)
                        style.HeaderText = s;
                }
            }
        }
#endif
        static void LocalizeDataGridViewTableStyles(DataGridView view, Hashtable ht)
        {
            if (view?.Columns != null)
            {
                foreach (DataGridViewColumn col in view.Columns)
                {
                    string s = Lookup(col.HeaderText, ht);
                    if (s != null)
                        col.HeaderText = s;
                }
            }
        }
        static void LocalizeToolStripItems(ToolStripItemCollection tsic, Hashtable ht)
        {
            if (tsic != null)
            {
                foreach (ToolStripItem tsi in tsic)
                {
                    string s = Lookup(tsi.Text, ht);
                    if (s != null) tsi.Text = s;
                    s = Lookup(tsi.ToolTipText, ht);
                    if (s != null) tsi.ToolTipText = s;

                    if (tsi is ToolStripDropDownItem)
                        LocalizeToolStripItems(((ToolStripDropDownItem)tsi).DropDownItems, ht);

                    else if (tsi is ToolStripComboBox)
                        LocalizeStringIList(((ToolStripComboBox)tsi).Items, ht);
                }
            }
        }

        // back to string lookup/localization
        internal static string Lookup(string key, Hashtable ht)
        {
            return string.IsNullOrEmpty(key)
                ? null
                : (string)ht[key];
        }
        static void LocalizeStringIList(IList list, Hashtable ht)
        {
            if (list != null && !list.IsReadOnly)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string curText = list[i] as string;
                    if (curText != null)
                    {
                        curText = Lookup(curText, ht);
                        if (curText != null)
                            list[i] = curText;
                    }
                }
            }
        }
    }
#endregion
}
