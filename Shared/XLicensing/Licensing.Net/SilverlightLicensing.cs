﻿// 
// This file should be included in Silverlight controls.
// 
// It provides:
// 1) The C1ProductInfo class used by designers to identify the control as
//    a ComponentOne licensed control, and
// 2) The LicenseMode.Evaluation property which is attached to the control
//    and causes it to nag when the control is not licensed.
// 
using System;
using System.Windows;
using System.Diagnostics;

namespace C1.Util.Licensing
{
    /// <summary>
    /// Attribute used to attach licensing/product information to assemblies.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    internal class C1ProductInfoAttribute : Attribute
    {
        string _productCode;
        string _productGUID;
        bool _primary;

        private void init(string productCode, string productGUID, bool primary)
        {
            Debug.Assert(productCode.Length == 2 || productCode == null, "Invalid productCode");
            Debug.Assert(productGUID.Length == 36, "Invalid productGUID");
            _productCode = productCode;
            _productGUID = productGUID;
            _primary = primary;
        }
        public C1ProductInfoAttribute(string productCode, string productGUID, bool primary)
        {
            init(productCode, productGUID, primary);
        }
        public C1ProductInfoAttribute(string productCode, string productGUID)
        {
            init(productCode, productGUID, false);
        }
        public string ProductCode
        {
            get { return _productCode; }
        }
        public string ProductGUID
        {
            get { return _productGUID; }
        }
        public bool Primary
        {
            get { return _primary; }
        }
    }

#if (!WINRT && (FLEXGRID || ORGCHART || C1DATASOURCE)) // <IP> other assemblies use NagScreen
    /// <summary>
    /// Provides the licensing mode connection between a control and its designer.
    /// </summary>
    public class LicenseMode
    {
        // nag only once please
        static bool _alreadyNagged;

        /// <summary>
        /// This property is for internal use only.
        /// </summary>
        public static bool GetEvaluation(DependencyObject obj)
        {
            return (bool)obj.GetValue(EvaluationProperty);
        }
        /// <summary>
        /// This property is for internal use only.
        /// </summary>
        public static void SetEvaluation(DependencyObject obj, bool value)
        {
            obj.SetValue(EvaluationProperty, value);
        }
        /// <summary>
        /// This property is for internal use only.
        /// </summary>
        public static readonly DependencyProperty EvaluationProperty =
            DependencyProperty.RegisterAttached(
                "Evaluation", 
                typeof(bool), 
                typeof(LicenseMode), 
                new PropertyMetadata((s, e) =>
                {
                    var element = (FrameworkElement)s;
                    // <<IP>> should not be shown in design time
                    if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(s))
                        return;
                    element.Loaded += element_Loaded;
                }));

        static void element_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_alreadyNagged)
            {
                _alreadyNagged = true;
#if (GRAPECITY)
                        MessageBox.Show("ComponentOne Studio Silverlight Edition トライアル版\r\n" +
                             "ライセンスのご購入は弊社直販もしくは販売パートナーをご利用ください。\r\n\r\n" +
                             "    http://www.grapecity.co.jp/developer",
                             "ComponentOne", MessageBoxButton.OK);
#else
                        MessageBox.Show("Thank you for evaluating ComponentOne Studio Silverlight Edition.\r\n" +
                            "When you are ready to purchase the product, please visit our store at:\r\n\r\n" +
                            "    https://www.grapecity.com/pricing",
                            "ComponentOne", MessageBoxButton.OK);
#endif
            }
        }
    }
#endif
}
