﻿//----------------------------------------------------------------------------
// AppAboutForm.cs
//
// This is based on AboutForm, but adapted to use in C1 applications
// (such as C1Report designer app or C1Theme designer app).
//----------------------------------------------------------------------------
using System;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Collections.Generic;

#if !DESIGNER_SAMPLE
// Note - in C1ReportDesigner's own about box:
// "ja":
// - OK was <value>OK</value>
// "ko":
// - OK was <value>확인</value>
// "zh":
// - OK was <value>确认</value>
#if from_theme_designer_app
  <data name="AboutForm.AboutFmt" xml:space="preserve">
    <value>{0} のバージョン情報</value>
    <comment>About {0}</comment>
  </data>
  <data name="AboutForm.AppNameFmt" xml:space="preserve">
    <value>{0} アプリケーション（{1} ビットモード）:</value>
    <comment>{0} Application ({1} bit mode):</comment>
  </data>
  <data name="AboutForm.AsmbVersionFmt" xml:space="preserve">
    <value>{0}、バージョン {1}</value>
    <comment>{0}, Version {1}</comment>
  </data>
  <data name="Forms.AboutForm._lblResources.Text" xml:space="preserve">
    <value>オンラインリソース:</value>
    <comment>Online Resources:</comment>
  </data>
  <data name="Forms.AboutForm._linkHome.Text" xml:space="preserve">
    <value>ホーム</value>
    <comment>Home</comment>
  </data>
#endif
#endif

namespace C1.Util.Licensing
{
	internal partial class AppAboutForm : System.Windows.Forms.Form
	{
#if !DESIGNER_SAMPLE
#if GRAPECITY
        const string C1_ROOT = "http://www.grapecity.co.jp/developer";

        const string c_aboutFmt = "{0} のバージョン情報";
        const string c_asmNameVerFmt = "{0}、バージョン {1}";
        const string c_appNameModeFmt = "{0} アプリケーション ({1} bit モード):";
#else
        const string c_aboutFmt = "About {0}";
        const string c_asmNameVerFmt = "{0}, Version {1}";
        const string c_appNameModeFmt = "{0} Application ({1} bit mode):";
        const string C1_ROOT = "https://www.grapecity.com/en/componentone";
#endif

#if !(CLR40 || CLR45)
        public delegate TResult Func<in T, out TResult>(T arg);
#endif
#else
        const string C1_ROOT = "https://www.grapecity.com/en/componentone";

        const string c_aboutFmt = "About {0}";
        const string c_asmNameVerFmt = "{0}, Version {1}";
        const string c_appNameModeFmt = "{0} Application ({1} bit mode):";
#endif

        internal AppAboutForm()
		{
			// regular initialization
			InitializeComponent();

#if !DESIGNER_SAMPLE
#if GRAPECITY
            // localize without C1Localize because:
            // - this is a special case, we need to show/hide controls and handle links.
            // - to reduce dependencies within our own code.

            // show GC info, hide ours
            _panelLinks.Visible = false;
            _panelGC.Visible = true;

            // just in case...
            this._linkGC.Text = C1_ROOT;
            _lblResources.Text = "開発元：";
		    _lblResources.Location = new Point(_lblResources.Left, label1.Top);

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppAboutForm));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.GCBackgroundImage")));
#endif
#endif
            Text = c_aboutFmt;

            _linkHome.Text = C1_ROOT.Replace("http://", "");

            // show main exe and all other loaded C1 assemblies:
            Func<AssemblyName, string> getName = (asmName) => string.Format(c_asmNameVerFmt, asmName.Name, asmName.Version);
            //
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            var entryName = entryAssembly.GetName();
            int year = entryName.Version.Build / 10;
            _copyright.Text = string.Format(_copyright.Text, year);
            //
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(c_appNameModeFmt, entryName.Name, IntPtr.Size * 8);
            sb.AppendLine();
            sb.AppendLine(getName(entryName));
            // get a sorted list of all C1 assemblies:
            var asms = AppDomain.CurrentDomain.GetAssemblies();
            List<string> names = new List<string>();
            foreach (Assembly asm in asms)
            {
                if (asm == entryAssembly)
                    continue;
                var name = asm.GetName();
                if (name.Name.ToLower().StartsWith("c1"))
                    names.Add(getName(name));
            }
            names.Sort(StringComparer.OrdinalIgnoreCase);
            names.ForEach((s_) => sb.AppendLine(s_));

            //
            _txtVersionInfo.Text = sb.ToString();
        }

        #region Public interface
        public AppAboutForm(string productName)
            :this()
        {
            _prodName.Text = productName;
            Text = string.Format(Text, productName);
        }

        public void AddInfo(string additionalInfo)
        {
            _txtVersionInfo.Text += additionalInfo;
        }

        public string Info
        {
            get { return _txtVersionInfo.Text; }
            set { _txtVersionInfo.Text = value; }
        }

        public Button ButtonOK { get { return _btnOk; } }

        public TextBox TextBoxInfo { get { return _txtVersionInfo; } }
        #endregion

        #region Hyperlinks
        private void resource_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(C1_ROOT);		// argument should be const string to avoid Veracode issue
        }

        private void _linkGC_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(C1_ROOT);		// argument should be const string to avoid Veracode issue
        }
        #endregion

        #region Form moving
        // from http://stackoverflow.com/a/24561946/4608039
        private bool _movingMouseDown;
        private Point _movingLastLocation;
        private void Mover_MouseDown(object sender, MouseEventArgs e)
        {
            // this is ugly but better than either immovable window or X that doesn't close it:
            if (e.Location.X > pictureBox1.Width - pictureBox1.Height)
                this.Close();
            else
            {
                _movingMouseDown = true;
                _movingLastLocation = e.Location;
            }
        }

        private void Mover_MouseMove(object sender, MouseEventArgs e)
        {
            if (_movingMouseDown)
            {
                this.Location = new Point((this.Location.X - _movingLastLocation.X) + e.X, (this.Location.Y - _movingLastLocation.Y) + e.Y);
                this.Update();
            }
        }

        private void Mover_MouseUp(object sender, MouseEventArgs e)
        {
            _movingMouseDown = false;
        }
        #endregion

    }
}
