﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

#pragma warning disable CS0436
namespace C1.Util.Licensing
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    internal partial class AboutForm : Form
    {
        // ** fields
        string      _productName;   // name of product that owns the about box
        Type        _componentType; // type of component that owns the about box
        LicenseInfo _licInfo;       // license information displayed in dialog

        // Studio Enterprise GUID (used to show/hide the label that talks about SE)
        const string SE_GUID = "724e8a91-af12-4a3b-9aeb-ef89612e692e";

        // used for most links
#if GRAPECITY
        const string C1_ROOT = "http://www.grapecity.co.jp/developer";
#else
        const string C1_ROOT = "https://www.grapecity.com/en/componentone";
#endif

        // ** initialize
        public AboutForm()
        {
            InitializeComponent();
#if GRAPECITY
            InitializeComponentGC();
#endif
        }
        internal AboutForm(Type type, LicenseInfo licInfo) : this(
            type, 
            licInfo, 
            LicenseManager.CurrentContext.UsageMode == LicenseUsageMode.Designtime)
        {
        }
        internal AboutForm(Type type, LicenseInfo licInfo, bool designTime)
        {
            InitializeComponent();
#if GRAPECITY
            InitializeComponentGC();
#endif
            // save component type
            // (so we can find the assembly later in case it's not this one)
            _componentType = type;

            // get product name
            var asm = type.Assembly;
            foreach (object att in type.GetCustomAttributes(typeof(C1AboutNameAttribute), true))
            {
                _productName = ((C1AboutNameAttribute)att).AboutName;
            }
            if (_productName == null)
            {
                var productNameAtt = (AssemblyProductAttribute)Attribute.GetCustomAttribute(asm, typeof(AssemblyProductAttribute));
                _productName = productNameAtt.Product;
            }
            if (_productName.StartsWith("ComponentOne", StringComparison.OrdinalIgnoreCase))
            {
                _productName = _productName.Substring(12);
            }
            _productName = _productName.Trim();

            ActiveControl = _btnOK;

            // show product name
            SetControlText(this, _productName);
            SetControlText(_lblProductName, _productName);

            // show product version
            var version = asm.FullName.Split(',')[1].Split('=')[1];
            SetControlText(this, version);
            SetControlText(_lblProductVersion, version);

            // show copyright notice
            var versionYear = version.Split('.')[2];
            versionYear = versionYear.Length == 5 ? versionYear.Substring(0, 4) : "???";
            SetControlText(_lblCopyright, versionYear);

            // get license state (if not provided, then assume design time)
            _licInfo = licInfo;
            if (_licInfo == null)
            {
                designTime = true;
                try
                {
#if BETA_LICENSE
                    // BETA is always unlicensed.
                    _licInfo = new LicenseInfo(type, LicenseStatus.Unlicensed);
#else
                    _licInfo = ProviderInfo.ValidateDesigntime(type, null);
#endif
                }
                catch
                {
                    // GrapeCity licensing is very finicky...
                    _licInfo = new LicenseInfo(type, LicenseStatus.Unlicensed);
                    _licInfo.License = new ProductLicense();
                }
            }

            // 1) Valid license, easy to show
            if (_licInfo.LicenseStatus == LicenseStatus.Valid)
            {
#if GRAPECITY
        var user = string.Format("{0}\r\n{1}",
                    _licInfo.License.UserName, _licInfo.License.CompanyName);
#else
        string user = (_licInfo.License != null)
                    ? string.Format("{0}\r\n{1}", _licInfo.License.UserName, _licInfo.License.CompanyName)
                    : null;
                if (_licInfo != null && _licInfo.License != null && string.IsNullOrEmpty(_licInfo.License.UserName))
                {
                    _lblStateValid.Text = "Licensed with: \r\n     {0}";
                    user = _licInfo.License.CompanyName;
                }
#endif
        SetControlText(_lblStateValid, user);
                ShowLicenseStatus(_lblStateValid);
            }

            // 2) Nag at runtime
            else if (!designTime)
            {
				Label lblState = (licInfo.GetStop && licInfo.EvaluationDaysLeft <= 0) ? _lblStateEvalOver : _lblStateNag;
                ShowLicenseStatus(lblState);
            }

            // 3) License has expired
            else if (_licInfo.LicenseStatus == LicenseStatus.Expired)
            {
                var expDate = new DateTime(2000 + _licInfo.License.Year, _licInfo.License.Quarter * 4, 1);
                expDate = expDate.AddYears(1);
                SetControlText(_lblStateExpired, expDate);
                ShowLicenseStatus(_lblStateExpired);
            }

            // 4) Evaluation period
            else
            {
                var evalDaysLeft = _licInfo.EvaluationDaysLeft;
                if (evalDaysLeft > 0)
                {
#if BETA_LICENSE
#if GRAPECITY
                    // The date must always be Japanese.
                    System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("ja-JP");
                    string longDateJapanese = DateTime.Today.AddDays(evalDaysLeft).ToString("D", ci);
                    _lblStateEval.Text = "このアセンブリはベータ版です。あと{0}日（" + longDateJapanese + "）で提供期間が終了します。";
#else
                    _lblStateEval.Text = "This assembly is currently in BETA testing and will expire\r\nin {0} days (on " +
                        DateTime.Today.AddDays(evalDaysLeft).ToLongDateString() + ").";
#endif
#endif
                    SetControlText(_lblStateEval, evalDaysLeft);
                    ShowLicenseStatus(_lblStateEval);
                }
                else
                {
#if BETA_LICENSE
#if GRAPECITY
                    // This is a google translation.  Probably a bad one.
					_lblStateEvalOver.Text = "このアセンブリのベータ版の提供期間は終了しました。" + "\r\n" +
						"本製品を継続して使用するには、最新バージョンを取得する必要があります。詳しくは、弊社までお問い合わせください。";
#else
                    _lblStateEvalOver.Text = "The BETA test period for this assembly has ended.\r\n" +
                        "To continue using this product, please visit our website or contact us to obtain the latest version.";
#endif
#endif
                    ShowLicenseStatus(_lblStateEvalOver);
                }
            }
        }
        void SetControlText(Control ctl, object value)
        {
            var fmt = ctl.Text;
            ctl.Text = string.Format(fmt, value);
        }
        void ShowLicenseStatus(Label showLabel)
        {
            foreach (Label label in _panelLicenseInfo.Controls)
            {
                label.Visible = label == showLabel;
            }
        }

        List<string> GetProductCodesFromAssembly(Assembly asm)
        {
            var list = new List<string>();
            foreach (Attribute att in asm.GetCustomAttributes(true))
            {
                if (att.GetType().Name == "C1ProductInfoAttribute")
                {
                    var pi = att.GetType().GetProperty("ProductCode");
                    if (pi != null)
                    {
                        list.Add(pi.GetValue(att, null).ToString());
                    }
                }
            }
            return list;
        }

        void CloseOn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
 	        base.OnPaint(e);
            e.Graphics.DrawRectangle(Pens.Black, 0, 0, ClientSize.Width - 1, ClientSize.Height - 1);
        }

        //--------------------------------------------------------------------------------
#region ** GrapeCity localization

#if GRAPECITY

        // localize without C1Localize because:
        // - this is a special case, we need to show/hide controls and handle links.
        // - to reduce dependencies within our own code.
        void InitializeComponentGC()
        {
            // show GC info, hide ours
            _panelLinks.Visible = false;
            _panelGC.Visible = true;

            //Text = "About {0}";
            Text = "{0} のバージョン情報";

            // just in case...
            this._linkGC.Text = C1_ROOT;

            //this._lblProductVersion.Text = "version {0}";
            this._lblProductVersion.Text = "バージョン {0}";
            this._lblProductVersion.Font = new System.Drawing.Font("メイリオ", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));

            //_lblResources.Text = "Online Resources";
            _lblResources.Text = "開発元：";

            // _lblStateEval
            //_lblStateEval.Text = "Not licensed.\r\nYou can use this product for evaluation purposes for an additional {0} days.\r\nPlease visit our website or contact us to purchase a license.";
            _lblStateEval.Text = "トライアル版\r\n" + "本製品の評価を目的に残り{0}日使用できます。";
            _lblStateEval.Font = new System.Drawing.Font("メイリオ", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            // _lblStateEvalOver
            //_lblStateEvalOver.Text = "Your trial period has expired.\r\nPlease contact us or visit our site to purchase a license.\r\nVisit https://www.grapecity.com/en/licensing/componentone to learn how to extend your trial another 30 days.";
            _lblStateEvalOver.Text = "トライアル版\r\n" + "試用期間が過ぎています。ライセンスの購入が必要です。";
            _lblStateEvalOver.Font = new System.Drawing.Font("メイリオ", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            // _lblStateExpired
            //_lblStateExpired.Text = "Your license expired in {0:MMMM yyyy}.\r\nTo use this version, please renew your license and rebuild the application.";
            //_lblStateExpired.Text = "{0:yyyy年MM月} にライセンスの有効期間が切れています。\r\n" + "このバージョンを使用する場合は、ライセンスを更新し、\r\nアプリケーションをリビルドしてください。";
            _lblStateExpired.Text = "{0:yyyy年MM月}でライセンスの有効期限が切れております。\r\n" + "このバージョンの製品版の利用を継続する場合は、\r\n" +
                "ライセンスを購入またはリニューアル手続き後、\r\n" + "アプリケーションをリビルドしてください。";
			_lblStateExpired.Font = new System.Drawing.Font("メイリオ", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));


            // _lblStateValid
            //_lblStateValid.Text = "This product is licensed to:\r\n{0}\r\n";
            _lblStateValid.Text = "本製品は次のユーザーに使用を許諾しています:\r\n{0}";
            _lblStateValid.Font = new System.Drawing.Font("メイリオ", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            // _lblStateNag
            //_lblStateNag.Text = "Evaluation version.\r\nThis dialog will not be shown if you rebuild your application using a licensed version of the product.";
            _lblStateNag.Text = "トライアル版\r\n" + "このダイアログは正規の製品版を使用して\r\nアプリケーションをリビルドすることで表示されなくなります。";
            _lblStateNag.Font = new System.Drawing.Font("メイリオ", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.GCBackgroundImage")));
        }
#endif
#endregion
    }
}
#pragma warning restore CS0436
