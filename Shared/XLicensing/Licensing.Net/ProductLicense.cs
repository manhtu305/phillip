﻿///////////////////////////////////////////////////////////////
//
// ProductLicense.cs
//
// -----------------------------------------------------------------------
// History:
// -----------------------------------------------------------------------
// Dec  2011 (BJC):
// 
// - changed GetDesignTimeLicenseStatus to support two GC attributes:
//      SupportedProductAttribute (Silverlight) and 
//      GCLicenseInfoAttribute (everything else)
//      [fixes TFS 18217]
//   
///////////////////////////////////////////////////////////////
#define LEGACY_LICENSING
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Security.Cryptography;
using Microsoft.Win32;
using System.Reflection;

#pragma warning disable CS0436
namespace C1.Util.Licensing
{
    /// <summary>
    /// Represents a license for a product (e.g. C1BarCode, C1Studio Enterprise).
    /// 
    /// Provides methods for:
    /// - generating new license keys (administrative task)
    /// - checking license key validity (used by installer and products)
    /// - generating and checking runtime keys (used by products)
    /// - installing/uninstalling licenses (used by installer)
    /// 
    /// The install/uninstall code requires elevated permissions, and is
    /// used only by setups and utilities (not by controls). The code is 
    /// implemented in the ProductLicense.Installer.cs, which controls should
    /// not include since it won't be useful to them.
    /// 
    /// When a product is sold, we send the user a license KEY. The user installs
    /// a license on his machine by providing his name, his company name, and the 
    /// key.
    /// 
    /// The <see cref="Key"/> is a string in the following format:
    /// 
    ///     AAQYYCC-XX-NNNNNN-DD-DZZZZZZ
    ///     
    /// Where:
    /// 
    ///     AA:     Product code (should match one of the products in the 
    ///             product catalog (see ProductInformation.GetProductCatalog()).
    ///     Q:      Quarter when the license key was issued (1, 2, or 3)
    ///     YY:     Year when the license key was issued (e.g., 10 for 2010)
    ///     CC:     Order code, two chars that indicate whether this is a 
    ///             new sale, upgrade, renewal, special offer, etc.
    ///     XX:     Hash code to ensure key validity.
    ///     NNNNNN: Unique serial number.
    ///     DDD:    Vendor code, three chars
    ///     ZZZZZZ: Combination of random numbers for uniqueness and extra hash.
    ///     
    /// </summary>
    internal partial class ProductLicense
    {
        //------------------------------------------------------------
        #region ** fields

        string  _code = "SE";
        int     _quarter = 1;
        int     _year = 9;
        string  _orderCode = "00";
        string  _vendorCode = "000";
        string  _userName = string.Empty;
        string  _companyName = string.Empty;
        int     _serial = 0;
        Guid    _guid;
        bool    _expired;
        string  _key; // without random noise

        const string BAD_SERIAL     = "Invalid Serial Number.";
        const string BAD_RUNTIME    = "Invalid Runtime Key.";
        const string LICENSES       = "Licenses";
        const string LICENSESWOW    = @"Software\Wow6432Node\Classes\Licenses";

        #endregion

        //------------------------------------------------------------
        #region ** license information

        internal string ProductCode
        {
            get { return _code; }
            set 
            {
                if (string.IsNullOrEmpty(value) || value.Length != 2)
                {
                    throw new ArgumentException(); // "ProductCode must be two digits");
                }
                if (value != ProductCode)
                {
                    _code = value;
                    _key = null;
                }
            }
        }
        internal int Quarter
        {
            get { return _quarter; }
            set
            {
                if (value < 1 || value > 3)
                {
                    throw new ArgumentException(); // "Quarter must be 1, 2, or 3.");
                }
                if (value != Quarter)
                {
                    _quarter = value;
                    _key = null;
                }
            }
        }
        internal int Year
        {
            get { return _year; }
            set
            {
                if (value < 0 || value > 99)
                {
                    throw new ArgumentException(); // "Year must be between 0 and 99");
                }
                if (value != _year)
                {
                    _year = value;
                    _key = null;
                }
            }
        }
        internal string OrderCode
        {
            get { return _orderCode; }
            set 
            {
                if (value == null || value.Length != 2)
                {
                    throw new ArgumentException(); // "OrderCode must be two characters long.");
                }
                if (value != OrderCode)
                {
                    _orderCode = value;
                    _key = null;
                }
            }
        }
        internal string VendorCode
        {
            get { return _vendorCode; }
            set
            {
                // TODO: define vendor table for validation?
                if (value == null || value.Length != 3)
                {
                    throw new ArgumentException(); // "VendorCode must be three characters long.");
                }
                if (value != VendorCode)
                {
                    _vendorCode = value;
                    _key = null;
                }
            }
        }
        internal int Serial
        {
            get { return _serial; }
            set 
            {
                if (value < 0 || value > 999999)
                {
                    throw new ArgumentException(); // "Serial must be between 0 and 999,999");
                }
                if (value != Serial)
                {
                    _serial = value;
                    _key = null;
                }
            }
        }
        internal string UserName
        {
            get { return _userName; }
            set 
            {
                _userName = value == null
                    ? string.Empty
                    : value.Replace('\t', ' '); 
            }
        }
        internal string CompanyName
        {
            get { return _companyName; }
            set 
            {
                _companyName = value == null
                    ? string.Empty
                    : value.Replace('\t', ' ');
            }
        }

        // build/parse user key (the string used for licensing, AKA SerialNumber)
        internal string Key
        {
            get
            {
                // return stored key if we have it
                if (_key != null)
                {
                    return _key;
                }

                // build product/date/ordercode string
                var s1 = string.Format("{0}{1}{2:00}{3}",
                    ProductCode,
                    Quarter,
                    Year,
                    OrderCode);

                // build serial number string
                var s2 = string.Format("{0:000000}", Serial);

                // combine both strings with hash and vendor code
                _key = string.Format("{0}{1}{2}{3}", s1, MakeCheck(s1, s2), s2, VendorCode);

                // append uniqueness string with embedded hash
                string randomDigits = MakeRandomString(4);
                string hash = MakeCheck(_key, randomDigits);
                int keymark = (randomDigits[0] & 1) != 0 ? 1 : 3;
                _key += string.Format("{0}{1}{2}", randomDigits.Substring(0, keymark), hash, randomDigits.Substring(keymark));

                // insert separators to go from 
                //  AAQYYCCXXNNNNNNDDDZZZZZZ to
                //  AAQYYCC-XX-NNNNNN-DD-DZZZZZZ
                _key = string.Format("{0}-{1}-{2}-{3}-{4}",
                    _key.Substring(0, 7),
                    _key.Substring(7, 2),
                    _key.Substring(9, 6),
                    _key.Substring(15, 2),
                    _key.Substring(17));

                // done 
                return _key;
            }
            set
            {
                // must match "AAQYYCC-XX-NNNNNN-DD-DZZZZZZ"
                if (!Regex.IsMatch(value, 
                    "[A-Za-z0-9]{2}" +      // AA:  product code (2 chars)
                    "[0-9]{3}" +            // QYY: quarter/year (3 digits)
                    "[A-Za-z0-9]{2}-" +     // CC:  order code (2 chars)
                    "[A-Za-z0-9]{2}-" +     // XX:  hash (2 chars)
                    "[0-9]{6}-" +           // N6:  serial (6 digits)
                    "[A-Za-z0-9]{2}-" +     // DD:  vendor code (2 first chars)
                    "[A-Za-z0-9]{7}"))      // DZ*: vendor code plus hash (7 chars)
                {
                    throw new ArgumentException(BAD_SERIAL);
                }

                // remove dashes: AAQYYCCXXNNNNNNDDDZZZZZZ
                var key = value.Replace("-", string.Empty);

                // check old hash
                var hash = MakeCheck(key.Substring(0, 7), key.Substring(9, 6));
                if (hash != key.Substring(7, 2))
                {
                    throw new ArgumentException(BAD_SERIAL);
                }

                // check new hash
                var randomDigits = key.Substring(18, 6);
                int keymark = (randomDigits[0] & 1) != 0 ? 1 : 3;
                hash = randomDigits.Substring(keymark, 2);
                randomDigits = randomDigits.Substring(0, keymark) + randomDigits.Substring(keymark + 2);
                if (MakeCheck(key.Substring(0, 18), randomDigits) != hash)
                {
                    throw new ArgumentException(BAD_SERIAL);
                }

                // parse elements
                ProductCode = key.Substring(0, 2);
                Quarter = int.Parse(key.Substring(2, 1));
                Year = int.Parse(key.Substring(3, 2));
                OrderCode = key.Substring(5, 2);
                Serial = int.Parse(key.Substring(9, 6));
                VendorCode = key.Substring(15, 3);

                // store key value provided
                _key = value;
            }
        }

        // gets a value that indicates whether the key was manufactured now or
        // whether it is an actual key stored in the registry (which can be activated/deactivated)
        internal bool HasActualKey
        {
            get { return !string.IsNullOrEmpty(_key); }
        }

        // build runtime key (for use by controls at runtime)
        internal string RuntimeKey
        {
            get
            {
                // build a string with the product code, user name, company name, and noise
                var rnd = new Random();
                var s = string.Format("{0}\t{1}\t{2}\t{3}",
                    ProductCode, UserName, CompanyName, rnd.Next(0xff));

                // encrypt that
                return Encrypt(s, true);
            }
            set
            {
                // decrypt string and parse pieces
                var s = Decrypt(value);
                if (s != null)
                {
                    string[] parts = s.Split('\t');
                    if (parts.Length == 4 && parts[0].Length == 2)
                    {
                        ProductCode = parts[0];
                        UserName = parts[1];
                        CompanyName = parts[2];
                        return;
                    }
                }
#if LEGACY_LICENSING
                // failed to parse new key, try old
                var data = Convert.FromBase64String(value);
                s = Encoding.Unicode.GetString(data);
                s = TextDecrypt(s, "86525597");
                if (s != null)
                {
                    ProductCode = s.Substring(0, 2);
                    return;
                }
#endif
                // failed to parse runtime key
                throw new Exception(BAD_RUNTIME);
            }
        }

        // get/set product GUID
        internal Guid ProductGuid
        {
            get { return _guid; }
            set { _guid = value; }
        }

        // get/set whether license is expired
        internal bool IsExpired
        {
            get { return _expired; }
            set { _expired = value; }
        }

        // get whether license is expired based on type/assembly date
        internal void CheckExpirationDate(Type type)
        {
            var assembly = type.Assembly
#if NETCORE && !NETCORE3
                ()
#endif
                ;

            // get assembly date from version number
            CheckExpirationDate(assembly.FullName.Split(',')[1].Split('=')[1]);
        }

        internal void CheckExpirationDate(string assemblyVersion)
        {
            var asmVersionParts = assemblyVersion.Split('.');
            Debug.Assert(asmVersionParts.Length == 4 && asmVersionParts[2].Length == 5, "Invalid assembly version format, should be 'x.y.YYYYQ.z'");
            var asmDate = asmVersionParts[2];

            // get assembly year and quarter
            int asmYear, asmQuarter;
            if (int.TryParse(asmDate.Substring(0, 4), out asmYear) &&
                int.TryParse(asmDate.Substring(4, 1), out asmQuarter))
            {
                Debug.Assert(asmYear > 2000 && asmQuarter >= 1 && asmQuarter <= 3, "Invalid assembly date in version");

                // compare to license date
                // to ensure at least one year of validity, we allow license YYYYQ
                // to unlock assemblies up to and including (YYYY+1)Q.
                int lq = (2000 + Year + 1) * 10 + Quarter;
                int pq = asmYear * 10 + asmQuarter;
                IsExpired = pq > lq;
            }
            else // bad date format, treat as expired
            {
                IsExpired = true;
            }
        }

        // parses a license value as stored in the registry
        // (ClassesRoot/Licenses/GUID, value "0")
        internal static ProductLicense ParseLicense(string encryptedStringAsInRegistry)
        {
            var keyparts = Decrypt(encryptedStringAsInRegistry).Split('\t');
            try
            {
                var pl = new ProductLicense();
                pl.Key = keyparts[0];
                pl.UserName = keyparts[1];
                pl.CompanyName = keyparts[2];
                return pl;
            }
            catch
            {
                return null;
            }
        }

#if LEGACY_LICENSING

        // build/parse user key (the string used for licensing)
        internal string ShortKey
        {
            get
            {
                // build product/date/ordercode string
                var s1 = string.Format("{0}{1}{2:00}{3}",
                    ProductCode,
                    Quarter,
                    Year,
                    OrderCode);

                // build serial number string
                var s2 = string.Format("{0:000000}", Serial);

                // combine both strings with hash
                return string.Format("{0}-{1}-{2}", s1, MakeCheck(s1, s2), s2);
            }
            set
            {
                // must match "AAQYYCC-XX-NNNNNN"
                if (!Regex.IsMatch(value, "[A-Za-z0-9]{2}[0-9]{5}-[A-Za-z0-9]{2}-[0-9]{6}"))
                {
                    throw new ArgumentException(BAD_SERIAL);
                }

                // parse elements
                ProductCode = value.Substring(0, 2);
                Quarter = int.Parse(value.Substring(2, 1));
                Year = int.Parse(value.Substring(3, 2));
                OrderCode = value.Substring(5, 2);
                Serial = int.Parse(value.Substring(11));

                // check hash
                var s1 = string.Format("{0}{1}{2:00}{3}",
                    ProductCode,
                    Quarter,
                    Year,
                    OrderCode);
                var s2 = string.Format("{0:000000}", Serial);
                var check = MakeCheck(s1, s2);

                // check checksum
                if (check != value.Substring(8, 2))
                {
                    throw new ArgumentException(BAD_SERIAL);
                }
            }
        }

        // ugly hack...
        internal Guid ProductGuidPlusOne
        {
            get { return IncrementGuid(ProductGuid); }
        }
        static Guid IncrementGuid(Guid guid)
        {
            // get first part of guid
            string guidString = guid.ToString();
            string start = guidString.Substring(0, 8);

            // add one
            UInt32 sp1 = UInt32.Parse(start, NumberStyles.AllowHexSpecifier);
            sp1 = sp1 <= UInt32.MaxValue ? sp1 + 1 : sp1 - 1;

            // done
            guidString = sp1.ToString("x8") + guidString.Substring(8);
            return new Guid(guidString);
        }
#endif

        #endregion

        //------------------------------------------------------------
        #region ** install/uninstall product

        // check whether this key is installed
        internal bool IsInstalled
        {
            get { return GetInstalledLicense(this.ProductGuid) != null; }
        }

        internal static ProductLicense GetInstalledLicense(string assemblyVersion, List<Guid> productGuidList)
        {
            ProductLicense plExpired = null;
            // see which one(s) we have licenses for
            foreach (var guid in productGuidList)
            {
                try
                {
                    var pl = GetInstalledLicense(guid);
                    if (pl != null)
                    {
                        pl.CheckExpirationDate(assemblyVersion);
                        if (pl.IsExpired)
                        {
                            plExpired = pl;
                        }
                        else
                        {
                            return pl;
                        }
                    }
                }
                catch // invalid product code, bad attribute in product assembly
                {
#if NETCORE
                    Debug
#else
                    Trace
#endif
                        .WriteLine("found invalid product guid, check C1ProductInfoAttribute!");
                }
            }

            return plExpired;
        }
        // get installed license by type
        internal static ProductLicense GetInstalledLicense(Type type)
        {
            // get licensing GUIDs associated with the type/assembly:
            var licenseGuids = new List<Guid>();
            var assembly = type.Assembly
#if NETCORE && !NETCORE3
                ()
#endif
                ;

            // look for attributes in assembly
            foreach (object att in assembly.GetCustomAttributes(true))
            {
                AddLicenseGuid(licenseGuids, att);
                }

            // and look for attributes in type (to support per-control licensing)
            foreach (object att in type.GetCustomAttributes(true))
            {
                AddLicenseGuid(licenseGuids, att);
            }

            // return expired license (may be null)
            var asmVersion = assembly.FullName.Split(',')[1].Split('=')[1];
            return GetInstalledLicense(asmVersion, licenseGuids);
        }

        // extract licensing Guid information from an Attribute
        static void AddLicenseGuid(List<Guid> licenseGuids, object att)
        {
            var guid = GetProductInfoGuid(att);
            if (guid != null)
            {
                try
                {
                    licenseGuids.Add(new Guid(guid));
                }
                catch 
                {
                    Debug.Assert(false, "Invalid GUID found in C1ProductInfo attribute.");
                }
            }
        }

        // note: use reflection to get ProductInfoAttribute 
        // so it works across assemblies (e.g. with the CF ControlDesigner)
        static string GetProductInfoGuid(object att)
        {
            Type attType = att.GetType();
            if (attType.Name == "C1ProductInfoAttribute")
            {
                try
                {
                    return (string)attType.GetProperty("ProductGUID").GetValue(att, null);
                }
                catch { }
            }
            return null;
        }

        // get a license installed in the registry
        internal static ProductLicense GetInstalledLicense(Guid guid)
        {
            // check that the guid is valid
            Debug.Assert(!guid.Equals(Guid.Empty), "Guid not set for this license!");

            // get from normal place
            using (var key = Registry.ClassesRoot.OpenSubKey(LICENSES))
            {
                var pl = GetInstalledLicense(guid, key);
#if LEGACY_LICENSING
                if (pl == null)
                {
                    pl = GetInstalledLegacyLicense(guid, key);
                }
#endif
                if (pl != null)
                {
                    return pl;
                }
            }

            // under win64, get from Wow node
            if (IntPtr.Size > 4)
            {
                using (var key = Registry.LocalMachine.OpenSubKey(LICENSESWOW))
                {
                    var pl = GetInstalledLicense(guid, key);
#if LEGACY_LICENSING
                    if (pl == null)
                    {
                        pl = GetInstalledLegacyLicense(guid, key);
                    }
#endif
                    if (pl != null)
                    {
                        return pl;
                    }
                }
            }

            // not found...
            return null;
        }

        // retrieve a license form the registry
        static ProductLicense GetInstalledLicense(Guid guid, RegistryKey key)
        {
            // sanity
            if (key == null)
            {
                return null;
            }

            // initialize license
            var pl = new ProductLicense();
            pl.ProductGuid = guid;

            // open product key if present
            using (var subKey = key.OpenSubKey(guid.ToString(), false))
            {
                if (subKey != null)
                {
                    try
                    {
                        // get value
                        var keyValue = subKey.GetValue("0") as string;
                        if (keyValue != null)
                        {
                            // decrypt value
                            keyValue = Decrypt(keyValue);
                            if (keyValue != null)
                            {
                                // parse tab-delimited data
                                string[] parts = keyValue.Split('\t');
                                pl.Key = parts[0];
                                pl.UserName = parts[1];
                                pl.CompanyName = parts[2];

                                // done
                                return pl;
                            }
                        }
                    }
                    catch
                    {
                        // found invalid key, ignore it
                    }
                }
            }

            // license not found
            return null;
        }

#if LEGACY_LICENSING

        // retrieve a legacy license from the registry
        static ProductLicense GetInstalledLegacyLicense(Guid guid, RegistryKey key)
        {
            // sanity
            if (key == null)
            {
                return null;
            }
                
            // initialize license
            var pl = new ProductLicense();
            pl.ProductGuid = guid;

            // try getting from (GUID + 1)
            Guid guidPlusOne = IncrementGuid(guid);
            using (var subKey = key.OpenSubKey(guidPlusOne.ToString(), false))
            {
                if (subKey != null)
                {
                    try
                    {
                        // get value (byte[] stored under "1")
                        var keyData = subKey.GetValue("1") as byte[];
                        if (keyData != null)
                        {
                            // parse old key
                            var keyValue = TextDecrypt(keyData);
                            if (keyValue != null)
                            {
                                ParseLegacyKey(pl, keyValue);
                                return pl;
                            }
                        }
                    }
                    catch { }
                }
            }

            // no luck, try getting from GUID
            using (var subKey = key.OpenSubKey(guid.ToString(), false))
            {
                if (subKey != null)
                {
                    try
                    {
                        // get value (short, stored as string under string.Empty)
                        var keyValue = subKey.GetValue(string.Empty) as string;
                        if (keyValue != null)
                        {
                            // parse old key
                            keyValue = TextDecrypt(keyValue);
                            if (keyValue != null)
                            {
                                ParseLegacyKey(pl, keyValue);
                                return pl;
                            }
                        }

                        // get value (byte[] stored under "1")
                        var keyData = subKey.GetValue("1") as byte[];
                        if (keyData != null)
                        {
                            // parse old key
                            keyValue = TextDecrypt(keyData);
                            if (keyValue != null)
                            {
                                ParseLegacyKey(pl, keyValue);
                                return pl;
                            }
                        }
                    }
                    catch { }
                }
            }

            // no dice...
            return null;
        }

        // parse legacy key value
        static void ParseLegacyKey(ProductLicense pl, string keyValue)
        {
            // remove 'trigger' stuff
            if (keyValue[0] == (char)4 && keyValue[1] == (char)2)
            {
                keyValue = keyValue.Substring(2);
            }

            // parse product code
            pl.ProductCode = keyValue.Substring(0, 2);

            // parse year, quarter (1, 2, or 3 only!)
            pl.Year = int.Parse(keyValue.Substring(3, 2));
            pl.Quarter = Math.Min(3, int.Parse(keyValue.Substring(2, 1)));

            // parse user and company name
            var lenUser = (int)keyValue[5];
            var lenCompany = (int)keyValue[6 + lenUser];
            pl.UserName = keyValue.Substring(6, lenUser);
            pl.CompanyName = keyValue.Substring(7 + lenUser, lenCompany);

            // parse extended information (see GetQualifyingInfo)
            // string.Format("FQ{0}0000000A{1}", OrderCode, VendorCode);
            var extraInfo = keyValue.Substring(7 + lenUser + lenCompany);
            if (extraInfo.Length > 0)
            {
                Debug.Assert(extraInfo.StartsWith("FQ") && extraInfo.Length >= 5);
                // FQCCf[DDD][ZZZZZZ]
                // FQ: fixed
                // f:  flags
                // DDD: Vendor Code (optional)
                // ZZZZZZ: Sequence Code (optional)
                pl.OrderCode = extraInfo.Substring(2, 2);
                if (extraInfo.Length == 8 || extraInfo.Length == 14)
                {
                    pl.VendorCode = extraInfo.Substring(5, 3);
                }
            }
        }
#endif // LEGACY_LICENSING

#endregion

        //------------------------------------------------------------
#region ** utilities

        static string MakeCheck(string fp, string lp)
        {
            StringBuilder s = new StringBuilder(fp);
            s.Append(lp);
            int i, j, sum;
            StringBuilder res = new StringBuilder();
            StringBuilder chars = Permute();
            for (j = 0; j < 2; j++)
            {
                sum = 0;
                for (i = 0; i < s.Length; i++)
                {
                    sum += (i ^ (j + 1)) * (int)s[i];
                }
                res.Append(chars[sum % chars.Length]);
            }
            return res.ToString();
        }
        static StringBuilder Permute()
        {
            int i, j, k;
            char ex_c;
            var chars = new StringBuilder("23456789ABCDEFGHJKLMNPQRSTUVWXY");
            int len = chars.Length;
            for (i = 1; i < len; i++)
            {
                ex_c = chars[i - 1];
                j = 0;
                for (k = 1; k < len; k++)
                {
                    if ((i * k) % len == 1)
                    {
                        j = k;
                        break;
                    }
                }
                if (i < j)
                {
                    chars[i - 1] = chars[j - 1];
                    chars[j - 1] = ex_c;
                }
            }
            return chars;
        }

        // original version
        static string MakeRandomString(int length)
        {
            var sb = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                int test = Guid.NewGuid().GetHashCode() & 0x7fffffff;
                int c = test % 35;
                sb.Append(c < 10 ? (char)(c + 48) : (char)(c + 55));
            }
            return sb.ToString();
        }
#if false
        // clearer but slower
        static string MakeRandomString(int length)
        {
            var sb = new StringBuilder(length);
            var rnd = new Random();
            for (int i = 0; i < length; i++)
            {
                int test = rnd.Next(35);
                sb.Append(test < 10 ? (char)(test + '0') : (char)(test - 10 + 'A'));
            }
            return sb.ToString();
        }
        // a lot faster but generates hexadecimals only (nothing higher than 'F')
        static string MakeRandomString(int length)
        {
            var sb = new StringBuilder();
            while (sb.Length < length)
            {
                var guid = Guid.NewGuid().ToString().ToUpper();
                for (int j = 0; j < guid.Length && sb.Length < length; j++)
                {
                    if (guid[j] != '-')
                        sb.Append(guid[j]);
                }
            }
            return sb.ToString();
        }
#endif
#endregion

        //------------------------------------------------------------
#region ** string encryption

        // FIPS (Federal Information Processing Standards) requires extra security
        // for the Rijndael and other encryption algorithms.
        // http://blog.aggregatedintelligence.com/2007/10/fips-validated-cryptographic-algorithms.html
        // So use our own internal encryption instead (requires LEGACY_LICENSING).

        internal static string Encrypt(string plainText)
        {
            return Encrypt(plainText, false);
        }

        internal static string Encrypt(string plainText, bool runtimeLicense)
        {
            string result = null;
#if !BETA_LICENSE
            if (plainText != null)
            {
                // first, use the code that almost all of C1 Users already have and is working.
                // this code will not break anything for most users.
                //
                // this is tricky, because we cannot change the algorithm because some users may
                // want to use old versions of our controls, and this encryption may be used
                // to install new licenses in the registry as well as runtime.
#if !NETCORE
                if (!runtimeLicense)
                {
                    try
                    {
                        using (var algorithm = Rijndael.Create())
                        using (var encryptor = algorithm.CreateEncryptor(GetIV(), GetIV()))
                        using (var ms = new MemoryStream())
                        using (var c = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                        {
                            var data = Encoding.UTF8.GetBytes(plainText);
                            c.Write(data, 0, data.Length);
                            c.FlushFinalBlock();
                            data = ms.ToArray();
                            result = Convert.ToBase64String(data, 0, data.Length);
                        }
                    }
                    catch { result = null; }
                }
#endif

                if (result == null)
                {
                    // if runtime or the current user cannot use Rijndael due
                    // to FIPS requirements, use the old legacy encryption.
                    string sdata = TextEncrypt(plainText);
                    byte[] bdata = Encoding.UTF8.GetBytes(sdata);
                    result = "c1v01" + Convert.ToBase64String(bdata);
                }
            }
#endif
            return result;
        }

        internal static string Decrypt(string cypher)
        {
            string result = null;
#if !BETA_LICENSE
            if (cypher != null)
            {
                // if the cypher starts with a c1 signature, use the alternate algorithm
                if (cypher.Length > 5 && cypher.StartsWith("c1v01"))
                {
                    string sdata = cypher.Substring(5);
                    if (sdata.Length > 0)
                    {
                        try
                        {
                            byte[] bdata = Convert.FromBase64String(sdata);
                            sdata = Encoding.UTF8.GetString(bdata);
                            result = TextDecrypt(sdata);
                        }
                        catch
                        {
                            result = null;
                        }
                    }
                }
#if !NETCORE
                else
                {
                    // since there is no c1 signature leading, the cypher must have been
                    // generated using Rijndael.
                    try
                    {
                        var data = Convert.FromBase64String(cypher);
                        using (var algorithm = Rijndael.Create())
                        using (var decryptor = algorithm.CreateDecryptor(GetIV(), GetIV()))
                        using (var ms = new MemoryStream(data))
                        using (var c = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                        using (var sr = new StreamReader(c))
                        {
                            return sr.ReadToEnd();
                        }
                    }
                    catch
                    {
                        result = null;
                    }
                }
#endif
            }
#endif
            return result;
        }

        // the following code is required for Rijndael encryption/decryption above.
        static byte[] _iv;
        static byte[] GetIV()
        {
            if (_iv == null)
            {
                // get encryption key (must be 16 bytes long!!)
#if BETA_LICENSE
                _iv = System.Text.Encoding.ASCII.GetBytes("SOMETHING WRONG FOR BETA LICENSING");
#else
                _iv = System.Text.Encoding.ASCII.GetBytes("ComponentOne LLC");
#endif
                Debug.Assert(_iv.Length == 16);
            }
            return _iv;
        }

#endregion

#if LEGACY_LICENSING

        //------------------------------------------------------------
#region ** LEGACY_LICENSING string encryption

        static string TextEncrypt(string s)
        {
            return TextEncrypt(s, GetKey());
        }
        static string TextDecrypt(string text)
        {
            return TextDecrypt(text, GetKey(), out text) ? text : null;
        }
        static string TextDecrypt(byte[] data)
        {
            var s = Encoding.Unicode.GetString(data);
            return TextDecrypt(s);
        }
        static string GetKey()
        {
            string key = "53087483046183F702FCF30639C89CB4";
            char[] arr = key.ToCharArray();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (char)((int)arr[i] - 45);
            }
            return new string(arr);
        }
        static string TextEncrypt(string s, string key)
        {
#if BETA_LICENSE
            return null;
#else
            if (s != null && s.Length > 0)
            {
                uint[] xkey = new uint[256];
                uint i, t, ind1 = 0, ind2 = 0, x = 6, y = 0;
                char[] cs = new char[6];
                uint[] crc = new uint[6];
                uint k1, k2;
                for (i = 0; i < 256; i++)
                {
                    xkey[i] = i;
                }
                for (i = 0; i < 256; i++)
                {
                    t = xkey[i];
                    ind2 = ((uint)key[(int)ind1] + xkey[i] + ind2) & 0xFF;
                    xkey[i] = xkey[ind2];
                    xkey[ind2] = t;
                    ind1 = (ind1 + 1) % (uint)key.Length;
                }
                CRC(s, out k1, out k2);
                crc[0] = k1 & 0xFF;
                crc[1] = k1 >> 8;
                crc[2] = k2 & 0xFF;
                crc[3] = k2 >> 8 & 0xFF;
                crc[4] = k2 >> 16 & 0xFF;
                crc[5] = k2 >> 24;
                cs[0] = (char)(crc[0] + 1);
                cs[1] = (char)(crc[1] + 1);
                cs[2] = (char)(crc[2] + 1);
                cs[3] = (char)(crc[3] + 1);
                cs[4] = (char)(crc[4] + 1);
                cs[5] = (char)(crc[5] + 1);
                for (i = 0; i < 6; i++)
                {
                    ind1 = xkey[i + 1];
                    y = (ind1 + y) & 0xFF;
                    ind2 = xkey[y];
                    xkey[i + 1] = ind2;
                    xkey[y] = ind1;
                    cs[i] = (char)((uint)cs[i] + xkey[(ind1 + ind2) & 0xFF]);
                }
                ind1 = 0;
                ind2 = 0;
                for (i = 0; i < 256; i++)
                {
                    t = xkey[i];
                    ind2 = (crc[ind1] + xkey[i] + ind2) & 0xFF;
                    xkey[i] = xkey[ind2];
                    xkey[ind2] = t;
                    ind1 = (ind1 + 1) % 6;
                }
                StringBuilder sb = new StringBuilder(s.Length);
                sb.Length = s.Length;
                for (i = 0; i < sb.Length; i++)
                {
                    x = (x + 1) & 0xFF;
                    ind1 = xkey[x];
                    y = (ind1 + y) & 0xFF;
                    ind2 = xkey[y];
                    xkey[x] = ind2;
                    xkey[y] = ind1;
                    ind1 = (ind1 + ind2) & 0xFF;
                    t = (uint)s[(int)i];
                    if (t < 0xAB00)
                        sb[(int)i] = (char)(t + xkey[ind1]);
                    else if (t >= 0xAC00)
                        sb[(int)i] = (char)(t ^ xkey[ind1]);
                    else
                        throw new Exception("Invalid character found!");
                }
                for (i = 0; i < 256; i++)
                {
                    xkey[i] = 0;
                }
                return new string(cs) + sb;
            }
            else
            {
                return string.Empty;
            }
#endif
        }
        static string TextDecrypt(string cypherstr, string key)
        {
            string plainText;
            return TextDecrypt(cypherstr, key, out plainText)
                ? plainText
                : null;
        }
        static bool TextDecrypt(string cypherstr, string key, out string s)
        {
#if BETA_LICENSE
            s = null;
            return false;
#else
            if (cypherstr != null && cypherstr.Length > 6)
            {
                uint[] xkey = new uint[256];
                uint i, t, ind1 = 0, ind2 = 0, x = 6, y = 0;
                uint[] crc = new uint[6];
                for (i = 0; i < 256; i++)
                {
                    xkey[i] = i;
                }
                for (i = 0; i < 256; i++)
                {
                    t = xkey[i];
                    ind2 = ((uint)key[(int)ind1] + xkey[i] + ind2) & 0xFF;
                    xkey[i] = xkey[ind2];
                    xkey[ind2] = t;
                    ind1 = (ind1 + 1) % (uint)key.Length;
                }
                for (i = 0; i < 6; i++)
                {
                    ind1 = xkey[i + 1];
                    y = (ind1 + y) & 0xFF;
                    ind2 = xkey[y];
                    xkey[i + 1] = ind2;
                    xkey[y] = ind1;
                    crc[i] = (uint)cypherstr[(int)i] - xkey[(ind1 + ind2) & 0xFF] - 1;
                }
                ind1 = 0;
                ind2 = 0;
                for (i = 0; i < 256; i++)
                {
                    t = xkey[i];
                    ind2 = (crc[ind1] + xkey[i] + ind2) & 0xFF;
                    xkey[i] = xkey[ind2];
                    xkey[ind2] = t;
                    ind1 = (ind1 + 1) % 6;
                }
                StringBuilder sb = new StringBuilder(cypherstr.Substring(6));
                for (i = 0; i < sb.Length; i++)
                {
                    x = (x + 1) & 0xFF;
                    ind1 = xkey[x];
                    y = (ind1 + y) & 0xFF;
                    ind2 = xkey[y];
                    xkey[x] = ind2;
                    xkey[y] = ind1;
                    ind1 = (ind1 + ind2) & 0xFF;
                    t = (uint)sb[(int)i];
                    if (t < 0xAC00)
                        sb[(int)i] = (char)(t - xkey[ind1]);
                    else
                        sb[(int)i] = (char)(t ^ xkey[ind1]);
                }
                for (i = 0; i < 256; i++)
                {
                    xkey[i] = 0;
                }
                s = sb.ToString();
                CRC(s, out ind1, out ind2);
                return (ind1 == (crc[0] + (crc[1] << 8)) &&
                    ind2 == (crc[2] + (crc[3] << 8) + (crc[4] << 16) + (crc[5] << 24)));
            }
            else
            {
                s = string.Empty;
                return (cypherstr == null || cypherstr.Length == 0);
            }
#endif
        }
        static uint[] CRC16_Table = new uint[256] {
            0x0000,0xC0C1,0xC181,0x0140,0xC301,0x03C0,0x0280,0xC241,0xC601,0x06C0,0x0780,
            0xC741,0x0500,0xC5C1,0xC481,0x0440,0xCC01,0x0CC0,0x0D80,0xCD41,0x0F00,0xCFC1,
            0xCE81,0x0E40,0x0A00,0xCAC1,0xCB81,0x0B40,0xC901,0x09C0,0x0880,0xC841,0xD801,
            0x18C0,0x1980,0xD941,0x1B00,0xDBC1,0xDA81,0x1A40,0x1E00,0xDEC1,0xDF81,0x1F40,
            0xDD01,0x1DC0,0x1C80,0xDC41,0x1400,0xD4C1,0xD581,0x1540,0xD701,0x17C0,0x1680,
            0xD641,0xD201,0x12C0,0x1380,0xD341,0x1100,0xD1C1,0xD081,0x1040,0xF001,0x30C0,
            0x3180,0xF141,0x3300,0xF3C1,0xF281,0x3240,0x3600,0xF6C1,0xF781,0x3740,0xF501,
            0x35C0,0x3480,0xF441,0x3C00,0xFCC1,0xFD81,0x3D40,0xFF01,0x3FC0,0x3E80,0xFE41,
            0xFA01,0x3AC0,0x3B80,0xFB41,0x3900,0xF9C1,0xF881,0x3840,0x2800,0xE8C1,0xE981,
            0x2940,0xEB01,0x2BC0,0x2A80,0xEA41,0xEE01,0x2EC0,0x2F80,0xEF41,0x2D00,0xEDC1,
            0xEC81,0x2C40,0xE401,0x24C0,0x2580,0xE541,0x2700,0xE7C1,0xE681,0x2640,0x2200,
            0xE2C1,0xE381,0x2340,0xE101,0x21C0,0x2080,0xE041,0xA001,0x60C0,0x6180,0xA141,
            0x6300,0xA3C1,0xA281,0x6240,0x6600,0xA6C1,0xA781,0x6740,0xA501,0x65C0,0x6480,
            0xA441,0x6C00,0xACC1,0xAD81,0x6D40,0xAF01,0x6FC0,0x6E80,0xAE41,0xAA01,0x6AC0,
            0x6B80,0xAB41,0x6900,0xA9C1,0xA881,0x6840,0x7800,0xB8C1,0xB981,0x7940,0xBB01,
            0x7BC0,0x7A80,0xBA41,0xBE01,0x7EC0,0x7F80,0xBF41,0x7D00,0xBDC1,0xBC81,0x7C40,
            0xB401,0x74C0,0x7580,0xB541,0x7700,0xB7C1,0xB681,0x7640,0x7200,0xB2C1,0xB381,
            0x7340,0xB101,0x71C0,0x7080,0xB041,0x5000,0x90C1,0x9181,0x5140,0x9301,0x53C0,
            0x5280,0x9241,0x9601,0x56C0,0x5780,0x9741,0x5500,0x95C1,0x9481,0x5440,0x9C01,
            0x5CC0,0x5D80,0x9D41,0x5F00,0x9FC1,0x9E81,0x5E40,0x5A00,0x9AC1,0x9B81,0x5B40,
            0x9901,0x59C0,0x5880,0x9841,0x8801,0x48C0,0x4980,0x8941,0x4B00,0x8BC1,0x8A81,
            0x4A40,0x4E00,0x8EC1,0x8F81,0x4F40,0x8D01,0x4DC0,0x4C80,0x8C41,0x4400,0x84C1,
            0x8581,0x4540,0x8701,0x47C0,0x4680,0x8641,0x8201,0x42C0,0x4380,0x8341,0x4100,
            0x81C1,0x8081,0x4040};

        private static uint[] CRC32_Table = new uint[256] {
            0x00000000,0x77073096,0xEE0E612C,0x990951BA,0x076DC419,0x706AF48F,0xE963A535,0x9E6495A3,
            0x0EDB8832,0x79DCB8A4,0xE0D5E91E,0x97D2D988,0x09B64C2B,0x7EB17CBD,0xE7B82D07,0x90BF1D91,
            0x1DB71064,0x6AB020F2,0xF3B97148,0x84BE41DE,0x1ADAD47D,0x6DDDE4EB,0xF4D4B551,0x83D385C7,
            0x136C9856,0x646BA8C0,0xFD62F97A,0x8A65C9EC,0x14015C4F,0x63066CD9,0xFA0F3D63,0x8D080DF5,
            0x3B6E20C8,0x4C69105E,0xD56041E4,0xA2677172,0x3C03E4D1,0x4B04D447,0xD20D85FD,0xA50AB56B,
            0x35B5A8FA,0x42B2986C,0xDBBBC9D6,0xACBCF940,0x32D86CE3,0x45DF5C75,0xDCD60DCF,0xABD13D59,
            0x26D930AC,0x51DE003A,0xC8D75180,0xBFD06116,0x21B4F4B5,0x56B3C423,0xCFBA9599,0xB8BDA50F,
            0x2802B89E,0x5F058808,0xC60CD9B2,0xB10BE924,0x2F6F7C87,0x58684C11,0xC1611DAB,0xB6662D3D,
            0x76DC4190,0x01DB7106,0x98D220BC,0xEFD5102A,0x71B18589,0x06B6B51F,0x9FBFE4A5,0xE8B8D433,
            0x7807C9A2,0x0F00F934,0x9609A88E,0xE10E9818,0x7F6A0DBB,0x086D3D2D,0x91646C97,0xE6635C01,
            0x6B6B51F4,0x1C6C6162,0x856530D8,0xF262004E,0x6C0695ED,0x1B01A57B,0x8208F4C1,0xF50FC457,
            0x65B0D9C6,0x12B7E950,0x8BBEB8EA,0xFCB9887C,0x62DD1DDF,0x15DA2D49,0x8CD37CF3,0xFBD44C65,
            0x4DB26158,0x3AB551CE,0xA3BC0074,0xD4BB30E2,0x4ADFA541,0x3DD895D7,0xA4D1C46D,0xD3D6F4FB,
            0x4369E96A,0x346ED9FC,0xAD678846,0xDA60B8D0,0x44042D73,0x33031DE5,0xAA0A4C5F,0xDD0D7CC9,
            0x5005713C,0x270241AA,0xBE0B1010,0xC90C2086,0x5768B525,0x206F85B3,0xB966D409,0xCE61E49F,
            0x5EDEF90E,0x29D9C998,0xB0D09822,0xC7D7A8B4,0x59B33D17,0x2EB40D81,0xB7BD5C3B,0xC0BA6CAD,
            0xEDB88320,0x9ABFB3B6,0x03B6E20C,0x74B1D29A,0xEAD54739,0x9DD277AF,0x04DB2615,0x73DC1683,
            0xE3630B12,0x94643B84,0x0D6D6A3E,0x7A6A5AA8,0xE40ECF0B,0x9309FF9D,0x0A00AE27,0x7D079EB1,
            0xF00F9344,0x8708A3D2,0x1E01F268,0x6906C2FE,0xF762575D,0x806567CB,0x196C3671,0x6E6B06E7,
            0xFED41B76,0x89D32BE0,0x10DA7A5A,0x67DD4ACC,0xF9B9DF6F,0x8EBEEFF9,0x17B7BE43,0x60B08ED5,
            0xD6D6A3E8,0xA1D1937E,0x38D8C2C4,0x4FDFF252,0xD1BB67F1,0xA6BC5767,0x3FB506DD,0x48B2364B,
            0xD80D2BDA,0xAF0A1B4C,0x36034AF6,0x41047A60,0xDF60EFC3,0xA867DF55,0x316E8EEF,0x4669BE79,
            0xCB61B38C,0xBC66831A,0x256FD2A0,0x5268E236,0xCC0C7795,0xBB0B4703,0x220216B9,0x5505262F,
            0xC5BA3BBE,0xB2BD0B28,0x2BB45A92,0x5CB36A04,0xC2D7FFA7,0xB5D0CF31,0x2CD99E8B,0x5BDEAE1D,
            0x9B64C2B0,0xEC63F226,0x756AA39C,0x026D930A,0x9C0906A9,0xEB0E363F,0x72076785,0x05005713,
            0x95BF4A82,0xE2B87A14,0x7BB12BAE,0x0CB61B38,0x92D28E9B,0xE5D5BE0D,0x7CDCEFB7,0x0BDBDF21,
            0x86D3D2D4,0xF1D4E242,0x68DDB3F8,0x1FDA836E,0x81BE16CD,0xF6B9265B,0x6FB077E1,0x18B74777,
            0x88085AE6,0xFF0F6A70,0x66063BCA,0x11010B5C,0x8F659EFF,0xF862AE69,0x616BFFD3,0x166CCF45,
            0xA00AE278,0xD70DD2EE,0x4E048354,0x3903B3C2,0xA7672661,0xD06016F7,0x4969474D,0x3E6E77DB,
            0xAED16A4A,0xD9D65ADC,0x40DF0B66,0x37D83BF0,0xA9BCAE53,0xDEBB9EC5,0x47B2CF7F,0x30B5FFE9,
            0xBDBDF21C,0xCABAC28A,0x53B39330,0x24B4A3A6,0xBAD03605,0xCDD70693,0x54DE5729,0x23D967BF,
            0xB3667A2E,0xC4614AB8,0x5D681B02,0x2A6F2B94,0xB40BBE37,0xC30C8EA1,0x5A05DF1B,0x2D02EF8D};
        static void CRC(string s, out uint crc16, out uint crc32)
        {
            uint v16 = 0xFFFF;
            uint v32 = 0xFFFFFFFF;
            if (s != null)
                for (int i = 0; i < s.Length; i++)
                {
                    v16 = (v16 >> 8) ^ CRC16_Table[((uint)s[i] ^ v16) & 0xFF];
                    v16 = (v16 >> 8) ^ CRC16_Table[((uint)s[i] >> 8 ^ v16) & 0xFF];
                    v32 = (v32 >> 8) ^ CRC32_Table[((uint)s[i] ^ v32) & 0xFF];
                    v32 = (v32 >> 8) ^ CRC32_Table[((uint)s[i] >> 8 ^ v32) & 0xFF];
                }
            crc16 = ~v16 & 0xFFFF;
            crc32 = ~v32;
        }
#endregion
#endif
    }
}
#pragma warning restore CS0436
