﻿#define HARD_EVAL_DATE
#if !SILVERLIGHT && !UWP && !CORE31
#define GCLICENSING
#endif
#if WPF
#define SMARTASSEMBLYUSAGE
#endif
//#if DEBUG
//#define DEBUGLIC
//#endif
//////////////////////////////////////////////////////////////////////////
//
// LicenseProvider.cs
// 
// -----------------------------------------------------------------------
// History:
// -----------------------------------------------------------------------
// Dec  2010 (BJC):
//
// - Changed GC licensing according to their specs (much stricter now)
// 
// - Made trial period variable (C1 uses 30 days, GC uses 10)
//
// - When standard GetSavedLicenseKey returns an invalid key, try with ours
//   also. This works in cases where the user renames an EXE (which causes 
//   MS licensing to fail, but still works with ours).
//
// - Added Gary's "intern" trick to avoid showing the license too often
//   even in cases where we have no access to the registry.
//
// - Changed ProviderInfo class so it inherits from LicenseProvider; this
//   allows users to write 
//    [LicenseProvider(typeof(C1.Util.Licensing.ProviderInfo))]
//   which may be needed for some non-standard (i.e. non-VS) hosts.
//
// Oct  2009 (GEH):
//
// - adjusted IsMsIdeDomainManager() to handle VS2010 as well as previous
//   Visual Studio environments.
//
// Sept 2009 (BJC):
//
// - changed the code so it only nags once per day for each type.
//   done in response to user requests.
//   after TRIAL_PERIOD trial days have elapsed, nags like before.
//
// - cache types for which the standard GetSavedLicenseKey fails, and
//   don't try again (exceptions are slow).
//
//////////////////////////////////////////////////////////////////////////
//#define LEGACY_LICENSING
using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Win32;
//using GrapeCity.Common;

#pragma warning disable CS0436
namespace C1.Util.Licensing
{
    //----------------------------------------------------------------------------
    #region ** enums

    internal enum LicenseStatus
    {
        Valid,      // license is valid
        Expired,    // license is too old for this dll
        Unlicensed  // license not found
    }

    #endregion

    //----------------------------------------------------------------------------
    #region ** ProviderInfo

    /// <summary>
    /// <para>Provides static <see cref="Validate(Type, object)"/> methods that 
    /// should be called from the constructors of licensed classes.</para>
    /// <para>Also provides a static <see cref="ShowAboutBox(object)"/> method that can 
    /// be called to show the about box with product and licensing information.</para>
    /// </summary>
#if SMARTASSEMBLYUSAGE
    [SmartAssembly.Attributes.DoNotObfuscateType]
#endif
    internal class ProviderInfo : LicenseProvider
    {
        static bool _nagged;
#if GCLICENSING && !NOGCLICENSING && !GRAPECITY
        static string _gcLicenseKey = null;
        static LicenseInfo _gcLicenseInfo = null;
#endif
        // ** abstract LicenseProvider overrides
        public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
        {
            return Validate(type, instance);
        }

        // ** object model

        /// <summary>
        /// Perform license validation. Call this method from the licensed object's 
        /// constructor to save a license key at design time, validate it at runtime, 
        /// and display a nag dialog if a valid license is not found.
        /// </summary>
        /// <param name="type">Type of licensed object (use typeof() and not GetType()).</param>
        /// <param name="instance">Reference to the licensed object (not currently used).</param>
        /// <returns>A <see cref="LicenseInfo"/> object that contains information about the license.</returns>
        /// <remarks>
        /// <para>Check the <b>ShouldNag</b> property of the returned <see cref="LicenseInfo"/> 
        /// to determine whether the licensed class should nag the user. This value is set
        /// to true in situations where a valid license was not found but a nag dialog
        /// could not be displayed. In these cases, the licensed class is supposed to nag
        /// in some other way (with a watermark for example).</para>
        /// </remarks>
        internal static LicenseInfo Validate(Type type, object instance)
        {
            return Validate(type, instance, true);
        }
        /// <summary>
        /// Perform license validation. Call this method from the licensed object's 
        /// constructor to save a license key at design time, validate it at runtime, 
        /// and display a nag dialog if a valid license is not found.
        /// </summary>
        /// <param name="type">Type of licensed object (use typeof() and not GetType()).</param>
        /// <param name="instance">Reference to the licensed object (not currently used).</param>
        /// <param name="showNagDialog">Whether the nag dialog should be displayed when a valid license is not found.</param>
        /// <returns>A <see cref="LicenseInfo"/> object that contains information about the license.</returns>
        /// <remarks>
        /// <para>This overload was created for use in WPF. It should be called in the control's constructor
        /// in order to support license persistence correctly. But it should not show the nag dialog until the
        /// control is fully loaded, or the VS designer may remain blank.</para>
        /// <para>So the solution is this:</para>
        /// <code>
        /// LicenseInfo _licInfo;
        /// public LicensedControl()
        /// {
        ///   // check license but don't nag yet
        ///   _licInfo = ProviderInfo.Validate(typeof(LicensedControl), this, false);
        ///   
        ///   // perform licensing after control is fully loaded
        ///   Loaded += LicensedControl_Loaded;
        /// }
        /// void LicensedControl_Loaded(object sender, RoutedEventArgs e)
        /// {
        ///   // nag after loading
        ///   if (_licInfo.ShouldNag)
        ///   {
        ///     ProviderInfo.ShowAboutBox(this);
        ///   }
        /// }
        /// </code>
        /// </remarks>
        internal static LicenseInfo Validate(Type type, object instance, bool showNagDialog)
        {
            // this overload maintains previous call logic in case it was used elsewhere.
            return Validate(type, instance, showNagDialog, true);
        }
        internal static LicenseInfo Validate(Type type, object instance, bool showNagDialog, bool useAppDomainTest)
        {
            //_doBreak();   // hard breakpoint necessary for compiles, lc.exe and w3wp.exe
#if !SILVERLIGHT && !WPF
            if (C1AllowedCallerAttribute.IsAllowedAssembly())
            {
#if DEBUG
                Debug.WriteLine("License Caller is allowed for: " + type.FullName);
#endif
                return new LicenseInfo(type, LicenseStatus.Valid);
            }
#if DEBUG
            else
            {
                Debug.WriteLine("License Caller is NOT allowed for: " + type.FullName);
            }
#endif
#endif
     // _doBreak();
            var context = LicenseManager.CurrentContext;
            string gcLicKey = null;
#if GCLICENSING && !NOGCLICENSING && !GRAPECITY
            {
#if WPF
                var gcLicProvider = new GrapeCity.Common.C1GCWpfLicenseProvider();
#elif WEB
                var gcLicProvider = new GrapeCity.Common.C1GCWebLicenseProvider(type);
#else
                var gcLicProvider = new GrapeCity.Common.C1GCWFLicenseProvider();
#endif
#if !NO_ABOUTBOX
                _gcLicenseKey = string.Empty;
                gcLicProvider.SetAboutBox(() => { ShowAboutBox(type, null); });
                var gclic = gcLicProvider.GetLicense(LicenseManager.CurrentContext, type, instance, false);
				_gcLicenseKey = null;
#else
				var gclic = gcLicProvider.GetLicense(LicenseManager.CurrentContext, type, instance, false);				
#endif
                if (gclic != null)
                {
                    gcLicKey = gclic.LicenseKey;
                    if (gcLicKey.StartsWith("Product License, Activated", StringComparison.Ordinal))
                    {
#if !NO_ABOUTBOX
                        _gcLicenseKey = gcLicKey;
#endif
                        _gcLicenseInfo = new LicenseInfo(type, LicenseStatus.Valid,
                            new ProductLicense()
                            {
                                UserName = "",
                                CompanyName = gcLicProvider.GetLicensedProduct()?.Name,
                            });
                        return _gcLicenseInfo;

                        //_gcLicensedProduct = gcLicProvider.GetLicensedProduct()?.Name;
                        //return new LicenseInfo(type, LicenseStatus.Valid,
                        //    new ProductLicense() { UserName = "", CompanyName = _gcLicensedProduct });
                    }
                    
                    if (gcLicKey.StartsWith("Trial License",StringComparison.Ordinal))
                    {
#if WEB
                        if (gcLicKey.Contains("Expired"))
                        {
                            _gcLicenseInfo = new LicenseInfo(type, LicenseStatus.Unlicensed, new ProductLicense())
                            {
                                EvaluationDaysLeft = gcLicProvider.GetTrialDaysRemaining(),
#if SCAFFOLDER
                                GetStop = false,
#else
                                GetStop = true,
#endif
                            };
                        }
                        else
#endif
                        {
                            _gcLicenseInfo = new LicenseInfo(type, LicenseStatus.Valid,
                                new ProductLicense()
                                {
                                    // UserName = string.Empty will not happen with the old license
                                    // so here it flags to AboutBoxForms that Company has new license info.
                                    UserName = string.Empty,
                                    CompanyName = gcLicKey,
                                })
                            {
                                EvaluationDaysLeft = gcLicProvider.GetTrialDaysRemaining(),
                                GetStop = true,
                            };
                        }
                    }
                
                    //    gcLicProvider.G
                    //bool allowExceptions = (if version.Build > 20202) then may or maynot allow old license.
                }
            }
#endif
            _skipDomainManagerTest = !useAppDomainTest;

            // get license information
            var designTime = IsDesignTime(context);

#if BETA_LICENSE
            var li = ValidateBetaLicense(type, instance, showNagDialog, context, designTime);
#else
            var li = designTime
                    ? ValidateDesigntime(type, context)
                    : ValidateRuntime(type, context);
            // special cases for ASP.NET controls:
            if (!li.IsValid)
            {
                // don't nag when running under the ASP.Net designer
                // (or the dialog will show twice!)
                if (designTime && context.UsageMode == LicenseUsageMode.Runtime)
                {
                    _nagged = true;
                }

                // don't nag at runtime under localhost
                // (we already nagged at design time)
                if (!designTime && IsLocalHost(instance))
                {
                    _nagged = true;

                    // remember we're running under localhost so the caller can 
                    // suppress alert boxes (we already nagged at design time).
                    li.IsLocalHost = true;
                }

                // before nagging at runtime, check the registry for a "RuntimeLicenseCheck" key.
                // if the key is present, look for a design-time license.
                // this was added to support automated testing scenarios using licensed machines.
                if (!designTime && !_nagged)
                {
                    try
                    {
                        var runtimeKey = @"Software\ComponentOne\RuntimeLicenseCheck";
                        using (var regKey = Registry.LocalMachine.OpenSubKey(runtimeKey, false))
                        {
                            if (regKey != null)
                            {
                                var dtl = ValidateDesigntime(type, context);
                                if (dtl.IsValid)
                                {
                                    _nagged = true;
                                }
                            }
                        }
                    }
                    catch {}
                }
            }

#if GCLICENSING && !NOGCLICENSING && !GRAPECITY
            if (li.IsValid) // valid old license, no need to nag
            {
                // return license info to caller
                return li;
            }


            if (!li.IsValid && !string.IsNullOrEmpty(gcLicKey) && gcLicKey.StartsWith("Trial License",StringComparison.Ordinal))
            {
#if !NO_ABOUTBOX
                _gcLicenseKey = gcLicKey;
#endif
                li = _gcLicenseInfo;
            }
#endif
            // nag if we can, otherwise tell control to nag
            if (!li.IsValid || !string.IsNullOrEmpty(gcLicKey ))
            {
                if (!_nagged)
                {
                    // normal case: show dialog
                    if (showNagDialog && Environment.UserInteractive)
                    {
                        try
                        {
                            _nagged = true; // <<IP>> set it to true here to avoid repeated nags if some reason, 
                            // VS fires license validation several times from different threads
                            Nag(type, li, designTime);
                        }
#if HARD_EVAL_DATE
                        catch (Exception e)
                        {
                            if (li.GetStop && e is LicenseException)
                            {
                                _nagged = false;
                                throw e;
                            }
                            _nagged = false;
                        } // otherwise failed, probably because of permissions...
#else
                        catch {_nagged = false;} // otherwise failed, probably because of permissions...
#endif
                    }

                    // if couldn't or failed, defer nagging to caller
                    if (!_nagged)
                    {
                        li.ShouldNag = true;
                        _nagged = true;

#if HARD_EVAL_DATE
                        // li.EvaluationDaysLeft sets GetTop value.  It must be called first
                        if (designTime && (li.EvaluationDaysLeft < 0 || li.GetStop))
                            if(li.GetStop) li.SaveEvalRuntimeLicense(type);

                        if (!designTime && li.EvaluationDaysLeft < 0 && li.GetStop)
                            throw new LicenseException(li.Type);
#endif
                    }
                }
#if HARD_EVAL_DATE
                // li.EvaluationDaysLeft sets GetTop value.  It must be called first
                else if (li.EvaluationDaysLeft < 0 || li.GetStop)
                {
#if GCLICENSING && !NOGCLICENSING && !GRAPECITY
                    if (designTime && li.GetStop && string.IsNullOrEmpty(_gcLicenseKey)) li.SaveEvalRuntimeLicense(type);
#else
                    if (designTime && li.GetStop) li.SaveEvalRuntimeLicense(type);
#endif
                    if(li.EvaluationDaysLeft < 0 && li.GetStop) throw new LicenseException(li.Type);
                }
#endif
            }
#endif

            // return license info to caller
            return li;
        }

		internal static void _doBreak()
		{
#if DEBUG
			if (Debugger.IsAttached)
				Debugger.Break();
			else
				Debugger.Launch();
#endif
		}

        class PersistentNagFlag
        {
            const string usageKey = @"Software\ComponentOne\UsageData";
            bool internPersisted = false;
            bool registryPersisted = false;
            string entry = null;
            string value = null;
            string intern = null;

            internal PersistentNagFlag(Type type, string unique=null)
            {
                entry = ComputeHash(string.Format("nag_{0}", type.AssemblyQualifiedName) + (unique != null ? unique : "")).ToString();
                value = ComputeHash(DateTime.Today.ToBinary().ToString()).ToString();
                intern = entry + value;
            }
            internal bool AlreadyNaggedToday()
            {
                bool result = false;
                try
                {
                    internPersisted = result = !string.IsNullOrEmpty(string.IsInterned(intern));
                    if (!result)
                    {
                        using (RegistryKey key = Registry.CurrentUser.OpenSubKey(usageKey, false))
                        {
                            if (key != null)
                            {
                                string storedValue = key.GetValue(entry) as string;
                                registryPersisted = result = (storedValue == value);
                            }
                        }
                    }
                }
                catch { }
                return result;
            }

            internal void SetNagFlag()
            {
                if (!internPersisted)
                {
                    string.Intern(intern);
                    if (!registryPersisted)
                    {
                        try
                        {
                            using (RegistryKey ckey = Registry.CurrentUser.CreateSubKey(usageKey))
                            {
                                if((ckey.GetValue(entry) as string) != value)
                                    ckey.SetValue(entry, value);
                                registryPersisted = true;
                            }
                        }
                        catch { }
                    }
                }
            }
        }

        /// <summary>
        /// Nag user by showing AboutBox with license information.
        /// Show it only once per day per assembly.
        /// </summary>
        /// <param name="type">Type of licensed object (use typeof() and not GetType()).</param>
        /// <param name="li"><see cref="LicenseInfo"/> object that contains information about the license.</param>
        /// <param name="designTime">Whether we're running at design or run time.</param>
        internal static void Nag(Type type, LicenseInfo li, bool designTime)
        {
//#if HARD_EVAL_DATE
//            if (!designTime && li.GetStop) li.evalDaysElapsed = 0;
//#endif
            // do not move the following line.
            // EvaluationDaysLeft property must execute before the li.GetStop test as the
            // li.EvaluationDaysLeft property calculates the value for li.GetStop.
            int evaluationDaysLeft = li.EvaluationDaysLeft;
            
#if GRAPECITY
            // GC wants to nag always!
            ShowAboutBox(type, li, designTime);
#if HARD_EVAL_DATE
            if (designTime && li != null && !li.IsValid && (li.IsExpired || li.EvaluationDaysLeft <= 0) && li.GetStop)
            {
                throw (new LicenseException(li.Type));
            }
#endif
#else
#if HARD_EVAL_DATE
            int warningTrigger = (li.GetStop) ? 5 : 0;
#else
            int warningTrigger = 0;
#endif
            var nagFlag = new PersistentNagFlag(type);

            if (evaluationDaysLeft > warningTrigger && nagFlag.AlreadyNaggedToday())
            {
#if !DEBUG || !DEBUGLIC
                return;
#else
                Debug.WriteLine("Would have returned without Nag due to Interned or Registered string");
#endif
            }

            // nag now
            ShowAboutBox(type, li, designTime);
#if HARD_EVAL_DATE
            if (li != null && !li.IsValid && li.GetStop)
            {
				if (designTime)
				{
                    li.SaveEvalRuntimeLicense(type);

                    if (li.IsExpired || li.EvaluationDaysLeft <= 0)
						throw (new LicenseException(li.Type));
				}
				else
				{
					if(li.EvaluationDaysLeft <= 0)
						throw (new LicenseException(li.Type));
				}
            }
#endif
            // remember we nagged today
            if (designTime)
            {
                nagFlag.SetNagFlag();
            }
#endif
        }
        /// <summary>
        /// Version of Validate used by constructors that take runtime keys.
        /// </summary>
        /// <param name="type">Type of licensed object (use typeof() and not GetType()).</param>
        /// <param name="instance">Reference to the licensed object.</param>
        /// <param name="callingAsm">Assembly that contains the owner licensed control.</param>
        /// <param name="runtimeKey">Any valid C1 runtime key.</param>
        /// <returns>A <see cref="LicenseInfo"/> with information about the runtime key.</returns>
        /// <remarks>
        /// <para>This allows a licensed C1 class to create other C1 objects bypassing 
        /// license verification for the child objects.</para>
        /// <para>For extra safety, we check that the owner object is defined in an assembly
        /// the contains a 'C1ProductInfo' attribute.</para>
        /// </remarks>
        internal static LicenseInfo Validate(Type type, object instance, Assembly callingAsm, string runtimeKey)
        {
            // check that the calling asm looks like a C1 asm
            if (!IsC1Assembly(callingAsm))
            {
                return new LicenseInfo(type, LicenseStatus.Unlicensed);
            }

            // check that the runtime key is valid (will throw otherwise)
            try
            {
                var pl = new ProductLicense();
                pl.RuntimeKey = runtimeKey;
                var asm = type.Assembly;
                foreach (C1ProductInfoAttribute att in asm.GetCustomAttributes(typeof(C1ProductInfoAttribute), true))
                {
                    if (att.ProductCode == pl.ProductCode)
                    {
                        return new LicenseInfo(type, LicenseStatus.Valid, pl);
                    }
                }
            }
            catch { }

            // failed, return invalid license info
            return new LicenseInfo(type, LicenseStatus.Unlicensed);
        }

#if BETA_LICENSE
        const int betaWarningTrigger = 5;
        internal static LicenseInfo ValidateBetaLicense(Type type, object instance, bool showNagDialog, LicenseContext context, bool designTime)
        {
            var li = new LicenseInfo(type, LicenseStatus.Unlicensed);
            int daysLeft = li.EvaluationDaysLeft;

            var nagFlag = new PersistentNagFlag(type, "beta");

            if (!_nagged && designTime && daysLeft > betaWarningTrigger)
            {
                _nagged = nagFlag.AlreadyNaggedToday();
            }

            if (!_nagged && showNagDialog && Environment.UserInteractive)
            {
                try
                {
                    _nagged = true;
                    ShowBetaNag(type, li, designTime);
                }
                catch (Exception e)
                {
                    _nagged = false;
                    throw e;
                }
            }

            if (!_nagged)
            {
                li.ShouldNag = true;
                _nagged = true;
            }

            if (daysLeft <= 0)
            {
#if GRAPECITY
				string betaMsg = "このアセンブリのベータ版の提供期間は終了しました。" + "\r\n" +
					"本製品を継続して使用するには、最新バージョンを取得する必要があります。詳しくは、弊社までお問い合わせください。";
#else
				string betaMsg = "The BETA test period for this assembly has ended.\r\n" +
					"To continue using this product, please visit our website or contact us to obtain the latest version.";
				
#endif
                throw new LicenseException(type, instance, betaMsg);
            }

            if (designTime) nagFlag.SetNagFlag();

            return li;
        }

        internal static void ShowBetaNag(Type type, LicenseInfo li, bool designTime)
        {
            //const int warningTrigger = 5;   // 5 day warning at runtime
            bool trigger = designTime || li.EvaluationDaysLeft <= betaWarningTrigger;
            if (trigger)
            {
                ShowAboutBox(type, li, true);   // always behave as if design time indicating days Left.
            }
        }

#else
        /// <summary>
        /// Design time validation. Looks for a license in the registry and saves it in the
        /// provided <see cref="LicenseContext"/>.
        /// </summary>
        /// <param name="type">Type of licensed object (use typeof() and not GetType()).</param>
        /// <param name="context"><see cref="LicenseContext"/> where the runtime key will be stored.</param>
        /// <returns>A <see cref="LicenseInfo"/> with information about the license.</returns>
        /// <remarks>
        /// Call this method from application-type products (that always require a license to be
        /// installed in the registry). In this case, the <paramref name="context"/> parameter 
        /// should be set to null.
        /// </remarks>
        internal static LicenseInfo ValidateDesigntime(Type type, LicenseContext context)
        {
            // look for a license
            var pl = ProductLicense.GetInstalledLicense(type);

            // not found
            if (pl == null)
            {
                // return unlicensed info
                return new LicenseInfo(type, LicenseStatus.Unlicensed);
            }

#if GRAPECITY
            // found: check vendor code
            if (!pl.VendorCode.StartsWith("JP"))
            {
                // GrapeCity builds only work for VendorCode values that start with "JP"
                return new LicenseInfo(type, LicenseStatus.Unlicensed);
            }
#endif

            // found: check whether it's expired
            if (pl.IsExpired)
            {
                return new LicenseInfo(type, LicenseStatus.Expired, pl);
            }

            // not expired: save it to use at runtime
            if (context != null)
            {
				context.SetSavedLicenseKey(type, pl.RuntimeKey);
            }
            return new LicenseInfo(type, LicenseStatus.Valid, pl);
        }
        /// <summary>
        /// Runtime validation.
        /// Looks for a runtime key stored in the current application's resources.
        /// </summary>
        /// <param name="type">Type of licensed object (use typeof() and not GetType()).</param>
        /// <param name="context"><see cref="LicenseContext"/> where the runtime key will be stored.</param>
        /// <returns>A <see cref="LicenseInfo"/> with information about the license.</returns>
        internal static LicenseInfo ValidateRuntime(Type type, LicenseContext context)
        {
            // At run time, look for key stored in the entry assembly.
            //
            // Instead of using 'LicenseContext.GetSavedLicenseKey', we use a custom
            // 'SafeLicenseContext.GetSavedLicenseKey' that doesn't require special
            // permissions. This allows the code to work under partial trust.
            //
            Assembly asm = Assembly.GetEntryAssembly();
            string runtimeKey = SafeLicenseContext.GetSavedLicenseKey(context, type, asm);

            // not found in entry assembly, so look in all other assemblies loaded
            // in the current application domain.
            //
            // this handles the case where the licensed component is used in a UserControl
            // or some other application which has its own assembly where the license key
            // is stored.
            //
            // NOTE1: this could be optimized by checking whether each assembly is in 
            // asm.GetReferencedAssemblies(); not sure it's worthwhile...
            //
            // NOTE2: the original version of our licensing code supports this UserControl
            // scenario; another way to do this would be to require that the user must add 
            // an instance of the licensed control to the form; this would add the required 
            // license to the main app assembly. That would be more efficient since the 
            // license would be found right away and this loop would not even be executed.
            //
            if (runtimeKey == null)
            {
                // get an array with all executing assemblies
                // reverse the order so system assemblies come last (e.g. mscorlib etc)
                var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                Array.Reverse(assemblies);
                foreach (var refAsm in assemblies)
                {
                    if (!IsDynamicAssembly(refAsm))
                    {
                        runtimeKey = SafeLicenseContext.GetSavedLicenseKey(context, type, refAsm);
                        if (runtimeKey != null)
                            break;
                    }
                }
            }

            // no key, return invalid license
            if (runtimeKey == null)
			{
#if DEBUG && DEBUGLIC
                Debug.WriteLine("no runtime key for " + type.ToString());
#endif
                LicenseInfo evalLicInfo = new LicenseInfo(type, LicenseStatus.Unlicensed);
#if HARD_EVAL_DATE
#if RUNTIME_EVAL_REQUIRED
				evalLicInfo.GetStop = true;
				evalLicInfo.evalDaysElapsed = LicenseInfo.EVAL_DURATION * 2;
#else
                if (asm != null)
                {
                    try
                    {
                        if (asm.Location != null && File.Exists(asm.Location))
                        {
                            DateTime dtExe = File.GetLastWriteTimeUtc(asm.Location);
                            DateTime utcNow = DateTime.UtcNow;
#if DEBUG
                            string utcNowStr = Environment.GetEnvironmentVariable("LicenseTestDate");
                            if (utcNowStr != null && utcNowStr.Length > 0)
                            {
                                DateTime dtnow = DateTime.MinValue;
                                if (DateTime.TryParse(utcNowStr, out dtnow))
                                {
                                    utcNow = dtnow;
                                }
                            }
#endif
                            TimeSpan tsc = utcNow.Subtract(dtExe).Duration();
                            evalLicInfo.evalDaysElapsed = tsc.Days;
                            if (evalLicInfo.EvaluationDaysLeft < 0)
                                evalLicInfo.GetStop = true;
                        }
                    }
                    catch { }
                }
#endif
#endif
                return evalLicInfo;
            }

            // found runtime key string, now validate it
            try
            {
                // validate runtime key
#if DEBUG && DEBUGLIC
                Debug.WriteLine("good runtime key for " + type.ToString());
#endif
                var pl = new ProductLicense();
                pl.RuntimeKey = runtimeKey;

                // return valid license
                return new LicenseInfo(type, LicenseStatus.Valid, pl);
            }
            catch
            {
#if DEBUG && DEBUGLIC
                Debug.WriteLine("invalid runtime key for " + type.ToString());
#endif
            }

			// still don't have a valid runtime key.  See if it is a hardstop eval key.
			LicenseInfo evalLicenseInfo = new LicenseInfo(type, LicenseStatus.Unlicensed);
#if HARD_EVAL_DATE
			if(runtimeKey.StartsWith(evalLicenseInfo.ToString()))
			{
                evalLicenseInfo.GetStop = true;
				evalLicenseInfo.evalDaysElapsed = int.MaxValue;

				string compileDateStr = runtimeKey.Substring(evalLicenseInfo.ToString().Length);
				long utcTicks = 0;
				if (long.TryParse(compileDateStr, out utcTicks))
				{
					DateTime utcNow = DateTime.UtcNow;
#if DEBUG
					string utcNowStr = Environment.GetEnvironmentVariable("LicenseTestDate");
					if (utcNowStr != null && utcNowStr.Length > 0)
					{
						DateTime dtnow = DateTime.MinValue;
						if (DateTime.TryParse(utcNowStr, out dtnow))
						{
							utcNow = dtnow;
						}
					}
#endif
					TimeSpan tsc = utcNow.Subtract(DateTime.FromBinary(utcTicks));
					evalLicenseInfo.evalDaysElapsed = tsc.Duration().Days;
				}
            }
#endif
            // got an invalid runtime key!
            return evalLicenseInfo;
        }
#endif

#if GCLICENSING && !NOGCLICENSING && !NO_ABOUTBOX && !GRAPECITY
        private static LicenseInfo newLicenseStatus(LicenseInfo licInfo, string licenseKey)
        {
            if (licInfo == null && licenseKey != null)
            {
                licInfo = _gcLicenseInfo;
            }
            return licInfo;
        }
#endif
        /// <summary>
        /// Shows the About Box for an instance of a C1 product.
        /// </summary>
#if SMARTASSEMBLYUSAGE
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        [Obfuscation(Exclude = true)]
        internal static void ShowAboutBox(object instance)
        {
            var type = GetC1Type(instance);
            LicenseInfo licInfo = null;
#if GCLICENSING && !NOGCLICENSING && !NO_ABOUTBOX && !GRAPECITY
            if (_gcLicenseInfo == null) // && LicenseManager.UsageMode == LicenseUsageMode.Designtime)
            {
                licInfo = Validate(type, instance);

                // the above call can occur with a runtime context even at
                // design time, but the result should only be used for new licenses
                if (licInfo.License == null || !string.IsNullOrEmpty(licInfo.License.UserName))
                    licInfo = null;
            }
#endif
            if (type != null)
            {
                ShowAboutBox(type, licInfo);
            }
        }
        // shows the AboutBox for a type of C1 product using a provided <see cref="LicenseInfo"/>.
        static void ShowAboutBox(Type type, LicenseInfo licInfo)
        {
#if !NO_ABOUTBOX
#if GCLICENSING && !NOGCLICENSING && !GRAPECITY
            licInfo = newLicenseStatus(licInfo, _gcLicenseKey);

#if WPF
            // WPF calls this function via reflection.
            if(licInfo == null)
            {
                ConstructorInfo ci = type.GetConstructor(Type.EmptyTypes);
                var o = ci.Invoke(new object[0]);
                licInfo = Validate(type, o);
                if (licInfo.License == null || !string.IsNullOrEmpty(licInfo.License.UserName))
                    licInfo = null;
            }
#endif
#endif
            using (var dlg = new AboutForm(type, licInfo))
            {
                dlg.ShowDialog();
            }
#endif
        }
        // shows the AboutBox for a type of C1 product using a provided <see cref="LicenseInfo"/>.
        internal static void ShowAboutBox(Type type, LicenseInfo licInfo, bool designTime)
        {
#if !NO_ABOUTBOX
#if GCLICENSING && !NOGCLICENSING && !GRAPECITY
            licInfo = newLicenseStatus(licInfo, _gcLicenseKey);
#endif
      using (var dlg = new AboutForm(type, licInfo, designTime))
            {
                dlg.ShowDialog();
            }
#endif
        }
        /// <summary>
        /// Get the type whose assembly contains a 'C1ProductInfoAttribute'.
        /// </summary>
        /// <param name="instance">Instance of an object whose type is to be checked.</param>
        /// <returns>The type whose assembly contains a 'C1ProductInfoAttribute'</returns>
        internal static Type GetC1Type(object instance)
        {
            for (Type type = instance.GetType();
                type != null && type != typeof(object);
                type = type.BaseType)
            {
                if (IsC1Assembly(type.Assembly))
                {
                    return type;
                }
            }

            // no licensed C1 type found
            //Debug.Assert(false, "C1ProductInfoAttribute not found...");
            return null;
        }
        // checks whether a given assembly contains a 'C1ProductInfoAttribute'.
        static bool IsC1Assembly(Assembly asm)
        {
            foreach (Attribute att in asm.GetCustomAttributes(false))
            {
                if (att.GetType().Name == "C1ProductInfoAttribute")
                {
                    return true;
                }
            }
            return false;
        }
        // checks whether an object is a web control running under localhost
        static bool IsLocalHost(object obj)
        {
      // the original code checked Context.Request.UserHostAddress and 
      // Context.Request.UserHostName against "localhost", "127.0.0.1", and "::1"
      //
      // we only check Context.Request.Url against "http://localhost*"
      // seems to work fine...
      //
      //if (SecurityManager.IsGranted(new SecurityPermission(SecurityPermissionFlag.ControlDomainPolicy)))
            try
            {
                var bf = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
                foreach (string s in "Context.Request.Url".Split('.'))
                {
                    if (obj != null)
                    {
                        var pi = obj.GetType().GetProperty(s, bf);
                        if (pi != null)
                        {
                            obj = pi.GetValue(obj, null);
                        }
                    }
                }
                if (obj != null)
                {
                    var url = obj.ToString();
                    // TFS 434131: Add case https
                    return url.StartsWith("http://localhost", StringComparison.OrdinalIgnoreCase) || url.StartsWith("https://localhost", StringComparison.OrdinalIgnoreCase);
                }
            }
            catch { }

            return false;
        }
        // ASP.NET requires additional work here (this is from the original licensing code)
        static bool IsDesignTime(LicenseContext context)
        {
            if (context.UsageMode == LicenseUsageMode.Designtime)
            {
                return true;
            }

            //if (SecurityManager.IsGranted(new SecurityPermission(SecurityPermissionFlag.ControlDomainPolicy)))
            if (!_skipDomainManagerTest)
            {
                try
                {
                    return IsMsIdeDomainManager();
                }
                catch 
                {
                    // failed once? don't try again (security, etc)
                    _skipDomainManagerTest = true;
                } 
            }

            return false;
        }
        static bool _skipDomainManagerTest;
        static bool IsMsIdeDomainManager()
        {
#if !CORE31
            // Whidbey ASP.Net designer has a domain running in which the LicenseContext is runtime. 
            // It can be identified by the DomainManager.
            if (System.Environment.Version.Major >= 2)
            {
                //const string MsIdeDomainManager = "Microsoft.VisualStudio.CommonIDE.VsAppDomainManager";
                // GaryH - 20-Oct-2009
                //  in VS2010, the DomainManager changes to "Microsoft.VisualStudio.Platform.VsAppDomainManager";
                //  To try and avoid issues, assume it will change again.
                const string MsIdeDomainManagerStart = "Microsoft.VisualStudio.";
                const string MsIdeDomainManagerFinish = ".VsAppDomainManager";
                const string VS2012WpfDesigner = "Microsoft.Expression.DesignHost.Isolation.Primitives.ProcessAppDomainManager";
                var domMgr = AppDomain.CurrentDomain.DomainManager;
                if (domMgr != null)
                {
                    string domMgrStr = domMgr.ToString();
                    return 
                        (domMgrStr.StartsWith(MsIdeDomainManagerStart) && domMgrStr.EndsWith(MsIdeDomainManagerFinish)) ||
                        domMgrStr == VS2012WpfDesigner;
                }
            }
#endif
            return false;
        }
        internal static bool IsDynamicAssembly(Assembly asm)
        {
            if (asm is System.Reflection.Emit.AssemblyBuilder)
                return true;
            if (asm.GetType().FullName == "System.Reflection.Emit.InternalAssemblyBuilder")
                return true;
            return false;
        }

        // something to produce relatively unique values for strings that is consistent across platforms.
        static int ComputeHash(string strval)
        {
            if(string.IsNullOrEmpty(strval))
                return 0;

            return ComputeHash(Encoding.UTF8.GetBytes(strval));
        }
        static int ComputeHash(byte[] val)
        {
            UInt32 hash = 0;
            foreach(byte s in val)
            {
                hash += s;
                hash += (hash << 10);
                hash ^= (hash >> 6);
            }
            hash += (hash << 3);
            hash ^= (hash << 11);
            hash += (hash << 15);
            return (int)hash;
        }
    }
#endregion

    //---------------------------------------------------------------------------------
#region ** LicenseInfo

    /// <summary>
    /// Contains information about a license stored in an application.
    /// </summary>
    internal class LicenseInfo : License
    {
        // a 30 day trial period (C1 and GC)
        internal const int EVAL_DURATION = 30;
        internal int evalDaysElapsed = int.MaxValue;        // hard_eval_date
		int getStop = -1;

        internal bool GetStop 
		{
			get { return getStop > 0; }
			set
			{
                // if GetStop is explicitly set rather than calculated internally by LicenseInfo,
                // then its value should be retained.
				getStop = value ? 1 : 0;
			}
		}

        // ** abstract License overrides
        public override void Dispose()
        {
            // nothing to do...
        }
        public override string LicenseKey
        {
            get { return License != null ? License.Key : null; }
        }

        /// <summary>
        /// Initializes a new instance of a <see cref="LicenseInfo"/> class.
        /// </summary>
        internal LicenseInfo(Type type, LicenseStatus licenseStatus, ProductLicense license)
        {
            Type = type;
            LicenseStatus = licenseStatus;
            License = license;
        }
        /// <summary>
        /// Initializes a new instance of a <see cref="LicenseInfo"/> class.
        /// </summary>
        internal LicenseInfo(Type type, LicenseStatus licenseStatus)
            : this(type, licenseStatus, null) { }
        /// <summary>
        /// Gets or sets the status of this license (valid, expired, unlicensed).
        /// </summary>
        internal LicenseStatus LicenseStatus { get; set; }
        /// <summary>
        /// Gets or sets the <see cref="ProductLicense"/> associated with this license.
        /// </summary>
        internal ProductLicense License { get; set; }
        /// <summary>
        /// Gets or sets the <see cref="Type"/> associated with this license.
        /// </summary>
        internal Type Type { get; set; }
        /// <summary>
        /// Gets the number of evaluation days elapsed.
        /// Returns -1 for valid licenses.
        /// </summary>
        internal int EvaluationDaysElapsed
        {
            get
            {
#if BETA_LICENSE
                return 0;
#else
                // license is valid, return -1
                if (LicenseStatus == LicenseStatus.Valid)
                {
                    return -1;
                }

                const string usageKeyString = @"Software\ComponentOne\UsageData";
                bool hardstop = GetStop;

                DateTime toDay = DateTime.Today;
                DateTime installDate = DateTime.MinValue;

#if HARD_EVAL_DATE
#if NEVER
                if (!hardstop && getStop < 0)
                {
                    try
                    {
                        using (RegistryKey usageKey = Registry.CurrentUser.CreateSubKey(usageKeyString))
                        {
                            if (usageKey != null)
                            {
                                string localeval = usageKey.GetValue(usageKeyString.GetHashCode().ToString()) as string;
                                if (localeval != null && localeval.Length > 8)
                                {
                                    byte[] localebytes = Convert.FromBase64String(localeval);
                                    if (localebytes != null && localebytes.Length > 8)
                                    {
                                        localeval = Encoding.UTF8.GetString(localebytes);
                                        localeval = localeval.Substring(localeval.Length - 2).ToLower();
                                        GetStop = hardstop = "-jp-ko-zh-in-".IndexOf(localeval) >= 0;
                                    }
                                }
                            }
                        }
                    }
                    catch { }
                }
#endif
                if (hardstop || getStop < 0)
				{
					if (evalDaysElapsed != int.MaxValue)
						return evalDaysElapsed;

                    GetStop = false;    // assume false for hardstop.  if found, then set to true.

					// find the latest eval install date that applies to this assembly, except the primary.
					// if there aren't any, look for the primary.
					// Tehe logic is that any non-primary eval is installed by a serial number and overrides
					// the default primary install.  If none are found, then allow the primary.
					installDate = DateTime.MinValue;

                    bool remnants = false;
                    string keyname = null;
					{
						C1ProductInfoAttribute[] cpis = C1ProductInfoAttribute.GetProductInfoAttributes(this.Type);
						if (cpis != null && cpis.Length > 0)
						{
							DateTime inDate = DateTime.MinValue;
							string kyname = null;

							foreach (C1ProductInfoAttribute cpi in cpis)
							{
								if (!cpi.Primary)
								{
									string kn = cpi.ProductGUID.ToUpper();
									DateTime timeData;
                                    if (DTStorage.GetUtcDateTime(kn, out timeData))
                                    {
                                        if (timeData > inDate)
                                        {
                                            inDate = timeData;
                                            kyname = kn;
                                        }
                                    }
                                    else
                                    {
                                        if (!remnants && DTStorage.GetUtcDateTime("today" + kn, out timeData))
                                            remnants = true;
                                    }
								}
							}

							if (kyname != null)
							{
								// something other than the primary.
								keyname = kyname;
								installDate = inDate;
                                GetStop = true;
							}
							else
							{
								// nothing found.  check the primary
								C1ProductInfoAttribute cpi = C1ProductInfoAttribute.GetPrimary(cpis);
								if (cpi != null)
								{
									kyname = cpi.ProductGUID.ToUpper();
                                    GetStop = DTStorage.GetUtcDateTime(kyname, out installDate);
                                    if (GetStop) keyname = kyname;
									//if (!DTStorage.GetUtcDateTime(keyname, out installDate))
										//installDate = DTStorage.GetCurrentUtcDateTime(keyname, DateTime.MinValue);
                                    if (!GetStop && !remnants)
                                    {
                                        DateTime timeData;
                                        remnants = DTStorage.GetUtcDateTime("today" + kyname, out timeData);
                                    }
								}
							}

              // if there are remnants of an eval license, but no eval license, then hard stop.
#if NOGCLICENSING || GRAPECITY
              if (!GetStop && remnants)  
#endif
                GetStop = true;
						}
					}
                    if (GetStop)
                    {
                        toDay = (keyname == null) ? DateTime.MaxValue :
                            DTStorage.GetCurrentUtcDateTime("today" + keyname, DateTime.MaxValue);
                    }
				}

				//else    // else not hard stop
#endif
              if(!GetStop)
                {
                    //#else  
                    // assume eval starts today
                    installDate = DateTime.Today;
                    try
                    {
                        // open/create key where eval start dates are saved
                        var key = Registry.CurrentUser.CreateSubKey(usageKeyString);
                        if (key != null)
                        {
                            // open value for this type
                            var name = Type.ToString().GetHashCode().ToString();
                            object val = key.GetValue(name);
                            if (val == null)
                            {
                                // not set, so save current day and start ticking...
                                key.SetValue(name, DateTime.Today.ToBinary().ToString());
                            }
                            else if (val is string)
                            {
                                // retrieve installation date
                                installDate = DateTime.FromBinary(long.Parse((string)val));
                            }
                        }
                    }
                    catch { }
				
					toDay = DateTime.Today;
				}

#if DEBUG
                {
                    string testDate = Environment.GetEnvironmentVariable("LicenseTestDate");
                    if (testDate != null)
                    {
                        DateTime tday = installDate;
                        if (DateTime.TryParse(testDate, out tday))
                        {
                            toDay = tday;
#if DEBUGLIC
							Debug.WriteLine("Using Env variable, LicenseTestDate, date is: " + testDate);
#endif
                        }
                    }
                }
#endif
				// return number of evaluation days elapsed so far
				evalDaysElapsed = (int)toDay.Subtract(installDate).TotalDays;
                return evalDaysElapsed;
#endif
            }
        }

        internal void SaveEvalRuntimeLicense(Type type)
        {
            // geh 2015/03/10 - always save the runtime key.  if LC.exe fails
            // it does not always fail the build.  saving the expired runtime
            // key will force the runtime exception.
            DateTime utcnow = DateTime.UtcNow;
            if(this.EvaluationDaysLeft < 0)
                utcnow = utcnow.AddDays(-(EVAL_DURATION + EVAL_DURATION));

            string runkeystr = this.ToString() + utcnow.Ticks.ToString();
            LicenseManager.CurrentContext.SetSavedLicenseKey(type, runkeystr);
        }

        int _explicitEvalDaysLeft = int.MinValue;

        /// <summary>
        /// Gets the number of evaluation days still left.
        /// </summary>
        internal int EvaluationDaysLeft
        {
#if BETA_LICENSE
            get
            {
                var c1Beta = Attribute.GetCustomAttribute(this.Type.Assembly,
                    typeof(C1BetaEndSpecificationAttribute)) as C1BetaEndSpecificationAttribute;

                Debug.Assert(c1Beta != null, "Beta EndDate is not specified through C1BetaEndSpecificationAttribute.");

                int daysLeft = (int)Math.Ceiling((c1Beta.EndDate - DateTime.Today).TotalDays);
                return (daysLeft > 0) ? daysLeft : 0;
            }
#else
#if NOGCLICENSING || GRAPECITY
      get { return EVAL_DURATION - EvaluationDaysElapsed; }
#else
      get
            {
                if (_explicitEvalDaysLeft == int.MinValue)
                    _explicitEvalDaysLeft = EVAL_DURATION - EvaluationDaysElapsed;
                return _explicitEvalDaysLeft ;
            }
            set
            {
                _explicitEvalDaysLeft = value;
                if(_explicitEvalDaysLeft <= 0)
                {
                    if(_explicitEvalDaysLeft == 0)
                        _explicitEvalDaysLeft = -1;
#if !SCAFFOLDER
                    LicenseStatus = LicenseStatus.Expired;
#endif
                }
            }
#endif
#endif
    }
        /// <summary>
        /// Gets or sets a value that determines whether the caller should
        /// nag the user. This is the case when the component/control is 
        /// not licensed, but is not running in interactive mode. So we can't
        /// show a dialog and the caller is supposed to nag some other way
        /// (typically by adding watermarks to the UI or output).
        /// </summary>
        internal bool ShouldNag { get; set; }
        /// <summary>
        /// Gets a value that determines whether the license found is valid.
        /// </summary>
        internal bool IsValid
        {
            get { return LicenseStatus == LicenseStatus.Valid; }
        }
        /// <summary>
        /// Gets a value that determines whether the license found is expired.
        /// </summary>
        internal bool IsExpired
        {
            get { return LicenseStatus == LicenseStatus.Expired; }
        }
        /// <summary>
        /// Gets a value that determines whether the component is running under
        /// localhost and therefore should not nag the user with alert dialogs.
        /// </summary>
        internal bool IsLocalHost { get; set; }
#if LEGACY_LICENSING
        /// <summary>
        /// Gets a value that determines whether the license found is valid.
        /// </summary>
        [Obsolete("Use 'IsValid' or 'LicenseStatus' instead.")]
        internal bool Full
        {
            get { return IsValid; }
        }
        /// <summary>
        /// Gets a value that determines whether the license found is expired.
        /// </summary>
        [Obsolete("Use 'IsExpired' or 'LicenseStatus' instead.")]
        internal bool Expired
        {
            get { return IsExpired; }
        }
#endif
    }

#endregion

    //---------------------------------------------------------------------------------
#region ** SafeLicenseContext

    /// <summary>
    /// 
    /// Provides a version of GetSavedLicenseKey that doesn't require
    /// special permissions.
    /// 
    /// The code was mostly copied from the .NET framework, but then changed 
    /// in a few places to require fewer permissions (assembly name, file io, 
    /// etc). 
    /// 
    /// The main change is the use of a custom deserializer to read Hashtable 
    /// objects instead of the original BinaryFormatter, which requires
    /// permissions.
    /// 
    /// </summary>
    static internal class SafeLicenseContext
    {
        static Hashtable _savedLicenseKeys = new Hashtable();
        static Dictionary<Assembly, bool> _regularMethodFailed = new Dictionary<Assembly, bool>();

        // pretty much the same as the one in the .NET framework
        static internal string GetSavedLicenseKey(LicenseContext context, Type type, Assembly resAsm)
        {
            // get license key from cache if possible
            var key = (string)_savedLicenseKeys[type.AssemblyQualifiedName];
            if (key != null)
            {
                return key;
            }

            // try using the regular LicenseContext unless we already tried and failed.
            bool failed;
            if (resAsm == null || !_regularMethodFailed.TryGetValue(resAsm, out failed))
            {
                // try using the regular LicenseContext first
                // note that this will not work if the exe has been renamed
                // (because it looks for a license named exename.licenses etc)
                // so if a license is not found, we fall through and look using
                // our custom code which works even if the asm has been renamed
                // (because it looks for a license named asmname.licenses etc)
                try
                {
                    key = context.GetSavedLicenseKey(type, resAsm);
                    if (!string.IsNullOrEmpty(key))
                    {
                        _savedLicenseKeys[type.AssemblyQualifiedName] = key;
                        return key;
                    }
                }
                catch (Exception x)
                {
                    // failed, probably due to permissions; fall through and use our custom
                    // code that requires fewer permissions but has a custom-made Hashtable
                    // de-serializer that is less reliable than the built-in one.
                    Debug.WriteLine(x.Message);

                    // remember this one failed and don't try again (Exceptions are expensive!).
                    if (resAsm != null)
                    {
                        _regularMethodFailed[resAsm] = true;
                    }
                }
            }

            // try again using safer method (but still not 100% exception-proof...)
            try
            {
                key = GetSavedLicenseKeySafer(context, type, resAsm);
                if (!string.IsNullOrEmpty(key))
                {
                    _savedLicenseKeys[type.AssemblyQualifiedName] = key;
                    return key;
                }
            }
            catch (Exception x)
            {
                Debug.WriteLine(x.Message);
            }

            // failed, return null
            return null;
        }
        static string GetSavedLicenseKeySafer(LicenseContext context, Type type, Assembly resAsm)
        {
            // get resource uri from License file (ASP.NET sites)
            Uri resUri = null;
#if !CORE31
            if (resAsm == null)
            {
                var setup = AppDomain.CurrentDomain.SetupInformation;
                string relativeUri = setup.LicenseFile;
                string applicationBase = setup.ApplicationBase;
                if (relativeUri != null && applicationBase != null)
                {
                    resUri = new Uri(new Uri(applicationBase), relativeUri);
                }
            }
#endif

            // no resource Uri? do full assembly scan
            if (resUri == null)
            {
                if (resAsm == null)
                {
                    resAsm = Assembly.GetEntryAssembly();
                }
                if (resAsm == null)
                {
                    // get an array with all executing assemblies
                    // reverse the order so system assemblies come last (e.g. mscorlib etc)
                    var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    Array.Reverse(assemblies);
                    foreach (Assembly asm in assemblies)
                    {
                        if (!ProviderInfo.IsDynamicAssembly(asm))
                        {
                            try
                            {
                                string path = GetLocalPath(asm.EscapedCodeBase);
                                path = new FileInfo(path).Name;
                                Stream s = CaseInsensitiveManifestResourceStreamLookup(asm, path + ".licenses");
                                if (s != null)
                                {
                                    Deserialize(s, path);
                                    break;
                                }
                            }
                            catch { } // security, not supported, etc
                        }
                    }
                }
                else if (!ProviderInfo.IsDynamicAssembly(resAsm))
                {
                    Stream s = null;

                    // read resource by name
                    //string fileName = GetLocalPath(resAsm.EscapedCodeBase);
                    //fileName = Path.GetFileName(fileName);
                    string fileName = resAsm.ManifestModule.Name; // << no security issues
                    string name = fileName + ".licenses";
                    s = CaseInsensitiveManifestResourceStreamLookup(resAsm, name);

                    // if we got it, de-serialize it
                    if (s != null)
                    {
                        Deserialize(s, fileName);
                    }
                }
            }
            else // read from resource Uri
            {
                Stream s = OpenRead(resUri);
                if (s != null)
                {
                    string[] segments = resUri.Segments;
                    string last = segments[segments.Length - 1];
                    string name = last.Substring(0, last.LastIndexOf("."));
                    Deserialize(s, name);
                }
            }

#if FALSE
            // to allow the use of ILMerge or other merge tools, it is necesary to
            // associate the runtime license with the new assembly that the C1 control
            // has been merged into
            string qualifiedKey = (string)_savedLicenseKeys[type.AssemblyQualifiedName];

            // if there is no key found using the qualified name, find one that has the same class
            // name and version number as the C1 assembly.  If there is a match, assume it is for
            if(string.IsNullOrEmpty(qualifiedKey))
            {
                string className = type.AssemblyQualifiedName;
                className = className.Substring(0, className.IndexOf(',') + 1);
                foreach(string sk in _savedLicenseKeys.Keys)
                {
                    if(sk.StartsWith(className) && sk.IndexOf(VersionConst.VersionStr) > 0 &&
                       sk.Substring(className.Length).TrimStart().StartsWith("C1."))
                    {
                        _savedLicenseKeys.Add(type.AssemblyQualifiedName, _savedLicenseKeys[sk]);
                        break;
                    }
                }
            }
#endif
            // return key from cache
            return (string)_savedLicenseKeys[type.AssemblyQualifiedName];
        }

        // pretty much the same as the one in the .NET framework
        static string GetLocalPath(string fileName)
        {
            Uri uri = new Uri(fileName);
            return (uri.LocalPath + uri.Fragment);
        }

        // pretty much the same as the one in the .NET framework
        // (including the horrible name, kept to make maintenance easier)
        static Stream CaseInsensitiveManifestResourceStreamLookup(Assembly satellite, string name)
        {
            //string asmName = satellite.GetName().Name;
            string asmName = satellite.FullName.Split(',')[0]; // << no security issues
            foreach (string resName in satellite.GetManifestResourceNames())
            {
                if (string.Compare(resName, name, StringComparison.OrdinalIgnoreCase) == 0 ||
                    string.Compare(resName, asmName + ".exe.licenses", StringComparison.OrdinalIgnoreCase) == 0 ||
                    string.Compare(resName, asmName + ".dll.licenses", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    name = resName;
                    break;
                }
            }
            return satellite.GetManifestResourceStream(name);
        }

        // don't use BinaryFormatter since that requires permissions.
        static void Deserialize(Stream stream, string key)
        {
            var lfr = new LicensesFileReader(stream);
            if (lfr != null && lfr.IsValid)
            {
                // copy information into our table
                var hash = lfr.GetHashtable();
                foreach (string k in hash.Keys)
                {
                    _savedLicenseKeys[k] = hash[k];
                }
            }
        }
#if false // original version
        //
        // BinaryFormatter used to de-serialize the Hashtable 
        // requires permissions.
        //
        static void Deserialize(Stream stream, string cryptoKey)
        {
            var formatter = new BinaryFormatter();
            var objArray = formatter.Deserialize(stream) as object[];
            if (objArray != null)
            {
                var key = objArray[0] as string;
                var hash = objArray[1] as Hashtable;
                if (key == cryptoKey && hash != null)
                {
                    // copy information into our table
                    foreach (string k in hash.Keys)
                    {
                        _savedLicenseKeys[k] = hash[k];
                    }
                }
            }
        }
#endif
        static Stream OpenRead(Uri resourceUri)
        {
            var client = new WebClient();
            client.Credentials = CredentialCache.DefaultCredentials;
            return client.OpenRead(resourceUri.ToString());
        }

        //----------------------------------------------------------------------------
#region ** Nick Kramer's safe HashTable de-serializer

        private class LicensesFileReader
        {
            string[] _controlNames;
            string[] _licenseKeys;
            string _assemblyName;

            bool _isValid = true;
            byte[] _buffer;
            int _bufferLength;
            int _position;
            Dictionary<int, object> _loadedObjects = new Dictionary<int, object>();

            public LicensesFileReader(Stream stream)
            {
                int expectedLength = (int)stream.Length;
                _buffer = new byte[expectedLength];
                _bufferLength = stream.Read(_buffer, 0, _buffer.Length);
                Debug.Assert(_bufferLength == expectedLength);
                this.ParseLicenses();
                _buffer = null;
            }
            public bool IsValid
            {
                get { return _isValid; }
            }
            public Hashtable GetHashtable()
            {
                Hashtable keytable = null;
                if (_isValid)
                {
                    keytable = new Hashtable();
                    for (int kn = 0; kn < _controlNames.Length; kn++)
                    {
                        keytable.Add(_controlNames[kn], _licenseKeys[kn]);
                    }
                }
                return keytable;
            }
            private void Expect(string hexdump)
            {
                string simplifiedhex = hexdump.Replace(" ", string.Empty);
                Debug.Assert((simplifiedhex.Length & 1) == 0);
                for (int i = 0; i < simplifiedhex.Length / 2; i++)
                {
                    string digits = simplifiedhex.Substring(i * 2, 2);
                    if (digits != "??") // ignore this byte, we don't know what to expect
                    {
                        byte expected = byte.Parse(digits, NumberStyles.HexNumber);
                        byte actual = _buffer[_position + i];
                        Debug.Assert(expected == actual, simplifiedhex.Substring(0, i * 2 + 2) + " vs " + actual.ToString(CultureInfo.InvariantCulture));
                    }
                }
                _position += simplifiedhex.Length / 2;
            }

            // only used for a string's length, not other integers
            private int ReadVariableLengthInteger()
            {
                int length = ReadByte();
                if (length > 127)
                {
                    int secondLength = ReadByte();
                    length |= secondLength << 7;
                    if (secondLength > 127)
                    {
                        int thirdLength = ReadByte();
                        length |= thirdLength << 14;
                    }
                }
                return length;
            }
            private byte ReadByte()
            {
                byte result = _buffer[_position];
                _position++;
                return result;
            }

            // 32bit
            private int ReadInt()
            {
                Debug.Assert(_buffer.Length >= _position + 4);
                int result = _buffer[_position]
                             | (_buffer[_position + 1] << 8)
                             | (_buffer[_position + 2] << 16)
                             | (_buffer[_position + 3] << 24);
                _position += 4;
                return result;
            }
            private void RegisterLoadedObject(int objectid, object obj)
            {
                Debug.Assert(!_loadedObjects.ContainsKey(objectid)); // no duplicate IDs
                _loadedObjects[objectid] = obj;
            }
            private object GetObjectReference(int objectid)
            {
                object obj = _loadedObjects[objectid];
                Debug.Assert(obj != null);
                return obj;
            }
            private string ReadString()
            {
                int stringType = ReadByte();
                int objectid = ReadInt();

                if (stringType == 0x09) // reference to previously = loaded string
                {
                    string result = (string)GetObjectReference(objectid);
                    return result;
                }
                else if (stringType == 0x06)  // string contents = inline
                {
                    int countBytes = ReadVariableLengthInteger();

                    // characters of a string are in UTF-8 format
                    char[] characters = UTF8Encoding.UTF8.GetChars(_buffer, _position, countBytes);
                    string result = new string(characters);
                    _position += countBytes;

                    RegisterLoadedObject(objectid, result);
                    return result;
                }
                else
                {
                    // not a string object, or at least not a type of string we understand
                    throw new Exception("invalid file");
                }
            }
            private string[] ReadStringArray()
            {
                Expect("10"); // obj type=string[]
                int objectid = ReadInt();

                int countElements = ReadInt();
                List<string> stringlist = new List<string>();
                for (int i = 0; i < countElements; i++)
                {
                    string str = ReadString();
                    stringlist.Add(str);
                }
                string[] result = stringlist.ToArray();

                // not strictly necessary since the object isn't referenced again
                // later, but good practice
                RegisterLoadedObject(objectid, result);
                return result;
            }
            void ParseLicenses()
            {
                // Because BinaryFormatters aren't are part of Silverlight,
                // we have to parse it the hard way.
                //
                // The overall format is binaryformatter.Serialize(stream, new object[] {assemblyname, context.savedLicenseKeys });
                // where savedLicenseKeys is a Hashtable mapping type.AssemblyQualifiedName (string) to license key (string)
                // All we need to get out is the assembly name and the keys/values of the hash table, everything else is noise.
                //
                // The format that CLR's BinaryFormatter uses is every object starts off with a single
                // byte record type, followed by a 32-bit object ID (so objects can be referenced
                // later -- serialized objects form a graph not a tree).
                //
                // We don't try to parse the full format here, we only pay attention to those parts
                // of the file that are relevant to our task, and in the parts we don't care about,
                // throw exceptions if we see something unexpected.

                Expect("00010000 00ffffff ff010000 00000000"); // ................
                Expect("00100100 00000200 0000             "); // ..........

                _assemblyName = ReadString();

                Expect("           090300 00000403 0000001c"); // ...........
                Expect("53797374 656d2e43 6f6c6c65 6374696f"); // System.Collectio
                Expect("6e732e48 61736874 61626c65 07000000"); // ns.Hashtable....
                Expect("0a4c6f61 64466163 746f7207 56657273"); // .LoadFactor.Vers
                Expect("696f6e08 436f6d70 61726572 10486173"); // ion.Comparer.Has
                Expect("68436f64 6550726f 76696465 72084861"); // hCodeProvider.Ha
                Expect("73685369 7a65044b 65797306 56616c75"); // shSize.Keys.Valu
                Expect("65730000 03030005 050b081c 53797374"); // es..........Syst
                Expect("656d2e43 6f6c6c65 6374696f 6e732e49"); // em.Collections.I
                Expect("436f6d70 61726572 24537973 74656d2e"); // Comparer$System.
                Expect("436f6c6c 65637469 6f6e732e 49486173"); // Collections.IHas
                Expect("68436f64 6550726f 76696465 7208ec51"); // hCodeProvider..Q
                Expect("383f??00 00000a0a ??000000 09040000"); // ................ (two unknowns!)
                Expect("00090500 0000");                       // ......

                _controlNames = ReadStringArray();
                _licenseKeys = ReadStringArray();

                Expect("0b");
                if (_position != _bufferLength)
                {
                    Debug.Assert(_position == _bufferLength); // reached end = of file
                    throw new Exception("LicenseFileReader5");
                }
            }
        }

#endregion
    }

#endregion

    //---------------------------------------------------------------------------------
#region ** Assembly attributes

#if BETA_LICENSE
    /// <summary>
    /// Attribute used to specify the BETA license end date.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
#if SMARTASSEMBLYUSAGE
    [SmartAssembly.Attributes.DoNotObfuscateType]
#else
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
#endif
    internal class C1BetaEndSpecificationAttribute : Attribute
    {
        /// <summary>
        /// Creates a C1BetaEndSpecification for BETA licensing (assembly wide).
        /// </summary>
        /// <param name="endYear">Year the beta ends (e.g. 2016).</param>
        /// <param name="endMonth">Month the beta ends (1-12).</param>
        /// <param name="endDay">Day of the specified month the beta ends.</param>
        public C1BetaEndSpecificationAttribute(int endYear, int endMonth, int endDay)
        {
            DateTime edate = new DateTime(endYear, endMonth, endDay);
            EndDate = edate;
        }
        public DateTime EndDate { get; set; }
    }
#endif

    /// <summary>
    /// Attribute used to specify the product name shown on the About Box.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = false)]
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    internal class C1AboutNameAttribute : Attribute
    {
        string _aboutName;
        public C1AboutNameAttribute(string aboutName)
        {
            _aboutName = aboutName;
        }
        public string AboutName
        {
            get { return _aboutName; }
        }
    }

#if !SILVERLIGHT && !WPF
    /// <summary>
    /// Attribute used to specify assemblies that can use this product without a license.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = true)]
#if SMARTASSEMBLYUSAGE
    [SmartAssembly.Attributes.DoNotObfuscateType]
#else
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
#endif
    internal sealed class C1AllowedCallerAttribute : Attribute
    {
        public C1AllowedCallerAttribute(params string[] tokens)
        {
            AllowedAssemblies = tokens;
        }

        private string[] AllowedAssemblies
        {
            get;
            set;
        }

        static private string GetStackframeAssemblyName(StackFrame sf)
        {
            string name = null;
            try
            {
                var mi = sf.GetMethod();
                if (mi != null)
                {
                    var rt = mi.ReflectedType;
                    if (rt != null)
                    {
                        var asm = rt.Assembly;
                        if (asm != null)
                            name = asm.FullName;
                    }
                }
            }
            catch { name = null; }
            return name;
        }

        static internal bool IsAllowedAssembly()
        {
            C1AllowedCallerAttribute[] cacas = null;

            try
            {
                cacas = Assembly.GetExecutingAssembly().
                    GetCustomAttributes(typeof(C1AllowedCallerAttribute), false) as C1AllowedCallerAttribute[];
            }
            catch { cacas = null; }

            if (cacas != null && cacas.Length > 0)
            {
                List<string> tokens = new List<string>();
                foreach (C1AllowedCallerAttribute caca in cacas)
                {
                    var allowed = caca.AllowedAssemblies;
                    if (allowed != null && allowed.Length > 0)
                        tokens.AddRange(allowed);
                }

                if (tokens != null && tokens.Count > 0)
                {
                    const string pkt = "PublicKeyToken=";

                    string thisAssm = Assembly.GetExecutingAssembly().FullName;
                    int tokenIndex = thisAssm.LastIndexOf(pkt, StringComparison.OrdinalIgnoreCase);
                    if (tokenIndex > 0)
                    {
                        string thisToken = thisAssm.Substring(tokenIndex);
                        var stackFrames = new System.Diagnostics.StackTrace().GetFrames();
                        if (stackFrames != null && stackFrames.Length > 0)
                        {
#if DEBUG && DEBUGLIC
                            {
                                // for debug purposes only.  Never used.
                                foreach (StackFrame sf in stackFrames)
                                {
                                    string aqn = GetStackframAssemblyName(sf);  //sf.GetMethod().ReflectedType.Assembly.FullName;
                                    Debug.WriteLine("Caller: {0}, {1}", sf.GetMethod().Name, aqn);
                                }
                                Debug.WriteLine("=".PadRight(20, '='));
                            }
#endif
                            foreach (StackFrame sf in stackFrames)
                            {
                                string aqn = GetStackframeAssemblyName(sf); //sf.GetMethod().ReflectedType.Assembly.FullName;
#if DEBUG && DEBUGLIC
                                Debug.WriteLine("Caller: {0}, {1}{2}", sf.GetMethod().Name, aqn,
                                    aqn == thisAssm ? "-- skipping" : "");
#endif
                                if (aqn == thisAssm) continue;

                                if (aqn != null)
                                {
                                    int aqnIndex = aqn.LastIndexOf(pkt, StringComparison.OrdinalIgnoreCase);
                                    string aqnToken = aqn.Substring(aqnIndex);
                                    if (aqnToken == thisToken) continue;

                                    foreach (string token in tokens)
                                    {
#if DEBUG && DEBUGLIC
                                        Debug.WriteLine("Testing: " + token);
#endif
                                        string[] ts = token.Split(';');
                                        if (ts != null && ts.Length > 0)
                                        {
                                            int sta = 0;
                                            foreach (string t in ts)
                                            {
                                                sta = aqn.IndexOf(t, sta, StringComparison.OrdinalIgnoreCase);
                                                if (sta < 0) break;
                                                sta += t.Length;
                                            }
                                            if (sta >= 0)
                                            {
#if DEBUG
                                                Debug.WriteLine("License Calling Assembly: " + aqn);
#endif
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
#endif

    /// <summary>
    /// Attribute used to attach licensing/product information to assemblies.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
#if SMARTASSEMBLYUSAGE
    [SmartAssembly.Attributes.DoNotObfuscateType]
#else
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
#endif
    internal class C1ProductInfoAttribute : Attribute
    {
        string _productCode;
        string _productGUID;

        public C1ProductInfoAttribute(string productCode, string productGUID)
        {
            Debug.Assert(productCode.Length == 2 || productCode == null, "Invalid productCode");
            Debug.Assert(productGUID.Length == 36, "Invalid productGUID");
            _productCode = productCode;
            _productGUID = productGUID;
        }
        public string ProductCode
        {
            get { return _productCode; }
        }
        public string ProductGUID
        {
            get { return _productGUID; }
        }

#if HARD_EVAL_DATE
        bool _primary = false;

        public bool Primary
        {
            get { return _primary; }
        }

        public C1ProductInfoAttribute(string productCode, string productGUID, bool primary)
        {
            Debug.Assert(productCode.Length == 2 || productCode == null, "Invalid productCode");
            Debug.Assert(productGUID.Length == 36, "Invalid productGUID");
            _productCode = productCode;
            _productGUID = productGUID;
            _primary = primary;
        }

        static public C1ProductInfoAttribute[] GetProductInfoAttributes(Type type)
        {
            // this method gets an C1ProductInfoAttribute[] for the assembly of the specified type.
            // Note that the attributes returned are consistent with the currently executing assembly
            // even if the type is not.  primaryOnly indicates that only the attribute marked primary
            // is returned, even if others are available.
            C1ProductInfoAttribute[] cpis = null;

            Assembly asm = (type == null) ? Assembly.GetExecutingAssembly() : type.Assembly;

            if (asm == Assembly.GetExecutingAssembly())
            {
                cpis = asm.GetCustomAttributes(typeof(C1ProductInfoAttribute), false) as C1ProductInfoAttribute[];
            }
            else
            {
                const string attName = "C1.Util.Licensing.C1ProductInfoAttribute";
                type = asm.GetType(attName, false);
#if SILVERLIGHT
                if (type == null)
                {
                    // asm.GetCustomerAttributes(false) cannot be used in the general case because
                    // it fails with an exception for assemblies that do not permit reflection due
                    // to security.  For Silverlight design time, it appears to be okay.
                    object[] oatts = null;
                    try { oatts = asm.GetCustomAttributes(false); }
                    catch { oatts = null; }
                    
                    if (oatts != null && oatts.Length > 0)
                    {
                        foreach (object oatt in oatts)
                        {
                            if (oatt.GetType().FullName == attName)
                            {
                                type = oatt.GetType();
                                break;
                            }
                        }
                    }
                }
#endif
                if(type == null)
                {
                    // if the Licensing and C1ProductInfoAttribute is not defined in the assembly
                    // then it must be using the internals of this assembly.
                    cpis = asm.GetCustomAttributes(typeof(C1ProductInfoAttribute), false) as C1ProductInfoAttribute[];
                }
                else if (Convert.ToBase64String(asm.GetName().GetPublicKeyToken()) == Convert.ToBase64String(Assembly.GetExecutingAssembly().GetName().GetPublicKeyToken()))
                {
                    // if the type is defined in a different assembly, then get the attributes, but
                    // copy them to the locally defined attribute type.
                    Attribute[] atts = asm.GetCustomAttributes(type, false) as Attribute[];
                    if (atts != null && atts.Length > 0)
                    {
                        try
                        {
                            cpis = new C1ProductInfoAttribute[atts.Length];
                            cpis.Initialize();
                            for(int cpi=0; cpi < cpis.Length; cpi++)
                            {
                                string piCode = type.GetProperty("ProductCode").GetValue(atts[cpi], null) as string;
                                string piGuid = type.GetProperty("ProductGUID").GetValue(atts[cpi], null) as string;
                                if (piCode != null && piGuid != null)
                                {
                                    bool primary = (bool)type.GetProperty("Primary").GetValue(atts[cpi], null);
                                    cpis[cpi] = new C1ProductInfoAttribute(piCode, piGuid, primary);
                                }
                            }
                        }
                        catch { }
                    }
                }
            }
            return cpis;
        }

        static public C1ProductInfoAttribute GetPrimary(Type type)
        {
            return GetPrimary(C1ProductInfoAttribute.GetProductInfoAttributes(type));
        }

        static public C1ProductInfoAttribute GetPrimary(C1ProductInfoAttribute[] cpis)
        {
            C1ProductInfoAttribute cpi = null;
            if (cpis != null && cpis.Length > 0)
            {
                foreach (C1ProductInfoAttribute cpia in cpis)
                {
                    if (cpia.Primary)
                    {
                        cpi = cpia;
                        break;
                    }
                }
            }
            //Debug.Assert(cpi != null, "No Primary ProductInfo specified");
            return cpi;
        }
#else
        public C1ProductInfoAttribute(string productCode, string productGUID, bool primary) : this(productCode, productGUID)
        {
        }
#endif
    }

#if HARD_EVAL_DATE && !BETA_LICENSE
    static internal class DTStorage
    {
        const int trigger = 201;
        const string KeyPath = @"Software\ComponentOne\UsageData";
        const string FilePath = @"\GrapeCity\Entropy";

        [DllImport("Kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        extern static bool GetVolumeInformation(string RootPathName, StringBuilder VolumeNameBuffer,
            int VolumeNameSize, out uint VolumeSerialNumber, out uint MaximumComponentLength,
            out uint FileSystemFlags, StringBuilder FileSystemNameBuffer, int nFileSystemNameSize);

        static string getVolumeId()
        {
            uint vsn = 0, dummy = 0;
            string root = Directory.GetDirectoryRoot(Environment.GetFolderPath(Environment.SpecialFolder.System));

            string vid = null;
            try
            {
                vid = GetVolumeInformation(root, null, 0, out vsn, out dummy, out dummy, null, 0)
                        ? vsn.ToString("X") : Environment.MachineName;
            }
            catch
            {
                vid = Environment.MachineName;
            }
            return vid;
        }

        static private string userFilePath(string key)
        {
            return userFilePath(key, false);
        }

        static private string userFilePath(string key, bool create)
        {
            // generate a path that design time user can always write given a value key filename.
            string result = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + FilePath;
            try
            {
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(result);
                if (!di.Exists)
                {
                    if (create)
                    {
                        di.Create();
                        di.Attributes = System.IO.FileAttributes.Directory | System.IO.FileAttributes.Hidden | System.IO.FileAttributes.NotContentIndexed;
                    }
                }
                result += "\\" + key;
            }
            catch
            {
                result = null;
            }

            return result;
        }

        static private string keyString(string key)
        {
            // one way encryption of a string.  The result must be
            // allowed filename characters.
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(key)).TrimEnd('=');
        }

        static private string valueString(string key, string value)
        {
            // encrypt incoming string into something else that can
            // be retrieved by valueStringParse
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(value)).TrimEnd('=');
        }

        static private string valueStringParse(string key, string value)
        {
            // decrypt incoming string into the original text created
            // by valueString
            value = value.PadRight((value.Length + 3) & (~3), '=');
            return Encoding.UTF8.GetString(Convert.FromBase64String(value));
        }

        static private string junk(int minlen)
        {
            double stuff = Math.PI;
            StringBuilder sb = new StringBuilder();
            for (int i = DateTime.UtcNow.Second; sb.Length < minlen; i++)
            {
                string ss = stuff.ToString();
                int j = i % 15;
                if (j > ss.Length - 3) j = ss.Length - 3;
                sb.Append(stuff.ToString().Substring(j));
                stuff += Math.PI;
            }
            return keyString(sb.ToString());
        }

        static private string swab(string value)
        {
            char[] cs = value.ToCharArray();
            for (int i = 0; i < value.Length / 2; i++)
            {
                char c = cs[i];
                cs[i] = cs[cs.Length - 1 - i];
                cs[cs.Length - 1 - i] = c;
            }
            return new string(cs);
        }

        static private string makeFileContent(string content)
        {
            // encrypt incoming string into something else that can
            // be retrieved by parseFileContent

            string cl = content.Length.ToString("000") + junk(content.Length + trigger + 500);
            content = cl.Substring(0, trigger) + swab(content) + cl.Substring(trigger);
            return content;
        }

        static private string parseFileContent(string content)
        {
            // decrypt incoming string into the original text created
            // by makeFileContent.
            int ip = 0;
            if (content.Length < 3) return null;

            if (int.TryParse(content.Substring(0, 3), out ip) && content.Length > ip + trigger)
            {
                content = content.Substring(trigger, ip);
                content = swab(content);
            }
            return content;
        }

        static internal void Marvin(DateTime marvin, object interest)
        {
            C1ProductInfoAttribute c1p = null;
            if(interest is Type || interest == null)
            {
                c1p = C1ProductInfoAttribute.GetPrimary((Type)interest);
            }
            else if(interest is string)
            {
                string[] ss = ((string)interest).Split(';');
                c1p = new C1ProductInfoAttribute(ss[0], ss[1]);
            }
            
            if (c1p != null)
            {
                // if bill gates' birthday, reset by deleting storage objects.
                if (marvin.Date == DateTime.FromBinary(616881312000000000))
                {
                    string[] keystrings = new string[] { keyString(c1p.ProductGUID), keyString("today" + c1p.ProductGUID) };

                    try
                    {
                        using (RegistryKey hkey = Registry.CurrentUser.OpenSubKey(KeyPath,true))
                        {
                            if (hkey != null)
                            {
                                foreach (string keystring in keystrings)
                                {
                                    try { hkey.DeleteValue(keystring); }
                                    catch { }
                                }
                            }
                        }
                    }
                    catch {}

                    foreach (string keystring in keystrings)
                    {
                        try
                        {
                            string fp = userFilePath(keystring);
                            if(File.Exists(fp)) File.Delete(fp);
                        }
                        catch { }
                    }
                }
            }
        }

        static private void WriteAllText(string filename, string text)
        {
            int count = 5;
            do
            {
                try
                {
                    FileMode mode = FileMode.OpenOrCreate;
                    if (File.Exists(filename)) mode |= FileMode.Truncate;   // only add if the file exists or failure.
                    FileStream fs = File.Open(filename, mode, FileAccess.ReadWrite, FileShare.None);
                    if (fs != null)
                    {
                        count = 0;
                        byte[] bytes = Encoding.UTF8.GetBytes(text);
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                    }
                }
                catch (IOException e)
                {
                    count = e.Message.Contains("another process") ? count - 1 : 0;
                    if (count > 0)
                        System.Threading.Thread.Sleep(100);
                    else
                        throw e;
                }
            } while (count > 0);
        }

        static private string ReadAllText(string filename)
        {
            string result = null;
            int count = 5;
            do
            {
                try
                {
                    result = File.ReadAllText(filename);
                    if (result != null && result.Length > 0) count = 0;
                }
                catch (IOException e)
                {
                    count = e.Message.Contains("another process") ? count - 1 : 0;
                    if (count > 0)
                        System.Threading.Thread.Sleep(100);
                    else
                        throw e;
                }
            } while (count > 0);
            return result;
        }

        static internal bool SaveUtcDateTime(string key, DateTime timeData)
        {
            return SaveUtcDateTime(key, timeData, null);
        }

        static internal bool SaveUtcDateTime(string key, DateTime timeData, string payload)
        {
            return SaveUtcDateTime(key, timeData, payload, false);
        }

        static internal bool SaveUtcDateTime(string key, DateTime timeData, string payload, bool forceCreationTime)
        {
            bool result = false;
            string keystring = keyString(key);
            string valuestring = valueString(keystring, getVolumeId() + ((payload==null) ? ";" : ":" + payload + ";") + timeData.ToBinary().ToString());
            try
            {
                using (RegistryKey hkey = Registry.CurrentUser.CreateSubKey(KeyPath))
                {
                    if (hkey != null)
                    {
                        hkey.SetValue(keystring, valuestring);

                        string fp = userFilePath(keystring, true);
                        bool newFile = !File.Exists(fp) || forceCreationTime;
                        WriteAllText(fp, makeFileContent(valuestring));
                        if (newFile) File.SetCreationTimeUtc(fp, timeData);

                        result = true;
                    }
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        static internal bool GetUtcDateTime(string key, out DateTime timeData)
        {
            string payload = null;
            return GetUtcDateTime(key, out timeData, out payload);
        }

        static internal bool GetUtcDateTime(string key, out DateTime timeData, out string payload)
        {
            // get the registry and file data for the date based on the key.
            // return true (return data valid) if the data can be read regardless
            // of its value.
            //
            // if for any reason the data is deemed inaccurate, set the timeData to MinValue

            bool result = true;
            DateTime creationTime = DateTime.MaxValue;
            timeData = DateTime.MinValue;
            payload = null;

            string keystring = keyString(key);
            string valuestring = null;
            try
            {
                string hkcontent = null;
                string fkcontent = null;

                using (Microsoft.Win32.RegistryKey hkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(KeyPath))
                {
                    if (hkey != null) hkcontent = hkey.GetValue(keystring) as string;
                }

                {
                    string fcontent = userFilePath(keystring);
                    System.IO.FileInfo fi = new FileInfo(fcontent);
                    if (fi.Exists && fi.CreationTimeUtc <= fi.LastWriteTimeUtc)
                    {
                        fkcontent = parseFileContent(ReadAllText(fcontent));
                        creationTime = fi.CreationTimeUtc;
                    }
                }

                // if either of the contents are found, then the result is true.
                result = hkcontent != null || fkcontent != null;

                if (hkcontent == fkcontent)
                    valuestring = hkcontent;

                if (valuestring != null && valuestring.Length > 0)
                {
                    valuestring = valueStringParse(keystring, valuestring);

                    string vid = getVolumeId();
                    if (valuestring != null && valuestring.Length > vid.Length + 1)
                    {
                        string[] vs = valuestring.Split(';');
                        valuestring = null;

                        if (vs.Length == 2)
                        {
                            string[] vsp = vs[0].Split(':');
                            if (vsp.Length == 2)
                            {
                                payload = vsp[1];
                                vs[0] = vsp[0];
                            }

                            if (vs[0] == vid)
                            {
                                valuestring = vs[1];
                            }
                        }
                    }

                    if (valuestring != null && valuestring.Length > 0)
                    {
                        long ldate = long.MinValue;
                        if (long.TryParse(valuestring, out ldate))
                        {
                            DateTime dataTime = DateTime.FromBinary(ldate);
                            if (dataTime >= creationTime)
                            {
                                timeData = dataTime;
                            }
                        }
                    }
                }
            }
            catch
            {
                timeData = DateTime.MinValue;
                result = true;
            }
            return result;
        }

        static internal DateTime GetCurrentUtcDateTime(string key, DateTime defaultTime)
        {
            DateTime result = DateTime.MinValue;
            DateTime utcNow = DateTime.UtcNow;

#if DEBUG
            {
                string sUtcNow = Environment.GetEnvironmentVariable("UTCNOW");
                if (sUtcNow != null && sUtcNow.Length > 0)
                {
                    DateTime utcNowTest = DateTime.MinValue;
                    if (DateTime.TryParse(sUtcNow, out utcNowTest))
                        utcNow = utcNowTest;
                }
            }
#endif
            if (GetUtcDateTime(key, out result) && result <= utcNow && result > DateTime.MinValue)
            {
                // successfull, normal behavior, with the dates in the correct order
                // do this first because it happens most often.
                SaveUtcDateTime(key, utcNow);
                result = utcNow;
                return result;
            }

            // failed to get the correct date from file and registry.
            // if the file and registry data does not exist at all, then
            // store and report the current time, otherwise the defaultTime.
            result = defaultTime;

            bool regkeyfound = true;
            bool filefound = true;

            string keystring = keyString(key);
            try
            {
                using (RegistryKey hkey = Registry.CurrentUser.OpenSubKey(KeyPath))
                {
                    regkeyfound = false;
                    if (hkey != null)
                    {
                        string hkcontent = hkey.GetValue(keystring) as string;
                        regkeyfound = (hkcontent != null && hkcontent.Length > 0);
                    }
                }

                {
                    string fcontent = userFilePath(keystring);
                    filefound = File.Exists(fcontent);
                }
            }
            catch
            {
                regkeyfound = true;
                filefound = true;
            }

            if (!regkeyfound && !filefound)
            {
                result = utcNow;
                SaveUtcDateTime(key, result);
            }

            return result;
        }
    }
#endif

#if LEGACY_LICENSING
    /// <summary>
    /// Attribute used to attach support information to assemblies.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    [Obsolete("This is no longer used, just remove it.", true)]
    internal class C1ProductSupportAttribute : System.Attribute
    {
        internal readonly string SupportURL;
        internal readonly string NewsgroupURL;
        internal readonly string UpdatesURL;

        public C1ProductSupportAttribute(string supportURL, string newsgroupURL, string updatesURL)
        {
            SupportURL   = string.IsNullOrEmpty(supportURL)   ? string.Empty : supportURL;
            NewsgroupURL = string.IsNullOrEmpty(newsgroupURL) ? string.Empty : newsgroupURL;
            UpdatesURL   = string.IsNullOrEmpty(updatesURL)   ? string.Empty : updatesURL;
        }
    }

    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    [Obsolete("Replace with a C1ProductInfoAttribute.", true)]
    internal class C1StudioKeyAttribute : C1ProductInfoAttribute
    {
        public C1StudioKeyAttribute(string studioGUID)
            : base(null, studioGUID) { }
    }

    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    [Obsolete("Replace with a C1ProductInfoAttribute.", true)]
    internal class C1ProductExtraCodeAttribute : C1ProductInfoAttribute
    {
        public C1ProductExtraCodeAttribute(string code, string guid)
            : base(code, guid) { }
    }
#endif // LEGACY_LICENSING

#endregion
}
#pragma warning restore CS0436
