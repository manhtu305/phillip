using System;

namespace SmartAssembly.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Delegate | AttributeTargets.Enum | AttributeTargets.Field | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Module | AttributeTargets.Struct | AttributeTargets.Property)]
    [DoNotObfuscateType]
    internal sealed class DoNotObfuscateAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class DoNotObfuscateTypeAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Constructor | AttributeTargets.Delegate | AttributeTargets.Enum | AttributeTargets.Event | AttributeTargets.Field | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Module | AttributeTargets.Parameter | AttributeTargets.Property | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class DoNotPruneAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class DoNotPruneTypeAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class DoNotSealTypeAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Method)]
    [DoNotObfuscateType]
    internal sealed class ReportExceptionAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class ObfuscateControlFlowAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class DoNotObfuscateControlFlowAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Field | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class ObfuscateToAttribute : Attribute
	{
		public ObfuscateToAttribute(string newName)
		{
		}
	}

 	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class ObfuscateNamespaceToAttribute : Attribute
	{
		public ObfuscateNamespaceToAttribute(string newName)
		{
		}
	}

   	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Module | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class DoNotEncodeStringsAttribute : Attribute
	{
	}

 	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class EncodeStringsAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Module | AttributeTargets.Struct)]
    [DoNotObfuscateType]
    internal sealed class ExcludeFromMemberRefsProxyAttribute : Attribute
	{
	}
}
