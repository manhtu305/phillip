// 1.07
function c1cb_createContext(id, controlStateFieldID, waitMessage, waitControl, action, onError, onWait)
{
    this.id = id;
    this.isBlocked = false;
    this.callback = null;
    this.contolStateFieldId = controlStateFieldID;
    this.controlStateField = document.getElementById(controlStateFieldID);
    this.argument = "";
    this.value = "";
    this.tmp = null;
    this.tmp2 = null;
    this.sender = null;
    this.ctrlId = "";
    
    this.beforeCallback = null;
    this.afterCallback = null;
    this.waitMessage = waitMessage;
    this.messsageItem = null;
    this.waitControl = waitControl;
    this.action = action.toLowerCase();
    this.onErrorCallback = onError;
		this.onWait = onWait;
}


function c1cb_doCallback(e, context, value, beforeCallbackFunc, afterCallbackFunc)
{
	if (!context.isBlocked)
	{
		context.isBlocked = true;
		context.value = value;

		if (!e)
			e = window.event;
		
		if (!e)
		{
			if (document && document.activeElement)
				e = document.activeElement;
		}
		else
			e = (e.target) ? e.target : e.srcElement;
		
		context.sender = e;
		context.ctrlId = (e && e.id) ? e.id : "";
        
		context.beforeCallback = beforeCallbackFunc;
		context.afterCallback = afterCallbackFunc;
		context.argument = "";

		if (context.beforeCallback)
			context.beforeCallback(context);

		c1cb_usrCallbackStart(context); 
          
		context.argument = c1cb__encodeString(new Array(value, context.argument.toString()));    
 
		context.callback();
	}
}


function c1cb_onCallback(result, context)
{
    if (context)
    {
        try
        {
            if (result)
            {
                var res = c1cb__decodeString(result);
                c1cb_resetControlState(res[1], context);
        
                if (context.afterCallback)
                    context.afterCallback(res[0], context);
            }
        
            c1cb_usrCallbackEnd(context); 
        }
        catch (e)
        {
            //alert(e.name + ": " + e.message);
        }
        finally
        {
            context.isBlocked = false;            
        }
    }
}


function c1cb_onErrorCallback(result, context)
{
    if (context)
    {
        try
        {
            c1cb_usrCallbackEnd(context); 
    
            if (context.onErrorCallback)
                eval(context.onErrorCallback + "(result, context)");
        }
        catch (excpt)
        {
            //alert(e.name + ": " + e.message);
        }
        finally
        {
            context.isBlocked = false;
        }                            
    }
}


function c1cb_usrCallbackStart(context)
{
    if (context.action == "disable")
        c1cb_disableControl(context.id);

    if (context.onWait)
        eval(context.onWait + "(context, true)");
    else        
        if (context.waitMessage)
            c1cb_showWaitMessage(context);
        else
            if (context.waitControl)
                c1cb_showWaitControl(context);          
}

function c1cb_usrCallbackEnd(context)
{
    if (context.action == "disable")
        c1cb_enableControl(context.id);
        
    if (context.onWait)
        eval(context.onWait + "(context, false)");
    else
        if (context.waitMessage && context.messageItem)
            c1cb_releaseMessage(context.messageItem); 
        else
            if (context.waitControl)
                c1cb_hideWaitControl(context);  
}


function c1cb_disableControl(target)
{
    c1cb_setControlEnabled(target, false, true);
}

function c1cb_enableControl(target)
{
    c1cb_setControlEnabled(target, true, true);
}


function c1cb_showWaitMessage(context)
{
    var obj = (context.ctrlId) ? document.getElementById(context.ctrlId) : document.getElementById(context.id);
    context.messageItem = c1cb_createMessage(context.waitMessage, obj, null, null, null);
    //context.messageItem = c1cb_createMessage(context.waitMessage, null, null, null, null);
}


function c1cb_showWaitControl(context)
{
    c1cb_setElementDisplay(context.waitControl, "");
}


function c1cb_hideWaitControl(context)
{
    c1cb_setElementDisplay(context.waitControl, "none");
}

function c1cb_resetControlState(newValue, context)
{
    c1cb__resetHiddenField(context.contolStateFieldId, newValue);
    if (context.controlStateField)
        context.controlStateField.value = newValue;
}

function c1cb__resetHiddenField(name, value)
{
    //update __theFormPostCollection
    if (typeof(__theFormPostCollection) != "undefined")
        for (var i = 0; i < __theFormPostCollection.length; i++)
            if (__theFormPostCollection[i].name == name)
            {
                __theFormPostCollection[i].value = value;
                break;
            }                
    
    //update __theFormPostData
    if (typeof(__theFormPostData) != "undefined")
        __theFormPostData = c1cb__replace(__theFormPostData, name, value, true);
}


function c1cb__decodeString(value)
{
    var res = new Array();
    
    while (value && (value != ""))
    {
        var idx = value.indexOf(",");
        var len = parseInt(value.substr(0, idx));
        res[res.length] = value.substr(idx + 1, len);
        value = value.substr(idx + 1 + len);  
    }
    
    return res;
}


function c1cb__encodeString(values)
{
    var res = "";
    var len = values.length;
    
    for (var i = 0; i < len; i++)
        res += values[i].length + "," + values[i];
    
    return res;
}


function c1cb__replace(original, name, newValue, encode)
{
    var tmp = name + "=";
    
	  if (encode)
        newValue = WebForm_EncodeCallback(newValue);

	var start = 0;
	if (original.substr(0, tmp.length) != tmp)
	{
		tmp = "&" + tmp;
		start = original.indexOf(tmp);
	}    
    
    if (start >= 0)
    {
      start += tmp.length;
     
      var end = original.indexOf("&", start);
    
      var oldValue = original.substr(start, end - start);
      original = original.replace(tmp + oldValue, tmp + newValue);
    }
    
    return original;
}


//====================================//
function c1cb_createMessage(html, obj, x, y, style)
{
    var msg = document.createElement("div");
     
    if (typeof(x) == "number")
    {
        document.body.appendChild(msg);
        msg.style.position = "absolute";
        msg.style.left = x + "px";
        msg.style.top = y + "px";
    }
    else
        if (obj && typeof(obj) == "object" && obj.appendChild)
            obj.appendChild(msg);
        else
            document.body.appendChild(msg);

    msg.innerHTML = html;
    c1cb_setstyle(msg, __c1msgDefaultStyle);
    
    if (typeof(style) == "string")
        c1cb_setstyle(msg, style);    
        
    return msg;
}


// Releases the message
function c1cb_releaseMessage(msg)
{
    if (msg && msg.parentNode)
        msg.parentNode.removeChild(msg);
}


function c1cb_setElementDisplay(target, val)
{
	if (typeof(target) == "object")
		el = target;
	else
        if (typeof(target) == "string")
		    el = document.getElementById(target);
		    
	if (el && el.tagName)
		el.style.display = val;
}



function c1cb_setstyle(item, style)
{
	var	ss = style.split(";");
	for	(var i = 0;	i <	ss.length; i++)
	{
		var	pair = c1cb_splitTwice(ss[i], ":");

		if (pair.length	== 2)
		{
			if (pair[0]	== "className")
				item.className = pair[1];
			else
				item.style[pair[0]]	= pair[1];
		}
	}
}


function c1cb_splitTwice(str, ch)
{
	var	res	= new Array();
	if (str)
	{
		var	i =	str.indexOf(ch);
		if (i >= 0)
		{
			res[0] = str.substr(0, i);
			res[1] = str.substr(i +	1, str.length);
		}
	}
	return res;
}



function c1cb_setControlEnabled(target, val, root)
{
    var el = null;

    if (typeof(target) == "object")
        el = target;
    else if (typeof(target) == "string")
        el = document.getElementById(target);
        
    if (el && el.tagName)
    {
	    if (!el._c1helper)
		    el._c1helper = new c1cb_helper(el, root);
		
        if (val)
	    {
		    el.disabled = "";
		    el._c1helper.Enable();
	    }
        else
	    {
		    el.disabled = "disabled";
		    el._c1helper.Disable();
	    }
            
        for (var i = 0; i < el.childNodes.length; i++)
		    c1cb_setControlEnabled(el.childNodes[i], val, false);
    }
}


function c1cb_helper(el, root)
{
	this._color = el.style.color;
	if (el.href)
	    this._href = el.href;
	this.Enable = function() {
		el.style.color = this._color;
		//if (el.href)
		//    el.href = this._href;
		if (root && el.removeEventListener)
		{
			el.removeEventListener("blur", c1cb_filterEvent, true);
			el.removeEventListener("click", c1cb_filterEvent, true);
			el.removeEventListener("dblclick", c1cb_filterEvent, true);
			el.removeEventListener("focus", c1cb_filterEvent, true);
			el.removeEventListener("keydown", c1cb_filterEvent, true);
			el.removeEventListener("keypress", c1cb_filterEvent, true);
			el.removeEventListener("keyup", c1cb_filterEvent, true);
			el.removeEventListener("mousedown", c1cb_filterEvent, true);
			el.removeEventListener("mousemove", c1cb_filterEvent, true);
			el.removeEventListener("mouseout", c1cb_filterEvent, true);
			el.removeEventListener("mouseover", c1cb_filterEvent, true);
			el.removeEventListener("mouseup", c1cb_filterEvent, true);
			el.removeEventListener("resize", c1cb_filterEvent, true);
		}
	}		
	this.Disable = function() {
		el.style.color = __c1cb_disabledColor;
		//if (el.href)
		//    el.href = "javascript:function(ev) {ev.returnValue = false;}";
		if (root && el.addEventListener)
		{
			el.addEventListener("blur", c1cb_filterEvent, true);
			el.addEventListener("click", c1cb_filterEvent, true);
			el.addEventListener("dblclick", c1cb_filterEvent, true);
			el.addEventListener("focus", c1cb_filterEvent, true);
			el.addEventListener("keydown", c1cb_filterEvent, true);
			el.addEventListener("keypress", c1cb_filterEvent, true);
			el.addEventListener("keyup", c1cb_filterEvent, true);
			el.addEventListener("mousedown", c1cb_filterEvent, true);
			el.addEventListener("mousemove", c1cb_filterEvent, true);
			el.addEventListener("mouseout", c1cb_filterEvent, true);
			el.addEventListener("mouseover", c1cb_filterEvent, true);
			el.addEventListener("mouseup", c1cb_filterEvent, true);
			el.addEventListener("resize", c1cb_filterEvent, true);
		}
	}		
}

function c1cb_filterEvent(ev)
{
	ev.preventDefault();
	ev.cancelBubble = true;
}

var __c1cb_disabledColor = "silver";
var __c1msgDefaultStyle = "borderStyle:solid;borderWidth:1px;borderColor:#e0e0e0;backgroundColor:#F1F1F1;paddingLeft:10px;paddingRight:10px;paddingTop:3px;paddingBottom:3px;fontFamily:Tahoma;fontSize:8pt;width:100px;";

