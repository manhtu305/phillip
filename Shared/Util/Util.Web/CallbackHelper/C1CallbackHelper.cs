using System;
using System.Collections; 
using System.Diagnostics;
using System.Web.UI;


namespace C1.Web.C1CallbackHelper
{
    internal class C1CallbackHelper
    {
		private const string S_ERROR = "owner should be a System.Web.UI.Control descendant and implement IC1Callback1 interface";
        private Control _owner = null;
        private IC1Callback1 _iowner1 = null;
        private IC1Callback2 _iowner2 = null;
        private string _onErrorCallback = string.Empty;
        private string _onWait = string.Empty;
        private bool _useAsync = true;
                
        public C1CallbackHelper(Control owner) : base()
        {
            Trace.Assert((owner != null) && (owner is IC1Callback1) && (owner is ICallbackEventHandler), S_ERROR);
            _owner = owner;
            _iowner1 = (owner as IC1Callback1);
            _iowner2 = (owner as IC1Callback2); 
            _owner.PreRender += new EventHandler(OnOwnerPreRender);
        }


        public C1CallbackHelper(Control owner, bool useAsync, string onErrorCallback) : this(owner)
        {
            _onErrorCallback = onErrorCallback;
            _useAsync = useAsync;
        }


        public C1CallbackHelper(Control owner, bool useAsync, string onErrorCallback, string onWait) : this(owner, useAsync, onErrorCallback) 
        {
            _onWait = onWait;
        }

        public C1CallbackHelper(Control owner, bool useAsync, string onErrorCallback, string onWait, bool createContextScript)
            : this(owner, useAsync, onErrorCallback, onWait)
        {
            _createContextStript = createContextScript;
        }

        public static string EncodeString(string[] values)
        {
            string res = string.Empty;

            foreach (string value in values)
                res += value.Length.ToString() + "," + value;

            return res;
        }

        public static string[] DecodeString(string value)
        {
            ArrayList res = new ArrayList();

            while (!string.IsNullOrEmpty(value))
            {
                int idx = value.IndexOf(',');
                int len = int.Parse(value.Substring(0, idx));
                res.Add(value.Substring(idx + 1, len));
                value = value.Substring(idx + 1 + len); 
            }   

            return (string[])res.ToArray(typeof(string));
        }

        private string CallbackContextVariable
        {
            get
            {
                return string.Format("{0}_cntxt", _owner.ClientID);
            }
        }

        public string CallbackControlStateField
        {
            get
            {
                return string.Format("{0}_cstate", _owner.ClientID); 
            }
        }


        public string PersistControlState()
        {
            ObjectStateFormatter formatter = new ObjectStateFormatter();
            string res = formatter.Serialize(_iowner1.ControlState);
            return res;
        }

        public void RestoreControlState()
        {
            if (_owner.Page != null)
            {
                string controlState = _owner.Page.Request.Params[CallbackControlStateField];
                if (!string.IsNullOrEmpty(controlState))
                {
                    ObjectStateFormatter formatter = new ObjectStateFormatter();
                    object state = formatter.Deserialize(controlState);
                    _iowner1.ControlState = state;
                }
            }
        }



        /*protected void RegisterScript()
        {
            if (_owner.Page != null)
            {
                string controlState = PersistControlState();
                if (!string.IsNullOrEmpty(controlState))
                    _owner.Page.ClientScript.RegisterHiddenField(CallbackControlStateField, controlState);

#if CLIENTDIR
				_owner.Page.ClientScript.RegisterClientScriptInclude(_owner.GetType(), string.Format("{0}_callbackScript", _owner.ClientID), "file://c:\\inetpub\\wwwroot\\callback\\c1cb_script.js");
#else
                C1.Web.Util.C1CommonClientFile.RegisterScript(_owner.GetType(), _owner.Page.ClientScript, "c1cb_script.js");
#endif

                if (_createContextStript)
                {
                    _owner.Page.ClientScript.RegisterClientScriptBlock(_owner.GetType(), string.Format("{0}_callbackStartup", _owner.ClientID), GetCreateContextScript(), true);
                }

            }
        }
         */

        protected void RegisterScript()
        {
            if (_owner.Page != null)
            {
#if !FFBUG
				string controlState = PersistControlState();
				if (!string.IsNullOrEmpty(controlState))
					C1.Web.Util.ScriptManagerHelper.RegisterHiddenField(_owner, CallbackControlStateField, controlState);
#endif

#if CLIENTDIR
				_owner.Page.ClientScript.RegisterClientScriptInclude(_owner.GetType(), string.Format("{0}_callbackScript", _owner.ClientID), "file://c:\\inetpub\\wwwroot\\callback\\c1cb_script.js");
#else
                C1.Web.Util.C1CommonClientFile.RegisterScript(_owner.GetType(), _owner.Page.ClientScript, "c1cb_script.js");
#endif

#if FFBUG
				string controlState = PersistControlState();
				if (!string.IsNullOrEmpty(controlState))
				{
					C1.Web.Util.ScriptManagerHelper.RegisterHiddenField(_owner, CallbackControlStateField, string.Empty);
					C1.Web.Util.ScriptManagerHelper.RegisterClientScriptBlock(_owner, _owner.GetType(), CallbackControlStateField, string.Format("document.getElementById(\"{0}\").value = \"{1}\";", CallbackControlStateField, controlState), true); 
				}
#endif
                if (_createContextStript)
                {
                    C1.Web.Util.ScriptManagerHelper.RegisterClientScriptBlock(_owner, _owner.GetType(), string.Format("{0}_callbackStartup", _owner.ClientID), GetCreateContextScript(), true);
                }
            }
        }
         

        public string GetCreateContextScript()
        {
            string callbackRef = _owner.Page.ClientScript.GetCallbackEventReference(_owner, CallbackContextVariable + ".argument", "c1cb_onCallback", CallbackContextVariable, "c1cb_onErrorCallback", _useAsync);

            string onError = string.IsNullOrEmpty(_onErrorCallback) ? "null" : string.Format("\"{0}\"", _onErrorCallback);
            string waitMessage = (_iowner2 != null) ? _iowner2.WaitMessage : string.Empty;
            string waitControl = (_iowner2 != null) ? _iowner2.WaitControlID : string.Empty;
            string action = (_iowner2 != null) ? _iowner2.CallbackAction.ToString() : CallbackAction.None.ToString();

            string onWait = string.IsNullOrEmpty(_onWait) ? "null" : string.Format("\"{0}\"", _onWait);
            string script = string.Format("\r\nvar {0} = new c1cb_createContext(\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",{6},{7});\r\n{0}.callback = function()\r\n{{\r\n  {8};\r\n}}\r\n", CallbackContextVariable, _owner.ClientID, CallbackControlStateField, waitMessage, waitControl, action, onError, onWait, callbackRef);
            return script;
        }

        public string GetCallbackScriptHref (string arg)
        {
            return GetCallbackScriptHref(arg, string.Empty, string.Empty); 
        }
        
        public string GetCallbackScriptHref(string arg, string beforeCallbackFunc, string afterCallbackFunc)
        {
			return GetCallbackScript(true, "event", arg, beforeCallbackFunc, afterCallbackFunc);    
        }

		public string GetCallbackScriptFunction(string arg)
		{
			return GetCallbackScriptFunction(arg, string.Empty, string.Empty);
		}

		public string GetCallbackScriptFunction(string arg, string beforeCallbackFunc, string afterCallbackFunc)
		{
			return GetCallbackScript(false, "null", arg, beforeCallbackFunc, afterCallbackFunc);    
		}

		public string GetCallbackScript(bool requiresPrefix, string param, string arg, string beforeCallbackFunc, string afterCallbackFunc)
		{
			string bc = string.IsNullOrEmpty(beforeCallbackFunc) ? "null" : beforeCallbackFunc;
			string ac = string.IsNullOrEmpty(afterCallbackFunc) ? "null" : afterCallbackFunc;
			string prefix = requiresPrefix ? "javascript:" : string.Empty;

			return string.Format("{0}c1cb_doCallback({1},{2},{3},{4},{5});", new object[] { prefix, param, CallbackContextVariable, arg, bc, ac });
		}


        public string GetCallbackResult()
        {
            string s1 = _iowner1.GetCallbackResult();
            string s2 = PersistControlState();

            return EncodeString(new string[] { s1, s2 });
        }


        private void OnOwnerPreRender (object sender, EventArgs e)
        {
            if (_owner != null && _owner.Page != null && _iowner1.EnableCallback)
                RegisterScript();

            _owner.PreRender -= new EventHandler(OnOwnerPreRender);
        }

        private bool _createContextStript = true;
    }
}
