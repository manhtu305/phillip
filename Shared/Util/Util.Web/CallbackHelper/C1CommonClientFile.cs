/*
 This class allows to share common client JS files between web controls
 How to use:
 1. Add this file to the project. 
 2. Add a client-script file to share to the project as embedded resource. 
 3. Add WebResource attribute for the file, example:
    [assembly: WebResource("C1.Web.Command.Client.v2_0.common_scripts.js", "text/javascript")]
 
 3. The first line of client-script file should be EXACTLY:
 // X.YY
 X - Major version.
 YY - Minor version.
 Example: 
 // 1.12

 4. Override OnPreRender method of the control, add line:
 C1.Web.Util.C1CommonClientFile.RegisterScript(this.GetType(), this.Page.ClientScript, <ClientFileName>);
 ClientFileName here is the file name and extention.
 Example:

 protected override void OnPreRender(EventArgs e)
 {
   base.OnPreRender(e);
   C1.Web.Util.C1CommonClientFile.RegisterScript(this.GetType(), this.Page.ClientScript, "common_scripts.js");
 }
*/


using System;
using System.Web;
using System.Web.UI;
using System.Reflection;
using System.IO;
using System.Security.Permissions;

namespace C1.Web.Util
{
    internal class C1CommonClientFile
    {
        public static void RegisterScript(Type type, ClientScriptManager manager, string filename)
        {
            Type asm_type;
            string resourceName = GetResourceName(filename, out asm_type);
            manager.RegisterClientScriptResource(asm_type, resourceName);
        }

        public static void RegisterScript(Type type, Control control, string filename)
        {
            Type asm_type;
            string resourceName = GetResourceName(filename, out asm_type);
            C1.Web.Util.ScriptManagerHelper.RegisterClientScriptResource(control, asm_type, resourceName);
        }

        private struct ResFileInfo
        {
            public string ResourceName;
            public int VersionMajor;
            public int VersionMinor;
            public Type type;
        }

        private static ResFileInfo GetResouceInfo(Assembly asm, string filename)
        {
            ReflectionPermission rp = new ReflectionPermission(ReflectionPermissionFlag.MemberAccess);
            rp.Demand();

            ResFileInfo res = new ResFileInfo();
            Object[] attrs = asm.GetCustomAttributes(false);
            foreach (Object attr in attrs)
            {
                if (attr is WebResourceAttribute)
                {
                    WebResourceAttribute wr = (WebResourceAttribute)attr;
                    if (wr.WebResource.EndsWith(filename, StringComparison.InvariantCultureIgnoreCase))
                    {
                        string resName = wr.WebResource;
                        Stream resStream = asm.GetManifestResourceStream(resName);
                        byte[] bts = new byte[4];
                        resStream.ReadByte();
                        resStream.ReadByte();
                        resStream.ReadByte();
                        int byte1 = resStream.ReadByte();
                        int byte2 = resStream.ReadByte();
                        int byte3 = resStream.ReadByte();
                        int byte4 = resStream.ReadByte();
                        bool isFormatOk = Convert.ToChar(byte2) == '.';
                        if (isFormatOk)
                        {
                            res.VersionMajor = Int16.Parse(Convert.ToChar(byte1).ToString());
                            res.VersionMinor = int.Parse(Convert.ToChar(byte3).ToString() + Convert.ToChar(byte4).ToString());
                            res.ResourceName = resName;
                            res.type = asm.GetTypes()[0];
                        }
                    }
                }
            }
            return res;
        }

        private static string GetResourceName(string filename, out Type type)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            Assembly[] assems = currentDomain.GetAssemblies();
            ResFileInfo fi = new ResFileInfo();
            foreach (Assembly asm in assems)
            {
                ResFileInfo fi1 = GetResouceInfo(asm, filename);
                if ((fi1.VersionMajor > fi.VersionMajor) || (fi1.VersionMajor == fi.VersionMajor && fi1.VersionMinor > fi.VersionMinor))
                    fi = fi1;
            }   
            if (fi.VersionMajor == 0)
                throw new Exception(string.Format("Can not find resource {0}", filename));
            type = fi.type;
            return fi.ResourceName;
        }
    }
}