using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.C1CallbackHelper
{
    interface IC1Callback1
    {
        object ControlState
        {
            get;
            set;
        }

        bool EnableCallback
        {
            get;
        }

        string GetCallbackResult();
    }


    interface IC1Callback2
    {
        CallbackAction CallbackAction
        {
            get;
            set;
        }

        string WaitMessage
        {
            get;
            set;
        }

        string WaitControlID
        {
            get;
            set;
        }
    }


    /*interface IC1Callback
    {
        bool EnableCallback
        {
            get;
            set;
        }

        CallbackAction CallbackAction
        {
            get;
            set;
        }

        object ControlState
        {
            get;
            set;
        }

        string WaitMessage
        {
            get;
            set;
        }

        string WaitControlID
        {
            get;
            set;
        }

        string GetCallbackResult();
    }*/
}
