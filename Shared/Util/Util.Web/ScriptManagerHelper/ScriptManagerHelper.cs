//-----------------------------------
// ScriptManagerHelper
//
// Modifications:
// 11/28/2006 - The code don't need FileIOPermissionAccess.PathDiscovery to the all loaded assemblies now.
// 
// Using
// Use ScriptManagerHelper instead of Page.ClientScripts to Register scripts and hidden fields
//-----------------------------------
using System;
using System.Runtime;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.IO;

namespace C1.Web.Util
{
    internal static class ScriptManagerHelper
    {
        private static readonly object s_reflectionLock = new object();

        private static bool s_methodsInitialized;

        private static MethodInfo s_registerClientScriptResourceMethod;
        private static MethodInfo s_registerClientScriptBlockMethod;
        private static MethodInfo s_registerStartupScriptMethod;
        private static MethodInfo s_registerHiddenFieldMethod;
        private static MethodInfo s_registerClientScriptIncludeMethod;
        private static MethodInfo s_getCurrentMethod;
        private static PropertyInfo s_isInAsyncPostbackProperty;

        private static void InitializeReflection()
        {
            if (!s_methodsInitialized)
            {
                lock (s_reflectionLock)
                { 
                    if (!s_methodsInitialized) 
                    {
                        AppDomain currentDomain = AppDomain.CurrentDomain;
                        Assembly[] assems = currentDomain.GetAssemblies();

                        Assembly ms_webex_asm = null;
                        string ms_webex_ns = "System";
                        foreach (Assembly asm in assems)
                        {
                            if (asm.FullName.StartsWith("System.Web.Extensions,"))
                            {
                                ms_webex_asm = asm;
                                break;
                            }
                            else if (asm.FullName.StartsWith("Microsoft.Web.Extensions,"))
                            {
                                ms_webex_ns = "Microsoft";
                                ms_webex_asm = asm;
                                break;
                            }
                        }
                        Type scriptManagerType = null;
                        if (ms_webex_asm != null)
                        {
                            scriptManagerType = Type.GetType(ms_webex_ns + ".Web.UI.ScriptManager, " + ms_webex_asm.FullName, false);
                        }
                        if (scriptManagerType != null) 
                        {
							s_registerClientScriptResourceMethod = scriptManagerType.GetMethod("RegisterClientScriptResource", new Type[] { typeof(Control), typeof(Type), typeof(string) });
							s_registerStartupScriptMethod = scriptManagerType.GetMethod("RegisterStartupScript", new Type[] { typeof(Control), typeof(Type), typeof(string), typeof(string), typeof(bool) });
							s_registerClientScriptBlockMethod = scriptManagerType.GetMethod("RegisterClientScriptBlock", new Type[] { typeof(Control), typeof(Type), typeof(string), typeof(string), typeof(bool) });
							s_registerHiddenFieldMethod = scriptManagerType.GetMethod("RegisterHiddenField", new Type[] { typeof(Control), typeof(string), typeof(string) });
							s_registerClientScriptIncludeMethod = scriptManagerType.GetMethod("RegisterClientScriptInclude", new Type[] { typeof(Control), typeof(Type), typeof(string), typeof(string) });
                            s_getCurrentMethod = scriptManagerType.GetMethod("GetCurrent", new Type[] { typeof(Page)});
                            s_isInAsyncPostbackProperty = scriptManagerType.GetProperty("IsInAsyncPostBack");
                        } 
                        s_methodsInitialized = true; 
                    } 
                }
            }
        }
        public static bool IsInAsyncPostback(Control control)
        {
            InitializeReflection();
            if (s_getCurrentMethod != null && s_isInAsyncPostbackProperty != null)
            {
                object o = s_getCurrentMethod.Invoke(null, new object[] { control.Page });
                if (o != null)
                {
                    return (bool)s_isInAsyncPostbackProperty.GetValue(o, null);
                }
            }
            return false;
        }
        public static void RegisterClientScriptResource(Control control, Type type, string resourceName)
        {
            InitializeReflection(); 
            if (s_registerClientScriptResourceMethod != null)
            {                
                // ASP.NET AJAX exists, so we use the ScriptManager                
                s_registerClientScriptResourceMethod.Invoke(null, new object[] { control, type, resourceName });            
            }            
            else 
            {                
                // No ASP.NET AJAX, so we just call to the ASP.NET 2.0 method                
                control.Page.ClientScript.RegisterClientScriptResource(type, resourceName);            
            }        
        }
        public static void RegisterClientScriptBlock(Control control, Type type, string key, string script)
        {
            InitializeReflection();
            RegisterClientScriptBlock(control, type, key, script, false);
        }

        public static void RegisterClientScriptBlock(Control control, Type type, string key, string script, bool addScriptTags)
        {
            InitializeReflection();
            if (s_registerClientScriptResourceMethod != null)
            {
                // ASP.NET AJAX exists, so we use the ScriptManager                
                s_registerClientScriptBlockMethod.Invoke(null, new object[] { control, type, key, script, addScriptTags });
            }
            else
            {
                // No ASP.NET AJAX, so we just call to the ASP.NET 2.0 method                
                control.Page.ClientScript.RegisterClientScriptBlock(type, key, script, addScriptTags);
            }
        }


        public static void RegisterStartupScript(Control control, Type type, string key, string script)
        {
            InitializeReflection();
            RegisterStartupScript(control, type, key, script, false);
        }

        public static void RegisterStartupScript(Control control, Type type, string key, string script, bool addScriptTags)
        {
            InitializeReflection();
            if (s_registerClientScriptResourceMethod != null)
            {
                // ASP.NET AJAX exists, so we use the ScriptManager                
                s_registerStartupScriptMethod.Invoke(null, new object[] { control, type, key, script, addScriptTags });
            }
            else
            {
                // No ASP.NET AJAX, so we just call to the ASP.NET 2.0 method                
                control.Page.ClientScript.RegisterStartupScript(type, key, script, addScriptTags);
            }
        }

        public static void RegisterHiddenField(Control control, string hiddenFieldName, string hiddenFieldInitialValue)
        {
            InitializeReflection();
            if (s_registerHiddenFieldMethod != null)
            {
                // ASP.NET AJAX exists, so we use the ScriptManager                
                s_registerHiddenFieldMethod.Invoke(null, new object[] { control, hiddenFieldName, hiddenFieldInitialValue });
            }
            else
            {
                // No ASP.NET AJAX, so we just call to the ASP.NET 2.0 method                
                control.Page.ClientScript.RegisterHiddenField(hiddenFieldName, hiddenFieldInitialValue);
            }
        }

       public static void RegisterClientScriptInclude(Control control, Type type, string key, string url)
       {
            InitializeReflection();
            if (s_registerClientScriptIncludeMethod != null)
            {
                // ASP.NET AJAX exists, so we use the ScriptManager                
                s_registerClientScriptIncludeMethod.Invoke(null, new object[] { control, type, key, url });
            }
            else
            {
                // No ASP.NET AJAX, so we just call to the ASP.NET 2.0 method                
                control.Page.ClientScript.RegisterClientScriptInclude(type, key, url);
            }
        }           
    }
}
