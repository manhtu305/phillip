/*----------------------------------------------------------------------------
 * PdfArabic
 * 
 * For support Arabic shaping.
 * 
 *----------------------------------------------------------------------------
 * Copyright (C) 2004 ComponentOne LLC
 *----------------------------------------------------------------------------
 * Status		Date			By			Comments
 *----------------------------------------------------------------------------
 * Created		October, 2004	Cornetov	based on IText project
 *----------------------------------------------------------------------------
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

namespace C1.Util
{
	/// <summary>
	/// Class for support Arabic character shaping.
	/// </summary>
	internal class FontSubSetArabic
	{
		//=============================================================================
		#region ** declarations

		private const ushort ALEF = 0x0627;
		private const ushort ALEFHAMZA = 0x0623;
		private const ushort ALEFHAMZABELOW = 0x0625;
		private const ushort ALEFMADDA = 0x0622;
		private const ushort LAM = 0x0644;
		private const ushort HAMZA = 0x0621;
		private const ushort TATWEEL = 0x0640;
		private const ushort ZWJ = 0x200D;

		private const ushort HAMZAABOVE = 0x0654;
		private const ushort HAMZABELOW = 0x0655;

		private const ushort WAWHAMZA = 0x0624;
		private const ushort YEHHAMZA = 0x0626;
		private const ushort WAW = 0x0648;
		private const ushort ALEFMAKSURA = 0x0649;
		private const ushort YEH = 0x064A;
		private const ushort FARSIYEH = 0x06CC;

		private const ushort SHADDA = 0x0651;
		private const ushort KASRA = 0x0650;
		private const ushort FATHA = 0x064E;
		private const ushort DAMMA = 0x064F;
		private const ushort MADDA = 0x0653;

		private const ushort LAM_ALEF = 0xFEFB;
		private const ushort LAM_ALEFHAMZA = 0xFEF7;
		private const ushort LAM_ALEFHAMZABELOW = 0xFEF9;
		private const ushort LAM_ALEFMADDA = 0xFEF5;

		private static ushort[][] CodingTable = new ushort[][] {
			new ushort[] {0x0621, 0xFE80}, /* HAMZA */
			new ushort[] {0x0622, 0xFE81, 0xFE82}, /* ALEF WITH MADDA ABOVE */
			new ushort[] {0x0623, 0xFE83, 0xFE84}, /* ALEF WITH HAMZA ABOVE */
			new ushort[] {0x0624, 0xFE85, 0xFE86}, /* WAW WITH HAMZA ABOVE */
			new ushort[] {0x0625, 0xFE87, 0xFE88}, /* ALEF WITH HAMZA BELOW */
			new ushort[] {0x0626, 0xFE89, 0xFE8A, 0xFE8B, 0xFE8C}, /* YEH WITH HAMZA ABOVE */
			new ushort[] {0x0627, 0xFE8D, 0xFE8E}, /* ALEF */
			new ushort[] {0x0628, 0xFE8F, 0xFE90, 0xFE91, 0xFE92}, /* BEH */
			new ushort[] {0x0629, 0xFE93, 0xFE94}, /* TEH MARBUTA */
			new ushort[] {0x062A, 0xFE95, 0xFE96, 0xFE97, 0xFE98}, /* TEH */
			new ushort[] {0x062B, 0xFE99, 0xFE9A, 0xFE9B, 0xFE9C}, /* THEH */
			new ushort[] {0x062C, 0xFE9D, 0xFE9E, 0xFE9F, 0xFEA0}, /* JEEM */
			new ushort[] {0x062D, 0xFEA1, 0xFEA2, 0xFEA3, 0xFEA4}, /* HAH */
			new ushort[] {0x062E, 0xFEA5, 0xFEA6, 0xFEA7, 0xFEA8}, /* KHAH */
			new ushort[] {0x062F, 0xFEA9, 0xFEAA}, /* DAL */
			new ushort[] {0x0630, 0xFEAB, 0xFEAC}, /* THAL */
			new ushort[] {0x0631, 0xFEAD, 0xFEAE}, /* REH */
			new ushort[] {0x0632, 0xFEAF, 0xFEB0}, /* ZAIN */
			new ushort[] {0x0633, 0xFEB1, 0xFEB2, 0xFEB3, 0xFEB4}, /* SEEN */
			new ushort[] {0x0634, 0xFEB5, 0xFEB6, 0xFEB7, 0xFEB8}, /* SHEEN */
			new ushort[] {0x0635, 0xFEB9, 0xFEBA, 0xFEBB, 0xFEBC}, /* SAD */
			new ushort[] {0x0636, 0xFEBD, 0xFEBE, 0xFEBF, 0xFEC0}, /* DAD */
			new ushort[] {0x0637, 0xFEC1, 0xFEC2, 0xFEC3, 0xFEC4}, /* TAH */
			new ushort[] {0x0638, 0xFEC5, 0xFEC6, 0xFEC7, 0xFEC8}, /* ZAH */
			new ushort[] {0x0639, 0xFEC9, 0xFECA, 0xFECB, 0xFECC}, /* AIN */
			new ushort[] {0x063A, 0xFECD, 0xFECE, 0xFECF, 0xFED0}, /* GHAIN */
			new ushort[] {0x0640, 0x0640, 0x0640, 0x0640, 0x0640}, /* TATWEEL */
			new ushort[] {0x0641, 0xFED1, 0xFED2, 0xFED3, 0xFED4}, /* FEH */
			new ushort[] {0x0642, 0xFED5, 0xFED6, 0xFED7, 0xFED8}, /* QAF */
			new ushort[] {0x0643, 0xFED9, 0xFEDA, 0xFEDB, 0xFEDC}, /* KAF */
			new ushort[] {0x0644, 0xFEDD, 0xFEDE, 0xFEDF, 0xFEE0}, /* LAM */
			new ushort[] {0x0645, 0xFEE1, 0xFEE2, 0xFEE3, 0xFEE4}, /* MEEM */
			new ushort[] {0x0646, 0xFEE5, 0xFEE6, 0xFEE7, 0xFEE8}, /* NOON */
			new ushort[] {0x0647, 0xFEE9, 0xFEEA, 0xFEEB, 0xFEEC}, /* HEH */
			new ushort[] {0x0648, 0xFEED, 0xFEEE}, /* WAW */
			new ushort[] {0x0649, 0xFEEF, 0xFEF0, 0xFBE8, 0xFBE9}, /* ALEF MAKSURA */
			new ushort[] {0x064A, 0xFEF1, 0xFEF2, 0xFEF3, 0xFEF4}, /* YEH */
			new ushort[] {0x0671, 0xFB50, 0xFB51}, /* ALEF WASLA */
			new ushort[] {0x0679, 0xFB66, 0xFB67, 0xFB68, 0xFB69}, /* TTEH */
			new ushort[] {0x067A, 0xFB5E, 0xFB5F, 0xFB60, 0xFB61}, /* TTEHEH */
			new ushort[] {0x067B, 0xFB52, 0xFB53, 0xFB54, 0xFB55}, /* BEEH */
			new ushort[] {0x067E, 0xFB56, 0xFB57, 0xFB58, 0xFB59}, /* PEH */
			new ushort[] {0x067F, 0xFB62, 0xFB63, 0xFB64, 0xFB65}, /* TEHEH */
			new ushort[] {0x0680, 0xFB5A, 0xFB5B, 0xFB5C, 0xFB5D}, /* BEHEH */
			new ushort[] {0x0683, 0xFB76, 0xFB77, 0xFB78, 0xFB79}, /* NYEH */
			new ushort[] {0x0684, 0xFB72, 0xFB73, 0xFB74, 0xFB75}, /* DYEH */
			new ushort[] {0x0686, 0xFB7A, 0xFB7B, 0xFB7C, 0xFB7D}, /* TCHEH */
			new ushort[] {0x0687, 0xFB7E, 0xFB7F, 0xFB80, 0xFB81}, /* TCHEHEH */
			new ushort[] {0x0688, 0xFB88, 0xFB89}, /* DDAL */
			new ushort[] {0x068C, 0xFB84, 0xFB85}, /* DAHAL */
			new ushort[] {0x068D, 0xFB82, 0xFB83}, /* DDAHAL */
			new ushort[] {0x068E, 0xFB86, 0xFB87}, /* DUL */
			new ushort[] {0x0691, 0xFB8C, 0xFB8D}, /* RREH */
			new ushort[] {0x0698, 0xFB8A, 0xFB8B}, /* JEH */
			new ushort[] {0x06A4, 0xFB6A, 0xFB6B, 0xFB6C, 0xFB6D}, /* VEH */
			new ushort[] {0x06A6, 0xFB6E, 0xFB6F, 0xFB70, 0xFB71}, /* PEHEH */
			new ushort[] {0x06A9, 0xFB8E, 0xFB8F, 0xFB90, 0xFB91}, /* KEHEH */
			new ushort[] {0x06AD, 0xFBD3, 0xFBD4, 0xFBD5, 0xFBD6}, /* NG */
			new ushort[] {0x06AF, 0xFB92, 0xFB93, 0xFB94, 0xFB95}, /* GAF */
			new ushort[] {0x06B1, 0xFB9A, 0xFB9B, 0xFB9C, 0xFB9D}, /* NGOEH */
			new ushort[] {0x06B3, 0xFB96, 0xFB97, 0xFB98, 0xFB99}, /* GUEH */
			new ushort[] {0x06BB, 0xFBA0, 0xFBA1, 0xFBA2, 0xFBA3}, /* RNOON */
			new ushort[] {0x06BE, 0xFBAA, 0xFBAB, 0xFBAC, 0xFBAD}, /* HEH DOACHASHMEE */
			new ushort[] {0x06C0, 0xFBA4, 0xFBA5}, /* HEH WITH YEH ABOVE */
			new ushort[] {0x06C1, 0xFBA6, 0xFBA7, 0xFBA8, 0xFBA9}, /* HEH GOAL */
			new ushort[] {0x06C5, 0xFBE0, 0xFBE1}, /* KIRGHIZ OE */
			new ushort[] {0x06C6, 0xFBD9, 0xFBDA}, /* OE */
			new ushort[] {0x06C7, 0xFBD7, 0xFBD8}, /* U */
			new ushort[] {0x06C8, 0xFBDB, 0xFBDC}, /* YU */
			new ushort[] {0x06C9, 0xFBE2, 0xFBE3}, /* KIRGHIZ YU */
			new ushort[] {0x06CB, 0xFBDE, 0xFBDF}, /* VE */
			new ushort[] {0x06CC, 0xFBFC, 0xFBFD, 0xFBFE, 0xFBFF}, /* FARSI YEH */
			new ushort[] {0x06D0, 0xFBE4, 0xFBE5, 0xFBE6, 0xFBE7}, /* E */
			new ushort[] {0x06D2, 0xFBAE, 0xFBAF}, /* YEH BARREE */
			new ushort[] {0x06D3, 0xFBB0, 0xFBB1} /* YEH BARREE WITH HAMZA ABOVE */
															 };

		// which 0=isolated, 1=final, 2=initial, 3=medial
		private enum WhichNumberEnum
		{
			Isolated = 0,
			Final,
			Initial,
			Medial,
		}

		// 0 == no ligature possible; 1 == vowel; 2 == two chars; 3 == Lam+Alef
		private enum LigatureEnum
		{
			NoPositive = 0,
			Vowel,
			TwoChars,
			LamAlef,
		}

		[Flags]
		private enum LevelFlags : byte
		{
			Nothing           = 0x00,
			NoVowel           = 0x01,
			Standard          = 0x02,
			ComposedTashKeel  = 0x04,
			Lig               = 0x08,
			MuleFont          = 0x10,
			LBoxFont          = 0x20,
			UniFont           = 0x40,
			NaqshFont         = 0x80,
		}

		private class CharStruct
		{
			public char BaseChar;
			public char MarkOne;             // has to be initialized to zero
			public char Vowel;
			public int Lignum;               // is a ligature with lignum additional characters 
			public int NumShapes = 1;

			internal bool IsConnectsToLeft
			{
				get { return NumShapes > 2; }
			}
		}; 

		// constants
		//private Hashtable _tags = new Hashtable();

		#endregion

		//=============================================================================
		#region ** static methods

		// return true if char is Arabic
		public static bool IsArabicChar(char ch)
		{
			return ch >= 0x0600 && ch <= 0x06FF;
		}

		// return true if char is special
		private static bool IsSpecialChar(char ch)
		{
			// backspace, punctuation or control char
            //if (ch == ' ' || ch == '-')
            //    return true;
            if (ch == '-')
                return true;

			return false;
			//return true;
		}

		// return true if char for right to left text
		public static bool IsRightToLeft(char ch)
		{
			// Basic Hebrew
			if (ch >= 0x0590 && ch <= 0x05ff)
				return true;

			// Basic Arabic, Arabic Presentation Forms-A, Forms-B
			if ((ch >= 0x0600 && ch <= 0x06ff) ||
				(ch >= 0xfb50 && ch <= 0xfdff) ||
				(ch >= 0xfe70 && ch <= 0xfefe))
				return true;

			// done
			return false;
		}

		// return true if char for shaping
		public static bool IsShapingChar(char ch)
		{
			return ch >= 0x0621 && ch <= 0x06D3;
		}

		// return true if char is vowel
		public static bool IsVowelChar(char ch)
		{
			return (ch >= 0x064B && ch <= 0x0655) || (ch == 0x0670);
		}

		public static char[] Shaping(string text, ref int[] kerns)
		{
			bool rightToLeft = false;
			char[] chs = text.ToCharArray();
//			char[] chs = new char[text.Length];
//			for (int i = 0; i < text.Length; i++)
//				chs[i] = text[text.Length - i - 1];

			int src = 0;
			int dest = 0;
			StringBuilder sb;
			int totalTextLength = chs.Length;
			while (true)
			{
				while (src < totalTextLength) 
				{
					char ch = chs[src];
					if (IsRightToLeft(ch))
					{
						rightToLeft = true;
						break;
					}
					if (src != dest) 
						chs[dest] = chs[src];
					src++;
					dest++;
				}
				if (src >= totalTextLength) 
				{
					totalTextLength = dest;

					//return chs;

					// TODO: add set kerns for Arabic texts
//					if (kerns != null)
//					{
//						int[] buf = new int[kerns.Length];
//						Array.Copy(kerns, buf, kerns.Length);
//						for (int i = 0; i < kerns.Length; i++)
//						{
//							kerns[i] = buf[kerns.Length - i - 1];
//						}
//					}

					if (!rightToLeft) return chs;

					sb = new StringBuilder();
                    List<char> data = new List<char>();
					//char[] data = new char[totalTextLength];
                    //List<int> list = new List<int>();
					for (int i = 0; i < totalTextLength; i++)
					{
						char ch = chs[totalTextLength - i - 1];

						// for right to left or special char - inversion
						// others (inside non Arabic chars) - directly
						if (IsSpecialChar(ch) || IsRightToLeft(ch))
						{
							if (sb.Length > 0)
							{
                                for (int j = 0; j < sb.Length; j++)
                                {
                                    //data[i - j - 1] = sb[j];
                                    data.Add(sb[j]);
                                }
								sb.Remove(0, sb.Length);
							}
							//data[i] = ch;
                            data.Add(ch);
							continue;
						}
                        sb.Append(ch);
					}
                    for (int j = 0; j < sb.Length; j++)
                    {
                        // data[totalTextLength - j - 1] = sb[j];
                        data.Insert(0, sb[j]);
                    }
                    // TODO: add set kerns for right to left texts
                    //kerns = list.ToArray();
                    kerns = null;
                    return data.ToArray();
				}

				int startArabicIdx = src;
				src++;
				while (src < totalTextLength)
				{
					char ch = chs[src];
					if (!IsArabicChar(ch)) break;
					if (kerns != null) kerns[src - 1] = 0;
					src++;
				}

				int level = 0;
				int arabicWordSize = src - startArabicIdx; 

				char[] str = new char[arabicWordSize];
				for (int i = arabicWordSize + startArabicIdx - 1; i >= startArabicIdx; i--)
					str[i - startArabicIdx] = chs[i];

				sb = new StringBuilder(arabicWordSize);
				Shape(str, sb, level);
				if ((level & (int)(LevelFlags.ComposedTashKeel | LevelFlags.Lig)) != 0)
					Doubling(sb, level);

				Array.Copy(sb.ToString().ToCharArray(), 0, chs, dest, sb.Length);
				dest += sb.Length;
			}
		}
		#endregion

		//=============================================================================
		#region ** implementation

		// which 0=isolated, 1=final, 2=initial, 3=medial
		private static char GetCharShape(char ch, WhichNumberEnum which)
		{
			int l, r, m;
			if (IsShapingChar(ch))
			{
				// shape characters
				l = 0;
				r = CodingTable.Length - 1;
				while (l <= r)
				{
					m = (l + r) / 2;
					if (ch == (char)CodingTable[m][0])
						return (char)CodingTable[m][(int)which + 1];
					else if ((int)ch < CodingTable[m][0])
						r = m - 1;
					else
						l = m + 1;
				}
			}
			return ch;
		}
		
		private static int GetShapeCount(char ch)
		{
			int l, r, m;
			if (IsShapingChar(ch) && !IsVowelChar(ch))
			{
				l = 0;
				r = CodingTable.Length - 1;
				while (l <= r)
				{
					m = (l + r) / 2;
					if (ch == (char)CodingTable[m][0])
						return CodingTable[m].Length - 1;
					else if ((int)ch < CodingTable[m][0])
						r = m - 1;
					else
						l = m + 1;
				}
			}
			else if (ch == ZWJ)
				return 4;

			return 1;
		}
    
		// 0 == no ligature possible; 1 == vowel; 2 == two chars; 3 == Lam+Alef
		private static int GetLigature(char newchar, CharStruct oldChar)
		{
			int retval = 0;
			if (oldChar.BaseChar == 0)
				return 0;
			if (IsVowelChar(newchar))
			{
				retval = 1;
				if ((oldChar.Vowel != 0) && (newchar != SHADDA))
				{
					// we eliminate the old vowel ..
					retval = 2;
				}
				switch ((ushort)newchar)
				{
					case SHADDA:
						if (oldChar.MarkOne == 0)
							oldChar.MarkOne = (char)SHADDA;
						else
							return 0;         // no ligature possible
						break;
					case HAMZABELOW:
					switch ((ushort)oldChar.BaseChar)
					{
						case ALEF:
							oldChar.BaseChar = (char)ALEFHAMZABELOW;
							retval = 2;
							break;
						case LAM_ALEF:
							oldChar.BaseChar = (char)LAM_ALEFHAMZABELOW;
							retval = 2;
							break;
						default:
							oldChar.MarkOne = (char)HAMZABELOW;
							break;
					}
						break;
					case HAMZAABOVE:
					switch ((ushort)oldChar.BaseChar)
					{
						case ALEF:
							oldChar.BaseChar = (char)ALEFHAMZA;
							retval = 2;
							break;
						case LAM_ALEF:
							oldChar.BaseChar = (char)LAM_ALEFHAMZA;
							retval = 2;
							break;
						case WAW:
							oldChar.BaseChar = (char)WAWHAMZA;
							retval = 2;
							break;
						case YEH:
						case ALEFMAKSURA:
						case FARSIYEH:
							oldChar.BaseChar = (char)YEHHAMZA;
							retval = 2;
							break;
						default:           // whatever sense this may make ...
							oldChar.MarkOne = (char)HAMZAABOVE;
							break;
					}
						break;
					case MADDA:
					switch ((ushort)oldChar.BaseChar)
					{
						case ALEF:
							oldChar.BaseChar = (char)ALEFMADDA;
							retval = 2;
							break;
					}
						break;
					default:
						oldChar.Vowel = newchar;
						break;
				}
				if (retval == 1)
					oldChar.Lignum++;
				return retval;
			}

			// if we already joined a vowel, we can't join a Hamza
			if (oldChar.Vowel != 0)
				return 0;
			
			switch ((ushort)oldChar.BaseChar)
			{
				case LAM:
				switch ((ushort)newchar)
				{
					case ALEF:
						oldChar.BaseChar = (char)LAM_ALEF;
						oldChar.NumShapes = 2;
						retval = 3;
						break;
					case ALEFHAMZA:
						oldChar.BaseChar = (char)LAM_ALEFHAMZA;
						oldChar.NumShapes = 2;
						retval = 3;
						break;
					case ALEFHAMZABELOW:
						oldChar.BaseChar = (char)LAM_ALEFHAMZABELOW;
						oldChar.NumShapes = 2;
						retval = 3;
						break;
					case ALEFMADDA:
						oldChar.BaseChar = (char)LAM_ALEFMADDA;
						oldChar.NumShapes = 2;
						retval = 3;
						break;
				}
					break;
				case 0:
					oldChar.BaseChar = newchar;
					oldChar.NumShapes = GetShapeCount(newchar);
					retval = 1;
					break;
			}
			return retval;
		}
		
		// s is a shaped CharStruct; i is the index into the string
		private static void CopyCsToString(StringBuilder sb, CharStruct s, int level)
		{
			if (s.BaseChar == 0) return;

			sb.Append(s.BaseChar);
			(s.Lignum)--;
			
			if (s.MarkOne != 0)
			{
				if ((level & (int)LevelFlags.NoVowel) == 0)
				{
					sb.Append(s.MarkOne);
					(s.Lignum)--;
				}
				else
					(s.Lignum)--;
			}
			if (s.Vowel != 0)
			{
				if ((level & (int)LevelFlags.NoVowel) == 0)
				{
					sb.Append(s.Vowel);
					(s.Lignum)--;
				}
				else                 // vowel elimination
					(s.Lignum)--;
			}
		}


		// return length
		// Ok. We have presentation ligatures in our font.
		private static void Doubling(StringBuilder sb, int level)
		{
			int len;
			int olen = len = sb.Length;
			int j = 0, si = 1;
			ushort lapresult;
			
			while (si < olen)
			{
				lapresult = 0;
				if ((level & (int)LevelFlags.ComposedTashKeel) != 0)
				{
					switch ((ushort)sb[j])
					{
						case SHADDA:
						switch ((ushort)sb[si]) 
						{
							case KASRA: lapresult = 0xFC62; break;
							case FATHA: lapresult = 0xFC60; break;
							case DAMMA: lapresult = 0xFC61; break;
							case 0x064C: lapresult = 0xFC5E; break;
							case 0x064D: lapresult = 0xFC5F; break;
						}
							break;
						case KASRA:
							if (sb[si] == (char)SHADDA)
								lapresult = 0xFC62;
							break;
						case FATHA:
							if (sb[si] == (char)SHADDA)
								lapresult = 0xFC60;
							break;
						case DAMMA:
							if (sb[si] == (char)SHADDA)
								lapresult = 0xFC61;
							break;
					}
				}
				
				if ((level & (int)LevelFlags.Lig) != 0)
				{
					switch ((ushort)sb[j])
					{
						case 0xFEDF:       // LAM initial
						switch ((ushort)sb[si])
						{
							case 0xFE9E: lapresult = 0xFC3F; break;        // JEEM final
							case 0xFEA0: lapresult = 0xFCC9; break;        // JEEM medial
							case 0xFEA2: lapresult = 0xFC40; break;        // HAH final
							case 0xFEA4: lapresult = 0xFCCA; break;        // HAH medial
							case 0xFEA6: lapresult = 0xFC41; break;        // KHAH final
							case 0xFEA8: lapresult = 0xFCCB; break;        // KHAH medial
							case 0xFEE2: lapresult = 0xFC42; break;        // MEEM final
							case 0xFEE4: lapresult = 0xFCCC; break;        // MEEM medial
						}
							break;
						case 0xFE97:       // TEH initial
						switch ((ushort)sb[si])
						{
							case 0xFEA0: lapresult = 0xFCA1; break;        // JEEM medial
							case 0xFEA4: lapresult = 0xFCA2; break;        // HAH medial
							case 0xFEA8: lapresult = 0xFCA3; break;        // KHAH medial
						}
							break;
						case 0xFE91:       // BEH initial
						switch ((ushort)sb[si])
						{
							case 0xFEA0: lapresult = 0xFC9C; break;        // JEEM medial
							case 0xFEA4: lapresult = 0xFC9D; break;        // HAH medial
							case 0xFEA8: lapresult = 0xFC9E; break;        // KHAH medial
						}
							break;
						case 0xFEE7:       // NOON initial
						switch ((ushort)sb[si])
						{
							case 0xFEA0: lapresult = 0xFCD2; break;        // JEEM initial
							case 0xFEA4: lapresult = 0xFCD3; break;        // HAH medial
							case 0xFEA8: lapresult = 0xFCD4; break;        // KHAH medial
						}
							break;
						case 0xFEE8:       // NOON medial
						switch ((ushort)sb[si])
						{
							case 0xFEAE: lapresult = 0xFC8A; break;        // REH final
							case 0xFEB0: lapresult = 0xFC8B; break;        // ZAIN final
						}
							break;
						case 0xFEE3:       // MEEM initial
						switch ((ushort)sb[si])
						{
							case 0xFEA0: lapresult = 0xFCCE; break;        // JEEM medial
							case 0xFEA4: lapresult = 0xFCCF; break;        // HAH medial
							case 0xFEA8: lapresult = 0xFCD0; break;        // KHAH medial
							case 0xFEE4: lapresult = 0xFCD1; break;        // MEEM medial
						}
							break;
						case 0xFED3:       // FEH initial
						switch ((ushort)sb[si])
						{
							case 0xFEF2: lapresult = 0xFC32; break;        // YEH final
						}
							break;
						default:
							break;
					} // end switch sb[si]
				}
				if (lapresult != 0)
				{
					sb[j] = (char)lapresult;
					len--;
					si++;                 // jump over one character
					// we'll have to change this, too.
				}
				else
				{
					j++;
					sb[j] = sb[si];
					si++;
				}
			}
			sb.Length = len;
		}
		
		// string is assumed to be empty and big enough.
		// text is the original text.
		// This routine does the basic Arabic reshaping.
		// len the number of non-null characters.
		//
		// Note: We have to unshape each character first!
		private static void Shape(char[] chs, StringBuilder sb, int level)
		{
			//int j = 0;
			int join;
			WhichNumberEnum which;
			char nextletter;
	        
			int p = 0;                     /* initialize for output */
			CharStruct oldChar = new CharStruct();
			CharStruct curChar = new CharStruct();
			while (p < chs.Length)
			{
				nextletter = chs[p++];
				
				join = GetLigature(nextletter, curChar);
				
				// shape curChar
				if (join == 0)
				{
					int nc = GetShapeCount(nextletter);

					// final or isolated
					if (nc == 1)
						which = WhichNumberEnum.Isolated;        // final or isolated
					else
						which = WhichNumberEnum.Initial;         // medial or initial

					if (oldChar.IsConnectsToLeft)
						which++;
	                
					which = (WhichNumberEnum)((int)which % curChar.NumShapes);
					curChar.BaseChar = GetCharShape(curChar.BaseChar, which);
	                
					// get rid of oldChar
					CopyCsToString(sb, oldChar, level);
					oldChar = curChar;    // new values in oldChar
	                
					// init new curChar
					curChar = new CharStruct();
					curChar.BaseChar = nextletter;
					curChar.NumShapes = nc;
					curChar.Lignum++;
				}
				else if ((join == 3) && (level & (int)LevelFlags.LBoxFont) != 0)
					curChar.Lignum++;    // Lam+Alef extra in langbox-font
			}
        
			// Handle last char
			which = oldChar.IsConnectsToLeft ? WhichNumberEnum.Final : WhichNumberEnum.Isolated;
			which = (WhichNumberEnum)((int)which % curChar.NumShapes);
			curChar.BaseChar = GetCharShape(curChar.BaseChar, which);
	        
			// get rid of oldChar
			CopyCsToString(sb, oldChar, level);
			CopyCsToString(sb, curChar, level);
		}

		#endregion
	}
}
