//----------------------------------------------------------------------------
// Util\MetafileConverterBase.cs
//----------------------------------------------------------------------------
// Abstract metafile class for metafile conversions.
// Base class for PdfMetafile, RtfMetafile, ExcelMetafile.
// For a basis is taken the code of Bernardo.
// 
// Copyright (C) 2004 - 2005 ComponentOne LLC
//----------------------------------------------------------------------------
// Status	Date		By			Comments
// Created	May 2004	Cornetov
// Modified Jan 2005	Bernardo	Added protected _refDC member. This allows
//									pdf converters to reuse the same _refDC 
//									when creating multi-page documents.
//----------------------------------------------------------------------------
//#define TRACETOFILE // dump metafile records to 'c:\temp\trace.txt'
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace C1.Util
{
	/// <summary>
	/// Abstract class to enumerate metafiles and interpret commands into simpler calls.
	/// Used as a base class for converting metafiles into pdf, xls, rtf, etc.
	/// </summary>
	internal abstract class MetafileConverterBase : IDisposable
	{
		//-----------------------------------------------------------------------------
		#region ** constants

		// GDI constants
		protected const int 
			ETO_OPAQUE			= 0x0002,
			ETO_CLIPPED			= 0x0004,
			ETO_PDY				= 0x2000,
			ETO_GLYPH_INDEX     = 0x0010,
			ETO_RTLREADING      = 0x0080,
			ETO_NUMERICSLOCAL   = 0x0400,
			ETO_NUMERICSLATIN   = 0x0800,
			ETO_IGNORELANGUAGE  = 0x1000,
			TRANSPARENT			= 1,
			OPAQUE				= 2,
			TA_LEFT				= 0,
			TA_TOP				= 0,
			TA_UPDATECP			= 1,
			TA_RIGHT			= 2,
			TA_CENTER			= 6,
			TA_BOTTOM			= 8,
			TA_BASELINE			= 24,
			PS_SOLID            = 0,
			PS_DASH             = 1,
			PS_DOT              = 2,
			PS_DASHDOT          = 3,
			PS_DASHDOTDOT       = 4,
			PS_NULL             = 5,
            PS_USERSTYLE        = 7,
            PS_STYLE_MASK       = 0x0000f,
			BS_NULL				= 1,
			PS_ENDCAP_ROUND     = 0x00000,
			PS_ENDCAP_SQUARE    = 0x00100,
			PS_ENDCAP_FLAT      = 0x00200,
			PS_ENDCAP_MASK      = 0x00f00,
			PS_JOIN_ROUND       = 0x00000,
			PS_JOIN_BEVEL       = 0x01000,
			PS_JOIN_MITER       = 0x02000,
			PS_JOIN_MASK        = 0x0f000,
			PS_COSMETIC         = 0x00000,
			PS_GEOMETRIC        = 0x10000,
			PS_TYPE_MASK        = 0xf0000,
			FW_BOLD				= 700,
			GM_COMPATIBLE       = 1,
			GM_ADVANCED         = 2,
			AD_COUNTERCLOCKWISE	= 1,
			AD_CLOCKWISE		= 2,
			ALTERNATE			= 1,
			WINDING				= 2;

		// Ternary raster operations
		protected const int 
			SRCCOPY             = 0x00cc0020, /* dest = source                   */
			SRCPAINT            = 0x00ee0086, /* dest = source OR dest           */
			SRCAND              = 0x008800c6, /* dest = source AND dest          */
			SRCINVERT           = 0x00660046, /* dest = source XOR dest          */
			SRCERASE            = 0x00440328, /* dest = source AND (NOT dest )   */
			NOTSRCCOPY          = 0x00330008, /* dest = (NOT source)             */
			NOTSRCERASE         = 0x001100a6, /* dest = (NOT src) AND (NOT dest) */
			MERGECOPY           = 0x00c000ca, /* dest = (source AND pattern)     */
			MERGEPAINT          = 0x00bb0226, /* dest = (NOT source) OR dest     */
			PATCOPY             = 0x00f00021, /* dest = pattern                  */
			PATPAINT            = 0x00fb0a09, /* dest = DPSnoo                   */
			PATINVERT           = 0x005a0049, /* dest = pattern XOR dest         */
			DSTINVERT           = 0x00550009, /* dest = (NOT dest)               */
			BLACKNESS           = 0x00000042, /* dest = BLACK                    */
			WHITENESS           = 0x00ff0062; /* dest = WHITE                    */
		#endregion

		//-----------------------------------------------------------------------------
		#region ** fields
		private Hashtable				_gdiHandles;		// handles to gdi objects
		private Stack                   _clipStack;         // clipping rectangles stack
		protected PointF				_dpi;				// original metafile resolution (before emf conversion)
		protected Metafile				_meta;				// metafile being enumerated
		protected bool					_clipAllStrings;	// optionally clip all strings
		protected float[]				_xform9x;	        // suport Win9x transforms
		Graphics.EnumerateMetafileProc	_callBack;			// allow caller to handle emf records
		protected EmfPlusRecordType		_previousRecord;	// used for clipping strings
		protected RectangleF			_clipRect;			// ditto, needed for Win9x
        protected PointF[]              _clipRegion;        // clip region (polyline)
        protected ReferenceDC           _refDC;				// optional, will create one if needed

		// hdc state variables
		protected MetaFont				_font;
		protected Pen					_pen;
		protected Brush					_brush;
		protected int					_textAlign;
		protected SolidBrush			_textBrush, _bkBrush;
		protected PointF				_currPoint;
		protected int					_stretchBltMode;
		protected bool					_clockWiseArcs;
		private Font					_knownFont;

		// lock object used for multi-threading
		//private static object[] _lockObject = new object[0];
        private static Bitmap _lockBitmap = new Bitmap(10, 10);

#if TRACETOFILE && DEBUG // for debugging only
		private StreamWriter			_swTrace;
#endif
		#endregion

		//-----------------------------------------------------------------------------
		#region ** ctor

		protected MetafileConverterBase()
		{
			_callBack = new Graphics.EnumerateMetafileProc(MetafileCallback);
			_clipAllStrings = false;
		}

        ~MetafileConverterBase()
        {
            Dispose();
        }
        public virtual void Dispose()
        {
            DisposeObjects();
        }
        private void DisposeObjects()
        {
            if (_sysObjects != null)
            {
                foreach (object obj in _sysObjects.Values)
                {
                    if (obj is IDisposable)
                        ((IDisposable)obj).Dispose();
                }
                _sysObjects.Clear();
                _sysObjects = null;
            }
        }

        #endregion

		//-----------------------------------------------------------------------------
		#region ** internal methods (called by owner class) **

		// main method, enumerates the given metafile, parsing each command
		// and calling the appropriate subordinate methods
		internal void Render(Metafile meta)
		{
			// if a refDC was provided, use it
			if (_refDC != null)
			{
				Render(meta, _refDC);
				return;
			}

			// create a temp refDC and use that instead
			using (ReferenceDC refDC = new ReferenceDC(false))
			{
				Render(meta, refDC);
			}
		}
		private void Render(Metafile meta, ReferenceDC refDC)
		{
			// initialize gdi objects
			_gdiHandles		= new Hashtable();
			_textBrush		= (SolidBrush)Brushes.Black;
			_bkBrush		= (SolidBrush)Brushes.Transparent;
			_brush			= Brushes.Black;
			_pen			= Pens.Black;
			_clockWiseArcs	= true;

			// save original metafile resolution 
			// (before converting metafile into Emf) <<B31>>
			_dpi = PointF.Empty;
			try
            {
                _dpi = new PointF(meta.HorizontalResolution, meta.VerticalResolution);
            } 
			catch (Exception x)
            {
                throw new ApplicationException("Cannot get metafile resolution: " + x.Message);
            }
			if (_dpi.X == 0) _dpi.X = 96f;
			if (_dpi.Y == 0) _dpi.Y = 96f;
            
			// we only do MetafileType.Emf, so convert Plus/Dual as needed
			_meta = ConvertToEmf(meta);

			// set advanced graphics mode so world transforms will work
			_hdcXform = refDC.Hdc;
			SetGraphicsMode(_hdcXform, GM_ADVANCED);

			// uncomment this to test Win9x compatibility
			//SetGraphicsMode(_hdcXform, GM_COMPATIBLE);

#if TRACETOFILE && DEBUG
			_swTrace = new System.IO.StreamWriter(@"c:\temp\trace.txt");
            //_swTrace = new System.IO.StreamWriter(Path.Combine(Path.GetTempPath(), "MetafileConverterBase_trace.txt"));
#endif
            // play metafile 
            // <<B223>> lock '_lockBitmap' for thread safety
            lock (_lockBitmap) //_lockObject)
            {
                using (Graphics g = Graphics.FromImage(_lockBitmap))
                {
                     g.EnumerateMetafile(_meta, PointF.Empty, _callBack);
                }

                //using (Graphics g = Graphics.FromHdc(refDC.Hdc)) // <<B265>>
                //using (Bitmap bmp = new Bitmap(5, 5))
                //using (Graphics g = Graphics.FromImage(bmp)) // <<B265>>
                //using (Graphics g = Graphics.FromImage(_lockBitmap)) // <<B265>>
                //{
                    //g.EnumerateMetafile(_meta, PointF.Empty, _callBack);
                //}
            }

#if TRACETOFILE && DEBUG
			_swTrace.Close();
#endif
            // dispose stock objects (fonts, pens, bruhses)
            DisposeObjects();

            // all done
			_hdcXform = IntPtr.Zero;
			_gdiHandles.Clear();
		}

		// we only do Emf metafiles, so convert if we have to
		private Metafile ConvertToEmf(Metafile meta)
		{
			// no need to convert
			if (meta.GetMetafileHeader().Type == MetafileType.Emf)
			{
				return meta;
			}
            
			// if a refDC was provided, use it
			if (_refDC != null)
			{
				return ConvertToEmf(meta, _refDC);
			}

			// create a temp refDC and use that instead
			using (ReferenceDC refDC = new ReferenceDC(false))
			{
				return ConvertToEmf(meta, refDC);
			}
		}
		private Metafile ConvertToEmf(Metafile meta, ReferenceDC refDC)
		{
			// convert if type is not Emf
			if (meta.GetMetafileHeader().Type != MetafileType.Emf)
			{
				// create new metafile
				SizeF sz = GetImageSizeInPoints(meta);
				RectangleF rc = new RectangleF(PointF.Empty, sz);
				Metafile metaNew = new Metafile(refDC.Hdc, rc, MetafileFrameUnit.Point, EmfType.EmfOnly);
				using (Graphics g = Graphics.FromImage(metaNew))
				{
					// account for the difference between 
					// logical and physical dpi in display metafiles
					rc = new RectangleF(PointF.Empty, meta.Size);
                    g.PageUnit = GraphicsUnit.Pixel;
					g.DrawImage(meta, rc);
					//g.DrawImageUnscaled(meta, 0, 0);
				}
				meta = metaNew;
			}

			// done
			return meta;
		}

		// set font from module where the metafile has been created <<OLEG>>
		internal void SetKnownFont(Font font)
		{
			_knownFont = font;
		}

		// get the size of an image expressed in points
		internal static SizeF GetImageSizeInPoints(Image img)
		{
			// get image resolution
			float resx = img.HorizontalResolution;
			float resy = img.VerticalResolution;

			// if either is zero, replace with screen res
			if (resx == 0) resx = 96f;
			if (resy == 0) resy = 96f;

			// calculate image size
			float wid = (float)Math.Round(img.Width  * 72f / resx, 4);
			float hei = (float)Math.Round(img.Height * 72f / resy, 4);
			return new SizeF(wid, hei);
		}
		#endregion

		//-----------------------------------------------------------------------------
		#region ** abstract properties/methods **

		// ** properties
		protected abstract float XFactor { get; }
		protected abstract float YFactor { get; }

		// for custom callback
		protected virtual bool CustomCallback(BinaryReader br, EmfPlusRecordType recordType, int flags)
		{
			return false;
		}

		// for move to overrides
		protected virtual void EmfMoveTo(PointF pt) { }

		// fill mode for document
		protected abstract void EmfSetFillMode(FillMode mode);

		// for path commands
		protected abstract void EmfPathCommand(EmfPlusRecordType recordType);

		// for custom gdi comments
		protected abstract void EmfGdiComment(RectangleF rc, string comment);

		// for draw text
        protected abstract void EmfExtTextOut(PointF pt, RectangleF rc, RectangleF rcl, string text, int fOptions, int[] lpdx, PointF[] pscl);

		// for bit/blit operations
		protected abstract void EmfStretchDIBBits(BinaryReader br, IntPtr data, RectangleF rcImage,
			RectangleF rcClip, int dwRop, int offBmiSrc, int cbBmiSrc, int offBitsSrc, int cbBitsSrc);

		// for draw graphics
		protected abstract void EmfFillRectangle(RectangleF rc);
		protected abstract void EmfDrawLine(PointF pt1, PointF pt2);
		protected abstract void EmfDrawRectangle(RectangleF rc);
		protected abstract void EmfDrawEllipse(RectangleF rc);
		protected abstract void EmfDrawRoundRectangle(RectangleF rc, SizeF corner);
		protected abstract void EmfPieArc(RectangleF rc, PointF ptlStart, PointF ptlEnd, bool pie, bool fill);
		protected abstract void EmfPolygon(PointF[] points, bool close);
		protected abstract void EmfPolyPolygon(PointF[][] points, bool close);
		protected abstract void EmfPolyBezier(PointF[] points);
		#endregion

		//-----------------------------------------------------------------------------
		#region ** metafile enumerator callback **
		private bool MetafileCallback(
			EmfPlusRecordType recordType, 
			int flags, 
			int dataSize,
			IntPtr data, 
			PlayRecordCallback callbackData)
		{
			// read record data
			BinaryReader br = null;
			byte[] dataArray = null;
			if (data != IntPtr.Zero)
			{
				dataArray = new byte[dataSize];
				Marshal.Copy(data, dataArray, 0, dataSize);
				br = new BinaryReader(new MemoryStream(dataArray));
			}

			// call user callback to allow custom handling of some/all records
			CustomCallback(br, recordType, flags);

			// handle meta record
			int gdiHandle;
			switch (recordType)
			{
				// handle custom comments
				case EmfPlusRecordType.EmfGdiComment:
					EmfGdiComment(br);
					break;

				// store clipping rectangle to use when the next record
				// is either EmfExtTextOutA or EmfExtTextOutW <<B9>>
				case EmfPlusRecordType.EmfIntersectClipRect:
					if (_clipRect != RectangleF.Empty)
					{
						_clipRect = RectangleF.Intersect(_clipRect, ReadRectangle(br));
					}
					else
					{
						_clipRect = ReadRectangle(br);
					}
					if (_clipRect.Bottom < 0 || _clipRect.Right < 0)
					{
						_clipRect = RectangleF.Empty;
					}
					break;

				// handle EmfExtSelectClipRgn structure
				case EmfPlusRecordType.EmfExtSelectClipRgn:
					{
						int cbRgnData = br.ReadInt32(); // size of region data, in bytes
						int iMode     = br.ReadInt32(); // operation to be performed

						// no clipping region, reset clip rect
						if (cbRgnData == 0)
						{
                            Debug.Assert(iMode == 5);
                            _clipRect = RectangleF.Empty;
                            _clipRegion = null;
							break;
						}

						// assert that we have at least one region
						Debug.Assert(cbRgnData >= 32);

						// read region header (RGNDATAHEADER)
						int dwSize   = br.ReadInt32(); 
						int iType    = br.ReadInt32(); 
						int nCount   = br.ReadInt32(); 
						int nRgnSize = br.ReadInt32(); 

						// set clip to region rectangle
						RectangleF rcClip = ReadRectangle(br);
                        if (nCount > 1)
                        {
                            // multi rectanges clip region
                            ArrayList left = new ArrayList();
                            ArrayList right = new ArrayList();
                            float middle = rcClip.Top + rcClip.Height / 2;
                            for (int i = 0; i < nCount; i++)
                            {
                                RectangleF r = ReadRectangle(br);
                                if (r.Top <= middle)
                                {
                                    left.Add(new PointF(r.Left, r.Top));
                                    right.Add(new PointF(r.Right, r.Top));
                                }
                                if (r.Bottom > middle)
                                {
                                    left.Add(new PointF(r.Left, r.Bottom));
                                    right.Add(new PointF(r.Right, r.Bottom));
                                }
                            }
                            for (int i = right.Count - 1; i >= 0; i--)
                            {
                                left.Add(right[i]);
                            }
                            _clipRegion = (PointF[])left.ToArray(typeof(PointF));
                        }

                        // apply operation
                        switch (iMode)
						{
							case 1:  // RGN_AND
								_clipRect = RectangleF.Intersect(rcClip, _clipRect);
								break;
							case 2:  // RGN_OR
								_clipRect = RectangleF.Union(rcClip, _clipRect);
								break;
							case 3:  // RGN_XOR
								break;
							case 4:  // RGN_DIFF
								break;
							case 5:  // RGN_COPY
								_clipRect = rcClip;
								break;
						}
					}
					if (_clipRect.Bottom < 0 || _clipRect.Right < 0)
					{
						_clipRect = RectangleF.Empty;
					}
					break;

				// render text
				case EmfPlusRecordType.EmfExtTextOutA:	
					EmfExtTextOut(br, false);
					break;
				case EmfPlusRecordType.EmfExtTextOutW:
					EmfExtTextOut(br, true);
					break;

				// bit/blt operations
				case EmfPlusRecordType.EmfSetStretchBltMode:
					_stretchBltMode = br.ReadInt32();
					break;
				case EmfPlusRecordType.EmfStretchDIBits:
					{
						RectangleF rcClip = ReadDeviceRect(br); // << device units
						PointF		ptDest			= ReadPoint(br);
						PointF		ptSrc			= ReadPoint(br);
						SizeF		szSrc			= ReadSize(br);
						int			offBmiSrc		= br.ReadInt32();
						int			cbBmiSrc		= br.ReadInt32();
						int			offBitsSrc		= br.ReadInt32();
						int			cbBitsSrc		= br.ReadInt32();
						int			iUsageSrc		= br.ReadInt32();
						int			dwRop			= br.ReadInt32();
						SizeF		szDest			= ReadSize(br);
						RectangleF	rcImage			= new RectangleF(ptDest, szDest);

						// note:
						//   the first rectangle (rcBounds/rcClip) contains the area
						//   on the device that will receive the image.
						//   the second (rcImage) contains the source portion of the 
						//   image that will be used.
                        //   together they allow for pretty much any type of scaling,
						//   alignment, and cropping of the image.
						//
						EmfStretchDIBBits(br, data, rcImage, rcClip, dwRop, offBmiSrc, cbBmiSrc, offBitsSrc, cbBitsSrc);
					}
					break;
				case EmfPlusRecordType.EmfStretchBlt:
					{
						RectangleF	rcClip			= ReadDeviceRect(br); // << device units
						PointF		ptDest			= ReadPoint(br);
						SizeF		szDest			= ReadSize(br);
						int			dwRop			= br.ReadInt32(); 
						PointF		ptSrc			= ReadPoint(br);
						float[]		xformSrc		= ReadTransform(br);; 
						int			crBkColorSrc	= br.ReadInt32(); 
						int			iUsageSrc		= br.ReadInt32(); 
						int			offBmiSrc		= br.ReadInt32(); 
						int			cbBmiSrc		= br.ReadInt32(); 
						int			offBitsSrc		= br.ReadInt32(); 
						int			cbBitsSrc		= br.ReadInt32(); 
						SizeF		szSrc			= ReadSize(br);
						RectangleF	rcImage			= new RectangleF(ptDest, szDest);

						// PATCOPY is used to fill rectangles
						if (dwRop == PATCOPY && cbBmiSrc == 0) 
						{
							EmfFillRectangle(rcImage);
						}
						
						// SRCCOPY/SRCPAINT can be handled with StretchDIBBits code
						if ((dwRop == SRCCOPY || dwRop == SRCPAINT) && offBmiSrc != 0) 
						{
							EmfStretchDIBBits(br, data, rcImage, rcClip, dwRop, offBmiSrc, cbBmiSrc, offBitsSrc, cbBitsSrc);
						}
					}
					break;
				case EmfPlusRecordType.EmfBitBlt:	
					{
						RectangleF rc		= ReadDeviceRect(br); // << device units
						PointF ptDest		= ReadPoint(br);
						SizeF szDest		= ReadSize(br);
						int dwRop			= br.ReadInt32();
						PointF ptSrc		= ReadPoint(br);
						float[] xform		= ReadTransform(br);
						int crBkColorSrc	= br.ReadInt32();
						int iUsageSrc		= br.ReadInt32();
						int offBmiSrc		= br.ReadInt32();
						int cbBmiSrc		= br.ReadInt32();
						int offBitsSrc		= br.ReadInt32();
						int cbBitsSrc		= br.ReadInt32();
						RectangleF rcDest = new RectangleF(ptDest, szDest);

                        // PATCOPY is used to fill rectangles
                        if (dwRop == PATCOPY && cbBmiSrc == 0)
                        {
                            if (!rc.IsEmpty)
                            {
                                rcDest = RectangleF.Intersect(rc, rcDest);
                            }
                            EmfFillRectangle(rcDest);
                        }

						// PATCOPY & PATINVERT are used to fill rectangles
                        //if ((dwRop == PATCOPY || dwRop == PATINVERT) && cbBmiSrc == 0)
                        //{
                        //    if (!rc.IsEmpty)
                        //    {
                        //        rcDest = RectangleF.Intersect(rc, rcDest);
                        //    }
                        //    Brush brush = _brush;
                        //    if (dwRop == PATINVERT && brush is SolidBrush)
                        //    {
                        //        Color clr = ((SolidBrush)brush).Color;
                        //        _brush = new SolidBrush(Color.FromArgb(clr.A, (byte)~clr.R, (byte)~clr.G, (byte)~clr.B));
                        //    }
                        //    EmfFillRectangle(rcDest);
                        //    _brush = brush;
                        //}
					}
					break;

				// gdi object creation/selection
				case EmfPlusRecordType.EmfCreateBrushIndirect:
					gdiHandle = br.ReadInt32();
					_gdiHandles.Add(gdiHandle, ReadBrush(br));
					break;
				case EmfPlusRecordType.EmfCreatePen:
					gdiHandle = br.ReadInt32();
					_gdiHandles.Add(gdiHandle, ReadPen(br));
					break;
				case EmfPlusRecordType.EmfExtCreatePen:
					gdiHandle = br.ReadInt32();
					_gdiHandles.Add(gdiHandle, ReadExtPen(br));
					break;
				case EmfPlusRecordType.EmfExtCreateFontIndirect:
					gdiHandle = br.ReadInt32();
					_gdiHandles.Add(gdiHandle, ReadFont(br));
					break;
				case EmfPlusRecordType.EmfSelectObject:
					gdiHandle = br.ReadInt32();
					object obj = ((gdiHandle & 0x80000000) == 0) 
						? _gdiHandles[gdiHandle]
						: GetStockObject(gdiHandle);
#if DEBUG
					if (obj == null)
						Debug.WriteLine("skipping object");
#endif
					if (obj is MetaFont)	_font  = (MetaFont)obj;
					if (obj is Brush)		_brush = (Brush)obj;
					if (obj is Pen)			_pen   = (Pen)obj;
					break;
				case EmfPlusRecordType.EmfDeleteObject:
					gdiHandle = br.ReadInt32();
					_gdiHandles.Remove(gdiHandle);
					break;

				// pattern brushes not supported, use gray instead <<B37>>
				case EmfPlusRecordType.EmfCreateMonoBrush:
				case EmfPlusRecordType.EmfCreateDibPatternBrushPt:
					gdiHandle = br.ReadInt32();
					_gdiHandles.Add(gdiHandle, Brushes.Gray);
					break;

				// simple state management
				case EmfPlusRecordType.EmfSetTextColor:	
					_textBrush = new SolidBrush(ColorTranslator.FromWin32(br.ReadInt32()));
					break;
				case EmfPlusRecordType.EmfSetBkColor:
					_bkBrush = new SolidBrush(ColorTranslator.FromWin32(br.ReadInt32()));
					break;
				case EmfPlusRecordType.EmfSetTextAlign:
					_textAlign = br.ReadInt32();
					break;

				// simple drawing
				case EmfPlusRecordType.EmfMoveToEx:
					_currPoint = ReadPoint(br);
					EmfMoveTo(_currPoint);
					break;
				case EmfPlusRecordType.EmfLineTo:
					PointF pt = ReadPoint(br);
					EmfDrawLine(_currPoint, pt);
					_currPoint = pt;
					break;
				case EmfPlusRecordType.EmfRectangle:
					{
						RectangleF rc = ReadRectangle(br);
						EmfDrawRectangle(rc);
					}
					break;
				case EmfPlusRecordType.EmfEllipse:
					{
						RectangleF rc = ReadRectangle(br);
						EmfDrawEllipse(rc);
					}	
					break;
				case EmfPlusRecordType.EmfRoundRect:
					RectangleF rcBox = ReadRectangle(br);
					SizeF szCorner = ReadSize(br);   // << read corner diameter
					szCorner.Width /= 2f;			 // << convert to corner radii
					szCorner.Height /= 2f;
					EmfDrawRoundRectangle(rcBox, szCorner);
					break;

				// arcs
				case EmfPlusRecordType.EmfPie:
					EmfPieArc(br, true, true);
					break;
				case EmfPlusRecordType.EmfChord:
					EmfPieArc(br, false, true);
					break;
				case EmfPlusRecordType.EmfArcTo:
				case EmfPlusRecordType.EmfRoundArc:
					EmfPieArc(br, false, false);
					break;
				case EmfPlusRecordType.EmfSetArcDirection:			
					_clockWiseArcs = (br.ReadInt32() == AD_CLOCKWISE);
					break;

				// path commands
				case EmfPlusRecordType.EmfBeginPath:
				case EmfPlusRecordType.EmfAbortPath:
				case EmfPlusRecordType.EmfEndPath:
				case EmfPlusRecordType.EmfCloseFigure:
				case EmfPlusRecordType.EmfStrokePath:
				case EmfPlusRecordType.EmfFillPath:
				case EmfPlusRecordType.EmfStrokeAndFillPath:
                case EmfPlusRecordType.EmfSelectClipPath:
                    EmfPathCommand(recordType);
					break;

				// polygons/lines
				case EmfPlusRecordType.EmfPolygon:
					EmfPolygon(br, false, true, false);
					break;
				case EmfPlusRecordType.EmfPolyline:
					EmfPolygon(br, false, false, false);
					break;
				case EmfPlusRecordType.EmfPolyLineTo:
					EmfPolygon(br, false, false, true);
					break;
				case EmfPlusRecordType.EmfPolygon16:
					EmfPolygon(br, true, true, false);
					break;
				case EmfPlusRecordType.EmfPolyline16:
					EmfPolygon(br, true, false, false);
					break;
				case EmfPlusRecordType.EmfPolylineTo16:
					EmfPolygon(br, true, false, true);
					break;

				case EmfPlusRecordType.EmfPolyPolygon:
					EmfPolyPolygon(br, false, true);
					break;
				case EmfPlusRecordType.EmfPolyPolyline:
					EmfPolyPolygon(br, false, false);
					break;
				case EmfPlusRecordType.EmfPolyPolygon16:
					EmfPolyPolygon(br, true, true);
					break;
				case EmfPlusRecordType.EmfPolyPolyline16:
					EmfPolyPolygon(br, true, false);
					break;

				case EmfPlusRecordType.EmfPolyBezier:
					EmfPolyBezier(br, false, false);
					break;
				case EmfPlusRecordType.EmfPolyBezierTo:
					EmfPolyBezier(br, false, true);
					break;
				case EmfPlusRecordType.EmfPolyBezier16:
					EmfPolyBezier(br, true, false);
					break;
				case EmfPlusRecordType.EmfPolyBezierTo16:
					EmfPolyBezier(br, true, true);
					break;

				case EmfPlusRecordType.EmfSetPolyFillMode:
					FillMode mode = (br.ReadInt32() == ALTERNATE) ? FillMode.Alternate : FillMode.Winding;
					EmfSetFillMode(mode);
					break;

				case EmfPlusRecordType.EmfPolyDraw:
				case EmfPlusRecordType.EmfPolyDraw16:
				case EmfPlusRecordType.EmfAngleArc:
					break;

				case EmfPlusRecordType.EmfFillRgn:
					EmfFillRgn(br);
					break;

				// save/restore dc
				case EmfPlusRecordType.EmfRestoreDC:
				case EmfPlusRecordType.EmfSaveDC:
					EmfPlusSaveRestoreDC(recordType, br);
					break;

				// coordinate transformations
				case EmfPlusRecordType.EmfModifyWorldTransform:
				case EmfPlusRecordType.EmfSetWorldTransform:
					EmfPlusWorldTransform(recordType, br);
					break;
				case EmfPlusRecordType.EmfScaleViewportExtEx:
				case EmfPlusRecordType.EmfScaleWindowExtEx:
				case EmfPlusRecordType.EmfSetViewportExtEx:
				case EmfPlusRecordType.EmfSetWindowExtEx:
				case EmfPlusRecordType.EmfSetViewportOrgEx:
				case EmfPlusRecordType.EmfSetWindowOrgEx:
					EmfPlusSetWindowViewPort(recordType, br);
					break;
				case EmfPlusRecordType.EmfSetMapMode:
					EmfSetMapMode(br);
					break;
#if DEBUG
				default:
					if (recordType != EmfPlusRecordType.EmfSetBkMode)
					{
                        Debug.WriteLine(string.Format("UNHANDLED: rec {0} datasize {1} dataptr {2}",
                            recordType, dataSize, data));
					}
					break;
#endif
			}

			// save last record type
			_previousRecord = recordType;

            // keep enumerating
			return true;
		}
		#endregion

		//-----------------------------------------------------------------------------
		#region ** internal read methods **

		// ** gdi comments are application-specific. VSView and VSReport use them to
		// mark documents with their outline structure, hyperlinks, and link targets.
		// c1report uses the same mechanism, and c1pdf honors it by interpreting 
		// comments with the following structure:
		//
		//	DWORD	dwIdent;	// signature: GDICOMMENT_VSMETATAGIDENTIFIER
		//	RECTL	rc;			// tag area in logical coords
		//	UINT	nLen;		// number of characters in tag string
		//	WCHAR	sTag[];		// tag text
		private void EmfGdiComment(BinaryReader br)
		{
			const uint GDICOMMENT_VSMETATAGIDENTIFIER = 0x88880001;
			const int  GDICOMMENT_VSMETATAGMINSIZE    = 20;

			// check comment size
			int cnt = br.ReadInt32();
			if (cnt < GDICOMMENT_VSMETATAGMINSIZE) return;

			// check for VSTAG identifier
			uint id = br.ReadUInt32();
			if (id != GDICOMMENT_VSMETATAGIDENTIFIER) return;

			// read tag info
			RectangleF rc  = ReadRectangle(br);
			int len        = br.ReadInt32();
			byte[] data    = br.ReadBytes(2*len);
			string comment = Encoding.Unicode.GetString(data);

			// call abstract method
			EmfGdiComment(rc, comment);
		}

		// ** draw text
		private void EmfExtTextOut(BinaryReader br, bool unicode)
		{
			// read information from emr record
			RectangleF rcBounds  = ReadDeviceRect(br);	// bounding rectangle, in device units
			int mode			 = br.ReadInt32();		// current graphics mode: GM_COMPATIBLE/GM_ADVANCED
			float xscale		 = br.ReadSingle();		// scaling factor from page to .01mm units in GM_COMPATIBLE mode
			float yscale		 = br.ReadSingle();		// scaling factor from page to .01mm units in GM_COMPATIBLE mode
			PointF ptlReference	 = ReadPoint(br);		// logical reference Point used to position the string (accounts for alignment)
			int nChars			 = br.ReadInt32();		// number of chars in the string
			int offString		 = br.ReadInt32();		// offset to string data
			int fOptions		 = br.ReadInt32();		// how to use the application-defined rectangle: ETO_CLIPPED/ETO_OPAQUE
			RectangleF rcl		 = ReadRectangle(br);	// clipping/opaquing rectangle, in logical units.
			int offDx			 = br.ReadInt32();		// offset to intercharacter spacing data

			// honor TA_UPDATECP flag <<B32>>
			if ((_textAlign & TA_UPDATECP) == TA_UPDATECP)
				ptlReference = _currPoint;

			// read inter-character data <<B29>>
			int[] lpdx = null;
			if (offDx > 8 && offDx - 8 + 4*nChars <= br.BaseStream.Length)
			{
				lpdx = new int[nChars];
				br.BaseStream.Position = offDx-8;
                for (int i = 0; i < nChars; i++)
                {
                    lpdx[i] = br.ReadInt32();
                }
			}

			// draw opaque background
			if ((fOptions & ETO_OPAQUE) != 0)
			{
				Brush brush = _brush;
				_brush = _bkBrush;
				EmfFillRectangle(rcl);
				_brush = brush;
			}

			// check that we have some text
            if (nChars == 0)
            {
                return;
            }

            // clip region
            PointF[] pscl = null;

#if TRACETOFILE && DEBUG
			if (_clipAllStrings || _previousRecord == EmfPlusRecordType.EmfIntersectClipRect)
			{
				_swTrace.WriteLine("-- clipped text: rcBounds     {0} {1} {2} {3}", rcBounds.Left, rcBounds.Top, rcBounds.Width, rcBounds.Height);
				_swTrace.WriteLine("-- clipped text: ptlReference {0} {1}",         ptlReference.X, ptlReference.Y);
				_swTrace.WriteLine("-- clipped text: rcl          {0} {1} {2} {3}", rcl.Left, rcl.Top, rcl.Width, rcl.Height);
			}
#endif
			// force string clipping if requested (_clipAllStrings) or
			// if the previous record was an EmfIntersectClipRect <<B7>>
			// (common sequence used to clip strings in .NET metafiles)
			if ((fOptions & ETO_CLIPPED) != ETO_CLIPPED)
			{
				// add clipping only for GM_ADVANCED mode
                if (mode == GM_ADVANCED && _clipAllStrings)
                {
                    // get logical clipping rectangle (rcl is bad if fOptions == 0)
                    if (fOptions == 0)
                    {
                        // set clipping flag
                        rcl = rcBounds;
                        fOptions |= ETO_CLIPPED;
                    }

                    // use clipping rectangle stored in EmfIntersectClipRect <<B9>>
                    if (_clipRect != RectangleF.Empty)
                    {
                        // set clipping flag
                        rcl = (rcl.IsEmpty)
                            ? _clipRect
                            : RectangleF.Intersect(rcl, _clipRect);
                        fOptions |= ETO_CLIPPED;
                    }

                    // clipping region
                    if (rcl.IsEmpty && _clipRegion != null && _clipRegion.Length > 2)
                    {
                        // set clipping flag
                        pscl = _clipRegion;
                        fOptions |= ETO_CLIPPED;
                    }
                }
                else if (mode == GM_COMPATIBLE && fOptions == 0 && rcl.IsEmpty)
                {
                    if (_clipRegion != null && _clipRegion.Length > 2)
                    {
                        // set clipping flag
                        pscl = _clipRegion;
                        fOptions |= ETO_CLIPPED;
                    }
                    else if (_clipRect != RectangleF.Empty)
                    {
                        // set clipping flag
                        rcl = _clipRect;
                        fOptions |= ETO_CLIPPED;
                    }
                }
			}
			else
			{
				// use bounds for clipping, rcl seems slightly off and occasionally
				// seems to clip the end of the strings <<B29>>
				if (rcBounds.Contains(rcl))
				{
					rcl = rcBounds;
				}
				else if (!rcBounds.IsEmpty)
				{
					rcl = RectangleF.Intersect(rcl, rcBounds);
				}
			}

			// reset clipping flag at empty clipping area
            if (rcl.IsEmpty && pscl == null)
			{
				fOptions &= ~ETO_CLIPPED;
			}

			// decode text bytes
			br.BaseStream.Position = offString-8;
			string text = string.Empty;
			if (unicode)
			{
				byte[] data = br.ReadBytes(2 * nChars);
				text = Encoding.Unicode.GetString(data);
			}
			else
			{
				byte[] data = br.ReadBytes(nChars);
				text = Encoding.Default.GetString(data);
			}

			// check that we have some printable text (no need to write spaces)
			Debug.Assert(_font != null);
            if (text.Trim().Length == 0 || _font == null)
            {
                return;
            }

#if TRACETOFILE && DEBUG
			_swTrace.WriteLine("-- \"{0}\"", text);
#endif

			// call abstract method
            EmfExtTextOut(ptlReference, rcBounds, rcl, text, fOptions, lpdx, pscl);
		}

		// ** TODO: draw image

		// ** draw graphics

		// draw pie/arc
		private void EmfPieArc(BinaryReader br, bool pie, bool fill)
		{
			// read parameters
			RectangleF rc	= ReadRectangle(br);
			PointF ptlStart	= ReadPoint(br);
			PointF ptlEnd	= ReadPoint(br);

			// call abstract method
			EmfPieArc(rc, ptlStart, ptlEnd, pie, fill);
		}

		// draw polygons/polylines
		private void EmfPolygon(BinaryReader br, bool shortPoints, bool close, bool useCurrent)
		{
			// read data size
			RectangleF rc	= ReadRectangle(br);
			int cpts		= br.ReadInt32();
			if (useCurrent) cpts++;

			// read data points
			PointF[] points = new PointF[cpts];
			int start = 0;
            if (useCurrent)
            {
                points[start++] = _currPoint;
            }
            for (int i = start; i < cpts; i++)
            {
                points[i] = (shortPoints) ? ReadPointShort(br) : ReadPoint(br);
            }

			// call abstract method
			EmfPolygon(points, close);

			// update current point
            if (useCurrent)
            {
                _currPoint = points[cpts - 1];
            }
		}
		private void EmfPolyPolygon(BinaryReader br, bool shortPoints, bool close)
		{
			// read data
			RectangleF rc	= ReadRectangle(br); 
			int npolys		= br.ReadInt32(); 
			int cpts		= br.ReadInt32();
			PointF[][] points = new PointF[npolys][];
            for (int poly = 0; poly < npolys; poly++)
            {
                points[poly] = new PointF[br.ReadInt32()];
            }

			// each of npolys polygons
			for (int poly = 0; poly < npolys; poly++)
			{
				// read data for this polygon
                for (int i = 0; i < points[poly].Length; i++)
                {
                    points[poly][i] = (shortPoints) ? ReadPointShort(br) : ReadPoint(br);
                }
			}

			// call abstract method
			EmfPolyPolygon(points, close);
		}

		// draw bezier
		private void EmfPolyBezier(BinaryReader br, bool shortPoints, bool useCurrent)
		{
			// read data size
			RectangleF rc	= ReadRectangle(br); 
			int cpts		= br.ReadInt32();
			if (useCurrent) cpts++;

			// read data points
			PointF[] points = new PointF[cpts];
			int start = 0;
            if (useCurrent)
            {
                points[start++] = _currPoint;
            }
            for (int i = start; i < points.Length; i++)
            {
                points[i] = (shortPoints) ? ReadPointShort(br) : ReadPoint(br);
            }

			// call abstract method
			EmfPolyBezier(points);

			// update current point
            if (useCurrent)
            {
                _currPoint = points[points.Length - 1];
            }
		}

		// fill region
		private void EmfFillRgn(BinaryReader br)
		{
			RectangleF rc = ReadDeviceRect(br);
			int cbRgnData = br.ReadInt32();
			int gdiHandle = br.ReadInt32();

			object obj = ((gdiHandle & 0x80000000) == 0) 
				? _gdiHandles[gdiHandle]
				: GetStockObject(gdiHandle);

			Brush brush = (Brush)obj;
			if (brush != null && brush is SolidBrush)
			{
				Brush old = _brush;
				_brush = brush;

				// TODO: need read all points and fill for this points
				EmfFillRectangle(rc);
				_brush = old;
			}
		}
		#endregion

		//-----------------------------------------------------------------------------
		#region ** data reading/conversion **

		internal RectangleF ReadDeviceRect(BinaryReader br)
		{
#if true
            // device units, convert pixels to points and don't apply any transforms
            // note:
            //   adding one pixel to right/bottom values before the conversion to get
            //   the rectangle size right. for example, if left = 1 and right = 1, then
            //   the rectangle is one pixel wide (Width = right - left + 1). 
            // this addresses an issue reported by Act. <<B68>>
            float left = br.ReadInt32() * 72f / _dpi.X * this.XFactor;
            float top = br.ReadInt32() * 72f / _dpi.Y * this.YFactor;
            float right = (br.ReadInt32() + 1) * 72f / _dpi.X * this.XFactor;
            float bottom = (br.ReadInt32() + 1) * 72f / _dpi.Y * this.YFactor;

            // done
            return new RectangleF(left, top, right - left, bottom - top);
#else
            // old version, may causes wrong clipping on small images
			// device units, convert pixels to points and don't apply any transforms
			float left   = br.ReadInt32() * 72f / _dpi.X * this.XFactor;
			float top    = br.ReadInt32() * 72f / _dpi.Y * this.YFactor;
			float right  = br.ReadInt32() * 72f / _dpi.X * this.XFactor;
			float bottom = br.ReadInt32() * 72f / _dpi.Y * this.YFactor;

			// <<B192>> do not include 1-pixel border in rectangle size
			//return new RectangleF(left, top, right - left + 1, bottom - top + 1);
			return new RectangleF(left, top, right - left, bottom - top);
#endif
		}
		internal RectangleF ReadRectangle(BinaryReader br)
		{
			PointF lt = ReadPoint(br);
			PointF rb = ReadPoint(br);
			return new RectangleF(lt.X, lt.Y, rb.X - lt.X, rb.Y - lt.Y);
		}
		internal PointF ReadPoint(BinaryReader br)
		{
			Point pt = Point.Empty;
			pt.X = br.ReadInt32();
			pt.Y = br.ReadInt32();
			return ConvertPoint(pt);
		}
		internal PointF ReadPointShort(BinaryReader br)
		{
			Point pt = Point.Empty;
			pt.X = br.ReadInt16();
			pt.Y = br.ReadInt16();
			return ConvertPoint(pt);
		}
		internal SizeF ReadSize(BinaryReader br)
		{
			PointF org = ConvertPoint(PointF.Empty);
			PointF pt  = ReadPoint(br);
			return new SizeF(pt.X - org.X, pt.Y - org.Y);
		}
		private Brush ReadBrush(BinaryReader br)
		{
			int lbStyle	= br.ReadInt32(); 
			int lbColor	= br.ReadInt32();
			int lbHatch	= br.ReadInt32();
			Color color = (lbStyle == BS_NULL)
				? Color.Transparent
				: ColorTranslator.FromWin32(lbColor);
			return (Brush)new SolidBrush(color);
		}
		private Pen ReadPen(BinaryReader br)
		{
			int lopnStyle = br.ReadInt32();
			SizeF lopnWidth = ReadSize(br);
			int lopnColor = br.ReadInt32();
            return CreatePen(lopnColor, lopnWidth.Width, lopnStyle & PS_STYLE_MASK, new float[0]);
		}
		protected Pen ReadExtPen(BinaryReader br)
		{
			int offBmi = br.ReadInt32();
			int cbBmi = br.ReadInt32();
			int offBits = br.ReadInt32();
			int cbBits = br.ReadInt32();
			int elpPenStyle = br.ReadInt32();
			int elpWidth = br.ReadInt32();
			int elpBrushStyle = br.ReadInt32();
			int elpColor = br.ReadInt32();
			int elpHatch = br.ReadInt32();
			int elpNumEntries = br.ReadInt32();

            float[] pattern = new float[elpNumEntries];
            for (int i = 0; i < elpNumEntries; i++)
            {
                pattern[i] = ConvertSize(br.ReadInt32());   // elpStyleEntry
            }
            return CreatePen(elpColor, ConvertSize(elpWidth), elpPenStyle, pattern);
		}
        protected Pen CreatePen(int color, float width, int style, float[] pattern)
		{
			// create pen
			Color clr = (style == PS_NULL)
				? Color.Transparent
				: ColorTranslator.FromWin32(color);

			// set pen color and width
			Pen pen = new Pen(clr, width);

			// set pen syle
			switch (style & PS_STYLE_MASK)
			{
				case PS_DASH:
					pen.DashStyle = DashStyle.Dash;
					break;
				case PS_DOT:
					pen.DashStyle = DashStyle.Dot;
					break;
				case PS_DASHDOT:
					pen.DashStyle = DashStyle.DashDot;
					break;
				case PS_DASHDOTDOT:
					pen.DashStyle = DashStyle.DashDotDot;
					break;
                case PS_USERSTYLE:
                    Debug.Assert(pattern.Length > 0);
                    pen.DashStyle = DashStyle.Custom;
                    pen.DashPattern = pattern;
                    break;
            }

			// set pen end cap
			switch (style & PS_ENDCAP_MASK)
			{
				case PS_ENDCAP_ROUND:
					pen.StartCap = pen.EndCap = LineCap.Round;
					pen.DashCap = DashCap.Round;
					break;
				case PS_ENDCAP_SQUARE:
					pen.StartCap = pen.EndCap = LineCap.Square;
					pen.DashCap = DashCap.Flat;
					break;
				case PS_ENDCAP_FLAT:
					pen.StartCap = pen.EndCap = LineCap.Flat;
					pen.DashCap = DashCap.Flat;
					break;
			}

			// set join type
			switch (style & PS_JOIN_MASK)
			{
				case PS_JOIN_ROUND:
					pen.LineJoin = LineJoin.Round;
					break;
				case PS_JOIN_BEVEL:
					pen.LineJoin = LineJoin.Bevel;
					break;
				case PS_JOIN_MITER:
					pen.LineJoin = LineJoin.Miter;
					break;
			}

			// return new pen
			return pen;
		}
		protected MetaFont ReadFont(BinaryReader br)
		{
			return new MetaFont(this, br);
		}
		protected float[] ReadTransform(BinaryReader br)
		{
			// eM11; eM12; eM21; eM22; eDx; eDy; 
			float[] xform = new float[6];
			for (int i = 0; i < xform.Length; i++)
				xform[i] = br.ReadSingle();
			return xform;
		}
		#endregion

		//-----------------------------------------------------------------------------
		#region ** gdi object management utilities **
        private Hashtable _sysObjects;
        private object GetStockObject(int handle)
        {
            // create stock object table
            if (_sysObjects == null)
            {
                Font monospaceFont = new Font(FontFamily.GenericMonospace, 12);
                Font sansSerifFont = new Font(FontFamily.GenericSansSerif, 12);
                _sysObjects = new Hashtable();
                _sysObjects.Add(0, new SolidBrush(Color.White));         // WHITE_BRUSH
                _sysObjects.Add(1, new SolidBrush(Color.LightGray));     // LTGRAY_BRUSH
                _sysObjects.Add(2, new SolidBrush(Color.Gray));          // GRAY_BRUSH
                _sysObjects.Add(3, new SolidBrush(Color.DarkGray));      // DKGRAY_BRUSH
                _sysObjects.Add(4, new SolidBrush(Color.Black));         // BLACK_BRUSH
                _sysObjects.Add(5, new SolidBrush(Color.Transparent));   // NULL_BRUSH
                _sysObjects.Add(6, new Pen(Color.White));                // WHITE_PEN
                _sysObjects.Add(7, new Pen(Color.Black));                // BLACK_PEN
                _sysObjects.Add(8, new Pen(Color.Transparent));          // NULL_PEN
                _sysObjects.Add(10, monospaceFont);                      // OEM_FIXED_FONT
                _sysObjects.Add(11, monospaceFont);                      // ANSI_FIXED_FONT
                _sysObjects.Add(12, sansSerifFont);                      // ANSI_VAR_FONT
                _sysObjects.Add(13, sansSerifFont);                      // SYSTEM_FONT
                _sysObjects.Add(14, sansSerifFont);                      // DEVICE_DEFAULT_FONT
                _sysObjects.Add(15, sansSerifFont);                      // DEFAULT_PALETTE
                _sysObjects.Add(16, monospaceFont);                      // SYSTEM_FIXED_FONT
                _sysObjects.Add(17, sansSerifFont);                      // DEFAULT_GUI_FONT
                _sysObjects.Add(18, new SolidBrush(Color.White));        // DC_BRUSH
                _sysObjects.Add(19, new Pen(Color.White));               // DC_PEN
            }

            // return stock object (all stock handles have 0x80000000 set)
            if ((handle & 0x80000000) != 0)
            {
                handle = handle & 0x7fffffff;
                return _sysObjects[handle];
            }
            Debug.Assert(false, "invalid stock object request");
            return null;
        }
		#endregion

		//-----------------------------------------------------------------------------
		#region ** coordinate transformations (interop) **
		//
		// instead of keeping track of all the transformations in this code, we
		// use a parallel hdc (_hdcXform) and apply all transformations to it.
		//
		// we then use the _hdcXform dc to keep track of all state variables and
		// to perform coordinate mapping.
		//
		//private Graphics _gRefXform;
		private IntPtr   _hdcXform;
		internal IntPtr HdcXform { get { return _hdcXform; } }

		[DllImport("GDI32.DLL")] private static extern IntPtr SaveDC(IntPtr hdc);
		[DllImport("GDI32.DLL")] private static extern IntPtr RestoreDC(IntPtr hdc, int nSavedDC);
		[DllImport("GDI32.DLL")] private static extern int GetTextAlign(IntPtr hdc);
		[DllImport("GDI32.DLL")] private static extern int GetTextColor(IntPtr hdc);
		[DllImport("GDI32.DLL")] private static extern int GetBkColor(IntPtr hdc);
		[DllImport("GDI32.DLL")] private static extern int SetTextAlign(IntPtr hdc, int ta);
		[DllImport("GDI32.DLL")] private static extern int SetTextColor(IntPtr hdc, int color);
		[DllImport("GDI32.DLL")] private static extern int SetBkColor(IntPtr hdc, int color);

		protected virtual void EmfPlusSaveRestoreDC(EmfPlusRecordType recordType, BinaryReader br)
		{
			IntPtr hdc = _hdcXform;
			switch (recordType)
			{
					// save hdc state to restore later
				case EmfPlusRecordType.EmfSaveDC:

					// save text alignment and brushes (to restore later)
					SetTextAlign(hdc, _textAlign);
					SetTextColor(hdc, ColorTranslator.ToWin32(_textBrush.Color));
					SetBkColor(hdc,	  ColorTranslator.ToWin32(_bkBrush.Color));

					// save dc now
					SaveDC(hdc);

                    // push clipping rectangle into stack <<O43>>
					if (_clipStack == null) _clipStack = new Stack();
					_clipStack.Push(_clipRect);
					break;

					// restore hdc state and some pdf state
				case EmfPlusRecordType.EmfRestoreDC:

					// restore hdc state
					RestoreDC(hdc, br.ReadInt32());

					// restore text alignment and brushes
					_textAlign = GetTextAlign(hdc);
					_textBrush = new SolidBrush(ColorTranslator.FromWin32(GetTextColor(hdc)));
					_bkBrush   = new SolidBrush(ColorTranslator.FromWin32(GetBkColor(hdc)));

					// restore clip rect (should really restore it with GetClipBox instead...) <<O42>>
					if (_clipStack != null && _clipStack.Count > 0)
					{
						_clipRect  = (RectangleF)_clipStack.Pop();
					}
					else
					{
						_clipRect  = RectangleF.Empty;
					}
					// clear xform for Win9x transform <<OLEG>>
					_xform9x = null;
					break;
			}
		}

		[DllImport("GDI32.DLL")] private static extern IntPtr SetWorldTransform(IntPtr hdc, float[] xform);
		[DllImport("GDI32.DLL")] private static extern IntPtr ModifyWorldTransform(IntPtr hdc, float[] xform, int iMode);
		[DllImport("GDI32.DLL")] private static extern IntPtr SetGraphicsMode(IntPtr hdc, int iMode);
		[DllImport("GDI32.DLL")] private static extern int GetGraphicsMode(IntPtr hdc);

		private void EmfPlusWorldTransform(EmfPlusRecordType recordType, BinaryReader br)
		{
			IntPtr hdc = _hdcXform;
			float[] xform = ReadTransform(br);
			switch (recordType)
			{
				case EmfPlusRecordType.EmfSetWorldTransform:
					SetWorldTransform(hdc, xform);
					break;
				case EmfPlusRecordType.EmfModifyWorldTransform:
					ModifyWorldTransform(hdc, xform, br.ReadInt32());
					break;
			}

#if TRACETOFILE && DEBUG
			_swTrace.WriteLine("-- xform: {0} {1} {2} {3} {4} {5}\r\n" +
								"   dpi: {6} {7}",
				xform[0], xform[1], xform[2], xform[3], xform[4], xform[5],
				_dpi.X, _dpi.Y);
#endif

			// if the graphics mode is not set to GM_ADVANCED (Win9x)
			// then save the transform to apply later
			if (GetGraphicsMode(_hdcXform) != GM_ADVANCED)
			{
				_xform9x = xform;
			}
		}

		[DllImport("GDI32.DLL")] private static extern IntPtr ScaleViewportExtEx(IntPtr hdc, int nXnum, int nXdenom, int nYnum, int nYdenom, IntPtr lpZero);
		[DllImport("GDI32.DLL")] private static extern IntPtr ScaleWindowExtEx  (IntPtr hdc, int nXnum, int nXdenom, int nYnum, int nYdenom, IntPtr lpZero);
		[DllImport("GDI32.DLL")] private static extern IntPtr SetViewportExtEx  (IntPtr hdc, int nX, int nY, IntPtr lpZero);
		[DllImport("GDI32.DLL")] private static extern IntPtr SetWindowExtEx    (IntPtr hdc, int nX, int nY, IntPtr lpZero);
		[DllImport("GDI32.DLL")] private static extern IntPtr SetViewportOrgEx  (IntPtr hdc, int nX, int nY, IntPtr lpZero);
		[DllImport("GDI32.DLL")] private static extern IntPtr SetWindowOrgEx    (IntPtr hdc, int nX, int nY, IntPtr lpZero);

		private void EmfPlusSetWindowViewPort(EmfPlusRecordType recordType, BinaryReader br)
		{
			IntPtr hdc = _hdcXform;
			switch (recordType)
			{
				case EmfPlusRecordType.EmfScaleViewportExtEx:
				case EmfPlusRecordType.EmfScaleWindowExtEx:
					int xNum	= br.ReadInt32(); 
					int xDenom	= br.ReadInt32(); 
					int yNum	= br.ReadInt32(); 
					int yDenom	= br.ReadInt32(); 
					if (recordType == EmfPlusRecordType.EmfScaleViewportExtEx)
						ScaleViewportExtEx(hdc, xNum, xDenom, yNum, yDenom, IntPtr.Zero);
					else
						ScaleWindowExtEx(hdc, xNum, xDenom, yNum, yDenom, IntPtr.Zero);
					break;

				case EmfPlusRecordType.EmfSetViewportExtEx:
				case EmfPlusRecordType.EmfSetWindowExtEx:
					int extentX = br.ReadInt32();
					int extentY	= br.ReadInt32();
					if (recordType == EmfPlusRecordType.EmfSetViewportExtEx)
						SetViewportExtEx(hdc, extentX, extentY, IntPtr.Zero);
					else
						SetWindowExtEx(hdc, extentX, extentY, IntPtr.Zero);
					break;

				case EmfPlusRecordType.EmfSetViewportOrgEx:
				case EmfPlusRecordType.EmfSetWindowOrgEx:
					int originX = br.ReadInt32();
					int originY	= br.ReadInt32();
					if (recordType == EmfPlusRecordType.EmfSetViewportOrgEx)
						SetViewportOrgEx(hdc, originX, originY, IntPtr.Zero);
					else
						SetWindowOrgEx(hdc, originX, originY, IntPtr.Zero);
					break;
			}
		}

		[DllImport("GDI32.DLL")] private static extern IntPtr SetMapMode(IntPtr hdc, int nMapMode);
		private void EmfSetMapMode(BinaryReader br)
		{
			SetMapMode(_hdcXform, br.ReadInt32());
		}

		// convert document dimensions into Points
		[DllImport("GDI32.DLL")] private static extern IntPtr LPtoDP(IntPtr hdc, ref Point lpPoint, int nCount);
		internal PointF ConvertPoint(PointF ptf)
		{
			//Point pt = Point.Truncate(ptf);
			Point pt = Point.Round(ptf);
			LPtoDP(_hdcXform, ref pt, 1);

			ptf.X = (float)pt.X;
			ptf.Y = (float)pt.Y;

			// add Win9x transform if the hdc doesn't support it <<OLEG>>
			if (_xform9x != null)
			{
				ptf.X = ptf.X * _xform9x[0] + ptf.X * _xform9x[1] + _xform9x[4];
				ptf.Y = ptf.X * _xform9x[2] + ptf.Y * _xform9x[3] + _xform9x[5];
			}

			// convert to points
			ptf.X = ptf.X * 72f / _dpi.X * this.XFactor;
			ptf.Y = ptf.Y * 72f / _dpi.Y * this.YFactor;
			return ptf;
		}
		internal SizeF ConvertSize(SizeF szf)
		{
			PointF org = ConvertPoint(PointF.Empty);
			PointF pt  = ConvertPoint(new PointF(szf.Width, szf.Height));
			return new SizeF(Math.Abs(pt.X - org.X), Math.Abs(pt.Y - org.Y));
		}
		internal float ConvertSize(float size)
		{
			//return size * 72f / _dpi.Y;
			PointF org = ConvertPoint(PointF.Empty);
			PointF pt  = ConvertPoint(new PointF(size, size));
			return (float)Math.Abs(pt.Y - org.Y);
		}
		#endregion

        //-----------------------------------------------------------------------------
        #region ** stretch DIB (interop)

        [DllImport("GDI32.DLL")]
        internal static extern int SetStretchBltMode(
            IntPtr hdc,                   // handle to DC
            int iStretchMode              // bitmap stretching mode
            );

        [DllImport("GDI32.DLL")]
        internal static extern int StretchDIBits(
            IntPtr hdc,         // handle to DC
            int XDest,          // x-coord of destination upper-left corner
            int YDest,          // y-coord of destination upper-left corner
            int nDestWidth,     // width of destination rectangle
            int nDestHeight,    // height of destination rectangle
            int XSrc,           // x-coord of source upper-left corner
            int YSrc,           // y-coord of source upper-left corner
            int nSrcWidth,      // width of source rectangle
            int nSrcHeight,     // height of source rectangle
            IntPtr lpBits,      // bitmap bits
            IntPtr lpBitsInfo,  // bitmap data
            uint iUsage,        // usage options
            int dwRop           // raster operation code
            );

        #endregion

        //-----------------------------------------------------------------------------
        #region ** logical font structure (interop) **

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        internal class LOGFONT
        {
            internal int lfHeight;
            internal int lfWidth;
            internal int lfEscapement;
            internal int lfOrientation;
            internal int lfWeight;
            internal byte lfItalic;
            internal byte lfUnderline;
            internal byte lfStrikeOut;
            internal byte lfCharSet;
            internal byte lfOutPrecision;
            internal byte lfClipPrecision;
            internal byte lfQuality;
            internal byte lfPitchAndFamily;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = LF_FACESIZE)]
            internal string lfFaceName;
            internal const int LF_FACESIZE = 32;
        };

        #endregion

		//-----------------------------------------------------------------------------
		#region ** subclasses (MetaFont) **

		/// data wrapper for LOGFONT
		internal class MetaFont
        {
            //-------------------------------------------------------------------------
            #region ** fields/ctor

            private Font _font;
			private float _angle;
            private float _widthCoeff;
            private static string _systemFaceName;

			internal MetaFont(MetafileConverterBase mf, BinaryReader br)
			{
				// read logfont info (LOGFONT data)
				LOGFONT lf          = new LOGFONT();
				lf.lfHeight			= br.ReadInt32();
				lf.lfWidth			= br.ReadInt32();
				lf.lfEscapement		= br.ReadInt32();
				lf.lfOrientation	= br.ReadInt32();
				lf.lfWeight			= br.ReadInt32();
				lf.lfItalic			= br.ReadByte();
				lf.lfUnderline		= br.ReadByte();
				lf.lfStrikeOut		= br.ReadByte();
				lf.lfCharSet		= br.ReadByte();
				lf.lfOutPrecision	= br.ReadByte();
				lf.lfClipPrecision  = br.ReadByte();
				lf.lfQuality		= br.ReadByte();
				lf.lfPitchAndFamily = br.ReadByte();
				byte[] faceName	    = br.ReadBytes(LOGFONT.LF_FACESIZE * 2); // 32 - LF_FACESIZE
				lf.lfFaceName		= Encoding.Unicode.GetString(faceName, 0, faceName.Length);

				if (mf._knownFont != null)
				{
					// if font already known in module where the file has been created,
					// for support custom user fonts not registered in system <<OLEG>>
					_font = mf._knownFont;

					// reset known font
					mf._knownFont = null;
				}
				else
				{
                    // trim font name
                    int pos = lf.lfFaceName.IndexOf((char)0);
                    if (pos > -1)
                    {
                        lf.lfFaceName = lf.lfFaceName.Substring(0, pos);
                    }

                    // build gdi font to get size and style (name doesn't work...) <<B21>>
					// note: "FromLogFont" only works for TrueType fonts <<B26>>
					try
					{
						using (Font f = Font.FromLogFont(lf, mf._hdcXform))
						{
							// asking for a "System" font throws an exception in Font ctor, which
							// is caught in ctor but slows down operation. Cache the name instead.
							bool isSystemFace = (lf.lfFaceName == "System");
							if (isSystemFace && _systemFaceName != null)
							{
								lf.lfFaceName = _systemFaceName;
							}
							float fontSize = mf.ConvertSize(f.Size);
							char[] trimChars = { '\0', ' ' };
							_font = new Font(lf.lfFaceName.TrimEnd(trimChars), fontSize, f.Style, f.Unit, f.GdiCharSet, f.GdiVerticalFont);

							// cache system font name
							if (isSystemFace && _font != null)
							{
								_systemFaceName = _font.Name;
							}
						}
					}
					catch { }
				}
				
				// if that failed, fall back and build gdi font without "FromLogFont"
				if (_font == null)
				{
					float size = mf.ConvertSize(Math.Abs(lf.lfHeight));
					if (size <= 0.5f) size = 0.5f;
					FontStyle style = FontStyle.Regular;
					if (lf.lfWeight >= FW_BOLD)  style |= FontStyle.Bold;
					if (lf.lfItalic    != 0)	 style |= FontStyle.Italic;
					if (lf.lfUnderline != 0)	 style |= FontStyle.Underline;
					if (lf.lfStrikeOut != 0)	 style |= FontStyle.Strikeout;
					_font = new Font(lf.lfFaceName, size, style);
				}

				// reverse font angle if y axis is reversed
				if (lf.lfEscapement != 0)
				{
					PointF p0 = mf.ConvertPoint(PointF.Empty);
					PointF p1 = mf.ConvertPoint(new PointF(0, 100));
					if (p1.Y < p0.Y)
						lf.lfEscapement = -lf.lfEscapement;
				}

                // save font width coefficient
                _widthCoeff = 1.0f;
                if (lf.lfWidth != 0 && lf.lfHeight != 0)
                {
                    _widthCoeff = (float)lf.lfWidth / lf.lfHeight;
                }

				// save font angle
				_angle = lf.lfEscapement / 10;
			}
			#endregion

            //-------------------------------------------------------------------------
            #region ** properties/operator

			/// <summary>
			/// Get GDI+ approximate font for this meta font. 
			/// </summary>
			internal Font Font
			{
				get { return _font; }
			}

			/// <summary>
			/// Get angle for this meta font.
			/// </summary>
			internal float FontAngle 
			{
				get { return _angle; }
			}

            /// <summary>
            /// Get width coefficient for this meta font.
            /// </summary>
            internal float WidthCoeff
            {
                get { return _widthCoeff; }
            }

            /// <summary>
			/// Convert implicitly to GDI+ font.
			/// </summary>
			/// <param name="mf">The mate font object.</param>
			/// <returns>GDI+ approximate font for meta font.</returns>
			public static implicit operator Font(MetaFont mf) 
			{
				return mf.Font;
			}

			#endregion
		}
		#endregion
    }
}