//----------------------------------------------------------------------------
// C1\Win\C1Report\ReferenceDC.cs
//
// This class creates a reference dc, based on a good-resolution printer or 
// on the screen dc, and provides methods for creating metafiles with the 
// right size accounting for discrepancies between image resolution (actual dpi)
// and screen hdc resolution (logical).
//
// Creating a good-quality reference dc can be pretty slow because it may 
// require enumerating and checking the resolution of several printers, some
// of which may be connected via network. Classes that need to create several
// metafiles should instantiate one ReferenceDC and reuse it as many times
// as possible.
//
// This class is used in C1Report and C1Pdf (and maybe in other places?).
//
// Copyright (C) 2004 - 2005 ComponentOne LLC
//----------------------------------------------------------------------------
// Status	Date		By			Comments
// Created	Feb 2004	Bernardo	-
// Changed	Nov 2004	Bernardo	Some cleanup
// Changed	Jan 2005	Bernardo	More cleanup, moved to C1.Util
//----------------------------------------------------------------------------
using System;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace C1.Util
{
	/// <summary>
	/// Summary description for ReferenceDC.
	/// </summary>
	internal class ReferenceDC : IDisposable
	{
		//---------------------------------------------------------------------------
		#region ** fields

		IntPtr   _hdc;
		PointF   _dpi;
		bool	 _tryPrinter;
		#endregion

		//---------------------------------------------------------------------------
		#region ** ctors

		internal ReferenceDC(bool tryPrinter) 
		{
			// don't create the dc until it is actually used
			_tryPrinter = tryPrinter;
            _hdc = IntPtr.Zero;
            _dpi = PointF.Empty;
		}
		internal ReferenceDC() : this(true) {}
		#endregion

		//---------------------------------------------------------------------------
		#region ** IDisposable

		public void Dispose()
		{
            try
            {
                DisposeReferenceDC();
            }
            catch { } // no permissions to call unmanaged code?
		}

		#endregion

		//---------------------------------------------------------------------------
		#region ** object model

		internal IntPtr Hdc
		{
			get 
			{
                if (_hdc == IntPtr.Zero)
                {
                    CreateReferenceDC();
                }
				return _hdc; 
			}
		}
		internal PointF Dpi
		{
			get 
			{
                if (_hdc == IntPtr.Zero)
                {
                    CreateReferenceDC();
                }
				return _dpi; 
			}
		}
		#endregion

		//---------------------------------------------------------------------------
		#region ** metafile creators

		public Metafile CreateMetafile(SizeF sz)
		{
			return CreateMetafile(null, sz, MetafileFrameUnit.Point, EmfType.EmfOnly);
		}
		public Metafile CreateMetafile(SizeF sz, MetafileFrameUnit unit, EmfType type)
		{
			return CreateMetafile(null, sz, unit, type);
		}
		public Metafile CreateMetafile(Stream stream, SizeF sz, MetafileFrameUnit unit, EmfType type)
		{
            // create frame rectangle
            RectangleF rc = new RectangleF(Point.Empty, sz);
#if true
            // create metafile
            return (stream != null)
                ? new Metafile(stream, Hdc, rc, unit, type)
                : new Metafile(Hdc, rc, unit, type);
#else
			// convert frame into pixels
			switch (unit)
			{
				case MetafileFrameUnit.Inch:
					rc.Width  *= Dpi.X;
					rc.Height *= Dpi.Y;
					break;
				case MetafileFrameUnit.Point:
					rc.Width  *= Dpi.X / 72f;
					rc.Height *= Dpi.Y / 72f;
					break;
				case MetafileFrameUnit.Document:
					rc.Width  *= 300;
					rc.Height *= 300;
					break;
				case MetafileFrameUnit.Millimeter:
					rc.Width  *= Dpi.X * 25.4f;
					rc.Height *= Dpi.Y * 25.4f;
					break;
				case MetafileFrameUnit.GdiCompatible:
					rc.Width  *= Dpi.X * 2540f;
					rc.Height *= Dpi.Y * 2540f;
					break;
				case MetafileFrameUnit.Pixel:
					break;
			}

			// create metafile
			Metafile meta = (stream != null)
				? new Metafile(stream, Hdc, rc, MetafileFrameUnit.Pixel, type)
				: new Metafile(Hdc, rc, MetafileFrameUnit.Pixel, type);

			// done
			return meta;
#endif
		}
		#endregion

		//---------------------------------------------------------------------------
		#region ** hdc creators (screen/printer)

		private void CreateReferenceDC()
		{
			// try getting a printer dc
			try
			{
				// but only if requested and there are printers installed
				if (_tryPrinter && PrinterSettings.InstalledPrinters.Count > 0)
				{
					CreatePrinterDC();
				}
			}
			catch {}

			// failed to get printer, use screen
			if (_hdc == IntPtr.Zero)
			{
				CreateScreenDC();
			}

            // done
			Debug.WriteLine("** refdc created");
		}
        private void DisposeReferenceDC()
        {
            if (_hdc != IntPtr.Zero)
            {
                DeleteDC(_hdc);
                _hdc = IntPtr.Zero;
            }
        }
		private void CreateScreenDC()
		{
			// create dc as usual
			_hdc = CreateIC("DISPLAY", null, IntPtr.Zero, IntPtr.Zero);

			// get logical resolution (usually 96 dpi)
			_dpi.X = GetDeviceCaps(_hdc, LOGPIXELSX);
			_dpi.Y = GetDeviceCaps(_hdc, LOGPIXELSY);

			// get physical resolution, used to calculate metafile sizes <<B175>>
            // bad bad idea, don't do it...
			//_dpi.X = GetDeviceCaps(_hdc, HORZRES) / (float)GetDeviceCaps(_hdc, HORZSIZE) * 25.4f;
			//_dpi.Y = GetDeviceCaps(_hdc, VERTRES) / (float)GetDeviceCaps(_hdc, VERTSIZE) * 25.4f;
		}
        private void CreatePrinterDC()
        {
            PrinterSettings ps = new PrinterSettings();
            if (ps != null)
            {
                // try current default printer (so we don't have to scan installed printers)
                if (TryPrinter(ps, ps.PrinterName))
                {
                    return;
                }

                // current is bad, look for a better one
                foreach (string printerName in PrinterSettings.InstalledPrinters)
                {
                    if (TryPrinter(ps, printerName))
                    {
                        return;
                    }
                }
            }
        }
        private bool TryPrinter(PrinterSettings ps, string printerName)
        {
            // get printer name
            ps.PrinterName = printerName;
            if (!ps.IsValid) return false;

            // get 'promised' resolution, skip anything below 300 dpi
            PrinterResolution res = ps.DefaultPageSettings.PrinterResolution;
            if (res.X != res.Y || res.X < 300) return false;

            // this one looks good, create reference graphics
            IntPtr hdc = CreateIC("WINSPOOL", printerName, IntPtr.Zero, IntPtr.Zero);
            PointF dpi = new PointF(GetDeviceCaps(hdc, LOGPIXELSX), GetDeviceCaps(hdc, LOGPIXELSY));

            // if the driver can't do bitblt, or lied about its resolution, keep looking 
            // (this avoids selecting Generic / Text Only printers) <<B163>>
            int rasterCaps = GetDeviceCaps(hdc, RASTERCAPS);
            if ((rasterCaps & RC_BITBLT) == 0 || dpi.X != res.X || dpi.Y != res.Y)
            {
                DeleteDC(hdc);
                return false;
            }

            // all seems fine, we're done
            _dpi = dpi;
            _hdc = hdc;
            return true;
		}
		#endregion

		//---------------------------------------------------------------------------
		#region ** Win32
		
		private const int 
			LOGPIXELSX	= 88,		// logical pixels/inch in X
			LOGPIXELSY	= 90,		// logical pixels/inch in Y
			HORZSIZE	= 4,		// horizontal size in millimeters
			VERTSIZE	= 6,		// vertical size in millimeters
			HORZRES		= 8,		// horizontal width in pixels
			VERTRES		= 10,		// vertical height in pixels
            PHYSICALWIDTH = 110,    // physical width in device units
            PHYSICALHEIGHT = 111,   // physical height in device units
            RASTERCAPS = 38,		// raster capabilities
			RC_BITBLT	= 1;		// standard bitblt
		[DllImport("gdi32.dll")] static private extern IntPtr CreateIC(string driverName, string deviceName, IntPtr output, IntPtr initData);
		[DllImport("gdi32.dll")] static private extern IntPtr DeleteDC(IntPtr dc);
		[DllImport("gdi32.dll")] static private extern int GetDeviceCaps(IntPtr dc, int index);
	}
	#endregion
}