/*----------------------------------------------------------------------------
 * FontSubSetBase
 * 
 * Base code for subsets a True Type font.
 * 
 *----------------------------------------------------------------------------
 * Copyright (C) 2003 ComponentOne LLC
 *----------------------------------------------------------------------------
 * Status			Date			By						Comments
 *----------------------------------------------------------------------------
 * Created			October, 2003	Oleg				-
 *----------------------------------------------------------------------------
 */
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Text;
using System.Collections;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace C1.Util
{
	/// <summary>
	/// Summary description for FontSubSet.
	/// </summary>
	internal abstract class FontSubSetBase : IComparer
	{
        //-----------------------------------------------------------------------------
        #region ** fields & ctors

        // constants
		private static readonly string[] s_tableNamesSimple =
			{ "cvt ", "fpgm", "gasp", "glyf", "head", "hhea", "hmtx", "loca", "maxp", "name", "prep" };
//		private static readonly string[] s_tableNamesSimple =
//          { "cmap", "cvt ", "fpgm", "gasp", "glyf", "head", "hhea", "hmtx", "loca", "maxp", "name", "post", "prep" };
        private static readonly string[] s_tableNamesCMap =
            { "OS/2", "VDMX", "cmap", "cvt ", "fpgm", "gasp", "glyf", "head", "hhea", "hmtx", "loca", "maxp", "name", "prep" };
//        private static readonly string[] s_tableNamesCMap =
//			{ "cmap", "cvt ", "fpgm", "gasp", "glyf", "head", "hhea", "hmtx", "loca", "maxp", "prep" };
		private static readonly int[] s_entrySelectors =
			{ 0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4 };

        // VNPDF000108 (was be 300, but it less for Unicode fonts)
		private static readonly int s_ttcMaxOffset = 0x600;

        // SimSun font sub set
        private static FontSubSetBase s_fssSimSun;

        // list of true type font families for font substitutions
        private static ArrayList s_alFontFamilies;

		// TrueType font directories with int[3] values (checksum, position, length)
		internal Hashtable _directory;

		// the file name
		internal string _fontName;

        // the file name
        internal string _familyName;

        // the content of table 'hmtx' normalized to 1000 units
		private int[] _glyphWidths;

		// the content of table 'cmap', encoding 1.0 & encoding 3.1
		private Hashtable _cmap_1_0;
		private Hashtable _cmap_3_1;
		internal Hashtable _tags;

		// the content of table 'kern', the value is the amount of kerning in
		// normalized 1000 units as an integer, this value is usually negative
		private Hashtable _kerning;

        // parameters
		private bool		_fontSpecific;
		private int			_numberOfHMetrics;
		private int			_unitsPerEm;
		private int			_style;              // bold or italic style
		private byte[]		_buffer;             // the output font buffer
        private int         _familyClass;
        private bool        _fixedPitch;
        private float       _italicAngle;
        internal byte[]     _panose;             // the PANOSE structure

		// ...
		private bool		_locaShortTable;
		private int[]		_locaTable;
		private int			_tableGlyphOffset;
		private int[]		_newLocaTable;
		private byte[]		_newLocaTableOut;
		private byte[]		_newGlyfTable;
		private int			_glyfTableRealSize;
		private int			_locaTableRealSize;

        //private byte[]      _fontData;
        //private PdfFont     _font;
		private int			_offset;
		internal ArrayList	_glyphsInList;
        internal Hashtable  _glyphsUsed;

		internal bool		_used;
		internal bool       _embedded;
		internal bool		_includeCMap;
		internal bool		_invalid;

        private ushort      _fsType;                // allowed flags

		internal bool		_dbcs;                  // for support Unicode fonts

		// ** ctors

		/// <summary>
		/// Base initialization font subset for existing font.
		/// </summary>
		public FontSubSetBase()
		{
		}

        /// <summary>
        /// The common initialization code.
        /// </summary>
        /// <param name="cmap">The flag for include CMap table of this font.</param>
        protected virtual void Init(bool cmap)
        {
            // fields initialization
            Debug.Assert(this.FontData != null && this.FontData.Length > 32);
            _includeCMap = cmap;
            _tags = new Hashtable();
            _glyphsInList = new ArrayList();
            _glyphsUsed = new Hashtable();

            // fix for symbol and Unicode fonts <<IP49>>
            //_dbcs = (_font.GdiCharSet >= 128 || _font.IsSymbol);

            // font directory and headers
            ReadDirectory();
            ReadHeaders();

            // read font names
            bool correctName = false;
            bool correctFamily = false;
            FontNameInfo[] fontNames;
            FontNameInfo[] familyNames;
            ReadFontNames(out fontNames, out familyNames);
            foreach (FontNameInfo info in fontNames)
            {
                if (info.Platform == 1 && info.Encoding == 0 && info.Language == 0)
                {
                    // correct font name
                    _fontName = info.Name;
                    correctName = true;
                }
                else if (!correctName && info.Platform == 3 && info.Encoding == 1 && info.Language == 1033)
                {
                    // alternative font name
                    _fontName = info.Name;
                }
            }
            foreach (FontNameInfo info in familyNames)
            {
                if (info.Platform == 1 && info.Encoding == 0 && info.Language == 0)
                {
                    // correct family name
                    _familyName = info.Name;
                    correctFamily = true;
                }
                else if (!correctFamily && info.Platform == 3 && info.Encoding == 1 && info.Language == 1033)
                {
                    // alternative family name
                    _familyName = info.Name;
                }
            }
        }

		#endregion

        //-----------------------------------------------------------------------------
        #region ** IComparer interface

        int IComparer.Compare(object x, object y)
        {
            int m1 = ((int[])x)[0];
            int m2 = ((int[])y)[0];
            if (m1 < m2) return -1;
            if (m1 > m2) return 1;
            return 0;
        }

        #endregion

		//-----------------------------------------------------------------------------
		#region ** properties

		/// get output font buffer for used glyphs only.
		internal virtual byte[] OutputBuffer
		{
			get
			{
                if (_buffer == null)
                {
                    // embed allowed test
                    if (!this.CanEmbed)
                    {
                        throw new NotSupportedException("Embedding is not allowed for this font.");
                    }

                    // subset allowed test
                    if (!this.CanSubset)
                    {
                        return this.FontData;
                    }

                    // create buffer
                    ReadLoca();
                    FlatGlyphs();
                    NewLocaGlyphTables();
                    WriteBuffer();
                }

                // done
                return _buffer;
			}
		}

        public string Name
        {
            get { return _fontName; }
        }
        public string FamilyName
        {
            get { return _familyName; }
        }
        public bool CanEmbed
        {
            // NOTE: the 0x0200 bit indicates "embedding of bitmap chars only".
            get { return _fsType != 0x0002 && (_fsType & 0x0200) == 0; }
        }
        public bool MustObfuscate
        {
            get { return (_fsType & (0x0004 | 0x0008 | 0x0100 | 0x0200)) != 0; }
        }
        public bool CanSubset
        {
            get { return (_fsType & 0x0100) == 0; }
        }
        public bool MustRestrict
        {
            get { return (_fsType & 0x0004) != 0; }
        }
		internal bool IsBold
		{
			get { return (_style & 0x01) != 0; }
		}
		internal bool IsItalic
		{
			get { return (_style & 0x02) != 0; }
		}

        internal bool IsUsed
        {
            get { return _used; }
        }
        internal bool IsEmbedded
        {
            get { return _embedded; }
        }
        internal bool HasCMap
        {
            get { return _includeCMap; }
        }
        internal bool IsInvalid
        {
            get { return _invalid; }
        }
        internal byte[] Buffer
        {
            get { return _buffer; }
        }

        // the angle for italic text
        internal float ItalicAngle
        {
            get { return _italicAngle; }
        }

        // the flag of the fixed pitch text
        internal bool IsFixedPitch
        {
            get { return _fixedPitch; }
        }

        // the symbolic flag of the font
        internal bool IsFontSpecific
        {
            get { return _fontSpecific; }
        }

        // binary font data (for override in children)
        internal abstract byte[] FontData
        {
            get;
        }

        #endregion

        //-----------------------------------------------------------------------------
		#region ** methods

		// gets a glyph width in normalized 1000 units
        internal int GetGlyphWidth(char ch)
        {
            int[] metrics = GetMetrics(ch);
            if (metrics == null)
                return GetGlyphWidth(0);
            return GetGlyphWidth(metrics[0]);
        }
        internal int GetGlyphWidth(int glyph)
		{
			if (_glyphWidths == null)
				ReadGlyphWidths();
			if (glyph >= _glyphWidths.Length)
				glyph = _glyphWidths.Length - 1;
			return _glyphWidths[glyph];
		}

        // gets a kerning in normalized 1000 units
        internal int GetKerning(char ch1, char ch2)
        {
            int[] metrics = GetMetrics(ch1);
            if (metrics == null)
                return 0;
            int c1 = metrics[0];
            metrics = GetMetrics(ch2);
            if (metrics == null)
                return 0;
            int c2 = metrics[0];
            int code = (c1 << 16) + c2;
            if (_kerning == null)
                ReadKerning();
            if (_kerning != null && _kerning.Contains(code))
                return (int)_kerning[code];
            return 0;
        }

		// gets the glyph index and metrics for a character
		// returns an integer array with {glyph index, width}
		internal int[] GetMetrics(int code) 
		{
			if (_cmap_1_0 == null && _cmap_3_1 == null)
				ReadCMap();

			if ((!_fontSpecific || _cmap_1_0 == null) && _cmap_3_1 != null) 
				return (int[])_cmap_3_1[code];

			if (_cmap_1_0 != null)
			{
				int[] ret = (int[])_cmap_1_0[code];
				if (ret == null && _fontSpecific && code < 0xff)
					return (int[])_cmap_1_0[code | 0xf000];
				return ret;
			}

			return null;
		}

		// gets the char for glyph index, returns character for glyph
		internal char GetCharForGlyph(int glyphIndex)
		{
			if (_cmap_1_0 == null && _cmap_3_1 == null)
				ReadCMap();

			if ((!_fontSpecific || _cmap_1_0 == null) && _cmap_3_1 != null) 
				return GetCharForGlyph(glyphIndex, _cmap_3_1);

			if (_cmap_1_0 != null)
				return GetCharForGlyph(glyphIndex, _cmap_1_0);

			return (char)glyphIndex;
		}
		private char GetCharForGlyph(int glyphIndex, Hashtable cmap)
		{
			foreach (int code in cmap.Keys)
			{
				int[] map = (int[])cmap[code];
				if (map[0] == glyphIndex)
					return (char)code;
			}
			return (char)glyphIndex;
		}

		#endregion

		//-----------------------------------------------------------------------------
		#region ** object model

        // return byte array for glyphs
        protected byte[] GetBytes(char[] glyphs)
        {
            byte[] buffer = new byte[2 * glyphs.Length];
            int ptr = 0;
            for (int i = 0; i < glyphs.Length; i++)
            {
                char ch = glyphs[i];
                buffer[ptr++] = (byte)(ch >> 8);
                buffer[ptr++] = (byte)(ch & 0xff);
            }
            return buffer;
        }

        /// <summary>
        /// splits text into array of strings with "known" chars present in the current font (even strings 0, 2, ...)
        /// and strings with "unknown" chars - NOT present in the current font (odd strings 1, 3, ...)
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        internal virtual string[] SplitByMetrics(string text)
        {
            int start = 0;
            bool unknown = false;
            ArrayList al = new ArrayList();

            for (int i = 0; i < text.Length; i++)
            {
                int charCode  = (char)text[i];
                int[] metrics = GetMetrics(charCode);
                if (metrics == null && !unknown && charCode > 0xff)
                {
                    al.Add(text.Substring(start, i - start));
                    unknown = true;
                    start = i;
                }
                if (metrics != null && unknown)
                {
                    al.Add(text.Substring(start, i - start));
                    unknown = false;
                    start = i;
                }
            }
            al.Add(text.Substring(start));

			return (string[])al.ToArray(typeof(string));
        }

        /// <summary>
        /// Counts the number of chars in <paramref name="text"/> that do have
        /// corresponding glyphs in this font.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
  		internal int GetCountMetrics(string text)
		{
			int count = 0;
			for (int i = 0; i < text.Length; i++)
			{
				if (GetMetrics((char)text[i]) != null)
					count++;
			}
			return count;
		}

        // calculate diff panose index
        internal int GetDiffPanoseIndex(FontSubSetBase fss)
        {
            int index = 0;

            if (_panose != null && fss._panose != null && _panose.Length >= fss._panose.Length)
            {
                for (int i = 0; i < _panose.Length; i++)
                {
                    if (i == 0)
                    {
                        // PANOSE "Family Kind" gets higher weight
                        index += Math.Abs(_panose[i] - fss._panose[i]) * 256;
                    }
                    else
                    {
                        // subtract others PANOSE attributes
                        index += Math.Abs(_panose[i] - fss._panose[i]);
                    }
                }
            }

            // done
            return index;
        }

		#endregion

		//-----------------------------------------------------------------------------
		#region ** statics

        // gets or sets SimSun font sub set
        internal static FontSubSetBase SimSunSubSet
        {
            get { return s_fssSimSun; }
            set { s_fssSimSun = value; }
        }

        // gets unique subset prefix to be added to the font name when embedded and subset.
        internal static string Prefix
		{
			get
			{
				Random random = new Random();
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < 6; i++)
					sb.Append((char)(random.Next(26) + 'A'));
				sb.Append('+');
				return sb.ToString();
			}
		}

        // sharing for text (need for Arabic texts)
        internal static char[] Shaping(string text)
        {
            int[] kerns = null;
            return FontSubSetArabic.Shaping(text, ref kerns);
        }
        internal static char[] Shaping(string text, ref int[] kerns)
        {
            return FontSubSetArabic.Shaping(text, ref kerns);
        }

        /// <summary>
        /// Initializes the static array of installed "standard" font families
        /// that may be used to search for substitute fonts, if it's not been
        /// initialized yet.
        /// </summary>
        private static void s_InitAlternativeFontFamilies()
        {
            lock (typeof(FontSubSetBase))
            {
                // first initialization
                if (s_alFontFamilies == null)
                {
                    // create array
                    s_alFontFamilies = new ArrayList();

                    // for installed fonts
                    InstalledFontCollection ifc = new InstalledFontCollection();
                    foreach (FontFamily ff in ifc.Families)
                    {
                        // using only Unicode fonts for substitution
                        switch (ff.Name)
                        {
                            case "MS UI Gothic":
                            case "MS Mincho":
                            case "Arial Unicode MS":
                            case "Batang":
                            case "Gulim":
                            case "Microsoft YaHei":
                            case "Microsoft JhengHei":
                            case "MingLiU":
                            case "SimHei":
                            case "SimSun":
                                s_alFontFamilies.Add(ff);
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets array of fonts for substitution of symbols for the current font name.
        /// </summary>
        /// <param name="fontName">The current font name.</param>
        /// <returns>The array of font names for substitution.</returns>
        /// <param name="bold">The bold flag of the font.</param>
        /// <param name="italic">The italic flag of the font.</param>
        internal static string[] GetFontSubstitutions(string fontName, bool bold, bool italic)
        {
            s_InitAlternativeFontFamilies();

            // create font style
            FontStyle style = FontStyle.Regular;
            if (bold)
                style |= FontStyle.Bold;
            if (italic)
                style |= FontStyle.Italic;

            // font filters
            ArrayList al = new ArrayList();
            foreach (FontFamily ff in s_alFontFamilies)
            {
                // is available style
                if (!ff.IsStyleAvailable(style))
                    continue;

                // is current font
                if (ff.Name.Equals(fontName))
                    continue;

                // possible font name
                al.Add(ff.Name);
            }

            // done
            return (string[])al.ToArray(typeof(string));
        }

        /// <summary>
        /// Gets array of fonts for substitution of symbols for the current font.
        /// </summary>
        /// <param name="font">The current <see cref="Font"/> object.</param>
        /// <returns>The array of <see cref="Font"/> objects for substitution (it is necessary disposing).</returns>
        internal static Font[] GetFontSubstitutions(Font font)
        {
            s_InitAlternativeFontFamilies();

            // font filters
            ArrayList al = new ArrayList();
            foreach (FontFamily ff in s_alFontFamilies)
            {
                // is available style
                if (!ff.IsStyleAvailable(font.Style))
                    continue;

                // is current font
                if (ff.Name.Equals(font.Name))
                    continue;

                // possible font
                al.Add(new Font(ff, font.Size, font.Style, font.Unit));
            }

            // done
            return (Font[])al.ToArray(typeof(Font));
        }

        /// <summary>
        /// Search best alternative for text in font subset array.
        /// </summary>
        /// <param name="text">The text for substitution.</param>
        /// <param name="current">The current font subset.</param>
        /// <param name="substitutions">The font subset array.</param>
        /// <returns>The index of best alternative font (font subset).</returns>
        internal static int GetAlternativeIndex(string text, FontSubSetBase current, FontSubSetBase[] substitutions)
        {
            // initialization
            int panoseIndex = int.MaxValue;
            int bestIndex = -1;
 
            // find best alternative font
            for (int i = 0; i < substitutions.Length; i++)
            {
                // calculate diff panose index and metrics for glyphs for best alternative font
                FontSubSetBase fss = substitutions[i];

                // has glyphs for text
                int count = fss.GetCountMetrics(text);
                if (count > 0)
                {
                    // if already best not found or equal fixed pitch attribute
                    if (bestIndex == -1 || current.IsFixedPitch == fss.IsFixedPitch)
                    {
                        // diff panose and metrics index
                        int index = current.GetDiffPanoseIndex(fss);
                        index += ((text.Length - count) * 16384) / text.Length;

                        // compare index
                        if (index < panoseIndex)
                        {
                            panoseIndex = index;
                            bestIndex = i;
                            if (index == 0)
                            {
                                // best!
                                break;
                            }
                        }
                    }
                }
            }

            // done
            return bestIndex;
        }

		#endregion

		//-----------------------------------------------------------------------------
		#region ** implementation

		#region ** read directory

        protected void ReadDirectory()
		{
			// read TrueType font file
			using (MemoryStream ms = new MemoryStream(this.FontData))
			using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
			{
				int[] locations;
				_directory = new Hashtable();
				//br.BaseStream.Position = _dirOffset;

				// check that font is TrueType
				int id = br.ReadInt32();
                if (id != 0x10000)
                {
                    throw new Exception("Font is not a TrueType font.");
                }

				// read font info tables
				int tables = br.ReadUInt16();
				br.BaseStream.Position += 6;
				for (int i = 0; i < tables; i++) 
				{
					string tag = ReadFontString(br, 4);
					locations = new int[3];
					locations[0] = br.ReadInt32();      // checksum
					locations[1] = br.ReadInt32();      // offset
					locations[2] = br.ReadInt32();      // length
					_directory.Add(tag, locations);
				}

				// calculate offset
				_offset = 0;
				locations = (int[])_directory["cmap"];
				while (_offset < s_ttcMaxOffset)
				{
					br.BaseStream.Position = locations[1] - _offset;
					int magic      = br.ReadUInt16();
					int count      = br.ReadUInt16();
					int idPlat     = br.ReadUInt16();
					int idPlatSpec = br.ReadUInt16();
					int offset     = br.ReadInt32();
					if (magic == 0 && count > 0 && idPlat < 4 && idPlatSpec < 4 && offset > 0)
					{
						int pos = locations[1] - _offset + offset;
						if (pos < br.BaseStream.Length)
						{
							br.BaseStream.Position = pos;
							int format = br.ReadUInt16();
							if (format == 0 || format == 4 || format == 6)
								break;
						}
					}
					_offset += 4;
				}

				// correct table offsets
				if (_offset == s_ttcMaxOffset)
				{
					_invalid = true;	// TTF with unknown structure
					_used = true;
					//throw new Exception("TrueType font with unknown structure.");
				}
				else if (_offset > 0)
				{
					foreach (int[] locs in _directory.Values)
						locs[1] -= _offset;
				}
			}
		}

		#endregion

		#region ** read headers: 'head' & 'hhea' tables

        protected void ReadHeaders()
		{
			using (MemoryStream ms = new MemoryStream(this.FontData))
			using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
			{
				// read font header
				int[] locations = (int[])_directory["head"];
                if (locations == null)
                {
                    throw new FormatException("Table 'head' does not exist in " + _fontName);
                }

				br.BaseStream.Position = locations[1] + 18;
				_unitsPerEm = br.ReadUInt16();
				br.BaseStream.Position += 24;
				_style = br.ReadUInt16();

				// read horizontal header
				locations = (int[])_directory["hhea"];
                if (locations == null)
                {
                    throw new FormatException("Table 'hhea' does not exist in " + _fontName);
                }

				br.BaseStream.Position = locations[1] + 34;
				_numberOfHMetrics = br.ReadUInt16();

				// read OS/2 block (for read allowed)
                _fsType = (ushort)0;
				locations = (int[])_directory["OS/2"];
				if (locations != null)
				{
					br.BaseStream.Position = locations[1] + 8;
                    _fsType = br.ReadUInt16();
#if DEBUG
                    Debug.Assert(this.CanEmbed && this.CanSubset, "not allowed font!");
#endif
                    br.BaseStream.Position += 20;
                    _familyClass = br.ReadUInt16();
                    _panose = br.ReadBytes(10);
                }

                // read 'post' block (for fixed pitch attribute)
                locations = (int[])_directory["post"];
                if (locations != null)
                {
                    br.BaseStream.Position = locations[1] + 4;
                    short mantissa = br.ReadInt16();
                    int fraction = br.ReadUInt16();
                    _italicAngle = (float)mantissa + (float)fraction / 16384.0f;
                    br.BaseStream.Position += 4;
                    _fixedPitch = (br.ReadInt32() != 0);
                }
			}
		}

		#endregion

		#region ** read glyph widths: 'hmtx' table

        protected void ReadGlyphWidths()
		{
			int[] locations = (int[])_directory["hmtx"];
            if (locations == null)
            {
                throw new FormatException("Table 'hmtx' does not exist in " + _fontName);
            }

			using (MemoryStream ms = new MemoryStream(this.FontData))
			using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
			{
				// read glyph widths
				br.BaseStream.Position = locations[1];
				_glyphWidths = new int[_numberOfHMetrics];
				for (int i = 0; i < _numberOfHMetrics; i++)
				{
					_glyphWidths[i] = (1000 * br.ReadUInt16()) / _unitsPerEm;
					br.ReadUInt16();
				}
			}
		}

		#endregion

		#region ** read 'cmap' table

        protected void ReadCMap()
		{
			int[] locations = (int[])_directory["cmap"];
            if (locations == null)
            {
                throw new FormatException("Table 'cmap' does not exist in " + _fontName);
            }

			using (MemoryStream ms = new MemoryStream(this.FontData))
			using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
			{
#if skip_dima
				br.BaseStream.Position = locations[1] + 2;
#else
                // this is just to make code clearer.
                // not sure what to do if version is not 0...
                br.BaseStream.Position = locations[1];
                int version = br.ReadUInt16();
                System.Diagnostics.Debug.Assert(version == 0, "Invalid (not 0) cmap version number");
#endif
				int count   = br.ReadUInt16();
				int map_1_0 = 0;
				int map_3_0 = 0;
				int map_3_1 = 0;
				for (int i = 0; i < count; i++)
				{
					int idPlat     = br.ReadUInt16();
					int idPlatSpec = br.ReadUInt16();
					int offset     = br.ReadInt32();
					if (idPlat == 3 && idPlatSpec == 0)
					{
						_fontSpecific = true;
						map_3_0 = offset;
					}
					else if (idPlat == 3 && idPlatSpec == 1)
					{
						map_1_0 = offset;
					}
					if (idPlat == 1 && idPlatSpec == 0)
					{
                        // dima: the line below set _fontSpecific to true for Verdana font - wrong.
                        // the only sensible test for idPlat==1 would be for idPlatSpec==32 ("unspecified, uninterpreted"
                        // script. Not clear what the i==0 test means. In any case, the line below is a bug,
                        // left here only for info till this code is reviewed properly. ~~18406.
						// if (i == 0) _fontSpecific = true;
						map_3_1 = offset;
					}
				}
				if (map_1_0 > 0) 
				{
					br.BaseStream.Position = locations[1] + map_1_0;
					int format = br.ReadUInt16();
					switch (format) 
					{
						case 0:
							_cmap_1_0 = ReadAppleFormat(br);
							break;
						case 4:
							_cmap_1_0 = ReadMicrosoftFormat(br);
							break;
						case 6:
							_cmap_1_0 = ReadFormat(br);
							break;
					}
				}
				if (map_3_0 > 0) 
				{
                    // dima/todo: this code is a PATCH! it should NOT read map3.0 into _cmap_1_0.
                    // but currently there is no _cmap_3_0, hence this was added at some point
                    // as a "fix" for a bug that is now lost (some glyph ranges were not found
                    // in *some* font - unknown now...).
					br.BaseStream.Position = locations[1] + map_3_0;
					int format = br.ReadUInt16();
					if (format == 4) 
						_cmap_1_0 = ReadMicrosoftFormat(br);
				}
				if (map_3_1 > 0) 
				{
					br.BaseStream.Position = locations[1] + map_3_1;
					int format = br.ReadUInt16();
					if (format == 0 && (_fontSpecific || _cmap_1_0 == null))
						_cmap_3_1 = ReadAppleFormat(br);
					else if (format == 4) 
						_cmap_3_1 = ReadMicrosoftFormat(br);
				}
			}
		}
		private Hashtable ReadAppleFormat(BinaryReader br)
		{
			Hashtable hash = new Hashtable();
			br.BaseStream.Position += 4;
			for (int i = 0; i < 256; i++)
			{
				int[] r = new int[2];
				r[0] = br.ReadByte();
				r[1] = GetGlyphWidth(r[0]);
				hash.Add(i, r);
			}
			return hash;
		}
		private Hashtable ReadMicrosoftFormat(BinaryReader br)
		{
			Hashtable hash = new Hashtable();
			int table_lenght = br.ReadUInt16();
			br.BaseStream.Position += 2;

			int segCount = br.ReadUInt16()/2;

			br.BaseStream.Position += 6;

			int[] endCount = new int[segCount];
			for (int i = 0; i < segCount; i++)
				endCount[i] = br.ReadUInt16();

			br.BaseStream.Position += 2;

			int[] startCount = new int[segCount];
            for (int i = 0; i < segCount; i++)
            {
                startCount[i] = br.ReadUInt16();
            }

			int[] idDelta = new int[segCount];
            for (int i = 0; i < segCount; i++)
            {
                idDelta[i] = br.ReadUInt16();
            }

			int[] idRO = new int[segCount];
            for (int i = 0; i < segCount; i++)
            {
                idRO[i] = br.ReadUInt16();
            }
			
			int[] glyphId = new int[table_lenght/2 - 8 - 4*segCount];
            for (int i = 0; i < glyphId.Length; i++)
            {
                glyphId[i] = br.ReadUInt16();
            }

			for (int i = 0; i < segCount; i++)
			{
				int glyph;
				for (int j = startCount[i]; j <= endCount[i] && j != 0xFFFF; ++j)
				{
					if (idRO[i] == 0)
						glyph = (j + idDelta[i]) & 0xFFFF;
					else
					{
						int idx = i + idRO[i]/2 - segCount + j - startCount[i];

                        // shinishi@grapecity.com 20060215 - add { <<GC48>>
                        if (idx >= glyphId.Length)
                        {
                            continue;
                        }
						// shinishi@grapecity.com 20060215 }
						glyph = (glyphId[idx] + idDelta[i]) & 0xFFFF;
					}
					int[] r = new int[2];
					r[0] = glyph;
					r[1] = GetGlyphWidth(r[0]);
					hash.Add(j, r);
				}
			}
			return hash;
		}
		private Hashtable ReadFormat(BinaryReader br)
		{
			Hashtable hash = new Hashtable();
			br.BaseStream.Position += 4;
			int start_code = br.ReadUInt16();
			int code_count = br.ReadUInt16();
			for (int i = 0; i < code_count; i++)
			{
				int[] r = new int[2];
				r[0] = br.ReadUInt16();
				r[1] = GetGlyphWidth(r[0]);
				hash.Add(i, r);
			}
			return hash;
		}

		#endregion
    
		#region ** read 'loca' table

        protected void ReadLoca()
		{
			int[] locations = (int[])_directory["head"];
            if (locations == null)
            {
                throw new Exception("Table 'head' does not exist in " + _fontName);
            }

			using (MemoryStream ms = new MemoryStream(this.FontData))
			using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
			{
				// set position on 'head' 'loca' format
				br.BaseStream.Position = locations[1] + 51;

				_locaShortTable = (br.ReadUInt16() == 0);

				locations = (int[])_directory["loca"];
                if (locations == null)
                {
                    throw new Exception("Table 'loca' does not exist in " + _fontName);
                }

				// set position on 'loca' format
				br.BaseStream.Position = locations[1];

				if (_locaShortTable)
				{
					int entries = locations[2] / 2;
					_locaTable = new int[entries];
                    for (int i = 0; i < entries; i++)
                    {
                        _locaTable[i] = 2 * br.ReadUInt16();
                    }
				}
				else
				{
					int entries = locations[2] / 4;
					_locaTable = new int[entries];
                    for (int i = 0; i < entries; i++)
                    {
                        _locaTable[i] = br.ReadInt32();
                    }
				}
			}
		}

		#endregion

        #region ** read 'kern' table

        protected void ReadKerning()
        {
            int[] locations = (int[])_directory["kern"];
            if (locations == null)
                return;

            using (MemoryStream ms = new MemoryStream(this.FontData))
            using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
            {
                // set position on 'head' 'loca' format
                br.BaseStream.Position = locations[1] + 2;

                int count = br.ReadUInt16();
                _kerning = new Hashtable();
                int checkpoint = locations[1] + 4;
                int length = 0;
                for (int i = 0; i < count; i++) 
                {
                    checkpoint += length;
                    br.BaseStream.Position = checkpoint + 2;
                    length = br.ReadUInt16();
                    int coverage = br.ReadUInt16();
                    if ((coverage & 0xfff7) == 0x0001) 
                    {
                        int pairs = br.ReadUInt16();
                        br.BaseStream.Position += 6;
                        for (int j = 0; j < pairs; j++) 
                        {
                            int pair = br.ReadInt32();
                            int value = (int)(br.ReadInt16() * 1000 / _unitsPerEm);
                            _kerning.Add(pair, value);
                        }
                    }
                }
            }
        }

        #endregion

        #region ** read 'name' tables of this font or font family

        // get names for font
        internal void ReadFontNames(out FontNameInfo[] fontNames, out FontNameInfo[] familyNames)
        {
            int[] locations = (int[])_directory["name"];
            if (locations == null)
            {
                throw new Exception(string.Format("Table 'name' does not exist in {0}.", _fontName));
            }

            using (MemoryStream ms = new MemoryStream(this.FontData))
            using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
            {
                // set position
                br.BaseStream.Position = locations[1] + 2;

                // initialization
                ArrayList names = new ArrayList();
                ArrayList families = new ArrayList();

                // read records
                int numRecords = br.ReadUInt16();
                int startOfStorage = br.ReadUInt16();
                for (int i = 0; i < numRecords; i++)
                {
                    int platformId = br.ReadUInt16();
                    int platformEncodingId = br.ReadUInt16();
                    int languageId = br.ReadUInt16();
                    int nameId = br.ReadUInt16();
                    int length = br.ReadUInt16();
                    int offset = br.ReadUInt16();
                    if (nameId == 1 || nameId == 4)
                    {
                        // save position
                        long pos = br.BaseStream.Position;
                        br.BaseStream.Position = locations[1] + startOfStorage + offset;

                        // font or family name
                        string name;
                        if (platformId == 0 || platformId == 3 || (platformId == 2 && platformEncodingId == 1))
                        {
                            name = Encoding.BigEndianUnicode.GetString(br.ReadBytes(length));
                        }
                        else if (platformId == 1) // Macintosh
                        {
                            // masakazu.segawa@grapecity.com 20081226: Macintosh LanguageId cannot be recognized. Try ANSI code page.
                            name = Encoding.GetEncoding(0).GetString(br.ReadBytes(length));
                        }
                        else
                        {
                            name = Encoding.GetEncoding(languageId).GetString(br.ReadBytes(length));
                        }
                        name = name.Trim(' ', '\x00').Split('\n', '\r')[0];

                        switch (nameId)
                        {
                            case 1:
                                families.Add(new FontNameInfo(platformId, platformEncodingId, languageId, name));
                                break;
                            case 4:
                                names.Add(new FontNameInfo(platformId, platformEncodingId, languageId, name));
                                break;
                        }

                        // restore position
                        br.BaseStream.Position = pos;
                    }
                }

                // done
                fontNames = (FontNameInfo[])names.ToArray(typeof(FontNameInfo));
                familyNames = (FontNameInfo[])families.ToArray(typeof(FontNameInfo));
            }
        }

        #endregion

        #region ** read 'glyf' table

        protected void FlatGlyphs()
		{
			int[] locations = (int[])_directory["glyf"];
            if (locations == null)
            {
                throw new Exception("Table 'glyf' does not exist in " + _fontName);
            }

			int glyphNull = 0;
			if (!_glyphsUsed.ContainsKey(glyphNull))
			{
				_glyphsUsed.Add(glyphNull, null);
				_glyphsInList.Add(glyphNull);
			}

			_tableGlyphOffset = locations[1];
			for (int i = 0; i < _glyphsInList.Count; i++)
			{
				int glyph = (int)_glyphsInList[i];
				CheckGlyphComposite(glyph);
			}
		}
		private void CheckGlyphComposite(int glyph)
		{
			int start = _locaTable[glyph];

			// no contour
			if (start == _locaTable[glyph + 1])
				return;
			if (start < 0)
				return;

			using (MemoryStream ms = new MemoryStream(this.FontData))
			using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
			{
				br.BaseStream.Position = _tableGlyphOffset + start;
				int numContours = br.ReadInt16();

				if (numContours >= 0)
					return;

				br.BaseStream.Position += 8;

				for(;;) 
				{
					int flags = br.ReadUInt16();
					int cGlyph = br.ReadUInt16();

					if (!_glyphsUsed.ContainsKey(cGlyph)) 
					{
						_glyphsUsed.Add(cGlyph, null);
						_glyphsInList.Add(cGlyph);
					}

					// more components
					if ((flags & 0x20) == 0)
						return;

					// one argument and two words
					int skip;
					if ((flags & 0x01) != 0)
						skip = 4;
					else
						skip = 2;

					// scale
					if ((flags & 0x08) != 0)        // normal scale
						skip += 2;
					else if ((flags & 0x40) != 0)   // x & y scale
						skip += 4;
					if ((flags & 0x80) != 0)        // tow by two
						skip += 8;

					br.BaseStream.Position += skip;
				}
			}
		}

		#endregion
    
		#region ** create new 'loca' & 'glyf' tables

        protected void NewLocaGlyphTables()
		{
			// new 'loca' table
			_newLocaTable = new int[_locaTable.Length];
			int[] activeGlyphs = new int[_glyphsInList.Count];
			for (int i = 0; i < activeGlyphs.Length; i++)
				activeGlyphs[i] = (int)_glyphsInList[i];
			Array.Sort(activeGlyphs);

			// calculate glyphs size
			int glyfSize = 0;
			for (int i = 0; i < activeGlyphs.Length; i++)
			{
				int glyph = activeGlyphs[i];
				glyfSize += _locaTable[glyph + 1] - _locaTable[glyph];
			}

			// new 'glyf' table
			_glyfTableRealSize = glyfSize;
			glyfSize = (glyfSize + 3) & (~3);
			_newGlyfTable = new byte[glyfSize];

			// add active glyph to new 'glyf' table
			using (MemoryStream ms = new MemoryStream(this.FontData))
			using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
			{
				int glyfPtr = 0;
				int listGlyf = 0;
				for (int i = 0; i < _newLocaTable.Length; i++)
				{
					_newLocaTable[i] = glyfPtr;
					if (listGlyf < activeGlyphs.Length && activeGlyphs[listGlyf] == i) 
					{
						listGlyf++;
						_newLocaTable[i] = glyfPtr;
						int start = _locaTable[i];
						int len = _locaTable[i + 1] - start;
						if (len > 0) 
						{
							br.BaseStream.Position = _tableGlyphOffset + start;
							br.Read(_newGlyfTable, glyfPtr, len);
							glyfPtr += len;
						}
					}
				}
			}

			// convert 'loca' table to byte's buffer
            _locaTableRealSize = _newLocaTable.Length * (_locaShortTable ? 2 : 4);
			_newLocaTableOut = new byte[(_locaTableRealSize + 3) & (~3)];

			using (MemoryStream ms = new MemoryStream(_newLocaTableOut))
			using (BigEndianBinaryWriter bw = new BigEndianBinaryWriter(ms))
			{
				for (int i = 0; i < _newLocaTable.Length; i++)
				{
					if (_locaShortTable)
						bw.Write((short)(_newLocaTable[i] / 2));
					else
						bw.Write((int)_newLocaTable[i]);
				}
			}
		}

		#endregion

		#region ** write output font buffer

		protected void WriteBuffer()
		{
			int[] locations;
			int fullFontSize = 0;
			string[] tableNames;
			if (_includeCMap)
				tableNames = s_tableNamesCMap;
			else
				tableNames = s_tableNamesSimple;
			int tablesUsed = 2;
			int len = 0;
			for (int i = 0; i < tableNames.Length; i++)
			{
				string name = tableNames[i];
				if (name.Equals("glyf"))
					fullFontSize += _newGlyfTable.Length;
				else if (name.Equals("loca"))
					fullFontSize += _newLocaTableOut.Length;
				else
				{
					locations = (int[])_directory[name];
					if (locations == null)
						continue;
					tablesUsed++;
					fullFontSize += (locations[2] + 3) & (~3);
				}
			}
			int r = 16 * tablesUsed + 12;
			fullFontSize += r;

			_buffer = new byte[fullFontSize];

			using (MemoryStream msBuffer = new MemoryStream(_buffer))
			using (BigEndianBinaryWriter bw = new BigEndianBinaryWriter(msBuffer))
			{
				// write header identifier
				bw.Write((int)0x10000);

				// write font tables
				bw.Write((short)tablesUsed);
				int selector = s_entrySelectors[tablesUsed];
				bw.Write((short)((1 << selector) * 16));
				bw.Write((short)selector);
				bw.Write((short)((tablesUsed - (1 << selector)) * 16));

				for (int i = 0; i < tableNames.Length; i++)
				{
					string name = tableNames[i];
					locations = (int[])_directory[name];
					if (locations == null)
						continue;
					bw.Write(Encoding.Default.GetBytes(name));
					if (name.Equals("glyf")) 
					{
						bw.Write(GetChecksum(_newGlyfTable));
						len = _glyfTableRealSize;
					}
					else if (name.Equals("loca")) 
					{
						bw.Write(GetChecksum(_newLocaTableOut));
						len = _locaTableRealSize;
					}
					else 
					{
						bw.Write(locations[0]);
						len = locations[2];
					}
					bw.Write(r);
					bw.Write(len);
					r += (len + 3) & (~3);
				}

				// read font data and copy to buffer
				using (MemoryStream ms = new MemoryStream(this.FontData))
				using (BigEndianBinaryReader br = new BigEndianBinaryReader(ms))
				{
					for (int i = 0; i < tableNames.Length; i++)
					{
						string name = tableNames[i];
						locations = (int[])_directory[name];
						if (locations == null)
							continue;
						if (name.Equals("glyf"))
						{
							//Array.Copy(_newGlyfTable, 0, _buffer, _fontPtr, _newGlyfTable.Length);
							//_fontPtr += _newGlyfTable.Length;
							bw.Write(_newGlyfTable);
							_newGlyfTable = null;
						}
						else if (name.Equals("loca"))
						{
							//Array.Copy(_newLocaTableOut, 0, _buffer, _fontPtr, _newLocaTableOut.Length);
							//_fontPtr += _newLocaTableOut.Length;
							bw.Write(_newLocaTableOut);
							_newLocaTableOut = null;
						}
						else
						{
							br.BaseStream.Position = locations[1];
							br.Read(_buffer, (int)bw.BaseStream.Position, locations[2]);
							bw.BaseStream.Position += (locations[2] + 3) & (~3);
						}
					}
				}
			}
		}

		#endregion
    
		#region ** helper methods

		private string ReadFontString(BinaryReader br, int length)
		{
			byte[] buf = new byte[length];
			br.Read(buf, 0, buf.Length);
			char[] chs = Encoding.ASCII.GetChars(buf);
			return new string(chs);
		}
		private int GetChecksum(byte[] b) 
		{
			int len = b.Length / 4;
			int v0 = 0;
			int v1 = 0;
			int v2 = 0;
			int v3 = 0;
			int ptr = 0;
			for (int i = 0; i < len; i++)
			{
				v3 += (int)b[ptr++] & 0xff;
				v2 += (int)b[ptr++] & 0xff;
				v1 += (int)b[ptr++] & 0xff;
				v0 += (int)b[ptr++] & 0xff;
			}
			return v0 + (v1 << 8) + (v2 << 16) + (v3 << 24);
		}

		#endregion

		#endregion

		//-----------------------------------------------------------------------------
		#region ** internal helper classes

        /// <summary>
        /// Font name information from the font subset.
        /// </summary>
        internal struct FontNameInfo
        {
            /// <summary>The platform identifier.</summary>
            internal readonly int Platform;
            /// <summary>The platform encoding code (identifier).</summary>
            internal readonly int Encoding;
            /// <summary>The language identifier.</summary>
            internal readonly int Language;
            /// <summary>The text name of the font or font family.</summary>
            internal readonly string Name;

            internal FontNameInfo(int platform, int encoding, int language, string name)
            {
                Platform = platform;
                Encoding = encoding;
                Language = language;
                Name = name;
            }
        }

		/// <summary>
		/// TTF files are stored in big-endian format, we need special 
		/// readers and writers
		/// </summary>
		internal class BigEndianBinaryReader : BinaryReader
		{
			//-----------------------------------------------------------------------------
			#region ** fields & ctors
			private Encoding _encoding = Encoding.Default;  // string encoding

			internal BigEndianBinaryReader(Stream stream) : base(stream) {}
			internal BigEndianBinaryReader(Stream stream, Encoding encoding) : base(stream, encoding)
			{
				_encoding = encoding;
			}
			#endregion

			//-----------------------------------------------------------------------------
			#region ** properties
			//		/// Get or set the base stream position
			//		internal long Position
			//		{
			//			get { return BaseStream.Position; }
			//			set { BaseStream.Position = value; }
			//		}
			internal Encoding Encoding
			{
				get { return _encoding; }
				set { _encoding = value; }
			}
			#endregion

			//-----------------------------------------------------------------------------
			#region ** overrides
			public override short ReadInt16()
			{
				return (short)((ReadByte() << 8) + ReadByte());
			}
			public override int ReadInt32()
			{
				return (int)((ReadByte() << 24) + (ReadByte() << 16) + (ReadByte() << 8) + ReadByte());
			}
			public override long ReadInt64()
			{
				return ((long)(ReadUInt32()) << 32) + (ReadUInt32() & 0xffffffffL);
			}
			public override ushort ReadUInt16()
			{
				return (ushort)((ReadByte() << 8) + ReadByte());
			}
			public override uint ReadUInt32()
			{
				return (uint)((ReadByte() << 24) + (ReadByte() << 16) + (ReadByte() << 8) + ReadByte());
			}
			public override ulong ReadUInt64()
			{
				return (ulong)(((long)(ReadUInt32()) << 32) + (ReadUInt32() & 0xffffffffL));
			}
			public override char ReadChar()
			{
				return (char)((ReadByte() << 8) + ReadByte());
			}
			public override char[] ReadChars(int count)
			{
				char[] buffer = new char[count*2];
				for (int i = 0; i < count; i++)
					buffer[i] = ReadChar();
				return buffer;
			}
			public override float ReadSingle()
			{
				return (float)BitConverter.Int64BitsToDouble(ReadUInt32());
			}
			public override double ReadDouble()
			{
				return BitConverter.Int64BitsToDouble(ReadInt64());
			}
			#endregion
		}
		internal class BigEndianBinaryWriter : BinaryWriter
		{
			//-----------------------------------------------------------------------------
			#region ** fields & ctors
			private Encoding _encoding = Encoding.Default;  // the encoding of the strings

			internal BigEndianBinaryWriter(Stream stream) : base(stream) {}
			internal BigEndianBinaryWriter(Stream stream, Encoding encoding) : base(stream, encoding)
			{
				_encoding = encoding;
			}
			#endregion

			//-----------------------------------------------------------------------------
			#region ** properties
			internal Encoding Encoding
			{
				get { return _encoding; }
				set { _encoding = value; }
			}
			#endregion

			//-----------------------------------------------------------------------------
			#region ** overrides
			public override void Write(short value)
			{
				base.Write((byte)(value >> 8));
				base.Write((byte)(value & 0xff));
			}
			public override void Write(int value)
			{
				this.Write((short) (value>>16));
				this.Write((short) (value & 0xffff));
			}
			public override void Write(long value)
			{
				this.Write((int) (value >> 32));
				this.Write((int) (value & 0xffffffff));
			}
			public override void Write(string value)
			{
				Write(value.ToCharArray());
			}
			#endregion
		}

		#endregion
	}
}
