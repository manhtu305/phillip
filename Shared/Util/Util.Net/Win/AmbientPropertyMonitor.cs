using System;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.ComponentModel.Design.Serialization;
using System.Runtime.Serialization;
using System.Reflection;
using System.Diagnostics;

namespace C1.Util.Win
{
    /// <summary>
    /// A helper class for implementing an ambient property on a control.
    /// </summary>
    /// <remarks>
    /// Usage:
    /// An instance of AmbientPropertyMonitor must be created by the host control with the reference
    /// to that host as the parameter.
    /// The AmbientPropertyMonitor subscribes to the following events:
    /// <list type="bullet">
    /// <item>ParentChanged on the host and the whole parents' chain;
    /// </item>
    /// <item>(ambient property name)Changed or INotifyPropertyChanged on the host and all parents in the chain.
    /// NOTE: For the AmbientPropertyMonitor to work correctly, all controls that have the target property declared
    /// must also provide either a property changed event, or implement INotifyPropertyChanged and fire
    /// it at appropriate moments.
    /// </item>
    /// </list>
    /// <para>The AmbientPropertyMonitor monitors changes of the ambient property value on the host and all parents,
    /// and if a change occurs applies the value of the ambient property on the closest parent
    /// to that property on the host control, unless it has been changed independently of the AmbientPropertyMonitor
    /// (AmbientPropertyMonitor assumes that when it is created, the ambient property on the host has default,
    /// unchanged value).
    /// </para>
    /// <para>It is recommended that all controls declaring the target property also provide
    /// the Reset(property) and ShouldSerialize(property) methods, calling the
    /// <see cref="ResetValue"/> and <see cref="IsValueAmbient"/> methods as needed.
    /// </para>
    /// </remarks>
    internal class AmbientPropertyMonitor
    {
        #region IC1GetAmbientValue
        // In some cases it may be needed for a container providing an ambient property
        // to override the ambient value of that property picked up by nested controls.
        // E.g. a container can have VisualStyle set to an Office2007Blue-based
        // Custom style. Because the container has no control over what "Custom" style
        // means in the possible nested control, it might be better if they pick up
        // "Office2007Blue" style in this case instead. If a container control needs
        // such functionality, it should expose a method with the signature:
        //      public object IC1GetAmbientValue(string propertyName);
        // acessible via reflection. If such a method is found, the value returned by
        // it will be used when setting nested controls' property value rather than
        // what is returned by the container property's getter.

        #endregion

        #region IC1GetAmbientRootSource
        //  public Component IC1GetAmbientRootSource(string propertyName);
        //  Returns the object to query for the property value if no parent
        //  defines that property. The object may also define a public event
        //  AmbientRootSourceChanged, allowing to monitor changes of the root
        //  object. The root object may define a public event
        //  <propertyName>Changed, allowing to monitor changes of the target
        //  property on the root object.
        #endregion

        #region private data members
        /// <summary>
        /// The target property name.
        /// </summary>
        private string _pName = string.Empty;
        /// <summary>
        /// At all times, should contain the up-to-date parent chain,
        /// starting with the host itself. Needed to keep track of
        /// parent changes, and changes of ambient property on the parents.
        /// </summary>
        private List<Control> _parentChain = new List<Control>();
        /// <summary>
        /// Prevents raising the _propertySetByUser while we adjust the
        /// property ourselves.
        /// </summary>
        private bool _internalChange = false;
        /// <summary>
        /// If true, we consider the property to have a value explicitly set
        /// ty the user, so we do not update it anymore until reset.
        /// </summary>
        private bool _propertySetByUser = false;
        private Component _rootSource = null;
        #endregion

        #region public methods
        /// <summary>
        /// Initializes a new instance of the AmbientPropertyMonitor class.
        /// </summary>
        /// <param name="host">Control on which the ambient property will be monitored.</param>
        /// <param name="propertyName">The name of the monitored property.
        /// The type of the property MUST be an enumeration.</param>
        /// <remarks>
        /// An instance of this class must be created in the constructor 
        /// of the host control.
        /// It is assumed that the value of the ambient property that will be monitored
        /// by the AmbientPropertyMonitor has not been set yet (i.e. has the default value)
        /// at the time the AmbientPropertyMonitor is constructed.
        /// </remarks>
        public AmbientPropertyMonitor(Control host, string propertyName)
        {
            _pName = propertyName;
            _parentChain.Add(host);
            host.ParentChanged += new EventHandler(ParentChangedHandler);
            UpdateParentChain();
            SubscribeToPropertyChangedHandlers(host);
        }

        /// <summary>
        /// Clears the all references.
        /// </summary>
        /// <remarks>
        /// Call Dispose() in the Dispose method 
        /// of the host control.
        /// 
        /// An instance of the AmbientPropertyMonitor class holds references to the 
        /// host in private variables and event handles.
        /// Host holds reference to the instance of the AmbientPropertyMonitor.
        /// 
        /// If not to clear the refs then the host control and the all referenced objects (Form, etc.) may be never released to GC.
        /// This cause a memory leak.
        /// </remarks>
        /// <example>
        /// public class C1OutBar...
        /// {
        ///  public C1OutBar()
        ///  {
        ///    ...
        ///    _ambientVisualStyleMonitor = new AmbientPropertyMonitor(this, "VisualStyle");
        ///  }
        /// 
        ///  protected override void Dispose(bool disposing)
        ///  {
        ///   if (disposing)
        ///   {
        ///     // To prevent the memory leak
        ///     if (_ambientVisualStyleMonitor != null)
        ///     {
        ///         _ambientVisualStyleMonitor.Dispose();
        ///         _ambientVisualStyleMonitor = null;
        ///     }
        ///     ...
        ///     base.Dispose(disposing);
        ///   }
        /// ...
        /// }
        /// </example>
        public void Dispose()
        {
            if (_rootSource != null)
            {
                UnsubscribeFromPropertyChangedHandlers(_rootSource);
                this._rootSource = null;
            }
            // unsubscribe from events
            for (int i=0;i<_parentChain.Count;i++)
            {
                Control c = _parentChain[i];
                if (c != null)
                {
                    c.ParentChanged -= new EventHandler(ParentChangedHandler);
                    UnsubscribeFromPropertyChangedHandlers(c);
                }
            }
            _parentChain.Clear();
        }

        /// <summary>
        /// Notifies the AmbientPropertyMonitor that the value of the monitored property
        /// has been reset. If the host control is currently parented to a container
        /// which itself defines the monitored property, the value from that parent
        /// is assigned to the property on the host.
        /// </summary>
        /// <remarks>
        /// It is recommended that the host defines a Reset method for the target
        /// property, and calls this method from that. E.g.:
        /// <code>
        /// private AmbientPropertyMonitor __ambientVisualStyleMonitor = null;
        /// ...
        /// ctor() {
        ///     __ambientVisualStyleMonitor = new AmbientPropertyMonitor(this, "VisualStyle");
        ///     ...
        /// }
        /// public VisualStyle VisualStyle {
        ///     get { ... }
        ///     set { ... }
        /// }
        /// protected void ResetVisualStyle() {
        ///     ...
        ///     __ambientVisualStyleMonitor.ResetValue();
        /// }
        /// </code>
        /// </remarks>
        public void ResetValue()
        {
            if (!_internalChange)
            {
                _propertySetByUser = false;
                UpdateValueOnHost();
            }
        }

        /// <summary>
        /// Gets the value indicating whether the monitored property
        /// currently has the ambient value, i.e. the value has been set
        /// (by the AmbientPropertyMonitor) based on the value of a parent of the host.
        /// </summary>
        /// <remarks>
        /// It is recommended that the host defines a ShouldSerialize method for the target
        /// property, tests IsValueAmbient in that method, and returns false if IsValueAmbient
        /// returns true. E.g.:
        /// <code>
        /// private AmbientPropertyMonitor __ambientVisualStyleMonitor = null;
        /// ...
        /// ctor() {
        ///     __ambientVisualStyleMonitor = new AmbientPropertyMonitor(this, "VisualStyle");
        ///     ...
        /// }
        /// public VisualStyle VisualStyle {
        ///     get { ... }
        ///     set { ... }
        /// }
        /// protected bool ShouldSerializeVisualStyle() {
        ///     if (__ambientVisualStyleMonitor.IsValueAmbient)
        ///         return false;
        ///     ...
        /// }
        /// </code>
        /// </remarks>
        public bool IsValueAmbient
        {
            get { return !_propertySetByUser; }
        }
        #endregion

        #region private methods
        private void UpdateValueOnHost()
        {
            if (_propertySetByUser)
                return;

            _internalChange = true;
            try
            {
                bool done = false;
                // update host with ambient value:
                PropertyDescriptor pHost = TypeDescriptor.GetProperties(_parentChain[0])[_pName];
                PropertyInfo pi = _parentChain[0].GetType().GetProperty(_pName);
                object value = null;
                for (int i = 1; i < _parentChain.Count && !done; ++i)
                {
                    try
                    {
                        MethodInfo mi = _parentChain[i].GetType().GetMethod("IC1GetAmbientValue", new Type[] { typeof(string) });
                        if (mi != null)
                        {
                            value = mi.Invoke(_parentChain[i], new object[] { _pName });
                        }
                        else
                        {
                            PropertyDescriptor pParent = TypeDescriptor.GetProperties(_parentChain[i])[_pName];
                            if (pParent != null)
                                value = pParent.GetValue(_parentChain[i]);
                        }
                        if (value != null)
                        {
                            value = Enum.Parse(pHost.PropertyType, value.ToString());
                            pi.SetValue(_parentChain[0], value, null);
                            done = true;
                        }
                    }
                    catch
                    {
                    }
                }
                if (!done && _rootSource != null)  // no parents with such property - find logical "parent"
                {
                    try
                    {
                        value = GetAmbientRootSourceValue();
                        if (value != null)
                        {
                            value = Enum.Parse(pHost.PropertyType, value.ToString());
                            pi.SetValue(_parentChain[0], value, null);
                            done = true;
                        }
                    }
                    catch
                    {
                    }
                }
                if (!done)
                {
                    _propertySetByUser = true;
                    if (pHost.ShouldSerializeValue(_parentChain[0]))
                        pHost.ResetValue(_parentChain[0]);
                    _propertySetByUser = false;
                }
            }
            finally
            {
                _internalChange = false;
            }
        }

        private Component FindAmbientRootSource()
        {
            Component rootsource = null;
            MethodInfo mi = _parentChain[0].GetType().GetMethod("IC1GetAmbientRootSource", new Type[] { typeof(string) });
            if (mi != null)
            {
                rootsource = mi.Invoke(_parentChain[0], new object[] { _pName }) as Component;
                if (rootsource != null)
                    return rootsource;
            }
            return null;
        }

        private object GetAmbientRootSourceValue()
        {
            if (_rootSource != null)
            {
                PropertyDescriptor proproot = TypeDescriptor.GetProperties(_rootSource)[_pName];
                if (proproot != null)
                    return proproot.GetValue(_rootSource);
            }
            return null;
        }

        private void SubscribeToPropertyChangedHandlers(Component c)
        {
            // NOTE: using EventDescriptor ed = TypeDescriptor.GetEvents(c)[_pName + "Changed"];
            // does not work correctly at design time: for each instance of control,
            // only one event handler is attached. So if a container and nested control
            // have property FooProp, container will have only one NamedPropertyChangedHandler
            // attached to its FooPropChanged event. Same works at runtime...
            // Using EventInfo ei = c.GetType().GetEvent(_pName + "Changed");
            // works correctly at design and runtime.
            EventInfo ei = c.GetType().GetEvent(_pName + "Changed");
            if (ei != null)
                ei.AddEventHandler(c, new EventHandler(NamedPropertyChangedHandler));
            else
            {
                INotifyPropertyChanged inpc = c as INotifyPropertyChanged;
                if (inpc != null)
                    inpc.PropertyChanged += new PropertyChangedEventHandler(INotifyPropertyChangedHandler);
            }
        }

        private void UnsubscribeFromPropertyChangedHandlers(Component c)
        {
            EventInfo ei = c.GetType().GetEvent(_pName + "Changed");
            if (ei != null)
                ei.RemoveEventHandler(c, new EventHandler(NamedPropertyChangedHandler));
            else
            {
                INotifyPropertyChanged inpc = c as INotifyPropertyChanged;
                if (inpc != null)
                    inpc.PropertyChanged -= new PropertyChangedEventHandler(INotifyPropertyChangedHandler);
            }
        }

        private bool UpdateParentChain()
        {
            Debug.Assert(_parentChain.Count > 0, "AmbientPropertyMonitor: Host must always be present in the _parentChain.");
            bool changed = false;
            // scan the chain for the last up-to-date parent,
            // release all others:
            Control host = _parentChain[0];
            for (int i = 1; i < _parentChain.Count; ++i)
            {
                if (_parentChain[i - 1].Parent != _parentChain[i])
                {
                    // release no longer up-to-date parents:
                    for (int j = i; j < _parentChain.Count; ++j)
                    {
                        UnsubscribeFromPropertyChangedHandlers(_parentChain[j]);
                        _parentChain[j].ParentChanged -= new EventHandler(ParentChangedHandler);
                    }
                    _parentChain.RemoveRange(i, _parentChain.Count - i);
                    changed = true;
                    break;
                }
            }
            // subscribe to new parents' ParentChanged events:
            for (Control c = _parentChain[_parentChain.Count - 1].Parent; c != null; c = c.Parent)
            {
                c.ParentChanged += new EventHandler(ParentChangedHandler);
                SubscribeToPropertyChangedHandlers(c);
                _parentChain.Add(c);
                changed = true;
            }
            return UpdateRootSource() || changed;
        }

        internal bool UpdateRootSource()
        {
            bool changed = false;
            if (_rootSource != null)
            {
                UnsubscribeFromPropertyChangedHandlers(_rootSource);
                changed = true;
            }
            _rootSource = FindAmbientRootSource();
            if (_rootSource != null)
            {
                SubscribeToPropertyChangedHandlers(_rootSource);
                changed = true;
            }
            return changed;
        }

        private void ParentChangedHandler(object sender, EventArgs e)
        {
            if (UpdateParentChain())
                UpdateValueOnHost();
        }

        private void NamedPropertyChangedHandler(object sender, EventArgs e)
        {
            if (!_internalChange)
            {
                if (sender == _parentChain[0])
                    _propertySetByUser = true;
                else
                    UpdateValueOnHost();
            }
        }

        private void INotifyPropertyChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == _pName)
                NamedPropertyChangedHandler(sender, EventArgs.Empty);
        }
        #endregion
    }
}
