using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections;
using System.Reflection;
using System.Drawing.Imaging;

namespace C1.Util.Win
{
    #region THEME Type enums

    internal enum THEMESIZE
    {
        TS_MIN,             // minimum size of a visual style part.
        TS_TRUE,            // size of the visual style part that will best fit the available space
        TS_DRAW,            // size that the theme manager uses to draw a part
    }

    internal enum BGTYPE     //BackgroundType
    {
        BT_IMAGEFILE = 0,
        BT_BORDERFILL,
        BT_NONE,
    }

    internal enum IMAGELAYOUT
    {
        IL_VERTICAL = 0,
        IL_HORIZONTAL,
    }

    internal enum BORDERTYPE
    {
        BT_RECT = 0,
        BT_ROUNDRECT,
        BT_ELLIPSE,
    }

    internal enum FILLTYPE
    {
        FT_SOLID = 0,
        FT_VERTGRADIENT,
        FT_HORZGRADIENT,
        FT_RADIALGRADIENT,
        FT_TILEIMAGE,
    }

    internal enum SIZINGTYPE
    {
        ST_TRUESIZE = 0,
        ST_STRETCH,
        ST_TILE,
    }

    internal enum HALIGN
    {
        HA_LEFT = 0,
        HA_CENTER,
        HA_RIGHT,
    }

    internal enum CONTENTALIGNMENT
    {
        CA_LEFT = 0,
        CA_CENTER,
        CA_RIGHT,
    }

    internal enum VALIGN
    {
        VA_TOP = 0,
        VA_CENTER,
        VA_BOTTOM,
    }

    internal enum OFFSETTYPE
    {
        OT_TOPLEFT = 0,
        OT_TOPRIGHT,
        OT_TOPMIDDLE,
        OT_BOTTOMLEFT,
        OT_BOTTOMRIGHT,
        OT_BOTTOMMIDDLE,
        OT_MIDDLELEFT,
        OT_MIDDLERIGHT,
        OT_LEFTOFCAPTION,
        OT_RIGHTOFCAPTION,
        OT_LEFTOFLASTBUTTON,
        OT_RIGHTOFLASTBUTTON,
        OT_ABOVELASTBUTTON,
        OT_BELOWLASTBUTTON,
    }

    internal enum ICONEFFECT
    {
        ICE_NONE = 0,
        ICE_GLOW,
        ICE_SHADOW,
        ICE_PULSE,
        ICE_ALPHA,
    }

    internal enum TEXTSHADOWTYPE
    {
        TST_NONE = 0,
        TST_SINGLE,
        TST_CONTINUOUS,
    }

    internal enum GLYPHTYPE
    {
        GT_NONE = 0,
        GT_IMAGEGLYPH,
        GT_FONTGLYPH,
    }

    internal enum IMAGESELECTTYPE
    {
        IST_NONE = 0,
        IST_SIZE,
        IST_DPI,
    }

    internal enum TRUESIZESCALINGTYPE
    {
        TSST_NONE = 0,
        TSST_SIZE,
        TSST_DPI,
    }

    internal enum GLYPHFONTSIZINGTYPE
    {
        GFST_NONE = 0,
        GFST_SIZE,
        GFST_DPI,
    }
    #endregion

    #region THEME properties - commented, if needed - convert *.h syntax to *.cs syntax

    //-----------------------------------------------------------------
    //    PROPERTIES - used by uxtheme rendering and controls
    //-----------------------------------------------------------------
    /*
    internal enum THEMEPROPS   //PROPERTIES (theme types,
    {
        //---- primitive types ----
         201, TMT_STRING,    STRING,
         202, TMT_INT,       INT,
         203, TMT_BOOL,      BOOL,
         204, TMT_COLOR,     COLOR,
         205, TMT_MARGINS_  MARGINS,
         206, TMT_FILENAME,  FILENAME,
         207, TMT_SIZE,      SIZE,
         208, TMT_POSITION,  POSITION,
         209, TMT_RECT,      RECT,
         210, TMT_FONT,      FONT,
         211, TMT_INTLIST,   INTLIST,
        //---- special misc. properties ----
         401, TMT_COLORSCHEMES_  STRING,
         402, TMT_SIZES_         STRING,
         403, TMT_CHARSET,        INT,
        //---- [documentation] properties ----
         601, TMT_DISPLAYNAME,    STRING,
         602, TMT_TOOLTIP_       STRING,
         603, TMT_COMPANY,        STRING,
         604, TMT_AUTHOR,         STRING,
         605, TMT_COPYRIGHT,      STRING,
         606, TMT_URL,            STRING,
         607, TMT_VERSION,        STRING,
         608, TMT_DESCRIPTION,    STRING,
        //---- theme metrics: fonts ----
         801, TMT_CAPTIONFONT,        FONT,
         802, TMT_SMALLCAPTIONFONT,   FONT,
         803, TMT_MENUFONT,           FONT,
         804, TMT_STATUSFONT,         FONT,
         805, TMT_MSGBOXFONT,         FONT,
         806, TMT_ICONTITLEFONT,      FONT,
        //---- theme metrics: bools ----
         1001, TMT_FLATMENUS_           BOOL,
        //---- theme metrics: sizes ----
         1201, TMT_SIZINGBORDERWIDTH,    SIZE,
         1202, TMT_SCROLLBARWIDTH,       SIZE,
         1203, TMT_SCROLLBARHEIGHT,      SIZE,
         1204, TMT_CAPTIONBARWIDTH,      SIZE,
         1205, TMT_CAPTIONBARHEIGHT,     SIZE,
         1206, TMT_SMCAPTIONBARWIDTH,    SIZE,
         1207, TMT_SMCAPTIONBARHEIGHT,   SIZE,
         1208, TMT_MENUBARWIDTH,         SIZE,
         1209, TMT_MENUBARHEIGHT,        SIZE,
        //---- theme metrics: ints ----
         1301, TMT_MINCOLORDEPTH,     INT,
        //---- theme metrics: strings ----
         1401, TMT_CSSNAME,            STRING,
         1402, TMT_XMLNAME,            STRING,
        //---- theme metrics: colors ----
         1601, TMT_SCROLLBAR,          COLOR,
         1602, TMT_BACKGROUND,         COLOR,
         1603, TMT_ACTIVECAPTION,      COLOR,
         1604, TMT_INACTIVECAPTION,    COLOR,
         1605, TMT_MENU,               COLOR,
         1606, TMT_WINDOW,             COLOR,
         1607, TMT_WINDOWFRAME,        COLOR,
         1608, TMT_MENUTEXT,           COLOR,
         1609, TMT_WINDOWTEXT,         COLOR,
         1610, TMT_CAPTIONTEXT,        COLOR,
         1611, TMT_ACTIVEBORDER,       COLOR,
         1612, TMT_INACTIVEBORDER,     COLOR,
         1613, TMT_APPWORKSPACE,       COLOR,
         1614, TMT_HIGHLIGHT,          COLOR,
         1615, TMT_HIGHLIGHTTEXT,      COLOR,
         1616, TMT_BTNFACE,            COLOR,
         1617, TMT_BTNSHADOW,          COLOR,
         1618, TMT_GRAYTEXT,           COLOR,
         1619, TMT_BTNTEXT,            COLOR,
         1620, TMT_INACTIVECAPTIONTEXT,     COLOR,
         1621, TMT_BTNHIGHLIGHT,            COLOR,
         1622, TMT_DKSHADOW3D,              COLOR,
         1623, TMT_LIGHT3D,                 COLOR,
         1624, TMT_INFOTEXT,                COLOR,
         1625, TMT_INFOBK,                  COLOR,
         1626, TMT_BUTTONALTERNATEFACE,     COLOR,
         1627, TMT_HOTTRACKING,             COLOR,
         1628, TMT_GRADIENTACTIVECAPTION,   COLOR,
         1629, TMT_GRADIENTINACTIVECAPTION, COLOR,
         1630, TMT_MENUHILIGHT,             COLOR,
         1631, TMT_MENUBAR,                 COLOR,
        //---- hue substitutions ----
         1801, TMT_FROMHUE1,  INT,
         1802, TMT_FROMHUE2,  INT,
         1803, TMT_FROMHUE3,  INT,
         1804, TMT_FROMHUE4,  INT,
         1805, TMT_FROMHUE5,  INT,
         1806, TMT_TOHUE1,    INT,
         1807, TMT_TOHUE2,    INT,
         1808, TMT_TOHUE3,    INT,
         1809, TMT_TOHUE4,    INT,
         1810, TMT_TOHUE5,    INT,
        //---- color substitutions ----
         2001, TMT_FROMCOLOR1,  COLOR,
         2002, TMT_FROMCOLOR2,  COLOR,
         2003, TMT_FROMCOLOR3,  COLOR,
         2004, TMT_FROMCOLOR4,  COLOR,
         2005, TMT_FROMCOLOR5,  COLOR,
         2006, TMT_TOCOLOR1,    COLOR,
         2007, TMT_TOCOLOR2,    COLOR,
         2008, TMT_TOCOLOR3,    COLOR,
         2009, TMT_TOCOLOR4,    COLOR,
         2010, TMT_TOCOLOR5,    COLOR,
        //---- rendering BOOL properties ----
         2201, TMT_TRANSPARENT,   BOOL,       // image has transparent areas (see TransparentColor,
         2202, TMT_AUTOSIZE,      BOOL,       // if TRUE, nonclient caption width varies with text extent
         2203, TMT_BORDERONLY,    BOOL,       // only draw the border area of the image
         2204, TMT_COMPOSITED,    BOOL,       // control will handle the composite drawing
         2205, TMT_BGFILL,        BOOL,       // if TRUE, TRUESIZE images should be drawn on bg fill
         2206, TMT_GLYPHTRANSPARENT,  BOOL,   // glyph has transparent areas (see GlyphTransparentColor,
         2207, TMT_GLYPHONLY,         BOOL,   // only draw glyph (not background,
         2208, TMT_ALWAYSSHOWSIZINGBAR, BOOL,
         2209, TMT_MIRRORIMAGE,         BOOL, // default=TRUE means image gets mirrored in RTL (Mirror, windows
         2210, TMT_UNIFORMSIZING,       BOOL, // if TRUE, height & width must be uniformly sized 
         2211, TMT_INTEGRALSIZING,      BOOL, // for TRUESIZE and Border sizing; if TRUE, factor must be integer
         2212, TMT_SOURCEGROW,          BOOL, // if TRUE, will scale up src image when needed
         2213, TMT_SOURCESHRINK,        BOOL, // if TRUE, will scale down src image when needed
        //---- rendering INT properties ----
         2401, TMT_IMAGECOUNT,        INT,    // the number of state images in an imagefile
         2402, TMT_ALPHALEVEL,        INT,    // (0-255, alpha value for an icon (DrawThemeIcon part,
         2403, TMT_BORDERSIZE,        INT,    // the size of the border line for bgtype=BorderFill
         2404, TMT_ROUNDCORNERWIDTH,  INT,    // (0-100, % of roundness for rounded rects
         2405, TMT_ROUNDCORNERHEIGHT, INT,    // (0-100, % of roundness for rounded rects
         2406, TMT_GRADIENTRATIO1,    INT,    // (0-255, - amt of gradient color 1 to use (all must total=255,
         2407, TMT_GRADIENTRATIO2,    INT,    // (0-255, - amt of gradient color 2 to use (all must total=255,
         2408, TMT_GRADIENTRATIO3,    INT,    // (0-255, - amt of gradient color 3 to use (all must total=255,
         2409, TMT_GRADIENTRATIO4,    INT,    // (0-255, - amt of gradient color 4 to use (all must total=255,
         2410, TMT_GRADIENTRATIO5,    INT,    // (0-255, - amt of gradient color 5 to use (all must total=255,
         2411, TMT_PROGRESSCHUNKSIZE, INT,    // size of progress control chunks
         2412, TMT_PROGRESSSPACESIZE, INT,    // size of progress control spaces
         2413, TMT_SATURATION,        INT,    // (0-255, amt of saturation for DrawThemeIcon(, part
         2414, TMT_TEXTBORDERSIZE,    INT,    // size of border around text chars
         2415, TMT_ALPHATHRESHOLD,    INT,    // (0-255, the min. alpha value of a pixel that is solid
         2416, TMT_WIDTH,             SIZE,   // custom window prop: size of part (min. window,
         2417, TMT_HEIGHT,            SIZE,   // custom window prop: size of part (min. window,
         2418, TMT_GLYPHINDEX,        INT,    // for font-based glyphS_the char index into the font
         2419, TMT_TRUESIZESTRETCHMARK, INT,  // stretch TrueSize image when target exceeds source by this percent
         2420, TMT_MINDPI1,         INT,      // min DPI ImageFile1 was designed for
         2421, TMT_MINDPI2,         INT,      // min DPI ImageFile1 was designed for
         2422, TMT_MINDPI3,         INT,      // min DPI ImageFile1 was designed for
         2423, TMT_MINDPI4,         INT,      // min DPI ImageFile1 was designed for
         2424, TMT_MINDPI5,         INT,      // min DPI ImageFile1 was designed for
        //---- rendering FONT properties ----
         2601, TMT_GLYPHFONT,         FONT,   // the font that the glyph is drawn with
        //---- rendering INTLIST properties ----
        // start with 2801
        //---- rendering FILENAME properties ----
         3001, TMT_IMAGEFILE,         FILENAME,   // the filename of the image (or basename, for mult. images,
         3002, TMT_IMAGEFILE1,        FILENAME,   // multiresolution image file
         3003, TMT_IMAGEFILE2,        FILENAME,   // multiresolution image file
         3004, TMT_IMAGEFILE3,        FILENAME,   // multiresolution image file
         3005, TMT_IMAGEFILE4,        FILENAME,   // multiresolution image file
         3006, TMT_IMAGEFILE5,        FILENAME,   // multiresolution image file
         3007, TMT_STOCKIMAGEFILE,    FILENAME,   // These are the only images that you can call GetThemeBitmap on
         3008, TMT_GLYPHIMAGEFILE,    FILENAME,   // the filename for the glyph image
        //---- rendering STRING properties ----
         3201, TMT_TEXT,              STRING,
        //---- rendering POSITION (x and y values, properties ----
         3401, TMT_OFFSET,            POSITION,   // for window part layout
         3402, TMT_TEXTSHADOWOFFSET,  POSITION,   // where char shadows are drawn, relative to orig. chars
         3403, TMT_MINSIZE,           POSITION,   // min dest rect than ImageFile was designed for
         3404, TMT_MINSIZE1,          POSITION,   // min dest rect than ImageFile1 was designed for
         3405, TMT_MINSIZE2,          POSITION,   // min dest rect than ImageFile2 was designed for
         3406, TMT_MINSIZE3,          POSITION,   // min dest rect than ImageFile3 was designed for
         3407, TMT_MINSIZE4,          POSITION,   // min dest rect than ImageFile4 was designed for
         3408, TMT_MINSIZE5,          POSITION,   // min dest rect than ImageFile5 was designed for
         3409, TMT_NORMALSIZE,        POSITION,   // size of dest rect that exactly source
        //---- rendering MARGIN properties ----
         3601, TMT_SIZINGMARGINS_    MARGINS,    // margins used for 9-grid sizing
         3602, TMT_CONTENTMARGINS_   MARGINS,    // margins that define where content can be placed
         3603, TMT_CAPTIONMARGINS_   MARGINS,    // margins that define where caption text can be placed
        //---- rendering COLOR properties ----
         3801, TMT_BORDERCOLOR,      COLOR,       // color of borders for BorderFill 
         3802, TMT_FILLCOLOR,        COLOR,       // color of bg fill 
         3803, TMT_TEXTCOLOR,        COLOR,       // color text is drawn in
         3804, TMT_EDGELIGHTCOLOR,     COLOR,     // edge color
         3805, TMT_EDGEHIGHLIGHTCOLOR, COLOR,     // edge color
         3806, TMT_EDGESHADOWCOLOR,    COLOR,     // edge color
         3807, TMT_EDGEDKSHADOWCOLOR,  COLOR,     // edge color
         3808, TMT_EDGEFILLCOLOR,  COLOR,         // edge color
         3809, TMT_TRANSPARENTCOLOR, COLOR,       // color of pixels that are treated as transparent (not drawn,
         3810, TMT_GRADIENTCOLOR1,   COLOR,       // first color in gradient
         3811, TMT_GRADIENTCOLOR2,   COLOR,       // second color in gradient
         3812, TMT_GRADIENTCOLOR3,   COLOR,       // third color in gradient
         3813, TMT_GRADIENTCOLOR4,   COLOR,       // forth color in gradient
         3814, TMT_GRADIENTCOLOR5,   COLOR,       // fifth color in gradient
         3815, TMT_SHADOWCOLOR,      COLOR,       // color of text shadow
         3816, TMT_GLOWCOLOR,        COLOR,       // color of glow produced by DrawThemeIcon
         3817, TMT_TEXTBORDERCOLOR,  COLOR,       // color of text border
         3818, TMT_TEXTSHADOWCOLOR,  COLOR,       // color of text shadow
         3819, TMT_GLYPHTEXTCOLOR,        COLOR,  // color that font-based glyph is drawn with
         3820, TMT_GLYPHTRANSPARENTCOLOR, COLOR,  // color of transparent pixels in GlyphImageFile
         3821, TMT_FILLCOLORHINT, COLOR,          // hint about fill color used (for custom controls,
         3822, TMT_BORDERCOLORHINT, COLOR,        // hint about border color used (for custom controls,
         3823, TMT_ACCENTCOLORHINT, COLOR,        // hint about accent color used (for custom controls,
        //---- rendering enum properties (must be declared in TM_ENUM section above, ----
         4001, TMT_BGTYPE,           ENUM,        // basic drawing type for each part
         4002, TMT_BORDERTYPE,       ENUM,        // type of border for BorderFill parts
         4003, TMT_FILLTYPE,         ENUM,        // fill shape for BorderFill parts
         4004, TMT_SIZINGTYPE,       ENUM,        // how to size ImageFile parts
         4005, TMT_HALIGN,           ENUM,        // horizontal alignment for TRUESIZE parts & glyphs
         4006, TMT_CONTENTALIGNMENT, ENUM,        // custom window prop: how text is aligned in caption
         4007, TMT_VALIGN,           ENUM,        // horizontal alignment for TRUESIZE parts & glyphs
         4008, TMT_OFFSETTYPE,       ENUM,        // how window part should be placed
         4009, TMT_ICONEFFECT,       ENUM,        // type of effect to use with DrawThemeIcon
         4010, TMT_TEXTSHADOWTYPE,   ENUM,        // type of shadow to draw with text
         4011, TMT_IMAGELAYOUT,      ENUM,        // how multiple images are arranged (horz. or vert.,
         4012, TMT_GLYPHTYPE,             ENUM,   // controls type of glyph in imagefile objects
         4013, TMT_IMAGESELECTTYPE,       ENUM,   // controls when to select from IMAGEFILE1...IMAGEFILE5
         4014, TMT_GLYPHFONTSIZINGTYPE,   ENUM,   // controls when to select a bigger/small glyph font size
         4015, TMT_TRUESIZESCALINGTYPE,   ENUM,   // controls how TrueSize image is scaled
        //---- custom properties (used only by controls/shell, ----
         5001, TMT_USERPICTURE,           BOOL,
         5002, TMT_DEFAULTPANESIZE,       RECT,
         5003, TMT_BLENDCOLOR,            COLOR,
    }
    */
    #endregion

    #region THEME Parts and states
    //---------------------------------------------------------------------------------------
    //   "Window" (i.e., non-client, Parts & States
    //---------------------------------------------------------------------------------------

    internal enum WINDOWPARTS
    {
        WP_CAPTION = 1,
        WP_SMALLCAPTION,
        WP_MINCAPTION,
        WP_SMALLMINCAPTION,
        WP_MAXCAPTION,
        WP_SMALLMAXCAPTION,
        WP_FRAMELEFT,
        WP_FRAMERIGHT,
        WP_FRAMEBOTTOM,
        WP_SMALLFRAMELEFT,
        WP_SMALLFRAMERIGHT,
        WP_SMALLFRAMEBOTTOM,
        //---- window frame buttons ----
        WP_SYSBUTTON,
        WP_MDISYSBUTTON,
        WP_MINBUTTON,
        WP_MDIMINBUTTON,
        WP_MAXBUTTON,
        WP_CLOSEBUTTON,
        WP_SMALLCLOSEBUTTON,
        WP_MDICLOSEBUTTON,
        WP_RESTOREBUTTON,
        WP_MDIRESTOREBUTTON,
        WP_HELPBUTTON,
        WP_MDIHELPBUTTON,
        //---- scrollbars 
        WP_HORZSCROLL,
        WP_HORZTHUMB,
        WP_VERTSCROLL,
        WP_VERTTHUMB,
        //---- dialog ----
        WP_DIALOG,
        //---- hit-test templates ---
        WP_CAPTIONSIZINGTEMPLATE,
        WP_SMALLCAPTIONSIZINGTEMPLATE,
        WP_FRAMELEFTSIZINGTEMPLATE,
        WP_SMALLFRAMELEFTSIZINGTEMPLATE,
        WP_FRAMERIGHTSIZINGTEMPLATE,
        WP_SMALLFRAMERIGHTSIZINGTEMPLATE,
        WP_FRAMEBOTTOMSIZINGTEMPLATE,
        WP_SMALLFRAMEBOTTOMSIZINGTEMPLATE,
    }

    internal enum FRAME
    {
        FS_ACTIVE = 1,
        FS_INACTIVE,
    }

    internal enum CAPTION
    {
        CS_ACTIVE = 1,
        CS_INACTIVE,
        CS_DISABLED,
    }

    internal enum MAXCAPTION
    {
        MXCS_ACTIVE = 1,
        MXCS_INACTIVE,
        MXCS_DISABLED,
    }

    internal enum MINCAPTION
    {
        MNCS_ACTIVE = 1,
        MNCS_INACTIVE,
        MNCS_DISABLED,
    }

    internal enum HORZSCROLL
    {
        HSS_NORMAL = 1,
        HSS_HOT,
        HSS_PUSHED,
        HSS_DISABLED,
    }

    internal enum HORZTHUMB
    {
        HTS_NORMAL = 1,
        HTS_HOT,
        HTS_PUSHED,
        HTS_DISABLED,
    }

    internal enum VERTSCROLL
    {
        VSS_NORMAL = 1,
        VSS_HOT,
        VSS_PUSHED,
        VSS_DISABLED,
    }

    internal enum VERTTHUMB
    {
        VTS_NORMAL = 1,
        VTS_HOT,
        VTS_PUSHED,
        VTS_DISABLED,
    }

    internal enum SYSBUTTON
    {
        SBS_NORMAL = 1,
        SBS_HOT,
        SBS_PUSHED,
        SBS_DISABLED,
    }

    internal enum MINBUTTON
    {
        MINBS_NORMAL = 1,
        MINBS_HOT,
        MINBS_PUSHED,
        MINBS_DISABLED,
    }

    internal enum MAXBUTTON
    {
        MAXBS_NORMAL = 1,
        MAXBS_HOT,
        MAXBS_PUSHED,
        MAXBS_DISABLED,
    }

    internal enum RESTOREBUTTON
    {
        RBS_NORMAL = 1,
        RBS_HOT,
        RBS_PUSHED,
        RBS_DISABLED,
    }

    internal enum HELPBUTTON
    {
        HBS_NORMAL = 1,
        HBS_HOT,
        HBS_PUSHED,
        HBS_DISABLED,
    }

    internal enum CLOSEBUTTON
    {
        CBS_NORMAL = 1,
        CBS_HOT,
        CBS_PUSHED,
        CBS_DISABLED,
    }

    //---------------------------------------------------------------------------------------
    //   "Button" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum BUTTONPARTS
    {
        BP_PUSHBUTTON = 1,
        BP_RADIOBUTTON,
        BP_CHECKBOX,
        BP_GROUPBOX,
        BP_USERBUTTON,
    }

    internal enum PUSHBUTTON
    {
        PBS_NORMAL = 1,
        PBS_HOT,
        PBS_PRESSED,
        PBS_DISABLED,
        PBS_DEFAULTED,
    }

    internal enum RADIOBUTTON
    {
        RBS_UNCHECKEDNORMAL = 1,
        RBS_UNCHECKEDHOT,
        RBS_UNCHECKEDPRESSED,
        RBS_UNCHECKEDDISABLED,
        RBS_CHECKEDNORMAL,
        RBS_CHECKEDHOT,
        RBS_CHECKEDPRESSED,
        RBS_CHECKEDDISABLED,
    }

    internal enum CHECKBOX
    {
        CBS_UNCHECKEDNORMAL = 1,
        CBS_UNCHECKEDHOT,
        CBS_UNCHECKEDPRESSED,
        CBS_UNCHECKEDDISABLED,
        CBS_CHECKEDNORMAL,
        CBS_CHECKEDHOT,
        CBS_CHECKEDPRESSED,
        CBS_CHECKEDDISABLED,
        CBS_MIXEDNORMAL,
        CBS_MIXEDHOT,
        CBS_MIXEDPRESSED,
        CBS_MIXEDDISABLED,
    }

    internal enum GROUPBOX
    {
        GBS_NORMAL = 1,
        GBS_DISABLED,
    }

    //---------------------------------------------------------------------------------------
    //   "Rebar" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum REBARPARTS
    {
        RP_GRIPPER = 1,
        RP_GRIPPERVERT,
        RP_BAND,
        RP_CHEVRON,
        RP_CHEVRONVERT,
    }

    internal enum CHEVRON
    {
        CHEVS_NORMAL = 1,
        CHEVS_HOT,
        CHEVS_PRESSED,
    }

    //---------------------------------------------------------------------------------------
    //   "Toolbar" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum TOOLBARPARTS
    {
        TP_BUTTON = 1,
        TP_DROPDOWNBUTTON,
        TP_SPLITBUTTON,
        TP_SPLITBUTTONDROPDOWN,
        TP_SEPARATOR,
        TP_SEPARATORVERT,
    }

    internal enum TOOLBAR
    {
        TS_NORMAL = 1,
        TS_HOT,
        TS_PRESSED,
        TS_DISABLED,
        TS_CHECKED,
        TS_HOTCHECKED,
    }

    //---------------------------------------------------------------------------------------
    //   "Status" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum STATUSPARTS
    {
        SP_PANE = 1,
        SP_GRIPPERPANE,
        SP_GRIPPER,
    }

    //---------------------------------------------------------------------------------------
    //   "Menu" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum MENUPARTS
    {
        MP_MENUITEM = 1,
        MP_MENUDROPDOWN,
        MP_MENUBARITEM,
        MP_MENUBARDROPDOWN,
        MP_CHEVRON,
        MP_SEPARATOR,
    }

    internal enum MENU
    {
        MS_NORMAL = 1,
        MS_SELECTED,
        MS_DEMOTED,
    }

    //---------------------------------------------------------------------------------------
    //   "ListView" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum LISTVIEWPARTS
    {
        LVP_LISTITEM = 1,
        LVP_LISTGROUP,
        LVP_LISTDETAIL,
        LVP_LISTSORTEDDETAIL,
        LVP_EMPTYTEXT,
    }

    internal enum LISTITEM
    {
        LIS_NORMAL = 1,
        LIS_HOT,
        LIS_SELECTED,
        LIS_DISABLED,
        LIS_SELECTEDNOTFOCUS,
    }

    //---------------------------------------------------------------------------------------
    //   "Header" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum HEADERPARTS
    {
        HP_HEADERITEM = 1,
        HP_HEADERITEMLEFT,
        HP_HEADERITEMRIGHT,
        HP_HEADERSORTARROW,
    }

    internal enum HEADERITEM
    {
        HIS_NORMAL = 1,
        HIS_HOT,
        HIS_PRESSED,
    }

    internal enum HEADERITEMLEFT
    {
        HILS_NORMAL = 1,
        HILS_HOT,
        HILS_PRESSED,
    }

    internal enum HEADERITEMRIGHT
    {
        HIRS_NORMAL = 1,
        HIRS_HOT,
        HIRS_PRESSED,
    }

    internal enum HEADERSORTARROW
    {
        HSAS_SORTEDUP = 1,
        HSAS_SORTEDDOWN,
    }

    //---------------------------------------------------------------------------------------
    //   "Progress" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum PROGRESSPARTS
    {
        PP_BAR = 1,
        PP_BARVERT,
        PP_CHUNK,
        PP_CHUNKVERT,
    }

    //---------------------------------------------------------------------------------------
    //   "Tab" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum TABPARTS
    {
        TABP_TABITEM = 1,
        TABP_TABITEMLEFTEDGE,
        TABP_TABITEMRIGHTEDGE,
        TABP_TABITEMBOTHEDGE,
        TABP_TOPTABITEM,
        TABP_TOPTABITEMLEFTEDGE,
        TABP_TOPTABITEMRIGHTEDGE,
        TABP_TOPTABITEMBOTHEDGE,
        TABP_PANE,
        TABP_BODY,
    }

    internal enum TABITEM
    {
        TIS_NORMAL = 1,
        TIS_HOT,
        TIS_SELECTED,
        TIS_DISABLED,
        TIS_FOCUSED,
    }

    internal enum TABITEMLEFTEDGE
    {
        TILES_NORMAL = 1,
        TILES_HOT,
        TILES_SELECTED,
        TILES_DISABLED,
        TILES_FOCUSED,
    }

    internal enum TABITEMRIGHTEDGE
    {
        TIRES_NORMAL = 1,
        TIRES_HOT,
        TIRES_SELECTED,
        TIRES_DISABLED,
        TIRES_FOCUSED,
    }

    internal enum TABITEMBOTHEDGES
    {
        TIBES_NORMAL = 1,
        TIBES_HOT,
        TIBES_SELECTED,
        TIBES_DISABLED,
        TIBES_FOCUSED,
    }

    internal enum TOPTABITEM
    {
        TTIS_NORMAL = 1,
        TTIS_HOT,
        TTIS_SELECTED,
        TTIS_DISABLED,
        TTIS_FOCUSED,
    }

    internal enum TOPTABITEMLEFTEDGE
    {
        TTILES_NORMAL = 1,
        TTILES_HOT,
        TTILES_SELECTED,
        TTILES_DISABLED,
        TTILES_FOCUSED,
    }

    internal enum TOPTABITEMRIGHTEDGE
    {
        TTIRES_NORMAL = 1,
        TTIRES_HOT,
        TTIRES_SELECTED,
        TTIRES_DISABLED,
        TTIRES_FOCUSED,
    }

    internal enum TOPTABITEMBOTHEDGES
    {
        TTIBES_NORMAL = 1,
        TTIBES_HOT,
        TTIBES_SELECTED,
        TTIBES_DISABLED,
        TTIBES_FOCUSED,
    }

    //---------------------------------------------------------------------------------------
    //   "Trackbar" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum TRACKBARPARTS
    {
        TKP_TRACK = 1,
        TKP_TRACKVERT,
        TKP_THUMB,
        TKP_THUMBBOTTOM,
        TKP_THUMBTOP,
        TKP_THUMBVERT,
        TKP_THUMBLEFT,
        TKP_THUMBRIGHT,
        TKP_TICS,
        TKP_TICSVERT,
    }

    internal enum TRACKBAR
    {
        TKS_NORMAL = 1,
    }

    internal enum TRACK
    {
        TRS_NORMAL = 1,
    }

    internal enum TRACKVERT
    {
        TRVS_NORMAL = 1,
    }

    internal enum THUMB
    {
        TUS_NORMAL = 1,
        TUS_HOT,
        TUS_PRESSED,
        TUS_FOCUSED,
        TUS_DISABLED,
    }

    internal enum THUMBBOTTOM
    {
        TUBS_NORMAL = 1,
        TUBS_HOT,
        TUBS_PRESSED,
        TUBS_FOCUSED,
        TUBS_DISABLED,
    }

    internal enum THUMBTOP
    {
        TUTS_NORMAL = 1,
        TUTS_HOT,
        TUTS_PRESSED,
        TUTS_FOCUSED,
        TUTS_DISABLED,
    }

    internal enum THUMBVERT
    {
        TUVS_NORMAL = 1,
        TUVS_HOT,
        TUVS_PRESSED,
        TUVS_FOCUSED,
        TUVS_DISABLED,
    }

    internal enum THUMBLEFT
    {
        TUVLS_NORMAL = 1,
        TUVLS_HOT,
        TUVLS_PRESSED,
        TUVLS_FOCUSED,
        TUVLS_DISABLED,
    }

    internal enum THUMBRIGHT
    {
        TUVRS_NORMAL = 1,
        TUVRS_HOT,
        TUVRS_PRESSED,
        TUVRS_FOCUSED,
        TUVRS_DISABLED,
    }

    internal enum TICS
    {
        TSS_NORMAL = 1,
    }

    internal enum TICSVERT
    {
        TSVS_NORMAL = 1,
    }

    //---------------------------------------------------------------------------------------
    //   "Tooltips" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum TOOLTIPPARTS
    {
        TTP_STANDARD = 1,
        TTP_STANDARDTITLE,
        TTP_BALLOON,
        TTP_BALLOONTITLE,
        TTP_CLOSE,
    }

    internal enum CLOSE
    {
        TTCS_NORMAL = 1,
        TTCS_HOT,
        TTCS_PRESSED,
    }

    internal enum STANDARD
    {
        TTSS_NORMAL = 1,
        TTSS_LINK,
    }

    internal enum BALLOON
    {
        TTBS_NORMAL = 1,
        TTBS_LINK,
    }

    //---------------------------------------------------------------------------------------
    //   "TreeView" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum TREEVIEWPATS
    {
        TVP_TREEITEM = 1,
        TVP_GLYPH,
        TVP_BRANCH,
    }

    internal enum TREEITEM
    {
        TREIS_NORMAL = 1,
        TREIS_HOT,
        TREIS_SELECTED,
        TREIS_DISABLED,
        TREIS_SELECTEDNOTFOCUS,
    }

    internal enum GLYPH
    {
        GLPS_CLOSED = 1,
        GLPS_OPENED,
    }

    //---------------------------------------------------------------------------------------
    //   "Spin" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum SPINPARTS
    {
        SPNP_UP = 1,
        SPNP_DOWN,
        SPNP_UPHORZ,
        SPNP_DOWNHORZ,
    }

    internal enum UP
    {
        UPS_NORMAL = 1,
        UPS_HOT,
        UPS_PRESSED,
        UPS_DISABLED,
    }

    internal enum DOWN
    {
        DNS_NORMAL = 1,
        DNS_HOT,
        DNS_PRESSED,
        DNS_DISABLED,
    }

    internal enum UPHORZ
    {
        UPHZS_NORMAL = 1,
        UPHZS_HOT,
        UPHZS_PRESSED,
        UPHZS_DISABLED,
    }

    internal enum DOWNHORZ
    {
        DNHZS_NORMAL = 1,
        DNHZS_HOT,
        DNHZS_PRESSED,
        DNHZS_DISABLED,
    }

    //---------------------------------------------------------------------------------------
    //   "Page" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum PAGEPARTS
    {
        PGRP_UP = 1,
        PGRP_DOWN,
        PGRP_UPHORZ,
        PGRP_DOWNHORZ,
    }

    //--- Pager uses same states as Spin ---

    //---------------------------------------------------------------------------------------
    //   "Scrollbar" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum SCROLLBARPATS
    {
        SBP_ARROWBTN = 1,
        SBP_THUMBBTNHORZ,
        SBP_THUMBBTNVERT,
        SBP_LOWERTRACKHORZ,
        SBP_UPPERTRACKHORZ,
        SBP_LOWERTRACKVERT,
        SBP_UPPERTRACKVERT,
        SBP_GRIPPERHORZ,
        SBP_GRIPPERVERT,
        SBP_SIZEBOX,
    }

    internal enum ARROWBTN
    {
        ABS_UPNORMAL = 1,
        ABS_UPHOT,
        ABS_UPPRESSED,
        ABS_UPDISABLED,
        ABS_DOWNNORMAL,
        ABS_DOWNHOT,
        ABS_DOWNPRESSED,
        ABS_DOWNDISABLED,
        ABS_LEFTNORMAL,
        ABS_LEFTHOT,
        ABS_LEFTPRESSED,
        ABS_LEFTDISABLED,
        ABS_RIGHTNORMAL,
        ABS_RIGHTHOT,
        ABS_RIGHTPRESSED,
        ABS_RIGHTDISABLED,
    }

    internal enum SCROLLBAR
    {
        SCRBS_NORMAL = 1,
        SCRBS_HOT,
        SCRBS_PRESSED,
        SCRBS_DISABLED,
    }

    internal enum SIZEBOX
    {
        SZB_RIGHTALIGN = 1,
        SZB_LEFTALIGN,
    }

    //---------------------------------------------------------------------------------------
    //   "Edit" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum EDITPARTS
    {
        EP_EDITTEXT = 1,
        EP_CARET,
    }

    internal enum EDITTEXT
    {
        ETS_NORMAL = 1,
        ETS_HOT,
        ETS_SELECTED,
        ETS_DISABLED,
        ETS_FOCUSED,
        ETS_READONLY,
        ETS_ASSIST,
    }

    //---------------------------------------------------------------------------------------
    //   "ComboBox" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum COMBOBOXPATS
    {
        CP_DROPDOWNBUTTON = 1,
    }

    internal enum COMBOBOX
    {
        CBXS_NORMAL = 1,
        CBXS_HOT,
        CBXS_PRESSED,
        CBXS_DISABLED,
    }

    //---------------------------------------------------------------------------------------
    //   "Taskbar Clock" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum CLOCKPARTS
    {
        CLP_TIME = 1,
    }

    internal enum CLOCK
    {
        CLS_NORMAL = 1,
    }

    //---------------------------------------------------------------------------------------
    //   "Tray Notify" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum TRAYNOTIFYPARTS
    {
        TNP_BACKGROUND = 1,
        TNP_ANIMBACKGROUND,
    }

    //---------------------------------------------------------------------------------------
    //   "TaskBar" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum TASKBARPATS
    {
        TBP_BACKGROUNDBOTTOM = 1,
        TBP_BACKGROUNDRIGHT,
        TBP_BACKGROUNDTOP,
        TBP_BACKGROUNDLEFT,
        TBP_SIZINGBARBOTTOM,
        TBP_SIZINGBARRIGHT,
        TBP_SIZINGBARTOP,
        TBP_SIZINGBARLEFT,
    }

    //---------------------------------------------------------------------------------------
    //   "TaskBand" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum TASKBANDPARTS
    {
        TDP_GROUPCOUNT = 1,
        TDP_FLASHBUTTON,
        TDP_FLASHBUTTONGROUPMENU,
    }

    //---------------------------------------------------------------------------------------
    //   "StartPanel" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum STARTPANELPARTS
    {
        SPP_USERPANE = 1,
        SPP_MOREPROGRAMS,
        SPP_MOREPROGRAMSARROW,
        SPP_PROGLIST,
        SPP_PROGLISTSEPARATOR,
        SPP_PLACESLIST,
        SPP_PLACESLISTSEPARATOR,
        SPP_LOGOFF,
        SPP_LOGOFFBUTTONS,
        SPP_USERPICTURE,
        SPP_PREVIEW,
    }

    internal enum MOREPROGRAMSARROW
    {
        SPS_NORMAL = 1,
        SPS_HOT,
        SPS_PRESSED,
    }

    internal enum LOGOFFBUTTONS
    {
        SPLS_NORMAL = 1,
        SPLS_HOT,
        SPLS_PRESSED,
    }

    //---------------------------------------------------------------------------------------
    //   "ExplorerBar" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum EXPLORERBARPARTS
    {
        EBP_HEADERBACKGROUND = 1,
        EBP_HEADERCLOSE,
        EBP_HEADERPIN,
        EBP_IEBARMENU,
        EBP_NORMALGROUPBACKGROUND,
        EBP_NORMALGROUPCOLLAPSE,
        EBP_NORMALGROUPEXPAND,
        EBP_NORMALGROUPHEAD,
        EBP_SPECIALGROUPBACKGROUND,
        EBP_SPECIALGROUPCOLLAPSE,
        EBP_SPECIALGROUPEXPAND,
        EBP_SPECIALGROUPHEAD,
    }

    internal enum HEADERCLOSE
    {
        EBHC_NORMAL = 1,
        EBHC_HOT,
        EBHC_PRESSED,
    }

    internal enum HEADERPIN
    {
        EBHP_NORMAL = 1,
        EBHP_HOT,
        EBHP_PRESSED,
        EBHP_SELECTEDNORMAL,
        EBHP_SELECTEDHOT,
        EBHP_SELECTEDPRESSED,
    }

    internal enum IEBARMENU
    {
        EBM_NORMAL = 1,
        EBM_HOT,
        EBM_PRESSED,
    }

    internal enum NORMALGROUPCOLLAPSE
    {
        EBNGC_NORMAL = 1,
        EBNGC_HOT,
        EBNGC_PRESSED,
    }

    internal enum NORMALGROUPEXPAND
    {
        EBNGE_NORMAL = 1,
        EBNGE_HOT,
        EBNGE_PRESSED,
    }

    internal enum SPECIALGROUPCOLLAPSE
    {
        EBSGC_NORMAL = 1,
        EBSGC_HOT,
        EBSGC_PRESSED,
    }

    internal enum SPECIALGROUPEXPAND
    {
        EBSGE_NORMAL = 1,
        EBSGE_HOT,
        EBSGE_PRESSED,
    }

    //---------------------------------------------------------------------------------------
    //   "TaskBand" Parts & States
    //---------------------------------------------------------------------------------------
    internal enum MENUBANDPARTS
    {
        MDP_NEWAPPBUTTON = 1,
        MDP_SEPERATOR,
    }

    internal enum MENUBAND
    {
        MDS_NORMAL = 1,
        MDS_HOT,
        MDS_PRESSED,
        MDS_DISABLED,
        MDS_CHECKED,
        MDS_HOTCHECKED,
    }
    #endregion

    #region THEME classes
#if WHIDBEY
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#endif
    internal enum CLASSES
    {
        BUTTON = 1,
        CLOCK,
        COMBOBOX,
        EDIT,
        EXPLORERBAR,
        GLOBALS,
        HEADER,
        LISTVIEW,
        MENU,
        MENUBAND,
        PAGE,
        PROGRESS,
        REBAR,
        SCROLLBAR,
        SPIN,
        STARTPANEL,
        STATUS,
        TAB,
        TASKBAND,
        TASKBAR,
        TOOLBAR,
        TOOLTIP,
        TRACKBAR,
        TRAYNOTIFY,
        TREEVIEW,
        WINDOW,
    }
    #endregion

    internal class XPTheme : IDisposable
    {
        private IntPtr _hdc = IntPtr.Zero;
        private Hashtable _themeHandles = new Hashtable();
        static private bool s_available = false;

        static XPTheme()
        {
            try
            {
                s_available = OSFeature.Feature.IsPresent(OSFeature.Themes);
                //if (s_available)
                //    s_available = IsAppThemed() && IsThemeActive();
            }
            catch
            {
                // fxcop note: System.DllNotFoundException is thrown on w2k,
                // but that is not documented in msdn, so a safer way seems
                // to be to catch everything.
                s_available = false;
            }
        }

        void IDisposable.Dispose()
        {
            foreach (DictionaryEntry de in _themeHandles)
            {
                CloseThemeData((IntPtr)de.Value);
            }
            _themeHandles.Clear();
        }

        static internal bool Available
        {
            get
            {
                try
                {
                    return s_available && IsAppThemed() && IsThemeActive();
                }
                catch
                {
                    return false;
                }
            }
        }

        static internal string ThemeName
        {
            get
            {
                if (Available)
                {
                    StringBuilder builder1 = new StringBuilder(0x200);
                    XPTheme.GetCurrentThemeName(null, 0, builder1, builder1.Capacity, null, 0);
                    return builder1.ToString();
                }
                return string.Empty;
            }
        }

        private IntPtr GetThemeHandle(CLASSES controlName)
        {
            // get theme handle
            if (_themeHandles.ContainsKey(controlName))
                return (IntPtr)_themeHandles[controlName];

            IntPtr hTheme = OpenThemeData(IntPtr.Zero, controlName.ToString());
            _themeHandles.Add(controlName, hTheme);
            return hTheme;
        }

        internal bool IsThemePartDefined(CLASSES controlName, int btnPart, int btnState)
        {
            IntPtr hTheme = GetThemeHandle(controlName); // get theme handle
            return IsThemePartDefined(hTheme, btnPart, btnState);
        }

        internal Rectangle GetBackgroundContentRect(CLASSES controlClass, IntPtr dc, int iPartId, int iStateId, Rectangle bounds)
        {
            IntPtr hTheme = GetThemeHandle(controlClass);
            Win32.RECT rcBounds = new Win32.RECT(bounds);
            Win32.RECT rcResult = new Win32.RECT();
            GetThemeBackgroundContentRect(hTheme, dc, iPartId, iStateId, ref rcBounds, ref rcResult);
            return rcResult.ToRectangle();
        }

        internal void DrawBackground(CLASSES controlClass, IntPtr dc, int iPartId, int iStateId, Rectangle bounds)
        {
            IntPtr hTheme = GetThemeHandle(controlClass);
            Win32.RECT rcBounds = new Win32.RECT(bounds);
            DrawThemeBackground(hTheme, dc, iPartId, iStateId, ref rcBounds, ref rcBounds);
        }

        internal void DrawClipBackground(CLASSES controlClass, IntPtr dc, int iPartId, int iStateId, Rectangle bounds, Rectangle clip)
        {
            IntPtr hTheme = GetThemeHandle(controlClass);
            Win32.RECT rcBounds = new Win32.RECT(bounds);
            Win32.RECT rcClip = new Win32.RECT(clip);
            DrawThemeBackground(hTheme, dc, iPartId, iStateId, ref rcBounds, ref rcClip);
        }

        internal void DrawText(CLASSES controlName, IntPtr dc, int iPartId, int iStateId, string s, int dwTextFlags, Rectangle bounds)
        {
            IntPtr hTheme = GetThemeHandle(controlName);
            Win32.RECT rcBounds = new Win32.RECT(bounds);
            DrawThemeText(hTheme, dc, iPartId, iStateId, s, s.Length, dwTextFlags, 0, ref rcBounds);
        }

        internal static void DrawParentBackground(IntPtr hwnd, IntPtr dc, Rectangle bounds)
        {
            Win32.RECT rcBounds = new Win32.RECT(bounds);
            DrawThemeParentBackground(hwnd, dc, ref rcBounds);
        }

        internal Size GetPartSize(Graphics g, int btnPart, int btnState, CLASSES controlName)
        {
            if (!s_available)
                return Size.Empty;
            _hdc = g.GetHdc();
            Size ret = Size.Empty;
            try
            {
                // get theme handle
                IntPtr hTheme = GetThemeHandle(controlName);
                Win32.SIZE rcSize;
                rcSize.cx = 0;
                rcSize.cy = 0;
                // TS_MIN, TS_TRUE, TS_DRAW
                GetThemePartSize(hTheme, _hdc, btnPart, btnState, IntPtr.Zero, (int)THEMESIZE.TS_TRUE, ref rcSize);
                ret = new Size(rcSize.cx, rcSize.cy);
            }
            finally
            {
                g.ReleaseHdc(_hdc);
            }
            return ret;
        }

        internal void DrawPartState(Graphics g, Rectangle r, int btnPart, int btnState, CLASSES controlName)
        {
            if (!s_available)
                return;

            _hdc = g.GetHdc();
            try
            {
                //if (IsThemeBackgroundPartiallyTransparent(btnPart, btnState, controlName))
                //DrawParentBackground(hwnd, _hdc, r);
                DrawBackground(controlName, _hdc, btnPart, btnState, r);
            }
            finally
            {
                g.ReleaseHdc(_hdc);
            }
        }

        internal void DrawClipPartState(Graphics g, Rectangle r, Rectangle clip, int btnPart, int btnState, CLASSES controlName)
        {
            if (!s_available)
                return;

            _hdc = g.GetHdc();
            try
            {
                //if (IsThemeBackgroundPartiallyTransparent(btnPart, btnState, controlName))
                //DrawParentBackground(hwnd, _hdc, r);
                DrawClipBackground(controlName, _hdc, btnPart, btnState, r, clip);
            }
            finally
            {
                g.ReleaseHdc(_hdc);
            }
        }

        internal bool IsThemeBackgroundPartiallyTransparent(int btnPart, int btnState, CLASSES className)
        {
            if (!s_available)
                return false;

            IntPtr hTheme = GetThemeHandle(className);
            return IsThemeBackgroundPartiallyTransparent(hTheme, btnPart, btnState);
        }

        internal void DrawEdge(Graphics g, Rectangle r, int btnPart, int btnState, uint edge, uint flags, CLASSES controlName)
        {
            if (!s_available)
                return;
            _hdc = g.GetHdc();
            try
            {
                // get theme handle
                IntPtr hTheme = GetThemeHandle(controlName); // get theme handle
                Win32.RECT rcDraw = new Win32.RECT(r);
                DrawThemeEdge(hTheme, _hdc, btnPart, btnState, ref rcDraw, edge, flags, ref rcDraw);
            }
            finally
            {
                g.ReleaseHdc(_hdc);
            }
        }

        internal static Color GetSystemColor(int index)
        {
            return ColorTranslator.FromWin32(Win32.GetSysColor(index));
        }

        internal Color GetColorFromTheme(int part, int state, int prop, CLASSES controlName)
        {
            int color = 0;
            // get theme handle
            IntPtr hTheme = GetThemeHandle(controlName); // get theme handle
            GetThemeColor(hTheme, part, state, prop, ref color);
            return ColorTranslator.FromWin32(color);
        }

        private static int Encode(int alpha, int red, int green, int blue)
        {
            return ((((red << 0x10) | (green << 8)) | blue) | (alpha << 0x18));
        }

        #region WindowsXP themes functions

        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "IsThemeActive")]
        internal static extern bool IsThemeActive();

        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "IsAppThemed")]
        internal static extern bool IsAppThemed();

        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "GetCurrentThemeName")]
        public static extern int GetCurrentThemeName(
            StringBuilder pszThemeFileName,
            int dwMaxNameChars,
            StringBuilder pszColorBuff,
            int dwMaxColorChars,
            StringBuilder pszSizeBuff,
            int cchMaxSizeChars);

        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "OpenThemeData")]
        internal static extern IntPtr OpenThemeData(
            IntPtr hwnd,
            string pszClassList);

        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "CloseThemeData")]
        internal static extern IntPtr CloseThemeData(
            IntPtr hTheme);

        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "IsThemePartDefined")]
        internal static extern bool IsThemePartDefined(
            IntPtr hTheme,
            int iPartId,
            int iStateId);

        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "DrawThemeBackground")]
        internal static extern IntPtr DrawThemeBackground(
            IntPtr hTheme,
            IntPtr hdc,
            int iPartId,
            int iStateId,
            ref Win32.RECT pRect,
            ref Win32.RECT pClipRect);

        // Draws the part of a parent control that is covered by a partially-transparent or alpha-blended child control.
        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "DrawThemeParentBackground")]
        internal static extern IntPtr DrawThemeParentBackground(
            IntPtr hwnd,
            IntPtr hdc,
            ref Win32.RECT pRect);

        // Retrieves whether the background specified by the visual style has transparent pieces or alpha-blended pieces.
        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "IsThemeBackgroundPartiallyTransparent")]
        internal static extern bool IsThemeBackgroundPartiallyTransparent(
            IntPtr hTheme,
            int iPartId,
            int iStateId);

        // Calculates the original size of the part defined by a visual style.
        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "GetThemePartSize")]
        internal static extern IntPtr GetThemePartSize(
            IntPtr hTheme,
            IntPtr hdc,
            int iPartId,
            int iStateId,
            IntPtr zero,
            int eSize,
            ref Win32.SIZE size);

        [DllImport("UxTheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "DrawThemeText")]
        internal extern static int DrawThemeText(
            IntPtr hTheme,
            IntPtr hDC,
            int iPartId,
            int iStateId,
            string pszText,
            int iCharCount,
            int dwTextFlag,
            int dwTextFlags2,
            ref Win32.RECT pRect);

        // Draws one or more edges defined by the visual style of a rectangle.
        [ DllImport("UxTheme.dll", ExactSpelling=true, CharSet=CharSet.Unicode, EntryPoint="DrawThemeEdge") ]
        internal static extern bool DrawThemeEdge(
            IntPtr hTheme,
            IntPtr hDC,
            int iPartId,
            int iStateId,
            ref Win32.RECT pRect,
            uint edge,
            uint flags,
            ref Win32.RECT pContentRect);

        // Retrieves the size of the content area for the background defined by the visual style.
        [DllImport("UxTheme.dll", CharSet = CharSet.Unicode)]
        internal extern static int GetThemeBackgroundContentRect(
            IntPtr hTheme,
            IntPtr hDC,
            int iPartId,
            int iStateId,
            ref Win32.RECT pBoundingRect,
            ref Win32.RECT pContentRect);

        [DllImport("UxTheme.dll")]
        internal extern static int GetThemeColor(
            IntPtr hTheme,
            int iPartId,
            int iStateId,
            int iPropId,
            ref int ColorRef);

        [DllImport("UxTheme.dll")]
        internal extern static int GetThemeBackgroundRegion(
            IntPtr hTheme,
            IntPtr hdc,
            int iPartId,
            int iStateId,
            ref Win32.RECT pRect,
            ref IntPtr region);

        
        //        [ DllImport("UxTheme.dll", ExactSpelling=true, CharSet=CharSet.Unicode, EntryPoint="DrawThemeIcon") ]
        //        private static extern bool DrawThemeIcon(IntPtr hTheme, IntPtr hdc, int part, int state, ref Win32.RECT pRect, IntPtr himl, int iImageIndex);

        #endregion
    }
}
