using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.ComponentModel.Design;
using System.Reflection;
using System.Threading;

#if TRUEGRID_2
namespace C1.Win.C1TrueDBGrid
#elif C1COMMAND2005
namespace C1.Win.C1Command
#elif C1Input2005
namespace C1.Win.C1Input
#elif PREVIEW_2
namespace C1.C1Preview
#elif PREVIEW_1
namespace C1.C1PrintDocument
#elif FLEXGRID_2
namespace C1.Win.C1FlexGrid
#else
namespace C1.Util
#endif
{
    public class FindTypeByName
    {
        static public Type Find(string typeName)
        {
            Type result = Type.GetType(typeName, false);
            if (result != null)
                return result;
            Assembly[] loadedList = Thread.GetDomain().GetAssemblies();
            for (int i = 0; i < loadedList.Length; i++)
            {
                result = loadedList[i].GetType(typeName, false);
                if (result != null)
                    return result;
            }
            Assembly asm = Assembly.GetExecutingAssembly();
            if (asm == null)
                return null;
            return FindInReferencedAssemblies(typeName, asm);
        }

        static private Type FindInReferencedAssemblies(string typeName, Assembly parent)
        {
            AssemblyName[] asmNames = parent.GetReferencedAssemblies();
            Assembly[] loadedList = Thread.GetDomain().GetAssemblies();
            ArrayList newLoaded = new ArrayList();
            string asmFullName;
            Assembly asm = null;
            bool found;
            Type result = null;
            int i, j;
            for (i = 0; i < asmNames.Length; i++)
            {
                asmFullName = asmNames[i].FullName;
                found = false;
                for (j = 0; j < loadedList.Length; j++)
                    if (asmFullName == loadedList[j].FullName)
                    {
                        found = true;
                        break;
                    }
                if (!found)
                {
                    asm = Assembly.Load(asmNames[i]);
                    newLoaded.Add(asm);
                    result = asm.GetType(typeName, false);
                    if (result != null)
                        return result;
                }
            }
            for (i = 0; i < newLoaded.Count; i++)
            {
                result = FindInReferencedAssemblies(typeName, (Assembly)newLoaded[i]);
                if (result != null)
                    return result;
            }
            return null;
        }
    }
}
