using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Text;

using C1.Util.Localization;

#if TRUEGRID_2
namespace C1.Win.C1TrueDBGrid
#elif C1COMMAND2005
namespace C1.Win.C1Command
#elif C1Input2005
namespace C1.Win.C1Input
#elif PREVIEW_2
namespace C1.C1Preview
#elif C1Spell2005
namespace C1.Win.C1Spell
#elif PREVIEW_1
namespace C1.C1PrintDocument
#else
namespace C1.Util
#endif
{
    #region Internal helper classes
    internal class UIString
	{
		public int Ordinal = 0;
		public string Value = String.Empty;
		public string DefaultValue = String.Empty;
		public string Description = String.Empty;

		public UIString(int ordinal, string value, string description)
		{
			Ordinal = ordinal;
			Value = DefaultValue = value;
			Description = description;
		}
		public bool IsDefault
		{
			get { return Value == DefaultValue; }
		}
		public void Reset()
		{
			Value = DefaultValue;
		}
	}

	internal class UisSorter : IComparable
	{
		public object Key;
		public string StrKey;
		public UIString Uis;

		public UisSorter(object key, string strKey, UIString uis)
		{
			Key = key;
			StrKey = strKey;
			Uis = uis;
		}
		public int CompareTo(object obj)
		{
			UisSorter other = obj as UisSorter;
			if (other == null)
				return 1;
			else if (Uis.Ordinal < other.Uis.Ordinal)
				return -1;
			else if (Uis.Ordinal > other.Uis.Ordinal)
				return 1;
			return string.Compare(StrKey, other.StrKey, false, CultureInfo.InvariantCulture);
		}
    }
    #endregion

    /// <summary>
    /// Represents a handler for an <see cref="UIStrings"/> item related event.
    /// </summary>
	public delegate void UIStringsItemEventHandler(object sender, UIStringsItemEventArgs e);

    /// <summary>
    /// Provides data for an <see cref="UIStrings"/> item related event.
    /// </summary>
	public class UIStringsItemEventArgs : EventArgs
    {
        #region Private data
        private object _key;
		private string _value;
		private bool _isDefault;
		private string _description;
        #endregion

        #region ctor
        internal UIStringsItemEventArgs(object key, string value, bool isDefault, string description)
		{
			_key = key;
			_value = value;
			_isDefault = isDefault;
			_description = description;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Gets key of the item being added or changed.
        /// </summary>
        /// <value>The key.</value>
		[C1Description("The key of the item being added or changed.")]
		public object Key
		{
			get { return _key; }
		}
        /// <summary>
        /// Gets the string value.
        /// </summary>
        /// <value>The value.</value>
		[C1Description("The new string value.")]
		public string Value
		{
			get { return _value; }
		}
        /// <summary>
        /// Gets a value indicating whether this instance is default.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is default; otherwise, <c>false</c>.
        /// </value>
		[C1Description("True if the value is equal to the default string.")]
		public bool IsDefault
		{
			get { return _isDefault; }
		}
        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
		[C1Description("The description provided for the item.")]
		public string Description
		{
			get { return _description; }
        }
        #endregion
    }

    /// <summary>
    /// Represents a collection of end user visible UI strings.
    /// </summary>
	[TypeConverter(typeof(UIStrings.TypeConverter))]
	public class UIStrings
    {
        #region Private data
        private Hashtable _items = new Hashtable();
        // private string _description = "Count = {0}";
        private string _description = "(UIStrings)";
		private Type _indexer = null;
		private UIStringsItemEventHandler _onItemAdded;
		private UIStringsItemEventHandler _onItemChanged;
		private EventHandler _onCollectionChanged;
		private ArrayList _sorter = new ArrayList();
		private bool _bunchUpdate = false;
		private bool _modified = false;
		private const string c_keyValueFmt = "{0}:{1}";
		private const char c_delim = ':';
		private const char c_escape = '`';
        #endregion // Private data

        #region Public properties
        /// <summary>
        /// Gets or sets the string value for the specified key.
        /// </summary>
        /// <param name="key">The key of the string.</param>
        /// <returns>The string corresponding to the specified key.</returns>
        [C1Description("Gets or sets the string value for the specified key.")]
        public string this[object key]
        {
            get { return ((UIString)_items[ValidateKey(key)]).Value; }
            set
            {
                key = ValidateKey(key);
                UIString us = (UIString)_items[key];
                if (us.Value != value)
                {
                    us.Value = value;
                    OnItemChanged(new UIStringsItemEventArgs(key, value, us.IsDefault, us.Description));
                    if (!_bunchUpdate)
                        OnCollectionChanged(EventArgs.Empty);
                    else
                        _modified = true;
                }
            }
        }

        /// <summary>
        /// Used for serialization of strings.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Localizable(true)]
        [C1Description("Used for serialization of strings.")]
        public string[] Content
        {
            get { return SaveToStringArray(); }
            set { RestoreFromStringArray(value); }
        }

        /// <summary>
        /// Gets or sets the description shown in Properties window in the IDE.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [C1Description("This text is shown in Properties window in the IDE.")]
        public string Description
        {
            // get { return string.Format(_description, this.Count); }
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Gets the number of elements contained in the collection.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [C1Description("Gets the number of elements contained in the collection.")]
        public int Count
        {
            get { return _items.Count; }
        }

        /// <summary>
        /// For internal use.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Hashtable Items
        {
            get { return _items; }
        }

        /// <summary>
        /// For internal use.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ArrayList Sorter
        {
            get { return _sorter; }
        }
        #endregion // Public properties

        #region Public methods
        /// <summary>
        /// Adds a string to the collection, specifying the ordinal.
		/// </summary>
		/// <param name="key">The key of the string.</param>
        /// <param name="ordinal">The ordinal of the string.</param>
        /// <param name="value">The string.</param>
        /// <param name="description">The description of the string.</param>
        public void Add(object key, int ordinal, string value, string description)
		{
			if (_indexer == null)
				_indexer = key.GetType();
			else if (!_indexer.Equals(key.GetType()))
				throw new ArgumentException(C1Localizer.GetString("Invalid key type: ") + key.GetType().ToString());
			string strKey = ConvertToStr(key);
			if (strKey == String.Empty)
				throw new ArgumentException(C1Localizer.GetString("key"), 
                    C1Localizer.GetString("Key must not be empty."));
			UIString us = new UIString(ordinal, value, description);
			_items.Add(key, us);
			UisSorter s = new UisSorter(key, strKey, us);
			_sorter.Insert(BinarySearch(s), s);
			OnItemAdded(new UIStringsItemEventArgs(key, value, true, description));
			if (!_bunchUpdate)
				OnCollectionChanged(EventArgs.Empty);
			else
				_modified = true;
		}

        /// <summary>
        /// Adds a string to the collection in alphabetical order.
        /// </summary>
        /// <param name="key">The key of the string.</param>
        /// <param name="value">The string.</param>
        /// <param name="description">The description of the string.</param>
        public void Add(object key, string value, string description)
		{
			Add(key, 0, value, description);
		}

        /// <summary>
        /// Adds a string to the collection, preserving the order.
        /// </summary>
        /// <param name="key">The key of the string.</param>
        /// <param name="value">The string.</param>
        /// <param name="description">The description of the string.</param>
        public void AddInOrder(object key, string value, string description)
		{
			Add(key, _items.Count, value, description);
        }

        /// <summary>
        /// Sets all strings in collection to their default values.
        /// </summary>
        public void Reset()
        {
            bool prevBunchUpdate = _bunchUpdate;
            _bunchUpdate = true;
            try
            {
                foreach (DictionaryEntry o in _items)
                {
                    UIString us = (UIString)o.Value;
                    if (!us.IsDefault)
                    {
                        us.Reset();
                        OnItemChanged(new UIStringsItemEventArgs(o.Key, us.Value, true, us.Description));
                        _modified = true;
                    }
                }
            }
            finally
            {
                _bunchUpdate = prevBunchUpdate;
            }
            if (!_bunchUpdate && _modified)
            {
                _modified = false;
                OnCollectionChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Indicates whether any of the strings in the current collection
        /// have non-default values.
        /// </summary>
        /// <returns><c>true</c> if any of the strings have non-default values, <c>false</c> otherwise.</returns>
        public bool ShouldSerialize()
        {
            foreach (DictionaryEntry o in _items)
                if (!((UIString)o.Value).IsDefault)
                    return true;
            return false;
        }

        /// <summary>
        /// Tests whether a string in the collection has default value.
        /// </summary>
        /// <param name="key">The key of the string to test.</param>
        /// <returns><c>true</c> if the string has default value, <c>false</c> otherwise.</returns>
        public bool IsDefault(object key)
        {
            return ((UIString)_items[ValidateKey(key)]).IsDefault;
        }

        /// <summary>
        /// Returns the description of a string.
        /// </summary>
        /// <param name="key">The key of the string to get the description of.</param>
        /// <returns>The string's description</returns>
        public string GetDescription(object key)
        {
            return ((UIString)_items[ValidateKey(key)]).Description;
        }

        /// <summary>
        /// Resets a string to its default value.
        /// </summary>
        /// <param name="key">The key of the string to reset.</param>
        public void Reset(object key)
        {
            key = ValidateKey(key);
            UIString us = (UIString)_items[key];
            if (!us.IsDefault)
            {
                us.Reset();
                OnItemChanged(new UIStringsItemEventArgs(key, us.Value, true, us.Description));
                if (!_bunchUpdate)
                    OnCollectionChanged(EventArgs.Empty);
                else
                    _modified = true;
            }
        }

        /// <summary>
        /// For internal use.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public object ConvertFromStr(string key)
        {
            return TypeDescriptor.GetConverter(_indexer).ConvertFromInvariantString(key);
        }

        /// <summary>
        /// For internal use.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string ConvertToStr(object key)
        {
            return TypeDescriptor.GetConverter(_indexer).ConvertToInvariantString(key);
        }

        /// <summary>
        /// Returns the key of an item with the specified index.
        /// </summary>
        /// <param name="index">The item index.</param>
        /// <returns>The item's key.</returns>
        public object GetKeyAt(int index)
        {
            return ((UisSorter)_sorter[index]).Key;
        }

        /// <summary>
        /// Gets the string by its index.
        /// </summary>
        /// <param name="index">The string index.</param>
        /// <returns>The string.</returns>
        public string GetValueAt(int index)
        {
            return ((UisSorter)_sorter[index]).Uis.Value;
        }

        /// <summary>
        /// Sets the value of a string with the specified index.
        /// </summary>
        /// <param name="index">The string index.</param>
        /// <param name="value">The new string value.</param>
        public void SetValueAt(int index, string value)
        {
            UisSorter s = (UisSorter)_sorter[index];
            UIString us = s.Uis;
            if (us.Value != value)
            {
                us.Value = value;
                OnItemChanged(new UIStringsItemEventArgs(s.Key, value, us.IsDefault, us.Description));
                if (!_bunchUpdate)
                    OnCollectionChanged(EventArgs.Empty);
                else
                    _modified = true;
            }
        }

        /// <summary>
        /// For internal use.
        /// </summary>
        /// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string[] SaveValuesToStringArray()
        {
            string[] result = new string[_sorter.Count];
            for (int i = 0; i < _sorter.Count; i++)
                result[i] = ((UisSorter)_sorter[i]).Uis.Value;
            return result;
        }

        /// <summary>
        /// For internal use.
        /// </summary>
        /// <param name="values"></param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void RestoreValuesFromStringArray(string[] values)
        {
            if (values == null)
            {
                Reset();
                return;
            }
            if (values.Length > _items.Count)
                throw new ArgumentException(C1Localizer.GetString("values"),
                    C1Localizer.GetString("Values array contains too many items."));
            bool prevBunchUpdate = _bunchUpdate;
            _bunchUpdate = true;
            try
            {
                Reset();
                for (int i = 0; i < values.Length; i++)
                {
                    UisSorter s = (UisSorter)_sorter[i];
                    UIString us = s.Uis;
                    if (us.Value != values[i])
                    {
                        us.Value = values[i];
                        OnItemChanged(new UIStringsItemEventArgs(s.Key, us.Value, us.IsDefault, us.Description));
                        _modified = true;
                    }
                }
            }
            finally
            {
                _bunchUpdate = prevBunchUpdate;
            }
            if (!_bunchUpdate && _modified)
            {
                _modified = false;
                OnCollectionChanged(EventArgs.Empty);
            }
        }
        #endregion // Public methods

        #region Public events
        /// <summary>
        /// Occurs when a new item is added to the collection.
        /// </summary>
        [Browsable(false)]
        [C1Description("Occurs when a new item is added to the collection.")]
        public event UIStringsItemEventHandler ItemAdded
        {
            add
            {
                _onItemAdded = (UIStringsItemEventHandler)Delegate.Combine(_onItemAdded, value);
            }
            remove
            {
                _onItemAdded = (UIStringsItemEventHandler)Delegate.Remove(_onItemAdded, value);
            }
        }

        /// <summary>
        /// Fires the <see cref="ItemAdded"/> event.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected virtual void OnItemAdded(UIStringsItemEventArgs e)
        {
            if (_onItemAdded != null)
                _onItemAdded(this, e);
        }

        /// <summary>
        /// Occurs when an item in the collection is changed.
        /// </summary>
        [Browsable(false)]
        [C1Description("Occurs when an item in the collection is changed.")]
        public event UIStringsItemEventHandler ItemChanged
        {
            add
            {
                _onItemChanged = (UIStringsItemEventHandler)Delegate.Combine(_onItemChanged, value);
            }
            remove
            {
                _onItemChanged = (UIStringsItemEventHandler)Delegate.Remove(_onItemChanged, value);
            }
        }

        /// <summary>
        /// Fires the <see cref="ItemChanged"/> event.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected virtual void OnItemChanged(UIStringsItemEventArgs e)
        {
            if (_onItemChanged != null)
                _onItemChanged(this, e);
        }

        /// <summary>
        /// Occurs when the collection has been changed.
        /// </summary>
        [Browsable(false)]
        [C1Description("Occurs when the collection has been changed.")]
        public event EventHandler CollectionChanged
        {
            add
            {
                _onCollectionChanged = (EventHandler)Delegate.Combine(_onCollectionChanged, value);
            }
            remove
            {
                _onCollectionChanged = (EventHandler)Delegate.Remove(_onCollectionChanged, value);
            }
        }

        /// <summary>
        /// Fires the <see cref="CollectionChanged"/> event.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected virtual void OnCollectionChanged(EventArgs e)
        {
            if (_onCollectionChanged != null)
                _onCollectionChanged(this, e);
        }
        #endregion // Public events

        #region Private methods
        private string Escape(string key)
		{
			if (key.IndexOf(c_delim) == -1)
				return key;
			StringBuilder sb = new StringBuilder(key.Length + 3);
			for (int i = 0; i < key.Length; i++)
			{
				char c = key[i];
				if (c == c_delim || c == c_escape)
					sb.Append(c_escape);
				sb.Append(c);
			}
			return sb.ToString();
		}

        private string Unescape(string key)
		{
			if (key.IndexOf(c_delim) == -1)
				return key;
			StringBuilder sb = new StringBuilder(key.Length);
			for (int i = 0; i < key.Length; i++)
			{
				char c = key[i];
				if (c == c_escape)
				{
					i++;
					if (i == key.Length)
						throw new ArgumentException(string.Format(C1Localizer.GetString("Key has invalid format:")+" \"{0}\".", key));
					c = key[i];
				}
				sb.Append(c);
			}
			return sb.ToString();
        }

        private string[] SaveToStringArray()
        {
            ArrayList strings = new ArrayList(Count);
            foreach (DictionaryEntry o in _items)
            {
                UIString us = (UIString)o.Value;
                if (!us.IsDefault)
                    strings.Add(string.Format(c_keyValueFmt, Escape(ConvertToStr(o.Key)), us.Value));
            }
            return (string[])strings.ToArray(typeof(string));
        }

        private void RestoreFromStringArray(string[] items)
        {
            if (items == null)
            {
                Reset();
                return;
            }
            bool prevBunchUpdate = _bunchUpdate;
            _bunchUpdate = true;
            try
            {
                Reset();
                foreach (string s in items)
                {
                    int startIndex, pos = 0;
                    do
                    {
                        startIndex = pos + 1;
                        pos = s.IndexOf(c_delim, startIndex);
                        if (pos == -1)
                            throw new ArgumentException(C1Localizer.GetString("Invalid format of saved UIStrings."));
                    } while (s[pos - 1] == c_escape);
                    this[ConvertFromStr(Unescape(s.Substring(0, pos)))] = s.Substring(pos + 1);
                }
            }
            finally
            {
                _bunchUpdate = prevBunchUpdate;
            }
            if (!_bunchUpdate && _modified)
            {
                _modified = false;
                OnCollectionChanged(EventArgs.Empty);
            }
        }

        private object ValidateKey(object key)
        {
            if (key is string && _indexer != typeof(string))
                key = ConvertFromStr((string)key);
            if (!_items.ContainsKey(key))
                throw new ArgumentException(C1Localizer.GetString("Key not found:") + " " + key.ToString());
            return key;
        }

        private int BinarySearch(UisSorter item)
        {
            int m, l = 0;
            int h = _sorter.Count - 1;
            while (l <= h)
            {
                m = (l + h) >> 1;
                int c = item.CompareTo(_sorter[m]);
                if (c > 0)
                    l = m + 1;
                else
                    h = m - 1;
            }
            return l;
        }
        #endregion // Private methods

        #region Nested classes
        /// <summary>
        /// Provides type conversion for the <see cref="UIStrings"/> type.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public class TypeConverter : System.ComponentModel.TypeConverter
		{
            /// <summary>
            /// For internal use.
            /// </summary>
            /// <param name="context"></param>
            /// <param name="culture"></param>
            /// <param name="value"></param>
            /// <param name="type"></param>
            /// <returns></returns>
			public override object ConvertTo(ITypeDescriptorContext context, 
				System.Globalization.CultureInfo culture, object value, Type type)
			{
				if (type == typeof(string))
					return ((UIStrings)value).Description;
				return base.ConvertTo(context, culture, value, type);
			}

            /// <summary>
            /// For internal use.
            /// </summary>
            /// <param name="context"></param>
            /// <param name="value"></param>
            /// <param name="attrFilter"></param>
            /// <returns></returns>
            public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context,
				object value, Attribute[] attrFilter)
			{
				UIStrings uss = (UIStrings)value;
				int count = uss.Count;
				PropertyDescriptor[] props = new PropertyDescriptor[count];
				string[] sorted = new string[count];
				int i = 0;
				foreach (UisSorter s in uss.Sorter)
				{
					Attribute[] attributes = new Attribute[3];
					attributes[0] = new DescriptionAttribute(s.Uis.Description);
					attributes[1] = new DefaultValueAttribute(s.Uis.DefaultValue); 
					attributes[2] = new RefreshPropertiesAttribute(RefreshProperties.Repaint);
					props[i] = new StringPropertyDescriptor(s.StrKey, attributes, context);
					sorted[i++] = s.StrKey;
				}
				return (new PropertyDescriptorCollection(props)).Sort(sorted);
			}

            /// <summary>
            /// For internal use.
            /// </summary>
            /// <param name="context"></param>
            /// <returns></returns>
            public override bool GetPropertiesSupported(ITypeDescriptorContext context) 
			{
				return true;
			}

            /// <summary>
            /// For internal use.
            /// </summary>
			[EditorBrowsable(EditorBrowsableState.Never)]
            protected class StringPropertyDescriptor : TypeConverter.SimplePropertyDescriptor
			{
				private string _name;
				private ITypeDescriptorContext _context;

                /// <summary>
                /// For internal use.
                /// </summary>
                /// <param name="name"></param>
                /// <param name="attributes"></param>
                /// <param name="context"></param>
				public StringPropertyDescriptor(string name, System.Attribute[] attributes, ITypeDescriptorContext context)
					: base(typeof(UIStrings), name, typeof(string), attributes)
				{
					_name = name;
					_context = context;
				}

                /// <summary>
                /// For internal use.
                /// </summary>
                /// <param name="component"></param>
                /// <returns></returns>
                public override bool CanResetValue(object component)
				{
					return ShouldSerializeValue(component);
				}

                /// <summary>
                /// For internal use.
                /// </summary>
                /// <param name="component"></param>
                public override void ResetValue(object component)
				{
					if (_context != null && !_context.OnComponentChanging())
						return;
					((UIStrings)component).Reset(_name);
					if (_context != null)
						_context.OnComponentChanged();
				}

                /// <summary>
                /// For internal use.
                /// </summary>
                /// <param name="component"></param>
                /// <returns></returns>
                public override object GetValue(object component)
				{
					UIStrings us = component as UIStrings;
					return us[us.ConvertFromStr(_name)];
				}

                /// <summary>
                /// For internal use.
                /// </summary>
                /// <param name="component"></param>
                /// <param name="value"></param>
                public override void SetValue(object component, object value)
				{
					if (_context != null && !_context.OnComponentChanging())
						return;
					UIStrings us = component as UIStrings;
					us[us.ConvertFromStr(_name)] = (string)value;
					if (_context != null)
						_context.OnComponentChanged();
				}

                /// <summary>
                /// For internal use.
                /// </summary>
                /// <param name="component"></param>
                /// <returns></returns>
                public override bool ShouldSerializeValue(object component)
				{
					UIStrings us = component as UIStrings;
					return !us.IsDefault(us.ConvertFromStr(_name));
				}

                /// <summary>
                /// For internal use.
                /// </summary>
                public override bool IsReadOnly
				{
					get { return false; }
				}

                /// <summary>
                /// For internal use.
                /// </summary>
                public override bool DesignTimeOnly
				{
					get { return false; }
				}
			}
        }
        #endregion // Nested classes
    }
}
