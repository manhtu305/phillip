using System;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.Reflection;

#if TRUEGRID_2
namespace C1.Win.C1TrueDBGrid
#elif C1COMMAND2005
namespace C1.Win.C1Command
#elif C1Input2005
namespace C1.Win.C1Input
#elif PREVIEW_2
namespace C1.C1Preview
#elif C1WebGrid2005
namespace C1.Web.C1WebGrid.Design
#else
namespace C1.Util.Design
#endif
{
/*	********************************************
	***  An example of using FlagsConverter  ***
	********************************************

	// NavigatorButtonConverter
	internal class NavigatorButtonConverter : FlagsConverter
	{
		public NavigatorButtonConverter() :
			base(typeof(NavigatorButtonFlags), flagsNavigatorButtons) {}
	}

	// NavigatorButtonFlags
	[
	Flags,
	TypeConverter(typeof(NavigatorButtonConverter)),
	]
	public enum NavigatorButtonFlags
	{
		None			= 0x0000,
		First			= 0x0001,
		Previous		= 0x0002,
		Next			= 0x0004,
		Last			= 0x0008,
		Apply			= 0x0080,
		Cancel			= 0x0100,
		Update			= 0x0200,
		Refresh			= 0x0400,
		Navigation		= First | Previous | Next | Last,
		All				= Navigation | Apply | Cancel | Update | Refresh
	}

	//-------------------------------------------------------------------
	// Flags Info (for FlagsConverter's ctor)
	//
	//  Columns meaning:
	//
	//  0 - flags enum element,
	//  1 - where the item should be displayed (one of FlagsItem constants),
	//  2 - whether the name should be parenthesized in the expandable list,
	//  3 - flag's description text.
	//
	// The expandable and dropdown lists are not sorted alphabetically.
	// Items will be shown in the same order as they are listed in the
	// corresponding array. Only exception is parenthesized items. They
	// precede all other items in the expandable list.
	//
	internal static readonly object[,] flagsNavigatorButtons =
	{
		{NavigatorButtonFlags.None,			FlagsItem.Everywhere,		true,	"No buttons."},
		{NavigatorButtonFlags.First,		FlagsItem.ExpandableList,	false,	"'First row' button."},
		{NavigatorButtonFlags.Previous,		FlagsItem.ExpandableList,	false,	"'Previous row' button."},
		{NavigatorButtonFlags.Next,			FlagsItem.ExpandableList,	false,	"'Next row' button."},
		{NavigatorButtonFlags.Last,			FlagsItem.ExpandableList,	false,	"'Last row' button."},
		{NavigatorButtonFlags.Apply,		FlagsItem.ExpandableList,	false,	"'Apply' button."},
		{NavigatorButtonFlags.Cancel,		FlagsItem.ExpandableList,	false,	"'Cancel' button."},
		{NavigatorButtonFlags.Update,		FlagsItem.ExpandableList,	false,	"'Update' button."},
		{NavigatorButtonFlags.Refresh,		FlagsItem.ExpandableList,	false,	"'Refresh' button."},
		{NavigatorButtonFlags.Navigation,	FlagsItem.DropDownList,		false,	""},
		{NavigatorButtonFlags.All,			FlagsItem.DropDownList,		false,	""}
	};

    // To use UpdateValidator, the host must implement IFlagsUpdateValidatorHost
	public class C1DbNavigator : System.Windows.Forms.UserControl, IFlagsUpdateValidatorHost
	{
		private NavigatorButtonFlags _visibleButtons = NavigatorButtonFlags.Navigation;
		
		// Each flag enum property must have its own update validator.
		// It is necessary because of some weird .NET bugs.
		private IFlagsUpdateValidator _uvVisibleButtons;

		public C1DbNavigator()
		{
			InitializeComponent();
			
			// In the following line "VisibleButtons" is name of the property.
			_uvVisibleButtons = FlagsConverter.NewUpdateValidator(this, "VisibleButtons");
			...
		}

        IFlagsUpdateValidator IFlagsUpdateValidatorHost.GetValidator(string propertyName)
        {
            // A class can have more than one update flags props, so the propertyName
            // is neccessary. But this method should never receive unknown prop names.
            System.Diagnostics.Debug.Assert(propertyName == "VisibleButtons");
            return _uvVisibleButtons;
        }        

		[
		Description("Flags enumeration specifying which buttons are visible."),
		DefaultValue(NavigatorButtonFlags.Navigation),
		Localizable(true),
		]
		public NavigatorButtonFlags VisibleButtons
		{
			get { return _visibleButtons; }
			set
			{
				// Ask the update validator before change the property's underlying variable.
				if (_uvVisibleButtons.NeedsUpdate(_visibleButtons, value))
				{
					_visibleButtons = value;
					...
				}
			}
		...
		
	Notes:
	(1) Any class inherited from the FlagsConverter should be excluded from obfuscation.
	(2) Unfortunately, it is not possible to apply FlagsConverter to the standard enums.
	    You need to create your own enum with elements of the standard one. For example:

		[Flags]
		[TypeConverter(typeof(NumberStylesConverter))]
		public enum NumberStyleFlags
		{
			None					= NumberStyles.None,
			AllowCurrencySymbol		= NumberStyles.AllowCurrencySymbol,
			AllowDecimalPoint		= NumberStyles.AllowDecimalPoint,
			AllowExponent			= NumberStyles.AllowExponent,
			...
			Any						= NumberStyles.Any,
			Currency				= NumberStyles.Currency,
			Float					= NumberStyles.Float,
		...
    (3) There is a special case: the "none" flag, equal to 0. For it there is no mask,
        so though it is easy to set it to "True" (by setting the whole value - all flags -
        to 0), normally it is not possible to set it to "False". To facilitate this, an
        extra field may be specified in the flags info array, of the type FlagsSpecial.
        If specified, it should be either null or FlagsSpecial.None for all elements except
        two: the "all" and "none" elements, for which it should be set to FlagsSpecial.IsAll
        and FlagsSpecial.IsNone correspondingly. Also, the IsNone can only be specified for
        the element with the value 0. If this extra info is specified, the "none" value
        cab be set to False as well (which has the same effect as setting the "all" value
        to True).
*/

	// FlagsItem
	internal enum FlagsItem
	{
		Everywhere,			// show the item in both lists: dropdown and expandable
		ExpandableList,		// show it in the expandable list only (default option)
		DropDownList,		// show item in the dropdown list only
		Hide				// don't show this item anywhere
	}

    // used to mark special flags. Needed to be able to set "None" to True
    // (which is defined as "not All"). (see Note 3 above).
    internal enum FlagsSpecial
    {
        None,               // not a special elem
        IsAll,              // this elem is the "all flags on" elem (complement to "none")
        IsNone,             // this elem is the "none" (all off) elem (complement to "all")
    }

	// IFlagsUpdateValidator
	internal interface IFlagsUpdateValidator
	{
        bool NeedsUpdate(object oldValue, object newValue);
	}

    internal interface IFlagsUpdateValidatorHost
    {
        IFlagsUpdateValidator GetValidator(string propertyName);
    }

	// FlagsConverter
#if WHIDBEY
    [Obfuscation(Exclude=true, ApplyToMembers=true)]
#endif
	internal class FlagsConverter : EnumConverter
	{
		private object[,] _valueInfo = null;
		private StandardValuesCollection _standardValues = null;

		public FlagsConverter(Type type) : base(type) {}

		public FlagsConverter(Type type, object[,] valueInfo) : base(type)
		{
#if DEBUG
			if (valueInfo != null)
			{
#if skip_dima //old version, remove when the dust settles.
				if (!(valueInfo.Length > 0 && valueInfo.GetUpperBound(1) == 3 &&
					(valueInfo.Length & 3) == 0 && type.IsEnum))
					throw new ArgumentException("valueInfo");
                for (int i = 0; i < valueInfo.Length >> 2; i++)
                    if (!(valueInfo[i, 0].GetType().Equals(type) &&
                        valueInfo[i, 1].GetType().Equals(typeof(FlagsItem)) &&
                        valueInfo[i, 2].GetType().Equals(typeof(bool)) &&
                        (valueInfo[i, 3] == null || valueInfo[i, 3].GetType().Equals(typeof(string)))))
                        throw new ArgumentException("valueInfo");
#else
                if (valueInfo.Length == 0 || valueInfo.GetLength(1) > 5 || !type.IsEnum)
                    throw new ArgumentException("valueInfo1");

                for (int i = 0; i < valueInfo.GetLength(0); ++i)
                {
                    if (!(
                        valueInfo[i, 0].GetType().Equals(type) &&
                        valueInfo[i, 1].GetType().Equals(typeof(FlagsItem)) &&
                        valueInfo[i, 2].GetType().Equals(typeof(bool)) &&
                        (valueInfo[i, 3] == null || valueInfo[i, 3].GetType().Equals(typeof(string)))))
                        throw new ArgumentException("valueInfo2");
                    if (valueInfo.GetLength(1) == 5 &&
                        !(valueInfo[i, 4] == null || valueInfo[i, 4].GetType().Equals(typeof(FlagsSpecial))))
                        throw new ArgumentException("valueInfo2");
                }
#endif
			}
#endif
			_valueInfo = valueInfo;
		}
		internal static string CalcHashKey(object instance, PropertyDescriptor pd)
		{
			return instance.GetHashCode().ToString(CultureInfo.InvariantCulture) + pd.Name;
		}

        internal static IFlagsUpdateValidator NewUpdateValidator(
            IFlagsUpdateValidatorHost instance, string propertyName)
        {
            PropertyDescriptorCollection pdc = TypeDescriptor.
                GetProperties(instance, new Attribute[] {new FlagsAttribute()}, false);
            PropertyDescriptor pd = pdc.Find(propertyName, false); 
            if (pd != null)
            {
                return new FlagsPropertyCollection(instance, pd);
            }
            throw new Exception(String.Format(CultureInfo.InvariantCulture, "Property {0} is not found in the control.", propertyName));
        }

		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			if (context != null)
			{
				if (_valueInfo == null)
					return true;
                int count = _valueInfo.GetLength(0);
				for (int i = 0; i < count; i++)
				{
					FlagsItem itemKind = (FlagsItem)_valueInfo[i, 1];
					if (itemKind == FlagsItem.Everywhere || itemKind == FlagsItem.ExpandableList)
						return true;
				}
			}
			return false;
		}
		public override PropertyDescriptorCollection GetProperties(
			ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			if (context != null)
			{
				object inst = context.Instance;
				if (inst != null && !(inst is Object[]))
				{
					PropertyDescriptor pd = context.PropertyDescriptor;
					if (pd.Attributes.Contains(attributes))
					{
                        IFlagsUpdateValidatorHost fuvh = inst as IFlagsUpdateValidatorHost;
                        if (fuvh != null)
                        {
                            FlagsPropertyCollection fepc = fuvh.GetValidator(pd.Name) as FlagsPropertyCollection;
                            if (fepc != null)
                            {
                                if (!fepc.initialized)
                                {
                                    if (_valueInfo != null)
                                    {
                                        fepc.valueInfo = _valueInfo;
                                        fepc.CleanUpPropertyCollection();
                                    }
                                    fepc.initialized = true;
                                }
                                return fepc.pdc;
                            }
                        }
					}
				}
			}
			return PropertyDescriptorCollection.Empty;
		}
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			if (context != null && _valueInfo != null)
			{
                int count = _valueInfo.GetLength(0);
                for (int i = 0; i < count; i++)
				{
					FlagsItem itemKind = (FlagsItem)_valueInfo[i, 1];
					if (itemKind == FlagsItem.Everywhere || itemKind == FlagsItem.DropDownList)
						return true;
				}
			}
			return false;
		}
		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return false;
		}
		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			if (_standardValues == null && context != null && _valueInfo != null)
			{
				Type propType = context.PropertyDescriptor.PropertyType;
				Array values = Enum.GetValues(propType);
				int vcount = values.Length;
				ArrayList itemList = new ArrayList(vcount);
				int[] orders = new int[vcount];
                int icount = _valueInfo.GetLength(0);
                for (int i = 0; i < vcount; i++)
				{
					object value = values.GetValue(i);
					for (int j = 0; j < icount; j++)
						if (value.Equals(_valueInfo[j, 0]))
						{
							FlagsItem itemKind = (FlagsItem)_valueInfo[j, 1];
							if (itemKind == FlagsItem.Everywhere || itemKind == FlagsItem.DropDownList)
							{
								orders[itemList.Count] = j;
								itemList.Add(value);
							}
							break;
						}
				}
				icount = itemList.Count;
				int distance = icount >> 1;
				while (distance > 0)
				{
					for (int i = 0, j = distance; j < icount; i++, j++)
						if (orders[i] > orders[j])
						{
							object value = itemList[i];
							int order = orders[i];
							itemList[i] = itemList[j];
							orders[i] = orders[j];
							itemList[j] = value;
							orders[j] = order;
						}
					distance--;
				}
				_standardValues = new StandardValuesCollection(itemList);
			}
			return _standardValues;
		}
	}

	// FlagPropertyDescriptor
	internal class FlagPropertyDescriptor : PropertyDescriptor
	{
		private object _host;
		private PropertyDescriptor _pd;
		internal string _name;
		internal string _displayName;
		internal string _description;
		internal object _value;
        // used only for "none" elements, specifies the "all" value
        internal object _complement = null;
		private long _mask;
		private bool _hasDefaultValue;
		private long _defaultMask;
		private FlagsPropertyCollection _fepc;

		internal FlagPropertyDescriptor(object host, PropertyDescriptor pd, string name,
			object value, object defaultValue, Attribute[] attributes, FlagsPropertyCollection fepc)
			: base(name, attributes)
		{
			_host = host;
			_pd = pd;
			_name = name;
			_value = value;
			_mask = Convert.ToInt64(value, CultureInfo.InvariantCulture);
			_fepc = fepc;
			if (defaultValue != null)
			{
				_defaultMask = Convert.ToInt64(defaultValue, CultureInfo.InvariantCulture);
				_hasDefaultValue = true;
			}
		}
		private long GetLongFromEnum(object objValue)
		{
			if (objValue != null && !(objValue is System.DBNull))
				return Convert.ToInt64(objValue, CultureInfo.InvariantCulture);
			return 0;
		}
		public override Type ComponentType
		{
			get { return _pd.ComponentType; }
		}
		public override string Description
		{
			get { return _description; }
		}
		public override bool DesignTimeOnly
		{
			get { return _pd.DesignTimeOnly; }
		}
		public override string DisplayName
		{
			get
			{
				if (_displayName == null)
					return _name;
				return _displayName;
			}
		}
		public override string Name
		{
			get { return _name; }
		}
		public override bool IsReadOnly
		{
			get
			{
				TypeConverter tc = TypeDescriptor.GetConverter(_host.GetType());
				if (tc != null && tc.GetPropertiesSupported(null))
				{
					PropertyDescriptorCollection pdc = tc.GetProperties(null, _host, new Attribute[] {BrowsableAttribute.Yes});
					PropertyDescriptor pd = pdc.Find(_pd.Name, false);
					if (pd != null)
						return pd.IsReadOnly;
				}
				return _pd.IsReadOnly;
			}
		}
		public override Type PropertyType
		{
			get { return typeof(bool); }
		}
		private bool IntCanResetValue()
		{
			long currValue = GetLongFromEnum(_pd.GetValue(_host));
			if (_mask == 0)
				return (currValue != 0 && _defaultMask == 0) || (currValue == 0 && _defaultMask != 0);
			return (((currValue & _mask) == 0 && (_defaultMask & _mask) == _mask) ||
				((currValue & _mask) == _mask && (_defaultMask & _mask) == 0));
		}
		public override bool CanResetValue(object component)
		{
			bool result = _pd.CanResetValue(_host);
			if (result && _hasDefaultValue)
				return IntCanResetValue();
			return result;
		}
		public override object GetValue(object component)
		{
			long currValue = GetLongFromEnum(_pd.GetValue(_host));
			if (_mask != 0)
				return (currValue & _mask) == _mask;
			return currValue == 0;
		}
		private void IntSetValue(long newValue, object prevValue)
		{
			_fepc.checkIsNeeded = false;
			_pd.SetValue(_host, Enum.Parse(_pd.PropertyType, newValue.ToString(CultureInfo.InvariantCulture)));
			_fepc.prevValue = prevValue;
			_fepc.checkIsNeeded = true;
		}
		public override void ResetValue(object component)
		{
			if (_hasDefaultValue)
			{
				object objValue = _pd.GetValue(_host);
				if (_mask == 0)
					IntSetValue(_defaultMask, objValue);
				else
				{
					long currValue = GetLongFromEnum(objValue);
					if ((currValue & _mask) == 0)
						currValue |= _mask;
					else
						currValue &= ~_mask;
					IntSetValue(currValue, objValue);
				}
				return;
			}
			_pd.ResetValue(_host);
		}
		public override void SetValue(object component, object value)
		{
			object objValue = _pd.GetValue(_host);
			long currValue = GetLongFromEnum(objValue);
			long newValue = currValue;
            if (_mask != 0)
            {
                if ((bool)value)
                    newValue |= _mask;
                else
                    newValue &= ~_mask;
            }
            else
            {
                if ((bool)value)
                    newValue = 0;
                else if (_complement != null)
                    newValue = GetLongFromEnum(_complement);
            }
            if (newValue != currValue)
				IntSetValue(newValue, objValue);
		}
		public override bool ShouldSerializeValue(object component)
		{
			bool result = _pd.ShouldSerializeValue(_host);
			if (result && _hasDefaultValue)
				return IntCanResetValue();
			return result;
		}
	}

	// FlagsPropertyCollection
	internal class FlagsPropertyCollection : IFlagsUpdateValidator
	{
		internal bool initialized = false;
		internal PropertyDescriptorCollection pdc = null;
		internal object[,] valueInfo = null;
		internal bool checkIsNeeded = false;
		internal object prevValue;
		private PropertyDescriptor _hostProperty;
		private object _instance;

		internal FlagsPropertyCollection(object instance, PropertyDescriptor hostProperty)
		{
			_hostProperty = hostProperty;
			_instance = instance;
			object defaultValue = null;
			Type enumType = hostProperty.PropertyType;
			ArrayList attList = new ArrayList();
			foreach (Attribute att in hostProperty.Attributes)
			{
				if (att is BrowsableAttribute || att is CategoryAttribute ||
					att is CLSCompliantAttribute || att is MergablePropertyAttribute)
					attList.Add(att);
				else if (att is DefaultValueAttribute)
				{
					DefaultValueAttribute dva = (DefaultValueAttribute)att;
					if (dva.Value != null && dva.Value.GetType() == enumType)
						defaultValue = dva.Value;
				}
			}
			attList.Add(new RefreshPropertiesAttribute(RefreshProperties.Repaint));
			Attribute[] atts = new Attribute[attList.Count];
			attList.CopyTo(atts, 0);
			string[] names = Enum.GetNames(enumType);
			Array values = Enum.GetValues(enumType);
			PropertyDescriptor[] pdarr = new PropertyDescriptor[values.Length];
			for (int i = 0; i < values.Length; i++)
				pdarr[i] = new FlagPropertyDescriptor(instance, hostProperty, names[i],
					values.GetValue(i), defaultValue, atts, this);
			pdc = new PropertyDescriptorCollection(pdarr);
		}
		internal void CleanUpPropertyCollection()
		{
			int icount = pdc.Count;
			ArrayList propList = new ArrayList(icount);
			bool[] brackets = new bool[icount];
			int[] orders = new int[icount];
            int count = valueInfo.GetLength(0);
            for (int i = 0; i < pdc.Count; i++)
			{
				bool a = true;
				icount = propList.Count;
				orders[icount] = 1000 + i;
				brackets[icount] = false;
				FlagPropertyDescriptor pd = (FlagPropertyDescriptor)pdc[i];
				for (int j = 0; j < count; j++)
					if (pd._value.Equals(valueInfo[j, 0]))
					{
						FlagsItem itemKind = (FlagsItem)valueInfo[j, 1];
						if (itemKind == FlagsItem.DropDownList || itemKind == FlagsItem.Hide)
							a = false;
						else
						{
							orders[icount] = j;
							if ((bool)valueInfo[j, 2])
							{
								brackets[icount] = true;
								pd._displayName = String.Format(CultureInfo.InvariantCulture, "({0})", pd._name);
							}
							pd._description = (string)valueInfo[j, 3];
                            FlagsSpecial fs = GetFlagsSpecial(j);
                            if (fs == FlagsSpecial.IsNone)
                            {
                                // object complement: if the element is "none" (0), we try to find
                                // its complement value (which is "all"), so that the "none" element
                                // can be set to false (i.e. "all").
                                for (int k = 0; k < count; ++k)
                                {
                                    FlagsSpecial fsa = GetFlagsSpecial(k);
                                    if (fsa == FlagsSpecial.IsAll)
                                    {
                                        pd._complement = valueInfo[k, 0];
                                        break;
                                    }
                                }
                            }
						}
						break;
					}
				if (a)
					propList.Add(pd);
			}
			icount = propList.Count;
			PropertyDescriptor[] pdarr = new PropertyDescriptor[icount];
			propList.CopyTo(pdarr);
			string[] names = new string[icount];
			for (int i = 0; i < icount; i++)
				names[i] = pdarr[i].Name;
			int distance = icount >> 1;
			while (distance > 0)
			{
				for (int i = 0, j = distance; j < icount; i++, j++)
					if ((!brackets[i] && brackets[j]) || (brackets[i] == brackets[j] && orders[i] > orders[j]))
					{
						bool br = brackets[i];
						int order = orders[i];
						string name = names[i];
						brackets[i] = brackets[j];
						orders[i] = orders[j];
						names[i] = names[j];
						brackets[j] = br;
						orders[j] = order;
						names[j] = name;
					}
				distance--;
			}
			pdc = (new PropertyDescriptorCollection(pdarr)).Sort(names);
		}
		bool IFlagsUpdateValidator.NeedsUpdate(object oldValue, object newValue)
		{
			bool res = true;
			if (newValue == oldValue || (checkIsNeeded && newValue.Equals(prevValue)))
				res = false;
			checkIsNeeded = false;
			return res;
		}

        private FlagsSpecial GetFlagsSpecial(int idx)
        {
            if (valueInfo.GetLength(1) <= 4 || valueInfo[idx, 4] == null)
                return FlagsSpecial.None;
            return (FlagsSpecial)valueInfo[idx, 4];
        }
	}
}
