using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;
//using System.Drawing;

using C1.Util.Localization;
using C1.Util.Licensing;

namespace C1.Util.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    internal class C1AboutActionList : DesignerActionList
    {
        static private readonly string s_display = C1Localizer.GetString("About...");
        static private readonly string s_description = C1Localizer.GetString("About {0}...");
        private object _c1product = null;
        // for display, "" or null means use default
        private string _display = null;
        // for description (tooltip), "" means do not show, null means use default.
        private string _description = null;

        public C1AboutActionList(IComponent component, object c1product)
            : base(component)
        {
            _c1product = c1product;
        }

        public C1AboutActionList(IComponent component, object c1product, string display, string description)
            : base(component)
        {
            _c1product = c1product;
            _display = display;
            _description = description;
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            if (_c1product == null)
                return null;

            string display = string.IsNullOrEmpty(_display) ? s_display : _display;
            string description = _description == null ?
                string.Format(s_description,
                ((AssemblyProductAttribute)Attribute.GetCustomAttribute(Assembly.GetExecutingAssembly(),
                typeof(AssemblyProductAttribute))).Product) :
                _description;

            DesignerActionItemCollection items = new DesignerActionItemCollection();
            items.Add(new DesignerActionMethodItem(this, "ShowAboutBox", display, "About", description, true));
            return items;
        }

        public void ShowAboutBoxEventHandler(object sender, EventArgs e)
        {
            ShowAboutBox();
        }

        public void ShowAboutBox()
        {
#if WPF
            C1.WPF.ProviderInfo.ShowAboutBox(_c1product, null); // todo: pass license as 2nd par (get it from owner)
#else
            ProviderInfo.ShowAboutBox(_c1product);
#endif
        }
    }
}