using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Design;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Reflection;

#if PREVIEW_2
using C1.C1Preview;
using C1.C1Preview.Localization;
#endif

namespace C1.Util
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal class FlagsEnumEditorControl : UserControl
    {
        private TreeView treeView;
        private Button btnOk;
        private Button btnCancel;
        private ImageList imageList;
        private IContainer components;

        private IWindowsFormsEditorService _edSvc;
        private UInt64 _value;
        private Type _enumType;
        private EnumValueDescCollection _enumValues;
        private bool _ok;

        #region Constructors

        public FlagsEnumEditorControl(IWindowsFormsEditorService edSvc)
            : base()
        {
            _edSvc = edSvc;
            InitializeComponent();
#if PREVIEW_2
            StringsManager.LocalizeControl(typeof(C1.C1Preview.Design.Strings), this, typeof(FlagsEnumEditorControl));
#endif
        }

        #endregion

        #region Private
        private void UpdateNodesCheckState(TreeNodeCollection nodes)
        {
            foreach (EnumTreeNode node in nodes)
            {
                node.UpdateCheckState();
                UpdateNodesCheckState(node.Nodes);
            }
        }

        private void Init()
        {
            _ok = true;
            _enumValues = new EnumValueDescCollection(_enumType);

            EnumTreeNode[] nodes = new EnumTreeNode[_enumValues.Count];
            for (int i = 0; i < _enumValues.Count; i++)
            {
                EnumValueDesc evd = _enumValues[i];
                if (evd.Value == 0)
                    continue;

                // create the node
                EnumTreeNode node = new EnumTreeNode(evd);

                // search parent for node
                TreeNode parent = null;
                for (int j = i - 1; j >= 0; j--)
                    if ((evd.Value & _enumValues[j].Value) == evd.Value &&
                        evd.Value != _enumValues[j].Value)
                    {
                        parent = nodes[j];
                        break;
                    }

                if (parent == null)
                    treeView.Nodes.Add(node);
                else
                    parent.Nodes.Add(node);

                nodes[i] = node;
            }
            UpdateNodesCheckState(treeView.Nodes);
            foreach (TreeNode node in treeView.Nodes)
                node.Expand();

            treeView.Focus();
        }
        #endregion

        #region Public properties
        public object Value
        {
            get { return Enum.ToObject(_enumType, _value); }
            set
            {
                if (value == null)
                    throw new ArgumentException();
                _enumType = value.GetType();
                _value = ((IConvertible)value).ToUInt64(null);
                Init();
            }
        }

        public bool Ok
        {
            get { return _ok; }
        }
        #endregion

        #region Sub classes
        /// <summary>
        /// Represents the one value in the enumeration.
        /// </summary>
        private class EnumValueDesc
        {
            private EnumValueDescCollection _owner;
            private object _enumValue;
            private UInt64 _value;
            private int _bitCount;

            #region Constructors
            public EnumValueDesc(EnumValueDescCollection owner, object enumValue)
            {
                _owner = owner;
                _enumValue = enumValue;
                _value = ((IConvertible)_enumValue).ToUInt64(null);
                _bitCount = 0;
                for (UInt64 v = _value; v != 0; v = v >> 1)
                    if ((v & 1) != 0)
                        _bitCount++;
            }
            #endregion

            #region Public properties
            /// <summary>
            /// Gets the EnumValueDescCollection collection containing this object.
            /// </summary>
            public EnumValueDescCollection Owner
            {
                get { return _owner; }
            }

            /// <summary>
            /// Gets the enumeration's type.
            /// </summary>
            public Type EnumType
            {
                get { return _owner == null ? null : _owner.EnumType; }
            }

            /// <summary>
            /// Gets the count of significant bits in EnumValue.
            /// </summary>
            public int BitCount
            {
                get { return _bitCount; }
            }

            /// <summary>
            /// Gets the enumeration's value.
            /// </summary>
            public object EnumValue
            {
                get { return _enumValue; }
            }

            /// <summary>
            /// Gets the enumeration's value as long unsigned integer.
            /// </summary>
            public UInt64 Value
            {
                get { return _value; }
            }
            #endregion
        }

        /// <summary>
        /// Collection of the EnumValueDesc objects.
        /// </summary>
        private class EnumValueDescCollection : CollectionBase
        {
            private Type _enumType;

            #region Constructors
            public EnumValueDescCollection(Type enumType)
                : base()
            {
                _enumType = enumType;
                // fill the collection
                Array enumValues = Enum.GetValues(enumType);
                foreach (object enumValue in enumValues)
                {
                    // respect the Browsable(false) attribute on enum elements
                    string tstr = enumValue.ToString();
                    FieldInfo fi = EnumType.GetField(tstr);
                    object[] attrs = fi.GetCustomAttributes(typeof(BrowsableAttribute), false);
                    if (attrs == null || attrs.Length == 0 ||
                        !(attrs[0] is BrowsableAttribute) || ((BrowsableAttribute)attrs[0]).Browsable)
                        InnerList.Add(new EnumValueDesc(this, enumValue));
                }
                InnerList.Sort(Comparer.Instance);
            }
            #endregion

            #region Public properties
            public Type EnumType
            {
                get { return _enumType; }
            }

            public EnumValueDesc this[int index]
            {
                get { return (EnumValueDesc)InnerList[index]; }
            }
            #endregion

            #region Sub classes
            private class Comparer : IComparer
            {
                public static Comparer Instance = new Comparer();

                #region IComparer implementatation
                int IComparer.Compare(object x, object y)
                {
                    EnumValueDesc vx = (EnumValueDesc)x;
                    EnumValueDesc vy = (EnumValueDesc)y;
                    if (vx.BitCount == vy.BitCount)
                        return 0;
                    else if (vx.BitCount == 0 && vy.BitCount != 0)
                        return -1;
                    else if (vy.BitCount == 0 && vx.BitCount != 0)
                        return 1;
                    else
                        return vy.BitCount - vx.BitCount;
                }
                #endregion
            }
            #endregion
        }

        private class EnumTreeNode : TreeNode
        {
            public EnumValueDesc EnumValue;

            #region Constructors
            public EnumTreeNode(EnumValueDesc enumValue)
                : base()
            {
                EnumValue = enumValue;
                Text = Enum.GetName(enumValue.EnumType, enumValue.EnumValue);
            }
            #endregion

            #region Public
            public void UpdateCheckState()
            {
                if ((EnumValue.Value & Editor._value) == 0)
                    CheckState = CheckState.Unchecked;
                else if ((EnumValue.Value & Editor._value) == EnumValue.Value)
                    CheckState = CheckState.Checked;
                else
                    CheckState = CheckState.Indeterminate;
            }
            #endregion

            #region Public properties
            public FlagsEnumEditorControl Editor
            {
                get { return this.TreeView.Parent as FlagsEnumEditorControl; }
            }

            public CheckState CheckState
            {
                get
                {
                    switch (StateImageIndex)
                    {
                        case 0: return CheckState.Unchecked;
                        case 1: return CheckState.Checked;
                        default: return CheckState.Indeterminate;
                    }
                }
                set
                {
                    switch (value)
                    {
                        case CheckState.Unchecked:
                            StateImageIndex = 0;
                            break;
                        case CheckState.Checked:
                            StateImageIndex = 1;
                            break;
                        default:
                            StateImageIndex = 2;
                            break;
                    }
                }
            }
            #endregion
        }
        #endregion

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlagsEnumEditorControl));
            this.treeView = new System.Windows.Forms.TreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(4, 4);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(220, 220);
            this.treeView.Sorted = true;
            this.treeView.StateImageList = this.imageList;
            this.treeView.TabIndex = 0;
            this.treeView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeView_KeyDown);
            this.treeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDown);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Unchecked.bmp");
            this.imageList.Images.SetKeyName(1, "Grayed.bmp");
            this.imageList.Images.SetKeyName(2, "Grayed2.bmp");
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.Location = new System.Drawing.Point(68, 230);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(149, 230);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // FlagsEnumEditorControl
            // 
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.treeView);
            this.Name = "FlagsEnumEditorControl";
            this.Size = new System.Drawing.Size(227, 256);
            this.ResumeLayout(false);

        }

        private void treeView_MouseDown(object sender, MouseEventArgs e)
        {
            TreeViewHitTestInfo hti = treeView.HitTest(e.X, e.Y);
            if (hti == null || hti.Node == null || hti.Location != TreeViewHitTestLocations.StateImage)
                return;

            EnumTreeNode node = (EnumTreeNode)hti.Node;
            if (node.CheckState == CheckState.Checked)
            {
                node.CheckState = CheckState.Unchecked;
                _value = _value & ~node.EnumValue.Value;
            }
            else
            {
                node.CheckState = CheckState.Checked;
                _value = _value | node.EnumValue.Value;
            }
            UpdateNodesCheckState(treeView.Nodes);
        }

        private void treeView_KeyDown(object sender, KeyEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.KeyCode.ToString());
            if (e.KeyCode == Keys.Space)
            {
                e.Handled = true;
                EnumTreeNode node = treeView.SelectedNode as EnumTreeNode;
                if (node == null)
                    return;

                if (node.CheckState == CheckState.Checked)
                {
                    node.CheckState = CheckState.Unchecked;
                    _value = _value & ~node.EnumValue.Value;
                }
                else
                {
                    node.CheckState = CheckState.Checked;
                    _value = _value | node.EnumValue.Value;
                }
                UpdateNodesCheckState(treeView.Nodes);
            }
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            _ok = true;
            _edSvc.CloseDropDown();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            _ok = false;
            _edSvc.CloseDropDown();
        }
    }
}

#if PREVIEW_2
namespace C1.C1Preview.Design
#else
namespace C1.Util
#endif
{
    /// <summary>
    /// Represents the editor of enum properties with FlagsAttribute attribute.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class FlagsEnumEditor : UITypeEditor
    {
        /// <summary>
        /// Gets the editor style used by the <see cref="EditValue"/> method.
        /// The override for <see cref="FlagsEnumEditor"/> returns <see cref="UITypeEditorEditStyle.DropDown"/>.
        /// </summary>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        /// <summary>
        /// Gets a value indicating whether drop-down editors should be resizable by the user. 
        /// The override for <see cref="FlagsEnumEditor"/> returns true;
        /// </summary>
        public override bool IsDropDownResizable
        {
            get { return true; }
        }

        /// <summary>
        /// Edits the specified object's value using the editor style 
        /// indicated by the <see cref="GetEditStyle"/> method. 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context == null || provider == null || value == null)
                return value;
            IWindowsFormsEditorService edSvc = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;
            if (edSvc == null)
                return value;
            Type enumType = value.GetType();
            if (!enumType.IsEnum)
                return value; // only enumeration type can be edited
            AttributeCollection attrs = TypeDescriptor.GetAttributes(enumType);
            int i;
            for (i = 0; i < attrs.Count && !(attrs[i] is FlagsAttribute); i++) ;
            if (i >= attrs.Count)
                return value; // only enumeration with Flags attribute can be edited.

            using (C1.Util.FlagsEnumEditorControl control = new C1.Util.FlagsEnumEditorControl(edSvc))
            {
                control.Value = value;
                edSvc.DropDownControl(control);
                if (control.Ok)
                    return control.Value;
                else
                    return value;
            }
        }
    }
}
