using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Test
{
    using C1.Design;

    public partial class Form1 : Form
    {
        C1.Design.Util.ToolStripFontComboBox _fontCombo = new C1.Design.Util.ToolStripFontComboBox();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.toolStrip1.Items.Add(_fontCombo);
            _fontCombo.ComboBox.MaxDropDownItems  = 10;
            this._fontCombo.ComboBox.SelectedIndexChanged += new EventHandler(ComboBox_SelectedIndexChanged);

        }

        void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string FontFamily = (string) this._fontCombo.ComboBox.SelectedItem;
            this.label1.Text = FontFamily;
            this.label1.Font = new Font(FontFamily, this.label1.Font.Size);
        }
    }
}