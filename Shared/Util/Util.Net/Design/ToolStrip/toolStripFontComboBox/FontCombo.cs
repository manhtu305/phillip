using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace C1.Design.Util
{
    internal class ToolStripFontComboBox : ToolStripControlHost
    {
        FontComboBox _combo = null;
        public ToolStripFontComboBox() : base(new FontComboBox())
        {
            _combo = this.Control as FontComboBox;
        }

        // make the inner control visible so we can get any needed events
        public FontComboBox ComboBox { get { return _combo; }  }
    }

    internal class FontComboBox : ComboBox
    {
        Bitmap _ttimg = null;           // truetype image
        int _imgWidth = 0;              // image width
        int _maxwid = 0;                // max width of the dropdown

        public FontComboBox()
        {
            MaxDropDownItems = 20;
            IntegralHeight = false;
            Sorted = false;
            DropDownStyle = ComboBoxStyle.DropDownList;
            DrawMode = DrawMode.OwnerDrawVariable;

            // get all the TT fonts
            foreach (FontFamily ff in FontFamily.Families)
            {
                if (ff.IsStyleAvailable(FontStyle.Regular))
                    Items.Add(ff.Name);
            }

            if (Items.Count > 0)
                SelectedIndex = 0;

            _ttimg = getTrueTypeImage();
            _ttimg.MakeTransparent(Color.Blue);
            _imgWidth = _ttimg.Width + (_ttimg.Width / 4);
        }

        private Bitmap getTrueTypeImage()
        {
            // true type image
            string uuString = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8" +
                        "YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAADBQTFRFAAAAgAAA" +
                        "AIAAgIAAAACAgACAAICAwMDAgICA/wAAAP8A//8AAAD//wD/AP//////TxMmSQAAAEFJREFUGNNjOAMF" +
                        "DAjG8fLy8howo6b8OIRxpvwMlFHAwMDAA1JcwMNwAMIAiqMyykGmARHQHAaYyRgMoDnIlkIBACSnUhNG" +
                        "h1mXAAAAAElFTkSuQmCC";

            System.IO.MemoryStream strm = new System.IO.MemoryStream(Convert.FromBase64String(uuString));
            Bitmap bmp = new Bitmap(strm);
            return bmp;
        }
        protected override void OnMeasureItem(System.Windows.Forms.MeasureItemEventArgs e)
        {
            if (e.Index > -1)
            {
                int w = 0;
                string fontstring = Items[e.Index].ToString();
                Graphics g = CreateGraphics();
                e.ItemHeight = (int)g.MeasureString(fontstring, new Font(fontstring, 10)).Height;
                w = (int)g.MeasureString(fontstring, new Font(fontstring, 10)).Width;
                w += _imgWidth;
                if (w > _maxwid)
                    _maxwid = w;
                if (e.ItemHeight > 20)
                    e.ItemHeight = 20;
            }
            base.OnMeasureItem(e);
        }

        protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
        {
            if (e.Index > -1 )
            {
                Font nfont;
                int w = _imgWidth;
                if ((e.State & DrawItemState.ComboBoxEdit) != 0) w = 0;

                string fontstring = Items[e.Index].ToString();
                if (w == 0)
                    nfont = this.Font;
                else
                    nfont = new Font(fontstring, 10);

                SolidBrush backbrush = null;
                SolidBrush textbrush = null;
                if ((e.State & DrawItemState.Focus) == 0)
                {
                    backbrush = new SolidBrush(SystemColors.Window);
                    textbrush = new SolidBrush(SystemColors.WindowText);
                }
                else
                {
                    backbrush = new SolidBrush(SystemColors.Highlight);
                    textbrush = new SolidBrush(SystemColors.HighlightText);
                }

                e.Graphics.FillRectangle(backbrush,
                        e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height);
                
                e.Graphics.DrawString(fontstring, nfont, textbrush,
                        e.Bounds.X + w, e.Bounds.Y);

                if( w != 0 )
                    e.Graphics.DrawImage(_ttimg, new Point(e.Bounds.X, e.Bounds.Y));
            }
            base.OnDrawItem(e);
        }

        protected override void OnDropDown(System.EventArgs e)
        {
            this.DropDownWidth = _maxwid + 5;
        }

    }
}