using System;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Reflection;
using System.Diagnostics;
using System.Text;

#if PREVIEW_2
using C1.C1Preview.Design;
#else
// using ...
#endif

namespace C1.Util.Design
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class EscTextBox : TextBox
    {
        private bool _handled = false;
        private string _value = string.Empty;

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            _value = Text;
        }

        protected override void OnValidated(EventArgs e)
        {
            base.OnValidated(e);
            _value = Text;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.None)
            {
                _handled = true;
                CancelEventArgs ce = new CancelEventArgs(false);
                OnValidating(ce);
                if (!ce.Cancel)
                    OnValidated(EventArgs.Empty);
            }
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            if (_handled)
                e.Handled = true;
            _handled = false;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape && Text != _value)
            {
                Text = _value;
                return true;
            }
            else
                return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
