using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Globalization;
using C1.Util.Localization;

namespace C1.Design
{
    // <summary>
    // Summary description for ColorPickerForm.
    // </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal class ColorPickerOfficeForm : System.Windows.Forms.Form
    {
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ListBox lbWebColors;
        private IContainer components;
        // <summary>
        // Required designer variable.
        // </summary>
        private System.Windows.Forms.TabPage tpWeb;
        private System.Windows.Forms.TabPage tpSystem;
        private System.Windows.Forms.ListBox lbSysColors;
        private System.Windows.Forms.TabPage tpCustom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown udR;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown udB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown udG;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown udA;
        private ColorButton btnCustColor;
        private System.Windows.Forms.Panel pnlPal;

        private Color _clr = Color.Red;

        private static ArrayList webClrs = new ArrayList();
        private static ArrayList sysClrs = new ArrayList();
        bool _canClose = false;
        private Button button1;
        private ColorButton[] _colorButtons = null;
        private ColorButton _selectedButton = null;
        private ToolTip toolTip1;

        private struct ColorInfo
        {
            public Color Color;
            public string Desc;
            public ColorInfo(Color color, string desc)
            {
                Color = color;
                Desc = desc;
            }
        }

        // NOTE/TODO: color names are not used yet. They must be
        // verified and translated into Japanese - then they
        // may be used.
        private static ColorInfo[,] s_officePlt = new ColorInfo[5, 8] {
            {
            new ColorInfo(Color.FromArgb(0x00, 0x00, 0x00), "Black"),
            new ColorInfo(Color.FromArgb(0x99, 0x33, 0x00), "Brown"),
            new ColorInfo(Color.FromArgb(0x33, 0x33, 0x00), "Dark Olive Green"),
            new ColorInfo(Color.FromArgb(0x00, 0x33, 0x00), "Dark Green"),
            new ColorInfo(Color.FromArgb(0x00, 0x33, 0x66), "Dark Teal"),
            new ColorInfo(Color.FromArgb(0x00, 0x00, 0x80), "Dark blue"),
            new ColorInfo(Color.FromArgb(0x33, 0x33, 0x99), "Indigo"),
            new ColorInfo(Color.FromArgb(0x33, 0x33, 0x33), "Dark grey"),
            },{
            new ColorInfo(Color.FromArgb(0x80, 0x00, 0x00), "Dark red"),
            new ColorInfo(Color.FromArgb(0xFF, 0x66, 0x00), "Orange"),
            new ColorInfo(Color.FromArgb(0x80, 0x80, 0x00), "Dark yellow"),
            new ColorInfo(Color.FromArgb(0x00, 0x80, 0x00), "Green"),
            new ColorInfo(Color.FromArgb(0x00, 0x80, 0x80), "Teal"),
            new ColorInfo(Color.FromArgb(0x00, 0x00, 0xFF), "Blue"),
            new ColorInfo(Color.FromArgb(0x66, 0x66, 0x99), "Blue-grey"),
            new ColorInfo(Color.FromArgb(0x80, 0x80, 0x80), "Grey - 40"),
            },{
            new ColorInfo(Color.FromArgb(0xFF, 0x00, 0x00), "Red"),
            new ColorInfo(Color.FromArgb(0xFF, 0x99, 0x00), "Light orange"),
            new ColorInfo(Color.FromArgb(0x99, 0xCC, 0x00), "Lime"),
            new ColorInfo(Color.FromArgb(0x33, 0x99, 0x66), "Sea green"),
            new ColorInfo(Color.FromArgb(0x33, 0xCC, 0xCC), "Aqua"),
            new ColorInfo(Color.FromArgb(0x33, 0x66, 0xFF), "Light blue"),
            new ColorInfo(Color.FromArgb(0x80, 0x00, 0x80), "Violet"),
            new ColorInfo(Color.FromArgb(0x99, 0x99, 0x99), "Grey - 50"),
            },{
            new ColorInfo(Color.FromArgb(0xFF, 0x00, 0xFF), "Pink"),
            new ColorInfo(Color.FromArgb(0xFF, 0xCC, 0x00), "Gold"),
            new ColorInfo(Color.FromArgb(0xFF, 0xFF, 0x00), "Yellow"),
            new ColorInfo(Color.FromArgb(0x00, 0xFF, 0x00), "Bright green"),
            new ColorInfo(Color.FromArgb(0x00, 0xFF, 0xFF), "Turquoise"),
            new ColorInfo(Color.FromArgb(0x00, 0xCC, 0xFF), "Skyblue"),
            new ColorInfo(Color.FromArgb(0x99, 0x33, 0x66), "Plum"),
            new ColorInfo(Color.FromArgb(0xC0, 0xC0, 0xC0), "Light grey"),
            },{
            new ColorInfo(Color.FromArgb(0xFF, 0x99, 0xCC), "Rose"),
            new ColorInfo(Color.FromArgb(0xFF, 0xCC, 0xFF), "Tan"),
            new ColorInfo(Color.FromArgb(0xFF, 0xFF, 0x99), "Light yellow"),
            new ColorInfo(Color.FromArgb(0xCC, 0xFF, 0xCC), "Pale green"),
            new ColorInfo(Color.FromArgb(0xCC, 0xFF, 0xFF), "Pale turquoise"),
            new ColorInfo(Color.FromArgb(0x99, 0xCC, 0xFF), "Pale blue"),
            new ColorInfo(Color.FromArgb(0xCC, 0x99, 0xFF), "Lavender"),
            new ColorInfo(Color.FromArgb(0xFF, 0xFF, 0xFF), "White"),
            }
        };

        public Color Color
        {
            get
            {
                return _clr;
            }
            set
            {
                _clr = value;
                ShowSelectedColor(true);
            }
        }

        public ColorPickerOfficeForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            // adjust list box item height (needed for systems with large fonts)
            lbSysColors.ItemHeight = FontHeight;
            lbWebColors.ItemHeight = FontHeight;

            InitWebColors();

            int nrows = s_officePlt.GetLength(0);
            int ncols = s_officePlt.GetLength(1);
            int marginX = 0;
            int marginY = 0;
            int dx = 1;
            int dy = 1;
            int w = (pnlPal.Width - marginX * 2 - (ncols - 1) * dx) / ncols;
            int h = (pnlPal.Height - marginY * 2 - (nrows - 1) * dy) / nrows;
            marginX = (pnlPal.Width - w * ncols - dx * (ncols - 1)) / 2;
            marginY = (pnlPal.Height - h * nrows - dy * (nrows - 1)) / 2;
            int y = marginY;
            _colorButtons = new ColorButton[nrows * ncols];
            int i = 0;
            for (int row = 0; row < nrows; ++row)
            {
                int x = marginX;
                for (int col = 0; col < ncols; ++col)
                {
                    ColorButton btn = new ColorButton(s_officePlt[row, col].Color);
                    btn.Click += new System.EventHandler(btnPalette_Click);
                    btn.Bounds = new Rectangle(x, y, w, h);
                    //NOTE/TODO: see comment in s_officePlt declaration:
                    // toolTip1.SetToolTip(btn, s_officePlt[row, col].Color.Name);
                    toolTip1.SetToolTip(btn, ColorName(s_officePlt[row, col].Color));
                    pnlPal.Controls.Add(btn);
                    x += w + dx;
                    _colorButtons[i++] = btn;
                }
                y += h + dy;
            }

// TODO: Correct for WPF
#if !WINFX
            // localize:
            C1Localizer.LocalizeForm(this, this.components);
#endif
        }

        private class ColorButton : Button
        {
            private Brush _brush = null;
            private bool _hot = false;
            private bool _selected = false;
            private static Brush _hotBack = new SolidBrush(Color.FromArgb(100, Color.Blue));

            public ColorButton()
            {
                _brush = new SolidBrush(BackColor);
                DialogResult = DialogResult.OK;
            }
            public ColorButton(Color color)
            {
                BackColor = color;
                _brush = new SolidBrush(BackColor);
                DialogResult = DialogResult.OK;
            }
            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    if (_brush != null)
                    {
                        _brush.Dispose();
                        _brush = null;
                    }
                }
                base.Dispose(disposing);
            }
            public override Color BackColor
            {
                get
                {
                    return base.BackColor;
                }
                set
                {
                    base.BackColor = value;
                    if (_brush != null)
                        _brush.Dispose();
                    _brush = new SolidBrush(BackColor);
                }
            }
            public bool Selected
            {
                get { return _selected; }
                set
                {
                    _selected = value;
                    Invalidate();
                }
            }
            protected override void OnPaint(PaintEventArgs e)
            {
                Rectangle r = new Rectangle(Point.Empty, Size);
                Rectangle rb = new Rectangle(0, 0, r.Width - 1, r.Height - 1);
                Rectangle rf = new Rectangle(2, 2, r.Width - 4, r.Height - 4);
                using (Brush b = new SolidBrush(Parent.BackColor))
                    e.Graphics.FillRectangle(b, r);
                if (_hot)
                {
                    e.Graphics.FillRectangle(_hotBack, r);
                    e.Graphics.DrawRectangle(Pens.Blue, rb);
                }
                else if (_selected)
                    e.Graphics.DrawRectangle(Pens.DarkBlue, rb);
                e.Graphics.FillRectangle(_brush, rf);
                rf.Width -= 1;
                rf.Height -= 1;
                e.Graphics.DrawRectangle(Pens.Gray, rf);
                if (Focused)
                    ControlPaint.DrawFocusRectangle(e.Graphics, r);
            }
            protected override void OnMouseEnter(EventArgs e)
            {
                base.OnMouseEnter(e);
                _hot = true;
                Invalidate();
            }
            protected override void OnMouseLeave(EventArgs e)
            {
                base.OnMouseLeave(e);
                _hot = false;
                Invalidate();
            }
        }

        // <summary>
        // Clean up any resources being used.
        // </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        static int[] s_customColors = null;
        private void button1_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.AllowFullOpen = true;
            dlg.AnyColor = true;
            dlg.FullOpen = true;
            dlg.CustomColors = s_customColors;
            dlg.Color = _clr;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                s_customColors = dlg.CustomColors;
                // keep color "custom" (unknown) for uniformity
                _clr = Color.FromArgb(dlg.Color.A, dlg.Color.R, dlg.Color.G, dlg.Color.B);
                ShowSelectedColor(false);
            }
        }

        private class SystemColorComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                Color color1 = (Color)x;
                Color color2 = (Color)y;
                return string.Compare(color1.Name, color2.Name, false, CultureInfo.InvariantCulture);
            }

        }
        private class ColorComparer : IComparer
        {
            public ColorComparer() { }
            //******************************
            // Note:  The following Compare DOES NOT WORK under VS2005 retail builds
            // Seems to be a floating point problem...
#if false
            public int Compare(object x, object y)
            {
                Color color1 = (Color)x;
                Color color2 = (Color)y;
                if (color1.A < color2.A)
                {
                    return -1;
                }
                if (color1.A > color2.A)
                {
                    return 1;
                }
                if (color1.GetHue() < color2.GetHue())
                {
                    return -1;
                }
                if (color1.GetHue() > color2.GetHue())
                {
                    return 1;
                }
                if (color1.GetSaturation() < color2.GetSaturation())
                {
                    return -1;
                }
                if (color1.GetSaturation() > color2.GetSaturation())
                {
                    return 1;
                }
                if (color1.GetBrightness() < color2.GetBrightness())
                {
                    return -1;
                }
                if (color1.GetBrightness() > color2.GetBrightness())
                {
                    return 1;
                }
                return 0;
            }
#else
            public int Compare(object x, object y)
            {
                Color color1 = (Color)x;
                Color color2 = (Color)y;

                byte a1 = color1.A, a2 = color2.A;
                float h1 = color1.GetHue(), h2 = color2.GetHue();
                float s1 = color1.GetSaturation(), s2 = color2.GetSaturation();
                float b1 = color1.GetBrightness(), b2 = color2.GetBrightness();

                h1 = (float)Math.Round(h1, 6, MidpointRounding.ToEven);
                h2 = (float)Math.Round(h2, 6, MidpointRounding.ToEven);
                s1 = (float)Math.Round(s1, 6, MidpointRounding.ToEven);
                s2 = (float)Math.Round(s2, 6, MidpointRounding.ToEven);
                b1 = (float)Math.Round(b1, 6, MidpointRounding.ToEven);
                b2 = (float)Math.Round(b2, 6, MidpointRounding.ToEven);

                int rv = 0;

                if (a1 < a2)
                    rv = -1;
                else if (a1 > a2)
                    rv = 1;
                else if (h1 < h2)
                    rv = -1;
                else if (h1 > h2)
                    rv = 1;
                else if (s1 < s2)
                    rv = -1;
                else if (s1 > s2)
                    rv = 1;
                else if (b1 < b2)
                    rv = -1;
                else if (b1 > b2)
                    rv = 1;

                return rv;
            }
#endif
        }
        private static object[] GetConstants(Type enumType)
        {
            MethodAttributes attributes1 = MethodAttributes.Static | MethodAttributes.Public;
            PropertyInfo[] infoArray1 = enumType.GetProperties();
            ArrayList list1 = new ArrayList();
            for (int num1 = 0; num1 < infoArray1.Length; num1++)
            {
                PropertyInfo info1 = infoArray1[num1];
                if (info1.PropertyType == typeof(Color))
                {
                    MethodInfo info2 = info1.GetGetMethod();
                    if ((info2 != null) && ((info2.Attributes & attributes1) == attributes1))
                    {
                        object[] objArray1 = null;
                        list1.Add(info1.GetValue(null, objArray1));
                    }
                }
            }
            return list1.ToArray();
        }

        private object[] _webColors = null;
        private object[] WebColors
        {
            get
            {
                if (this._webColors == null)
                {
                    this._webColors = C1.Design.ColorPickerOfficeForm.GetConstants((typeof(Color)));
                }
                return this._webColors;
            }
        }

        private object[] _sysColors = null;
        private object[] SystemColors
        {
            get
            {
                if (this._sysColors == null)
                {
                    this._sysColors = C1.Design.ColorPickerOfficeForm.GetConstants(typeof(SystemColors));
                }
                return this._sysColors;
            }
        }

        private void InitWebColors()
        {
            Array.Sort(this.WebColors, new ColorComparer());
            Array.Sort(this.SystemColors, new SystemColorComparer());

            for (int i = 0; i < this.SystemColors.Length; i++)
                lbSysColors.Items.Add(this.SystemColors[i]);

            for (int i = 0; i < this.WebColors.Length; i++)
                lbWebColors.Items.Add(this.WebColors[i]);
        }

        #region Windows Form Designer generated code
        // <summary>
        // Required method for Designer support - do not modify
        // the contents of this method with the code editor.
        // </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpCustom = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.udA = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlPal = new System.Windows.Forms.Panel();
            this.btnCustColor = new C1.Design.ColorPickerOfficeForm.ColorButton();
            this.label4 = new System.Windows.Forms.Label();
            this.udG = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.udB = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.udR = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tpWeb = new System.Windows.Forms.TabPage();
            this.lbWebColors = new System.Windows.Forms.ListBox();
            this.tpSystem = new System.Windows.Forms.TabPage();
            this.lbSysColors = new System.Windows.Forms.ListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1.SuspendLayout();
            this.tpCustom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udR)).BeginInit();
            this.tpWeb.SuspendLayout();
            this.tpSystem.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpCustom);
            this.tabControl1.Controls.Add(this.tpWeb);
            this.tabControl1.Controls.Add(this.tpSystem);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(194, 214);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ColorPickerForm_KeyDown);
            // 
            // tpCustom
            // 
            this.tpCustom.Controls.Add(this.button1);
            this.tpCustom.Controls.Add(this.udA);
            this.tpCustom.Controls.Add(this.label5);
            this.tpCustom.Controls.Add(this.pnlPal);
            this.tpCustom.Controls.Add(this.btnCustColor);
            this.tpCustom.Controls.Add(this.label4);
            this.tpCustom.Controls.Add(this.udG);
            this.tpCustom.Controls.Add(this.label3);
            this.tpCustom.Controls.Add(this.udB);
            this.tpCustom.Controls.Add(this.label2);
            this.tpCustom.Controls.Add(this.udR);
            this.tpCustom.Controls.Add(this.label1);
            this.tpCustom.Location = new System.Drawing.Point(4, 23);
            this.tpCustom.Name = "tpCustom";
            this.tpCustom.Size = new System.Drawing.Size(186, 187);
            this.tpCustom.TabIndex = 0;
            this.tpCustom.Text = "Custom";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(3, 161);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(178, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "More colors...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // udA
            // 
            this.udA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.udA.Location = new System.Drawing.Point(141, 46);
            this.udA.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udA.Name = "udA";
            this.udA.Size = new System.Drawing.Size(40, 20);
            this.udA.TabIndex = 9;
            this.udA.ValueChanged += new System.EventHandler(this.udR_ValueChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(141, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "A:";
            // 
            // pnlPal
            // 
            this.pnlPal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPal.Location = new System.Drawing.Point(3, 73);
            this.pnlPal.Name = "pnlPal";
            this.pnlPal.Size = new System.Drawing.Size(178, 82);
            this.pnlPal.TabIndex = 10;
            // 
            // btnCustColor
            // 
            this.btnCustColor.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCustColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCustColor.Location = new System.Drawing.Point(97, 5);
            this.btnCustColor.Name = "btnCustColor";
            this.btnCustColor.Selected = false;
            this.btnCustColor.Size = new System.Drawing.Size(84, 20);
            this.btnCustColor.TabIndex = 1;
            this.btnCustColor.Click += new System.EventHandler(this.btnCustColor_Click);
            this.btnCustColor.BackColorChanged += new System.EventHandler(this.btnCustColor_BackColorChanged);
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(49, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "G:";
            // 
            // udG
            // 
            this.udG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.udG.Location = new System.Drawing.Point(49, 46);
            this.udG.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udG.Name = "udG";
            this.udG.Size = new System.Drawing.Size(40, 20);
            this.udG.TabIndex = 7;
            this.udG.ValueChanged += new System.EventHandler(this.udR_ValueChanged);
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(95, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "B:";
            // 
            // udB
            // 
            this.udB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.udB.Location = new System.Drawing.Point(95, 46);
            this.udB.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udB.Name = "udB";
            this.udB.Size = new System.Drawing.Size(40, 20);
            this.udB.TabIndex = 8;
            this.udB.ValueChanged += new System.EventHandler(this.udR_ValueChanged);
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(3, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "R:";
            // 
            // udR
            // 
            this.udR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.udR.Location = new System.Drawing.Point(3, 46);
            this.udR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udR.Name = "udR";
            this.udR.Size = new System.Drawing.Size(40, 20);
            this.udR.TabIndex = 6;
            this.udR.ValueChanged += new System.EventHandler(this.udR_ValueChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selected color:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tpWeb
            // 
            this.tpWeb.Controls.Add(this.lbWebColors);
            this.tpWeb.Location = new System.Drawing.Point(4, 23);
            this.tpWeb.Name = "tpWeb";
            this.tpWeb.Size = new System.Drawing.Size(186, 187);
            this.tpWeb.TabIndex = 1;
            this.tpWeb.Text = "Web";
            // 
            // lbWebColors
            // 
            this.lbWebColors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbWebColors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbWebColors.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbWebColors.IntegralHeight = false;
            this.lbWebColors.Location = new System.Drawing.Point(0, 0);
            this.lbWebColors.Name = "lbWebColors";
            this.lbWebColors.Size = new System.Drawing.Size(186, 187);
            this.lbWebColors.TabIndex = 0;
            this.lbWebColors.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbWebColors_DrawItem);
            this.lbWebColors.DoubleClick += new System.EventHandler(this.lbWebColors_DoubleClick);
            this.lbWebColors.SelectedIndexChanged += new System.EventHandler(this.lbWebColors_SelectedIndexChanged);
            this.lbWebColors.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbWebColors_KeyDown);
            // 
            // tpSystem
            // 
            this.tpSystem.Controls.Add(this.lbSysColors);
            this.tpSystem.Location = new System.Drawing.Point(4, 23);
            this.tpSystem.Name = "tpSystem";
            this.tpSystem.Size = new System.Drawing.Size(186, 187);
            this.tpSystem.TabIndex = 2;
            this.tpSystem.Text = "System";
            // 
            // lbSysColors
            // 
            this.lbSysColors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbSysColors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSysColors.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbSysColors.IntegralHeight = false;
            this.lbSysColors.Location = new System.Drawing.Point(0, 0);
            this.lbSysColors.Name = "lbSysColors";
            this.lbSysColors.Size = new System.Drawing.Size(186, 187);
            this.lbSysColors.TabIndex = 0;
            this.lbSysColors.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbWebColors_DrawItem);
            this.lbSysColors.SelectedIndexChanged += new System.EventHandler(this.lbWebColors_SelectedIndexChanged);
            // 
            // ColorPickerOfficeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(194, 214);
            this.ControlBox = false;
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(200, 220);
            this.Name = "ColorPickerOfficeForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Deactivate += new System.EventHandler(this.ColorPickerForm_Deactivate);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ColorPickerForm_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ColorPickerForm_MouseDown);
            this.tabControl1.ResumeLayout(false);
            this.tpCustom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.udA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udR)).EndInit();
            this.tpWeb.ResumeLayout(false);
            this.tpSystem.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private void ColorPickerForm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void lbWebColors_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            ListBox lb = sender as ListBox;
            e.DrawBackground();

            Color clr = (Color)lb.Items[e.Index];

            SolidBrush sb = new SolidBrush(clr);
            Pen pen = new Pen(e.ForeColor);
            e.Graphics.FillRectangle(sb, e.Bounds.X + 2, e.Bounds.Y + 2, 20, e.Bounds.Height - 4);
            e.Graphics.DrawRectangle(pen, e.Bounds.X + 2, e.Bounds.Y + 2, 20, e.Bounds.Height - 4);

            sb.Color = e.ForeColor;
            e.Graphics.DrawString(clr.Name, e.Font, sb, e.Bounds.X + 24, e.Bounds.Y);

            sb.Dispose();
            pen.Dispose();

            if (e.State == DrawItemState.Selected)
                e.DrawFocusRectangle();
        }

        private void lbWebColors_DoubleClick(object sender, System.EventArgs e)
        {
            //Close();
        }

        private void lbWebColors_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void ColorPickerForm_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //Close();
        }

        private void ColorPickerForm_Deactivate(object sender, System.EventArgs e)
        {
            Close();
        }

        private void lbWebColors_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ListBox lb = sender as ListBox;
            if (lb != null)
            {
                _clr = (Color)lb.SelectedItem;
                this.DialogResult = DialogResult.OK;
                if (_canClose)
                    Close();
            }
        }

        private void udR_ValueChanged(object sender, System.EventArgs e)
        {
            if (!_initCust)
            {
                _clr = Color.FromArgb((int)udA.Value, (int)udR.Value, (int)udG.Value, (int)udB.Value);
                ShowSelectedColor(false);
                DialogResult = DialogResult.OK;
            }
        }

        private void btnCustColor_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnPalette_Click(object sender, System.EventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                btnCustColor.BackColor = btn.BackColor;
                Close();
            }
        }

        private void btnCustColor_BackColorChanged(object sender, System.EventArgs e)
        {
            _clr = btnCustColor.BackColor;
            toolTip1.SetToolTip(btnCustColor, ColorName(_clr));
            // btnCustColor.Text = _clr.Name;
        }

        bool _initCust = false;

        private void ShowSelectedColor(bool canSwitchTab)
        {
            _initCust = true;
            // sync the color with the UI
            if (_selectedButton != null)
            {
                _selectedButton.Selected = false;
                _selectedButton = null;
            }
            int index = lbWebColors.Items.IndexOf(_clr);
            if (index != -1)
            {
                if (canSwitchTab)
                    tabControl1.SelectedTab = tpWeb;
                lbWebColors.SelectedIndex = index;
            }
            else
            {
                index = lbSysColors.Items.IndexOf(_clr);
                if (index != -1)
                {
                    if (canSwitchTab)
                        tabControl1.SelectedTab = tpSystem;
                    lbSysColors.SelectedIndex = index;
                }
                else
                {
                    if (canSwitchTab)
                        tabControl1.SelectedTab = tpCustom;
                    foreach (ColorButton btn in _colorButtons)
                    {
                        if (btn.BackColor == _clr)
                        {
                            _selectedButton = btn;
                            break;
                        }
                    }
                }
            }
            btnCustColor.BackColor = _clr;
            udR.Value = _clr.R;
            udG.Value = _clr.G;
            udB.Value = _clr.B;
            udA.Value = _clr.A;
            if (_selectedButton == null)
                _selectedButton = btnCustColor;
            _selectedButton.Selected = true;
            _canClose = true;
            _initCust = false;
        }

        string ColorName(Color clr)
        {
            return clr.Name;
        }
    }
}
