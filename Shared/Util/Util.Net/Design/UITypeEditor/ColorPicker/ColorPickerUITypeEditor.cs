// add additional projects as required
#if C1COMMAND2005 || PREVIEW_2
#define OFFICE_STYLE_COLORS_FORM // use ColorPickerOfficeForm
#else
// default - use ColorPickerForm
#endif
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing;
using System.Drawing.Design;

namespace C1.Design
{
    /// <summary>
    /// 
    /// </summary>
    internal class ColorPickerUITypeEditor : System.Drawing.Design.UITypeEditor
    {
        IWindowsFormsEditorService edSvc = null;
#if OFFICE_STYLE_COLORS_FORM
        private Type _formType = typeof(ColorPickerOfficeForm);
#else
        private Type _formType = typeof(ColorPickerForm);
#endif

        /// <summary>
        /// 
        /// </summary>
        public ColorPickerUITypeEditor()
        {
        }

        /// <summary>
        /// Gets or sets the type of the form used as the color picker.
        /// The form must expose the public property Color Color {get;set;}.
        /// </summary>
        public Type FormType
        {
            get { return _formType; }
            set { _formType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            // use modal dialog
            return (context != null && context.Instance != null)
                ? UITypeEditorEditStyle.DropDown 
                : base.GetEditStyle(context);
        }

        public override bool GetPaintValueSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override void PaintValue(PaintValueEventArgs e)
        {
            Brush b = new SolidBrush((Color)e.Value);
            Rectangle r = e.Bounds;
            e.Graphics.FillRectangle(b, e.Bounds);
            b.Dispose();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (provider == null)
                return value;

            // get service
            edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (edSvc == null)
                return value;

            // initialize editor
            Form frm = Activator.CreateInstance(_formType) as Form;
            if (frm == null)
                return value;

            frm.TopLevel = false;
            frm.FormClosed += new FormClosedEventHandler(frm_FormClosed);
            PropertyDescriptor pdColor = TypeDescriptor.GetProperties(frm)["Color"];
            if (pdColor != null)
                pdColor.SetValue(frm, value);
            edSvc.DropDownControl(frm);
            // dirty it and get the new value if they said picked a selection
            if (frm.DialogResult == DialogResult.OK)
            {
                if (pdColor != null)
                    value = pdColor.GetValue(frm);
                if (context != null && context.Instance != null)
                    context.OnComponentChanged();
            }

            // done			
            return value;
        }

        void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            edSvc.CloseDropDown();
        }
    }
}