using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Globalization;

namespace C1.Design
{
	// <summary>
	// Summary description for ColorPickerForm.
	// </summary>

    // NOTE: If this file is used in a .NET 1.* project, the Color property
    // must be excluded from obfuscation in the project!
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    internal class ColorPickerForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.ListBox lbWebColors;
		// <summary>
		// Required designer variable.
		// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TabPage tpWeb;
		private System.Windows.Forms.TabPage tpSystem;
		private System.Windows.Forms.ListBox lbSysColors;
		private System.Windows.Forms.TabPage tpCustom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown udR;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown udB;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown udG;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown udA;
		private System.Windows.Forms.Button btnCustColor;
		private System.Windows.Forms.Panel pnlPal;

		private Color _clr = Color.Red;

		private static ArrayList webClrs = new ArrayList();
		private static ArrayList sysClrs = new ArrayList();
        bool _canClose = false;

		public Color Color
		{
			get { return _clr; }
			set { _clr = value; }
		}

		public ColorPickerForm( /*Control owner*/)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			Size = new Size( Size.Width, Size.Height - FontHeight);
			InitWebColors();
			Color[,] clrs = new Color[7,7];

			for( int i=0; i<7; i++)
				clrs[0,i] = Color.FromArgb( (i * 255) / 6, (i * 255) / 6, (i * 255) / 6);
			for( int i=0; i<7; i++)
				clrs[1,i] = Color.FromArgb( 96 + (i * 159) / 6, 0, 0);
			for( int i=0; i<7; i++)
				clrs[2,i] = Color.FromArgb( 0, 96 + (i * 159) / 6, 0);
			for( int i=0; i<7; i++)
				clrs[3,i] = Color.FromArgb( 0, 0, 96 + (i * 159) / 6);
			for( int i=0; i<7; i++)
				clrs[4,i] = Color.FromArgb( 0, 96 + (i * 159) / 6, 96 + (i * 159) / 6);
			for( int i=0; i<7; i++)
				clrs[5,i] = Color.FromArgb( 96 + (i * 159) / 6, 0, 96 + (i * 159) / 6);
			for( int i=0; i<7; i++)
				clrs[6,i] = Color.FromArgb( 96 + (i * 159) / 6, 96 + (i * 159) / 6, 0);

            GenerateButtons(pnlPal, 7, 7, clrs);

            C1.Util.Localization.C1Localizer.LocalizeForm(this, components);
        }

		// <summary>
		// Clean up any resources being used.
		// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

        private class SystemColorComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                Color color1 = (Color)x;
                Color color2 = (Color)y;
                return string.Compare(color1.Name, color2.Name, false, CultureInfo.InvariantCulture);
            }

        }
        private class ColorComparer : IComparer
        {
            public ColorComparer() { }
            //******************************
            // Note:  The following Compare DOES NOT WORK under VS2005 retail builds
            // Seems to be a floating point problem...
#if false
            public int Compare(object x, object y)
            {
                Color color1 = (Color)x;
                Color color2 = (Color)y;
                if (color1.A < color2.A)
                {
                    return -1;
                }
                if (color1.A > color2.A)
                {
                    return 1;
                }
                if (color1.GetHue() < color2.GetHue())
                {
                    return -1;
                }
                if (color1.GetHue() > color2.GetHue())
                {
                    return 1;
                }
                if (color1.GetSaturation() < color2.GetSaturation())
                {
                    return -1;
                }
                if (color1.GetSaturation() > color2.GetSaturation())
                {
                    return 1;
                }
                if (color1.GetBrightness() < color2.GetBrightness())
                {
                    return -1;
                }
                if (color1.GetBrightness() > color2.GetBrightness())
                {
                    return 1;
                }
                return 0;
            }
#else
            public int Compare(object x, object y)
            {
                Color color1 = (Color)x;
                Color color2 = (Color)y;

                byte a1 = color1.A, a2 = color2.A;
                float h1 = color1.GetHue(), h2 = color2.GetHue();
                float s1 = color1.GetSaturation(), s2 = color2.GetSaturation();
                float b1 = color1.GetBrightness(), b2 = color2.GetBrightness();

                h1 = (float)Math.Round(h1, 6, MidpointRounding.ToEven);
                h2 = (float)Math.Round(h2, 6, MidpointRounding.ToEven);
                s1 = (float)Math.Round(s1, 6, MidpointRounding.ToEven);
                s2 = (float)Math.Round(s2, 6, MidpointRounding.ToEven);
                b1 = (float)Math.Round(b1, 6, MidpointRounding.ToEven);
                b2 = (float)Math.Round(b2, 6, MidpointRounding.ToEven);

                int rv = 0;

                if (a1 < a2)
                    rv = -1;
                else if (a1 > a2)
                    rv = 1;
                else if (h1 < h2)
                    rv = -1;
                else if (h1 > h2)
                    rv = 1;
                else if (s1 < s2)
                    rv = -1;
                else if (s1 > s2)
                    rv = 1;
                else if (b1 < b2)
                    rv = -1;
                else if (b1 > b2)
                    rv = 1;

                return rv;
            }
#endif
        }
        private static object[] GetConstants(Type enumType)
        {
            MethodAttributes attributes1 = MethodAttributes.Static | MethodAttributes.Public;
            PropertyInfo[] infoArray1 = enumType.GetProperties();
            ArrayList list1 = new ArrayList();
            for (int num1 = 0; num1 < infoArray1.Length; num1++)
            {
                PropertyInfo info1 = infoArray1[num1];
                if (info1.PropertyType == typeof(Color))
                {
                    MethodInfo info2 = info1.GetGetMethod();
                    if ((info2 != null) && ((info2.Attributes & attributes1) == attributes1))
                    {
                        object[] objArray1 = null;
                        list1.Add(info1.GetValue(null, objArray1));
                    }
                }
            }
            return list1.ToArray();
        }

        private object[] _webColors = null;
        private object[] WebColors
        {
            get
            {
                if (this._webColors == null)
                {
                    this._webColors = C1.Design.ColorPickerForm.GetConstants((typeof(Color)));
                }
                return this._webColors;
            }
        }

        private object[] _sysColors = null;
        private object[] SystemColors
        {
            get
            {
                if (this._sysColors == null)
                {
                    this._sysColors = C1.Design.ColorPickerForm.GetConstants(typeof(SystemColors));
                }
                return this._sysColors;
            }
        }

        private void InitWebColors()
		{
            Array.Sort(this.WebColors, new ColorComparer());
            Array.Sort(this.SystemColors, new SystemColorComparer());

            for (int i = 0; i < this.SystemColors.Length; i++)
                lbSysColors.Items.Add(this.SystemColors[i]);

            for (int i = 0; i < this.WebColors.Length; i++)
                lbWebColors.Items.Add(this.WebColors[i]);
		}

		#region Windows Form Designer generated code
		// <summary>
		// Required method for Designer support - do not modify
		// the contents of this method with the code editor.
		// </summary>
		private void InitializeComponent()
		{
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpWeb = new System.Windows.Forms.TabPage();
            this.lbWebColors = new System.Windows.Forms.ListBox();
            this.tpSystem = new System.Windows.Forms.TabPage();
            this.lbSysColors = new System.Windows.Forms.ListBox();
            this.tpCustom = new System.Windows.Forms.TabPage();
            this.udA = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlPal = new System.Windows.Forms.Panel();
            this.btnCustColor = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.udG = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.udB = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.udR = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tpWeb.SuspendLayout();
            this.tpSystem.SuspendLayout();
            this.tpCustom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udR)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpWeb);
            this.tabControl1.Controls.Add(this.tpSystem);
            this.tabControl1.Controls.Add(this.tpCustom);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(197, 234);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ColorPickerForm_KeyDown);
            // 
            // tpWeb
            // 
            this.tpWeb.Controls.Add(this.lbWebColors);
            this.tpWeb.Location = new System.Drawing.Point(4, 22);
            this.tpWeb.Name = "tpWeb";
            this.tpWeb.Size = new System.Drawing.Size(186, 208);
            this.tpWeb.TabIndex = 0;
            this.tpWeb.Text = "Web";
            // 
            // lbWebColors
            // 
            this.lbWebColors.BackColor = System.Drawing.SystemColors.Control;
            this.lbWebColors.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbWebColors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbWebColors.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbWebColors.IntegralHeight = false;
            this.lbWebColors.Location = new System.Drawing.Point(0, 0);
            this.lbWebColors.Name = "lbWebColors";
            this.lbWebColors.Size = new System.Drawing.Size(186, 208);
            this.lbWebColors.TabIndex = 0;
            this.lbWebColors.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbWebColors_DrawItem);
            this.lbWebColors.DoubleClick += new System.EventHandler(this.lbWebColors_DoubleClick);
            this.lbWebColors.SelectedIndexChanged += new System.EventHandler(this.lbWebColors_SelectedIndexChanged);
            this.lbWebColors.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbWebColors_KeyDown);
            // 
            // tpSystem
            // 
            this.tpSystem.Controls.Add(this.lbSysColors);
            this.tpSystem.Location = new System.Drawing.Point(4, 22);
            this.tpSystem.Name = "tpSystem";
            this.tpSystem.Size = new System.Drawing.Size(186, 208);
            this.tpSystem.TabIndex = 1;
            this.tpSystem.Text = "System";
            // 
            // lbSysColors
            // 
            this.lbSysColors.BackColor = System.Drawing.SystemColors.Control;
            this.lbSysColors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbSysColors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSysColors.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbSysColors.IntegralHeight = false;
            this.lbSysColors.Location = new System.Drawing.Point(0, 0);
            this.lbSysColors.Name = "lbSysColors";
            this.lbSysColors.Size = new System.Drawing.Size(186, 208);
            this.lbSysColors.TabIndex = 1;
            this.lbSysColors.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbWebColors_DrawItem);
            this.lbSysColors.SelectedIndexChanged += new System.EventHandler(this.lbWebColors_SelectedIndexChanged);
            // 
            // tpCustom
            // 
            this.tpCustom.Controls.Add(this.udA);
            this.tpCustom.Controls.Add(this.label5);
            this.tpCustom.Controls.Add(this.pnlPal);
            this.tpCustom.Controls.Add(this.btnCustColor);
            this.tpCustom.Controls.Add(this.label4);
            this.tpCustom.Controls.Add(this.udG);
            this.tpCustom.Controls.Add(this.label3);
            this.tpCustom.Controls.Add(this.udB);
            this.tpCustom.Controls.Add(this.label2);
            this.tpCustom.Controls.Add(this.udR);
            this.tpCustom.Controls.Add(this.label1);
            this.tpCustom.Location = new System.Drawing.Point(4, 22);
            this.tpCustom.Name = "tpCustom";
            this.tpCustom.Size = new System.Drawing.Size(189, 208);
            this.tpCustom.TabIndex = 2;
            this.tpCustom.Text = "Custom";
            // 
            // udA
            // 
            this.udA.Location = new System.Drawing.Point(4, 48);
            this.udA.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udA.Name = "udA";
            this.udA.Size = new System.Drawing.Size(40, 21);
            this.udA.TabIndex = 7;
            this.udA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udA.ValueChanged += new System.EventHandler(this.udR_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "A:";
            // 
            // pnlPal
            // 
            this.pnlPal.Location = new System.Drawing.Point(4, 72);
            this.pnlPal.Name = "pnlPal";
            this.pnlPal.Size = new System.Drawing.Size(184, 136);
            this.pnlPal.TabIndex = 17;
            // 
            // btnCustColor
            // 
            this.btnCustColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCustColor.Location = new System.Drawing.Point(100, 6);
            this.btnCustColor.Name = "btnCustColor";
            this.btnCustColor.Size = new System.Drawing.Size(78, 25);
            this.btnCustColor.TabIndex = 9;
            this.btnCustColor.Click += new System.EventHandler(this.btnCustColor_Click);
            this.btnCustColor.BackColorChanged += new System.EventHandler(this.btnCustColor_BackColorChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(107, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "G:";
            // 
            // udG
            // 
            this.udG.Location = new System.Drawing.Point(100, 48);
            this.udG.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udG.Name = "udG";
            this.udG.Size = new System.Drawing.Size(40, 21);
            this.udG.TabIndex = 5;
            this.udG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udG.ValueChanged += new System.EventHandler(this.udR_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(155, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "B:";
            // 
            // udB
            // 
            this.udB.Location = new System.Drawing.Point(148, 48);
            this.udB.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udB.Name = "udB";
            this.udB.Size = new System.Drawing.Size(40, 21);
            this.udB.TabIndex = 3;
            this.udB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udB.ValueChanged += new System.EventHandler(this.udR_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(59, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "R:";
            // 
            // udR
            // 
            this.udR.Location = new System.Drawing.Point(52, 48);
            this.udR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.udR.Name = "udR";
            this.udR.Size = new System.Drawing.Size(40, 21);
            this.udR.TabIndex = 1;
            this.udR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udR.ValueChanged += new System.EventHandler(this.udR_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selected color:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ColorPickerForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(194, 234);
            this.ControlBox = false;
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(200, 240);
            this.Name = "ColorPickerForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Deactivate += new System.EventHandler(this.ColorPickerForm_Deactivate);
            this.Shown += new System.EventHandler(this.ColorPickerForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ColorPickerForm_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ColorPickerForm_MouseDown);
            this.tabControl1.ResumeLayout(false);
            this.tpWeb.ResumeLayout(false);
            this.tpSystem.ResumeLayout(false);
            this.tpCustom.ResumeLayout(false);
            this.tpCustom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udR)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void ColorPickerForm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Escape)
				Close();
		}

		private void lbWebColors_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
		{
			ListBox lb = sender as ListBox;
			e.DrawBackground();

			Color clr = (Color)lb.Items[ e.Index];

			SolidBrush sb = new SolidBrush( clr);
			Pen pen = new Pen( e.ForeColor);
			e.Graphics.FillRectangle( sb, e.Bounds.X + 2, e.Bounds.Y + 2, 20, e.Bounds.Height - 4);
			e.Graphics.DrawRectangle( pen, e.Bounds.X + 2, e.Bounds.Y + 2, 20, e.Bounds.Height - 4);

			sb.Color = e.ForeColor;
			e.Graphics.DrawString( clr.Name, e.Font, sb, e.Bounds.X + 24, e.Bounds.Y);

			sb.Dispose();
			pen.Dispose();

			if( e.State == DrawItemState.Selected)
				e.DrawFocusRectangle();
		}

		private void lbWebColors_DoubleClick(object sender, System.EventArgs e)
		{
			//Close();
		}

		private void lbWebColors_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Escape)
				Close();
		}

		private void ColorPickerForm_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			//Close();
		}

		private void ColorPickerForm_Deactivate(object sender, System.EventArgs e)
		{
			Close();
		}

		private void lbWebColors_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListBox lb = sender as ListBox;
			if( lb!=null)
			{
				_clr = (Color)lb.SelectedItem;
                this.DialogResult = DialogResult.OK;
                if( _canClose)
				    Close();
			}
		}

		bool _initCust = false;

		private void tabControl1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if( tabControl1.SelectedTab == tpCustom)
			{
				_initCust = true;
				udR.Value = _clr.R;
				udG.Value = _clr.G;
				udB.Value = _clr.B;
				udA.Value = _clr.A;
				btnCustColor.BackColor = _clr;
				_initCust = false;
			}
		}

		private void udR_ValueChanged(object sender, System.EventArgs e)
		{
			if( !_initCust)
			{
				_clr = Color.FromArgb( (int)udA.Value, (int)udR.Value, (int)udG.Value, (int)udB.Value);
				btnCustColor.BackColor = _clr;
				btnCustColor.Text = _clr.Name;
			}
		}

		private void btnCustColor_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void btnPalette_Click(object sender, System.EventArgs e)
		{
			Button btn = sender as Button;
			if( btn!=null)
			{
				btnCustColor.BackColor = btn.BackColor;
				Close();
			}
		}

		private void GenerateButtons( Panel pnl, int xn, int yn, Color[,] clrs)
		{
			int margin = 4;
			int btnW = pnl.Width / xn - margin;
			int btnH = pnl.Height/ yn - margin;

			Button[,] btns = new Button[xn, yn];

			int locx = margin / 2;
			for( int ix = 0; ix<xn; ix++)
			{
				int locy = margin / 2;
				for( int iy = 0; iy<yn; iy++)
				{
					btns[ix,iy] = new Button();
					btns[ix,iy].Size = new Size( btnW, btnH);
					btns[ix,iy].Location = new Point( locx, locy);
					btns[ix,iy].FlatStyle = FlatStyle.Popup;

					if( clrs!=null)
						btns[ix,iy].BackColor = clrs[ix,iy];

					btns[ix,iy].Click += new System.EventHandler(btnPalette_Click);

                    btns[ix, iy].DialogResult = DialogResult.OK;
					pnl.Controls.Add( btns[ix,iy]);

					locy += btnH + margin;
				}

				locx += btnW + margin;
			}
		}

		private void btnCustColor_BackColorChanged(object sender, System.EventArgs e)
		{
			_clr = btnCustColor.BackColor;
		}

        private void ColorPickerForm_Shown(object sender, EventArgs e)
        {
            // sync the color with the UI
            int index = lbWebColors.Items.IndexOf(_clr);
            if (index != -1)
            {
                tabControl1.SelectedIndex = 0;
                lbWebColors.SelectedIndex = index;
            }
            else
            {
                index = lbSysColors.Items.IndexOf(_clr);
                if (index != -1)
                {
                    tabControl1.SelectedIndex = 1;
                    lbSysColors.SelectedIndex = index;
                }
                else
                    tabControl1.SelectedIndex = 2;
            }
            _canClose = true;
        }
	}

#if false
	internal class DropDownButton : System.Windows.Forms.Button
	{
		protected bool _isDialog = false;
		public bool IsDialog
		{
			get{ return _isDialog;}
		}

		public DropDownButton()
		{
            FlatStyle = FlatStyle.Standard;
            Paint += new PaintEventHandler(DropDownButton_Paint);
		}

        void DropDownButton_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rc = new Rectangle(ClientRectangle.Right - 20,
                ClientRectangle.Top + 3, 15, ClientRectangle.Height - 6);

            if (this.FlatStyle == FlatStyle.Flat)
                ControlPaint.DrawComboButton(e.Graphics, rc, ButtonState.Flat);
            else
            {
                //if (System.Windows.Forms.VisualStyles.VisualStyleInformation.IsEnabledByUser)
                if( Application.RenderWithVisualStyles)
                {
                    System.Windows.Forms.VisualStyles.ComboBoxState state = System.Windows.Forms.VisualStyles.ComboBoxState.Normal;

                    if (!Enabled)
                        state = System.Windows.Forms.VisualStyles.ComboBoxState.Disabled;

                    ComboBoxRenderer.DrawDropDownButton(e.Graphics, rc, state);
                }
                else
                    ControlPaint.DrawComboButton(e.Graphics, rc, ButtonState.Flat);

            }
        }
	}

	internal class ColorPicker : DropDownButton
	{
		private SolidBrush _sb;
		private Color _clr = Color.Red;

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public Color Color
		{
			get
			{
				return _clr;
			}
			set
			{
				_clr = value;
				Invalidate();
			}
		}

		public ColorPicker()
		{
			_sb = new SolidBrush( _clr);
		}

		override protected void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			Rectangle rc = new Rectangle( ClientRectangle.Right - 53,
				ClientRectangle.Top + 5, 30, ClientRectangle.Height - 10);

			if( _sb!=null)
			{
				if( _sb.Color!=_clr)
					_sb.Color = _clr;
				e.Graphics.FillRectangle( _sb, rc);
			}
		}

		override protected void OnClick(EventArgs e)
		{
			_isDialog = true;
			ColorPickerForm cpf = new ColorPickerForm(/* this*/);
			cpf.Color = Color;

#if false
#if !Chart3DCode
			ChartWizard2Form _wf = FindForm() as ChartWizard2Form;

			if( _wf!=null)
				_wf.IsPopup = true;
#endif


#if !Chart3DCode
			if( _wf!=null)
				_wf.IsPopup = false;
#endif
#endif
            cpf.ShowDialog();

			this.Color = cpf.Color;
			base.OnClick(e);
			_isDialog = false;
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(_sb!=null)
				{
					_sb.Dispose();
					_sb = null;
				}
			}
			base.Dispose( disposing );
		}
	}
#endif
}