using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace C1.Design
{
    internal partial class ColorPickerControl : ComboBox
    {
        ColorPickerForm _colorPickerForm = null;
        Color _clr = Color.Transparent;
        string _text = string.Empty;

        public ColorPickerControl()
        {
            //InitializeComponent();
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            this.DrawMode = DrawMode.OwnerDrawVariable;
            this.Items.Add(this.Text);
            this.SelectedIndex = 0;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            // TODO: Add custom paint code here

            // Calling the base class OnPaint
            base.OnPaint(pe);
        }

        protected override void OnMeasureItem(System.Windows.Forms.MeasureItemEventArgs e)
        {
            if (e.Index > -1)
            {
                string fontstring = this.Text;
                Graphics g = CreateGraphics();
                e.ItemHeight = (int)g.MeasureString(this.Text, this.Font).Height;
                e.ItemWidth = System.Math.Max((int)g.MeasureString(this.Text, this.Font).Width, this.Width);

            }
            base.OnMeasureItem(e);
        }
        protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
        {
            if (e.Index > -1 && (e.State & DrawItemState.ComboBoxEdit) != 0 )
            {
                SolidBrush backbrush = null;
                SolidBrush textbrush = null;
                    backbrush = new SolidBrush(SystemColors.Window);
                    textbrush = new SolidBrush(SystemColors.WindowText);

                e.Graphics.FillRectangle(backbrush,
                        e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height);

                e.Graphics.DrawString(this.Text, this.Font, textbrush,
                        e.Bounds.X, e.Bounds.Y);

                int width = 1;
                if (this.Text.Length > 0)
                    width += (int)e.Graphics.MeasureString(this.Text, this.Font).Width+4;
                Brush b = new SolidBrush(this.Color);
                Rectangle r = e.Bounds;
                r.X += width;
                r.Width -= width;
                ControlPaint.DrawFocusRectangle(e.Graphics, r);
                r.Inflate(-1, -1);
                e.Graphics.FillRectangle(b, r);
                b.Dispose();

            }
            base.OnDrawItem(e);
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            if (this._colorPickerForm != null && _colorPickerForm.Visible)
            {
                this._colorPickerForm.Close();
            }
            else
            {
                this._colorPickerForm = new ColorPickerForm();
                this._colorPickerForm.Color = this._clr;
                Rectangle r = this.ClientRectangle;
                r = this.Bounds;
                r = this.Parent.RectangleToScreen(r);
                //r = this.RectangleToScreen(r);
                this._colorPickerForm.Location = new Point(r.X, r.Y + r.Height);
                this._colorPickerForm.Width = r.Width;
                this._colorPickerForm.FormClosed += new FormClosedEventHandler(_colorPickerForm_FormClosed);
                this._colorPickerForm.Show();
            }
        }

        void _colorPickerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this._colorPickerForm.FormClosed -= new FormClosedEventHandler(_colorPickerForm_FormClosed);
            this.Color = _colorPickerForm.Color;
            this.OnDropDownClosed(EventArgs.Empty);
        }
        public Color Color
        {
            get { return this._clr; }
            set 
            { 
                _clr = value; 
                Invalidate(); 
            }
        }

        //[DefaultValue("")]
        //public string Text
        //{
        //    get { return this._text; }
        //    set { this._text = value; Invalidate(); }
        //}
    }
}
