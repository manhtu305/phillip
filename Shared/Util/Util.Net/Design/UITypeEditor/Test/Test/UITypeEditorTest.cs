using System;
using System.Drawing;
using System.ComponentModel;

namespace Test
{
    // identifies what property changed
    public enum PropertyChangedEnum
    {
        Mask,
        Format,
        Color
    }
 
    public class PropertyChangedEventArgs : EventArgs
    {
        public readonly PropertyChangedEnum _pce = PropertyChangedEnum.Mask;
        internal PropertyChangedEventArgs(PropertyChangedEnum pce)
        {
            _pce = pce;
        }
    }

    // an object to test the various UITypeEditors
    // Make sure to update the PropertyChangeEnum and
    // also call the OnChange() method on the property setter
    public class UITypeEditorTest
    {
        string _mask = string.Empty;
        string _format = string.Empty;
        Color _color = Color.Red;

        public delegate void PropertyChangedEventHandler(object sender, PropertyChangedEventArgs e);
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnChanged(PropertyChangedEnum pce)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(pce));
            }
        }

        [Editor(typeof(C1.Design.C1InputMaskUITypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Mask
        {
            get { return _mask; }
            set 
            {
                if( _mask != value )
                {
                    _mask = value; 
                    OnChanged(PropertyChangedEnum.Mask);
                }
            }
        }

        [Editor(typeof(C1.Design.C1FormatStringUITypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Format
        {
            get { return _format; }
            set
            {
                if (_format != value)
                {
                    _format = value;
                    OnChanged(PropertyChangedEnum.Format);
                }
            }
        }

        [Editor(typeof(C1.Design.ColorPickerUITypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public Color Color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    _color = value;
                    OnChanged(PropertyChangedEnum.Color);
                }
            }
        }
    }
}