using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Test
{
    public partial class Form1 : Form
    {
        UITypeEditorTest _testObj = new UITypeEditorTest();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this._testObj.PropertyChanged += new UITypeEditorTest.PropertyChangedEventHandler(_testObj_PropertyChanged);
            this.propertyGrid1.SelectedObject = _testObj;

        }

        void _testObj_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.maskedTextBox1.Mask = this._testObj.Mask;
            this.textBox1.Text = this._testObj.Mask;
        }
    }
}