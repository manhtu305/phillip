using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Text;

namespace C1.Design
{
	[System.Reflection.Obfuscation(Exclude = true, ApplyToMembers = true)]
	internal class FlagsUITypeEditor : UITypeEditor
	{
		private IWindowsFormsEditorService _edSvc;
		private CheckedListBox _listBox;
		private bool _cancel;

		public FlagsUITypeEditor()
		{
			// build selector list
			_listBox = new CheckedListBox();
			_listBox.BorderStyle = BorderStyle.None;
			_listBox.CheckOnClick = true;
			_listBox.Width = 200;
			_listBox.IntegralHeight = true;
			_listBox.ThreeDCheckBoxes = false;
			_listBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(_listBox_KeyPress);
			_listBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(_listBox_ItemCheck);
		}
		override public UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext ctx)
		{
			return UITypeEditorEditStyle.DropDown;
		}
		override public object EditValue(ITypeDescriptorContext ctx, IServiceProvider provider, object value)
		{
			// initialize editor service
			_edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
			if (_edSvc == null) return value;

			// populate the list
			_listBox.Items.Clear();
			foreach (object item in Enum.GetValues(ctx.PropertyDescriptor.PropertyType))
			{
				// skip 'none', people have to uncheck all options to get it
				// (if we added this to the list, it would always be checked...)
				if ((int)item == 0) continue;

				// add this item with the proper check state
				CheckState check = (((int)value & (int)item) == (int)item)
					? CheckState.Checked
					: CheckState.Unchecked;
				_listBox.Items.Add(item, check);
			}

			// show the list
			_cancel = false;
			_edSvc.DropDownControl(_listBox);

			// build return value from checked items on the list
			if (!_cancel)
			{
				// build a comma-delimited string with the checked items
				StringBuilder sb = new StringBuilder();
				foreach (object item in _listBox.CheckedItems)
				{
					if (sb.Length > 0) sb.Append(", ");
					sb.Append(item.ToString());
				}

				// convert empty string into '0' (no flags) and then into proper type
				string s = sb.Length > 0 ? sb.ToString() : "0";
				value = ctx.PropertyDescriptor.Converter.ConvertFrom(s);
			}

			// done
			return value;
		}

		// close editor if the user presses enter or escape
		private void _listBox_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			switch (e.KeyChar)
			{
				case (char)27:
					_cancel = true;
					if (_edSvc != null) _edSvc.CloseDropDown();
					break;
				case (char)13:
					if (_edSvc != null) _edSvc.CloseDropDown();
					break;
			}
		}

		// after an item is checked, update all other items
		// (in case we have combination flags, e.g. All)
		bool _checking = false;
		private void _listBox_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			// no reentrancy
			if (!_checking)
			{
				_checking = true;

				// get current value
				int value = 0;
				for (int i = 0; i < _listBox.Items.Count; i++)
				{
					if (i != e.Index && _listBox.GetItemChecked(i))
						value |= (int)_listBox.Items[i];
				}

				// apply the change that was just made
				if (e.NewValue == CheckState.Checked)
					value |= (int)_listBox.Items[e.Index];
				else
					value &= ~(int)_listBox.Items[e.Index];

				// apply new value to all items on the list
				for (int i = 0; i < _listBox.Items.Count; i++)
				{
					bool check = (value & (int)_listBox.Items[i]) == (int)_listBox.Items[i];
					_listBox.SetItemChecked(i, check);
				}

				// all done
				_checking = false;
			}
		}
	}
}