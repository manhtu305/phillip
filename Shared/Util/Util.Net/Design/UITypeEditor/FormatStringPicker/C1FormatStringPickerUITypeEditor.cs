using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing.Design;

namespace C1.Design
{
	/// <summary>
	/// 
	/// </summary>
	internal class C1FormatStringUITypeEditor : System.Drawing.Design.UITypeEditor
	{
		/// <summary>
		/// 
		/// </summary>
		public C1FormatStringUITypeEditor()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			// use modal dialog
			return (context != null && context.Instance != null)
					? UITypeEditorEditStyle.Modal
					: base.GetEditStyle(context);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="provider"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (context == null || context.Instance == null || provider == null)
				return value;

			// get service
			IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
			if (edSvc == null)
				return value;

			// initialize editor
			C1FormatStringPickerForm frm = new C1FormatStringPickerForm();
            frm.ShowNullText = false; // NullText value is not used and should not be displayed to the user!

			// set the format string
			frm.FormatString = (string)value;

			// show the editor
			DialogResult dr = edSvc.ShowDialog(frm);

			// dirty it and get the new value if they said picked a selection
			if (dr == DialogResult.OK)
			{
				value = frm.FormatString;
				context.OnComponentChanged();
			}

			// done			
			return value;
		}
	}
}