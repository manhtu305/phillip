using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using C1.Util.Localization;


namespace C1.Design
{
    internal partial class C1FormatStringPicker : UserControl
    {
        private bool customTyping = false;
        private string m_format = string.Empty;
        private string[] fmtNumeric = { "N0", "N1", "N2", "N3", "N4", "N5", "N6" };
        private string[] fmtCurrency = { "C0", "C1", "C2", "C3", "C4", "C5", "C6" };
        private string[] fmtScientific = { "E0", "E1", "E2", "E3", "E4", "E5", "E6" };
        private string[] fmtDateTime = { "d", "D", "f", "F", "g", "G", "t", "T", "M" };
        private StringCollection collNumeric = new StringCollection();
        private StringCollection collCurrency = new StringCollection();
        private StringCollection collScientific = new StringCollection();
        private StringCollection collDateTime = new StringCollection();
        private int _nonUserUpdating = 0;

        public C1FormatStringPicker()
        {
            InitializeComponent();
            collNumeric.AddRange(fmtNumeric);
            collCurrency.AddRange(fmtCurrency);
            collScientific.AddRange(fmtScientific);
            collDateTime.AddRange(fmtDateTime);

#if GRAPECITY
            // use MS UI Gothic so the yen sign displays correctly
            // (with two cross-bars instead of one, see delta issue VNFLX000778)
            //labelSample.Font = Control.DefaultFont; // not sure this would work...
            labelSample.Font = new Font("MS UI Gothic", 9);
#endif
        }

        private void ResetControls()
        {
            labelDecimalPlaces.Visible = IsDecimal;
            domainDecimalPlaces.Visible = IsDecimal;
            labelCustom.Visible = IsCustom;
            textBoxCustom.Visible = IsCustom;
            labelDateType.Visible = IsDateTime;
            listBoxDateType.Visible = IsDateTime;
        }

        private void ResetComponent()
        {
            if (customTyping)
            {
                return;
            }
            string type;

            //if (m_format == string.Empty) <<B319>> fixes delta issue VNFLX000770
            if (m_format == string.Empty && FormatType != "Custom")
            {
                type = "None";
            }
            else if (collNumeric.Contains(m_format))
            {
                type = "Numeric";
                domainDecimalPlaces.SelectedItem = m_format.Substring(1, 1);
            }
            else if (collCurrency.Contains(m_format))
            {
                type = "Currency";
                domainDecimalPlaces.SelectedItem = m_format.Substring(1, 1);
            }
            else if (collScientific.Contains(m_format))
            {
                type = "Scientific";
                domainDecimalPlaces.SelectedItem = m_format.Substring(1, 1);
            }
            else if (collDateTime.Contains(m_format))
            {
                type = "DateTime";
                listBoxDateType.SelectedItem = m_format;
            }
            else
            {
                type = "Custom";
                textBoxCustom.Text = m_format;
            }
            if (type != FormatType)
            {
                FormatType = type;
                ResetControls();
            }
        }

        private bool IsDecimal
        {
            get
            {
                string t = FormatType;
                return (t == "Numeric" || t == "Currency" || t == "Scientific");
            }
        }

        private bool IsCustom
        {
            get { return FormatType == "Custom"; }
        }

        private bool IsDateTime
        {
            get { return FormatType == "DateTime"; }
        }

        private string FormatType
        {
            get { return (listBoxType.SelectedItem != null) ? listBoxType.SelectedItem.ToString() : string.Empty; }
            set 
            {
                BeginNonUserUpdate();
                try
                {
                    listBoxType.SelectedItem = value;
                }
                finally
                {
                    EndNonUserUpdate();
                }
            }
        }

        private object Sample
        {
            get
            {
                if (IsDateTime)
                {
                    return (object)DateTime.Now;
                }
                
                if (IsCustom)
                {
                    string fmt = textBoxCustom.Text.ToUpper();
                    if (fmt == "D" || fmt.StartsWith("DD"))
                    {
                        return (object)DateTime.Now;
                    }
                    if (fmt.StartsWith("X") || fmt.StartsWith("D"))
                    {
                        return (object)-1234;
                    }
                }

                return (object) (double) -1234.5678;
            }
        }

        private void RecalcFormat()
        {
            string fmt = string.Empty;

            switch (FormatType)
            {
                case "Numeric":
                    fmt = "N" + domainDecimalPlaces.Text;
                    break;
                case "Currency":
                    fmt = "C" + domainDecimalPlaces.Text;
                    break;
                case "Scientific":
                    fmt = "E" + domainDecimalPlaces.Text;
                    break;
                case "DateTime":
                    fmt = listBoxDateType.SelectedItem.ToString();
                    break;
                case "Custom":
                    fmt = textBoxCustom.Text;
                    break;
            }

            Format = fmt;
            if (!InNonUserUpdate)
            {
                OnUserPropertyChanged();
            }
        }

        private void RecalcSample()
        {
            string disp = IsCustom ? string.Empty : Sample.ToString();

            if (m_format != null && m_format.Trim() != string.Empty)
            {
                string fmt = string.Format("{{0:{0}}}", m_format.Trim());
                try
                {
                    disp = string.Format(fmt, Sample);
                }
                catch
                {
                    disp = string.Format(fmt, DateTime.Now);
                }
            }

            labelSample.Text = disp;
        }

        private string GetString(string name)
		{
			// localize the return strings in this member function only
            switch (name)
			{
				case "FormatDescCurrency":
					return C1Localizer.GetString("Specify the format for monetary values.");

				case "FormatDescCustom":
					return C1Localizer.GetString("Type a custom format string. A custom string may require extra handling unless it is a read-only value.");

				case "FormatDescDateTime":
					return C1Localizer.GetString("Specify the format for date and time values.");

				case "FormatDescNone":
					return C1Localizer.GetString("Use no formatting to display the value from the source without adornment.");

				case "FormatDescNumeric":
					return C1Localizer.GetString("Specify the format for numbers. Note that the currency format type offers specialized formatting for monetary values.");

				case "FormatDescScientific":
					return C1Localizer.GetString("Specify the format for values that use scientific notation.");

				case "FormatTypeCurrency":
					return C1Localizer.GetString("Currency");

				case "FormatTypeCustom":
					return C1Localizer.GetString("Custom");

				case "FormatTypeDateTime":
					return C1Localizer.GetString("Date Time");

				case "FormatTypeNone":
					return C1Localizer.GetString("No Formatting");

				case "FormatTypeNumeric":
					return C1Localizer.GetString("Numeric");

				case "FormatTypeScientific":
					return C1Localizer.GetString("Scientific");

				default:
					return name;
			}
        }

        [DefaultValue("")]
        [Bindable(BindableSupport.Yes)]
        public string NullText
        {
            get { return textBoxNullValue.Text; }
            set { textBoxNullValue.Text = value; }
        }

        [DefaultValue(true)]
        public bool ShowNullText
        {
            get { return textBoxNullValue.Visible; }
            set 
            {
                labelNullValue.Visible = value;
                textBoxNullValue.Visible = value;
            }
        }

        [DefaultValue("")]
        [Bindable(BindableSupport.Yes)]
        public string Format
        {
            get { return m_format; }
            set
            {
                if (m_format != value)
                {
                    BeginNonUserUpdate();
                    try
                    {
                        m_format = value;
                        ResetComponent();
                        RecalcSample();

                        Binding b = DataBindings["Format"];
                        if (b != null) b.WriteValue();
                    }
                    finally
                    {
                        EndNonUserUpdate();
                    }
                }
            }
        }

        public event EventHandler UserPropertyChanged;

        private void OnUserPropertyChanged()
        {
            if (UserPropertyChanged != null)
            {
                UserPropertyChanged(this, EventArgs.Empty);
            }
        }

        private void listBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = "FormatDesc" + FormatType;
            labelDescription.Text = GetString(name);
            ResetControls();
            RecalcFormat();
        }

        private void listBoxType_Format(object sender, ListControlConvertEventArgs e)
        {
            string name = "FormatType" + e.ListItem.ToString();
            e.Value = GetString(name);
        }

        private void FormatStringPicker_Load(object sender, EventArgs e)
        {
            BeginNonUserUpdate();
            try
            {
                if (listBoxType.SelectedItem == null)
                    FormatType = "None";

                if (domainDecimalPlaces.SelectedItem == null)
                    domainDecimalPlaces.SelectedItem = "2";

                if (listBoxDateType.SelectedIndex == -1)
                    listBoxDateType.SelectedIndex = 0;
            }
            finally
            {
                EndNonUserUpdate();
            }
        }

        private void domainDecimalPlaces_SelectedItemChanged(object sender, EventArgs e)
        {
            RecalcFormat();
        }

        private void textBoxCustom_TextChanged(object sender, EventArgs e)
        {
            customTyping = true;
            RecalcFormat();
            customTyping = false;
        }

        private void listBoxDateType_Format(object sender, ListControlConvertEventArgs e)
        {
            StringBuilder sb = new StringBuilder("{0:");
            sb.Append(e.ListItem.ToString());
            sb.Append("}");
            e.Value = string.Format(sb.ToString(), DateTime.Now);
        }

        private void listBoxDateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            RecalcFormat();
        }

        private void textBoxNullValue_Enter(object sender, EventArgs e)
        {
            textBoxNullValue.Modified = false;
        }

        private void textBoxNullValue_Validated(object sender, EventArgs e)
        {
            if (textBoxNullValue.Modified)
            {
                OnUserPropertyChanged();
            }
        }

        private void BeginNonUserUpdate()
        {
            _nonUserUpdating++;
        }
        private void EndNonUserUpdate()
        {
            if (_nonUserUpdating > 0)
            {
                _nonUserUpdating--;
            }
        }
        private bool InNonUserUpdate
        {
            get { return _nonUserUpdating > 0; }
        }
    }
}
