using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace C1.Design
{
    internal partial class C1FormatStringPickerForm : Form
    {
        public C1FormatStringPickerForm()
        {
            InitializeComponent();
            C1.Util.Localization.C1Localizer.LocalizeForm(this, components);
        }

        public string FormatString
        {
            get { return this.c1FormatStringPicker1.Format; }
            set { this.c1FormatStringPicker1.Format = value; }
        }

        public bool ShowNullText
        {
            get { return this.c1FormatStringPicker1.ShowNullText; }
            set { this.c1FormatStringPicker1.ShowNullText = value; }
        }

        public string NullText
        {
            get { return this.c1FormatStringPicker1.NullText; }
            set { this.c1FormatStringPicker1.NullText = value; }
        }
    }
}