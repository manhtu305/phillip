namespace C1.Design
{
    partial class C1FormatStringPicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxContainer = new System.Windows.Forms.GroupBox();
            this.domainDecimalPlaces = new System.Windows.Forms.DomainUpDown();
            this.labelDecimalPlaces = new System.Windows.Forms.Label();
            this.textBoxNullValue = new System.Windows.Forms.TextBox();
            this.labelNullValue = new System.Windows.Forms.Label();
            this.groupBoxSample = new System.Windows.Forms.GroupBox();
            this.labelSample = new System.Windows.Forms.Label();
            this.listBoxType = new System.Windows.Forms.ListBox();
            this.labelType = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelCustom = new System.Windows.Forms.Label();
            this.textBoxCustom = new System.Windows.Forms.TextBox();
            this.listBoxDateType = new System.Windows.Forms.ListBox();
            this.labelDateType = new System.Windows.Forms.Label();
            this.groupBoxContainer.SuspendLayout();
            this.groupBoxSample.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxContainer
            // 
            this.groupBoxContainer.Controls.Add(this.domainDecimalPlaces);
            this.groupBoxContainer.Controls.Add(this.labelDecimalPlaces);
            this.groupBoxContainer.Controls.Add(this.textBoxNullValue);
            this.groupBoxContainer.Controls.Add(this.labelNullValue);
            this.groupBoxContainer.Controls.Add(this.groupBoxSample);
            this.groupBoxContainer.Controls.Add(this.listBoxType);
            this.groupBoxContainer.Controls.Add(this.labelType);
            this.groupBoxContainer.Controls.Add(this.labelDescription);
            this.groupBoxContainer.Controls.Add(this.labelCustom);
            this.groupBoxContainer.Controls.Add(this.textBoxCustom);
            this.groupBoxContainer.Controls.Add(this.listBoxDateType);
            this.groupBoxContainer.Controls.Add(this.labelDateType);
            this.groupBoxContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxContainer.Location = new System.Drawing.Point(0, 0);
            this.groupBoxContainer.Name = "groupBoxContainer";
            this.groupBoxContainer.Size = new System.Drawing.Size(390, 256);
            this.groupBoxContainer.TabIndex = 1;
            this.groupBoxContainer.TabStop = false;
            this.groupBoxContainer.Text = "Format";
            // 
            // domainDecimalPlaces
            // 
            this.domainDecimalPlaces.Items.Add("6");
            this.domainDecimalPlaces.Items.Add("5");
            this.domainDecimalPlaces.Items.Add("4");
            this.domainDecimalPlaces.Items.Add("3");
            this.domainDecimalPlaces.Items.Add("2");
            this.domainDecimalPlaces.Items.Add("1");
            this.domainDecimalPlaces.Items.Add("0");
            this.domainDecimalPlaces.Location = new System.Drawing.Point(260, 133);
            this.domainDecimalPlaces.Name = "domainDecimalPlaces";
            this.domainDecimalPlaces.Size = new System.Drawing.Size(122, 21);
            this.domainDecimalPlaces.TabIndex = 8;
            this.domainDecimalPlaces.Visible = false;
            this.domainDecimalPlaces.SelectedItemChanged += new System.EventHandler(this.domainDecimalPlaces_SelectedItemChanged);
            // 
            // labelDecimalPlaces
            // 
            this.labelDecimalPlaces.AutoSize = true;
            this.labelDecimalPlaces.Location = new System.Drawing.Point(124, 136);
            this.labelDecimalPlaces.Name = "labelDecimalPlaces";
            this.labelDecimalPlaces.Size = new System.Drawing.Size(80, 13);
            this.labelDecimalPlaces.TabIndex = 6;
            this.labelDecimalPlaces.Text = "Decimal places:";
            this.labelDecimalPlaces.Visible = false;
            // 
            // textBoxNullValue
            // 
            this.textBoxNullValue.Location = new System.Drawing.Point(260, 106);
            this.textBoxNullValue.Name = "textBoxNullValue";
            this.textBoxNullValue.Size = new System.Drawing.Size(122, 21);
            this.textBoxNullValue.TabIndex = 5;
            this.textBoxNullValue.Enter += new System.EventHandler(this.textBoxNullValue_Enter);
            this.textBoxNullValue.Validated += new System.EventHandler(this.textBoxNullValue_Validated);
            // 
            // labelNullValue
            // 
            this.labelNullValue.AutoSize = true;
            this.labelNullValue.Location = new System.Drawing.Point(124, 109);
            this.labelNullValue.Name = "labelNullValue";
            this.labelNullValue.Size = new System.Drawing.Size(57, 13);
            this.labelNullValue.TabIndex = 4;
            this.labelNullValue.Text = "Null value:";
            // 
            // groupBoxSample
            // 
            this.groupBoxSample.Controls.Add(this.labelSample);
            this.groupBoxSample.Location = new System.Drawing.Point(120, 60);
            this.groupBoxSample.Name = "groupBoxSample";
            this.groupBoxSample.Size = new System.Drawing.Size(262, 36);
            this.groupBoxSample.TabIndex = 3;
            this.groupBoxSample.TabStop = false;
            this.groupBoxSample.Text = "Sample";
            // 
            // labelSample
            // 
            this.labelSample.AutoSize = true;
            this.labelSample.Location = new System.Drawing.Point(3, 16);
            this.labelSample.Name = "labelSample";
            this.labelSample.Size = new System.Drawing.Size(59, 13);
            this.labelSample.TabIndex = 0;
            this.labelSample.Text = "1234.5678";
            // 
            // listBoxType
            // 
            this.listBoxType.FormattingEnabled = true;
            this.listBoxType.IntegralHeight = false;
            this.listBoxType.Items.AddRange(new object[] {
            "None",
            "Numeric",
            "Currency",
            "DateTime",
            "Scientific",
            "Custom"});
            this.listBoxType.Location = new System.Drawing.Point(7, 76);
            this.listBoxType.Name = "listBoxType";
            this.listBoxType.Size = new System.Drawing.Size(107, 175);
            this.listBoxType.TabIndex = 2;
            this.listBoxType.SelectedIndexChanged += new System.EventHandler(this.listBoxType_SelectedIndexChanged);
            this.listBoxType.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.listBoxType_Format);
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(6, 60);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(70, 13);
            this.labelType.TabIndex = 1;
            this.labelType.Text = "Format type:";
            // 
            // labelDescription
            // 
            this.labelDescription.Location = new System.Drawing.Point(6, 16);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(376, 31);
            this.labelDescription.TabIndex = 0;
            this.labelDescription.Text = "labelDescription";
            // 
            // labelCustom
            // 
            this.labelCustom.AutoSize = true;
            this.labelCustom.Location = new System.Drawing.Point(124, 136);
            this.labelCustom.Name = "labelCustom";
            this.labelCustom.Size = new System.Drawing.Size(82, 13);
            this.labelCustom.TabIndex = 9;
            this.labelCustom.Text = "Custom format:";
            this.labelCustom.Visible = false;
            // 
            // textBoxCustom
            // 
            this.textBoxCustom.Location = new System.Drawing.Point(260, 133);
            this.textBoxCustom.Name = "textBoxCustom";
            this.textBoxCustom.Size = new System.Drawing.Size(122, 21);
            this.textBoxCustom.TabIndex = 10;
            this.textBoxCustom.Visible = false;
            this.textBoxCustom.TextChanged += new System.EventHandler(this.textBoxCustom_TextChanged);
            // 
            // listBoxDateType
            // 
            this.listBoxDateType.FormattingEnabled = true;
            this.listBoxDateType.IntegralHeight = false;
            this.listBoxDateType.Items.AddRange(new object[] {
            "d",
            "D",
            "f",
            "F",
            "g",
            "G",
            "t",
            "T",
            "M"});
            this.listBoxDateType.Location = new System.Drawing.Point(127, 152);
            this.listBoxDateType.Name = "listBoxDateType";
            this.listBoxDateType.Size = new System.Drawing.Size(255, 99);
            this.listBoxDateType.TabIndex = 12;
            this.listBoxDateType.Visible = false;
            this.listBoxDateType.SelectedIndexChanged += new System.EventHandler(this.listBoxDateType_SelectedIndexChanged);
            this.listBoxDateType.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.listBoxDateType_Format);
            // 
            // labelDateType
            // 
            this.labelDateType.AutoSize = true;
            this.labelDateType.Location = new System.Drawing.Point(124, 136);
            this.labelDateType.Name = "labelDateType";
            this.labelDateType.Size = new System.Drawing.Size(35, 13);
            this.labelDateType.TabIndex = 11;
            this.labelDateType.Text = "Type:";
            this.labelDateType.Visible = false;
            // 
            // C1FormatStringPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxContainer);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(390, 256);
            this.Name = "C1FormatStringPicker";
            this.Size = new System.Drawing.Size(390, 256);
            this.Load += new System.EventHandler(this.FormatStringPicker_Load);
            this.groupBoxContainer.ResumeLayout(false);
            this.groupBoxContainer.PerformLayout();
            this.groupBoxSample.ResumeLayout(false);
            this.groupBoxSample.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxContainer;
        private System.Windows.Forms.DomainUpDown domainDecimalPlaces;
        private System.Windows.Forms.Label labelDecimalPlaces;
        private System.Windows.Forms.TextBox textBoxNullValue;
        private System.Windows.Forms.Label labelNullValue;
        private System.Windows.Forms.GroupBox groupBoxSample;
        private System.Windows.Forms.Label labelSample;
        private System.Windows.Forms.ListBox listBoxType;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelCustom;
        private System.Windows.Forms.TextBox textBoxCustom;
        private System.Windows.Forms.Label labelDateType;
        private System.Windows.Forms.ListBox listBoxDateType;
    }
}
