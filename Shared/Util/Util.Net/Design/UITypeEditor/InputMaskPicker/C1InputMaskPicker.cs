using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using C1.Util.Localization;

namespace C1.Design
{
    internal partial class C1InputMaskPicker : UserControl
    {
        Hashtable _mask = new Hashtable();
        ListViewItem _custom = null;
        private int _nonUserUpdating = 0;

        public C1InputMaskPicker()
        {
            InitializeComponent();
            LoadListView();

            C1.Util.Localization.C1Localizer.LocalizeForm(this, components);

            //post localize for mask description & data format captions
            this.listView1.Columns[0].Text = C1Localizer.GetString(this.listView1.Columns[0].Text);
            this.listView1.Columns[1].Text = C1Localizer.GetString(this.listView1.Columns[1].Text);
        }

        #region <<<<<< private methods and implementation
        // adds a row to the list view
        private void AddListViewRow(string desc, string sample, string format)
        {
            ListViewItem item = this.listView1.Items.Add(format, desc, 0);
            item.SubItems.Add(sample);
            item.Tag = format;
            if (desc == C1Localizer.GetString("<Custom>"))
                _custom = item;
        }
        private void LoadListView()
        {
            AddListViewRow(C1Localizer.GetString("Zip Code"), "98052-6399", "00000-9999");
            AddListViewRow(C1Localizer.GetString("Time (US)"), "11:20", "90:00");
            AddListViewRow(C1Localizer.GetString("Time (European/Military)"), "23:20", "00:00");
            AddListViewRow(C1Localizer.GetString("Social Security number"), "000-000-1234", "000-00-0000");
            AddListViewRow(C1Localizer.GetString("Short date and time (US)"), "12/11/2003 11:20", "00/00/0000 90:00");
            AddListViewRow(C1Localizer.GetString("Short date"), "12/11/2003", "00/00/0000");
            AddListViewRow(C1Localizer.GetString("Phone number no area code"), "555-0123", "000-0000");
            AddListViewRow(C1Localizer.GetString("Phone number"), "(574) 555-0123", "(999) 000-0000");
            AddListViewRow(C1Localizer.GetString("Numeric (5-digits)"), "1234", "00000");
            AddListViewRow(C1Localizer.GetString("<Custom>"), "", "");
        }

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected && e.Item != _custom)
            {
                this.textBox1.Text = this.maskedTextBox1.Mask = (string)e.Item.Tag;
                this.textBox1.Modified = false;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (this.textBox1.Modified)
                _custom.Selected = true;
            if (!InNonUserUpdate)
            {
                Binding bind = DataBindings["Mask"];
                if (bind != null)
                    bind.WriteValue();
            }

            OnMaskChanged();
            if (!InNonUserUpdate)
                OnUserPropertyChanged();
        }


        private void SyncListView(string mask)
        {
            string s = mask;
            ListViewItem item = _custom;
            ListViewItem[] items = this.listView1.Items.Find(mask, false);
            if (items.Length > 0)
                item = items[0];

            this.textBox1.Text = this.maskedTextBox1.Mask = mask;
            this.textBox1.Modified = false;
            if (Mask.Length == 0)
            {
                this.listView1.SelectedItems.Clear();
            }
            else if (item != null)
                item.Selected = true;
        }

        internal delegate void MaskedChangedEventHandler(object sender, EventArgs e);
        internal event MaskedChangedEventHandler MaskedChanged;
        internal void OnMaskChanged()
        {
            if (MaskedChanged != null)
                MaskedChanged(this, EventArgs.Empty);
        }
        internal event MaskedChangedEventHandler MaskedUpdateChanged;
        internal void OnMaskUpdateChanged()
        {
            if (MaskedUpdateChanged != null)
                MaskedUpdateChanged(this, EventArgs.Empty);
        }

        private void BeginNonUserUpdate()
        {
            _nonUserUpdating++;
        }
        private void EndNonUserUpdate()
        {
            if (_nonUserUpdating > 0)
                _nonUserUpdating--;
        }
        private bool InNonUserUpdate
        {
            get { return _nonUserUpdating > 0; }
        }


        private void OnUserPropertyChanged()
        {
            if (UserPropertyChanged != null)
                UserPropertyChanged(this, EventArgs.Empty);
        }
        #endregion

        #region <<<<<<<< Properties
		[DefaultValue("")]
		[Bindable(BindableSupport.Yes)]
        public string Mask
        {
            get { return this.textBox1.Text; }
            set 
            {
                BeginNonUserUpdate();
                try
                {
                    SyncListView(value);
                }
                finally
                {
                    EndNonUserUpdate();
                }
            } // this.textBox1.Text = value; }
        }

        public bool UpdateWithLiterals
        {
            get { return this.checkBox1.Checked; }
            set { this.checkBox1.Checked = value; }
        }

        public bool UpdateWithLiteralsPromptVisible
        {
            get { return this.checkBox1.Visible; }
            set { this.checkBox1.Visible = value; }
        }
        
        public event EventHandler UserPropertyChanged;

        #endregion

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            OnMaskUpdateChanged();
        }
    }
}
