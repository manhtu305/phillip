using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace C1.Design
{
    internal partial class C1InputMaskPickerForm : Form
    {
        public C1InputMaskPickerForm()
        {
            InitializeComponent(); 
            C1.Util.Localization.C1Localizer.LocalizeForm(this, components);
        }

        /// <summary>
        /// Gets or sets the mask
        /// </summary>
        public string EditMask
        {
            get { return this.c1InputMaskPicker1.Mask; }
            set { this.c1InputMaskPicker1.Mask = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the underlying datasource should be updated with the literals in the mask.
        /// </summary>
        public bool UpdateWithLiterals
        {
            get { return this.c1InputMaskPicker1.UpdateWithLiterals; }
            set { this.c1InputMaskPicker1.UpdateWithLiterals = value; }
        }
        public bool UpdateWithLiteralsPromptVisible
        {
            get { return this.c1InputMaskPicker1.UpdateWithLiteralsPromptVisible; }
            set { this.c1InputMaskPicker1.UpdateWithLiteralsPromptVisible = value; }
        }
    }
}