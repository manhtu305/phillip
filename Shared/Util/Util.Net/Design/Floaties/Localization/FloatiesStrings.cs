#if RESX_FLOATIES_LOCALIZATION
#pragma warning disable 1591

using System;
using System.Resources;
using System.Globalization;
using System.Reflection;

using C1.Win.Localization;

namespace C1.Util.Design.Floaties
{
    /// <summary>
    /// Contains localizable design-time strings.
    /// </summary>
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class FloatiesStrings
    {
        public static string OpenMainMenu
        {
            get
            {
                return StringsManager.GetString(MethodBase.GetCurrentMethod(),
                    "Open the main menu");
            }
        }

        public static string CloseMainMenu
        {
            get
            {
                return StringsManager.GetString(MethodBase.GetCurrentMethod(),
                    "Close the main menu");
            }
        }
    }
}
#endif
