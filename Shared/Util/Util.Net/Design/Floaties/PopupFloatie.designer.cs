namespace C1.Util.Design.Floaties
{
    partial class PopupFloatie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_toolStrip = new C1.Util.Design.Floaties.FloatieToolStrip();
            this.m_help = new System.Windows.Forms.Label();
            this.m_toolStripPanel = new System.Windows.Forms.Panel();
            this.m_toolStripPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_toolStrip
            // 
            this.m_toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.m_toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.m_toolStrip.Location = new System.Drawing.Point(14, 0);
            this.m_toolStrip.Name = "m_toolStrip";
            this.m_toolStrip.Padding = new System.Windows.Forms.Padding(2);
            this.m_toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.m_toolStrip.TabIndex = 0;
            this.m_toolStrip.Text = "m_toolStrip";
            // 
            // m_help
            // 
            this.m_help.AutoSize = true;
            this.m_help.BackColor = System.Drawing.SystemColors.Info;
            this.m_help.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_help.ForeColor = System.Drawing.SystemColors.InfoText;
            this.m_help.Location = new System.Drawing.Point(131, 49);
            this.m_help.Name = "m_help";
            this.m_help.Padding = new System.Windows.Forms.Padding(2);
            this.m_help.TabIndex = 1;
            this.m_help.Visible = false;
            this.m_help.Margin = new System.Windows.Forms.Padding(0, 4, 0, 0);
            // 
            // m_toolStripPanel
            // 
            this.m_toolStripPanel.BackColor = System.Drawing.SystemColors.Control;
            this.m_toolStripPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_toolStripPanel.Controls.Add(this.m_toolStrip);
            this.m_toolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.m_toolStripPanel.Margin = new System.Windows.Forms.Padding(5);
            this.m_toolStripPanel.Name = "m_toolStripPanel";
            this.m_toolStripPanel.Size = new System.Drawing.Size(64, 61);
            this.m_toolStripPanel.TabIndex = 2;
            // 
            // PopupFloatie
            // 
            this.MinimumSize = new System.Drawing.Size(1, 1);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Magenta;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(1, 1);
            this.ControlBox = false;
            this.Controls.Add(this.m_toolStripPanel);
            this.Controls.Add(this.m_help);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PopupFloatie";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TransparencyKey = System.Drawing.Color.Magenta;
            this.m_toolStripPanel.ResumeLayout(false);
            this.m_toolStripPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected FloatieToolStrip m_toolStrip;
        protected System.Windows.Forms.Label m_help;
        protected System.Windows.Forms.Panel m_toolStripPanel;
    }
}