using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms.Design.Behavior;
using System.Reflection;
using System.Diagnostics;

using C1.Util.Win;

namespace C1.Util.Design.Floaties
{
    internal interface IFloatieOwner
    {
        Control Control { get; }
        bool ControlSelected { get; }
        Form OwnerForm { get; }
        IDesignerEventService DesignerEventService { get; }
        //BehaviorService BehaviorService { get; }
        bool InsideOwner { get; }
        Point Position { get; }
        double Opacity { get; }
        void OnFloatieShowing();
        void OnFloatieHidden();
    }

    /// <summary>
    /// Additional stuff required (so far - Aug 2006) for C1Command only.
    /// </summary>
    internal interface IFloatieOwnerExt
    {
        /// <summary>
        /// Gets the control that is to be used instead of the real parent
        /// when checking that the parent is in focus etc.
        /// Used for popup menus.
        /// </summary>
        Control LogicalParent { get; }
        /// <summary>
        /// Gets the designer component if it is distinct from what
        /// is returned by Control property.
        /// </summary>
        IComponent DesignedComponent { get; }
    }

    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class FloatieBase : Form, IShadowOwner
    {
        private IFloatieOwner _owner = null;
        private IFloatieOwnerExt _ownerExt = null;
        private IDesignerEventService _des = null;
        private bool _inOwnerDropDown = false;
        private static Timer s_timer = null;
        private static List<FloatieBase> s_floaties = new List<FloatieBase>();
        private static FloatieBase s_trackingFloatie = null;

        private const int TimerInterval = 100;

        static FloatieBase()
        {
            s_timer = new Timer();
            s_timer.Interval = TimerInterval;
            s_timer.Tick += new EventHandler(s_timer_Tick);
        }

#if DEBUG
        public FloatieBase()
        {
            // this ctor is a convenience for design of the floatie only;
            // NOT FOR PRODUCTION USE!
        }
#endif

        public FloatieBase(IFloatieOwner owner)
        {
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.UserPaint, true);

            Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            _owner = owner;
            Debug.Assert(_owner != null);
            if (_owner == null)
                return;

            _ownerExt = _owner as IFloatieOwnerExt;

            _des = _owner.DesignerEventService;
            if (_des != null)
                _des.ActiveDesignerChanged += new ActiveDesignerEventHandler(_des_ActiveDesignerChanged);

            s_floaties.Add(this);
            if (s_floaties.Count == 1)
                s_timer.Start();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (s_floaties.Count == 1)
                    s_timer.Stop();
                if (s_trackingFloatie == this)
                    s_trackingFloatie = null;
                s_floaties.Remove(this);
                if (_des != null)
                {
                    _des.ActiveDesignerChanged -= new ActiveDesignerEventHandler(_des_ActiveDesignerChanged);
                    _des = null;
                }
                if (ToolStrip != null)
                    ToolStrip.Items.Clear();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This method MUST be called by the inheritor's ctor before exiting!
        /// </summary>
        protected void FloatieConstructed()
        {
            Debug.Assert(ToolStrip != null);
            if (ToolStrip == null)
                return;

            foreach (ToolStripItem item in ToolStrip.Items)
                _itemAdded(item);
            ToolStrip.ItemAdded += new ToolStripItemEventHandler(ToolStrip_ItemAdded);
            ToolStrip.ItemRemoved += new ToolStripItemEventHandler(ToolStrip_ItemRemoved);
        }

        private void ToolStrip_ItemAdded(object sender, ToolStripItemEventArgs e)
        {
            _itemAdded(e.Item);
        }

        private void ToolStrip_ItemRemoved(object sender, ToolStripItemEventArgs e)
        {
            _itemRemoved(e.Item);
        }

        private void _itemAdded(ToolStripItem item)
        {
            if (item is ToolStripDropDownItem)
                ((ToolStripDropDownItem)item).DropDownClosed += new EventHandler(_dropDownClosed);
        }

        private void _itemRemoved(ToolStripItem item)
        {
            if (item is ToolStripDropDownItem)
                ((ToolStripDropDownItem)item).DropDownClosed -= new EventHandler(_dropDownClosed);
        }

        // The reason for this is a bug in MS' dropdown (toolstrip) menu code:
        // if it is closed programmatically, there's something left over.
        // To repro the issue, comment out this code and:
        // - open a form with C1PrintPreviewControl
        // - open the main floatie
        // - click on icons menu (1st one)
        // - move the mouse to next main floatie item to hide the menu
        // - move the mouse back to icons and click to open the menu again
        // - move the mouse to next main floatie item to hide the menu
        // - click on the form (outside of preview control)
        // - change to a text window in Visual Studio
        // - booom. Text cannot be edited.
        private void _dropDownClosed(object sender, EventArgs e)
        {
            // NOTE/TODO? - DOES NOT WORK FOR MENUS!!!
            if (sender is ToolStripDropDownItem)
                ((ToolStripDropDownItem)sender).DropDown.Close(ToolStripDropDownCloseReason.ItemClicked);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams result = base.CreateParams;
                if (_owner != null)
                {
                    result.Style |= Win32.WS.WS_POPUP;
                }
                // the following hides the window from alt-tab list
                result.ExStyle |= Win32.WS_EX.WS_EX_TOOLWINDOW;

                return result;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (_owner != null && _owner.OwnerForm != null)
                Owner = _owner.OwnerForm;
        }

        public IFloatieOwner FloatieOwner
        {
            get { return _owner; }
        }

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        public virtual FloatieToolStrip ToolStrip
        {
            get { return null; }
        }

        protected virtual bool IsMain
        {
            get { return false; }
        }

        protected virtual bool HasShadow
        {
            get { return true; }
        }

        public bool InOwnerDropDown
        {
            get { return _inOwnerDropDown; }
            set { _inOwnerDropDown = value; }
        }

        private bool _isTracking()
        {
            if (_owner == null || ToolStrip == null || !Visible)
                return false;
            if (InOwnerDropDown)
                return true;
            if (Bounds.Contains(Cursor.Position))
                return true;
            foreach (ToolStripItem item in ToolStrip.Items)
                if (item.Selected ||
                    (item is ToolStripDropDownItem && ((ToolStripDropDownItem)item).DropDown.Visible))
                    return true;
            return false;
        }

        public bool IsTracking
        {
            get
            {
                bool ret = _isTracking();
                if (ret)
                    s_trackingFloatie = this;
                else if (s_trackingFloatie == this)
                    s_trackingFloatie = null;
                return ret;
            }
        }

        protected static FloatieBase TrackingFloatie
        {
            get { return s_trackingFloatie; }
        }


        Region IShadowOwner.GetDisposableRegion()
        {
            return null;
        }

        Rectangle[] IShadowOwner.GetRectangles()
        {
            return this.GetRectangles();
        }

        protected virtual Rectangle[] GetRectangles()
        {
            return new Rectangle[] { this.ToolStrip.Bounds };
        }

        protected IntPtr GetRootWindow()
        {
            try
            {
                if (_owner == null || _owner.Control == null)
                    return IntPtr.Zero;
                if (_owner.OwnerForm != null && _owner.OwnerForm.IsHandleCreated)
                    return _owner.OwnerForm.Handle;
                if (_owner.Control.IsHandleCreated)
                    return Win32.GetAncestor(_owner.Control.Handle, Win32.GA.GA_ROOTOWNER);
                return IntPtr.Zero;
            }
            catch
            {
                return IntPtr.Zero;
            }
        }

        private bool IsFloatiePositionVisible(Point p)
        {
            Control c = _owner.Control.Parent;
            while (c != null)
            {
                string s = c.GetType().FullName;
                // O.Vlasova: System.Windows.Forms.Design.DesignerFrame is an internal
                // control, and contains OverlayControl as designer region. It is a parent of
                // designed form or usercontrol.
                // it's bounds is the same as BehaviorService.AdornerWindowGraphics.VisibleClipBounds,
                // but it is more then we need. We need only it's client area.
                if (s.Contains("DesignerFrame+OverlayControl"))
                {
                    return c.ClientRectangle.Contains(c.PointToClient(p));
                }
                c = c.Parent;
            }
            return true;
        }

#if skip_olga
        private bool IsScreenPointVisible(Point p)
        {
            try
            {
                if (_owner == null || _owner.Control == null)
                    return false;

                // we must skip the designed control itself
                for (IntPtr h = Win32.GetAncestor(_owner.Control.Handle, Win32.GA.GA_PARENT);
                    h != IntPtr.Zero;
                    h = Win32.GetAncestor(h, Win32.GA.GA_PARENT))
                {
                    Win32.RECT rectWnd = new Win32.RECT();//(0, 0, 0, 0);
                    if (!Win32.GetWindowRect(h, ref rectWnd))
                        break;
                    Rectangle r = Rectangle.Empty;
                    rectWnd.AssignTo(ref r);
                    if (!r.Contains(p))
                    {
                        // for desktop window, GetWindowRect returns the rect of the primary
                        // monitor, hence this (otherwise, we'll always be outside the root
                        // window on secondary display):
                        return (h == Win32.GetDesktopWindow());
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
#endif
         ///<summary>
         ///Returns false if the floatie should not be shown for reasons such as:
         ///- the designer is hidden;
         ///- ???
         ///Returning true does not mean that the floatie must show, only that it can.
         ///</summary>
         ///<returns></returns>
        public bool CanShow()
        {
            try
            {
                if (!Util.InActiveApplication)
                    return false;
                if (_owner == null || _owner.Control == null || !_owner.Control.IsHandleCreated)
                    return false;
                // check that active designer exists but only if des service is available
                if (_des != null && _des.ActiveDesigner == null)
                    return false;
                if (IsTracking)
                    return true;
                // check that our control or its parent has focus
                IntPtr activeWnd = Win32.GetFocus();
                Control ctrl = Control.FromHandle(activeWnd);
                if (!(ctrl is FloatieBase))
                {
                    IntPtr ownerWnd = _owner.Control.Handle;
                    bool found = false;
                    for (IntPtr prn = ownerWnd; prn != IntPtr.Zero; prn = Win32.GetParent(prn))
                        if (prn == activeWnd)
                        {
                            found = true;
                            break;
                        }
                    // test foster parent if available
                    if (!found && _ownerExt != null && _ownerExt.LogicalParent != null)
                    {
                        for (IntPtr prn = _ownerExt.LogicalParent.Handle; prn != IntPtr.Zero; prn = Win32.GetParent(prn))
                            if (prn == activeWnd)
                            {
                                found = true;
                                break;
                            }
                    }
                    if (!found)
                        return false;
                }
                if (!IsMain)
                {
                    foreach (FloatieBase fl in s_floaties)
                        if (fl != this && fl.IsTracking)
                            return false;
                }
#if skip_dima // dima001
                if (!IsScreenPointVisible(_owner.Position))
                    return false;
#else
                Point p1 = _owner.Position;  // desired position for main or popup floatie toolbar
                if (!IsFloatiePositionVisible(p1))
                    return false;
#if skip_olga
                p1.Offset(Width / 2, Height / 2);  // the center of floatie toolbar
                if (!IsScreenPointVisible(p1))
                    return false;
#endif
#endif           
                bool ok = false;
                if (_des != null)
                {
                    object designerComponent;
                    if (_ownerExt != null && _ownerExt.DesignedComponent != null)
                        designerComponent = _ownerExt.DesignedComponent;
                    else
                        designerComponent = _owner.Control;
                    foreach (object o in _des.ActiveDesigner.Container.Components)
                        if (o == designerComponent)
                        {
                            ok = true;
                            break;
                        }
                    if (!ok)
                        return false;
                }
                // if floatie is visible, can show while the mouse is inside floatie
                ok = (Visible && Bounds.Contains(Cursor.Position)) || IsTracking;
                // otherwise, test whether the mouse is inside the control
                if (!ok)
                    ok = _owner.ControlSelected &&
                        _owner.Control.ClientRectangle.Contains(_owner.Control.PointToClient(Cursor.Position));
                return ok;
            }
            catch (Exception e)
            {
                Debug.Print("CanShow: Exception: {0}", e);
                return false;
            }
        }

        public ToolStripItemCollection Items
        {
            get
            {
                if (ToolStrip != null)
                    return ToolStrip.Items;
                return null;
            }
        }

        private void _des_ActiveDesignerChanged(object sender, ActiveDesignerEventArgs e)
        {
            try
            {
                if (!CanShow())
                  Hide();
            }
            catch
            {
            }
        }

        private static void s_timer_Tick(object sender, EventArgs e)
        {
            try
            {
                foreach (FloatieBase floatie in s_floaties)
                    floatie.OnTimerTick();
            }
            // eat exceptions (one possible "valid" source is when a floatie
            // is removed by a designer, and "collection is modified in a foreach loop"
            // occurs.
            catch
            {
            }
        }

        protected virtual void OnTimerTick()
        {
        }

        protected virtual void OnShowing()
        {
            if (_owner != null)
            {
                Location = _owner.Position;
                // todo: adjust position if at screen edge
                Opacity = _owner.Opacity;
                _owner.OnFloatieShowing();
            }
        }

        protected virtual void OnHidden()
        {
#if todo_dima // should not be needed due to same code in _dropDownClosed, remove eventually
            // NOTE: In this scenario:
            // - we have a floatie with a menu (ToolStripMenuItem)
            // - we open (drop down) the menu
            // - we move the mouse to another floatie top level item, so that the menu hides
            // - we close the floatie by moving the mouse away from it/control
            // - we switch to another, text editor, VS window
            // boom. Text editor does not accept keystrokes. The culprit is closing
            // the dropdown - it remains in a "shown" state. Hence the code below.
            // Also note that not all ToolStripDropDownCloseReason's work...
            try
            {
                if (ToolStrip != null)
                {
                    foreach (ToolStripItem item in ToolStrip.Items)
                        if (item is ToolStripDropDownItem)
                            ((ToolStripDropDownItem)item).DropDown.Close(ToolStripDropDownCloseReason.ItemClicked);
                }
            }
            catch
            {
            }
#endif
            if (_owner != null)
                _owner.OnFloatieHidden();
        }

        protected void DrawShadow()
        {
            if (Visible && HasShadow)
                Shadow.DrawShadow(this);
        }

        protected void HideShadow()
        {
            Shadow.HideShadow(this);
        }

        protected override void SetVisibleCore(bool value)
        {
            if (value)
                OnShowing();
            base.SetVisibleCore(value);
            if (!value)
                OnHidden();

            if (value && Owner == null)
                Win32.SetWindowPos(Handle, Win32.SWP_HWND.HWND_TOPMOST, 0, 0, 0, 0,
                    Win32.SWP.SWP_NOMOVE | Win32.SWP.SWP_NOSIZE | Win32.SWP.SWP_NOACTIVATE);
        }

        protected override void WndProc(ref Message m)
        {
            if (_owner == null)
            {
                base.WndProc(ref m);
                return;
            }
            switch (m.Msg)
            {
                case Win32.WM.WM_PAINT:
                    // dunno why but OnPaint was not called for main floaties, hence
                    // draw the shadow here
                    DrawShadow();
                    base.WndProc(ref m);
                    break;
                case Win32.WM.WM_ACTIVATEAPP:
                    base.WndProc(ref m);
                    // Note: this is not absolutely needed as InActiveApp will be eventually
                    // called by the timer proc and hide us. But this call hides us right away:
                    if ((int)m.WParam == 0)
                        Hide();
                    break;
                case Win32.WM.WM_ACTIVATE:
                    if ((((uint)m.WParam) & 0x1111) != 0) // != WA_INACTIVE
                    {
                        // our window is being activated - make sure our master does not
                        // lose "active" border
                        IntPtr root = GetRootWindow();
                        if (root != IntPtr.Zero)
                            Win32.SendMessage(root, Win32.WM.WM_NCACTIVATE, (IntPtr)1, IntPtr.Zero);
                    }
                    base.WndProc(ref m);
                    break;
                case Win32.WM.WM_MOUSEACTIVATE:
                    m.Result = (IntPtr)Win32.MA.MA_NOACTIVATE;
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
    }
}
