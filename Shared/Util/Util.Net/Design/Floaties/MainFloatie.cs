using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;
using System.IO;

using C1.Util.Localization;

namespace C1.Util.Design.Floaties
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class MainFloatie : PopupFloatie
    {
        #region Floatie open/close image data

        private static string s_dataMainMenuClose =
            "iVBORw0KGgoAAAANSUhEUgAAAAkAAAAQCAYAAADESFVDAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8" +
            "YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAAALOgAA" +
            "CzoBZH9XDQAAAE5JREFUKFNj/P//PwNBAFJECBNUALaJkCnkKQJ6AKgR4UYYH24dSABZETIfrAgmANcJ" +
            "1YBiElGKkE2DuQnDOmQJvA7HF140CkxcVhJlHQDEO2apEwH54AAAAABJRU5ErkJggg==";

        private static string s_dataMainMenuOpen =
            "iVBORw0KGgoAAAANSUhEUgAAAAkAAAAICAYAAAArzdW1AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8" +
            "YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAAALOgAA" +
            "CzoBZH9XDQAAADdJREFUKFNj/P//PwNBAFUEpP4zwDBQEyofpghZAsRG4SMrgknAFMH5JCtCdhNW6wg6" +
            "HFkBNjYA40aPcjgSSywAAAAASUVORK5CYII=";

        #endregion

        #region Private fields

        private bool _isOpen = false;
        private ToolStripButton _openButton = null;
        private ToolStripButton _closeButton = null;

        // image transparent color
        private static Color s_transColor = Color.Magenta;
        // direct images
        private static Bitmap s_mainMenuOpenImage = null;
        private static Bitmap s_mainMenuCloseImage = null;
        // inverted images - to use when back color is black
        private static Bitmap s_mainMenuOpenImageInv = null;
        private static Bitmap s_mainMenuCloseImageInv = null;

        // note: looks like there's a bug in treatment of padding - 1 pixels is added at the top
        //private static Padding s_closedPadding = new Padding(1, 0, 1, 2);
        //private static Padding s_openPadding = new Padding(2, 1, 2, 2);
        private static Padding s_closedPadding = new Padding(1, 0, 0, 0);
        private static Padding s_openPadding = new Padding(2, 1, 2, 1);

        private const int s_openButtonDX = 2;   // added to image on each side
        private const int s_openButtonDY = 2;
        private const int s_closeButtonDX = 2;

        #endregion

        #region Construction

        static MainFloatie()
        {
            MemoryStream strm = new MemoryStream(Convert.FromBase64String(s_dataMainMenuClose));
            s_mainMenuCloseImage = new Bitmap(strm);
            s_mainMenuCloseImageInv = s_mainMenuCloseImage.Clone() as Bitmap;
            PrepImages(s_mainMenuCloseImage, s_mainMenuCloseImageInv);

            strm = new MemoryStream(Convert.FromBase64String(s_dataMainMenuOpen));
            s_mainMenuOpenImage = new Bitmap(strm);
            s_mainMenuOpenImageInv = s_mainMenuOpenImage.Clone() as Bitmap;
            PrepImages(s_mainMenuOpenImage, s_mainMenuOpenImageInv);
        }

#if DEBUG
        public MainFloatie()
        {
            // this ctor is only a convenience for design of main menu itself.
        }
#endif

        public MainFloatie(IFloatieOwner owner)
            : base(owner, typeof(MainFloatie))
        {
            InitializeComponent();
            this.m_toolStrip.SuspendLayout();
            this.m_help.SuspendLayout();
            this.SuspendLayout();

            m_toolStrip.Items.Clear();

            m_help.Visible = false;

            this.Padding = Padding.Empty;

            _openButton = new ToolStripButton();
            if (IsDarkColor(_openButton.BackColor))
                _openButton.Image = s_mainMenuOpenImageInv;
            else
                _openButton.Image = s_mainMenuOpenImage;
            _openButton.ImageTransparentColor = s_transColor;
#if RESX_FLOATIES_LOCALIZATION
            _openButton.ToolTipText = FloatiesStrings.OpenMainMenu;
#else
            _openButton.ToolTipText = C1Localizer.GetString("Open the main menu");
#endif
            _openButton.AutoSize = false;
            _openButton.ImageScaling = ToolStripItemImageScaling.None;
            _openButton.Width = s_mainMenuOpenImage.Width + s_openButtonDX * 2;
            _openButton.Height = s_mainMenuOpenImage.Height + s_openButtonDY * 2;
            _openButton.Padding = Padding.Empty;
            _openButton.Click += new EventHandler(_openButton_Click);
            m_toolStrip.Items.Add(_openButton);

            _closeButton = new ToolStripButton();
            if (IsDarkColor(_closeButton.BackColor))
                _closeButton.Image = s_mainMenuCloseImageInv;
            else
                _closeButton.Image = s_mainMenuCloseImage;
            _closeButton.ImageTransparentColor = s_transColor;
#if RESX_FLOATIES_LOCALIZATION
            _closeButton.ToolTipText = FloatiesStrings.CloseMainMenu;
#else
            _closeButton.ToolTipText = C1Localizer.GetString("Close the main menu");
#endif
            _closeButton.AutoSize = false;
            _closeButton.ImageScaling = ToolStripItemImageScaling.None;
            _closeButton.Width = s_mainMenuCloseImage.Width + s_closeButtonDX * 2;
            _closeButton.Padding = Padding.Empty;
            _closeButton.Click += new EventHandler(_closeButton_Click);
            m_toolStrip.Items.Add(_closeButton);

            _isOpen = !_isOpen;
            IsOpen = !_isOpen;

            this.m_toolStrip.ResumeLayout(true);
            this.m_help.ResumeLayout(true);
            this.ResumeLayout(true);

            base.FloatieConstructed();
        }

        #endregion

        #region Overrides

        protected override bool IsMain
        {
            get { return true; }
        }

        protected override bool HasShadow
        {
            get { return IsOpen; }
        }

        protected override void OnTimerTick()
        {
            try
            {
                bool canShow = CanShow() && (TrackingFloatie == null || TrackingFloatie == this || !TrackingFloatie.InOwnerDropDown);
                if (canShow != Visible)
                {
                    if (canShow)
                    {
                        Show();
                        DrawShadow();
                    }
                    else
                        Hide();
                }
                HandleHelpOnTimerTick(Visible && ShowHelp && IsOpen);
            }
            catch
            {
            }
        }

        #endregion

        #region open/close button handling

        private void _closeButton_Click(object sender, EventArgs e)
        {
            ShowHelp = false;
            IsOpen = false;
        }

        private void _openButton_Click(object sender, EventArgs e)
        {
            IsOpen = true;
            if (!InOwnerDropDown)
                ShowHelp = true;
        }

        public bool IsOpen
        {
            get { return _isOpen; }
            set
            {
                if (value != _isOpen)
                {
                    _isOpen = value;
                    SuspendLayout();
                    m_toolStripPanel.SuspendLayout();
                    m_toolStrip.SuspendLayout();
                    m_help.SuspendLayout();
                    foreach (ToolStripItem item in m_toolStrip.Items)
                    {
                        if (item == _openButton)
                            item.Visible = !_isOpen;
                        else // if (item.Tag == CloseButtonTag)
                            item.Visible = _isOpen;
                    }
                    if (_isOpen)
                    {
                        m_toolStrip.AutoSize = true;
                        m_toolStrip.Padding = s_openPadding;
                    }
                    else
                    {
                        m_toolStrip.AutoSize = false;
                        m_toolStrip.CanOverflow = false;
                        m_toolStrip.Padding = s_closedPadding;
                        Size s = _openButton != null ? _openButton.Size : Size.Empty;
                        s.Width += s_closedPadding.Horizontal + s_openButtonDX;
                        s.Height += s_closedPadding.Vertical + s_openButtonDY;
                        m_toolStrip.Size = s;
                    }
                    m_toolStrip.ResumeLayout(true);
                    m_toolStripPanel.ResumeLayout(true);
                    m_help.ResumeLayout(true);
                    ResumeLayout(false);
                    PerformLayout();
                    if (_isOpen)
                        DrawShadow();
                    else
                        HideShadow();
                }
            }
        }

        #endregion

        #region open/close button color

        private static bool IsDarkColor(Color c)
        {
            return c.R < 64 && c.G < 64 && c.B < 64;
        }

        private static void PrepImages(Bitmap bmpDirect, Bitmap bmpInverted)
        {
            if (bmpDirect == null || bmpInverted == null)
                return;

            Debug.Assert(bmpDirect.Size == bmpInverted.Size);
            Color trc = bmpDirect.GetPixel(0, 0);
            for (int x = 0; x < bmpDirect.Width; ++x)
            {
                for (int y = 0; y < bmpDirect.Height; ++y)
                {
                    Color c = bmpDirect.GetPixel(x, y);
                    if (c == trc)
                    {
                        bmpDirect.SetPixel(x, y, s_transColor);
                        bmpInverted.SetPixel(x, y, s_transColor);
                    }
                    else
                        bmpInverted.SetPixel(x, y, Color.FromArgb(255 - c.R, 255 - c.G, 255 - c.B));
                }
            }
        }

        #endregion
    }
}
