using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Windows.Forms.Design;
using System.Reflection;
using System.Diagnostics;

using C1.Util;

namespace C1.Util.Design.Floaties
{
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    internal partial class PopupFloatie : FloatieBase
    {
        #region private data
        // holds the moment when the floatie is ready to be shown
        private DateTime _insideOwner = DateTime.MinValue;
        private TimeSpan _autoPopupDelay = TimeSpan.FromMilliseconds(500);

        private bool _showHelp = true;
        private bool _helpVisible;

        private const double MinOpacity = 0.01;
        private const double MinShadowedOpacity = 0.8;
        #endregion

        #region Public props and events
        /// <summary>
        /// Gets or sets the timespan between the moment the floatie is ready to be shown
        /// and the moment it is actually shown.
        /// </summary>
        public TimeSpan AutoPopupDelay
        {
            get { return _autoPopupDelay; }
            set { _autoPopupDelay = value; }
        }

        public event EventHandler HelpVisibleChanged;
        #endregion

        #region ctor
#if DEBUG
        /// <summary>
        /// Debug-only ctor, to allow visual design of floatie.
        /// </summary>
        public PopupFloatie()
        {
        }
#endif

        public PopupFloatie(IFloatieOwner owner)
            : this(owner, null)
        {
            // called only if we are not a base class
            base.FloatieConstructed();
        }

        /// <summary>
        /// ctor for inherited classes
        /// </summary>
        protected PopupFloatie(IFloatieOwner owner, Type inheritor)
            : base(owner)
        {
            InitializeComponent();
            this.m_toolStrip.SuspendLayout();
            this.m_help.SuspendLayout();
            this.SuspendLayout();

            Padding = Padding.Empty; // do not want to deal with form's own padding
            m_toolStrip.Items.Clear();
            m_toolStrip.ShowItemToolTips = false;
            _layout(true);

            this.m_toolStrip.ResumeLayout(true);
            this.m_help.ResumeLayout(true);
            this.ResumeLayout(true);

            m_help.VisibleChanged += new EventHandler(m_help_VisibleChanged);
        }
        #endregion

        #region layout
        private void _layout(bool initial)
        {
            SuspendLayout();
            if (initial)
            {
                m_toolStripPanel.Location = Point.Empty;
                m_toolStrip.Location = Point.Empty;
            }
            m_toolStrip.PerformLayout();
            // note: "-1"s here hide the line under the toolstrip and ugly spacing on the right
            Size s = new Size(m_toolStrip.Width - 1, m_toolStrip.Height - 1);
            if (m_toolStripPanel.ClientSize != s)
                m_toolStripPanel.ClientSize = s;
            Point pt = new Point(m_help.Margin.Left, m_toolStripPanel.Bottom + m_help.Margin.Top);
            if (m_help.Location != pt)
                m_help.Location = pt;
            int maxw, maxh;
            if (ShowingHelp)
            {
                maxw = Math.Max(m_toolStripPanel.Width, m_help.Width + m_help.Margin.Left);
                maxh = m_toolStripPanel.Height + m_help.Height + m_help.Margin.Top;
            }
            else
            {
                maxw = m_toolStripPanel.Width;
                maxh = m_toolStripPanel.Height;
            }
            s = new Size(maxw, maxh);
            if (Size != s)
                Size = s;
            ResumeLayout(false);
            _updateRegion();
        }

        /// <summary>
        /// Updates the region to include just the toolbar and the help label.
        /// This helps avoid flickering due to whole form resizing when help string changes.
        /// </summary>
        private void _updateRegion()
        {
            Region rgn = new Region();
            rgn.MakeEmpty();
            rgn.Union(new Rectangle(m_toolStripPanel.Location, m_toolStripPanel.Size));
            rgn.Union(new Rectangle(m_help.Location, m_help.Size));
            this.Region = rgn;
        }
        #endregion

        #region help handling
        private bool ShowingHelp
        {
            get { return ShowHelp && !string.IsNullOrEmpty(m_help.Text); }
        }

        public bool ShowHelp
        {
            get { return _showHelp; }
            set
            {
                if (value != _showHelp)
                {
                    _showHelp = value;
                    if (!_showHelp)
                        HideHelp();
                    DrawShadow();
                }
            }
        }

        public bool HelpVisible
        {
            get { return _helpVisible; }
        }

        protected void HideHelp()
        {
            m_help.Hide();
            m_help.Text = string.Empty;
        }

        protected void HandleHelpOnTimerTick(bool show)
        {
            if (show)
            {
                Point p = PointToClient(Cursor.Position);
                ToolStripItem item = ToolStrip.GetItemAt(p);
                if (item is ToolStripDropDownItem && ((ToolStripDropDownItem)item).DropDown.Visible)
                    item = null;
                string help = item == null ? Text : item.ToolTipText;
                if (!string.IsNullOrEmpty(help))
                {
                    if (m_help.Text != help)
                    {
                        SuspendLayout();
                        m_help.SuspendLayout();
                        m_help.Text = help;
                        if (!m_help.Visible)
                            m_help.Show();
                        m_help.ResumeLayout(true);
                        ResumeLayout(true);
                        DrawShadow();
                    }
                }
                else
                {
                    bool wasVisible = m_help.Visible;
                    HideHelp();
                    if (wasVisible)
                        DrawShadow();
                }
            }
        }

        private void m_help_VisibleChanged(object sender, EventArgs e)
        {
            _helpVisible = m_help.Visible;
            if (HelpVisibleChanged != null)
                HelpVisibleChanged(this, EventArgs.Empty);
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            if (_helpVisible)
            {
                m_help_VisibleChanged(this, EventArgs.Empty);
            }
        }
        #endregion

        #region overrides
        public override Size GetPreferredSize(Size proposedSize)
        {
            return new Size(1, 1);
        }

        public override FloatieToolStrip ToolStrip
        {
            get { return m_toolStrip; }
        }

        protected override Rectangle[] GetRectangles()
        {
            if (_showHelp && !string.IsNullOrEmpty(m_help.Text))
                return new Rectangle[] { ToolStrip.Bounds, m_help.Bounds };
            else
                return new Rectangle[] { ToolStrip.Bounds };
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            base.OnLayout(levent);
            if (FloatieOwner != null)
                _layout(false);
        }

        protected override bool HasShadow
        {
            get { return Opacity > MinShadowedOpacity; }
        }

        protected override void OnTimerTick()
        {
            try
            {
                if (!CanShow() || (!IsTracking && !FloatieOwner.InsideOwner))
                {
                    // must hide
                    _insideOwner = DateTime.MinValue;
                    if (Visible)
                        Hide();
                }
                else if (!Visible && FloatieOwner.Opacity >= MinOpacity)
                {
                    // might show
                    if (_insideOwner == DateTime.MinValue)
                        _insideOwner = DateTime.Now;
                    if (DateTime.Now - _insideOwner >= AutoPopupDelay)
                    {
                        Show();
                        DrawShadow();
                    }
                }
                else if (!IsTracking)
                {
                    // visible and should stay so - but might change opacity
                    double opacity = FloatieOwner.Opacity;
                    // if owner wants a shadow, no transparency (looks better):
                    if (opacity >= MinShadowedOpacity)
                        opacity = 1;
                    if (opacity != Opacity)
                    {
                        if (opacity >= MinOpacity)
                        {
                            bool hadShadow = HasShadow;
                            Opacity = opacity;
                            if (HasShadow != hadShadow)
                            {
                                if (HasShadow)
                                    DrawShadow();
                                else
                                    HideShadow();
                            }
                        }
                        else
                            Hide();
                    }
                }
                else if (Opacity != 1)
                {
                    // tracking
                    Opacity = 1;
                    DrawShadow();
                }

                HandleHelpOnTimerTick(Visible && ShowHelp);
            }
            catch
            {
            }
        }
        #endregion
    }
}
