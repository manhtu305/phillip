using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using C1.Util.Win;

namespace C1.Util.Design.Floaties
{
    internal class FloatieToolStrip : ToolStrip
    {
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case Win32.WM.WM_MOUSEACTIVATE:
                    m.Result = (IntPtr)Win32.MA.MA_NOACTIVATE;
                    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }
    }
}

