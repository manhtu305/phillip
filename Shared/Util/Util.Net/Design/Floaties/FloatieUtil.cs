#region Using directive

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using C1.Util.Win;
#endregion

namespace C1.Util.Design.Floaties
{
    internal class Util
    {
        /// <summary>
        /// Gets the value indicating whether the caller process owns
        /// the current foreground window.
        /// </summary>
        public static bool InActiveApplication
        {
            get
            {
                int currProcId = Win32.GetCurrentProcessId();
                IntPtr h = Win32.GetForegroundWindow();
                int foreProcId = 0;
                Win32.GetWindowThreadProcessId(h, ref foreProcId);
                return currProcId == foreProcId;
            }
        }
    }
}
