using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms.Design.Behavior;
using System.Reflection;
using System.Diagnostics;

using C1.Util.Win;

namespace C1.Util.Design.Floaties
{
    internal interface IShadowOwner
    {
        Region GetDisposableRegion();
        Rectangle[] GetRectangles();
    }

    internal class Shadow : NativeWindow
    {
        // shadow width
        internal const int ShadowWidth = 4;
        // shadow offset from top/left
        internal const int ShadowOffset = 3;
        // keys are owners, values as Shadow's
        private static Hashtable s_shadows = new Hashtable();

        public static void DrawShadow(Form owner)
        {
            HideShadow(owner);
            s_shadows[owner] = new Shadow(owner);
        }

        public static void HideShadow(Form owner)
        {
            Shadow oldShadow = s_shadows[owner] as Shadow;
            if (oldShadow != null)
                oldShadow.Detach();
        }

        #region only private members below
        private Form _owner = null;
        private Rectangle _bounds = Rectangle.Empty;

        private Shadow(Form owner)
        {
            System.Diagnostics.Debug.Assert(owner != null);
            System.Diagnostics.Debug.Assert(owner.Visible);

            _owner = owner;
            _owner.Closed += new EventHandler(_owner_Closed);
            _owner.VisibleChanged += new EventHandler(_owner_VisibleChanged);
            _owner.SizeChanged += new EventHandler(_owner_SizeChanged);

            _bounds = new Rectangle(_owner.Left, _owner.Top,
                _owner.Width + ShadowWidth, _owner.Height + ShadowWidth);

            // CreateParams
            CreateParams cp = new CreateParams();
            cp.Parent = _owner.Handle;
            cp.X = _bounds.X;
            cp.Y = _bounds.Y;
            cp.Width = _bounds.Width;
            cp.Height = _bounds.Height;
            cp.Style = unchecked((int)(uint)Win32.WS.WS_POPUP);
            cp.ExStyle = (int)(
                Win32.WS_EX.WS_EX_LAYERED |
                Win32.WS_EX.WS_EX_NOACTIVATE |
                Win32.WS_EX.WS_EX_TOOLWINDOW
                );
            base.CreateHandle(cp);

            // using SetWindowPos puts the shadow immediately below the owner,
            // which is good when owner is overlapped (e.g. by a dropdown menu)
            // SetWindowPosWin32.ShowWindow(this.Handle, (int)Win32.SW.SW_SHOWNOACTIVATE);
            Win32.SetWindowPos(Handle, _owner.Handle, cp.X, cp.Y, cp.Width, cp.Height, 
                Win32.SWP.SWP_NOACTIVATE | Win32.SWP.SWP_SHOWWINDOW);
            Redraw();
        }

        void _owner_Closed(object sender, EventArgs e)
        {
            Detach();
        }

        void _owner_SizeChanged(object sender, EventArgs e)
        {
            Detach();
        }

        void _owner_VisibleChanged(object sender, EventArgs e)
        {
            Detach();
        }

        private void Detach()
        {
            _owner.Closed -= new EventHandler(_owner_Closed);
            _owner.VisibleChanged -= new EventHandler(_owner_VisibleChanged);
            _owner.SizeChanged -= new EventHandler(_owner_SizeChanged);
            s_shadows.Remove(_owner);
            _owner = null;
            Win32.ShowWindow(this.Handle, Win32.SW.SW_HIDE);
            this.DestroyHandle();
        }

        private Region MakeRegion(int layer)
        {
            Region r1;
            IShadowOwner owner = _owner as IShadowOwner;
            if (owner == null)
            {
                r1 = (Region)_owner.Region.Clone();
            }
            else
            {
                r1 = owner.GetDisposableRegion();
                if (r1 == null)
                {
                    Rectangle[] rrs = owner.GetRectangles();
                    r1 = new Region();
                    r1.MakeEmpty();
                    foreach (Rectangle r in rrs)
                        r1.Union(r);
                }
            }
            Region ownerRegion = (Region)r1.Clone();
            Region r2 = (Region)r1.Clone();
            r2.Translate(ShadowOffset, ShadowOffset);
            r1.Intersect(r2);
            r2.Dispose();
            r1.Translate(layer, layer);
            r1.Exclude(ownerRegion);
            ownerRegion.Dispose();
            return r1;
        }

        private void Redraw()
        {
            if (_owner.Region == null && !(_owner is IShadowOwner))
                return;

            // make a bitmap and a graphics to draw on
            Bitmap memBmp = new Bitmap(_bounds.Width, _bounds.Height, PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(memBmp);

            // draw the shadow by overlapping _width shadows one over the other
            SolidBrush b = new SolidBrush(Color.FromArgb(25, 0, 0, 16));
            for (int i = ShadowWidth; i > 0; --i)
            {
                System.Drawing.Region r = MakeRegion(i);
                g.FillRegion(b, r);
                r.Dispose();
            }
            b.Dispose();

            // make a screen-compatible dc in memory
            IntPtr screenDC = Win32.GetDC(IntPtr.Zero);
            IntPtr memDC = Win32.CreateCompatibleDC(screenDC);

            // get the handle of the bitmap with the shadow
            IntPtr hBmp = memBmp.GetHbitmap(Color.FromArgb(0));
            // and select it to update the window
            IntPtr oldBmp = Win32.SelectObject(memDC, hBmp);

            // window size and position
            Win32.SIZE size;
            size.cx = _bounds.Width;
            size.cy = _bounds.Height;
            Win32.POINT pos;
            pos.x = _bounds.Left;
            pos.y = _bounds.Top;

            // memory bitmap is 0-based
            Win32.POINT srcOffset;
            srcOffset.x = 0;
            srcOffset.y = 0;

            // blend
            Win32.BLENDFUNCTION blend = new Win32.BLENDFUNCTION();
            blend.BlendOp = (byte)Win32.AlphaFormat.AC_SRC_OVER;
            blend.BlendFlags = 0;
            blend.SourceConstantAlpha = 255;
            blend.AlphaFormat = (byte)Win32.AlphaFormat.AC_SRC_ALPHA;

            // paint the window
            Win32.UpdateLayeredWindow(Handle, screenDC, ref pos, ref size,
                memDC, ref srcOffset, 0, ref blend, (int)Win32.ULW.ULW_ALPHA);

            // restore the old bitmap
            Win32.SelectObject(memDC, oldBmp);

            // cleanup
            Win32.ReleaseDC(IntPtr.Zero, screenDC);
            Win32.DeleteObject(hBmp);
            Win32.DeleteDC(memDC);
            memBmp.Dispose();
            g.Dispose();
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case (int)Win32.WM.WM_ERASEBKGND:
                    break;
                case (int)Win32.WM.WM_MOUSEACTIVATE:
                    m.Result = (IntPtr)Win32.MA.MA_NOACTIVATEANDEAT;
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion
    }
}