//
// NOTE: THIS CODE IS FOR .NET 2.0 AND HIGHER ONLY!!!
//
#if DEBUG
#define PublicUITypeEditors
#else
// All classes in this module are internal in a release build.
#endif
// #define NEW_COLOR_PICKER to use office-like interface, the following files
// should be added to the project:
// .NET\C1\Util\Design\UITypeEditor\ColorPicker\ColorPickerOfficeForm.cs
// .NET\C1\Util\Design\UITypeEditor\ColorPicker\ColorPickerUITypeEditor.cs
// To also use chart-style color picker, #define CHART_COLOR_PICKER additionally,
// and include this:
// .NET\C1\Util\Design\UITypeEditor\ColorPicker\ColorPickerForm.cs
// NOTE: To use ColorPickerOfficeForm, a #if for the project def must also be
// added at the top of ColorPickerUITypeEditor.cs.
#if C1COMMAND2005 || PREVIEW_2
#define NEW_COLOR_PICKER
// #define CHART_COLOR_PICKER
#else
// by default, use old style
#endif

// include this in your project def to use Tahoma 8 font
// #define TAHOMA_8

#pragma warning disable 1591 // 'Missing XML comment for publicly visible type or member'

//----------------------------------------------------------------------------
// UITypeEditorControl.cs (stolen from Bernardo's CustomEditors flex grid
// sample, adjusted a little bit for designer use). (see "dima" for additions)
//----------------------------------------------------------------------------
/* TODO for color picker:
 * - tab does not tab out of control (with drop down closed)
 * - make esc & enter keys work when drop down open.
 * - when dropdown closes, the control stays in a kind of dropped down state,
 *   so that next click on the dropdown button does not open the drop down
 *   but rather resets that state.
 * - drop down form is invisible. show something.
 * 
 */
//----------------------------------------------------------------------------
// 
// Control wrapper for UITypeEditors so they can be used as grid editors.
// 
// UITypeEditors are used extensively in .NET. They are directly supported by
// the PropertyGrid control and allow esiting specific types using custom popup
// or dropdown controls. Many UITypeEditors are available for editing types
// such as Color, Font, Images, etc.
// 
// However, UITypeEditors are not based no the Control class and therefore
// cannot be used directly as grid editors. This class does inherit from 
// Control and wraps a UITypeEditor so it can be used as a grid editor.
// 
// UITypeEditorControl can be used directly or it can be used as a base class
// for specialized UITypeEditors.
//
// The constructor specifies the UITypeEditor to use and whether the user 
// should be allowed to edit the string representation of the value (if 
// allowTyping is set to false, then the user can only edit usign the drop 
// down).
//
//----------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Reflection;
using System.Diagnostics;
using System.Text;

#if PREVIEW_2
using C1.C1Preview.Design;
#else
// using ...
#endif

namespace C1.Util.Design
{
    //===============================================================================
    #region ** UITypeEditorControl (base class for all of the controls below)

    /// <summary>
    /// UITypeEditorControl
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
    [DefaultEvent("ValueChanged")]
#if PublicUITypeEditors
    public
#else
    internal
#endif
        class UITypeEditorControl :
        ComboBox,
        IServiceProvider,
        IWindowsFormsEditorService
    {
        //===========================================================================
        #region ** fields
        // UITypeEditor responsible for editing the values
        private UITypeEditor _editor = null;
        // form used to show the drop down
        private Form _form = null;
        // drop down control's parent within the form
        private Control _controlParent = null;
        // cell bounds (used to position control and form)
        private Rectangle _bounds = Rectangle.Empty;
        // current editor value 
        private object _value = null;
        // whether to allow user to edit the string representation of the value
        private bool _allowTyping = true;
        // access to design time
        private ITypeDescriptorContext _typeDescriptorContext = null;
        // form we're on (must be re-activated after closing drop-down)
        private Form _parentForm = null;
        // flag used to prevent having more than one dropdown open
        private static bool s_inDropDown = false;
        // string format for default draw item
        private static readonly StringFormat s_sf = null;
        #endregion

        //===========================================================================
        #region ** ctor

        static UITypeEditorControl()
        {
            s_sf = new StringFormat();
            s_sf.Trimming = StringTrimming.EllipsisWord;
            s_sf.LineAlignment = StringAlignment.Center;
        }


        public UITypeEditorControl(UITypeEditor editor, bool allowTyping)
        {
            // save ctor parameters
            _editor = editor;
            _allowTyping = allowTyping;

            // initialize combo
            DropDownStyle = ComboBoxStyle.DropDown;
            DrawMode = DrawMode.OwnerDrawFixed;

            // initialize drop down editor
            _form = new Form();
            _form.StartPosition = FormStartPosition.Manual;
            _form.FormBorderStyle = FormBorderStyle.None;
            _form.ShowInTaskbar = false;
            _form.TopMost = true;
#if TAHOMA_8
            _form.Font = new Font("Tahoma", 8);
#endif
            _controlParent = new Panel();
            ((Panel)_controlParent).BorderStyle = BorderStyle.FixedSingle;
            _controlParent.Dock = DockStyle.Fill;
            _form.Controls.Add(_controlParent);
        }

        protected UITypeEditor Editor
        {
            get { return _editor; }
            set { _editor = value; }
        }

        protected virtual string GetValueAsString()
        {
            return Value == null ? string.Empty : Value.ToString();
        }

        #endregion

        #region ** typed value

        private EventHandler _valueChanged;

        protected virtual void OnValueChanged()
        {
            if (_valueChanged != null)
            {
                try
                {
                    _valueChanged(this, EventArgs.Empty);
                }
                catch
                {
                    _valueChanged = null;
                }
            }
        }

        public event EventHandler ValueChanged
        {
            add { _valueChanged += value; }
            remove { _valueChanged -= value; }
        }

        public object Value
        {
            get { return _value; }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    OnValueChanged();
                    try
                    {
                        if (!IsDisposed)
                            Invalidate();
                    }
                    catch
                    {
                    }
                }
            }
        }

        public virtual bool ShouldSerializeValue()
        {
            return Value != null;
        }

        public virtual void ResetValue()
        {
            Value = null;
        }

        #endregion

        internal ITypeDescriptorContext TypeDescriptorContext
        {
            get { return _typeDescriptorContext; }
            set { _typeDescriptorContext = value; }
        }

        //===========================================================================
        #region ** overrides

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            base.OnDrawItem(e);
            using (SolidBrush bBack = new SolidBrush(e.BackColor))
            using (SolidBrush bFore = new SolidBrush(e.ForeColor))
            using (Pen pFore = new Pen(e.ForeColor))
            {
                e.Graphics.FillRectangle(bBack, e.Bounds);
                e.Graphics.DrawString(GetValueAsString(), Font, bFore, e.Bounds);
            }
        }

        // if the user can't type, show drop down right away
        override protected void OnEnter(EventArgs e)
        {
            if (!_allowTyping)
                DoDropDown();
        }

        // if the user can type, show drop down when requested
        override protected void OnDropDown(EventArgs e)
        {
            if (_allowTyping)
                DoDropDown();
        }

        // sometimes VS crashes mysteriously. this is an attempt to prevent that:
        protected override void CreateHandle()
        {
            try
            {
                base.CreateHandle();
            }
            catch
            {
            }
        }

        #endregion

        //===========================================================================
        #region ** private members

        private void DoDropDown()
        {
            if (s_inDropDown)
                return;
            s_inDropDown = true;
            try
            {
                _parentForm = Parent.FindForm();
                _bounds = Parent.RectangleToScreen(this.Bounds);
                // fire event as usual
                base.OnDropDown(EventArgs.Empty);

                // edit value
                Value = _editor.EditValue(_typeDescriptorContext, (IServiceProvider)this, Value);

                // force the drop down to close
                DrawMode = DrawMode.Normal;
                DrawMode = DrawMode.OwnerDrawFixed;
                if (DroppedDown)
                    DroppedDown = false;
                Capture = false;

                base.OnDropDownClosed(EventArgs.Empty);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, "Exception in DoDropDown: " + e.ToString());
            }
            finally
            {
                s_inDropDown = false;
                if (_parentForm != null)
                {
                    _parentForm.Activate();
                    _parentForm = null;
                }
            }
        }

        #endregion

        //===========================================================================
        #region ** event handlers

        // close drop down when form deactivated
        private void _form_Deactivate(object sender, EventArgs e)
        {
            ((IWindowsFormsEditorService)this).CloseDropDown();
        }

        #endregion

        //===========================================================================
        #region ** IServiceProvider interface

        object IServiceProvider.GetService(Type serviceType)
        {
            if (serviceType == typeof(IWindowsFormsEditorService))
                return (IWindowsFormsEditorService)this;
            if (_typeDescriptorContext != null)
                return _typeDescriptorContext.GetService(serviceType);
            return null;
        }

        #endregion

        //===========================================================================
        #region ** IWindowsFormsEditorService interface

        void IWindowsFormsEditorService.DropDownControl(Control control)
        {
            // size form and/or control
            if (control.Width < _bounds.Width)
                control.Width = _bounds.Width;
            _form.ClientSize = new Size(control.Width + 2, control.Height + 2);  // we have a border around control - so include it's size

            // calculate form location to avoid being off the screen
            Point pt = new Point(_bounds.Right - control.Width, _bounds.Bottom);
            Rectangle rc = Screen.GetWorkingArea(this);
            if (_bounds.Bottom + control.Height > rc.Bottom)	// check bottom
                pt.Y = _bounds.Top - _form.Height;
            if (pt.Y < 0) pt.Y = 0;					// check top
            if (_bounds.X + control.Width > rc.Right)		// check right
                pt.X = rc.Right - _form.Width;
            if (pt.X < 0) pt.X = 0;					// check left

            // position form
            _form.Location = pt;

            // add control to form and show it
            control.Dock = DockStyle.Fill;
            control.Visible = true;
            _controlParent.Controls.Add(control);

            _form.Show();

            // wait until user makes a selection
            while (Form.ActiveForm == _form)
            {
                Application.DoEvents();
                MsgWaitForMultipleObjects(1, IntPtr.Zero, 0, 5, 255);
            }

            // done
            if (_form.Visible)
                _form.Hide();
            _controlParent.Controls.Clear();
        }

        void IWindowsFormsEditorService.CloseDropDown()
        {
            if (_form.Visible)
                _form.Hide();
            _controlParent.Controls.Clear();
        }

        // support modal editors
        DialogResult IWindowsFormsEditorService.ShowDialog(Form dialog)
        {
            bool topmost = dialog.TopMost;
            dialog.TopMost = true;
            DialogResult ret = dialog.ShowDialog();
            dialog.TopMost = topmost;
            return ret;
        }

        #endregion

        //===========================================================================
        #region ** dll imports

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern int MsgWaitForMultipleObjects(
            int nCount, IntPtr pHandles, short bWaitAll, int dwMilliseconds, int dwWakeMask);

        #endregion
    }
    #endregion

    //===============================================================================
    #region ** ColorPicker

    /// <summary>
    /// ColorPicker
    /// Uses the Color UITypeEditor to show a form where users can pick colors
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class ColorPicker : UITypeEditorControl
    {
        private static readonly StringFormat s_sf = null;
        private const int c_swatchWidth = 18;
#if NEW_COLOR_PICKER
        private EditorStyleEnum _editorStyle = EditorStyleEnum.Classic;
#endif

#if NEW_COLOR_PICKER
        public enum EditorStyleEnum
        {
            Office,
            Classic,
            Chart,
        }
#endif

        static ColorPicker()
        {
            s_sf = new StringFormat();
            s_sf.LineAlignment = StringAlignment.Center;
        }

        public ColorPicker() 
#if NEW_COLOR_PICKER
            : base(null, true)
#else
            : base(new ColorEditor(), true)
#endif
        {
#if NEW_COLOR_PICKER
            _setEditorStyle(EditorStyleEnum.Office);
#endif
            base.DropDownStyle = ComboBoxStyle.DropDownList;
        }

#if NEW_COLOR_PICKER
        private void _setEditorStyle(EditorStyleEnum editorStyle)
        {
            _editorStyle = editorStyle;
            switch (_editorStyle)
            {
                case EditorStyleEnum.Classic:
                    Editor = new ColorEditor();
                    break;
#if CHART_COLOR_PICKER
                case EditorStyleEnum.Chart:
                    Editor = new C1.Design.ColorPickerUITypeEditor();
                    ((C1.Design.ColorPickerUITypeEditor)Editor).FormType = typeof(C1.Design.ColorPickerForm);
                    break;
#endif
                case EditorStyleEnum.Office:
                default:
                    Editor = new C1.Design.ColorPickerUITypeEditor();
                    ((C1.Design.ColorPickerUITypeEditor)Editor).FormType = typeof(C1.Design.ColorPickerOfficeForm);
                    break;
            }
        }
#endif

        [Browsable(false)]
        public new ComboBoxStyle DropDownStyle
        {
            get { return ComboBoxStyle.DropDownList; }
            set { }
        }

#if NEW_COLOR_PICKER
        public EditorStyleEnum EditorStyle
        {
            get { return _editorStyle; }
            set
            {
                if (value != _editorStyle)
                    _setEditorStyle(value);
            }
        }
#endif

        public new Color Value
        {
            get { return base.Value is Color ? (Color)base.Value : Color.Empty; }
            set { base.Value = value; }
        }

        public override bool ShouldSerializeValue()
        {
            return !Value.IsEmpty;
        }

        public override void ResetValue()
        {
            Value = Color.Empty;
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            Color c = Value;
            string txt = c.IsNamedColor ? c.Name : string.Format("{0}, {1}, {2}", c.R, c.G, c.B);
            Rectangle rColor = new Rectangle(e.Bounds.X, e.Bounds.Y,
                Math.Min(e.Bounds.Width, c_swatchWidth), e.Bounds.Height - 1);
            Rectangle rText = new Rectangle(rColor.Right + 1, e.Bounds.Y,
                e.Bounds.Width - rColor.Width, e.Bounds.Height);
            using (SolidBrush bBack = new SolidBrush(e.BackColor))
            using (SolidBrush bFore = new SolidBrush(e.ForeColor))
            using (SolidBrush bSwatch = new SolidBrush(c))
            using (Pen pFore = new Pen(e.ForeColor))
            {
                e.Graphics.FillRectangle(bBack, e.Bounds);
                e.Graphics.FillRectangle(bSwatch, rColor);
                e.Graphics.DrawRectangle(pFore, rColor);
                e.Graphics.DrawString(txt, Font, bFore, rText, s_sf);
            }
        }
    }
    #endregion

    //===============================================================================
    #region ** FileNamePicker

    /// <summary>
    /// FileNamePicker
    /// Uses the FileName UITypeEditor to show a form where users can pick filenames
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class FileNamePicker : UITypeEditorControl
    {
        public FileNamePicker() : base(new FileNameEditor(), true) { }
    }
    #endregion

    //===============================================================================
    #region ** ImagePicker

    /// <summary>
    /// ImagePicker
    /// Uses the Image UITypeEditor to show a form where users can pick images
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class ImagePicker : UITypeEditorControl
    {
        private static readonly StringFormat s_sf = null;
        private const int c_swatchWidth = 18;

        static ImagePicker()
        {
            s_sf = new StringFormat();
            s_sf.LineAlignment = StringAlignment.Center;
        }
        
        public ImagePicker()
            : base(new ImageEditor(), true)
        {
            base.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        [Browsable(false)]
        public new ComboBoxStyle DropDownStyle
        {
            get { return ComboBoxStyle.DropDownList; }
            set { }
        }

        public new Image Value
        {
            get { return base.Value is Image ? (Image)base.Value : null; }
            set { base.Value = value; }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                ResetValue();
            base.OnKeyDown(e);
        }
        
        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            Image img = Value;
            string txt = img == null ? string.Empty : img.GetType().Name;
            Rectangle rPic = new Rectangle(e.Bounds.X, e.Bounds.Y,
                Math.Min(e.Bounds.Width, c_swatchWidth), e.Bounds.Height - 1);
            Rectangle rText = new Rectangle(rPic.Right + 1, e.Bounds.Y,
                e.Bounds.Width - rPic.Width, e.Bounds.Height);
            using (SolidBrush bBack = new SolidBrush(e.BackColor))
            using (SolidBrush bFore = new SolidBrush(e.ForeColor))
            using (Pen pFore = new Pen(e.ForeColor))
            {
                e.Graphics.FillRectangle(bBack, e.Bounds);
                e.Graphics.FillRectangle(Brushes.White, rPic);
                if (img != null)
                    e.Graphics.DrawImage(img, rPic);
                e.Graphics.DrawRectangle(pFore, rPic);
                e.Graphics.DrawString(txt, Font, bFore, rText, s_sf);
            }
        }
    }

    #endregion

    //===============================================================================
    #region ** FontPicker

    /// <summary>
    /// FontPicker
    /// Uses the Font UITypeEditor to show a form where users can pick fonts
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class FontPicker : UITypeEditorControl
    {
        public FontPicker() 
            : base(new FontEditor(), true)
        {
            base.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        [Browsable(false)]
        public new ComboBoxStyle DropDownStyle
        {
            get { return ComboBoxStyle.DropDownList; }
            set { }
        }

        public new Font Value
        {
            get { return base.Value is Font ? (Font)base.Value : null; }
            set { base.Value = value; }
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            Font f = Value;
            string txt = f == null ? string.Empty :
                string.Format("{0}, {1}pt, {2}", f.FontFamily.Name, f.SizeInPoints, f.Style.ToString());
            using (SolidBrush bBack = new SolidBrush(e.BackColor))
            using (SolidBrush bFore = new SolidBrush(e.ForeColor))
            using (Pen pFore = new Pen(e.ForeColor))
            {
                e.Graphics.FillRectangle(bBack, e.Bounds);
                e.Graphics.DrawString(txt, f == null ? Font : f, bFore, e.Bounds);
            }
        }
    }

    #endregion

    //===============================================================================
    #region ** DockPicker

    /// <summary>
    /// DockPicker
    /// Uses the Dock UITypeEditor to show a form where users can pick Dock settings
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class DockPicker : UITypeEditorControl
    {
        public DockPicker() : base(new DockEditor(), true) { }
    }
    #endregion

    //===============================================================================
    #region ** AnchorPicker

    /// <summary>
    /// AnchorPicker
    /// Uses the Anchor UITypeEditor to show a form where users can Anchor settings
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class AnchorPicker : UITypeEditorControl
    {
        public AnchorPicker() : base(new AnchorEditor(), true) { }
    }
    #endregion

    //===============================================================================
    #region ** ExternalEditorWrapper

    /// <summary>
    /// ExternalEditorWrapper
    /// Wraps an external UITypeEditor.
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
 class ExternalEditorWrapper : UITypeEditorControl
    {
        public ExternalEditorWrapper()
            : base(null, true)
        {
            base.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        [Browsable(false)]
        [DefaultValue(null)]
        public new UITypeEditor Editor
        {
            get { return base.Editor; }
            set { base.Editor = value; }
        }

        [Browsable(false)]
        [DefaultValue(ComboBoxStyle.DropDownList)]
        public new ComboBoxStyle DropDownStyle
        {
            get { return ComboBoxStyle.DropDownList; }
            set { }
        }
    }

    #endregion

    //===============================================================================
    #region ** CollectionEditor

    /// <summary>
    /// FontPicker
    /// Uses the Font UITypeEditor to show a form where users can pick fonts
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class CollectionEditorControl : ExternalEditorWrapper
    {
        public CollectionEditorControl()
        {
        }

        [Browsable(false)]
        public new ComboBoxStyle DropDownStyle
        {
            get { return ComboBoxStyle.DropDownList; }
            set { }
        }

        protected override string GetValueAsString()
        {
            ICollection coll = Value as ICollection;
            if (coll == null)
                return string.Empty;
            StringBuilder sb = new StringBuilder();
            foreach (object o in coll)
                sb.AppendFormat("{0}, ", o);
            if (sb.Length > 2)
                sb.Remove(sb.Length - 2, 2);
            return sb.ToString();
        }
    }

    #endregion

    //===============================================================================
    #region ** FlagsEnumPicker

    /// <summary>
    /// FontPicker
    /// Uses the Font UITypeEditor to show a form where users can pick fonts
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true), SmartAssembly.Attributes.DoNotObfuscateType]
#if PublicUITypeEditors
    public
#else
    internal
#endif
    class FlagsEnumPicker : UITypeEditorControl
    {
        public FlagsEnumPicker() 
            : base(new FlagsEnumEditor(), true)
        {
            base.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        [Browsable(false)]
        public new ComboBoxStyle DropDownStyle
        {
            get { return ComboBoxStyle.DropDownList; }
            set { }
        }
    }

    #endregion
}
