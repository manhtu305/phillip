using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.ComponentModel.Design;
using System.Reflection;
using System.Threading;
using System.Security;
using System.Security.Permissions;

#if TRUEGRID_2
namespace C1.Win.C1TrueDBGrid
#elif C1COMMAND2005
namespace C1.Win.C1Command
#elif FLEXGRID_2
namespace C1.Win.C1FlexGrid
#else
namespace C1.Util.Win.Design
#endif
{
    public abstract class DropDownFormClassNameEditor : System.Drawing.Design.UITypeEditor
    {
        abstract protected string BaseFormClassName { get; }
        virtual protected string NoneString { get { return "(none)"; } }
        virtual protected bool IncludeNoneInList { get { return true; } }
        virtual protected bool IncludeBaseFormClassInList { get { return false; } }

        private IWindowsFormsEditorService _edSvc = null;
        private ListBox _box;

        private Type _projectItemType;
        private Type _projectItemsType;
        private Type _projectType;
        private Type _projectsType;
        private Type _dteType;
        private Type _solutionType;
        private Type _fileCodeModelType;
        private Type _codeElementType;
        private Type _codeElementsType;
        private Type _codeNamespaceType;
        private Type _codeClassType;
        private object _namespaceEC;
        private object _classEC;
        private object _publicEC;
        private string _vbProjectC;
        private string _csProjectC;
        private MethodInfo _get_ContainingProject;
        private MethodInfo _get_ProjectKind;
        private MethodInfo _get_DTE;
        private MethodInfo _get_Solution;
        private MethodInfo _get_Projects;
        private MethodInfo _projectsEM;
        private MethodInfo _get_ProjectItems;
        private MethodInfo _projectItemsEM;
        private MethodInfo _get_Name;
        private MethodInfo _get_FileCodeModel;
        private MethodInfo _get_ProjectSubItems;
        private MethodInfo _get_CodeElements;
        private MethodInfo _codeElementsEM;
        private MethodInfo _get_Kind;
        private MethodInfo _get_Members;
        private MethodInfo _isDerivedMethod;
        private MethodInfo _get_IsAbstract;
        private MethodInfo _get_Access;
        private MethodInfo _get_FullName;
        private MethodInfo _projectItemsTemplatePath;
        private MethodInfo _addFromTemplate;
        private bool _initialized = false;
        private object _project;
        private object _solution;
        private IDesignerHost _dh;

        [PermissionSetAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context) 
        {
            return (context != null && context.Instance != null && !(context.Instance is Object[])) 
                ? UITypeEditorEditStyle.DropDown : base.GetEditStyle(context);
        }
        [PermissionSetAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value) 
        {
            if (context == null || provider == null) 
                return value;
            object inst = context.Instance;
            if (inst == null || inst is Object[])
                return value;
            _edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (_edSvc == null)
                return value;
            _box = new ListBox();
            _box.Click += new System.EventHandler(OnListBox_Click);
            _box.BorderStyle = BorderStyle.None;
            if (IncludeNoneInList)
                _box.Items.Add(NoneString);
            FillListBox(inst, provider);
            if (IncludeBaseFormClassInList && !_box.Items.Contains(BaseFormClassName))
                _box.Items.Insert(IncludeNoneInList ? 1 : 0, BaseFormClassName);
#if SKIP_THIS // wizard creating a new dropdown form is not included
			_box.Items.Add(C1InputStrings.strNewDropDownForm);
#endif //SKIP_THIS
            int index = value != null ? _box.Items.IndexOf(value) : -1;
            if (index != -1)
                _box.SelectedIndex = index;
            _edSvc.DropDownControl(_box);
            index = _box.SelectedIndex;
            if (index == 0)
                value = "";
#if SKIP_THIS // wizard creating a new dropdown form is not included
			else if (index == _box.Items.Count - 1)
			{
				string newDropName = CreateDropDownForm();
				if (newDropName != null)
					value = newDropName;
			}
#endif //SKIP_THIS
            else if (index != -1)
                value = _box.SelectedItem;
            return value;
        }
        #region skipped
#if SKIP_THIS // wizard creating a new dropdown form is not included
        private string CreateDropDownForm()
		{
			if (!_initialized || _dh == null)
				return null;
			string dropNamePattern = C1InputStrings.strDropNamePattern;
			string prjKind = (string)_get_ProjectKind.Invoke(_project, null);
			bool isVBProject = false;
			if (String.Compare(prjKind, _vbProjectC, true) == 0)
			{
				dropNamePattern += ".vb";
				isVBProject = true;
			}
			else if (String.Compare(prjKind, _csProjectC, true) == 0)
				dropNamePattern += ".cs";
			else
				return null;
			string formName = "";
			bool found = true;
			object ps = _get_ProjectItems.Invoke(_project, null);
			IEnumerator pie = (IEnumerator)_projectItemsEM.Invoke(ps, null);
			for (int i = 1; found && i < 10000; i++)
			{
				formName = String.Format(dropNamePattern, i);
				found = false;
				pie.Reset();
				while (pie.MoveNext())
					if (String.Compare(formName, (string)_get_Name.Invoke(pie.Current, null), true) == 0)
					{
						found = true;
						break;
					}
			}
			NameForm nameForm = new NameForm();
			nameForm.txtName.Text = !found ? formName : "";
			do
			{
				if (nameForm.ShowDialog() != DialogResult.OK)
					return null;
				formName = nameForm.txtName.Text;
				found = false;
				pie.Reset();
			while (pie.MoveNext())
				if (String.Compare(formName, (string)_get_Name.Invoke(pie.Current, null), true) == 0)
				{
					found = true;
					MessageBox.Show(C1InputStrings.strAlreadyUsedMsg, C1InputStrings.strNotificationError,
						MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					break;
				}
			} while (found);
			string templatePath = (string)_projectItemsTemplatePath.Invoke(_solution, new object[] {prjKind});
			object item;
			try
			{
				if (isVBProject)
					item = _addFromTemplate.Invoke(ps, new object[] {templatePath + @"\" +
						C1InputStrings.strVBWizardFileName, formName});
				else
					item = _addFromTemplate.Invoke(ps, new object[] {templatePath + @"\" +
						C1InputStrings.strCSWizardFileName, formName});
			}
			catch
			{
				MessageBox.Show(C1InputStrings.strNoWizardMessage, C1InputStrings.strErrorCaption,
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				return null;
			}
			_dh.Activate();
			if (item != null)
			{
				object fcm = _get_FileCodeModel.Invoke(item, null);
				if (fcm != null)
				{
					IEnumerator codeElementsEnum1 = (IEnumerator)_codeElementsEM.Invoke(_get_CodeElements.Invoke(fcm, null), null);
					while (codeElementsEnum1.MoveNext())
					{
						object ce1 = codeElementsEnum1.Current;
						object kind1 = _get_Kind.Invoke(ce1, null);
						if (_namespaceEC.Equals(kind1))
						{
							IEnumerator codeElementsEnum2 = (IEnumerator)_codeElementsEM.Invoke(_get_Members.Invoke(ce1, null), null);
							while (codeElementsEnum2.MoveNext())
							{
								object ce2 = codeElementsEnum2.Current;
								if (_classEC.Equals(_get_Kind.Invoke(ce2, null)) &&
									(bool)_isDerivedMethod.Invoke(ce2, new object[] {BaseFormClassName}))
									return (string)_get_FullName.Invoke(ce2, null);
							}
						}
						else if (_classEC.Equals(kind1) &&
							(bool)_isDerivedMethod.Invoke(ce1, new object[] {BaseFormClassName}))
							return (string)_get_FullName.Invoke(ce1, null);
					}
				}
			}
			return null;
		}
#endif //SKIP_THIS
        #endregion

        private void InitializeFields()
        {
            Assembly[] asmList = Thread.GetDomain().GetAssemblies();
            Assembly envDTE = null;
            Assembly vsLangProj = null;
            string asmName;
            for (int i = 0; i < asmList.Length; i++)
            {
                asmName = asmList[i].GetName().Name;
                if (String.Compare(asmName, "EnvDTE", true, CultureInfo.InvariantCulture) == 0)
                    envDTE = asmList[i];
                else if (String.Compare(asmName, "VSLangProj", true, CultureInfo.InvariantCulture) == 0)
                    vsLangProj = asmList[i];
            }
            if (envDTE == null || vsLangProj == null)
                return;
            Type vsCMElementType = null;
            Type vsCMAccessType = null;
            Type[] types = envDTE.GetTypes();
            for (int i = 0; i < types.Length; i++)
                switch (types[i].Name)
                {
                    case "ProjectItem":		_projectItemType = types[i];	break;
                    case "ProjectItems":	_projectItemsType = types[i];	break;
                    case "Project":			_projectType = types[i];		break;
                    case "Projects":		_projectsType = types[i];		break;
                    case "_DTE":			_dteType = types[i];			break;
                    case "_Solution":		_solutionType = types[i];		break;
                    case "FileCodeModel":	_fileCodeModelType = types[i];	break;
                    case "CodeElement":		_codeElementType = types[i];	break;
                    case "CodeElements":	_codeElementsType = types[i];	break;
                    case "vsCMElement":		vsCMElementType = types[i];		break;
                    case "CodeNamespace":	_codeNamespaceType = types[i];	break;
                    case "CodeClass":		_codeClassType = types[i];		break;
                    case "vsCMAccess":		vsCMAccessType = types[i];		break;
                }
            _namespaceEC = Enum.Parse(vsCMElementType, "vsCMElementNamespace", false);
            _classEC = Enum.Parse(vsCMElementType, "vsCMElementClass", false);
            _publicEC = Enum.Parse(vsCMAccessType, "vsCMAccessPublic", false);
            Type prjKindType = null;
            types = vsLangProj.GetTypes();
            for (int i = 0; i < types.Length; i++)
                if (types[i].Name == "PrjKind")
                {
                    prjKindType = types[i];
                    break;
                }
            _vbProjectC = (string)prjKindType.GetField("prjKindVBProject").GetValue(null);
            _csProjectC = (string)prjKindType.GetField("prjKindCSharpProject").GetValue(null);
            _get_ContainingProject = _projectItemType.GetMethod("get_ContainingProject");
            _get_ProjectKind = _projectType.GetMethod("get_Kind");
            _get_DTE = _projectType.GetMethod("get_DTE");
            _get_Solution = _dteType.GetMethod("get_Solution");
            _get_Projects = _solutionType.GetMethod("get_Projects");
            _projectsEM = _projectsType.GetMethod("GetEnumerator");
            _get_ProjectItems = _projectType.GetMethod("get_ProjectItems");
            _projectItemsEM = _projectItemsType.GetMethod("GetEnumerator");
            _get_Name = _projectItemType.GetMethod("get_Name");
            _get_FileCodeModel = _projectItemType.GetMethod("get_FileCodeModel");
            _get_ProjectSubItems = _projectItemType.GetMethod("get_ProjectItems");
            _get_CodeElements = _fileCodeModelType.GetMethod("get_CodeElements");
            _codeElementsEM = _codeElementsType.GetMethod("GetEnumerator");
            _get_Kind = _codeElementType.GetMethod("get_Kind");
            _get_Members = _codeNamespaceType.GetMethod("get_Members");
            _isDerivedMethod = _codeClassType.GetMethod("get_IsDerivedFrom");
            _get_IsAbstract = _codeClassType.GetMethod("get_IsAbstract");
            _get_Access = _codeClassType.GetMethod("get_Access");
            _get_FullName = _codeClassType.GetMethod("get_FullName");
            _projectItemsTemplatePath = _solutionType.GetMethod("ProjectItemsTemplatePath");
            _addFromTemplate = _projectItemsType.GetMethod("AddFromTemplate");
            _initialized = true;
        }

        private void FillListBox(object instance, IServiceProvider provider)
        {
            _dh = null;

            if (instance is Component && ((Component)instance).Site != null)
                _dh = ((Component)instance).Site.GetService(typeof(IDesignerHost)) as IDesignerHost;
            else if (provider != null)
                _dh = provider.GetService(typeof(IDesignerHost)) as IDesignerHost;
            if (_dh == null)
                return;

            if (!_initialized)
                InitializeFields();

            if (!_initialized)
                return;

            object epi = _dh.GetService(_projectItemType);
            if (epi == null)
                return;
            _project = _get_ContainingProject.Invoke(epi, null);
            _solution = _get_Solution.Invoke(_get_DTE.Invoke(_project, null), null);
            object eps = _get_Projects.Invoke(_solution, null);
            IEnumerator projectsEnum = (IEnumerator)_projectsEM.Invoke(eps, null);
            while (projectsEnum.MoveNext())
            {
                object p = projectsEnum.Current;
                try
                {
                    IEnumerator projectItemsEnum = (IEnumerator)_projectItemsEM.Invoke(_get_ProjectItems.Invoke(p, null), null);
                    while (projectItemsEnum.MoveNext())
                        RetrieveFormsFromProjectItem(p, projectItemsEnum.Current);
                }
#if FXCOP_NO_GENERIC_EXCEPTION_CATCHING
				catch (ApplicationException)
#else
                catch (Exception)
#endif
                {
                }
            }
        }

        private void RetrieveFormsFromProjectItem(object p, object projectItem)
        {
            object fcm = _get_FileCodeModel.Invoke(projectItem, null);
            if (fcm != null)
            {
                IEnumerator codeElementsEnum1 = (IEnumerator)_codeElementsEM.Invoke(_get_CodeElements.Invoke(fcm, null), null);
                while (codeElementsEnum1.MoveNext())
                {
                    object ce1 = codeElementsEnum1.Current;
                    object kind1 = _get_Kind.Invoke(ce1, null);
                    if (_namespaceEC.Equals(kind1))
                    {
                        IEnumerator codeElementsEnum2 = (IEnumerator)_codeElementsEM.Invoke(_get_Members.Invoke(ce1, null), null);
                        while (codeElementsEnum2.MoveNext())
                            if (_classEC.Equals(_get_Kind.Invoke(codeElementsEnum2.Current, null)))
                                AddFormInList(codeElementsEnum2.Current, p);
                    }
                    else if (_classEC.Equals(kind1))
                        AddFormInList(ce1, p);
                }
            }
            else
            {
                object subItems = _get_ProjectSubItems.Invoke(projectItem, null);
                if (subItems != null)
                {
                    IEnumerator projectItemsEnum = (IEnumerator)_projectItemsEM.Invoke(subItems, null);
                    while (projectItemsEnum.MoveNext())
                        RetrieveFormsFromProjectItem(p, projectItemsEnum.Current);
                }
            }
        }

        private void AddFormInList(object ce, object p)
        {
            if ((bool)_isDerivedMethod.Invoke(ce, new object[] {BaseFormClassName}) &&
                !(bool)_get_IsAbstract.Invoke(ce, null) &&
                (p == _project || _publicEC.Equals(_get_Access.Invoke(ce, null))))
                _box.Items.Add(_get_FullName.Invoke(ce, null));
        }

        private void OnListBox_Click(object sender, System.EventArgs e)
        {
            if (_edSvc != null)
                _edSvc.CloseDropDown();            
        }
    }
}
