using System;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Reflection;
using System.Diagnostics;

namespace C1.Util.Design
{
    internal class TypeDescriptorContextImpl : ITypeDescriptorContext
    {
        private object _instance = null;
        private PropertyDescriptor _propertyDescriptor = null;
        private ComponentDesigner _designer = null;
        private IServiceProvider _serviceProvider = null;

        public TypeDescriptorContextImpl(object instance,
            PropertyDescriptor propertyDescriptor,
            ComponentDesigner designer,
            IServiceProvider serviceProvider)
        {
            _instance = instance;
            _propertyDescriptor = propertyDescriptor;
            _designer = designer;
            _serviceProvider = serviceProvider;
        }

        public object Instance
        {
            get { return ((ITypeDescriptorContext)this).Instance; }
            set { _instance = value; }
        }

        public IServiceProvider ServiceProvider
        {
            get { return _serviceProvider; }
            set { _serviceProvider = value; }
        }

        public ComponentDesigner Designer
        {
            get { return _designer; }
            set { _designer = value; }
        }

        public PropertyDescriptor PropertyDescriptor
        {
            get { return _propertyDescriptor; }
            set { _propertyDescriptor = value; }
        }

        object IServiceProvider.GetService(Type serviceType)
        {
            return _serviceProvider.GetService(serviceType);
        }

        IContainer ITypeDescriptorContext.Container
        {
            get { return _designer == null ? null : _designer.Component.Site.Container; }
        }

        object ITypeDescriptorContext.Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                else if (_designer != null)
                    return _designer.Component;
                else
                    return null;
            }
        }

        PropertyDescriptor ITypeDescriptorContext.PropertyDescriptor
        {
            get { return _propertyDescriptor; }
        }

        private IComponentChangeService ComponentChangeService
        {
            get
            {
                return ((IServiceProvider)this).GetService(typeof(IComponentChangeService)) as IComponentChangeService;
            }
        }

        void ITypeDescriptorContext.OnComponentChanged()
        {
            IComponentChangeService ccs = ComponentChangeService;
            if (ccs != null)
                ccs.OnComponentChanged(((ITypeDescriptorContext)this).Instance,
                    ((ITypeDescriptorContext)this).PropertyDescriptor, null, null);
        }

        bool ITypeDescriptorContext.OnComponentChanging()
        {
            IComponentChangeService ccs = ComponentChangeService;
            if (ccs != null)
            {
                try
                {
                    ccs.OnComponentChanging(((ITypeDescriptorContext)this).Instance,
                        ((ITypeDescriptorContext)this).PropertyDescriptor);
                }
                catch (CheckoutException e)
                {
                    if (e == CheckoutException.Canceled)
                        return false;
                    throw e;
                }
            }
            return true;
        }
    }
}
