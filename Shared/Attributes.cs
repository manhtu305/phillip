﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo
{
    [AttributeUsage(AttributeTargets.Property)]
    public class WidgetOptionAttribute : Attribute
    {
        public WidgetOptionAttribute()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class WidgetOptionNameAttribute : Attribute
    {
        // Fields
        private string _name;

        // Methods
        public WidgetOptionNameAttribute()
        {
        }

        public WidgetOptionNameAttribute(string name)
        {
            this._name = name;
        }

        public override bool IsDefaultAttribute()
        {
            return string.IsNullOrEmpty(this.Name);
        }

        // Properties
        public string Name
        {
            get
            {
                return this._name;
            }
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class WidgetEventAttribute : Attribute
    {
        public WidgetEventAttribute()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    class WidgetRequiredOptionAttribute : Attribute
    {
        public WidgetRequiredOptionAttribute()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ElementReferenceAttribute : Attribute
    {
        // Methods
        public ElementReferenceAttribute() { }
    }

}
