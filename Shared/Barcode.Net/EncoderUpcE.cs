//----------------------------------------------------------------------------
// EncoderUpcA.cs
//
// Copyright (C) 2006 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		June 2005	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderUpcE
	/// </summary>
    internal class EncoderUpcE : EncoderUpcA
	{
        // UPC-E parity tables
        // #c1spell(off)
        private static string[] _parity0 = new string[]
	    {
            "eeeooo",
            "eeoeoo",
            "eeooeo",
            "eeoooe",
            "eoeeoo",
            "eooeeo",
            "eoooee",
            "eoeoeo",
            "eoeooe",
            "eooeoe",
        };
        private static string[] _parity1 = new string[]
        {
            "oooeee",
            "ooeoee",
            "ooeeoe",
            "ooeeeo",
            "oeooee",
            "oeeooe",
            "oeeeoo",
            "oeoeoe",
            "oeoeeo",
            "oeeoeo",
        };
        // #c1spell(on)

        // ** ctor
        internal EncoderUpcE(C1BarCode ctl) : base(ctl) { }

		// ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
            // allow user to supply the correct check digit
            text = StripOptionalControlDigit(text);

            // sanity
            if (BadText(text))
                throw new ArgumentException("UpcE requires exactly 11 digits.");
            if (text[0] != '0' && text[0] != '1')
                throw new ArgumentException("First digit in UpcE must be '0' or '1'.");

            // interpret code
            char numberSystem = text[0];
            string manufacturer = text.Substring(1, 5);
            string productCode = text.Substring(6, 5);
            char checkDigit = GetCheckDigit("0" + text);
            text = GetUpcE(manufacturer, productCode, checkDigit);
            if (text == null)
                throw new ArgumentException("Cannot encode this text as UpcE (invalid manufacturer/product combination).");

            // initialize
            _cursor = new Point(0, 0);
            _totalWidth = 0;

            // quiet zone
            DrawPattern(g, br, "000000000");

            // left guards
            DrawPattern(g, br, "101");

            // first number system digit determines manufacturer parity
            int mfIndex = checkDigit - '0';
            string mf = numberSystem == '0' ? _parity0[mfIndex] : _parity1[mfIndex];

            // 6 code digits
            for (int index = 0; index < 6; index++)
            {
                DrawDigit(g, br, text[index], mf[index]);
            }

            // draw center guards
            DrawPattern(g, br, "01010");

            // right guard
            DrawPattern(g, br, "1");

            // quiet zone
            DrawPattern(g, br, "000000000");

            // return total width
            return _totalWidth;
		}
        override protected void DrawText(Graphics g, string text, Font font, Brush br, Rectangle rc)
        {
            // allow user to supply the correct check digit
            text = StripOptionalControlDigit(text);

            // skip invalid stuff
            if (BadText(text))
            {
                base.DrawText(g, text, font, br, rc);
                return;
            }

            // interpret code
            char numberSystem = text[0];
            string manufacturer = text.Substring(1, 5);
            string productCode = text.Substring(6, 5);
            char checkDigit = GetCheckDigit("0" + text);
            text = GetUpcE(manufacturer, productCode, checkDigit);
            if (text == null)
            {
                base.DrawText(g, text, font, br, rc);
                return;
            }

            // draw control digit on the left
            Rectangle r = rc;
            r.Y -= font.Height / 2;
            r.Width = rc.Width * 9 / 89;
            DrawTextBase(g, numberSystem.ToString(), font, br, r);

            // draw check digit on the right
            r.X = rc.Width * 62 / 69;
            r.Width = rc.Width * 9 / 69;
            DrawTextBase(g, checkDigit.ToString(), font, br, r);

            // draw middle part
            r.Y = rc.Y;
            r.X = rc.Width * 11 / 69;
            r.Width = rc.Width * 50 / 69;
            DrawTextBase(g, text, font, br, r);
        }   

        // ** private
        private string GetUpcE(string manufacturer, string productCode, char checkDigit)
        {
			// get product number
			int productNumber = int.MaxValue;
            int.TryParse(productCode, out productNumber);

            // If the manufacturer code ends in 000, 100, or 200, 
            // the UPC-E code consists of the first two characters of the manufacturer code, 
            // the last three characters of the product code, followed by the third character of the 
            // manufacturer code. The product code must be 00000 to 00999. 
            if (manufacturer.EndsWith("000") || manufacturer.EndsWith("100") || manufacturer.EndsWith("200"))
            {
                return productNumber <= 999
                    ? manufacturer.Substring(0, 2) + productCode.Substring(2, 3) + manufacturer[2]
                    : null;
            }

            // If the manufacturer code ends in 00 but does not qualify for #1 above, the UPC-E code consists 
            // of the first three characters of the manufacturer code, the last two characters of the product code, 
            // followed by the digit "3". The product code must be 00000 to 00099. 
            if (manufacturer.EndsWith("00"))
            {
                return productNumber <= 99
                    ? manufacturer.Substring(0, 3) + productCode.Substring(3, 2) + '3'
                    : null;
            }

            // If the manufacturer code ends in 0 but does not quality for #1 or #2 above, the UPC-E code consists of 
            // the first four characters of the manufacturer code, the last character of the product code, followed 
            // by the digit "4". The product code must be 00000 to 00009. 
            if (manufacturer.EndsWith("0"))
            {
                return productNumber <= 9
                    ? manufacturer.Substring(0, 4) + productCode.Substring(4, 1) + '4'
                    : null;
            }

            // If the manufacturer code does not end in zero, the UPC-E code consists of the entire manufacturer code 
            // and the last digit of the product code. Note that the last digit of the product code must be in the 
            // range of 5 through 9. The product code must be 00005 to 00009. 
            return productNumber >= 5 && productNumber <= 9
                ? manufacturer + productCode.Substring(4, 1)
                : null;
        }
	}
}
