﻿using System;
using System.Data;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using C1.Util.Localization;

namespace C1.Win.C1BarCode
{
    /// <summary>
    /// Creates QR barcode representations of alphanumeric values.
    /// </summary>
    /// <remarks>
    /// <para>To use the C1QRCode control, simply set the <see cref="Text"/> 
    /// property to the value you want to encode.</para>
    /// <para>The control will show the QR image. If you want to include 
    /// the QR image in a document, use the <see cref="Image"/> property 
    /// to retrieve an image of the barcode.</para>
    /// <para>The QR (Quick Response) format is one of the most popular 2D 
    /// barcode formats today, with free readers available for virtually 
    /// all smart phones.</para>
    /// <para>The main reasons for the popularity of the QR format are its 
    /// efficiency (it is very compact), flexibility (you don't need special 
    /// scanners to read it), and the fact that the original developer of 
    /// the format (the DENSO-WAVE company) made it an open and freely 
    /// available standard (ISO/IEC18004 and others).</para>
    /// <para>
    /// For details on the QR format, please see:
    /// http://www.denso-wave.com/qrcode/qrstandard-e.html and
    /// http://en.wikipedia.org/wiki/QR_code.
    /// </para>
    /// </remarks>
    /// <example>This sample shows how to use the Text property to encode
    /// the value "123456" as a QR barcode.
    /// <code>
    /// c1qr1.Text = "Hello QR!";
    /// pictureBox1.Image = c1qr1.Image;
    /// </code>
    /// </example>
#if EXPOSE_BARCODE
    [
#if CLR40
    Designer("C1.Win.C1BarCode.Design.Designer, C1.Win.C1BarCode.4.Design, Version=" + C1.Util.Licensing.VersionConst.VerString),
#else
    Designer(typeof(C1.Win.C1BarCode.Design.Designer)),
#endif
    LicenseProvider(typeof(LicenseProvider)),
    ToolboxBitmap(typeof(C1BarCode), "C1QRCode.png")
    ]
    public class C1QRCode : Control
#else
    internal class C1QRCode : Control
#endif
    {
        //-----------------------------------------------------------------------
        #region ** fields

        QRCodeEncoder _encoder = new QRCodeEncoder();
        int _codeVersion = 0;
        ErrorCorrectionLevel _ecl = ErrorCorrectionLevel.L;
        Encoding _encoding = Encoding.Automatic;
        int _symbolSize = 3;
        PictureBoxSizeMode _sizeMode = PictureBoxSizeMode.Normal;
        Image _img;
        bool _dirty;

        #endregion

        //-----------------------------------------------------------------------
        #region ** ctor
#if EXPOSE_BARCODE
#if CLR40
        // register the dynamic designer provider so that we can specify a 
        // fall-back designer when our custom designer type is not available.
        static C1QRCode()
        {
            TypeDescriptor.AddProvider(
                new C1.Util.Design.DynamicDesignerProvider(
                    typeof(C1.Win.C1BarCode.C1QRCode),
                    TypeDescriptor.GetProvider(typeof(object))),
                    typeof(object));
        }
#endif
        /// <summary>
        /// Displays the AboutBox for this control.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ShowAboutBox()
        {
            C1.Util.Licensing.ProviderInfo.ShowAboutBox(this);
        }
#endif
        /// <summary>
        /// Creates a new instance of the <see cref="C1QRCode"/> control.
		/// </summary>
        public C1QRCode()
		{
#if EXPOSE_BARCODE
			// enforce licensing
            C1.Util.Licensing.ProviderInfo.Validate(typeof(C1QRCode), this);
#endif
			// set styles
			SetStyle(ControlStyles.ResizeRedraw, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			Text			= string.Empty;
			BackColor		= Color.White;
			ForeColor		= Color.Black;
		}

        #endregion

        //-----------------------------------------------------------------------
        #region ** object model

        /// <summary>
        /// Gets or sets the "version" of QR code to generate.
        /// </summary>
        /// <remarks>
        /// <para>The QR specification includes 40 "versions" of QR codes. 
        /// Higher versions consume more space and are able to encode more 
        /// information.</para>
        /// <para>The <see cref="C1QRCode"/> control can generate QR codes
        /// versions one through ten only, which allow encoding up to 
        /// 652 digits, or 395 alphanumeric characters, or 271 bytes.</para>
        /// <para>Set the <see cref="CodeVersion"/> property to zero (the default
        /// value) to automatically use the most compact version able to render the
        /// content specified in the <see cref="Text"/> property.</para>
        /// </remarks>
        [
        C1.Win.Localization.C1Description(typeof(Strings), "C1QRCode.CodeVersion", "Gets or sets the 'version' of QR code to generate (set to zero to auto-select)."),
        DefaultValue(0)
        ]
        public int CodeVersion
        {
            get { return _codeVersion; }
            set
            {
                if (value < 0 || value > 10) 
                {
                    throw new Exception(Strings.Errors.InvalidCodeVersion);
                }
                if (value != _codeVersion)
                {
                    _codeVersion = value;
                    SetDirty();
                }
            }
        }
        /// <summary>
        /// Gets or sets the error correction level used to create the QR code.
        /// </summary>
        /// <remarks>
        /// <para>Higher levels of error correction add more redundant information to 
        /// the QR code, making it more resistant to damage.
        /// Higher levels of error correction also reduce the amount of data
        /// that can be encoded in a given area.</para>
        /// <para>The default value for this property is the lowest level, which
        /// produces the most compact QR codes.</para>
        /// </remarks>
        [
        C1.Win.Localization.C1Description(typeof(Strings), "C1QRCode.ErrorCorrectionLevel", "Gets or sets the error correction level used to create the QR code."),
        DefaultValue(ErrorCorrectionLevel.L)
        ]
        public ErrorCorrectionLevel ErrorCorrectionLevel
        {
            get { return _ecl; }
            set
            {
                if (value != _ecl)
                {
                    _ecl = value;
                    SetDirty();
                }
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="C1QRCode.Encoding"/> used to translate
        /// the content in the <see cref="Text"/> property into binary values
        /// to be encoded in the QR code.
        /// </summary>
        /// <remarks>
        /// <para>More flexible encodings have lower capacity. The <see cref="C1QRCode"/>
        /// control can encode up to 652 digits, or 395 alphanumeric characters, or
        /// 271 bytes.</para>
        /// <para>The default value for this property is 
        /// <see cref="C1.Win.C1BarCode.Encoding.Automatic"/>, which causes the 
        /// control to automatically select the most compact encoding that can be 
        /// used based on the content specified in the <see cref="Text"/> 
        /// property.</para>
        /// </remarks>
        [
        C1.Win.Localization.C1Description(typeof(Strings), "C1QRCode.Encoding", "Determines how the Text should be encoded (automatic, text, numeric, or bytes)."),
        DefaultValue(Encoding.Automatic)
        ]
        public Encoding Encoding
        {
            get { return _encoding; }
            set 
            {
                if (value != _encoding)
                {
                    _encoding = value;
                    SetDirty();
                }
            }
        }
        /// <summary>
        /// Gets or sets the size, in pixels, of the symbols used to build the QR image.
        /// </summary>
        /// <remarks>
        /// <para>Larger values will result in larger images which consume more space 
        /// but may be easier to for some scanners to read.</para>
        /// <para>The default symbol size is three pixels, which usually represents a 
        /// good compromise between size and readability.</para>
        /// <para>This property must be set to values between 2 and 10.</para>
        /// </remarks>
        [
        C1.Win.Localization.C1Description(typeof(Strings), "C1QRCode.SymbolSize", "Gets or sets the size, in pixels, of the symbols used to build the QR image."),
        DefaultValue(3)
        ]
        public int SymbolSize
        {
            get { return _symbolSize; }
            set 
            {
                if (value < 2 || value > 10)
                {
                    throw new Exception(Strings.Errors.InvalidSymbolSize);
                }
                if (value != _symbolSize)
                {
                    _symbolSize = value;
                    SetDirty();
                }
            }
        }
        /// <summary>
        /// Gets or sets the background color for the control.
        /// </summary>
        /// <remarks>
        /// The background color only applies to the control. The barcode
        /// image that is available through the <see cref="Image"/> 
        /// property always has a transparent background.
        /// </remarks>
        [DefaultValue(typeof(Color), "White")]
        override public Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                if (value != base.BackColor)
                {
                    base.BackColor = value;
                    SetDirty();
                }
            }
        }
        /// <summary>
        /// Gets or sets the foreground color for the control.
        /// </summary>
        /// <remarks>
        /// The foreground color applies to the control and also to the barcode
        /// image that is available through the <see cref="Image"/> 
        /// property.
        /// </remarks>
        [DefaultValue(typeof(Color), "Black")]
        override public Color ForeColor
        {
            get { return base.ForeColor; }
            set
            {
                if (value != base.ForeColor)
                {
                    base.ForeColor = value;
                    SetDirty();
                }
            }
        }
        /// <summary>
        /// Gets or sets how the barcode image is displayed within the control.
        /// </summary>
        /// <remarks>
        /// <para>This property only affects how the barcode is displayed within
        /// the control. It has no effect on the actual barcode image, as returned by
        /// the <see cref="Image"/> property.</para>
        /// <para>In most applications, you will either center or left-align the barcode
        /// image within the control. Although stretching the image is possible, it may
        /// make it unreadable for some barcode readers.</para>
        /// </remarks>
        [
        C1.Win.Localization.C1Description(typeof(Strings), "C1QRCode.SizeMode", "Gets or sets how the barcode image is displayed within the control."),
        DefaultValue(PictureBoxSizeMode.Normal)
        ]
        public PictureBoxSizeMode SizeMode
        {
            get { return _sizeMode; }
            set
            {
                if (_sizeMode != value)
                {
                    _sizeMode = value;
                    Invalidate();
                }
            }
        }
        /// <summary>
        /// Overridden to remove from property window.
        /// </summary>
        [
        Browsable(false),
        EditorBrowsable(EditorBrowsableState.Never)
        ]
        override public Font Font
        {
            get { return base.Font; }
            set { base.Font = value; }
        }
        /// <summary>
        /// Gets the <see cref="Exception"/> that prevented the <see cref="Text"/> from being encoded.
        /// </summary>
        /// <remarks>
        /// <para>The <see cref="C1QRCode"/> control can be used to encode up to
        /// 652 digits, or 395 alphanumeric characters (uppercase only), or 271 bytes.</para>
        /// <para>If these limits are exceeded, or if any of the control properties is set
        /// to values that prevent the content of the <see cref="Text"/> property from
        /// being encoded, the control remains blank, and the <see cref="EncodingException"/>
        /// property contains details that explain why the code could not be generated.</para>
        /// </remarks>
        /// <example>
        /// The code below sets the <see cref="Text"/> property and shows a message in case 
        /// any errors are detected:
        /// <code>
        /// c1QRCode1.Text = textBox1.Text;
        /// label1.Text = c1QRCode1.EncodingException == null 
        ///   ? string.Empty 
        ///   : c1QRCode1.EncodingException.Message;
        /// </code>
        /// </example>
        [Browsable(false)]
        public Exception EncodingException
        {
            get
            {
                Update();
                return _encoder.Exception;
            }
        }
        /// <summary>
        /// Gets or sets the value that is encoded as a barcode image.
        /// </summary>
        [
        C1.Win.Localization.C1Description(typeof(Strings), "C1QRCode.Text", "Gets or sets the value that is encoded as a barcode image."),
        DefaultValue("")
        ]
        override public string Text
        {
            get { return base.Text; }
            set
            {
                if (base.Text != value)
                {
                    base.Text = value;
                    SetDirty();
                }
            }
        }
        /// <summary>
        /// Gets an image of the barcode that represents the value in the <see cref="Text"/> 
        /// property.
        /// </summary>
        /// <remarks>
        /// <para>The <b>Image</b> property returns a <see cref="Metafile"/> that can be 
        /// inserted in reports and other documents. If the value stored in the <see cref="Text"/>
        /// property is invalid for the current encoding, the image returned is blank.</para>
        /// <para>Although the barcode image returned by the control is scalable, the default
        /// size is optimal for common barcode readers. Keep in mind that if you stretch
        /// the image and make it very large or very small, it may become unreadable to
        /// some readers.</para>
        /// <para>The metafile image has a transparent background and therefore cannot be 
        /// easily converted to a solid-background bitmap. To obtain a <see cref="Bitmap"/> 
        /// image of the barcode, use the <see cref="GetImage(ImageFormat)"/> method instead.</para>
        /// </remarks>
        [
        C1.Win.Localization.C1Description(typeof(Strings), "C1QRCode.Image", "Gets an image of the barcode that represents the value in the Text property."),
        Browsable(false)
        ]
        public Image Image
        {
            get
            {
                if (_dirty)
                {
                    UpdateImage();
                    _dirty = false;
                }
                return _img;
            }
        }
        /// <summary>
        /// Gets an image of the QR code that represents the value in the <see cref="Text"/> 
        /// property.
        /// </summary>
        /// <param name="format">An <see cref="ImageFormat"/> value that specifies the type of image to be created.</param>
        /// <returns>An image of the barcode with the specified format.</returns>
        /// <remarks>
        /// The <see cref="Image"/> property can also be used to obtain an image of the barcode. However,
        /// it always returns a <see cref="Metafile"/> image of a pre-set size and with a transparent 
        /// background. This method returns solid-background bitmaps.
        /// </remarks>
        public Image GetImage(ImageFormat format)
        {
            Image emf = this.Image;
            return emf == null 
                ? null 
                : GetImage(format, emf.Width, emf.Height);
        }
        /// <summary>
        /// Gets an image of the QR code that represents the value in the <see cref="Text"/> 
        /// property.
        /// </summary>
        /// <param name="format">An <see cref="ImageFormat"/> value that specifies the type of image to be created.</param>
        /// <param name="width">The width of the image, in pixels.</param>
        /// <param name="height">The height of the image, in pixels.</param>
        /// <returns>An image of the barcode with the specified format and dimensions.</returns>
        /// <remarks>
        /// The <see cref="Image"/> property can also be used to obtain an image of the barcode. However,
        /// it always returns a <see cref="Metafile"/> image of a pre-set size and with a transparent 
        /// background. This method returns solid-background bitmaps.
        /// </remarks>
        public Image GetImage(ImageFormat format, int width, int height)
        {
            // get internal image (metafile)
            Image emf = this.Image;

            // return metafile (or null)
            if (emf == null || format.Equals(ImageFormat.Emf) || format.Equals(ImageFormat.Wmf))
            {
                return emf;
            }

            // or bitmap
            var bmp = new Bitmap(width, height);
            var rc = new Rectangle(0, 0, width, height);
            using (var g = Graphics.FromImage(bmp))
            using (var br = new SolidBrush(BackColor))
            {
                g.FillRectangle(br, rc);
                g.DrawImage(emf, rc);
            }
            return bmp;
        }

        #endregion

        //-----------------------------------------------------------------------
        #region ** overrides

        /// <summary>
        /// Raises the Paint event.
        /// </summary>
        /// <param name="e">A PaintEventArgs that contains the event data.</param>
        override protected void OnPaint(PaintEventArgs e)
        {
            // get image
            Image img = Image;
            if (img == null || img.Width < 1 || img.Height < 1)
            {
                return;
            }

            // get client rectangle
            Rectangle rc = ClientRectangle;
            if (rc.Width < 1 || rc.Height < 1) return;

            // draw image
            Graphics g = e.Graphics;
            switch (_sizeMode)
            {
                case PictureBoxSizeMode.AutoSize:
                    if (ClientSize != img.Size)
                    {
                        ClientSize = img.Size;
                    }
                    goto case PictureBoxSizeMode.Normal;

                case PictureBoxSizeMode.Normal:
                    g.DrawImageUnscaled(img, 0, 0);
                    break;

                case PictureBoxSizeMode.StretchImage:
                    g.DrawImage(img, rc);
                    break;

                case PictureBoxSizeMode.CenterImage:
                    int x = (rc.Width - img.Width) / 2;
                    int y = (rc.Height - img.Height) / 2;
                    g.DrawImageUnscaled(img, x, y);
                    break;

                case PictureBoxSizeMode.Zoom: // <<B10>>
                    float zoom = Math.Min(rc.Width / (float)img.Width, rc.Height / (float)img.Height);
                    RectangleF rcRender = new RectangleF(0, 0, img.Width * zoom, img.Height * zoom);
                    rcRender.Offset((rc.Width - rcRender.Width) / 2, (rc.Height - rcRender.Height) / 2);
                    g.DrawImage(img, rcRender);
                    break;
            }

            // fire event as usual
            base.OnPaint(e);
        }

        //-----------------------------------------------------------------------
        #region ** implementation

        void SetDirty()
        {
            _dirty = true;
            Invalidate();
        }
        void UpdateImage()
        {
            // set encoder parameters
            _encoder.ErrorCorrectionLevel = this.ErrorCorrectionLevel;
            _encoder.Encoding = GetEncoding();
            _encoder.CodeVersion = CodeVersion;

            // get bits
            var matrix = _encoder.GetMatrix(Text);

            // create image from bits
            _img = null;
            if (matrix != null)
            {
                // create metafile with specified dimensions
                var rc = new Rectangle(0, 0, (matrix.Length * _symbolSize) + 1, (matrix.Length * _symbolSize) + 1);
                using (Graphics g = this.CreateGraphics())
                {
                    IntPtr dc = g.GetHdc();
                    _img = new Metafile(dc, rc, MetafileFrameUnit.Pixel);
                    g.ReleaseHdc(dc);
                }
                if (_img != null)
                {
                    using (var g = Graphics.FromImage(_img))
                    using (var bBack = new SolidBrush(Color.Transparent))
                    using (var bFore = new SolidBrush(ForeColor))
                    {
                        g.FillRectangle(bBack, new Rectangle(0, 0, rc.Width, rc.Height));
                        for (int i = 0; i < matrix.Length; i++)
                        {
                            for (int j = 0; j < matrix.Length; j++)
                            {
                                if (matrix[j][i])
                                {
                                    g.FillRectangle(bFore, j * _symbolSize, i * _symbolSize, _symbolSize, _symbolSize);
                                }
                            }
                        }
                    }
                }
            }
        }
#if false
        void UpdateImageBitmap()
        {
            // set encoder parameters
            _encoder.ErrorCorrectionLevel = this.ErrorCorrectionLevel;
            _encoder.Encoding = GetEncoding();
            _encoder.CodeVersion = CodeVersion;

            // get bits
            var matrix = _encoder.GetMatrix(Text);

            // create image from bits
            _img = null;
            if (matrix != null)
            {
                _img = new Bitmap((matrix.Length * _symbolSize) + 1, (matrix.Length * _symbolSize) + 1);
                using (var g = Graphics.FromImage(_img))
                using (var bBack = new SolidBrush(Color.Transparent))
                using (var bFore = new SolidBrush(ForeColor))
                {
                    g.FillRectangle(bBack, new Rectangle(0, 0, _img.Width, _img.Height));
                    for (int i = 0; i < matrix.Length; i++)
                    {
                        for (int j = 0; j < matrix.Length; j++)
                        {
                            if (matrix[j][i])
                            {
                                g.FillRectangle(bFore, j * _symbolSize, i * _symbolSize, _symbolSize, _symbolSize);
                            }
                        }
                    }
                }
            }
        }
#endif
        Encoding GetEncoding()
        {
            var enc = this.Encoding;
            if (enc == Win.C1BarCode.Encoding.Automatic)
            {
                if (IsValidEncoding(Encoding.Numeric, Text))
                {
                    enc = Encoding.Numeric;
                }
                else if (IsValidEncoding(Encoding.AlphaNumeric, Text))
                {
                    enc = Encoding.AlphaNumeric;
                }
                else
                {
                    enc = Encoding.Byte;
                }
            }
            return enc;
        }
        static bool IsValidEncoding(Encoding enc, string content)
        {
            return QRCodeEncoder.IsValidEncoding(enc, content);
        }
        #endregion

        #endregion
    }
}
