using System;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing;
using System.Resources;
using System.Globalization;

namespace C1.Win.C1BarCode
{
    //-----------------------------------------------------------------------------
    #region ** enums

    /// <summary>
    /// Specifies the type of encoding to use (more flexible encodings consume more space).
    /// </summary>
#if EXPOSE_BARCODE
    public enum Encoding
#else
    internal enum Encoding
#endif
    {
        /// <summary>
        /// Select encoding automatically based on the content.
        /// </summary>
        Automatic,
        /// <summary>
        /// Encode up to 395 alpha-numeric values. Alpha-numeric values include
        /// digits from 0 to 9, uppercase letters from A to Z, space, 
        /// and the following additional characters: dollar, percentage, 
        /// asterisk, plus, minus, slash, and colon ([0-9][A-Z][$%*+-./:]).
        /// </summary>
        AlphaNumeric,
        /// <summary>
        /// Encode up to 652 numeric values.
        /// </summary>
        Numeric,
        /// <summary>
        /// Encode up to 271 bytes.
        /// </summary>
        Byte
    };
    /// <summary>
    /// Specifies the error-correction level (higher levels consume more space).
    /// </summary>
#if EXPOSE_BARCODE
    public enum ErrorCorrectionLevel
#else
    internal enum ErrorCorrectionLevel
#endif
    {
        /// <summary>
        /// Able to correct up to 7% damage.
        /// </summary>
        L,
        /// <summary>
        /// Able to correct up to 15% damage.
        /// </summary>
        M,
        /// <summary>
        /// Able to correct up to 25% damage.
        /// </summary>
        Q,
        /// <summary>
        /// Able to correct up to 30% damage.
        /// </summary>
        H,
    };
    #endregion

    // QR encoder class.
    // Returns a bit array that must be converted into an image.
    internal class QRCodeEncoder
    {
        //-----------------------------------------------------------------------------
        #region ** fields

        ErrorCorrectionLevel _ecl;
        Encoding _encodeMode;
        int _codeVersion;
        int _appendN;
        int _appendM;
        int _appendParity;
        string _appendOriginaldata;
        Exception _exception = null;

        const string OVERFLOW_MSG = "Overflow (too much data to encode).";

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ctor

        /// <summary>
        /// Constructor
        /// </summary>
        public QRCodeEncoder()
        {
            _ecl = ErrorCorrectionLevel.M;
            _encodeMode = Encoding.Byte;
            _codeVersion = 0;

            _appendN = 0;
            _appendM = 0;
            _appendParity = 0;
            _appendOriginaldata = string.Empty;
        }
        
        #endregion

        //-----------------------------------------------------------------------------
        #region ** object model

        public ErrorCorrectionLevel ErrorCorrectionLevel
        {
            get { return _ecl; }
            set { _ecl = value; }
        }
        public int CodeVersion
        {
            get { return _codeVersion; }
            set
            {
                if (value < 0 || value > 40) 
                {
                    throw new Exception("Invalid CodeVersion value (must be between 0 and 40).");
                }
                _codeVersion = value;
            }
        }
        public Encoding Encoding
        {
            get { return _encodeMode; }
            set { _encodeMode = value; }
        }
        public Exception Exception
        {
            get { return _exception; }
        }
        public bool[][] GetMatrix(string content)
        {
            // sanity
            if (!IsValidEncoding(Encoding, content))
            {
                _exception = new Exception("Invalid Encoding value for the current content.");
                return null;
            }

            // convert content to bytes (use UT8 to handle Unicode/Japanese chars)
            var bytes = System.Text.Encoding.UTF8.GetBytes(content);
            // convert content to bytes (ASCII or Unicode)
            //var encAsc = System.Text.Encoding.ASCII;
            //var bytesAsc = encAsc.GetBytes(content);
            //var encUni = System.Text.Encoding.UTF8; // System.Text.Encoding.Unicode;
            //var bytesUni = encUni.GetBytes(content);
            //var bytes = encAsc.GetString(bytesAsc) == encUni.GetString(bytesUni)
            //    ? bytesAsc 
            //    : bytesUni;

            // convert bytes to matrix
            try
            {
                _exception = null;
                return calQrcode(bytes);
            }
            catch (Exception x)
            {
                _exception = x;
                return null;
            }
        }
        public static bool IsValidEncoding(Encoding enc, string content)
        {
            switch (enc)
            {
                case Encoding.Numeric:
                    for (int i = 0; i < content.Length; i++)
                    {
                        if (!char.IsDigit(content[i]))
                        {
                            return false;
                        }
                    }
                    break;
                case Encoding.AlphaNumeric:
                    var specialChars = " $%*+-./:";
                    for (int i = 0; i < content.Length; i++)
                    {
                        var c = content[i];
                        bool ok = 
                            (c >= '0' && c <= '9') ||
                            (c >= 'A' && c <= 'Z') ||
                            specialChars.IndexOf(c) > -1;
                        if (!ok)
                        {
                            return false;
                        }
                    }
                    break;
            }
            return true;
        }
        #endregion

        //-----------------------------------------------------------------------------
        #region ** implementation

        void setStructureappend(int m, int n, int p)
        {
            if (n > 1 && n <= 16 && m > 0 && m <= 16 && p >= 0 && p <= 255)
            {
                _appendM = m;
                _appendN = n;
                _appendParity = p;
            }
        }
        int calStructureappendParity(sbyte[] originaldata)
        {
            int i = 0;
            int parity = 0;
            int originaldataLength = originaldata.Length;
            if (originaldataLength > 1)
            {
                parity = 0;
                while (i < originaldataLength)
                {
                    parity = (parity ^ (originaldata[i] & 0xFF));
                    i++;
                }
            }
            else
            {
                parity = -1;
            }
            return parity;
        }
        bool[][] calQrcode(byte[] qrcodeData)
        {
            int ctr = 0;
            int len = qrcodeData.Length;

            int[] dataValue = new int[len + 32];
            sbyte[] dataBits = new sbyte[len + 32];

            if (len <= 0)
            {
                bool[][] ret = new bool[][] { new bool[] { false } };
                return ret;
            }

            if (_appendN > 1)
            {
                dataValue[0] = 3;
                dataBits[0] = 4;

                dataValue[1] = _appendM - 1;
                dataBits[1] = 4;

                dataValue[2] = _appendN - 1;
                dataBits[2] = 4;

                dataValue[3] = _appendParity;
                dataBits[3] = 8;

                ctr = 4;
            }
            dataBits[ctr] = 4;

            // determine encode mode
            int[] codewordNumPlus;
            int codewordNumCounterValue;
            switch (_encodeMode)
            {
                case Encoding.AlphaNumeric:
                    codewordNumPlus = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 };
                    dataValue[ctr] = 2;
                    ctr++;
                    dataValue[ctr] = len;
                    dataBits[ctr] = 9;
                    codewordNumCounterValue = ctr;
                    ctr++;
                    for (int i = 0; i < len; i++)
                    {
                        char chr = (char)qrcodeData[i];
                        sbyte chrValue = 0;
                        if (chr >= 48 && chr < 58)
                        {
                            chrValue = (sbyte)(chr - 48);
                        }
                        else
                        {
                            if (chr >= 65 && chr < 91)
                            {
                                chrValue = (sbyte)(chr - 55);
                            }
                            else
                            {
                                if (chr == 32)
                                {
                                    chrValue = 36;
                                }
                                if (chr == 36)
                                {
                                    chrValue = 37;
                                }
                                if (chr == 37)
                                {
                                    chrValue = 38;
                                }
                                if (chr == 42)
                                {
                                    chrValue = 39;
                                }
                                if (chr == 43)
                                {
                                    chrValue = 40;
                                }
                                if (chr == 45)
                                {
                                    chrValue = 41;
                                }
                                if (chr == 46)
                                {
                                    chrValue = 42;
                                }
                                if (chr == 47)
                                {
                                    chrValue = 43;
                                }
                                if (chr == 58)
                                {
                                    chrValue = 44;
                                }
                            }
                        }
                        if (i % 2 == 0)
                        {
                            dataValue[ctr] = chrValue;
                            dataBits[ctr] = 6;
                        }
                        else
                        {
                            dataValue[ctr] = dataValue[ctr] * 45 + chrValue;
                            dataBits[ctr] = 11;
                            if (i < len - 1)
                            {
                                ctr++;
                            }
                        }
                    }
                    ctr++;
                    break;

                case Encoding.Numeric:
                    codewordNumPlus = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 };
                    dataValue[ctr] = 1;
                    ctr++;
                    dataValue[ctr] = len;
                    dataBits[ctr] = 10; /* #version 1-9*/
                    codewordNumCounterValue = ctr;
                    ctr++;
                    for (int i = 0; i < len; i++)
                    {
                        if (i % 3 == 0)
                        {
                            dataValue[ctr] = (int)(qrcodeData[i] - 0x30);
                            dataBits[ctr] = 4;
                        }
                        else
                        {
                            dataValue[ctr] = dataValue[ctr] * 10 + (int)(qrcodeData[i] - 0x30);
                            if (i % 3 == 1)
                            {
                                dataBits[ctr] = 7;
                            }
                            else
                            {
                                dataBits[ctr] = 10;
                                if (i < len - 1)
                                {
                                    ctr++;
                                }
                            }
                        }
                    }
                    ctr++;
                    break;

                // binary
                default:
                    codewordNumPlus = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 };
                    dataValue[ctr] = 4;
                    ctr++;
                    dataValue[ctr] = len;
                    dataBits[ctr] = 8; /* #version 1-9 */
                    codewordNumCounterValue = ctr;
                    ctr++;
                    for (int i = 0; i < len; i++)
                    {
                        dataValue[i + ctr] = (qrcodeData[i] & 0xFF);
                        dataBits[i + ctr] = 8;
                    }
                    ctr += len;
                    break;
            }

            int totalDataBits = 0;
            for (int i = 0; i < ctr; i++)
            {
                totalDataBits += dataBits[i];
            }

            int ec = 0; // M is default
            switch (_ecl)
            {
                case ErrorCorrectionLevel.L:
                    ec = 1;
                    break;
                case ErrorCorrectionLevel.Q:
                    ec = 3;
                    break;
                case ErrorCorrectionLevel.H:
                    ec = 2;
                    break;
            }

            var maxDataBitsArray = new int[][] { new int[] { 0, 128, 224, 352, 512, 688, 864, 992, 1232, 1456, 1728, 2032, 2320, 2672, 2920, 3320, 3624, 4056, 4504, 5016, 5352, 5712, 6256, 6880, 7312, 8000, 8496, 9024, 9544, 10136, 10984, 11640, 12328, 13048, 13800, 14496, 15312, 15936, 16816, 17728, 18672 }, new int[] { 0, 152, 272, 440, 640, 864, 1088, 1248, 1552, 1856, 2192, 2592, 2960, 3424, 3688, 4184, 4712, 5176, 5768, 6360, 6888, 7456, 8048, 8752, 9392, 10208, 10960, 11744, 12248, 13048, 13880, 14744, 15640, 16568, 17528, 18448, 19472, 20528, 21616, 22496, 23648 }, new int[] { 0, 72, 128, 208, 288, 368, 480, 528, 688, 800, 976, 1120, 1264, 1440, 1576, 1784, 2024, 2264, 2504, 2728, 3080, 3248, 3536, 3712, 4112, 4304, 4768, 5024, 5288, 5608, 5960, 6344, 6760, 7208, 7688, 7888, 8432, 8768, 9136, 9776, 10208 }, new int[] { 0, 104, 176, 272, 384, 496, 608, 704, 880, 1056, 1232, 1440, 1648, 1952, 2088, 2360, 2600, 2936, 3176, 3560, 3880, 4096, 4544, 4912, 5312, 5744, 6032, 6464, 6968, 7288, 7880, 8264, 8920, 9368, 9848, 10288, 10832, 11408, 12016, 12656, 13328 } };
            int maxDataBits = 0;
            if (_codeVersion == 0)
            {
                /* 0 means "auto version select" */
                _codeVersion = 1;
                for (int i = 1; i <= 40; i++)
                {
                    if (maxDataBitsArray[ec][i] >= totalDataBits + codewordNumPlus[_codeVersion])
                    {
                        maxDataBits = maxDataBitsArray[ec][i];
                        break;
                    }
                    _codeVersion++;
                    if (_codeVersion > 10)
                    {
                        throw new Exception(OVERFLOW_MSG);
                    }
                }
            }
            else
            {
                maxDataBits = maxDataBitsArray[ec][_codeVersion];
            }
            totalDataBits += codewordNumPlus[_codeVersion];
            dataBits[codewordNumCounterValue] = (sbyte)(dataBits[codewordNumCounterValue] + codewordNumPlus[_codeVersion]);
            var maxCodewordsArray = new int[] { 0, 26, 44, 70, 100, 134, 172, 196, 242, 292, 346, 404, 466, 532, 581, 655, 733, 815, 901, 991, 1085, 1156, 1258, 1364, 1474, 1588, 1706, 1828, 1921, 2051, 2185, 2323, 2465, 2611, 2761, 2876, 3034, 3196, 3362, 3532, 3706 };
            var maxCodewords = maxCodewordsArray[_codeVersion];
            var maxModules1side = 17 + (_codeVersion << 2);
            var matrixRemainBit = new int[] { 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0 };

            // read version ECC data file
            var byte_num = matrixRemainBit[_codeVersion] + (maxCodewords << 3);

            var matrixX = new sbyte[byte_num];
            var matrixY = new sbyte[byte_num];
            var maskArray = new sbyte[byte_num];
            var formatInformationX2 = new sbyte[15];
            var formatInformationY2 = new sbyte[15];
            var rsEccCodewords = new sbyte[1];
            var rsBlockOrderTemp = new sbyte[128];

            var resName = string.Format("qrv{0}_{1}.dat", _codeVersion, ec);
            var bytes = DecompressResourceBytes(resName);
            using (var ms = new MemoryStream(bytes))
            {
                ReadInput(ms, matrixX, 0, matrixX.Length);
                ReadInput(ms, matrixY, 0, matrixY.Length);
                ReadInput(ms, maskArray, 0, maskArray.Length);
                ReadInput(ms, formatInformationX2, 0, formatInformationX2.Length);
                ReadInput(ms, formatInformationY2, 0, formatInformationY2.Length);
                ReadInput(ms, rsEccCodewords, 0, rsEccCodewords.Length);
                ReadInput(ms, rsBlockOrderTemp, 0, rsBlockOrderTemp.Length);
            }

            sbyte rsBlockOrderLength = 1;
            for (int i = 1; i < 128; i++)
            {
                if (rsBlockOrderTemp[i] == 0)
                {
                    rsBlockOrderLength = (sbyte)i;
                    break;
                }
            }
            var rsBlockOrder = new sbyte[rsBlockOrderLength];
            Array.Copy(rsBlockOrderTemp, 0, rsBlockOrder, 0, (byte)rsBlockOrderLength);

            var formatInformationX1 = new sbyte[] { 0, 1, 2, 3, 4, 5, 7, 8, 8, 8, 8, 8, 8, 8, 8 };
            var formatInformationY1 = new sbyte[] { 8, 8, 8, 8, 8, 8, 8, 8, 7, 5, 4, 3, 2, 1, 0 };

            var maxDataCodewords = maxDataBits >> 3;

            // read frame data
            var modules1Side = 4 * _codeVersion + 17;
            var matrixTotalBits = modules1Side * modules1Side;
            var frameData = new sbyte[matrixTotalBits + modules1Side];

            resName = string.Format("qrvfr{0}.dat", _codeVersion);
            bytes = DecompressResourceBytes(resName);
            using (var ms = new MemoryStream(bytes))
            {
                ReadInput(ms, frameData, 0, frameData.Length);
            }

            // set terminator
            if (totalDataBits <= maxDataBits - 4)
            {
                dataValue[ctr] = 0;
                dataBits[ctr] = 4;
            }
            else
            {
                if (totalDataBits < maxDataBits)
                {
                    dataValue[ctr] = 0;
                    dataBits[ctr] = (sbyte)(maxDataBits - totalDataBits);
                }
                else
                {
                    if (totalDataBits > maxDataBits)
                    {
                        throw new Exception(OVERFLOW_MSG);
                    }
                }
            }
            var dataCodewords = divideDataBy8Bits(dataValue, dataBits, maxDataCodewords);
            var codewords = calculateRSECC(dataCodewords, rsEccCodewords[0], rsBlockOrder, maxDataCodewords, maxCodewords);

            // flash matrix
            var matrixContent = new sbyte[modules1Side][];
            for (int i2 = 0; i2 < modules1Side; i2++)
            {
                matrixContent[i2] = new sbyte[modules1Side];
            }
            for (int i = 0; i < modules1Side; i++)
            {
                for (int j = 0; j < modules1Side; j++)
                {
                    matrixContent[j][i] = 0;
                }
            }

            // attach data
            for (int i = 0; i < maxCodewords; i++)
            {
                var codeword_i = codewords[i];
                for (int j = 7; j >= 0; j--)
                {
                    int codewordBitsNumber = (i * 8) + j;
                    matrixContent[matrixX[codewordBitsNumber] & 0xFF][matrixY[codewordBitsNumber] & 0xFF] = (sbyte)((255 * (codeword_i & 1)) ^ maskArray[codewordBitsNumber]);
                    codeword_i = (sbyte)(URShift((codeword_i & 0xFF), 1));
                }
            }
            for (int matrixRemain = matrixRemainBit[_codeVersion]; matrixRemain > 0; matrixRemain--)
            {
                int remainBitTemp = matrixRemain + (maxCodewords * 8) - 1;
                matrixContent[matrixX[remainBitTemp] & 0xFF][matrixY[remainBitTemp] & 0xFF] = (sbyte)(255 ^ maskArray[remainBitTemp]);
            }

            // mask select
            var maskNumber = selectMask(matrixContent, matrixRemainBit[_codeVersion] + maxCodewords * 8);
            var maskContent = (sbyte)(1 << maskNumber);

            // format information
            var formatInformationValue = (sbyte)((byte)ec << 3 | (byte)maskNumber);
            var formatInformationArray = new string[] { "101010000010010", "101000100100101", "101111001111100", "101101101001011", "100010111111001", "100000011001110", "100111110010111", "100101010100000", "111011111000100", "111001011110011", "111110110101010", "111100010011101", "110011000101111", "110001100011000", "110110001000001", "110100101110110", "001011010001001", "001001110111110", "001110011100111", "001100111010000", "000011101100010", "000001001010101", "000110100001100", "000100000111011", "011010101011111", "011000001101000", "011111100110001", "011101000000110", "010010010110100", "010000110000011", "010111011011010", "010101111101101" };
            for (int i = 0; i < 15; i++)
            {
                var content = (sbyte)System.SByte.Parse(formatInformationArray[formatInformationValue].Substring(i, (i + 1) - (i)));
                matrixContent[formatInformationX1[i] & 0xFF][formatInformationY1[i] & 0xFF] = (sbyte)(content * 255);
                matrixContent[formatInformationX2[i] & 0xFF][formatInformationY2[i] & 0xFF] = (sbyte)(content * 255);
            }
            var out_Renamed = new bool[modules1Side][];
            for (int i3 = 0; i3 < modules1Side; i3++)
            {
                out_Renamed[i3] = new bool[modules1Side];
            }
            int c = 0;
            for (int i = 0; i < modules1Side; i++)
            {
                for (int j = 0; j < modules1Side; j++)
                {
                    if ((matrixContent[j][i] & maskContent) != 0 || frameData[c] == (char)49)
                    {
                        out_Renamed[j][i] = true;
                    }
                    else
                    {
                        out_Renamed[j][i] = false;
                    }
                    c++;
                }
                c++;
            }
            return out_Renamed;
        }
        static sbyte[] divideDataBy8Bits(int[] data, sbyte[] bits, int maxDataCodewords)
        {
            // divide Data By 8bit and add padding char
            int l1 = bits.Length;
            int l2;
            int ctr = 0;
            int remainingBits = 8;
            int max = 0;
            int buffer;
            int bufferBits;
            bool flag;

            if (l1 != data.Length)
            {
                // REVIEW: NO-OP? throw?
            }
            for (int i = 0; i < l1; i++)
            {
                max += bits[i];
            }
            l2 = (max - 1) / 8 + 1;
            var codewords = new sbyte[maxDataCodewords];
            for (int i = 0; i < l2; i++)
            {
                codewords[i] = 0;
            }
            for (int i = 0; i < l1; i++)
            {
                buffer = data[i];
                bufferBits = bits[i];
                flag = true;
                if (bufferBits == 0)
                {
                    break;
                }
                while (flag)
                {
                    if (remainingBits > bufferBits)
                    {
                        codewords[ctr] = (sbyte)((codewords[ctr] << bufferBits) | buffer);
                        remainingBits -= bufferBits;
                        flag = false;
                    }
                    else
                    {
                        bufferBits -= remainingBits;
                        codewords[ctr] = (sbyte)((codewords[ctr] << remainingBits) | (buffer >> bufferBits));

                        if (bufferBits == 0)
                        {
                            flag = false;
                        }
                        else
                        {
                            buffer = (buffer & ((1 << bufferBits) - 1));
                            flag = true;
                        }
                        ctr++;
                        remainingBits = 8;
                    }
                }
            }
            if (remainingBits != 8)
            {
                codewords[ctr] = (sbyte)(codewords[ctr] << remainingBits);
            }
            else
            {
                ctr--;
            }
            if (ctr < maxDataCodewords - 1)
            {
                flag = true;
                while (ctr < maxDataCodewords - 1)
                {
                    ctr++;
                    codewords[ctr] = flag
                        ? (sbyte)-20
                        : (sbyte)+17;
                    flag = !flag;
                }
            }
            return codewords;
        }
        static sbyte[] calculateRSECC(sbyte[] codewords, sbyte rsEccCodewords, sbyte[] rsBlockOrder, int maxDataCodewords, int maxCodewords)
        {
            var rsCalTableArray = new sbyte[256][];
            for (int i = 0; i < 256; i++)
            {
                rsCalTableArray[i] = new sbyte[rsEccCodewords];
            }
            var resName = string.Format("rsc{0}.dat", rsEccCodewords);
            var bytes = DecompressResourceBytes(resName);
            using (var ms = new MemoryStream(bytes))
            {
                for (int i = 0; i < 256; i++)
                {
                    ReadInput(ms, rsCalTableArray[i], 0, rsCalTableArray[i].Length);
                }
            }

            // RS-ECC prepare
            int i2 = 0;
            int j = 0;
            int rsBlockNumber = 0;

            sbyte[][] rsTemp = new sbyte[rsBlockOrder.Length][];
            sbyte[] res = new sbyte[maxCodewords];
            Array.Copy(codewords, 0, res, 0, codewords.Length);

            i2 = 0;
            while (i2 < rsBlockOrder.Length)
            {
                rsTemp[i2] = new sbyte[(rsBlockOrder[i2] & 0xFF) - rsEccCodewords];
                i2++;
            }
            i2 = 0;
            while (i2 < maxDataCodewords)
            {
                rsTemp[rsBlockNumber][j] = codewords[i2];
                j++;
                if (j >= (rsBlockOrder[rsBlockNumber] & 0xFF) - rsEccCodewords)
                {
                    j = 0;
                    rsBlockNumber++;
                }
                i2++;
            }

            // RS-ECC main
            rsBlockNumber = 0;
            while (rsBlockNumber < rsBlockOrder.Length)
            {
                sbyte[] rsTempData;
                rsTempData = new sbyte[rsTemp[rsBlockNumber].Length];
                rsTemp[rsBlockNumber].CopyTo(rsTempData, 0);

                int rsCodewords = (rsBlockOrder[rsBlockNumber] & 0xFF);
                int rsDataCodewords = rsCodewords - rsEccCodewords;

                j = rsDataCodewords;
                while (j > 0)
                {
                    sbyte first = rsTempData[0];
                    if (first != 0)
                    {
                        sbyte[] leftChr = new sbyte[rsTempData.Length - 1];
                        Array.Copy(rsTempData, 1, leftChr, 0, rsTempData.Length - 1);
                        sbyte[] cal = rsCalTableArray[(first & 0xFF)];
                        rsTempData = calByteArrayBits(leftChr, cal, "xor");
                    }
                    else
                    {
                        if (rsEccCodewords < rsTempData.Length)
                        {
                            sbyte[] rsTempNew = new sbyte[rsTempData.Length - 1];
                            Array.Copy(rsTempData, 1, rsTempNew, 0, rsTempData.Length - 1);
                            rsTempData = new sbyte[rsTempNew.Length];
                            rsTempNew.CopyTo(rsTempData, 0);
                        }
                        else
                        {
                            sbyte[] rsTempNew = new sbyte[rsEccCodewords];
                            Array.Copy(rsTempData, 1, rsTempNew, 0, rsTempData.Length - 1);
                            rsTempNew[rsEccCodewords - 1] = 0;
                            rsTempData = new sbyte[rsTempNew.Length];
                            rsTempNew.CopyTo(rsTempData, 0);
                        }
                    }
                    j--;
                }

                Array.Copy(rsTempData, 0, res, codewords.Length + rsBlockNumber * rsEccCodewords, (byte)rsEccCodewords);
                rsBlockNumber++;
            }
            return res;
        }
        static sbyte[] calByteArrayBits(sbyte[] xa, sbyte[] xb, string ind)
        {
            int ll;
            int ls;
            sbyte[] res;
            sbyte[] xl;
            sbyte[] xs;

            if (xa.Length > xb.Length)
            {
                xl = new sbyte[xa.Length];
                xa.CopyTo(xl, 0);
                xs = new sbyte[xb.Length];
                xb.CopyTo(xs, 0);
            }
            else
            {
                xl = new sbyte[xb.Length];
                xb.CopyTo(xl, 0);
                xs = new sbyte[xa.Length];
                xa.CopyTo(xs, 0);
            }
            ll = xl.Length;
            ls = xs.Length;
            res = new sbyte[ll];

            for (int i = 0; i < ll; i++)
            {
                if (i < ls)
                {
                    if ((System.Object)ind == (System.Object)"xor")
                    {
                        res[i] = (sbyte)(xl[i] ^ xs[i]);
                    }
                    else
                    {
                        res[i] = (sbyte)(xl[i] | xs[i]);
                    }
                }
                else
                {
                    res[i] = xl[i];
                }
            }
            return res;
        }
        static sbyte selectMask(sbyte[][] matrixContent, int maxCodewordsBitWithRemain)
        {
            int l = matrixContent.Length;
            int[] d1 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            int[] d2 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            int[] d3 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            int[] d4 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };

            int d2And = 0;
            int d2Or = 0;
            int[] d4Counter = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };

            for (int y = 0; y < l; y++)
            {
                int[] xData = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
                int[] yData = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
                bool[] xD1Flag = new bool[] { false, false, false, false, false, false, false, false };
                bool[] yD1Flag = new bool[] { false, false, false, false, false, false, false, false };

                for (int x = 0; x < l; x++)
                {
                    if (x > 0 && y > 0)
                    {
                        d2And = matrixContent[x][y] & matrixContent[x - 1][y] & matrixContent[x][y - 1] & matrixContent[x - 1][y - 1] & 0xFF;
                        d2Or = (matrixContent[x][y] & 0xFF) | (matrixContent[x - 1][y] & 0xFF) | (matrixContent[x][y - 1] & 0xFF) | (matrixContent[x - 1][y - 1] & 0xFF);
                    }
                    for (int maskNumber = 0; maskNumber < 8; maskNumber++)
                    {
                        xData[maskNumber] = ((xData[maskNumber] & 63) << 1) | ((URShift((matrixContent[x][y] & 0xFF), maskNumber)) & 1);
                        yData[maskNumber] = ((yData[maskNumber] & 63) << 1) | ((URShift((matrixContent[y][x] & 0xFF), maskNumber)) & 1);
                        if ((matrixContent[x][y] & (1 << maskNumber)) != 0)
                        {
                            d4Counter[maskNumber]++;
                        }
                        if (xData[maskNumber] == 93)
                        {
                            d3[maskNumber] += 40;
                        }
                        if (yData[maskNumber] == 93)
                        {
                            d3[maskNumber] += 40;
                        }
                        if (x > 0 && y > 0)
                        {
                            if (((d2And & 1) != 0) || ((d2Or & 1) == 0))
                            {
                                d2[maskNumber] += 3;
                            }
                            d2And = d2And >> 1;
                            d2Or = d2Or >> 1;
                        }
                        if (((xData[maskNumber] & 0x1F) == 0) || ((xData[maskNumber] & 0x1F) == 0x1F))
                        {
                            if (x > 3)
                            {
                                if (xD1Flag[maskNumber])
                                {
                                    d1[maskNumber]++;
                                }
                                else
                                {
                                    d1[maskNumber] += 3;
                                    xD1Flag[maskNumber] = true;
                                }
                            }
                        }
                        else
                        {
                            xD1Flag[maskNumber] = false;
                        }
                        if (((yData[maskNumber] & 0x1F) == 0) || ((yData[maskNumber] & 0x1F) == 0x1F))
                        {
                            if (x > 3)
                            {
                                if (yD1Flag[maskNumber])
                                {
                                    d1[maskNumber]++;
                                }
                                else
                                {
                                    d1[maskNumber] += 3;
                                    yD1Flag[maskNumber] = true;
                                }
                            }
                        }
                        else
                        {
                            yD1Flag[maskNumber] = false;
                        }
                    }
                }
            }
            int minValue = 0;
            sbyte res = 0;
            int[] d4Value = new int[] { 90, 80, 70, 60, 50, 40, 30, 20, 10, 0, 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 90 };
            for (int maskNumber = 0; maskNumber < 8; maskNumber++)
            {
                d4[maskNumber] = d4Value[(int)((20 * d4Counter[maskNumber]) / maxCodewordsBitWithRemain)];
                int demerit = d1[maskNumber] + d2[maskNumber] + d3[maskNumber] + d4[maskNumber];
                if (demerit < minValue || maskNumber == 0)
                {
                    res = (sbyte)maskNumber;
                    minValue = demerit;
                }
            }
            return res;
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** utilities
        
        static int ReadInput(Stream stream, sbyte[] target, int start, int count)
        {
            var buff = new byte[target.Length];
            var read = stream.Read(buff, 0, buff.Length);
            for (int i = 0; i < read; i++)
            {
                target[i] = (sbyte)buff[i];
            }
            return read;
        }
        static int URShift(int number, int bits)
        {
            return number >= 0
                ? number >> bits
                : (number >> bits) + (2 << ~bits);
        }
        static byte[] DecompressResourceBytes(string resName)
        {
            //Console.WriteLine(resName);

            // get deflated resource name
            var dft = Path.ChangeExtension(resName, ".dft");

            // get resource
            var asm = Assembly.GetExecutingAssembly();
            foreach (var n in asm.GetManifestResourceNames())
            {
                if (n.EndsWith(dft))
                {
                    // found resource, decompress into memory stream
                    using (var sin = asm.GetManifestResourceStream(n))
                    using (var sut = new MemoryStream())
                    using (var ds = new DeflateStream(sin, CompressionMode.Decompress))
                    {
                        // this is nice, but not available in .NET 2.x
                        //ds.CopyTo(sut);

                        // so copy by brute force
                        var buff = new byte[1024];
                        for (; ; )
                        {
                            int read = ds.Read(buff, 0, buff.Length);
                            if (read == 0)
                            {
                                break;
                            }
                            sut.Write(buff, 0, read);
                        }
                        sut.Flush();
                        return sut.ToArray();
                    }
                }
            }
            throw new Exception("Resource not found: " + resName);
        }

        #endregion
    }
}
