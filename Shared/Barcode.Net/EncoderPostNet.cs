//----------------------------------------------------------------------------
// EncoderPostNet.cs
//
// These are the nifty barcodes we see on the bottoms of envelopes and around
// the addresses on bulk mail. The theory is that these allow the postal
// service to deliver mail in 72 hours instead of 3 days. These codes are 
// actually binary coded with an even number of tall bars guaranteed by the 
// last bar for each character (parity bit, if you will, so we have even 
// parity). I've been told that this is really just a code 2 of 5 scheme 
// that happens to look like a BCD/parity code. This code differs from the 
// others in that it is not based on the width, but rather the height of the
// barcodes. It is also more anal about the spacing of the bars; there are 
// supposed to be 22 bars to the inch. The tall bars are about 1/10 of an 
// inch high, and the short bars are about half that height at 1/20 of an 
// inch. 
//
// Copyright (C) 2004 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2004	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Text;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderPostNET
	/// </summary>
	internal class EncoderPostNet : EncoderBase
	{
        // ** ctor
        internal EncoderPostNet(C1BarCode ctl) : base(ctl) { }

        // ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
			// initialize
			_cursor = new Point(0,0);
			_totalWidth = 0;

			// filter string and add check digits <<B2>>
			text = EncodeText(text);

			// build image only if there's valid content <<B3>>
			if (text.Length > 0)
			{
				// start frame (binary 1) <<B2>>
				DrawPattern(g, br, "W");

				// draw each character in the message
				for (int i = 0; i < text.Length; i++)
					DrawPattern(g, br, RetrievePattern(text[i]));

				// end frame (binary 1) <<B2>>
				DrawPattern(g, br, "W");
			}

			// return total width
			return _totalWidth;
		}

		// ** protected

		// see http://www.barcodeisland.com/postnet.phtml for details
		protected virtual string EncodeText(string text) // <<B2>>
		{
			// calculate check digit
			int digitSum = 0;
			for (int i = 0; i < text.Length; i++)
			{
				// add all digits
				if (text[i] > '0' && text[i] <= '9')
					digitSum += (int)text[i] - (int)'0';
			}
			int check = (10 - (digitSum % 10)) % 10; // <<B12>>

			// rebuild string removing non-digits
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < text.Length; i++)
			{
				if (text[i] >= '0' && text[i] <= '9') // <<B6>>
					sb.Append(text[i]);
			}

			// check that we have some content
			if (sb.Length == 0)
				return string.Empty;

			// add the check digit
			sb.Append(check);

			// done
			return sb.ToString();
		}
		protected virtual string RetrievePattern(char c)
		{
			switch (c)
			{
				case '0':	return "NWwnn";
				case '1':	return "nwwNN";
				case '2':	return "nwWnN";
				case '3':	return "nwWNn";
				case '4':	return "nWwnN";
				case '5':	return "nWwNn";
				case '6':	return "nWWnn";
				case '7':	return "NwwnN";
				case '8':	return "NwwNn";
				case '9':	return "NwWnn";
			}
            ThrowInvalidCharacterException("PostNet", c);
            return null;
        }

		// ** private
		private void DrawPattern(Graphics g, Brush br, string pattern)
		{
			int space = _barWidthWide + 2 * _barWidthNarrow;

			// skip invalid patterns
			if (pattern.Length != 5 && pattern.Length != 1)
			{
				_cursor.X += 5 * space;
				_totalWidth += 5 * space;
				return;
			}

			// draw the whole pattern
			for (int i = 0; i < pattern.Length; i++)
			{
				// get bar top
				int top = (pattern[i] == 'N' || pattern[i] == 'W')
					? 0
					: (6 * _barHeight) / 10;

				// get bar width
				int wid = (pattern[i] == 'N' || pattern[i] == 'n')
					? _barWidthNarrow
					: _barWidthWide;

				// draw this bar
				if (g != null)
					g.FillRectangle(br, _cursor.X + (space - wid)/2, _cursor.Y + top, wid, _barHeight - top);

				// advance the starting position
				_cursor.X += space;
				_totalWidth += space;
			}
		}
	}
}
