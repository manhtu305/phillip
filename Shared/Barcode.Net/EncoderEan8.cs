//----------------------------------------------------------------------------
// EncoderEan8.cs
//
// Copyright (C) 2005 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2005	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderEan8
	/// </summary>
	internal class EncoderEan8 : EncoderEan13
	{
        // ** ctor
        internal EncoderEan8(C1BarCode ctl) : base(ctl) { }

		// ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
			// initialize
			_cursor = new Point(0,0);
			_totalWidth = 0;

			// sanity
			if (BadText(text))
				throw new ArgumentException("Ean8 requires exactly 7 digits.");

			// calculate the check digit
			char checkDigit = GetCheckDigit(text);

            // draw quiet zone <<B13>>
            DrawPattern(g, br, "000000000");
            
            // draw left guards
			DrawPattern(g, br, "101");

			// first four digits, left-hand odd parity
			DrawDigit(g, br, text[0], Parity.LeftOdd);
			DrawDigit(g, br, text[1], Parity.LeftOdd);
			DrawDigit(g, br, text[2], Parity.LeftOdd);
			DrawDigit(g, br, text[3], Parity.LeftOdd);

			// draw center guards
			DrawPattern(g, br, "01010");

			// last three digits, right hand
			DrawDigit(g, br, text[4], Parity.Right);
			DrawDigit(g, br, text[5], Parity.Right);
			DrawDigit(g, br, text[6], Parity.Right);

			// check digit, right hand
			DrawDigit(g, br, checkDigit, Parity.Right);

			// right guard
			DrawPattern(g, br, "101");

            // quiet zone <<B13>>
            DrawPattern(g, br, "000000000");

			// return total width
			return _totalWidth;
		}
		override protected void DrawText(Graphics g, string text, Font font, Brush br, Rectangle rc)
		{
			// skip invalid stuff
			if (BadText(text))
			{
				DrawText(g, text, font, br, rc);
				return;
			}

			// draw left part (x = 12, w = 24 / 77)
			string digits = text.Substring(0, 4);
			Rectangle r = rc;
            r.X = rc.Width * 12 / 77;
            r.Width = rc.Width * 24 / 77;
            DrawTextBase(g, digits, font, br, r);

			// draw right part (x = 41, w = 24 / 77)
            digits = text.Substring(4) + GetCheckDigit(text);
            r.X = rc.Width * 41 / 77;
            DrawTextBase(g, digits, font, br, r);
		}

		// ** private
		private bool BadText(string text)
		{
			if (text.Length != 7) return true;
			foreach (char c in text)
			{
				if (!char.IsDigit(c))
					return true;
			}
			return false;
		}
	}
}
