//----------------------------------------------------------------------------
// Encoder39.cs
//
// ** from Neil Van Eps:
//
// Code 39 was the first alphanumeric symbology developed, and is widely used 
// in industrial settings. Code 39 has two different element widths, wide and 
// narrow, which are usually specified by giving the narrow width and the 
// narrow/wide ratio.
//
// Each Code 39 character has five bars and four spaces for a total of nine 
// elements. Of the nine elements, three are wide and six are narrow, leading 
// to the name Code 39 (3 of 9). Each character is followed by an inter-
// character gap, usually equal to the width of a narrow element.
//
// A Code 39 message begins and ends with an asterisk, which serves as a 
// start/stop code for this symbology.
//
// ** from barcode1:
// 
// Code 39 is an alphanumeric bar code. The symbol can be as long as 
// necessary to store the encoded data. It is designed to encode 26 
// uppercase letters, 10 digits and 7 special characters. It can be 
// extended to code all 128 ASCII characters by using a two character 
// coding scheme. 
// 
// Each data character encoded in a Code 39 symbol is made up of 5 bars 
// and 4 spaces for a total of 9 elements. Each bar or space is either 
// "wide" or "narrow" and 3 out of the 9 elements are always wide. 
// That's what gave the code its other name - Code 3 of 9. 
// The symbol includes a quiet zone (10 x-dimensions or 0.10 inches 
// which every is greater), the start character "*", the encoded data, 
// the stop character "*", and a trailing quiet zone (10 x-dimensions 
// or 0.10 inches whichever is greater). The asterisk is only used as 
// a start and stop code. 
// 
// The X-dimension is the width of the smallest element in a bar code 
// symbol. The minimum X-dimension for an "open system" (a bar code label 
// that will be read by scanners from outside your company) is 7.5 mils 
// (a mil is 1/1000 inch) or 0.19 mm. The "wide" element is a multiple 
// of the "narrow" element and this multiple must remain the same 
// throughout the symbol. This multiple can range between 2.0 and 3.0 
// if the narrow element is greater than 20 mils. If the narrow element 
// is less than 20 mils, the multiple can only range between 2.0 and 2.2. 
// The height of the bars must be at least .15 times the symbol's length 
// or .25 inches, whichever is larger.
// 
// Note that the maximum value (based on the Code 39 specification) for I 
// is 5.3X for X less than 10 mils. If X is 10 mils or greater, the value 
// of I is 3X or 53 mils, whichever is greater. However, for good quality 
// printers, I often equals X. I, strictly speaking, equals X-t where t 
// is the print tolerance in mils. If you do not know the actual value 
// for t, you can calculate the length using the maximum value for I and 
// calculate the minimum value setting I=X. 
//
// Copyright (C) 2004 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2004	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// Encoder39
	/// </summary>
	internal class Encoder39 : EncoderBase
	{
        // ** ctor
        internal Encoder39(C1BarCode ctl) : base(ctl) { }

        // ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
			// initialize
			_cursor = new Point(0,0);
			_totalWidth = 0;

			// draw start character, an asterisk
			DrawPattern(g, br, RetrievePattern('*'));
	
			// draw each character in the message
			for (int i = 0; i < text.Length; i++)
				DrawPattern(g, br, RetrievePattern(text[i]));

			// draw stop character, also an asterisk
			DrawPattern(g, br, RetrievePattern('*'));

			// return total width
			return _totalWidth;
		}

		// ** private
		private void DrawPattern(Graphics g, Brush br, string pattern)
		{
			// draw the whole pattern
			for (int i = 0; i < pattern.Length; i++)
			{
				// decide if narrow or wide bar
				int wid = (pattern[i] == 'n')
					? _barWidthNarrow
					: _barWidthWide;

				// draw this bar
				if (i % 2 == 0 && g != null)
					g.FillRectangle(br, _cursor.X, _cursor.Y, wid, _barHeight);

				// advance the starting position
				_cursor.X += wid;
				_totalWidth += wid;
			}
		}
		private string RetrievePattern(string s)
		{
			StringBuilder sb = new StringBuilder();
			foreach (char c in s)
				sb.Append(RetrievePattern(c));
			return sb.ToString();
		}
		private string RetrievePattern(char c)
		{
            // #c1spell(off)
			switch (c)
			{
				// regular Code 39
				case '1':	return "wnnwnnnnwn";
				case '2':	return "nnwwnnnnwn";
				case '3':	return "wnwwnnnnnn";
				case '4':	return "nnnwwnnnwn";
				case '5':	return "wnnwwnnnnn";
				case '6':	return "nnwwwnnnnn";
				case '7':	return "nnnwnnwnwn";
				case '8':	return "wnnwnnwnnn";
				case '9':	return "nnwwnnwnnn";
				case '0':	return "nnnwwnwnnn";
				case 'A':	return "wnnnnwnnwn";
				case 'B':	return "nnwnnwnnwn";
				case 'C':	return "wnwnnwnnnn";
				case 'D':	return "nnnnwwnnwn";
				case 'E':	return "wnnnwwnnnn";
				case 'F':	return "nnwnwwnnnn";
				case 'G':	return "nnnnnwwnwn";
				case 'H':	return "wnnnnwwnnn";
				case 'I':	return "nnwnnwwnnn";
				case 'J':	return "nnnnwwwnnn";
				case 'K':	return "wnnnnnnwwn";
				case 'L':	return "nnwnnnnwwn";
				case 'M':	return "wnwnnnnwnn";
				case 'N':	return "nnnnwnnwwn";
				case 'O':	return "wnnnwnnwnn";
				case 'P':	return "nnwnwnnwnn";
				case 'Q':	return "nnnnnnwwwn";
				case 'R':	return "wnnnnnwwnn";
				case 'S':	return "nnwnnnwwnn";
				case 'T':	return "nnnnwnwwnn";
				case 'U':	return "wwnnnnnnwn";
				case 'V':	return "nwwnnnnnwn";
				case 'W':	return "wwwnnnnnnn";
				case 'X':	return "nwnnwnnnwn";
				case 'Y':	return "wwnnwnnnnn";
				case 'Z':	return "nwwnwnnnnn";
				case '-':	return "nwnnnnwnwn";
				case '.':	return "wwnnnnwnnn";
				case ' ':	return "nwwnnnwnnn";
				case '*':	return "nwnnwnwnnn";
				case '$':	return "nwnwnwnnnn";
				case '/':	return "nwnwnnnwnn";
				case '+':	return "nwnnnwnwnn";
				case '%':	return "nnnwnwnwnn";

				// extended code39
				case '!':	return RetrievePattern("/A");
				case '"':	return RetrievePattern("/B");
				case '#':	return RetrievePattern("/C");
				case '&':	return RetrievePattern("/F");
				case '\'':	return RetrievePattern("/G");
				case '(':	return RetrievePattern("/H");
				case ')':	return RetrievePattern("/I");
				case ',':	return RetrievePattern("/L");
				case ':':	return RetrievePattern("/Z");
				case ';':	return RetrievePattern("%F");
				case '<':	return RetrievePattern("%G");
				case '=':	return RetrievePattern("%H");
				case '>':	return RetrievePattern("%I");
				case '?':	return RetrievePattern("%J");
				case '@':	return RetrievePattern("%V");
				case '[':	return RetrievePattern("%K");
				case '\\':	return RetrievePattern("%L");
				case ']':	return RetrievePattern("%M");
				case '^':	return RetrievePattern("%N");
				case '_':	return RetrievePattern("%O");
				case '`':	return RetrievePattern("%W");
				case '{':	return RetrievePattern("%P");
				case '|':	return RetrievePattern("%Q");
				case '}':	return RetrievePattern("%R");
				case '~':	return RetrievePattern("%S");
			}
            // #c1spell(on)

			// lowercase characters <<B151>>
            if (c >= 'a' && c <= 'z')
            {
                return RetrievePattern('+') + RetrievePattern(char.ToUpper(c));
            }

			// no dice
            ThrowInvalidCharacterException("Code39", (char)c);
            return null;
        }
    }
}
