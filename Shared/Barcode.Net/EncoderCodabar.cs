//----------------------------------------------------------------------------
// EncoderCodabar.cs
//
// ** from Neil Van Eps:
//
// Codabar is used today in blood bank, library, and certain express air 
// parcel applications. Codabar has two different element widths, wide and 
// narrow, which are usually specified by giving the narrow width and the 
// narrow/wide ratio. Each Codabar character has four bars separated by three 
// spaces for a total of seven elements.
//
// Because of the symbology structure, there are two different character 
// widths. Each character is followed by an inter-character gap, generally 
// the width of a narrow element.
//
// A Codabar message begins/ends with an a start/stop character (A-D). 
// Additional data can be encoded by the choice of start/stop characters.
//
// ** from barcode1:
//
// Codabar was developed was developed in 1972 by Pitney Bowes, Inc. It 
// is a discrete, self-checking symbology that may encode 16 different 
// characters, plus an additional 4 start/stop characters. This symbology 
// is used by U.S. blood banks, photo labs, and on FedEx air bills. 
//
// ** from http://www.mecsw.com/specs/codabar.html
//
// Codabar can encode the digits 0 through 9, six symbols (-:.$/+), and the 
// start/stop characters A, B, C, D, E, *, N, or T. The start/stop characters 
// must be used in matching pairs and may not appear elsewhere in the barcode. 
// Codabar is used in libraries, blood banks, the overnight package delivery 
// industry, and a variety of other information processing applications.
//
// There is no checksum defined as part of the Codabar standard. 
//
// Copyright (C) 2004 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2004	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderCodabar
	/// </summary>
	internal class EncoderCodabar : EncoderBase
	{
        // ** ctor
        internal EncoderCodabar(C1BarCode ctl) : base(ctl) { }

		// ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
			// initialize
			_cursor = new Point(0,0);
			_totalWidth = 0;

			// note: to be readable, the text must start with (A-D) and end with (A-D) <<B7>>
			string abcd = "ABCD";
			text = text.ToUpper();
			if (text.Length < 1 || abcd.IndexOf(text[0]) < 0)
				text = "A" + text;
			if (text.Length < 2 || abcd.IndexOf(text[text.Length-1]) < 0)
				text = text + "D";

			// draw each character in the message
			for (int i = 0; i < text.Length; i++)
				DrawPattern(g, br, RetrievePattern(text[i]));

			// return total width
			return _totalWidth;
		}

		// ** private
		private void DrawPattern(Graphics g, Brush br, string pattern)
		{
			// initialize X pixel value
			for (int i = 0; i < pattern.Length; i++)
			{
				// decide if narrow or wide bar
				int wid = (pattern[i] == 'n')
					? _barWidthNarrow
					: _barWidthWide;

				// draw this bar
				if (i % 2 == 0 && g != null)
					g.FillRectangle(br, _cursor.X, _cursor.Y, wid, _barHeight);

				// advance the starting position
				_cursor.X += wid;
				_totalWidth += wid;
			}
		}
		private string RetrievePattern(char c)
		{
            // #c1spell(off)
            switch (c)
			{
				case '0':	return "nnnnnwwn";
				case '1':	return "nnnnwwnn";
				case '2':	return "nnnwnnwn";
				case '3':	return "wwnnnnnn";
				case '4':	return "nnwnnwnn";
				case '5':	return "wnnnnwnn";
				case '6':	return "nwnnnnwn";
				case '7':	return "nwnnwnnn";
				case '8':	return "nwwnnnnn";
				case '9':	return "wnnwnnnn";
				case '-':	return "nnnwwnnn";
				case '$':	return "nnwwnnnn";
				case ':':	return "wnnnwnwn";
				case '/':	return "wnwnnnwn";
				case '.':	return "wnwnwnnn";
				case '+':	return "nnwnwnwn";
				case 'A':	return "nnwwnwnn";
				case 'B':	return "nwnwnnwn";
				case 'C':	return "nnnwnwwn";
				case 'D':	return "nnnwwwnn";
			}
            // #c1spell(on)
            ThrowInvalidCharacterException("Codabar", (char)c);
            return null;
		}
	}
}
