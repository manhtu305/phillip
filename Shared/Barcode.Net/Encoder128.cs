//----------------------------------------------------------------------------
// Encoder128.cs
//
// CheckDigit, code switch, stop digit
//
// Code 128 is a very high density alphanumeric bar code. The symbol can 
// be as long as necessary to store the encoded data. It is designed to 
// encode all 128 ASCII characters, and will use the least amount of space 
// for data of **6 characters or more** of any 1-D encoding. 
// Each data character encoded in a Code 128 symbol is made up of 11 black 
// or white modules. The stop character, however, is made up of 13 modules.
// Three bars and three spaces are formed out of these 11 modules. Bar 
// and spaces can vary between 1 and 4 modules wide. 
// The symbol includes a quiet zone (10 x-dimensions), a start character, 
// the encoded data, a check character, the stop character, and a trailing 
// quiet zone (10 x-dimensions). 
// 
// There are 106 different 3 bar/3space combinations. Each of the 106 
// combinations can be assigned one of three different character set 
// meanings. These meanings are set by using one of three different start 
// characters. Start Code A allows encoding all the standard alphanumeric 
// keyboard characters plus control characters and special characters. 
// Start Code B includes all standard alphanumeric keyboard characters 
// plus lower case alpha and special characters. Start Code C includes 
// a set of 100 digit pairs from 00 to 99 and can be used to double the 
// density of encoding numeric-only data. 
// 
// NOTE: Code C is the most compact, but it can only be used to encode
// strings that contain an even number of digits (>= 6).
//
// Within a symbol, one can shift between code sets by using the special 
// character CODE and SHIFT. The CODE character shifts the code for all 
// subsequent characters to the specified code set. The SHIFT character 
// just changes the next character and only changes between Code Set A 
// and Code Set B or the reverse. 
// 
// The FNC codes define instructions for the bar code reader. FNC 1 is 
// reserved for future use. FNC 2 tells the reader to store the data read 
// and transmit it with the next symbol. FNC 3 is reserved for code reader 
// initializing and other code reader functions. FNC 4 is reserved for 
// future applications. 
// 
// Each character has a value ranging from 0 to 105. This value is used 
// to calculate the check character for each symbol. 
// 
// The check character is a Modulus 103 Checksum that is calculated by 
// summing the start code value plus the product of each character position 
// (most significant character position equals 1) and the character value 
// of the character at that position. This sum is divided by 103. The 
// remainder of the answer is the value of the Check Character 
// (which can be looked up from the table). Every encoded character is 
// included except the Stop and Check Character. 
// 
// Example: BarCode 1
// Message : Start B   B   a   r   C   o   d   e      1
// Value      104      34  65  82  35  79  68  69  0  17
// Position:   -       1   2   3   4   5   6   7   8  9
// Calculate Total: 104 + (34x1) + (65x2) + (82x3) + (35x4) + (79x5) +
//                  (68x6) + (69x7) + (0x8) + (17x9) = 2093
// 2093/103 = 20 remainder 33
// 33 = A
// Final message: (Start B)BarCode 1(A)(STOP)
// 
// The height of the bars must be at least .15 times the symbol's length 
// or .25 inches, whichever is larger. The overall length of the symbol 
// is given by the equation: 
// 
// L = (11C + 35)X (alphanumeric) L = (5.5C + 35)X (numeric only using Code C) 
// where
// L = length of symbol (not counting quiet zone) 
// C = number of data characters, code characters and shift characters 
//     (do not include start, stop or checksum. They are automatically added in.) 
// X = X-dimension 
//
// Additional info from http://www.idautomation.com/code128faq.html#Creating_UCC_EAN_128
//
// Character set A allows for uppercase characters, punctuation and numbers. Lower case characters
// create several special functions in set A such as a return or tab. It may be necessary to use 
// set A to manually encode these functions in a barcode. 
//
// Character set B is the most common because it encodes everything from ASCII 32 to ASCII 126. 
// It allows for upper and lower case letters, punctuation, numbers and a few select functions. 
//
// Character set C encodes only even numbers. Because the numbers are "interleaved" into pairs, 
// two numbers are encoded into every barcode character which makes it a very high density barcode. 
// If the number to encode is not an even number, a leading zero should be added. Our Code 128 
// auto function, "Code128( )", in our products will automatically switch to set C as necessary 
// and also encode FNC characters. 
//
// Check Digit Requirement - in Code 128, the modulo 103 Symbol Check Character is required and it
// is only encoded in the bar code. In Code 128, the modulo 103 check digit should never appear in 
// human the readable interpretation below the bar code. MOD 10 check digits are sometimes required 
// when encoding numbers for UCC and EAN applications. If required, the MOD 10 should be shown in 
// the human the readable interpretation. More about this is documented here. 
//
// Create barcodes:
// http://www.idautomation.com/servercontrols/LinearServerControl.aspx
//
// Copyright (C) 2004 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2004	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// Encoder128
	/// </summary>
	internal class Encoder128 : EncoderBase
	{
        // ** fields
		private int _currSubset;

		// ** constants
		private const int SUBSET_A = 0;
		private const int SUBSET_B = 1;
		private const int SUBSET_C = 2;

		private const char FNC1 = (char)202;
		private const char FNC2 = (char)197;
		private const char FNC3 = (char)196;

		private const char START_A = (char)203;
		private const char START_B = (char)204;
		private const char START_C = (char)205;

		private const char SWITCH_C = (char)199;
		private const char SWITCH_B = (char)200;
		private const char SWITCH_A = (char)201;

		private static char[] _codeStart  = new char[] { START_A, START_B, START_C };
		private static char[] _codeSwitch = new char[] { SWITCH_A, SWITCH_B, SWITCH_C };

		private static int[] _codesA = // 0-95, 195-206 
		{
			64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 
			80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 
			00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 
			16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 
			32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 
			48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106
		};
		private static int[] _codesB = // 32-126, 195-206
		{
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 
			16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 
			32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 
			48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 
			64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 
			80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
			-1, -1, -1, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106
		};

        // ** ctor
        internal Encoder128(C1BarCode ctl) : base(ctl) { }

        // ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
			// sanity <<B7>>
			// we can do fewer than 5 characters, no problem <<B10>>
			//if (text == null || text.Length < 5)
			//{
			//	throw new ArgumentException("Code128 requires at least 5 characters.");
			//}
			// insert code switches <<B9>>
            text = InsertCodeSwitches(text);

			// calculate the check digit
			int checkDigit = GetCheckDigit(text);

			// draw message
			ScanDraw(g, br, text);

			// draw check digit
			DrawPattern(g, br, checkDigit);
	
			// draw stop character
			DrawPattern(g, br, 106);

			// return total width
			return _totalWidth;
		}
		override protected void DrawText(Graphics g, string text, Font font, Brush br, Rectangle rc)
		{
			// hide code switches/fncx <<B14>>
			for (char c = (char)196; c <= (char)206; c++)
			{
				text = text.Replace(c.ToString(), string.Empty);
			}
			// #c1spell(off)
            // hide code switches/fncx <<B9>>
			//for (int i = 202; i <= 206; i++)
			//{
			//	text = text.Replace((char)i, ' ');
			//}
            // #c1spell(on)
			base.DrawText(g, text, font, br, rc);
		}

		// ** private

		private int GetStartingSet(string text) // <<B9>>
		{
			// sanity
			if (text == null || text.Length == 0)
			{
				return SUBSET_B;
			}

			// check for explicit starting set
			char c = text[0];
			switch (c)
			{
				case START_A: return SUBSET_A;
				case START_B: return SUBSET_B;
				case START_C: return SUBSET_C;
			}

			// select starting char set
			int start = SUBSET_B;
			if (StartsWithDigits(text, 0, 4))
			{
				start = SUBSET_C;
			}
            // starting with A doesn't seem to work too well... <<B27>>
			//else if (c < 32)
			//{
            //  start = SUBSET_A;
			//}

			// 202 & 212-215 is for the FNC1, with this Start C is mandatory
			if (c == FNC1 || (c >= 212 && c <= 215))
			{
				start = SUBSET_C;
			} 
			else if (c == FNC2)
			{
				start = SUBSET_B;
			}

			// done
			return start;
		}
		private string InsertCodeSwitches(string text)
		{
			// if the text already has switches, switch only if strictly necessary <<B9>>
			bool autoSwitch = true;
			if (text.IndexOfAny(_codeSwitch) > -1 ||
				text.IndexOfAny(_codeStart) == 0)
			{
				autoSwitch = false;
			}

			// scan string
			StringBuilder sb = new StringBuilder();
			int currSet = GetStartingSet(text);
			for (int pos = 0; pos < text.Length; pos++)
			{
                // handle special chars (including explicit switches)
                char c = text[pos];
				if (c >= 196)
				{
					switch (c)
					{
                        // handle explicit switch
                        case SWITCH_C:
							currSet = SUBSET_C;
							break;
						case SWITCH_B:
                            if (currSet == SUBSET_C) currSet = SUBSET_B;
							break;
						case SWITCH_A:
							if (currSet == SUBSET_C) currSet = SUBSET_A;
							break;

                        // other special char: switch away from C if necessary <<B14>>
						//case FNC1:
						//case FNC2:
						//case FNC3:
                        default:
							if (currSet == SUBSET_C)
							{
								autoSwitch = true;
								sb.Append(SWITCH_B);
								currSet = SUBSET_B;
							}
							break;
					}

					// append special char and continue
					sb.Append(c);
					continue;
				}

                // switch to A if necessary (to encode control chars) <<B27>>
                else if (currSet != SUBSET_A && c < 32)
                {
                    sb.Append(SWITCH_A);
                    currSet = SUBSET_A;
                }

                // switch to B if necessary (to encode lowercase chars) <<B27>>
                else if (currSet != SUBSET_B && char.IsLower(c))
                {
                    sb.Append(SWITCH_B);
                    currSet = SUBSET_B;
                }

                // switch to C if possible (most compact)
                else if (currSet != SUBSET_C && autoSwitch && StartsWithDigits(text, pos, 4))
                {
                    sb.Append(SWITCH_C);
                    currSet = SUBSET_C;
                }

				// switch away from C if necessary
                else if (currSet == SUBSET_C && !StartsWithDigits(text, pos, 2))
                {
                    // switch to B or A
                    if (c >= 32 && c <= 126)
                    {
                        sb.Append(SWITCH_B);
                        currSet = SUBSET_B;
                    }
                    else
                    {
                        sb.Append(SWITCH_A);
                        currSet = SUBSET_A;
                    }
                }

				// append the character
				sb.Append(c);

				// subset C does pairs of digits, so skip the second one
				if (currSet == SUBSET_C)
				{
					pos++;
					sb.Append(text[pos]);
				}
			}

			// return the result
			return sb.ToString();
		}
		private bool StartsWithDigits(string text, int pos, int len)
		{
			// check that we have enough room for the digits
			if (pos + len > text.Length) return false;

			// check the digits
			for (int i = 0; i < len; i++)
			{
				if (!char.IsDigit(text, pos + i))
					return false;
			}

			// seems ok
			return true;
		}
		private int GetCheckDigit(string text)
		{
			return ScanDraw(null, null, text);
		}
		private int ScanDraw(Graphics g, Brush br, string text)
		{
			// initialize 
			_cursor = new Point(0,0);
			_totalWidth = 0;
			_currSubset = GetStartingSet(text);
            //Trace(g, "-------------\r\nRendering: '{0}'", text);

			// draw start character for current subset
			int sum = 0;
			switch (_currSubset)
			{ 
				case SUBSET_A:
                    sum = 103;
					DrawPattern(g, br, 103);
					break;
				case SUBSET_B:
                    sum = 104;
                    DrawPattern(g, br, 104);
					break;
				case SUBSET_C:
                    sum = 105;
                    DrawPattern(g, br, 105);
					break;
			}
	
			// initialize for scanning
			int pos = 0;
			int weight = 1;
			int code = 0;

			// scan text, render, calculate code digit
			while (pos < text.Length)
			{
				// skip explicit start code, we've rendered that already <<B9>>
				char c = text[pos];
				if (pos == 0 && c >= START_A && c <= START_C) // <<B12>> START_B)
				{
                    //Trace(g, "skipping explicit start code");
                    pos++;
					continue;
				}

				// render
				if (_currSubset == SUBSET_C)
				{
					// if it's a switch to SUBSET_A - same character (103) for all subsets
					if (Get128Code(SUBSET_A, c) == 101)
					{
						// draw the startA code
						code = 101;
						sum += code * weight;
                        DrawPattern(g, br, 101);

						// we've moved one message character
						pos++;
						weight++;

						// actually change the subset
                        //Trace(g, "switching to A");
						_currSubset = SUBSET_A;
					}
					
					// if it's a switch to SUBSET_B - same character (104) for all subsets
					else if (Get128Code(SUBSET_A, c) == 100)
					{
						// draw the startB code
						code = 100;
						sum += code * weight;
                        DrawPattern(g, br, 100);

						// we've moved one message character
						pos++;
						weight++;

						// actually change the subset
                        //Trace(g, "switching to B");
                        _currSubset = SUBSET_B;
					}

					// it's FNC1 - just print it out
					else if (Get128Code(SUBSET_A, c) == 102)
					{
						// draw the FNC1 <<B9>>
						code = 102;
						sum += code * weight;
                        DrawPattern(g, br, code);

						// we've moved one message character
						pos++;
						weight++;
                        //Trace(g, "draw FNC1");
                    }

//                  // #c1spell(off)
//					// handle FN in code C (I think this is wrong (bad code), 
//                  // but IDAutomation renders it anyway,
//					// and GC has reported it a few times... <<B14>>
//					else if (c == FNC1 || c == FNC2 || c == FNC3)
//					{
//						// draw the FNC*
//						code128 = c - 100;
//						sum += code128 * weight;
//						DrawPattern(g, br, code128);
//
//						// we've moved one message character
//						pos++;
//						weight++;
//					}
//					
//                  // #c1spell(on)

                    // it's a digit - pull two at a time
					else
					{
						// get the next two characters as an integer <<B3>>
						code = int.Parse(text.Substring(pos, 2));
						sum += code * weight;

						// draw the code 128 character
						DrawPattern(g, br, code);

						// we've moved two message characters
						pos += 2;
						weight++;
                        //Trace(g, "draw 2 chars in CODE_C");
                    }
				}
					
				// we're in SUBSET_A or SUBSET_B
				else
				{
					// handle upper ASCII characters if necessary
					code = (int)c;
					if (code < -1) code &= 255;
			
					// retrieve the message character
					code = Get128Code(_currSubset, (char)code);
					sum += code * weight;

					// draw the char
					DrawPattern(g, br, code);

					// we've moved one character position
					pos++;
					weight++;
                    //Trace(g, "draw {0}", code);

                    // switch char sets
                    if (c == SWITCH_A && _currSubset != SUBSET_A)
                    {
                        Debug.Assert(code == 101);
                        _currSubset = SUBSET_A;
                        //Trace(g, "switch to A");
                    }
                    else if (c == SWITCH_B && _currSubset != SUBSET_B)
                    {
                        Debug.Assert(code == 100);
                        _currSubset = SUBSET_B;
                        //Trace(g, "switch to B");
                    }
                    else if (c == SWITCH_C && _currSubset != SUBSET_C)
                    {
                        Debug.Assert(code == 99);
                        _currSubset = SUBSET_C;
                        //Trace(g, "switch to C");
                    }

					// switch from A to B or C
                    //if (_currSubset == SUBSET_A)
                    //{
                    //    if (code128 == 100)
                    //    {
                    //        _currSubset = SUBSET_B;
                    //    }
                    //    else if (code128 == 99)
                    //    {
                    //        _currSubset = SUBSET_C;
                    //    }
                    //}

                    //// switch from B to A or C
                    //else if (_currSubset == SUBSET_B)
                    //{
                    //    if (code128 == 101)
                    //    {
                    //        _currSubset = SUBSET_A;
                    //    }
                    //    else if (code128 == 99)
                    //    {
                    //        _currSubset = SUBSET_C;
                    //    }
                    //}

					// if a shift character
					else if (code == 98)
					{
						// shift subsets for the next character only
                        int subset = (_currSubset == SUBSET_A) ? SUBSET_B : SUBSET_A;
						code = Get128Code(subset, c);

						// draw the shifted character
						DrawPattern(g, br, code);
						sum += code * weight;

						// since we've handled two characters advance character position again
						pos++;
						weight++;
                        //Trace(g, "shift {0}", code);
                    }
				}
			}

			// return checksum
			return sum % 103;
		}
		private void DrawPattern(Graphics g, Brush br, int code)
		{
            // get pattern
            string pattern = RetrievePattern(code);

			// initialize X pixel value
			for (int i = 0; i < pattern.Length; i++)
			{
				// draw this bar
                if (pattern[i] == 'b' && g != null)
                {
                    g.FillRectangle(br, _cursor.X, _cursor.Y, _barWidthNarrow, _barHeight);
                }

				// advance the starting position
				_cursor.X += _barWidthNarrow;
				_totalWidth += _barWidthNarrow;
			}
		}
		private string RetrievePattern(int c)
		{
            // #c1spell(off)
			switch (c)
			{
				case -1:	return string.Empty;
				case 0:		return "bbsbbssbbss";
				case 1:		return "bbssbbsbbss";
				case 2:		return "bbssbbssbbs";
				case 3:		return "bssbssbbsss";
				case 4:		return "bssbsssbbss";
				case 5:		return "bsssbssbbss";
				case 6:		return "bssbbssbsss";
				case 7:		return "bssbbsssbss";
				case 8:		return "bsssbbssbss";
				case 9:		return "bbssbssbsss";
				case 10:	return "bbssbsssbss";
				case 11:	return "bbsssbssbss";
				case 12:	return "bsbbssbbbss";
				case 13:	return "bssbbsbbbss";
				case 14:	return "bssbbssbbbs";
				case 15:	return "bsbbbssbbss";
				case 16:	return "bssbbbsbbss";
				case 17:	return "bssbbbssbbs";
				case 18:	return "bbssbbbssbs";
				case 19:	return "bbssbsbbbss";
				case 20:	return "bbssbssbbbs";
				case 21:	return "bbsbbbssbss";
				case 22:	return "bbssbbbsbss";
				case 23:	return "bbbsbbsbbbs";
				case 24:	return "bbbsbssbbss";
				case 25:	return "bbbssbsbbss";
				case 26:	return "bbbssbssbbs";
				case 27:	return "bbbsbbssbss";
				case 28:	return "bbbssbbsbss";
				case 29:	return "bbbssbbssbs";
				case 30:	return "bbsbbsbbsss";
				case 31:	return "bbsbbsssbbs";
				case 32:	return "bbsssbbsbbs";
				case 33:	return "bsbsssbbsss";
				case 34:	return "bsssbsbbsss";
				case 35:	return "bsssbsssbbs";
				case 36:	return "bsbbsssbsss";
				case 37:	return "bsssbbsbsss";
				case 38:	return "bsssbbsssbs";
				case 39:	return "bbsbsssbsss";
				case 40:	return "bbsssbsbsss";
				case 41:	return "bbsssbsssbs";
				case 42:	return "bsbbsbbbsss";
				case 43:	return "bsbbsssbbbs";
				case 44:	return "bsssbbsbbbs";
				case 45:	return "bsbbbsbbsss";
				case 46:	return "bsbbbsssbbs";
				case 47:	return "bsssbbbsbbs";
				case 48:	return "bbbsbbbsbbs";
				case 49:	return "bbsbsssbbbs";
				case 50:	return "bbsssbsbbbs";
				case 51:	return "bbsbbbsbsss";
				case 52:	return "bbsbbbsssbs";
				case 53:	return "bbsbbbsbbbs";
				case 54:	return "bbbsbsbbsss";
				case 55:	return "bbbsbsssbbs";
				case 56:	return "bbbsssbsbbs";
				case 57:	return "bbbsbbsbsss";
				case 58:	return "bbbsbbsssbs";
				case 59:	return "bbbsssbbsbs";
				case 60:	return "bbbsbbbbsbs";
				case 61:	return "bbssbssssbs";
				case 62:	return "bbbbsssbsbs";
				case 63:	return "bsbssbbssss";
				case 64:	return "bsbssssbbss";
				case 65:	return "bssbsbbssss";
				case 66:	return "bssbssssbbs";
				case 67:	return "bssssbsbbss";
				case 68:	return "bssssbssbbs";
				case 69:	return "bsbbssbssss";
				case 70:	return "bsbbssssbss";
				case 71:	return "bssbbsbssss";
				case 72:	return "bssbbssssbs";
				case 73:	return "bssssbbsbss";
				case 74:	return "bssssbbssbs";
				case 75:	return "bbssssbssbs";
				case 76:	return "bbssbsbssss";
				case 77:	return "bbbbsbbbsbs";
				case 78:	return "bbssssbsbss";
				case 79:	return "bsssbbbbsbs";
				case 80:	return "bsbssbbbbss";
				case 81:	return "bssbsbbbbss";
				case 82:	return "bssbssbbbbs";
				case 83:	return "bsbbbbssbss";
				case 84:	return "bssbbbbsbss";
				case 85:	return "bssbbbbssbs";
				case 86:	return "bbbbsbssbss";
				case 87:	return "bbbbssbsbss";
				case 88:	return "bbbbssbssbs";
				case 89:	return "bbsbbsbbbbs";
				case 90:	return "bbsbbbbsbbs";
				case 91:	return "bbbbsbbsbbs";
				case 92:	return "bsbsbbbbsss";
				case 93:	return "bsbsssbbbbs";
				case 94:	return "bsssbsbbbbs";
				case 95:	return "bsbbbbsbsss";
				case 96:	return "bsbbbbsssbs";
				case 97:	return "bbbbsbsbsss";
				case 98:	return "bbbbsbsssbs";
				case 99:	return "bsbbbsbbbbs";
				case 100:	return "bsbbbbsbbbs";
				case 101:	return "bbbsbsbbbbs";
				case 102:	return "bbbbsbsbbbs";
				case 103:	return "bbsbsbbbbss";
				case 104:	return "bbsbssbssss";
				case 105:	return "bbsbssbbbss";
				case 106:	return "bbsssbbbsbsbb";
			}

            // #c1spell(on)
            ThrowInvalidCharacterException("Code128", (char)c);
            return null;
		}
		private int Get128Code(int subset, char c)
		{
			return (subset == SUBSET_A)
				? _codesA[c]
				: _codesB[c];
		}
#if DEBUG
        void Trace(Graphics g, string fmt, params object[] parms)
        {
            if (g != null)
            {
                Debug.Write(string.Format(fmt, parms));
                Debug.WriteLine(string.Format(" ({0})", 
                    _currSubset == SUBSET_A ? "A" : _currSubset == SUBSET_B ? "B" : "C"));
            }
        }
#endif
	}
}
