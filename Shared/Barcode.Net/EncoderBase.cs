//----------------------------------------------------------------------------
// EncoderBase.cs
//
// Abstract class used as a base for all Encoder classes.
//
// This code is mostly based on an original C++ library written by
// Neil Van Eps and posted at CodeProject:
//
//		http://www.codeproject.com/bitmap/barcode1.asp
//
//
// There's an interesting barcode Java implementation available at:
//
//		http://krysalis.org/barcode/index.html
//
//
// Additional barcode information and links are available at:
//
//		http://www.barcode-1.net/
//
//
// Copyright (C) 2004 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2004	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderBase
	/// Base class for 1-d barcode encoders
	/// </summary>
	internal abstract class EncoderBase
	{
		// ** fields
		protected int					_barHeight;		    // all units in screen pixels
		protected int					_barWidthNarrow;
		protected int					_barWidthWide;
		protected Point					_cursor;		    // used for drawing the barcode image
		protected int					_totalWidth;	    // used for measuring the image
		protected BarDirectionEnum		_barDirection;	    // bar direction (horizontal, vertical)
		protected static StringFormat	_fmt;			    // used for drawing the string
        protected C1BarCode             _ctl;               // owner control
        private Exception               _exception;         // last encoding exception

		// ** abstracts (every encoder must implement this)

		// Draw the barcode into the Graphics object and return the 
		// total width of the barcode image.
		//
		// If g == null, don't draw anything, only return the width
		// needed to render the code.
		//
		abstract protected int Draw(Graphics g, Brush br, string text);

		// ** ctors

		static EncoderBase()
		{
			_fmt = new StringFormat(StringFormat.GenericTypographic);
			_fmt.Alignment = StringAlignment.Center;
			_fmt.LineAlignment = StringAlignment.Far;
			_fmt.FormatFlags |= StringFormatFlags.NoClip;
		}
        internal EncoderBase(C1BarCode ctl)
        {
            _ctl = ctl;
            _cursor = Point.Empty;
            _totalWidth = 0;
            _barWidthNarrow = ctl.BarNarrow;
            _barWidthWide = ctl.BarWide;
            _barHeight = ctl.BarHeight;
            _barDirection = ctl.BarDirection;
        }
		// build barcode image (EMF)
		public Image GetImage(string text, Color color, Font font)
		{
            lock (_ctl) // <<B265>> extra safety
            {
                // get image width (height is constant, = _barHeight)
                int wid = GetBarSize(text).Width;
                if (wid == 0) return null;

                // create metafile with specified dimensions
                Rectangle rc = new Rectangle(0, 0, wid, _barHeight);
                Metafile mf = null;
                using (Graphics g = _ctl.CreateGraphics())
                {
                    IntPtr dc = g.GetHdc();
                    mf = new Metafile(dc, rc, MetafileFrameUnit.Pixel);
                    g.ReleaseHdc(dc);
                }

                // extra safety
                if (mf == null)
                {
                    return null;
                }

                // draw image into the metafile
                try
                {
                    using (SolidBrush br = new SolidBrush(color))
                    using (Graphics g = Graphics.FromImage(mf))
                    {
                        // draw text first, clip it out
                        if (font != null && font.Height > 1 && text != null && text.Length > 0)
                        {
                            DrawText(g, text, font, br, rc);
                        }

                        // draw barcode image
                        g.PageUnit = GraphicsUnit.Pixel;
                        int widDrawn = Draw(g, br, text);
                        Debug.Assert(widDrawn == wid);
                    }
                }
                catch (Exception x)
                {
                    Debug.WriteLine(x.Message);
                }

                // apply rotation
                if (mf != null && _barDirection != BarDirectionEnum.Normal)
                {
                    Image img = mf;
                    rc = new Rectangle(0, 0, rc.Height, rc.Width);
                    using (Graphics g = _ctl.CreateGraphics())
                    {
                        IntPtr dc = g.GetHdc();
                        mf = new Metafile(dc, rc, MetafileFrameUnit.Pixel);
                        g.ReleaseHdc(dc);
                    }
                    using (Graphics g = Graphics.FromImage(mf))
                    using (Matrix xform = new Matrix())
                    {
                        if (_barDirection == BarDirectionEnum.Down)
                        {
                            xform.Rotate(90);
                            xform.Translate(0, -rc.Width);
                        }
                        else // Up
                        {
                            xform.Rotate(-90);
                            xform.Translate(-rc.Height, 0);
                        }
                        g.Transform = xform;
                        g.DrawImageUnscaled(img, 0, 0);
                    }
                }

                // return the image
                return mf;
            }
		}

        // draw a string centered along the bottom of the barcode image, clip out
        protected virtual void DrawText(Graphics g, string text, Font font, Brush br, Rectangle rc)
		{
            DrawTextBase(g, text, font, br, rc);
		}

		// draw a string centered along the bottom of the barcode image, clip out
		protected void DrawTextBase(Graphics g, string text, Font font, Brush br, Rectangle rc)
		{
			// measure text
			Rectangle r = rc;
			r.Size = Size.Ceiling(g.MeasureString(text, font, PointF.Empty, _fmt));
			r.Inflate(2, 0); // add 2 pixels instead of one <<B6>>

			// position text
			int offx = Math.Max(0, (rc.Width - r.Width)/2);
			int offy = Math.Max(0, rc.Height - r.Height + 1);
			r.Offset(offx, offy);

			// blank image
			g.FillRectangle(Brushes.White, r);
			g.DrawString(text, font, br, r, _fmt);
			g.ExcludeClip(r);
		}

		// get image size in pixels (used for creating the metafile)
		protected Size GetBarSize(string text)
		{
            _exception = null;
			try
			{
                int wid = Draw(null, null, text);
				return new Size(wid, _barHeight);
			}
            catch (Exception x)
			{
                _exception = x;
				return Size.Empty; // << invalid characters
			}
		}

        // gets a reference to the last encoding exception
        public Exception EncodingException
        {
            get { return _exception; }
        }

        // consistent exception message for common error (invalid char for encoding)
        public void ThrowInvalidCharacterException(string encoding, char badChar)
        {
            string msg = string.Format("Cannot encode character '{0}' using encoding {1}.", badChar, encoding);
            throw new ArgumentException(msg);
        }

	}
}
