﻿// TBD: add xml comments?
#pragma warning disable 1591
//#define LOCALIZE_TO_CURRENT_CULTURE
//#define LOCTEST

using System;
using System.Resources;
using System.Globalization;
using System.Reflection;
using C1.Win.Localization;

namespace C1.Win.C1BarCode
{
    /// <summary>
    /// Static class containing UI strings used by the designer.
    /// </summary>
    public static class Strings
    {
        #region Infrastructure
        private static ResourceManager s_ResourceManager;

        public static ResourceManager ResourceManager
        {
            get { return s_ResourceManager; }
            set { s_ResourceManager = value; }
        }

        public static CultureInfo UICulture
        {
#if LOCTEST
			get { return new CultureInfo("ja"); }
#else
#if DEBUG && GRAPECITY
            get { return new CultureInfo("ja"); }
#else
            // 2012/10/26
            // As result of C1 developers discussion now CurrentUICulture used
            // instead of CurrentCulture to get culture for localized strings
#if !LOCALIZE_TO_CURRENT_CULTURE
            get { return CultureInfo.CurrentUICulture; }
#else
            get { return CultureInfo.CurrentCulture; }
#endif
#endif
#endif
        }
        #endregion

        public static class Errors
        {
            public static string BarNarrowMustBePositive
            {
                get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "BarNarrow must be > 0."); }
            }
            public static string BarWideMustBePositive
            {
                get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "BarWide must be > 0."); }
            }
            public static string BarHeightMustBePositive
            {
                get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "BarHeight must be > 0."); }
            }
            public static string InvalidCodeVersion
            {
                get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "Invalid CodeVersion value (must be between 0 and 10)."); }
            }
            public static string InvalidSymbolSize
            {
                get { return StringsManager.GetString(MethodBase.GetCurrentMethod(), "Invalid symbol size (should be between 2 and 10)."); }
            }


        }
    }
}
