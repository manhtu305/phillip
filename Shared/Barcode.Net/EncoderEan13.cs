//----------------------------------------------------------------------------
// EncoderEan13.cs
//
// Copyright (C) 2005 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2005	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderEan13
	/// </summary>
	internal class EncoderEan13 : EncoderBase
	{
        // ** ctor
        internal EncoderEan13(C1BarCode ctl) : base(ctl) { }

		// ** enums
		internal enum Parity
		{
			LeftOdd,
			LeftEven,
			Right
		}

		// ** statics

		// patterns used to encode each digit
		private static string[] _codeLeftOdd = new string[] 
		{
			"0001101", // 0
			"0011001", // 1
			"0010011", // 2
			"0111101", // 3
			"0100011", // 4
			"0110001", // 5
			"0101111", // 6
			"0111011", // 7
			"0110111", // 8
			"0001011", // 9
		};
		private static string[] _codeLeftEven = new string[] 
		{
			"0100111", // 0
			"0110011", // 1
			"0011011", // 2
			"0100001", // 3
			"0011101", // 4
			"0111001", // 5
			"0000101", // 6
			"0010001", // 7
			"0001001", // 8
			"0010111", // 9
		};
		private static string[] _codeRight = new string[] 
		{
			"1110010", // 0
			"1100110", // 1
			"1101100", // 2
			"1000010", // 3
			"1011100", // 4
			"1001110", // 5
			"1010000", // 6
			"1000100", // 7
			"1001000", // 8
			"1110100", // 9
		};

		// parity values used to encode manufacturer's first digit (second is always odd)
		// each character means odd ("o") or even ("e").
        // #c1spell(off)
        private static string[] _manufacturerParity = new string[]
		{
			"ooooo", // 0
			"oeoee", // 1
			"oeeoe", // 2
			"oeeeo", // 3
			"eooee", // 4
			"eeooe", // 5
			"eeeoo", // 6
			"eoeoe", // 7
			"eoeeo", // 8
			"eeoeo", // 9
		};
        // #c1spell(on)


		// ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
			// initialize
			_cursor = new Point(0,0);
			_totalWidth = 0;

			// sanity
			if (BadText(text))
				throw new ArgumentException("Ean13 encoding requires exactly 12 digits.");

			// calculate the check digit
			char checkDigit = GetCheckDigit(text);

            // draw quiet zone <<B13>>
            DrawPattern(g, br, "000000000");
   
            // draw left guards
			DrawPattern(g, br, "101");

			// first number system digit determines manufacturer parity
			int mfIndex = text[0] - '0';
			string mf = _manufacturerParity[mfIndex];

			// second number system digit, odd parity
			DrawDigit(g, br, text[1], Parity.LeftOdd);

			// 5 manufacturer digits
			DrawDigit(g, br, text[2], mf[0]);
			DrawDigit(g, br, text[3], mf[1]);
			DrawDigit(g, br, text[4], mf[2]);
			DrawDigit(g, br, text[5], mf[3]);
			DrawDigit(g, br, text[6], mf[4]);

			// draw center guards
			DrawPattern(g, br, "01010");

			// 5 product code digits
			DrawDigit(g, br, text[7],  Parity.Right);
			DrawDigit(g, br, text[8],  Parity.Right);
			DrawDigit(g, br, text[9],  Parity.Right);
			DrawDigit(g, br, text[10], Parity.Right);
			DrawDigit(g, br, text[11], Parity.Right);

			// check digit, right hand
			DrawDigit(g, br, checkDigit, Parity.Right);

			// right guard
			DrawPattern(g, br, "101");

            // draw quiet zone <<B13>>
            DrawPattern(g, br, "000000000");

			// return total width
			return _totalWidth;
		}
		override protected void DrawText(Graphics g, string text, Font font, Brush br, Rectangle rc)
		{
			// skip invalid stuff
			if (BadText(text))
			{
				base.DrawText(g, text, font, br, rc);
				return;
			}

            // draw control digit in quiet zone (x = 0, width = 9 / 101) <<B13>>
            string digits = text.Substring(0, 1);
            Rectangle r = rc;
            r.Width = rc.Width * 9 / 101;
            DrawTextBase(g, digits, font, br, r);

            // draw left part (x = 12, width = 36 / 101)
            digits = text.Substring(1, 6);
            r.X = rc.Width * 12 / 101;
            r.Width = rc.Width * 36 / 101;
            DrawTextBase(g, digits, font, br, r);

            // draw right part (x = 53, width = 36 / 101)
            digits = text.Substring(7) + GetCheckDigit(text);
            r.X = rc.Width * 53 / 101;
            DrawTextBase(g, digits, font, br, r);
        }

		// ** protected
		protected internal string RetrievePattern(char digit, Parity parity)
		{
			// sanity
			if (!char.IsDigit(digit))
				throw new ArgumentException("Ean13 can only encode digits.");

			// retrieve pattern
			int index = digit - '0';
			switch (parity)
			{
				case Parity.LeftOdd:	return _codeLeftOdd[index];
				case Parity.LeftEven:	return _codeLeftEven[index];
				case Parity.Right:		return _codeRight[index];
			}

			// should never get here
			throw new ArgumentException("Invalid parity for current encoding");
		}
		protected void DrawPattern(Graphics g, Brush br, string pattern)
		{
			// draw the whole pattern
			for (int i = 0; i < pattern.Length; i++)
			{
				// '1' is bar, '0' is space
				int wid = _barWidthNarrow;
				if (pattern[i] == '1' && g != null)
					g.FillRectangle(br, _cursor.X, _cursor.Y, wid, _barHeight);

				// advance the starting position
				_cursor.X += wid;
				_totalWidth += wid;
			}
		}
		protected void DrawDigit(Graphics g, Brush br, char digit, char parity)
		{
			switch (parity)
			{
				case 'o':
					DrawDigit(g, br, digit, Parity.LeftOdd);
					break;
				case 'e':
					DrawDigit(g, br, digit, Parity.LeftEven);
					break;
				case 'r':
					DrawDigit(g, br, digit, Parity.Right);
					break;
				default:
					// should never get here
					throw new ArgumentException("Invalid parity code.");
			}
		}
		protected void DrawDigit(Graphics g, Brush br, char digit, Parity parity)
		{
			string pattern = RetrievePattern(digit, parity);
			DrawPattern(g, br, pattern);
		}
        protected char GetCheckDigit(string text)
        {
            // start with zero
            int check = 0;

            // initialize weight (1 for ean 12, 3 for ean 8) <<B279>>
            int weight = (text.Length == 12) ? 1 : 3;
            Debug.Assert(text.Length == 12 || text.Length == 7);

            // calculate weighted sum
            for (int i = 0; i < text.Length; i++)
            {
                check += weight * (text[i] - '0');
                weight = 4 - weight;
            }

            // calculate check
            check = 10 - check % 10;
            if (check == 10) check = 0;
            return (char)(check + '0');
        }

		// ** private
		private bool BadText(string text)
		{
			if (text.Length != 12) return true;
			foreach (char c in text)
			{
				if (!char.IsDigit(c))
					return true;
			}
			return false;
		}
	}
}
