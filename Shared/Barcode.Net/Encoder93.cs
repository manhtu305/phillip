//----------------------------------------------------------------------------
// Encoder93.cs
//
// Code 93 offers higher information density for alphanumeric data than 
// either Code 39 or Code 128.
//
// Copyright (C) 2004 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2004	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// Encoder93
	/// </summary>
	internal class Encoder93 : EncoderBase
	{
        // ** ctor
        internal Encoder93(C1BarCode ctl) : base(ctl) { }

		// ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
			// initialize
			_cursor = new Point(0,0);
			_totalWidth = 0;

			// draw start character
			DrawPattern(g, br, RetrievePattern(47));
	
			// draw each character in the message
			for (int i = 0; i < text.Length; i++)
			{
				Point pt = ASCIItoCode93Sequence(text[i]);
				DrawPattern(g, br, RetrievePattern(pt.X));
				if (pt.Y > -1)
					DrawPattern(g, br, RetrievePattern(pt.Y));
			}

			// add the check digit characters
			DrawCheckDigits(g, br, text);

			// draw stop character
			DrawPattern(g, br, RetrievePattern(48));

			// return total width
			return _totalWidth;
		}

		// ** private
		private void DrawPattern(Graphics g, Brush br, string pattern)
		{
			// initialize X pixel value
			for (int i = 0; i < pattern.Length; i++)
			{
				// draw this bar
				if (pattern[i] == 'b' && g != null)
					g.FillRectangle(br, _cursor.X, _cursor.Y, _barWidthNarrow, _barHeight);

				// advance the starting position
				_cursor.X += _barWidthNarrow;
				_totalWidth += _barWidthNarrow;
			}
		}
		private void DrawCheckDigits(Graphics g, Brush br, string text)
		{
			// "C" check digit character
			int nWeight = 1;
			int nSum = 0;
			for (int i = text.Length - 1; i > -1; i--)
			{
				Point pt = ASCIItoCode93Sequence(text[i]);

				// add to the sum
				nSum += nWeight * pt.X;
				nWeight++;
				if (nWeight > 20) nWeight = 1;

				// if it's a two sequence character
				if (pt.Y != -1)
				{
					nSum += (nWeight * pt.Y);
					nWeight++;
					if (nWeight > 20) nWeight = 1;
				}
			}

			// draw the "C" check digit character
			DrawPattern(g, br, RetrievePattern(nSum % 47));
	
			// "K" check digit character - include the "C" check digit character in calculations
			nWeight = 2;
			nSum = nSum % 47;
			for (int i = text.Length-1; i > -1; i--)
			{
				Point pt = ASCIItoCode93Sequence(text[i]);

				// add to the sum
				nSum += (nWeight * pt.X);
				nWeight++;
				if (nWeight > 15) nWeight = 1;

				// if its a two sequence character
				if (pt.Y != -1) 
				{
					nSum += (nWeight * pt.Y);
					nWeight++;
					if (nWeight > 15) nWeight = 1;
				}
			}

			// draw the "K" check digit character
			DrawPattern(g, br, RetrievePattern(nSum % 47));
		}
		private Point ASCIItoCode93Sequence(int c)
		{
			switch (c)
			{
				case 0:		return new Point(44,30);
				case 1:		return new Point(43,10);
				case 2:		return new Point(43,11);
				case 3:		return new Point(43,12);
				case 4:		return new Point(43,13);
				case 5:		return new Point(43,14);
				case 6:		return new Point(43,15);
				case 7:		return new Point(43,16);
				case 8:		return new Point(43,17);
				case 9:		return new Point(43,18);
				case 10:	return new Point(43,19);
				case 11:	return new Point(43,20);
				case 12:	return new Point(43,21);
				case 13:	return new Point(43,22);
				case 14:	return new Point(43,23);
				case 15:	return new Point(43,24);
				case 16:	return new Point(43,25);
				case 17:	return new Point(43,26);
				case 18:	return new Point(43,27);
				case 19:	return new Point(43,28);
				case 20:	return new Point(43,29);
				case 21:	return new Point(43,30);
				case 22:	return new Point(43,31);
				case 23:	return new Point(43,32);
				case 24:	return new Point(43,33);
				case 25:	return new Point(43,34);
				case 26:	return new Point(43,35);
				case 27:	return new Point(44,10);
				case 28:	return new Point(44,11);
				case 29:	return new Point(44,12);
				case 30:	return new Point(44,13);
				case 31:	return new Point(44,14);
				case 32:	return new Point(38,-1);
				case 33:	return new Point(45,10);
				case 34:	return new Point(45,11);
				case 35:	return new Point(45,12);
				case 36:	return new Point(39,-1);
				case 37:	return new Point(42,-1);
				case 38:	return new Point(45,15);
				case 39:	return new Point(45,16);
				case 40:	return new Point(45,17);
				case 41:	return new Point(45,18);
				case 42:	return new Point(45,19);
				case 43:	return new Point(41,-1);
				case 44:	return new Point(45,21);
				case 45:	return new Point(36,-1);
				case 46:	return new Point(37,-1);
				case 47:	return new Point(40,-1);
				case 48:	return new Point(0,-1);
				case 49:	return new Point(1,-1);
				case 50:	return new Point(2,-1);
				case 51:	return new Point(3,-1);
				case 52:	return new Point(4,-1);
				case 53:	return new Point(5,-1);
				case 54:	return new Point(6,-1);
				case 55:	return new Point(7,-1);
				case 56:	return new Point(8,-1);
				case 57:	return new Point(9,-1);
				case 58:	return new Point(45,35);
				case 59:	return new Point(44,15);
				case 60:	return new Point(44,16);
				case 61:	return new Point(44,17);
				case 62:	return new Point(44,18);
				case 63:	return new Point(44,19);
				case 64:	return new Point(44,31);
				case 65:	return new Point(10,-1);
				case 66:	return new Point(11,-1);
				case 67:	return new Point(12,-1);
				case 68:	return new Point(13,-1);
				case 69:	return new Point(14,-1);
				case 70:	return new Point(15,-1);
				case 71:	return new Point(16,-1);
				case 72:	return new Point(17,-1);
				case 73:	return new Point(18,-1);
				case 74:	return new Point(19,-1);
				case 75:	return new Point(20,-1);
				case 76:	return new Point(21,-1);
				case 77:	return new Point(22,-1);
				case 78:	return new Point(23,-1);
				case 79:	return new Point(24,-1);
				case 80:	return new Point(25,-1);
				case 81:	return new Point(26,-1);
				case 82:	return new Point(27,-1);
				case 83:	return new Point(28,-1);
				case 84:	return new Point(29,-1);
				case 85:	return new Point(30,-1);
				case 86:	return new Point(31,-1);
				case 87:	return new Point(32,-1);
				case 88:	return new Point(33,-1);
				case 89:	return new Point(34,-1);
				case 90:	return new Point(35,-1);
				case 91:	return new Point(44,20);
				case 92:	return new Point(44,21);
				case 93:	return new Point(44,22);
				case 94:	return new Point(44,23);
				case 95:	return new Point(44,24);
				case 96:	return new Point(44,32);
				case 97:	return new Point(46,10);
				case 98:	return new Point(46,11);
				case 99:	return new Point(46,12);
				case 100:	return new Point(46,13);
				case 101:	return new Point(46,14);
				case 102:	return new Point(46,15);
				case 103:	return new Point(46,16);
				case 104:	return new Point(46,17);
				case 105:	return new Point(46,18);
				case 106:	return new Point(46,19);
				case 107:	return new Point(46,20);
				case 108:	return new Point(46,21);
				case 109:	return new Point(46,22);
				case 110:	return new Point(46,23);
				case 111:	return new Point(46,24);
				case 112:	return new Point(46,25);
				case 113:	return new Point(46,26);
				case 114:	return new Point(46,27);
				case 115:	return new Point(46,28);
				case 116:	return new Point(46,29);
				case 117:	return new Point(46,30);
				case 118:	return new Point(46,31);
				case 119:	return new Point(46,32);
				case 120:	return new Point(46,33);
				case 121:	return new Point(46,34);
				case 122:	return new Point(46,35);
				case 123:	return new Point(44,25);
				case 124:	return new Point(44,26);
				case 125:	return new Point(44,27);
				case 126:	return new Point(44,28);
				case 127:	return new Point(44,29);
			}
            ThrowInvalidCharacterException("Code93", (char)c);
            return Point.Empty;
		}
		private string RetrievePattern(int c)
		{
			// Code93 characters follow are all 9 narrow bar elements wide
			// b - bar element
			// s - space element
			// 9 elements in all

            // #c1spell(off)
            switch (c)
			{
				case 0:		return "bsssbsbss";
				case 1:		return "bsbssbsss";
				case 2:		return "bsbsssbss";
				case 3:		return "bsbssssbs";
				case 4:		return "bssbsbsss";
				case 5:		return "bssbssbss";
				case 6:		return "bssbsssbs";
				case 7:		return "bsbsbssss";
				case 8:		return "bsssbssbs";
				case 9:		return "bssssbsbs";
				case 10:	return "bbsbsbsss";
				case 11:	return "bbsbssbss";
				case 12:	return "bbsbsssbs";
				case 13:	return "bbssbsbss";
				case 14:	return "bbssbssbs";
				case 15:	return "bbsssbsbs";
				case 16:	return "bsbbsbsss";
				case 17:	return "bsbbssbss";
				case 18:	return "bsbbsssbs";
				case 19:	return "bssbbsbss";
				case 20:	return "bsssbbsbs";
				case 21:	return "bsbsbbsss";
				case 22:	return "bsbssbbss";
				case 23:	return "bsbsssbbs";
				case 24:	return "bssbsbbss";
				case 25:	return "bsssbsbbs";
				case 26:	return "bbsbbsbss";
				case 27:	return "bbsbbssbs";
				case 28:	return "bbsbsbbss";
				case 29:	return "bbsbssbbs";
				case 30:	return "bbssbsbbs";
				case 31:	return "bbssbbsbs";
				case 32:	return "bsbbsbbss";
				case 33:	return "bsbbssbbs";
				case 34:	return "bssbbsbbs";
				case 35:	return "bssbbbsbs";
				case 36:	return "bssbsbbbs";
				case 37:	return "bbbsbsbss";
				case 38:	return "bbbsbssbs";
				case 39:	return "bbbssbsbs";
				case 40:	return "bsbbsbbbs";
				case 41:	return "bsbbbsbbs";
				case 42:	return "bbsbsbbbs";
				case 43:	return "bssbssbbs";
				case 44:	return "bbbsbbsbs";
				case 45:	return "bbbsbsbbs";
				case 46:	return "bssbbssbs";
				case 47:	return "bsbsbbbbs";
				case 48:	return "bsbsbbbbsb";
			}
            // #c1spell(on)
            ThrowInvalidCharacterException("Code93", (char)c);
            return null;
        }
	}
}
