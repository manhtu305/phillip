//----------------------------------------------------------------------------
// EncoderI2of5.cs
//
// Interleaved 2 of 5 is a numbers-only bar code. The symbol can be as 
// long as necessary to store the encoded data. The code is a high density 
// code that can hold up to 18 digits per inch when printed using a 7.5 mil 
// X dimension. A check digit is optional. 
// 
// The "Interleaved" part of the name comes from the fact that a digit is
// encoded in the bars and the next digit is encoded in the spaces. The encoded
// digits are "Interleaved" together. There are five bars, two of which are 
// wide and five spaces, two of which are wide. 
// 
// The symbol includes a quiet zone, the start character (narrow bar-narrow 
// space- narrow bar-narrow space), the encoded data, the stop character (Wide 
// bar-narrow space-narrow bar), and a trailing quiet zone. 
// 
// The X-dimension is the width of the smallest element in a bar code symbol. 
// The minimum X-dimension for an "open system" (a bar code label that will be
// read by scanners from outside your company) is 7.5 mils (a mil is 1/1000 inch)
// or 0.19 mm. The "wide" element is a multiple of the "narrow" element 
// and this multiple must remain the same throughout the symbol. This multiple 
// can range between 2.0 and 3.0 if the narrow element is greater than 20 mils. 
// If the narrow element is less than 20 mils, the ratio must exceed 2.2. Quiet
// zones must be at least 10X or at least .25 inches 
// 
// The height of the bars must be at least .15 times the symbol's length or .25 
// inches, whichever is larger.
//
// Copyright (C) 2004 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2004	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderI2of5
	/// </summary>
	internal class EncoderI2of5 : EncoderBase
	{
        // ** ctor
        internal EncoderI2of5(C1BarCode ctl) : base(ctl) { }

        // ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
			// validate string (should contain only digits)
			for (int i = 0; i < text.Length; i++)
			{
                if (text[i] < '0' || text[i] > '9') // <<B6>>
                    ThrowInvalidCharacterException("I2of5", text[i]);
			}

			// initialize
			_cursor = new Point(0,0);
			_totalWidth = 0;

			// draw the start character
			DrawPattern(g, br, "nnnn");

			// for each character in the message
			for (int i = 0; i < text.Length; i += 2)
			{
				// retrieve the next two digit number
				int n = text[i] - '0';
				n *= 10;
				if (i+1 < text.Length)
					n += text[i+1] - '0';

				// draw the two digit number
				DrawPattern(g, br, RetrievePattern(n));
			}

			// draw the stop character
			DrawPattern(g, br, "wnn");

			// return total width
			return _totalWidth;
		}
		
		// ** private
		private void DrawPattern(Graphics g, Brush br, string pattern)
		{
			// initialize X pixel value
			for (int i = 0; i < pattern.Length; i++)
			{
				// decide if narrow or wide bar
				int wid = (pattern[i] == 'n')
					? _barWidthNarrow
					: _barWidthWide;

				// draw this bar
				if (i % 2 == 0 && g != null)
					g.FillRectangle(br, _cursor.X, _cursor.Y, wid, _barHeight);

				// advance the starting position
				_cursor.X += wid;
				_totalWidth += wid;
			}
		}
		private string RetrievePattern(int c)
		{
            // #c1spell(off)
            switch (c)
			{
				case 0:		return "nnnnwwwwnn";
				case 1:		return "nwnnwnwnnw";
				case 2:		return "nnnwwnwnnw";
				case 3:		return "nwnwwnwnnn";
				case 4:		return "nnnnwwwnnw";
				case 5:		return "nwnnwwwnnn";
				case 6:		return "nnnwwwwnnn";
				case 7:		return "nnnnwnwwnw";
				case 8:		return "nwnnwnwwnn";
				case 9:		return "nnnwwnwwnn";
				case 10:	return "wnnnnwnwwn";
				case 11:	return "wwnnnnnnww";
				case 12:	return "wnnwnnnnww";
				case 13:	return "wwnwnnnnwn";
				case 14:	return "wnnnnwnnww";
				case 15:	return "wwnnnwnnwn";
				case 16:	return "wnnwnwnnwn";
				case 17:	return "wnnnnnnwww";
				case 18:	return "wwnnnnnwwn";
				case 19:	return "wnnwnnnwwn";
				case 20:	return "nnwnnwnwwn";
				case 21:	return "nwwnnnnnww";
				case 22:	return "nnwwnnnnww";
				case 23:	return "nwwwnnnnwn";
				case 24:	return "nnwnnwnnww";
				case 25:	return "nwwnnwnnwn";
				case 26:	return "nnwwnwnnwn";
				case 27:	return "nnwnnnnwww";
				case 28:	return "nwwnnnnwwn";
				case 29:	return "nnwwnnnwwn";
				case 30:	return "wnwnnwnwnn";
				case 31:	return "wwwnnnnnnw";
				case 32:	return "wnwwnnnnnw";
				case 33:	return "wwwwnnnnnn";
				case 34:	return "wnwnnwnnnw";
				case 35:	return "wwwnnwnnnn";
				case 36:	return "wnwwnwnnnn";
				case 37:	return "wnwnnnnwnw";
				case 38:	return "wwwnnnnwnn";
				case 39:	return "wnwwnnnwnn";
				case 40:	return "nnnnwwnwwn";
				case 41:	return "nwnnwnnnww";
				case 42:	return "nnnwwnnnww";
				case 43:	return "nwnwwnnnwn";
				case 44:	return "nnnnwwnnww";
				case 45:	return "nwnnwwnnwn";
				case 46:	return "nnnwwwnnwn";
				case 47:	return "nnnnwnnwww";
				case 48:	return "nwnnwnnwwn";
				case 49:	return "nnnwwnnwwn";
				case 50:	return "wnnnwwnwnn";
				case 51:	return "wwnnwnnnnw";
				case 52:	return "wnnwwnnnnw";
				case 53:	return "wwnwwnnnnn";
				case 54:	return "wnnnwwnnnw";
				case 55:	return "wwnnwwnnnn";
				case 56:	return "wnnwwwnnnn";
				case 57:	return "wnnnwnnwnw";
				case 58:	return "wwnnwnnwnn";
				case 59:	return "wnnwwnnwnn";
				case 60:	return "nnwnwwnwnn";
				case 61:	return "nwwnwnnnnw";
				case 62:	return "nnwwwnnnnw";
				case 63:	return "nwwwwnnnnn";
				case 64:	return "nnwnwwnnnw";
				case 65:	return "nwwnwwnnnn";
				case 66:	return "nnwwwwnnnn";
				case 67:	return "nnwnwnnwnw";
				case 68:	return "nwwnwnnwnn";
				case 69:	return "nnwwwnnwnn";
				case 70:	return "nnnnnwwwwn";
				case 71:	return "nwnnnnwnww";
				case 72:	return "nnnwnnwnww";
				case 73:	return "nwnwnnwnwn";
				case 74:	return "nnnnnwwnww";
				case 75:	return "nwnnnwwnwn";
				case 76:	return "nnnwnwwnwn";
				case 77:	return "nnnnnnwwww";
				case 78:	return "nwnnnnwwwn";
				case 79:	return "nnnwnnwwwn";
				case 80:	return "wnnnnwwwnn";
				case 81:	return "wwnnnnwnnw";
				case 82:	return "wnnwnnwnnw";
				case 83:	return "wwnwnnwnnn";
				case 84:	return "wnnnnwwnnw";
				case 85:	return "wwnnnwwnnn";
				case 86:	return "wnnwnwwnnn";
				case 87:	return "wnnnnnwwnw";
				case 88:	return "wwnnnnwwnn";
				case 89:	return "wnnwnnwwnn";
				case 90:	return "nnwnnwwwnn";
				case 91:	return "nwwnnnwnnw";
				case 92:	return "nnwwnnwnnw";
				case 93:	return "nwwwnnwnnn";
				case 94:	return "nnwnnwwnnw";
				case 95:	return "nwwnnwwnnn";
				case 96:	return "nnwwnwwnnn";
				case 97:	return "nnwnnnwwnw";
				case 98:	return "nwwnnnwwnn";
				case 99:	return "nnwwnnwwnn";
			}
            // #c1spell(on)
            ThrowInvalidCharacterException("I2of5", (char)c);
            return null;
		}
	}
}
