//----------------------------------------------------------------------------
// EncoderPlanet.cs
//
// Planet is similar to PostNet.
//
// Copyright (C) 2004 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		Feb 2004	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderPlanet
	/// </summary>
	internal class EncoderPlanet : EncoderPostNet
	{
		// ** ctor
        internal EncoderPlanet(C1BarCode ctl) : base(ctl) { }

		// ** overrides
		override protected string RetrievePattern(char c)
		{
			switch (c)
			{
				case '0':	return "wwWNW";
				case '1':	return "WWWnw";
				case '2':	return "WWwNw";
				case '3':	return "WWwnW";
				case '4':	return "WwWNw";
				case '5':	return "WwWnW";
				case '6':	return "WwwNW";
				case '7':	return "wWWNw";
				case '8':	return "wWWnW";
				case '9':	return "wWwNW";
			}
			throw new Exception("Invalid character for Planet");
		}
	}
}
