//----------------------------------------------------------------------------
// EncoderUpcA.cs
//
// Copyright (C) 2006 ComponentOne LLC
//----------------------------------------------------------------------------
// Status		Date		By			Comments
//----------------------------------------------------------------------------
// Created		June 2005	Bernardo	-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;

namespace C1.Win.C1BarCode
{
	/// <summary>
	/// EncoderUpcA
	/// </summary>
	internal class EncoderUpcA : EncoderEan13
	{
        // ** ctor
        internal EncoderUpcA(C1BarCode ctl) : base(ctl) { }

		// ** overrides
		override protected int Draw(Graphics g, Brush br, string text)
		{
            // allow user to supply the correct check digit
            text = StripOptionalControlDigit(text);

            // sanity
            if (BadText(text))
                throw new ArgumentException("UpcA requires exactly 11 digits.");

            // prepend a zero and use the base class
            return base.Draw(g, br, "0" + text);
		}
		override protected void DrawText(Graphics g, string text, Font font, Brush br, Rectangle rc)
		{
            // allow user to supply the correct check digit
            text = StripOptionalControlDigit(text);
            
            // skip invalid stuff
			if (BadText(text))
			{
				base.DrawText(g, text, font, br, rc);
				return;
			}

            // draw control digit on the left (x = 0, width = 9 / 89) <<B13>>
            string digits = text.Substring(0, 1);
            Rectangle r = rc;
            r.Y -= font.Height / 2;
            r.Width = rc.Width * 9 / 89;
            DrawTextBase(g, digits, font, br, r);

            // draw check digit on the right (x = 80, width = 9 / 89)
            digits = GetCheckDigit("0" + text).ToString();
            r.X = rc.Width * 85 / 89;
            r.Width = rc.Width * 9 / 89;
            DrawTextBase(g, digits, font, br, r);

            // draw left part of code (x = 12, width = 30 / 89)
            digits = text.Substring(1, 5);
            r.Y = rc.Y;
            r.X = rc.Width * 12 / 89;
            r.Width = rc.Width * 30 / 89;
            DrawTextBase(g, digits, font, br, r);

            // draw right part of code (x = 47, width = 30 / 89)
            digits = text.Substring(6, 5);
            r.X = rc.Width * 47 / 89;
            DrawTextBase(g, digits, font, br, r);
        }   

		// ** protected
        protected string StripOptionalControlDigit(string text)
        {
            if (text.Length == 12)
            {
                string newText = text.Substring(0, 11);
                char checkDigit = GetCheckDigit("0" + newText);
                if (text[11] == checkDigit)
                    return newText;
            }
            return text;
        }
        protected bool BadText(string text)
		{
			if (text.Length != 11) return true;
			foreach (char c in text)
			{
				if (!char.IsDigit(c))
					return true;
			}
			return false;
		}
	}
}
