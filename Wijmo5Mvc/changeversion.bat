
@echo off
setlocal enabledelayedexpansion

set oldver=0.20171.115
set newver=0.20172.55555
set tfPath="C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\tf.exe"

@if '%1'=='' (
    if exist %tfPath% %tfPath% edit "%~dpnx0"
)

@if not '%1'=='' (
    @echo process: %1
    if exist %tfPath% %tfPath% edit %1
    if exist %1 attrib -R -S -H %1
    %~dp0replace.vbs %1 %oldver% %newver%
    goto :EOF
)

call :createScript

forfiles /p C1.AspNetCore.Mvc /s /m project.json /c "cmd /c %0 @path"
forfiles /p C1.AspNetCore.Mvc /s /m updatePackage.cmd /c "cmd /c %0 @path"
forfiles /p C1.Web.Mvc /s /m AssemblyInfo.cs /c "cmd /c %0 @path"
forfiles /p C1.Web.Mvc\Client\Shared /s /m Control.ts /c "cmd /c %0 @path"
forfiles /p C1.Scaffolder /s /m AssemblyInfo.cs /c "cmd /c %0 @path"
forfiles /p C1.Scaffolder /s /m source.extension.vsixmanifest /c "cmd /c %0 @path"
forfiles /p Samples /s /m project.json /c "cmd /c %0 @path"
forfiles /p Samples\CS\ASPNETCore /s /m *.csproj /c "cmd /c %0 @path"
forfiles /p Samples /s /m ASPNetMVCCollectionView101.csproj /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m project_json /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m source.extension.vsixmanifest /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m AssemblyInfo.cs /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m *.vstemplate /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m *.csproj /c "cmd /c %0 @path"
forfiles /p ItemTemplates /s /m *.vstemplate /c "cmd /c %0 @path"
forfiles /p ItemTemplates /s /m source.extension.vsixmanifest /c "cmd /c %0 @path"
forfiles /p ItemTemplates /s /m AssemblyInfo.cs /c "cmd /c %0 @path"

if exist replace.vbs del /F /Q replace.vbs
@echo done.
pause
goto :EOF


:createScript
    if exist replace.vbs attrib -R -S -H replace.vbs
    if exist replace.vbs del /F /Q replace.vbs
    @echo filePath = wscript.arguments(0)                           > replace.vbs
    @echo oldver = wscript.arguments(1)                             >> replace.vbs
    @echo newver = wscript.arguments(2)                             >> replace.vbs
    @echo set fso = CreateObject("Scripting.FileSystemObject")      >> replace.vbs
    @echo Set readerStream = CreateObject("Adodb.Stream")           >> replace.vbs
    @echo readerStream.Type = 2                                     >> replace.vbs
    @echo readerStream.charset = "utf-8"                            >> replace.vbs
    @echo readerStream.Open                                         >> replace.vbs
    @echo readerStream.LoadFromFile filePath                        >> replace.vbs
    @echo text = readerStream.readtext                              >> replace.vbs
    @echo readerStream.close                                        >> replace.vbs
    @echo If InStr(text, oldver) ^>0 Then                           >> replace.vbs
    @echo     text = Replace(text, oldver, newver, 1, -1, 0)        >> replace.vbs
    @echo     fso.getfile(filePath).attributes = 0                  >> replace.vbs
    @echo     fso.deleteFile(filePath)                              >> replace.vbs
    @echo     Set writerStream = CreateObject("Adodb.Stream")       >> replace.vbs
    @echo     writerStream.Type = 2                                 >> replace.vbs
    @echo     writerStream.charset = "utf-8"                        >> replace.vbs
    @echo     writerStream.Open                                     >> replace.vbs
    @echo     writerStream.WriteText text                           >> replace.vbs
    @echo     writerStream.SaveToFile filePath                      >> replace.vbs
    @echo     writerStream.close                                    >> replace.vbs
    @echo End If                                                    >> replace.vbs
    @echo set fso = Nothing                                         >> replace.vbs
goto :EOF