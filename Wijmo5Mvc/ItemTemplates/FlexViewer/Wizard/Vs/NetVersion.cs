﻿namespace C1.Web.Mvc.FlexViewerWizard
{
    internal static class NetVersion
    {
        public const string net451 = "net451";
        public const string net462 = "net462";
        public const string netcoreapp = "netcoreapp";
        public const string netcoreapp10 = "netcoreapp1.0";
        public const string netcoreapp11 = "netcoreapp1.1";
        public const string netcoreapp20 = "netcoreapp2.0";
        public const string netstandard = "netstandard";
    }
}
