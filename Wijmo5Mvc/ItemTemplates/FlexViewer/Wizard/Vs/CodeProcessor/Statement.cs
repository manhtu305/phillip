﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class Statement
    {
        public Statement()
        {
            Start = -1;
            End = -1;
        }

        public int Start { get; set; }

        public int End { get; set; }
    }
}
