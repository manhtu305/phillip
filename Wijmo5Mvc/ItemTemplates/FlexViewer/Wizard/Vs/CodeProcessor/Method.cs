﻿using System.Collections.Generic;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class Method
    {
        public Method()
        {
            Start = -1;
            End = -1;
            NestedIndent = string.Empty;
            Name = string.Empty;
        }

        public string NestedIndent { get; set; }

        public string Name { get; set; }

        public int Start { get; set; }

        public int End { get; set; }

        public IEnumerable<Argument> Arguments { get; set; }
    }
}
