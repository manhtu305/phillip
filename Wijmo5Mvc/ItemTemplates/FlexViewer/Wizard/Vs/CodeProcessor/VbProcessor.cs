﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class VbProcessor : CodeProcessor
    {
        private readonly static Regex ArgumentNamesRegex = new Regex(@"(?<=\ )(\w*)(?=\ As)");
        private readonly static Regex ArgumentTypesRegex = new Regex(@"(?<=As\ )(\w*)(?=\b)");

        public override string CommentsPrefix
        {
            get { return "'"; }
        }

        public override string FileExtension
        {
            get
            {
                return ".vb";
            }
        }

        public override string NewInstanceKeyword
        {
            get { return "New"; }
        }

        public override string CreateStatement(string content)
        {
            return content;
        }

        protected override string CreateNoReturnMethodHeader(string methodName)
        {
            return string.Format("Public Sub {0}(", methodName);
        }

        protected override string CreateUsingCore(string content)
        {
            return "Imports " + content;
        }

        protected override int EndScopeCount(string line)
        {
            return line.Contains("End Sub") ? 1 : 0;
        }

        protected override int StartScopeCount(string line)
        {
            return (line.Contains("Sub") && !line.Contains("End Sub")) ? 1 : 0;
        }

        protected override IEnumerable<Argument> GetArguments(string content)
        {
            var arguments = new List<Argument>();
            var typeMatches = ArgumentTypesRegex.Matches(content);
            var nameMatches = ArgumentNamesRegex.Matches(content);
            for (int i = 0, length = Math.Min(typeMatches.Count, nameMatches.Count); i < length; i++)
            {
                arguments.Add(new Argument(nameMatches[i].Value, typeMatches[i].Value));
            }

            return arguments;
        }

        public override string CreateActionArg(string content)
        {
            return "AddressOf " + content;
        }
    }
}
