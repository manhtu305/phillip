﻿namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class Argument
    {
        public Argument(string name, string type)
        {
            Name = name;
            Type = type;
        }

        public string Name { get; private set; }

        public string Type { get; private set; }
    }
}
