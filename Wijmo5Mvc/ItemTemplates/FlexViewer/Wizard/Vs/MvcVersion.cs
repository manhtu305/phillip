﻿namespace C1.Web.Mvc.FlexViewerWizard
{
    internal enum MvcVersion
    {
        Unknown,
        Mvc3,
        Mvc4,
        Mvc5,
        AspNetCore
    }
}
