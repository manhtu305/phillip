﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal abstract class StartupHelperBase
    {
        protected abstract string UseProvidersName { get; }
        protected abstract string AddDiskStorageName { get; }

        public void ConfigStartup(IWizardHelper wizardHelper, string startupItem, Func<string> rootPathGetter, Action<Method> configMethodProcess = null)
        {
            var allLines = new List<string>(File.ReadAllLines(startupItem));
            var method = wizardHelper.CodeProcessor.FindMethod(wizardHelper.StartupConfigMethodName, allLines);
            if (configMethodProcess != null)
            {
                configMethodProcess(method);
            }
            // the result of rootPathGetter() depends on the result of configMethodProcess().
            var rootPath = rootPathGetter();

            var argument = (method.Arguments ?? new Argument[] { }).FirstOrDefault(a => a.Type.EndsWith(wizardHelper.StartupAppTypeName));
            var appArgName = argument == null ? "app" : argument.Name;

            ResolveLines(wizardHelper, allLines, method, rootPath, appArgName);
            File.WriteAllLines(startupItem, allLines);
        }

        protected virtual void ResolveLines(IWizardHelper wizardHelper, IList<string> allLines, Method method, string rootPath, string appArgName)
        {
            ResolveAddDiskStorage(wizardHelper, allLines, method, rootPath, appArgName);
            ResolveSystemUsing(wizardHelper, allLines, method);
        }

        private void ResolveAddDiskStorage(IWizardHelper wizardHelper, IList<string> allLines, Method method, string rootPath, string appArgName)
        {
            if (!wizardHelper.Model.ShouldAddDiskStorage) return;

            var codeProcessor = wizardHelper.CodeProcessor;
            var addStorageWithRootIndex = -1;
            var addStorageWithEmptyIndex = -1;
            var addStorageWithEmptyPrefixes = new[]
            {
                AddDiskStorageName + "(\"\"",
                AddDiskStorageName + "(string.Empty",
                AddDiskStorageName + "(String.Empty"
            };
            for (int i = method.Start; i < method.End; i++)
            {
                var line = allLines[i];
                if (addStorageWithRootIndex == -1)
                {
                    addStorageWithRootIndex = line.Contains(AddDiskStorageName + "(\"~\"") ? i : -1;
                }

                if (addStorageWithEmptyIndex == -1)
                {
                    addStorageWithEmptyIndex = addStorageWithEmptyPrefixes.Any(line.Contains) ? i : -1;
                }

                if (addStorageWithRootIndex != -1 && addStorageWithEmptyIndex != -1)
                {
                    break;
                }
            }

            if (addStorageWithEmptyIndex == -1)
            {
                addStorageWithEmptyIndex = method.End;
                var statement = codeProcessor.CreateStatement(
                    string.Format("{0}{1}.{2}().{3}(\"\", {4})", method.NestedIndent, appArgName, UseProvidersName, AddDiskStorageName, rootPath));
                allLines.Insert(method.End, statement);
                ResolveMethodRange(method, addStorageWithEmptyIndex);
            }

            if (addStorageWithRootIndex == -1)
            {
                addStorageWithRootIndex = addStorageWithEmptyIndex;
                var statement = codeProcessor.CreateStatement(
                    string.Format("{0}{1}.{2}().{3}(\"~\", {4})", method.NestedIndent, appArgName, UseProvidersName, AddDiskStorageName, rootPath));
                allLines.Insert(addStorageWithRootIndex, statement);
                ResolveMethodRange(method, addStorageWithRootIndex);
            }
        }

        private static void ResolveSystemUsing(IWizardHelper wizardHelper, IList<string> allLines, Method method)
        {
            var sysUsingDirective = wizardHelper.CodeProcessor.CreateUsing("System");
            var hasSystemUsing = FindString(sysUsingDirective, allLines, 0, method.Start - 1);
            if (!hasSystemUsing)
            {
                allLines.Insert(0, sysUsingDirective);
                ResolveMethodRange(method, 0);
            }
        }

        protected static void ResolveMethodRange(Method method, int insertIndex)
        {
            if (insertIndex <= method.Start)
            {
                method.Start += 1;
                method.End += 1;
            }
            else if (insertIndex <= method.End)
            {
                method.End += 1;
            }
        }

        private static bool FindString(string text, IList<string> lines, int startIndex, int endIndex)
        {
            for (int i = startIndex; i <= endIndex; i++)
            {
                if (lines[i].Contains(text)) return true;
            }

            return false;
        }
    }
}
