﻿namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class PdfViewerStartupHelper : StartupHelperBase
    {
        protected override string AddDiskStorageName
        {
            get { return "AddDiskStorage"; }
        }

        protected override string UseProvidersName
        {
            get { return "UseStorageProviders"; }
        }
    }
}
