﻿using EnvDTE;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using C1.Web.Mvc.FlexViewerWizard.Localization;

namespace C1.Web.Mvc.FlexViewerWizard
{
    /// <summary>
    /// For mock up convenience.
    /// The interface contains members of <see cref="WizardHelperBase"/> 
    /// which will be used in <see cref="StartupHelperBase"/> class.
    /// </summary>
    internal interface IWizardHelper
    {
        CodeProcessor CodeProcessor { get; }
        ViewerSettingsBase Model { get; }
        string StartupConfigMethodName { get; }
        string StartupAppTypeName { get; }
    }

    internal abstract class WizardHelperBase : IWizardHelper
    {
        public const string StartupItemName = "Startup";
        public const string CsExt = ".cs";
        public const string VbExt = ".vb";
        public const string ResPrefix = "C1.Web.Mvc.FlexViewerWizard.Templates.";

        protected WizardHelperBase(ProjectEx project, ViewerSettingsBase model, StartupHelperBase startupHelper, Dictionary<string, string> replacements)
        {
            Project = project;
            Model = model;
            StartupHelper = startupHelper;
            Replacements = replacements;
            CodeProcessor = CodeProcessor.Create(project.VsLanguage);
        }

        public CodeProcessor CodeProcessor { get; private set; }

        public Dictionary<string, string> Replacements { get; private set; }

        public ViewerSettingsBase Model { get; private set; }

        public StartupHelperBase StartupHelper { get; private set; }

        protected ProjectEx Project { get; private set; }

        public void RunStarted(object[] customParams)
        {
            AddDocumentFile();
            AddReplacements();
            RunStartedCore(customParams);
            AddReplacements(Replacements, Model);
            AdjustReplacements();
        }

        protected virtual void RunStartedCore(object[] customParams)
        {
            EnsureViewImports();
            EnsureLicense();

            var model = Model;
            if (!model.CurrentProjectAsService)
            {
                const string noWebApiTemplateSuffix = ".noWebApi";
                var template = customParams[0] as string;
                var noWebApiTemplate = Path.Combine(Path.GetDirectoryName(template),
                    Path.GetFileNameWithoutExtension(template) + noWebApiTemplateSuffix + Path.GetExtension(template));
                if (!File.Exists(noWebApiTemplate))
                {
                    var doc = XDocument.Load(template);
                    RemoveWebApiDependencies(doc);
                    doc.Save(noWebApiTemplate);
                }

                customParams[0] = noWebApiTemplate;
                return;
            }

            AdjustWebConfig();
            ConfigStartup();
        }

        protected virtual void AdjustReplacements()
        {

        }

        /// <summary>
        /// Add replacements before ensuring items.
        /// </summary>
        private void AddReplacements()
        {
            // used by Startup and WebApiConfig templates.
            Replacements.Add("$projectdefaultnamespace$", Project.DefaultNamespace);
        }

        protected abstract void RemoveWebApiDependencies(XDocument doc);

        private void ConfigStartup()
        {
            EnsureStartupItem();
            ConfigStartupCore();
        }

        protected abstract void ConfigStartupCore();

        protected virtual void EnsureStartupItem()
        {
        }

        public virtual void RunFinished()
        {
        }

        protected virtual void EnsureLicense()
        {
        }

        private void AddDocumentFile()
        {
            var model = Model;
            if (!model.ShouldAddDocuemntFile)
            {
                return;
            }

            var resolvedFile = ResolveFileConflict(model.DestFilePathOfCopy);
            model.InnerDocumentFile = Path.Combine(Path.GetDirectoryName(model.InnerDocumentFile), Path.GetFileName(resolvedFile));

            var file = model.DestFilePathOfCopy;
            Directory.CreateDirectory(Path.GetDirectoryName(file));
            File.Copy(model.OriginInnerDocumentFile, file);
            Project.AddItem(file.Substring(Project.ProjectFolder.Length));
        }

        protected virtual void AdjustWebConfig(XDocument doc)
        {
            var configNode = doc.XPathSelectElement(@"/configuration");
            var webServerNode = configNode.XPathSelectElement(@"system.webServer");
            if (webServerNode == null)
            {
                webServerNode = new XElement("system.webServer");
                configNode.Add(webServerNode);
            }

            var handlersNode = webServerNode.XPathSelectElement(@"handlers");
            if (handlersNode == null)
            {
                handlersNode = new XElement("handlers");
                webServerNode.Add(handlersNode);
            }

            const string webDAVName = "WebDAV";
            if (handlersNode.XPathSelectElement(@"remove[@name='" + webDAVName + "']") == null)
            {
                var ele = new XElement("remove");
                ele.SetAttributeValue("name", webDAVName);
                handlersNode.Add(ele);
            }

            var modulesNode = webServerNode.XPathSelectElement(@"modules");
            if (modulesNode == null)
            {
                modulesNode = new XElement("modules");
                webServerNode.Add(modulesNode);
            }

            const string webDAVModuleName = "WebDAVModule";
            if (modulesNode.XPathSelectElement(@"remove[@name='" + webDAVModuleName + "']") == null)
            {
                var ele = new XElement("remove");
                ele.SetAttributeValue("name", webDAVModuleName);
                modulesNode.Add(ele);
            }
        }

        private void AdjustWebConfig()
        {
            var file = Path.Combine(Project.ProjectFolder, "Web.config");
            if (!File.Exists(file))
            {
                // for VS2017 ASP.NET Core project, there is no web.config by default.
                if (Project.MvcVersion == MvcVersion.AspNetCore && Project.VsVersion >= new Version("15.0"))
                {
                    return;
                }

                throw new FileNotFoundException(Resources.FileNotFoundMessage, file);
            }

            var doc = XDocument.Load(file);
            AdjustWebConfig(doc);
            doc.Save(file);
        }

        protected virtual void EnsureViewImports()
        {
            EnsureNamespaces(new[] { "C1.Web.Mvc", "C1.Web.Mvc.Fluent", "C1.Web.Mvc.Viewer", "C1.Web.Mvc.Viewer.Fluent" });
        }

        protected abstract void EnsureNamespaces(IEnumerable<string> namespaces);

        public static string FindItem(ProjectItems projectItems, string startsWith)
        {
            foreach (var projectItem in projectItems.Cast<ProjectItem>())
            {
                if (projectItem.Name.StartsWith(startsWith, StringComparison.OrdinalIgnoreCase))
                {
                    return projectItem.Properties.Item("FullPath").Value.ToString();
                }
            }

            return null;
        }

        public static string GetResText(string name)
        {
            var assembly = typeof(ProjectEx).Assembly;
            using (var stream = assembly.GetManifestResourceStream(name))
            {
                stream.Position = 0;
                var reader = new StreamReader(stream);
                return reader.ReadToEnd();
            }
        }

        public static void CreateItemFromResource(string resKey, string path, Dictionary<string, string> replacementsDictionary)
        {
            var text = GetResText(resKey);
            if (replacementsDictionary != null)
            {
                foreach (var keyValuePair in replacementsDictionary)
                {
                    text = text.Replace(keyValuePair.Key, keyValuePair.Value);
                }
            }

            File.AppendAllText(path, text);
        }

        public static string ResolveFileConflict(string file)
        {
            var resolvedFile = file;
            var folder = Path.GetDirectoryName(file);
            var fileNameWithoutExt = Path.GetFileNameWithoutExtension(file);
            var ext = Path.GetExtension(file);
            int index = 1;
            const string resolvedFileNamePattern = "{0}_{1}{2}";
            while (File.Exists(resolvedFile))
            {
                resolvedFile = Path.Combine(folder, string.Format(resolvedFileNamePattern, fileNameWithoutExt, index++, ext));
            }

            return resolvedFile;
        }

        public abstract string StartupConfigMethodName { get; }

        public abstract string StartupAppTypeName { get; }

        protected void ConfigStartup(Func<string> rootPathGetter, Action<Method> configMethodProcess = null)
        {
            var startupItem = FindItem(Project.Project.ProjectItems, StartupItemName);
            StartupHelper.ConfigStartup(this, startupItem, rootPathGetter, configMethodProcess);
        }

        public static WizardHelperBase Create(ProjectEx project, ViewerSettingsBase model, StartupHelperBase startupHelper, Dictionary<string, string> replacements)
        {
            if (project.MvcVersion == MvcVersion.AspNetCore)
            {
                return new AspNetCoreWizardHelper(project, model, startupHelper, replacements);
            }

            return new AspNetWizardHelper(project, model, startupHelper, replacements);
        }

        private static void AddReplacements(Dictionary<string, string> replacementsDictionary, ViewerSettingsBase model)
        {
            if (replacementsDictionary == null)
            {
                throw new ArgumentNullException("replacementsDictionary");
            }

            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            var properties = model.GetType().GetProperties().Where(p => !p.GetCustomAttributes<ReplacementIgnoreAttribute>().Any());
            foreach (var prop in properties)
            {
                var value = prop.GetValue(model);
                if (value != null)
                {
                    replacementsDictionary.Add(string.Format("${0}$", prop.Name.ToLower()), value.ToString());
                }
            }
        }
    }
}
