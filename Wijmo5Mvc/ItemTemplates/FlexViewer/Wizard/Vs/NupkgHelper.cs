﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using EnvDTE;
//using Microsoft.VisualStudio.ComponentModelHost;
//using NuGet.VisualStudio;

//namespace C1.Web.Mvc.FlexViewerWizard
//{
//    // This class is not used currently.
//    internal class NupkgHelper
//    {
//        private readonly IComponentModel _componentModel;
//        private readonly IVsPackageInstallerServices _service;

//        public NupkgHelper(Project project, IServiceProvider provider)
//        {
//            Project = project;
//            // There is a InvalidCastException in vs2012. We can use reflection instead of casting to IComponentModel.
//            _componentModel = (IComponentModel)provider.GetService(typeof(SComponentModel));
//            _service = _componentModel.GetService<IVsPackageInstallerServices>();
//        }

//        public Project Project { get; private set; }

//        public void InstallPackage(string package, string version, string src = null)
//        {
//            try
//            {
//                if (string.IsNullOrEmpty(src))
//                {
//                    src = @"http://packages.nuget.org";
//                }

//                var service = _componentModel.GetService<IVsPackageInstaller>();
//                //TODO: it takes long time when the nextwork is bad.
//                service.InstallPackage(src, Project, package, version, true);
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine(ex.Message);
//            }
//        }

//        private IEnumerable<IVsPackageMetadata> Packages
//        {
//            get
//            {
//                return _service.GetInstalledPackages();
//            }
//        }

//        public bool IsPackageInstalled(string name, string versionStr = null)
//        {
//            return Packages.Any((p =>
//            {
//                if (!string.Equals(name, p.Id))
//                {
//                    return false;
//                }

//                if (string.IsNullOrEmpty(versionStr))
//                {
//                    return true;
//                }

//                var version = new Version(versionStr);
//                var existVersion = new Version(p.VersionString);
//                return existVersion >= version;
//            }));
//        }

//        public void EnsurePackage(string name, string version, string src = null)
//        {
//            if (IsPackageInstalled(name, version))
//            {
//                return;
//            }

//            InstallPackage(name, version, src);
//        }
//    }
//}
