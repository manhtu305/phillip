﻿using System;
using System.Collections.Generic;
using System.IO;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using EnvDTE80;
using System.Xml.Linq;
using System.Linq;
using System.Xml.XPath;
using C1.Web.Mvc.FlexViewerWizard.Localization;
using VSLangProj;
using Newtonsoft.Json.Linq;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class ProjectEx
    {
        //private NupkgHelper _nupkgHelper;

        public ProjectEx(object automationObject)
        {
            VsVersion = new Version(((dynamic)automationObject).Version.ToString());
            var serviceProvider = automationObject as Microsoft.VisualStudio.OLE.Interop.IServiceProvider;
            ServiceProvider = new ServiceProvider(serviceProvider);
            Project = GetCurrentProject(ServiceProvider);
            MvcVersion = GetMvcVersion();
            NetVersions = MvcVersion == MvcVersion.AspNetCore ? GetNetVersions() : new string[] { };
            VsLanguage = GetVsLanguage();
            UpdateFoldersInfo();
        }

        private string[] GetNetVersions()
        {
            var versions = new List<string>();
            var jsonFile = Path.Combine(ProjectFolder, "project.json");
            if (File.Exists(jsonFile))
            {
                var json = File.ReadAllText(jsonFile);
                var obj = JObject.Parse(json);
                var frameworks = obj["frameworks"];
                foreach (var property in frameworks.Children<JProperty>())
                {
                    versions.Add(property.Name);
                }
            }
            else
            {
                var projectFile = XDocument.Load(ProjectFile);
                var targetFrameworkNode = projectFile
                    .XPathSelectElements(@"//*[local-name()='TargetFramework']").FirstOrDefault();
                if(targetFrameworkNode != null)
                {
                    versions.Add(targetFrameworkNode.Value);
                }
            }

            return versions.ToArray();
        }

        //private NupkgHelper NupkgHelper
        //{
        //    get
        //    {
        //        return _nupkgHelper = new NupkgHelper(Project, ServiceProvider);
        //    }
        //}

        public Version VsVersion
        {
            get;
            private set;
        }

        public string DefaultNamespace
        {
            get
            {
                return Project.Properties.Item("DefaultNamespace").Value as string;
            }
        }

        public Reference FindReference(string name)
        {
            var vsProj = Project.Object as VSProject;
            foreach(Reference reference in vsProj.References)
            {
                if (reference.Name == name)
                {
                    return reference;
                }
            }

            return null;
        }

        public void AddReference(string name)
        {
            var vsProj = Project.Object as VSProject;
            vsProj.References.Add(name);
        }

        private VsLanguage GetVsLanguage()
        {
            if (ProjectFile.EndsWith(".vbproj", StringComparison.OrdinalIgnoreCase))
            {
                return VsLanguage.Vb;
            }

            return VsLanguage.CSharp;
        }

        private MvcVersion GetMvcVersion()
        {
            if (IsAspNetCore())
            {
                return MvcVersion.AspNetCore;
            }

            var file = Path.Combine(ProjectFolder, "Web.config");
            if (!File.Exists(file))
            {
                throw new FileNotFoundException(Resources.FileNotFoundMessage, file);
            }

            var webConfig = XDocument.Load(file);
            var aspNetCoreHandlerNode = webConfig
                .XPathSelectElement(@"/configuration/system.webServer/handlers/add[@name='aspNetCore']");
            if(aspNetCoreHandlerNode != null)
            {
                return MvcVersion.AspNetCore;
            }

            var mvcIdNode = webConfig
                .XPathSelectElement(@"/configuration/runtime/*[local-name()='assemblyBinding']/*[local-name()='dependentAssembly']/*[local-name()='assemblyIdentity' and @name='System.Web.Mvc']");

            if (mvcIdNode == null)
            {
                return MvcVersion.Mvc3;
            }

            var versionNode = mvcIdNode.Parent.XPathSelectElement(@"*[local-name()='bindingRedirect']");
            if (versionNode == null)
            {
                return MvcVersion.Mvc3;
            }

            var versionStr = versionNode.Attribute("newVersion");
            if (versionStr == null || string.IsNullOrEmpty(versionStr.Value))
            {
                return MvcVersion.Mvc3;
            }

            var version = new Version(versionStr.Value);
            switch (version.Major)
            {
                case 4:
                    return MvcVersion.Mvc4;
                case 5:
                    return MvcVersion.Mvc5;
                case 3:
                default:
                    return MvcVersion.Mvc3;
            }
        }

        private bool IsAspNetCore()
        {
            // for VS2015, the Asp.Net Core project file is "*.xproj".
            if (Project.FullName.ToLower().EndsWith(".xproj"))
                return true;

            // for VS2017, the Asp.Net Core project file changes to "*.csproj".
            // We think it's an Asp.Net Core project if it has the reference "Microsoft.AspNetCore.*".
            var vsproject = Project.Object as VSLangProj.VSProject;
            if (vsproject != null)
            {
                foreach (VSLangProj.Reference reference in vsproject.References)
                {
                    if (reference.Name.ToLower().StartsWith("microsoft.aspnetcore."))
                        return true;
                }
            }

            return false;
        }

        public VsLanguage VsLanguage
        {
            get;
            private set;
        }

        public MvcVersion MvcVersion
        {
            get;
            private set;
        }

        public IServiceProvider ServiceProvider { get; private set; }

        public Project Project { get; private set; }

        private static Project GetCurrentProject(IServiceProvider serviceProvider)
        {
            var projectItem = serviceProvider.GetService(typeof(ProjectItem)) as ProjectItem;
            if (projectItem != null)
                return projectItem.ContainingProject;
            var dtE2 = serviceProvider.GetService(typeof(SDTE)) as DTE2;
            if (dtE2 != null)
            {
                var array = (Array)dtE2.ActiveSolutionProjects;
                if (array.Length > 0)
                    return (Project)array.GetValue(0);
            }

            return null;
        }

        //internal void EnsureNupkg(string name, string version, string src = null)
        //{
        //    NupkgHelper.EnsurePackage(name, version, src);
        //}

        public void AddItem(string name)
        {
            name = Path.Combine(ProjectFolder, name.TrimStart('/', '\\')).Replace('/', '\\');
            Project.ProjectItems.AddFromFile(name);
        }

        public bool ItemExists(string name)
        {
            var path = Path.Combine(ProjectFolder, name);
            return Directory.Exists(path) ||  File.Exists(path);
        }

        public string ProjectFolder
        {
            get
            {
                try
                {
                    return Project.Properties.Item("FullPath").Value as string;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public string ProjectFile
        {
            get
            {
                try
                {
                    return Project.FileName;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public string[] NetVersions { get; private set; }

        public bool IsRazorPages { get; private set; }

        public string AreaName { get; private set; }

        private void UpdateFoldersInfo()
        {
            AreaName = GetAreaName();
            IsRazorPages = GetIsRazorPages();
        }

        private bool GetIsRazorPages()
        {
            var pagesFolder = string.IsNullOrEmpty(AreaName) ? "Pages" : "Areas/" + AreaName + "/Pages";
            bool hasPagesFolder = ItemExists(pagesFolder);
            return hasPagesFolder && IsAspNetCore() && !GetIsInViews();
        }

        private string GetAreaName()
        {
            var projectItem = GetSelectedProjectItem();
            if (projectItem == null) return string.Empty;

            var relativeName = GetRelativeName(projectItem);
            if (string.IsNullOrEmpty(relativeName)) return string.Empty;

            var names = relativeName.Split('/', '\\');
            if(names.Length>1 && string.Equals(names[0], "areas", StringComparison.OrdinalIgnoreCase))
            {
                return names[1];
            }

            return string.Empty;
        }

        private bool GetIsInViews()
        {
            var projectItem = GetSelectedProjectItem();
            if (projectItem == null) return false;

            var relativeName = GetRelativeName(projectItem);
            if (string.IsNullOrEmpty(relativeName)) return false;

            var names = relativeName.Split('/', '\\');
            if(string.Equals(names[0], "views", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            if(names.Length>2 && string.Equals(names[0], "areas", StringComparison.OrdinalIgnoreCase))
            {
                if (string.Equals(names[2], "views", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        private ProjectItem GetSelectedProjectItem()
        {
            var dte2 = ServiceProvider.GetService(typeof(SDTE)) as DTE2;
            if (dte2 == null) return null;

            var selectedItems = dte2.ToolWindows.SolutionExplorer.SelectedItems as Array;
            if (selectedItems == null || selectedItems.Length != 1) return null;

            var selectedItem = selectedItems.GetValue(0) as UIHierarchyItem;
            if (selectedItem == null) return null;

           return selectedItem.Object as ProjectItem;
        }

        private string GetFullName(ProjectItem pi)
        {
            return (string)pi.Properties.Item("FullPath").Value;
        }

        private string GetRelativeName(ProjectItem pi)
        {
            var fullName = GetFullName(pi);
            if (string.IsNullOrEmpty(fullName)) return string.Empty;

            return fullName.Substring(ProjectFolder.TrimEnd('/', '\\').Length).TrimStart('/', '\\');
        }
    }
}
