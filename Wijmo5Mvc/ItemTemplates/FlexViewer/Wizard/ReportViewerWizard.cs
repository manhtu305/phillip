﻿using System.Windows;

namespace C1.Web.Mvc.FlexViewerWizard
{
    public class ReportViewerWizard : ViewerWizardBase
    {
        private readonly static StartupHelperBase _startupHelper = new ReportViewerStartupHelper();

        internal override ViewerSettingsBase CreateModel(ProjectEx project)
        {
            ReportViewerSettings model;
            if (CheckProjectServiceSupported(project))
            {
                model = new ReportViewerSettings(true, project.ProjectFolder,
                    project.MvcVersion == MvcVersion.Mvc5 ? "Content\\ReportsRoot" : "wwwroot\\ReportsRoot");
            }
            else
            {
                model = new ReportViewerSettings();
            }

            return model;
        }

        protected override Window CreateSettingsWindow(ViewerSettingsBase model)
        {
            return new ReportViewerSettingsWindow(model as ReportViewerSettings);
        }

        internal override StartupHelperBase StartupHelper
        {
            get { return _startupHelper; }
        }
    }
}
