﻿using System.Diagnostics;
using System.IO;
using System.Windows;

namespace C1.Web.Mvc.FlexViewerWizard
{
    public abstract class ViewerSettingsBase
    {
        private string _originInnerDocumentFile = string.Empty;
        private string _innerDocumentFile = string.Empty;
        private const string RelativePathPrefix = "~";
        private string _serverUrl;
        private string _outerDocumentFile;

        public ViewerSettingsBase(bool currentProjectServiceSupported = false, string wwwroot = null, string documentsFolder = null)
        {
            CurrentProjectServiceSupported = currentProjectServiceSupported;
            CurrentProjectAsService = currentProjectServiceSupported;
            WwwRoot = wwwroot ?? string.Empty;
            CurrentProjectDocumentsFolder = string.IsNullOrEmpty(documentsFolder) ? DefaultDocumentsFolder : documentsFolder;
        }

        protected abstract string DefaultDocumentsFolder { get; }

        [ReplacementIgnore]
        public virtual bool IsValid
        {
            get
            {
                return CurrentProjectAsService ? (!string.IsNullOrEmpty(OriginInnerDocumentFile))
                    : (!string.IsNullOrEmpty(ServerUrl) && !string.IsNullOrEmpty(OuterDocumentFile));
            }
        }

        [ReplacementIgnore]
        public string FileMessage
        {
            get;
            private set;
        }

        [ReplacementIgnore]
        public string CurrentProjectDocumentsFolder
        {
            get;
            private set;
        }

        [ReplacementIgnore]
        public bool CurrentProjectServiceSupported
        {
            get;
            private set;
        }

        [ReplacementIgnore]
        public virtual bool ShouldAddDocuemntFile
        {
            get { return CurrentProjectAsService && WillCopyFile; }
        }

        [ReplacementIgnore]
        public virtual bool ShouldAddDiskStorage
        {
            get { return CurrentProjectAsService; }
        }

        public bool IsRazorPages
        {
            get;
            set;
        }

        public bool CurrentProjectAsService
        {
            get;
            set;
        }

        [ReplacementIgnore]
        public string WwwRoot
        {
            get;
            private set;
        }

        public virtual string DocumentPath { get { return CurrentProjectAsService ? InnerDocumentFile : OuterDocumentFile; } }

        public virtual string ServerUrl { get { return _serverUrl ?? (_serverUrl = string.Empty); } set { _serverUrl = value; } }      
        
        [ReplacementIgnore]
        public string OuterDocumentFile { get { return _outerDocumentFile ?? (_outerDocumentFile = string.Empty); } set { _outerDocumentFile = value; } }

        public bool WillCopyFile { get; private set; }

        [ReplacementIgnore]
        public string DestFilePathOfCopy
        {
            get
            {
                return Path.Combine(WwwRoot, CurrentProjectDocumentsFolder, Path.GetFileName(InnerDocumentFile));
            }
        }

        [ReplacementIgnore]
        public string InnerDocumentFile
        {
            get
            {
                return _innerDocumentFile;
            }
            set
            {
                if (_innerDocumentFile == value)
                {
                    return;
                }

                _innerDocumentFile = value ?? string.Empty;
            }
        }

        [ReplacementIgnore]
        public string OriginInnerDocumentFile
        {
            get
            {
                return _originInnerDocumentFile;
            }
            set
            {
                if (_originInnerDocumentFile == value)
                {
                    return;
                }

                _originInnerDocumentFile = value ?? string.Empty;
                WillCopyFile = !_originInnerDocumentFile.StartsWith(WwwRoot, System.StringComparison.OrdinalIgnoreCase);
                InnerDocumentFile = WillCopyFile ? Path.Combine("~", CurrentProjectDocumentsFolder, Path.GetFileName(_originInnerDocumentFile)) : ToRelativePath(_originInnerDocumentFile, WwwRoot);
                FileMessage = GetFileMessage();
            }
        }

        [ReplacementIgnore]
        public virtual bool IsBeta
        {
            get
            {
                return false;
            }
        }

        [ReplacementIgnore]
        public abstract string WebApiAssembly { get; }

        [ReplacementIgnore]
        public abstract string WebApiLicenseDetector { get; }

        private string GetFileMessage()
        {
            if (string.IsNullOrEmpty(InnerDocumentFile))
            {
                return Localization.Resources.EmptyFileText;
            }

            return WillCopyFile ? string.Format(Localization.Resources.CopyFileTip, CurrentProjectDocumentsFolder)
                : string.Format(Localization.Resources.RelativePathTip, InnerDocumentFile);
        }

        private static string ToRelativePath(string file, string root)
        {
            if (!file.StartsWith(root, System.StringComparison.OrdinalIgnoreCase))
            {
                Debug.Fail(string.Format("File:{0} should be in root:{1}.", file, root));
                return file;
            }

            var relative = file.Substring(root.Length);
            return Path.Combine(RelativePathPrefix, relative).Replace('\\', '/');
        }
    }
}
