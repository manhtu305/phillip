﻿using System.Windows;

namespace C1.Web.Mvc.FlexViewerWizard
{
    public class PdfViewerWizard : ViewerWizardBase
    {
        private readonly static StartupHelperBase _startupHelper = new PdfViewerStartupHelper();

        internal override ViewerSettingsBase CreateModel(ProjectEx project)
        {
            PdfViewerSettings model;
            if (CheckProjectServiceSupported(project))
            {
                model = new PdfViewerSettings(true, project.ProjectFolder,
                    project.MvcVersion == MvcVersion.Mvc5 ? "Content\\PdfFilesRoot" : "wwwroot\\PdfFilesRoot");
            }
            else
            {
                model = new PdfViewerSettings();
            }

            return model;
        }

        internal override StartupHelperBase StartupHelper
        {
            get { return _startupHelper; }
        }

        protected override Window CreateSettingsWindow(ViewerSettingsBase model)
        {
            return new PdfViewerSettingsWindow(model as PdfViewerSettings);
        }
    }
}
