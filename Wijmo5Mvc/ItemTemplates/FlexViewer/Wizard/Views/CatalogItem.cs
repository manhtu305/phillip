﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class CatalogItem
    {
        private IList<CatalogItem> _items = new List<CatalogItem>();

        public CatalogItem(string name, string path, CatalogItemKind type)
        {
            Name = name;
            Path = path;
            Type = type;
        }

        public CatalogItemKind Type { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public IList<CatalogItem> Items { get { return _items; } }
    }

    internal enum CatalogItemKind
    {
        Folder,
        File,
        Report,
        Loading = 100,
        Warning
    }

    public class CatalogItemIconConverter : IValueConverter
    {
        private const string ImagesResources = "/C1.Web.Mvc.FlexViewerWizard;component/Resources/Images/";
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var kind = (CatalogItemKind)value;
            switch (kind)
            {
                case CatalogItemKind.Folder:
                    return ImagesResources + "folder_32.gif";
                case CatalogItemKind.File:
                    return ImagesResources + "C1FlexReport.png";
                case CatalogItemKind.Report:
                    return ImagesResources + "RB_document_32.gif";
                case CatalogItemKind.Loading:
                    return ImagesResources + "Refresh.ico";
                case CatalogItemKind.Warning:
                    return ImagesResources + "Warning.png";
                default:
                    return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
