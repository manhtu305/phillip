﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using System.Windows.Controls;
using System.Xml;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace C1.Web.Mvc.FlexViewerWizard
{
    /// <summary>
    /// Interaction logic for ReportViewerSettingsWindow.xaml
    /// </summary>
    public partial class ReportViewerSettingsWindow
    {
        private OpenFileDialog _openFileDialog;
        private readonly ReportViewerSettings _model;
        private string _innerReportFile;
        private WebClient _ssrsReportsClient;
        private WebClient _outerReportsClient;

        public ReportViewerSettingsWindow(ReportViewerSettings model)
        {
            InitializeComponent();

            Height = model.CurrentProjectServiceSupported ? 680 : 580;

            _model = model;
            InnerReportFile = model.OriginInnerDocumentFile;
            OuterFolderPath = model.OuterDocumentFile;
            DataContext = model;

            // the PasswordBox.Password property can only bind to dependency property,
            // set this property manually.
            ssrsLoginPasswordPasswordBox.Password = _model.SsrsLoginPassword;
        }

        #region FlexReport
        private string InnerReportFile
        {
            get
            {
                return _innerReportFile;
            }
            set
            {
                if (_innerReportFile == value)
                {
                    return;
                }

                _innerReportFile = value;
                UpdateInnerReportNames();
            }
        }

        private void UpdateInnerReportNames()
        {
            ResetComboBox(reportNameComboBox);
            lblFileMessage.Text = _model.FileMessage;
            if (string.IsNullOrEmpty(InnerReportFile))
            {
                return;
            }

            string error;
            var names = ReadFlexReportNames(InnerReportFile, out error);
            if (!string.IsNullOrEmpty(error))
            {
                MakeUnselectable(reportNameComboBox);
                AddMarker(reportNameComboBox, error);
                return;
            }

            reportNameComboBox.ItemsSource = names;
        }

        private static IEnumerable<string> ReadFlexReportNames(string path, out string error)
        {
            XmlDocument doc = null;
            error = string.Empty;
            string ext = Path.GetExtension(path).ToLowerInvariant();
            if (ext != ".flxr" && ext != ".xml")
            {
                error = string.Format(Localization.Resources.CannotOpenFileError, path);
                return null;
            }

            doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            try
            {
                doc.Load(path);
                return ReadFlexReportNames(doc);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return null;
            }
        }

        public static IEnumerable<string> ReadFlexReportNames(XmlDocument doc)
        {
            var reports = doc.SelectNodes("Reports/Report/Name");
            var list = new string[reports.Count];
            for (int i = 0; i < reports.Count; i++)
            {
                list[i] = reports[i].InnerText;
            }

            return list;
        }

        private void BrowseBtnClick(object sender, RoutedEventArgs e)
        {
            if (_openFileDialog == null)
            {
                _openFileDialog = new OpenFileDialog();
                _openFileDialog.InitialDirectory = _model.WwwRoot ?? string.Empty;
                _openFileDialog.Filter = Localization.Resources.ReportFilter;
                _openFileDialog.RestoreDirectory = true;
            }
            else
            {
                _openFileDialog.FileName = string.Empty;
                _openFileDialog.InitialDirectory = string.Empty;
            }

            if (_openFileDialog.ShowDialog() != true)
            {
                return;
            }

            var file = _openFileDialog.FileName;
            _model.OriginInnerDocumentFile = file;
            reportFileTextBox.Text = file;
            InnerReportFile = file;
            SettingsChanged(sender, e);
        }

        private void ReportFileTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            InnerReportFile = reportFileTextBox.Text;
        }
        #endregion

        #region SSRS Report
        private string SsrsReportPath
        {
            get { return _model.SsrsReportPath; }
            set
            {
                // it's not a dependency property, update the model and textbox manually.
                _model.SsrsReportPath = value;
                ssrsReportPathTextBox.Text = value;
            }
        }

        private void SsrsConnectBtnClick(object sender, RoutedEventArgs e)
        {
            var currentCursor = Cursor;
            Cursor = System.Windows.Input.Cursors.Wait;
            try
            {
                UpdateSsrsReports();
            }
            catch (Exception ex)
            {
                // The ssrsConnectButton is disabled on UpdateSsrsReports() method,
                // and will be enabled on SsrsReportsClient_UploadStringCompleted() method.
                // Should make it enabled on exception is thrown.
                ssrsConnectButton.IsEnabled = true;

                AddWarningMarker(ssrsReportsTreeView,
                    string.Format("{0}\r\n{1}", Localization.Resources.CannotConnectSsrsError, ex.Message));
            }
            finally
            {
                Cursor = currentCursor;
            }
        }

        private void UpdateSsrsReports()
        {
            lock (ssrsReportsTreeView)
            {
                if (_ssrsReportsClient != null)
                {
                    _ssrsReportsClient.CancelAsync();
                    _ssrsReportsClient = null;
                }

                ResetTreeView(ssrsReportsTreeView);
                if (string.IsNullOrEmpty(_model.SsrsServerUrl))
                {
                    return;
                }

                var url = ResolveUrl(_model.SsrsServerUrl);
                url += "/ReportService2005.asmx";
                var uri = new Uri(url);

                AddLoadingMarker(ssrsReportsTreeView);
                ssrsConnectButton.IsEnabled = false;

                var credential = new NetworkCredential(_model.SsrsLoginUserName, _model.SsrsLoginPassword, _model.SsrsLoginDomain);
                _ssrsReportsClient = CreateWebClient(credential);
                _ssrsReportsClient.Headers.Add("Content-Type", "application/soap+xml");
                _ssrsReportsClient.UploadStringCompleted += SsrsReportsClient_UploadStringCompleted;
                _ssrsReportsClient.UploadStringAsync(uri, GetListChildrenSoapData());
            }
        }

        private static string GetListChildrenSoapData()
        {
            var soap = new StringBuilder();
            soap.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            soap.Append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
            soap.Append("<soap:Body>");
            soap.Append("<ListChildren xmlns=\"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices\">");
            soap.Append("<Item>/</Item>");
            soap.Append("<Recursive>true</Recursive>");
            soap.Append("</ListChildren>");
            soap.Append("</soap:Body>");
            soap.Append("</soap:Envelope>");
            return soap.ToString();
        }

        private void SsrsReportsClient_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            lock (ssrsReportsTreeView)
            {
                _ssrsReportsClient = null;
                ssrsConnectButton.IsEnabled = true;
                ResetTreeView(ssrsReportsTreeView);
                if (e.Cancelled) return;

                if (e.Error != null)
                {
                    AddWarningMarker(ssrsReportsTreeView,
                        string.Format("{0}\r\n{1}", Localization.Resources.CannotConnectSsrsError, e.Error.Message));
                    return;
                }

                var text = e.Result;
                if (text == null) return;

                string error;
                var items = ReadSsrsReports(text, out error);
                if (!string.IsNullOrEmpty(error))
                {
                    AddWarningMarker(ssrsReportsTreeView, error);
                    return;
                }

                ssrsReportsTreeView.ItemsSource = items;
            }
        }

        private static CatalogItem[] ReadSsrsReports(string text, out string error)
        {
            error = string.Empty;
            var doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            try
            {
                doc.LoadXml(text);
                return ReadSsrsReports(doc);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return null;
            }
        }

        private static CatalogItem[] ReadSsrsReports(XmlDocument doc)
        {
            const string ns = "rs2005";
            var nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace(ns, "http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices");
            var nodes = doc.SelectNodes(string.Format("//{0}:CatalogItem", ns), nsmgr);
            var ssrsItems = new List<CatalogItem>();
            foreach (XmlNode node in nodes)
            {
                var hiddenText = GetChildNodeText(node, ns + ":Hidden", nsmgr);
                if (!string.IsNullOrEmpty(hiddenText) && hiddenText.ToLower() == "true") continue;

                var typeText = GetChildNodeText(node, ns + ":Type", nsmgr);
                if (typeText != "Folder" && typeText != "Report") continue;

                var ssrsItem = new CatalogItem(GetChildNodeText(node, ns + ":Name", nsmgr),
                    GetChildNodeText(node, ns + ":Path", nsmgr),
                    typeText == "Folder" ? CatalogItemKind.Folder : CatalogItemKind.Report);

                ssrsItems.Add(ssrsItem);
            }

            var allSsrsItems = from item in ssrsItems
                           orderby item.Type, item.Path
                           select item;

            var items = new List<CatalogItem>();
            AddSsrsReport(items, allSsrsItems, 1);
            return items.ToArray();
        }

        private static string GetChildNodeText(XmlNode node, string name, XmlNamespaceManager nsmgr)
        {
            var childNode = node.SelectSingleNode(name, nsmgr);
            return childNode == null ? string.Empty : childNode.InnerText;
        }

        private static void AddSsrsReport(IList<CatalogItem> nodes, IEnumerable<CatalogItem> allItems, int level = 1)
        {
            foreach (var levelItem in allItems.Where(item => GetPathLevel(item.Path) == level))
            {
                if (levelItem.Type == CatalogItemKind.Folder)
                {
                    // remove confiction of "/A" and "/AAA"
                    var itemPath = levelItem.Path + "/";
                    var subItems = allItems.Where(item => item.Type == CatalogItemKind.Report && item.Path.StartsWith(itemPath));
                    if(subItems.Any())
                    {
                        nodes.Add(levelItem);
                        AddSsrsReport(levelItem.Items, subItems, level + 1);
                    }

                    continue;
                }

                nodes.Add(levelItem);
            }
        }

        private static int GetPathLevel(string path)
        {
            return path.Count(c => c == '/');
        }

        private void ssrsReportsTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var item = e.NewValue as CatalogItem;
            if (item != null && item.Type == CatalogItemKind.Report)
            {
                SsrsReportPath = item.Path;
            }
        }

        private void ssrsLoginPasswordPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            // the PasswordBox.Password property can only bind to dependency property,
            // get this property manually.
            _model.SsrsLoginPassword = ssrsLoginPasswordPasswordBox.Password;
            SettingsChanged(sender, e);
        }
        #endregion

        #region Outer Report
        private string OuterFolderPath
        {
            get { return outerFolderPathTextBox.Text; }
            set { outerFolderPathTextBox.Text = value; }
        }

        private string OuterReportPath
        {
            get { return _model.OuterReportPath; }
            set
            {
                // it's not a dependency property, update the model and textbox manually.
                _model.OuterReportPath = value;
                outerReportPathTextBox.Text = value;
            }
        }

        private void OuterConnectBtnClick(object sender, RoutedEventArgs e)
        {
            var currentCursor = Cursor;
            Cursor = System.Windows.Input.Cursors.Wait;
            try
            {
                UpdateOuterReports();
            }
            catch(Exception ex)
            {
                // The outerConnectButton is disabled on UpdateOuterReports() method,
                // and will be enabled on OuterReportsClient_DownloadStringCompleted() method.
                // Should make it enabled on exception is thrown.
                outerConnectButton.IsEnabled = true;

                AddWarningMarker(outerReportsTreeView, 
                    string.Format("{0}\r\n{1}", Localization.Resources.CannotConnectServiceError, ex.Message));
            }
            finally
            {
                Cursor = currentCursor;
            }
        }

        private void UpdateOuterReports()
        {
            lock (outerReportsTreeView)
            {
                if (_outerReportsClient != null)
                {
                    _outerReportsClient.CancelAsync();
                    _outerReportsClient = null;
                }

                ResetTreeView(outerReportsTreeView);
                if (string.IsNullOrEmpty(_model.ServerUrl))
                {
                    return;
                }

                var url = ResolveUrl(_model.ServerUrl);
                url += "/";
                if (!string.IsNullOrEmpty(OuterFolderPath))
                {
                    url += OuterFolderPath.Replace('\\', '/').TrimStart('/');
                }
                url += "?recursive=true";
                var uri = new Uri(url);

                outerConnectButton.IsEnabled = false;
                AddLoadingMarker(outerReportsTreeView);

                _outerReportsClient = CreateWebClient();
                _outerReportsClient.DownloadStringCompleted += OuterReportsClient_DownloadStringCompleted;
                _outerReportsClient.DownloadStringAsync(uri);
            }
        }

        private void OuterReportsClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            lock (outerReportsTreeView)
            {
                _outerReportsClient = null;
                outerConnectButton.IsEnabled = true;
                ResetTreeView(outerReportsTreeView);
                if (e.Cancelled) return;

                if (e.Error != null)
                {
                    AddWarningMarker(outerReportsTreeView, 
                        string.Format("{0}\r\n{1}", Localization.Resources.CannotConnectServiceError, e.Error.Message));
                    return;
                }

                var text = e.Result;
                if (text == null || text=="null") return;

                string error;
                var items = ReadOuterReports(text, out error);
                if (!string.IsNullOrEmpty(error))
                {
                    AddWarningMarker(outerReportsTreeView, error);
                    return;
                }

                outerReportsTreeView.ItemsSource = items;
            }
        }

        private static CatalogItem[] ReadOuterReports(string text, out string error)
        {
            error = string.Empty;
            try
            {
                var catalogItem = JsonConvert.DeserializeObject<CatalogItem>(text);
                return catalogItem.Items.ToArray();
            }
            catch(Exception ex)
            {
                error = ex.Message;
                return null;
            }
        }

        private void outerReportsTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var item = e.NewValue as CatalogItem;
            if (item != null && item.Type == CatalogItemKind.Report)
            {
                OuterReportPath = item.Path;
            }
        }
        #endregion

        #region WebClient
        private static string ResolveUrl(string text)
        {
            var url = text.ToLower().Replace('\\', '/').TrimEnd('/');
            if (!url.StartsWith("http://") && !url.StartsWith("https://"))
            {
                url = "http://" + url;
            }

            return url;
        }

        private static WebClient CreateWebClient(NetworkCredential credential = null)
        {
            var client = new WebClient();
            client.Credentials = credential ?? CredentialCache.DefaultCredentials;
            var proxy = WebRequest.GetSystemWebProxy();
            if (proxy != null)
            {
                proxy.Credentials = CredentialCache.DefaultCredentials;
                client.Proxy = proxy;
            }
            return client;
        }
        #endregion

        #region ComboBox
        private static void ResetComboBox(ComboBox comboBox)
        {
            comboBox.SelectionChanged -= RemoveComboBoxSelection;
            comboBox.ItemsSource = null;
        }

        private static void AddMarker(ComboBox comboBox, string marker)
        {
            comboBox.ItemsSource = new[] { marker };
        }

        private static void MakeUnselectable(ComboBox comboBox)
        {
            comboBox.SelectionChanged += RemoveComboBoxSelection;
        }

        private static void RemoveComboBoxSelection(object sender, SelectionChangedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (combo == null)
            {
                return;
            }

            if (combo.SelectedIndex == -1)
            {
                return;
            }

            combo.SelectedIndex = -1;
        }
        #endregion

        #region TreeView
        private static void ResetTreeView(TreeView tv)
        {
            tv.ItemsSource = null;
        }

        private static void AddLoadingMarker(TreeView tv)
        {
            var items = new[] { new CatalogItem(Localization.Resources.LoadingText, "", CatalogItemKind.Loading) };
            tv.ItemsSource = items;
        }

        private static void AddWarningMarker(TreeView tv, string message)
        {
            var items = new[] { new CatalogItem(message, "", CatalogItemKind.Warning) };
            tv.ItemsSource = items;
        }
        #endregion

        #region Buttons
        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SettingsChanged(object sender, RoutedEventArgs e)
        {
            // the binding source will be updated on the LostFocus event,
            // force updating the binding source on TextChanged event.
            var textBox = sender as TextBox;
            if (textBox != null)
            {
                var bindingExpression = textBox.GetBindingExpression(TextBox.TextProperty);
                bindingExpression.UpdateSource();
            }

            buttonOk.IsEnabled = _model.IsValid;
        }
        #endregion
    }
}
