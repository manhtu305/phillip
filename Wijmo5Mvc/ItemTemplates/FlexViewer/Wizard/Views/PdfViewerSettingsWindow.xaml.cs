﻿using System.Windows;
using Microsoft.Win32;
using System.Windows.Controls;

namespace C1.Web.Mvc.FlexViewerWizard
{
    /// <summary>
    /// Interaction logic for PdfViewerSettingsWindow.xaml
    /// </summary>
    public partial class PdfViewerSettingsWindow
    {
        private OpenFileDialog _openFileDialog;
        private readonly ViewerSettingsBase _model;

        public PdfViewerSettingsWindow(PdfViewerSettings model)
        {
            InitializeComponent();
            _model = model;
            DataContext = model;
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void BrowseBtnClick(object sender, RoutedEventArgs e)
        {
            if (_openFileDialog == null)
            {
                _openFileDialog = new OpenFileDialog();
                _openFileDialog.InitialDirectory = _model.WwwRoot ?? string.Empty;
                _openFileDialog.Filter = Localization.Resources.PdfFilter;
                _openFileDialog.RestoreDirectory = true;
            }
            else
            {
                _openFileDialog.FileName = string.Empty;
                _openFileDialog.InitialDirectory = string.Empty;
            }

            if (_openFileDialog.ShowDialog() != true)
            {
                return;
            }

            var file = _openFileDialog.FileName;
            _model.OriginInnerDocumentFile = file;
            pdfFileTextBox.Text = file;
            SettingsChanged(sender, e);
        }

        private void SettingsChanged(object sender, RoutedEventArgs e)
        {
            // the binding source will be updated on the LostFocus event,
            // force updating the binding source on TextChanged event.
            var textBox = sender as TextBox;
            if (textBox != null)
            {
                var bindingExpression = textBox.GetBindingExpression(TextBox.TextProperty);
                bindingExpression.UpdateSource();
            }

            buttonOk.IsEnabled = _model.IsValid;
        }
    }
}
