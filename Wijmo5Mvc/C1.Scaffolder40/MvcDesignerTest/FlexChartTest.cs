﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class FlexChartTest : Base.BaseTest
    {
        string CShaprFileName = "FlexChart/FlexChart.cshtml";
        string VBFileName = "FlexChart/FlexChart.vbhtml";
        string CoreFileName = "FlexChart/FlexChart_core.cshtml";

        protected override string[] GetListComplexProperties()
        {
            return new string[] { "Series", "DotnetCoreSeries"};
        }

        #region Mvc project's files.
        [TestMethod]
        public void GetFlexChart()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "ChartType", "Height", "Id" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

          [TestMethod]
        public void GetFinancialChart()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FinancialChart", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            {  "Id", "Bind", "BindingX", "ChartType", "Series", "Tooltip"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

           [TestMethod]
        public void GetFlexChart2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "ChartType", "AddAnnotationLayer", "LegendToggle", "Height", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

           [TestMethod]
        public void GetFlexChart3()
        {
            int index = 3;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Bind", "BindingX", "Series"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

           [TestMethod]
        public void GetFlexChart4()
        {
            int index = 4 ;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Header", "Footer", "Tooltip", "Bind", "BindingX", "AxisX", "AxisY", "Series"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
#endregion

        #region vbhtml
        [TestMethod]
        public void GetFlexChartVB()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            Assert.AreEqual("Input101.InputModel", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "AddAnnotationLayer", "Height", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void FlexChartVB1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            Assert.AreEqual("ModelStyle", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "Bind", "BindingX", "Series"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion

        #region for core project
        [TestMethod]
        public void GetFlexChartCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "BindingX", "Sources", "DotnetCoreSeries" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
       
        [TestMethod]
        public void GetFlexChartCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "BindingX", "Header", "Footer", "LegendPosition", "Sources", "Axis", "DotnetCoreSeries" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetFlexChartCore2()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "BindingX", "Tooltips",  "Sources", "Axis", "DotnetCoreSeries" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetFlexChartCore3()
        {
            int index = 3;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexChart", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Height", "Id", "Options", "Datalabels"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion coreproject

    }
}
