﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class DashboardTest : Base.BaseTest
    {
        string CoreFileName = "Dashboard/Index.cshtml";
        
        [TestMethod]
        public void ComplexControlTest()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("DashboardLayout", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "FormatTile", "Layouts" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

            //assert child
            Assert.IsTrue(control.Properties["Layouts"].Value is MVCControlCollection);
            MVCControl gridLayout = ((MVCControlCollection)control.Properties["Layouts"].Value).Items[0];
            Assert.IsTrue(gridLayout != null, "Child control must exist");

            expectedProperties = new string[]
            { "Orientation", "MaxRowsOrColumns", "CellSize", "Groups" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, gridLayout.Properties));
        }
    }
}
