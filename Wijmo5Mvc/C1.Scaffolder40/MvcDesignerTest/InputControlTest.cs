﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    /// <summary>
    /// Test cases for all input controls
    /// </summary>
    [TestClass]
    public class InputControlTest : Base.BaseTest
    {
        #region Mvc project's files.
        #region InputNumber

        [TestMethod]
        public void GetParsedInputNumber()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Value", "Format", "Step", "Min", "Max"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputNumber1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Placeholder", "IsRequired", "Value"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputNumber2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Value", "Format"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion

        #region InputTime
        [TestMethod]
        public void GetParsedInputTime()
        {
            int index = 3;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Format", "Id", "Bind", "Value"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

         [TestMethod]
        public void GetParsedInputTime1()
        {
            int index = 4;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Min", "Max"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

           [TestMethod]
        public void GetParsedInputColor()
        {
            int index = 5;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputColor", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Value", "OnClientValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion
        #region InputDate
           [TestMethod]
        public void GetParsedInputDate()
        {
            int index = 6;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputDate", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Min", "Max", "OnClientValueChanged", "ItemValidator"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

          [TestMethod]
        public void GetParsedInputDateTime()
        {
            int index = 7;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputDateTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Value", "ItemValidator"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputDate2()
        {
            int index = 10;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputDate", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Value", "Min", "Max", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion
        #region inputmask
         [TestMethod]
        public void GetParsedInputMask()
        {
            int index = 8;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputMask", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Mask", "HtmlAttributes"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

          [TestMethod]
        public void GetParsedInputMask1()
        {
            int index = 9;
            MVCControl control = GetControlSetting(index, "Input.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputMask", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Placeholder", "OnClientValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion
        #endregion

        #region vbhtml
         #region InputNumber

        [TestMethod]
        public void GetParsedInputNumberVb()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Format"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputNumberVb1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Format", "Step", "Min", "Max"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputNumberVb2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Placeholder", "Required", "Value"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputNumberForVb()
        {
            int index = 9;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion
        #region InputTime
        [TestMethod]
        public void GetParsedInputTimeVb()
        {
            int index = 3;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            {  "Id", "Value", "Format", "Mask"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

         [TestMethod]
        public void GetParsedInputTime1vb()
        {
            int index = 4;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Min", "Max", "Step", "OnClientValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion
        
        #region InputDate
           [TestMethod]
        public void GetParsedInputDateVB()
        {
            int index = 5;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputDate", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Min", "Max", "Format", "OnClientValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

          [TestMethod]
        public void GetParsedInputDateTimeVB()
        {
            int index = 6;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputDateTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Min", "Max", "TimeStep", "Format", "TimeMin", "TimeMax", "OnClientValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputDateVB2()
        {
            int index = 10;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputDate", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Value", "Min", "Max", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion
        #region InputColor
        //[TestMethod]
        //public void GetParsedInputColorvb()
        //{
        //    int index = 7;
        //    MVCControl control = GetControlSetting(index, "Input.vbhtml");
        //    Assert.IsTrue(control != null, "must exist control");
        //    Assert.AreEqual("InputColor", control.Name, "Wrong name of control");
        //    Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
        //    string[] expectedProperties = new string[] 
        //    { "Value", "OnClientValueChanged"};
        //    Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        //}

        #endregion
        #region inputmask
         [TestMethod]
        public void GetParsedInputMaskVB()
        {
            int index = 7;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputMask", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Placeholder", "OnClientValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

          [TestMethod]
        public void GetParsedInputMaskVB1()
        {
            int index = 8;
            MVCControl control = GetControlSetting(index, "Input.vbhtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputMask", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Mask"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion
        #endregion

        #region for core project
               #region InputNumber

        [TestMethod]
        public void GetParsedInputNumberCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Min", "Max", "Placeholder", "IsRequired", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputNumber1Core()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetParsedInputNumber2Core()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Value", "Format"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

         [TestMethod]
        public void GetParsedInputNumberForCore()
        {
            int index = 10;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputNumber", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "CssClass", "Width", "Height"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion

        #region InputTime
        [TestMethod]
        public void GetParsedInputTimeCOre()
        {
            int index = 3;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Min", "Max" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

         [TestMethod]
        public void GetParsedInputTime1Core()
        {
            int index = 4;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Value", "Format", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

           [TestMethod]
        public void GetParsedInputColorCore()
        {
            int index = 5;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputColor", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Value", "ValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion
        #region InputDate
           [TestMethod]
        public void GetParsedInputDateCore()
        {
            int index = 6;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputDate", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Value", "Min", "Max", "ValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

          [TestMethod]
        public void GetParsedInputDateTimeCore()
        {
            int index = 7;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputDateTime", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Value"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion
        #region inputmask
        
          [TestMethod]
        public void GetParsedInputMaskCore()
        {
            int index = 8;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputMask", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Mask", "Title"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

         [TestMethod]
        public void GetParsedInputMask1Core()
        {
            int index = 9;
            MVCControl control = GetControlSettingInCore(index, "Input_core.cshtml");
            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("InputMask", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "Placeholder", "ValueChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion
        #endregion coreproject
    }
}
