﻿using C1.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder.UnitTest.Base
{

    public class BaseTest
    {
        /// <summary>
        /// Use to get control setting in MVC project
        /// </summary>
        /// <param name="index"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        protected MVCControl GetControlSetting(int index, string fileName)
        {
            string html = string.Empty;
            bool isCSharp = fileName.EndsWith("cshtml");
            StreamReader sr = new StreamReader("..\\..\\SamplePages\\" + fileName);
            html = sr.ReadToEnd();
            RazorParser parser = new RazorParser(html, isCSharp ? BlockCodeType.CSharp : BlockCodeType.VB);
            List<ControlBlockCode> list = parser.GetControlBlocks();
            if (index >= 0 && index < list.Count)
            {
                ParserManager pm = new ParserManager();
                BaseHtmlParser htmlParser = pm.CreateParser(list[index].CodeType);
                return  htmlParser.GetControlSetting(list[index].Content);
            }
            
            return null;
        }

        protected MVCControl GetControlSettingInCore(int index, string fileName)
        {
            string html = string.Empty;
            StreamReader sr = new StreamReader("..\\..\\SamplePages\\" + fileName);
            html = sr.ReadToEnd();
            RazorParser parser = new RazorParser(html, BlockCodeType.HTMLNode);
            List<ControlBlockCode> list = parser.GetControlBlocks();
            if (index >= 0 && index < list.Count)
            {
                if (index >= 0 && index < list.Count)
                {
                    ParserManager pm = new ParserManager();
                    BaseHtmlParser htmlParser = pm.CreateParser(BlockCodeType.HTMLNode);
                    return htmlParser.GetControlSetting(list[index].Content);
                }
            }
            return null;
        }

        protected bool CheckSameProperties(string[] expected, Dictionary<string, MVCProperty> properties, bool ignoreComplexProperty = false)
        {
            int length = expected.Length;
            Assert.AreEqual(expected.Length, properties.Count, "Wrong number of properties");
            for (int i = 0; i < length; i++)
            {
                Assert.AreEqual(expected[i], properties.Keys.ElementAt(i), string.Format("Wrong name of control's properties at {0}", i) );
                
            }
            if (!ignoreComplexProperty)
            {
                string[] complexProperties = GetListComplexProperties();
                for (int i = 0; i < complexProperties.Length; i++)
                {
                    string propertyName = complexProperties[i];
                    if (properties.ContainsKey(propertyName))
                        Assert.IsTrue(properties[propertyName].Value is MVCControlCollection, "Type of " + propertyName + " parsed wrong!");
                }
                ResetComplexProperties();
            }
            return true;
        }

        protected virtual string[] GetListComplexProperties()
        {
            return new string[] { };
        }

        protected virtual void ResetComplexProperties()
        {

        }
    }
}
