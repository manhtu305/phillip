﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    /// <summary>
    /// Test ability to parse both TagHelper and HTMLTagHelper in one file
    /// </summary>
    [TestClass]
    public class CombineParserTest : Base.BaseTest
    {
        string CShaprFileName = "combine.cshtml";

        [TestMethod]
        public void TestMethod1()
        {
            MVCControl control = GetControlSetting(-1, CShaprFileName);
            Assert.IsTrue(control == null, "must not exist control");
        }

        #region for core project
        [TestMethod]
        public void GetParsedGrid1()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual("Category", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "AutoGenerateColumns", "Columns", "Bind", "AllowAddNew", "AllowDelete", "CssClass"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        [TestMethod]
        public void GetParsedGrid2()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "AutoGenerateColumns", "CssClass", "IsReadOnly", "AllowSorting", "SelectionMode", 
                "Columns", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        #endregion coreproject
    }
}
