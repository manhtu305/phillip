﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class MultiAutoCompleteTest : Base.BaseTest
    {
        string CShaprFileName = "MultiAutoComplete.cshtml";
        string VBFileName = "MultiAutoComplete.vbhtml";
        string CoreFileName = "MultiAutoComplete_core.cshtml";

        #region Mvc project's files.
        [TestMethod]
        public void GetMultiAutoComplete()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiAutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "DisplayMemberPath", "SelectedIndexes"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetAutoComplete1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiAutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "MaxItems", "CssMatch", "ItemsSourceAction"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetAutoComplete2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiAutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "CssMatch", "SelectedIndexes", "MaxSelectedItems"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
         
#endregion

        #region vbhtml
        [TestMethod]
        public void GetMultiAutoCompleteVb()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiAutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual("MVCFlexGrid101.Startup", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "MaxSelectedItems", "IsEditable", "IsRequired", "Placeholder", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
             [TestMethod]
        public void GetMultiAutoCompleteVb1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiAutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "OnClientIsDroppedDownChanging", "CssClass", "Width", "Height", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        
        #endregion

        #region for core project
        [TestMethod]
        public void GetMultiAutoCompleteCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiAutoComplete", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "DisplayMemberPath", "SelectedIndexes",  "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        [TestMethod]
        public void GetMultiAutoCompleteCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiAutoComplete", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "MaxItems", "CssMatch",  "ItemsSourceAction"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        [TestMethod]
        public void GetMultiAutoCompleteCore2()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiAutoComplete", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "CssMatch", "MaxSelectedItems", "SelectedIndexes", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion coreproject

    }
}
