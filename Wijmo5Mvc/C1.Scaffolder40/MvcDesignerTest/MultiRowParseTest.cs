﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class MultiRowParseTest : Base.BaseTest
    {
        string CShaprFileName = "multirow/multirow.cshtml";
        string VBFileName = "multirow/multirow.vbhtml";
        string CoreFileName = "multirow/multirow_core.cshtml";


        protected override string[] GetListComplexProperties()
        {
            return new string[] { "LayoutDefinition"};
        }

        protected override void ResetComplexProperties()
        {
            base.ResetComplexProperties();
        }
        #region Mvc project's files.
        [TestMethod]
        public void GetMultiRow()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiRow", control.Name, "Wrong name of control");
            Assert.AreEqual("Order", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "KeyActionEnter", "DefaultColumnSize", "LayoutDefinition"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

         [TestMethod]
        public void GetMultiRow1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiRow", control.Name, "Wrong name of control");
            Assert.AreEqual("Supplier", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "LayoutDefinition", "Bind", "AllowAddNew", "AllowDelete", "CssClass"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
          [TestMethod]
        public void GetMultiRow2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiRow", control.Name, "Wrong name of control");
            Assert.AreEqual("Order", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "ShowGroupPanel", "Bind", "CssClass", "IsReadOnly", "LayoutDefinition" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties, true));

        }

#endregion

        #region vbhtml
        [TestMethod]
        public void GetMultiRowVB()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiRow", control.Name, "Wrong name of control");
            Assert.AreEqual("MVCFlexGrid101.Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "SelectionMode", "CssClass", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        [TestMethod]
        public void GetMultiRowVB1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiRow", control.Name, "Wrong name of control");
            Assert.AreEqual("MVCFlexGrid101.MonthData", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "AutoSizeMode", "OnClientRowAdded", "OnClientRowEditEnded", "Width", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }


        #endregion

        #region for core project
        [TestMethod]
        public void GetMultiRowCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiRow", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "AllowAddNew",  "AllowDelete", "CssClass", "Sources", "LayoutDefinition" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
       
        [TestMethod]
        public void GetMultiRowCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiRow", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "CssClass",  "IsReadOnly", "GroupBy", "ShowGroups", "Sources", "LayoutDefinition" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetMultiRowCore2()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiRow", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "IsReadOnly","CssClass", "SelectionMode", "Sources", "LayoutDefinition" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion coreproject
    }
}
