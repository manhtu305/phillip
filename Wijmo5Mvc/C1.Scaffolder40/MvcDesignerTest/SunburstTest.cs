﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class SunBurstParseTest : Base.BaseTest
    {
        string CShaprFileName = "SunBurst/SunBurst.cshtml";
        string VBFileName = "SunBurst/SunBurst.vbhtml";
        string CoreFileName = "SunBurst/SunBurst_core.cshtml";

        #region Mvc project's files.
        [TestMethod]
        public void SunBurst0()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("Sunburst", control.Name, "Wrong name of control");
            Assert.AreEqual("HierarchicalData", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "InnerRadius", "Bind", "DataLabel", "BindingName"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        
#endregion

        #region vbhtml

        [TestMethod]
        public void SunburtsVB0()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("Sunburts", control.Name, "Wrong name of control");
            Assert.AreEqual("Sunburts.OtherStyle", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "SelectionMode", "CssClass", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void SunburtsVB1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("Sunburts", control.Name, "Wrong name of control");
            Assert.AreEqual("Sunburts.Style", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "Height", "Width"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }


        #endregion

        #region for core project

        [TestMethod]
        public void Sunburst0()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("Sunburst", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "Id", "Header",  "IsAnimated", "SelectionMode", "SelectedItemOffset", "SelectedItemPosition", "BindingName", "Binding", "Sources", "Datalabels" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void Sunburst1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("Sunburst", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "Id", "Header",  "InnerRadius", "BindingName", "Binding", "Sources", "Datalabels"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void Sunburst2()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("Sunburst", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "Id", "LegendPosition", "Header", "Footer", "BindingName", "Binding", "Sources", "Datalabels", "Styles"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion coreproject
    }
}
