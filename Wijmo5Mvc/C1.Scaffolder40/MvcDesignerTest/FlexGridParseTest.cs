﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class FlexGridParseTest : Base.BaseTest
    {
        string CShaprFileName = "FlexGrid.cshtml";
        string VBFileName = "FlexGrid.vbhtml";
        string CoreFileName = "FlexGrid_core.cshtml";

        private string[] complexProperties = null;
        protected override string[] GetListComplexProperties()
        {
            if (complexProperties == null || complexProperties.Length == 0)
                return new string[] { "Columns", "Bind" };
            else return complexProperties;
        }
        protected override void ResetComplexProperties()
        {
            complexProperties = null;
        }

        [TestMethod]
        public void TestMethod1()
        {
            MVCControl control = GetControlSetting(-1, CShaprFileName);
            Assert.IsTrue(control == null, "must not exist control");
        }


        #region Mvc project's files.
        [TestMethod]
        public void GetParsedGrid1()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual("Category", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "AutoGenerateColumns", "Columns",
                "Bind", "AllowAddNew", "AllowDelete", "CssClass"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        [TestMethod]
        public void GetParsedGrid2()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual("InvoiceStockItem", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "AutoGenerateColumns", "AllowDelete", "AllowAddNew", "AllowDragging", "SelectionMode", "Id", "CssClass",
               "Bind","Columns","OnClientCellEditEnded","OnClientDraggedRow","OnClientDraggingRow","OnClientSelectionChanged",
               "OnClientFormatItem"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

            Assert.AreEqual(14, control.Properties.Count, "Wrong number of properties");

        }
        [TestMethod]
        public void GetParsedGrid3()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] { "Height", "AutoGenerateColumns", "IsReadOnly", "TemplateBind", "Columns", "ToTemplate"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        [TestMethod]
        public void GetParsedGrid4()
        {
            int index = 3;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual("Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] { "Id", "AutoGenerateColumns", "AllowSorting", "SelectionMode",
                "Bind", "CssClass", "AllowDragging", "Columns"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        [TestMethod]
        public void GetParsedGrid5()
        {
            int index = 4;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual("Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] { "AllowSorting", "IsReadOnly", "Bind", "Height", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
          [TestMethod]
        public void GetParsedGrid6()
        {
            int index = 5;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual("Abc", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] { "AllowSorting", "IsReadOnly", "Bind", "Height", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
#endregion

        #region vbhtml
        [TestMethod]
        public void GetParsedGridVB1()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual("Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "IsReadOnly", "AutoGenerateColumns", "AllowSorting", "Bind", "CssClass", "Columns"};
            complexProperties = new string[] { "Columns"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        [TestMethod]
        public void GetParsedGridVB2()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "AutoGenerateColumns", "AllowSorting", "Bind", "Columns"};
            complexProperties = new string[] { "Columns" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
         [TestMethod]
        public void GetParsedGridVB3()
        {
            int index = 2;//from <!--2-->
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "AutoGenerateColumns", "AllowSorting", "IsReadOnly", "Bind","CssClass", "Filterable", "Columns"};
            complexProperties = new string[] { "Columns" };//in this, property Bind is bind with string value
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

         [TestMethod]
        public void GetParsedGridVB4()
        {
            int index = 3;//from <!--3-->
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "AutoGenerateColumns", "IsReadOnly", "Bind", "SelectionMode", "AllowResizing", "AllowSorting",
               "ChildItemsPath",  "CssClass", "Columns"};
            complexProperties = new string[] { "Columns" };//in this, property Bind is bind with string value
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        [TestMethod]
        public void GetParsedGridVB5()
        {
            int index = 4;//from <!--4-->
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            Assert.AreEqual("MVCFlexGrid101.Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "AllowSorting", "AutoGenerateColumns", "IsReadOnly", "Columns", "Bind", "Height",  "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        #endregion

        #region for core project
        [TestMethod]
        public void GetParsedGridCore1()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "AutoGenerateColumns",  "CssClass", "IsReadOnly", "AllowSorting", "SelectionMode",
                "Columns","Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        [TestMethod]
        public void GetParsedGridCore2()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "IsReadOnly","SelectionMode", "AutoGenerateColumns", "ItemFormatter", "Style",
                "ScrollPositionChanged", "ResizingColumn", "DraggingColumn", "SortingColumn",
                "Sources", "Columns"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

         [TestMethod]
        public void GetParsedGridCore3()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
                                { "Id", "AutoGenerateColumns", "IsReadOnly","SelectionMode", "Columns", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
               [TestMethod]
        public void GetParsedGridCore4()
        {
            int index = 3;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
                                { "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
               [TestMethod]
        public void GetParsedGridCore5()
        {
            int index = 4;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexGrid", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
                                { "AutoGenerateColumns", "Height", "Id", "Columns", "Panels", "OdataSources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        #endregion coreproject
    }
}
