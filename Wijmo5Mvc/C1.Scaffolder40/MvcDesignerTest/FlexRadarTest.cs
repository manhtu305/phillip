﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;
using System.Text;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class FlexRadarParseTest : Base.BaseTest
    {
        string CShaprFileName = "FlexRadar/FlexRadar.cshtml";
        string VBFileName = "FlexRadar/FlexRadar.vbhtml";
        string CoreFileName = "FlexRadar/FlexRadar_core.cshtml";

        #region Mvc project's files.

        [TestMethod]
        public void FlexRada0()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexRadar", control.Name, "Wrong name of control");
            Assert.AreEqual("CustomModel", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "ChartType", "Stacking", "Bind", "DataLabel", "Series", "Legend", "Width", "Height"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        [TestMethod]
        public void FlexRada1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexRadar", control.Name, "Wrong name of control");
            Assert.AreEqual("ModelStyle", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "ChartType", "Stacking", "Bind", "Series", "Legend", "Width", "Height"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        #endregion

        #region vbhtml

        [TestMethod]
        public void FlexRadar0()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexRadar", control.Name, "Wrong name of control");
            Assert.AreEqual("FlexRadar101.Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "SelectionMode", "CssClass", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        [TestMethod]
        public void FlexRadar1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexRadar", control.Name, "Wrong name of control");
            Assert.AreEqual("FlexRadar101.CountryData", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Height", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion

        #region for core project

        [TestMethod]
        public void FlexRadarCore0()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexRadar", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "BindingX", "Binding",  "ChartType", "Stacking", "Id", "Height", "Width", "LegendPosition", "Sources", "Series" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void FlexRadarCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexRadar", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Binding", "BindingX", "Height", "Width", "ChartType", "Stacking", "Id", "LegendPosition", "Sources", "Series"};
            
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion coreproject
    }
}
