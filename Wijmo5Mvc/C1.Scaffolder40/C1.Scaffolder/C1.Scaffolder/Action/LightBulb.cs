﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;
using System.Threading;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

namespace C1.Scaffolder
{
#if SMARTASSEMBLYUSAGE
    [SmartAssembly.Attributes.DoNotObfuscateType]
#endif
    [Export(typeof(ISuggestedActionsSourceProvider))]
    [Name("Test Suggested Actions")]
    [ContentType("text")]
    internal class SuggestedActionsSourceProvider : ISuggestedActionsSourceProvider
    {
        public ISuggestedActionsSource CreateSuggestedActionsSource(ITextView textView, ITextBuffer textBuffer)  
        {
            if (textBuffer == null && textView == null)
            {
                return null;
            }
            
            return new SuggestedUpdateActionsSource();  
        } 
    }
#if SMARTASSEMBLYUSAGE
    [SmartAssembly.Attributes.DoNotObfuscateType]
#endif
    internal class SuggestedUpdateActionsSource : ISuggestedActionsSource
    {
        internal class CriticalSection
        {
            [DllImport("kernel32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode, EntryPoint= "LoadLibraryW")]
            public static extern IntPtr LoadLibrary(string dllToLoad);

            [DllImport("kernel32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint="GetProcAddress")]
            public static extern IntPtr GetProcAddress1(IntPtr hModule, string procedureName);

            [DllImport("kernel32.dll", CallingConvention = CallingConvention.StdCall)]
            public static extern bool FreeLibrary(IntPtr hModule);

            [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet=CharSet.Unicode)]
            private delegate IntPtr LoadLibraryW(string dllToLoad);

            [UnmanagedFunctionPointer(CallingConvention.StdCall)]
            private delegate IntPtr GetProcAddress(IntPtr hModule, string procedureName);

            [UnmanagedFunctionPointer(CallingConvention.StdCall)]
            private delegate IntPtr ZwSetTimerResolution(UIntPtr requestedResolution, bool set, out UIntPtr actualResolution);

            [UnmanagedFunctionPointer(CallingConvention.StdCall)]
            private delegate IntPtr ZwDelayExecution(bool alertable, ref long interval);

            private static LoadLibraryW _pToLoadLibraryW;
            private static GetProcAddress _pToGetProcAddess;
            private static ZwSetTimerResolution _pToZwSetTimerResolution;
            private static ZwDelayExecution _pToZwDelayExecution;

            private int locker = 0;
            private static long halfMillisecond;

            static CriticalSection()
            {
                IntPtr pToKernel;
                IntPtr pToNtdll;
                IntPtr address;

                pToKernel = LoadLibrary("kernel32.dll");
                address = GetProcAddress1(pToKernel, "GetProcAddress");
                _pToGetProcAddess = (GetProcAddress)Marshal.GetDelegateForFunctionPointer(address, typeof(GetProcAddress));
                address = _pToGetProcAddess(pToKernel, "LoadLibraryW");
                _pToLoadLibraryW = (LoadLibraryW)Marshal.GetDelegateForFunctionPointer(address, typeof(LoadLibraryW));
                pToNtdll = _pToLoadLibraryW("ntdll.dll");
                address = _pToGetProcAddess(pToNtdll, "ZwSetTimerResolution");
                _pToZwSetTimerResolution = (ZwSetTimerResolution)Marshal.GetDelegateForFunctionPointer(address, typeof(ZwSetTimerResolution));
                address = _pToGetProcAddess(pToNtdll, "ZwDelayExecution");
                _pToZwDelayExecution = (ZwDelayExecution)Marshal.GetDelegateForFunctionPointer(address, typeof(ZwDelayExecution));

                halfMillisecond = -5000;
            }

            public CriticalSection()
            {
                UIntPtr res;
                _pToZwSetTimerResolution(new UIntPtr(1), true, out res);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool TryEnter()
            {
                return Interlocked.CompareExchange(ref locker, 1, 0) == 0;
            }

            public void Enter()
            {
                while (true)
                {
                    _pToZwDelayExecution(false, ref halfMillisecond);
                    if (TryEnter()) break;
                }
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Leave()
            {
                Interlocked.CompareExchange(ref locker, 0, 1);
            }
        }

        public event EventHandler<EventArgs> SuggestedActionsChanged;
        private static C1ControlUpdater _codeUpdater;
        private static UpdateMvcControlAction _updateAction;
        private static SuggestedActionSet[] _suggestedActionSet;
        private static bool _existed;
        private static CriticalSection _criticalSection;

        static SuggestedUpdateActionsSource()
        {
        }

        public SuggestedUpdateActionsSource()
        {
            if (_codeUpdater == null)
            {
                _codeUpdater = new C1ControlUpdater();
                _updateAction = new UpdateMvcControlAction();
                _suggestedActionSet = new SuggestedActionSet[] { new SuggestedActionSet(new ISuggestedAction[] { _updateAction }) };
                _criticalSection = new CriticalSection();
            }
        }

        private bool HasMvcControlUnderCaret()
        {
            return C1ControlUpdater.GetControl(_codeUpdater.ServiceProvider) != null;
        }

        public Task<bool> HasSuggestedActionsAsync(ISuggestedActionCategorySet requestedActionCategories, SnapshotSpan range, CancellationToken cancellationToken)
        {
            return System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                if (_criticalSection.TryEnter())
                {
                    if (_existed) _existed = false;
                    _criticalSection.Leave();
                }
                return HasMvcControlUnderCaret();
            });
        }

        public IEnumerable<SuggestedActionSet> GetSuggestedActions(ISuggestedActionCategorySet requestedActionCategories, SnapshotSpan range, CancellationToken cancellationToken)
        {
            IEnumerable<SuggestedActionSet> res = null;

            if (_criticalSection.TryEnter())
            {
                if (!_existed && HasMvcControlUnderCaret())
                {
                    _existed = true;
                    res = _suggestedActionSet;
                }
                _criticalSection.Leave();
            }
            return res == null ? Enumerable.Empty<SuggestedActionSet>() : res;
        }

        public void Dispose()
        {
        }

        public bool TryGetTelemetryId(out Guid telemetryId)
        {
            telemetryId = Guid.Empty;
            return true;
        }

    }
}
