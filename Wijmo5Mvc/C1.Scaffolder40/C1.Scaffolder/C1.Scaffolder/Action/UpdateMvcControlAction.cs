﻿using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Imaging.Interop;
using System.Threading;
using C1.Scaffolder.Localization;

namespace C1.Scaffolder
{
#if SMARTASSEMBLYUSAGE
    [SmartAssembly.Attributes.DoNotObfuscateType]
#endif
    internal class UpdateMvcControlAction: ISuggestedAction   
    {
        private string __display;
        private C1ControlUpdater __codeUpdater;

        public bool HasActionSets  
        {  
            get { return false; }  
        }  

        public string DisplayText  
        {  
            get { return __display; }  
        }
         
        public ImageMoniker IconMoniker  
        {
           get { return default(ImageMoniker); }  
        }

        public string IconAutomationText  
        {  
            get
            {  
                return null;  
            }  
        }  

        public string InputGestureText  
        {
            get  
            {  
                return null;  
            }  
        }

        public bool HasPreview  
        {  
            get { return false; }  
        }

        public UpdateMvcControlAction()
        {
            __display = Resources.QuickActionText;
        }

        public Task<IEnumerable<SuggestedActionSet>> GetActionSetsAsync(CancellationToken cancellationToken)  
        {  
            return Task.FromResult<IEnumerable<SuggestedActionSet>>(null);  
        }

        public Task<object> GetPreviewAsync(CancellationToken cancellationToken)  
        {
            return null;        
        }  

        public void Invoke(CancellationToken cancellationToken)  
        {
            __codeUpdater = new C1ControlUpdater();
            __codeUpdater.ShowForm();
        }

        public void Dispose()  
        {
            __codeUpdater = null;
        }  

        public bool TryGetTelemetryId(out Guid telemetryId)  
        {  
            // This is a sample action and doesn't participate in LightBulb telemetry  
            telemetryId = Guid.Empty;  
            return false;  
        }
    }
}
