﻿using System.Collections;
using System.ComponentModel;
using System.Linq;
using C1.Scaffolder.Models;
using System.Drawing.Text;
using System.Drawing;

namespace C1.Scaffolder.Converters
{
    internal class FontFamilyConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return false;
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            FontFamily[] fontFamilies;
            var installedFontCollection = new InstalledFontCollection();
            fontFamilies = installedFontCollection.Families;
            return new StandardValuesCollection(fontFamilies.Select(p => p.Name).ToList());
        }
    }
}
