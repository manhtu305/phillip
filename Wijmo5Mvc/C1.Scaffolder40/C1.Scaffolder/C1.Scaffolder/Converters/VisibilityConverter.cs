﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace C1.Scaffolder.Converters
{
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(parameter != null)
            {
                if (parameter.GetType().IsArray)
                {
                    Array parameters = (Array)parameter;
                    foreach (object param in parameters)
                    {
                        if (object.Equals(value, param))
                        {
                            return Visibility.Visible;
                        }
                    }
                    return Visibility.Collapsed;
                }

                return object.Equals(value, parameter) ? Visibility.Visible : Visibility.Collapsed;
            }

            return ((bool)value) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
