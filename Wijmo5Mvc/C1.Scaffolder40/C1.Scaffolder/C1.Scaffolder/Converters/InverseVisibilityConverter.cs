﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace C1.Scaffolder.Converters
{
    public class InverseVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter != null)
            {
                if (parameter.GetType().IsArray)
                {
                    Array parameters = (Array)parameter;
                    foreach (object param in parameters)
                    {
                        if (object.Equals(value, param))
                        {
                            return Visibility.Collapsed;
                        }
                    }
                    return Visibility.Visible;
                }

                return object.Equals(value, parameter) ? Visibility.Collapsed : Visibility.Visible;
            }

            if(value.GetType() == typeof(Visibility))
            {
                return ((Visibility)value == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
            }

            return ((bool)value)? Visibility.Collapsed: Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
