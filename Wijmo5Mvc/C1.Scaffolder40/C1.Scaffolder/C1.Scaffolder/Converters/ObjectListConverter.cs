﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Linq;
using System.Collections.Generic;

namespace C1.Scaffolder.Converters
{
    public class ObjectListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var list = value as IEnumerable<object>;
            if (list == null) return string.Empty;

            return string.Join(",", list.Select(item => item == null ? string.Empty : item.ToString()));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = value as string;
            if (string.IsNullOrEmpty(s)) return null;

            return s.Split(',').ToArray();
        }
    }
}
