﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.AspNet.Scaffolding;
using System.Linq;
using System.Collections.Generic;

namespace C1.Scaffolder.UI
{
    /// <summary>
    /// Interaction logic for ControlSelectionWindow.xaml
    /// </summary>
    public partial class ControlSelectionWindow
    {
        public ControlSelectionWindow(bool isInsert)
        {
            this.DataContext = this;
            IsInsert = isInsert;
            LicenseHelper.Validate();
            InitializeComponent();
            SetControlsItemsSource();
            controls.Focus();
        }

        private static IEnumerable<ControlDescription> GetDataControlsItemsSource(bool isInsert)
        {
            if (!isInsert)
            {
                return ControlList.DataControls;
            }

            return ControlList.DataControls.Where(cd => 
            {
                return cd.ControlType != ControlList.Input;
            });
        }

        public bool IsInsert { get; private set; }

        internal string SelectedControl { get { return (string)controls.SelectedValue; } }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void controls_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (controls.SelectedItem != null)
            {
                DialogResult = true;
            }
        }

        private void TabControl_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (e.Source == Groups)
            {
                SetControlsItemsSource();
            }
        }

        private void SetControlsItemsSource()
        {
            var tabItem = Groups.SelectedItem as TabItem;
            var group = tabItem == null ? null : tabItem.Header as string;
            if (group == Localization.Resources.ControlsData)
            {
                controls.ItemsSource = GetDataControlsItemsSource(IsInsert);
                controls.SelectedIndex = 0;
            }
            else if (group == Localization.Resources.ControlsInput)
            {
                controls.ItemsSource = ControlList.InputControls;
                controls.SelectedIndex = 0;
            }
            else if (group == Localization.Resources.ControlsOlap)
            {
                controls.ItemsSource = ControlList.OlapControls;
                controls.SelectedIndex = 0;
            }
            else
            {
                controls.ItemsSource = null;
            }
        }
    }
}
