﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Navigation;
using C1.Scaffolder.Models;
using System.Globalization;
using C1.Web.Mvc;
using System.Windows.Media;
using System.ComponentModel;

namespace C1.Scaffolder.UI
{
    /// <summary>
    /// Interaction logic for ConfigurationWindow.xaml
    /// </summary>
    public partial class ControlConfigurationWindow
    {
        private DependencyPropertyDescriptor dp;
        private System.EventHandler controlName_handler;

        internal ControlConfigurationWindow(ComponentOptionsModel viewModel)
        {
            InitializeComponent();
            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();
            DataContext = viewModel;
            //this event is applied for all ComponentOptionsModel, not only for DataBoundOptionsModel
            viewModel.IsValidChanged += ViewModel_IsValidChanged;

            var dataBoundModel = viewModel as DataBoundOptionsModel;
            if (dataBoundModel != null)
            {
                dataBoundModel.ItemsBoundControl.IsBoundChanged += ViewModel_IsBoundChanged;
                //dataBoundModel.IsValidChanged += ViewModel_IsValidChanged;
            }

            viewModel.SelectedControllerChanged += ViewModel_SelectedControllerChanged;
            viewModel.SelectedActionChanged += ViewModel_SelectedActionChanged;

            dp = DependencyPropertyDescriptor.FromProperty(Label.ContentProperty, typeof(Label));
            controlName_handler = new System.EventHandler(MeasureString);
            dp.AddValueChanged(controlName, controlName_handler);
        }

        private void MeasureString(object obj, System.EventArgs args)
        {
            string content = this.controlName.Content.ToString();
            var typeface = new Typeface(controlName.FontFamily.FamilyNames.ToString());
            FormattedText formattedText = null;
            double fontSize = controlName.FontSize;
            double actualWidth = controlName.Width - controlName.BorderThickness.Left - controlName.BorderThickness.Right - controlName.Margin.Left - controlName.Margin.Right;

            while (true)
            {
                formattedText = new FormattedText(content, CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, typeface, fontSize, Brushes.Black);
                if (formattedText.Width > actualWidth)
                    fontSize -= 0.5;
                else
                    break;
            }

            controlName.FontSize = fontSize;
            formattedText = null;
            typeface = null;
            content = null;
            dp.RemoveValueChanged(controlName, controlName_handler);
            controlName_handler = null;
            dp = null;
        }

        private void MeasureString(string candidate)
        {
            
        }

        void ViewModel_SelectedActionChanged(object sender, System.EventArgs e)
        {
            UpdateButtonOk();
        }

        void ViewModel_SelectedControllerChanged(object sender, System.EventArgs e)
        {
            UpdateButtonOk();
        }

        private void ViewModel_IsBoundChanged(object sender, System.EventArgs e)
        {
            CollectionViewSource.GetDefaultView(TabControl.Items).Refresh();
        }

        private void ViewModel_IsValidChanged(object sender, System.EventArgs e)
        {
            UpdateButtonOk();
        }

        private void UpdateButtonOk()
        {
            var expression = ButtonOk.GetBindingExpression(IsEnabledProperty);
            if (expression != null)
            {
                expression.UpdateTarget();
            }
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            var model = DataContext as ControlOptionsModel;
            if (model != null && !model.IsInsertOnCodePage && model.ControllerExist())
            {
                var result = MessageBox.Show(string.Format(CultureInfo.CurrentCulture,
                    Localization.Resources.ControllerExistWarning, model.ControllerName),
                    "ComponentOne MVC Scaffolder", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes) {
                    DialogResult = true;
                }
            }
            else
            {
                DialogResult = true;
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Frame_LoadCompleted(object sender, NavigationEventArgs e)
        {
            var frame = sender as Frame;
            if (frame == null)
            {
                return;
            }

            var content = frame.Content as FrameworkElement;
            if (content == null)
            {
                return;
            }

            content.DataContext = DataContext;

            UIUpdateUtils utils = new UIUpdateUtils();
            ControlOptionsModel optionModel = DataContext as ControlOptionsModel;
            ISupportUpdater controlSetting = optionModel.Component as ISupportUpdater;
            if (controlSetting != null && controlSetting.ParsedSetting != null)
            {
                //update button's text
                ButtonOk.Content = Localization.Resources.UpdateButtonText;
                //adjust some control to support edit property as textvalue
                utils.AdjustContentControl(content as ContentControl, controlSetting.ParsedSetting);
            }
        }

        private void Frame_Navigated(object sender, NavigationEventArgs e)
        {
            var frame = sender as Frame;
            if (frame == null)
            {
                return;
            }

            frame.NavigationService.RemoveBackEntry();
        }
    }
}
