﻿using System.Windows;

namespace C1.Scaffolder.UI.Inputs
{
    /// <summary>
    /// Interaction logic for Fields.xaml
    /// </summary>
    public partial class Fields
    {
        /// <summary>
        /// The constructor.
        /// </summary>
        public Fields()
        {
            InitializeComponent();
        }

        private void AutoGenerateFieldsCkb_OnChecked(object sender, RoutedEventArgs e)
        {
            var expression = FieldsEditor.GetBindingExpression(CollectionEditor.ItemsSourceProperty);
            if (expression != null)
            {
                expression.UpdateTarget();
            }
        }
    }
}
