﻿using C1.Scaffolder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using C1.Scaffolder.Models.Sheet;
using C1.Web.Mvc.Sheet;

namespace C1.Scaffolder.UI.FlexSheet
{
	/// <summary>
	/// Interaction logic for AppendedSheets.xaml
	/// </summary>
	public partial class AppendedSheets : UserControl
	{
        ContextMenu contextMenu;
		public AppendedSheets()
		{
			InitializeComponent();
			SheetEditor.AddButton.Visibility = Visibility.Collapsed;
			var btn = new Button() {
				Content = SheetEditor.AddButton.Content,
				MinHeight = 24, 
				HorizontalAlignment = HorizontalAlignment.Right };
			SheetEditor.Toolbar.Children.Add(btn);
            object ItemsEditor = SheetEditor.FindName("ItemsEditor");
            if (ItemsEditor != null && ItemsEditor is PropertyGrid)
            {
                ((PropertyGrid)ItemsEditor).ValueChanged += AppendedSheets_PropertyValueChanged;
            }
			contextMenu = new ContextMenu();
			var btnBound = new MenuItem() { Header = C1.Scaffolder.Localization.Resources.BoundSheet };
            
            var btnUnbound = new MenuItem() { Header = C1.Scaffolder.Localization.Resources.UnboundSheet };
			btnBound.Click += (o, e) => {
				var sheet = new BoundSheet<object>();
				AddListItem(sheet);
                //TODO for GroupDescriptions and SortDescriptions
			};
			btnUnbound.Click += (o, e) => AddListItem(UnboundSheet.CreateNew());
			contextMenu.Items.Add(btnBound);
			contextMenu.Items.Add(btnUnbound);
			btn.ContextMenu = contextMenu;
			btn.Click += (o, ev) =>
			{
				btn.ContextMenu.PlacementTarget = btn;
				btn.ContextMenu.Placement = PlacementMode.Bottom;
				ContextMenuService.SetPlacement(btn, PlacementMode.Bottom);
				btn.ContextMenu.IsOpen = true;
			};

            DataContextChanged += AppendedSheets_DataContextChanged;
		}

        private void AppendedSheets_PropertyValueChanged(object s, System.Windows.Forms.PropertyValueChangedEventArgs e)
        {
            if (e.ChangedItem.Label.Equals("DataContext") || e.ChangedItem.Label.Equals("ModelClass"))
            {
                FlexSheetOptionsModel optionModel = DataContext as FlexSheetOptionsModel;
                optionModel.UpdateIsValid();
            }
        }

        private void AppendedSheets_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {            
            OptionModel_BoundModeChanged(null, EventArgs.Empty);//for first time

            ControlOptionsModel optionModel = DataContext as ControlOptionsModel;
            optionModel.BoundModeChanged += OptionModel_BoundModeChanged;
        }

        private void OptionModel_BoundModeChanged(object sender, EventArgs e)
        {
            if (contextMenu != null && contextMenu.Items.Count>0)
            {
                MenuItem btBound = contextMenu.Items[0] as MenuItem;
                ControlOptionsModel optionModel = DataContext as ControlOptionsModel;
                btBound.IsEnabled = optionModel.IsBoundMode;
            }
        }

        private void AddListItem(Sheet sheet)
		{
			SheetEditor.ItemsSource.Add(sheet);
			SheetEditor._showList.Add(new ListBoxItem() { Content = sheet.ToString() });
			SheetEditor.ItemsSelector.SelectedIndex = SheetEditor.ItemsSource.Count - 1;
		}
	}
}
