﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace C1.Scaffolder.UI.FlexGrid
{
    /// <summary>
    /// Interaction logic for ODataBaseSetting.xaml
    /// </summary>
    public partial class BaseODataSetting : UserControl
    {
        public BaseODataSetting()
        {
            InitializeComponent();
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            
            if(!(bool)e.NewValue)
            {
                return;
            }

            //IsVisible = true
            tbServiceUrl.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            tbTableName.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            tbFields.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            tbKeys.GetBindingExpression(TextBox.TextProperty).UpdateTarget();

            tbFilterDefinition.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            tbExpand.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            tbODataVersion.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            cbInferDataTypes.GetBindingExpression(CheckBox.IsCheckedProperty).UpdateTarget();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            e.Handled = !regex.IsMatch(e.Text);
        }
    }
}
