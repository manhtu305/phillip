﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using C1.Scaffolder.Models.Grid;
using C1.Web.Mvc;

namespace C1.Scaffolder.UI.FlexGrid
{
    /// <summary>
    /// Interaction logic for Grouping.xaml
    /// </summary>
    public partial class Grouping : UserControl
    {
        delegate Point GetPositionDelegate(IInputElement element);

        public Grouping()
        {
            InitializeComponent();
        }

        private void UpButton_Click(object sender, RoutedEventArgs e)
        {
            var oldSelectedIndex = lvGroupDescriptions.SelectedIndex;
            if (oldSelectedIndex == 0)
            {
                return;
            }
            DownButton.IsEnabled = true;
            var itemsSource = lvGroupDescriptions.ItemsSource as IList;
            (itemsSource as ObservableCollection<DesignPropertyGroupDescription>).Move(oldSelectedIndex, oldSelectedIndex - 1);
        }

        private void DownButton_Click(object sender, RoutedEventArgs e)
        {
            var oldSelectedIndex = lvGroupDescriptions.SelectedIndex;
            if (oldSelectedIndex >= lvGroupDescriptions.Items.Count - 1)
            {
                return;
            }
            UpButton.IsEnabled = true;
            var itemsSource = lvGroupDescriptions.ItemsSource as IList;
            (itemsSource as ObservableCollection<DesignPropertyGroupDescription>).Move(oldSelectedIndex, oldSelectedIndex + 1);
        }

        private void lvGroupDescriptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listview = sender as ListView;
            var selectedIndex = listview.SelectedIndex;
            UpButton.IsEnabled = selectedIndex > 0;
            DownButton.IsEnabled = selectedIndex > -1 && selectedIndex < (listview.Items.Count - 1);
        }
    }
}
