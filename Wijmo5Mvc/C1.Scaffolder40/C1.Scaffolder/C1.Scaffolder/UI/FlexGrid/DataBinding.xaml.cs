﻿using C1.Scaffolder.Models.Grid;
using C1.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace C1.Scaffolder.UI.FlexGrid
{
    /// <summary>
    /// Interaction logic for DataBinding.xaml
    /// </summary>
    public partial class DataBinding : UserControl
    {
        private bool _selectionChangedCauseByUpdate = false;

        public DataBinding()
        {
            InitializeComponent();
        }

        private void ComboBox_IsEnabledChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if (!((ComboBox)sender).IsEnabled)
            {
                ((ComboBox)sender).SelectedIndex = -1;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox.SelectedValue == null)
            {
                comboBox.SelectedIndex = -1;
                return;
            }

            EDataSourceType source = (EDataSourceType)comboBox.SelectedValue;
            var optionsModel = DataContext as FlexGridOptionsModel;

            switch (source)
            {
                case EDataSourceType.CollectionView:
                    InitCollectionView(optionsModel);
                    panelCollectionView.Visibility = Visibility.Visible;
                    panelOData.Visibility = Visibility.Collapsed;
                    panelODataVirtual.Visibility = Visibility.Collapsed;
                    break;

                case EDataSourceType.ODataCollectionView:
                    InitODataCollectionView(optionsModel);
                    panelCollectionView.Visibility = Visibility.Collapsed;
                    panelOData.Visibility = Visibility.Visible;
                    panelODataVirtual.Visibility = Visibility.Collapsed;
                    break;

                case EDataSourceType.ODataVirtualCollectionView:
                    InitODataVirtualCollectionView(optionsModel);
                    panelCollectionView.Visibility = Visibility.Collapsed;
                    panelOData.Visibility = Visibility.Collapsed;
                    panelODataVirtual.Visibility = Visibility.Visible;
                    break;
            }

            //first SelectedChange event cause by loading update UI.
            if (_selectionChangedCauseByUpdate)
            {
                _selectionChangedCauseByUpdate = false;
            }
            else
            {
                optionsModel.Grid.DesignColumns.Clear();
                optionsModel.Grid.DesignGroupDescriptions.Clear();
                optionsModel.Grid.DesignSortDescriptions.Clear();
            }
        }

        private void InitCollectionView(FlexGridOptionsModel optionsModel)
        {
            if ((optionsModel.Grid.ItemsSource is CollectionViewService<object>))
            {
                return;
            }

            optionsModel.InitODataSourceMode(EDataSourceType.CollectionView);

            cbBoundMode.GetBindingExpression(CheckBox.IsCheckedProperty).UpdateTarget();
            tbCollectionViewReadActionUrl.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
            cbCollectionViewDisableServerRead.GetBindingExpression(CheckBox.IsCheckedProperty).UpdateTarget();
        }

        private void InitODataCollectionView(FlexGridOptionsModel optionsModel)
        {
            if ((optionsModel.Grid.ItemsSource is ODataCollectionViewService<object>))
            {
                return;
            }

            optionsModel.InitODataSourceMode(EDataSourceType.ODataCollectionView);
            
            cbPageOnServer.GetBindingExpression(CheckBox.IsCheckedProperty).UpdateTarget();
            cbSortOnServer.GetBindingExpression(CheckBox.IsCheckedProperty).UpdateTarget();
            cbFilterOnServer.GetBindingExpression(CheckBox.IsCheckedProperty).UpdateTarget();

            ClearCollectionViewPanel();
        }

        private void InitODataVirtualCollectionView(FlexGridOptionsModel optionsModel)
        {
            if ((optionsModel.Grid.ItemsSource is ODataVirtualCollectionViewService<object>))
            {
                return;
            }

            optionsModel.InitODataSourceMode(EDataSourceType.ODataVirtualCollectionView);
            ClearCollectionViewPanel();
        }

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            var optionsModel = DataContext as FlexGridOptionsModel;
            EDataSourceType dataSourceType = optionsModel.DataSourceType;
            if (dataSourceType != default(EDataSourceType))
            {
                _selectionChangedCauseByUpdate = true;
                cbItemsSourceType.SelectedValue = dataSourceType;
            }
        }

        private void ClearCollectionViewPanel()
        {
            cbxDbContextTypes.SelectedIndex = -1;
            cbxModelTypes.SelectedIndex = -1;
            cbBoundMode.IsChecked = false;
        }
    }
}
