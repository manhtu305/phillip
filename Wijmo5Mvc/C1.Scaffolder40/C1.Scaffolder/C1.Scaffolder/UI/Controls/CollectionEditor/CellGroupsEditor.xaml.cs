﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using C1.Scaffolder.Models.MultiRow;
using C1.Web.Mvc.MultiRow;

namespace C1.Scaffolder.UI
{
    /// <summary>
    /// Define the CellGroupsEditor.
    /// </summary>
    public partial class CellGroupsEditor
    {
        private readonly ObservableCollection<ICellGroupWrapper> _showList = new ObservableCollection<ICellGroupWrapper>();

        /// <summary>
        /// Occurs after an item is created.
        /// </summary>
        public event EventHandler<ItemCreatedEventArgs> ItemCreated;

        /// <summary>
        /// Gets or sets the list to be edited by the control.
        /// </summary>
        public IList ItemsSource
        {
            get { return (IList) GetValue(ItemsSourceProperty); }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        /// <summary>
        /// Identifies the <see cref="ItemsSource"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register(
                "ItemsSource",
                typeof (IList),
                typeof (CellGroupsEditor),
                new PropertyMetadata(OnItemsSourcePropertyChanged)
                );

        private static void OnItemsSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var sender = d as CellGroupsEditor;
            if (sender != null)
            {
                sender.OnItemsSourceChanged();
            }
        }

        /// <summary>
        /// The constructor.
        /// </summary>
        public CellGroupsEditor()
        {
            InitializeComponent();
        }

        private void ItemEditor_ValueChanged(object sender, System.Windows.Forms.PropertyValueChangedEventArgs e)
        {
            if (e.ChangedItem == null || e.ChangedItem.PropertyDescriptor == null)
                return;
            
            var name = e.ChangedItem.PropertyDescriptor.Name;
            if (name == "Header" || name == "Binding")
            {
                var wrapper = ItemsSelector.SelectedItem as ICellGroupWrapper;
                if (wrapper != null)
                {
                    wrapper.OnPropertyChanged("DisplayText");
                }
            }
        }

        private void ItemsSelector_SelectionChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var wrapper = ItemsSelector.SelectedItem as ICellGroupWrapper;
            ItemEditor.SelectedObject = wrapper == null ? null : wrapper.Object;
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var wrapper = ItemsSelector.SelectedItem as ICellGroupWrapper;
            if (wrapper == null) return;

            var wrapperContainer = GetWrappersContainer(wrapper);
            var itemContainer = GetItemsContainer(wrapper);

            var index = wrapperContainer.IndexOf(wrapper);

            // select next item.
            var nextIndex = index + 1;
            // select the previous item if remove the last item.
            if (nextIndex >= wrapperContainer.Count)
            {
                nextIndex = index - 1;
            }

            // get the next item to be selected
            var nextItem = nextIndex < 0 ? wrapper.Parent : wrapperContainer[nextIndex];

            // remove the item
            itemContainer.RemoveAt(index);

            // sync with wrapper
            wrapperContainer.RemoveAt(index);
            wrapper.IsSelected = false;

            if (nextItem != null)
            {
                nextItem.IsSelected = true;
            }
        }

        private void UpButton_Click(object sender, RoutedEventArgs e)
        {
            MoveSelectedItem(-1);
        }

        private void DownButton_Click(object sender, RoutedEventArgs e)
        {
            MoveSelectedItem(1);
        }

        private void AddGroupButton_Click(object sender, RoutedEventArgs e)
        {
            var wrapper = ItemsSelector.SelectedItem as ICellGroupWrapper;
            if (wrapper != null)
            {
                wrapper.IsSelected = false;
            }

            var groupWrapper = CreateGroup();
            groupWrapper.IsSelected = true;
        }

        private void AddCellButton_Click(object sender, RoutedEventArgs e)
        {
            var wrapper = ItemsSelector.SelectedItem as ICellGroupWrapper;
            if (wrapper == null && _showList.Count > 0) return;

            var groupWrapper = wrapper == null ? CreateGroup() : (GroupWrapper)(wrapper.Parent ?? wrapper);

            // add new cell
            var cell = new Cell();
            groupWrapper.Group.Cells.Add(cell);
            OnItemCreated(cell);

            // sync with wrapper
            var cellWrapper = new CellWrapper(groupWrapper, cell);
            groupWrapper.Children.Add(cellWrapper);

            if (wrapper != null)
            {
                wrapper.IsSelected = false;
            }

            // select the created cell
            cellWrapper.IsSelected = true;
        }

        private void OnItemsSourceChanged()
        {
            _showList.Clear();

            foreach (CellGroup group in ItemsSource)
            {
                var groupWrapper = new GroupWrapper(group);
                _showList.Add(groupWrapper);

                foreach (Cell cell in group.Cells)
                {
                    var cellWrapper = new CellWrapper(groupWrapper, cell);
                    groupWrapper.Children.Add(cellWrapper);
                }
            }

            ItemsSelector.ItemsSource = _showList;
        }

        protected virtual void OnItemCreated(object item)
        {
            if (ItemCreated != null)
            {
                ItemCreated(this, new ItemCreatedEventArgs(item));
            }
        }


        #region drag drop

        private ICellGroupWrapper _draggedWrapper;
        private ICellGroupWrapper _droppedWrapper;
        private Point _startPoint;

        private void ItemsSelector_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _startPoint = e.GetPosition(ItemsSelector);
            var item = GetNearestContainer(e.OriginalSource as UIElement);
            _draggedWrapper = item == null ? null : GetWrapper(item);
            _droppedWrapper = null;
        }

        private void ItemsSelector_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            var pos = e.GetPosition(ItemsSelector);
            if (e.LeftButton == MouseButtonState.Pressed && _draggedWrapper != null 
                && CheckDragDistance(_startPoint - pos))
            {
                DragDrop.DoDragDrop(ItemsSelector, _draggedWrapper, DragDropEffects.Move | DragDropEffects.Copy);
            }
        }

        private void ItemsSelector_DragOver(object sender, DragEventArgs e)
        {
            ClearDroppedWrapper();
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            var pos = e.GetPosition(ItemsSelector);
            if (!CheckDragDistance(_startPoint - pos)) return;

            var item = GetNearestContainer(e.OriginalSource as UIElement);
            var wrapper = GetWrapper(item);
            if (wrapper != null && CheckDropTarget(wrapper))
            {
                var isCopy = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
                e.Effects = isCopy ? DragDropEffects.Copy : DragDropEffects.Move;
                _droppedWrapper = wrapper;
                _droppedWrapper.IsDropTarget = true;
            }
        }

        private void ItemsSelector_DragLeave(object sender, DragEventArgs e)
        {
            ClearDroppedWrapper();
        }

        private void ItemsSelector_Drop(object sender, DragEventArgs e)
        {
            ClearDroppedWrapper();
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            var item = GetNearestContainer(e.OriginalSource as UIElement);
            var targetWrapper = GetWrapper(item);
            if (targetWrapper != null && _draggedWrapper != null && CheckDropTarget(targetWrapper))
            {
                var isCopy = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
                if (isCopy)
                {
                    CopyItem(_draggedWrapper, targetWrapper);
                }
                else
                {
                    MoveItem(_draggedWrapper, targetWrapper);
                }
            }
        }

        private void CopyItem(ICellGroupWrapper srcWrapper, ICellGroupWrapper desWrapper)
        {
            // copy group
            var srcGroupWrapper = srcWrapper as GroupWrapper;
            if (srcGroupWrapper != null)
            {
                var desGroupWrapper = desWrapper as GroupWrapper;
                // can only copy the group next to another group
                if (desGroupWrapper == null) return;

                var desIndex = _showList.IndexOf(desGroupWrapper) + 1;

                // copy the group
                var clonedGroupWrapper = srcGroupWrapper.Clone();
                ItemsSource.Insert(desIndex, clonedGroupWrapper.Group);

                // sync with wrapper
                _showList.Insert(desIndex, clonedGroupWrapper);

                // adjust selection
                srcGroupWrapper.IsSelected = false;
                clonedGroupWrapper.IsSelected = true;

                return;
            }

            // copy cell
            var srcCellWrapper = srcWrapper as CellWrapper;
            if (srcCellWrapper != null)
            {
                var desGroupWrapper = desWrapper as GroupWrapper;
                int desIndex;

                if (desGroupWrapper == null)
                {
                    // des is cell
                    var desCellWrapper = desWrapper as CellWrapper;
                    if (desCellWrapper == null) return;

                    desGroupWrapper = desCellWrapper.Parent as GroupWrapper;
                    if (desGroupWrapper == null) return;

                    desIndex = desGroupWrapper.Children.IndexOf(desCellWrapper) + 1;
                }
                else
                {
                    // des is group, add to the end of children
                    desIndex = desGroupWrapper.Children.Count;
                }

                // copy the cell
                var clonedCell = (Cell)srcCellWrapper.Cell.Clone();
                desGroupWrapper.Group.Cells.Insert(desIndex, clonedCell);

                // syn with wrapper
                var cellWrapper = new CellWrapper(desGroupWrapper, clonedCell);
                desGroupWrapper.Children.Insert(desIndex, cellWrapper);

                // adjust selection
                srcCellWrapper.IsSelected = false;
                cellWrapper.IsSelected = true;
            }
        }

        private void MoveItem(ICellGroupWrapper srcWrapper, ICellGroupWrapper desWrapper)
        {
            // move the group
            var srcGroupWrapper = srcWrapper as GroupWrapper;
            if (srcGroupWrapper != null)
            {
                var desGroupWrapper = desWrapper as GroupWrapper;
                // can only move the group next to another group
                if (desGroupWrapper == null) return;

                var srcIndex = _showList.IndexOf(srcGroupWrapper);

                // remove the src group
                ItemsSource.RemoveAt(srcIndex);

                // sync with wrapper
                _showList.RemoveAt(srcIndex);

                var desIndex = _showList.IndexOf(desGroupWrapper) + 1;

                // move to the des
                ItemsSource.Insert(desIndex, srcGroupWrapper.Group);

                // sync with wrapper
                _showList.Insert(desIndex, srcGroupWrapper);

                // adjust selection
                srcGroupWrapper.IsSelected = true;

                return;
            }

            // move the cell
            var srcCellWrapper = srcWrapper as CellWrapper;
            if (srcCellWrapper != null)
            {
                srcGroupWrapper = srcCellWrapper.Parent as GroupWrapper;
                if (srcGroupWrapper == null) return;

                var desGroupWrapper = desWrapper as GroupWrapper;
                var desCellWrapper = desWrapper as CellWrapper;

                // des is cell
                if (desGroupWrapper == null)
                {
                    if (desCellWrapper == null) return;

                    desGroupWrapper = desCellWrapper.Parent as GroupWrapper;
                    if (desGroupWrapper == null) return;
                }

                var srcIndex = srcGroupWrapper.Children.IndexOf(srcCellWrapper);

                // remove the src cell
                srcGroupWrapper.Group.Cells.RemoveAt(srcIndex);

                // sync with wrapper
                srcGroupWrapper.Children.RemoveAt(srcIndex);

                var desIndex = desCellWrapper == null
                    ? desGroupWrapper.Children.Count
                    : desGroupWrapper.Children.IndexOf(desCellWrapper) + 1;

                // move the cell
                desGroupWrapper.Group.Cells.Insert(desIndex, srcCellWrapper.Cell);

                // sync with wrapper
                var cellWrapper = new CellWrapper(desGroupWrapper, srcCellWrapper.Cell);
                desGroupWrapper.Children.Insert(desIndex, cellWrapper);

                // adjust selection
                srcCellWrapper.IsSelected = false;
                cellWrapper.IsSelected = true;
            }
        }

        private bool CheckDragDistance(Vector diff)
        {
            return Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance
                   || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance;
        }

        private bool CheckDropTarget(ICellGroupWrapper targetWrapper)
        {
            if (targetWrapper == null) return false;

            if (_draggedWrapper is GroupWrapper)
            {
                // drag the group, can only be dropped to other groups.
                var targetGroupWrapper = targetWrapper as GroupWrapper;
                return targetGroupWrapper != null && _draggedWrapper != targetGroupWrapper;
            }

            // drag the cell, can be dropped to other cells and groups.
            return _draggedWrapper != targetWrapper;
        }

        private void ClearDroppedWrapper()
        {
             if (_droppedWrapper != null)
            {
                _droppedWrapper.IsDropTarget = false;
                _droppedWrapper = null;
            }
        }

        private ICellGroupWrapper GetWrapper(TreeViewItem item)
        {
            if (item == null) return null;

            var wrapper = item.Header as ICellGroupWrapper;
            if (wrapper == null)
            {
                var parent = FindVisualParent<ItemsControl>(item);
                if (parent != null)
                {
                    wrapper = parent.ItemContainerGenerator.ItemFromContainer(item) as ICellGroupWrapper;
                }
            }

            return wrapper;
        }

        private TreeViewItem GetNearestContainer(UIElement element)
        {
            var container = element as TreeViewItem;
            while (container == null && element != null)
            {
                element = VisualTreeHelper.GetParent(element) as UIElement;
                container = element as TreeViewItem;
            }

            return container;
        }

        private T FindVisualParent<T>(UIElement child) where T : UIElement
        {
            if (child == null) return null;

            var parent = VisualTreeHelper.GetParent(child) as UIElement;
            while(parent!=null)
            {
                var found = parent as T;
                if (found != null) return found;

                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }

            return null;
        }

        #endregion

        private GroupWrapper CreateGroup()
        {
            // create a new group
            var group = new CellGroup();
            ItemsSource.Add(group);
            OnItemCreated(group);

            // sync with wrapper
            var groupWrapper = new GroupWrapper(group);
            _showList.Add(groupWrapper);

            return groupWrapper;
        }

        private IList<ICellGroupWrapper> GetWrappersContainer(ICellGroupWrapper wrapper)
        {
            return wrapper.Parent == null ? _showList : wrapper.Parent.Children;
        }

        private IList GetItemsContainer(ICellGroupWrapper wrapper)
        {
            if (wrapper.Parent == null) return ItemsSource;

            var group = (CellGroup)wrapper.Parent.Object;
            return new ListWrapper<Cell>(group.Cells);
        }

        private void MoveSelectedItem(int delta)
        {
            var wrapper = ItemsSelector.SelectedItem as ICellGroupWrapper;
            if (wrapper == null) return;

            var wrapperContainer = GetWrappersContainer(wrapper);
            var index = wrapperContainer.IndexOf(wrapper);
            var newIndex = index + delta;
            if (newIndex < 0 || newIndex >= wrapperContainer.Count)
                return;

            // move the item
            var itemContainer = GetItemsContainer(wrapper);
            itemContainer.RemoveAt(index);
            itemContainer.Insert(newIndex, wrapper.Object);

            // sync with wrapper
            wrapperContainer.RemoveAt(index);
            wrapperContainer.Insert(newIndex, wrapper);
        }

        /// <summary>
        /// The event arguments which contains the created item.
        /// </summary>
        public class ItemCreatedEventArgs
        {
            /// <summary>
            /// The constructor.
            /// </summary>
            /// <param name="item">The created item.</param>
            public ItemCreatedEventArgs(object item)
            {
                Item = item;
            }

            /// <summary>
            /// Gets the created item.
            /// </summary>
            public object Item
            {
                get;
                private set;
            }
        }
    }

    internal class ListWrapper<T> : IList
    {
        private readonly IList<T> _list;
        private readonly object _locker = new object();

        public ListWrapper(IList<T> list)
        {
            _list = list;
        }

        public IEnumerator GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            for (int i = 0; i < Count; i++)
            {
                array.SetValue(_list[i], index + i);
            }
        }

        public int Count { get { return _list.Count; } }
        public object SyncRoot { get { return _locker; }}
        public bool IsSynchronized { get { return false; } }
        public int Add(object value)
        {
            _list.Add((T)value);
            return _list.Count - 1;
        }

        public bool Contains(object value)
        {
            return _list.Contains((T) value);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public int IndexOf(object value)
        {
            return _list.IndexOf((T) value);
        }

        public void Insert(int index, object value)
        {
            _list.Insert(index, (T) value);
        }

        public void Remove(object value)
        {
            _list.Remove((T) value);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        public object this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = (T) value; }
        }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }
        public bool IsFixedSize { get { return !_list.IsReadOnly; } }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal interface ICellGroupWrapper : INotifyPropertyChanged
    {
        ICellGroupWrapper Parent { get; }
        object Object { get; }
        bool IsSelected { get; set; }
        bool IsDropTarget { get; set; }
        string DisplayText { get; }
        ObservableCollection<ICellGroupWrapper> Children { get; }
        void OnPropertyChanged(string propertyName);
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal abstract class CellGroupWrapper : ICellGroupWrapper
    {
        private readonly ICellGroupWrapper _parent;
        private readonly object _obj;
        private bool _isSelected;
        private bool _isDropTarget;
        private readonly ObservableCollection<ICellGroupWrapper> _items = new ObservableCollection<ICellGroupWrapper>();

        protected CellGroupWrapper(ICellGroupWrapper parent, object obj)
        {
            _parent = parent;
            _obj = obj;
        }

        public ICellGroupWrapper Parent
        {
            get { return _parent; }
        }

        public object Object
        {
            get { return _obj; }
        }

        public ObservableCollection<ICellGroupWrapper> Children
        {
            get { return _items; }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value) return;

                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public bool IsDropTarget
        {
            get { return _isDropTarget; }
            set
            {
                if (_isDropTarget == value) return;

                _isDropTarget = value;
                OnPropertyChanged("IsDropTarget");
            }
        }

        public string DisplayText
        {
            get
            {
                return _obj.ToString();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class CellWrapper : CellGroupWrapper
    {
        private readonly Cell _cell;

        public CellWrapper(ICellGroupWrapper parent, Cell cell)
            : base(parent, cell)
        {
            _cell = cell;
        }

        public Cell Cell
        {
            get { return _cell; }
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class GroupWrapper : CellGroupWrapper
    {
        private readonly CellGroup _group;

        public GroupWrapper(CellGroup group)
            : base(null, group)
        {
            _group = group;
        }

        public CellGroup Group
        {
            get { return _group; }
        }

        public GroupWrapper Clone()
        {
            var cloneGroup = (CellGroup)_group.Clone();
            var groupWrapper = new GroupWrapper(cloneGroup);
            foreach (var cell in cloneGroup.Cells)
            {
                var cellWrapper = new CellWrapper(groupWrapper, cell);
                groupWrapper.Children.Add(cellWrapper);
            }
            return groupWrapper;
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class DropTargetBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isTarget = (bool)value;
            return isTarget ? Brushes.LightGray : Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}