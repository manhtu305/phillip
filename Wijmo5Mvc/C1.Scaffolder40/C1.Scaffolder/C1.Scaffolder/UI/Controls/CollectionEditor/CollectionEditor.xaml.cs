﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using C1.Scaffolder.Extensions;

namespace C1.Scaffolder.UI
{
    /// <summary>
    /// Define the CollectionEditor.
    /// </summary>
    public partial class CollectionEditor
    {
        public readonly ObservableCollection<ListBoxItem> _showList = new ObservableCollection<ListBoxItem>();

        /// <summary>
        /// Occurs after an item is created.
        /// </summary>
        public event EventHandler<ItemCreatedEventArgs> ItemCreated;

        /// <summary>
        /// Gets or sets the list to be edited by the control.
        /// </summary>
        public IList ItemsSource
        {
            get { return (IList) GetValue(ItemsSourceProperty); }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        /// <summary>
        /// Identifies the <see cref="ItemsSource"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register(
                "ItemsSource",
                typeof (IList),
                typeof (CollectionEditor),
                new PropertyMetadata(OnItemsSourcePropertyChanged)
                );

        private static void OnItemsSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var sender = d as CollectionEditor;
            if (sender != null)
            {
                sender.OnItemsSourceChanged();
            }
        }

        /// <summary>
        /// The constructor.
        /// </summary>
        public CollectionEditor()
        {
            InitializeComponent();
        }

        private void ItemsEditor_ValueChanged(object sender, PropertyValueChangedEventArgs e)
        {
            var index = ItemsSelector.SelectedIndex;
            _showList[index].Content = ItemsSource[index].ToString();
        }

        private void ItemsSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ItemsEditor.SelectedObject = ItemsSelector.SelectedIndex != -1
                ? ItemsSource[ItemsSelector.SelectedIndex]
                : null;
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var removedIndex = ItemsSelector.SelectedIndex;
            if (removedIndex == -1)
            {
                return;
            }

            _showList.RemoveAt(removedIndex);
            ItemsSource.RemoveAt(removedIndex);
            if (ItemsSource.Count <= 0)
            {
                return;
            }

            if (ItemsSource.Count <= removedIndex)
            {
                removedIndex = removedIndex - 1;
            }

            ItemsSelector.SelectedIndex = removedIndex;
        }

        private void UpButton_Click(object sender, RoutedEventArgs e)
        {
            int index = ItemsSelector.SelectedIndex;
            MoveItem(--index);
        }

        private void DownButton_Click(object sender, RoutedEventArgs e)
        {
            int index = ItemsSelector.SelectedIndex;
            MoveItem(++index);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            var item = CreateDataItem();
            if (item == null)
            {
                return;
            }

            ItemsSource.Add(item);
            _showList.Add(new ListBoxItem {Content = item.GetType().Name});
            if (item is ContentControl)
            {
                (item as ContentControl).Content = item.GetType().Name;
            }

            ItemsSelector.SelectedIndex = ItemsSource.Count - 1;
        }

        private void OnItemsSourceChanged()
        {
            _showList.Clear();
            if (ItemsSource != null)
            {
                foreach (var item in ItemsSource)
                {
                    _showList.Add(new ListBoxItem {Content = item.ToString()});
                }
            }

            ItemsSelector.ItemsSource = _showList;
            if (_showList.Count > 0)
            {
                ItemsSelector.SelectedIndex = 0;
            }
        }

        private object CreateDataItem()
        {
            object item = null;
            if (ItemsSource != null)
            {
                var dataItemType = ItemsSource.GetType().GetItemType();
                if (dataItemType != null)
                {
                    item = Activator.CreateInstance(dataItemType);
                }
            }

            OnItemCreated(item);
            return item;
        }

        protected virtual void OnItemCreated(object item)
        {
            if (ItemCreated != null)
            {
                ItemCreated(this, new ItemCreatedEventArgs(item));
            }
        }

        private void MoveItem(int newIndex)
        {
            if (newIndex < 0 || newIndex >= ItemsSource.Count)
            {
                return;
            }

            var selectedIndex = ItemsSelector.SelectedIndex;
            var selectedItem = ItemsSource[selectedIndex];
            ItemsSource.RemoveAt(selectedIndex);
            ItemsSource.Insert(newIndex, selectedItem);
            _showList.Move(selectedIndex, newIndex);
            ItemsSelector.SelectedIndex = newIndex;
        }

        /// <summary>
        /// The event arguments which contains the created item.
        /// </summary>
        public class ItemCreatedEventArgs
        {
            /// <summary>
            /// The constructor.
            /// </summary>
            /// <param name="item">The created item.</param>
            public ItemCreatedEventArgs(object item)
            {
                Item = item;
            }

            /// <summary>
            /// Gets the created item.
            /// </summary>
            public object Item
            {
                get;
                private set;
            }
        }
    }
}