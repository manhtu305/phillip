﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace C1.Scaffolder.UI
{
    /// <summary>
    /// Interaction logic for ControllerSelect.xaml
    /// </summary>
    public partial class ControllerOptions : UserControl
    {
        public ControllerOptions()
        {
            InitializeComponent();
        }

        private void controller_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbbActions.ItemsSource = ((C1.Scaffolder.VS.ControllerDescriptor)cbbControllers.SelectedItem).Actions;
        }
    }
}
