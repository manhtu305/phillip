﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;

namespace C1.Scaffolder.UI
{
    /// <summary>
    /// Interaction logic for PropertyGrid.xaml
    /// </summary>
    public partial class PropertyGrid
    {
        /// <summary>
        /// Occurs when property value is changed.
        /// </summary>
        public event PropertyValueChangedEventHandler ValueChanged;
        //CustomPropertyGrid InnerPropertyGrid;
        /// <summary>
        /// The constructor.
        /// </summary>
        public PropertyGrid()
        {
            InitializeComponent();
        }

        private void InnerPropertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }

        /// <summary>
        /// Gets or sets the selected object.
        /// </summary>
        public object SelectedObject
        {
            get
            {
                return InnerPropertyGrid.SelectedObject;
            }
            set
            {
                InnerPropertyGrid.SelectedObject = value;
            }
        }
    }
}
