﻿using System.Windows;
using C1.Scaffolder.Models;
using C1.Scaffolder.Models.MultiRow;
using C1.Web.Mvc.MultiRow;

namespace C1.Scaffolder.UI.MultiRow
{
    /// <summary>
    /// Interaction logic for Cells.xaml
    /// </summary>
    public partial class Cells
    {
        public Cells()
        {
            InitializeComponent();
        }

        private void AutoGenerateColumnsCkb_OnChecked(object sender, RoutedEventArgs e)
        {
            var expression = CellsEditor.GetBindingExpression(CellGroupsEditor.ItemsSourceProperty);
            if (expression != null)
            {
                expression.UpdateTarget();
            }
        }
    }
}
