﻿using System.Windows.Controls;

namespace C1.Scaffolder.UI.MultiRow
{
    /// <summary>
    /// Interaction logic for General.xaml
    /// </summary>
    public partial class General : UserControl
    {
        public General()
        {
            InitializeComponent();
        }

        private void ComboBox_IsEnabledChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if (!((ComboBox)sender).IsEnabled)
            {
                ((ComboBox)sender).SelectedIndex = -1;
            }
        }
    }
}
