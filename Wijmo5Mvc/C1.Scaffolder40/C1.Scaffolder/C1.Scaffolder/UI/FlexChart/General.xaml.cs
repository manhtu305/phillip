﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using C1.Scaffolder.Models.Chart;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using C1.Web.Mvc;

namespace C1.Scaffolder.UI.FlexChart
{
    /// <summary>
    /// Interaction logic for Generic.xaml
    /// </summary>
    public partial class General : UserControl
    {
        public General()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshData(cboBindingX);
            RefreshData(cboBinding);
            RefreshData(cboBindingName);
            var sunburstModel = DataContext as SunburstOptionsModel;
            if(sunburstModel != null)
            {
                var sunburst = sunburstModel.Sunburst;

                // todo, reset/update the binding related properties if the selected model type is changed.
                // As a workaround, reset the BindingName property if it's invalid for current FieldNames.
                if (!ValidateBindingName(sunburst)) sunburst.DesignBindingName = null;

                var checkedFieldNames = new CheckedFieldNamesCollection(sunburst.FieldNames, sunburst.BindingName);
                lvBindingNameSunburst.DataContext = checkedFieldNames;
                checkedFieldNames.CollectionChanged += (s, cce) => { sunburst.DesignBindingName = checkedFieldNames.CheckedNames; };
            }
            RefreshData(cboChildItemsPath);
        }

        private bool ValidateBindingName(Sunburst<object> model)
        {
            if (string.IsNullOrEmpty(model.BindingName)) return true;

            return model.BindingName.Split(CheckedFieldNamesCollection.BindingNameSeparator)
                    .All(field => model.FieldNames.Contains(field));
        }

        private void RefreshData(ComboBox cbo)
        {
            var expression = cbo.GetBindingExpression(ItemsControl.ItemsSourceProperty);
            if (expression != null)
            {
                expression.UpdateTarget();
            }
        }

        private void UpButton_Click(object sender, RoutedEventArgs e)
        {
            var oldSelectedIndex = lvBindingNameSunburst.SelectedIndex;
            if (oldSelectedIndex == 0)
            {
                return;
            }

            var itemsSource = lvBindingNameSunburst.ItemsSource as ObservableCollection<CheckedFieldName>;
            if (itemsSource != null)
            {
                itemsSource.Move(oldSelectedIndex, oldSelectedIndex - 1);
                UpdateUpDownButtonsState();
            }
        }

        private void DownButton_Click(object sender, RoutedEventArgs e)
        {
            var oldSelectedIndex = lvBindingNameSunburst.SelectedIndex;
            if (oldSelectedIndex >= lvBindingNameSunburst.Items.Count - 1)
            {
                return;
            }

            var itemsSource = lvBindingNameSunburst.ItemsSource as ObservableCollection<CheckedFieldName>;
            if (itemsSource != null)
            {
                itemsSource.Move(oldSelectedIndex, oldSelectedIndex + 1);
                UpdateUpDownButtonsState();
            }
        }

        private void lvBindingNameSunburst_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateUpDownButtonsState();
        }

        private void UpdateUpDownButtonsState()
        {
            var selectedIndex = lvBindingNameSunburst.SelectedIndex;
            UpButton.IsEnabled = selectedIndex > 0;
            DownButton.IsEnabled = selectedIndex > -1 && selectedIndex < (lvBindingNameSunburst.Items.Count - 1);
        }

        private void lvBindingNameSunburst_LayoutUpdated(object sender, EventArgs e)
        {
            var view = lvBindingNameSunburst.View as GridView;
            ResizeGridViewColumn(view.Columns[1]);
        }

        private void ResizeGridViewColumn(GridViewColumn column)
        {
            if (double.IsNaN(column.Width))
            {
                column.Width = column.ActualWidth;
            }

            column.Width = double.NaN;
        }

        private void ComboBox_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!((ComboBox)sender).IsEnabled)
            {
                ((ComboBox)sender).SelectedIndex = -1;
            }
        }
    }

    internal class CheckedFieldNamesCollection : ObservableCollection<CheckedFieldName>
    {
        internal const char BindingNameSeparator = ',';

        public CheckedFieldNamesCollection(IEnumerable<string> fieldNames, string bindingName)
        {
            var bindingFields = bindingName == null ? new string[0] : bindingName.Split(BindingNameSeparator);
            // sort field names by the checked fields
            var orderedFieldNames = fieldNames.OrderBy(f =>
            {
                var index = Array.IndexOf(bindingFields, f);
                return index == -1 ? bindingFields.Length : index;
            });

            foreach (var field in orderedFieldNames)
            {
                var item = new CheckedFieldName(field, bindingFields.Contains(field));
                item.PropertyChanged += (s, e) => OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, item));
                Add(item);
            }
        }

        public string CheckedNames
        {
            get
            {
                var checkedItems = this.Where(item => item.IsChecked).Select(item=>item.FieldName).ToArray();
                return (checkedItems.Length == 0) ? null : string.Join(BindingNameSeparator.ToString(CultureInfo.InvariantCulture), checkedItems);
            }
        }
    }

    internal class CheckedFieldName : INotifyPropertyChanged
    {
        private bool _isChecked;

        public CheckedFieldName(string name, bool ischecked = false)
        {
            FieldName = name;
            _isChecked = ischecked;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string FieldName { get; private set; }

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                if (_isChecked == value) return;

                _isChecked = value;
                NotifyPropertyChanged("IsChecked");
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
