﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using C1.Scaffolder.Models.Chart;
using Model = C1.Web.Mvc;
using Res = C1.Scaffolder.Localization.Resources;

namespace C1.Scaffolder.UI.FlexChart
{
    /// <summary>
    /// Interaction logic for Series.xaml
    /// </summary>
    public partial class Series : UserControl
    {
        public Series()
        {
            InitializeComponent();
        }
        private void AddListItem(Model.ChartSeriesBase<object> series, Model.FlexChartCore<object> owner)
        {
            if (series.AxisX == null)
            {
                series.AxisX = Model.ChartAxis<object>.CreateAxis(owner, true);
            }
            if (series.AxisY == null)
            {
                series.AxisY = Model.ChartAxis<object>.CreateAxis(owner, false);
            }

            SeriesEditor.ItemsSource.Add(series);
            SeriesEditor._showList.Add(new ListBoxItem() { Content = series.ToString() });
            SeriesEditor.ItemsSelector.SelectedIndex = SeriesEditor.ItemsSource.Count - 1;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SeriesEditor.AddButton.Visibility = Visibility.Collapsed;
            var btn = new Button
            {
                Content = SeriesEditor.AddButton.Content,
                MinHeight = 24,
                HorizontalAlignment = HorizontalAlignment.Right
            };
            SeriesEditor.Toolbar.Children.Add(btn);
            var contextMenu = new ContextMenu();
            var chartOptions = DataContext as ChartBaseOptionsModel;
            Debug.Assert(chartOptions != null);
            var chart = chartOptions.Chart;
            if (chart.ControlType == Model.ChartControlType.FlexChart)
            {
                var flexChart = ((FlexChartOptionsModel)chartOptions).FlexChart;
                var btnNormal = new MenuItem { Header = Res.NormalSeries };
                var btnTrendLine = new MenuItem { Header = Res.TrendLine };
                var btnMovingAverage = new MenuItem { Header = Res.MovingAverage };
                var btnYFunction = new MenuItem { Header = Res.YFunction };
                var btnParametricFunction = new MenuItem { Header = Res.ParametricFunction };
                var btnWaterfall = new MenuItem { Header = Res.Waterfall };
                var btnBoxWhisker = new MenuItem { Header = Res.BoxWhisker };
                var btnErrorBar = new MenuItem { Header = Res.ErrorBar };
                btnNormal.Click += (o, ev) => AddListItem(new Model.ChartSeries<object>(flexChart), flexChart);
                btnTrendLine.Click += (o, ev) => AddListItem(new Model.TrendLine<object>(flexChart), flexChart);
                btnMovingAverage.Click += (o, ev) => AddListItem(new Model.MovingAverage<object>(flexChart), flexChart);
                btnYFunction.Click += (o, ev) => AddListItem(new Model.YFunctionSeries<object>(flexChart), flexChart);
                btnParametricFunction.Click += (o, ev) => AddListItem(new Model.ParametricFunctionSeries<object>(flexChart), flexChart);
                btnWaterfall.Click += (o, ev) => AddListItem(new Model.Waterfall<object>(flexChart), flexChart);
                btnBoxWhisker.Click += (o, ev) => AddListItem(new Model.BoxWhisker<object>(flexChart), flexChart);
                btnErrorBar.Click += (o, ev) => AddListItem(new Model.ErrorBar<object>(flexChart), flexChart);
                contextMenu.Items.Add(btnNormal);
                contextMenu.Items.Add(btnTrendLine);
                contextMenu.Items.Add(btnMovingAverage);
                contextMenu.Items.Add(btnYFunction);
                contextMenu.Items.Add(btnParametricFunction);
                contextMenu.Items.Add(btnWaterfall);
                contextMenu.Items.Add(btnBoxWhisker);
                contextMenu.Items.Add(btnErrorBar);
            }
            else
            {
                var flexRadar = ((FlexRadarOptionsModel) chartOptions).FlexRadar;
                var btnRadar = new MenuItem { Header = Res.RadarSeries };
                btnRadar.Click += (o, ev) => AddListItem(new Model.FlexRadarSeries<object>(flexRadar), flexRadar);
                contextMenu.Items.Add(btnRadar);
            }

            btn.ContextMenu = contextMenu;
            btn.Click += (o, ev) =>
            {
                btn.ContextMenu.PlacementTarget = btn;
                btn.ContextMenu.Placement = PlacementMode.Bottom;
                ContextMenuService.SetPlacement(btn, PlacementMode.Bottom);
                btn.ContextMenu.IsOpen = true;
            };
        }
    }
}
