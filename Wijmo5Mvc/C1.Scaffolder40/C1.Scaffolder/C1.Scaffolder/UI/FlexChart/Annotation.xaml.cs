﻿using C1.Scaffolder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Res = C1.Scaffolder.Localization.Resources;
using Model = C1.Web.Mvc;
using System.Windows.Controls.Primitives;

namespace C1.Scaffolder.UI.FlexChart
{
    /// <summary>
    /// Interaction logic for Annotation.xaml
    /// </summary>
    public partial class Annotation : UserControl
    {
        public Annotation()
        {
            InitializeComponent();
            AnnotationsEditor.AddButton.Visibility = Visibility.Collapsed;
            var btn = new Button()
            {
                Content = AnnotationsEditor.AddButton.Content,
                MinHeight = 24,
                HorizontalAlignment = HorizontalAlignment.Right
            };
            AnnotationsEditor.Toolbar.Children.Add(btn);
            var contextMenu = new ContextMenu();
            var btnCircle = new MenuItem() { Header = Res.CircleText };
            var btnEllipse = new MenuItem() { Header = Res.EllipseText };
            var btnLine = new MenuItem() { Header = Res.LineText };
            var btnImage = new MenuItem() { Header = Res.ImageText };
            var btnRectangle = new MenuItem() { Header = Res.RectangleText };
            var btnSquare = new MenuItem() { Header = Res.SquareText };
            var btnPolygon = new MenuItem() { Header = Res.PolygonText };
            var btnText = new MenuItem() { Header = Res.Text };
            btnCircle.Click += (o, e) => AddListItem(new Model.Circle());
            btnEllipse.Click += (o, e) => AddListItem(new Model.Ellipse());
            btnImage.Click += (o, e) => AddListItem(new Model.Image());
            btnLine.Click += (o, e) => AddListItem(new Model.Line());
            btnPolygon.Click += (o, e) => AddListItem(new Model.Polygon());
            btnRectangle.Click += (o, e) => AddListItem(new Model.Rectangle());
            btnSquare.Click += (o, e) => AddListItem(new Model.Square());
            btnText.Click += (o, e) => AddListItem(new Model.Text());
            contextMenu.Items.Add(btnCircle);
            contextMenu.Items.Add(btnEllipse);
            contextMenu.Items.Add(btnImage);
            contextMenu.Items.Add(btnLine);
            contextMenu.Items.Add(btnPolygon);
            contextMenu.Items.Add(btnRectangle);
            contextMenu.Items.Add(btnSquare);
            contextMenu.Items.Add(btnText);
            btn.ContextMenu = contextMenu;
            btn.Click += (o, ev) =>
            {
                btn.ContextMenu.PlacementTarget = btn;
                btn.ContextMenu.Placement = PlacementMode.Bottom;
                ContextMenuService.SetPlacement(btn, PlacementMode.Bottom);
                btn.ContextMenu.IsOpen = true;
            };
        }
        private void AddListItem(Model.AnnotationBase annotation)
        {
            AnnotationsEditor.ItemsSource.Add(annotation);
            AnnotationsEditor._showList.Add(new ListBoxItem() { Content = annotation.ToString() });
            AnnotationsEditor.ItemsSelector.SelectedIndex = AnnotationsEditor.ItemsSource.Count - 1;
        }
    }
}
