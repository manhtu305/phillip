﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using C1.Web.Mvc;
using System.Globalization;

namespace C1.Scaffolder.UI
{
    public partial class CustomPropertyGrid : System.Windows.Forms.PropertyGrid
    {
        public CustomPropertyGrid()
        {
            InitializeComponent();
        }

        protected override PropertyTab CreatePropertyTab(Type tabType)
        {
            return new CustomTab();
        }
    }

    public class CustomTab : PropertyTab
	{
		public CustomTab()
		{            
		}

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object component, Attribute[] attributes)
        {
            if (context != null)
            {
                TypeConverter converter = context.PropertyDescriptor != null ? context.PropertyDescriptor.Converter : TypeDescriptor.GetConverter(component);
                if (converter != null && converter.GetPropertiesSupported())
                    return AdjustCustomProperties(component, converter.GetProperties(context, component, attributes));
            }
            return this.GetProperties(component, attributes);
        }
        // get the properties of the selected component
        public override PropertyDescriptorCollection GetProperties(object component, System.Attribute[] attributes)
        {
            PropertyDescriptorCollection properties;
            if (attributes != null)
                properties = TypeDescriptor.GetProperties(component, attributes);
            else
                properties = TypeDescriptor.GetProperties(component);

            return AdjustCustomProperties(component, properties);
        }

        private PropertyDescriptorCollection AdjustCustomProperties(object component, PropertyDescriptorCollection properties)
        {
            PropertyDescriptor[] arrProp = new PropertyDescriptor[properties.Count];
            properties.CopyTo(arrProp, 0);
            ISupportUpdater updater = component as ISupportUpdater;
            if (updater != null && updater.ParsedSetting != null)
            {
                Dictionary<string, MVCProperty> parsedProperties = updater.ParsedSetting.Properties;
                for (int i = 0; i < parsedProperties.Count; i++)
                {
                    string propName = parsedProperties.Keys.ElementAt(i);
                    if (parsedProperties[propName].NeedTextEditor)
                    {
                        //Find the properties in the array of the properties which name same with current property
                        PropertyDescriptor propDesc = properties.Find(propName, true);
                        if (propDesc != null)
                        {
                            int index = properties.IndexOf(propDesc);
                            //Build a new properties
                            arrProp[index] = new CustomPropertyDesc(propDesc, parsedProperties[propName].Value.ToString());

                        }
                    }
                }
            }

            return new PropertyDescriptorCollection(arrProp);
        }

		public override PropertyDescriptorCollection GetProperties(object component)
		{                     
			return this.GetProperties(component,null);
		}

		// PropertyTab Name
		public override string TabName
		{
			get
			{
				return "Properties";
			}
		}

		//Image of the property tab (return a blank 16x16 Bitmap)
		public override System.Drawing.Bitmap Bitmap
		{
			get
			{				
				return new Bitmap(16,16);;
			}
		}
	}

    internal class CustomPropertyDesc : PropertyDescriptor
    {
        PropertyDescriptor baseDesc;
        string value;
        public CustomPropertyDesc(PropertyDescriptor old, string oldStringValue, params Attribute[] attrs)
            :base(old.Name, attrs)
        {
            value = oldStringValue;
            baseDesc = old;
        }
        public override Type ComponentType
        {
            get
            {
                return baseDesc.ComponentType;
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public override Type PropertyType
        {
            get
            {
                return typeof(string);
            }
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        public override object GetValue(object component)
        {
            return value;
        }

        public override void ResetValue(object component)
        {
            //throw new NotImplementedException();
        }

        public override void SetValue(object component, object value)
        {
            ISupportUpdater updater = component as ISupportUpdater;
            if (updater != null && updater.ParsedSetting != null)
            {
                MVCProperty property = updater.ParsedSetting.Properties[Name];
                property.AdjustRenderedText(value);
            }

            //this.value = value.ToString();
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }
    }
}
