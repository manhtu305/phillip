﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Scaffolder.Extensions
{
    /// <summary>
    /// Defines extensions for <see cref="Type"/> class.
    /// </summary>
    internal static class TypeExtension
    {
        /// <summary>
        /// Gets the item type of an enumerable type.
        /// </summary>
        /// <param name="enumerableType">The enumerable type.</param>
        /// <returns>The item type if the type implements <see cref="IEnumerable{T}"/> interface; otherwise, null.</returns>
        public static Type GetItemType(this Type enumerableType)
        {
            if (!IsEnumerableType(enumerableType))
            {
                return null;
            }

            var type = FindGenericType(typeof(IEnumerable<>), enumerableType);
            return type != null ? type.GetGenericArguments().FirstOrDefault() : null;
        }

        private static bool IsEnumerableType(this Type enumerableType)
        {
            return (FindGenericType(typeof(IEnumerable<>), enumerableType) != null);
        }

        private static Type FindGenericType(Type definition, Type type)
        {
            while (type != null && type != typeof(object))
            {
                if (type.IsGenericType && (type.GetGenericTypeDefinition() == definition))
                {
                    return type;
                }

                if (definition.IsInterface)
                {
                    var genericType = type.GetInterfaces().Select(i => FindGenericType(definition, i)).FirstOrDefault();
                    if (genericType != null)
                    {
                        return genericType;
                    }
                }

                type = type.BaseType;
            }

            return null;
        }
    }
}
