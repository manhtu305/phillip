﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using C1.Scaffolder.Extensions;
using C1.Scaffolder.Scaffolders;
using C1.Scaffolder.Services;
using C1.Scaffolder.UI;
using C1.Scaffolder.VS;
using C1.Web.Mvc.Services;
using EnvDTE;
using Microsoft.AspNet.Scaffolding;

namespace C1.Scaffolder
{
    public class C1ScaffolderCodeGenerator : CodeGenerator
    {
        private IControlScaffolder _scaffolder;

        /// <summary>
        /// Constructor for the custom code generator
        /// </summary>
        /// <param name="context">Context of the current code generation operation based on how scaffolder was invoked(such as selected project/folder) </param>
        /// <param name="information">Code generation information that is defined in the factory class.</param>
        public C1ScaffolderCodeGenerator(
            CodeGenerationContext context,
            CodeGeneratorInformation information)
            : base(context, information)
        {
        }

        /// <summary>
        /// Any UI to be displayed after the scaffolder has been selected from the Add Scaffold dialog.
        /// Any validation on the input for values in the UI should be completed before returning from this method.
        /// </summary>
        /// <returns></returns>
        public override bool ShowUIAndValidate()
        {
            // Bring up the selection dialog and allow user to select a model type
            var controlSelectionWindow = new ControlSelectionWindow(false);
            var showDialog = controlSelectionWindow.ShowDialog();
            if (!(showDialog.HasValue && (bool)showDialog))
            {
                return false;
            }

            var controlName = controlSelectionWindow.SelectedControl;
            _scaffolder = ScaffolderFactory.CreateInstance(Context.ServiceProvider, Context, controlName);
            var designHelper = _scaffolder.ServiceProvider.GetService(typeof(IDesignHelper)) as DesignHelper;
            designHelper.IsDesignTime = true;
            var controlConfigurationWindow = new ControlConfigurationWindow(_scaffolder.OptionsModel);
            var dialogResult = controlConfigurationWindow.ShowDialog();
            designHelper.IsDesignTime = false;

            return dialogResult.HasValue && (bool)dialogResult;
        }

        /// <summary>
        /// List of search paths of this code generator where templates/static files are searched for.
        /// By default, this list contains only one folder which is a sub-directory named
        /// "Templates\Microsoft.AspNet.Scaffolding.CodeGeneratorInformation.Id
        /// inside the directory containing the CodeGenerator assembly.
        /// Custom code generators can override the default behavior.
        /// </summary>
        public override IEnumerable<string> TemplateFolders
        {
            get
            {
                string templatesFolder = Path.Combine(Path.GetDirectoryName(GetType().Assembly.Location), "Templates");
                return new[] {templatesFolder};
            }
        }

        /// <summary>
        /// This method is executed after the ShowUIAndValidate method, and this is where the actual code generation should occur.
        /// We are generating some new files from t4 template.
        /// </summary>
        public override void GenerateCode()
        {
            _scaffolder.BeforeGenerateCode();

            var isCs = Context.ActiveProject.IsCs();
            var project = Context.ActiveProject;

            var mvcProject = _scaffolder.ServiceProvider.GetService(typeof(IMvcProject)) as MvcProject;
            var areaName = mvcProject.SelectedAreaName;
            var areaPath = string.IsNullOrEmpty(areaName) ? "" : string.Format("Areas\\{0}\\", areaName);
            var controllerGenerators = _scaffolder.GetControllerGenerators();
            var controllerFileExtension = isCs ? "cs" : "vb";
            ProjectItem controllerItem = null;
            foreach (var generator in controllerGenerators)
            {
                var outputPath = areaPath + generator.OutputPath;
                AddFileFromTemplateInternal(project, outputPath, generator.TemplatePath, generator.Parameters, false);
                if (generator.IsMain)
                {
                    controllerItem = project.ProjectItems.GetProjectItem(outputPath + "." + controllerFileExtension);
                }
            }

            var viewGenerators = _scaffolder.GetViewGenerators();
            var viewFileExtension = isCs ? "cshtml" : "vbhtml";
            ProjectItem viewItem = null;
            foreach (var generator in viewGenerators)
            {
                var outputPath = areaPath + generator.OutputPath;
                AddFileFromTemplateInternal(project, outputPath, generator.TemplatePath, generator.Parameters, false);
                if (generator.IsMain)
                {
                    viewItem = project.ProjectItems.GetProjectItem(outputPath + "." + viewFileExtension);
                }
            }

            if (_scaffolder.NeedGenerateCode && controllerItem != null && viewItem != null)
            {
                InsertCode(controllerItem, viewItem);
            }
        }

        private void InsertCode(ProjectItem controllerItem, ProjectItem viewItem)
        {
            var project = _scaffolder.ServiceProvider.GetService(typeof (IMvcProject)) as MvcProject;
            var mvcController = MvcProjectItem.Get(controllerItem, project);
            var mvcView = MvcProjectItem.Get(viewItem, project);

            var controllerCodeType = MvcProject.GetCodeTypes(new[] {mvcController}).FirstOrDefault();
            var controllerDescriptor = mvcController.Source.FileCodeModel == null
                ? null
                : new ControllerDescriptor(controllerCodeType, project);
            IActionDescriptor actionDescriptor = null;
            if (controllerDescriptor != null)
            {
                var actionName = project.IsRazorPages ? "OnGet" : _scaffolder.OptionsModel.ViewName;
                actionDescriptor = controllerDescriptor.Actions.FirstOrDefault(a => a.Name == actionName);
            }
            var generator = new C1CodeGenerator(_scaffolder);
            generator.GenerateCode(mvcController, controllerDescriptor, actionDescriptor, mvcView.ViewDescriptor);
        }

        private bool AddFileFromTemplateInternal(EnvDTE.Project project, string outputPath, string templatePath, System.Collections.Generic.IDictionary<string, object> templateParameters, bool skipIfExists)
        {
            var service = (ICodeGeneratorActionsService)ServiceProvider.GetService(typeof(ICodeGeneratorActionsService));
            ThrowIfTemplateNotFound(templatePath, TemplateFolders);
            return service.AddFileFromTemplate(project, outputPath, templatePath, templateParameters, skipIfExists);
        }

        private void ThrowIfTemplateNotFound(string templatePath, IEnumerable<string> searchFolders)
        {
            if (string.IsNullOrEmpty(templatePath))
            {
                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Localization.Resources.CannotFindTemplates, new object[] { templatePath, string.Join("\r\n", searchFolders) }));
            }
        }
    }
}
