﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Dashboard
{
    internal class DashboardOptionsModel : ControlOptionsModel
    {
        private static int _counter = 1;

        public DashboardOptionsModel(IServiceProvider serviceProvider):this(serviceProvider, new DashboardLayout())
        {
        }

        public DashboardOptionsModel(IServiceProvider serviceProvider, DashboardLayout dashboard)
            : base(serviceProvider, dashboard)
        {
            ControllerName = GetDefaultControllerName("DashboardLayout");

            Dashboard = dashboard;
            dashboard.Id = "dashboard";
            if (IsInsertOnCodePage) dashboard.Id += _counter++;
            else
                IsBoundMode = true;
            dashboard.Height = "800px";
        }
        internal DashboardOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            :this (serviceProvider, new DashboardLayout(settings), settings)
        {

        }

        internal DashboardOptionsModel(IServiceProvider serviceProvider, DashboardLayout dashboard, MVCControl settings)
            :base (serviceProvider, dashboard)
        {
            ControllerName = GetDefaultControllerName("DashboardLayout");

            Dashboard = dashboard;
            if (settings != null)
            {
                if (!string.IsNullOrEmpty(settings.ModelType))
                {
                    //ItemsBoundControl.ModelType = ModelTypes.Where(c => (c.ShortTypeName.Equals(settings.ModelType) || c.TypeName.Equals(settings.ModelType))).First();
                    //ItemsBoundControl.DbContextType = DbContextTypes.FirstOrDefault(ct => (true));//select first dbcontext
                    IsBoundMode = true;
                }
                else
                    IsBoundMode = false;

                InitControlValues(settings);
            }
            else
            {
                if (!IsInsertOnCodePage) IsBoundMode = true;
            }

            //control.IsBoundChanged += OnIsBoundChanged;
        }

        public virtual bool SupportUnboundMode
        {
            get { return true; }
            private set { }
        }
        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.FlexGridOptionsTab_General,
                    Path.Combine("Dashboard", "General.xaml")),
                new OptionsCategory(Resources.DashboardOptionsTab_AttachLayout,
                    Path.Combine("Dashboard", "AttachLayouts.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_HtmlAttributes,
                    Path.Combine("Dashboard", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_ClientEvents,
                    Path.Combine("Dashboard", "ClientEvents.xaml"))
            };

            return categories.ToArray();
        }

        [IgnoreTemplateParameter]
        public DashboardLayout Dashboard { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.DashboardModelName; }
        }
        
    }
}
