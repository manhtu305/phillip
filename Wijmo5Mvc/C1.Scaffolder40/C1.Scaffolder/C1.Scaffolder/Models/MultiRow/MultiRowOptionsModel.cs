﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Scaffolder.Models.Grid;
using C1.Web.Mvc;
using C1.Web.Mvc.MultiRow;

namespace C1.Scaffolder.Models.MultiRow
{
    internal class MultiRowOptionsModel : FlexGridOptionsModel
    {
        private static int _counter = 1;

        public MultiRowOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new MultiRow<object>())
        {
        }

        public MultiRowOptionsModel(IServiceProvider serviceProvider, MultiRow<object> multiRow)
            : this(serviceProvider, multiRow, null)
        {
            //ControllerName = GetDefaultControllerName("MultiRow");

            //MultiRow = multiRow;
            multiRow.Id = "multirow";
            if (IsInsertOnCodePage) multiRow.Id += _counter++;
        }

        public MultiRowOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            :this (serviceProvider, new MultiRow<object>(), settings)
        {
        }

        public MultiRowOptionsModel(IServiceProvider serviceProvider, MultiRow<object> multiRow, MVCControl settings)
            :base (serviceProvider, multiRow, settings)
        {
            ControllerName = GetDefaultControllerName("MultiRow");
            MultiRow = multiRow;
            if(settings == null)
            {
                MultiRow.AllowSorting = false;
                MultiRow.IsReadOnly = true;
            }
            IsBoundMode = true;//Multirow doesn't support unbound mode
            multiRow.Height = "800px";
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.FlexGridOptionsTab_General,
                    Path.Combine("MultiRow", "General.xaml")),
                new OptionsCategory(Resources.MultiRowOptionsTab_Cells,
                    Path.Combine("MultiRow", "Cells.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Editing,
                    Path.Combine("FlexGrid", "Editing.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Grouping,
                    Path.Combine("FlexGrid", "Grouping.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Filtering,
                    Path.Combine("FlexGrid", "Filtering.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Sorting,
                    Path.Combine("FlexGrid", "Sorting.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Paging,
                    Path.Combine("FlexGrid", "Paging.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Scrolling,
                    Path.Combine("FlexGrid", "Scrolling.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_ClientEvents,
                    Path.Combine("FlexGrid", "ClientEvents.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_HtmlAttributes,
                    Path.Combine("FlexGrid", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.MultiRowOptionsTab_Misc,
                    Path.Combine("MultiRow", "Misc.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }

        [IgnoreTemplateParameter]
        public MultiRow<object> MultiRow { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.MultiRowModelName; }
        }

        public override bool SupportUnboundMode
        {
            get
            {
                return false;//Multirow control doesn't support unbound mode
            }
        }
    }
}
