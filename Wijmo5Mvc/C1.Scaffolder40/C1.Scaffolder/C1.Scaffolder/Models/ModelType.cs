﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using C1.Web.Mvc;
using C1.Web.Mvc.Localization;
using EnvDTE;
using Microsoft.AspNet.Scaffolding;
using Microsoft.AspNet.Scaffolding.Core.Metadata;
using Microsoft.AspNet.Scaffolding.EntityFramework;

namespace C1.Scaffolder.Models
{
    /// <summary>
    /// Wrapper for CodeType so we can use it in the UI.
    /// </summary>
    internal class ModelType : IModelTypeDescription
    {
        public const string NoneValueDisplayName = "(none)";

        /// <summary>
        /// Initializes a new instance of <see cref="ModelType"/> object.
        /// </summary>
        /// <param name="codeType">The <see cref="CodeType"/>.</param>
        /// <param name="serviceProvider">The service provider.</param>
        public ModelType(CodeType codeType, IServiceProvider serviceProvider)
        {
            if (codeType == null)
            {
                throw new ArgumentNullException("codeType");
            }

            ServiceProvider = serviceProvider;
            CodeType = codeType;
            TypeName = codeType.FullName;
            ShortTypeName = codeType.Name;
            DisplayName = (codeType.Namespace == null || string.IsNullOrWhiteSpace(codeType.Namespace.FullName))
                            ? codeType.Name
                            : string.Format(CultureInfo.InvariantCulture, "{0} ({1})", codeType.Name, codeType.Namespace.FullName);
        }

        /// <summary>
        /// Gets the service provider.
        /// </summary>
        [IgnoreTemplateParameter]
        public IServiceProvider ServiceProvider { get; private set; }

        /// <summary>
        /// Gets or sets the underlying <see cref="CodeType"/>.
        /// </summary>
        public CodeType CodeType { get; set; }

        /// <summary>
        /// Gets or sets the display name for the code type.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the full type name of the code type.
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the short type name of the code type.
        /// </summary>
        public string ShortTypeName { get; set; }

        private IList<PropertyDescription> _properties;

        /// <summary>
        /// Gets the properties in the code type.
        /// </summary>
        [IgnoreTemplateParameter]
        public IList<PropertyDescription> Properties
        {
            get { return _properties ?? (_properties = GetModelProperties()); }
        }

        private List<PropertyDescription> GetModelProperties()
        {
            return CodeType.Members.Cast<CodeElement>()
                .Where(e => e.Kind == vsCMElement.vsCMElementProperty)
                .Select(e => new PropertyDescription {Name = e.Name, TypeName = (((CodeProperty) e).Type).AsString})
                .ToList();
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return DisplayName;
        }

        /// <summary>
        /// Gets the entity metadata for the specified database context and model.
        /// </summary>
        /// <param name="context">The context for code generation.</param>
        /// <param name="contextTypeFullName">The full name of the database context.</param>
        /// <param name="entityTypeFullName">The full name of the model.</param>
        /// <returns>The metadata of the model type.</returns>
        /// <remarks>
        /// It is only used for mvc4 & 5. It does NOT work for mvc6.
        /// </remarks>
        public static ModelMetadata GetEfMetadata(CodeGenerationContext context, string contextTypeFullName, string entityTypeFullName)
        {
            var frameworkService = (IEntityFrameworkService)context.ServiceProvider.GetService(typeof(IEntityFrameworkService));
            try
            {
                return frameworkService.AddRequiredEntity(context, contextTypeFullName, entityTypeFullName);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException(Resources.InvalidModelConfiguration, ex);
            }
        }
    }
}
