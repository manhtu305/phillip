﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Grid
{
    internal class FlexGridOptionsModel : DataBoundOptionsModel
    {
        private static int _counter = 1;

        public FlexGridOptionsModel(IServiceProvider serviceProvider) : this(serviceProvider, new FlexGrid<object>())
        {
        }

        public FlexGridOptionsModel(IServiceProvider serviceProvider, FlexGridBase<object> grid)
            : base(serviceProvider, grid)
        {
            ControllerName = GetDefaultControllerName(UI.ControlList.FlexGrid);

            Grid = grid;
            grid.Id = string.Empty;

            if (IsInsertOnCodePage)
            {
                grid.Id = UI.ControlList.FlexGrid.ToLower() + _counter++;
            }
            else if (!IsUpdate)
            {
                grid.Id = UI.ControlList.FlexGrid.ToLower();
            }

            grid.Height = "800px";
            grid.IsReadOnly = true;            
        }
        internal FlexGridOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new FlexGrid<object>(), settings)
        {

        }

        internal FlexGridOptionsModel(IServiceProvider serviceProvider, FlexGridBase<object> grid, MVCControl settings)
            : base(serviceProvider, grid, settings)
        {
            ControllerName = GetDefaultControllerName("FlexGrid");

            Grid = grid;
            if (!IsInsertOnCodePage)
                IsBoundMode = true;

            //InitControlValues(settings) has been called in DataBoundOptionsModel constructor before.
            //if (settings != null)
            //{
            //    InitControlValues(settings);
            //}

            if (IsUpdate)
            {
                DataSourceType = GetDataSourceType();
            }
        }

        public virtual bool SupportUnboundMode
        {
            get { return true; }
            private set { }
        }
        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.FlexGridOptionsTab_General,
                    Path.Combine("FlexGrid", "General.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_DataBinding,
                    Path.Combine("FlexGrid", "DataBinding.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Columns,
                    Path.Combine("FlexGrid", "Columns.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Editing,
                    Path.Combine("FlexGrid", "Editing.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Grouping,
                    Path.Combine("FlexGrid", "Grouping.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Filtering,
                    Path.Combine("FlexGrid", "Filtering.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Sorting,
                    Path.Combine("FlexGrid", "Sorting.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Paging,
                    Path.Combine("FlexGrid", "Paging.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Scrolling,
                    Path.Combine("FlexGrid", "Scrolling.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_ClientEvents,
                    Path.Combine("FlexGrid", "ClientEvents.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_HtmlAttributes,
                    Path.Combine("FlexGrid", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Misc,
                    Path.Combine("FlexGrid", "Misc.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }

        [IgnoreTemplateParameter]
        public FlexGridBase<object> Grid { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.FlexGridModelName; }
        }

        public EDataSourceType DataSourceType { get; set; }

        private EDataSourceType GetDataSourceType()
        {
            var itemsSource = Grid.ItemsSource;
            if (itemsSource is CollectionViewService<object>)
            {
                return EDataSourceType.CollectionView;
            }

            if (itemsSource is ODataCollectionViewService<object>)
            {
                return EDataSourceType.ODataCollectionView;
            }

            if (itemsSource is ODataVirtualCollectionViewService<object>)
            {
                return EDataSourceType.ODataVirtualCollectionView;
            }

            return default(EDataSourceType);
        }

        internal void InitODataSourceMode(EDataSourceType sourceType)
        {
            if (sourceType == EDataSourceType.CollectionView)
            {
                Grid.ItemsSource = new CollectionViewService<object>();
                Grid.AutoGenerateColumns = true;
                Grid.DesignAutoGenerateColumns = true;
                return;
            }

            Grid.ModelType = null;
            Grid.DbContextType = null;
            Grid.AutoGenerateColumns = false;
            Grid.DesignAutoGenerateColumns = false;
            IsBoundMode = false;

            if (sourceType == EDataSourceType.ODataCollectionView)
            {
                Grid.ItemsSource = new ODataCollectionViewService<object>();
            }
            else
            {
                Grid.ItemsSource = new ODataVirtualCollectionViewService<object>();
            }
        }
    }

    public enum EDataSourceType
    {
        CollectionView = 1,
        ODataCollectionView = 2,
        ODataVirtualCollectionView = 3
    }
}
