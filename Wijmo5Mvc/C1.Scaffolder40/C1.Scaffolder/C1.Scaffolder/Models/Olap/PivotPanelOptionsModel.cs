﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using C1.Scaffolder.Localization;
using System.IO;
using System.Collections.ObjectModel;

namespace C1.Scaffolder.Models.Olap
{
    internal class PivotPanelOptionsModel : ControlOptionsModel
    {
        private static int _counter;
        public PivotPanelOptionsModel(IServiceProvider serviceProvider, PivotPanel control)
            : base(serviceProvider, control)
        {
            PivotPanel = control;
            PivotPanel.Id = string.Format("pivotPanel{0}", ++_counter);
            PivotPanel.Engine.Id = string.Format("pivotEngine{0}", _counter);
            ControllerName = GetDefaultControllerName("PivotPanel");
            InitFieldsItems();
        }

        internal PivotPanelOptionsModel(IServiceProvider serviceProvider, PivotPanel pivotPanel, MVCControl settings)
            : base(serviceProvider, pivotPanel)
        {
            PivotPanel = pivotPanel;
            PivotPanel.ParsedSetting = settings;
            ControllerName = GetDefaultControllerName("PivotPanel");
            InitFieldsItems();

            if (settings != null)
            {
                InitControlValues(settings);
            }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            return new OptionsCategory[]
            {
                new OptionsCategory(Resources.PivotEngineOptionsTab_General,
                    Path.Combine("PivotPanel", "General.xaml")),
                new OptionsCategory(Resources.PivotEngineOptionsTab_ClientEvents,
                    Path.Combine("PivotPanel", "ClientEvents.xaml")),
                //new OptionsCategory(Resources.PivotEngineOptionsTab_Fields,
                //    Path.Combine("PivotPanel", "Fields.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_DataBinding,
                    Path.Combine("PivotPanel", "DataBinding.xaml"))
            };
        }

        private void InitFieldsItems()
        {
            Fields = new ObservableCollection<PivotFieldBase>();
            PivotPanel.Engine.RowFields.Items = new ObservableCollection<PivotFieldBase>();
            PivotPanel.Engine.ColumnFields.Items = new ObservableCollection<PivotFieldBase>();
            PivotPanel.Engine.ValueFields.Items = new ObservableCollection<PivotFieldBase>();
            PivotPanel.Engine.FilterFields.Items = new ObservableCollection<PivotFieldBase>();

            //Importance: handle Engine's event to validate Add button
            PivotPanel.Engine.BindingInfoChanged += Engine_BindingInfoChanged;
        }

        private void Engine_BindingInfoChanged(object sender, EventArgs e)
        {
            IsValid = GetIsValid();
        }

        [IgnoreTemplateParameter]
        public PivotPanel PivotPanel { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get
            {
                return Resources.PivotPanelModelName;
            }
        }

        [IgnoreTemplateParameter]
        public IEnumerable<IModelTypeDescription> ModelTypes
        {
            get { return ServiceManager.DbContextProvider.ModelTypes; }
        }

        public IEnumerable<IModelTypeDescription> DbContextTypes
        {
            get { return ServiceManager.DbContextProvider.DbContextTypes; }
        }

        /// <summary>
        /// ModelType.Properties to IList<PivotFieldBase> mapping.
        /// </summary>
        [IgnoreTemplateParameter]
        public ObservableCollection<PivotFieldBase> Fields { get; set; }

        /// <summary>
        /// Only enable ReadActionUrl when the ModelType is selected and Project supports EF
        /// </summary>
        public bool ShouldEnableReadActionUrl
        {
            get
            {
                if (IsUpdate)
                {
                    return true;
                }

                return PivotPanel.Engine.ModelType != null && IsProjectSupportEF;
            }
        }

        /// <summary>
        /// "Add" button will be enabled when both ReadActionURL and DbContextType are filled or cleared.
        /// </summary>
        protected override bool GetIsValid()
        {
            if (IsUpdate)
            {
                return base.GetIsValid();
            }

            //defaut = true
            if (PivotPanel == null || PivotPanel.Engine == null)
            {
                return true;
            }

            return !(string.IsNullOrEmpty(PivotPanel.Engine.ReadActionUrl) ^ (PivotPanel.Engine.DbContextType == null));
        }
    }
}
