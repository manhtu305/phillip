﻿using C1.Scaffolder.Localization;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder.Models.Olap
{
    internal class SlicerOptionsModel : ControlOptionsModel
    {
        private static int _counter;
        public SlicerOptionsModel(IServiceProvider serviceProvider, Slicer slicer)
            : base(serviceProvider, slicer)
        {
            Slicer = slicer;
            Slicer.Id = string.Format("slicer{0}", ++_counter);
            ControllerName = GetDefaultControllerName("Slicer");
        }

        internal SlicerOptionsModel(IServiceProvider serviceProvider, Slicer slicer, MVCControl settings)
            : base(serviceProvider, slicer)
        {
            Slicer = slicer;
            Slicer.ParsedSetting = settings;
            ControllerName = GetDefaultControllerName("Slicer");

            if (settings != null)
            {
                InitControlValues(settings);
            }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            return new OptionsCategory[]
            {
                new OptionsCategory(Resources.PivotEngineOptionsTab_General,
                    Path.Combine("Slicer", "General.xaml")),
                //new OptionsCategory(Resources.PivotEngineOptionsTab_Fields,
                //    Path.Combine("PivotPanel", "Fields.xaml")),
                new OptionsCategory(Resources.FlexChartOptionsTab_Appearance,
                    Path.Combine("Slicer", "Appearance.xaml")),
            };
        }

        [IgnoreTemplateParameter]
        public Slicer Slicer { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get
            {
                return Resources.SlicerModelName;
            }
        }
    }
}
