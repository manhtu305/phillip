﻿using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using System;
using C1.Scaffolder.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace C1.Scaffolder.Models.Olap
{
    internal class PivotChartOptionsModel : ControlOptionsModel
    {
        private static int _counter;
        public PivotChartOptionsModel(IServiceProvider serviceProvider, PivotChart chart)
            : base(serviceProvider, chart)
        {
            PivotChart = chart;
            PivotChart.Id = string.Format("pivotChart{0}", ++_counter);
            ControllerName = GetDefaultControllerName("PivotChart");
        }

        internal PivotChartOptionsModel(IServiceProvider serviceProvider, PivotChart chart, MVCControl settings)
            : base(serviceProvider, chart)
        {
            PivotChart = chart;
            PivotChart.ParsedSetting = settings;
            ControllerName = GetDefaultControllerName("PivotChart");

            if (settings != null)
            {
                InitControlValues(settings);
            }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            return new OptionsCategory[]
            {
                new OptionsCategory(Resources.PivotEngineOptionsTab_General,
                    Path.Combine("PivotChart", "General.xaml")),
                //new OptionsCategory(Resources.PivotEngineOptionsTab_Fields,
                //    Path.Combine("pivotGrid", "Fields.xaml")),
                new OptionsCategory(Resources.FlexChartOptionsTab_Appearance,
                    Path.Combine("PivotChart", "Appearance.xaml")),
            };
        }

        [IgnoreTemplateParameter]
        public PivotChart PivotChart { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get
            {
                return Resources.PivotChartModelName;
            }
        }
    }
}
