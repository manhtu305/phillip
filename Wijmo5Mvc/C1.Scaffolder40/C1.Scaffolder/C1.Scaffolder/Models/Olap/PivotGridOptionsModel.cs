﻿using C1.Scaffolder.Localization;
using C1.Scaffolder.Models.Grid;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder.Models.Olap
{
    internal class PivotGridOptionsModel : FlexGridOptionsModel
    {
        private static int _counter;
        public PivotGridOptionsModel(IServiceProvider serviceProvider, PivotGrid pivotGrid)
            : base(serviceProvider, pivotGrid)
        {
            PivotGrid = pivotGrid;
            PivotGrid.Id = string.Format("pivotGrid{0}", ++_counter);
            ControllerName = GetDefaultControllerName("PivotGrid");
        }

        internal PivotGridOptionsModel(IServiceProvider serviceProvider, PivotGrid pivotGrid, MVCControl settings)
            : base(serviceProvider, pivotGrid)
        {
            PivotGrid = pivotGrid;
            PivotGrid.ParsedSetting = settings;
            ControllerName = GetDefaultControllerName("PivotGrid");

            if (settings != null)
            {
                InitControlValues(settings);
            }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            return new OptionsCategory[]
            {
                new OptionsCategory(Resources.PivotEngineOptionsTab_General,
                    Path.Combine("PivotGrid", "General.xaml")),
                //new OptionsCategory(Resources.FlexGridOptionsTab_Grouping,
                //    Path.Combine("PivotGrid", "Grouping.xaml")),
                //new OptionsCategory(Resources.FlexGridOptionsTab_Filtering,
                //    Path.Combine("PivotGrid", "Filtering.xaml")),
                //new OptionsCategory(Resources.FlexGridOptionsTab_Sorting,
                //    Path.Combine("PivotGrid", "Sorting.xaml")),
                //new OptionsCategory(Resources.FlexGridOptionsTab_Paging,
                //    Path.Combine("PivotGrid", "Paging.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Scrolling,
                    Path.Combine("PivotGrid", "Scrolling.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_ClientEvents,
                    Path.Combine("PivotGrid", "ClientEvents.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_HtmlAttributes,
                    Path.Combine("PivotGrid", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_Misc,
                    Path.Combine("PivotGrid", "Misc.xaml"))
            };
        }

        [IgnoreTemplateParameter]
        public PivotGrid PivotGrid { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get
            {
                return Resources.PivotGridModelName;
            }
        }
    }
}
