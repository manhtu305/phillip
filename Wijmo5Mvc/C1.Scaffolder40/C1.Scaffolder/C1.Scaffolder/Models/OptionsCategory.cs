﻿namespace C1.Scaffolder.Models
{
    internal class OptionsCategory
    {
        public string DisplayName { get; private set; }
        public string ConfigurationPage { get; private set; }

        public OptionsCategory(string displayName, string configurationPage)
        {
            DisplayName = displayName;
            ConfigurationPage = configurationPage;
            IsEnabled = true;
        }

        public bool IsEnabled { get; set; }
    }
}
