﻿using System;
using System.Collections.Generic;
using C1.Web.Mvc;
using C1.Scaffolder.Localization;

namespace C1.Scaffolder.Models
{
    class TabPanelOptionsModel : ControlOptionsModel
    {        
        public TabPanelOptionsModel(IServiceProvider serviceProvider, TabPanel control)
            : base(serviceProvider, control)
        {
            TabPanel = control;

            // case insert
            if (control.ParsedSetting == null)
            {
                ControllerName = GetDefaultControllerName("TabPanel");
                
                if (!IsInsertOnCodePage)
                    IsBoundMode = true;
            }
            // case update
            else
            {
                InitControlValues(control.ParsedSetting);
            }

            if (!IsUpdate)
                TabPanel.Height = "800px";
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var allTabs = new OptionsCategory[]
            {
                new OptionsCategory(Resources.TabPanelOptionsTab_General, "TabPanel/General.xaml"),
                new OptionsCategory(Resources.TabPanelOptionsTab_HtmlAttributes, "TabPanel/HtmlAttributes.xaml"),
                new OptionsCategory(Resources.TabPanelOptionsTab_ClientEvents, "TabPanel/ClientEvents.xaml")
            };
            
            return allTabs;
        }

        [IgnoreTemplateParameter]
        public TabPanel TabPanel { get; internal set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.TabPanelModelName; }
        }

    }
}
