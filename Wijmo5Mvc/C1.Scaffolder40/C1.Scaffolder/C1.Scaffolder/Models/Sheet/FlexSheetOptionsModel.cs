﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;
using C1.Web.Mvc.Sheet;
using Microsoft.AspNet.Scaffolding;

namespace C1.Scaffolder.Models.Sheet
{
    internal class FlexSheetOptionsModel : DataBoundOptionsModel
    {
        private static int _counter = 1;

        public FlexSheetOptionsModel(IServiceProvider serviceProvider)
            :this(serviceProvider, new FlexSheet())
        {
        }

        public FlexSheetOptionsModel(IServiceProvider serviceProvider, FlexSheet sheet)
            : this(serviceProvider, sheet, null)
        {
            //ControllerName = GetDefaultControllerName("FlexSheet");

            //FlexSheet = sheet;
            sheet.Id = "flexsheet";
            if (IsInsertOnCodePage) sheet.Id += _counter++;
            sheet.Height = "500px";

            //To Enable the other tabs
            sheet.IsBound = true;
        }

        public FlexSheetOptionsModel(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new FlexSheet(), controlSettings)
        {
        }

         public FlexSheetOptionsModel(IServiceProvider serviceProvider, FlexSheet sheet, MVCControl controlSettings)
            : base(serviceProvider, sheet, controlSettings)
        {
            ControllerName = GetDefaultControllerName("FlexSheet");
            FlexSheet = sheet;
            if (FlexSheet != null)
            {
                for (int i = 0; i < FlexSheet.AppendedSheets.Count; i++)
                {
                    if (FlexSheet.AppendedSheets[i].IsBound)
                    {
                        BoundSheet<object> bs = FlexSheet.AppendedSheets[i] as BoundSheet<object>;
                        IsBoundMode = true;
                        break;
                    }
                }
            }
        }

        private bool HasBoundSheetInvalid()
        {
            if (FlexSheet != null)
            {
                for (int i = 0; i < FlexSheet.AppendedSheets.Count; i++)
                {
                    if (FlexSheet.AppendedSheets[i].IsBound)
                    {
                        BoundSheet<object> bs = FlexSheet.AppendedSheets[i] as BoundSheet<object>;
                        if (bs.DbContextType == null || bs.ModelType == null)
                            return true;
                    }
                }
            }
            return false;
        }
        protected override bool GetIsValid()
        {
            return !IsBoundMode || !IsProjectSupportEF || !HasBoundSheetInvalid();
        }
        internal void UpdateIsValid() {
            IsValid = GetIsValid();
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.FlexSheetOptionsTab_General,
                    Path.Combine("FlexSheet", "General.xaml")),
                new OptionsCategory(Resources.FlexSheetOptionsTab_AppendedSheets,
                    Path.Combine("FlexSheet", "AppendedSheets.xaml")),
                new OptionsCategory(Resources.FlexSheetOptionsTab_ClientEvents,
                    Path.Combine("FlexSheet", "ClientEvents.xaml")),
                new OptionsCategory(Resources.FlexSheetOptionsTab_HtmlAttributes,
                    Path.Combine("FlexSheet", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.FlexSheetOptionsTab_Misc,
                    Path.Combine("FlexSheet", "Misc.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }

        [IgnoreTemplateParameter]
        public FlexSheet FlexSheet { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.FlexSheetModelName; }
        }
    }
}
