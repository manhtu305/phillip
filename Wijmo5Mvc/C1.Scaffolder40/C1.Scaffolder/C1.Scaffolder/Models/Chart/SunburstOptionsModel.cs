﻿using System;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Chart
{
    internal class SunburstOptionsModel : FlexPieOptionsModel
    {
        private static int _counter = 1;

        public SunburstOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new Sunburst<object>())
        {
        }

        public SunburstOptionsModel(IServiceProvider serviceProvider, Sunburst<object> sunburst)
            : this(serviceProvider, sunburst, null)
        {
            //ControllerName = GetDefaultControllerName("Sunburst");

            //Sunburst = sunburst;
            sunburst.Id = "sunburst";
            if (IsInsertOnCodePage) sunburst.Id += _counter++;
            sunburst.Height = "300px";
        }

        public SunburstOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new Sunburst<object>(), settings)
        {
        }
        public SunburstOptionsModel(IServiceProvider serviceProvider, Sunburst<object> sunburst, MVCControl settings)
            : base(serviceProvider, sunburst, settings)
        {
            ControllerName = GetDefaultControllerName("Sunburst");
            Sunburst = sunburst;
        }

        [IgnoreTemplateParameter]
        public Sunburst<object> Sunburst { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.SunburstModelName; }
        }
    }
}
