﻿using System;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;
using System.Linq;
using System.IO;

namespace C1.Scaffolder.Models.Chart
{
    internal class FlexChartOptionsModel : ChartCoreOptionsModel
    {
        private static int _counter = 1;

        public FlexChartOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new FlexChart<object>())
        {
        }

        public FlexChartOptionsModel(IServiceProvider serviceProvider, C1.Web.Mvc.FlexChart<object> flexChart)
            : this(serviceProvider, flexChart, null)
        {
            //ControllerName = GetDefaultControllerName("FlexChart");

            //FlexChart = flexChart;
            flexChart.Id = "flexchart";
            if (IsInsertOnCodePage) flexChart.Id += _counter++;
            else IsBoundMode = true;
            flexChart.Height = "300px";

        }

        public FlexChartOptionsModel(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new FlexChart<object>(), controlSettings)
        {
        }

        public FlexChartOptionsModel(IServiceProvider serviceProvider, FlexChart<object> flexChart, MVCControl controlSettings)
            : base(serviceProvider, flexChart, controlSettings)
        {
            ControllerName = GetDefaultControllerName("FlexChart");
            FlexChart = flexChart;
            if (!IsInsertOnCodePage)
                IsBoundMode = true;
        }

        [IgnoreTemplateParameter]
        public FlexChart<object> FlexChart { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.FlexChartModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = base.CreateCategoryPages().ToList();
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Series,
                Path.Combine("FlexChart", "Series.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Axes,
                Path.Combine("FlexChart", "Axes.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_PlotArea,
                Path.Combine("FlexChart", "PlotArea.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_HeaderFooter,
                Path.Combine("FlexChart", "HeaderFooter.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Legend,
                Path.Combine("FlexChart", "Legend.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_ClientEvents,
                Path.Combine("FlexChart", "ClientEvents.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Selection,
                Path.Combine("FlexChart", "Selection.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Tooltip,
                Path.Combine("FlexChart", "Tooltip.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_LineMarker,
                Path.Combine("FlexChart", "LineMarker.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Annotation,
                Path.Combine("FlexChart", "Annotation.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_RangeSelector,
                Path.Combine("FlexChart", "RangeSelector.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Animation,
                Path.Combine("FlexChart", "Animation.xaml")));
            UpdateCategoryPagesEnabled(categories);

            return categories.ToArray();
        }
    }
}
