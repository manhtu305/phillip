﻿using System;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Chart
{
    internal abstract class ChartCoreOptionsModel : ChartBaseOptionsModel
    {
        protected ChartCoreOptionsModel(IServiceProvider serviceProvider, FlexChartCore<object> chart)
            : this(serviceProvider, chart, null)
        {
        }
        
        protected ChartCoreOptionsModel(IServiceProvider serviceProvider, FlexChartCore<object> chart, MVCControl controlSettings)
            : base(serviceProvider, chart, controlSettings)
        {
            ChartCore = chart;
        }

        [IgnoreTemplateParameter]
        public FlexChartCore<object> ChartCore { get; private set; }

        /// <summary>
        /// Gets a value indicating whether should add the generic parameter to the control builder.
        /// </summary>
        public override bool ShouldSerializeGenericParameter
        {
            get
            {
                if ((ChartCore.AxisX != null && ChartCore.AxisX.ShouldSerializeItemsSource())
                    || (ChartCore.AxisY != null && ChartCore.AxisY.ShouldSerializeItemsSource()))
                {
                    return false;
                }

                foreach (var series in ChartCore.Series)
                {
                    if (series.ShouldSerializeItemsSource()
                        || (series.AxisX != null && series.AxisX.ShouldSerializeItemsSource())
                        || (series.AxisY != null && series.AxisY.ShouldSerializeItemsSource()))
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
