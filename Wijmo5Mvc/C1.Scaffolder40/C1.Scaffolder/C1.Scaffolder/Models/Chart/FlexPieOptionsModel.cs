﻿using System;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;
using System.Linq;
using System.IO;

namespace C1.Scaffolder.Models.Chart
{
    internal class FlexPieOptionsModel : ChartBaseOptionsModel
    {
        private static int _counter = 1;

        public FlexPieOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new FlexPie<object>())
        {
        }

        public FlexPieOptionsModel(IServiceProvider serviceProvider, FlexPieBase<object> flexPie)
            : this(serviceProvider, flexPie, null)
        {
            //ControllerName = GetDefaultControllerName("FlexPie");

            //FlexPie = flexPie;
            flexPie.Id = "flexpie";
            if (IsInsertOnCodePage) flexPie.Id += _counter++;
            flexPie.Height = "300px";
        }
        public FlexPieOptionsModel(IServiceProvider serviceProvider, MVCControl settings) 
            : this(serviceProvider, new FlexPie<object>(), settings)
        {
        }

        public FlexPieOptionsModel(IServiceProvider serviceProvider, FlexPieBase<object> flexPie, MVCControl settings) 
            : base(serviceProvider, flexPie, settings)
        {
            ControllerName = GetDefaultControllerName("FlexPie");
            FlexPie = flexPie;
        }

        [IgnoreTemplateParameter]
        public FlexPieBase<object> FlexPie { get; private set; }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = base.CreateCategoryPages().ToList();
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_HeaderFooter,
                Path.Combine("FlexChart", "HeaderFooter.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Legend,
                Path.Combine("FlexChart", "Legend.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_ClientEvents,
                Path.Combine("FlexChart", "ClientEvents.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Selection,
                Path.Combine("FlexChart", "Selection.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Tooltip,
                Path.Combine("FlexChart", "Tooltip.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Animation,
                Path.Combine("FlexChart", "Animation.xaml")));
            UpdateCategoryPagesEnabled(categories);

            return categories.ToArray();
        }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.FlexPieModelName; }
        }

        protected override bool GetIsValid()
        {
            return !IsBoundMode || !IsProjectSupportEF ||
                (FlexPie != null && FlexPie.IsBound
                && !string.IsNullOrEmpty(FlexPie.Binding) 
                && !string.IsNullOrEmpty(FlexPie.BindingName));
        }
    }
}
