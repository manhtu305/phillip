﻿using System;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;
using System.Linq;
using System.IO;

namespace C1.Scaffolder.Models.Chart
{
    internal class FlexRadarOptionsModel : ChartCoreOptionsModel
    {
        private static int _counter = 1;

        public FlexRadarOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new FlexRadar<object>())
        {
        }

        public FlexRadarOptionsModel(IServiceProvider serviceProvider, FlexRadar<object> flexRadar)
            : this(serviceProvider, flexRadar, null)
        {
            //ControllerName = GetDefaultControllerName("FlexRadar");

            //FlexRadar = flexRadar;
            flexRadar.Id = "flexRadar";
            if (IsInsertOnCodePage) flexRadar.Id += _counter++;
            else IsBoundMode = true;
            flexRadar.Height = "300px";
        }

        public FlexRadarOptionsModel(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new FlexRadar<object>(), controlSettings)
        {
        }
        public FlexRadarOptionsModel(IServiceProvider serviceProvider, FlexRadar<object> flexRadar, MVCControl controlSettings)
            : base(serviceProvider, flexRadar, controlSettings)
        {
            ControllerName = GetDefaultControllerName("FlexRadar");

            FlexRadar = flexRadar;
            if (!IsInsertOnCodePage)
                IsBoundMode = true;
        }

        [IgnoreTemplateParameter]
        public FlexRadar<object> FlexRadar { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.FlexRadarModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = base.CreateCategoryPages().ToList();
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Series,
                Path.Combine("FlexChart", "Series.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Axes,
                Path.Combine("FlexChart", "Axes.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_HeaderFooter,
                Path.Combine("FlexChart", "HeaderFooter.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Legend,
                Path.Combine("FlexChart", "Legend.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_ClientEvents,
                Path.Combine("FlexChart", "ClientEvents.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Selection,
                Path.Combine("FlexChart", "Selection.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Tooltip,
                Path.Combine("FlexChart", "Tooltip.xaml")));
            categories.Add(new OptionsCategory(Resources.FlexChartOptionsTab_Animation,
                Path.Combine("FlexChart", "Animation.xaml")));
            UpdateCategoryPagesEnabled(categories);

            return categories.ToArray();
        }
    }
}
