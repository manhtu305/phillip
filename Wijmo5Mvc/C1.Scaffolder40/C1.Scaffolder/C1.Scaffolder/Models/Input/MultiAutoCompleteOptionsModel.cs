﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal class MultiAutoCompleteOptionsModel : AutoCompleteOptionsModel
    {
        private static int _counter = 1;

        public MultiAutoCompleteOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new MultiAutoComplete<object>())
        {
        }

        public MultiAutoCompleteOptionsModel(IServiceProvider serviceProvider, MultiAutoComplete<object> multiAutoComplete)
            : this(serviceProvider, multiAutoComplete, null)
        {
            //MultiAutoComplete = multiAutoComplete;

            multiAutoComplete.Id = "multiAutoComplete";
            if (IsInsertOnCodePage) multiAutoComplete.Id += _counter++;
        }

        public MultiAutoCompleteOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new MultiAutoComplete<object>(), settings)
        {
        }
        public MultiAutoCompleteOptionsModel(IServiceProvider serviceProvider, MultiAutoComplete<object> multiAutoComplete, MVCControl settings)
            : base(serviceProvider, multiAutoComplete, settings)
        {
             MultiAutoComplete = multiAutoComplete;
        }

        [IgnoreTemplateParameter]
        public MultiAutoComplete<object> MultiAutoComplete { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.MultiAutoCompleteModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "MultiAutoComplete", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "MultiAutoComplete", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ClientEvents,
                    Path.Combine("Controls", "ClientEvents.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "MultiAutoComplete", "Misc.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }
    }
}
