﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    class InputColorOptionsModel : DropDownOptionsModel
    {
        private static int _counter = 1;

        public InputColorOptionsModel(IServiceProvider serviceProvider)
            :this(serviceProvider, new InputColor())
        {
        }

        public InputColorOptionsModel(IServiceProvider serviceProvider, InputColor inputColor)
            : this(serviceProvider, inputColor, null)
        {
            //InputColor = inputColor;

            inputColor.Id = "inputColor";
            if (IsInsertOnCodePage) inputColor.Id += _counter++;
        }

        public InputColorOptionsModel(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new InputColor(), controlSettings)
        {
        }

        public InputColorOptionsModel(IServiceProvider serviceProvider, InputColor inputColor, MVCControl controlSettings)
            : base(serviceProvider, inputColor, controlSettings)
        {
            InputColor = inputColor;
        }

        [IgnoreTemplateParameter]
        public InputColor InputColor { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.InputColorModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "InputColor", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ClientEvents,
                    Path.Combine("Controls", "ClientEvents.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "InputColor", "Misc.xaml")),
            };

            return categories.ToArray();
        }
    }
}
