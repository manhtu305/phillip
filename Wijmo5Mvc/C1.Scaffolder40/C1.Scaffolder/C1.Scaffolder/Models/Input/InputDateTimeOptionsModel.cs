﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal class InputDateTimeOptionsModel : InputDateOptionsModel
    {
        private static int _counter = 1;

        public InputDateTimeOptionsModel(IServiceProvider serviceProvider)
            :this(serviceProvider, new InputDateTime())
        {
        }

        public InputDateTimeOptionsModel(IServiceProvider serviceProvider, InputDateBase inputDateTime)
            :this(serviceProvider, inputDateTime, null)
        {
            //InputDateTime = inputDateTime;

            inputDateTime.Id = "inputDateTime";
            if (IsInsertOnCodePage) inputDateTime.Id += _counter++;
        }

        public InputDateTimeOptionsModel(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new InputDateTime(), controlSettings)
        {
        }

         public InputDateTimeOptionsModel(IServiceProvider serviceProvider, InputDateBase inputDateTime, MVCControl controlSettings)
            : base(serviceProvider, inputDateTime, controlSettings)
        {
            InputDateTime = inputDateTime;
        }

        [IgnoreTemplateParameter]
        public InputDateBase InputDateTime { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.InputDateTimeModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "InputDateTime", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ClientEvents,
                    Path.Combine("Controls", "ClientEvents.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "InputDateTime", "Misc.xaml")),
            };

            return categories.ToArray();
        }
    }
}
