﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal abstract class DropDownBoundOptionsModel : DataBoundOptionsModel
    {
        public DropDownBoundOptionsModel(IServiceProvider serviceProvider, DropDown<object> dropdown)
            : base(serviceProvider, dropdown)
        {
            DropDown = dropdown;
        }

        protected DropDownBoundOptionsModel(IServiceProvider serviceProvider, DropDown<object> dropdown, MVCControl settings)
            : base (serviceProvider, dropdown, settings)
        {
            DropDown = dropdown;
        }

        [IgnoreTemplateParameter]
        public DropDown<object> DropDown { get; private set; }
    }
}
