﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal class InputTimeOptionsModel : ComboBoxOptionsModel
    {
        private static int _counter = 1;

        public InputTimeOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new InputTime<object>())
        {
        }

        public InputTimeOptionsModel(IServiceProvider serviceProvider, InputTime<object> inputTime)
            : this(serviceProvider, inputTime, null)
        {
            //InputTime = inputTime;

            inputTime.Id = "inputTime";
            if (IsInsertOnCodePage) inputTime.Id += _counter++;

            //To Enable the other tabs
            inputTime.IsBound = true;
        }

        public InputTimeOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new InputTime<object>(), settings)
        {
        }

        public InputTimeOptionsModel(IServiceProvider serviceProvider, InputTime<object> inputTime, MVCControl settings)
            : base(serviceProvider, inputTime, settings)
        {
            InputTime = inputTime;
        }

        [IgnoreTemplateParameter]
        public InputTime<object> InputTime { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.InputTimeModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "ComboBox", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "InputTime", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ClientEvents,
                    Path.Combine("Controls", "ClientEvents.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "InputTime", "Misc.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }
    }
}
