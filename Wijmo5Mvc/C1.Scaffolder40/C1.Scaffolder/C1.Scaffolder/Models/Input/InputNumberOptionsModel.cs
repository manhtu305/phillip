﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    class InputNumberOptionsModel : FormInputOptionsModel
    {
        private static int _counter = 1;

        public InputNumberOptionsModel(IServiceProvider serviceProvider)
            :this(serviceProvider, new InputNumber())
        {
        }

        public InputNumberOptionsModel(IServiceProvider serviceProvider, InputNumber inputNumber)
            :this(serviceProvider, inputNumber, null)
        {
            //InputNumber = inputNumber;

            inputNumber.Id = "inputNumber";
            if (IsInsertOnCodePage) inputNumber.Id += _counter++;
        }
        public InputNumberOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            :this (serviceProvider, new InputNumber(), settings)
        {
        }

        public InputNumberOptionsModel(IServiceProvider serviceProvider, InputNumber inputNumber, MVCControl settings)
            :base (serviceProvider, inputNumber, settings)
        {
            InputNumber = inputNumber;
        }

        [IgnoreTemplateParameter]
        public InputNumber InputNumber { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.InputNumberModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "InputNumber", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "InputNumber", "Misc.xaml")),
            };

            return categories.ToArray();
        }
    }
}
