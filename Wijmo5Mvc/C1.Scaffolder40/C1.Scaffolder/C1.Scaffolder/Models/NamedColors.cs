﻿using System.Collections.Generic;
using System.Drawing;
using System.Reflection;

namespace C1.Scaffolder.Models
{
    internal static class NamedColors
    {
        private static IList<Color> _colors;
        public static IEnumerable<Color> Colors
        {
            get
            {
                if (_colors == null)
                {
                    _colors = new List<Color>();
                    foreach (var p in typeof (Color).GetProperties(BindingFlags.Public|BindingFlags.Static))
                    {
                        if (p.PropertyType == typeof(Color))
                        {
                            var color = (Color)p.GetValue(null);
                            if (color.IsNamedColor)
                            {
                                _colors.Add(color);
                            }
                        }
                    }
                }

                return _colors;
            }
        }
    }
}
