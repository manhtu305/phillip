﻿using System.Collections.Generic;

namespace C1.Scaffolder.Models
{
    internal static class TimeValues
    {
        private readonly static IList<int> _hours;
        private readonly static IList<int> _minutes;
        private readonly static IList<int> _seconds;

        static TimeValues()
        {
            _hours = new List<int>();
            for (var i = 0; i < 24; i++)
            {
                _hours.Add(i);
            }

            _minutes = new List<int>();
            for (var i = 0; i < 60; i++)
            {
                _minutes.Add(i);
            }

            _seconds=new List<int>();
            for (var i = 0; i < 60; i++)
            {
                _seconds.Add(i);
            }
        }

        public static IEnumerable<int> Hours{get { return _hours; }}
        public static IEnumerable<int> Minutes { get { return _minutes; } }
        public static IEnumerable<int> Seconds { get { return _seconds; } }
    }
}
