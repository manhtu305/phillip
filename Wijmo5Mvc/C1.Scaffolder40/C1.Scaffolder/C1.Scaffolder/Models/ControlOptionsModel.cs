﻿using System;
using C1.Web.Mvc;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace C1.Scaffolder.Models
{
    internal abstract class ControlOptionsModel : ComponentOptionsModel
    {
        protected ControlOptionsModel(IServiceProvider serviceProvider, Component control)
            : base(serviceProvider, control)
        {
            Control = control;            
        }

        [IgnoreTemplateParameter]
        public Component Control { get; private set; }

        protected void InitControlValues(MVCControl ctrlSettings)
        {
            BaseControlBinder controlBinder = ControlBinderFactory.CreateBinder(ctrlSettings.Name);
            controlBinder.BindControlProperties(ctrlSettings, Control);            
        }
    }
}
