﻿using C1.Util.Licensing;

namespace C1.Scaffolder
{
#if GRAPECITY
    [C1AboutName("ComponentOne Controls for ASP.NET MVC Edition JPN")]
#else
    [C1AboutName("ComponentOne Controls for ASP.NET MVC Edition")]
#endif
    [C1ProductInfo("SU", "757CCC59-F365-4325-A676-0674C656B7A2")] // Studio Ultimate
    [C1ProductInfo("SE", "724e8a91-af12-4a3b-9aeb-ef89612e692e")] // Studio Enterprise
    [C1ProductInfo("S9", "08f7d405-7096-4b5f-a288-f749b8c83e6a")] // Studio for ASP.NET
    [C1ProductInfo("AH", "01667B7D-83D9-47D9-A4E0-E54FB46CA797")] // Wijmo5 Mvc
    internal class MvcLicenseDetector
    {
    }
}
