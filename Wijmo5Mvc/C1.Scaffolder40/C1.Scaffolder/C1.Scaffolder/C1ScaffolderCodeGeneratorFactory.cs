﻿using System;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using EnvDTE;
using Microsoft.AspNet.Scaffolding;
using C1.Scaffolder.Localization;
using Microsoft.VisualStudio.Shell;

namespace C1.Scaffolder
{
    public static class FactoryHelper
    {
        /// <summary>
        ///  Information about the code generator goes here.
        /// </summary>
        public static CodeGeneratorInformation Info = new CodeGeneratorInformation(
            displayName: Resources.InformationName,
            description: Resources.InformationDescription,
            author: Resources.InformationAuthor,
            version: new Version(AssemblyInfo.Version),
            id: "C1Scaffolder",
            icon: ToImageSource(Resources.TemplateIcon),
            gestures: new[] { "Controller", "View", "Area" },
            categories: CategoryNames);

        private static string[] _categoryNames = null;
        private static string[] CategoryNames
        {
            get
            {
                if (_categoryNames != null)
                    return _categoryNames;

                var dte = Package.GetGlobalService(typeof(EnvDTE.DTE)) as EnvDTE.DTE;
                if (new Version(dte.Version) >= new Version("15.0"))
                {
                    // Only new version of assembly Microsoft.AspNet.Scaffolding.12.0 contains RazorPage.
                    _categoryNames = new[] { Categories.Common, Categories.MvcController, Categories.MvcView, Categories.Other, Categories.Common + "/RazorPage" };
                }
                else
                {
                    _categoryNames = new[] { Categories.Common, Categories.MvcController, Categories.MvcView, Categories.Other };
                }

                return _categoryNames;
            }
        }

        public static bool IsSupported(CodeGenerationContext codeGenerationContext)
        {
            var language = codeGenerationContext.ActiveProject.CodeModel.Language;
            return language == CodeModelLanguageConstants.vsCMLanguageCSharp || language == CodeModelLanguageConstants.vsCMLanguageVB;
        }

        /// <summary>
        /// Helper method to convert Icon to Imagesource.
        /// </summary>
        /// <param name="icon">Icon</param>
        /// <returns>Imagesource</returns>
        public static ImageSource ToImageSource(Icon icon)
        {
            return Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }
    }

    [Export(typeof(CodeGeneratorFactory))]
    public class C1ScaffolderCodeGeneratorFactory : CodeGeneratorFactory
    {
        public C1ScaffolderCodeGeneratorFactory()
            : base(FactoryHelper.Info)
        {
        }

        /// <summary>
        /// This method creates the code generator instance.
        /// </summary>
        /// <param name="context">The context has details on current active project, project item selected, Nuget packages that are applicable and service provider.</param>
        /// <returns>Instance of CodeGenerator.</returns>
        public override ICodeGenerator CreateInstance(CodeGenerationContext context)
        {
            return new C1ScaffolderCodeGenerator(context, Information);
        }

        /// <summary>
        /// Provides a way to check if the custom scaffolder is valid under this context
        /// </summary>
        /// <param name="codeGenerationContext">The code generation context</param>
        /// <returns>True if valid, False otherwise</returns>
        public override bool IsSupported(CodeGenerationContext codeGenerationContext)
        {
            return FactoryHelper.IsSupported(codeGenerationContext);
        }
    }
}
