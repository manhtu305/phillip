﻿using System;
using C1.Scaffolder.Scaffolders;
using C1.Scaffolder.Services;
using C1.Scaffolder.Snippets;
using C1.Scaffolder.VS;
using C1.Web.Mvc;
using C1.Web.Mvc.Services;
using EnvDTE;
using C1.Web.Mvc.Serialization;
using C1.Scaffolder.Extensions;

namespace C1.Scaffolder
{
    internal class C1CodeGenerator
    {
        public C1CodeGenerator(IControlScaffolder scaffolder)
        {
            Scaffolder = scaffolder;
            ServiceProvider = scaffolder.ServiceProvider;
        }

        public IControlScaffolder Scaffolder { get; private set; }
        public IServiceProvider ServiceProvider { get; private set; }

        public virtual void GenerateCode(MvcProjectItem controllerItem, IControllerDescriptor controller, IActionDescriptor action, IViewDescriptor view)
        {
            var scaffolderDescriptor =
                ServiceProvider.GetService(typeof (IScaffolderDescriptor)) as ScaffolderDescriptor;
            scaffolderDescriptor.ShouldSerializeGenericParameter = Scaffolder.OptionsModel.ShouldSerializeGenericParameter;
            scaffolderDescriptor.ControllerItem = controllerItem;
            scaffolderDescriptor.Controller = controller;
            scaffolderDescriptor.Action = action;
            scaffolderDescriptor.View = view;
            scaffolderDescriptor.TemplateParameters = Scaffolder.TemplateParameters;

            var controllerSnippet = new ControllerSnippet(scaffolderDescriptor);
            ServiceManager.Instance.AddService(typeof(IControllerSnippets), controllerSnippet);
            var viewSnippet = new ViewSnippet();
            ServiceManager.Instance.AddService(typeof(IViewSnippets), viewSnippet);

            Scaffolder.BeforeSerializeComponent();
            var html = ViewSerializer.SerializeComponent(Scaffolder.OptionsModel.Component, ServiceManager.Instance, null);
            viewSnippet.AddComponent(html);
            Scaffolder.AfterSerializeComponent();
            BeforeGenerateCode();
            viewSnippet.Apply(ServiceProvider);//run before controller, because it might change project file.
            controllerSnippet.Apply(ServiceProvider);
            AfterGenerateCode();
            controllerItem.Source.SafeSave();
            view.ProjectItem.Source.SafeSave();
            view.ProjectItem.Source.SafeOpen();
        }


        internal virtual void BeforeGenerateCode()
        {
        }

        internal virtual void AfterGenerateCode()
        {
        }
    }
}
