﻿using C1.Scaffolder.Extensions;
using C1.Scaffolder.Scaffolders;
using C1.Scaffolder.Services;
using C1.Scaffolder.Snippets;
using C1.Scaffolder.UI;
using C1.Scaffolder.VS;
using C1.Web.Mvc;
using C1.Web.Mvc.Serialization;
using C1.Web.Mvc.Services;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder
{
    /// <summary>
    /// The controller to update a C1 Mvc control.
    /// </summary>
    public class C1ControlUpdater
    {
        private IControlScaffolder _scaffolder;
        MVCControl mvcControl;

        /// <summary>
        /// Instantiate a new C1ControlUpdater.
        /// </summary>
        /// <param name="serviceProvider"></param>
        public C1ControlUpdater(IServiceProvider serviceProvider = null)
        {
            if (serviceProvider == null)
            {
                DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;

                var serviceBag = new ServiceBag(serviceProvider, null);
                serviceBag.AddService(typeof(DTE), dte);
                ServiceManager.UpdateInstance(serviceBag);
                ServiceManager.Instance.AddService(typeof(DTE), dte);
                ServiceProvider = ServiceManager.Instance;
            }
            else
                ServiceProvider = serviceProvider;
            mvcControl = GetControl(ServiceProvider);
        }

        /// <summary>
        /// Get control at the current cursor position.
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public static MVCControl GetControl(IServiceProvider serviceProvider)
        {
            try
            {
                DTE dte = (DTE)serviceProvider.GetService(typeof(DTE));
                MVCControl control = null;
                if (dte != null && dte.ActiveDocument != null)
                {
                    var projectItem = dte.ActiveDocument.ProjectItem;
                    //checkNull safeguards againsts NullReferenceException when loading solution.
                    if (projectItem == null || projectItem.ContainingProject == null)
                    {
                        return GetSupportedControl(control);
                    }

                    Project project = projectItem.ContainingProject;
                    MvcProject mvcProject = new MvcProject(project, false);

                    ParserManager parserManager = new ParserManager();
                    control = parserManager.GetMVCControl(serviceProvider, mvcProject);
                }
                return GetSupportedControl(control);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the control instance.
        /// </summary>
        public MVCControl Control
        {
            get { return mvcControl; }
        }

        public IServiceProvider ServiceProvider { get; private set; }

        /// <summary>
        /// Show UI form to update.
        /// </summary>
        public void ShowForm()
        {
            if (!ShowFormInternal()) return;

            GenerateCode();

            C1InsertCodeGenerator.NotifyActivateLicense(_scaffolder);
        }

        private bool ShowFormInternal()
        {
            if (mvcControl != null)
            {
                _scaffolder = ScaffolderFactory.CreateInstance(ServiceProvider, null, mvcControl, true, true);

                var designHelper = _scaffolder.ServiceProvider.GetService(typeof(IDesignHelper)) as DesignHelper;
                designHelper.IsDesignTime = true;
                var controlConfigurationWindow = new ControlConfigurationWindow(_scaffolder.OptionsModel);
                var dialogResult = controlConfigurationWindow.ShowDialog();
                designHelper.IsDesignTime = false;
                return dialogResult.HasValue && (bool)dialogResult;
            }
            return false;
        }

        private void GenerateCode()
        {
            _scaffolder.BeforeGenerateCode();
            var project = _scaffolder.ServiceProvider.GetService(typeof(IMvcProject)) as MvcProject;

            var generator = new C1CodeUpdater(_scaffolder, mvcControl);
            generator.GenerateCode(_scaffolder.OptionsModel.SelectedAction.ProjectItem,
                _scaffolder.OptionsModel.SelectedController, _scaffolder.OptionsModel.SelectedAction,
                project.ActiveItem.ViewDescriptor);
        }

        /// <summary>
        /// Return null if control isn't support Insert/Update
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private static MVCControl GetSupportedControl(MVCControl control)
        {
            if (control == null) return null;
            switch (control.Name)
            {
                case "FlexGrid":
                case "MultiRow":
                case "FlexSheet":
                case "FlexChart":
                case "FlexPie":
                case "FlexRadar":
                case "Sunburst":
                case "TabPanel":
                case "DashboardLayout":

                case "InputMask":
                case "InputNumber":
                case "InputTime":
                case "InputDateTime":
                case "InputDate":
                case "AutoComplete":
                case "MultiAutoComplete":
                case "ComboBox":
                case "InputColor":
                case "MultiSelect":

                case "PivotEngine":
                case "PivotPanel":
                case "PivotGrid":
                case "PivotChart":
                case "Slicer":
                    return control;
            }
            return null;
        }
    }

    internal class C1CodeUpdater : C1CodeGenerator
    {
        private MVCControl controlSetting;

        public C1CodeUpdater(IControlScaffolder scaffolder, MVCControl controlSetting)
            : base(scaffolder)
        {
            this.controlSetting = controlSetting;
        }

        internal override void BeforeGenerateCode()
        {
            base.BeforeGenerateCode();
            //for updating, we remove current code before generate
            ViewEditor.RemoveContent(ServiceProvider, controlSetting.StartIndex, controlSetting.EndIndex);
        }

        public override void GenerateCode(MvcProjectItem controllerItem, IControllerDescriptor controller, IActionDescriptor action, IViewDescriptor view)
        {
            var scaffolderDescriptor =
                ServiceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            scaffolderDescriptor.ShouldSerializeGenericParameter = Scaffolder.OptionsModel.ShouldSerializeGenericParameter;
            scaffolderDescriptor.ControllerItem = controllerItem;
            scaffolderDescriptor.Controller = controller;
            scaffolderDescriptor.Action = action;
            scaffolderDescriptor.View = view;
            scaffolderDescriptor.TemplateParameters = Scaffolder.TemplateParameters;

            var controllerSnippet = new ControllerSnippet(scaffolderDescriptor);
            ServiceManager.Instance.AddService(typeof(IControllerSnippets), controllerSnippet);
            var viewSnippet = new ViewSnippet();
            ServiceManager.Instance.AddService(typeof(IViewSnippets), viewSnippet);

            Scaffolder.BeforeSerializeComponent();
            var html = ViewSerializer.UpdateComponent(Scaffolder.OptionsModel.Component, ServiceManager.Instance, null, controlSetting);
            viewSnippet.AddComponent(new CustomComponentHtmlSnippet(html, controlSetting.CodeType == BlockCodeType.HTMLNode));
            Scaffolder.AfterSerializeComponent();
            BeforeGenerateCode();
            controllerSnippet.Apply(ServiceProvider);
            viewSnippet.Apply(ServiceProvider);
            AfterGenerateCode();
            controllerItem.Source.SafeSave();
            view.ProjectItem.Source.SafeSave();
            view.ProjectItem.Source.SafeOpen();
        }
    }
}
