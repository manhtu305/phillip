﻿using System;
using C1.Scaffolder.Extensions;
using C1.Scaffolder.Scaffolders;
using C1.Web.Mvc.Services;
using EnvDTE;

namespace C1.Scaffolder.VS
{
    internal class ActionEditor
    {
        public static void AddContent(IServiceProvider serviceProvider, string content)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            AddContent(scaffolder.Action.CodeFunction, content);
        }

        public static void AddContent(CodeFunction codeFunction, string content)
        {
            var projectItem = codeFunction.ProjectItem;
            projectItem.SafeOpen();

            var editPoint = codeFunction.GetStartPoint(vsCMPart.vsCMPartBody).CreateEditPoint();
            editPoint.InsertAndFormat(projectItem, content);
        }
    }
}
