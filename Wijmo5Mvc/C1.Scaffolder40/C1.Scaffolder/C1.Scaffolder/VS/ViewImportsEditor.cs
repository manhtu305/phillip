﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using C1.Scaffolder.Extensions;
using C1.Scaffolder.Scaffolders;
using C1.Web.Mvc.Services;
using EnvDTE;

namespace C1.Scaffolder.VS
{
    /// <summary>
    /// The editor helper use for adjusting "_ViewImports.cshtml" file.
    /// </summary>
    internal class ViewImportsEditor
    {
        public static void AddImports(IServiceProvider serviceProvider, IEnumerable<string> namespaces)
        {
            var project = serviceProvider.GetService(typeof (IMvcProject)) as MvcProject;
            var prefix = project.IsCs ? "@using " : "@Imports ";
            var viewImportsItem = GetViewImportsItem(serviceProvider);
            var lines = namespaces.Select(n => prefix + n);
            FileEditor.EnsureLines(viewImportsItem, lines);
        }

        public static void AddImportTags(IServiceProvider serviceProvider, IEnumerable<string> tags)
        {
            var viewImportsItem = GetViewImportsItem(serviceProvider);
            var lines = tags.Select(t => "@addTagHelper *, " + t);
            FileEditor.EnsureLines(viewImportsItem, lines);
        }

        /// <summary>
        /// Add register the Styles(), Script() into _Layout.cshtml file
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="scripts"></param>
        public static void AddWebResourcesForMvc(IServiceProvider serviceProvider, IEnumerable<string> scripts)
        {
            var layoutItem = GetLayoutItem(serviceProvider);
            var fileLines = FileEditor.ReadLines(layoutItem.GetAllText()).ToList();

            int headTagIndex = -2;
            //insert styles:
            int index = InsertStyleResource(fileLines, "@Html.C1().Styles()", ref headTagIndex);

            //insert scripts
            List <string> scriptLines = new List<string>();
            
            string scriptPrefix = "@Html.C1().Scripts().";
            //- find current registed scripts:
            index = fileLines.FindIndex(s => s.TrimStart().StartsWith(scriptPrefix));
            if (index > -1)//adjust
            {
                scriptLines.AddRange(scripts.ToList());
                string currentScript = fileLines[index];
                int scriptsCount = scriptLines.Count;
                while (currentScript.TrimStart().StartsWith(scriptPrefix) && scriptsCount > 0)
                {
                    for (int i= scriptsCount - 1; i > -1; i--)
                    {
                        if (currentScript.Contains(scriptLines[i])) scriptLines.RemoveAt(i);
                    }
                    scriptsCount = scriptLines.Count;
                    index++;
                    currentScript = fileLines[index];
                }

                if (scriptsCount > 0)
                {
                    fileLines.InsertRange(index, scriptLines.Select(s => scriptPrefix + s));
                }
                    
            }
            else//insert
            {
                scriptLines.AddRange(scripts.Select(s => scriptPrefix + s ));

                if (headTagIndex == -2)
                {
                    headTagIndex = fileLines.FindLastIndex(s => s.TrimStart().StartsWith("</head>"));
                }
                index = headTagIndex;

                if (index != -1) fileLines.InsertRange(index, scriptLines);
            }

            //replace old content with new content
            layoutItem.ReplaceAllText(string.Join(Environment.NewLine, fileLines));
        }

        private static int InsertStyleResource(List<string> fileLines, string styleResourceBlock, ref int headTagIndex)
        {
            int index = fileLines.FindLastIndex(s => s.TrimStart().StartsWith(styleResourceBlock.Substring(0, styleResourceBlock.Length-3)));
            if (index == -1)//only insert when current file doesn't contain it
            {
                index = fileLines.FindIndex(s => s.TrimEnd().EndsWith("</title>"));
                if (index > -1) index++;//insert after the tag </title>
                else
                {
                    headTagIndex = fileLines.FindLastIndex(s => s.TrimStart().StartsWith("</head>"));
                    if (headTagIndex > 0)//at least have <head> tag
                    {
                        index = headTagIndex++;//insert before tag </head>
                    }
                }

                if (index != -1)
                    fileLines.Insert(index, styleResourceBlock);
            }

            return index;
        }

        /// <summary>
        /// Add the tags (styles and scripts) into _Layout.cshtml file (of core project)
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="scripts"></param>
        public static void AddWebResources(IServiceProvider serviceProvider, IEnumerable<string> scripts)
        {
            var layoutItem = GetLayoutItem(serviceProvider);
            var fileLines = FileEditor.ReadLines(layoutItem.GetAllText()).ToList();

            int headTagIndex = -2;
            //insert styles:
            int index = InsertStyleResource(fileLines, "<c1-styles />", ref headTagIndex);

            //insert scripts
            List<string> scriptLines = new List<string>();

            //- find current registed scripts:
            index = fileLines.FindIndex(s => s.TrimStart().StartsWith("</c1-scripts>"));
            if (index > -1)//adjust
            {
                scriptLines.AddRange(scripts);

                int i = index - 1;
                while (i > 0)
                {
                    scriptLines.Remove(fileLines[i].Trim());
                    if (fileLines[i--].TrimStart().StartsWith("<c1-scripts")) break;
                }
                if (scriptLines.Count > 0)
                    fileLines.InsertRange(index, scriptLines);
            }
            else//insert
            {
                scriptLines.Add("<c1-scripts>");
                scriptLines.AddRange(scripts);
                scriptLines.Add("</c1-scripts>");

                if (headTagIndex == -2)
                {
                    headTagIndex = fileLines.FindLastIndex(s => s.TrimStart().StartsWith("</head>"));
                }
                index = headTagIndex;

                if (index != -1) fileLines.InsertRange(index, scriptLines);
            }

            //replace old content with new content
            layoutItem.ReplaceAllText(string.Join(Environment.NewLine, fileLines));
        }

        private static ProjectItem GetViewImportsItem(IServiceProvider serviceProvider)
        {
            MvcProject project;
            string path = GetFilePath(serviceProvider, "_ViewImports", out project);
            return FileEditor.EnsureViewImportsFile(project, path);
        }

        private static ProjectItem GetLayoutItem(IServiceProvider serviceProvider)
        {
            MvcProject project;
            string path = GetFilePath(serviceProvider, "_Layout", out project);
            var filePath = Path.Combine(project.ProjectFolder, path);
            if (!File.Exists(filePath))
            {
                path = GetFilePath(serviceProvider, "_Layout", out project, true);
            }

            return FileEditor.EnsureViewImportsFile(project, path);
        }

        private static string GetFilePath(IServiceProvider serviceProvider, string fileName, out MvcProject project, bool inSharedFolder = false)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var view = scaffolder.View;
            project = view.ProjectItem.Project;
            fileName += project.IsCs ? ".cshtml" : ".vbhtml";
            var viewsFolder = project.IsRazorPages ? "Pages" : "Views";
            if (inSharedFolder) viewsFolder = Path.Combine(viewsFolder, "Shared");
            string path = string.IsNullOrEmpty(view.AreaName)
                ? Path.Combine(viewsFolder, fileName)
                : Path.Combine("Areas", view.AreaName, viewsFolder, fileName);
            return path;
        }

    }
}
