﻿using System;
using System.IO;

namespace C1.Scaffolder.VS
{
    internal class ViewDescriptor : IViewDescriptor
    {
        public ViewDescriptor(MvcProjectItem mvcProjectItem)
        {
            ProjectItem = mvcProjectItem;
            Name = Path.GetFileNameWithoutExtension(ProjectItem.FileName);
            if (mvcProjectItem.Project.IsRazorPages)
            {
                //var extension = mvcProjectItem.Project.IsCs ? ".cs" : ".vb";
                //ControllerName = Path.GetFileName(ProjectItem.FileName) + extension;
                ControllerName = Name;
                ActionName = "OnGet";
            }
            else
            {
                ControllerName = Path.GetFileName(Path.GetDirectoryName(ProjectItem.FullName));
                ActionName = Name;
            }
            if (ProjectItem.RelativeName.StartsWith("areas", StringComparison.OrdinalIgnoreCase))
            {
                AreaName = ProjectItem.RelativeName.Split('/', '\\')[1];
            }
        }

        public string Name
        {
            get;
            private set;
        }

        public MvcProjectItem ProjectItem { get; private set; }

        public string ControllerName
        {
            get;
            private set;
        }

        public string ActionName
        {
            get;
            private set;
        }

        public string AreaName
        {
            get;
            private set;
        }
    }
}
