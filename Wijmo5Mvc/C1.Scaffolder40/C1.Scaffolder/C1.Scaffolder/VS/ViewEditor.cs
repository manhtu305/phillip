﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using C1.Scaffolder.Extensions;
using C1.Scaffolder.Scaffolders;
using C1.Web.Mvc.Services;
using EnvDTE;

namespace C1.Scaffolder.VS
{
    internal class ViewEditor
    {
        public static void AddContent(IServiceProvider serviceProvider, string content)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var position = scaffolder.IsInsert ? EditorPosition.Current : EditorPosition.EndOfDocument;

            var projectItem = scaffolder.View.ProjectItem.Source;
            AddContent(projectItem, content, position);
        }

        private static void AddContent(ProjectItem projectItem, string content, EditorPosition position)
        {
            if (string.IsNullOrEmpty(content)) return;

            projectItem.SafeOpen();

            var textSelection = projectItem.Document.Selection as TextSelection;
            if (textSelection == null) return;

            switch (position)
            {
                case EditorPosition.StartOfDocument:
                    textSelection.StartOfDocument();
                    content += Environment.NewLine;
                    break;
                case EditorPosition.EndOfDocument:
                    textSelection.EndOfDocument();
                    content = Environment.NewLine + content;
                    break;
            }

            textSelection.ActivePoint.CreateEditPoint().InsertAndFormat(projectItem, content);
        }

        public static void RemoveContent(IServiceProvider serviceProvider, int startAbsolutePos, int endAbsolutePos)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var position = scaffolder.IsInsert ? EditorPosition.Current : EditorPosition.EndOfDocument;

            var projectItem = scaffolder.View.ProjectItem.Source;
            projectItem.SafeOpen();

            var textSelection = projectItem.Document.Selection as TextSelection;
            if (textSelection == null) return;
            textSelection.MoveToAbsoluteOffset(startAbsolutePos);
            //clear current content
            EditPoint editPoint = textSelection.ActivePoint.CreateEditPoint();
            editPoint.Delete(endAbsolutePos - startAbsolutePos + 1);
        }
        public static void AddImports(IServiceProvider serviceProvider, IEnumerable<string> imports)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var view = scaffolder.View;
            var projectItem = view.ProjectItem.Source;
            var mvcProject = view.ProjectItem.Project;

            if (mvcProject.IsAspNetCore)
            {
                ViewImportsEditor.AddImports(serviceProvider, imports);
            }
            else
            {
                var webConfigFilePath = string.IsNullOrEmpty(view.AreaName)
                    ? Path.Combine("Views", "Web.config")
                    : Path.Combine("Areas", view.AreaName, "Views", "Web.Config");
                var projectItemWebConfig = mvcProject.Source.ProjectItems.GetProjectItem(webConfigFilePath);
                if (projectItemWebConfig != null)
                {
                    WebConfigEditor.AddImports(projectItemWebConfig, imports);
                }
                else
                {
                    projectItem.SafeOpen();
                    var prefix = mvcProject.IsCs ? "@using " : "@Imports ";
                    var fileImports = SelectImports(view.ProjectItem.FullName,
                        l => l.StartsWith(prefix, StringComparison.OrdinalIgnoreCase),
                        l => l.Substring(prefix.Length).Trim());
                    AddImportsContent(view.ProjectItem.Source, fileImports, imports, s => prefix + s);
                }

            }
        }

        public static void AddImportTags(IServiceProvider serviceProvider, IEnumerable<string> tags)
        {
            var project = serviceProvider.GetService(typeof (IMvcProject)) as MvcProject;
            if (!project.IsAspNetCore) return;

            ViewImportsEditor.AddImportTags(serviceProvider, tags);

#if INSERT_IN_CURRENT_VIEW
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var view = scaffolder.View;
            var projectItem = view.ProjectItem.Source;
            var mvcProject = view.ProjectItem.Project;

            var fileImports = SelectImports(view.ProjectItem.FullName,
                l => l.StartsWith("@addTagHelper", StringComparison.OrdinalIgnoreCase),
                l =>
                {
                    var index = l.LastIndexOf(",", StringComparison.OrdinalIgnoreCase);
                    return index > 0 ? l.Substring(index + 1).Trim() : "";
                });
            projectItem.SafeOpen();
            AddImportsContent(projectItem, fileImports, tags, s => "@addTagHelper *, " + s);
#endif
        }

        public static void AddWebResources(IServiceProvider serviceProvider, IEnumerable<string> scripts)
        {
            var project = serviceProvider.GetService(typeof(IMvcProject)) as MvcProject;
            if (project.IsAspNetCore)
            {
                ViewImportsEditor.AddWebResources(serviceProvider, scripts);
            }
            else
            {
                ViewImportsEditor.AddWebResourcesForMvc(serviceProvider, scripts);
            }
        }

        private static void AddImportsContent(ProjectItem projectItem, IEnumerable<string> fileImports,
            IEnumerable<string> imports, Func<string, string> selector)
        {
            var importsList = imports.ToList();
            foreach (var import in fileImports)
            {
                importsList.Remove(import);
            }

            var content = string.Join(Environment.NewLine, importsList.Select(selector));
            AddContent(projectItem, content, EditorPosition.StartOfDocument);
        }

        private static IEnumerable<string> SelectImports(string filePath, Func<string, bool> predicate, Func<string, string> selector)
        {
            return File.ReadAllLines(filePath)
                .Select(line => line.Trim())
                .Where(predicate)
                .Select(selector);
        }
    }
}
