﻿using System;
using C1.Scaffolder.CodeBuilder;
using C1.Scaffolder.Scaffolders;
using C1.Scaffolder.VS;

namespace C1.Scaffolder.Snippets
{
    internal class ImportCodeSnippet : Snippet
    {
        public ImportCodeSnippet(string ns)
        {
            Namespace = ns;
        }

        public string Namespace { get; private set; }

        public override void Apply(IServiceProvider serviceProvider)
        {
            ControllerEditor.AddImport(serviceProvider, Namespace);
        }
    }

    internal class DbContextCodeSnippet : Snippet
    {
        public DbContextCodeSnippet(string name, string typeName)
        {
            Name = name;
            TypeName = typeName;
        }

        public string Name { get; private set; }
        public string TypeName { get; private set; }

        public override void Apply(IServiceProvider serviceProvider)
        {
            ControllerEditor.AddDbContext(serviceProvider, Name, TypeName);
        }
    }

    // for ASP.NET Core, flexsheet, to keep the IHostingEnviorment.WebRootPath
    internal class WebRootPathCodeSnippet : Snippet
    {
        public WebRootPathCodeSnippet(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }

        public override void Apply(IServiceProvider serviceProvider)
        {
            ControllerEditor.AddWebRootPath(serviceProvider, Name);
        }
    }

    internal class SimpleCodeSnippet : ContentSnippet
    {
        public SimpleCodeSnippet(string content)
        {
            Content = content;
        }

        public string Content { get; private set; }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            return Content;
        }

        public override void Apply(IServiceProvider serviceProvider)
        {
            ControllerEditor.AddContent(serviceProvider, Content);
        }
    }

    internal class CodeSnippetsCollection : ContentSnippetsCollection<ContentSnippet>
    {
        public override void Apply(IServiceProvider serviceProvider)
        {
            ControllerEditor.AddContent(serviceProvider, GetContent(serviceProvider));
        }
    }

    internal abstract class ActionCodeSnippet : ContentSnippet
    {
        public override void Apply(IServiceProvider serviceProvider)
        {
            ActionEditor.AddContent(serviceProvider, GetContent(serviceProvider));
        }
    }

    internal class DbProxyCreationDisabledCodeSnippet : ActionCodeSnippet
    {
        public DbProxyCreationDisabledCodeSnippet(string dbName)
        {
            DbName = dbName;
        }

        public string DbName { get; private set; }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            var generator = serviceProvider.GetService(typeof(ICodeBuilder)) as ICodeBuilder;
            var left = generator.GenerateMemberRef(DbName, "Configuration");
            left = generator.GenerateMemberRef(left, "ProxyCreationEnabled");
            return generator.GenerateAssignment(left, generator.KeywordFalse);
        }
    }

    internal class ViewBagCodeSnippet : ActionCodeSnippet
    {
        public ViewBagCodeSnippet(string name, string dbName, string entitySetName)
        {
            Name = name;
            DbName = dbName;
            EntitySetName = entitySetName;
        }

        public string Name { get; private set; }
        public string DbName { get; private set; }
        public string EntitySetName { get; private set; }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            var generator = serviceProvider.GetService(typeof(ICodeBuilder)) as ICodeBuilder;
            var scaffolder = serviceProvider.GetService(typeof(IControlScaffolder)) as IControlScaffolder;
            return scaffolder.OptionsModel.Project.IsRazorPages
                ? generator.GenerateViewDataAssignment(Name, DbName, EntitySetName)
                : generator.GenerateViewBagAssignment(Name, DbName, EntitySetName);
        }
    }

    internal class ActionCodeSnippetsCollection : ContentSnippetsCollection<ActionCodeSnippet>
    {
        public override void Apply(IServiceProvider serviceProvider)
        {
            ActionEditor.AddContent(serviceProvider, GetContent(serviceProvider));
        }
    }
}
