﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C1.Web.Mvc;
using Microsoft.AspNet.Scaffolding;

namespace C1.Scaffolder.Services
{
    internal class ServiceBag : ServiceContainer
    {
        public ServiceBag(IServiceProvider serviceProvider, CodeGenerationContext context)
            : base(serviceProvider)
        {
            Context = context;
            if (context != null)
            {
                AddService(typeof (CodeGenerationContext), context);
            }
        }

        public CodeGenerationContext Context { get; private set; }

        public override object GetService(Type serviceType)
        {
            var result =  base.GetService(serviceType);
            if (result == null && Context != null)
            {
                result = Context.ServiceProvider.GetService(serviceType);
            }
            return result;
        }
    }
}
