﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Scaffolder.Models;
using C1.Scaffolder.VS;
using C1.Web.Mvc;
using C1.Web.Mvc.Services;
using EnvDTE;
using Microsoft.AspNet.Scaffolding;
using Microsoft.AspNet.Scaffolding.EntityFramework;
using CodeTypeExtensions = Microsoft.AspNet.Scaffolding.CodeTypeExtensions;

namespace C1.Scaffolder.Services
{
    internal class DbContextProvider : IDbContextProvider
    {
        private IList<IModelTypeDescription> _contexts;
        private IList<IModelTypeDescription> _models;

        public DbContextProvider(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public IServiceProvider ServiceProvider { get; private set; }

        public IList<IModelTypeDescription> DbContextTypes
        {
            get
            {
                if (_contexts == null)
                {
                    InitModelTypes();
                }
                return _contexts;
            }
        }

        public IList<IModelTypeDescription> ModelTypes
        {
            get
            {
                if (_models == null)
                {
                    InitModelTypes();
                }
                return _models;
            }
        }

        public string GetEntitySetName(IModelTypeDescription dbContext, IModelTypeDescription modelType)
        {
            if (dbContext == null || modelType == null)
            {
                return string.Empty;
            }

            var project = ServiceProvider.GetService(typeof(IMvcProject)) as IMvcProject;
            var codeGeneration = ServiceProvider.GetService(typeof(CodeGenerationContext)) as CodeGenerationContext;

            if (project.IsAspNetCore || codeGeneration == null)
            {
                var dbContextProperties = dbContext.Properties;
                foreach (var property in dbContextProperties)
                {
                    var typeName = project.IsCs
                        ? string.Format("DbSet<{0}>", modelType.TypeName)
                        : string.Format("DbSet(Of {0})", modelType.TypeName);
                    if (property.TypeName.Contains(typeName))
                    {
                        return property.Name;
                    }
                }

                return string.Empty;
            }

            var metaData = ModelType.GetEfMetadata(codeGeneration, dbContext.TypeName, modelType.TypeName);
            return metaData.EntitySetName;
        }

        private void InitModelTypes()
        {
            var project = ServiceProvider.GetService(typeof(IMvcProject)) as MvcProject;
            var allCodeTypes = project.CodeTypes.OrderBy(c => c.Name).Distinct(new CodeTypeComparer()).ToArray();
            _contexts = allCodeTypes.Where(IsValidDbContextType).Select<CodeType, IModelTypeDescription>(c => new ModelType(c, ServiceProvider)).ToList();
            _models = allCodeTypes.Where(c => c.IsValidWebProjectEntityType()).Select<CodeType, IModelTypeDescription>(c => new ModelType(c, ServiceProvider)).ToList();
        }

        private static bool IsValidDbContextType(CodeType codeType)
        {
            return CodeTypeExtensions.IsDerivedType(codeType, "Microsoft.EntityFrameworkCore.DbContext") || codeType.IsValidDbContextType();
        }

        internal static bool IsProjectSupportEF(MvcProject project, IServiceProvider serviceProvider)
        {
            var allCodeTypes = project.CodeTypes.OrderBy(c => c.Name).Distinct(new CodeTypeComparer()).ToArray();
            var _contexts = allCodeTypes.Where(IsValidDbContextType).Select<CodeType, IModelTypeDescription>(c => new ModelType(c, serviceProvider)).ToList();
            return _contexts != null;
        }
    }
}
