﻿using System;
using C1.Scaffolder.Models.Input;
using System.Collections.Generic;
using C1.Web.Mvc;

namespace C1.Scaffolder.Scaffolders
{
    internal abstract class FormInputScaffolder : ControlScaffolderBase
    {
        private readonly FormInputOptionsModel _formInputOptionsModel;

        protected FormInputScaffolder(IServiceProvider serviceProvider, FormInputOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _formInputOptionsModel = optionsModel;
        }
    }

    internal abstract class DropDownScaffolder : FormInputScaffolder
    {
        private readonly DropDownOptionsModel _dropDownOptionsModel;

        protected DropDownScaffolder(IServiceProvider serviceProvider, DropDownOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _dropDownOptionsModel = optionsModel;
        }
    }

    internal class InputDateScaffolder : DropDownScaffolder
    {
        private readonly InputDateOptionsModel _inputDateOptionsModel;

        public InputDateScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new InputDateOptionsModel(serviceProvider))
        {
        }

        public InputDateScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings) 
            : this(serviceProvider, new InputDateOptionsModel(serviceProvider, controlSettings))
        {
        }

        protected InputDateScaffolder(IServiceProvider serviceProvider, InputDateOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _inputDateOptionsModel = optionsModel;
        }
    }

    internal class InputDateTimeScaffolder : InputDateScaffolder
    {
        private readonly InputDateTimeOptionsModel _inputDateTimeOptionsModel;

        public InputDateTimeScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new InputDateTimeOptionsModel(serviceProvider))
        {
        }

        public InputDateTimeScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new InputDateTimeOptionsModel(serviceProvider, controlSettings))
        {
        }

        private InputDateTimeScaffolder(IServiceProvider serviceProvider, InputDateTimeOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _inputDateTimeOptionsModel = optionsModel;
        }
    }

    internal class InputColorScaffolder : DropDownScaffolder
    {
        private readonly InputColorOptionsModel _inputColorOptionsModel;

        public InputColorScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new InputColorOptionsModel(serviceProvider))
        {
        }

        public InputColorScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new InputColorOptionsModel(serviceProvider, controlSettings))
        {
        }

        private InputColorScaffolder(IServiceProvider serviceProvider, InputColorOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _inputColorOptionsModel = optionsModel;
        }
    }

    internal class InputNumberScaffolder :FormInputScaffolder
    {
        private readonly InputNumberOptionsModel _inputNumberOptionsModel;

        public InputNumberScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new InputNumberOptionsModel(serviceProvider))
        {
        }
        public InputNumberScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this (serviceProvider, new InputNumberOptionsModel(serviceProvider, settings))
        {

        }
        private InputNumberScaffolder(IServiceProvider serviceProvider, InputNumberOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _inputNumberOptionsModel = optionsModel;
        }
    }

    internal class InputMaskScaffolder : FormInputScaffolder
    {
        private readonly InputMaskOptionsModel _inputMaskOptionsModel;

        public InputMaskScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new InputMaskOptionsModel(serviceProvider))
        {
        }
        public InputMaskScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this (serviceProvider, new InputMaskOptionsModel(serviceProvider, settings))
        {
        }

        private InputMaskScaffolder(IServiceProvider serviceProvider, InputMaskOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _inputMaskOptionsModel = optionsModel;
        }
    }

    internal class ComboBoxScaffolder : DataBoundScaffolder
    {
        private readonly ComboBoxOptionsModel _comboBoxOptionsModel;

        public ComboBoxScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new ComboBoxOptionsModel(serviceProvider))
        {
        }

        public ComboBoxScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings) 
            : this (serviceProvider, new ComboBoxOptionsModel(serviceProvider, controlSettings))
        {
        }

        protected ComboBoxScaffolder(IServiceProvider serviceProvider, ComboBoxOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _comboBoxOptionsModel = optionsModel;
        }
    }

    internal class InputTimeScaffolder : ComboBoxScaffolder
    {
        private readonly InputTimeOptionsModel _inputTimeOptionsModel;

        public InputTimeScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new InputTimeOptionsModel(serviceProvider))
        {
        }

        public InputTimeScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new InputTimeOptionsModel(serviceProvider, controlSettings))
        {
        }

        private InputTimeScaffolder(IServiceProvider serviceProvider, InputTimeOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _inputTimeOptionsModel = optionsModel;
        }
    }

    internal class MultiSelectScaffolder : ComboBoxScaffolder
    {
        private readonly MultiSelectOptionsModel _multiSelectOptionsModel;

        public MultiSelectScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new MultiSelectOptionsModel(serviceProvider))
        {
        }

        public MultiSelectScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings) 
            :  this(serviceProvider, new MultiSelectOptionsModel(serviceProvider, controlSettings))
        {
        }

        private MultiSelectScaffolder(IServiceProvider serviceProvider, MultiSelectOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _multiSelectOptionsModel = optionsModel;
        }
    }

    internal class AutoCompleteScaffolder : ComboBoxScaffolder
    {
        private readonly AutoCompleteOptionsModel _autoCompleteOptionsModel;

        public AutoCompleteScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new AutoCompleteOptionsModel(serviceProvider))
        {
        }

        public AutoCompleteScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings) 
            : this(serviceProvider, new AutoCompleteOptionsModel(serviceProvider, controlSettings))
        {
        }

        protected AutoCompleteScaffolder(IServiceProvider serviceProvider, AutoCompleteOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _autoCompleteOptionsModel = optionsModel;
        }
    }

    internal class MultiAutoCompleteScaffolder : AutoCompleteScaffolder
    {
        private readonly MultiAutoCompleteOptionsModel _multiAutoCompleteOptionsModel;

        public MultiAutoCompleteScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new MultiAutoCompleteOptionsModel(serviceProvider))
        {
        }

        private MultiAutoCompleteScaffolder(IServiceProvider serviceProvider, MultiAutoCompleteOptionsModel optionsModel)
            :base(serviceProvider, optionsModel)
        {
            _multiAutoCompleteOptionsModel = optionsModel;
        }

        public MultiAutoCompleteScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings) 
            : this(serviceProvider, new MultiAutoCompleteOptionsModel(serviceProvider, controlSettings))
        {
        }

    }
}
