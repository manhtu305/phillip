﻿using System.Collections.Generic;

namespace C1.Scaffolder.Scaffolders
{
    internal class CodeGeneratorOption
    {
        public string TemplatePath { get; private set; }
        public string OutputPath { get; private set; }
        public Dictionary<string, object> Parameters { get; private set; }
        public bool IsMain { get; private set; }

        public CodeGeneratorOption(string outputPath, string templatePath, Dictionary<string, object> parameters,
            bool isMain = false)
        {
            TemplatePath = templatePath;
            OutputPath = outputPath;
            Parameters = parameters;
            IsMain = isMain;
        }
    }
}
