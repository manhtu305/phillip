﻿using C1.Scaffolder.Models.Olap;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder.Scaffolders
{
    internal class SlicerScaffolder : PivotScaffolder
    {
        private readonly SlicerOptionsModel _slicerOptionsModel;

        public SlicerScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new SlicerOptionsModel(serviceProvider, new Slicer()))
        {

        }

        public SlicerScaffolder(IServiceProvider serviceProvider, SlicerOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _slicerOptionsModel = optionsModel;
        }

        public SlicerScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new SlicerOptionsModel(serviceProvider, new Slicer(), settings))
        {

        }
    }
}
