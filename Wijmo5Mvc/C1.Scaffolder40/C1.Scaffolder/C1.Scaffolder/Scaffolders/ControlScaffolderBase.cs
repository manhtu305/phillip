﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using C1.Scaffolder.Extensions;
using C1.Scaffolder.Models;
using C1.Scaffolder.Services;
using C1.Scaffolder.Snippets;
using C1.Web.Mvc;
using C1.Web.Mvc.Serialization;
using C1.Web.Mvc.Services;

namespace C1.Scaffolder.Scaffolders
{
    internal abstract partial class ControlScaffolderBase : IControlScaffolder
    {
        private const string ControllerSuffix = "Controller";
        private const string PageModelSuffix  = "Model";
        private Dictionary<string, object> _templateParameters;

        public ControlOptionsModel OptionsModel { get; private set; }
        public IServiceProvider ServiceProvider { get; private set; }

        protected ControlScaffolderBase(IServiceProvider serviceProvider, ControlOptionsModel optionsModel)
        {
            ServiceProvider = serviceProvider;
            OptionsModel = optionsModel;
        }

        public virtual void BeforeGenerateCode()
        {
            if (
                OptionsModel != null && OptionsModel.Component != null &&
                OptionsModel.Component is IControlValue &&
                (ServiceProvider.GetService(typeof(IMvcProject)) as VS.MvcProject).IsAspNetCore
               )
            {
                (OptionsModel.Component as IControlValue).CorrectIdForAspnetCore();
            }
            OptionsModel.Component.BeforeSerialize();
        }

        public virtual void BeforeSerializeComponent()
        {
            MetadataDescriptor descriptor = GetDescriptor();
            var controllerSnippet = ServiceProvider.GetService(typeof(IControllerSnippets)) as IControllerSnippets;
            var viewSnippet = ServiceProvider.GetService(typeof(IViewSnippets)) as IViewSnippets;
            if (controllerSnippet == null || viewSnippet == null) return;
            if (OptionsModel.IsInsertOnCodePage)
            {
                foreach (var import in descriptor.ControllerNamespaces)
                {
                    controllerSnippet.AddImport(import);
                }
            }

            if (OptionsModel.IsAspNetCore)
            {
                foreach (var tag in descriptor.Namespaces)
                {
                    viewSnippet.AddImportTag(tag);
                }
            }
            else
            {
                foreach (var ns in descriptor.Namespaces)
                {
                    viewSnippet.AddImport(ns);
                }
            }

            foreach (var script in descriptor.Scripts)
            {
                viewSnippet.AddResourceScript(script);
            }

            foreach (var pk in descriptor.Packages)
            {
                viewSnippet.AddPackage(pk);
            }
        }

        public virtual void AfterSerializeComponent()
        {
            // do nothing.
        }

        public virtual List<CodeGeneratorOption> GetControllerGenerators()
        {
            var tm = ServiceProvider.GetService(typeof (ITemplateProvider)) as TemplateManager;
            Debug.Assert(tm != null);
            string controllersFolder, controllerName;
            if (OptionsModel.IsRazorPages)
            {
                controllersFolder = Path.Combine("Pages", OptionsModel.Project.SelectedPageFolder);
                var shortName = GetControllerShortName(OptionsModel.ControllerName);
                var suffix = OptionsModel.Project.IsCs ? ".cshtml" : ".vbhtml";
                controllerName = shortName + suffix;
            }
            else
            {
                controllersFolder = "Controllers";
                controllerName = OptionsModel.ControllerName;
            }
            
            return new[]
            {
                new CodeGeneratorOption(Path.Combine(controllersFolder, controllerName), tm.GetController(),
                    TemplateParameters, true)
            }.ToList();
        }

        public virtual List<CodeGeneratorOption> GetViewGenerators()
        {
            var tm = ServiceProvider.GetService(typeof(ITemplateProvider)) as TemplateManager;
            Debug.Assert(tm != null);
            string viewsFolder, viewName;
            var viewPath = GetViewPath();
            if (OptionsModel.IsRazorPages)
            {
                viewsFolder = Path.Combine("Pages", OptionsModel.Project.SelectedPageFolder);
                viewName = viewPath;
            }
            else
            {
                viewsFolder = Path.Combine("Views", viewPath);
                viewName = OptionsModel.ViewName;
            }

            return new[]
            {
                new CodeGeneratorOption(Path.Combine(viewsFolder, viewName), tm.GetView(),
                    TemplateParameters, true)
            }.ToList();
        }

        public virtual bool NeedGenerateCode { get { return true; } }

        internal string GetControllerShortName(string name)
        {
            var suffix = OptionsModel.Project.IsRazorPages ? PageModelSuffix : ControllerSuffix;
            return name.EndsWith(suffix)
                ? name.Remove(name.Length - suffix.Length, suffix.Length)
                : name;
        }

        public Dictionary<string, object> TemplateParameters
        {
            get { return _templateParameters ?? (_templateParameters = GetTemplateParameters()); }
        }

        protected virtual Dictionary<string, object> GetTemplateParameters()
        {
            var resources = new Dictionary<string, string>();
            FillTemplateResources(resources);

            var parameters = new Dictionary<string, object>
            {
                {"Resources", resources},
                {"Namespace", ResolveNamespace()},
                {"ControllerName", OptionsModel.ControllerName},
                {"ViewName", OptionsModel.ViewName},
                {"Component", GenerateModelParameters(OptionsModel.Component)},
                //{"ControlHtml", GenerateComponentHtml(OptionsModel)},
                {"DbContextsHolder", ControllerSnippet.DbContextsHolder},
                {"ViewBagsHolder", ControllerSnippet.ViewBagsHolder},
                {"ActionsHolder", ControllerSnippet.ActionsHolder}
            };
            return parameters;
        }

        protected virtual void FillTemplateResources(IDictionary<string, string> resources)
        {
        }

        protected virtual string ResolveNamespace()
        {
            var proj = OptionsModel.Project;
            string ns = string.Empty;
            var last = proj.IsRazorPages ? ".Pages" : ".Controllers";
            if (proj.IsCs)
            {
                ns = proj.Source.GetDefaultNamespace();
                if (!string.IsNullOrEmpty(proj.SelectedAreaName))
                {
                    ns += ".Areas." + proj.SelectedAreaName;
                }
                ns += last;
            }
            else
            {
                if (!string.IsNullOrEmpty(proj.SelectedAreaName))
                {
                    ns = "Areas." + proj.SelectedAreaName + last;
                }
            }

            if (proj.IsRazorPages && !string.IsNullOrEmpty(proj.SelectedPageFolder))
            {
                ns += "." + string.Join(".", proj.SelectedPageFolder.Split('/', '\\'));
            }

            return ns;
        }

        internal string GetViewPath()
        {
            var suffix = OptionsModel.IsRazorPages ? PageModelSuffix : ControllerSuffix;
            if (OptionsModel.ControllerName.EndsWith(suffix))
            {
                return OptionsModel.ControllerName.Remove(OptionsModel.ControllerName.Length - suffix.Length);
            }

            throw new FormatException(Localization.Resources.InvalidControllerName);
        }

#if SMARTASSEMBLYUSAGE
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        protected dynamic GenerateModelParameters(object model)
        {
            if (model == null)
            {
                return null;
            }

            var type = model.GetType();

            // process enum
            if (type.IsEnum)
            {
                return OptionsModel.IsAspNetCore ? Enum.GetName(type, model) : type.Name + "." + Enum.GetName(type, model);
            }

            var enumerableModel = model as IEnumerable;
            if (enumerableModel != null && !(model is string))
            {
                return (from dynamic item in enumerableModel select GenerateModelParameters(item)).ToArray();
            }

            if (Type.GetTypeCode(type) == TypeCode.Object)
            {
                var param = new Dictionary<string, dynamic>();
                var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (
                    var property in
                        properties.Where(p => p.GetCustomAttribute(typeof (IgnoreTemplateParameter)) == null))
                {
                    var value = property.GetValue(model);
                    param.Add(property.Name, GenerateModelParameters(value));
                }
                return param;
            }

            return model;
        }
    }
}
