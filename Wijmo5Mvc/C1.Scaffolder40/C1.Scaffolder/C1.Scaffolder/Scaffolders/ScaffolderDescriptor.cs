﻿using C1.Web.Mvc.Services;
using C1.Scaffolder.VS;
using EnvDTE;
using System.Collections.Generic;

namespace C1.Scaffolder.Scaffolders
{
    internal class ScaffolderDescriptor : IScaffolderDescriptor
    {
        public bool IsInsert { get; set; }
        public bool ShouldSerializeGenericParameter { get; set; }
        public MvcProjectItem ControllerItem { get; set; }
        public IControllerDescriptor Controller { get; set; }
        public IActionDescriptor Action { get; set; }
        public IViewDescriptor View { get; set; }
        public Dictionary<string, object> TemplateParameters { get; set; }

        /// <summary>
        /// Gets a value indicating whether the scaffolder is for insert command.
        /// </summary>
        public bool IsUpdate { get; set; }
        
    }
}
