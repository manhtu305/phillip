﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Scaffolder.Models.Sheet;
using C1.Web.Mvc;
using C1.Web.Mvc.Services;

namespace C1.Scaffolder.Scaffolders
{
    internal partial class FlexSheetScaffolder : DataBoundScaffolder
    {
        private readonly FlexSheetOptionsModel _flexSheetOptionsModel;

        public FlexSheetScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new FlexSheetOptionsModel(serviceProvider))
        {
        }

        public FlexSheetScaffolder(IServiceProvider serviceProvider, FlexSheetOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _flexSheetOptionsModel = optionsModel;
        }

        public FlexSheetScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings) 
            : this(serviceProvider, new FlexSheetOptionsModel(serviceProvider, controlSettings))
        {
        }

        protected override bool ShouldAddBoundParameters
        {
            get { return false; }
        }

        public override void AfterSerializeComponent()
        {
            base.AfterSerializeComponent();

            if (_flexSheetOptionsModel.FlexSheet.UseRemoteSave)
            {
                var viewSnippets = ServiceProvider.GetService(typeof(IViewSnippets)) as IViewSnippets;
                var tp = ServiceProvider.GetService(typeof(ITemplateProvider)) as ITemplateProvider;
                if (viewSnippets != null && tp != null)
                {
                    var path = tp.Get("Views\\FlexSheet\\SaveButton.t4");
                    var parameters = new Dictionary<string, object>();
                    parameters.Add("Id", _flexSheetOptionsModel.FlexSheet.Id);
                    parameters.Add("SaveText", C1.Scaffolder.Localization.Resources.TemplateFlexSheetSave);
                    var content = tp.TransformTemplate(path, parameters);
                    viewSnippets.AddHtml(content);
                }
            }
        }
    }
}
