﻿using System;
using C1.Scaffolder.Models.Chart;
using C1.Web.Mvc;

namespace C1.Scaffolder.Scaffolders
{
    class SunburstScaffolder : FlexPieScaffolder
    {
        public SunburstScaffolder(IServiceProvider serviceProvider)
            : base(serviceProvider, new SunburstOptionsModel(serviceProvider))
        {
        }

        public SunburstScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings) 
            : base(serviceProvider, new SunburstOptionsModel(serviceProvider, controlSettings))
        {
        }
    }
}
