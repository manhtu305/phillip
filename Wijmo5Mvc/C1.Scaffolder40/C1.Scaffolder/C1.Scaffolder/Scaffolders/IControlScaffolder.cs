﻿using System;
using System.Collections.Generic;
using C1.Scaffolder.Models;

namespace C1.Scaffolder.Scaffolders
{
    interface IControlScaffolder
    {
        bool NeedGenerateCode { get; }
        List<CodeGeneratorOption> GetControllerGenerators();
        List<CodeGeneratorOption> GetViewGenerators();
        void BeforeGenerateCode();
        void BeforeSerializeComponent();
        void AfterSerializeComponent();
        ControlOptionsModel OptionsModel { get; }
        IServiceProvider ServiceProvider { get; }
        Dictionary<string, object> TemplateParameters { get; }
    }
}
