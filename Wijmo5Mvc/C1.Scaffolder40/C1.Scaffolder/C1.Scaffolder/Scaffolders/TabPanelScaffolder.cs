﻿using System;
using C1.Scaffolder.Models;
using C1.Web.Mvc;

namespace C1.Scaffolder.Scaffolders
{
    internal class TabPanelScaffolder : ControlScaffolderBase
    {
        private readonly TabPanelOptionsModel _optionsModel;

        public TabPanelScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new TabPanelOptionsModel(serviceProvider, new TabPanel()))
        {
        }

        public TabPanelScaffolder(IServiceProvider serviceProvider, ControlOptionsModel optionsModel) 
            :base(serviceProvider, optionsModel)
        {
            this._optionsModel = (TabPanelOptionsModel)optionsModel;
        }

        public TabPanelScaffolder(IServiceProvider serviceProvider, ControlOptionsModel optionsModel, MVCControl controlSettings)
            : base(serviceProvider, optionsModel)
        {
            this._optionsModel = (TabPanelOptionsModel)optionsModel;
        }

        public TabPanelScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new TabPanelOptionsModel(serviceProvider, new TabPanel(controlSettings)))
        {
            
        }
    }
}
