﻿using System;
using C1.Scaffolder.Models.Chart;
using C1.Web.Mvc;

namespace C1.Scaffolder.Scaffolders
{
    internal class FlexPieScaffolder : ChartBaseScaffolder
    {
        public FlexPieScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new FlexPieOptionsModel(serviceProvider))
        {
        }

        public FlexPieScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new FlexPieOptionsModel(serviceProvider, controlSettings))
        {
        }

        public FlexPieScaffolder(IServiceProvider serviceProvider, FlexPieOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
        }
    }
}
