﻿using System;
using System.Collections.Generic;
using C1.Scaffolder.Models;
using C1.Web.Mvc;

namespace C1.Scaffolder.Scaffolders
{
    internal abstract class DataBoundScaffolder : ControlScaffolderBase
    {
        private readonly DataBoundOptionsModel _dataBoundOptionsModel;

        protected DataBoundScaffolder(IServiceProvider serviceProvider, DataBoundOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _dataBoundOptionsModel = optionsModel;
        }

        protected override Dictionary<string, object> GetTemplateParameters()
        {
            var parameters = base.GetTemplateParameters();
            if (ShouldAddBoundParameters)
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                if (dbService != null)
                {
                    parameters.Add("DbContextTypeName",
                        dbService.DbContextType == null
                            ? string.Empty
                            : dbService.DbContextType.TypeName);
                    parameters.Add("ViewDataTypeName",
                        dbService.ModelType == null ? string.Empty : dbService.ModelType.TypeName);
                    parameters.Add("ModelTypeName",
                        dbService.ModelType == null
                            ? string.Empty
                            : dbService.ModelType.ShortTypeName);
                    parameters.Add("PrimaryKeys", dbService.PrimaryKeys);
                }
            }
            return parameters;
        }

        protected virtual bool ShouldAddBoundParameters
        {
            get { return true; }
        }
    }
}
