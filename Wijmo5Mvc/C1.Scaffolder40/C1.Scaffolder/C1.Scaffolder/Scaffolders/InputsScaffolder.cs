﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using C1.Scaffolder.Localization;
using C1.Scaffolder.Models.Inputs;
using C1.Scaffolder.Services;
using C1.Web.Mvc;
using C1.Web.Mvc.Services;

namespace C1.Scaffolder.Scaffolders
{
    internal class InputsScaffolder : DataBoundScaffolder
    {
        private static readonly string[] SubViews = new string[] { "Index", "Create", "Details", "Delete", "Edit"};
        private readonly InputsOptionsModel _inputOptionsModel = null;

        public InputsScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new InputsOptionsModel(serviceProvider))
        {
        }

        public InputsScaffolder(IServiceProvider serviceProvider, InputsOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _inputOptionsModel = optionsModel;
        }

        public InputsScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new InputsOptionsModel(serviceProvider, controlSettings))
        {
        }

        public override List<CodeGeneratorOption> GetControllerGenerators()
        {
            if (!OptionsModel.Project.IsRazorPages)
            {
                return base.GetControllerGenerators();
            }
            else
            {
                var tm = ServiceProvider.GetService(typeof(ITemplateProvider)) as TemplateManager;
                Debug.Assert(tm != null);
                var shortName = GetControllerShortName(OptionsModel.ControllerName);
                string controllersFolder = Path.Combine("Pages", OptionsModel.Project.SelectedPageFolder, shortName);
                var suffix = OptionsModel.Project.IsCs ? ".cshtml" : ".vbhtml";

                return SubViews.Select(v => new CodeGeneratorOption(
                    Path.Combine(controllersFolder, v+suffix), tm.GetController(v), TemplateParameters, v == "Index"))
                    .ToList();
            }
        }

        public override List<CodeGeneratorOption> GetViewGenerators()
        {
            var tm = ServiceProvider.GetService(typeof(ITemplateProvider)) as TemplateManager;
            Debug.Assert(tm != null);
            string viewsFolder;
            var viewPath = GetViewPath();
            if (OptionsModel.IsRazorPages)
            {
                viewsFolder = Path.Combine("Pages", OptionsModel.Project.SelectedPageFolder, viewPath);
            }
            else
            {
                viewsFolder = Path.Combine("Views", viewPath);
            }

            return SubViews.Select(v => new CodeGeneratorOption(
                Path.Combine(viewsFolder, v), tm.GetView(v), TemplateParameters, v == "Index"))
                .ToList();
        }

        protected override void FillTemplateResources(IDictionary<string, string> resources)
        {
            base.FillTemplateResources(resources);

            resources["InputsBackToList"] = Resources.TemplateInputsBackToList;
            resources["InputsCreate"] = Resources.TemplateInputsCreate;
            resources["InputsCreateNew"] = Resources.TemplateInputsCreateNew;
            resources["InputsDelete"] = Resources.TemplateInputsDelete;
            resources["InputsDeleteConfirmation"] = Resources.TemplateInputsDeleteConfirmation;
            resources["InputsDetails"] = Resources.TemplateInputsDetails;
            resources["InputsEdit"] = Resources.TemplateInputsEdit;
            resources["InputsSave"] = Resources.TemplateInputsSave;
        }

        protected override string ResolveNamespace()
        {
            var ns = base.ResolveNamespace();
            var shortName = GetControllerShortName(OptionsModel.ControllerName);
            return string.IsNullOrEmpty(ns) ? shortName : ns + "." + shortName;
        }

        public override bool NeedGenerateCode { get { return false; } }
    }
}
