﻿using System;
using System.ComponentModel.Design;
using C1.Scaffolder.CodeBuilder;
using C1.Scaffolder.Services;
using C1.Scaffolder.UI;
using C1.Scaffolder.VS;
using C1.Web.Mvc;
using C1.Web.Mvc.Services;
using EnvDTE;
using Microsoft.AspNet.Scaffolding;
using System.Collections.Generic;

namespace C1.Scaffolder.Scaffolders
{
    internal class ScaffolderFactory
    {
        public static IControlScaffolder CreateInstance(IServiceProvider serviceProvider, CodeGenerationContext context, string controlName, bool isInsert = false)
        {
            IControlScaffolder scaffolder = null;
            var serviceContainer = CreateServices(serviceProvider, context, isInsert);
            switch (controlName)
            {
                case ControlList.FlexGrid:
                    scaffolder = new FlexGridScaffolder(serviceContainer);
                    break;
                case ControlList.MultiRow:
                    scaffolder = new MultiRowScaffolder(serviceContainer);
                    break;
                case ControlList.Input:
                    scaffolder = new InputsScaffolder(serviceContainer);
                    break;
                case ControlList.FlexSheet:
                    scaffolder = new FlexSheetScaffolder(serviceContainer);
                    break;
                case ControlList.FlexChart:
                    scaffolder = new FlexChartScaffolder(serviceContainer);
                    break;
                case ControlList.FlexPie:
                    scaffolder = new FlexPieScaffolder(serviceContainer);
                    break;
                case ControlList.Sunburst:
                    scaffolder = new SunburstScaffolder(serviceContainer);
                    break;
                case ControlList.FlexRadar:
                    scaffolder = new FlexRadarScaffolder(serviceContainer);
                    break;
                case ControlList.InputDate:
                    scaffolder = new InputDateScaffolder(serviceContainer);
                    break;
                case ControlList.InputDateTime:
                    scaffolder = new InputDateTimeScaffolder(serviceContainer);
                    break;
                case ControlList.InputTime:
                    scaffolder = new InputTimeScaffolder(serviceContainer);
                    break;
                case ControlList.InputColor:
                    scaffolder = new InputColorScaffolder(serviceContainer);
                    break;
                case ControlList.InputMask:
                    scaffolder = new InputMaskScaffolder(serviceContainer);
                    break;
                case ControlList.InputNumber:
                    scaffolder = new InputNumberScaffolder(serviceContainer);
                    break;
                case ControlList.ComboBox:
                    scaffolder = new ComboBoxScaffolder(serviceContainer);
                    break;
                case ControlList.MultiSelect:
                    scaffolder = new MultiSelectScaffolder(serviceContainer);
                    break;
                case ControlList.AutoComplete:
                    scaffolder = new AutoCompleteScaffolder(serviceContainer);
                    break;
                case ControlList.MultiAutoComplete:
                    scaffolder = new MultiAutoCompleteScaffolder(serviceContainer);
                    break;
                case ControlList.PivotEngine:
                    scaffolder = new PivotEngineScaffolder(serviceContainer);
                    break;
                case ControlList.PivotPanel:
                    scaffolder = new PivotPanelScaffolder(serviceContainer);
                    break;
                case ControlList.PivotChart:
                    scaffolder = new PivotChartScaffolder(serviceContainer);
                    break;
                case ControlList.PivotGrid:
                    scaffolder = new PivotGridScaffolder(serviceContainer);
                    break;
                case ControlList.Slicer:
                    scaffolder = new SlicerScaffolder(serviceContainer);
                    break;
                case ControlList.TabPanel:
                    scaffolder = new TabPanelScaffolder(serviceContainer);
                    break;
                case ControlList.Dashboard:
                    scaffolder = new DashboardScaffolder(serviceContainer);
                    break;
                default:
                    var type = typeof (ScaffolderFactory);
                    var scaffolderTypeName = string.Format("{0}.{1}Scaffolder", type.Namespace, controlName);
                    var scaffolderType = type.Assembly.GetType(scaffolderTypeName);
                    if (scaffolderType != null)
                    {
                        try
                        {
                            scaffolder = (IControlScaffolder)Activator.CreateInstance(scaffolderType, serviceContainer);
                            break;
                        }
                        catch
                        {
                            throw new NotSupportedException();
                        }
                    }

                    throw new NotSupportedException();
            }

            serviceContainer.AddService(typeof(IControlScaffolder), scaffolder);
            serviceContainer.AddService(typeof(ITemplateProvider), new TemplateManager(serviceContainer));

            return scaffolder;
        }

        private static IServiceContainer CreateServices(IServiceProvider serviceProvider, CodeGenerationContext context, bool isInsert, bool isUpdate = false)
        {
            Project project;
            if (context == null)
            {
                var dte = (DTE) serviceProvider.GetService(typeof (DTE));
                var projectItem = dte.ActiveDocument.ProjectItem;
                project = projectItem.ContainingProject;
                //context=new CodeGenerationContext(project, projectItem, serviceProvider);
            }
            else
            {
                project = context.ActiveProject;
            }

            var serviceBag = new ServiceBag(serviceProvider, context);
            var mvcProject = new MvcProject(project, isInsert);
            serviceBag.AddService(typeof(IMvcProject), mvcProject);
            serviceBag.AddService(typeof(IScaffolderDescriptor), new ScaffolderDescriptor { IsInsert = isInsert , IsUpdate = isUpdate});
            var generator = mvcProject.IsCs ? (ICodeBuilder) new CsCodeBuilder() : new VbCodeBuilder();
            serviceBag.AddService(typeof(ICodeBuilder), generator);
            serviceBag.AddService(typeof(IDesignHelper), new DesignHelper());
            ServiceManager.UpdateInstance(serviceBag);

            ServiceManager.Instance.AddService(typeof(IDbContextProvider), new DbContextProvider(ServiceManager.Instance));
            ServiceManager.Instance.AddService(typeof(IDefaultDbContextProvider), new DefaultDbContextProvider(ServiceManager.Instance));

            return ServiceManager.Instance;
        }

        public static IControlScaffolder CreateInstance(IServiceProvider serviceProvider, CodeGenerationContext context, MVCControl controlSettings, bool isInsert, bool isUpdate)
        {
            IControlScaffolder scaffolder = null;
            var serviceContainer = CreateServices(serviceProvider, context, isInsert, isUpdate);
            string controlName = controlSettings.Name;
            switch (controlName)
            {
                case ControlList.FlexGrid:
                    scaffolder = new FlexGridScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.MultiRow:
                    scaffolder = new MultiRowScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.Input:
                    scaffolder = new InputsScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.FlexSheet:
                    scaffolder = new FlexSheetScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.FlexChart:
                    scaffolder = new FlexChartScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.FlexPie:
                    scaffolder = new FlexPieScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.Sunburst:
                    scaffolder = new SunburstScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.FlexRadar:
                    scaffolder = new FlexRadarScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.InputDate:
                    scaffolder = new InputDateScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.InputDateTime:
                    scaffolder = new InputDateTimeScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.InputTime:
                    scaffolder = new InputTimeScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.InputColor:
                    scaffolder = new InputColorScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.InputMask:
                    scaffolder = new InputMaskScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.InputNumber:
                    scaffolder = new InputNumberScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.ComboBox:
                    scaffolder = new ComboBoxScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.MultiSelect:
                    scaffolder = new MultiSelectScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.AutoComplete:
                    scaffolder = new AutoCompleteScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.MultiAutoComplete:
                    scaffolder = new MultiAutoCompleteScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.PivotEngine:
                    scaffolder = new PivotEngineScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.PivotPanel:
                    scaffolder = new PivotPanelScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.PivotChart:
                    scaffolder = new PivotChartScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.PivotGrid:
                    scaffolder = new PivotGridScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.Slicer:
                    scaffolder = new SlicerScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.TabPanel:
                    scaffolder = new TabPanelScaffolder(serviceContainer, controlSettings);
                    break;
                case ControlList.Dashboard:
                    scaffolder = new DashboardScaffolder(serviceContainer, controlSettings);
                    break;
                default:
                    var type = typeof(ScaffolderFactory);
                    var scaffolderTypeName = string.Format("{0}.{1}Scaffolder", type.Namespace, controlName);
                    var scaffolderType = type.Assembly.GetType(scaffolderTypeName);
                    if (scaffolderType != null)
                    {
                        try
                        {
                            scaffolder = (IControlScaffolder)Activator.CreateInstance(scaffolderType, serviceContainer);
                            break;
                        }
                        catch
                        {
                            throw new NotSupportedException();
                        }
                    }

                    throw new NotSupportedException();
            }

            serviceContainer.AddService(typeof(IControlScaffolder), scaffolder);
            serviceContainer.AddService(typeof(ITemplateProvider), new TemplateManager(serviceContainer));

            return scaffolder;
        }
    }
}
