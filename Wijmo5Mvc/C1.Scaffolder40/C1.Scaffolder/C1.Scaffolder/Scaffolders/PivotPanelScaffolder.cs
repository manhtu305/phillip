﻿using C1.Scaffolder.Models.Olap;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder.Scaffolders
{
    internal class PivotPanelScaffolder : PivotScaffolder
    {
        private readonly PivotPanelOptionsModel _pivotPanelOptionsModel;

        public PivotPanelScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new PivotPanelOptionsModel(serviceProvider, new PivotPanel()))
        {
        }

        public PivotPanelScaffolder(IServiceProvider serviceProvider, PivotPanelOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _pivotPanelOptionsModel = optionsModel;
        }

        public PivotPanelScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new PivotPanelOptionsModel(serviceProvider, new PivotPanel(), settings))
        {
        }

        protected override Dictionary<string, object> GetTemplateParameters()
        {
            //Because ReadActionUrl and ServiceUrl will be overriden by CubeService initialization (inside PivotEngine control)
            //we need to backup these values.
            PivotPanel pivotPanel = OptionsModel.Component as PivotPanel;
            PivotEngine engine = pivotPanel.Engine;
            string serviceUrl = engine.ServiceUrl ?? null;
            string readUrlAction = pivotPanel.ItemsSource.ReadActionUrl ?? null;

            var parameters = base.GetTemplateParameters();

            if (!string.IsNullOrEmpty(serviceUrl))
            {
                engine.ServiceUrl = serviceUrl;
            }

            if (!string.IsNullOrEmpty(readUrlAction))
            {
                pivotPanel.ItemsSource.ReadActionUrl = readUrlAction;
            }

            var dbService = ServiceManager.DefaultDbContextProvider;
            if (dbService != null)
            {
                parameters.Add("DbContextTypeName",
                    dbService.DbContextType == null
                        ? string.Empty
                        : dbService.DbContextType.TypeName);
                parameters.Add("ViewDataTypeName",
                    dbService.ModelType == null ? string.Empty : dbService.ModelType.TypeName);
                parameters.Add("ModelTypeName",
                    dbService.ModelType == null
                        ? string.Empty
                        : dbService.ModelType.ShortTypeName);
                parameters.Add("PrimaryKeys", dbService.PrimaryKeys);
            }

            return parameters;
        }
    }
}
