﻿using System;
using C1.Scaffolder.Scaffolders;
using C1.Scaffolder.UI;
using C1.Scaffolder.VS;
using C1.Web.Mvc.Services;
using C1.Scaffolder.Services;
using C1.Util.Licensing;
using System.IO;

namespace C1.Scaffolder
{
    public class C1InsertCodeGenerator
    {
        private IControlScaffolder _scaffolder;

        public C1InsertCodeGenerator(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public IServiceProvider ServiceProvider { get; private set; }

        public void Run()
        {
            if (!ShowUiAndValidate()) return;

            GenerateCode();
            NotifyActivateLicense(_scaffolder);
        }

        internal static void NotifyActivateLicense(IControlScaffolder scaffolder)
        {
            var project = scaffolder != null ? scaffolder.ServiceProvider.GetService(typeof(IMvcProject)) as MvcProject : null;
#if !GRAPECITY
            if (project != null && project.IsAspNetCore)
            {
                string licenseFileName = "GCDTLicenses.xml";
                if (project.Source.ProjectItems.GetProjectItem(licenseFileName) == null)
                {
                    System.Windows.Forms.MessageBox.Show("Run Tools/Grapecity License Manager to add license to the project.", "License information missing");
                }
            }
#endif
        }

        private bool ShowUiAndValidate()
        {
            // Bring up the selection dialog and allow user to select a model type
            var controlSelectionWindow = new ControlSelectionWindow(true);
            var showDialog = controlSelectionWindow.ShowDialog();
            if (!(showDialog.HasValue && (bool)showDialog))
            {
                return false;
            }

            var controlName = controlSelectionWindow.SelectedControl;
            _scaffolder = ScaffolderFactory.CreateInstance(ServiceProvider, null, controlName, true);
            var designHelper = _scaffolder.ServiceProvider.GetService(typeof(IDesignHelper)) as DesignHelper;
            designHelper.IsDesignTime = true;
            var controlConfigurationWindow = new ControlConfigurationWindow(_scaffolder.OptionsModel);
            var dialogResult = controlConfigurationWindow.ShowDialog();
            designHelper.IsDesignTime = false;
            return dialogResult.HasValue && (bool)dialogResult;
        }

        private void GenerateCode()
        {
            _scaffolder.BeforeGenerateCode();
            var project = _scaffolder.ServiceProvider.GetService(typeof (IMvcProject)) as MvcProject;
            var generator = new C1CodeGenerator(_scaffolder);
            generator.GenerateCode(_scaffolder.OptionsModel.SelectedAction.ProjectItem,
                _scaffolder.OptionsModel.SelectedController, _scaffolder.OptionsModel.SelectedAction,
                project.ActiveItem.ViewDescriptor);
        }
    }
}
