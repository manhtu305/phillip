﻿using System;
using System.ComponentModel.Design;
using Microsoft.VisualStudio.Shell;

namespace C1.Scaffolder
{
    internal sealed class AsyncInsertCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("dd4299f1-33ca-4dc8-901c-070e41eeed56");

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncInsertCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private AsyncInsertCommand(AsyncPackage package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package");
            }
            asyncPackage = package;
        }


        private void InsertCmdItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;
            if (null != menuCommand)
            {
                menuCommand.Enabled = C1ControlUpdater.GetControl(AsyncPackage as Package) == null;
            }
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void MenuItemCallback(object sender, EventArgs e)
        {
            var codeGenerator = new C1InsertCodeGenerator(AsyncPackage as Package);
            codeGenerator.Run();
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static AsyncInsertCommand Instance
        {
            get;
            private set;
        }


        private readonly AsyncPackage asyncPackage;
        private AsyncPackage AsyncPackage
        {
            get
            {
                return asyncPackage;
            }
        }
        /// <summary>
        /// Initializes the instance of the command.
        /// </summary>
        /// <param name="asyncPackage"></param>
        public static async System.Threading.Tasks.Task InitinalizeAsync(AsyncPackage asyncPackage)
        {
            Instance = new AsyncInsertCommand(asyncPackage);
            var commandService = await Instance.AsyncPackage.GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                OleMenuCommand insertCmdItem = new OleMenuCommand(new EventHandler(Instance.MenuItemCallback), menuCommandID);
                insertCmdItem.BeforeQueryStatus += Instance.InsertCmdItem_BeforeQueryStatus;
                commandService.AddCommand(insertCmdItem);
            }
        }


    }
}
