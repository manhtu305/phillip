﻿using System;
using System.ComponentModel.Design;
using Microsoft.VisualStudio.Shell;

namespace C1.Scaffolder
{
    internal sealed class InsertMvcControlCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("dd4299f1-33ca-4dc8-901c-070e41eeed56");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly Package _package;

        /// <summary>
        /// Initializes a new instance of the <see cref="InsertMvcControlCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private InsertMvcControlCommand(Package package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package");
            }

            _package = package;

            var commandService = ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                OleMenuCommand insertCmdItem = new OleMenuCommand(new EventHandler(this.MenuItemCallback), menuCommandID);
                insertCmdItem.BeforeQueryStatus += InsertCmdItem_BeforeQueryStatus;
                commandService.AddCommand(insertCmdItem);
            }
        }

        private void InsertCmdItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;            
            if (null != menuCommand)
            {
                menuCommand.Enabled = C1ControlUpdater.GetControl(ServiceProvider) == null;
            }  
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static InsertMvcControlCommand Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private IServiceProvider ServiceProvider
        {
            get
            {
                return _package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static void Initialize(Package package)
        {
            Instance = new InsertMvcControlCommand(package);
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void MenuItemCallback(object sender, EventArgs e)
        {
            var codeGenerator = new C1InsertCodeGenerator(ServiceProvider);
            codeGenerator.Run();
        }
    }
}
