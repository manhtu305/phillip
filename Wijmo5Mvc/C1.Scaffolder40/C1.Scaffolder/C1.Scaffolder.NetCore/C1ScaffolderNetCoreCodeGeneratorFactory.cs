﻿using System;
using System.ComponentModel.Composition;
using EnvDTE;
using Microsoft.AspNet.Scaffolding;
using Microsoft.VisualStudio.Shell;
using C1.Scaffolder.Extensions;

namespace C1.Scaffolder
{
    [Export(typeof(CodeGeneratorFactory))]
    public class C1ScaffolderNetCoreCodeGeneratorFactory : CodeGeneratorFactory, INetCoreScaffolderFactory
    {
        public C1ScaffolderNetCoreCodeGeneratorFactory()
            : base(FactoryHelper.Info)
        {
        }

        /// <summary>
        /// This method creates the code generator instance.
        /// </summary>
        /// <param name="context">The context has details on current active project, project item selected, Nuget packages that are applicable and service provider.</param>
        /// <returns>Instance of CodeGenerator.</returns>
        public override ICodeGenerator CreateInstance(CodeGenerationContext context)
        {
            return new C1ScaffolderCodeGenerator(context, Information);
        }

        /// <summary>
        /// Provides a way to check if the custom scaffolder is valid under this context
        /// </summary>
        /// <param name="codeGenerationContext">The code generation context</param>
        /// <returns>True if valid, False otherwise</returns>
        public override bool IsSupported(CodeGenerationContext codeGenerationContext)
        {
            var dte = Package.GetGlobalService(typeof(EnvDTE.DTE)) as EnvDTE.DTE;

            // only for VS2017 NET CORE project.
            if (new Version(dte.Version) < new Version("15.0") 
                || !codeGenerationContext.ActiveProject.IsAspNetCore())
            {
                return false;
            }


            return FactoryHelper.IsSupported(codeGenerationContext);
        }
    }
}
