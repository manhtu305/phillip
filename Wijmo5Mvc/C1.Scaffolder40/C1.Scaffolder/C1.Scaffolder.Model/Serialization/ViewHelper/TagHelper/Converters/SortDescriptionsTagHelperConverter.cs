﻿using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc.Serialization
{
    internal sealed class SortDescriptionsTagHelperConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var sortDescriptions = (IList<SortDescription>)value;
            var txtSortDescriptions = string.Join(",", sortDescriptions.Select(sd => string.Format("{0}{1}", sd.Property, sd.Ascending ? "" : " DESC")));
            writer.WriteObject(txtSortDescriptions);
        }
    }
}
