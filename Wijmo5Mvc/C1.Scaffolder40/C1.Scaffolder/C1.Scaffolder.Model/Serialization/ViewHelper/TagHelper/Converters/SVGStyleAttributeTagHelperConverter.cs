﻿using System;
using System.IO;

namespace C1.Web.Mvc.Serialization
{
    internal sealed class ComplexAsAttributeTagHelperConverter : ViewConverter
    {
        private readonly string _propertyName;

        public ComplexAsAttributeTagHelperConverter(string propertyName)
        {
            _propertyName = propertyName.Substring(0,1).ToLower() + propertyName.Substring(1);
        }

        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            // Add a variable in the view.
            var variableName = writer.View.GetMemberName(_propertyName);
            var codes = CsCodeSnippetWriter.SerializeObject(value, context);
            writer.View.AddCode(string.Format("var {0} = {1};", variableName, codes));

            // Use the variable.
            writer.WriteText(string.Format("@{0}", variableName));
        }
    }

    internal sealed class CsCodeSnippetWriter : HtmlHelperWriter
    {
        #region Properties
        internal override string ItemSeperator
        {
            get
            {
                return null;
            }
        }
        #endregion Properties

        #region Ctor
        public CsCodeSnippetWriter(IContext context, TextWriter writer)
            : base(context, writer)
        {
        }
        #endregion Ctor

        #region ShouldSerialize
        internal override bool HasIgnoreAttribute(object memberInfo, object parent)
        {
            return false;
        }
        #endregion ShouldSerialize

        #region Raw Values
        #region Simple
        internal override void WriteSimpleValue(char value)
        {
            WriteRawText(string.Format("'{0}'", value.ToString()), false);
        }

        internal override void WriteSimpleValue(bool value)
        {
            WriteRawText(value ? "true" : "false", false);
        }
        #endregion Simple

        #region Complex
        internal override void StartWriteComplex()
        {
            base.StartWriteComplex();
            CheckScopes();
            WriteRawText(string.Format("new {0}()", Scopes.Peek().Object.GetType().FullName), false);
        }

        internal override void WriteComplexPrefix(object value)
        {
            WriteNewLine(true);
            DecreaseIndent();
            WriteText("{");
            IncreaseIndent();
            WriteNewLine(true);
        }

        internal override void EndWriteComplex()
        {
            CheckScopes();
            var scope = Scopes.Peek();
            base.EndWriteComplex();
            if(scope.ObjectCount > 0)
            {
                DecreaseIndent();
                WriteNewLine();
                WriteText("}");
                IncreaseIndent();
            }
        }

        internal override void WriteComplexBuilderName(string name)
        {
        }
        #endregion Complex

        #region Collection
        internal override void WriteCollectionPrefix()
        {
        }

        internal override void WriteCollectionSuffix()
        {
        }
        #endregion Collection
        #endregion Raw Values

        #region Value
        internal override void WriteMemberValueWithScope(object value, object memberInfo, BaseConverter converter = null, bool withoutComplexBuilderName = false)
        {
            StartScope(value);
            WriteRawMemberValue(value, memberInfo);
            EndScope();
        }
        #endregion Value

        #region Name
        internal override void WriteMemberName(string name, object value, object memberInfo)
        {
            StartWriteNewMember();
            WriteText(string.Format("{0} = ", name));
        }
        #endregion Name

        #region Member
        internal override void WriteMemberInfo(string name, object value, object memberInfo, BaseConverter converter)
        {
            CheckScopes();
            var scope = Scopes.Peek();
            if (scope.Type != ScopeType.Complex)
            {
                throw new NotImplementedException();
            }
            if (scope.ObjectCount > 0)
            {
                WriteText(",");
                WriteNewLine();
            }
            base.WriteMemberInfo(name, value, memberInfo, converter);
        }
        #endregion Member

        #region Public
        public override void WriteObject(object value, BaseConverter converter = null)
        {
            WriteMemberValueWithScope(value, null);
        }

        internal override string GetGenericType()
        {
            return null;
        }
        #endregion Public

        #region Helper
        public static string SerializeObject(object value, IContext context)
        {
            using (var sw = new StringWriter())
            {
                var setting = new ViewSetting();
                setting.Converters.Clear();
                setting.Resolvers.Clear();
                var codeContext = new BaseContext(setting, context.ServiceProvider);
                var writer = new CsCodeSnippetWriter(codeContext, sw);
                writer.WriteObject(value);
                return sw.ToString();
            }
        }

        /// <summary>
        /// Writes a new line.
        /// </summary>
        /// <param name="connected">A boolean value indicates whether to write a connected new line.</param>
        public override void WriteNewLine(bool connected = false)
        {
            Output.WriteLine();
            base.WriteNewLine(connected);
        }

        internal override System.CodeDom.Compiler.CodeDomProvider GetCodeDomProvider()
        {
            return new Microsoft.CSharp.CSharpCodeProvider();
        }
        #endregion Helper
    }

}
