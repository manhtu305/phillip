﻿using System;
using System.Linq;

namespace C1.Web.Mvc.Serialization
{
    internal abstract class C1PropertyResolver : BaseResolver
    {
        public abstract string[] Names { get; }

        public override bool CanResolve(string name, object value, Type type, IContext context)
        {
            return Names.Contains(name) && (typeof(IC1Property).IsAssignableFrom(type) || value is IC1Property);
        }

        public override BaseConverter ResolveConverter(string name, object value, Type type, IContext context)
        {
            var cpValue = value as IC1Property;
            if(cpValue != null)
            {
                cpValue.C1Property = name;
            }

            return null;
        }
    }

    internal class AxisResolver : C1PropertyResolver
    {
        private static string[] AXIS_NAMES = new string[] { "AxisX", "AxisY", "DesignAxisX", "DesignAxisY" };
        public override string[] Names
        {
            get
            {
                return AXIS_NAMES;
            }
        }

        public override string ResolvePropertyName(string name, object value, Type type, IContext context)
        {
            var propertyName = name;
            switch (propertyName)
            {
                case "DesignAxisX":
                    propertyName = "AxisX";
                    break;
                case "DesignAxisY":
                    propertyName = "AxisY";
                    break;
            }
            return propertyName;
        }

        public override BaseConverter ResolveConverter(string name, object value, Type type, IContext context)
        {
            base.ResolveConverter(name, value, type, context);
            return new ChartIItemsSourceBindingHolderConverter();
        }
    }

    internal class TitleStyleResolver : C1PropertyResolver
    {
        private static string[] TITLESTYLE_NAMES = new string[] { "HeaderStyle", "FooterStyle" };
        public override string[] Names
        {
            get
            {
                return TITLESTYLE_NAMES;
            }
        }
    }

    internal class PivotFieldCollectionResolver : C1PropertyResolver
    {
        private static string[] FIELDS_NAMES = new string[] { "RowFields", "ColumnFields", "ValueFields", "FilterFields" };
        public override string[] Names
        {
            get
            {
                return FIELDS_NAMES;
            }
        }
    }
}
