﻿namespace C1.Web.Mvc.Serialization
{
    internal class ViewSetting : BaseSetting
    {
        public int MaxLineLength { get; set; }
        public int DefaultIndent { get; set; }

        public ViewSetting()
        {
            MaxLineLength = 100;
            DefaultIndent = 4;
            Converters.Add(new ViewPointConverter());
        }
    }
}
