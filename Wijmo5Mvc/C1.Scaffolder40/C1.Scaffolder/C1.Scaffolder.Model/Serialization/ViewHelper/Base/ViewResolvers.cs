﻿using System;

namespace C1.Web.Mvc.Serialization
{
    internal class ViewClientEventResolver : C1ClientEventResolver
    {
        public override BaseConverter ResolveConverter(string name, object value, Type type, IContext context)
        {
            return new ViewFunctionConverter();
        }
    }
}
