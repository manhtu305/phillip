﻿using C1.Web.Mvc.Services;
using System;

namespace C1.Web.Mvc.Serialization
{
    internal class ViewUtility
    {
        public static ViewType? GetCurrentViewType(IServiceProvider serviceProvider)
        {
            if(serviceProvider == null)
            {
                return null;
            }

            var svMVCProject = serviceProvider.GetService(typeof(IMvcProject)) as IMvcProject;
            if (svMVCProject == null)
            {
                return null;
            }

            if (svMVCProject.IsAspNetCore)
            {
                return ViewType.TagHelper;
            }

            if (svMVCProject.IsCs)
            {
                return ViewType.CSHtmlHelper;
            }

            return ViewType.VBHtmlHelper;
        }

        public static string GetNameWithoutGeneric(string name)
        {
            var index = name.IndexOf("`");
            if (index == -1)
            {
                return name;
            }

            return name.Substring(0, index);
        }

        public static string ConverViewBagToModelType(string viewBagName, string modeTypeName, IContext context)
        {
            var proj = GetProjectInfo(context);
            var viewBagItem = proj != null && proj.IsRazorPages
                    ? string.Format("ViewData[\"{0}\"]", viewBagName)
                    : string.Format("ViewBag.{0}", viewBagName);

            if (proj == null || proj.IsCs)
            {
                var model = string.Format("(IEnumerable<{0}>){1}", modeTypeName, viewBagItem);
                if (proj != null && proj.IsAspNetCore)
                {
                    model = string.Format("@({0})", model);
                }
                return model;
            }
            else
            {
                return string.Format("DirectCast({0},IEnumerable(Of {1}))", viewBagItem, modeTypeName);
            }
        }

        public static IMvcProject GetProjectInfo(IContext context)
        {
            return context.ServiceProvider.GetService(typeof(IMvcProject)) as IMvcProject;
        }
    }

    internal enum ViewType
    {
        CSHtmlHelper,
        VBHtmlHelper,
        TagHelper
    }
}
