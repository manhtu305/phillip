﻿using System;
using System.IO;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the class to serialize the component.
    /// </summary>
    public class ViewSerializer
    {
        /// <summary>
        /// Serialize the component.
        /// </summary>
        /// <param name="component">The component to be serialized.</param>
        /// <param name="writer">The writer.</param>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="setting">The setting.</param>
        public static void SerializeComponent(object component, TextWriter writer, IServiceProvider serviceProvider, ISetting setting)
        {
            var vw = GetViewWriter(serviceProvider, setting, writer);
            vw.WriteControl(component);
        }

        /// <summary>
        /// Serialize the component.
        /// </summary>
        /// <param name="component">The component to be serialized.</param>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="setting">The setting.</param>
        /// <returns>The text which the component is serialized into.</returns>
        public static string SerializeComponent(object component, IServiceProvider serviceProvider, ISetting setting)
        {
            using (var sw = new StringWriter())
            {
                SerializeComponent(component, sw, serviceProvider, setting);
                return sw.ToString();
            }
        }

        private static ViewWriter GetViewWriter(IServiceProvider serviceProvider, ISetting settings, TextWriter writer)
        {
            var vt = ViewUtility.GetCurrentViewType(serviceProvider);
            if(vt == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            switch (vt.Value)
            {
                case ViewType.CSHtmlHelper:
                    return new CSHtmlHelperWriter(new BaseContext(settings ?? new HtmlHelperSetting(), serviceProvider), writer);
                case ViewType.VBHtmlHelper:
                    return new VBHtmlHelperWriter(new BaseContext(settings ?? new HtmlHelperSetting(), serviceProvider), writer);
                case ViewType.TagHelper:
                    return new TagHelperWriter(new BaseContext(settings ?? new TagHelperSetting(), serviceProvider), writer);
            }

            throw new ArgumentOutOfRangeException();
        }

        /// <summary>
        /// Serialize the component.
        /// </summary>
        /// <param name="component">The component to be serialized.</param>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="setting">The setting.</param>
        /// <returns>The text which the component is serialized into.</returns>
        public static string UpdateComponent(object component, IServiceProvider serviceProvider, ISetting setting, MVCControl currentControlSetting)
        {
            using (var sw = new StringWriter())
            {
                if (currentControlSetting != null)
                {
                    var vw = GetViewWriter(serviceProvider, setting, sw, currentControlSetting.CodeType);
                    vw.WriteControl(component);
                }
                else
                {
                    SerializeComponent(component, sw, serviceProvider, setting);
                }
                return sw.ToString();
            }
        }

        private static ViewWriter GetViewWriter(IServiceProvider serviceProvider, ISetting settings, TextWriter writer, BlockCodeType codeType)
        {
            switch (codeType)
            {
                case BlockCodeType.CSharp:
                    return new CSHtmlHelperWriter(new BaseContext(settings ?? new HtmlHelperSetting(), serviceProvider), writer);
                case BlockCodeType.VB:
                    return new VBHtmlHelperWriter(new BaseContext(settings ?? new HtmlHelperSetting(), serviceProvider), writer);
                case BlockCodeType.HTMLNode:
                    return new TagHelperWriter(new BaseContext(settings ?? new TagHelperSetting(), serviceProvider), writer);
            }

            throw new ArgumentOutOfRangeException();
        }
    }
}
