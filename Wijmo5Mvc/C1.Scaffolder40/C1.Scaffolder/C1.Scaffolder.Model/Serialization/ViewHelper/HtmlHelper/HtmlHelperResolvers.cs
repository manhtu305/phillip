﻿using System;

namespace C1.Web.Mvc.Serialization
{
    internal class HtmlHelperClientEventResolver : ViewClientEventResolver
    {
        public override string ResolvePropertyName(string name, object value, Type type, IContext context)
        {
            return name;
        }
    }
}
