﻿using C1.Web.Mvc.Services;
using System.IO;
using System.CodeDom.Compiler;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Serializes an object to an htmlhelper in VB language.
    /// </summary>
    public sealed class VBHtmlHelperWriter : HtmlHelperWriter
    {
        #region Fields
        private const string SUB = "Sub";
        private const string END = "End";
        #endregion Fields

        #region Properties
        internal override string ItemSeperator
        {
            get
            {
                return string.Empty;
            }
        }

        internal override string TemplateFileSuffix
        {
            get
            {
                return ".vb" + base.TemplateFileSuffix;
            }
        }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Creates an instance of <see cref="VBHtmlHelperWriter"/>.
        /// </summary>
        /// <param name="context">The context object.</param>
        /// <param name="writer">The writer.</param>
        public VBHtmlHelperWriter(IContext context, TextWriter writer)
            : base(context, writer)
        {
        }
        #endregion Constructors

        #region Methods
        #region Control
        internal override string GetGenericType()
        {
            var currentDbContext = Context.ServiceProvider.GetService(typeof(IDefaultDbContextProvider)) as IDefaultDbContextProvider;
            return currentDbContext.ModelType == null
                ? string.Empty
                : string.Format("(Of {0})", currentDbContext.ModelType.TypeName);
        }
        #endregion Control

        #region Raw Values
        #region Simple
        internal override void WriteNull()
        {
            WriteRawText("Nothing", false);
        }
        internal override void WriteSimpleValue(char value)
        {
            WriteRawText(value.ToString());
        }

        internal override void WriteSimpleValue(bool value)
        {
            WriteRawText(value ? "True" : "False", false);
        }
        #endregion Simple

        #region Complex
        internal override void WriteComplexBuilderName(string name)
        {
            WriteRawText(string.Format("{0}({1}) {1}", SUB, name), false);
        }
        #endregion Complex

        #region Collection
        internal override void WriteCollectionPrefix()
        {
            var collectionBuilderName = CollectionBuilderNames.Peek();
            WriteRawText(string.Format("{0}({1})", SUB, collectionBuilderName), false);
            WriteViewLine();
        }

        internal override void WriteCollectionSuffix()
        {
            WriteRawText(END + " " + SUB, false);
        }
        #endregion Collection
        #endregion Raw Values

        #region Helpers
        /// <summary>
        /// Writes a new line.
        /// </summary>
        /// <param name="connected">A boolean value indicates whether to write a connected new line.</param>
        public override void WriteNewLine(bool connected = false)
        {
            if (connected)
            {
                Output.Write(" _");
            }

            Output.WriteLine();
            base.WriteNewLine(connected);
        }

        internal override CodeDomProvider GetCodeDomProvider()
        {
            return new Microsoft.VisualBasic.VBCodeProvider();
        }
        #endregion Helpers
        #endregion Methods
    }
}
