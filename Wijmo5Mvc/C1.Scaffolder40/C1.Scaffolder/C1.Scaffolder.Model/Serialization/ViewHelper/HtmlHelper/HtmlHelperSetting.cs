﻿namespace C1.Web.Mvc.Serialization
{
    internal class HtmlHelperSetting : ViewSetting
    {
        public HtmlHelperSetting()
        {
            Resolvers.Add(new HtmlHelperClientEventResolver());
        }
    }
}
