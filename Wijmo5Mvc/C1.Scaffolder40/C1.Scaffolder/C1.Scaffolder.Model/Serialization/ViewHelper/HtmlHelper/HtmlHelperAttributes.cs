﻿using System;

namespace C1.Web.Mvc.Serialization
{
    #region Attributes
    /// <summary>
    /// Defines the attribute to set the dictionary item builder name.
    /// For example, TemplateBind(key, value) in the htmlhelper is used 
    /// for the TemplateBindings property which type is <see cref="System.Collections.IDictionary"/>.
    /// In order to get the "TemplateBind" text, we need add the following attribute above the TemplateBindings property.
    ///     [C1HtmlHelperDictionaryEntryName("TemplateBind")]
    ///     public IDictionary&lt;string, string&gt; TemplateBindings
    ///     {
    ///         ...
    ///     }
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class)]
    public sealed class C1HtmlHelperDictionaryEntryNameAttribute : C1NameAttribute
    {
        /// <summary>
        /// Creates an instance of <see cref="C1HtmlHelperDictionaryEntryNameAttribute"/>.
        /// </summary>
        /// <param name="name">The specified name.</param>
        public C1HtmlHelperDictionaryEntryNameAttribute(string name) 
            : base(name)
        {
        }
    }

    /// <summary>
    /// Defines the attribute to set the builder name for some property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class C1HtmlHelperBuilderNameAttribute : C1NameAttribute
    {
        /// <summary>
        /// Creates an instance of <see cref="C1HtmlHelperBuilderNameAttribute"/>.
        /// </summary>
        /// <param name="name">The specified name.</param>
        public C1HtmlHelperBuilderNameAttribute(string name)
            : base(name)
        {
        }
    }

    /// <summary>
    /// Defines the attribute to set the converter which is used for writer and reader.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface | AttributeTargets.Enum | AttributeTargets.Parameter)]
    public sealed class C1HtmlHelperConverterAttribute : C1ConverterAttribute
    {
        /// <summary>
        /// Create an instance of <see cref="C1HtmlHelperConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        public C1HtmlHelperConverterAttribute(Type converterType)
            :base(converterType)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="C1ConverterAttribute"/> class.
        /// </summary>
        /// <param name="converterType">Type of the converter.</param>
        /// <param name="converterParameters">Parameter list to use when constructing the JsonConverter. Can be null.</param>
        public C1HtmlHelperConverterAttribute(Type converterType, params object[] converterParameters)
            :base(converterType, converterParameters)
        {
        }
    }

    /// <summary>
    /// Defines the attribute to set the collection item converter.
    /// </summary>
    public sealed class C1HtmlHelperCollectionItemConverterAttribute : C1CollectionItemConverterAttribute
    {
        /// <summary>
        /// Creates an instance of <see cref="C1HtmlHelperCollectionItemConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        public C1HtmlHelperCollectionItemConverterAttribute(Type converterType)
            :base(converterType)
        {
        }

        /// <summary>
        /// Creates an instance of <see cref="C1HtmlHelperCollectionItemConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        /// <param name="converterParameters">The parameters used to create the converter.</param>
        public C1HtmlHelperCollectionItemConverterAttribute(Type converterType, params object[] converterParameters)
            :base(converterType, converterParameters)
        {
        }
    }
    #endregion Attributes

    #region Helper
    internal sealed class HtmlHelperAttributeHelper : AttributeHelper
    {
        public override C1CollectionItemConverterAttribute GetCollectionItemConverterAttribute(object memberInfo, object collection)
        {
            return Utility.GetAttribute<C1HtmlHelperCollectionItemConverterAttribute>(memberInfo, collection);
        }

        public override C1ConverterAttribute GetConverterAttribute(object memberInfo, object value)
        {
            return Utility.GetAttribute<C1HtmlHelperConverterAttribute>(memberInfo, value);
        }

        public override C1NameAttribute GetNameAttribute(object memberInfo, object value)
        {
            return Utility.GetAttribute<C1HtmlHelperBuilderNameAttribute>(memberInfo, value);
        }
    }
    #endregion Helper
}
