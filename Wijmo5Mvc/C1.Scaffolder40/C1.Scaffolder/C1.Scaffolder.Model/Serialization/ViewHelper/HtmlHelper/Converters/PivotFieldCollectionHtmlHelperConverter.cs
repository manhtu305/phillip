﻿using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Serialization
{
    internal sealed class PivotFieldCollectionHtmlHelperConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            PivotFieldCollection fieldCollection = value as PivotFieldCollection;
            if (value == null)
            {
                return;
            }

            int count = fieldCollection.Items.Count;
            for (int i = 0; i < count; i++)
            {
                writer.WriteText(string.Format("\"{0}\"", fieldCollection.Items[i].Binding));
                if (i != count - 1)
                {
                    writer.WriteText(", ");
                }
            }
        }
    }
}
