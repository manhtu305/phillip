﻿namespace C1.Web.Mvc.Serialization
{
    internal sealed class StringArrayHtmlHelperConverter : StringArrayConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var arrStr = GetStringArrayCodeSnippets((string[])value);
            writer.WriteText(arrStr);
        }
    }
}
