﻿namespace C1.Web.Mvc.Serialization
{
    internal sealed class AnnotationLayerHtmlHelperConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var annotationLayer = value as AnnotationLayer<object>;
            var hhw = writer as HtmlHelperWriter;
            if (annotationLayer != null && hhw != null)
            {
                var builderName = hhw.GetUniqueBuilderName("alb");
                hhw.CollectionBuilderNames.Push(builderName);
                hhw.WriteCollectionPrefix();
                foreach (var annotation in annotationLayer.Items)
                {
                    var annotationName = ViewUtility.GetNameWithoutGeneric(annotation.GetType().Name);
                    writer.WriteText(string.Format("{0}.Add{1}", builderName, annotationName));
                    writer.WriteText("(");
                    writer.WriteMemberValue(annotation);
                    writer.WriteText(")");
                    writer.WriteRawText(hhw.ItemSeperator, false);
                    writer.WriteViewLine();
                }
                hhw.WriteCollectionSuffix();
                hhw.CollectionBuilderNames.Pop();
            }
        }
    }
}
