﻿namespace C1.Web.Mvc.Serialization
{
    internal sealed class SeriesItemHtmlHelperConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            ChartIItemsSourceBindingHolderConverter.ConvertBindingHolder(writer, value, context);

            var itemName = (value is ChartSeries<object> || value is FlexRadarSeries<object>) ? "" : ViewUtility.GetNameWithoutGeneric(value.GetType().Name);
            var hw = writer as HtmlHelperWriter;
            hw.WriteCollectionItem(value, itemName);
        }
    }
}
