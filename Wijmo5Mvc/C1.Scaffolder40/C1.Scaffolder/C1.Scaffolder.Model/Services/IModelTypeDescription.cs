﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the description of a model type.
    /// </summary>
    public interface IModelTypeDescription
    {
        /// <summary>
        /// Gets the full name of the type.
        /// </summary>
        string TypeName { get; }
        /// <summary>
        /// Gets the short name of the type.
        /// </summary>
        string ShortTypeName { get; }
        /// <summary>
        /// Ges the display name of the type.
        /// </summary>
        string DisplayName { get; }
        /// <summary>
        /// Gets the collection of properties in the type.
        /// </summary>
        IList<PropertyDescription> Properties { get; }
    }
}
