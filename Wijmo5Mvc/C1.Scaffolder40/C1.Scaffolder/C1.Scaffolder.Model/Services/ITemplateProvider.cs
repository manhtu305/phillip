﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Services
{
    public interface ITemplateProvider
    {
        /// <summary>
        /// Gets the template path for specified path.
        /// </summary>
        /// <param name="path">The relative path of the template file.</param>s
        /// <returns>The full path of the template file if existing.</returns>
        string Get(string path);

        /// <summary>
        /// Transform the template file.
        /// </summary>
        /// <param name="templatePath">The full path of the template file.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>The transformed content.</returns>
        string TransformTemplate(string templatePath, Dictionary<string, object> parameters = null);
    }
}
