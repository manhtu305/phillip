﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the service which provides information of database context types and model types.
    /// </summary>
    public interface IDbContextProvider
    {
        /// <summary>
        /// Gets all available database context types.
        /// </summary>
        IList<IModelTypeDescription> DbContextTypes { get; }
        /// <summary>
        /// Gets all available model types.
        /// </summary>
        IList<IModelTypeDescription> ModelTypes { get; }
        /// <summary>
        /// Gets the entity set name for the specified database context and model type.
        /// </summary>
        /// <param name="dbContext">The database context type.</param>
        /// <param name="modelType">The model type.</param>
        /// <returns>The name of the entity set.</returns>
        string GetEntitySetName(IModelTypeDescription dbContext, IModelTypeDescription modelType);
    }

    /// <summary>
    /// Represents the service which provides information of default database context type and model type.
    /// </summary>
    public interface IDefaultDbContextProvider : IBindingHolder
    {
        /// <summary>
        /// Gets all primary keys in the model type.
        /// </summary>
        IDictionary<string, string> PrimaryKeys { get; }
    }

    /// <summary>
    /// Represents the holder which can select individual database context type.
    /// </summary>
    public interface IDbContextHolder
    {
        /// <summary>
        /// Gets or sets the the database context type.
        /// </summary>
        IModelTypeDescription DbContextType { get; set; }
    }

    /// <summary>
    /// Represents the holder which can select individual model type.
    /// </summary>
    public interface IModelTypeHolder
    {
        /// <summary>
        /// Gets or sets the model type.
        /// </summary>
        IModelTypeDescription ModelType { get; set; }
    }

    /// <summary>
    /// Represents the holder which can select individual database context annd model type.
    /// </summary>
    public interface IBindingHolder : IDbContextHolder, IModelTypeHolder
    {
    }
}
