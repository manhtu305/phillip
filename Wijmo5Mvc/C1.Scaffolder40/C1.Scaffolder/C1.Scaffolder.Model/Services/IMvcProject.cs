﻿using System;
namespace C1.Web.Mvc.Services
{
    /// <summary>
    /// Represents the information of MVC project.
    /// </summary>
    public interface IMvcProject
    {
        /// <summary>
        /// Gets a value indicating whether it's an ASP.NET Core project.
        /// </summary>
        bool IsAspNetCore { get; }
        /// <summary>
        /// Gets a value indicating whether it's a Razor Pages project.
        /// </summary>
        bool IsRazorPages { get; }
        /// <summary>
        /// Gets a value indicating whether it's a CSharp project.
        /// </summary>
        bool IsCs { get; }
        /// <summary>
        /// Gets the version of Microsoft.EntityFrameworkCore in AspNetCore project.
        /// </summary>
        Version EntityFrameworkCoreVersion { get; }
    }
}
