﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Services
{
    /// <summary>
    /// Represents the snippets collection for view.
    /// </summary>
    public interface IViewSnippets
    {
        /// <summary>
        /// Adds a client script snippet.
        /// </summary>
        /// <param name="content">The content of the script.</param>
        void AddScript(string content);
        /// <summary>
        /// Adds a client function script snippet.
        /// </summary>
        /// <param name="name">The name of the function.</param>
        /// <param name="body">The body content of the function.</param>
        /// <param name="parameters">The parameters list of the function.</param>
        void AddFunctionScript(string name, string body, params string[] parameters);
        /// <summary>
        /// Adds a client event function script snippet.
        /// </summary>
        /// <param name="eventName">The name of the function.</param>
        void AddClientEventScript(string eventName);
        /// <summary>
        /// Adds a server code block snippet.
        /// </summary>
        /// <param name="code">The content of the code.</param>
        void AddCode(string code);
        /// <summary>
        /// Adds a html content snippet.
        /// </summary>
        /// <param name="content">The html content.</param>
        void AddHtml(string content);
        /// <summary>
        /// Adds a component html snippet.
        /// </summary>
        /// <param name="content">The html content for the component.</param>
        void AddComponent(string content);
        /// <summary>
        /// Adds an import snippet.
        /// </summary>
        /// <param name="ns">The namespace to import.</param>
        void AddImport(string ns);
        /// <summary>
        /// Adds an import tag snippet.
        /// </summary>
        /// <param name="tag">The tag to import.</param>
        void AddImportTag(string tag);
        /// <summary>
        /// Adds an init script to register.
        /// </summary>
        /// <param name="script">The tag to import.</param>
        void AddResourceScript(string script);
        /// <summary>
        /// Adds a package to register.
        /// </summary>
        /// <param name="script">The tag to import.</param>
        void AddPackage(string package);
        /// <summary>
        /// Gets an unique variable name which is used in builder.
        /// </summary>
        /// <param name="names">The list of existing names.</param>
        /// <param name="defaultName">The default name.</param>
        /// <returns>The unique variable name.</returns>
        string GetBuilderVariableName(IEnumerable<string> names, string defaultName);
        /// <summary>
        /// Gets an unique member name.
        /// </summary>
        /// <param name="defaultName">The default name.</param>
        /// <returns>The unique member name.</returns>
        string GetMemberName(string defaultName);

    }
}
