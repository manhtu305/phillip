﻿using C1.Web.Mvc.Services;
using System;
using System.ComponentModel.Design;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the manager to get related services.
    /// </summary>
    public class ServiceManager : ServiceContainer
    {
        private static ServiceManager _instance = new ServiceManager(null);

        private ServiceManager(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Gets the current instance.
        /// </summary>
        public static ServiceManager Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Updates the current instance with specified service provider.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        public static void UpdateInstance(IServiceProvider serviceProvider)
        {
            _instance = new ServiceManager(serviceProvider);
        }

        /// <summary>
        /// Gets the service of type <see cref="IDbContextProvider"/>.
        /// </summary>
        public static IDbContextProvider DbContextProvider
        {
            get { return _instance.GetService(typeof (IDbContextProvider)) as IDbContextProvider; }
        }

        /// <summary>
        /// Gets the service of type <see cref="IDefaultDbContextProvider"/>.
        /// </summary>
        public static IDefaultDbContextProvider DefaultDbContextProvider
        {
            get { return _instance.GetService(typeof (IDefaultDbContextProvider)) as IDefaultDbContextProvider; }
        }

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> value which indicates whether it's in design time.
        /// </summary>
        public static bool IsDesignTime
        {
            get
            {
                var designHelper = _instance.GetService(typeof(IDesignHelper)) as IDesignHelper;
                return designHelper != null && designHelper.IsDesignTime;
            }
        }
    }
}
