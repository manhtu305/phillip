﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// The description of property.
    /// </summary>
    public class PropertyDescription
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type name.
        /// </summary>
        public string TypeName { get; set; }
    }
}
