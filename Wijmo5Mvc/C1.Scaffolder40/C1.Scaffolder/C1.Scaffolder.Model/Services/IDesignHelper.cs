﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Services
{
    /// <summary>
    /// Represents the helper for designer.
    /// </summary>
    public interface IDesignHelper
    {
        /// <summary>
        /// Gets a value indicates whether it's in design time.
        /// </summary>
        bool IsDesignTime { get; }
    }
}
