﻿using System.Collections.Generic;
namespace C1.Web.Mvc.Services
{
    /// <summary>
    /// Represents the scaffolder info.
    /// </summary>
    public interface IScaffolderDescriptor
    {
        /// <summary>
        /// Gets a value indicating whether the scaffolder is for insert command.
        /// </summary>
        bool IsInsert { get; }
        bool ShouldSerializeGenericParameter { get; }
        Dictionary<string, object> TemplateParameters { get; }
        /// <summary>
        /// Gets a value indicating whether the scaffolder is for update command.
        /// </summary>
        bool IsUpdate { get; }
    }
}
