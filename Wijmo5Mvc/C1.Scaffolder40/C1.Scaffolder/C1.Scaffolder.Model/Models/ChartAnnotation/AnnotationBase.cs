﻿using System.ComponentModel;
using C1.Web.Mvc.Serialization;
namespace C1.Web.Mvc
{
    partial class AnnotationBase
    {
        [C1Description("AnnotationBase_Point")]
        [DisplayName("Point")]
        [C1Ignore]
        public DataPoint DesignPoint
        {
            get
            {
                return Point ?? (Point = new DataPoint());
            }
        }

        /// <summary>
        /// Specifies whether the DesignPoint property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializePoint()
        {
            return Point != null && Point.X != null && Point.Y != null;
        }
    }
}
