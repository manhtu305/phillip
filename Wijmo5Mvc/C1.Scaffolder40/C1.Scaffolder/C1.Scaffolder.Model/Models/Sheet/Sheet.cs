﻿using C1.Web.Mvc.Serialization;
using System.ComponentModel;

namespace C1.Web.Mvc.Sheet
{
    partial class Sheet
    {
        /// <summary>
        /// Gets a value indicating whether the sheet is in bound mode.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        public abstract bool IsBound { get; }
    }
}
