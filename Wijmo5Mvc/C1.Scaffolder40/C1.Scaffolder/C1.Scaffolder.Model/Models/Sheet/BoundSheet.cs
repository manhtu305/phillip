﻿using C1.Web.Mvc.Converters;
using C1.Web.Mvc.Serialization;
using System.ComponentModel;
namespace C1.Web.Mvc.Sheet
{
    partial class BoundSheet<T> : IBindingHolder
    {
        /// <summary>
        /// Gets a value indicating whether the control is in bound mode.
        /// </summary>
        public override bool IsBound { get { return true; } }

        /// <summary>
        /// Gets and sets the source of DataMap.
        /// </summary>
        [DisplayName("ModelClass")]
        [C1Description("BoundSheet_ModelType")]
        [TypeConverter(typeof(ModelTypeConverter))]
        [C1Ignore]
        public IModelTypeDescription ModelType { get; set; }

        /// <summary>
        /// Gets and sets the source of DataMap.
        /// </summary>
        [DisplayName("DataContext")]
        [C1Description("BoundSheet_DbContextType")]
        [TypeConverter(typeof(DbContextTypeConverter))]
        [C1Ignore]
        public IModelTypeDescription DbContextType { get; set; }
    }
}
