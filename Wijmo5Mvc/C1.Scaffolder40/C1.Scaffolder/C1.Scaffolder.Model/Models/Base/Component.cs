﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc
{
    partial class Component
    {
        public virtual void InitControl()
        {
        }

        private List<ClientEvent> _clientEvents;
        /// <summary>
        /// Gets the collection of client events which are visible in the wizard.
        /// </summary>
        [C1Ignore]
        public List<ClientEvent> ClientEvents { get { return _clientEvents ?? (_clientEvents = CreateClientEvents()); }}

        /// <summary>
        /// Gets a value indicates whether should generate script tag.
        /// </summary>
        [C1Ignore]
        public virtual bool GenerateScriptTag { get { return ClientEvents.Any(ce => ce.Handled); } }

        /// <summary>
        /// Creates the collection of client events which are visible in the wizard.
        /// </summary>
        /// <returns></returns>
        protected virtual List<ClientEvent> CreateClientEvents()
        {
            return new List<ClientEvent>();
        }

        /// <summary>
        /// Occurs when the binding info is changed.
        /// </summary>
        public event EventHandler BindingInfoChanged;

        /// <summary>
        /// Raises the <see cref="BindingInfoChanged"/> event.
        /// </summary>
        protected virtual void OnBindingInfoChanged()
        {
            if (BindingInfoChanged != null)
            {
                BindingInfoChanged(this, new EventArgs());
            }
        }

        /// <summary>
        /// Invoked before serialize this component.
        /// </summary>
        public virtual void BeforeSerialize()
        {
            ApplyClientEvents();
        }

        private void ApplyClientEvents()
        {
            foreach (var clientEvent in ClientEvents)
            {
                if (clientEvent.Handled)
                {
                    var eventName = GetClientEventName(clientEvent.DisplayName);
                    var prop = GetType().GetProperty(clientEvent.Name);
                    prop.SetValue(this, eventName);
                }
            }
        }

        protected string GetClientEventName(string displayName)
        {
            var eventName = (Id ?? "") + "_" + displayName;
            return char.ToLower(eventName[0]) + eventName.Substring(1);
        }
    }
}
