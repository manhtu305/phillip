﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines the IC1Property interface.
    /// </summary>
    public interface IC1Property
    {
        /// <summary>
        /// Gets or sets the c1-property attribute for taghelper.
        /// </summary>
        string C1Property { get; set; }
    }
}
