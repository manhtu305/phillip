﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Olap
{
    public partial class PivotGrid : ISupportUpdater
    {
        [C1Ignore]
        [Browsable(false)]
        public MVCControl ParsedSetting { get; set; }

        [C1HtmlHelperBuilderName("ItemsSourceId")]
        [C1TagHelperName("ItemsSourceId")]
        public string PivotEngineId
        {
            get { return ItemsSourceId; }
            set { ItemsSourceId = value; }
        }
    }
}
