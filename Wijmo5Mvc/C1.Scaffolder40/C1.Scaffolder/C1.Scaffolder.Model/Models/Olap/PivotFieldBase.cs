﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Olap
{
    public partial class PivotFieldBase
    {
        [C1Ignore]
        [Browsable(false)]
        public string KeyWrapper
        {
            get { return Key; }
            set { Key = value; }
        }
    }
}
