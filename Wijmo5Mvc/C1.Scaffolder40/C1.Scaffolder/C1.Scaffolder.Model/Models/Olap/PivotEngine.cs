﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Olap
{
    public partial class PivotEngine : ISupportUpdater, IBindingHolder
    {
        [C1Ignore]
        [Browsable(false)]
        public MVCControl ParsedSetting { get; set; }

        [C1Ignore]
        [Browsable(false)]
        public IEnumerable<PivotFieldCollection> FieldsCollections
        {
            get
            {
                return new List<PivotFieldCollection>
                {
                    RowFields, ColumnFields, ValueFields, FilterFields
                };
            }
        }

        [IgnoreTemplateParameter]
        [C1Ignore]
        public IModelTypeDescription DbContextType
        {
            get
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                return dbService == null ? null : dbService.DbContextType;
            }
            set
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                if (dbService != null && dbService.DbContextType != value)
                {
                    dbService.DbContextType = value;
                    OnBindingInfoChanged();
                }
            }
        }

        /// <summary>
        /// Wrap ItemSource.ReadActionUrl for firing binding changed event.
        /// </summary>
        [IgnoreTemplateParameter]
        [C1Ignore]
        public string ReadActionUrl
        {
            get
            {
                return ItemsSource.ReadActionUrl;
            }
            set
            {
                ItemsSource.ReadActionUrl = value;
                OnBindingInfoChanged();
            }
        }

        /// <summary>
        /// Gets the selected model type.
        /// </summary>
        [IgnoreTemplateParameter]
        [C1Ignore]
        public virtual IModelTypeDescription ModelType
        {
            get
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                return dbService == null ? null : dbService.ModelType;
            }
            set
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                if (dbService != null && dbService.ModelType != value)
                {
                    dbService.ModelType = value;
                }
            }
        }

        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[] {
                "OnClientError",
                "OnClientItemsSourceChanged",
                "OnClientUpdatingView",
                "OnClientUpdatedView",
                "OnClientViewDefinitionChanged"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeServiceUrl()
        {
            return _itemsSource == null &&
                string.IsNullOrEmpty(_itemsSourceId) &&
                !string.IsNullOrEmpty(_serviceUrl);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeRowFields()
        {
            return RowFields != null && RowFields.ShouldSerializeItems();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeColumnFields()
        {
            return ColumnFields != null && ColumnFields.ShouldSerializeItems();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeValueFields()
        {
            return ValueFields != null && ValueFields.ShouldSerializeItems();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeFilterFields()
        {
            return FilterFields != null && FilterFields.ShouldSerializeItems();
        }
    }
}
