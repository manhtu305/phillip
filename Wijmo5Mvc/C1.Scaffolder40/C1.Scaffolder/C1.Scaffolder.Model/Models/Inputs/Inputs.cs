﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the model for Input scaffolder.
    /// </summary>
    public class Inputs : ItemsBoundControl<object>
    {
        public override void InitControl()
        {
            base.InitControl();
            Fields = new List<Field>();
            AutoGenerateFields = true;
        }

        private bool _autoGenerateFields;

        /// <summary>
        /// Gets or sets whether should auto generate the fields in design time.
        /// </summary>
        public bool AutoGenerateFields
        {
            get
            {
                return _autoGenerateFields;
            }
            set
            {
                if (_autoGenerateFields != value)
                {
                    _autoGenerateFields = value;
                    if (value)
                    {
                        ResetFields();
                    }
                }
            }
        }

        /// <summary>
        /// Gets the collection of fields.
        /// </summary>
        public List<Field> Fields { get; private set; }

        private void ResetFields()
        {
            Fields = new List<Field>();

            if (SelectedModelProperties == null) return;

            var primaryKeys = PrimaryKeys ?? new Dictionary<string, string>();
            foreach (var p in SelectedModelProperties)
            {
                var field = new Field {Binding = p.Name, IsReadOnly = primaryKeys.ContainsKey(p.Name)};
                Fields.Add(field);
            }
        }

        protected override void OnSelectedDbContextTypeChanged(EventArgs args)
        {
            base.OnSelectedDbContextTypeChanged(args);
            AutoGenerateFields = true;
            ResetFields();
        }

        protected override void OnSelectedModelTypeChanged(EventArgs args)
        {
            base.OnSelectedModelTypeChanged(args);
            AutoGenerateFields = true;
            ResetFields();
        }
    }
}
