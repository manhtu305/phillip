﻿using System;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents client event settings.
    /// </summary>
    public class ClientEvent
    {
        /// <summary>
        /// Initializes a new instance of <see cref="ClientEvent"/> object.
        /// </summary>
        /// <param name="name"></param>
        public ClientEvent(string name)
        {
            Name = name ?? string.Empty;
            DisplayName = Name.Replace("OnClient", String.Empty);
        }

        /// <summary>
        /// Gets the display name of the client event.
        /// </summary>
        public string DisplayName { get; private set; }

        /// <summary>
        /// Gets the name of the client event.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the client event is handed or not.
        /// </summary>
        public bool Handled { get; set; }
    }
}
