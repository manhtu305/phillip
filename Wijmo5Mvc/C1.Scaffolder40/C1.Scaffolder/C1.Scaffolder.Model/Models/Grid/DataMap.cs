﻿using C1.Web.Mvc.Converters;
using C1.Web.Mvc.Serialization;
using System.ComponentModel;
using C1.Web.Mvc.Services;

namespace C1.Web.Mvc
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    partial class DataMap : IModelTypeHolder
    {
        private string _entitySetName;

        /// <summary>
        /// Gets or sets a value indicating whether the data map is bound with remote data.
        /// </summary>
        [DefaultValue(false)]
        [C1Ignore]
        [C1Description("DataMap_UseRemoteBinding")]
        public bool UseRemoteBinding
        {
            get; set;
        }

        /// <summary>
        /// Gets a value indicating whether the data map has valid settings.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool IsValid
        {
            get
            {
                return (ShouldSerializeItemsSource()
                    && !string.IsNullOrEmpty(DisplayMemberPath)
                    && !string.IsNullOrEmpty(SelectedValuePath));
            }
        }

        /// <summary>
        /// Gets and sets the source of DataMap.
        /// </summary>
        [TypeConverter(typeof(ModelTypeConverter))]
        [DisplayName("Source")]
        [C1Ignore]
        [C1Description("DataMap_ModelType")]
        public IModelTypeDescription ModelType { get; set; }

        private bool ShouldSerializeItemsSource_Scaffolder()
        {
            if (ServiceManager.IsDesignTime)
            {
                // Get EntitySetName frequently brings bad performance
                return ModelType != null;
            }

            return !string.IsNullOrEmpty(EntitySetName);
        }

        /// <summary>
        /// For Inputs scaffolder. Gets the data name which is used to add to ViewBag.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        public string ViewBagName
        {
            get
            {
                return "DataMap_" + EntitySetName + "_" + DisplayMemberPath + "_" + SelectedValuePath;
            }
        }

        /// <summary>
        /// Gets the entity set name.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        public string EntitySetName
        {
            get
            {
                return _entitySetName ?? (_entitySetName = ServiceManager.DbContextProvider.GetEntitySetName(ServiceManager.DefaultDbContextProvider.DbContextType, ModelType));
            }
        }

        /// <summary>
        /// Gets the read action url. For Inputs scaffolder.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        public string ReadActionUrl
        {
            get
            {
                return "DataMap_" + EntitySetName + "_" + DisplayMemberPath + "_" + SelectedValuePath + "_Read";
            }
        }
    }
}


