﻿using C1.Web.Mvc.MultiRow;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Indicates the kind of the Grid control.
    /// </summary>
    public enum GridControlType
    {
        /// <summary>
        /// It's a <see cref="FlexGrid{T}"/> control.
        /// </summary>
        Grid,
        /// <summary>
        /// It's a <see cref="MultiRow{T}"/> control.
        /// </summary>
        MultiRow
    }
}
