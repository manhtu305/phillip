﻿using C1.Web.Mvc.Serialization;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System;

namespace C1.Web.Mvc
{
    partial class FlexGridBase<T> : ISupportUpdater
    {
        private C1Point _c1ScrollPosition;

        /// <summary>
        /// Gets the scroll position of type <see cref="C1Point"/> for design usage.
        /// </summary>
        [DisplayName("ScrollPosition")]
        [C1Ignore]
        [IgnoreTemplateParameter]
        public C1Point DesignScrollPosition
        {
            get
            {
                if (_c1ScrollPosition == null)
                {
                    _c1ScrollPosition = new C1Point { X = ScrollPosition.X, Y = ScrollPosition.Y };

                    _c1ScrollPosition.PropertyChanged += (s, e) =>
                    {
                        ScrollPosition = new Point(_c1ScrollPosition.X, _c1ScrollPosition.Y);
                    };
                }

                return _c1ScrollPosition;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the collection view validation is enabled.
        /// </summary>
        [C1Ignore]
        public bool EnableValidation { get; set; }

        /// <summary>
        /// Gets the kind the grid control.
        /// </summary>
        [C1Ignore]
        public virtual GridControlType ControlType
        {
            get { return GridControlType.Grid; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the paging feature of collection view is enabled.
        /// </summary>
        [C1Ignore]
        public bool AllowPaging { get; set; }

        /// <summary>
        /// Gets a value indicates whether the collection view should use complex binding.
        /// </summary>
        [C1Ignore]
        public bool UseComplexBind
        {
            get
            {
                return
                    AllowAddNew ||
                    !IsReadOnly ||
                    AllowDelete ||
                    !string.IsNullOrEmpty(ItemsSource.BatchEditActionUrl) && ItemsSource.BatchEdit ||
                    ItemsSource.DisableServerRead ||
                    ItemsSource.InitialItemsCount.HasValue ||
                    ItemsSource.PageIndex > 0 ||
                    ItemsSource.PageSize > 0 ||
                    DesignGroupDescriptions.Any(g => g.Enabled) ||
                    DesignSortDescriptions.Any(s => s.Enabled) ||
                    EnableValidation;
            }
        }

        private ObservableCollection<DesignPropertyGroupDescription> _designGroupDescriptions;
        /// <summary>
        /// Gets the collection of group descriptions.
        /// </summary>
        [C1Ignore]
        public ObservableCollection<DesignPropertyGroupDescription> DesignGroupDescriptions { get { return _designGroupDescriptions ?? (_designGroupDescriptions = new ObservableCollection<DesignPropertyGroupDescription>()); } }

        /// <summary>
        /// Gets or sets a value indicating whether the group panel is visibled.
        /// </summary>
        [C1Ignore]
        public bool ShowGroupPanel { get; set; }
        private FlexGridGroupPanel<T> _groupPanel;
        /// <summary>
        /// Gets the group panel extension assicated with the grid.
        /// </summary>
        [C1HtmlHelperBuilderNameAttribute("ShowGroupPanel")]
        [C1TagHelperIsAttribute(false)]
        public FlexGridGroupPanel<T> GroupPanel { get { return _groupPanel ?? (_groupPanel = new FlexGridGroupPanel<T>(this)); } }

        /// <summary>
        /// Specifies whether the GroupPanel property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeGroupPanel()
        {
            return ShowGroupPanel;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filter estension is enabled.
        /// </summary>
        [C1Ignore]
        public bool AllowFiltering { get; set; }
        private FlexGridFilter<T> _filter;
        /// <summary>
        /// Gets the filter extension assciated with the grid..
        /// </summary>
        [C1HtmlHelperBuilderNameAttribute("Filterable")]
        [C1TagHelperIsAttribute(false)]
        public virtual FlexGridFilter<T> FilterExtension
        {
            get { return _filter ?? (_filter = new FlexGridFilter<T>(this)); }
        }

        /// <summary>
        /// Specifies whether the Filter property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeFilterExtension()
        {
            return AllowFiltering;
        }

        private ObservableCollection<DesignSortDescription> _designSortDescriptions;
        /// <summary>
        /// Gets the collection of sort descriptions.
        /// </summary>
        [C1Ignore]
        public ObservableCollection<DesignSortDescription> DesignSortDescriptions { get { return _designSortDescriptions ?? (_designSortDescriptions = new ObservableCollection<DesignSortDescription>()); } }

        private bool _designAutoGenerateColumns;
        /// <summary>
        /// Gets or sets a value indicating whether the columns should be auto generated in design time.
        /// </summary>
        [C1Ignore]
        [IgnoreTemplateParameter]
        public virtual bool DesignAutoGenerateColumns
        {
            get
            {
                return _designAutoGenerateColumns;
            }
            set
            {
                var oldValue = _designAutoGenerateColumns;
                _designAutoGenerateColumns = value;
                if (value) AutoGenerateColumns = true;
                if (value && (!oldValue || IsDesignColumnsEmpty))
                {
                    FillDesignColumns();
                }
            }
        }
        List<ColumnBase> designColumns;
        /// <summary>
        /// Gets the collection for columns for design usage.
        /// </summary>
        [C1Ignore]
        [IgnoreTemplateParameter]
        public virtual List<ColumnBase> DesignColumns
        {
            get
            {
                if (designColumns == null) designColumns = new List<ColumnBase>();
                return designColumns;
            }
            private set { }
        }

        /// <summary>
        /// Gets a value indicating whether the columns in design time is empty.
        /// </summary>
        internal virtual bool IsDesignColumnsEmpty
        {
            get
            {
                return DesignColumns != null && !DesignColumns.Any();
            }
        }

        /// <summary>
        /// Clear the columns at design time.
        /// </summary>
        internal virtual void ClearDesignColumns()
        {
            DesignColumns.Clear();
        }

        /// <summary>
        /// Fills the columns at design time.
        /// </summary>
        internal virtual void FillDesignColumns(bool forUpdate = false)
        {
            ClearDesignColumns();
            if (forUpdate)
            {
                if (!AutoGenerateColumns)
                {
                    DesignGroupDescriptions.Clear();
                    DesignSortDescriptions.Clear();
                    DesignAutoGenerateColumns = false;
                    foreach (var column in Columns)
                    {
                        DesignColumns.Add(column);
                        DesignGroupDescriptions.Add(new DesignPropertyGroupDescription { PropertyName = column.Binding });
                        DesignSortDescriptions.Add(new DesignSortDescription { Property = column.Binding, Ascending = true });
                    }
                    Columns.Clear();
                }
            }
            else
            {
                if (SelectedModelProperties == null)
                {
                    return;
                }

                foreach (var property in SelectedModelProperties)
                {
                    var column = new Column { Binding = property.Name };
                    DesignColumns.Add(column);
                }
            }
        }

        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[] {
                "OnClientBeginningEdit",
                "OnClientCellEditEnded",
                "OnClientCellEditEnding",
                "OnClientDeletedRow",
                "OnClientDeletingRow",
                "OnClientFormatItem",
                "OnClientPrepareCellForEdit",
                "OnClientRowAdded",
                "OnClientRowEditEnded",
                "OnClientRowEditEnding",
                "OnClientSelectionChanged",
                "OnClientSelectionChanging",
                "OnClientRowEditStarted",
                "OnClientRowEditStarting"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }

        protected override void OnSelectedModelTypeChanged(System.EventArgs args)
        {
            base.OnSelectedModelTypeChanged(args);

            DesignAutoGenerateColumns = AutoGenerateColumns;

            FillDesignColumns();
            InitializeGroupDescriptions();
            InitializeSortDescriptions();
        }

        private void InitializeSortDescriptions()
        {
            DesignSortDescriptions.Clear();
            if (SelectedModelProperties != null)
            {
                SelectedModelProperties.ToList().ForEach(p =>
                    DesignSortDescriptions.Add(new DesignSortDescription { Property = p.Name, Ascending = true }));
            }
        }

        private void InitializeGroupDescriptions()
        {
            DesignGroupDescriptions.Clear();
            if (SelectedModelProperties != null)
            {
                SelectedModelProperties.ToList().ForEach(p =>
                    DesignGroupDescriptions.Add(new DesignPropertyGroupDescription { PropertyName = p.Name }));
            }
        }

        internal virtual bool ShouldApplyColumns
        {
            get { return DesignColumns != null; }
        }

        public override void BeforeSerialize()
        {
            base.BeforeSerialize();

            if (AllowFiltering && DesignColumns != null)
            {
                foreach (var col in DesignColumns)
                {
                    if (col.FilterType != FilterType.None)
                    {
                        FilterExtension.ColumnFilters.Add(new ColumnFilter { Column = col.Binding, FilterType = col.FilterType });
                    }
                }

                Extenders.Add(FilterExtension);
            }

            if (ShowGroupPanel)
            {
                Extenders.Add(GroupPanel);
            }

            if (!DesignAutoGenerateColumns && ShouldApplyColumns)
            {
                AutoGenerateColumns = false;
                foreach (var column in DesignColumns)
                {
                    Columns.Add((Column)column);//to keep changed in DesignColumns
                }
            }
            else if (DesignAutoGenerateColumns)
            {
                AutoGenerateColumns = true;
                Columns.Clear();
            }

            if (!AllowPaging)
            {
                ItemsSource.PageIndex = 0;
                ItemsSource.PageSize = 0;
            }

            foreach (var sortDescription in DesignSortDescriptions)
            {
                if (sortDescription.Enabled)
                {
                    ItemsSource.SortDescriptions.Add(new SortDescription
                    {
                        Property = sortDescription.Property,
                        Ascending = sortDescription.Ascending
                    });
                }
            }

            foreach (var groupDescription in DesignGroupDescriptions)
            {
                if (groupDescription.Enabled)
                {
                    ItemsSource.GroupDescriptions.Add(new PropertyGroupDescription
                    {
                        PropertyName = groupDescription.PropertyName
                    });
                }
            }

            ApplyActionUrls();
        }

        protected void ApplyActionUrls()
        {
            //ODataCollectionView/ODataVirtualCollectionView skips this.
            if (BaseODataSource != null)
            {
                return;
            }

            var id = Id;
            if (!ItemsSource.BatchEdit)
            {
                if (AllowAddNew)
                {
                    ItemsSource.CreateActionUrl = id + "_Create";
                }

                if (!IsReadOnly)
                {
                    ItemsSource.UpdateActionUrl = id + "_Update";
                }

                if (AllowDelete)
                {
                    ItemsSource.DeleteActionUrl = id + "_Delete";
                }
            }
            else
            {
                ItemsSource.BatchEditActionUrl = id + "_BatchEdit";
            }

            ItemsSource.GetError = EnableValidation ? id + "_GetError" : null;
        }

        /// <summary>
        /// Specifies whether the ImeEnabled property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeImeEnabled()
        {
            return !IsReadOnly && ImeEnabled;
        }

        /// <summary>
        /// Specifies whether ReadActionUrl property should be serialized
        /// </summary>
        /// <returns>
        /// 
        /// </returns>
        internal override bool _ShouldSerializeItemsSource()
        {
            if (!string.IsNullOrEmpty(ItemsSource.ReadActionUrl))
            {
                return true;
            }

            //if ItemsSource = ODataSource, do not serialize with name = itemssources
            //let the scaffolder serialize with name = BindODataSource || BindODataVirtualSource
            if (IsODataSourceMode)
            {
                return false;
            }

            return base._ShouldSerializeItemsSource();
        }

        [C1HtmlHelperBuilderName("BindODataVirtualSource")]
        [C1TagHelperIsAttribute(false)]
        [C1HtmlHelperConverter(typeof(CollectionViewServiceConverter))]
        [C1TagHelperConverter(typeof(CollectionViewServiceConverter))]
        public ODataVirtualCollectionViewService<T> ODataVirtualSource
        {
            get
            {
                return ItemsSource as ODataVirtualCollectionViewService<T>;
            }
        }


        [C1HtmlHelperBuilderName("BindODataSource")]
        [C1TagHelperIsAttribute(false)]
        [C1HtmlHelperConverter(typeof(CollectionViewServiceConverter))]
        [C1TagHelperConverter(typeof(CollectionViewServiceConverter))]
        public ODataCollectionViewService<T> ODataSource
        {
            get
            {
                return ItemsSource as ODataCollectionViewService<T>;
            }
        }


        [C1Ignore]
        [Browsable(false)]
        public BaseODataCollectionViewService<T> BaseODataSource
        {
            get
            {
                return ItemsSource as BaseODataCollectionViewService<T>;
            }
        }

        [C1Ignore]
        [Browsable(false)]
        public bool IsODataSourceMode
        {
            get
            {
                return BaseODataSource != null;
            }
        }

        [C1Ignore]
        [Browsable(false)]
        public MVCControl ParsedSetting { get; set; }
    }
}
