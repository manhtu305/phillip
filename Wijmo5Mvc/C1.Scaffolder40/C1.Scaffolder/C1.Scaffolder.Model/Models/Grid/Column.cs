﻿using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class ColumnBase
    {
        /// <summary>
        /// Gets or sets the filter type.
        /// </summary>
        [C1Description("ColumnBase_FilterType")]
        [C1Ignore]
        public FilterType FilterType { get; set; }

        /// <summary>
        /// Convert to string.
        /// </summary>
        /// <returns>The string.</returns>
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Header))
            {
                return Header;
            }

            if (!string.IsNullOrEmpty(Binding))
            {
                return Binding;
            }

            return GetType().Name;
        }
    }
}
