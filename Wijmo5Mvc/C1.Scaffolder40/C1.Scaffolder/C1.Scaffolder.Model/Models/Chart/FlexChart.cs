﻿using System.Collections.Generic;
using System.ComponentModel;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class FlexChart<T>
    {
        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEvents = base.CreateClientEvents();
            clientEvents.Add(new ClientEvent("OnClientSeriesVisibilityChanged"));
            return clientEvents;
        }

        [C1Ignore]
        public override ChartControlType ControlType
        {
            get
            {
                return ChartControlType.FlexChart;
            }
        }

        private AnnotationLayer<T> _annotationLayer;
        /// <summary>
        /// Gets the annotation layer.
        /// </summary>
        [C1HtmlHelperBuilderName("AddAnnotationLayer")]
        [C1HtmlHelperConverter(typeof(AnnotationLayerHtmlHelperConverter))]
        [C1TagHelperIsAttribute(false)]
        public AnnotationLayer<T> AnnotationLayer
        {
            get { return _annotationLayer ?? (_annotationLayer = new AnnotationLayer<T>(this)); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the range selector is used.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        public bool UseRangeSelector { get; set; }

        private RangeSelector<T> _rangeSelector;
        /// <summary>
        /// Gets the RangeSelector.
        /// </summary>
        [C1HtmlHelperBuilderName("AddRangeSelector")]
        [C1TagHelperIsAttribute(false)]
        public RangeSelector<T> RangeSelector
        {
            get { return _rangeSelector ?? (_rangeSelector = new RangeSelector<T>(this)); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the line marker is used.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        public bool UseLineMarker { get; set; }

        private LineMarker<T> _lineMarker;
        /// <summary>
        /// Gets the LineMarker.
        /// </summary>
        [C1HtmlHelperBuilderName("AddLineMarker")]
        [C1TagHelperIsAttribute(false)]
        public LineMarker<T> LineMarker
        {
            get { return _lineMarker ?? (_lineMarker = new LineMarker<T>(this)); }
        }

        /// <summary>
        /// Adds for the LineMarker serialization.
        /// </summary>
        public bool ShouldSerializeLineMarker()
        {
            return UseLineMarker;
        }

        /// <summary>
        /// Adds for the LineMarker serialization.
        /// </summary>
        public bool ShouldSerializeRangeSelector()
        {
            return UseRangeSelector;
        }

        /// <summary>
        /// Adds for the AnnotationLayer serialization.
        /// </summary>
        public bool ShouldSerializeAnnotationLayer()
        {
            return AnnotationLayer.Items.Count > 0;
        }

        public override void BeforeSerialize()
        {
            base.BeforeSerialize();

            if (!string.IsNullOrEmpty(EntitySetName)
                && Series.Count == 0)
            {
                Series.Add(new ChartSeries<T>());
            }

            var index = 0;
            foreach(var item in Series)
            {
                if(item is YFunctionSeries<object>)
                {
                    var ySeries = item as YFunctionSeries<object>;
                    ySeries.Func = Id + "_YFunctionSeries_Func" + index;
                }
                else if(item is ParametricFunctionSeries<object>)
                {
                    var pSeries = item as ParametricFunctionSeries<object>;
                    pSeries.XFunc = Id + "_ParametricFunctionSeries_XFunc" + index;
                    pSeries.YFunc = Id + "_ParametricFunctionSeries_YFunc" + index;
                }
                index++;
            }

            if (UseLineMarker)
            {
                LineMarker.Content =LineMarker.ShowContent? Id + "LineMarkerContent" : null;

                if (LineMarker.ListenPositionChangedEvent)
                {
                    LineMarker.OnClientPositionChanged = GetClientEventName("lineMarker_positionChanged");
                }
                else
                    LineMarker.OnClientPositionChanged = null;
            }

            if (UseRangeSelector)
            {
                if (RangeSelector.ListenRangeChangedEvent)
                {
                    RangeSelector.OnClientRangeChanged = GetClientEventName("rangeSelector_rangeChanged");
                }
                else
                    RangeSelector.OnClientRangeChanged = null;
            }
        }
    }
}
