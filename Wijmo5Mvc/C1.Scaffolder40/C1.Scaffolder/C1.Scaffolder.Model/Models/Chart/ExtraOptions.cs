﻿using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class ExtraOptions
    {
        /// <summary>
        /// Gets Funnel options. The following options are supported : NeckWidth, NeckHeight, Type.
        /// </summary>
        [C1HtmlHelperBuilderName("Funnel")]
        [C1TagHelperIsAttribute(false)]
        public FunnelOptions DesignFunnel
        {
            get { return Funnel; }
        }

        /// <summary>
        /// Specifies whether the Funnel property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeDesignFunnel()
        {
            return ShouldSerializeFunnel();
        }
    }
}
