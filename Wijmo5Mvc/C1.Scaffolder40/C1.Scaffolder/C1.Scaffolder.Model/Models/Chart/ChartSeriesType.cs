﻿namespace C1.Web.Mvc
{
    public enum ChartSeriesType
    {
        Normal = 0,
        TrendLine = 1,
        MovingAverage = 2,
        YFunctionSeries = 3,
        ParametricFunctionSeries = 4,
        WaterfallSeries = 5,
        RadarSeries = 6,
        BoxWhiskerSeries = 7,
        ErrorBarSeries = 8
    }
}
