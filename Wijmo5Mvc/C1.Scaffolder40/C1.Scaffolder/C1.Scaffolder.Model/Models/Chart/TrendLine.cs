﻿using C1.Web.Mvc.Serialization;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    partial class TrendLine<T>
    {
        [Serialization.C1Ignore]
        public override ChartSeriesType SeriesType
        {
            get { return ChartSeriesType.TrendLine; }
        }

        [C1Ignore]
        [Browsable(false)]
        public object AddTrendLine { get; set; }
    }
}
