﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents a section in a chart.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Section<T>
    {
        /// <summary>
        /// Gets or sets the Title of section.
        /// </summary>
        [C1Description("Section_Title")]
        public abstract string Title { get; set; }

        /// <summary>
        /// Gets or sets the Style of section.
        /// </summary>
        [C1Description("Section_Style")]
        public abstract TitleStyle Style { get; }
    }

    /// <summary>
    /// Represents the header of a chart.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class HeaderSection<T> : Section<T>
    {
        private readonly FlexChartBase<T> _chart;

        public HeaderSection(FlexChartBase<T> chart)
        {
            _chart = chart;
        }

        public override string Title
        {
            get { return _chart.Header; }
            set { _chart.Header = value; }
        }

        public override TitleStyle Style
        {
            get { return _chart.HeaderStyle; }
        }
    }

    /// <summary>
    /// Represents the footer of a chart.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FooterSection<T> : Section<T>
    {
        private readonly FlexChartBase<T> _chart;

        public FooterSection(FlexChartBase<T> chart)
        {
            _chart = chart;
        }

        public override string Title
        {
            get { return _chart.Footer; }
            set { _chart.Footer = value; }
        }

        public override TitleStyle Style
        {
            get { return _chart.FooterStyle; }
        }
    }
}
