﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Indicates the type of the chart control.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscate]
    public enum ChartControlType
    {
        /// <summary>
        /// A <see cref="FlexChart"/> chart control.
        /// </summary>
        FlexChart = 0,
        /// <summary>
        /// A <see cref="FlexPie"/> chart control.
        /// </summary>
        FlexPie = 1,
        /// <summary>
        /// A <see cref="Sunburst"/> chart control.
        /// </summary>
        Sunburst = 2,
        /// <summary>
        /// A <see cref="FlexRadar"/> chart control.
        /// </summary>
        FlexRadar = 3,
        /// <summary>
        /// A <see cref="Financial"/> chart control.
        /// </summary>
        Financial = 4
    }
}
