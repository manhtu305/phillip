﻿namespace C1.Web.Mvc.Finance
{
    partial class FinancialChart<T>
    {
        public override ChartControlType ControlType
        {
            get { return ChartControlType.Financial; }
        }
    }
}
