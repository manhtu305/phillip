﻿namespace C1.Web.Mvc.MultiRow
{
    partial class Cell
    {
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            var cell = new Cell();

            // cell
            cell.Colspan = Colspan;

            // Column
            cell.AllowDragging = AllowDragging;
            cell.AllowMerging = AllowMerging;
            cell.AllowResizing = AllowResizing;
            cell.CssClass = CssClass;
            cell.IsContentHtml = IsContentHtml;
            cell.IsSelected = IsSelected;
            cell.Visible = Visible;
            cell.WordWrap = WordWrap;
            cell.Aggregate = Aggregate;
            cell.Align = Align;
            cell.AllowSorting = AllowSorting;
            cell.DataType = DataType;
            cell.Mask = Mask;
            cell.MaxWidth = MaxWidth;
            cell.MinWidth = MinWidth;
            cell.Name = Name;
            cell.Width = Width;
            cell.SortMemberPath = SortMemberPath;
            cell.FilterType = FilterType;
            cell.DropDownCssClass = DropDownCssClass;

            // Field
            cell.Format = Format;
            cell.Header = Header;
            cell.IsReadOnly = IsReadOnly;
            cell.Binding = Binding;

            // DataMap
            CopyDataMap(DataMap, cell.DataMap);

            return cell;
        }

        private void CopyDataMap(DataMap src, DataMap des)
        {
            des.SortByDisplayValues = src.SortByDisplayValues;
            des.DisplayMemberPath = src.DisplayMemberPath;
            des.SelectedValuePath = src.SelectedValuePath;
            des.UseRemoteBinding = src.UseRemoteBinding;
            des.ModelType = src.ModelType;
        }
    }
}
