﻿using System.ComponentModel;
using C1.Web.Mvc.Services;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the holder for input value binding.
    /// </summary>
    public interface IInputValueBindingHolder
    {
        /// <summary>
        /// Gets the value binding settings.
        /// </summary>
        InputValueBinding ValueBinding { get; }
        /// <summary>
        /// Gets or sets the content for xxxxFor() builder.
        /// </summary>
        string BuilderFor { get; set; }
        /// <summary>
        /// Gets or sets the content for tag helper "for" attribute.
        /// </summary>
        string TagHelperFor { get; set; }
        /// <summary>
        /// Resets the value while value binding is valid.
        /// </summary>
        void ResetValue();
    }

    internal static class InputValueBindingExtensions
    {
        public static void SetupForProperties(this IInputValueBindingHolder holder)
        {
            var binding = holder.ValueBinding;
            if (!binding.Enabled || string.IsNullOrEmpty(binding.ValuePath)) return;

            holder.ResetValue();

            const string parameterName = "model";
            string modelName = binding.BindToModel
                ? parameterName
                : (string.IsNullOrEmpty(binding.BindingName) ? "Model" : binding.BindingName);
            BlockCodeType codeType = BlockCodeType.HTMLNode;
            ISupportUpdater supportUpdater = holder as ISupportUpdater;
            if (supportUpdater != null && supportUpdater.ParsedSetting != null)
            {
                codeType = supportUpdater.ParsedSetting.CodeType;
            }
            else
            {
                var proj = ServiceManager.Instance.GetService(typeof(IMvcProject)) as IMvcProject;
                if (proj.IsAspNetCore)
                {
                    codeType = BlockCodeType.HTMLNode;
                }
                else
                {
                    codeType = proj.IsCs ? BlockCodeType.CSharp : BlockCodeType.VB;
                }
            }
            switch (codeType)
            {
                case BlockCodeType.CSharp:
                    holder.BuilderFor = string.Format("{0} => {1}.{2}", parameterName, modelName, binding.ValuePath);
                    break;
                case BlockCodeType.VB:
                    holder.BuilderFor = string.Format("Function({0}) {1}.{2}", parameterName, modelName, binding.ValuePath);
                    break;
                default:
                    holder.TagHelperFor = binding.BindToModel
                        ? binding.ValuePath
                        : string.Format("@{0}.{1}", modelName, binding.ValuePath);
                    break;
            }
        }
    }

    /// <summary>
    /// Represents the settings for value binding.
    /// </summary>
    public class InputValueBinding : IModelTypeHolder
    {
        private IModelTypeDescription _modelType;

        /// <summary>
        /// Initializes a new instance of <see cref="InputValueBinding"/> object.
        /// </summary>
        public InputValueBinding()
        {
            BindToModel = true;
            BindingName = "Model";
        }

        /// <summary>
        /// Gets or sets whether the value is bound to a model.
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the binding model is page model or other object.
        /// </summary>
        [DefaultValue(true)]
        public bool BindToModel { get; set; }

        /// <summary>
        /// Gets or sets the name of the binding model.
        /// </summary>
        [DefaultValue("Model")]
        public string BindingName { get; set; }

        /// <summary>
        /// For designer used. Gets or sets type of the binding model.
        /// </summary>
        public IModelTypeDescription ModelType
        {
            get { return _modelType; }
            set
            {
                if (_modelType == value) return;
                _modelType = value;
                ValuePath = null;
            }
        }

        /// <summary>
        /// Gets or sets the property name in the binding model to be bound.
        /// </summary>
        public string ValuePath { get; set; }
    }
}
