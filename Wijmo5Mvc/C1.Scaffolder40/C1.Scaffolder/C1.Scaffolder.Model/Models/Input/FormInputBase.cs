﻿using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class FormInputBase : IInputValueBindingHolder
    {
        private InputValueBinding _valueBinding;

        /// <summary>
        /// Resets the value while value binding is valid.
        /// </summary>
        public virtual void ResetValue()
        {
           // do nothing
        }

        /// <summary>
        /// Gets the value binding settings.
        /// </summary>
        [C1Ignore]
        public InputValueBinding ValueBinding
        {
            get { return _valueBinding ?? (_valueBinding = new InputValueBinding()); }
        }

        /// <summary>
        /// Gets or sets the content for xxxxFor() builder.
        /// </summary>
        [C1Ignore]
        public string BuilderFor { get; set; }

        /// <summary>
        /// Gets or sets the content for tag helper "for" attribute.
        /// </summary>
        [C1TagHelperName("For")]
        public string TagHelperFor { get; set; }

        /// <summary>
        /// Invoked before serialize this component.
        /// </summary>
        public override void BeforeSerialize()
        {
            base.BeforeSerialize();
            this.SetupForProperties();
        }
    }
}
