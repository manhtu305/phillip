﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    partial class InputDateBase
    {
        /// <summary>
        /// Resets the value while value binding is valid.
        /// </summary>
        public override void ResetValue()
        {
            base.ResetValue();
            Value = null;
        }
    }
}
