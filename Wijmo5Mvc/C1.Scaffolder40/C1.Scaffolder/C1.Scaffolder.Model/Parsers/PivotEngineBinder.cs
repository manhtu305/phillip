﻿using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    internal class PivotEngineBinder : BaseControlBinder
    {
        private static string[] FieldsName = new string[] { "RowFields", "ColumnFields", "ValueFields", "FilterFields" };

        public PivotEngineBinder()
        {
            base.MappedProperties.Add("Collections", "FieldsCollections");

            //client events(dotnet core)
            base.MappedProperties.Add("Error", "OnClientError");
            base.MappedProperties.Add("ItemsSourceChanged", "OnClientItemsSourceChanged");
            base.MappedProperties.Add("UpdatingView", "OnClientUpdatingView");
            base.MappedProperties.Add("UpdatedView", "OnClientUpdatedView");
            base.MappedProperties.Add("ViewDefinitionChanged", "OnClientViewDefinitionChanged");
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settingsFromBase, string realPropName, object instance)
        {
            var propertyValue = settingsFromBase[realPropName].Value;
            if (base.BindComplexProperty(property, settingsFromBase, realPropName, instance))
            {
                return true;
            }

            PivotEngine pivotEngine = instance as PivotEngine;
            if (pivotEngine == null)
            {
                return false;
            }

            var settings = propertyValue as MVCControlCollection;

            //bind ReadActionUrl
            if ("ItemsSource".Equals(property.Name, StringComparison.OrdinalIgnoreCase))
            {
                //this case for Bind(Model only)
                if (settings == null)
                {
                    return false;
                }

                var setting = settings.Items[0];
                string readActionUrl = setting.Properties.ContainsKey("Bind") ?
                    setting.Properties["Bind"].Value.ToString() :
                    setting.Properties.ContainsKey("ReadActionUrl") ?
                    setting.Properties["ReadActionUrl"].Value.ToString() :
                    string.Empty;

                if (!string.IsNullOrEmpty(readActionUrl))
                {
                    pivotEngine.ItemsSource.ReadActionUrl = GetUrlAction(readActionUrl);
                    return true;
                }
                return false;
            }

            //bind FieldsCollections (tag helper case)
            if ("FieldsCollections".Equals(property.Name, StringComparison.OrdinalIgnoreCase))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                var pvFieldCollectionBinder = new PivotFieldCollectionBinder();
                foreach (var mvcControl in builder.Items)
                {
                    MVCProperty nameProp = mvcControl.Properties.Values
                        .FirstOrDefault(name => "Property".Equals(name.Name, StringComparison.OrdinalIgnoreCase));
                    if (nameProp == null)
                    {
                        continue;
                    }

                    var fieldsCollectionProp = pivotEngine.GetType().GetProperty(nameProp.Value.ToString());
                    if (fieldsCollectionProp != null)
                    {
                        var fieldsCollection = fieldsCollectionProp.GetValue(pivotEngine, null);
                        pvFieldCollectionBinder.BindControlProperties(mvcControl, fieldsCollection);
                    }
                }
                return true;
            }

            //bind FieldsCollections (html builder case)
            if (FieldsName.Any(name => name.Equals(property.Name, StringComparison.OrdinalIgnoreCase)))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                //case 1: RowFields(pfcb => pfcb.Items("Country"))
                if (builder != null)
                {
                    var mvcControl = builder.Items[0];
                    var filedsCollectionProp = pivotEngine.GetType().GetProperty(property.Name);
                    if (filedsCollectionProp != null)
                    {
                        var fieldsCollection = filedsCollectionProp.GetValue(pivotEngine, null);
                        var pvFieldCollectionBinder = new PivotFieldCollectionBinder();
                        pvFieldCollectionBinder.BindControlProperties(mvcControl, fieldsCollection);
                        return true;
                    }
                }
                else
                {
                    //case 2: RowFields("Country")               
                    string[] fields = null;
                    if (propertyValue is string)
                    {
                        fields = ((string)propertyValue).Replace("\"", "").Split(',').Select(f => f.Trim()).ToArray();
                    }
                    else if (propertyValue is object[])
                    {
                        fields = ((object[])propertyValue).Select(i => ((string)i).Replace("\"", "").Trim()).ToArray();
                    }

                    if (fields == null)
                    {
                        return false;
                    }

                    var filedsCollectionProp = pivotEngine.GetType().GetProperty(property.Name);
                    var fieldsCollection = filedsCollectionProp.GetValue(pivotEngine, null);
                    var viewFieldColletion = fieldsCollection as ViewFieldCollection;

                    if (viewFieldColletion != null)
                    {
                        viewFieldColletion.SetItems(fields);
                        return true;
                    }
                }
            }

            return false;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return base.IsRequireBindComplex(property, propertyValue) ||
                "FieldsCollections".Equals(property.Name, StringComparison.OrdinalIgnoreCase) ||
                FieldsName.Any(name => name.Equals(property.Name, StringComparison.OrdinalIgnoreCase));
        }
    }

    internal class PivotFieldCollectionBinder : BaseControlBinder
    {
        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return base.IsRequireBindComplex(property, propertyValue) ||
                "Items".Equals(property.Name, StringComparison.OrdinalIgnoreCase);
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            var propertyValue = settings[realPropName].Value;
            if (base.BindComplexProperty(property, settings, realPropName, instance))
            {
                return true;
            }

            PivotFieldCollection pivotFieldCollection = instance as PivotFieldCollection;
            if (pivotFieldCollection == null)
            {
                return false;
            }

            if ("Items".Equals(property.Name, StringComparison.OrdinalIgnoreCase))
            {
                string[] fields = null;
                if (propertyValue is string)
                {
                    fields = ((string)propertyValue).Replace("\"", "").Split(',').Select(f => f.Trim()).ToArray();
                }
                else if (propertyValue is object[])
                {
                    fields = ((object[])propertyValue).Select(i => ((string)i).Replace("\"", "").Trim()).ToArray();
                }

                if (fields == null)
                {
                    return false;
                }

                var viewFieldColletion = pivotFieldCollection as ViewFieldCollection;
                if (viewFieldColletion != null)
                {
                    viewFieldColletion.SetItems(fields);
                    return true;
                }
            }

            return false;
        }
    }
}
