﻿using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    internal class PivotPanelBinder : BaseControlBinder
    {
        public PivotPanelBinder()
        {
            base.MappedProperties.Add("BindEngine", "Engine");
            base.MappedProperties.Add("Engines", "Engine");

            //client events(dotnet core)
            base.MappedProperties.Add("ItemsSourceChanged", "OnClientItemsSourceChanged");
            base.MappedProperties.Add("ViewDefinitionChanged", "OnClientViewDefinitionChanged");
            base.MappedProperties.Add("UpdatingView", "OnClientUpdatingView");
            base.MappedProperties.Add("UpdatedView", "OnClientUpdatedView");
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settingsFromBase, string realPropName, object instance)
        {
            var propertyValue = settingsFromBase[realPropName].Value;
            if (base.BindComplexProperty(property, settingsFromBase, realPropName, instance))
            {
                return true;
            }

            PivotPanel pivotPanel = instance as PivotPanel;
            if (pivotPanel == null)
            {
                return false;
            }

            if ("Engine".Equals(property.Name, StringComparison.OrdinalIgnoreCase))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                var pivotEngineBinder = new PivotEngineBinder();
                pivotEngineBinder.BindControlProperties(builder.Items[0], pivotPanel.Engine);
                return true;
            }

            return false;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return base.IsRequireBindComplex(property, propertyValue) ||
                "Engine".Equals(property.Name, StringComparison.OrdinalIgnoreCase);
        }
    }
}
