﻿using C1.Web.Mvc;
using C1.Web.Mvc.MultiRow;
using C1.Web.Mvc.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    public class ControlBinderFactory
    {
        public static BaseControlBinder CreateBinder(string mvcControlName)
        {
            switch (mvcControlName)
            {
                case "FlexGrid":
                    return new FlexGridBinder();
                case "MultiRow":
                    return new MultiRowBinder();
                case "FlexSheet":
                    return new FlexSheetBinder();
                case "FlexPie":
                    return new FlexPieChartBinder();
                case "FlexRadar":
                    return new FlexRadarChartBinder();
                case "FlexChart":
                case "Sunburst":
                    return new FlexChartBinder();
                case "TabPanel":
                    return new TabPanelBinder();
                case "DashboardLayout":
                    return new DashboardBinder();

                case "InputDate":
                case "InputDateTime":
                case "InputTime":
                case "InputNumber":
                case "InputMask":

                case "ComboBox":
                case "MultiSelect":
                case "AutoComplete":
                case "MultiAutoComplete":
                    return new InputBaseBinder();
                case "InputColor":
                    return new InputColorBinder();
                case "PivotEngine":
                    return new PivotEngineBinder();
                case "PivotPanel":
                    return new PivotPanelBinder();
                default:
                    return new BaseControlBinder();
            }
        }
    }
    /// <summary>
    /// Base binder help bind value to property of C1 MVC control
    /// </summary>
    public class BaseControlBinder
    {
        /// <summary>
        /// Bind a complex property, it might be override by each inherite control
        /// </summary>
        /// <param name="property">A property of control</param>
        /// <param name="settings">Contains value of property</param>
        /// <param name="realPropName">The real name of property</param>
        /// <param name="instance">An instance of C1MVC control need bind properties</param>
        /// <returns></returns>
        public virtual bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            return BindClientEventProperties(property, instance);
        }

        /// <summary>
        /// Binding the client events
        /// </summary>
        /// <param name="propertyValue">Value of property</param>
        /// <param name="instance">An instance of C1MVC control need bind properties</param>
        /// <returns>Return false if it is not client event</returns>
        protected bool BindClientEventProperties(PropertyInfo property, object instance)
        {
            if (property.Name.StartsWith("OnClient"))
            {

                //if instance is not control such as PivotEngine, this always returns false.
                //Control temp = instance as Control;
                //if (temp == null)
                //    return false;

                var component = instance as Component;
                if (component == null)
                {
                    return false;
                }

                foreach (var clientEvent in component.ClientEvents)
                {
                    if (!clientEvent.Handled && clientEvent.Name.Equals(property.Name))
                    {
                        clientEvent.Handled = true;
                        return true;
                    }
                }
            }

            return false;
        }

        private Dictionary<string, string> mappedProperties;

        /// <summary>
        /// Some properties is generated with different name. 
        /// This property will be override in inherited binder to handle exception cases
        /// Key: propertyName that serialized in page
        /// Value: real propertyName of instance
        /// </summary>
        public virtual Dictionary<string, string> MappedProperties
        {
            get
            {
                if (mappedProperties == null)
                {
                    mappedProperties = new Dictionary<string, string>();
                    mappedProperties.Add("Bind", "ItemsSource");
                    mappedProperties.Add("Sources", "ItemsSource");
                }
                return mappedProperties;
            }
            private set
            {
            }
        }


        /// <summary>
        /// Bind all properties of passing instance of C1MVCcontrol.
        /// </summary>
        /// <param name="ctrlSettings">A <see cref="MVCControl"/> contains all parsed properties of this control</param>
        /// <param name="instance">An instance of C1MVC control need bind properties</param>
        public void BindControlProperties(MVCControl ctrlSettings, object instance)
        {
            Type type = instance.GetType();
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Dictionary<string, MVCProperty> settings = ctrlSettings.Properties;
            if (Type.GetTypeCode(type) == TypeCode.Object)
            {
                for (int i = 0; i < settings.Count; i++)
                {
                    string propName = settings.Keys.ElementAt(i);
                    object paramsValue = settings[propName].Value;
                    bool isSupportProperty = false;
                    var property = properties.FirstOrDefault(p => p.Name.Equals(propName));
                    bool isTryBinding = false;
                    if (property != null && !IsIgnoreProperty(property))
                    {
                        isSupportProperty = BindAProperty(settings, propName, instance, property);
                        isTryBinding = true;
                    }

                    if (!isSupportProperty)
                    {
                        string mappedPropName = GetMappedPropertyName(propName);

                        if (!isTryBinding || !mappedPropName.Equals(propName))
                        {
                            if ((property = properties.FirstOrDefault(p => p.Name.Equals(mappedPropName))) != null)
                            {
                                isSupportProperty = BindAProperty(settings, propName, instance, property);
                            }
                            else if (instance is FlexChartBase<object>)
                            {
                                switch (propName)
                                {
                                    case "Axis":
                                        if ((property = properties.FirstOrDefault(p => p.Name.Equals("AxisX"))) != null)
                                            isSupportProperty = BindAProperty(settings, propName, instance, property);
                                        if ((property = properties.FirstOrDefault(p => p.Name.Equals("AxisY"))) != null)
                                            isSupportProperty = BindAProperty(settings, propName, instance, property);
                                        break;
                                    case "Styles":
                                        foreach (var item in (settings["Styles"].Value as C1.Web.Mvc.MVCControlCollection).Items)
                                        {
                                            if (item.Properties.ContainsKey("Property"))
                                            {
                                                if (item.Properties["Property"].Value.ToString().Equals("HeaderStyle"))
                                                    (this as FlexChartBinder).HandleStyles(item, instance, 0);
                                                else if (item.Properties["Property"].Value.ToString().Equals("FooterStyle"))
                                                    (this as FlexChartBinder).HandleStyles(item, instance, 1);

                                                isSupportProperty = true;
                                            }
                                        }

                                        break;
                                }
                            }
                        }
                    }

                    if (!isSupportProperty)
                    {
                        settings[propName].IsIgnored = true;
                    }
                }
                if (instance is ISupportUpdater)
                {
                    ((ISupportUpdater)instance).ParsedSetting = ctrlSettings;
                }

                if (instance is IControlValue && ctrlSettings.ControlValue != null)
                    (instance as IControlValue).ControlValue = ctrlSettings.ControlValue;

                IInputValueBindingHolder inputValueBinding;
                if (ctrlSettings.Binding != null && (inputValueBinding = instance as IInputValueBindingHolder) != null)
                {
                    inputValueBinding.ValueBinding.BindToModel = true;
                    inputValueBinding.ValueBinding.Enabled = true;
                    inputValueBinding.ValueBinding.BindingName = ctrlSettings.Binding.Name;
                    inputValueBinding.ValueBinding.ValuePath = ctrlSettings.Binding.Value.ToString();
                }
            }
        }

        protected virtual bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return propertyValue is MVCControlCollection || propertyValue is Array
                || property.Name.Equals("ItemsSource")
                || property.Name.Equals("ScrollPosition")
                || (property.Name.Equals("Value") && property.ReflectedType.Name.Equals("InputColor"))//specical need bind color value
                || property.Name.StartsWith("OnClient");
        }

        protected string GetUrlAction(object value)
        {
            string urlAction = value.ToString();
            // dotnet core case
            if (urlAction.Contains("@Url.Action"))
            {
                return urlAction.Replace("@Url.Action(\"", "").Replace("\")", "");
            }
            // normal dotnet case
            else if (urlAction.Contains("Url.Action"))
            {
                return urlAction.Replace("Url.Action(\"", "").Replace("\")", "");
            }
            return null;
        }

        private bool BindAProperty(Dictionary<string, MVCProperty> settings, string realPropName, object instance, PropertyInfo property)
        {
            MVCProperty propertySetting = settings[realPropName];
            bool ret = false;
            object propertyValue = propertySetting.Value;
            if (property != null)
            {
                ret = true;
                object value = null;
                if (IsRequireBindComplex(property, propertyValue))
                {
                    return BindComplexProperty(property, settings, realPropName, instance);
                }
                if (propertyValue is string)
                {
                    //case enum
                    if (property.PropertyType.IsEnum)
                    {
                        string strValue = propertyValue.ToString().Split('.').Last();//remove fullname
                        if (Enum.IsDefined(property.PropertyType, strValue))
                        {
                            value = Enum.Parse(property.PropertyType, strValue);
                        }
                        else
                        {
                            propertySetting.NeedTextEditor = true;
                            ret = false;
                        }
                    }
                    else
                        value = propertyValue;
                }

                else
                    value = propertyValue;

                try
                {
                    Type propertyType = property.PropertyType;
                    switch (propertyType.Name)
                    {
                        case "String":
                            {
                                string strValue = value.ToString();
                                if ((strValue.StartsWith("\"") && strValue.EndsWith("\"")) ||
                                    (strValue.StartsWith("\'") && strValue.EndsWith("\'")))
                                {
                                    strValue = strValue.Substring(1, strValue.Length - 2);
                                }

                                propertySetting.NeedTextEditor = false;
                                property.SetValue(instance, strValue);
                            }
                            break;

                        case "Nullable`1":
                            {
                                if (propertyType.GenericTypeArguments[0] == typeof(Single))
                                {
                                    float fValue;
                                    if (TryGetSingleValue(value, out fValue))
                                    {
                                        property.SetValue(instance, fValue);
                                    }
                                }
                                else if (propertyType.GenericTypeArguments[0].IsEnum)
                                {
                                    string strValue = propertyValue.ToString().Split('.').Last();//remove fullname
                                    if (Enum.IsDefined(property.PropertyType.GenericTypeArguments[0], strValue))
                                    {
                                        value = Enum.Parse(property.PropertyType.GenericTypeArguments[0], strValue);
                                        property.SetValue(instance, value);
                                    }
                                    else
                                    {
                                        propertySetting.NeedTextEditor = true;
                                        ret = false;
                                    }
                                }
                                else
                                {
                                    property.SetValue(instance, Convert.ChangeType(value, propertyType.GenericTypeArguments[0]));
                                }
                            }
                            break;

                        case "Single":
                            {
                                float fValue;
                                if (float.TryParse(value as string, out fValue))
                                    property.SetValue(instance, fValue);
                            }
                            break;

                        case "Boolean":
                            {
                                string tmp = null;

                                if (value is string)
                                {
                                    tmp = (value as string).ToLower();
                                    if(tmp != "true" && tmp != "false")
                                    propertySetting.NeedTextEditor = true;
                                    ret = false;
                                    break;
                                }

                                property.SetValue(instance, value);
                            }
                            break;

                        default:
                            property.SetValue(instance, Convert.ChangeType(value, propertyType));
                            break;
                    }
                }
                catch (Exception)
                {
                    propertySetting.NeedTextEditor = true;
                    ret = false;
                }
            }
            return ret;
        }

        private bool TryGetSingleValue(object value, out float singleValue)
        {
            try
            {
                string sValue = value.ToString().ToLowerInvariant();
                if (sValue.EndsWith("f")) sValue = sValue.Replace("f", "");
                if (float.TryParse(sValue, out singleValue))
                    return true;
            }
            catch (ArgumentException e)
            {
                throw e;
            }
            singleValue = 0f;
            return false;
        }

        protected virtual string GetMappedPropertyName(string propName)
        {
            if (MappedProperties != null && MappedProperties.ContainsKey(propName))
                return MappedProperties[propName];
            return propName;
        }
        private bool IsIgnoreProperty(PropertyInfo property)
        {
            var tAttrs = property.GetCustomAttributes(typeof(C1IgnoreAttribute), true).OfType<C1IgnoreAttribute>();
            return tAttrs.FirstOrDefault() != null;
        }


        internal static string[] ConvertToArray(object value)
        {
            string[] groups = null;

            if (value is Array || value is string)
            {
                if (value is Array)
                {
                    groups = Array.ConvertAll<object, string>(value as object[], x => x.ToString().Trim().Replace("\"", ""));
                }
                else if (value is string)
                {
                    string pValue = value.ToString().Trim().Replace("\"", "");
                    if (pValue.Contains(','))
                        groups = pValue.Split(',').Select(v => v.Trim()).ToArray();
                    else
                    {
                        groups = new string[1] { pValue };
                    }
                }
            }
            return groups;
        }
    }

    internal class FlexGridBinder : BaseControlBinder
    {
        public FlexGridBinder()
        {
            base.MappedProperties.Add("Filterable", "AllowFiltering");
            // dotnet core
            base.MappedProperties.Add("Filters", "AllowFiltering");

            base.MappedProperties.Add("ScrollIntoView", "DesignScrollPosition");
            // dotnet core
            base.MappedProperties.Add("Panels", "ShowGroupPanel");

            #region dotnet core client events
            base.MappedProperties.Add("BeginningEdit", "OnClientBeginningEdit");
            base.MappedProperties.Add("CellEditEnded", "OnClientCellEditEnded");
            base.MappedProperties.Add("CellEditEnding", "OnClientCellEditEnding");
            base.MappedProperties.Add("DeletedRow", "OnClientDeletedRow");
            base.MappedProperties.Add("DeletingRow", "OnClientDeletingRow");
            base.MappedProperties.Add("FormatItem", "OnClientFormatItem");
            base.MappedProperties.Add("PrepareCellForEdit", "OnClientPrepareCellForEdit");
            base.MappedProperties.Add("RowAdded", "OnClientRowAdded");
            base.MappedProperties.Add("RowEditEnded", "OnClientRowEditEnded");
            base.MappedProperties.Add("RowEditEnding", "OnClientRowEditEnding");
            base.MappedProperties.Add("SelectionChanged", "OnClientSelectionChanged");
            base.MappedProperties.Add("SelectionChanging", "OnClientSelectionChanging");
            base.MappedProperties.Add("RowEditStarted", "OnClientRowEditStarted");
            base.MappedProperties.Add("RowEditStarting", "OnClientRowEditStarting");
            #endregion

            //odata support
            base.MappedProperties.Add("BindODataSource", "ItemsSource");
            base.MappedProperties.Add("BindODataVirtualSource", "ItemsSource");
            base.MappedProperties.Add("OdataSources", "ItemsSource");
            base.MappedProperties.Add("OdataVirtualSources", "ItemsSource");
            base.MappedProperties.Add("Version", "ODataVersion");
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            if (base.BindComplexProperty(property, settings, realPropName, instance))
            {
                return true;
            }

            bool ret = false;
            string propertyName = property.Name;
            var propertyValue = settings[realPropName].Value;
            MVCControlCollection builder = propertyValue as MVCControlCollection;

            if (builder != null)
            {
                if (propertyName.Equals("Columns"))
                {
                    FlexGridBase<object> grid = instance as FlexGridBase<object>;
                    Column colParent = instance as Column;
                    bool isGrid = grid != null;//case ColumnGroups - column has the child Columns, grid will be null 

                    for (int j = 0; j < builder.Items.Count; j++)
                    {
                        MVCControl columnSetting = builder.Items[j];
                        Column col = new Column();
                        if (columnSetting.Properties.Count == 1 && columnSetting.Properties.Keys.ElementAt(0) == "Add")
                        {
                            MVCControlCollection columnBuilder = columnSetting.Properties["Add"].Value as MVCControlCollection;
                            if (columnBuilder != null)
                            {
                                BindControlProperties(columnBuilder.Items[0], col);
                            }
                        }
                        else  //case cb.Add().IsReadOnly(true)....
                        {
                            BindControlProperties(columnSetting, col);
                        }

                        if (isGrid)
                        grid.Columns.Add(col);
                        else
                            colParent.Columns.Add(col);
                    }
                    if (isGrid)
                    grid.FillDesignColumns(true);

                    ret = true;
                    
                }

                else if (propertyName.Equals("DataMap"))
                {
                    if (builder.Items.Count == 1)
                    {
                        MVCControl setting = builder.Items[0];
                        BindControlProperties(setting, ((Column)instance).DataMap);
                        ret = true;
                    }
                }

                else if (propertyName.Equals("CellTemplate"))
                {
                    if (builder.Items.Count == 1)
                    {
                        MVCControl setting = builder.Items[0];
                        BindControlProperties(setting, ((Column)instance).CellTemplate);
                        ret = true;
                    }
                }

                else if (propertyName.Equals("ItemsSource"))
                {
                    #region The instance variable is FlexGrid type
                    if (instance is FlexGridBase<object>)
                    {
                        ret = BindingFlexGridItemsSource(instance, builder, realPropName);
                    }
                    #endregion

                    #region The instance variable is DataMap type
                    else if (instance is DataMap)
                    {
                        DataMap dataMap = instance as DataMap;

                        if (builder.Items.Count == 1)
                        {
                            MVCControl itemSourceSetting = builder.Items[0];
                            BindControlProperties(itemSourceSetting, dataMap.ItemsSource);
                            if (itemSourceSetting.Properties.ContainsKey("Bind"))//special case for Bind ReadActionUrl
                            {
                                dataMap.ItemsSource.ReadActionUrl = GetUrlAction(itemSourceSetting.Properties["Bind"].Value);
                            }
                        }
                    }
                    #endregion
                }
                #region ShowGroupPanel
                else if (propertyName.Equals("ShowGroupPanel"))
                {
                    FlexGridBase<object> grid = instance as FlexGridBase<object>;
                    grid.ShowGroupPanel = true;
                    if (builder.Items.Count == 1)
                    {
                        BindControlProperties(builder.Items[0], grid.GroupPanel);
                        ret = true;
                    }
                }
                #endregion
            }
            #region AllowFiltering
            if (propertyName.Equals("AllowFiltering"))
            {
                FlexGridBase<object> grid = instance as FlexGridBase<object>;
                grid.AllowFiltering = true;
                if (builder == null)
                    return true;
                if (builder.Items.Count == 1)
                {
                    BindControlProperties(builder.Items[0], grid.FilterExtension);
                    ret = true;
                }
            }
            #endregion
            #region Scroll Position of normal dotnet
            else if (propertyName.Equals("DesignScrollPosition"))
            {
                object[] scrolls = propertyValue as object[];
                if (scrolls != null && scrolls.Length == 2)
                {
                    FlexGridBase<object> grid = instance as FlexGridBase<object>;
                    grid.DesignScrollPosition.X = (int)scrolls[0];
                    grid.DesignScrollPosition.Y = (int)scrolls[1];

                    ret = true;
                }
            }
            #endregion
            #region Scroll Position of dotnet core
            else if (propertyName.Equals("ScrollPosition"))
            {
                FlexGridBase<object> grid = instance as FlexGridBase<object>;
                // SubString(25) meaning for remove "new System.Drawing.Point(" in propertyValue
                string[] tempPointStrings = propertyValue.ToString().Substring(25).Split(',');
                grid.ScrollPosition = new System.Drawing.Point(Int32.Parse(tempPointStrings[0]), Int32.Parse(tempPointStrings[1].Remove(tempPointStrings[1].Length - 1)));
                tempPointStrings = null;

                return true;
            }
            #endregion

            return ret;
        }

        private bool BindingFlexGridItemsSource(object instance, MVCControlCollection builder, string realPropName)
        {
            bool ret = false;
            FlexGridBase<object> grid = instance as FlexGridBase<object>;
            if (builder.Items.Count == 1)
            {
                bool hasBindedProperty = false;
                MVCControl itemSourceSetting = builder.Items[0];

                switch (realPropName)
                {
                    case "BindODataSource":
                    case "OdataSources":
                        grid.ItemsSource = new ODataCollectionViewService<object>();
                        break;
                    case "BindODataVirtualSource":
                    case "OdataVirtualSources":
                        grid.ItemsSource = new ODataVirtualCollectionViewService<object>();
                        break;
                }

                BindControlProperties(itemSourceSetting, grid.ItemsSource);
                // normal dotnet case
                if (itemSourceSetting.Properties.ContainsKey("Bind"))//special case for Bind ReadActionUrl
                {
                    grid.ItemsSource.ReadActionUrl = GetUrlAction(itemSourceSetting.Properties["Bind"].Value);
                    hasBindedProperty = true;
                }
                // dotnet core case
                else if (itemSourceSetting.Properties.ContainsKey("ReadActionUrl"))
                {
                    grid.ItemsSource.ReadActionUrl = GetUrlAction(itemSourceSetting.Properties["ReadActionUrl"].Value);
                    hasBindedProperty = true;
                }

                if (itemSourceSetting.Properties.ContainsKey("CreateActionUrl"))
                {
                    grid.ItemsSource.CreateActionUrl = GetUrlAction(itemSourceSetting.Properties["CreateActionUrl"].Value);
                    hasBindedProperty = true;
                }

                if (itemSourceSetting.Properties.ContainsKey("DeleteActionUrl"))
                {
                    grid.ItemsSource.DeleteActionUrl = GetUrlAction(itemSourceSetting.Properties["DeleteActionUrl"].Value);
                    hasBindedProperty = true;
                }

                if (itemSourceSetting.Properties.ContainsKey("UpdateActionUrl"))
                {
                    grid.ItemsSource.UpdateActionUrl = GetUrlAction(itemSourceSetting.Properties["UpdateActionUrl"].Value);
                    hasBindedProperty = true;
                }

                if (itemSourceSetting.Properties.ContainsKey("Update"))
                {
                    if (builder != null && builder.Items.Count == 1)
                        grid.IsReadOnly = false;
                }

                if (itemSourceSetting.Properties.ContainsKey("BatchEdit"))//special case for Bind BatchEditUrl
                {
                    grid.ItemsSource.BatchEdit = true;
                    grid.ItemsSource.BatchEditActionUrl = GetUrlAction(itemSourceSetting.Properties["BatchEdit"].Value);
                    hasBindedProperty = true;
                }

                if (itemSourceSetting.Properties.ContainsKey("GetError"))
                {
                    grid.EnableValidation = true;
                    hasBindedProperty = true;
                }

                bool containMoveToPage = false;

                if ((containMoveToPage = itemSourceSetting.Properties.ContainsKey("MoveToPage")) || itemSourceSetting.Properties.ContainsKey("PageSize"))//special case for Pager
                {
                    grid.AllowPaging = true;
                    if (containMoveToPage)
                    {
                        MVCProperty p = itemSourceSetting.Properties["MoveToPage"];
                        int pageIndex;
                        if (int.TryParse(p.Value.ToString(), out pageIndex))
                        {
                            grid.ItemsSource.PageIndex = pageIndex;
                        }
                    }
                    hasBindedProperty = true;
                }

                if (itemSourceSetting.Properties.ContainsKey("SortDescriptions"))
                {
                    MVCControlCollection sortSettings = itemSourceSetting.Properties["SortDescriptions"].Value as MVCControlCollection;

                    // iTop top most index position item can hold.
                    int iTop = 0;
                    var listPropertyNameTemp = grid.DesignSortDescriptions.Select(x => x.Property);
                    var orderHolder = new List<DesignSortDescription>(grid.DesignSortDescriptions.Count);

                    for (int i = 0; i < sortSettings.Items.Count; ++i)
                    {
                        var item = sortSettings.Items[i];
                        string itemValue = null;
                        if (item.Properties["Property"].Value.ToString().Contains('\"'))
                            itemValue = item.Properties["Property"].Value.ToString().Substring(1, item.Properties["Property"].Value.ToString().Length - 2);
                        else
                            itemValue = item.Properties["Property"].Value.ToString();


                        if (!listPropertyNameTemp.Contains(itemValue))
                            continue;

                        for (int j = 0; j < grid.DesignSortDescriptions.Count; ++j)
                        {
                            if (!grid.DesignSortDescriptions[j].Property.Equals(itemValue)) continue;
                            grid.DesignSortDescriptions[j].Enabled = true;
                            if (!item.Properties.ContainsKey("Ascending"))
                                grid.DesignSortDescriptions[j].Ascending = false;
                            orderHolder.Add(grid.DesignSortDescriptions[j]);
                            ++iTop;
                            break;
                        }
                    }

                    if (iTop != 0)
                    {
                        var restItems = from item in grid.DesignSortDescriptions.ToList() where !orderHolder.Contains(item) select item;
                        orderHolder.AddRange(restItems);
                        for (int i = 0; i < grid.DesignSortDescriptions.Count; ++i)
                            if (grid.DesignSortDescriptions[i] != orderHolder[i])
                                grid.DesignSortDescriptions[i] = orderHolder[i];
                        restItems = null;
                    }

                    orderHolder = null;
                    listPropertyNameTemp = null;
                    hasBindedProperty = true;
                }
                // dotnet core case
                else if (itemSourceSetting.Properties.ContainsKey("OrderBy"))
                {
                    string[] items = itemSourceSetting.Properties["OrderBy"].Value.ToString().Split(',');
                    List<string[]> map = new List<string[]>(items.Length);
                    for (int i = 0; i < items.Length; ++i)
                        map.Add(items[i].Split(' '));

                    // iTop top most index position item can hold.
                    int iTop = 0;
                    var listPropertyNameTemp = grid.DesignSortDescriptions.Select(x => x.Property);
                    var orderHolder = new List<DesignSortDescription>(grid.DesignSortDescriptions.Count);

                    for (int i = 0; i < map.Count; ++i)
                    {
                        string itemValue = map[i][0];

                        if (!listPropertyNameTemp.Contains(itemValue))
                            continue;

                        for (int j = 0; j < grid.DesignSortDescriptions.Count; ++j)
                        {
                            if (!grid.DesignSortDescriptions[j].Property.Equals(itemValue)) continue;
                            grid.DesignSortDescriptions[j].Enabled = true;
                            if (map[i].Length == 2)
                                if (map[i][1].ToLower().Equals("desc"))
                                    grid.DesignSortDescriptions[j].Ascending = false;
                            orderHolder.Add(grid.DesignSortDescriptions[j]);
                            ++iTop;
                            break;
                        }
                    }

                    if (iTop != 0)
                    {
                        var restItems = from item in grid.DesignSortDescriptions.ToList() where !orderHolder.Contains(item) select item;
                        orderHolder.AddRange(restItems);
                        for (int i = 0; i < grid.DesignSortDescriptions.Count; ++i)
                            if (grid.DesignSortDescriptions[i] != orderHolder[i])
                                grid.DesignSortDescriptions[i] = orderHolder[i];
                        restItems = null;
                    }

                    orderHolder = null;
                    listPropertyNameTemp = null;
                    map = null;
                    items = null;
                    hasBindedProperty = true;
                }

                if (itemSourceSetting.Properties.ContainsKey("GroupBy"))
                {
                    MVCProperty p = itemSourceSetting.Properties["GroupBy"];
                    if (p.Value is Array || p.Value is string)
                    {
                        string[] groups = null;

                        if (p.Value is Array)
                        {
                            groups = Array.ConvertAll<object, string>(p.Value as object[], x => x.ToString());
                        }
                        else if (p.Value is string)
                        {
                            string pValue = p.Value.ToString();
                            if (pValue.Contains(','))
                                groups = pValue.Split(',');
                            else
                            {
                                groups = new string[1] { pValue };
                            }
                        }

                        #region /*--------------------- Handle group by checkbox and order  ---------------------*/
                        {
                            //remove quote
                            for (int i = 0; i < groups.Length; ++i)
                            {
                                if (groups[i].StartsWith("\""))
                                    groups[i] = groups[i].Substring(1, groups[i].Length - 2);
                            }

                            // iTop top most index position item can hold.
                            int iTop = 0;
                            var listPropertyNameTemp = grid.DesignGroupDescriptions.Select(x => x.PropertyName);
                            var orderHolder = new List<DesignPropertyGroupDescription>(grid.DesignGroupDescriptions.Count);

                            for (int i = 0; i < groups.Length; ++i)
                            {
                                var item = groups[i];
                                if (!listPropertyNameTemp.Contains(item))
                                    continue;

                                for (int j = 0; j < grid.DesignGroupDescriptions.Count; ++j)
                                {
                                    if (!grid.DesignGroupDescriptions[j].PropertyName.Equals(item)) continue;
                                    grid.DesignGroupDescriptions[j].Enabled = true;
                                    orderHolder.Add(grid.DesignGroupDescriptions[j]);
                                    ++iTop;
                                    break;
                                }
                            }

                            if (iTop != 0)
                            {
                                var restItems = from item in grid.DesignGroupDescriptions.ToList() where !orderHolder.Contains(item) select item;
                                orderHolder.AddRange(restItems);
                                for (int i = 0; i < grid.DesignGroupDescriptions.Count; ++i)
                                    if (grid.DesignGroupDescriptions[i] != orderHolder[i])
                                        grid.DesignGroupDescriptions[i] = orderHolder[i];
                                restItems = null;
                            }

                            orderHolder = null;
                            listPropertyNameTemp = null;
                        }
                        #endregion
                        hasBindedProperty = true;
                    }
                }

                //odata support
                if (itemSourceSetting.Properties.ContainsKey("Fields"))
                {
                    grid.BaseODataSource.Fields = ConvertToArray(itemSourceSetting.Properties["Fields"].Value);
                    hasBindedProperty = true;
                }

                if (itemSourceSetting.Properties.ContainsKey("Keys"))
                {
                    grid.BaseODataSource.Keys = ConvertToArray(itemSourceSetting.Properties["Keys"].Value);
                    hasBindedProperty = true;
                }

                if (!hasBindedProperty)
                {
                    int countProperties = itemSourceSetting.Properties.Count;
                    foreach (var item in itemSourceSetting.Properties)
                    {
                        if (item.Value.IsIgnored) countProperties--;
                        else break;
                    }
                    if (countProperties == 0) return false;//all properties are ignore, this parent property also be ignored
                }
                ret = true;
            }

            return ret;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return
                   property.Name.Equals("AllowFiltering")
                || base.IsRequireBindComplex(property, propertyValue);

        }
    }

    public interface ISupportUpdater
    {
        MVCControl ParsedSetting { get; set; }
    }

    /// <summary>
    /// Interface for whom will need to hold value of the MVC control.
    ///     example:
    ///         @(Html.C1().MVC_Control("something").property("val")
    ///         the control value in this case is "something".
    /// </summary>
    public interface IControlValue
    {
        [C1Ignore]
        [System.ComponentModel.Browsable(false)]
        string ControlValue { get; set; }

        void CorrectIdForAspnetCore();
    }
}
