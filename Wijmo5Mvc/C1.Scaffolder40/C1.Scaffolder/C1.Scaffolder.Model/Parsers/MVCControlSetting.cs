﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Contains all setting of MVC control base text
    /// </summary>
    public class MVCControl
    {
        
        public MVCControl(BlockCodeType codeType = BlockCodeType.CSharp)
        {
            Properties = new Dictionary<string, MVCProperty>();
            CodeType = codeType;
        }

        /// <summary>
        /// Start absolute character index of this control in page.
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>
        /// End absolute character index of this control in page.
        /// </summary>
        public int EndIndex { get; set; }

        /// <summary>
        /// MVC control's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Model name of this control
        /// </summary>
        public string ModelType { get; set; }

        /// <summary>
        /// List of parsed properties.
        /// </summary>
        public Dictionary<string, MVCProperty> Properties { get; set;}

        /// <summary>
        /// Gets or sets information about ValueBinding of InputControlBase
        /// </summary>
        public MVCProperty Binding { get; set; }

        /// <summary>
        /// Gets or sets information about type of block code contains this control
        /// </summary>
        public BlockCodeType CodeType { get; private set; }

        internal string RenderedText { get; set; }

        /// <summary>
        /// this will hold outter value of the current control,
        ///     example:
        ///         @(Html.C1().TabPanel("something").property("val")
        ///         the outter value in this case is "something".
        /// </summary>
        internal string ControlValue;
    }

    /// <summary>
    /// A class contains result of property's parsed
    /// </summary>
    public class MVCProperty {
        public MVCProperty(string name, object value, string renderedText)
        {
            Name = name;
            Value = value;
            RenderedText = renderedText;
        }
        /// <summary>
        /// Name of property.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Current rendered text of this property.
        /// </summary>
        public string RenderedText { get; set; }
        /// <summary>
        ///Parsed result of this property.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Is this property ignored when update.
        /// </summary>
        public bool IsIgnored { get; set; }

        /// <summary>
        /// This property is input text expression, then update with text editor.
        /// </summary>
        public bool NeedTextEditor { get; set; }

        /// <summary>
        /// Allow adjust rendered text of this property
        /// </summary>
        /// <param name="newValue"></param>
        public void AdjustRenderedText(object newValue)
        {
            string oldRenderderText = RenderedText;
            oldRenderderText = oldRenderderText.Replace(Value.ToString(), newValue.ToString());
            Value = newValue;
            RenderedText = oldRenderderText;
        }
    }
    public class MVCControlCollection {
        public MVCControlCollection() {
            Items = new List<MVCControl>();
        }
        public string VarialbeName { get; set; }
        public List<MVCControl> Items { get; set; }
    }
}
