﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;

namespace C1.Web.Mvc
{
    class DashboardBinder : BaseControlBinder
    {
        private string defaultSplitMappedValue = "Items";
        //stack use for map correct Tile/Group
        List<bool> stackBindingChildOfGroup;
        public DashboardBinder()
        {
            MappedProperties.Add("AttachAutoGridLayout", "Layout");
            MappedProperties.Add("AttachSplitLayout", "Layout");
            MappedProperties.Add("AttachFlowLayout", "Layout");
            MappedProperties.Add("AttachManualGridLayout", "Layout");

            //core
            MappedProperties.Add("Layouts", "Layout");
            MappedProperties.Add("Groups", "Items");
            MappedProperties.Add("Tiles", "Children");
            MappedProperties.Add("FlowTiles", "Items");
            MappedProperties.Add("SplitTiles", defaultSplitMappedValue);
            MappedProperties.Add("SplitGroups", defaultSplitMappedValue);

            stackBindingChildOfGroup = new List<bool>();
            
            #region netcore onclient events
            MappedProperties.Add("FormatTile", "OnClientFormatTile");
            MappedProperties.Add("TileSizeChanged", "OnClientTileSizeChanged");
            MappedProperties.Add("TileActivated", "OnClientTileActivated");
            #endregion
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            if (base.BindComplexProperty(property, settings, realPropName, instance))
                return true;

            var propertyValue = settings[realPropName].Value;
            MVCControlCollection collection = propertyValue as MVCControlCollection;

            if (property.Name.Equals("Layout"))
            {
                var dashboard = instance as DashboardLayout;
                if (realPropName == "Layouts" && collection != null)
                {
                    realPropName = collection.VarialbeName;
                }
                //set LayoutType will create specified type for Layout of dashboard
                switch (realPropName)
                {
                    case "AttachAutoGridLayout":
                    case "c1-auto-grid-layout":
                        dashboard.LayoutType = LayoutType.Auto;
                        break;
                    case "AttachSplitLayout":
                    case "c1-split-layout":
                        dashboard.LayoutType = LayoutType.Split;
                        break;
                    case "AttachFlowLayout":
                    case "c1-flow-layout":
                        dashboard.LayoutType = LayoutType.Flow;
                        break;
                    case "AttachManualGridLayout":
                    case "c1-manual-grid-layout":
                        dashboard.LayoutType = LayoutType.Manual;
                        break;
                }
            }

            PropertyInfo pi = instance.GetType().GetProperty(property.Name);
            if (pi == null) return false;
            var holder = pi.GetValue(instance, null);
            if (holder == null)
            {
                try
                {
                    holder = Activator.CreateInstance(instance.GetType().GetProperty(property.Name).GetType());
                }
                catch (MissingMethodException ex)
                {
                    holder = FormatterServices.GetUninitializedObject(instance.GetType().GetProperty(property.Name).GetType());
                }
                catch (Exception other)
                {
                    throw new NotImplementedException();
                }
            }

            try
            {
                if (collection != null)
                {
                    var methodAdd = holder.GetType().GetMethod("Add");
                    if (methodAdd == null && collection.Items.Count == 1)
                    {
                        BindControlProperties(collection.Items[0], holder);
                    }
                    else
                    {
                        var type = holder.GetType().GetGenericArguments().Single();
                        if (type.Name.Equals("ISplitLayoutItem"))
                        {
                            foreach (var propertyItem in collection.Items)
                            {
                                SplitGroup group = null;
                                SplitTile tile = null;
                                object dest;
                                bool bindingGroup = true;
                                if (propertyItem.Name == "SplitTile" || (propertyItem.Properties.Count > 0 && propertyItem.Properties.ElementAt(0).Key == "AddTile"))
                                {
                                    tile = new SplitTile();
                                    propertyItem.Properties.Remove("AddTile");
                                    dest = tile;
                                    bindingGroup = false;
                                }
                                else
                                {
                                    group = new SplitGroup();
                                    propertyItem.Properties.Remove("AddGroup");
                                    //is binding SplitGroup, split tile will be child of group
                                    bindingGroup = true;
                                    dest = group;
                                }
                                stackBindingChildOfGroup.Insert(0, bindingGroup);
                                BindControlProperties(propertyItem, dest);                                
                                methodAdd.Invoke(holder, new object[] { dest });
                                stackBindingChildOfGroup.Remove(bindingGroup);                                
                                dest = null;
                                tile = null;
                                group = null;
                            }
                            goto SPLIT_DONE;
                        }

                        foreach (var item in collection.Items)
                        {
                            object temp = Activator.CreateInstance(type);
                            BindControlProperties(item, temp);
                            methodAdd.Invoke(holder, new object[] { temp });
                            temp = null;
                        }

                        methodAdd = null; type = null;

                    SPLIT_DONE:;
                    }
                }
                
                else if (propertyValue is MVCControl)
                {
                    BindControlProperties(propertyValue as MVCControl, holder);
                }
                else
                {
                    holder = null;
                    return false;
                }

                holder = null;
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected override string GetMappedPropertyName(string propName)
        {
            if (propName == "SplitTiles")
                MappedProperties["SplitTiles"] = (stackBindingChildOfGroup.Count > 0 ? stackBindingChildOfGroup[0] : false) ? "Children" : defaultSplitMappedValue;

            string mappedValue = base.GetMappedPropertyName(propName);

            if (propName == "SplitGroups")
            {
                MappedProperties[propName] = "Children";
            }

            return mappedValue;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return
                property.Name.StartsWith("OnClient") ||
                property.Name.Equals("Layout") ||
                property.Name.Equals("Items") ||
                property.Name.Equals("Children") ||
                property.Name.Equals("LayoutItems")
                ;
        }
    }
}
