﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Parsers
{
    internal sealed class ChartSeriesBinder : BaseControlBinder
    {
        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return base.IsRequireBindComplex(property, propertyValue);
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            return base.BindComplexProperty(property, settings, realPropName, instance);
        }
    }
}
