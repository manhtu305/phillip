﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace C1.Web.Mvc.Converters
{
    internal class BindingConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return false;
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            ICollection values = new string[] { };
            IList<PropertyDescription> properties = null;
            var useDefaultModelType = false;
            var modelClassHolder = context.Instance as IModelTypeHolder;
            if (modelClassHolder != null)
            {
                if (modelClassHolder.ModelType != null)
                {
                    properties = modelClassHolder.ModelType.Properties;
                }
                else
                {
                    useDefaultModelType = context.Instance is IChartSeries;
                }
            }

            if(modelClassHolder == null || useDefaultModelType)
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                if (dbService != null && dbService.ModelType != null)
                {
                    properties = dbService.ModelType.Properties;
                }
            }

            if (properties != null)
            {
                values = properties.Select(p => p.Name).ToArray();
            }

            return new StandardValuesCollection(values);
        }
    }
}
