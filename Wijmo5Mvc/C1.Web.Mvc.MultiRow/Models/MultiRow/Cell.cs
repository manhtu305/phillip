﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc.MultiRow
{
    public partial class Cell
    {
#if !MODEL
        private readonly HtmlHelper _helper;

        #region Ctor
        /// <summary>
        /// Creates one <see cref="Cell"/> instance.
        /// </summary>
        public Cell()
            : this(null)
        {
        }

        /// <summary>
        /// Creates one <see cref="Cell"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        internal Cell(HtmlHelper helper)
            : base(helper)
        {
            Initialize();
            _helper = helper;
        }

        #endregion Ctor
#else
        /// <summary>
        /// Creates one <see cref="Cell"/> instance.
        /// </summary>
        public Cell()
        {
            Initialize();
        }
#endif
    }
}
