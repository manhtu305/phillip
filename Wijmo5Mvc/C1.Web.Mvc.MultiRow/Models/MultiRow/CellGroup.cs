﻿using System.Collections.Generic;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc.MultiRow
{
    public partial class CellGroup
    {
        #region Ctor
#if !MODEL
        /// <summary>
        /// Creates one <see cref="CellGroup"/> instance.
        /// </summary>
        public CellGroup()
            : this(null)
        {
        }

        /// <summary>
        /// Creates one <see cref="CellGroup"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        internal CellGroup(HtmlHelper helper)
            : base()
        {
            Initialize();
            _helper = helper;
        }
#else
        /// <summary>
        /// Creates one <see cref="CellGroup"/> instance.
        /// </summary>
        public CellGroup()
        {
            Initialize();
        }
#endif
        #endregion Ctor

        #region Cells
        private IList<Cell> _cells;
        private IList<Cell> _GetCells()
        {
            if (_cells != null)
            {
                return _cells;
            }

            return _cells = new List<Cell>();
        }
        #endregion Cells
    }
}
