﻿using System.ComponentModel;
using System.Collections.Generic;
#if MODEL
using C1.Web.Mvc.Serialization;
#endif

namespace C1.Web.Mvc.MultiRow
{
    public partial class MultiRow<T>
    {
        private IList<CellGroup> _layoutDefinition;
        private IList<HeaderCellGroup> _headerLayoutDefinition;

        private IList<CellGroup> _GetLayoutDefinition()
        {
            return _layoutDefinition ?? (_layoutDefinition = new List<CellGroup>());
        }

        private IList<HeaderCellGroup> _GetHeaderLayoutDefinition()
        {
            return _headerLayoutDefinition ?? (_headerLayoutDefinition = new List<HeaderCellGroup>());
        }

        /// <summary>
        /// Disabled for MultiRow. Gets the grid's column collection.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
#if MODEL
        [C1Ignore]
#endif
        public override IList<Column> Columns
        {
            get { return base.Columns; }
        }

        /// <summary>
        /// Override to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDraggingColumnOver
        {
            get;
            set;
        }

        /// <summary>
        /// Override to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDraggingRowOver
        {
            get;
            set;
        }
    }
}
