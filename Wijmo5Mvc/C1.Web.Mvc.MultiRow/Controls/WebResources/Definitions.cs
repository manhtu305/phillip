﻿using C1.Web.Mvc.WebResources;

[assembly: AssemblyScripts(typeof(C1.Web.Mvc.MultiRow.WebResources.Definitions.All))]
[assembly: AssemblyStyles(typeof(C1.Web.Mvc.MultiRow.WebResources.Definitions.All))]

namespace C1.Web.Mvc.MultiRow.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Definitions
    {
        [Scripts(typeof(MultiRow))]
        [Styles(typeof(MultiRow))]
        public abstract class All { }

        [Scripts(MultiRowWebResourcesHelper.WijmoJs + "wijmo.grid.multirow",
            MultiRowWebResourcesHelper.Shared + "Grid.MultiRow",
            MultiRowWebResourcesHelper.Mvc + "Grid.MultiRow",
            MultiRowWebResourcesHelper.Mvc + "Cast.MultiRow")]
        public abstract class MultiRow : Mvc.WebResources.Definitions.Grid { }
    }
}
