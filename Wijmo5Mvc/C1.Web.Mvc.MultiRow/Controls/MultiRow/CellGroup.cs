﻿#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.MultiRow
{
    public partial class CellGroup
    {
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Gets or sets the html helper object.
        /// </summary>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal HtmlHelper Helper
        {
            get { return _helper; }
        }
    }
}
