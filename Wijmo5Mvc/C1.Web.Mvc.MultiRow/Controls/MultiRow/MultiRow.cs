﻿using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc.Grid;
using C1.Web.Mvc.WebResources;
using System;

namespace C1.Web.Mvc.MultiRow
{
    [Scripts(typeof(WebResources.Definitions.MultiRow))]
    public partial class MultiRow<T>
    {
        private const string MultiRowSort = "MultiRowSort";
        private const string ClientModuleMultirow = "grid.multirow.";

        static MultiRow()
        {
            SortHelper.AddSortDescriptor<CellInfo>(MultiRowSort);
        }

        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        internal override string ClientSubModule
        {
            get { return ClientModuleMultirow; }
        }

        internal override bool SupportsUnobtrusiveValidation
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets all columns in the grid.
        /// </summary>
        /// <returns>Collection of columns.</returns>
        internal override IEnumerable<ColumnBase> GetFlatColumns()
        {
            return LayoutDefinition.SelectMany(g => g.Cells);
        }

#if !ASPNETCORE
        #region callback

        /// <summary>
        /// Gets column by the specified infomation.
        /// </summary>
        /// <param name="columnInfo">The column info.</param>
        /// <returns>The column meets the specified column info.</returns>
        internal override ColumnBase GetColumn(ColumnInfo columnInfo)
        {
            var cellInfo = columnInfo as CellInfo;
            if (cellInfo.Group < 0 || cellInfo.Group >= LayoutDefinition.Count) return null;

            var group = LayoutDefinition[cellInfo.Group];
            if (cellInfo.Index < 0 || cellInfo.Index >= group.Cells.Count) return null;

            return group.Cells[cellInfo.Index];
        }

        #endregion
#endif

    }
}
