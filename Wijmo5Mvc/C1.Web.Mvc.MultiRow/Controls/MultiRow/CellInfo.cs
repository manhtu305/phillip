﻿namespace C1.Web.Mvc.MultiRow
{
    /// <summary>
    /// Represents a cell info that describes a column on the MultiRow.
    /// </summary>
    public class CellInfo : ColumnInfo
    {
        /// <summary>
        /// Creates one <see cref="CellInfo"/> instance.
        /// </summary>
        public CellInfo()
        {
            Group = -1;
        }

        /// <summary>
        /// Gets or sets the index of the cell group which the cell belongs to.
        /// </summary>
        public int Group { get; set; }

        /// <summary>
        /// Gets or sets the index of the column in the cell group's cells collection.
        /// </summary>
        public override int Index
        {
            get { return base.Index; }
            set { base.Index = value; }
        }
    }
}
