﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.MultiRow.Fluent
{
    /// <summary>
    /// Extends <see cref="ScriptsBuilder"/> class for MultiRow scripts.
    /// </summary>
    public static class ScriptsBuilderExtension
    {
        /// <summary>
        /// Registers olap related script bundle.
        /// </summary>
        /// <param name="scriptsBuilder">The <see cref="ScriptsBuilder"/>.</param>
        /// <returns>The <see cref="ScriptsBuilder"/>.</returns>
        public static ScriptsBuilder MultiRow(this ScriptsBuilder scriptsBuilder)
        {
            scriptsBuilder.OwnerTypes.AddRange(MultiRowWebResourcesHelper.AllScriptOwnerTypes.Value);
            return scriptsBuilder;
        }
    }
}
