﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.MultiRow.Fluent
{
    /// <summary>
    /// Extends ControlBuilderFactory for MultiRowChart related controls creation.
    /// </summary>
    public static class ControlBuilderFactoryExtension
    {
        /// <summary>
        /// Create a MultiRowBuilder.
        /// </summary>
        /// <typeparam name="T">The data item type.</typeparam>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <param name="selector">The selector.</param>
        /// <returns>The MultiRowBuilder.</returns>
        public static MultiRowBuilder<T> MultiRow<T>(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new MultiRowBuilder<T>(new MultiRow<T>(controlBuilderFactory._helper, selector));
        }
    }
}