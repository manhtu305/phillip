﻿using System;
using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.MultiRow.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use FlexGridFilter extender.
    /// </summary>
    public static class MultiRowGroupPanelExtension
    {
        /// <summary>
        /// Apply the FlexGridGroupPanel extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T">The data record type.</typeparam>
        /// <param name="gridBuilder">The specified FlexGrid builder.</param>
        /// <param name="gridGroupPanelBuilder">The specified FlexGridGroupPanel builder</param>
        /// <param name="selector">The specified selector for the FlexGridGroupPanel's host element.</param>
        /// <returns>The MultiRow builder.</returns>
        public static MultiRowBuilder<T> ShowGroupPanel<T>(this MultiRowBuilder<T> gridBuilder, Action<FlexGridGroupPanelBuilder<T>> gridGroupPanelBuilder, string selector = null)
        {
            MultiRow<T> grid = gridBuilder.Object;
            FlexGridGroupPanel<T> gridGroupPanel = new FlexGridGroupPanel<T>(grid, selector);
            gridGroupPanelBuilder(new FlexGridGroupPanelBuilder<T>(gridGroupPanel));
            grid.Extenders.Add(gridGroupPanel);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default FlexGridGroupPanel extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T">The data record type.</typeparam>
        /// <param name="gridBuilder">The specified FlexGrid builder.</param>
        /// <param name="selector">The specified selector for the FlexGridGroupPanel's host element.</param>
        /// <returns>The MultiRow builder.</returns>
        public static MultiRowBuilder<T> ShowGroupPanel<T>(this MultiRowBuilder<T> gridBuilder, string selector = null)
        {
            MultiRow<T> grid = gridBuilder.Object;
            FlexGridGroupPanel<T> gridGroupPanel = new FlexGridGroupPanel<T>(grid, selector);
            grid.Extenders.Add(gridGroupPanel);
            return gridBuilder;
        }
    }
}
