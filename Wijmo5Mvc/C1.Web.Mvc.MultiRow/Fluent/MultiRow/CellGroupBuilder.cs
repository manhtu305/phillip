﻿using System;
using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.MultiRow.Fluent
{
    public partial class CellGroupBuilder
    {
        /// <summary>
        /// Configure <see cref="Cell"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>The cell group builder.</returns>
        public virtual CellGroupBuilder Cells(Action<ListItemFactory<Cell, CellBuilder>> builder)
        {
            builder(new ListItemFactory<Cell, CellBuilder>(Object.Cells,
                () => new Cell(Object.Helper), c => new CellBuilder(c)));
            return this;
        }
    }
}
