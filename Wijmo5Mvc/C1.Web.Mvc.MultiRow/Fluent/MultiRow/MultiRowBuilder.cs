﻿using System;
using System.ComponentModel;
using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.MultiRow.Fluent
{
    public partial class MultiRowBuilder<T>
    {
        /// <summary>
        /// Configure <see cref="CellGroup"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>The MultiRow builder.</returns>
        public virtual MultiRowBuilder<T> LayoutDefinition(Action<ListItemFactory<CellGroup, CellGroupBuilder>> builder)
        {
            builder(new ListItemFactory<CellGroup, CellGroupBuilder>(Object.LayoutDefinition,
                () => new CellGroup(Object.Helper), c => new CellGroupBuilder(c)));
            return this;
        }

        /// <summary>
        /// Configure <see cref="HeaderCellGroup"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>The MultiRow builder.</returns>
        public virtual MultiRowBuilder<T> HeaderLayoutDefinition(Action<ListItemFactory<HeaderCellGroup, HeaderCellGroupBuilder>> builder)
        {
            builder(new ListItemFactory<HeaderCellGroup, HeaderCellGroupBuilder>(Object.HeaderLayoutDefinition,
                () => new HeaderCellGroup(Object.Helper), c => new HeaderCellGroupBuilder(c)));
            return this;
        }

        /// <summary>
        /// Configure <see cref="Column"/>.
        /// </summary>
        /// <remarks>Disabled for MultiRow.</remarks>
        /// <param name="builder">The builder action.</param>
        /// <returns>The MultiRow builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override MultiRowBuilder<T> Columns(Action<ListItemFactory<Column, ColumnBuilder>> builder)
        {
            return base.Columns(builder);
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override MultiRowBuilder<T> OnClientDraggingColumnOver(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override MultiRowBuilder<T> OnClientDraggingRowOver(string value)
        {
            return this;
        }
    }
}
