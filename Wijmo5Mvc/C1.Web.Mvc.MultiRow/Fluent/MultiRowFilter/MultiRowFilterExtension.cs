﻿using System;
using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.MultiRow.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use FlexGridFilter extender.
    /// </summary>
    public static class MultiRowFilterExtension
    {
        /// <summary>
        /// Apply the FlexGridFilter extender in MultiRow.
        /// </summary>
        /// <typeparam name="T">The data record type.</typeparam>
        /// <param name="multiRowBuilder">The specified MultiRow builder.</param>
        /// <param name="gridFilterBuilder">The specified FlexGridFilter builder.</param>
        /// <returns>The MultiRow builder.</returns>
        public static MultiRowBuilder<T> Filterable<T>(this MultiRowBuilder<T> multiRowBuilder, Action<FlexGridFilterBuilder<T>> gridFilterBuilder)
        {
            var multiRow = multiRowBuilder.Object;
            FlexGridFilter<T> gridFilter = new FlexGridFilter<T>(multiRow);
            gridFilterBuilder(new FlexGridFilterBuilder<T>(gridFilter));
            multiRow.Extenders.Add(gridFilter);
            return multiRowBuilder;
        }

        /// <summary>
        /// Apply the default FlexGridFilter extender in MultiRow.
        /// </summary>
        /// <typeparam name="T">The data record type.</typeparam>
        /// <param name="multiRowBuilder">The specified MultiRow builder.</param>
        /// <returns>The MultiRow filter.</returns>
        public static MultiRowBuilder<T> Filterable<T>(this MultiRowBuilder<T> multiRowBuilder)
        {
            var multiRow = multiRowBuilder.Object;
            FlexGridFilter<T> gridFilter = new FlexGridFilter<T>(multiRow);
            multiRow.Extenders.Add(gridFilter);
            return multiRowBuilder;
        }
    }
}
