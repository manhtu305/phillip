﻿[サンプル名]ColumnGroups（ASP.NET Core）
[カテゴリ]FlexGrid for ASP.NET MVC
------------------------------------------------------------------------
[概要]
複数レベルのマージ済みヘッダーを使用して構造化データを表示する方法を示します。

ほとんどのテーブルとグリッドには、列が参照するフィールドの名前を示す単一のヘッダー行が含まれています。

多くの場合、テーブルデータは階層的な性質を有しており、階層を公開するためにいくつかのレベルの列見出しを追加することが望ましいです。

このサンプルでは、「ColumnGroupProvider」クラスを使用して、列階層を記述するJavaScriptオブジェクトに基づいて、複数レベルのマージ済みヘッダーを作成します。

例として、次のようなテーブルを作成することができます：

<pre>
    /-----------------------------------------\
    |          |      Average      |   Red    |
    |          |-------------------|  eyes    |
    |          |  height |  weight |          |
    |-----------------------------------------|
    |  Males   | 1.9     | 0.003   |   40%    |
    |-----------------------------------------|
    | Females  | 1.7     | 0.002   |   43%    |
    \-----------------------------------------/
</pre>

この場合には次のコードを使用します：

<pre>
  // define the data
  // http://www.w3.org/TR/html401/struct/tables.html
  var w3Data = [
    { gender: 'Males', height: 1.9, weight: 0.003, red: .4 },
    { gender: 'Females', height: 1.7, weight: 0.002, red: .43 },
  ];

  // define the columns
  var w3Columns = [
    { header: ' ', binding: 'gender' },
    {
      header: 'Average', columns: [
        { header: 'Height', binding: 'height', format: 'n1' },
        { header: 'Weight', binding: 'weight', format: 'n3' }
      ]
    },
    { header: 'Red Eyes', binding: 'red', format: 'p0' }
  ];

  // bind columns and data to a FlexGrid
  bindColumnGroups(flex, w3Columns);
</pre>

w3Dataオブジェクトがどのように列を配列として記述しているかに注目してください。各要素は個々の列または列グループに対応しています。