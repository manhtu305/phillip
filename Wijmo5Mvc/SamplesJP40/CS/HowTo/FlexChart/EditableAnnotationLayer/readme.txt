﻿[サンプル名]EditableAnnotationLayer
[カテゴリ]FlexChart for ASP.NET MVC
------------------------------------------------------------------------
[概要]
FlexChartで注釈を編集する方法を示します。

このサンプルは、FlexChartで複数の形状の注釈を追加、削除、およびドラッグする方法を示します。
注釈機能はC1AnnotationLayerリソースに含まれます。
