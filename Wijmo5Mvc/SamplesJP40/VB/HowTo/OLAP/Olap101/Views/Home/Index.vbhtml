﻿@Imports System.Text.RegularExpressions


@ModelType OLAP101Model

@Code
    ViewBag.Title = "OLAP入門"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="container">
    <!-- はじめに -->
    <div>
        <h2>はじめに</h2>
        <p>
            <b>C1.Web.Mvc.Olap</b>アセンブリには、Excelに似たピボットテーブルやピボットチャートの機能をASP.NET MVCアプリケーションに追加できるコントロールが含まれています。
        </p>
        <p>
            MVCアプリケーションでOlapモジュールを使い始める手順は次のとおりです。
        </p>
        <ol>
            <li>Create a new MVC project using the ComponentOne ASP.NET MVC application template, in Visual Studio.</li>
            <li>コントローラと対応するビューをプロジェクトに追加します。</li>
            <li>Razor構文を使用して、ビューの<b>PivotEngine</b>コントロールを初期化します。</li>
            <li>
                <b>Bind</b>メソッドを使用して、解析する生データを含む配列をバインドします。
            </li>
            <li>
                <b>PivotPanel</b>コントロールを初期化し、ビュー定義をカスタマイズする場合は、<b>ItemsSourceId</b>プロパティを<b>PivotEngine</b>コントロールに設定します。
            </li>
            <li>
                いくつかの<b>PivotGrid</b>コントロールおよび<b>PivotChart</b>コントロールをページに追加し、それらの<b>ItemsSourceId</b>プロパティを<b>PivotEngine</b>コントロールまたは<b>PivotPanel</b>コントロールに設定することで、コントロールを<b>PivotPanel</b>に接続します。
            </li>
        </ol>
        <p>
            これで完了です。<b>PivotPanel</b>に使用できるフィールドのリストが表示され、ユーザーは、サマリー領域間でフィールドをドラッグしてデータサマリー（「ビュー」）を生成できます。
            フィールドのヘッダー、集計関数、フィルタ、書式などを設定することもできます。
        </p>
        <p>
            データの分析、結果の印刷、結果のXLSXまたはPDFへのエクスポートが行えるほか、ビュー定義を保存して後で再利用することもできます。
        </p>
        <p>
            次に、簡単な例を示します。
        </p>

        <h4>
            PivotEngine、PivotPanel、PivotGridのページへの追加
        </h4>
        <p>
            <b>PivotPanel</b>コントロールは、ピボットテーブルやピボットチャートに関連付けられたExcelの［フィールドリスト］ウィンドウに似ています。
        </p>
        <ol>
            <li>
                ビューにフィールドを追加するには、フィールドをドラッグするか、チェックボックスを使用します。
            </li>
            <li>
                ビューからフィールドを削除するには、フィールドを上部の領域にドラッグして戻すか、フィールドのコンテキストメニューを使用します。
            </li>
            <li>
                フィールドを設定するには、フィールドのコンテキストメニューを使用します。 ヘッダー、集計関数、書式を変更できます。 書式は、データをグループ化する際に使用されます。 したがって、たとえば日付フィールドの書式を変更することで、データを日、月、四半期、または年別にグループ化することができます。
            </li>
        </ol>
        <p>
            <b>PivotGrid</b>コントロールは、<b>FlexGrid</b>を拡張して、カスタムセル結合、階層的な行および列グループ、カスタムコンテキストメニューなどのピボット機能をサポートします。 カスタムコンテキストメニューからは、ピボットフィールドを設定したり、特定の集計セルの計算で使用されたデータ項目にドリルダウンすることができます。
        </p>
        <div class="col-md-12">
            <h4>結果（ライブ）:</h4>
        </div>
        <div class="row">
            <div class="col-md-4">
                @(Html.C1().PivotEngine().Id("pivotEngine") _
                    .Bind(Model.Data) _
                    .RowFields(Sub(pfcb) pfcb.Items("Country")) _
                    .ColumnFields(Sub(cfcb) cfcb.Items("Product")) _
                    .ValueFields(Sub(vfcb) vfcb.Items("Sales")))
                @Html.C1().PivotPanel().Id("gsPivotPanel").CssStyle("height", "550px").ItemsSourceId("pivotEngine")
            </div>
            <div class="col-md-8">
                @Html.C1().PivotGrid().Id("gsPivotGrid").ItemsSourceId("gsPivotPanel")
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#gsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#gsCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#gsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li class="active"><a href="#gsClose" role="tab" data-toggle="tab">X</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content" id="gsHtml">
@@(Html.C1().PivotEngine().Id("pivotEngine") _
    .Bind(Model.Data) _
    .RowFields(Sub(pfcb) pfcb.Items("Country")) _
    .ColumnFields(Sub(cfcb) cfcb.Items("Product")) _
    .ValueFields(Sub(vfcb) vfcb.Items("Sales")))
@@Html.C1().PivotPanel().Id("gsPivotPanel").CssStyle("height", "550px").ItemsSourceId("pivotEngine")
@@Html.C1().PivotGrid().Id("gsPivotGrid").ItemsSourceId("gsPivotPanel")
                        </div>
                        <div class="tab-pane pane-content" id="gsCS">
Imports C1.Web.Mvc.Olap

Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Dim model As New OLAP101Model()
        model.Data = ProductData.GetData(10000)
        Return View(model)
    End Function
End Class
                        </div>
                        <div class="tab-pane pane-content active" id="gsClose" style="padding:0px!important;height:0px!important;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- PivotPanelのプロパティの設定 -->
    <div>
        <h2>PivotPanelのプロパティの設定</h2>
        <p>
            PivotPanelコントロールには、ビューをカスタマイズするためのプロパティが用意されています。 次のコントロールを使用して、いくつかのプロパティの値を変更し、その効果を確認してください。
        </p>
        <div class="col-md-12">
            <h4>結果（ライブ）:</h4>
        </div>
        <div class="row">
                <div class="col-md-4">
                    @Html.C1().PivotPanel().Id("cPPPivotPanel").ItemsSourceId("pivotEngine").CssStyle("height", "550px")
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        @Html.C1().PivotGrid().Id("cPPPivotGrid").ItemsSourceId("cPPPivotPanel")
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Row Totals:
                        </div>
                        <div class="col-md-4">
                            @(Html.C1().ComboBox().Id("cmbRowTotals").Bind(Model.Settings("RowTotals")) _
                    .OnClientSelectedIndexChanged("cmbRowTotals_SelectedIndexChanged")
                            )
                        </div>
                        <div class="col-md-2">
                            Column Totals:
                        </div>
                        <div class="col-md-4">
                            @(Html.C1().ComboBox().Id("cmbColumnTotals").Bind(Model.Settings("ColumnTotals")) _
                .OnClientSelectedIndexChanged("cmbColumnTotals_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Show Zeros:
                        </div>
                        <div class="col-md-4">
                            @(Html.C1().ComboBox().Id("cmbShowZeros").Bind(Model.Settings("ShowZeros")) _
                    .OnClientSelectedIndexChanged("cmbShowZeros_SelectedIndexChanged")
                            )
                        </div>
                        <div class="col-md-2">
                            Totals Before Data:
                        </div>
                        <div class="col-md-4">
                            @(Html.C1().ComboBox().Id("cmbTotalsBeforeData").Bind(Model.Settings("TotalsBeforeData")) _
                    .OnClientSelectedIndexChanged("cmbTotalsBeforeData_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#cPPHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#cPPJS" role="tab" data-toggle="tab">JS</a></li>
                        <li><a href="#cPPCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li class="active"><a href="#cPPClose" role="tab" data-toggle="tab">X</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content" id="cPPHtml">
@@(Html.C1().PivotEngine().Id("pivotEngine") _
    .Bind(Model.Data) _
    .RowFields(Sub(pfcb) pfcb.Items("Country")) _
    .ColumnFields(Sub(cfcb) cfcb.Items("Product")) _
    .ValueFields(Sub(vfcb) vfcb.Items("Sales")))
@@Html.C1().PivotPanel().Id("cPPPivotPanel").ItemsSourceId("pivotEngine").CssStyle("height", "550px")
@@Html.C1().PivotGrid().Id("cPPPivotGrid").ItemsSourceId("cPPPivotPanel")
Row Totals:
@@(Html.C1().ComboBox().Id("cmbRowTotals").Bind(Model.Settings("RowTotals")) _
    .OnClientSelectedIndexChanged("cmbRowTotals_SelectedIndexChanged")
)
Column Totals: 
@@(Html.C1().ComboBox().Id("cmbColumnTotals").Bind(Model.Settings("ColumnTotals")) _
    .OnClientSelectedIndexChanged("cmbColumnTotals_SelectedIndexChanged")
)
Show Zeros:
@@(Html.C1().ComboBox().Id("cmbShowZeros").Bind(Model.Settings("ShowZeros")) _
.OnClientSelectedIndexChanged("cmbShowZeros_SelectedIndexChanged")
)
Totals Before Data:
@@(Html.C1().ComboBox().Id("cmbTotalsBeforeData").Bind(Model.Settings("TotalsBeforeData")) _
    .OnClientSelectedIndexChanged("cmbTotalsBeforeData_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="cPPJS">
// PivotPanelのプロパティの設定
function cmbRowTotals_SelectedIndexChanged(sender) {
    var value = sender.selectedValue;
    var control = wijmo.Control.getControl("#cPPPivotPanel");
    if (control && control.engine) {
        control.engine.showRowTotals = value;
    }
};

function cmbColumnTotals_SelectedIndexChanged(sender) {
    var value = sender.selectedValue;
    var control = wijmo.Control.getControl("#cPPPivotPanel");
    if (control && control.engine) {
        control.engine.showColumnTotals = value;
    }
};

function cmbShowZeros_SelectedIndexChanged(sender) {
    var value = String(sender.selectedValue).toLowerCase() == 'true' ? true : false;
    var control = wijmo.Control.getControl("#cPPPivotPanel");
    if (control && control.engine) {
        control.engine.showZeros = value;
    }
};

function cmbTotalsBeforeData_SelectedIndexChanged(sender) {    
    var value = String(sender.selectedValue).toLowerCase() == 'true' ? true : false;
    var control = wijmo.Control.getControl("#cPPPivotPanel");
    if (control && control.engine) {
        control.engine.totalsBeforeData = value;
    }
};
                        </div>
                        <div class="tab-pane pane-content" id="cPPCS">
Imports C1.Web.Mvc.Olap

Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Dim model As New OLAP101Model()
        model.Settings = GetSettings()
        model.Data = ProductData.GetData(10000)
        Return View(model)
    End Function

    Private Function GetSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
            {"RowTotals", New Object() {ShowTotals.Subtotals.ToString(), ShowTotals.None.ToString(), ShowTotals.GrandTotals.ToString()}},
            {"ColumnTotals", New Object() {ShowTotals.Subtotals.ToString(), ShowTotals.None.ToString(), ShowTotals.GrandTotals.ToString()}},
            {"ShowZeros", New Object() {False.ToString(), True.ToString()}},
            {"TotalsBeforeData", New Object() {False.ToString(), True.ToString()}}            
    }
        Return settings
    End Function

End Class
                        </div>
                        <div class="tab-pane pane-content active" id="cPPClose" style="padding:0px!important;height:0px!important;">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- PivotChartでの結果の表示 -->
    <div>
        <h2>PivotChartでの結果の表示</h2>
        <p>
            <b>PivotChart</b>コントロールは、結果をグラフィカルに視覚化して提供します。 Excelのピボットチャートに似ており、複数のチャートタイプや階層的な軸をサポートしています。
        </p>
        <p>
            <b>PivotChart</b>コントロールを使用するには、<b>ItemsSourceId</b>プロパティを使用して、コントロールを<b>PivotPanel</b>または<b>PivotEngine</b>に接続します。
        </p>
        <div class="col-md-12">
            <h4>結果（ライブ） :   </h4>
        </div>

        <div class="row">
            <div class="col-md-4">
                @Html.C1().PivotPanel().Id("rPCPivotPanel").CssStyle("height", "550px").ItemsSourceId("pivotEngine")
            </div>
            <div Class="col-md-8">
                    チャートタイプ：  
                    @(Html.C1().ComboBox().Id("cmbChartType").Bind(Model.Settings("ChartType")) _
            .OnClientSelectedIndexChanged("cmbChartType_SelectedIndexChanged")
                    )
                <div class="row">
                    @Html.C1().PivotChart().Id("rPCPivotChart").ItemsSourceId("rPCPivotPanel")
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#rPCHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#rPCJS" role="tab" data-toggle="tab">JS</a></li>
                        <li><a href="#rPCCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li class="active"><a href="#rPCClose" role="tab" data-toggle="tab">X</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content" id="rPCHtml">
@@(Html.C1().PivotEngine().Id("pivotEngine").CssStyle("height", "550px") _
    .Bind(Model.Data) _
    .RowFields(Sub(pfcb) pfcb.Items("Country").Items("Product")) _
    .ValueFields(Sub(vfcb) vfcb.Items("Sales").Items("Downloads"))
)
@@Html.C1().PivotPanel().Id("rPCPivotPanel").CssStyle("height", "550px").ItemsSourceId("pivotEngine")
チャートタイプ： 
@@(Html.C1().ComboBox().Id("cmbChartType").Bind(Model.Settings("ChartType")) _
    .OnClientSelectedIndexChanged("cmbChartType_SelectedIndexChanged")
)
@@Html.C1().PivotChart().Id("rPCPivotChart").ItemsSourceId("rPCPivotPanel")
                            
                        </div>
                        <div class="tab-pane pane-content" id="rPCJS">
// PivotChartでの結果の表示
function cmbChartType_SelectedIndexChanged(sender) {
    var value = sender.selectedValue;
    var control = wijmo.Control.getControl("#rPCPivotChart");
    if (control) {
        control.chartType = value;
    }
};
                        </div>
                        <div class="tab-pane pane-content" id="rPCCS">
Imports C1.Web.Mvc.Olap

                                            Public Class HomeController
                                                Inherits System.Web.Mvc.Controller
                                                Function Index() As ActionResult
                                                    Dim model As New OLAP101Model()
                                                    model.Settings = GetSettings()
                                                    model.Data = ProductData.GetData(10000)
                                                    Return View(model)
                                                End Function

                                                Private Function GetSettings() As IDictionary(Of String, Object())
                                                    Dim settings = New Dictionary(Of String, Object())() From {
                                                        {"ChartType", New Object() {PivotChartType.Column.ToString(), PivotChartType.Area.ToString(), PivotChartType.Bar.ToString(), PivotChartType.Line.ToString(), PivotChartType.Pie.ToString(), PivotChartType.Scatter.ToString()}}
                                                }
                                                    Return settings
                                                End Function

                                            End Class
                        </div>
                        <div class="tab-pane pane-content active" id="rPCClose" style="padding:0px!important;height:0px!important;">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- ソースデータの表示と編集 -->
    <div>
        <h2>ソースデータの表示と編集</h2>
        <p>
              ピボットビューはライブです。 10,000個ある項目のいずれかを編集すると、ピボットビューは自動的に更新されます。
        </p>
        <div class="col-md-12">
            <h4>結果（ライブ） : </h4>
        </div>
        <div class="row">
            <div class="col-md-6">
                @(Html.C1().FlexGrid().Id("vESourceFGrid") _
                        .ShowSelectedHeaders(C1.Web.Mvc.Grid.HeadersVisibility.All).CssStyle("border", "none") _
                        .Filterable()
                )
            </div>
            <div class="col-md-6">
                @Html.C1().PivotGrid().Id("vESourcePivotGrid").ItemsSourceId("pivotEngine")
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                                                                        <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#vESourceHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#vESourceJS" role="tab" data-toggle="tab">JS</a></li>
                        <li><a href="#vESourceCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li class="active"><a href="#vESourceClose" role="tab" data-toggle="tab">X</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content" id="vESourceHtml">
@@(Html.C1().FlexGrid().Id("vESourceFGrid") _
    .ShowSelectedHeaders(C1.Web.Mvc.Grid.HeadersVisibility.All).CssStyle("border", "none") _
    .Filterable()
)
@@Html.C1().PivotGrid().Id("vESourcePivotGrid").ItemsSourceId("pivotEngine")
                        </div>
                        <div class="tab-pane pane-content" id="vESourceJS">
// view and edit the source data
var pivotEngine = null,
    vESourceFGrid = null;
function InitialControls() {
    // view and edit the source data
    pivotEngine = c1.getService('pivotEngine');
    vESourceFGrid = wijmo.Control.getControl('#vESourceFGrid');
    vESourceFGrid.itemsSource = pivotEngine.itemsSource;
}
                        </div>
                        <div class="tab-pane pane-content" id="vESourceCS">
Imports C1.Web.Mvc.Olap

                                                                                                Public Class HomeController
                                                                                                    Inherits System.Web.Mvc.Controller
                                                                                                    Function Index() As ActionResult
                                                                                                        Dim model As New OLAP101Model()
                                                                                                        model.Data = ProductData.GetData(10000)
                                                                                                        Return View(model)
                                                                                                    End Function
                                                                                                End Class

                        </div>
                        <div class="tab-pane pane-content active" id="vESourceClose" style="padding:0px!important;height:0px!important;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 結果のExcelへのエクスポート -->
    <div>
        <h2>結果のExcelへのエクスポート</h2>
        <p>
            <b>PivotGrid</b>コントロールは<b>FlexGrid</b>の拡張です。したがって、<b>FlexGrid</b>に付属する拡張モジュールでサポートされている形式であれば、どの形式にもエクスポートできます。 サポートされている形式には、XLSLX、CSV、PDFなどがあります。
        </p>
        <p>
            たとえば、次のボタンは、現在のビュー、現在のビューを転置したバージョン、生データの3つのシートを含むExcelファイルを作成します。
        </p>
        <div class="col-md-12">
            <h4>結果（ライブ） : </h4>
        </div>
        <div class="row">
            <div class="col-md-10">
                @Html.C1().PivotGrid().Id("exportExcelPivotGrid").ItemsSourceId("pivotEngine")
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" onclick="exportToExcel()">XLSXファイルに保存</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                                                                                                                                <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#exportExcelHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#exportExcelJS" role="tab" data-toggle="tab">JS</a></li>
                        <li><a href="#exportExcelCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li class="active"><a href="#exportExcelClose" role="tab" data-toggle="tab">X</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content" id="exportExcelHtml">
@@(Html.C1().PivotEngine().Id("pivotEngine").Bind(Model.Data) _
    .RowFields(Sub(pfcb) pfcb.Items("Country")) _
    .ColumnFields(Sub(cfcb) cfcb.Items("Product")) _
    .ValueFields(Sub(vfcb) vfcb.Items("Sales")))
@@Html.C1().PivotGrid().Id("exportExcelPivotGrid").ItemsSourceId("pivotEngine")
&lt;button class="btn btn-primary" onclick="exportToExcel()"&gt;XLSXファイルに保存&lt;/button&gt;
                        </div>
                        <div class="tab-pane pane-content" id="exportExcelJS">
// export the result to excel
var pivotEngine = null,
    exportExcelPivotGrid = null;

function InitialControls() {
    // export the result to excel
    pivotEngine = c1.getService('pivotEngine');
    exportExcelPivotGrid = wijmo.Control.getControl('#exportExcelPivotGrid');
}

// export to excel
function exportToExcel() {
    // create book with current view
    var book = wijmo.grid.xlsx.FlexGridXlsxConverter.save(exportExcelPivotGrid, {
        includeColumnHeaders: true,
        includeRowHeaders: true
    });
    book.sheets[0].name = 'Main View';
    addTitleCell(book.sheets[0], getViewTitle(pivotEngine));

    // add sheet with transposed view
    transposeView(pivotEngine);
    var transposed = wijmo.grid.xlsx.FlexGridXlsxConverter.save(exportExcelPivotGrid, {
        includeColumnHeaders: true,
        includeRowHeaders: true
    });
    transposed.sheets[0].name = 'Transposed View';
    addTitleCell(transposed.sheets[0], getViewTitle(pivotEngine));
    book.sheets.push(transposed.sheets[0]);
    transposeView(pivotEngine);

    // save the book
    book.save('wijmo.olap.xlsx');
};

// save/load/transpose/export views
function transposeView(ng) {
    ng.deferUpdate(function () {

        // save row/col fields
        var rows = [],
            cols = [];
        for (var r = 0; r < ng.rowFields.length; r++) {
            rows.push(ng.rowFields[r].header);
        }
        for (var c = 0; c < ng.columnFields.length; c++) {
            cols.push(ng.columnFields[c].header);
        }

        // clear row/col fields
        ng.rowFields.clear();
        ng.columnFields.clear();

        // restore row/col fields in transposed order
        for (var r = 0; r < rows.length; r++) {
            ng.columnFields.push(rows[r]);
        }
        for (var c = 0; c < cols.length; c++) {
            ng.rowFields.push(cols[c]);
        }
    });
}

// build a title for the current view
function getViewTitle(ng) {
    var title = '';
    for (var i = 0; i < ng.valueFields.length; i++) {
        if (i > 0) title += ', ';
        title += ng.valueFields[i].header;
    }
    title += ' by ';
    if (ng.rowFields.length) {
        for (var i = 0; i < ng.rowFields.length; i++) {
            if (i > 0) title += ', ';
            title += ng.rowFields[i].header;
        }
    }
    if (ng.rowFields.length && ng.columnFields.length) {
        title += ' and by ';
    }
    if (ng.columnFields.length) {
        for (var i = 0; i < ng.columnFields.length; i++) {
            if (i > 0) title += ', ';
            title += ng.columnFields[i].header;
        }
    }
    return title;
}

// adds a title cell into an xlxs sheet
function addTitleCell(sheet, title) {
    // create cell
    var cell = new wijmo.xlsx.WorkbookCell();
    cell.value = title;
    cell.style = new wijmo.xlsx.WorkbookStyle();
    cell.style.font = new wijmo.xlsx.WorkbookFont();
    cell.style.font.bold = true;

    // create row to hold the cell
    var row = new wijmo.xlsx.WorkbookRow();
    row.cells[0] = cell;

    // and add the new row to the sheet
    sheet.rows.splice(0, 0, row);
}

// gets a random integer between zero and max
function randomInt(max) {
    return Math.floor(Math.random() * (max + 1));
}
                        </div>
                        <div class="tab-pane pane-content" id="exportExcelCS">
Imports C1.Web.Mvc.Olap

Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Dim model As New OLAP101Model()
        model.Data = ProductData.GetData(10000)
        Return View(model)
    End Function
End Class
                        </div>
                        <div class="tab-pane pane-content active" id="exportExcelClose" style="padding:0px!important;height:0px!important;">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- PivotGridのセルのカスタマイズ -->
    <div>
        <h2>PivotGridのセルのカスタマイズ</h2>
        <p>
            <b>PivotGrid</b>コントロールは、<b>FlexGrid</b>の拡張です。したがって、<b>ItemFormatter</b>プロパティを使用し、各セルのコンテンツを完全に自由に変更して、グリッドセルの表示をカスタマイズできます。
        </p>
        <p>
            たとえば、次の<b>PivotGrid</b>は、Excelのアイコンセットと同様の色とアイコンを使用して、四半期ごとの売上高の変化を示しています。
        </p>
        <div class="col-md-12">
            <h4>結果（ライブ） : </h4>
        </div>
        <div>
            <div class="col-md-4">
                @Html.C1().PivotPanel().Id("cTPGCPivotPanel").CssStyle("height", "550px").ItemsSourceId("pivotEngine")
            </div>
            <div class="col-md-8">
                @(Html.C1().PivotGrid().Id("cTPGCPivotGrid").ItemsSourceId("cTPGCPivotPanel") _
                        .ShowSelectedHeaders(C1.Web.Mvc.Grid.HeadersVisibility.All) _
                        .ItemFormatter("cTPGCPivotGrid_ItemFormatter")
                )
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                                                                                                                                                                                <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#cTPGCHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#cTPGCJS" role="tab" data-toggle="tab">JS</a></li>
                        <li><a href="#cTPGCCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li class="active"><a href="#cTPGCClose" role="tab" data-toggle="tab">X</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content" id="cTPGCHtml">
@@(Html.C1().PivotEngine().Id("pivotEngine").Bind(Model.Data) _
    .RowFields(Sub(pfcb) pfcb.Items("Country")) _
    .ColumnFields(Sub(cfcb) cfcb.Items("Product")) _
    .ValueFields(Sub(vfcb) vfcb.Items("Sales")))
@@Html.C1().PivotPanel().Id("cTPGCPivotPanel").CssStyle("height", "550px").ItemsSourceId("pivotEngine")
@@(Html.C1().PivotGrid().Id("cTPGCPivotGrid").ItemsSourceId("cTPGCPivotPanel") _
    .ShowSelectedHeaders(C1.Web.Mvc.Grid.HeadersVisibility.All) _
    .ItemFormatter("cTPGCPivotGrid_ItemFormatter")
)
                        </div>
                        <div class="tab-pane pane-content" id="cTPGCJS">
// PivotGridのセルのカスタマイズ
function cTPGCPivotGrid_ItemFormatter(panel, r, c, cell) {
    if (wijmo.grid.CellType.Cell == panel.cellType && c % 2 == 1) {
        var value = panel.getCellData(r, c),
        color = '#d8b400',
        glyph = 'circle';
        if (value != null) {
            if (value < 0) { // negative variation
            color = '#9f0000';
            glyph = 'down';
        } else if (value > 0.05) { // positive variation
            color = '#4c8f00';
            glyph = 'down';
        }
        cell.style.color = color;
        cell.innerHTML += ' &lt;span style="font-size:120%" class="wj-glyph-' + glyph + '"&gt;&lt;/span&gt;';
        }
    }
};
                        </div>
                        <div class="tab-pane pane-content" id="cTPGCCS">
Imports C1.Web.Mvc.Olap

Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Dim model As New OLAP101Model()
        model.Data = ProductData.GetData(10000)
        Return View(model)
    End Function
End Class

                        </div>
                        <div class="tab-pane pane-content active" id="cTPGCClose" style="padding:0px!important;height:0px!important;">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- ビューの保存と読み込み -->
    <div>
        <h2>ビューの保存と読み込み</h2>
        <p>
            <b>viewDefinition</b>プロパティを使用すると、ビュー定義を保存および復元できます。次に例を示します。
        </p>

        <p></p>

        <div class="col-md-12">
            <h4>結果（ライブ） : </h4>
        </div>
        <div>
            <div class="col-md-4">
                @Html.C1().PivotPanel().Id("saveLoadViewsPivotPanel").CssStyle("height", "550px").ItemsSourceId("pivotEngine")
            </div>
            <div class="col-md-8">
                @Html.C1().PivotGrid().Id("saveLoadViewsPivotGrid").ItemsSourceId("saveLoadViewsPivotPanel")
                <Button Class="btn btn-primary" onclick="saveView()">ビューを保存</button>
                <Button Class="btn btn-primary" onclick="loadView()">ビューを読み込み</button>
                <p></p>
                <p>
                    あるいは、ユーザーが選択できる定義済みのビューのリストを作成できます。次に例を示します。
                </p>
                <ul id = "views" ></ul>
             </div>
        </div>
        <div Class="row">
            <div Class="col-md-12">
                <div>
                    <ul Class="nav nav-tabs" role="tablist">
                        <li> <a href = "#saveLoadHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li> <a href = "#saveLoadJS" role="tab" data-toggle="tab">JS</a></li>
                        <li> <a href = "#saveLoadCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li Class="active"><a href="#saveLoadClose" role="tab" data-toggle="tab">X</a></li>
                    </ul>
                    <div Class="tab-content">
                        <div Class="tab-pane pane-content" id="saveLoadHtml">
@@(Html.C1().PivotEngine().Id("pivotEngine").Bind(Model.Data) _
    .RowFields(Sub(pfcb) pfcb.Items("Country")) _
    .ColumnFields(Sub(cfcb) cfcb.Items("Product")) _
    .ValueFields(Sub(vfcb) vfcb.Items("Sales")))
@@Html.C1().PivotPanel().Id("saveLoadViewsPivotPanel").CssStyle("height", "550px").ItemsSourceId("pivotEngine")
@@Html.C1().PivotGrid().Id("saveLoadViewsPivotGrid").ItemsSourceId("saveLoadViewsPivotPanel")
&lt;button class="btn btn-primary" onclick="saveView()"&gt;ビューを保存&lt;/button&gt;
&lt;button class="btn btn-primary" onclick="loadView()"&gt;ビューを読み込み&lt;/button&gt;
&lt;ul id="views"&gt;&lt;/ul&gt;
                        </div>
                        <div Class="tab-pane pane-content" id="saveLoadJS">
// save And load views
var app = {
    viewDefs:    null
};
var saveLoadViewsPivotPanel = null;
Function InitialControls() {
    // save And load views

    // pre-defined views
    app.viewDefs = [
        {
            name: "Sales by Product",
            def: "{\"showColumnTotals\":2,\"showRowTotals\":2,\"defaultFilterType\":3,\"fields\":[{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"ID\",\"header\":\"ID\"},{\"dataType\":1,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Product\",\"header\":\"Product\"},{\"dataType\":1,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Country\",\"header\":\"Country\"},{\"dataType\":4,\"format\":\"d\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Date\",\"header\":\"Date\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Sales\",\"header\":\"Sales\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Downloads\",\"header\":\"Downloads\"},{\"dataType\":3,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Active\",\"header\":\"Active\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Discount\",\"header\":\"Discount\"}],\"rowFields\":{\"items\":[\"Product\"]},\"columnFields\":{\"items\":[]},\"filterFields\":{\"items\":[]},\"valueFields\":{\"items\":[\"Sales\"]}}"
        },
        {
            name: "Sales by Country",
            def: "{\"showZeros\":false,\"showColumnTotals\":2,\"showRowTotals\":2,\"defaultFilterType\":3,\"fields\":[{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"ID\",\"header\":\"ID\"},{\"dataType\":1,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Product\",\"header\":\"Product\"},{\"dataType\":1,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Country\",\"header\":\"Country\"},{\"dataType\":4,\"format\":\"d\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Date\",\"header\":\"Date\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Sales\",\"header\":\"Sales\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Downloads\",\"header\":\"Downloads\"},{\"dataType\":3,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Active\",\"header\":\"Active\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Discount\",\"header\":\"Discount\"}],\"rowFields\":{\"items\":[\"Country\"]},\"columnFields\":{\"items\":[]},\"filterFields\":{\"items\":[]},\"valueFields\":{\"items\":[\"Sales\"]}}"
        },
        {
            name: "Sales and Downloads by Country",
            def: "{\"showZeros\":false,\"showColumnTotals\":2,\"showRowTotals\":2,\"defaultFilterType\":3,\"fields\":[{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"ID\",\"header\":\"ID\"},{\"dataType\":1,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Product\",\"header\":\"Product\"},{\"dataType\":1,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Country\",\"header\":\"Country\"},{\"dataType\":4,\"format\":\"d\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Date\",\"header\":\"Date\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Sales\",\"header\":\"Sales\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Downloads\",\"header\":\"Downloads\"},{\"dataType\":3,\"format\":\"\",\"aggregate\":2,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Active\",\"header\":\"Active\"},{\"dataType\":2,\"format\":\"n0\",\"aggregate\":1,\"showAs\":0,\"descending\":false,\"isContentHtml\":false,\"binding\":\"Discount\",\"header\":\"Discount\"}],\"rowFields\":{\"items\":[\"Country\"]},\"columnFields\":{\"items\":[]},\"filterFields\":{\"items\":[]},\"valueFields\":{\"items\":[\"Sales\",\"Downloads\"]}}"
        },
        {
            name: "Sales Trend by Product",
            def: "{\"showZeros\": false, \"showColumnTotals\": 2, \"showRowTotals\": 2, \"defaultFilterType\": 3, \"fields\": [{ \"dataType\": 2, \"format\": \"n0\", \"aggregate\": 1, \"showAs\": 0, \"descending\": false, \"isContentHtml\": false, \"binding\": \"ID\", \"header\": \"ID\" }, { \"dataType\": 1, \"format\": \"\", \"aggregate\": 2, \"showAs\": 0, \"descending\": false, \"isContentHtml\": false, \"binding\": \"Product\", \"header\": \"Product\" }, { \"dataType\": 1, \"format\": \"\", \"aggregate\": 2, \"showAs\": 0, \"descending\": false, \"isContentHtml\": false, \"binding\": \"Country\", \"header\": \"Country\" }, { \"dataType\": 4, \"format\": \"yyyy \\\"Q\\\"q\", \"aggregate\": 2, \"showAs\": 0, \"descending\": false, \"isContentHtml\": false, \"binding\": \"Date\", \"header\": \"Date\" }, { \"dataType\": 2, \"format\": \"p2\", \"aggregate\": 3, \"showAs\": 2, \"descending\": false, \"isContentHtml\": false, \"binding\": \"Sales\", \"header\": \"Sales\" }, { \"dataType\": 2, \"format\": \"n0\", \"aggregate\": 1, \"showAs\": 0, \"descending\": false, \"isContentHtml\": false, \"binding\": \"Downloads\", \"header\": \"Downloads\" }, { \"dataType\": 3, \"format\": \"\", \"aggregate\": 2, \"showAs\": 0, \"descending\": false, \"isContentHtml\": false, \"binding\": \"Active\", \"header\": \"Active\" }, { \"dataType\": 2, \"format\": \"n0\", \"aggregate\": 1, \"showAs\": 0, \"descending\": false, \"isContentHtml\": false, \"binding\": \"Discount\", \"header\": \"Discount\" }, { \"dataType\": 2, \"format\": \"n0\", \"aggregate\": 1, \"showAs\": 0, \"descending\": false, \"isContentHtml\": false, \"binding\": \"Sales\", \"header\": \"Sales2\" }], \"rowFields\": { \"items\": [\"Date\"] }, \"columnFields\": { \"items\": [\"Product\"] }, \"filterFields\": { \"items\": [] }, \"valueFields\": { \"items\": [\"Sales\"]}}"
        }
    ]
    saveLoadViewsPivotPanel = wijmo.Control.getControl('#saveLoadViewsPivotPanel');

    // populate list of pre-defined views
    var viewList = document.getElementById('views');
    for (var i = 0; i &lt; app.viewDefs.length; i++) {
        var li = wijmo.createElement('&lt;li&gt;&lt;a href="#theView" index="' + i + '"&gt;' + app.viewDefs[i].name + '&lt;/a&gt;&lt;/li&gt;');
        viewList.appendChild(li);
    }

    // apply pre-defined views
    viewList.addEventListener('click', function (e) {
        var el = &lt;HTMLElement&gt;e.toElement;
        If (el.tagName == 'A') {
            var index = el.getAttribute('index');
            saveLoadViewsPivotPanel.viewDefinition = app.viewDefs[index].def;
        }
    });
};

// save/load view definition

// save view definition
function saveView() {
    if (saveLoadEngine && saveLoadEngine.viewDefinition)
        localStorage.setItem('viewDefinition', saveLoadEngine.viewDefinition);
};

// restore view definition
function loadView() {
    if (localStorage.getItem('viewDefinition'))
        saveLoadEngine.viewDefinition = localStorage.getItem('viewDefinition');
};
                        </div>
                        <div Class="tab-pane pane-content" id="saveLoadCS">
Imports C1.Web.Mvc.Olap

Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Dim model As New OLAP101Model()
        model.Data = ProductData.GetData(10000)
        Return View(model)
    End Function
End Class
                        </div>
                        <div Class="tab-pane pane-content active" id="saveLoadClose" style="padding:0px!important;height:0px!important;">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- Analyze huge Data -->
    <div>
        <h2> 巨大なデータの分析</h2>
        <p>
            この例では、PivotEngineコンポーネントはDataEngineサービスにバインドされます。 DataEngine Web APIは、OLAPコントロールがDataEngineサービスにバインドする際のデータ集約を担当します。 DataEngine Web APIは、多くのオープンソースおよび商用の分析データベースおよびライブラリで広く使用されている、カラム指向のデータモデルを使用しています。 DataEngineは、ほんの一瞬で最大で数億のレコードを処理できます。
            このモードでは、サービスURLがPivotEngineに提供される必要があります。
        </p>
        <p>
            DataEngine Web APIは、MVCアプリケーションとは別に、またはMVCアプリケーション内でホストすることができます。<br />
            DataEngine Web APIを個別に作成してホストするには、<a href="http://docs.grapecity.com/help/c1/aspnet-mvc/webapi/#DataEngine.html">Web APIヘルプ</a>を参照してください。<br />
            同じMVCプロジェクトでDataEngineを使用するには、次のことを確認してください。<br />
            <ul>
                <li>MVCアプリケーションはWeb APIをサポートする必要があります。</li>
                <li>GrapeCity NuGetソースからDataEngine Web APIをアプリケーションに追加します。</li>
                <li>データをインポートするには、Owin Startup.csクラスにDataEngineを登録します。 以下の例のStartup.csコードを参照してください。</li>
            </ul>
        </p>
        <p>
            この例では、PivotEngineコンポーネントをDataEngineデータに接続します。 PivotPanelコントロールとPivotGridコントロールは、PivotEngineにバインドされています。
            PivotPanelコントロールでビュー定義を変更できます。 集計されたデータはサービスから取得されます。 PivotGridコントロールは、集計されたデータを表示します。 PivotGridコントロール内のセルをダブルクリックすると、グリッドに表示される詳細な生データを見つけることができます。
        </p>
        <div Class="col-md-12">
            <h4> 結果（ライブ） :   </h4>
        </div>
        <div>
            <div Class="col-md-4">
                @(Html.C1().PivotEngine().Id("dataEngineE").BindService("~/api/dataengine/complex10") _
                    .RowFields(Sub(pfcb) pfcb.Items("Country")) _
                    .ColumnFields(Sub(cfcb) cfcb.Items("Product")) _
                    .ValueFields(Sub(vfcb) vfcb.Items("Sales")))

                @Html.C1().PivotPanel().ItemsSourceId("dataEngineE")
            </div>
            <div class="col-md-8">
                @Html.C1().PivotGrid().Id("dataEnginePivotGrid").ItemsSourceId("dataEngineE")
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#dataEngineEHtml" role="tab" data-toggle="tab">Index.cshtml</a></li>
                        <li><a href="#dataEngineECS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li><a href="#dataEngineEJS" role="tab" data-toggle="tab">Startup.vb</a></li>
                        <li class="active"><a href="#dataEngineEClose" role="tab" data-toggle="tab">X</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content" id="dataEngineEHtml">
@@(Html.C1().PivotEngine().Id("dataEngineE").BindService("~/api/dataengine/complex10") _
    .RowFields(Sub(pfcb) pfcb.Items("Country")) _
    .ColumnFields(Sub(cfcb) cfcb.Items("Product")) _
    .ValueFields(Sub(vfcb) vfcb.Items("Sales")))

@@Html.C1().PivotPanel().ItemsSourceId("dataEngineE")
@@Html.C1().PivotGrid().Id("dataEnginePivotGrid").ItemsSourceId("dataEngineE")
                        </div>

                        <div class="tab-pane pane-content" id="dataEngineECS">
Imports C1.Web.Mvc.Olap

Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View()
    End Function
End Class
</div>

                        <div class="tab-pane pane-content" id="dataEngineEJS">
Imports Owin
Imports Microsoft.Owin

&lt;Assembly: OwinStartupAttribute(GetType(Startup))&gt;
Partial Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        app.UseDataEngineProviders().AddDataEngine("complex10", Function() ProductData.GetData(100000))
    End Sub
End Class
</div>
                        <div class="tab-pane pane-content active" id="dataEngineEClose" style="padding:0px!important;height:0px!important;">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<script type="text/javascript">
    c1.documentReady(function () {
        if (window["InitialControls"]) {
            window["InitialControls"]();
        }
    });
</script>