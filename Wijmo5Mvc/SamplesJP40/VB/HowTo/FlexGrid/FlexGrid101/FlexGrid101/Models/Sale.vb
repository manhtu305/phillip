﻿Public Class Sale
    Public Property ID() As Integer
        Get
            Return m_ID
        End Get
        Set
            m_ID = Value
        End Set
    End Property
    Private m_ID As Integer
    Public Property Start() As DateTime
        Get
            Return m_Start
        End Get
        Set
            m_Start = Value
        End Set
    End Property
    Private m_Start As DateTime
    Public Property [End]() As DateTime
        Get
            Return m_End
        End Get
        Set
            m_End = Value
        End Set
    End Property
    Private m_End As DateTime
    Public Property Country() As String
        Get
            Return m_Country
        End Get
        Set
            m_Country = Value
        End Set
    End Property
    Private m_Country As String
    Public Property Product() As String
        Get
            Return m_Product
        End Get
        Set
            m_Product = Value
        End Set
    End Property
    Private m_Product As String
    Public Property Color() As String
        Get
            Return m_Color
        End Get
        Set
            m_Color = Value
        End Set
    End Property
    Private m_Color As String
    Public Property Amount() As Double
        Get
            Return m_Amount
        End Get
        Set
            m_Amount = Value
        End Set
    End Property
    Private m_Amount As Double
    Public Property Amount2() As Double
        Get
            Return m_Amount2
        End Get
        Set
            m_Amount2 = Value
        End Set
    End Property
    Private m_Amount2 As Double
    Public Property Discount() As Double
        Get
            Return m_Discount
        End Get
        Set
            m_Discount = Value
        End Set
    End Property
    Private m_Discount As Double
    Public Property Active() As Boolean
        Get
            Return m_Active
        End Get
        Set
            m_Active = Value
        End Set
    End Property
    Private m_Active As Boolean

    Public Property Trends() As MonthData()
        Get
            Return m_Trends
        End Get
        Set
            m_Trends = Value
        End Set
    End Property
    Private m_Trends As MonthData()
    Public Property Rank() As Integer
        Get
            Return m_Rank
        End Get
        Set
            m_Rank = Value
        End Set
    End Property
    Private m_Rank As Integer
    Private Shared Function GetTrends(rand As Random) As IEnumerable(Of MonthData)
        Dim TrendsList As List(Of MonthData) = New List(Of MonthData)()
        For x As Integer = 0 To 11
            Dim Trend As New MonthData
            Trend.Month() = x + 1
            Trend.Data = rand.[Next](0, 100)
            TrendsList.Add(Trend)
        Next
        Return TrendsList.ToArray()
    End Function

    ''' <summary>
    ''' Get the data.
    ''' </summary>
    ''' <param name="total"></param>
    ''' <returns></returns>
    Public Shared Function GetData(total As Integer) As IEnumerable(Of Sale)
        Dim countries = New String() {"US", "UK", "Canada", "Japan", "China", "France",
            "German", "Italy", "Korea", "Australia"}
        Dim products = New String() {"Widget", "Gadget", "Doohickey"}
        Dim colors = New String() {"Black", "White", "Red", "Green", "Blue"}
        Dim rand = New Random(0)
        Dim dt = DateTime.Now
        Dim list = Enumerable.Range(0, total) _
        .[Select](Function(i)
                      Dim country = countries(rand.[Next](0, countries.Length - 1))
                      Dim product = products(rand.[Next](0, products.Length - 1))
                      Dim color = colors(rand.[Next](0, colors.Length - 1))
                      Dim [date] = New DateTime(dt.Year, i Mod 12 + 1, 25, i Mod 24, i Mod 60, i Mod 60)

                      Dim SaleRec As New Sale
                      SaleRec.[ID] = i + 1
                      SaleRec.[Start] = [date]
                      SaleRec.[End] = [date]
                      SaleRec.Country = country
                      SaleRec.Product = product
                      SaleRec.Color = color
                      SaleRec.Amount = rand.NextDouble() * 10000 - 5000
                      SaleRec.Amount2 = rand.NextDouble() * 10000 - 5000
                      SaleRec.Discount = rand.NextDouble() / 4
                      SaleRec.Active = (i Mod 4 = 0)
                      SaleRec.Trends = GetTrends(rand)
                      SaleRec.Rank = rand.[Next](1, 6)
                      Return SaleRec
                  End Function)
        Return list
    End Function
End Class

Public Class MonthData
    Public Property Month() As Integer
        Get
            Return m_Month
        End Get
        Set
            m_Month = Value
        End Set
    End Property
    Private m_Month As Integer
    Public Property Data() As Double
        Get
            Return m_Data
        End Get
        Set
            m_Data = Value
        End Set
    End Property
    Private m_Data As Double
End Class

