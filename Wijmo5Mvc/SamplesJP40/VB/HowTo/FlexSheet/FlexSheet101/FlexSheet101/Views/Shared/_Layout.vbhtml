﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title</title>
    <!-- styles -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="@Url.Content("~/Content/app.css")" />

    <!-- jQuery and Bootstrap scripts -->
    <script src="https://code.jquery.com/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- App scripts -->
    <script src="@Url.Content("~/Scripts/app.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jszip.min.js")" type="text/javascript"></script>

    @Html.C1().Resources("default")
    @Html.C1().FlexSheetResources()
</head>
<body>
    @RenderBody()
</body>
</html>
