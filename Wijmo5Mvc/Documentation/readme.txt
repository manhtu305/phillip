================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.288/4.5.20202.288)		Drop Date: 08/06/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.438/4.5.20202.438

Samples
    [456029] Vertical Scroll Bar of Right Panel is shown in middle of FileManager control.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.287/4.5.20202.287)		Drop Date: 07/28/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.438/4.5.20202.438

FileManager
    [431862] Some inconsistent behavior are observed when screen size is no enough to displayed the control.


================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.286/4.5.20202.286)		Drop Date: 07/23/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.438/4.5.20202.438

All
    Upgrade DataEngine dlls to build EN 154/JP 155.

Samples
    [444975] GCDTLicenses.xml file is shown with error sign in JP core CloudFileExplorer.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.285/4.5.20202.285)		Drop Date: 07/14/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.438/4.5.20202.438

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.284/4.5.20202.284)		Drop Date: 07/13/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.438/4.5.20202.438

All
    Upgrade to use C1.DataEngine 153.

Licenses
    [444310] 'A valid license can't generate for xxx' error is observed in Viewer control when creating NonCore project with FlexViewer, Olap, DataEngine if no/invalid key in GCLM and valid key is activated in LicenseActivation.
    [444725] 'Internal server error' is occurred for all api services by unchecking 'Self host application' checkbox when GCLM has SE license key added.

Samples
    [444980] GCDTLicenses.xml file is not involved in core CloudFileExplorer sample in ENG.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.283/4.5.20202.283)		Drop Date: 07/10/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.438/4.5.20202.438

All
    Upgrade Winforms dlls from 20202.428 to 20202.438.

ProjectTemplates
    [444623] Request to remove core VSIX that support to VS 15 at both EN/JP.

Samples
    [442338] Check-boxes are displayed with inconsistent align in some FlexSheet and MultiRow samples.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.282/4.5.20202.282)		Drop Date: 07/07/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.428/4.5.20202.428

All
    [425665] Update JP xmlDocument.

Licenses
    [443918] 'ComponentOne ASP.NET MVC Edition' and 'ComponentOne Studio Enterprise' content is showed at GCLM when creating MVC core project with NetCore 3.0 version at JP.
    [436651] License Error occur when create non-core MVC template with WebApi using certain scenario although Sutdio Enterprise key is activated in GCLM.
    [443589] Evaluation message is displayed when Trial License key of New License(GCLM) is activated if C1 AspNet MVC (non-core) project is created with WebAPI DataEngine service.

Samples
    [442027] 'Package is not compatible with netcoreapp1.x' errors are observed when creating the MVC core project with NetCore 1.0 or 1.1 version.
    [442338] Check-boxes are displayed with inconsistent align in some FlexSheet and MultiRow samples.
    [440346] Error occur when run core CloudFileExplorer JP sample.

FileManager
    [440375] [Wijmo 5 MVC][CloudFileExplorer] ScreenShot of FileManager in CloudFileExplorer is not updated as new design.
    [434621] Applying C1MVC's CSS (@Html.C1().Styles()) causes Bootstrap4's navigation bar to be displayed incorrectly.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.281/4.5.20202.281)		Drop Date: 07/02/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.428/4.5.20202.428

Licenses
    [436651] License Error occur when create non-core MVC template with WebApi using certain scenario although Sutdio Enterprise key is activated in GCLM.
    [443589] Evaluation message is displayed when Trial License key of New License(GCLM) is activated if C1 AspNet MVC (non-core) project is created with WebAPI DataEngine service.
    [443384] Different behavior are observed between Trial license is expired and removed the Trial license when system date is changed to + 30 days .

ProjectTemplates
    [432004] 500 internal server error is occurred when using .NetFramework to <=4.5.1 version by checking with FlexViewer and C1Pdf checkboxes (JA).
    [441671] 'Unable to find version 4.5.20202.428 of packages C1.DataEngine' error is observed when creating the MVC NonCore project by checking with C1 WebAPI DataEngine (JA).

Samples
    [442372] Console error is occurred at browser when exporting with PDF format at Purchase slip after adding some value at 'Remarks' column.
    [442023] Sample restore is failed and errors are occurred when build the sample 'CloudFileExplorer(JP Non Core)'.
    [442337] 'Microsoft.AnalysisServices.AdomdClient' dlls is included twice at 'Samples\VB\HowTo\OLAP\Olap101' of JP and conflict version is observed at WebAPI noncore project (CS/VB).
    [442676] Error occurs when navigate to a folder using link in Path Folder if that folder is opened by double clicking and that folder has child folder.
    [443546] Request to localize and add description in some pages of JP MVCExplorer.
    [442377] Drop-down's appearance in dialog is inconsistent when click on 'Settings' icon to change chart type in 'Custom Tile' page.
    [443544] 'Unable to get property 'columns' of undefined or null reference' error is occurred when run the 'Financial' sample under the path '~\Samples\CS'.

FileManager
    [443315] FileManager is not shown properly if click on ListView and Detail when scroll bar is shown in Right Panel.
    [422127] Extra horizontal scroll bar is appeared when run the sample 'CloudFileExplorer'.

FlexSheet
    [443529] FlexSheet InsertRows method insert rows at incorrect place.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.280/4.5.20202.280)		Drop Date: 06/27/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.428/4.5.20202.428

All
    [425660] [PrepairCC] JP localization.
    [442197] When 'KeyActionTab' property is set to 'None', KeyAction move across.
	
Samples
    [441935] Displayed files are closer than before and spaces between files are shrunk when click on list view icon.
    [442338] Check-boxes are displayed with inconsistent align in some FlexSheet and MultiRow samples.
    [442023] Sample restore is failed and errors are occurred when build the sample 'CloudFileExplorer(JP Non Core)'.
    [442337] 'Microsoft.AnalysisServices.AdomdClient' dlls is included twice at 'Samples\VB\HowTo\OLAP\Olap101' of JP.
    [442369] 'Syntax error' error is occurred when navigate to 'MultiSelectListBox' control.
    [442488] Remove support netcore1.0.
    [442607] Correct reference of C1.DataEngine.Ja.
    [442557] Error occur when sort CellMarker SparkLine Column in Custom Cells Page.
    [442182] Checked Items are not shown in list at initialize state although some items are checked in control.
    [441857] Incorrect C1WebAPI call for CloudType LocalStorage.
    [442507] After resizeing the FlexGrid column ColumnPicker can't work correctly.

FlexGrid
    [440605] Filer Dialog is not shown at correct position with FF browser.
    [440640] BigCheckBox is worked as true eventhough it is set as false in OverView page of FlexGrid.

ProjectTemplates
    [441671] 'Unable to find version 4.5.20202.428 of packages C1.DataEngine' error is observed when creating the MVC NonCore project by checking with C1 WebAPI DataEngine.
    [432004] 500 internal server error is occurred when using .NetFramework to <=4.5.1 version by checking with FlexViewer and C1Pdf checkboxes
    [442356] Remove support netcore1.0.
    
FileManager
    [438361] [Wijmo 5 MVC][FileManager][Regression] 'OneDrive' service is not worked and 500 Internal Server Error is displayed.
    [423005] Request to display vertical scroll bar in right panel when many Folders/Files is shown in right panel that is enough to display the scroll bar.
    [422157] Request to localize in message and button title of some alert in FileManager.

Licenses
    [442012] License error do not occurs and it works as license although license.licx file or '331cf6cd-b73c-429f-ba79-fa2f85eebd68.gclicx' file or both are deleted in project.
    [441598] Unlike with EN, 'Invalid key activated' message is observed when creating MVC or WebAPI core project with GCLM to NoKey at JP environment.
    [434141] Trial License key of New License(GCLM) do not work as expected.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.279/4.5.20202.279)		Drop Date: 06/23/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.428/4.5.20202.428

FlexGrid
    [441649] 'C1.AspNetCore.Mvc' nuget version is '55555' instead of '278' in 'C1WebWijmo.AspnetMvc-nc30_4.5.20202.278' builds.
    [441637] Column header and row header can't show and 'Uncaught TypeError: cannot read property 'cells' of undefined' console error is observed at FlexGrid control.

Samples
    [403870] Inconsistent values are displayed in 'Country' column of 'SampleExtensionControls(Core)' sample in certain scenario.
    [440567] Column can't drag to GroupPanel of MultiRow Non-Core project.
    [439010] 'Resource' is not declared. It may be inaccessible due to its protection level.' error is observed after running Olap101 and FlexChart101 samples.

Scaffold
    [438807] Request to change 'PinningType' property instead of 'AllowPinning' property in Scaffold UI.

FileManager
    [439551] Upload and MoveFile dialog are not shown properly in FileManager.
    [441763] 'CloudFileExplorer(Core)' sample restore is failed and error displays.

MultiSelect
    [441387] Request to support �FilterInputPlaceholder� property.    

ProjectTemplates    
    [441670] All vsix files under 'ProjectTemplates' folder of 4.0 build version can't install and 'Install Failed' error is displayed.
    [441671] 'Unable to find version 4.5.20202.428 of packages C1.DataEngine' error is observed when creating the MVC NonCore project by checking with C1 WebAPI DataEngine.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.278/4.5.20202.278)		Drop Date: 06/17/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20202.428/4.5.20202.428

All
    Upgrade Winforms dlls from 20201.424 to 20202.428.
    [440388] 'ClientDoc/JsIntellisense' folders and 'Third party licenses.txt' file are missing in both MVC 4.0 and 4.5 builds (both Eng and JP).
    [434144] [Document] Update links in xml docs.

CollectionView
    [440637] 'SortNulls' property is not worked correctly when sort null column.

Samples
    [352285] Range Selector is remained although Chart Type is changed from 'Retracements' to others.
    [424975] 'FlexGrid101(Core)' sample restore is failed and can't run with VS 19 and warning signs are displayed with VS 17.
    [439151] New features sample pages are hidden in JP non-core MVCExplorer.
    [439013] 'Error: ** Assertion failed in Wijmo: Unknown property 'pinningType'.' error is displayed when 'PinningType' property(Both/ColumnRange/SingleColumn) is set in 'FlexSheet' and PivotGrid controls.
    [439367] 'ClickAction' property can't set in some Input DropDown controls (AutoComplete, ComboBox, InputTime, Menu and MutliSelect).
    [265386]The "MoveToPage" property of FlexGrid can't work after setting "ReadActionURL" and "DisableServerRead(True)" of FlexGrid.
    [439370] 'ColumnBaseBuilder<Cell, CellBuilder>.ShowDropDown(bool)' is obsolete: 'Please use DataMapEditor instead.' warning sign is observed at 'MultiRowExplorer\Models\LayoutDefinitions.cs'.
    [438229] MVC and WebAPI dll version is incorrect and used 'xxxxx.55555' in MVC dll and 'xxxxx.44444' in WebAPI dll in some samples.
    [440366] Displayed FlexSheet control's height is very small and can't display properly although row count is added in unbound sheet using Html helper.
    [440370] New Sample page for Wijmo.grid.cellmaker is not involved in Enhancements list page.
    [440273] Checkboxes are shown as botton align in FlexGrid sample pages.
    [440417] Request to update compatible code for IE browser because error occurs in GroupPanel sample page when groupDecriptionCreator' is set true.
    [440465] Texts are disppeared in header and cell when selector is set in Column.
    [439486] Extra 'Layout.cshtml' view file is observed.
    [440547] When run the PeriodicTable sample project If click on PeriodicTable ‘ ” ’ extra code is shown in browser.
    [440578] Any filter value can't shown at Filter dialog of Value column when setting with DisableServerRead property to false value.
    [440567] Column can't drag to GroupPanel of MultiRow Non-Core project.
    [440648] Error occur when cells are created using CellMarker.
    [439401] Some warning signs are observed when building the MultiRowExplorer sample.
    [441077] Column header and data becomes misaligned when select the columns until the scrolling the horizontal scroll bar if 'ShowSelectedHeaders'property is set to 'None'.
    [440506] In all browser, When change 'Material’ theme in some samples selection is shown incorrectly on navigation bar and Vertical Scroll bars are displayed in side menu of some Explorer samples
    [439456] Vertical and horizontal scrollbar is displayed while dragging the tile and dropped/cancel the tile's dragging if 'RTL' property is set in control.

FileManager
    [438361] Error occur when run LocalStorage page of FileManager.
    [439458] Alignment of Folder and File is not same in Left Panel when click on ListView.
    [438355] Column bar of Name. Size, Date Modified are missing when click on the folder/files using certain scenarios.
    [438375] Selection can't be displayed properly in FileManager when click Folder/File in SmallIcon view.
    [427258] Behavior of folder tree view is not good.
    [439564] Left Panel is not updated  if open Folder more than once when Opened Folder have only File or Empty.
    [440031] Dots are shown in Folder path of FileManager after run project.
    [440375] ScreenShot of FileManager in CloudFileExplorer is not updated as new design.
    [439489] ModifiedDate and Size is not shown correctly in Files of GoogleDrive.
    [434209] Previous state(files) is displayed again in right panel after made an action and click on parent folder from left panel.
    [434205] Inconsistent behavior is observed when a file is moved from parent folder to child folder using 'OneDrive' service.

License
    [436651] License Error occur when create non-core MVC template with WebApi using certain scenario although Sutdio Enterprise key is activated in GCLM.
    [440098] About box(license dialog box) for Trial message can't be popup when call the scaffoldUI (Add or Insert or Update) when it is call the first time.
    [440099] Message of license dialog box for scaffold is incorrectly displayed in Trail Expired state.
    [440116] 'License Information missing' alert box is displayed when controls is added/inserted from scaffold into core project.
    [434141] Trial License key of New License(GCLM) do not work as expected.
    [433601] Webform controls works as License although Invalid License key(e.g ComponentOne for Windows Form/Desktop) is activated and no license keys are activated in Old License.
    [434484] Unlike the ENG environment, GCLM dialog box doesn't popup when build the project although C1 nugets are added into project in JP environment.
    [436226] Some of the scaffold vsix template can't installed and checkbox of VS 2019 is dim in vsix dialog although VS 2019 is already installed.
    [434459] License Exception occurs although Old license key(RTL license) is activated in core project.
    [438189] Unlike AH keys from Old license, 'ComponentOne ASP.NET MVC' keys is working properly in MVC controls.
    [440608] Found different behavior of license in About box between Webform project and other projects (e.gWPF/Winform etc).

FlexGrid
    [438792] Checkbox that is using 'bigCheckBoxes' property to 'true' value can't drag and drop to GroupPanel.
    
================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.277/4.5.20202.277)		Drop Date: 06/07/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.680
Winform 4.0.20201.424/4.5.20201.424

All
    [425655] Merge wijmo 680.

Samples
    [403870] Inconsistent values are displayed in 'Country' column of 'SampleExtensionControls(Core)' sample in certain scenario.
    [435298] Showing filter value can't retain properly when clicking filtering dialog again and again by setting with 'DisableServerRead' property to 'false' value.
    [378467] Date value is incorrect exported when export with excel in 'MultiRow -> Transfer Slip' sample page.
    [433440] Data from MultiColumnComboBox drop down are not refreshed in certain scenarios.
    [438365] [RazorPagesExplorer] License error is occurred when run the sample RazorPagesExplorer.
    [438376] Error displays and cannot run properly the 'DashboardDemo' sample.
    [438814] Blank data is shown in Filter Dialog of DateTime Columns when 'DisableServerRead' is set as false in FlexGrid.
    [438999] Add new sample for Wijmo.grid.cellmaker into MvcExplorer sample.
	[439010] 'Resource' is not declared. It may be inaccessible due to its protection level.' error is observed after running Olap101 and FlexChart101 samples.

FlexGid	
    [438749] Tooltip description of 'allowPinning' property is not displayed as Enum value.
    [438350] FlexGrid can't be displayed as paging size when page size is set in item source collection.
    [438528] Sorting for descending does not work in all data types.
    [431827] Add new modules  Wijmo.grid.cellmaker to MVC for using with javascript and make FlexGrid cell Template support CellMaker.

FileManager
    [438230] Console error occurs when run the CloudFileExplorer sample.
    [438221] Request to support CloudFileExplorer sample in JP sample dropped.

MultiSelect
    [438990] Request to add new feature properties in 'Input > MultiSelect > Overview' sample page of 'MvcExplorer(Core)' sample.

FlexViewer
    [439261] The sample design is inconsistently displayed after running 'Samples\CS\FlexViewerExplorer' sample project.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20202.276/4.5.20202.276)		Drop Date: 05/26/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.663
Winform 4.0.20201.424/4.5.20201.424

All
    [424879] Merge wijmo 663 to Dev for start 2020v2.
    [426733] Fix tsdocx build tool.
    Upgrade Winforms dlls from 20201.416 to 20201.424.

Selector
    [431821] Add new Selector extender which add checkboxes to rows header or a column of FlexGrid for multiple rows selection.
    [431823] Add new BooleanChecker extender which add checkboxes to header or group headers of boolean FlexGrid.Column for checking all.

MultiSelectListBox
    [426849] Add new MultiSelectListBox control that is used as a drop-down for the MultiSelect control.
    [426851] Add new sample page for MultiSelectListBox control.
	
FlexGid
    [426822] Add new PinningColumn and PinnedColumn events for FlexGrid.
    [426820] Change AllowPinning property type from boolean to an enumeration in FlexGrid.
    [426823] Add new sample for the new PinningColumn and PinnedColumn events of FlexGrid.
    [426870] Add samples for 2 new properties InnerText/InnerTextStyle of FlexPie.
    [426793] Deprecate Column.ShowDropDown property in FlexGrid.
    [426794] Add new DataMapEditor property to the Column class instead of Column.ShowDropDown in FlexGrid.
    [426804] Add new BigCheckboxes property that makes checkboxes in boolean columns of FlexGrid wider.
    [426817] Add new sample page for BigCheckboxes property of FlexGrid.
    [426865] Add more new members or parameters �catalog�,�user�,�password� for ItemSource as CubeService in PivotEngine.
    [426826] Add new PreserveWhiteSpace property for FlexGrid.
    [426803] Add new sample page or update all related sample pages for new DataMapEditor and deprecated Column.ShowDropDown properties of FlexGrid.
    [431822] Add new sample page for new Selector extender.
    [431824] Add new sample page for new BooleanChecker extender.

FileManager
    [421143] Should correct the layout of control like design.
    [426327] Add support local storage for control.
    [426325] Add APIs support local storage.
    [426206] Research support local storage.
    [425675] Add sample support local storage.
    [427051] Add support thumnail view.
    [429452] Add API support detail view.
    [425674] Add detail view.
    [430616] Add support simple list view.
    [425675] Add sample support local storage.
    [425308] Sign in page of GoogleDrive is appeared in browser when GoogleDrive is set in FileManager and run core CloudFileExplorer sample.
    [422425] Request to support 'DoubleClick' action to open folder in FileManager.
    [431452] Remove dependency of the control to bootstrap.
    [434844] Apply theme color for selected item of treeview.
    [434845] Apply theme color for context menu on mouse hovered.
    [435442] Add css to support theme for icons svg set.
    [431864] Arrow, area path box and search text box do not apply Theme.
    [431452] Remove dependency of the control to bootstrap.
    [431697] Some theme is not applied properly to FileManager.

MultiSelect
    [426845] Add new ShowFilterInput property for MultiSelect.
    [426846] Add new sample for new ShowFilterInput property of MultiSelect.
    [426847] Add new CheckOnFilter property for MultiSelect.
    [426850] Add sample for new CheckOnFilter property of MultiSelect.

Sample
    [415511] [Wijmo 5 MVC][FlexSheetExplorer] Error occurs when save the files with any format in 'Remote Load/Save' sample page of FlexSheetExplorer(non-core) if sample is opened with VS 2019 .
    [413243] Unobtrusive validation error border is not observed when inserting invalid value at DateTime column.
    [352285] Range Selector is remained although Chart Type is changed from 'Retracements' to others.
    [265386] The "MoveToPage" property of FlexGrid can't work after setting "ReadActionURL" and "DisableServerRead(True)" of FlexGrid.
    [395553] JavaScript error is occurred when delete the sheet and make certain scenarios.
    [383905] Different behavior between Wijmo 5 and MVC are observed when press Tab key and space bar after open the filter.
    [336979] "Select Mouse Mode" checkbox, 'WARNING: "selectMouseMode" has been deprecated; please use "mouseMode" instead' error occurs when check on 'Select Mouse Mode' checkbox.
    [355777] Request to localize in Placeholder value of DashboradDemo sample for JP samples drop.
    [420386] Some of the columns are missing when 'AutoGenerateColumns' setting are changed.
    [351120] Data of 'Customer' column are disappeared after sorting the column.
    [404941] Warning messages occur after build SampleExtensionControls non core sample.
    [378467] Date value is incorrect exported when export with excel in 'MultiRow -> Transfer Slip' sample page.    
    [429976] Missing many masks in ASP.NET MVC documentation if we compare Wijmo documentation.
    [356123] JP localization - FlexChart101.
    [356124] JP localization - FlexGrid101.
    [356125] JP localization - Input101.
    [356127] JP localization - Olap101.
    [356126] JP localization - FlexSheet101.
    [431820] Add sample for new groupDescriptionCreator property of GroupPanel.
    [424975] 'FlexGrid101(Core)' sample restore is failed and can't run with VS 19 and warning signs are displayed with VS 17.
    [431818] Add new sample page batch-updates which using new deferCommits property and commitEdits, cancelChanges methods of ODataCollectionView.
    [433420] Error is occurred in 'Spinners' page that uses 'https://preloaders.net/preloaders/159/Rounded%20blocks.gif' link.
    [435118] When run the ExcelImportExport sample project ‘)’ extra code is shown in browser.
    [435321] Extra file(licenses.licx) is displayed with cross sign under properties when opened with VS although that license file do not exist under properties folder.
    [427227] Collect all changes and new features to update for Enhancement List page.
    [434965] Can't effect in 'Allow multi select' checkbox by selecting/deselecting on 'Multi Select' icon of 'Slicer' control.

FlexPie
    [426869] Add 2 new properties InnerText/InnerTextStyle for FlexPie.

CollectionView
    [426828] Deprecate SortNullsFirst property of CollectionView.
    [426829] Add new SortNulls property instead of sortNullsFirst for CollectionView.
    [426830] Add new or update sample for new SortNulls and deprecated SortNullsFirst properties of CollectionView.
    [431816] Add new deferCommits property for ODataCollectionView that support batch updates.

Popup
    [426863] Find out and add several new values to the PopupTrigger enumeration.
	
Dropdown
    [426866] Add new DropDown.ClickAction property to customize click actions.
    [426868] Add sample for new DropDown.ClickAction property.

Dashboard Layout
    [400643] Extra space in background are appeared and control becomes flicker when dragging the tile to right side of control if right to left direction is set.

FlexChart
    [426790] Add new BreakEven chart series type for chart.analytics in FlexChart.
    [426802] Add new sample page for new BreakEven chart series type of chart.analytics in FlexChart.

GroupPanel
    [431819] Add new groupDescriptionCreator property for GroupPanel that allows customizing GroupDescription for grouped column.


================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.275/4.5.20201.275)		Drop Date: 05/13/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.663
Winform 4.0.20201.424/4.5.20201.424

All
    Upgrade Winforms dlls from 20201.416 to 20201.424.

ProjectTeplates
    [434887] Extra string (like 'if$') is included in 'packages.config' file when creating MVC Non-Core (VB) project with Standard Template.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.274/4.5.20201.274)		Drop Date: 05/10/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.663
Winform 4.0.20201.416/4.5.20201.416

Sample
    [433186] [SampleExtensionControls] 'MultiColumnComboBox' drop down is displayed inconsistently and occupied the full size of browser width.

FileManager
    [433417] [Wijmo 5 MVC][FileManger][Regression] Upward button is not shown infront of Path Folder.

FlexGrid
    [431421] rowEdiEnded gets triggered on cell edit end.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.273/4.5.20201.273)		Drop Date: 04/29/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.663
Winform 4.0.20201.416/4.5.20201.416

FileManager
    [427247] Control display not good in Ipad.
    [433050] Error occur when Delete Folder eventhough Delete success in core CloudFileExplorer.
    [432127] Request to update the sample code from Initial path to RootFolder and ContainerName in CloudFileExplorer sample.

Sample
    [433186] [SampleExtensionControls] 'MultiColumnComboBox' drop down is displayed inconsistently and occupied the full size of browser width.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.272/4.5.20201.272)		Drop Date: 04/24/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.663
Winform 4.0.20201.416/4.5.20201.416

Samples
    [431671] '0' value of 'AlternatingRowStep' property doesn't affect properly at FlexGrid control.
    [427010] [CloudFileExplorer] Recheck reference of API and MVC libraries.

FileManager
    [427247] Control display not good in Ipad.
    [417625] 'Move File/Download/Delete' functions can't work correctly using 'AWS' cloud storage account.
    [417651] 'Create Folder' function cannot work properly in certain scenarios.
    [417654] 'Move File' and 'Delete' functions work inconsistently in certain scenarios.
    [431839] Error occur if RootFooter is set when CloudType OneDrive Service is used.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.271/4.5.20201.271)		Drop Date: 04/17/2020
================================================================================================
Dependencies
------------
Wijmo 5.20201.663
Winform 4.0.20201.416/4.5.20201.416

FileManager
    [427247] Control display not good in Ipad.
    [423025] Data is shown when any character(invalid path) is set in parent folder name of Initial Path.
    [425337][Regression] Error occur when click firstly on right panel after run project.
    [417651] 'Create Folder' function cannot work properly in certain scenarios.
    [417654] 'Move File' and 'Delete' functions work inconsistently in certain scenarios.
ProjectTeplates
    [426667] [Wijmo 5 MVC][TransposedGrid][Razor] Need to add manually script and resources in '_Layout.cshtml' and '_ViewImports.cshtml' when add 'TransposedGrid' control in 'Razor' templates.

FlexViewer
    [424935] Position of viewer split button is changed.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.270/4.5.20201.270)		Drop Date: 04/08/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

ItemTemplates
    [430601] License error is observed when adding Pdf with local service.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.269/4.5.20201.269)		Drop Date: 04/08/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

ItemTemplates
    [430273] WF builds both 4.0 and 4.5.2 dll are included at MVC 4.5 standard template after adding itemTemplates 4.5 to project.
	
Samples
    [425693] 'Column Pinning' page is not included in 'Enhancements list' of 'MvcExplorer' sample.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.268/4.5.20201.268)		Drop Date: 04/03/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

FlexSheet
    [429688] 'Uncaught ReferenceError: isArray is not defined' error is occurred when add bound sheet in 'FlexSheet' control.
    [429715] 'Cannot set property 'visible' of undefined at onload' error is occurred when 'Visible' property and bound sheet is set in 'FlexSheet' control.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.267/4.5.20201.267)		Drop Date: 03/31/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

All
    [429424] Merge a changeset from wijmo to fix some bugs.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.266/4.5.20201.266)		Drop Date: 03/12/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.265/4.5.20201.265)		Drop Date: 03/12/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.264/4.5.20201.264)		Drop Date: 03/12/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

FlexGrid
    [423975] [FlexGrid for ASP.NET MVC] When the custom editor of the template column is specified in the HTML input element, the input keyboard is displayed for a moment on Chrome of Android smartphone, then disappears immediately and the edit mode ends.

Samples
    [424273] Some consistent behaviours are observed in core CloudFileExplorer.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.263/4.5.20201.263)		Drop Date: 03/10/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

Samples
    [424273] Some consistent behaviours are observed in core CloudFileExplorer.
    [422188] Request to update 'screenshot.png' image file for latest UI design in 'CloudFileExplorer' sample.
    [421827] Inconsistent behaviors are observed in 'Clear Buttons' and 'Material Design' sample pages.
    [409376] Check reference of Adomd.Client library.

ProjectTemplate
    [408829] Microsoft.AnalysisServices.AdomdClient dll is shown as warning sign in Reference when create 4.5 non-core project .

FileManager
    [423239] Files(e.g pdf, xlsx etc) which do not have folder as a parent are not shown in FileManager control.
    [422318] Add code to block edit functions (create/delete/move) files/folder on demo samples.
    [422994] Move file dialog and Upload file dialog should be closed when other actions are did (i.e other buttons are clicked or context-menu is opened).
    [423723] Request to display warning alert if click on 'OK' button without choosing any file to upload.
    [424563] Error occur when click on Folder/File in rightpanel in IE,Edge&FireFox browsers.

FlexGrid
    [423975] [FlexGrid for ASP.NET MVC] When the custom editor of the template column is specified in the HTML input element, the input keyboard is displayed for a moment on Chrome of Android smartphone, then disappears immediately and the edit mode ends.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.262/4.5.20201.262)		Drop Date: 03/06/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

Samples
    [423013] Request to add WindowsAzure.Storage dll in Reference of CloudFileExplorer sample.
    [421827] Inconsistent behaviors are observed in 'Clear Buttons' and 'Material Design' sample pages.
    [422930] Request to remove 'C1.Web.Mvc.TransposedGrid' dll and related Transposed grid's Views and Controllers folders in 'NonCore\MvcExplorer' sample of JP.
    [403045] Unlike the Wijmo 5 FlexGrid control, edit icon doesn't remain visible at all time in row header when clicking checkbox value.
    [423024] Exported UI is different with previous build at summary footer of 'Purchase Slip' page when exporting with PDF type.
    [422993] 'beta' versions are used in 'API' packages for 'CloudFileExplorer(Core)' sample.
    [424020] Request to update readme file of TransposedGridExplorer in both non-core and core.
    [422872] TransposedGridExplorer sample can not run properly in both non-core and core.

FileManager
    [422382] Uploaded file is entered in first child folder of selected folder when extract that folder.
    [422167] Displayed buttons are changed when extract and collapse the folder if that folder hasn't any folder inside.
    [424027] License error occur when build project with FileManager that used WebApi (Cloud) if AH license key is activated.
    [422155] 'Cancel' should be displayed instead of 'None' in 'Upload a file to cloud' alert box.
    [421143] Should correct the layout of control like design.
    [423647] File is disappeared after move file from one folder to another folder in FileManager.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.261/4.5.20201.261)		Drop Date: 03/04/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20201.416/4.5.20201.416

All
    Upgrade Winforms dlls from 20193.398 to 20201.416.

Samples
    [421613] The background color is missing at 'Total Profit' and 'Profit Rate' column after exporting with PDF format.
    [422873] Request to add 'screenshot.png' in TransposedGridExplorer sample Folder.
    [422930] Request to remove 'C1.Web.Mvc.TransposedGrid' dll and related Transposed grid's Views and Controllers folders in 'NonCore\MvcExplorer' sample of JP.
    [422922] Errors messages are observed at 'CS\ASPNETCore\MvcExplorer\' samples.
    [422872] TransposedGridExplorer sample can not run properly in both non-core and core.
    [423003] Name of Initial Folder is not shown correctly in Path Folder.
    [423012] Error occur if click on Parent Folder when 'AWS' Service is used in FileManager.
    [423014] Control works opposite when 'CloudType' property is set 'OneDrive' and 'GoogleDrive'.
    [422912] Add Column Pinning Sample.

FileManager
    [422155] 'Cancel' should be displayed instead of 'None' in 'Upload a file to cloud' alert box.
    [421861] Alert Box of FileManager is displayed at incorrect position after maximize/minimize tile of DashboardLayout when FileManager is placed in Dashboard layout.
    [422172][IE] Folders/Files are not shown correctly in right panel when Folder name is create as other languages (e.g JP, CN languages etc).
    [422998] Control is displayed in middle of 'Sample/Source/Documentation' tab.
    [422157] Request to localize in message and button title of some alert in FileManager.
    [422996] Request to Update Link in Path Folder after navigate back to Folder when click on the Link in Path Folder.
    [422424] Vertical scroll bar should be displayed in Move File dialog when there is many files which is enough to display the scroll bar.
    [423005] Request to display vertical scroll bar in right panel when many Folders/Files is shown in right panel that is enough to display the scroll bar.
    [422192] Horizontal scroll bar of right panel is not shown correctly.
    [423239] Files(e.g pdf, xlsx etc) which do not have folder as a parent are not shown in FileManager control.
    [422151] Request to display confirmation alert box for delete process.
    [422141] Focus is entered in wrong position when press on buttons, text box and extra space.

ProjectTemplates
    [422980] Error occurs when create the C1 AspNet MVC 5 Application(VB) with TranspsedGrid dll in JP environment.
    [421957] WF dlls can not be restored properly and error occur when create WebAPI 4.0/4.5 and MVC 4.5 project with WF dlls.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.260/4.5.20201.260)		Drop Date: 02/27/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.646
Winform 4.0.20193.398/4.5.20193.398

FileManager
    [420424][CloudFileExplorer] 500 Internal server error is occurred and cloud service drop-down is missing.
    [421819] Error occur when create FileManager control with 4.0 project.
    [421950] License error is occurred when run the sample 'CloudFileExplorer'.
    [421960] Can not create new folder.
    [421983] '[object Object]' are shown in unsearch file when searching in Search of FileManager control.
    [421992] Displayed files are not updated in right panel when click on folder if that folder have been opened once.
    [422122] Search keyword should not be case sensitive.
    [421982] Can't search by pressing 'Enter' key from keyboard for 'Search' function.
    [422163] 'Upload a file to cloud' alert box is not updated and uploaded file name is still displayed in 'You are uploading file :' text.
    [422167] Displayed buttons are changed when extract and collapse the folder if that folder hasn't any folder inside.
    [422192] Horizontal scroll bar of right panel is not shown correctly.
    [422234] Errors display when cancel to create the new folder.
    [421983] '[object Object]' are shown in unsearch file when searching in Search of FileManager control.
    [421992] Displayed files are not updated in right panel when click on folder if that folder have been opened once.
    [422122] Search keyword should not be case sensitive.
    [422157] Request to localize in message and button title of some alert in FileManager.

Samples
    [421415] Some of the dll can't restore and error occur when restore non-core MVCExplorer(non-core) sample.
    [421947] License error occur when navigate to TransposedGrid page  in non-core MVCExplorer sample.
    [422017] Request to localize 'Auto Filling' text in side menu.
    [421457] Request to create FileMananger sample for Core like other new features.
    [421827] Inconsistent behaviors are observed in 'Clear Buttons' and 'Material Design' sample pages.
    [421512] Warning Message is displayed when open index.html file after build Financial sample.
    [421679] Add show source code, document for CloudFileExplorer.
    [421987] Warning messages are observed after building 'CS\ASPNETCore\MvcExplorer\MvcExplorer.Core30.2019.sln' sample.
    [421416] TransposedGrid nuget is missing and error occur when run core MVCExplorer sample.
    [420447] Request to use SortType instead of AllowSorting in some FlexGrid sample page.
    [422493] [TransposedGrid] Move to a separated sample.

ProjectTemplates
    [422103] MVC '4.5' dlls are included instead of '4.0' dlls when creating the projects using MVC Non Core templates(Standard, AJAX, Model, SpreadSheet) by choosing 'Target .NET Framework <= 4.5.1'.
    [421411] [Wijmo 5 MVC][Template] Request to add checkbox for TransposedGrid dll/nuget in templates(both core and non-core).
    [418606] License error occur when create 4.0 projects while in Trial state.

ItemTemplates
    [422182] Some of WF dlls are missing under 'References' folder when adding itemTemplates for pdf and report if MVC template is create with target .NetFramework <=4.5.1.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20201.259/4.5.20201.259)		Drop Date: 02/20/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.398/4.5.20193.398

All
    Merge source from Main to Dev to Start 2020v1.
    [407689] Merge wijmo5 ver637 to Dev for start 2020v1.
    [408139] Migrate Wijmo5Mvc Client project to Wijmo 637.

FileManager
    [409588] Create UI for Delete Folder API on FileManger.
    [409589] Create UI for Create Folder API on FileManger.
    [415086] [enhance] Combine Browse and Upload Button into on Popup.
    [409587] Create UI for MoveFile API on FileManger.
    [416822] [UI] Add context menu.
    [419261] [UI] Apply new Icons from Raj.
    [419810] Check why can not upload a file to cloud storage using normal ajax.
    [421143] Should correct the layout of control like design.
    [421485] Some properties of are applied to background of FileManager control.
    [421307] Move the sample into MVC's.
    [420424][CloudFileExplorer] 500 Internal server error is occurred and cloud service drop-down is missing.

Sample
    [400541] Error occurs when restore 'MVCExplorer' core sample using 'MvcExplorer.sln' or 'MvcExplorer.2017.sln'.
    [400862][Wijmo 5 MVC][CollectionViewNavigator] Request to auto adjust default width of  CollectionViewNavigator to show Page Number on touch device.
    [395553][FlexSheetExplorer][ExcelBook][FlexSheet101] JavaScript error is occurred when delete the sheet and make certain scenarios.
    [407776] Create sample page show new AutoFilling and AutoFilled events for FlexSheet.
    [407725] Create sample page show new property FlexGrid.DefaultTypeWidth.
    [407653] Create sample page show new property FlexGrid.AllowPinning
    [407756] Create sample page show new property FlexGrid.CopyHeaders
    [407774] Create sample page show new control FlexGridSearch.
    [414477] Add sample for OdataVirtualCollectionView.
    [407775] Create sample page show new control FlexGridTransposed.
    [412889] Request to displayed hidden tile in context menu of 'Show' setting when 'Headertext' and 'Content' properties are not set in tiles.
    [413063] Collect all changes and new features to update for Enhancement List page.
    [420447] Request to use SortType instead of AllowSorting in some FlexGrid sample page.
    [417117] AllowMerging property is set twice in MergeCells sample page of non-core MVCExplorer.
    [418357] Data from 'Current Opportunities' are disappeared when change the data by dragging range selector.
    [417105] [MVCExplorer] Error occurs when run the MVCExplorer core sample from IIS if it is open and deployed from VS 2019 using MvcExplorer.Core30.2019.sln.
    [417644] Data cannot load and 500 Internal server error occurs when navigate to 'FlexGrid > ODataBind' sample page of MVCExplorer (Core) JP sample.
    [420598] Console error occurs when sorting on any column of second flexgird after set 'AutoGenerateColumns' false in MVCExplorer(non-core) sample.
    [420386] [Wijmo5 MVC][MVCExplorer] Some of the columns are missing when 'AutoGenerateColumns' setting are changed.
    [417307] Error occur when sort more than 2 columns in ODataBind sample page of Non-Core MVCExplorer.
    [420412][Wijmo 5 MVC] Vertical Scroll bars are displayed in side menu of some Explorer samples.
    [407536] JP localization.
    [415511] [Wijmo 5 MVC][FlexSheetExplorer] Error occurs when save the files with any format in 'Remote Load/Save' sample page of FlexSheetExplorer(non-core) if sample is opened with VS 2019 .
    [413243] Unobtrusive validation error border is not observed when inserting invalid value at DateTime column.
    [421415] Some of the dll can't restore and error occur when restore non-core MVCExplorer(non-core) sample.
    [421416] TransposedGrid nuget is missing and error occur when run core MVCExplorer sample.
    [420447] Request to use SortType instead of AllowSorting in some FlexGrid sample page.
    [421512] Warning Message is displayed when open index.html file after build Financial sample.

MultiRow
    [402095] [MultiSelect][Regression] Javascript error occurs when run the project if control is created without data binding.
    [402656] [Regression] Custom editor drop down that is merging column cell is incorrectly display in MultiRow control.
    [407630] Add MultiRowGroupHeaders property for MultiRow.
    [407758] Create sample page show new property MultiRowGroupHeaders for MultiRow.
    [407631] Add HeaderLayoutDefinition property for MultiRow.
    [407771] Create sample page show new property HeaderLayoutDefinition for MultiRow.

ProjectTemplate
    [402239] [Ajax] Error occur when save the data if template is create with .net core 3.0.
    [407629] Update project template to use WF packages.
    [407296] Package Installation Error occur after create project with dll which is used WF dlls in non-core project.

FlexGrid
    [408921] [Wijmo 5 MVC][FlexGrid] FlexGrid sorting, filtering, grouping stop working with new Wijmo5 637.
    [407610] Change type of the FlexGrid.AllowSorting property from boolean to enumeration.
    [407613] Add FlexGrid.AllowPinning property that adds pin icons to the column headers.
    [407626] Add FlexGrid.CopyHeaders property.
    [407639] Add the AutoFilling and AutoFilled events for FlexSheet.
    [407625] Add FlexGrid.DefaultTypeWidth static property.
    [407627] Add Column.CellTemplate property for FlexGrid.
    [407757] Create sample page show new property Column.CellTemplate for FlexGrid.
    [414518] Request to add 'MutiRange' enum value in 'SelectionMode' property of Selection Mode sample of FlexGrid101.
    [413265] 'ScrollIntoView' property is not working properly as expected.
    [416145] On setting the columnLayout again the row gets appended to the column footers.
    [416919] Add new feature Column Groups for FlexGrid.

Scaffolder
    [417656] Request to use 'SortType' property instead of 'AllowSorting' property in Scaffold UI of FlexGrid.
    [418717] Request to change 'SortType' property instead of 'AllowSorting' property in Scaffold UI of PivotGrid.
    [418785] Request to add Description of MaxResizeHeight and MaxResizeWidth of Tile in DashboardLayout Scaffold UI.
    [418771] Error occur when open Update UI of FlexGrid if ColumnGroups are set.
    [418236] SingleColumn value is not selected in SortType dropdown of Add/Insert scaffold UI in FlexGrid.
    [421646] Error occur when controls(FlexGrid, MulitRow) are added using Scaffold with AllowEdit/AllowAdd/AllowDelete in core 3.0.

AutoComplete
    [407640] Change the default value of CssMatch property from 'wj-autocomplete-match' to 'wj-state-match' for AutoComplete.

FlexGridSearch
    [407635] Add new FlexGridSearch control.

TransposedGrid
    [407638] Add new FlexGridTransposed control.
    [415367] Separate FlexGridTransposed control to another project.

CollectionView
    [407634] Add CollectionView.RefreshOnEdit property.
    [407773] Create sample page show new property CollectionView.RefreshOnEdit.

DashboardLayout
    [407505] Request create LayoutChanged event.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.258/4.5.20193.258)		Drop Date: 01/10/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.398/4.5.20193.398

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.257/4.5.20193.257)		Drop Date: 01/08/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.398/4.5.20193.398

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.256/4.5.20193.256)		Drop Date: 01/07/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.398/4.5.20193.398

Project Template
    [415232] Copy right status is incorrectly displayed at Razor project view page which is created with NetCore 3.0.

MultiRow
    [415263] 'ListBox' value of SelectionMode property is not working as expected when setting with NewRowAtTop property.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.255/4.5.20193.255)		Drop Date: 01/01/2020
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.398/4.5.20193.398

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.254/4.5.20193.254)		Drop Date: 12/27/2019
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.398/4.5.20193.398

All
    Upgrade Winforms dlls from 393 to 398.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.253/4.5.20193.253)		Drop Date: 12/24/2019
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.393/4.5.20193.393


Samples
    [414521] Error occurs when load or save the files in 'ServerLoad' sample and 'Remote Load/Save' samples of FlexSheetExplorer (non core 4.5) sample.
    [414506] Unbound sheets are displayed inconsistently when scroll the scrollbar of sheet or add new sheet in 'FlexSheetExplorer' samples.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.252/4.5.20193.252)		Drop Date: 12/18/2019
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.393/4.5.20193.393

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.251/4.5.20193.251)		Drop Date: 12/17/2019
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.393/4.5.20193.393

FlexSheet
    [413150] OnClientLoadedRows event does not get triggered.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.250/4.5.20193.250)		Drop Date: 12/12/2019
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.393/4.5.20193.393

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.249/4.5.20193.249)		Drop Date: 12/12/2019
================================================================================================
Dependencies
------------
Wijmo 5.20193.637
Winform 4.0.20193.393/4.5.20193.393

All
    [407529] Merge wijmo to Main for hotfix.
    [408566] Should exclude FileManager control from the release package.

Samples
    [404499] Error occur when build AccessibilityExtender and ColumnGroups sample.
    [407498] MultiRowLOB sample cannot be restored correctly.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.248/4.5.20193.248)		Drop Date: 11/28/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.393/4.5.20193.393

Samples
    [408284] C1.DataEngine is shown in Reference of OlapExplorer sample instead of C1.DataEngine.4.5.2.

ProjectTemplate
    [407296] [Wijmo 5 MVC][WebAPI][ProjectTemplate] Package Installation Error occur after create project with dll which is used WF dlls in non-core project.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.247/4.5.20193.247)		Drop Date: 11/26/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.393/4.5.20193.393

MultiRow
    [403910] 'Uncaught TypeError:xxxxx' is occurred when deleting selection row in new row at top template.

Samples
    [408292] Error occur when run Olap101 sample under both CS/VB.
    [408284] C1.DataEngine is shown in Reference of OlapExplorer sample instead of C1.DataEngine.4.5.2.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.246/4.5.20193.246)		Drop Date: 11/22/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.393/4.5.20193.393

All
    [402163] C1 icon cannot display properly for API dll in NuGet Package Management with NetCore 3.0.

Scaffolder
    [403894] Error occur if DataType is set in Column when FlexGrid is added by Scaffold.
    [404545] Error occur when add control using scaffold in JP enviroment in non-core project.

Samples
    [407239] Some WF dlls are not hide and cannot auto restore in some 4.5 sample.

DashboardLayout
    [403711] [SplitLayout][IE] JavaScript error is occurred in 'Split Layout' page when make certain scenarios.

ItemTemplate
    [408103][Wijmo 5 MVC]][ItemTemplate] Package Installation error occur when report/pdf file is added with ItemTempalte if local service and SSRS report service is used.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.245/4.5.20193.245)		Drop Date: 11/15/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.393/4.5.20193.393

All
    [401687] license error when browse the project with C1 controls which is created with .NetCore 3.0.

Samples
    [404938][SampleExtensionControls][Regression] Error occur after browse 'SampleExtensionControls(Core)' sample.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.244/4.5.20193.244)		Drop Date: 11/12/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.393/4.5.20193.393

All
    Upgrade Winforms dlls from 387 to 393.
    [401687] license error when browse the project with C1 controls which is created with .NetCore 3.0.
    [406129] Generate XML file for project core.

Samples
    [404938][SampleExtensionControls][Regression] Error occur after browse 'SampleExtensionControls(Core)' sample.

Scaffolder
    [403392] Update C1 MVC Control is not dim when Right click on the control which is comment in Core project.
    [404545] Error occur when add control using scaffold in JP enviroment in non-core project.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.243/4.5.20193.243)		Drop Date: 11/08/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.387/4.5.20193.387

Samples
    [404388] WinForm DLL version is used incorrect version (4.0.20192.382/4.5.20192.382) instead of (4.0.20193.387/4.5.20193.387).
    [402163] C1 icon cannot display properly for API dll in NuGet Package Management with NetCore 3.0.
    [404283] [MVCExplorer] Error occur when navigate to OData Service Bind page when opened with 'MvcExplorer.Core30.2019.sln'.
    [402819] Fix some issues in JP sample

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.242/4.5.20193.242)		Drop Date: 11/08/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.387/4.5.20193.387

Samples
    [404388] WinForm DLL version is used incorrect version (4.0.20192.382/4.5.20192.382) instead of (4.0.20193.387/4.5.20193.387).
    [402163] C1 icon cannot display properly for API dll in NuGet Package Management with NetCore 3.0.
    [404283] [MVCExplorer] Error occur when navigate to OData Service Bind page when opened with 'MvcExplorer.Core30.2019.sln'.
    [402819] Fix some issues in JP sample

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.241/4.5.20193.241)		Drop Date: 11/06/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.387/4.5.20193.387

All
    [404266] [TreeView][FlexChart][FlexSheet] Error occur when control is bind with remote binding in core 3.0.
    [404280] [FlexGrid][FlexPie][MultiRow] Error occur when control is bind with remote binding in core 3.0.

FlexGrid
    [403869] [Regression] Filtering value is not working properly as expected.

Samples
    [400823][MvcExplorer] Some of the tick text is missing when ShowTicks and ShowTickText are set as true in Editing sample page of RadialGauge.
    [404279] [MvcExplorer] Error occurr when navigate to UnobtrusiveValidation page when sample is opened with 'MvcExplorer.Core30.2019.sln'.
    [404278] [MvcExplorer] Internal server error when navigate to Enhancement List page when sample is opened with 'MvcExplorer.Core30.2019.sln'.
    [404273] 'Could not load file or assembly xxxxxxxx'error is occurred when restore some samples in 4.5 dll version with VS 19.
    [MvcExplorer] Add AllowSynchronousIO.

ProjectTemplates
    [404199] 'KeyNotFoundException' error is occurred when creating C1Template prj (.Net Core/.NetFramework) with ASP.NET Core 2.1 or lower version.
    [404231] 'The given key was not present in the dictionary.' error is occurred  when create C1 MVC Non-core templates (CS/VB) using '.NET Framework 4.5.2' and upper.
    [403628] Cannot restore and error occurs when C1 AspNet MVC Core (razor) template is create with .net core 3.0.

DashboardLayout
    [403711] [SplitLayout][IE] JavaScript error is occurred in 'Split Layout' page when make certain scenarios.
    [403734] [Regression] Script error 'Error: The child type is a invalid type!' is occurred when only Tile without any properties is added in FlowLayout.

Scaffolder
    [384369][Wijmo 5 MVC][C1Scaffold] Request to change "PivotEngineId" label in slicer scaffold dialog.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.240/4.5.20193.240)		Drop Date: 11/02/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.387/4.5.20193.387

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20193.239/4.5.20193.239)		Drop Date: 11/01/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.631
Winform 4.0.20193.387/4.5.20193.387

All
    Upgrade Winforms dlls from 382 to 387.
    Merge Dev to Main for 2019v3.
    [388057] Upgrade wijmo version to 631.
    [392558] Create a wrapper of js control for MVC\Core.
    [392557] Create JS control.
    [395077] Add download function to current js control of file manager..
    [401687] license error when browse the project with C1 controls which is created with .NetCore 3.0.

Samples
    [403531] [SampleExtensionControls] Inconsistent title is displayed in 'SampleExtensionControls(Non Core)' sample.
    [400541] Error occurs when restore 'MVCExplorer' core sample using 'MvcExplorer.sln' or 'MvcExplorer.2017.sln'.
    [370457] [SampleExtensionControls] Two 'SampleExtensionControls' samples (Core and Non-core sample) show differently and inconsistently.
    [400862] [Wijmo 5 MVC][CollectionViewNavigator] Request to auto adjust default width of CollectionViewNavigator to show Page Number on touch device.
    [395553] [FlexSheetExplorer][ExcelBook][FlexSheet101] JavaScript error is occurred when delete the sheet and make certain scenarios.
    [403657] [Wijmo 5 MVC][FlexGrid][Chrome] Delete button is displayed in next line in certain scenario when run with Chrome browser.

ProjectTemplates
    [403512] No item is chose in initial dropdown of AspNet.Core when create Framework project.
    [403603] Dialog for Syles and Sort setting cannot be displayed when click on that icon in SpreadSheet template if it is created with .net core 3.0.
    [403628] Cannot restore and error occurs when C1 AspNet MVC Core (razor) template is create with .net core 3.0.
    [402239] [Ajax] Error occur when save the data if template is create with .net core 3.0.

MultiRow
    [402095] [MultiSelect][Regression] Javascript error occurs when run the project if control is created without data binding.
    [402656] [Regression] Custom editor drop down that is merging column cell is incorrectly display in MultiRow control.

Scaffolder
    [403695][Wijmo 5 MVC][Scaffold] Request to use 'OnClientBeginDroppingRowColumn' and 'OnClientEndDroppingRowColumn'  events instead of 'OnClientDroppingRowColumn' event in Scaffold UI of flexsheet control.

DashboardLayout
    [403638] [Regression] Inconsistent behavior is observed when Right to Left direction is set and Tiles are dragged in all Layouts.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.238/4.5.20192.238)		Drop Date: 09/19/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.382/4.5.20192.382

All
    [398484] 'Could not load type C1.Web.Mvc.TagHelpers.xxxxxx because the parent type is sealed.' error is occurred when run the project after adding C1 dll in Core 3.0 VS standard template.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.237/4.5.20192.237)		Drop Date: 09/10/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.382/4.5.20192.382

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.236/4.5.20192.236)		Drop Date: 09/06/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.382/4.5.20192.382

All
    Upgrade Winforms dll from 375 to 382.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.235/4.5.20192.235)		Drop Date: 09/05/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.375/4.5.20192.375

Scaffolder
    [369163] Web.config/ resources/licensing stuffs is not automatically created when add controls using 'New Scaffold Item' in non C1 template projects.

Samples
    [388659] [C1ControlPanel] Error occur when restore DashboradDemo sample from installer.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.234/4.5.20192.234)		Drop Date: 09/03/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.375/4.5.20192.375

Scaffolder
    [369163] Web.config/ resources/licensing stuffs is not automatically created when add controls using 'New Scaffold Item' in non C1 template projects.

FlexSheet
    [395695] JavaScript error is occurred when 'Decimal Fomat' is chose from 'Format' Drop-down.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.233/4.5.20192.233)		Drop Date: 08/29/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.375/4.5.20192.375

Scaffolder
    [395527] Install Fail dialog displayed when install 'C1.Scaffolder.Extension.Vs2019.vsix' in VS 2019 environment.
    [395966] 'This extension is not installable on any currently installed products' error occurs when 'C1.Scaffolder.Extension' vsix (from 4.0 folder) is installed in VS 2019.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.232/4.5.20192.232)		Drop Date: 08/29/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.375/4.5.20192.375

Scaffolder
    [395527] Install Fail dialog displayed when install 'C1.Scaffolder.Extension.Vs2019.vsix' in VS 2019 environment.
    [395966] 'This extension is not installable on any currently installed products' error occurs when 'C1.Scaffolder.Extension' vsix (from 4.0 folder) is installed in VS 2019.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.231/4.5.20192.231)		Drop Date: 08/29/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.375/4.5.20192.375

Scaffolder
    [395966] 'This extension is not installable on any currently installed products' error occurs when 'C1.Scaffolder.Extension' vsix (from 4.0 folder) is installed in VS 2019.
    [395527] Install Fail dialog displayed when install 'C1.Scaffolder.Extension.Vs2019.vsix' in VS 2019 environment.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.230/4.5.20192.230)		Drop Date: 08/28/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.375/4.5.20192.375

Scaffolder
    [395527] Install Fail dialog displayed when install 'C1.Scaffolder.Extension.Vs2019.vsix' in VS 2019 environment.
    [369163] Web.config/ resources/licensing stuffs is not automatically created when add controls using 'New Scaffold Item' in non C1 template projects.
    [384544] Option Model is not shown in Update UI when OptionModel is set in any properties of control.

FlexSheet
    [373049] Is FlexSheet support the Create/Delete same as Update operation?
    [395695] JavaScript error is occurred when 'Decimal Fomat' is chose from 'Format' Drop-down.

Sample
    [395546] Settings changes doesn't effected on Chart when changing properties at run time in FlexChart -> Selection sample (Core).

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.229/4.5.20192.229)		Drop Date: 08/20/2019
================================================================================================
Dependencies
------------
Wijmo 5.20192.624
Winform 4.0.20192.375/4.5.20192.375

All
    Upgrade wijmo to version 624.

FlexSheet
    [373049] Is FlexSheet support the Create/Delete same as Update operation?

DashboardLayout
    [388218] Console error occurs when only dashboardlayout control is added if deferred script is check.

DropDown
    [386094] [DropDown] [InputType] DropDown<T> class does not have the InputType property.

ProjectTemplates
    [386271] [InputNumber][Regression] Color of disable style is displayed incorrectly in InputNumber control when 'IsDisabled' property is set to true and 'Width' property is more than 300.

Scaffolder
    [369163] Web.config/ resources/licensing stuffs is not automatically created when add controls using 'New Scaffold Item' in non C1 template projects.
    [387566] Control is not retained which place inside the tile of Layout after update dashboard layout in Core project.

FlexGrid
    [342751] First character is lost when second character is typed if edit selected cell with customer editor.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.228/4.5.20192.228)		Drop Date: 07/29/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

Sample
    [392044] Error occurs when run the non core JP sample 'CollectionView101' with 4.5 verison.
    [392079] Error is occurred when run the sample 'CustomReportProvider'.
    [326987] Javascript error occurs when browse 'FlexChart(HowTo) -> MultiSelection' sample.
    [392087] Error error when run WeatherChart sample.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.227/4.5.20192.227)		Drop Date: 07/25/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

Sample
    [391861] License error occur in 'ExcelBook' of JP sample with 4.5 version.
    [391842] [Wijmo5 MVC][Sample] Error occurs when run some JP samples.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.226/4.5.20192.226)		Drop Date: 07/24/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

All
    Include Scaffolder 4.0 and ItemTemplate 4.0 into build.

Sample
    [388378] Some samples(both 4.0 and 4.5) cannot be restored.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.225/4.5.20192.225)		Drop Date: 07/10/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.224/4.5.20192.224)		Drop Date: 07/09/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

All
    No changes, just version upgrade.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.223/4.5.20192.223)		Drop Date: 07/08/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

Scaffolder
    [384153] Light bulb of all controls is not shown when click inside the controls.

Sample
    [386942] [CustomPdfProvider][Regression] Both 'CustomPdfProvider' samples (CS and VB) restore is failed and error is occurred in both 4.0 and 4.5 version.
    [387893] Dll version of WebApi link is not correct in core FlexSheetExplorer Sample.
    [387915] InputMask of MvcExplorer throw exception.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.222/4.5.20192.222)		Drop Date: 07/07/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

Scaffolder
    [387568] Error occur when open Update UI of FlexChart , FlexPie, FlexRadar & Sunburst.

Sample
    [385278] [MultiRowExplorer]Date format is incorrectly displayed in 'MultiRow->DataMap' of core project sample page.
    [370651] MVC & Core C1Finance samples does not display charts for shorter durations.
    [386942] [CustomPdfProvider][Regression] Both 'CustomPdfProvider' samples (CS and VB) restore is failed and error is occurred in both 4.0 and 4.5 version.
    [386932] [OlapExplorer][Regression] 'OlapExplorer(Non Core)' sample can't work properly and 'A valid license cannot be generated' error is occurred when sample is run from 4.0 version.
    [384354] [OlapExplorer][Sample] Error is occurred when change the value of 'Allow Merging' drop-down.
    [387569] WebApi link is not correct in ReportViewer101 JP Sample.
    [386932] 'OlapExplorer(Non Core)' sample can't work properly and 'A valid license cannot be generated' error is occurred when sample is run from 4.0 version.

ProjectTemplates
    [385524] [Regression] Restore failed and error occurs when MVC Project(non-core) is created checking with 'Add TS file'

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.221/4.5.20192.221)		Drop Date: 07/06/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

All
    Upgrade version only.

================================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.220/4.5.20192.220)		Drop Date: 07/04/2019
================================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.375/4.5.20192.375

All
    Upgrade Winforms dll to 375.

Scaffolder
    [384544] [Wijmo 5 MVC][C1Scaffold][Regression] Option Model is not shown in Update UI when OptionModel is set in any properties of control.
    [384153] [Wijmo 5 MVC][C1Scaffold][Regression] Light bulb of all controls is not shown when click inside the controls.
    [365223] [Wijmo 5 MVC][C1Scaffold] Current value of 'Group' can not be displayed in 'Update UI form' although group is added in Split Dashboard Layout.
    [384287] [Wijmo 5 MVC][C1Scaffold][Regression] Error occurs when Update Dashboardlayout in Core project.
    [363472] [Wijmo5 MVC][C1Scaffold] Request to add description in each properties for DashboardLayout control in Scaffold UI.

Sample
    [386942] [CustomPdfProvider][Regression] Both 'CustomPdfProvider' samples (CS and VB) restore is failed and error is occurred in both 4.0 and 4.5 version.
    [382314] [Regression] Can't restore the MVC and WebAPI dlls in some MVC and WebAPI samples.    
    [385278] [MultiRowExplorer]Date format is incorrectly displayed in 'MultiRow->DataMap' of core project sample page.
    [370651] MVC & Core C1Finance samples does not display charts for shorter durations.
    [386932] [OlapExplorer][Regression] 'OlapExplorer(Non Core)' sample can't work properly and 'A valid license cannot be generated' error is occurred when sample is run from 4.0 version

FlexGrid
    [386090] [FlexGridFilter] ExclusiveValueSearch can't be set for FlexGridFilter

ProjectTemplates
    [382262] [Regression] Evaluation version number of wijmo 5 is displayed in AspNet MVC templates, samples etc
    [385524] [Regression] Restore failed and error occurs when MVC Project(non-core) is created checking with 'Add TS file'

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.219)		Drop Date: 07/01/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.372

Sample
    [386656] In MultiRowExplorer(non-core) sample, JP words are displayed instead of ENG words in 4.5 version for ENG build.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.218)		Drop Date: 07/01/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.372

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.217)		Drop Date: 06/28/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.372

ItemTemplates
    [386060] When adding item templates in all projects types, error dialog is displayed.

Sample
    [384160] [FlexSheetExplorer][Regression][IE] Syntax error occurs when navigate to Sorting sample page.
    [384386] [MvcExplorer] Request to change the value of 'Mask' property in 'InputDate with Mask' JPN sample.
    [385210] MVC and WebAPI dll versions are incorrect in 4.5 version.
    [384548] [SampleExtensionControls] Some of the words should be localized for Non Core Japan sample.
    [386239] 'System.Web.Mvc' can't restore and many errors are displayed when build the 'BookMyFlight' sample(4.5 version).
    [355581] [FlexSheet] Javascript error occur when unfreeze the frozen pane using 'freezeAtCursor' method after delete all columns and rows in flexsheet.
    [384354] [OlapExplorer] Error is occurred when change the value of 'Allow Merging' drop-down.

PivotGrid
    [385358] Request to support 'isReadOnly' property in PivotGrid like Wijmo 5 PivotGrid control.

ProjectTemplates
    [382524] Restore failed and error occurs when MVC Project(non-core) is created checking with 'Add TS file'.
    [386054] Error dialog displayed when create MVC 4 CS and VB applications.
    [386078] Error alert is displayed and cannot create the project using 'C1 ASP.NET MVC 3 Web Application' in VS 12.

FlexSheet
    [384434] 'AutoGenerateColumns' property work differently in 'HtmlHelper' and 'TagHelper'.

Scaffolder
    [385511][FlexSheet] Fix RemoteSaveFileAction.vb template.
    [386074] 'System.WIndows.Baml2006.TypeConverterMarkupExtension error occurs when call the scaffold UI (Add/Insert/Upate).
    [382677] [Wijmo 5 MVC][C1Scaffold][Regression] Error alert is appeared when call 'Update UI form' and cannot update the control if only group is added without Tile in 'Auto/Manual/Split' Layouts for core projects
    [384158] [Wijmo 5 MVC][C1Scaffold] File path is not shown correctly in Update UI of FlexSheet in VB project
    [384289] [Wijmo 5 MVC][C1Scaffold] Texts are disppeared from 'li' and 'a' tag when Update Tabpanel in core project
    [384275] [Wijmo 5 MVC][C1Scaffold] Like FlexGrid control, request to add 'newRowAtTop' property for multirow control in scaffolding

FlexGrid
    [386120] [FlexGrid] [ErrorTip] Custom Error Tooltip can't be set for ErrorTip

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.216)		Drop Date: 06/24/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.372


FlexGrid
    [384087] Fix wrong position of input date's editing box.
    [371411] 'MovetoPage/pageindex' setting do not take effect when BindODataSource is used in FlexGrid control if GroupBy property is set
    [382635] Fix missing shown values after filtering if ExclusiveValueSearch=false.
    [382543][Wijmo 5 MVC][FlexGrid] Error occur when Content or Threshold is set in ErrorTip
    [382636] Filter icon is not displayed on grouped column header in Grouppanel when columfiltertype is set in filter property of GroupPanel

Sample
    [385319] [AntiForgeryToken] enhance sample, add some step to user can easy test this feature.
    [355630] [FlexSheet] Scripts error occurs when sort setting is changed after delete all columns/rows if sort setting is set before deleted the rows/cols in all FlexSheet samples which is used sort setting
    [384160] [FlexSheetExplorer][Regression][IE] Syntax error occurs when navigate to Sorting sample page.
    [385243]  Winform dll version is incorrect , still used 4 dll version in 'licenses.licx' file of some MVC samples of 4.5 version.
    [385347] [CollectionView101] Cannot build the sample(4.5 version) and errors are displayed.

ProjectTemplates
    [385706] Change number version in vsixmanifest files from 4.0.XXXXX.xxx -> 4.5.XXXXX.xxx.

ItemTemplates
    [385441] Change .NET target framework to 4.5.2.

Scaffolder
    [384353] [Wijmo 5 MVC][C1Scaffold][Regression] Autogeneratecolumns property is added in code when add FlexSheet using scaffold dialog
    [370350][FlexGrid] Fix duplicated Url.Action when updating AllowAdd/Delete/Edit.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.215)		Drop Date: 06/18/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.372

All
    Upgrade WinForms dll to 372.
    [384175] Update JP XML Document.    

ProjectTemplate
    [384016] Add missing tags for CS and VB Web projects.

Scaffolder
    [372461][FlexSheet] Fix RemoteSaveFile templates.
    [382601][MultiRow] Set default Height=800px.
    [384287][Dashboard] Use default CoreHtmlParser instead of DashboardLayoutParser.
    [384541][PivotEngine] Update FieldCollection binder.

Sample
    [372397] [Localization] Collect new resource strings for adding new sample pages
    [376250] Replace the quandl data source with our own datasource for AspNet MVC.

FlexGrid
    [382636] Filter icon is not displayed on grouped column header in Grouppanel when columfiltertype is set in filter property of GroupPanel
    [379832] ValueFilter does not apply by setting the filterDefinition if the DisableServerRead is set to false
	
DashboardLayout
    [382827] 'Uncaught ** Assertion failed..............' error is displayed when a group is added in Split layout without children Tile and 'Orientation' property is set to that group

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.214)		Drop Date: 06/18/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20192.372

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.213)		Drop Date: 06/17/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20191.362

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.212)		Drop Date: 06/12/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20191.362

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20192.211)		Drop Date: 06/10/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20191.615
Winform 4.0.20191.362

All    
    Upgrade wijmo version to 615.
    Upgrade major version to 20192.
    [382587][VS2019] Apply tagging for all C1 web project template.

Menu
    [382824] Set default value CloseOnLeave=true.

Sample
    [376164] Create the page list all new enhancements/features merging from Wijmo

PivotGrid
    [376722] Add DefaultColumnSize and DefaultRowSize properties.

Scaffolder
    [382548][MultiRow] Set attribute HtmlInclude=true to CssClassAll and RowSpan.
    [382505][FlexGrid] Fix wrong parsing logic if the property is a generic method.
    [368182] Add Japanese text to Olap control resource.
    [382540] Add description for new properties: CssClassAll and RowSpan.

ProjectTemplate
    [382278][VS2019] Cannot install and 'The install of xxxx was not succesful xxxxxx' message is displayed when installed C1ProjectType template in VS 2019.

Sample
    [376250] Replace the quandl data source with our own datasource for AspNet MVC.
    [355758] Localize "Colors" Text on ComboBox->ItemTemplate page.

PivotGrid
    [371842] Add "allowMerging" property for PivotGrid.
    [371843] Create sample page display new property AllowMerging of PivotGrid.
    [371699] Create sample show new property of PivotGrid "outlineMode and showValueFieldHeaders"

FlexGrid
    [371759] Add property CssClassAll for Row and Column of FlexGrid.
    [371760] Create sample page show new property CssClassAll of Row and Column.
    [371439] Add base class Tooltip, adjust ChartToolTip and add 'ErrorTip' property into FlexGrid using Tooltip
    [371697] Create sample show new property ErrorTip of FlexGrid
    [375947] [FlexGridFilter] Add DataMap support for ValueFilter
    [363821] Json mapping ColumnFilters to FilterColumns because unknown property columnFilters on client.
    [371554] Added a new "exclusiveValueSearch" property to the PivotEngine and FlexGridFilter.
    [371549] [Wijmo5 MVC][FlexGrid] Edit mode cannot enter when a cell value is copied and pasted to another cell
    [356699] [Wijmo5 MVC][FlexGrid] RowEditEnding/RowEditEnded/RowEditStarted/RowEditStarting events are not fired when edit in data which is used customeditor
    [371708] Create sample for ExclusiveValueSearch property for FlexGrid
    [379832] ValueFilter does not apply by setting the filterDefinition if the DisableServerRead is set to false

DropDown
    [371756] Add property InputType for DropDown and InputMask.
    [371757] Create sample page for InputType property

Input
    [371422] [InputDateTime] InputDateTime control displays current time when 'value' property is not set
    
InputMask
    [371756] Add property InputType for DropDown and InputMask.
    [371757] Create sample page for InputType property

Scaffolder
    [365223] Current value of 'Group' can not be displayed in 'Update UI form' although group is added in Split Dashboard Layout.
    [364649] 'Error: The child type is a invalid type!' is occurred when empty group is added in Group Layouts.
    [365228] 'Flow Direction' property is displayed in 'Split Dashboard Layout' although that property is not supported in Split Layout.
    [347342] Current values of some control cannot set in Update UI form and another method are added if setting is changed in Update UI.
    [352863] Error alert is appeared when Input control is added by 'Add New Scaffold' with unbound mode.
    [365824] Current value for some properties setting are shown as dim in Update UI form.
    [364379] request to set default value in Height property for TabPanel control in Scaffold UI.
    [365146] Cannot restore project when controls are inserted/updated using Insert/Update UI form in non-core project.
    [359429] Support PivotPanel-Engine intergration.
    [363236] Add ControllerName for OLAP controls.
    [358864] Fix update process of AddWaterfall (FlexChart.Series).
    [365086] Fix the UI of tab ClientEvents for PivotEngine.
    [365036] Add description for Slicer control.
    [356331] Support OData ItemsSource for FlexGrid.
    [367679][FlexGrid] Fix "Model class" and "DbContext" comboboxes are not refreshed when datasource selection changed from ODataSource to CollectionView.
    [365260] Fix ReadActionUrl not generated in the controller page for PivotEngine and PivotPanel.
    [351220][FlexGrid] Fix some properties are not generated using Update UI.
    [372461][FlexSheet] Fix error occurs when sets UseRemoteSave is true, FilePath is filled but SavePath is not.
    [376721][PivotEngine] Fix wrong generated code from adding multiples fields.

TreeView
    [371467] Add new properties ExpandOnLoad, CheckOnClick and CheckedMemberPath.
    [371704] Create sample page(s) for new properties of TreeView

Olap
    [371457] Add "outlineMode" property for PivotGrid.
    [371458] Add "showValueFieldHeaders" property for PivotGrid.

FlexSheet
    [363794] Set default EnableFormular = true.
    [363394] Set default EnableDragDrop = true.
    [371752] Add 'ExclusiveValueSearch' property into ValueFilterSetting.
    [371414] [Wijmo 5 MVC][FlexSheet][Regression] 'Uncaught ** Assertion failed in Wijmo: Unknown property droppingRowColumn' error is occurred when 'OnClientDroppingRowColumn' event is set in FlexSheet control
    [374609] Columns get duplicated when Columns() method is used to create Columns in FlexSheet when FlexSheet is used as Detail


Samples
    [369365] [Wijmo 5 MVC][Sample][MvcExplorer] Selection is shown incorrectly on navigation bar when navigate to sample pages in both MvcExplorers
    [368423] [Wijmo 5 MVC][Sample][MvcExplorer] Inconsistent behavior is observed when layout orientation is changed in SplitLayout sample page
    [372344] [Wijmo 5 MVC][Sample][MultiRow] Edited value is not retained when copy that value and paste in the cell which has different value if CustomEditor is used
    [353172] [Wijmo 5 MVC][Sample][BookMyFlight] 'Null Reference' Error occurred when book the flight in IE browser
    [355630] [FlexSheet] Scripts error occurs when sort setting is changed after delete all columns/rows if sort setting is set before deleted the rows/cols in all FlexSheet samples which is used sort setting
    [378171] Updated URL of "Getting Started Guides" of MVCExplorer
    [375444] [Sample] [LearnMvcClient] Wrong typing in description of page CustomFilterTypes

DashboardLayout
    [368479] [Wijmo 5 MVC][DashboardLayout] Split Tiles change size when click on gap area between two Tiles if these tiles are changed their sizes once
    [342049] [DashboardLayout] Border of cue boxes do not show properly and shaded portion showed in wrong position when Right to Left direction is set
    [343741] AllowResize of Flow/AutoGrid/ManualGrid Layout does not work if it is set in the SplitLayout	
    [351210] [Regression][IE] Horizontal scrollbar of Split DashboardLayout cannot scroll if there is next one horizontal scrollbar inside it and the inner scrollbar is overlap
    [351221] [IE] Horizontal scrollbar of DashboardLayout does not appear and cannot resize the width of Tile to become larger than the width of Flow DashboardLayout
    [348619] [IE] HeaderText and content are overlapped when resize the Tile to small size if 'HeaderText' is long
    [343838] Request to define the default maximum size of tile for resizing	

ProjectTemplate
    [368039] [Wijmo 5 MVC][ProjectTemplate][Spreadsheet] Data cannot load properly at initialize state in Chrome browser when use VB project

Menu
    [371468] Add "CloseOnLeave" property for Menu
    [371705] Create sample pages for CloseOnLeave of Menu

FlexChart
    [371553] Add "CssClass" property for Tooltip
    [371707] Create sample page show new property CssClass of Tooltip
    [371466] Add "tooltipContent" and "itemFormatter" property for Series
    [371703] Create sample page for tooltipContent and itemFormatter of Series

MultiSelect
    [371513][Wijmo 5 MVC][MultiSelect][Edge] MultiSelect control can't work properly in Edge browser when 'DisplayMemberPath' and 'Name' properties are set in control

MultiRow
    [371751] Add 'RowSpan' property into Cell of MultiRow
    [371465] Add "newRowAtTop" property for MultiRow
    [371700] Create sample display newRowAtTop of MultiRow

WijmoReference
    [377750] [NPM support] Change reference to support wijmo npm

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.210)		Drop Date: 05/21/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.569
Winform 4.0.20191.362

FlexGrid
    [371555] Error is occurred when 'PivotGrid' and 'PivotPanel' control are used even though any properties are not set in controls.
	[380628] [Regression] 'ShowAlternatingRows' property 'false' is not work properly in Flexgrid.

Samples
    [378434] [Wijmo5 MVC][Sample][Regression] ''Uncaught TypeError: Cannot read property 'Header' of null' console error is occurred when format cells page is navigated in some FlexSheet sample
    [380347] [Wijmo 5 MVC][Sample][FlexChartGroup][Regression] 'Ucaught TypeError: Cannot read property 'CommandParameter' of null' error occurs when choose 'Pie' in 'Chart Type' dropdown

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.209)		Drop Date: 05/15/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.569
Winform 4.0.20191.362

FlexGrid
    [371448] Error is occured when showAlternatingRows is set as option model in FlexGrid.
    [371555] Error is occurred when 'PivotGrid' and 'PivotPanel' control are used even though any properties are not set in controls.

FlexSheet
    [371414] [Wijmo 5 MVC][FlexSheet][Regression] 'Uncaught ** Assertion failed in Wijmo: Unknown property droppingRowColumn' error is occurred when 'OnClientDroppingRowColumn' event is set in FlexSheet control

Samples
    [377928] [Wijmo 5 MVC][Sample] Setting OptionsMenus lost values causing Convert Exception when select item
    [378407] [Wijmo5 MVC][Sample][MVCExplorer][MultiRowExplorer][Regression] 'Must specify valid information for parsing in the string.' error is occurred in some samples when changed the value from setting's dropdown
    [378468] [Wijmo5 MVC][Sample][FlexSheetExplorer][Regression] The selected item in first combo box is reset instantly in Remote load/Save page in FlexSheet Explorer sample(non-core)

ComboBox
    [363469] [MVC ComboBox/AutoCompelte] The C1ComboBox does not allow to set the null value from selected-value with is-required to false

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.208)		Drop Date: 05/08/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.569
Winform 4.0.20191.362

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.207)		Drop Date: 05/08/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.569
Winform 4.0.20191.362

All
    Merge wijmo version 569.

Scaffolder
    [369163] Web.config/ resources/licensing stuffs is not automatically created when add controls using 'New Scaffold Item' in non C1 template projects.

Input
    [363469] The C1ComboBox does not allow to set the null value from selected-value with is-required to false.

FlexGrid
    [372455] Internet Explorer 11 shut down suddenly when user input space in non edit mode.
    [371448] Error is occured when showAlternatingRows is set as option model in FlexGrid.

FlexChart
    [361084] [Enhancement] FlexChart deep binding supports.

FlexSheet
    [371414] [Wijmo 5 MVC][FlexSheet][Regression] 'Uncaught ** Assertion failed in Wijmo: Unknown property droppingRowColumn' error is occurred when 'OnClientDroppingRowColumn' event is set in FlexSheet control

Samples
    [371615] [GRIDLAYOUT]Layout Collapses when Zoom Factor is changed.
    [373815] Error occurs when run the StockChart sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.206)		Drop Date: 04/08/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.362

All
    Upgrade winform build 359 to 362.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.205)		Drop Date: 04/07/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359


Sample
    [373815] Error occurs when run the StockChart sample.


=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.204)		Drop Date: 04/04/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

FlexGrid
    [373599][Grid.Validator] Value is not shown when enter edit-mode in unobtrusive validation.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.203)		Drop Date: 04/03/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.202)		Drop Date: 04/03/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

ItemTemplates    
    [373506] Error dialog displayed when install itemTemplates and AspNetCoreLicenseGenerator from VSIX installer in VS 17 and VS 19.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.201)		Drop Date: 04/02/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

All
    [364960] Update JP License agreement.
    [366403] Support new JP Era.

ProjectTemplates
    [353273] 'C1 ASP.NET Core MVC Application (.NET Framework)' template is displayed in C1 Tag of Visual Studio 2015.

ItemTemplates
    [372460] Error occurs when PdfViewer/ReportViewer ItemTemplate is added in VS 2015.

FlexGrid
    [370317][Grid.Validator] Fix the red border is not shown when input data is invalid on IE11, Edge and Firefox when ImeEnabled=true.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.200)		Drop Date: 03/26/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

FlexGrid
    [370317][Grid.Validator] Fix hidden input on IE11 when ImeEnabled=true.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.199)		Drop Date: 03/22/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

Samples
    [355179] 2018v3 JP Sample issues.

DashboardLayout
    [368479] [Wijmo 5 MVC][DashboardLayout] Split Tiles change size when click on gap area between two Tiles if these tiles are changed their sizes once.

FlexGrid
    [370317][Grid.Validator] Fix missing inputted data after typing Enter to commit changes when ImeEnabled=true.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.198)		Drop Date: 03/14/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.569
Winform 4.0.20191.359

All
    [370586] Merge latest Wijmo 5 (ver .569 build).

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.197)		Drop Date: 03/11/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

Scaffolder
    [370349] Message dialog is appeared and  License error occur when add controls using Insert/Update scaffold UI in JP environment.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.196)		Drop Date: 03/11/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

Scaffolder
    [370116] In Trial state, GrapecityLicenseManager dialog is opened when control is inserted or updated using scaffold either core or non-core application.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.195)		Drop Date: 03/08/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.194)		Drop Date: 03/08/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

ItemTemplates
    [368871] Package Installation Error occur when PdfViewer/ReportViewer Item Template is added in VS 2019.

Scaffolder
    [369604] Another 'List Items' is added when delete on SplitGroup.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.193)		Drop Date: 03/07/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

ItemTemplates
    [368871] Package Installation Error occur when PdfViewer/ReportViewer Item Template is added in VS 2019.

Scaffolder
    [364221] Tile/Group in Items list are not created properly and error occurs when SplitTile is deleted if Tile is added without adding group in SplitLayout Scaffold UI
    [369203] Fix Fields values do not retained when changing tabs on PivotEngine/PivotPanel UI.

Sample
    [369140] Update new document link in the core sample.


=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.192)		Drop Date: 03/06/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.359

All
    Upgrade winform build 358 to 359.
	
Project Templates
    [368844] MVC(Core) project template('C1.AspNetCore.Mvc.ProjectTemplates') cannot install in VS 17.

Scaffolder
    [369171] Added Split Tiles/Groups are remained although change to other layout and select again 'Split' Layout.
    [369200] 'Return View()' is missing in VB project.
    [364221] Tile/Group in Items list are not created properly and error occurs when SplitTile is deleted if Tile is added without adding group in SplitLayout Scaffold UI.
    [369065][FlexGrid] Fix bug BindODataVirtualSource is generated instead of BindODataSource.
    [369070][FlexGrid] Fix bug Checkbox PageOnServer/SortOnServer/FilterOnServer is not checked when BindODataSource is selected.
    [368877][FlexGrid] Fix bug Values in 'Data Binding' tab is not retained when changing tabs.
    [368872][FlexGrid] Fix bug 'Auto Generate Column' is not checked when BindCollectionView is selected.
    [369212][PivotGrid] Temporarily hide 'Paging' tab because the current supported data source do not support PageSize property.
    [365262][PivotGrid] Hide 'Filtering' tab because the control do not support Filterable property.
    [369151] Fix client events are not updated on the following controls: FlexGrid, Multirow, TabPanel, PivotEngine, PivotPanel.
		
Sample
    [368396] Some inconsistent behaviors are observed in 'Custom Header Template' page of FlexGrid control.
    [368300][MultiColumnComboBox] Fix some MultiColumnComboBox Issues.
    [368393][OlapExplorer] Different behaviors between Non-Core and Core projects.
    [368470][OlapExplorer] Fix the width and height of slicer when 'Show Header' is unchecked.
    [368477][Wijmo 5 MVC][Sample][MvcExplorer][Regression] Thumb of gauge is displayed incorrectly in LinearGauge and BulletGraph.
    [355758][ASP.NET Wijmo][MvcExplorer Net Core] Localize some text in Combobox and Listbox samples.
    [365247][Wijmo 5 MVC][Sample][SampleExtensionControls] Both 'SampleExtensionControls' samples cannot restore and run the project.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.191)		Drop Date: 03/01/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.358

All
    Upgrade winform build 357 to 358.

Custom Header Templates
    [359236] Add resource string for feature ColumnGroup/ColumnHeader row template.

Scaffolder
    [364221 + 367595] Tile/Group in Items list are not created properly and error occurs when SplitTile is deleted if Tile is added without adding group in SplitLayout Scaffold UI.
    [368223] Title of Dashboard Layout control is not fully displayed in Scaffold UI when use JP build.
    [368217] Both ControllerName/ViewName and ControllerName/ActionName is shown in Insert/Add/Update UI form.
    [365146] Cannot restore project when controls are inserted/updated using Insert/Update UI form in non-core project.
    [367922] [FlexGrid] Reset columns when Data Source is changed.
    [367714] [FlexGrid] Disable AutoGenerateColumns checkbox when Data source is ODataCollectionView/ODataVirtualCollectionView.
    [368394] The scaffolder should add the script tags in HEAD instead of BODY.
    [367893] Fix some of the titles are hidden on Olap control UI in Razor application.
    [368474] Disable Enable Validation checkbox when data source is ODataCollectionView/ODataVirtualCollectionView.
    [368555] [PivotEngine] Fix null exception when parsing Bind(Model) in updating progress.
    [349189] Error is occurred when opening "Update C1 MVC Control" of MultiRow after add DataMpap in desire column in not core project.
    [368689] License.licx file added but not include in the project.

Samples
    [368390] [FlexGrid][Antifogery] Fix typo Antifogency to Antifogery.
    [365170] [Wijmo 5 MVC][Sample][SampleExtensionControls] Title in 'readme' file are different between Core and Not Core samples(SampleExtensionControls).
    [355758] [ASP.NET Wijmi][MvcExplorer] Localize some text in Combobox and Listbox samples.  

InputTime
    [368585] Fix default time value is set to DateTime.Now
    [368583] Fix default value of IsEditable is set to false. 

ProjectTemplates
    [367466] 'C1 ASP.NET Core MVC Application(.Net Core)(C#)' is displayed twice in project templates of VS 2019.
    [368457] Different behavior are observed between 'VB' project and other projects(CS, Core, FW) when use 'Spreadsheet' template.

DashboardLayout
    [355743] Error occurs when dragged the tile after resize the other tile of same group in DashboardLayout -> SplitLayout sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.190)		Drop Date: 02/22/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20191.357

All
    [365126] Request to update EULA in JP builds.

Samples
    [364413] [ColumnHeaderTemplate] Update the sample.

Scaffolder
    [365870] 'DemoSettingsModel.ControlId' is dislpayed as dim in Update UI.
    [364217] 'Group' is created instead of 'Tile' in Items of ScaffoldUI if Tile is added without group in Auto and ManualGrid of Scaffold UI.
    [367945] Split Tile cannot be created and Split Layout code is not added into DashboardLayout control although Split Tile is added without group using scaffold.
    [367691] ID of Flexgrid control do not set in Scaffold UI when it is called from 'Add New Scaffold'.
    [367741] Error occurs when Splitlayout of DashboardLayout control is added with Scaffold in VB project.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.189)		Drop Date: 02/18/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.568
Winform 4.0.20183.357

All
    Upgrade winform build 343 to 357.
    [365126] Request to update EULA in JP builds.
    [364400] Merge Wijmo 5 .568 build.

Samples
    [364500] Add sample for new properties of Calendar RepeateButtons and ShowYearPicker.
    [364501] Add sample for new properties of Menu: OpenOnHover and SubItemPath.

Scaffolder
    [363453] 'SplitLayoutItemFactoryBuilder' does not contain a definition...
    [363222] Some inconsistent behavior are observed in ID of TabPanel control of Scaffold UI.
    [363210] Height of 'ID' and 'Selected Index' textbox for TabPanel control is more larger than the others textbox.
    [364217] 'Group' is created instead of 'Tile' in Items of ScaffoldUI  if Tile is added without group in Auto and ManualGrid of Scaffold UI.
    [365086] Fix the UI of tab ClientEvents for PivotEngine.
    [365036] Add description for Slicer control.
    [356331] Support OData ItemsSource for FlexGrid.
    [363259] Add template text files for new controls.
    [363227] [WIjmo5 MVC][C1Scaffold] Unlike the other controls, ControllerName does not exist in Scaffold UI for TabPanel and DashboardLayout controls.

FlexSheet
    [363394] Set default EnableDragDrop = true.

FlexGrid
    [365226] Fix Fiter icons not display on column headers.
    [136529] Fix JS error occurs when expand a detail row after drag and drop.

ItemTemplates
    [365763] 'Package Installation Error' occurs when create the WebApi 2.2 templates in VS 2019 Preview 2.1.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.188)		Drop Date: 02/01/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.343

All
    [364960] Update JP License agreement.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20191.187)		Drop Date: 01/31/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.343

All 
    Merged all from Main 20183.171 to Dev.
    Upgrade major version from 20183 to 20191.
    [363240] WebApi, MVC and WinForm dll versions are incorrect.
    [363247] Cannot build and error occurs when build MVC projects which is created with VS 2013.

Core
    [353388] [AntiForgency] Support MVC AntiForgery.
    
Scaffolder
    [363453] Fix 'SplitLayoutItemFactoryBuilder' does not contain a definition'.
    [364227] Current value of flowtile cannot be displayed in Items of 'Attach Layouts' tab if Dashboardlayout control is created using TagHelper.
    [363283] 'Object reference not set to an instance of an object' error occurs when TabPanel control.
    [364420] Children text-box that is appeared when add group in Dashboard layout can't take effect anything.
    [363264] [Scaffold] Index was outside the bounds of the array' error occurs when call the Update.
    [354810] Support TabPanel.
    [359429] Support OLAP PivotEngine.
    [354811] Support DashboardLayout.
    [354809] Support OLAP PivotGrid, PivotPanel, PivotChart, Slicer.
    [359712] Fixed bugs that make VS crashes (edit CoreHtmlParser, CSHtmlParser and VBHtmlParser).
    [354388] Update Web.config, resources, licensing stuffs in a project that created using standard VS template but not C1 Template.
    [356331] Support Remote Action URL for FlexGrid.
    [360614] DashboardLayout update.
    [359429] Support PivotPanel-Engine intergration.
    [363236] Add ControllerName for OLAP controls.
    [358864] Fix update process of AddWaterfall (FlexChart.Series).

Calendar
    [354844] Add RepeateButtons and ShowYearPicker properties.

InputDate
    [354844] Add RepeateButtons and ShowYearPicker properties.

InputDate
    [362167] Unable to enter decimal values for InputNumber control if the culture has "," as seperator.
    
Menu
    [354849] Add OpenOnHover and SubItemsPath properties.

GroupPanel
    [356157] Add new property Filter to the GroupPanel control.

Olap
    [354851] Research to add new control Slicer and its properties.
    [358330] Add sample page for new control Slicer.
    [354850] Add new properties GetValue and Visible to the PivotField control.

FlexChart
    [354824] Added a interpolateNulls property to the SeriesBase controls.
    [361084] FlexChart deep binding supports.

FlexSheet
    [354842] Added 'enableDragDrop' property to FlexSheet. (TFS 342191).
    [356160] Add 'enableFormulas' property in  FlexSheet. (TFS 341806).
    [363794] Set default value of EnableFormular = true.

Scaffolder
    [342366] Invalid EF model cause Scaffolder stop working.

Flexgrid
    [349658] ColumnGroup/ColumnHeader row template.
    [354376][ImeEnabled] IE gets crashed if the Editor focus is lost using keyboard.
    [363821] Json mapping ColumnFilters to FilterColumns because unknown property columnFilters on client.
    [360988] Color defined in CSS class do not get applied for frozen cells.

CollectionView
    [354852] Add new property SortNullFirst to the CollectionView control.
    [364595] 'SortNullFirst' is not affected in CollectionView.

AutoComplete
    [316076] Unable to use AutoCompleteFor while data source is list of Object.

DashboardLayout
    [341872] Blank area is appeared and tile in outer group cannot changed the size when resized the tile from outer group after resize the tile from the inner group at first.
    [342059] Cannot drag the tile which has no header if that tile is drag and drop in any place of other tile and before selecting other tiles if 'ShowHeader' is set 'false' in Splitlayout.
    [343450] Request to disappear the cue boxes if cursor is move to the outside of the dashboard.
    [343198] Javascript error occurs when drag and drop the tile at second time in Split Layout.
    [344191] Remove the center square in the cue boxes for SplitLayout.
    [342824] DashboardLayout disappear when user drags the ONLY Tile in Split Layout.
    [342567] Shaded portion for drop is still visible after dragging is cancel with 'esc' key if dragged tile is placed on cue boxes which is not outermost.
    [342681] Shaded portion is remained and dragdrop process failed if scroll mouse wheel and drop.
    [341814] The cue box is not appeared when drag the one tile and place on another tile if scroll bar is appeared after drag and drop the tiles.
    [343760] Resized Red Line display incorrectly when resize the Tile in Split Layout as certain conditions.
    [342851] Fixed value of Size property does not applied to Tiles from Horizontal Group in Split Layout and changed again after dragdrop action.
    [342703] Actual space after dropping is different with shaded portion when hover on cue box.

Samples
    [343880][Wijmo5 MVC][LearnMvcClient][Sample] Settings and Exports icon is not shown properly in Custom Title sample
    [342909][MVCExplorer] Javascript error occur after delete the data if select on multiple cells in both MVCExplorer samples.
    [343182][MVCExplorer] Error occurs when navigate to Grouping samples of ComboBox and ListBox control for Core sample.
    [341410] Request to localize in button title of setting dialog in DashboardDemo-Analysis sample.
    [343335][FlexViewerExplorer] Report cannot load and error occurs when navigate to AR report sample pages.
    [342472][MultiRowExplorer] In Core sample, 'ASP.NET MVC Edition Olap Explorer' is displayed instead of 'ASP.NET MVC Edition MultiRow Explorer' in task-bar.
    [356687][FinancialChartExplorer] Extra word('Â') are displayed in Copyright portion of FinancialChartExplorer (Not Core) sample.
    [356189][localization] C1WebWijmo.webapi.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.186)		Drop Date: 01/10/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.343

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.185)		Drop Date: 01/08/2019
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.343

Scaffolder
    [359712] Fixed CsHtmlParser.cs infinite loop make VS crashes.
=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.184)		Drop Date: 12/27/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.343

Sample
    [359242] [FlightStatistics] Console error occur when click on the Title 'FlightStatistics'.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.183)		Drop Date: 12/21/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.343

Scaffolder
    [358916] 'Object reference not set to an instance of an object' error occurs when click on 'Update C1 MVC Control'  if FlexRadar is added with Axis X or Axis Y.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.182)		Drop Date: 12/19/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.343

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.181)		Drop Date: 12/12/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.338

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.180)		Drop Date: 12/07/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.338

ProjectTemplate
    [355796] Chart data values are disappeared if click on some columns after inserting the chart.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.179)		Drop Date: 12/05/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.338

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.178)		Drop Date: 12/05/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20183.550
Winform 4.0.20183.338

FlexSheet
    [355641] Date format cannot be set in FlexSheet.

Samples
    [355796] Chart data values are disappeared if click on some columns after inserting the chart.

Menu
    [355775] Some inconsistent behaviors are observed if 'Command' property is used.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.177)		Drop Date: 11/27/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20183.338

All
    Upgrade wijmo5 to build 550.

Samples
    [355179] 2018v3 JP Sample issues.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.176)		Drop Date: 11/13/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20183.338

Project Template
    [353387] 'C1 ASP.NET Core MVC Application (.Net Core)(C#)' is displayed twice in C1 Tag of VS 2015.

Scaffolder
    [353433] [353439] [Internal] VB projects.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.175)		Drop Date: 11/12/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20183.338

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.174)		Drop Date: 11/12/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20183.338

Project Template
    [353273] 'C1 ASP.NET Core MVC Application (.NET Framework)' template is displayed in C1 Tag of Visual Studio 2015.

Scaffolder
    [353229] [Internal] Error occurs when Update UI is called if min/max value are set in 'InputDate' and 'InputDateTime' controls of VB project.
    [353241] [Internal] Incorrect code is generated after Update FlexRadar choosing DataModel classes in Core project.
    [353244] [Internal] Another model class are added into code when click on the update button after choose the DataModel Classes in all charts for Core applications.
    [353267] [Internal] Item of Update C1 MVC Control is dim after Update FlexChart choosing DataModel classes in Core project.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.173)		Drop Date: 11/09/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20183.338

Scaffolder
    [352899] [Internal] 'Object reference not set to an instance of an object' error occurs when click on 'Update C1 MVC Control' if FlexRadar is added with bound mode.
    [352868] [Internal] Item of Update C1 MVC Control is dim after columns are added using Update UI in FlexGrid and MultiRow in Core project.
    [349511] [Internal] items source element lost after code generation| designer.
    [352418] [Internal] Item of Update C1 MVC Control is dim when right click on control in VS 2017 for JP environment.

Project Template
    [350489] missing some projects listed in C# or VB category.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.172)		Drop Date: 11/07/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20183.338

Project Template
    [351434] Package Installion Error occur when create 'C1 ASP.NET MVC 4 Web Application(C#)' with 2015 in ENG build.

Scaffolder
    [347342] [Internal] Current values of some control cannot set in Update UI form and another method are added if setting is changed in Update UI.
    [352258] [Internal]  'Object reference not set to an instance of an object' error occurs when click on 'Update C1 MVC Control'  if DataLabel is set.
    [346846] [Internal] Add code to avaiable quick ation for Scaffolder.
    [349829] [Internal]  OData source element lost after update setting.

Sample
    [352431][FlexViewerExplorer] JavaScript error occurs when open Search, Export,Page Thumbnail and Document Map panel by clicking on related icons when 'ContinuousInvoice.flxr' is loaded into viewer.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.171)		Drop Date: 11/05/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20183.336

Scaffolder
    [346846] [Internal] Add code to avaiable quick ation for Scaffolder.

DashboardLayout
    [350753] Add the jp resources.

FlexGrid
    [339628][ImeEnabled][Romaji][iOS] First Charecter gets missed on quick editing.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.170)		Drop Date: 11/01/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20183.335
Item Templates
    [335769] AR support in ReportViewer template.

Project Template
    [350489] missing some projects listed in C# or VB category.

Scaffolder
    [350617] [Internal] 'Object reference not setxxxxxx' error occurs when click on the Next button of Scaffold UI in FlexSheet control if it is added by 'Add New Scaffold'.
    [349829] [Internal]  OData source element lost after update setting.
    [349360] [Internal] Error occurs when update the control(i.e used value binding) with Update UI or value binding setting is set with Update Scaffold UI if control is created with HTMLHelper in Core project.
    [289882] [Internal] 'FontSize' of Header Style for all Charts can be set by Scaffold but it does not take effect.
    [350722] [Internal] Columns are not updated in Column tab of Update UI if change bound to unbound mode or unbound to bound mode.
    [347229] [Internal] Add button is dim in Input/FlexPie/Sunburst/ control if Model class and Data Context class is not choose.
    [349511] [Internal] items source element lost after code generation| designer.

FlexGrid
    [350566] TimeSpan as column data type throws StackOverflow Exception.
    [350738] MVC FlexGrid do not support daylight saving for DateTime field.

Samples
    [343879] [Wijmo5 MVC][FlexGridFullRowEdit][Sample] Editing row can not saved record after delete the other row while in edit state in FlexGridFullRowEdit sample.
    [350806] Update the icons for jp for ColumnGroups and AccessbilityExtender samples.
    [327033] Update the pages for DashboardLayout in MVCExplorer and LearnMvcClient.

DashboardLayout
    [350753] Add the culture resources for DashboardLayout.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.169)		Drop Date: 10/29/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20182.326

Scaffolder
    [347411] [Internal] Value is displayed incorrectly and error occurs after save the data if input control is added by  'Add New Scaffold' which is from  r-click on the project and some of the setting are modified with Update C1 MVC control.
    [347225] [Internal] Email: MVC Designer Fixed Issues for FlexGrid and InputNumber.
    [349164] [Internal] Request to localize the tile of 'In bound mode' checkbox in all controls which is supported in Scaffold Insert/Update UI.
    [349185][349185] [Internal] Auto Generate Columns checkbox is not checked after binding data model classes is set.
    [349187] [Internal] [Regression] The last column is missing in Column tab of Update Scaffold UI if  AutoGenerate column is not used in VB project.
    [347342] [Internal] Current values of some control cannot set in Update UI form and another method are added if setting is changed in Update UI.
    [347322] [Internal] FlexGrid GroupBy settings in GroupDescriptions cannot be filled in Update UI form.
    [347313] [Internal] Checkbox of BatchEditing works incorrectly in Update UI.
    [348210] [Internal] Error occur when update(i.e using Update UI) the flexgrid which is used option model.
    [349360] [Internal] Error occurs when update the control(i.e used value binding) with Update UI or value binding setting is set with Update Scaffold UI if control is created with HTMLHelper in Core project.
    [347229] [Internal] Add button is dim in Input/FlexPie/Sunburst/ control if Model class and Data Context class is not choose.
    [349189] [Internal] Error is occurred when opening "Update C1 MVC Control" of MultiRow after add DataMpap in desire column in not core project.
	[349188] [Internal] Columns are removed when updating MultiRow control without changing in Update UI form in not core project.
    [349268] [Internal] 'Object reference not set to an instance of and object' error occurs when open Update UI form in  add flexgrid/MultiRow if it is bind with certain scenarios.

Sample
    [347962] Cannot restore and error occurs after build the 'C1Finance(Core)' sample.
    [348761] Add Japanese Reports to FlexViewerExplorer sample. (rename report name)
    [347622] In Description of MVCExplorer -> Combobox -> Grouping sample page(Not Core), Listbox control is described instead of Combobox control for JP sample drop.
    [342260] Apply JP localizations to AccessibilityExtender.
    [342259] Apply JP localizations to ColumnsGroups.

DashboardLayout
    [349142] Icons in toolbar and resize icon are missing when show the tile from first children group using show specific option tiles after all tiles are hidden in ManualGrid and AutoGrid Layout.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.168)		Drop Date: 10/18/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20182.326

FlexGrid
    [339628] [Romaji] [iOS] First Charecter gets missed on quick editing.

Scaffolder
    [347391] [Internal] Cannot build the project and error occurs when settings are changed in Update UI if AutoGenerate column is not used in VB project.
    [347355] [Internal] ScrollPosition value is not updated in code although value is changed in Update UI form in Core project.
    [347342] [Internal] Current values of some control cannot set in Update UI form and another method are added if setting is changed in Update UI.
    [289882] [Internal] [Charts] 'FontSize' of Header Style for all Charts can be set by Scaffold but it does not take effect.
    [347358] [Internal] Add code to support case project don't have EF.
    [348127] [Internal] Add code to support case user input both Html builder and TagHelper in page.
    [348160] [Internal] 'Update C1 MVC Control' should be disabled when r-click on the control if that control is not supported in Insert/Update Scaffold UI.
    [348171] [Internal] 'Update C1 MVC Control' is disabled when r-click on the FlexSheet control if unboundsheet is added between two boundsheet in Core project.
    [348169] [Internal] Boundsheet and only one unboundsheet can be displayed in Update UI form when more than one unboundsheet is added after boundsheet in Core project.
    [347422] [Internal] Setting of Column items from Update UI do not take effect in FlexGrid control.
    [347434] [Internal] Some inconsistent behaviors are observed in FlexGrid, MultiRow and all charts if Model class is changed in Update UI form.
    [347450] [Internal] In VB project, Leading '.' or '!' can only appear inside a 'With' statement error occurs when some of the setting are changed using Scaffold Update UI.
    [347431] [Internal]  In FlexSheet control, another RemoteSave/RemoteLoad action and button of remoteSave action is added in view and controller page .

Item Templates
    [341041] Request to localized in Active Report's Item template dialog.

DashboardLayout
    [346959] Setting of 'AllowResize' property is not refresh in control if it is changed in runtime.
    [347975] Javascript error occurs when property setting is changed in runtime after tile is hide in all Layouts.
    [348207] Javascript error occurs when all tile are hide in split layout of MVCExplorer samples.

Samples
    [348479] Javascript error occurs when drag the range selector's thumb button until data in 'Category Sales vs. Goal' is lost in Dashboard sample page.
    [348761] Add Japanese Reports to FlexViewerExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.167)		Drop Date: 10/12/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20182.326

Scaffolder
    [347355] ScrollPosition value is not updated in code although value is changed in Update UI form in Core project.
    [347322] FlexGrid GroupBy settings in GroupDescriptions cannot be filled in Update UI form.
    [347334] Error occurs when navigate to Appearance tab of all Charts in Update UI form if some of the values are set in control.
    [346847] Add code to avaiable update for localize.
    [340193] Scaffold is missing on the Core project in VS 2017 (version 15.8.1).

Item Templates
    [336794] Add localization for new text in AR support in ReportViewer template.

DashboardLayout
    [346850] Javascript error occurs when drag the tile which is set with(*) in tile' size and hover on the cue boxes.
    [342059] Cannot drag the tile which has no header if that tile is drag and drop in any place of other tile and before selecting other tiles if 'ShowHeader' is set 'false' in Splitlayout.
    [346959] Setting of 'AllowResize' property is not refresh in control if it is changed in runtime.

Samples
    [343923] 'HTTP Error 500' error occurs when browse the AccessibilityExtender core sample with all solutions.
    [347142] Popup is closed immediately when choose the chart type and popup is flicker when click again on 'Settings' icon in 'Custom Tile' sample page  in touch devices (both mobile and ipad).
    [347420] Data cannot load in 'C1Finance' and 'StockChart' samples.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.166)		Drop Date: 10/08/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20182.326

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20183.165)		Drop Date: 10/06/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.524
Winform 4.0.20182.326

All
    Upgrade wijmo5 to build 523.

FlexGrid
    [341345] Blank row data is added in new position when dragging the row from original position and dropped in original position + more than 60 rows.
    [340268] Position of Dragged row is not retained in new position after edited the cells.
    [341461] The CustomEditor gets open while beginningEdit event cancel property is true.
    [333672] Add 'defaultSize' property of Wijmo RowColCollection class.
    [330653] Improved FlexGrid.virtualizationThreshold to support arrays: [rowThreshold, columnThreshold].
    [337291] CustomEditor do not allow editing if Grouping is allowed for FlexGrid.
    [339627] Error occur when multiple rows are copied and pasted in new row template.
    [332042] Add autoSearch property.

Scaffolder
    [330611] MVC Control Designer.
    [342366] Invalid EF model cause Scaffolder stop working.
    [322085] Filter cannot be added using scaffold dialog.

AutoComplete
    [316076] Unable to use AutoCompleteFor while data source is list of Object.
    [316076] Unable to use AutoCompleteFor while data source is list of Object.
    [339573] HTML tags are displayed in dropdown of AutoComplete even if the IsContentHtml is set to true.

DashboardLayout
    [341872] Blank area is appeared and tile in outer group cannot changed the size when resized the tile from outer group after resize the tile from the inner group at first.
    [342059] Cannot drag the tile which has no header if that tile is drag and drop in any place of other tile and before selecting other tiles if 'ShowHeader' is set 'false' in Splitlayout.
    [343450] Request to disappear the cue boxes if cursor is move to the outside of the dashboard.
    [343198] Javascript error occurs when drag and drop the tile at second time in Split Layout.
    [344191] Remove the center square in the cue boxes for SplitLayout.
    [342824] DashboardLayout disappear when user drags the ONLY Tile in Split Layout.
    [342567] Shaded portion for drop is still visible after dragging is cancel with 'esc' key if dragged tile is placed on cue boxes which is not outermost.
    [342681] Shaded portion is remained and dragdrop process failed if scroll mouse wheel and drop.
    [341814] The cue box is not appeared when drag the one tile and place on another tile if scroll bar is appeared after drag and drop the tiles.
    [343760] Resized Red Line display incorrectly when resize the Tile in Split Layout as certain conditions.
    [342851] Fixed value of Size property does not applied to Tiles from Horizontal Group in Split Layout and changed again after dragdrop action.
    [342703] Actual space after dropping is different with shaded portion when hover on cue box.
    [341259] Provide options to show specific tiles while tiles are hidden.
    [328184] Support 'Esc' key for dragging cancel.
    [341716] Improve Split Layout Drag & Drop feature.
    [334753] The OnClientFormatTile event will not be fired when first loading.

Samples
    [343879] [Wijmo5 MVC][FlexGridFullRowEdit][Sample] Editing row can not saved record after delete the other row while in edit state in FlexGridFullRowEdit sample.
    [343880][Wijmo5 MVC][LearnMvcClient][Sample] Settings and Exports icon is not shown properly in Custom Title sample.
    [342909][MVCExplorer] Javascript error occur after delete the data if select on multiple cells in both MVCExplorer samples.
    [343182][MVCExplorer] Error occurs when navigate to Grouping samples of ComboBox and ListBox control for Core sample.
    [341410] Request to localize in button title of setting dialog in DashboardDemo-Analysis sample.
    [343335][FlexViewerExplorer] Report cannot load and error occurs when navigate to AR report sample pages.
    [342472][MultiRowExplorer] In Core sample, 'ASP.NET MVC Edition Olap Explorer' is displayed instead of 'ASP.NET MVC Edition MultiRow Explorer' in task-bar.
    [343922] Like other Core samples, request to displayed 'ASP.NET MVC Core AcessibilityExtender' instead of 'ASP.NET MVC AcessibilityExtender' in read me file of Core sample.
    [341445] Maximize and more icons are displayed instead of setting icon in PivotGrid of Analysis sample page for Core sample of JP version.
    [341406] Javascript error occurs when changed the value in field setting for Region field in Analysis sample page.
    [342555] 'Uncaught TypeError: Cannot set property 'width' of undefined' error occurs when all field are removed from PivotPanel and data is not showed in PivotGrid.
    [341262] 'JavaScript runtime error: 'directions' is undefined' - error occurs after changed the value in 'Tab Position' in Core sample.
    [341459][MultiRowExplorer] 'CellGroup' image is missing and 404 Not found error occurs after browse the core sample. 
    [341457][MultiRowExplorer] Remove unuse file '~\App_GlobalResources\MultiRowExplorer1.designer.cs'.
    [340261][MVCExplorer] Group panel's placeholder is displayed incorrect words after changed the value in properties dropdown of setting in Core sample.
    [333346] Add TabPanel sample to LearnMVC client.
    [333347] Add Dashboard layout sample to LearnMVC client.
    [335850] Update the demo of changing Tab header position in the Styling page of MvcExplorer.
    [330608] Add ColumnGroups sample.
    [330610] Add AccessibilityExtender sample.
    [337294] Update UI of Dashboard Demo as per Material design.
    [336860] Vertical Scroll bar is displayed in side menu in some samples.
    [339571] Field setting dialog is closed and filtering is not cleared after click on the 'Clear' button of filtering.
    [235205] It will throw exception when editing new row when setting NewRowAtTop in Edit page.
    [332992][Localization] Add JP resource for OlapExplorer.
    [332990][Localization] Add JP resource for FlexViewerExplorer.
    [332991][Localization] Add JP resource for MultiRowExplorer.
    [332989][Localization] Add JP resource for FlexSheetExplorer.

ComboBox
    [342747] Implement and add sample for Group feature.

Item Templates
    [341053] Some inconsistent behavior is observed if Serivce URL and Report Path is add in ItemTemplate for other service or Active Report service.
    [335769] AR support in ReportViewer template.

Gauge 
    [336038] Add 'stackRanges' property.

PivotChart
    [341357] HeaderStyle and FooterStyle properties can not set properly in PivotChart.
    [332045] Add new properties.

Inputs
    [320060][iPad] Unlike the PC, dropdown is not closed by clicking on other region of the page in some input controls.

Core
    [334637] Create nuget package for Wijmo5 Styles.
    [340107] Zip the wijmo5 styles into the drop.

ODataCollectionView
    [332044] Add new properties.

PivotEngine
    [332046] Add new property (SortOnServer).

FlexGridDetailProvider
    [330652] Added a keyActionEnter property to the FlexGridDetailProvider class.
FlexViewer
    [330640] Added a requestHeaders property to the ReportViewer and PdfViewer controls.
    [330643] Added beforeSendRequest event to the ReportViewer and PdfViewer controls.
TreeMap
    [332043] Support SelectionMode.
ReportViewer
    [336034] Add new parameter to ReportViewer as the options to pass to run the report at the client side.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.164)		Drop Date: 08/22/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.500
Winform 4.0.20182.314

FlexGrid
    [339627] Error occur when multiple rows are copied and pasted in new row template.

AutoComplete
    [339573] HTML tags are displayed in dropdown of AutoComplete even if the IsContentHtml is set to true.

Samples
    [339571] Field setting dialog is closed and filtering is not cleared after click on the 'Clear' button of filtering.
    [235205] It will throw exception when editing new row when setting NewRowAtTop in Edit page.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.163)		Drop Date: 08/15/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.500
Winform 4.0.20182.314

FlexGrid
    [337291] CustomEditor do not allow editing if Grouping is allowed for FlexGrid.
Inputs
    [320060][iPad] Unlike the PC, dropdown is not closed by clicking on other region of the page in some input controls.

Samples
    [336860] Vertical Scroll bar is displayed in side menu in some samples.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.162)		Drop Date: 08/01/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.500
Winform 4.0.20182.314

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.161)		Drop Date: 07/31/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20182.500
Winform 4.0.20182.314

What's new in Wijmo5 20182.500?
    [a11y] Added a keyActionEnter property to the FlexGridDetailProvider class.
    Improved FlexGrid.virtualizationThreshold to support arrays: [rowThreshold, columnThreshold].
    Added a FlexGrid.autoSearch property that allows users to search for content in the grid by into non-editable cells.
    Added an ODataCollectionView.expand property to allow retrieval of related entities (as in OData's $expand option).
    Added an ODataCollectionView.jsonReviver property that allows customization of the parsing process for JSON data returned from the server.
    Added a beforeSendRequest event to the ReportViewer and PdfViewer controls.

Samples
    [333315] Update package references in DashboardDemo sample.
    [332320] Fix OData Service Url in the samples.
    Using JP resource for summary in MvcExplorer->FlexGrid->CustomFooters sample.

DashboardLayout
    [334753] The OnClientFormatTile event will not be fired when first loading.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.160)		Drop Date: 07/06/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.314

Samples
    [332311] Update the jp resources in the samples.
    [332320] Error occurs if accessing online samples via https.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.159)		Drop Date: 07/04/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.314

License
    [328745] License error does not occurs when Eval key or LS Expired key(e.g 2017v1 SU key) is generated after +30 days.

DashboardLayout
    [330264] Some of the content of 'Sales Dashboard for 2018' is missing in all sample pages when click on the showAll icon from any tile if 'Sales Dashboard for 2018' tile is hide in maximum state.
    [331380] The problem with "ShowAll" when setting a tile's Visible to false and the context menu problem in grid layout.
    [328195] Displayed inconsistent number of boxes that indicates possible positions where the dragged tile will drop in splitlayout.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.158)		Drop Date: 07/02/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.314

Samples
    [330700] Error occurs when browse the 'CustomReportProvider' sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.157)		Drop Date: 07/02/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.314

Samples
    [330362] Update supported cultures in CORE samples.
    [330280] Data are lost in 'Category Sales vs Goal' and 'Regional Sales vs Goal' charts when Drag the thumbnail button until data cannot display in Funnel chart of 'Annalysis' sample.
    [330695] Update the link in the mvc and api samples.

MultiSelect, MultiAutoComplete
    [329948] Model values do not get updated for integer type SelectedValues.

DashboardLayout
    [330264] Some of the content of 'Sales Dashboard for 2018' is missing in all sample pages when click on the showAll icon from any tile if 'Sales Dashboard for 2018' tile is hide in maximum state.
    [328195] Displayed inconsistent number of boxes that indicates possible positions where the dragged tile will drop in splitlayout.

All
    [330673] Update the year range to "1978-2018" in the license readme for Scaffolder, ItemTemplates and ProjectTemplates.

License
    [328745] License error does not occurs when Eval key or LS Expired key(e.g 2017v1 SU key) is generated after +30 days.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.156)		Drop Date: 06/27/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.313

Core
    [329318] Apply JP xml documents.

FlexGrid
    [325130] Editing is not working properly in IE if UnobtrusiveValidation is applied.

InputNumber
    [329410] Format property doesn't work correctly.

Samples
    [329433] Error occur when add and delete the new data in Custom Editor.
    [329008] 'Uncaught 500 Internal server' error occurs when new record is deleted.
    [327074] Extract text for DashboardLayout control sample pages into resources.
    [327737] Scroll bar are displayed in side menu if JP culture is set.
    [328864] Data cannot load and '500 (Internal Server Error)' error occur in 'Tasks' page for Dashboard Demo Core sample.
    [329491] Update Dashboard overview sample with grid layout in MvcExplorer.
    [329546] Update the DashboardDemo samples.
    [329574] Update GTM in samples.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.155)		Drop Date: 06/21/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.312

Scaffolder
    [327677] Request to add description for 'Multiline' property in scaffold dialog.

InputColor
    [328471] The control value cannot set to null although 'IsRequired' property set with false in InputColor control.

FlexGrid
    [325130] Editing is not working properly in IE if UnobtrusiveValidation is applied.

Samples
    [327737] Scroll bar are displayed in side menu if JP culture is set.
    [327697] Icon is not Asian market C1 icon in MVCExplorer Core sample.
    [327735] Some of the words in side menu and setting need to localize.
    [327764] Request to localize in some sample pages.
    [328166] Error occur when drag the range selector's thumb button in  "Dashboard" or "Analysis" pages for Dashboard Demo Core sample.

DashboardLayout
    [327037] "Uncaught TypeError: Cannot read property 'saveImageToFile' of null" error occurs when click on the Export icon.
    [327730] In SplitLayout, when dragging a tile and dropping it without selecting any clue icon, the dragged tile will disappear after droppping.
    [327768] Error occurs 'AllowResize' property set with false in tile of SplitLayout.
    [327119] Request to add more groups in AutoGrid Layout and ManualGrid Layout sample pages.
    [327763] Javascript error occurred when hide a tile and drag another tile if set with two tiles in SplitLayout.
    [328191] Display the extra space beside the vertical scroll bar of Dashboard layout when set with small value of height to layout.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.154)		Drop Date: 06/15/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.310

Samples
    [326987] Javascript error occurs when browse 'FlexChart(HowTo) -> MultiSelection' sample.
    [326789] Apply JP Resources to some samples.
    [326909] Compile error occurs when browse the sample.
    [327015] Layout for controls are disorted in DashboardDemo->Anaylsis sample page.

Scaffolder
    [325634] Request to display with Dropdown to the value of 'TitleAlign' in Chartlegend of Chart controls.

Project Templates
    [326950] Error occurs when rebuild the Core/Framework(Core 2.1) project.

DashboardLayout
    [326879] FlowTile's allowResize property does not work.
    [326888] 'AllowHide' property doesn't work properly in certain scenarios.
    [326897] Request to define the proper default size of tile when resize and minimize the tile.
    [326911] Error occurred and more item icon is displayed although 'AllowHide', 'AllowMaximize' and 'AllowShowAll' properties  are set with false.
    [327037] "Uncaught TypeError: Cannot read property 'saveImageToFile' of null" error occurs when click on the Export icon.
    [327115] Content of tile is disappeared when click on the load layout button if tile is maximize.
    [327116] Error occurs when click on the showAll icon if one of the tile is hidden and changed properties dropdown.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.153)		Drop Date: 06/10/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.310

FlexViewer
    [317063] Textbox becomes temporary disabled twice after enter the value if cursor is still in textbox.

OData
    [316044] '404 bad request' console error occurs when bind with 'BindODataVirtualSource'.

Samples
    [324699] Add Dashboard sample into Sample Applications list in MvcExplorer.
    [324659] Error occurs when sorting on the picture and description column in "Creating Programmatically" sample page of TabPanel control.
    [324791] Data are not displayed in Editing and Batch Editing sample pages of flexgrid.
    [324508] Error occurs when click on the content whileloading is still progress in Dashboard Layout control sample.
    [324510] ChartType dialog cannot close using 'x' in DashboardLayout->CustomTile sample page.
    [325649] Default value of 'Layout Cell Size' is inconsistent in Dashboard Layout.

DashboardLayout
    [325512] Add new features to DashboardLayout control.
    [325549] Add some configurations for DashboardLayout.
    [325131] 'Layout Direction' cannot take effect in FlowLayout of Dashboard Layout control.
    [324741] Content of tile is disappeared after dragging at Maximize state.
    [324622] Javascript error occurred when place the cursor on the control after resize the browser if Dashboard layout control is maximize.
    [324672] The height of content cannot maximize.

Project Templates
    [325929] Update the version number for Core 2.1.

FlexGrid
    [325130] Editing is not working properly in IE if UnobtrusiveValidation is applied.    

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.152)		Drop Date: 05/29/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.462
Winform 4.0.20182.309

All
    Rollback wijmo5 to build 462.

Samples
    [316073]Some inconsistent behavior is observed in SSAS Data Engine when check/uncheck and dragging the PivotPanel.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20182.151)		Drop Date: 05/27/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20181.474
Winform 4.0.20182.309

All
    Upgrade Wijmo5 to build 462.
    Upgrade Wijmo5 to build 474.
    [315864] Use JP xml docments for JP build.

What's new in Wijmo5 2181.462?
    [wijmo.pdf] Moved to PDFKit 0.8.3.
    [wijmo.pdf] PdfPageArea.openImage() method added.
    [ReportViewer] Added support for new properties of string parameters, maxLength and allowBlank.
    PivotGrid now honors the allowDragging property and allows users to reorder the row fields by dragging their column headers.
    Optimized the FlexGrid.autoSizeRows method.

Core
    [313354] Appearance of C1 control is not correct when we set tag helper with one tag.
    [311038] Request web resources without MVC routing in RazorPages project.
    [270800] Cannot set Culture as Traditional Chinese.

Samples
    [319871] [MVCExplorer] Height of group features in FireFox browser is larger than other browsers and selection cannot displayed properly.
    [313256] Add sample for different layout.
    [322041] Add MaterialDesignLite sample.
    [322042] Add ThemeDesigner sample.
    [323281] Some inconsistent behavior are observed in Table sample.
    [319612] [MvcExplorer][MultiRowExplorer] 'undefined' is displayed in textbox of detail editing when editing the new row without deleting any row.
    [315793][FlexSheetExplorer][FlexSheet101] Some inconsistent behavior are observed in result of 'Total row' for Table sample.
    [315804] [ExcelBook] Request to disable the Merge icon when select the cell in table.
    [315813] [ExcelBook] 'Table Style Options' are displayed in toolbar although delete the flexsheet columns.
    [313250] Add JP Resources in FinancialChartExplorer sample.
    [317576] Update MvcExplorer with features group and TabPanel.
    [318491] Appy JP resources to DashboardDemo sample.
    [318865] Group the features for FlexGrid and FlexChart in MvcExplorer sample.
    [313251] Add JP resources in MvcExplorer sample.
    [315791] [MVCExplorer] Row height is very small during the editing data after delete the all rows.
    [313248] Add multiline option in Editing sample of FlexGrid in MvcExplorer.
    [315457] Fix some localization bugs in LearnMvcClient sample.
    [313241] Add sample for table in FlexSheetExplorer and FlexSheet101.
    [313242] Upate ExecelBook for table.
    [313227] Add sample to FlexViewerExplorer for ActiveReports support.

Project Templates
    [321713] Check box and description are overlapped in Componentone Application Wizard.
    [315804] [SpreadSheet] Request to disable the Merge icon when select the cell in table.
    [315813] [SpreadSheet] 'Table Style Options' are displayed in toolbar although delete the flexsheet columns.
    [320679] Replace C1 with ComponentOne in JP build.
    [321551] Add missing metadata in vsix for EN build.
    [316504] Request to get focus on 'OK' button in project wizard form.
    [315963] Add CORE 2.1 in all MVC and API project templates.
    [313242] Upate SpreadSheet project template for table.

Item Templates
    [322046] Title of ReportViewer and PdfViewer are cut-off in item template wizard for japan environment.
    [320679] Replace C1 with ComponentOne in JP build.
    [321551] Add missing metadata in vsix for EN build.

Scaffolder
    [322085] Filter cannot added using scaffold dialog.
    [322125] Add 'RepeatButton', 'AutoScroll', 'Title' and 'TitleAlign' properties in scaffold for related control.
    [322096] Error occurs when controls are added by scaffold  in Razor page if Read action is used in VS (15.7.1).
    [316933] Error occurs if 'FlexSheet'control is added by scaffold in all AspNet MVC templates.
    [320679] Replace C1 with ComponentOne in JP build.
    [321551] Add missing metadata in vsix for EN build.

Dashboard
    [306331] Update the DashboardDemo sample with the Dashboard control.

FlexGrid
    [317573] CustomEditor do not appear for FrozenColumns.
    [323297] Value do not get update on pressing Tab key, after making changes for custom editor.
    [313201] Add new properties.

InputDateTime
    [320086][iPad] Height of time dropdown is very small and cannot see the data in time dropdown if 'TimeMax' and 'TimeMin' properties are set.

InputTime
    [320091][iPad] InputTime control becomes disabled when Min and Max properties are set.

ListBox
    [319488] Item template does not work when refreshing the ListBox control.

FlexSheet
    [316847] Unlike the FlexGrid, column filter works as a default filter type if default filter type in flexsheet filter is set with "Condition/Value/None".
    [317116] 'DataMap' property in value filter cannot work properly.
    [316183] Javascript error occurs when 'BindODataVirtualSource' is used in sheet table.
    [313212] Expose FlexSheetFilter to FlexSheet.
    [313213] Add new properties for Workbook.
    [313229] Add new properties for table support.
    [315077] Support binding data to Table.

AutoComplete
    [316076] Unable to use AutoCompleteFor while data source is list of Object.

Pager
    [316031] 'OnClientRefreshed' and 'OnClientRefreshing' events do not fire when control is initialized.

Calendar
    [316574] 'Refreshing' event does not fire when the control is initialized.

FlexPie
    [316640] Some consistent behaviours are observed when 'onClientRefreshed' and 'onClientRefreshing' events fired.

Sunburst
    [316640] Some consistent behaviours are observed when 'onClientRefreshed' and 'onClientRefreshing' events fired.

TreeView
    [316640] Some consistent behaviours are observed when 'onClientRefreshed' and 'onClientRefreshing' events fired.

TreeMap
    [316640] Some consistent behaviours are observed when 'onClientRefreshed' and 'onClientRefreshing' events fired.

InputNumber
    [316103] 'RepeatButtons' property can't work correctly when the property is set as 'False'.

Report Viewer
    [316429] The AllowBlank and MaxLength of parameter does not work.

FlexChart
    [312375] Adding new properties for ChartLegend.

CollectionView
    [313198] Add new property (ShowDatesAsGmt).

Control
    [313200] Add new properties.

Input
    [313203] Add new properties.

MultiRow
    [313208] Add new properties.

PivotEngine
    [313244] Add new properties for direct SSAS cube connection.
    [313245] Add sample for direct SSAS cube connection in OlapExplorer.
    [313210] Add new properties.

PivotPanel
    [313211] Add new properties.

TabPanel
    [313209] Add new TabPanel control.
    [313220] Add new samples for TabPanel usage.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20181.150)		Drop Date: 03/15/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20173.430
Winform 4.0.20181.296

All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20181.149)		Drop Date: 03/07/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20173.430
Winform 4.0.20181.296

All
    Upgrade Wijmo5 to build 430.

What's new in Wijmo 5.0.20173.430?
    * [accessibility] Improved ARIA support on FlexGrid, FlexGridFilter, and ComboBox components.
    * [Olap] Added support for binding PivotEngine components directly to SSAS cubes.
    * [ReportViewer] Added support for the GrapeCity ActiveReports report generator.
    * Added a wijmo.nav.TabPanel control that provides tab navigation. This new control can be used to replace bootstrap tabs, which require bootstrap.js and jQuery.
    * [olap.PivotPanel] Added support for hierarchical field structures in non-cube data sources.
    * [Excel] Added support for tables in the FlexSheet control and wijmo.xlsx module.
    * [FlexSheet] Expose _filter as public property instead of private.
    * [FlexGrid] Added a Column.multiLine (and Row.multiLine) feature that supports multi-line text in cells.
    * [FlexGrid] Added support for Column.textAlign = 'justify-all'.
    * [Globalize] Added support for escaped characters in date formats, e.g. wijmo.Globalize.format(date, '\\h h \\m m');
    * Added a FlexGrid.autoScroll property that determines whether the grid should automatically scroll its contents when the user drags rows or columns near the edges of the control.

Samples
    [112551]Some date time value with mask can be deleted in Input101 sample.
    [310855]Update the ODTA sample page with CRUD in MvcExplorer.
    [311405]Inconsistent display in Multi-Column Dropdown of MultiSelect.
    [311304]Apply JP documentation links in LearnMvcClient sample JP resources.
    [311678]Error occurs when navigate to FlexSheetExplorer->ServerRead sample in Mac.

Scaffolder
    [311332]Error occurs if controls is added by scaffold in Asp.net Mvc Core/Framwork project.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20181.148)		Drop Date: 03/02/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20173.428
Winform 4.0.20181.296

All
    Upgrade Wijmo5 to build 428.

License
    [308926]Invalid certificate error occurs when run the ASP.NET Core application on Visual Studio for MAC.

Project Templates
    [308852]In MVC 5 Templates, 500 Internal server error occurs in certain scenarios.
    [311050]Some inconsistent behavior are observed between 'Spreadsheet' and 'Excelbook' sample.

FlexChart
    [308980]DateTime values on XAxis not getting displayed correctly in Android browser and IPAD Safari.

Samples
    [310210]Localize for DashboardDemo samples.
    [228134]JavaScript runtime errors occur when value less than min value is set in InputNumber.
    [112551]Some date time value with mask can be deleted in Input101 sample.
    [144527]The dialog will flicker after rendering popup sample in Google Chrome.
    [311221]Replace "C1" in JP resources to "ComponentOne" in LearnMvcClient sample.

FlexSheet
    [308063]Javascript error occurs after add the new sheet if 'DisableServerRead' is false and bound sheet is already added.
    [311059]SelectedSheetChanged event fired twice in FlexSheet control.

Scaffolder
    [310355]Put C1 Scaffolder under RazorPages node for RazorPages project.
    [309321]Insert Scaffolder should use existing DataContext instance.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20181.147)		Drop Date: 02/25/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20173.427
Winform 4.0.20181.296

All
    Upgrade Wijmo5 to build 427.

Project Templates
    [270789]It pops up message for missing SQL Server 2012 when creating MVC5 template of only VS2017 environment.

Samples
    [308863][DashboardDemo] Javascript error occurs when navigate to 'Reporting', 'Tasks', 'Products' sample pages.
    [310102][DashboardDemo] Request to update the name in read me file.
    [308878][DashboardDemo] Console error occurs when navigate to 'Analysis' sample page from 'Dashboard' sample page if charts are disappeared in both sample pages.
    [310209] Update structure of some samples.
    [309066]Cannot go back to original state for Bold, Italic, Underline in ExcelBook sample.

FlexGrid
    [309405]Cell focus alternates between cells when quickly pressing Enter key on Custom Editor.
    [307959]Difficult to retain the scroll position when drag/drop the scroll bar if 'InitialItemsCount' property is set.

Olap
    [306286]Javascript error occurs when loading the data in 'DataEngine Service (SSAS)' sample page.
    [305693]DataEngine will thrown an exception when the field in Columns area has multiple members which have same captions.
    [301806]Filter do not get applied if Field is in Filter Area and DataSource is SSAS cube service.
    [302203]The aggregated result is not correct for some view definition.
    [301799]Javascript error occurs when currency format is set in Date column of Pivot field through Field Setting.
    [304425]Javascript error occurs when invalid format is set in Date column of Pivot field through Field Setting.

OData
    [305876]Javascript error occurs when filtered by second condition if first condition is not set and 'FilterOnServer' is set true .

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20181.146)		Drop Date: 02/09/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20173.403
Winform 4.0.20181.293

FlexPie
    [307214]Animation does not work when browse the project if selected index is more than 0 and 'RemoteBind' is used.

Input
    [304961]Some inconsistent behaviors are observed when min value is greater than value.
    [306847][ComboBox] Setting SelectedValue doesn't work when remote binding ComboBox.
    [307945]Javascript error occurs when run 'Input101' samples.
    [304961]Unlike the Wijmo 5 build (5.20173.380), some inconsistent behaviors are observed when min value is greater than value.

Samples
    [307002]Editing cannot be canceled after column is moved/resized in FlexGridFullRowEdit sample.
    [307158][DashboardDemo] Console error occurs when drag the range selector's thumb button until the data of chart is disappeared in 'Analysis' sample.
    [307165][DashboardDemo] Console error occurs when navigate to 'Dashboard' sample page from other sample pages.    
    [307337][DashboardDemo] Update splitter style and open/close animation in Dashboard and Analysis pages.
    [307166][DashboardDemo][Chrome] Some inconsistent behavior are observed after export the files in 'Reporting' sample page.
    [307157][DashboardDemo] 'Readme' and 'scrrenshot' files are missing in Dashboard Demo samples.
    [307305][LearnMvcClient] Console error occurs when navigate to 'MVC -> Other service -> Glyphs' sample page.
    [307307][LearnMvcClient] Error occurs when changed the color in 'Default' and 'Custom' dropdown of 'InputColor' sample page.
    [307308][LearnMvcClient] Some inconsistent behavior are observed when editing in 'FlexGrid -> Editing -> Custom Editing -> Inline Editing' sample page.
    [139411]Error 'Cannot read property 'series' of undefined' is observed after refreshing the webpage in 'WeatherChart' sample.
    [308016][PdfViewer] Error occurs when click on 'Export' button without choosing any file.

Project Templates
    [307661]'Sequence contains no elements' alert is displayed after save the new records if all records which is displayed in FlexGrid are deleted.

Scaffolder
    [308402]'Binding Name' are cut off when 'Product Model Class' is changed in Scaffold dialog.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20181.145)		Drop Date: 02/02/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20173.403
Winform 4.0.20181.292

FlexPie
    [306182]Selected index cannot work properly when 'RemoteBind' is used.

Input
    [304961]Some inconsistent behaviors are observed when min value is greater than value.

Samples
    [305977]Update font status on selection changed for ExcelBook sample.
    [306712]Update the tooltip for MVC GanttChart howto sample.
    [305710]Update readme.txt for GanttChart howto sample.
    Add the DashboardDemo howto sample.
    Add JP resourses in LearnMvcClient sample.

Project Templates
    [305984]Different behaviors of CS and VB templates are observed when adding incorrect data in 'Country' and 'Hired' column and after refresh the browser.
    [305990]Data cannot be sorted properly in 'Country' column when adding the invalid data for VB project.
    [306471]Incorrect date of copy right status in Razor projects.

Scaffolder
    [306346]Request to add "AutoSizeMode" property in FlexSheet Scaffold dialog box.

OData
    [306786]"500 Internal Server" error occurs when invalid value is set in 'MoveToPage' property and 'PageOnServer' is set true.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20181.144)		Drop Date: 01/26/2018
=====================================================================================
Dependencies
------------
Wijmo 5.20173.403
Winform 4.0.20181.292

All
    Upgrade Wijmo5 to build 403.
    Upgrade Wijmo5 to build 380.
    Upgrade major version 20173 to 20181.
    [305070]Add iconUrl for nuget packages in En build.
    [301749]Serialize ExpandoObject as dictionary.

What's new in Wijmo 5.0.20173.403?
    * Improved FlexGrid layout performance.
    * Improved export to xlsx performance for FlexGrid containing Angular cell templates.
    * [FlexGrid] Added a 'refresh' parameter to the CellEditEnding event parameters. This allows event handlers to suppress the full refresh that happens by default when cell editing ends.
    * [OLAP] Allow generating views with value fields only (grand totals) or row/column fields only (unique values).
    * [OLAP] Improved PivotEngine sorting of value fields (measures) to sort groups by aggregate. (this can be disabled by setting the new PivotEngine.sortableGroups property to false).
    * [OLAP] Improved PivotGrid collapse/expand to keep the original state of child nodes.
    * [OLAP] PivotGrid now honors the preserveOutlineState property (inherited from FlexGrid).
    * Added an InputNumber repeatButtons property that determines whether the control's spinner buttons should act as repeat buttons (fire continuously while pressed).
    * Added several new members to the TreeView and TreeNode classes:
        - TreeNode.addChildNode(index, dataItem) adds a child node to a TreeNode
        - TreeView.addChildNode(index, dataItem) adds a child node to a TreeView
        - TreeNode.remove() removes a node from the TreeView
        - TreeNode.refresh() refreshes a node with updated data content
        - TreeNode.itemsSource gets a reference to the array that contains the node data

What's new in Wijmo 5.0.20173.380?
    * [FlexGrid] Major performance enhancement: Added a quickAutoSize property to the wijmo.grid.FlexGrid and wijmo.grid.Column classes.
    * [FlexGrid] Added support for row/column resizing on touch devices.
    * [Accessibility] Improved (ARIA attributes and keyboard support) for the ListBox, ComboBox, Menu, InputNumber, Gauge, and Calendar controls.
    * Made some improvements in InputNumber's editing experience.
    * Improved popup positioning on mobile devices with pinch-zooming (including Android and IOS).
    * [wijmo.grid.pdf] Added support of cell text wrapping in FlexGrid.
    * [FinancialChart] Added new financial chart type 'PointAndFigure'.

Scaffolder
    Support Razor Pages.
    [305711]Update some labels for Razor Pages in general tab of options dialog.
    [305716]Select proper mode in core project which has both MVC model and RazorPages model.

FlexPie
    [304845] Data label is missing when use animation.

FlexChart
    [305206]Position and AxisLine do not work for chart series's AxisY.

FlexGrid
    [302756]Support UnobtrusiveValidation in FlexGrid without providing the Model of the view.

MultiRow
    [269997]The editor is not full of the related cell when MultiRow enters editing.
    [300966]Support null for MultiRow.CollapsedHeaders property.

FlexSheet
    [300592]"AutoSizeMode" property cannot be set in FlexSheet control.

ODataCollectionView
    [274081]Add ODataCollectionViewService and ODataVirtualCollectionViewService.
    [274083]Add the samples for ODataCollectionViewService and ODataVirtualCollectionViewService.
    [299461]404 bad request error occurs when 3 or more columns are filtered if 'FilterOnServer' is set true.
    [299470]Result of filter value which is used 'filter by value' cannot not retained when another column is filtered and if 'FilterOnServer' is set true.
    [302755]Data cannot load and loading message is not disappeared if 'MoveToPage'/'PageIndex' is larger than 0 and 'PageOnServer' is false.
    [299845]Console error occurs when 'MoveToPage' is set in 'BindODataSource' of FlexGrid.

Samples
    [304131]Create MVC GanttChart howto sample.
    [304132]Create MVC Calendar howto sample.
    [305731]Cache stock data in C1Finance and StockChart samples.
    [305969][305977]Update buttons on selection changed.
    [305196]Error occurs when insert the chart and filtering the data which does not exists the data for chart in ExcelBook sample.
    [304104]Editing cannot be canceled after column is moved in 'Inline Editing' sample.
    [302738]Cancel editing on scrolling the FlexGrid inline editing sample.
    [303245]Some inconsistent behavior are observed when edit the data in 'UnobtrusiveValidation' sample.
    [303359]Javascript error occurs when checked the 'Active' column checkbox in new row if there is no data in other column of MvcExplorer -> 'BuiltInValidation' sample.
    [301523]JavaScript errors occur when value which is less than min value or greater than max value is set in Period number for all Types in StockChart sample.
    [301788]Update the documentation link in samples.
    [303018]Row cannot be deleted in 'Editing' sample page of 'MultiRowExplorer' Core sample.
    [299474]Update url for getting stock events in StockChart sample.
    [299424]Cannot navigate to 'OData Service Bind' sample page of Core sample.
    [299774]Update the Point & Figure chart sample.
    [295424]Request to update C1 logo in some samples.
    [297430]Add a globalization sample in MVCExplorer.
    [298251]'favicon' and 'ci1ball' icons cannot display new C1 logo under 'HowTo' folder.
    [298531]Make LearnMvcClient sample localizable.

Project Templates
    [305969][305977]Update buttons on selection changed for SpreadSheet template.
    [299775]Run license manager after creating core projects via templates.
    [274116]Add project templates for Razor Pages.

Item Templates
    [302166]Error occurs when PdfViewer/ReportViewer Item Template is added in Framework Project which is used Razor Page.
    [274114]Support Item Templates for RazorPages project.
    [300564]Support Area in Item Templates.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.143)		Drop Date: 11/30/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.286

All
    [299014]Update nuget package icon url for JP build.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.142)		Drop Date: 11/30/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.286

Project Templates
    [298539]Version number of templates are displayed incorrectly in some vsix templates.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.141)		Drop Date: 11/29/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.286

All
    Update authors of vsix/nuget packages to "GrapeCity, Inc.".

Project Templates
    [298539]Version number of templates are displayed incorrectly in some vsix templates.
    [298546]Templates name is not consistent in Extensions & Updates of AspNET MVC 5 templates.

Samples
    [298660]License exception occurs after browse the 'RazorPages' sample in JP environment.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.140)		Drop Date: 11/26/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.285

All
    [298430]Update icons for 2017v3 JP release.

Project Templates
    [298165]MoreInfoURL is missing in C1.Web.Mvc.ProjectTemplates.Mvc4 template.
    [298176]'MoreInfoURL' value is missing and License agreement box is different with other vsix template in C1ProjectType.
    [298361] Different behavior are observed in EULA between vsix template and word file"(GC_EULA_DEV_TOOLS_(07-26-17)_JPN)".

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.139)		Drop Date: 11/21/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.282

All
    [298035]Update links for 2017v3 JP release.

Samples
    [295424]Request to update C1 logo in some samples.

CollectionView
    [296657]collectionChanged event fired twice after commiting the changes in FlexGrid.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.138)		Drop Date: 11/02/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.282

Samples
    [294708]'Funnel Type' control display twice in MvcExplorer->'FlexChart -> Funnel' sample.
    [295031]Request to update C1 logo in BookMyFlight sample.

FlexGrid
    [238510]In core project, value in filter dropdown cannot displayed after delete the record in grid which is filtering.
    [294944]Cannot save the update data in "FlexGridFullRowEdit" sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.137)		Drop Date: 10/31/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.282

Samples
    [294333]Update favicon of some samples.
    [294599]Update the link address of C1 logo in all samples.
    [294663]Update Yahoo Finance to Quandl in readme.txt of C1Finance sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.136)		Drop Date: 10/29/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.282

Samples
    [292921]Support request parameters in some pages of MvcExplorer sample.
    [292922]Unlike the other samples, "ASP.NET MVC" is missing in Title of readme file of Learn MVC Client Sample.
    [293714]Cannot navigate to other pages when publish the RazorPagesExplorer to IIS.
    [293711]Browser scroll down when 'Spacebar' is pressed to enter edit mode in MvcExplorer->FlexGrid/Custom Editors sample.
    [293725]Add more links at home page in MvcExplorer sample.
    [293730]Image lost in MvcExplorer->FlexChart/Annotation sample.
    [294035]Javascript error occurs after drag the thumb button of range selector in "FlexChart -> Chart Elements -> Extra Elements -> Range Selector" sample page.
    [293759]Sample with core20 solution cannot be loaded via IIS.
    [294008]Update Core samples to 2.0.

FlexGrid
    [238510]In core project, value in filter dropdown cannot displayed after delete the record in grid which is filtering.

InputTime
    [292315]Data in the dropdown cannot show properly when binding with customize items.

Project Templates
    [293729]Data cannot load and 404 Not Found error occurs when Ajax template is host in IIS.
    [294062]Error occurs after publish the SpreadSheet Core2.0 template project.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.135)		Drop Date: 10/23/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.359
Winform 4.0.20173.281

All
    upgrade wijmo to 5.20173.359.

What's new in Wijmo? refer to https://www.grapecity.com/en/blogs/wijmo-build-5-20172-359-available
- Improved accessibility by adding ARIA 1.1 built-in support.
- [FinancialChart] Added new financial chart type 'PointAndFigure'.
- Made Popup ignore Escape key while IME mode is active.
- Added a Popup.removeOnHide property to control whether the Popup should be removed from the DOM or just hidden when the popup closes.
- [ReportViewer] Hide the Parameters tab if all parameters are hidden.
- Added a MultiSelect.showSelectAllCheckbox property to display a "Select All" checkbox above the items, so users can select/de-select all items with a single click (in addition to the ctrl+A keyboard shortcut that performs the same function).
- Added a MultiSelect.selectAllLabel property to customize the label shown next to the "Select All" checkbox displayed when the showSelectAllCheckbox property is set to true.
- Added some configuration properties to the wijmo.olap.PivotEngine class: serverTimeOut: the timeout value for retrieving results from the server, serverPollInterval: the poll interval for getting progress reports from the server, serverMaxDetail: the maximum number of detail records to retrieve from the server.

FlexGrid
    [292587]Add KeyAction.CycleOut enum item.
    [216627]Can't cancel the "Editing" operation by pressing "ESC" key in "MVCExplorer-->FlexGrid-->Unobtrusive Validation" sample.

FlexViewer
    [277830]ReportViewer Height cannot be set to 100% using Height property.

Olap
    [271793]JS error displays after some operations in olap.

Project Templates
    [292306]C# template icons are displayed in 'ComponentOne ASP.NET MVC Application Wizard' although VB template is choose.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.134)		Drop Date: 10/19/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.357
Winform 4.0.20173.277

FlexGrid
    [274998]Focus does not shifts to the next cell when pressing Tab key on a cell having custom editor.

Samples
    [291317]404 Not Found error occurs for some icons when deploy Explorer samples on Azure.
    [291837]Add Tutorials section and update TreeMap value format in MvcExplorer.
    [253401]Buttons are not in a horizontal alignment in MvcExplorer->FlexGrid/Editing sample.

Scaffolder
    [289882]'FontSize' of Header Style for all Charts can be set by Scaffold but it does not take effect.

InputDate
    [288740]Javascript error occurs when value binding is used in InputDate/InputDateTime control of Core project.

Project Templates
    [291534]Logo of MVC classic is not updated and C1 logo cannot be displayed in all templates of AspNET MVC and WebApi.
    [290248]Javascript error occurs after click save button in the Ajax sample for all templates (Core, Framework, MVC 5 template).
    [291594]Template names are cut off in application wizard of Core/Framework/MVC 5 template.

InputTime
    [284259]Data in the dropdown cannot show properly when min/max value is set in input time control.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.133)		Drop Date: 10/13/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.357
Winform 4.0.20173.277

Scaffolder
    [289882]'FontSize' of Header Style for all Charts can be set by Scaffold but it does not take effect.

Samples
    [290420]Console error occurs when enter the value which is exceed than max value in "Neck Height" inputnumber in MvcExplorer->FlexChart/Funnel sample.
    [280445]Error occurs after restore the C1Finance sample.
    [290701]Range selector cannot be displayed properly and Javascript error occurs when range selector is moved in FinancialChartExplorer sample.

All
    [290382]Update c1 icons.

FlexGrid
    [290400]Editing mode is lost and cannot edit data when double-clicking on any cell in FlexGrid's Unobtrusive Validation sample.

Project Templates
    [279325]Javascript error occurs after browse the Ajax sample for all templates (Core, Framework, MVC 5 template).

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20173.132)		Drop Date: 09/29/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20173.357
Winform 4.0.20173.277

All
    Upgrade major version 20172 to 20173.
    upgrade winform build from 271 to 277.
    upgrade wijmo to 5.20173.357.
    [275140]Implement the new DateTime process and add unit test for it.
    [274071][274072]Add missing features of Wijmo5 in MVC.

What's new in DateTime process?
1) Kept the DateTime object on both server and client.
2) Provided the user two client events(OnClientReponseTextParsing and OnClientReponseTextParsing) to make custom transformations for DateTime objects.

What's new in Wijmo?
1) Added a new TreeMap control.
2) Improved accessibility by adding ARIA 1.1 built-in support to the following controls:
  - FlexGrid: https://www.w3.org/TR/wai-aria-1.1/#grid
  - TreeView: https://www.w3.org/TR/wai-aria-1.1/#tree
  - Gauge: https://www.w3.org/TR/wai-aria-1.1/#range
  - ListBox: https://www.w3.org/TR/wai-aria-1.1/#listbox
  - ComboBox: https://www.w3.org/TR/wai-aria-1.1/#combobox
  - Menu: https://www.w3.org/TR/wai-aria-1.1/#menu
3) Added two new properties to improve FlexGrid keyboard accessibility: keyActionTab and keyActionEnter. These properties allow you to customize the behavior of special keys so the grid becomes more accessible or more compatible with Excel.
4) Added a new property to improve FlexGrid screen-reader accessibility: rowHeaderPath. If provided, this property specifies the name of a binding to use as a provider of ��row header�� accessibility values. If not provided, most readers will use the content of the first visible column as the row header.
5) Added a new PivotField.sortComparer property to allow customization of the sort order in dimension fields. This is similar to the CollectionView's sortComparer property, except it applies to pivot dimensions (grid headers) as opposed to measures (summary data).
6) Added several new properties to make the Calendar control more customizable: formatYearMonth, formatDayHeaders, formatDays, formatYear, and formatMonths. All these properties represent format strings used to format different parts of the Calendar in month and year view.
7) Added time zone offset date format parts ('z', 'zz', 'zzz') to Globalize.formatDate.
8) Added a new FlexGrid.itemValidator property to improve validation support, especially for unbound grids (bound grids can be validated using the CollectionView.getError property which provides the same functionality).

CollectionView
    [276501]Add forceRefresh method in RemoteCollectionView.
    [275221]User encounters run-time error when he enable property "group-by".

FlexChart
    [276820]Question of usage of Bind method in FlexChart Series.

TreeMap
    [274069]Add TreeMap control.
    [274070]Add TreeMap sample pages.
    [277023]TreeMap is unable to display any legend, if I set its property "Legend" as "C1.Web.Mvc.Chart.Position.Right".
    [276482]Add sample page for adding TreeMap bread crumbs.
    [277317]Hide ShowAnimation() from html builder.

Samples
    Add LearnMvcClient sample.
    [274115]Add RazorPagesExplorer sample.
    [275141]Add DateTimeFiellds sample.
    [271762]There are some duplicate records in the sample project "C1 Finance".
    [274327]Hide SSRS credential from startup.cs.
    [287638]Filter.cshtml of MultiRowExplorer can't be deployed.
    [288730]Add LearnMvcClient sample Link in MvcExplorer sample.
    [285293]Update new icons and links in all MVC (except how to) samples.
    [288728]Update new icons and links in all MVC how to samples.
    [285289]Add google analytics tags in all web demo samples.
    [279728]Error occurs after navigate to "Popup -> Popup Dialog" sample page of core MvcExplorer sample.
    [279569]Javascript error occurs after navigate to "FlexChart -> Animation" sample page.
    [270741]There are two issues in "CS-->HowTo-->MultiRow-->MultiRowLOB" sample.
    [265176]Cannot show a complete appearance of file uploading button.

FlexGrid
    [279548]Console error occurs after clicked on any cell in "custom editors" page.

Project Templates
    Create MVC project templates for Core 2.0.
    [274091]Update MVC project template wizard for Core 2.0.
    [277908]Group templates (Standard, Model Binding, Ajax Binding, Spreadsheet) by updating the exisitng project wizard.
    [279280]Update description of standard MVC5 templates.
    [279554]Japanese text show on 'Freeze' button.
    [266095]The "Undo"/"Redo" is enabled with no operation in FlexSheet.
    [279332]All theme do not apply in all .NetCore (both core and framework) projects.

PivotEngine
    [287644]Expose new properties in PivotEngine as per Wijmo5.

PivotChart
    [277940]PivotChart cannot show Legend when I set property "ShowLegend" as "Auto".

Scaffolder
    [288740]Description of all properties of all controls in scaffold dialog box are not localized.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.131)		Drop Date: 08/25/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.300
Winform 4.0.20172.271

All
    Re-implement js cast method. (for 279569)

Samples
    [279728]Error occurs after navigate to "Popup -> Popup Dialog" sample page of core MvcExplorer sample.
    [279569]Javascript error occurs after navigate to "FlexChart -> Animation" sample page.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.130)		Drop Date: 08/21/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.300
Winform 4.0.20172.271

All
    [274060]Add JS cast method for each control/service/EventArgs.
    [273458]Fixed Controls on ASP.NET Core 2.0 issue.
    [275137]Fix MVC on Razor Pages issue.
    [274759]WebApi and Mvc control license check throws exception under asp.net core 2.0.
    [273460]Fixed Sample on ASP.NET Core 2.0 issue.

Project Templates
    [271445]Add MVC .NET Core templates for VS2015 under the .NET Core node.

Samples
    [271457]Update new controls section in the home page of MvcExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.129)		Drop Date: 06/30/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.300
Winform 4.0.20172.271

All
    [271101]Add dependencies in readme.txt.

Project Templates
    [271073]Hide VS2017 .NET CORE templates from it's parent folder.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.128)		Drop Date: 06/29/2017
=====================================================================================
Samples
    [270466]Need to delete the global.json file in some core samples.

Project Templates
    [270134]Can not restore packaging successfully if not points to packaging while adding dll by local.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.127)		Drop Date: 06/28/2017
=====================================================================================
Samples
    [269696]Web browser encounters JS error when I perform sample project "CustomPdfProvider".
    [270466]Need to delete the global.json file in some core samples.
    [270471]Can't restore the "C1.C1Report.4.dll" correctly in "CS-->HowTo-->PDFViewer-->CustomPdfProvider" sample.

CollectionView
    [269996]The event "OnClientError" can be triggered without any condition.

ProjectTemplates
    [270134]Can not restore packaging successfully if not points to packaging while adding dll by local.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.126)		Drop Date: 06/26/2017
=====================================================================================
Samples
    [268986]Update readme.txt for some samples.
    [269696]Web browser encounters JS error when I perform sample project "CustomPdfProvider".

FlexGrid
    [257083]ComboBox used as the EditTemplate of FlexGrid not getting displayed when placing FlexGrid in JQueryUI dialog.

Scaffolder
    [269695]C1 Scaffold generates wrong code for "GroupBy" method.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.125)		Drop Date: 06/24/2017
=====================================================================================
Olap
    [255409]Add new aggregates in Olap Server. (Merge from WijmoNxt)
    [240384]The detail row show no date if the fields contains "Date" in the OlapExplorer sample. (Merge from WijmoNxt)
    [262631]Client event "Error" doesn't implement on TagHelper.

FlexGrid
    [258675]Value Filter cannot select item which contains HTML element.

CollectionView
    [249138]Expose an event that user can customize error showing behavior.
    [269693]Fix unit test error of CollectionViewServiceTests.VirtualScrollingTest.

Samples
    [262088]It is better to adjust the panel to fit the data size.
    [268976]Rename "Custom Action" to "Incremental Search" in MvcExplorer->AutoComplete/MultiAutoComplete sample.
    [268977]Move how to PdfViewer sample to sub folder.
    [267823]Checkbox loses its state after you finish editing any record in FlexGridFullRowEdit sample.
    [266082]The extra columns displays in the Drop-Down-List of sort after load excel file into flexsheet in "CS-->HowTo-->FlexSheet-->ExcelBook".
    [268978]Implement a how to sample for custom pdf provider.

MultiSelect
    [267156]Setting CheckedIndexes and CheckedValues together will throw exception.

Project Templates
    [268980]There are some issues about project template in VS2012 in JP build.
    [269239]The code in Viewer which is created by "ASP.NET MVC5 App-Model Binding" in JP build is different from the code in EN build.
    [269479]JS error displays after rendering MVC5 application which is created by "C1 ASP.NET MVC5 Web Application(C#)" with "Include C1 Web Api DataEngine Library" in some case.

MultiSelect
    [256121]It is better to remove some properties, such as "Text", "OnClientTextChanged", "SelectItem" and "SelectValue".

InputColor
    [250304]The dropped-down list shows in "InputColor" after setting "IsReadOnly" and "IsDroppedDown" to true.

Scaffolder
    [190708]Request to add their own client events for LineMarker and RangeSelector.
    [141102]User cannot change the sequence of sorting fields.

All
    [256706]Please remove method "AddExtender" from FlexGrid.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.124)		Drop Date: 06/20/2017
=====================================================================================
Samples
    [249480]Data cannot be refreshed after updating fails in server side. (MvcExplorer, MultiRowExplorer)
    [268427]Images in MVCExplorer/Treeview/Images page do not show up when it is hosted on IIS.
    [247177]FlexViewerExplorer sample shows Mobile View in PC sometimes.
    [261753]Cannot submit a complete web form in MvcExplorer->MultiAutoComplete->Unobtrusive Validation.

Project Templates
    [268406]Translate Chinese resources in MVC template wizard.
    [268782]Adjust MVC project template wizard layout.
    [261787]Should improve the layout of the dialog for SpreadSheet project template.

PivotEngine
    [240384]The detail row show no date if the fields contains "Date" in the OlapExplorer sample.

All
    [236261]Ability to access associated fields in FlexGrid Template.
    [245769]Optimize MVC template binding.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.123)		Drop Date: 06/18/2017
=====================================================================================
Project Templates
    [266514]"Global.json" is opened after creating "ASP.NET Core Application" in VS2015.
    [264301]Cannot start MVC project that I create it without selecting "Create directory for solution".
    [267218]500 Internal server error occurs after save the data if user enter invalid date format in Hired column.
    [266092]JS error displays after inserting Chart to FlexSheet with "Eable Deffer Script" checked.
    [266110]The English resource displays in the toolbar of FlexSheet in JP build.

Item Templates
    [266835]Should add "C1.Win.Bitmap.4.dll" to application after adding viewer by "PdfViewerView" Itemtemplate.

Samples
    [266078]Change samples layout as standard style(HowTo/FlexGrid/FilterPanel and Finance).
    [267211]Can't add other company to the "Portfolio" in "CS-->HowTo-->C1Finance" sample.
    [267496]JS error displays after clicking  some links in Olap101 samples.
    [261658]Error is occurred after edit in "Product" column of "Custom Editors" page.
    [267533]JS error displays after input characters to "AutoComplete" control in "MVCExplorer-->InputNumber-->Form" sample.
    [267482]500 items displays in the FlexGrid after setting the "Items" to other number in "CS-->ASPNETCore-->MVCExplorer-->FlexGrid--Index" sample.
    [249480]Data cannot be refreshed after updating fails in server side. (MvcExplorer, MultiRowExplorer)
    [267474]JS error displays after check/uncheck "Custom Cells" in "CS-->ASPNETCore-->HowTo-->FlexGrid-->Financial" sample.
    [204783]Error is occurred when column is selected twice in new added level.
    [258032]Failed to run some not core samples by VS2017 installed only.
    [257930]Need to support running \ASPNETCore\HowTo\C1Finance sample by VS2017 only.
    [268194]Change FlexViewerExplorer default ZoomMode as PageWidth.

All
    [251069]Error displays after changing the "SourceCollection" of CollectionViewer.
    [266438]It throws exception when adding two controls in certain case.
    [250893]Can't trigger the "OnClientPageChanged"/"OnClientPageChanging" event of the CollectionView after paging.
    [262104]500 Internal server error occurs after filter on Flexgrid in "View and Edit the Source Data" sample.
    [251882]Update the documentation for the client objects in c1 wrapper.
    [257197]FlexGrid 'CellTemplate'd cells do not hide when cell edit mode is finished.

Scaffolder
    [266099]There are too much needless comma separators of FlexGrid Sorting.
    [266066]C1 Scaffold cannot generate code of property "Mask" of "InputTime".
    [266887]Call defaultFormatter in chart's itemFormatter event handler.
    [266135]Translate Chinese resources in Scaffolder.
    [249480]Data cannot be refreshed after updating fails in server side.

MultiAutoComplete
    [252403]Unlike the autocomplete, Dropdown opened/closed immediately when collection view service is used in MultiAutoComplete control.

FlexGrid
    [250924]JS error displays after set "CellTemplate" to FlexGrid in some condition.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.122)		Drop Date: 06/12/2017
=====================================================================================
All
    [249750]Exception occurs when using InputDate in a list.
    [238181]Write the Alpha value of Color to client.
    [266156]Fix unit test failed for html color converter.

FlexPie
    [238181]Alpha of pie slices is always set to 0.7 even when setting a custom palette with alpha value as 1.

MultiSelect
    [251860]"IsDroppedDown" of "MultiSelect" can't work.
    [198200]CheckedValues of MultiSelect does not work.

Samples
    [198200]The status of checked items are wrong after clicking "Submit" in "MVCExplorer-->MultiSelect-->Form" sample.
    [265358]The value of "sort-by" property of FlexGrid is incorrect in "CS-->ASP.NET Core--MVCExplorer-->Views-->FlexGrid-->Sorting" sample.
    [254869]Some inconsistance behavior are observed in title and display box of code of flexSheet101 sample.
    [265371]Remove legacy TS definitions from sample.
    [228066]Sample BookMyFlight, Javascript error occurs when user enter the desired city.
    [230243]Position of some cell is changed when 'Workbook' is exported.

FlexSheet
    [253590]There is no way to set Aggregate functions for Column Footer.

Project Templates
    [261731]Should remove the "~/Scripts/app/sheetdata.js" from the created application by "Spreadsheet" project template.
    [257826]Error dialog displays after creating Core application by VS2015 which environment contains VS2017.
    [262658]Request to change ENG words to JP words in ribbon's footer text of all spreadsheet template.

TreeView
    [265401]Refactor the properties in TreeView.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.121)		Drop Date: 06/08/2017
=====================================================================================
Project Templates
    [262630]Javascript error occurs after clicked on the save button of spreadsheet template.
    [261731]Should remove the "~/Scripts/app/sheetdata.js" from the created application by "Spreadsheet" project template.
    [262110]The JP resource doesn't implement to the application which is created by "ASP.NET Core MVC App - Spreadsheet" in JP build.
    [262658]Request to change ENG words to JP words in ribbon's footer text of all spreadsheet template.
    [261774]Compile error displays after build the application which is created by "ASP.NET MVC5 App - Ajax Binding(C#)" in JP build.
    [264735]Error occurs after build "C1 Asp.Net MVC4 application".
    [261695]JS error displays after rendering the application which is created by Mvc project template with "DefferScript" checked.
    [265220]Compile error displays after creating MVC5 Template with "Add TS File" checked.
    [265208]The character is cut off in the dialog of Mvc5 ProjectTemplate.
    [265280]Warning displays after creating MVC5 CS Template.
    [265202]Should add the code for license of DataEngine after creating the application by "C1 ASP.NET MVC5 WebApplication" with "Include C1 Web Api DataEngine Library" checked.

Scaffolder
    [265288]The right border of "MaxTime"/"MinTime" in Scaffold of InputTime/InputDateTime is cut off.
    [260983]Can't generate the "FlexChart-->Series-->Style" correctly after adding FlexChart by Scaffold in TagHelper(Reactive).

MultiAutoComplete
    [251859]Can't trigger the "IsDroppedDownChanged"/"IsDroppedDownChanging" of "MultiAutoComplete" after setting "IsDroppedDown(true)".
    [252391]Unlike the autocomplete, DropDownCssClass does not apply if "IsDroppedDown" property is set true.
    [253190]Unlike the autocomplete, DropDown is show and it does not disable if "IsDisabled" and "IsDroppedDown" property are set "true" at the same time.
    [252509]The Mvc control fails in attributes "SelectedIndex", "SelectedItem", "SelectedValue" and "SelectedValues".
    [260980]MultiSelect and MultiAutoComplete cannot be excuted in Asp.Net Core project.

MultiSelect
    [260980]MultiSelect and MultiAutoComplete cannot be excuted in Asp.Net Core project.

Samples
    [261640]Error occurs after navigate to "Custom Function" page in FlexSheetExplorer sample.
    [253593]Compile error displays after loading "VB-->HowTo-->FlexGrid-->FlexGrid101" sample in VS2013.

CollectionView
    [250893]Can't trigger the "OnClientPageChanged"/"OnClientPageChanging" event of the CollectionView after paging.

MultiAutoComplete & AutoComplete
    [253007]Properties "ItemTemplateId" and "ItemTemplateContent" are invalid in Mvc control "AutoComplete" and "MultiAutoComplete".

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.120)		Drop Date: 06/05/2017
=====================================================================================
All
    [256342]Install TS definition files in MVC directory.
    [253938]CollectionView's commit() method doesn't appear in intellisense in TypeScript.
    [257506]Jp resource is not recognized for "C1 ASP.NET Core MVC (.NET Framework)" project in JP Environment.
    [262185]FlexGrid ExtraRequestData (First letter of key is capital).

TreeView
    [252612]Internet error displays after setting the "LazyLoadFunction" of TreeView.
    [252754]Error occurred after checking the checkbox of parent node if child node is not expand when Lazy load function is used in treeview.
    [252751]TreeView cannot load into borwser when LazyLoad function is used.

PivotEngine
    [252461]Remove the "auto-generate-fields" attribute from PivotEngineTagHelper.

Project Templates
    [253333]Does not support TreeView's TS intellisense.
    [260602]Update MVC 5 templates for add reference api option.
    [261638]Update MVC 5 templates for restore MVC packages from nugnet.
    [263129]Suggest to upate the icon of new templates with formal.
    [261697]There should be a default name "app.ts" displays for the "Add TS File".
    [262934]Suggest to adjust the layout of speedsheet template.
    [263089]Need to adjust the order of template of ASP.NET MVC 5 App -SpreadSheet.
    [263949]Add .ja suffix for C1 MVC packages in JP MVC templates.
    [261850]Error dialog displays after creating application by using "ASP.NET Core MVC App - Model Binding(.NET Framework)(C#)" JP build.

Scaffolder
    [260978]Extra double quotation marks displays for the "Value" property of InputColor after inserting by Scaffold in TagHelper.
    [260983]Can't generate the "FlexChart-->Series-->Style" correctly after adding FlexChart by Scaffold in TagHelper.
    [263946]Cannot add DataMap in desire column if the control is added by using "Insert C1 MVC Control" tag.
    [260986]Can't generate the "FlexChart-->Annotation-->Point" correctly after adding FlexChart by Scaffold in TagHelper.
    [260985]Scaffold adds an invalid child node "c1-flex-chart-series" to the parent node "c1-flex-radar".
    [260981]The "Min"/"Max" property of "YFunction"/"parameterFunction" of FlexChart displays after inserting FlexChart without setting.
    [260989]Can't generate the "FlexChart-->Annotation-->Polygon-->Points" correctly after adding FlexChart by Scaffold in TagHelper.

Samples
    [250450]Update FlexChartAnalytics sample.
    [261640]Error occurs after navigate to "Custom Function" page in FlexSheetExplorer sample.
    [264394]Update the pdf file in FlexViewer sample with the DefaultDocument.pdf.
    [253310]Internet error displays after inputting invalid value to "BirthDate" column in "MvcExplorer-->FlexGrid-->UnobtrusiveValidation" and  "MultiRowExplorer-->UnobtrusiveValidation".

GridInForm Sample
    [136461]Cannot delete item when DeleteActionUrl is not set.

FlexSheet
    [253533]Hide property "NewRowAtTop" for FlexSheet.

InputTime
    [255373]Setting value takes no effect in MvcExplorer sample.

Sunburst
    [255671]"Tooltip" fails by working with "ShowAnimation".

Core
    [264174]Fix an issue that when C1CollectionItemConverterAttribute is used for a collection, the separator is missing between the collection item.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.119)		Drop Date: 05/26/2017
=====================================================================================

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.118)		Drop Date: 05/26/2017
=====================================================================================

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20172.117)		Drop Date: 05/26/2017
=====================================================================================
All
    upgrade winform build from 260 to 263.
    upgrade wijmo to 5.20172.300.
    Upgrade major version 20171 to 20172.
    [251467]Make MVC 3/4/5 packages for all MVC assemblies.
    Add MultiAutoComplete control and samples.
    Add TreeView control and samples.
    Add the missing features comparing to wijmo5.
    Refactor MVC models for insert mvc controls in scaffolder.

Scaffolder
    Refactor scaffolder for insert mvc controls. 

Project Templates
    [260906]Update JP resources.
    [256213]Update MVC project templates with new structure.
    [251450]Business app template. (SpreadSheet).
    [251448]Responsive app template with server binding. (Grid & Menu).
    [251449]Responsive app template with AJAX binding. (Grid & Menu).
    Make small changes for supporting VS2017.
    [218118]User is unable to recover some references if he doesn't select "Create directory for solution".

Olap
    [255409]Add new aggregates in Olap Server.
    [256239]JS error displays after setting "Fields" of DataEngine to multiple fields.

PdfViewer
    [247145]Add search feature for PdfViewer.
    [250371]Add offical license verifying.
    [244631]Reduce the bar height to make the mobile UI better

FlexViewer
    [253832]Setting ZoomMode or ZoomFactor will throw exception.
    [253373]"Go to back" and "Go to forward" are disabled.
    [250463]Implement rotate document.
    [250461]Implement rotate page.
    [253724]Implement mouseMode for FlexViewer.
    [250083]Implement magnifier tool.
    [244790]Some localization of JP displays English characters.
    [228331]Javascript error is occurred when Print dialog is opened.
    [244659]Backward/Forward can't work well in mobile view.
    [244631]Reduce the bar height to make the mobile UI better

FlexGrid
    [256210]Javascript error occured when setting the ShowColumnFooters property for a FlexGrid which uses cell templates.
    [251320]FlexGrid Selection takes no effect.
    [236768]Memory usage increases when navigating between web pages rendering C1FlexGrid.

PivotEngine
    [251144]The "Error" property of the PivotEngine should change to "OnClientError" event of PivotEngine.
    [251030]The "ViewDefinition" property of PivotEngine is missed in Core application.

MultiRow
    [251073]It does not delete "OnClientDraggingColumnOver" and "OnClientDraggingRowOver".

Samples
    [251461]Add the Finance sample.
    [260014]Update C1Finance and StockChart sample with stock data from quandl.
    [251460]Add FilterPanel sample.
    [260014]Update C1Finance sample with stock data from quandl.
    [256215]FlexChart Gradient Support.
    [257885]Can not build VB Sample succesfully when only installed VS2017, such as FlexChart101.
    [251116]Content formatting issue in Editing view of MvcExplorer and MultiRowExplorer samples.
    [149092]Move database from mvc/webapi samples to common database folder.
    Remove useless assembly and package references in MvcExplorer(not .NET Core).
    [247190]No screenshot in ReportViewer 101.
    Make small changes for supporting VS2017.
    Add C1.Win.Bitmap.4.dll as the reference of FlexViewerExplorer, PdfViewer and CustomReportProvider samples.
    [236892]MuliRow LOB Sample.

Olap101
    [253579]JS error displays after rendering "CS-->HowTo-->OLAP-->Olap101" sample.

CollectionView101
    [253571]Can't create new row correctly in "CollectionView101" sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.116)		Drop Date: 05/09/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.271
Winform	4.0.20171.260

All
    Just upgrade winform build from 259 to 260.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.115)		Drop Date: 05/05/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.271
Winform	4.0.20171.259

MultiRowExplorer Sample
    Correct the spelling mistake (Cutomer -> Customer).

FlexViewer
    [244790]Some localization of JP displays English characters.

Scaffolder
    [244790]Some localization of JP displays English characters.

FlexGrid
    [256210]Javascript error occured when setting the ShowColumnFooters property for a FlexGrid which uses cell templates.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.114)		Drop Date: 03/01/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.271
Winform	4.0.20171.248

Item Template
    [244717]Can't find the .flxr file in the dialog of ReportItemTeplate in JP build.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.113)		Drop Date: 02/24/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.271
Winform	4.0.20171.243

Scaffolder
    [243281]Request to remove the "Hide Grouped Columns" from multirow scaffold's dialog box.

MvcExplorer
    [242877]Update new controls in MvcExplorer.

FlexViewer
    [242884]Multiple issues are observed on Mobile UI template when Tool menu is opened.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.112)		Drop Date: 02/23/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.271
Winform	4.0.20171.244

FlexGrid
    Add remarks to mention when the aggregated values work.

Item Template
    [242642]Error dialog displays after adding viewer to Core application by Itemtemplate in VS 2017.

Project Templates
    [242720]TypeScript doesn't support FlexRadar.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.111)		Drop Date: 02/22/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.270
Winform	4.0.20171.243

ProjectTemplates
    Make new project templates for vs2017.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.110)		Drop Date: 02/21/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.270
Winform	4.0.20171.243

Samples
    [233974]Some buttons don't work in FireFox and IE 11.
    [239322]Error displays in the "MVC application-->Property-->Build" which is created by VS2017.
    Improve ReportViewer101 sample code.

Scaffolding
    Translate zh resources.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.109)		Drop Date: 02/17/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20171.270
Winform	4.0.20171.243

MultiRowExplorer
    [239310]Update Editing view with table that has more columns.
    [239246]Refactor the grid customized editor and its sample.

ExcelBook Sample
    [239280]Filter button on toolbar is dimmed when unbound sheet is selected.

Sample
    [239156]Can't edit the MultiRow in "MultiRowExplorer-->Editing-->InlineEditing" sample sometimes.
    [233974]Some buttons don't work in FireFox and IE 11.

FlexSheet
    [235785]Property "ShowFilterIcons" doesn't take effect.

FlexGrid
    [239246]Refactor the grid customized editor and its sample.
    [229462]The pasting value is missing in FlexGrid.
    [239246]Refactor the grid customized editor and its sample.

MultiRow
    [235650]The property "WordWrap" of "Cell" in MultiRow can't work.

FlexViewer
    [238137]Can not open FullScreen on iphone by safari browser.
    Fixed sidebar remain white space on safari with mobile template issue.
    Shrink toolbar button size with mobile template.
    Implement search bar input auto width.

FlexViewerExplorer
    Improved FlexViewerExplorer menu touch behavior on mobile.
    Implement FlexViewer height auto changed with different screen width.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.108)		Drop Date: 02/15/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20163.265
Winform	4.0.20171.241

MvcExplorer
    [230268]Javascript error is displayed after clicked the import button without attached file.

FlexGrid
    [236768]Memory usage increases when navigating between web pages rendering C1FlexGrid.
    [229462]The pasting value is missing in FlexGrid.

FlexViewer
    [2374989]It will show sample tab in the middle when clicking full screen.
    Hide side panel if width of FlexViewer is too narrow with PC template.

Project Templates
    [239141]Unable to create C1 Mvc 3 project on VS2012 via C1 project template.

Olap101
    [237898] "favicon.ico" and "Global.asax" files are displayed like non-exist file in Visual Studio.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.107)		Drop Date: 02/15/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20163.265
Winform	4.0.20171.241

Sample
    [238729]Update MultiRow samples according to the review of Segawa-san.

ExcelBook Sample
    [236256]Styles and Sort dialog-boxes can not be closed by Esc key.

MvcExplorer
    [235205]It will throw exception when editing new row when setting NewRowAtTop in Edit page.

Olap101
    [237415]Error occurs after build the sample in "HowTo" folder.

Scaffolding
    [236342]Request to update icons of FlexRadar.

FlexViewer 
    [238548]Pages in viewer can't be scrolled up by 'Mouse-Wheel' when 'Single Page View' is set.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.106)		Drop Date: 02/13/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20163.259
Winform	4.0.20171.241

Sample
    [238509]Set ShowHeaderCollapseButton to true by default at MultiRowExplorer/CollapsedHeaders.

PdfViewer Sample
    [236207]The issue of sample project: License exception is observed.

FlexViewer
    [236612]Error is observed after clicked on the forward/backward button if continuous page is set.
    [236601]Mobile view of SSRS Report, Page setup icon is able to click even user didn't click on print layout icon.
    [236482]Javascript error is occurred when 'ZoomMode' value is set as 'PageWidth' or 'WholePage'.

Olap
    [238199]Should hide the "Show Detail" in context menu if the data source of PivotEngine is cube.

Input
    [237600]Can't trigger the "OnClientFormatItem" event of the MultiSelect.

Scaffolding
    [238406]The code of VB of some properties are not incorrect.
    [235803]Description of FlexRadar cannot displayed fully words in Japan version of Visual Studio.

PdfViewer
    [236583]Page setup is shown in mobile view and exception throw when page setup is set.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.105)		Drop Date: 02/10/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20163.259
Winform	4.0.20171.241

Sample
    [236434]Error is occurred when any row is selected and deleted in 'Custom Editors' sample page.
    [236054]Pager show incorrect page count and can't navigate to another page in "Filtering" sample page of Core project.
    Use new FlexGridPdfConverter in MvcExplorer(includes ASP.NET Core) FlexGrid/PDFExport page.
    [235531]Setting "stacked 100%" takes no effect in sample.
    [235822]Javascript error is occurred when zero value is selected for TotalAngle.
    [236260]Undo button on toolbar do not work properly.
    [238135]Javascript error occurs after import the excel file.

Olap
    Fix the problem about DimensionType so that it can fit with the client one.

OlapExplorer
    [238085]Need to update the description of OlapExplorer sample property for CubeField and PivotField.

ChartCore
    [238006]Add LabelPadding in ChartAxis.

FlexViewer
    [237425]Clicking export button will throw exception using IE browser.
    [237299]Can not close navigation item automatically by clicking in mobile phone.
    [236551]Errors occurred when FlexViewer is run in Firefox browser.
    [236446]Some inconsistent behavior found in Export setting of PdfViewer.
    [237224]Navigation items are not in same vertical line showing on Mobile view.
    [236619]Error occurs when enter page number which is larger than total page count+1 in navigation text box if continuous page is set.
    [237336]Will cover the navigation item when opening some sub time of Hamburger Menu.
    [237300]Items under Hamburger Menu and View menu are showing mess in mobile phone.
    [237302]Can not go to certain page by inputting page number in mobile phone.
    [236187]Enable defferred script error is observed in Pdf/Report viewer after browse the sample.
    [236484]Search icon in search bar of mobile template is distorted.
    [237303]Can not show content of source tab in mobile phone.

Scaffolding
    [236129]Error is displayed after browse the project if "Rotated" property is set in FlexRadar' Scaffolding.
    [235489]C1 Scaffold template cannot set up value of properties "StartAngle", "TotalAngle", "Reversed" and "Stacking".
    [235533]An incorrect namespace is used by C1 Scaffold.

MultiSelect
    [237527]Should hide the "IsRequired" property of the "MultiSelect".

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.104)		Drop Date: 02/08/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20163.259
Winform	4.0.20163.226

Sample
    [235770][MultiRowExplorer]Does not show out DataMap of city field in Index page of core sample.
    [235823][MultiRowExplorer]Uncaught (500 Internal Server Error) is observed when sorting on the cell template column("Trends" column).
    [237495][MultiRowExplorer]Group header does not show correctly in MultiRow after set the "Amount" in "Group By".
    [236547][MultiRowExplorer]Javascript error is observed when edit in Date column of "Custom Editors" page.
    [235664][MultiRowExplorer]Filter does not work in filtering page of sample.
    [236259]Existing non-exist file "readme-guids.txt" in ReportViewer101/Olap101 of CS project.

Scanffold
    [235675]Should hide the disabled properties and events of MultiRow.

MultiRow
    [235662]The layout won't change after set the "columnlayout" of MultiRow to another layout.
    [235497]Can't add new row in MultiRow after setting "NewRowAtTop" property to "true".
    [235650]The property "WordWrap" of "Cell" in MultiRow can't work.
    [235704]The column can be resized after setting "AllowResizing" property of MultiRow to false.
    [235772]The Visible property of Cells of MultiRow can't work after setting it to false in MVC application.
    [235854]NewRowAtTop shows at bottom in grouping.

Olap
    Refactor the codes according to the new design of wijmo5 olap.
    [237646]Add license for MVC Olap controls.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.103)		Drop Date: 02/07/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20163.259
Winform	4.0.20163.226

Sample
    [236218]Build number of WebApi and C1 DataEngine is not latest build number in "HowTo" folder.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20171.102)		Drop Date: 01/25/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20163.259
Winform	4.0.20163.226

FlexSheet
    Fix the problem caused by supporting JSZip3.0.
    [233698]Update FlexSheet control as wijmo5 updates.

ReportViewer/PdfViewer
    Implement mobile support for ReportViewer/PdfViewer.
    Add ThresholdWidth property to MVC ReportViewer/PdfViewer to support mobile.
    Add ZoomMode property for FlexViewer.
    [192890]Fit to page/width should takes effect after FlexViewer resized.
    [150275]Switch to a next page using scrollbar/mouse wheel.
    [151557]Export options of FlexViewer.

MultiRow
    Make MultiRow mvc control.
    [204346]vsProjectTemplate for MultiRow.

Gauge
    [233694]Update Gauge as wijmo5 updates.

Input
    [233684]Update Input controls as wijmo5 updates.

DetailRow
    [233687]Update FlexGridDetailRow as wijmo5 updates.

FlexGrid
    [233683]Update FlexGrid as wijmo5 updates.
    [222966]InBuilt Validations support alike Wijmo5.
    [222965]Column FooterPanel Aggregation support.

Core
    [209059]Skip the reference loop on serialization. Does not serialize property with IgnoreDataMemberAttribute.
    [222001]Remove the build warnings for MVC controls.
    [234744]Add Scaffolder for new features added by wijmo5.

CollectionView
    [204891]Add requestComplete event.

Samples
    [214690]Issue of sample project: user is unable to open external link by IE and Firefox.
    [218100]The initial value of the settings are not correct in "CS-->MvcExplorer-->Sunburst-->Legend and Titles" sample.
    [120184]Add a sample to HowTo to show how to inherit our mvc control.

Scaffolding
    [208542]Setting 'ChartType' property for 'Waterfall Series' throw exception in scafflod.
    [211492]Extra dropdown is found for Binding X value.
    [214744]Error dialog will displays after add Sunburst chart by scaffolder in JP build.
    [222650]Implement Funnel chart scaffolding.
    [228635]Implement scaffolding for BoxWhisker chart.

FlexChart
    [233699]Update Chart as wijmo5 updates.
    Add Funnel Chart.
    Implement BoxWhisker MVC control.
    Implement MVC ErrorBar.

FlexRadar
    Add Radar & Polar Chart.

Sunburst
    [223747]Sunburst slices are not rendered when the ChildItemsPath contains multiple property names.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.101)		Drop Date: 01/06/2017
=====================================================================================
Dependencies
------------
Wijmo 5.20163.240
Winform	4.0.20163.226

All
    Just upgrade winform build from 225 to 226.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.100)		Drop Date: 12/30/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.240
Winform	4.0.20163.225

Samples
    [228130]ID column cannot display in grid when browser is set with full screen size.

Item Templates
    [214558]It shows nothing when creating by item template with EnableDeferredScripts.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.99)		Drop Date: 12/28/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.240
Winform	4.0.20163.225

Samples
    [229896]CustomReportProvider sample as per the refactor of report Web API.
    [228134]JavaScript runtime errors occur when value less than min value is set in InputNumber.
    [228130]ID column cannot display in grid when browser is set with full screen size.

FlexViewer
    [228265]Print button does not take effect in all browser.
    [230281]Continuous page and single page step up is unable to click in PdfViewer.

Item Templates
    [214558]It shows nothing when creating by item template with EnableDeferredScripts.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.98)		Drop Date: 12/23/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.240
Winform	4.0.20163.224

Samples
    [228134]JavaScript runtime errors occur when value less than min value is set in InputNumber.
    [228002]It throws exception when running GridInForm sample.
    Scrollbar is shown when expanding the tree node of reports in the FlexViewerExplorer sample.

FlexSheet
    [227950]Can not show data out in some tabs of FlexSheetExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.97)		Drop Date: 12/22/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.240
Winform	4.0.20163.224

Samples
    [222936]Apply new layout to core FlexViewerExplorer sample.
    [222956]Apply new layout to FlexViewerExplorer sample.
    [227979]It shows license error when running not core sample.
    [227982]It shows db problem when clicking some tabs in MVCExplorer not core sample of FlexGrid.
    [228074]JS error displays after clicking "Export/Import" in "FlexSheetExplorer-->Excel Service" sample.
    [228071]Can't export the data to pdf file in "MVCExplorer-->PDF-->OverView/RichGraphics" sample.

Project Templates
    [227821]Can't restore the application which is created by ".NET FW4.5"-->C1 ASP.NET Core MVC Application (.NET Framework).

Olap
    [228287]Update the codes for modifying the detail request of DataEngine web api.
    [228300]Roll back to the beta license for MVC Olap controls.
    [228415]Update the olap client side as per the change of dataengine webapi.

Core
    [222001]Remove the build warnings for MVC controls.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.96)		Drop Date: 12/09/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.240
Winform	4.0.20163.219

Samples
    [222954]Apply new layout to FinancialChartExplorer sample.
    [222934]Apply new layout to core FinancialChartExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.95)		Drop Date: 12/08/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.240
Winform	4.0.20163.218

Samples
    [222933]Apply new layout to core MvcExplorer sample.
    [222955]Apply new layout to FlexSheetExplorer sample.
    [222939]Update core MvcExplorer to support vs2017.
    [222937]Apply new layout to core OlapExplorer sample.
    [222935]Apply new layout to core FlexSheetExplorer sample.
    [222959]Update FlexSheetExplorer to support vs2017.
    [222961]Update OlapExplorer to support vs2017.
    [222960]Update FlexViewerExplorer to support vs2017.
    [222958]Update FinacialChartExplorer to support vs2017.
    [226715]Merge MVC5Explorer and MVCExplorer into one sample.
    [222941]Update core FlexSheetExplorer to support vs2017.
    [222940]Update core FinancialChartExplorer to support vs2017.
    [222943]Update core OlapExplorer to support vs2017.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.94)		Drop Date: 12/05/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.226
Winform	4.0.20163.217

Project Templates
    [220954]Can't show the MVC controls in Core application which is create by VS2017 with "Enable Deferred Scripts" checked.
    [222598]Implement MVC project templates for VS2017 JP.

Olap
    [222640]Update mvc olap codes and samples.
    [217414]It shows with very large width in dataset setting in IE browser.
    [222952]Add license support to Olap Mvc controls.

Samples
    [222957]Apply new layout to OlapExplorer sample.

Sunburst
    [223747]Sunburst slices are not rendered when the ChildItemsPath contains multiple property names.

Item Templates
     [222600]Implement FlexViewer item templates for VS2017 JP.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.93)		Drop Date: 11/10/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.226
Winform	4.0.20163.212

Samples
    Add Mvc5Explorer sample.

Project Templates
    Make En ASP.NET Core project templates for VS2017.

Item Templates
    [220678]Does not show two item templates under VS2017 of Core/.netframework template.

Scanffolding
    [220679]Faild to rebuild the project with adding FlexSheet by scaffold of VS2017 in core template.
    [220685]Model Class and Data Content Class did not get correct value after setting model class of VS2017.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.92)		Drop Date: 11/08/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.226
Winform	4.0.20163.212

Samples
    [218991]It throws a JS error when clicking AutoComplete-->CustomAction.
    [218116]The font-color of the items in the ComboBox is the same as the background-color in "CS-->MvcExplorer-->CoboBox-->Item Template" sample.
    [219161]No OlapExplorer sample showing in shortcut of installation.

FlexChart
    [219339]JS error will throw out after setting ChartType from "Column" to "SplineArea" in "MVCExplorer" sample.

Project Templates
    [219453]Need to remove support TS function in template of VS2013.

Item Templates
    [218976]Cannot add PDF and Report Viewer in MVC project and package installation error is displayed in VS2017 RC.

Scaffolding
    [218979]In VS2017 RC, error is displayed after adding scaffold in MVC project.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.91)		Drop Date: 10/31/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.226
Winform	4.0.20163.212

Samples
    [217297]Error is occurred when sheet is exported.
    [217232]The width of the FlexChart is smaller than the before build in "ASPNETCore-->MVCExplorer-->FlexChart-->ItemFormatter" sample.
    [217433]No screenshot in some samples of installation.
    [218100]The initial value of the settings are not correct in "CS-->MvcExplorer-->Sunburst-->Legend and Titles" sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.90)		Drop Date: 10/31/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.226
Winform	4.0.20163.211

FlexViewer
    [207953]User is unable to print document in Firefox.
    [215396]It will throw exception when chaning SSRS report frequency when loading.
    [215332]Pop-up toolbar becomes transparent after Full screen mode is on.
    [214838]Exporting & Printing do not work.

Samples
    [215613]"readme.txt" file is displayed like non-exist file in Visual Studio.
    [215401]Error occurs when VB FlexSheet101 sample is run.
    [215518]Error is observed after browse the sample which is deploy in IIS.
    [215615]readme.txt file can't be found in FlexSheet101.
    [215414]Error occurs when cell content is edited in VB sample.
    [216289]Cannot open controls in mobile phone when click drop-down button in category.
    [215530]Check min/max value of InputNumber.

Olap
    [212464]PivotPanel cannot display filter value.
    [213200]Update the apis for PivotGrid, PivotFlexChart and PivotFlexPie.
    [213122]We should improve the Interactive experience of olap which uses cube as data source.

FlexPie
    [216274]The "Postion" property of "DateLabel" of FlexPie/Sunburst can't work after setting it to "None".

FlexGrid
    [216088]Javascript error is observed when editing in cell using custom editor.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.89)		Drop Date: 10/27/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.225
Winform	4.0.20163.211

ItemTemplate
    Update PdfViewer Itemtemplate with beta string.

License
    [215083]Some inconsistent behavior founds for MVC controls with AH and S9 license keys.

Olap
    [209880]Web browser shows error message when we use property "MaxItems".
    [212483]Property "IsActive" doesn't take effect on PivotEngine.

Samples
    [215348]Show Startup.cs in source tab when browsing OlapExplorer and FlexViewerExplorer samples.
    [215357]Error occur when 'Last page' or 'Continuous Page View' button is clicked.

FlexViewer
    [214846]Viewer and pdf document are not shown properly in printed file when print using browser printing.
    [214863]Select Tool is not working on ipad.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.88)		Drop Date: 10/25/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.225
Winform	4.0.20163.210

Olap
    [214404]Update the new settings to the ShowAs enumeration.
    [214554]Failded to show Olap when using defferscripts.
    [214403]Update the Olap codes to make serviceurl including "api".
    [212872]Remove "c1-property" from "c1-pivot-field" and "c1-pviot-filter" makes error.
    [214710]When the DataEngine api hangs, the PivotEngine control sends the status command without an end.
    [212330]Olap cannot update data in time in IE 11.

Samples
    [214725]Issue of sample project: user is unable to upload a pdf file with long name.
    [214690]Issue of sample project: user is unable to open external link by IE and Firefox.
    [214719]User is unable to visit external web sites.
    [214463]Update the OlapExplorer samples.

Scaffolding
    [214744]Error dialog will displays after add Sunburst chart by scaffolder in JP build.
    [212451]The fields name can't display in the combo-box correctly after click the blank area in the drop-down list in Sunburst chart scaffold.

FlexViewer
    Implement press 'Esc' to exit full screen mode.
    Show full screen toolbar for a short time when change to full screen mode.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.87)		Drop Date: 10/21/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.225
Winform	4.0.20163.200

License
    Implement 2016v3 licenses of PdfViewer, ReportViewer and Olap controls.

Samples
    [214425]License error occurs and sample can't run.
    [214424]Existing non-exist file "readme-guids.txt" in FlexChart101/FlexGrid101 of VB.

ReportViewer
    [214396]It will show error message at first when applying parameter in SSRS Report in FlexSheetExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.86)		Drop Date: 10/20/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.225
Winform	4.0.20163.200

Olap
    [213127]The event "OnClientPropertyChanged" cannot be triggered.
    [212872]Please remove "c1-property" from "c1-pivot-filter" and "c1-pivot-field-collection".
    [213764]Can not drag fields to values.
    [213499]Add some apis for PivotPanel.
    [212512]Console error is displayed after browse the OlapExplorer sample in IIS
    [212478]Filter type and Filter value don't take effect on PivotEngine.
    [214401]Update the OlapExplorer samples.

Samples
    [213687]Make ReportViewer and PdfViewer VB samples.
    [207575]Update MVC Explorer with new layout.

PdfViewer
    [210940]Extra white spaces show when 'Continuous Page View' is on.

FlexViewer
    [211365]UI distorted in certain scenario.
    [213702]Add public readonly property pageIndex for reportviewer/pdfviewer.
    Change all implementation of page navigation from pageNumber(1-based) to pageIndex(1-based).
    [214139]Update comments of ReportViewer/PdfViewer public apis.
    ServiceUrl property changes: should add "api" in the value, for example, "http://demos.componentone.com/aspnet/webapi/api".
    Add FullScreen property and OnClientFullScreenChanged event for FlexViewer MVC control.
    Add SelectMouseMode property and OnClientSelectMouseModeChanged event for FlexViewer MVC control.
    [214462]Browser will crash after input decimal point into "PageNumber" in toolbar of PDFViewer.

ReportViewer
    [213684]The parameter always work as default value after changing the value of parameter.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.85)		Drop Date: 10/16/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.220
Winform	4.0.20163.200

Project Templates
    [210298]Compiling error displays after rebuilding the application which is created by MVC template with "Add TS file" checked.
    [207904]Failed to create MVC5 template with all settings ticked.
    [212563]Sometimes it will pop up project.json used by another process when creating new project using c1mvc template.

ReportViewer
    [212281]Update loading mask layer.
    [209513]The parameter can't display is some case.
    [209532]It will show isRequired warning message in console when clicking Customers near Stores report of SSRS.
    [204497]Focus is lost and need to place cursor again after enter the first value in parameter text box of report.
    [208808]Can't change the parameters in drop-down list with the check box.
    [209265]'500 Internal Server Error' is observed when clicked the 'print layout' button after continuous mode is set in SSRS report.
    [213195]Update export menu after loading the document source.
    [212462]It is better to remove the hyperlink on Sales by Region for it takes no effect.

ItemTemplate
    [210939]In JP environment, "C1.Web.Api.Document" dll cannot add in VB project when create Report using ReportViewer.

Samples
    [211329]Errors occur when CustomReportProvider sample is rebuild.
    [212556]Errors occur and samples can't run properly.
    [208861]Suggest having legend for waterfall and show all data out.

All
    [208515]Setting one control's C1EnableDeferredScripts to false will make other controls DeferredScripts take no effect.

Scaffolding
    [208542]Setting 'ChartType' property for 'Waterfall Series' throw exception in scafflod.
    [211492]Extra dropdown is found for Binding X value.

FlexViewer
    [209191]Exception throw when page setup is setting.

Olap
    [212610]Add ItemsSource/ItemsSourceId properties to PivotPanel.
    [210400]An embedded PivotEngine of PivotPanel doesn't work.
    [212291]The issue of sample project: web browser shows error message when user navigate to SSAS page.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.84)		Drop Date: 09/30/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.214
Winform	4.0.20163.200

Olap
    Correct the file name in vstemplates to fix the error.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.83)		Drop Date: 09/29/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.214
Winform	4.0.20163.200

Olap
    Add the vstemplates.
    Update the OlapExplorer samples.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20163.82)		Drop Date: 09/29/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20163.214
Winform	4.0.20163.200

PdfViewer
    [202526]Implement MVC PdfViewer control.

ReportViewer
    [150274]Implement Move button.
    [151556]First Page, Last Page, Backward History, Forward History.
    [153548]Full Page view:Full-Page View - where page can enter in Full screen mode without toolbar.

Olap
    [207567]Implement MVC PivotChart.
    [204357]Implement MVC PivotGrid.
    [204352]Implement MVC PivotPanel.
    [202565]Implement PivotEngine.

All
    Implement deferred script.

ItemTemplate
    [207303]Implement ItemTemplate support for SSRS reportviewer.
    Implement ItemTemplate support for Pdfviewer.

Sunburst
    [202636]Implement MVC Sunburst chart.

FlexChart
    [203280]Implement Waterfall series.

Scaffolder
    [209267]Shortening names for C1Scaffolder.
    [202638]Add Scaffolder for Sunburst.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.81)		Drop Date: 08/30/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.198
Winform	4.0.20162.188

ReportViewer Item Template
    [205703]It does not been localizated in Extensions and updates tab.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.80)		Drop Date: 08/30/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.198
Winform	4.0.20162.188

FinancialChartExplorer
    [204006]JavaScript runtime error occur if any value that is less than min value or exceed than max is entered to inputNumbers.

ReportViewer
    [204948]Some console error are observed in FlexReportExplorer Sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.79)		Drop Date: 08/29/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.198
Winform	4.0.20162.188

FlexReportExplorer
    [204902]Remove map report under controls folder of FlexReportExplorer sample.

FlexViewer
    [204903]Setting parameter has problem in Cascading Parameters report.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.78)		Drop Date: 08/25/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.198
Winform	4.0.20162.181

MvcExplorer
    [136319]Wrong description in MvcExplorer FlexGrid Custom Footer page.
    [204240]User is unable to change values of existing data.
    [204341]After deploying "AspNetCore-->MVCExplorer" in IIS, can't render "AutoComplete-->Unobtrusive Validation". 

Scaffolding
    [189564]Suggest to add label for Content property of DataLabel.
    [202258]There are some needless codes generated by FlexChart Scaffold.

Samples
    [136462]The FlexGrid should not be editable in the save page of GridInForm sample.
    [129492]"Warning" message displays in the sample.

Project Templates
    [203098]Page layout in IE is not as beautiful as that in Chrome.

FlexSheet
    [202032]FlexSheet is lack of property "AllowAddNew" in .NetCore project.

UnobtrusiveValidation
    [204332]The warning message can't be triggered in the Input controls after losing focus in MVC Core application. 

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.77)		Drop Date: 08/16/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.198
Winform	4.0.20162.181

Input/FlexGrid
    Implement c1mvc unobtrusive validation.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.76)		Drop Date: 08/15/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.198
Winform	4.0.20162.181

Project Templates
    [203098]Page layout in IE is not as beautiful as that in Chrome.

Samples
    [203276]Can not build FlexGrid101 sample of core.
    [203259]The combox on home page showing "object object" content.
    [203282]Update Excel-like sample as per Wijmo 5.

AutoComplete
    [203226]Property "is-required" doesn't take effect on AutoComplete.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.74)		Drop Date: 08/09/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.195
Winform	4.0.20162.180

All
    [201087]Update new Wijmo5 features for flexgrid/flexgridfilter.
    [201024]Update Wijmo5 latest features for FlexSheet/Input controls.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.73)		Drop Date: 07/06/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.188
Winform	4.0.20162.166

MultiSelect
    [198200]The status of checked items are wrong after clicking "Submit" in "MultiSelect>Form" MVCExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.72)		Drop Date: 07/05/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.188
Winform	4.0.20162.166

MvcExplorer
    [198196]There aren't child tree nodes in the "ASPNETCore-->MVCExplorer-->FlexGrid-->PDFExport" sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.71)		Drop Date: 07/04/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.188
Winform	4.0.20162.166

VSTemplate
    [197936]Please adjust the style of Input control.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.70)		Drop Date: 07/01/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.188
Winform	4.0.20162.166

All
    [197744]Implement ASP.NET Core 1.0 support in mvc control.

VSTemplate
    [197767]Compile error displays after creating MVC application by template in VS2012.

MvcExplorer
    Add Validation sample to InputDateTime control.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.69)		Drop Date: 06/29/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.187
Winform	4.0.20162.166

FlexViewer
    [196221]Error "Unable to get property 'appendChild' of undefined or null reference" is observed by clicking 'previous/next' button when searching a word in report.
    [196225]Match selected word is not displayed correctly after changing orientation.
    [197642]Suggest showing the sample name same as other samples.

License
    [197333]Modify the license check code to support License Generator tool.

All
    [197672]Implement c1 mvc client js intellisense support.

VSTemplate
    [197436]Error occurs in project if 'Add TS File' check-box in MVC Application Wizard is checked.

Scaffolding
    [197377]Error occurs if remote load and save are used.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.68)		Drop Date: 06/24/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.185
Winform	4.0.20162.166

Scaffolding
    [195230]Error is occurred if Bound sheet is added by scaffold.

FlexReportExplorer
    Increase the indent of the navigate bar in the sample.

InputDateTime & InputDate & InputTime
    [195745]InputDateTime control throws error message if we don't use the property "Required".

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.67)		Drop Date: 06/23/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.170
Winform	4.0.20162.165

FlexPie
    [189208]The "SelectedIndex" of FlexPie can't work correctly in some case.

MvcExplorer
    [191253]Issue of sample project, data label at the bottom of FlexPie is covered.
    Update InputNumber.Form sample, implement input fields validation.
    Fix the issue that MvcExplorer can't be deployed.

FinanceChart
    [189847]Thumb buttons in range selector are doubled.

FlexChartGroup Sample
    The settings take no effect.

ASPNETCoreInput101
    [195469]Should set the "value-changed" event to "inputDateTime_ValueChanged" in "..\ASPNETCore\HowTo\Input\ASPNETCoreInput101" sample.

Scaffolding
    [190551]Error is observed when set the value less than '2' in period of MovingAverage series.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.66)		Drop Date: 06/22/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.169
Winform	4.0.20162.165

Scaffolding
    [189224]Need confirm removing some ClientEvents.
    [193418]The "Remoteload" can't get the data in some case.
    [195079]Error is occurred if 'Use Remote Bind' in scaffold is checked in .NetCore and .NetFramework application
    [190943]Error is occurred for 'MovingAverageType' if 'Finance' library is added to project.

MultiSelect & Menu
    [151631]Remove the property AutoExpandSelection/HeaderPath from MultiSelect and Menu.

FlexViewer
    [193902]The locallization of FlexViewer control doesn't implement in JP environment.
    [193641]Can not run FlexViewExplorer sample after deployed.

InputNumber
    [191537]The text should display "PlaceHolder" but "0" in browser after setting the "Value" property of the "InputNumber" to null.

FlexReportExplorer
    [196179]Rename FlexViewerExplorer to FlexReportExplorer.

Input
    [195169]The setting for FlexGrid can't work in "MVCExplorer" and "WebApiExploer" sample.

FlexChart
    [195183]FlexChart cannot render correctly in MVCExplorer.

ComboBox
    [194968]The "ItemTemplate" can't work in "MvcExplorer-->ComboBox-->ItemTemplate" sample.

FlexGridFilter
    [194964]Value filter cannot work correctly in MVC4/5 application in scaffold.

InputDateTime
    [189316]User cannot set InputDateTime's value as null via codes.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.65)		Drop Date: 06/21/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.169
Winform	4.0.20162.165

FlexViewer
    [194437]FlexViewer cannot show a certain report.
    [194441]Issue of sample project, user is unable to select parameter options.
    [194475]The minimum value of zoom value is 0% in MVC5 FlexViewer meanwhile is 5% in WinForm.

FlexSheet
    [150700]The border at right side of formula bar is disappeared.

InputTime
    [152881]Change the type of the Step property in InputTime from double to int.

Core
    [153706]Update C1ControlSpec to generate the protected constructors for abstract classes.

FlexViewerExplorer
    [194692]Update the FlexViewExplorer sample for MVC5 and ASPNETCore.

VSTemplate
    [194125]C1 MVC 5 project needs user to update "Newtonsoft.Json" if he wants to run FlexViewer.

Scaffolding
    [193147]There is no drop-down list in the "Style-->FontFamily" property of FlexChart scaffold.
    [193904]Some description of properties are not localize in scaffold dialog box.
    [194766]Description of properties are not localize in scaffold dialog box.
    [194767]Request to describe the description for some properties of FlexSheet in Scaffold dialog box.

MvcExplorer
    [194926]Fix the issue that popup can't work well when open it in MvcExplorer.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.64)		Drop Date: 06/17/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20162.168
Winform	4.0.20162.164

FlexChart
    [193339]The browser will crash after running FlexChart which contains "TrendLine" with AxisX/Y setting in ASPNETCore application.
    [190821]Error is occurred if 'Use Remote Bind' in scaffold is checked.

MVCExplorer
    [193856]Compiling error displays in the "ASPNETCore>MVCExplorer>FlexChart".

FlexViewer
    [192873]Can not load FlexReport correctly when setting some code, such as ViewMode.Continous.

Scaffolding
    [193297]Compiling error displays in the generated viewer after adding viewer by FlexChart scaffold in some case in ASPNETCore application.
    [193302]Compiling error displays in the generated viewer after adding viewer by FlexChart scaffold with "Rectangle" annotation setting in ASPNETCore application.
    [192858]The "ChartType" property should be removed from the "Trend Line"/"Moving Average"/"YFunction"/"Parametric Function" series.
    [194306]Error message is displayed after delete the data of flexgrid in .NetCore and .NetFramework application.
    [193901]Default value for Legend position does not work.
    [153993]Should generate a default series in viewer after adding FlexChart by scaffold.
    [189355]Set 'true' in 'All filtering' property and adding the filter type in more than one column and  in VB application, error will display in browser.

InputDateTime
    [194323]Update Scaffolder, VStemplate for InputDateTime.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.63)		Drop Date: 06/16/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.156

FlexViewer
    [193073]It will fire OnClientPageNumberChanged twice when clicking page navigation.
    [193145]Appearance has problem when loading FlexReport by ItemTemplate.
    [193342]Select Parameter option from report will throw exception.

FlexViewerExplorer
    [193409]Need setting C1.Web.MVC and C1.Web.Mvc.FlexViewer to CopyLocal in sample.
    [193641]Can not run FlexViewExplorer sample after deployed.
    Update report samples database to localdb.

ReportViewer ItemTemplate
    [193367]No end symbo of meta of item template.
    [193301]License information is wrong after creating VB itemtemplate.

VSProjectTemplates
    Missing flexviewer resources in the generated asp.net core application via vs template.
    [192862]The resource name from template does not create correctly.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.62)		Drop Date: 06/13/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.156

FlexViewer
    [192864]Label on Page Thumbnails and Parameters panel shows inproperly.
    [193060]Changing report quickly will throw exception in console.

FlexViewerExplorer
    [192892]Dll of win form is still 139 in project file.
    [192873]Can not load FlexReport correctly when setting some code, such as ViewMode.Continous.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.61)		Drop Date: 06/12/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.156

ReportViewerItemTemplate
    Rename C1.Web.Api to C1.AspNetCore.Api.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20162.60)		Drop Date: 06/12/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.156

Scaffold
    Implement FlexChart/FlexPie scaffolder.
    Implement FlexSheet scaffolder.
    Implement VB support to FlexGrid scaffolder.
    Implement VB support to input scaffolder.

FlexChart
    [153718]Implement "PlotArea" support in FlexChart.
    [149444]Support Gestures as an extension like wijmo5.
    [149442]Support Chart.Animation as an extension.
    Implement parameterless Addxxx() method support for Annotation objects.
    Implement parameterless ShowAnimation() method support for Animation.

FlexPie
    [153056]Correct the type of FlexPie's SelectedIndex property.

FlexGrid
    [149337]Support two new properties ImeEnabled and DropDownCssClass.

FlexSheet
    [149437]Support Formula Bar as an extension of FlexSheet.
    [149440]Add the OnClientVisibleChanged event in the Sheet class and the OnClientSheetVisibleChanged event in FlexSheet class.

ReportViewer
    [150271]Zoom can be set to any value.
    [150272]Whole page and page width.
    Implement FlexViewer MVC control.

InputDateTime
    [150260]Add InputDateTime control as per Wijmo5.

DropDown
    [150261]Alike Wijmo5 add corresponding server side AutoExpandSelection property to DropDown controls.
    [150264]Add DropDownCssClass property to DropDown.
    [150265]Add the samples for DropDownCssClass to MVCExplorer for ComboBox, MultiSelect, ListBox.

ComboBox:
    [150262]Alike Wijmo5 add HeaderPath property on server side to ComboBox control.

Samples
    [191538]Issue of sample project, input control is covered by the layout of webpage.
    [141394]Remove useless _Pager partialviewer from samples.
    [150263]For FlexGrid add another section in DataMap sample for demonstrating multicolumn DataMap.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.59)		Drop Date: 06/02/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.138

Scaffolder
    [191443]Compile error displays in the generated "InputController.cs" after add Wijmo5 input scaffold.
    [191465]Some properties do not generate correctly using asp.net core template.

Samples
    [191275]Issue of sample project, incorrect namespace in webpage.
    [191261]Issue of sample project, the html element "input" is covered by other controls.
    [191212]Error displays after rendering "BookMyFlight" sample which computer without "SqlServerExpress" installing.
    [191226]The layout in browser is wrong after rendering the "ASPNETCor-->C1Finance" sample in GoogleChrome.
    [191258]Does not exist .Net Framework 4.5.1 under ASPNETCore\HowTo\C1Finance\C1Finance sample.

VSTemplate
    [191367]Mvc project needs user to download npm components from internet.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.58)		Drop Date: 06/02/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.138

Scaffolder
    Update C1.Scaffolder for AspNetCore.

All controls
    [145268]C1.Web.Mvc.Serializition namespace is wrong.
    Rename C1.Web.Mvc to C1.AspNetCore.Mvc.

Samples
    [153685]The C1.C1Excel dll version is 139 in sample.
    [153681]It exists non-exist file readme-guide.txt information in project.
    Rename C1.Web.Mvc to C1.AspNetCore.Mvc.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.57)		Drop Date: 04/29/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.138

Samples
    [152382]Error displays after exporting FlexChart to image file in C1MVCFlexChartExport sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.56)		Drop Date: 04/21/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.138

VSTemplate
    [152015]Incorrect comments information in d.ts file and Client doc.

All controls
    [145268]C1.Web.Mvc.Serializition namespace is wrong.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.55)		Drop Date: 04/20/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.138

VSTemplate
    An exception is thrown when uncheck "Enable Client IntelliSense" and create project.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.54)		Drop Date: 04/19/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

Samples
    [147995]Open MVC_ASPNET5Gauge101 sample will pop up link to TFS window.
    [147639]Rebuild will make c1.c1zip.4 dll in bin folder lost.
    [144527]The dialog will flicker after rendering popup sample in Google Chrome.
    [141394]Remove useless _Pager partialviewer from samples.

VSTemplate
    [147488]Integer variable's value is zero which project is created by C1 MVC template.
    [148872]Copyrights is still "2015" of creating project.
    Add TS intellisense support in MVC controls.

HowTo Sample
    [144303]In SignalR sample, changing some columns value to some fomat, in another sheet becomes "0".
    [139786]Inconsistent behaviour is observed  in "StockChart" sample in some case.
    [111675]'Country' column is read-only which is inconsistent with the sample description.

MvcExplorer
    [112371]'And' and 'Or' radio buttons are not displayed properly in 'Disable Server Reading' sample.MvcExplorer
    [112550]Sample description is inconsistent with the actual grid in 'Disable Server Reading' sample.
    [139785]RadialGauge value range is not shown correctly at Scaling in MvcExplorer sample.
    [148290]Download links on demos.

FlexViewer Sample
    [147805]Error occurred after choosing Report File or Report Name from dropdown when flexviewer sample is deployed in IIS.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.53)		Drop Date: 03/04/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

FlexGridFilter
    [147734]Condition Filter does not work correctly when using AND/OR operator in ID column.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.52)		Drop Date: 03/03/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

HowTo Samples
    [145503]Should set the "Legend" property of FlexChart to "Chart.Position.None" in the "ASPNetMVCFlexChart101" sample.

FinancialChart
    [147176]The types of MinX and MaxX in the Fibonacci<T> class are different from wijmo5.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.51)		Drop Date: 03/02/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

MvcExplorer
    [145271]Line gauges are covered.
    [146638]Web browser shows error message for Scaling page in Gauge sample.

Scaffold
    [140965]If set the property as the default value in "GroupPanel" in grouping tab, the property can't be added to the viewer.
    [140954]Setting format of InputMask type is failed of Input.

FinancialChartExplorer
    [146025]Some inconsistent behaviors are occurred in 'Fibonacci Tool' of FinancialChart's Analytics sample.

VSTemplate
    [146645]Add jszip cdn to VSTemplate for FlexSheet.

FlexViewer Sample
    [144673]Checkbox is disappeared in IE 11 and Firefox.
    [146960]The "FlexViewer" sample can't render in browser after installing the "C1StudiosInstallerv20161.1.1.exe".

HowTo Samples
    [146132]'ColumnPicker' icon is lost after deploying the ColumnPicker sample in IIS.
    [146010]Description and code is not same in filtering of ASPNetMVCFlexGrid101 sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.50)		Drop Date: 02/23/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

Gauge
    [140100]Gauge value shows as '100' although its value is set as '0' in all gauges types.

Scaffold
    [145885]Error is occurred when edit the data in C1Scaffold's Input control in MVC4/MVC5 application.

MvcExplorer
    [145797]Custom Editors does not work correctly in Date column.

FinanceAnalytics
    [144568]Style properties does not work in stochastic and MACD series.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.49)		Drop Date: 02/22/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

MvcExplorer
    [138994]Selected items of Stacking and ChartTypes are not working correctly at first time in FlexChart's Selection.
    [145271]Line gauges are covered.
    [140444]The title of PDF Export sample in MVCExploer is different from the WIjmo5 sample.
    [145311]Web browser shows error message when import an excel file.

Scaffold
    [141101]Need adding colon (:) at end of each label.

FlexSheet
    Change the defualt value of "DisableServerRead" to true in BountSheet CollectionView.

HowTo Sample
    [146010]Description and code is not same in filtering of ASPNetMVCFlexGrid101 sample.
    [145927]'C1Icon' is lost after deploying the FlexChartGroup sample in IIS.
    [145990]Filter value cannot be displayed correctly in value filter of Discount column.
    [145855]JS error displays in "HowTo\FlexGrid\C1MVCExcelImportExport" sample.
    [146183]Error is occurred after deploying the sample.
    [144363]The cells don't synchronize data in two browser after editing one cell in the same time.
    [140404]Error is occurred with certain scenario.
    [146140]Error is occurred when click the 'Save Column Layout To Server' button after deploying the sample in IIS.

AutoComplete
    [137992]Dropdown button of the AutoComplete is no longer visible when the value which doesn't exist in the drop-down list is entered although isEditable is 'False'.

FlexViewer Sample
    [145510]"[*] is invalid value for parameter" displays after clicking "Apply" in FlexViewer sample after rendering "CascadingParameters" report.
    [143107]Document Map cannot adjust to sorting in Firefox.
    [145952]The flexviewer sample can't render after deploying in IIS.
    [143393]Page Thumbnails cannot keep status of selected page in a certain case.

FinancialChartExplorer
    [144370]Javascript error occur when set 1 in smoothing period of MACD type.
    [146025]Some inconsistent behaviors are occurred in 'Fibonacci Tool' of FinancialChart's Analytics sample.

FlexSheetExplorer
    [145758]Some inconsistent behaviors are occurred in 'Remote Load/Save' sample.
    [145821]'Freeze' button does not refresh when a new sheet is added.

FlexGrid
    [146344]Add jszip.js to FlexGrid resource.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.48)		Drop Date: 02/17/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

FinancialChartExplorer
    [144222]DateValue in ToolTip shows incorrect format.
    [144236]Fix the issue with FinancialChartExplorer sample.
    [144370]Javascript error occur when set 1 in smoothing period of MACD type.

HowTo Samples
    [138864]Unlike the previous build, webpage's message alert box is displayed after save the column layout of flexgrid.
    [139218]Unlike previous build, Error 'System.IndexOutofRangeExcepiton xxx' is observed after double clicking on data series in 'FlexChartGroup' sample.
    [145503]Should set the "Legend" property of flexchart to "Chart.Position.None" in the "ASPNetMVCFlexChart101" sample.
    [144380]It will throw exception when exporting to image of C1MVCFlexChartExport sample in IE.
    [144391]Exported image with black background in chrome.
    [144397]Can not export image in FireFox of C1MVCFlexChartExport sample.
    [139401]Console Errors are occurred after deploying the FlexChart Analytics sample in IIS.

FlexViewer Sample
    [145702]The report can't render after navigating to "Data Calculations-->GroupPageCounts" in "FlexViewer" sample.
    [145717]The report can't render after navigating to "StoredProcedure" or "Sorting" FlexReport in "FlexViewer" sample.

FlexSheet
    [145275]Fix the issue with FlexSheetExplorer asp.net core sample.

Menu
    [138932]The "isDroppedDownChanged" event does not fire when 'Context Menu' is opened.

ComboBox
    [144705]Unable to set SelectedValue and SelectedItem properties from Model.

FlexGridFilter
    [144948]Filtering is not working correctly at FlexGrid's Filter sample of MVCExplorer.

Scaffold
    [145621]Loading data is very slowly if setting nothing.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.46)		Drop Date: 02/16/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

FlexViewer Sample
    [143072]If a parameter value is number, suggest to remain two numbers of decimal.

FinancialChart
    [144293]JS error will display after rendering FibonacciArcs/FibonacciFans/FibonacciTimeZones.

HowTo Samples
    [139648]Error is occurred when 'C1MVCFinance' sample is run.
    [139234]Unlike the Wijmo5, Javascript error is observed after filter with decimal value in max price.
    [139755]Images are not displayed after deploying the FlexChartEditableAnnotationLayer sample in IIS.
    [140409]404 error of sample project.
    [144359]The format can't synchronize after setting format to "Decimal" in "SignalR" sample.
    [144547]Some issue of "SignalR" sample.

MvcExplorer
    [138988]Unlike Wijmo 5 sample, popup dialog close immediately without showing any message  when clicking "Facebook" or "Google".

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.45)		Drop Date: 02/14/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.122

Scaffold
    [145237]Fix the issue that width/height can't take effect via scaffolder.

FlexViewer Sample
    [145241]Add select all function for MultiSelect in FlexViewer parameter panel.

FlexSheetExplorer
    [144194]Sheet cann't be saved properly in 'Remote Load/Save' sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.44)		Drop Date: 02/06/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.117

FlexSheetExplorer
    Fix the issue that js source tab is disappeared when deploy this sample to IIS.

FlexSheet
    [141479]ScrollIntoView cannot impact on FlexSheet on C1 MVC.
    [141409]Should conform to the standard of properties description.

HowTo Samples
    [142942]Clicking document map does not navigate to correct report position when changing zoom value.
    [142977]Can not show FlexViewer and generate report correctly in document mode "9" in IE.
    [143073]Unselect some items of parameter, it will only unselect one item after opening again.
    [143223]There's no page number in Page Thumbnails on Firefox.
    [142800]Will not bring selected text into view in certain case.
    [144513]Clicking Next Page button will scroll many pages in certain case.
    [144622]PageThumbnails shows more one page as total page count in certain report.
    [144612]Improve the side panel.

FinancialChartExplorer
    [144236]Fix the issue with FinancialChartExplorer sample.

Scaffold
    [142188]'Sort Row Index' value can not remove properly in Sorting.
    [143132]Showgroups property does not work correctly in flexgrid which is defined in C1Scaffold designer.
    Update ja resources for localization.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20161.43)		Drop Date: 01/29/2016
=====================================================================================
Dependencies
------------
Wijmo 5.20161.116

FinanceChart
    [142415]Add new FinancialChart overlays and indicators.

FlexGridFilter
    [138442]Update mvc flexgridfilter according to the new features of wijmo5.

Input
    [139511] Update Mvc.Input CheckedIndexes/CheckedValues properties.

Chart
    [138444]Update Mvc.Chart/Chart.interaction features(as wijmo 5).

Gauge
    [138443]Update Mvc.Gauge features(as wijmo 5).

FlexGrid
    [138438]Update Mvc.Grid features(as wijmo 5).

Pager
    Implement Pager control.

FlexSheet
    Implement a FlexSheet control.

Scaffolder
    Implement scaffolder for flexgrid/input controls.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.42)		Drop Date: 12/29/2015
=====================================================================================
Dependencies
------------
Wijmo 5.20153.102

MvcExplorer
    [140403] Error is occurred when 'Paging' sample page is navigated.

FinancialChartExplorer
    [136401] Error <** Assertion failed in Wijmo: Integer expected> occurs after changing Reversal Amount is set '7' and Range Mode is set 'ATR' in Financial Chart-Kagi.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.41)		Drop Date: 12/17/2015
=====================================================================================
Dependencies
------------
Wijmo 5.20153.102

MvcExplorer
    [139733]Add control dropdown list to the MVCExplorer sample just like the attached screenshot shown.

Input
    [139519] Add some InputXXXFor methods to support Nullable values.

Samples
    Add the below samples:
        FlexGridCustomMerging, add Client Excel load/save and Pdf export pages to Explorer sample.
        FlexChart: add Scaling/Panning and Saling/PlotAreas pages to Explorer sample, EditableAnnotation, MultiSelect, AxisScrollBar.
        Gauge: add Scaling page to Explorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.40)		Drop Date: 12/2/2015
=====================================================================================
Dependencies
------------
Wijmo 5.20153.102

ASPNET5 Samples
    [139225]The "FinancialChartExplorer" sample can't render in browser after deploying the sample in IIS.
    [139233]The sample in "HowTo" folder can't render in browser in JP build.

MvcExplorer
    [138865]Unlike the previous build, Error "System.FormatException:xxxx" occurs when navigating to FlexChart->Annotation sample page.

Core
    Upgrade Mvc 6 to AspNet 5 RC1.

HeaderFilter
    Add the definition of model type in view page.

FlexGrid
    [132287]Exception occurs when perform conditional filtering with empty filter value.

HowTo Samples
    [136341]C1MVCExcelImportExport doesn't work.
    [136453]Use wijmo.httpRequest instead of c1.mvc.Utils.ajaxPost in ColumnLayout sample.

ASPNETMVCFinancialChart101
    [136174]Changing the chart type of trendlines doesn't work.

MvcExplorer
    [137669]Update the annotation sample of mvc flexchart.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.39)		Drop Date: 10/31/2015
=====================================================================================
Dependencies
------------
Wijmo 5.20153.102

    Just use wijmo5 102 drop.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.38)		Drop Date: 10/30/2015
=====================================================================================
Dependencies
------------
Wijmo 5.20153.101

ASPNET5Input101
    [135736]Mask setting '99/99/9999' is not effected in InputDate control as expected when deleting the date value.

MvcExplorer
    [112547]Dropdown lists item are not shown correctly after selecting combobox item in ComboBox / ItemTemplate sample.

MvcExplorer & TagHelperExplorer
    Fix the issue that FlexGrid "CustomEditor" sample throws js error in IE.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.37)		Drop Date: 10/28/2015
=====================================================================================
Dependencies
------------
Wijmo 5.20153.94

Core
    [135178]Refactor client side extender.
    [134735]Multiple issue are observed when run the MVCExplorer sample with trial version.

MVCExplore
    [135202]InputTime's value property can't work properly in "Custom Time" sample.
    [129766]Unlike sample in Wijmo 5 , some inconsistent behvaiors are found.
    [134868]Input Date Custom Editors is displayed  although it is exit from edit mode when no changes in custom editor.
    [129474]"SelectionMode" of FelxGrid in initialized time is "CellRange" will the setting option is "None" in "ASPNET5FlexGrid101" sample.
    [135697]Unlike previous build, Popup dialog close immediately after clicking another popup control.

InputNumber
    [135065]Fix the issue that the separator between textbox and dropdown button disappeared in Input control.

FlexGrid
    [128624]Loading event and loaded event does not fire in page load of Hierarchical grid.
    [128431]Unlike FlexGrid control in Wijmo5, 'tab' key action does not work properly in child grid and javascript error is occurred.

FinancialChart
    [134426]Change the namesapce of ChartType, DataFields and RangeMode.

HowTo Samples
    [135491]Javascript error is observe after setting period value and changing the range to 1m in Stock Chart.
    [135493]Javascript error is observe after checking "News" checkbox in StockChart sample.
    [136017]The unselected row is deleted after adding the new data with same value in "Name" column.

Template
    [128024]The rendering result of the MultiSelect with template is incorrect after binding data base which filed contains the backslash.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.36)		Drop Date: 10/22/2015
=====================================================================================
Dependencies
------------
Wijmo 5.20153.94

FinancialChart
    [134426]Rename all namespaces of financialChart.
    [133455]Styles are not effected correctly on line break chart.

HowTo Samples
    [134023]Update StockChart sample according to the changes of wijmo5.
    [131132]Series 'Point' details are not shown when choosing selection mode is 'Point' and clicking on data series.

ChartAnnotation
    Fix the issue cannot get the AnnotationLayer in client chart extenders.

FlexChart & FinancialChart
    [135167]Series.ChartType takes no effect when Series.ChartType is set to Column and FlexChart.ChartType is not Column.
    Fix the issue that FlexChart Palette property can't work well.

FinancialChartExplorer
    [134894]FitType of Trendline that show in chart is mismatched with the value in 'Fit Type' dropdown.

FinancialChartTagHelperExplorer
    [134894]FitType of Trendline that show in chart is mismatched with the value in 'Fit Type' dropdown.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.35)		Drop Date: 10/19/2015
=====================================================================================
Dependencies
------------
Wijmo 5.20153.92

CollectionView
    [134852]Fix the issue that querydata event doesn't fire when do CUD(Create/Update/Delete/Edit) operation with DisableServerRead = true

VSTemplate
    [133980]Update mvc vs template to add mvc finance support

FinancialChartExplorer
    [131803]Error <True Range period must be less than the length of the data> occurs after changing Box Size is set '64' and Range Mode is set 'ATR' in Financial Chart-Renko.
    [131860]AxisY need to adjust according to max/min value according to it's selected range.
    Add license.
    [134172]Adjust sample data source.

FinancialChartTagHelperExplorer
    [134172]Adjust sample data source.
    [134222]Remove "Script" section in project.json.

ASPNETMVCFinancialChart101
    [133401]Js error occurs when setting trendline period value to >78.

Control
    [133703]GotFocus and LostFocus events do not work in charts.

ChartAnnotation
    [131787] The "offset" property of annotation can't work if the value contains 0 in "PointF".
    [132038] Unlike wijmo5 FlexChart Annotation, Circle annotation is displayed although "radius" property is set "0".
    [131966] Unlike wijmo5 FlexChart Annotation, Eclipse, Rectangle and Image annotation is displayed when "Width" and "Height" property of annotation is set "0".

Core
    [131086] Re-fix: Request to hide 'html-encoder' property in Wijmo 5 TagHelper controls.

MultiSelect
    [134377] Fix the issue that "CheckedValues" and "CheckedIndexes" doesn't work in MultiSelect.
    [127969] The "MaxHeaderItems(0)" property of MultiSelect works as default value.

HowTo Samples
    [129463] Error thrown out in "StockChart" sample in some case.

AutoComplete & Menu
    [132202] The 'isContentHtml' property does not work at initial state.

MVCExplorer
    [118522] Default document mode of  IE browser is changed to IE7.
    Add collectionview101 sample link to GetStarted section in Home page.

TagHelperExplorer
    Add FlexPie101/Gauge101/Input101 sample links to GetStarted section in Home Page.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20153.34)		Drop Date: 10/10/2015
=====================================================================================
FlexGrid
    [133796] "undefined" is shown in the cell of a new row when celltemplate is applied to mvc flexgrid.

TagHelper Base
    [131086] Request to hide 'html-encoder' property in Wijmo 5 TagHelper controls.

FinancialChart
    Implements Financial Chart.

ChartAnnotation
    Implements FlexChart/Financial Chart Annotation.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.33)		Drop Date: 09/23/2015
=====================================================================================
FlexGrid
    [130718] 'cellEditEnding' event doesn't get fired when custom editors are used.

Menu
    Add Owner property to Menu control in order to support contextmenu.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.32)		Drop Date: 09/16/2015
=====================================================================================
All
    Add asp.net5 beta7 support to mvc controls.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.31)		Drop Date: 09/07/2015
=====================================================================================
HowTo Samples
    [129394]The piechart shouldn't navigate to the children piechart after clicking the area outside of parent piechart.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.30)		Drop Date: 09/01/2015
=====================================================================================
TagHelper
    Fix the issue that resources files for localization aren't embedded into c1.web.mvc package.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.29)		Drop Date: 08/31/2015
=====================================================================================
MultiSelect
    Add MultiSelect control.

Popup
    Add Popup control.

FlexGrid
    Add detail rows support to flexgrid control.

HowTo Samples
    Update the flexchart 101 samples.
    [123793] JS error will throw out after running "HowTo-->BookMyFlight" sample.
    [123809] All menus are not dispayed selected text value when selecting item in "ASPNetMVCGauge101" sample.
    [123806] JS error will throw out after some operations in  "HowTo-->StockChart" sample.

GridFilter
    [123753] Filter does not work properly when using value filter after searching is applied.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.28)		Drop Date: 07/07/2015
=====================================================================================
HowTo Samples
    Add asp.net5 flexgrid/flexchart 101 samples.
    Add download link to all 101 samples.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.27)		Drop Date: 07/06/2015
=====================================================================================
VS Template
    [122941]After creating MVC6 template with checking "Reference Document Libraries", the references restores unsuccessfully.
    [123328]Update JP resource for the MVC3/4/5 vs template extension

InputNumber
    [118441]bug-[MVCExplorer_InputTime_Mobile]The dropdown list become to textbox in "Input--->InputTime".

MVC Explorer
    [123097]bug-[MVC-Sample-Data] Sample data is not correct.
    [123085]bug-[MVC-Sample] Nothing shows in FlexGrid if switch source and sample tabs after scrolling operation in FlexGrid.

FlexGrid
    [122689]bug-[WebAPI-ImportExcel-MVCEditing] In editable FlexGrid of MVC, imported boolean value cannot be edited.

HowTo Samples
    [122553][Wijmo5 MVC][StockChart]Unlike previous build,newly added series disappear after scrolling range slider and choosing chart range.

FlexChart
    [122521][Wijmo5 MVC][C1MVCFinance]Unlike previous build,unexpected behaviors are found in chart.

TagHelperExplorer
    [123087]bug-[MVCExplorer-TagHelpExporer-Sample]The description of sample page is incorrect.
    [123095]bug-[MVC6-TagHelperExplorer-FlexChart]The label in axes of "TagHelperExplorer-->FlexChart--RemoteData Bind" is different from in the sample "MVCExplorer".

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.26)		Drop Date: 07/01/2015
=====================================================================================
Chart
    [119598]bug-[MVC-FlexPie] Threshold of Tooltip cannot work in the FlexPie.

TagHelperExplorer
    [122883] Make TagHelperExplorer support IIS deploy
    [122043][MVC6][TagHelperExplorer][FlexChart] 'Interpolate Nulls' and 'Legned Toggle' options do no work at initial state
    [122032][MVC6][TagHelperExplorer][FlexChart]Unlike other browsers,zoom does not work in Chrome
    [121535][MVC6][TagHelperExplorer][MVCExplorer][RadialGauge] RadialGauge is extremely large if 'Sweep Angle' option is set as '180' and value of 'Start Angle' option is changed

HowTo Samples
    [117886][Wijmo5 MVC][C1MVCExcelImportExport][IE11] Multiple issues are observed in 'C1MVCExcelImportExport' sample.

Core
    [121895][TagHelperExplorer][FlexGrid] Unlike previous build, JavaScript error occurs when navigate to 'Custom Footers' page.

MVC Explorer
    [121970][Wijmo 5 MVCExplorer][TagHelperExplorer][FlexChart] Javascript error is observed when hovering legend icon  in "Hint test" sample of FlexChart

FlexGrid
    [121896][TagHelperExplorer][FlexGrid] JavaScript error occurs when auto-sizing 'Trends' column in 'Custom Cells' sample

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.25)		Drop Date: 06/29/2015
=====================================================================================
Core
    [122144]Update the license exception messages for Jp version in ASP.NET 5.

VS Template
    [120285]Update the description of VS template for Jp version in ASP.NET 5.
    [116749]bug-[Wijmo5_MVC_VSTemplate] Web port of projects is same if creating multiple projects by the same MVC VS template.

FlexGrid
    [119069]bug-[MVC-FlexGrid] JS error shows when one FlexGrid is regarded  as Cell Template of another FlexGrid.
    [121977]bug-[MVC-FlexGrid]The data disappears after clicking "Apply" by "Amount"/"Amount2" in MVCExplorer sample.

Menu
    [120873]inquiry-[MVC6-TagHelper-Input] Should hide some attributes in Menu.

HowTo Samples
    [116568][Wijmo5 MVC][ASPNetMVCFlexGrid101][IE10] JavaScript error occurs and C1FlexGrids do not display properly
    [121220]Improve the stock chart sample.

Chart
    Add Trendline to FlexChart.
    [122019]bug-[MVC-FlexChart]The format of label in axes of FlexChart is different from the previous MVCExplorer sample.
    [122023]bug-[MVC-FlexChart]JS error will throw out after running the "HowTo-->FlexChart-->StockChart".

TagHelperExplorer
    [121887][MVC6][TagHelperExplorer] LicenseException 'Invalid certificate' is occurred when 'TagHelperExplorer' sample is run

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.23)		Drop Date: 06/24/2015
=====================================================================================
Gauge
    [119171][MVC-LinearGauge/RadialGauge] Error will thrown out if trigger the OnClientPropertyChanged event in Ranges in LinearGauge/RadialGauge.

VS Template
    [120248]bug-[MVC6-TagHelper-Template]Error message displays in the error list after creating a new project by MVC6 template.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.22)		Drop Date: 06/23/2015
=====================================================================================
ComboBox
    [119388]bug-[MVC6-TagHelper-Input]Server error will throw out after setting "select-value"/"select-item" of some controls.

InputNumber
    [119411]bug-[MVC6-TagHelper-InputNumber]The "text" of inputnumber can't work.

FlexGrid
    [119336]inquiry-[MVC6-TagHelper-FlexGrid] whether visible attribute's name is proper?
    [118990]bug-[MVC-FlexGrid] The delegated "Template"/"EditTemplate" method shows in intellisense.
    [118112][MVC-FlexGrid] Filter result is not correct after perform "ESC"operation in the filter editor.

MVC Explorer
    [121130]Add a border to title of CE.

TagHelperExploper
    [119926]Use TagHelper instead of HtmlHelper for theme editor

HowTo Samples
    Update the "C1MVCFinance" sample according to wijmo5.

License
    [118333] Implement licensing in MVC6
    
Core
    [116240]Modify the build cmd in order not to need change it when adding new wijmo5 culture
    [120119][Wijmo5 MVC][Japan Issue] Can't create control in MVC area
    [118236][MVC-FlexGrid]JS error will throw out after running some FlexGrid of MVCExporer sample in IE9.

Gauge
    [119171][MVC-LinearGauge/RadialGauge] Error will thrown out if trigger the OnClientPropertyChanged event in Ranges in LinearGauge/RadialGauge.

VS Template
    [117133][Wijmo5_MVC_VSTemplate] Some content on Account pages should be localized.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20152.20)		Drop Date: 06/10/2015
=====================================================================================
    Add MVC6 compatibility support to MVC controls.
    Add TagHelpers support to MVC controls.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.19)		Drop Date: 05/15/2015
=====================================================================================
    [115173]bug-[MVC_FlexChartSample] In the "Financial Chart" sample, data disappears after zoom operation/ChartType settings.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.18)		Drop Date: 05/14/2015
=====================================================================================
FlexGrid
    [117497][Wijmo5 MVC][ASPNetMVCFlexGrid101][FlexGrid] Checkbox cell need to check twice when checking the checkbox column for second time in FlexGrid.

MVC Explorer
    [118116][Wijmo5 MVCExplorer][FlexGrid] "Error: Invalid character" Javascript error is observe after opening the drop down menu without selecting the item and scrolling the FlexGrid.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.17)		Drop Date: 05/13/2015
=====================================================================================
MVC Explorer
    [117565][Wijmo5 MVCExplorer][FlexGrid] Javascirpt error is observe after browsing Editing sample in MVCExplorer.
    [117568][Wijmo5][MvcExplorer][ListBox] Unlike previous build, "JavaScript runtime error: 'selectedIndexChanged' is undefined" is observed in ListBox sample.
    [112598]Clicking "OK" in "Editing" sample can't work after some operations.

FlexGrid
    [117575][MVC-FlexGrid_Sample]JS error will throw out after editing the cell from the first row to the last in "Custom Editors".
    [117402][Wijmo5 MVC][FlexGrid]Javascirpt script error is observe when scrolling the web page after opening the custom drop down and selecting an item.
    [117497][Wijmo5 MVC][ASPNetMVCFlexGrid101][FlexGrid] Checkbox cell need to check twice when checking the checkbox column for second time in FlexGrid.

HowTo Samples
    [117578][Wijmo5 MVC][C1MVCFlexChartExport] License exception occurs since 'licenses.licx' file does not include in 'FlexChartExport' sample.
    [117676][Wijmo5 MVC][HeaderFilters][FlexGrid]Unlike previous build 15, Javascript error is observe after browsing HeaderFilters sample

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.16)		Drop Date: 05/10/2015
=====================================================================================
FlexGrid
    [116726][MVC-FlexGrid] Filter editor cannot be closed if perform the filtering operation on column with cell template in IE.
    [116616][MVC]FlexGrid not updating when returning a partial view from the controller method called through an AJAX call.
    [116585][MVC-FlexGrid] After sorting operation, data cannot be shown if drag virtual scrolling's thumb button to bottom.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.15)		Drop Date: 05/06/2015
=====================================================================================
FlexGrid
    [111587][Wijmo5 MVC][Asp.Net Wijmo]No license error is observe and the controls are working fine when hosting and browsing via IIS after trial license is expired
    [116578][MVC-FlexGrid]JS error will throw out after filtering grid with "CellTemplate"and "FrozenRows" setting.
    [116716]bug-[MVC-FlexGrid] In the FlexGid's filtering sample, same filter value shows in the value filter editor of "Discount"/"End" column.

FlexChart
	Fix an issue that JS error will occur when create an empty FlexChart.

MVC Explorer
    [110156][Wijmo5 MvcExplorer][FlexGrid] 'System.FormatException' occurs when generating new items count while menu dropdown is being opened on the sample page

InputDate
    [116459][MVC-InputDate] Border of DropDown-Calendar in InputDate is incorrect.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.14)		Drop Date: 04/30/2015
=====================================================================================
FlexGrid
    [116426]bug-[MVC-FlexGrid]The data in filter dialog can't display correctly in some condition.
    [116544][Wijmo5 MVC][FlexGrid] Unlike Wijmo5 FlexGrid, the collapse/expand icons is not display when grouping the column in Group Panel sample.
    [116542]bug-[MVC-FlexGrid]The filter result is not correct if set different value in the value filter editor twice.
    [116269]bug-[MVC-FlexGrid] The total page count is changed after perform default value filter operation for numeric value.
    [116482][MVC-FlexGrid_Sample]The filter work incorrectly if filter by "End" /"Discount"column twice in MvcExplorer sample.

MVC Explorer
    Fix an issue that link "FlexGrid" cannot be clicked easily in index page.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.13)		Drop Date: 04/29/2015
=====================================================================================

ListBox
    Add CheckedMemberPath property and OnClientItemChecked  to listbox to support multiple select feature.

FlexGrid
    Added a FlexGrid.deferResizing property to defer row and column resizing until the user releases the mouse.
    Added SortRowIndex property to the FlexGrid.
    Add a property SortMemberPath to flexgrid's column.
    Add group panel support for flexgrid.
    Enhance the feature of filter for flex grid.

Gauge
    Add a property Origin to Gauge.

FlexChart
    Add LineMarker Extender for FlexChart.
    Add event property "OnClientRendering" to FlexChart ChartSeries.
    Support Drag mode for LineMarker.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.12)		Drop Date: 04/27/2015
=====================================================================================

All Controls
    Add Spanish culture to the controls.

FlexGrid
    [114886]Fix the issue that an exception is thrown when sort/filter no-generic data(without <T>)

MVC Explorer
    Fix an issue that Header of group is incorrect when grouping by date.

HowTo Samples
    [111685]Fix the issue that "10000%" should be "100%" in "ASPNetMVCGauge101" sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.11)		Drop Date: 03/19/2015
=====================================================================================

MVC Explorer
    [110035]Fix FlexGrid BatchEditing issue
    Make flexgrid readonly in sample "StarSizing".

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.10)		Drop Date: 03/13/2015
=====================================================================================

FlexGrid
    Update/add the summary of members and types.

HowTo Samples
    [111310][Wijmo5 MVC][FlexGrid][ASPNetMVCFlexGrid101] Invalid character javascript error is observe when sorting any column of some FlexGrid in FlexGrid101 sample.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.9)		Drop Date: 03/13/2015
=====================================================================================

FlexChart
	Fixed an issue that "SelectedIndex" property doesn't work.

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.8)		Drop Date: 03/12/2015
=====================================================================================

InputDate
    [112283]Some property of MVC control can't work.

ColorPicker
    [106695]The "Palette" property of ColorPicker can't work correctly.

Gauge
    [112349][Wijmo5 MVC][Guages] Javascirpt error is observe after placing guage control in the MVC application without setting "Value" property.

FlexGrid
    [112385][Wijmo5 MVC][VB]Error is observe after browsing the page which contain FlexGrid in MVC VB Application

MVC Explorer
    Add the friendly error information and remove useless page.
    Fix a ThemeSelector issue caused by wijmo bug fixing.

VS Template
    [112366][Wijmo5 MVC Template][JP Env][VB] Compiling error is observe after creating MVC5 Wijmo Application template with VB in JP environment

=====================================================================================
ComponentOne Studio for ASP.NET MVC Edition (4.0.20151.7)		Drop Date: 03/10/2015
=====================================================================================

MVC Explorer
    [111483][Wijmo5 MVC Explorer]Format of source code become very ugly after double-click on it.
    [111583][Wijmo5 MvcExplorer][FlexGrid] C1FlexGrid is not filtered correctly for Date data type column with 'Equals' operator.
    [108679][Wijmo5 MVC][FlexChart] PlotMargin cannot be set for all sides by setting one string value.
    [111569]An error is thrown when using popup editing in some case.
    Add the ids to FlexGrid and remove useless settings in the sample.
    Fix an issue that an exception will be thrown when updating some cell value in the first grid.

HowTo Samples
    [111304][Wijmo5 MVC][HowTo][ASPNetMVCFlexChart101]Javascript error occurs when setting SelectionMode to 'Point' and getting selection
    [111307][Wijmo5 MVC][HowTo][ASPNetMVCFlexChart101]Incorrect image path for tooltip content in some flexchart samples.
    [111365][Wijmo5 MVC][HowTo][FlexChartGroup]Javascript error occurs when clicking outside of chart area
    [111364][Wijmo5 MVC][HowTo][ASPNetMVCFlexChart101]Incorrect chart types are rendered when choosing some chart types in Selection Mode module.
    [111310][Wijmo5 MVC][FlexGrid][ASPNetMVCFlexGrid101]"Invalid character" javascript error is observe when sorting any column of some FlexGrid in "FlexGrid101" sample.

FlexGrid
    [111565]The page number won't reduce after filtering.
    [111534]Validation message is accumulative when add new row several times.
    [109954][Wijmo5 MVCExplorer][FlexGrid][IE]Javascript error is observed after double click on any cell of FlexGrid in "CustomEditor" sample.
    [111056][Wijmo5 MVC][FlexGrid] Copied clipboard text is not getting pasted to grid selection.
    [108264]ScrollIntoView/Select property of FlexGrid can't work.
    [110160][Wijmo5 MVCExplorer][FlexGrid] "Value cannot be null" message box is shown when deleting data row about 3 times after sorting is applied in Inline Editing sample
    [110034][Wijmo5 MvcExplorer][FlexGrid] 'Trends' column is not displayed properly when reorder 'Trends' column in the grid.
    [110036][Wijmo5 MvcExplorer][FlexGrid] JavaScript error occurs on navigating to last page when the grid is grouped by date column.
    [110035][Wijmo5 MVCExplorer][FlexGrid] "Validation failed for one or more entities." Warning message box is display after exiting edit mode from "Add New Row"

InputMask
    [110816][Wijmo5 MVC][InputMask] JavaScript runtime error: ** Assertion failed in Wijmo: Unknown key "required" if 'Required' is set false

CollectionView
    [109973]Refix it to avoid the nullreference exception when the SourceCollection is not set and convert the text to boolean in client side.
    [109973][Wijmo5 MVCExplorer][FlexGrid] Multiple issues are observe when filtering  the data in FlexGrid in "Filter" sample of MVCExplorer.
    [111325]Error will throw out after filtering the grid in some case.

ComboBox
    [110850][Wijmo5 MVC][Listbox] Unlike wijmo5 ListBox, SelectedIndexChanged event does not work correctly at first run time
    [109705][Wijmo5 MVC Explorer][ComboBox] Large blank space is displayed in front of Text after choosing item from ItemTemplate of ComboBox

ListBox
    [110850][Wijmo5 MVC][Listbox] Unlike wijmo5 ListBox, SelectedIndexChanged event does not work correctly at first run time

InputTime
    [111473][Wijmo5 MVC][MvcExplorer][InputDate][InputTime] isDroppedDown 'true' property does not work correctly at run time
    [107883][Wijmo5 MVCExplorer][InputTime] Time cannot be edited properly when Mask is set for InputTime

Core
    [106717][asp.net]The "GetType"/"GetHashCode"/"Equals"/"Text" method/property should be hide in server.
    [110047][Wijmo5 MvcExplorer][FlexGrid] Empty cells are changed to 'null' when finish editing in a row of C1FlexGrid.
    [109967][Wijmo5 MVCExplorer][FlexGrid]Javascript error is observe after sorting any column in Custom Cells sample of Wijmo 5 MVCExplorer sample.

==================================================================================
ComponentOne Studio for ASP.NET MVC Edition	GrapeCity, Inc - www.componentone.com
==================================================================================

A set of modern UI controls built upon latest cutting edge technologies like HTML5, CSS, ECMA5 without making compromises to support legacy browsers. Inside ComponentOne ASP.NET MVC Edition you will find fast and lightweight controls ranging from data management to data visualization, project templates, and professionally designed themes. Shift you site into overdrive with ComponentOne ASP.NET MVC Edition.
