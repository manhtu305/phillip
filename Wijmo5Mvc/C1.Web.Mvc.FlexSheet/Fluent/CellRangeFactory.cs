﻿using C1.Web.Mvc.Fluent;
using C1.Web.Mvc.Grid;
using System.Collections.Generic;

namespace C1.Web.Mvc.Sheet.Fluent
{
    /// <summary>
    /// The CellRangeFactory for setting CellRanges with fluent api.
    /// </summary>
    public class CellRangeFactory : BaseBuilder<IList<CellRange>, CellRangeFactory>
    {
        /// <summary>
        /// The constructor for CellRangeFactory.
        /// </summary>
        /// <param name="ranges">The Range list</param>
        public CellRangeFactory(IList<CellRange> ranges)
            : base(ranges)
        {
        }

        /// <summary>
        /// Add the CellRange to Range list.
        /// </summary>
        /// <param name="row">The start row index</param>
        /// <param name="col">The start column index</param>
        /// <param name="row2">The end row index</param>
        /// <param name="col2">The end column index</param>
        /// <returns></returns>
        public CellRangeFactory AddCellRange(int row, int col, int? row2, int? col2)
        {
            Object.Add(new CellRange(row, col, row2 ?? row, col2 ?? col));
            return this;
        }
    }
}
