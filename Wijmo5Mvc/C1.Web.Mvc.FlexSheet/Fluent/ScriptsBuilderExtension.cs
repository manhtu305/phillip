﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.Sheet.Fluent
{
    /// <summary>
    /// Extends <see cref="ScriptsBuilder"/> class for FlexSheet control scripts.
    /// </summary>
    public static class ScriptsBuilderExtension
    {
        /// <summary>
        /// Registers FlexSheet related script bundle. This bundle contains FlexSheet control.
        /// </summary>
        /// <param name="scriptsBuilder">The <see cref="ScriptsBuilder"/>.</param>
        /// <returns>The <see cref="ScriptsBuilder"/>.</returns>
        public static ScriptsBuilder FlexSheet(this ScriptsBuilder scriptsBuilder)
        {
            scriptsBuilder.OwnerTypes.AddRange(FlexSheetWebResourcesHelper.AllScriptOwnerTypes.Value);
            return scriptsBuilder;
        }
    }
}
