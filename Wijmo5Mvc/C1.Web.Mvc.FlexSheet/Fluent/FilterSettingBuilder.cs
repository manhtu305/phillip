﻿using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Sheet.Fluent
{
    partial class FilterSettingBuilder
    {
        /// <summary>
        /// Sets an array containing the names or bindings of the columns that should have filters.
        /// </summary>
        /// <param name="columns">The array indicates which columns should be filterable.</param>
        /// <returns>Current builder</returns>
        public FilterSettingBuilder FilterColumns(params string[] columns)
        {
            Object.FilterColumns = columns;
            return this;
        }

        /// <summary>
        /// Sets the ColumnFilters property.
        /// </summary>
        /// <param name="build">The build action.</param>
        /// <returns>Current builder.</returns>
        public FilterSettingBuilder ColumnFilters(Action<ListItemFactory<ColumnFilterSetting, ColumnFilterSettingBuilder>> build)
        {
            build(new ListItemFactory<ColumnFilterSetting, ColumnFilterSettingBuilder>(Object.ColumnFilters,
                () => new ColumnFilterSetting(Object.Helper), t => new ColumnFilterSettingBuilder(t)));
            return this;
        }
    }

    partial class ValueFilterSettingBuilder
    {
        /// <summary>
        /// Sets the UniqueValues property.
        /// </summary>
        /// <remarks>
        /// Gets or sets an array containing the unique values to be displayed on the list.
        /// </remarks>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public ValueFilterSettingBuilder UniqueValues(params object[] value)
        {
            Object.UniqueValues = value;
            return this;
        }
    }
}
