﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.Sheet.Fluent
{
    /// <summary>
    /// Defines a builder to configurate <see cref="DefinedName" />.
    /// </summary>
    public partial class DefinedNameBuilder : BaseBuilder<DefinedName, DefinedNameBuilder>
    {
        /// <summary>
        /// Creates one <see cref="DefinedNameBuilder" /> instance to configurate <paramref name="obj"/>.
        /// </summary>
        /// <param name="obj">The <see cref="DefinedName" /> object to be configurated.</param>
        public DefinedNameBuilder(DefinedName obj) : base(obj)
        {
        }

        /// <summary>
        /// Configurates <see cref="DefinedName.Name" />.
        /// Sets the name of the defined name item.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public DefinedNameBuilder Name(string value)
        {
            Object.Name = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="DefinedName.SheetName" />.
        /// Sets the defined name item works in which sheet.
        /// If omitted, the defined name item works in workbook.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public DefinedNameBuilder SheetName(string value)
        {
            Object.SheetName = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="DefinedName.Value" />.
        /// Sets the value of the defined name item.
        /// The value could be a formula, a string constant or a cell range.
        /// For e.g. "Sum(1, 2, 3)", "1.2", "\"test\"" or "sheet1!A1:B2".
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public DefinedNameBuilder Value(string value)
        {
            Object.Value = value;
            return this;
        }
    }
}
