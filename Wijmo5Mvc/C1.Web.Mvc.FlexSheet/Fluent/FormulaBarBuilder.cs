﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.Sheet.Fluent
{
    partial class FormulaBarBuilder : ExtenderBuilder<FormulaBar, FormulaBarBuilder>
    {
        #region Ctor
        /// <summary>
        /// Create one FormulaBarBuilder instance.
        /// </summary>
        /// <param name="extender">The FormulaBar extender.</param>
        public FormulaBarBuilder(FormulaBar extender)
            : base(extender)
        {
        }
        #endregion Ctor
    }
}
