﻿using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Sheet.Fluent
{
    public abstract partial class SheetBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Sets the SelectionRanges property.
        /// </summary>
        /// <param name="build">The build action</param>
        /// <returns>Current builder</returns>
        public TBuilder SelectionRanges(Action<CellRangeFactory> build)
        {
            build(new CellRangeFactory(Object.SelectionRanges));
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the Tables property.
        /// </summary>
        /// <param name="build">The build action.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Tables(Action<ListItemFactory<Table, TableBuilder>> build)
        {
            build(new ListItemFactory<Table, TableBuilder>(Object.Tables,
                () => new Table(Object.Helper), t => new TableBuilder(t)));
            return this as TBuilder;
        }
    }
}
