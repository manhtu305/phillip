﻿using System;

namespace C1.Web.Mvc.Sheet.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use FormulaBar extender.
    /// </summary>
    public static class FormulaBarExtension
    {
        /// <summary>
        /// Apply the FormulaBar extender in FlexSheet.
        /// </summary>
        /// <param name="flexSheetBuilder">The specified FlexSheet builder.</param>
        /// <param name="selector">The specified selector for the FormulaBar's host element.</param>
        /// <returns>The FlexSheet builder</returns>
        public static FlexSheetBuilder ShowFormulaBar(this FlexSheetBuilder flexSheetBuilder, string selector = null)
        {
            var sheet = flexSheetBuilder.Object;
            sheet.Extenders.Add(new FormulaBar(sheet, selector));
            return flexSheetBuilder;
        }

        /// <summary>
        /// Apply the FormulaBar extender in FlexSheet.
        /// </summary>
        /// <param name="flexSheetBuilder">The specified FlexSheet builder.</param>
        /// <param name="formulaBarBuilder">The specified FormulaBar builder.</param>
        /// <returns>The FlexSheet builder</returns>
        public static FlexSheetBuilder ShowFormulaBar(this FlexSheetBuilder flexSheetBuilder, Action<FormulaBarBuilder> formulaBarBuilder)
        {
            var sheet = flexSheetBuilder.Object;
            var formulaBar = new FormulaBar(sheet);
            formulaBarBuilder(new FormulaBarBuilder(formulaBar));
            sheet.Extenders.Add(formulaBar);
            return flexSheetBuilder;
        }
    }
}
