﻿using C1.Web.Mvc.Fluent;
using C1.Web.Mvc.Grid;
using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Sheet.Fluent
{
    public partial class TableBuilder
    {
        #region CollectionViewService
        /// <summary>
        /// Sets ItemsSource by builder.
        /// </summary>
        /// <param name="itemsSourceBuild">The build action for setting ItemsSource.</param>
        /// <returns>Current builder.</returns>
        public TableBuilder Bind(Action<CollectionViewServiceBuilder<object>> itemsSourceBuild)
        {
            var itemsSource = Object.GetDataSource<CollectionViewService<object>>();
            itemsSource.DisableServerRead = true;
            itemsSourceBuild(new CollectionViewServiceBuilder<object>(itemsSource));
            return this;
        }

        /// <summary>
        /// Binds to a collection.
        /// </summary>
        /// <param name="sourceCollection">The source collection.</param>
        /// <returns>Current builder.</returns>
        public TableBuilder Bind(IEnumerable<object> sourceCollection)
        {
            return Bind(cvsb => cvsb.Bind(sourceCollection));
        }

        /// <summary>
        /// Sets the read action url.
        /// </summary>
        /// <param name="readActionUrl">The action url.</param>
        /// <returns>Current builder.</returns>
        public TableBuilder Bind(string readActionUrl)
        {
            return Bind(cvsb => cvsb.Bind(readActionUrl));
        }
        #endregion  CollectionViewService

        #region ODataCollectionViewService
        /// <summary>
        /// Configurates <see cref="Table.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public TableBuilder BindODataSource(Action<ODataCollectionViewServiceBuilder<object>> build)
        {
            build(new ODataCollectionViewServiceBuilder<object>(Object.GetDataSource<ODataCollectionViewService<object>>()));
            return this;
        }
        #endregion ODataCollectionViewService

        /// <summary>
        /// Sets the Columns property.
        /// </summary>
        /// <param name="build">The build action.</param>
        /// <returns>Current builder.</returns>
        public TableBuilder Columns(Action<ListItemFactory<TableColumn, TableColumnBuilder>> build)
        {
            build(new ListItemFactory<TableColumn, TableColumnBuilder>(Object.Columns,
                () => new TableColumn(), t => new TableColumnBuilder(t)));
            return this;
        }

        /// <summary>
        /// Sets the Range property.
        /// </summary>
        /// <param name="range">The cell range.</param>
        /// <returns>Current builder.</returns>
        public TableBuilder Range(CellRange range)
        {
            Object.Range = range;
            return this;
        }

        /// <summary>
        /// Sets the Range property.
        /// </summary>
        /// <param name="row">The start row index.</param>
        /// <param name="col">The start column index.</param>
        /// <param name="row2">The end row index.</param>
        /// <param name="col2">The end column index.</param>
        /// <returns>Current builder.</returns>
        public TableBuilder Range(int row, int col, int? row2 = null, int? col2 = null)
        {
            Object.Range = new CellRange(row, col, row2 ?? row, col2 ?? col);
            return this;
        }

        /// <summary>
        /// Sets the Style property.
        /// </summary>
        /// <param name="style">The table style of this table.</param>
        /// <returns>Current builder.</returns>
        public TableBuilder Style(TableStyle style)
        {
            Object.Style = style;
            return this;
        }
    }
}
