﻿using C1.Web.Mvc.Fluent;
using System;
using System.ComponentModel;
using System.Linq;
#if ASPNETCORE
using IHtmlString = Microsoft.AspNetCore.Html.IHtmlContent;
#else
using System.Web;
#endif

namespace C1.Web.Mvc.Sheet.Fluent
{
    /// <summary>
    /// Extends ControlBuilderFactory for FlexSheet related controls creation.
    /// </summary>
    public static class ControlBuilderFactoryExtension
    {
        /// <summary>
        /// Create a FlexSheetBuilder.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory</param>
        /// <param name="selector">The selector</param>
        /// <returns>The FlexSheetBuilder</returns>
        public static FlexSheetBuilder FlexSheet(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new FlexSheetBuilder(new FlexSheet(controlBuilderFactory._helper, selector));
        }

        /// <summary>
        /// Render the FlexSheet related js resources.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory</param>
        /// <param name="controlTypes">Specify the types of the controls</param>
        /// <returns>The html string</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please use Scripts method instead.")]
        public static IHtmlString FlexSheetResources(this ControlBuilderFactory controlBuilderFactory, params Type[] controlTypes)
        {
            var c1Resources = new FlexSheetWebResourcesManager(controlBuilderFactory._helper);
            controlTypes.ToList().ForEach(ct => c1Resources.ControlTypes.Add(ct));
            return c1Resources;
        }
    }
}
