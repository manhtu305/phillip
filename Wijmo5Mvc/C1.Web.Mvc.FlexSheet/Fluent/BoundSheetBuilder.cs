﻿using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc.Sheet.Fluent
{
    public partial class BoundSheetBuilder<T>
    {
        /// <summary>
        /// Configurates <see cref="BoundSheet{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="itemsSourceBuild">The build action for setting ItemsSource</param>
        /// <returns>Current builder</returns>
        public BoundSheetBuilder<T> Bind(Action<CollectionViewServiceBuilder<T>> itemsSourceBuild)
        {
            itemsSourceBuild(new CollectionViewServiceBuilder<T>(Object.ItemsSource as CollectionViewService<T>));
            return this;
        }

        /// <summary>
        /// Configurates <see cref="BoundSheet{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="sourceCollection">The source collection</param>
        /// <returns>Current builder</returns>
        public BoundSheetBuilder<T> Bind(IEnumerable<T> sourceCollection)
        {
            return Bind(itemsSourceBuilder => itemsSourceBuilder.Bind(sourceCollection));
        }

        /// <summary>
        /// Configurates <see cref="BoundSheet{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="readActionUrl">The action url.</param>
        /// <returns>Current builder</returns>
        public BoundSheetBuilder<T> Bind(string readActionUrl)
        {
            return Bind(itemsSourceBuilder => itemsSourceBuilder.Bind(readActionUrl));
        }

        /// <summary>
        /// Configurates <see cref="BoundSheet{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataVirtualCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public BoundSheetBuilder<T> BindODataVirtualSource(Action<ODataVirtualCollectionViewServiceBuilder<T>> build)
        {
            build(new ODataVirtualCollectionViewServiceBuilder<T>(Object.GetDataSource<ODataVirtualCollectionViewService<T>>()));
            return this;
        }

        /// <summary>
        /// Configurates <see cref="BoundSheet{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public BoundSheetBuilder<T> BindODataSource(Action<ODataCollectionViewServiceBuilder<T>> build)
        {
            build(new ODataCollectionViewServiceBuilder<T>(Object.GetDataSource<ODataCollectionViewService<T>>()));
            return this;
        }

        /// <summary>
        /// Configure <see cref="PropertyGroupDescription"/>.
        /// </summary>
        /// <param name="names">The property names used for grouping</param>
        /// <returns>Current builder</returns>
        public BoundSheetBuilder<T> GroupBy(params string[] names)
        {
            Object.AddGroupDescriptions(names.Select(n => new PropertyGroupDescription { PropertyName = n }));
            return this;
        }

        /// <summary>
        /// Configure <see cref="SortDescription"/>.
        /// </summary>
        /// <param name="names">Sort by these names</param>
        /// <returns>Current builder</returns>
        public BoundSheetBuilder<T> OrderBy(params string[] names)
        {
            Object.AddSortDescriptions(names.Select(n => new SortDescription { Property = n, Ascending = true }));
            return this;
        }

        /// <summary>
        /// Configure <see cref="SortDescription"/>.
        /// </summary>
        /// <param name="names">Sort by these names with descending</param>
        /// <returns>Current builder</returns>
        public BoundSheetBuilder<T> OrderByDescending(params string[] names)
        {
            Object.AddSortDescriptions(names.Select(n => new SortDescription { Property = n, Ascending = false }));
            return this;
        }

        /// <summary>
        /// Make ItemsSource pagable with specified page size.
        /// 0 means to disable paging.
        /// </summary>
        /// <param name="pageSize">The page size</param>
        /// <returns>Current builder</returns>
        public BoundSheetBuilder<T> PageSize(int pageSize)
        {
            Object.PageSize = pageSize;
            Bind(itemsSourceBuilder => itemsSourceBuilder.PageSize(pageSize));
            return this;
        }
    }
}
