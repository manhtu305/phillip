﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using C1.Web.Mvc.Fluent;
using C1.Web.Mvc.Grid;
using System.IO;

namespace C1.Web.Mvc.Sheet.Fluent
{
    public partial class FlexSheetBuilder
    {
        private const int _DEFAULT_ROW_COUNT = 200;
        private const int _DEFAULT_COLUMN_COUNT = 20;
        
        /// <summary>
        /// Selects a cell range and optionally scrolls it into view.
        /// </summary>
        /// <param name="row">Index of the first row in this range.</param>
        /// <param name="col">Index of the first column in this range.</param>
        /// <param name="row2">Index of the last row in this range.</param>
        /// <param name="col2">Index of the last column in this range.</param>
        /// <param name="scrollToSelection">Specify whether to scroll the seletion into view.</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder Select(int row, int col, int? row2, int? col2, bool scrollToSelection = true)
        {
            Object.Selection = new CellRange(row, col, row2 ?? row, col2 ?? col);
            Object.ScrollToSelection = scrollToSelection;
            return this;
        }

        /// <summary>
        /// Load the Workbook instance.
        /// </summary>
        /// <param name="workbook">The Workbook instance</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder Load(Workbook workbook)
        {
            Object.Workbook = workbook;
            return this;
        }

        /// <summary>
        /// Load the xlsx file with file path.
        /// </summary>
        /// <param name="filePath">The path of xlsx file</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder Load(string filePath)
        {
            Object.FilePath = filePath;
            return this;
        }

        /// <summary>
        /// Load the xlsx file with FileStream.
        /// </summary>
        /// <param name="fileStream">The file stream of xlsx file</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder Load(Stream fileStream)
        {
            Object.FileStream = fileStream;
            return this;
        }

        /// <summary>
        /// Sets the LoadActionUrl for remote loading.
        /// </summary>
        /// <param name="loadActionUrl">The url of load action.</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder RemoteLoad(string loadActionUrl)
        {
            Object.LoadActionUrl = loadActionUrl;
            return this;
        }

        /// <summary>
        /// Sets the SaveActionUrl for remote saving.
        /// </summary>
        /// <param name="saveActionUrl">The url of save action.</param>
        /// <param name="saveContentType">The content type.</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder RemoteSave(string saveActionUrl, ContentType saveContentType = ContentType.Xlsx)
        {
            Object.SaveContentType = saveContentType;
            Object.SaveActionUrl = saveActionUrl;
            return this;
        }

        /// <summary>
        /// Add a bound sheet. It will be put after the loaded Workbook or xlsx file.
        /// </summary>
        /// <typeparam name="T">The data item type</typeparam>
        /// <param name="build">The build action</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder AddBoundSheet<T>(Action<BoundSheetBuilder<T>> build)
        {
            BoundSheet<T> sheet = new BoundSheet<T>(Object.Helper);
            build(new BoundSheetBuilder<T>(sheet));
            Object.AppendedSheets.Add(sheet);
            return this;
        }

        /// <summary>
        /// Add a bound sheet. It will be put after the loaded Workbook or xlsx file.
        /// </summary>
        /// <param name="build">The build action</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder AddBoundSheet(Action<BoundSheetBuilder<object>> build)
        {
            return AddBoundSheet<object>(build);
        }

        /// <summary>
        /// Add an unbound sheet. It will be put after the loaded Workbook or xlsx file.
        /// </summary>
        /// <param name="name">The sheet name</param>
        /// <param name="rowCount">The number of rows in the sheet.</param>
        /// <param name="columnCount">The number of columns in the sheet.</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder AddUnboundSheet(string name = null, int rowCount = _DEFAULT_ROW_COUNT, int columnCount = _DEFAULT_COLUMN_COUNT)
        {
            UnboundSheet sheet = new UnboundSheet(Object.Helper)
            {
                Name = name,
                RowCount = rowCount,
                ColumnCount = columnCount
            };

            Object.AppendedSheets.Add(sheet);
            return this;
        }

        /// <summary>
        /// Add an unbound sheet. It will be put after the loaded Workbook or xlsx file.
        /// </summary>
        /// <param name="build">The build action</param>
        /// <returns>Current FlexSheet builder</returns>
        public FlexSheetBuilder AddUnboundSheet(Action<UnboundSheetBuilder> build)
        {
            UnboundSheet sheet = new UnboundSheet(Object.Helper)
            {
                RowCount = _DEFAULT_ROW_COUNT,
                ColumnCount = _DEFAULT_COLUMN_COUNT
            };

            build(new UnboundSheetBuilder(sheet));
            Object.AppendedSheets.Add(sheet);
            return this;
        }

        /// <summary>
        /// Configure <see cref="FlexSheet.DefinedNames"/>.
        /// </summary>
        /// <param name="build">The build action.</param>
        /// <returns>Current builder.</returns>
        public virtual FlexSheetBuilder DefinedNames(Action<ListItemFactory<DefinedName, DefinedNameBuilder>> build)
        {
            build(new ListItemFactory<DefinedName, DefinedNameBuilder>(Object.DefinedNames,
                () => new DefinedName(), c => new DefinedNameBuilder(c)));
            return this;
        }

        #region hidden FlexGrid methond
        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder AllowAddNew(bool value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder Bind(Action<CollectionViewServiceBuilder<object>> itemsSourceBuild)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder Bind(string readActionUrl)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder Bind(IEnumerable<object> sourceCollection)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder BindODataSource(Action<ODataCollectionViewServiceBuilder<object>> build)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder BindODataVirtualSource(Action<ODataVirtualCollectionViewServiceBuilder<object>> build)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder GroupBy(params string[] names)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder OrderBy(params string[] names)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder ItemsSourceId(string Id)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder OrderByDescending(params string[] names)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder PageSize(int pageSize)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder AllowSorting(bool value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder SortingType(AllowSorting value)
        {
            return this;
        }

        /// <summary>
        /// Sets a boolean value decides auto generate columns on FlexSheet.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current FlexSheet builder</returns>
        public override FlexSheetBuilder AutoGenerateColumns(bool value)
        {
            Object.AutoGenerateColumns = value;
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder QuickAutoSize(bool? value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder CellsTemplate(Action<CellTemplateBuilder> build)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder ChildItemsPath(string value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder ColumnHeadersTemplate(Action<CellTemplateBuilder> build)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder GroupHeaderFormat(string value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder HeadersVisibility(C1.Web.Mvc.Grid.HeadersVisibility value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder ItemFormatter(string value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder ShowGroups(bool value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder ShowSort(bool value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder StickyHeaders(bool value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder SortRowIndex(int? value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder TopLeftCellsTemplate(Action<CellTemplateBuilder> build)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder TreeIndent(int value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder RowHeadersTemplate(Action<CellTemplateBuilder> build)
        {
            return this;
        }

        /// <summary>
        /// This method is not recommended for FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder Columns(Action<ListItemFactory<Mvc.Column, ColumnBuilder>> builder)
        {
            return base.Columns(builder);
        }

        /// <summary>
        /// This method is useless for FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder ShowColumnFooters(bool value = true, string rowHeaderText = null)
        {
            return this;
        }

        /// <summary>
        /// This method is useless for FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder ColumnFootersTemplate(Action<CellTemplateBuilder> build)
        {
            return this;
        }

        /// <summary>
        /// This method is useless for FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override FlexSheetBuilder BottomLeftCellsTemplate(Action<CellTemplateBuilder> build)
        {
            return this;
        }

        #endregion

    }
}
