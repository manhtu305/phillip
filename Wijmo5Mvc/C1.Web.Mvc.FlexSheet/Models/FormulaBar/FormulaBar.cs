﻿namespace C1.Web.Mvc.Sheet
{
    public partial class FormulaBar
    {
        #region Ctors
        /// <summary>
        /// Creates one <see cref="FormulaBar"/> instance.
        /// </summary>
        /// <param name="sheet">The specified <see cref=" C1.Web.Mvc.Sheet.FlexSheet"/> object which uses formula bar.</param>
        /// <param name="selector">The selector.</param>
        public FormulaBar(FlexSheet sheet, string selector = null)
            :base(sheet)
        {
            Initialize();
            Selector = selector;
        }
        #endregion Ctors
    }
}
