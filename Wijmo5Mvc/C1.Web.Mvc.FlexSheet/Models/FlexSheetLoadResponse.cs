﻿using System.IO;

namespace C1.Web.Mvc.Sheet
{
    public partial class FlexSheetLoadResponse
    {
        /// <summary>
        /// Creates one <see cref="FlexSheetLoadResponse"/> instance.
        /// </summary>
        public FlexSheetLoadResponse()
        {
            Initialize();
        }

        #region FilePath
        private string _filePath;
        private void _SetFilePath(string value)
        {
            if (value != null)
            {
                _filePath = value;

#if !MODEL
                string path = _filePath;
                if (path.StartsWith("~"))
                {
                    path = Component.HostingEnvironment.MapPath(path);
                }

                FileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
#endif
            }
        }
        private string _GetFilePath()
        {
            return _filePath;
        }
        #endregion FilePath
    }
}
