﻿namespace C1.Web.Mvc.Sheet
{
    /// <summary>
    /// Specifies the type of remote save content.
    /// </summary>
    public enum ContentType
    {
        /// <summary>
        /// The xlsx file.
        /// </summary>
        Xlsx = 0,

        /// <summary>
        /// The Workbook object model.
        /// </summary>
        Workbook = 1
    }
}
