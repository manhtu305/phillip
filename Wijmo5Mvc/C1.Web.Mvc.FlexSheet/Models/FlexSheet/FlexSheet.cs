﻿using C1.JsonNet;
using System.Collections.Generic;
using System.IO;

namespace C1.Web.Mvc.Sheet
{
    public partial class FlexSheet
    {
        #region AppendedSheets
        private List<Sheet> _appendedSheets;
        private List<Sheet> _GetAppendedSheets()
        {
            return _appendedSheets ?? (_appendedSheets = new List<Sheet>());
        }
        #endregion AppendedSheets

        #region DefinedNames
        private List<DefinedName> _definedNames;
        private List<DefinedName> _GetDefinedNames()
        {
            return _definedNames ?? (_definedNames = new List<DefinedName>());
        }
        #endregion DefinedNames

        #region Filter
        private FlexSheetFilter _filter;
        private FlexSheetFilter _GetFilter()
        {
            return _filter ?? (_filter = new FlexSheetFilter());
        }
        #endregion

        #region FilePath
        private string _filePath;
        private void _SetFilePath(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                _filePath = value;
#if !MODEL
                string path = _filePath;
                if (path.StartsWith("~"))
                {
                    path = HostingEnvironment.MapPath(path);
                }

                _SetFileStreamDirectly(new FileStream(path, FileMode.Open, FileAccess.Read));
#endif
            }
        }
        private string _GetFilePath()
        {
            return _filePath;
        }
        #endregion FilePath

        #region FileStream
        private Stream _fileStream;
        private Stream _GetFileStream()
        {
            return _fileStream;
        }
        //Copy the stream and set to FileStream property.
        //The Stream instance won't be disposed.
        private void _SetFileStream(Stream value)
        {
            if (value != null)
            {
                var fileStream = new MemoryStream();
                value.Seek(0, SeekOrigin.Begin);
                value.CopyTo(fileStream);
                _SetFileStreamDirectly(fileStream);
            }
        }
        //The Stream instance will be disposed after serialize.
        private void _SetFileStreamDirectly(Stream value)
        {
            if (value != null)
            {
                if (_fileStream != null)
                {
                    _fileStream.Dispose();
                }
                _fileStream = value;
            }
        }
        #endregion FileStream

        #region hidden properties inherit FlexGrid

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override IItemsSource<object> ItemsSource
        {
            get
            {
                return base.ItemsSource;
            }
        }
        internal override bool _ShouldSerializeItemsSource()
        {
            return false;
        }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override string ItemsSourceId { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override bool AllowSorting { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override Grid.AllowSorting SortingType { get; set; }

        /// <summary>
        /// Gets Sets Auto Generate Columns on FlexSheet
        /// </summary>
        public override bool AutoGenerateColumns { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override bool? QuickAutoSize { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override CellTemplate CellsTemplate
        {
            get
            {
                return base.CellsTemplate;
            }
        }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override string ChildItemsPath { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override CellTemplate ColumnHeadersTemplate
        {
            get
            {
                return base.ColumnHeadersTemplate;
            }
        }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override string GroupHeaderFormat { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override Grid.HeadersVisibility HeadersVisibility { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override string ItemFormatter { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override CellTemplate RowHeadersTemplate
        {
            get
            {
                return base.RowHeadersTemplate;
            }
        }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override bool ShowGroups { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override bool ShowSort { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override bool StickyHeaders { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override int? SortRowIndex { get; set; }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override CellTemplate TopLeftCellsTemplate
        {
            get
            {
                return base.TopLeftCellsTemplate;
            }
        }

        /// <summary>
        /// This property is useless in FlexSheet.
        /// </summary>
        [Ignore]
        public override int TreeIndent { get; set; }

        /// <summary>
        /// This property is not recommended for FlexSheet.
        /// </summary>
        public override IList<Mvc.Column> Columns
        {
            get
            {
                return base.Columns;
            }
        }
        #endregion hidden properties inherit FlexGrid
    }
}
