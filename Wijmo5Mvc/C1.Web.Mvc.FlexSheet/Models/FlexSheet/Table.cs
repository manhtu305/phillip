﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
#if MODEL
using C1.Web.Mvc.Serialization;
#endif
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc.Sheet
{
    partial class Table
    {
#region Ctor
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="Table"/> instance.
        /// </summary>
        public Table() : this(null)
        {
        }

        /// <summary>
        /// Creates one <see cref="Table"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        public Table(HtmlHelper helper)
        {
            _helper = helper;
            Initialize();
        }
#else
        /// <summary>
        /// Creates one <see cref="Table/> instance.
        /// </summary>
        public Table()
        {
            Initialize();
        }
#endif
#endregion Ctor

#region Columns
        private IList<TableColumn> _columns;
        private IList<TableColumn> _GetColumns()
        {
            return _columns ?? (_columns = new List<TableColumn>());
        }
#endregion

#region ItemsSource
        private IItemsSource<object> _itemsSource;
        private IItemsSource<object> _GetItemsSource()
        {
#if !MODEL
            if (_itemsSource == null && _helper != null)
            {
                _itemsSource = new CollectionViewService<object>(_helper);
                _itemsSource.DisableServerRead = true;
            }
#else
            if (_itemsSource == null)
            {
                _itemsSource = new CollectionViewService<object>();
                _itemsSource.DisableServerRead = true;
            }
#endif
            return _itemsSource;
        }
        private void _SetItemsSource(IItemsSource<object> value)
        {
            _itemsSource = value;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        private bool _ShouldSerializeItemsSource()
        {
#if !MODEL
            return _itemsSource!=null;
#else
            return ItemsSourceBinding.IsItemsSourceDirty(DbContextType, ModelType);
#endif
        }
#endregion ItemsSource
    }

    partial class TableColumn
    {
#region Ctor
        /// <summary>
        /// Creates one <see cref="TableColumn"/> instance.
        /// </summary>
        public TableColumn()
        {
            Initialize();
        }
#endregion Ctor
    }

    partial class TableStyle
    {
#region Ctor
        /// <summary>
        /// Creates one <see cref="TableStyle"/> instance.
        /// </summary>
        /// <param name="name">The name of the table style.</param>
        public TableStyle(string name) : this()
        {
            Name = name;
        }

        /// <summary>
        /// Creates one <see cref="TableStyle"/> instance of built-in light style.
        /// </summary>
        /// <param name="index">The index of the built-in style. Valid range is from 1 to 21.</param>
        /// <returns>The TableStyle instance.</returns>
        public static TableStyle CreateBuiltInTableStyleLight(int index)
        {
            if (index < 1 || index > 21) throw new ArgumentOutOfRangeException();
            return new TableStyle("TableStyleLight" + index);
        }

        /// <summary>
        /// Creates one <see cref="TableStyle"/> instance of built-in medium style.
        /// </summary>
        /// <param name="index">The index of the built-in style. Valid range is from 1 to 28.</param>
        /// <returns>The TableStyle instance.</returns>
        public static TableStyle CreateBuiltInTableStyleMedium(int index)
        {
            if (index < 1 || index > 28) throw new ArgumentOutOfRangeException();
            return new TableStyle("TableStyleMedium" + index);
        }

        /// <summary>
        /// Creates one <see cref="TableStyle"/> instance of built-in dark style.
        /// </summary>
        /// <param name="index">The index of the built-in style. Valid range is from 1 to 11.</param>
        /// <returns>The TableStyle instance.</returns>
        public static TableStyle CreateBuiltInTableStyleDark(int index)
        {
            if (index < 1 || index > 11) throw new ArgumentOutOfRangeException();
            return new TableStyle("TableStyleDark" + index);
        }
#endregion Ctor

        /// <summary>
        /// Indicates whether the table style is built-in style.
        /// </summary>
        /// <remarks>
        /// The built-in style has the name of "TableStyleLight1" to "TableStyleLight21", "TableStyleMedium1" to "TableStyleMedium28", and "TableStyleDark1" to "TableStyleDark11".
        /// Settings of other properties will be ignored for the built-in style.
        /// </remarks>
        public bool IsBuiltIn
        {
            get
            {
                if (Name == null) return false;

                Func<string, int, int, bool> isValidIndex = (string s, int start, int end) =>
                {
                    int index;
                    if (!int.TryParse(s, out index)) return false;
                    return index >= start && index <= end;
                };

                if (Name.StartsWith("TableStyleLight"))
                {
                    return isValidIndex(Name.Substring(15), 1, 21);
                }
                else if (Name.StartsWith("TableStyleMedium"))
                {
                    return isValidIndex(Name.Substring(16), 1, 28);
                }
                else if (Name.StartsWith("TableStyleDark"))
                {
                    return isValidIndex(Name.Substring(14), 1, 11);
                }

                return false;
            }
        }
    }
}
