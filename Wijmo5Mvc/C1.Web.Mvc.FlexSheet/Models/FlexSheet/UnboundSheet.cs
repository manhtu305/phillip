﻿using System.ComponentModel;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc.Sheet
{
    partial class UnboundSheet
    {
#if !MODEL
        /// <summary>
        /// Creates one <see cref="UnboundSheet"/> instance.
        /// </summary>
        public UnboundSheet() : this(null) { }

        /// <summary>
        /// Creates one <see cref="UnboundSheet"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        public UnboundSheet(HtmlHelper helper) : base(helper)
        {
            Initialize();
        }
#else
        /// <summary>
        /// Creates one <see cref="UnboundSheet"/> instance.
        /// </summary>
        public UnboundSheet()
        {
            Initialize();
        }
#endif
    }
}
