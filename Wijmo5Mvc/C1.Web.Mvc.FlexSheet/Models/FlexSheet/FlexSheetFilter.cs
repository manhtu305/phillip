﻿using System.Collections.Generic;
using System.ComponentModel;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc.Sheet
{
    partial class FilterSetting
    {
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="FilterSetting"/> instance.
        /// </summary>
        public FilterSetting() : this(null) { }

        /// <summary>
        /// Creates one <see cref="FilterSetting"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        public FilterSetting(HtmlHelper helper)
        {
            _helper = helper;
            Initialize();
        }

        /// <summary>
        /// Gets or sets the htmlhelper object.
        /// </summary>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal HtmlHelper Helper
        {
            get { return _helper; }
        }
#else
        /// <summary>
        /// Creates one <see cref="FilterSetting"/> instance.
        /// </summary>
        public FilterSetting()
        {
            Initialize();
        }
#endif

        #region ColumnFilters
        private IList<ColumnFilterSetting> _columnFilters;
        private IList<ColumnFilterSetting> _GetColumnFilters()
        {
            return _columnFilters ?? (_columnFilters = new List<ColumnFilterSetting>());
        }
        #endregion
    }

    partial class ColumnFilterSetting
    {
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="ColumnFilterSetting"/> instance.
        /// </summary>
        public ColumnFilterSetting() : this(null) { }

        /// <summary>
        /// Creates one <see cref="ColumnFilterSetting"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        public ColumnFilterSetting(HtmlHelper helper)
        {
            _helper = helper;
            Initialize();
        }

        /// <summary>
        /// Gets or sets the htmlhelper object.
        /// </summary>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal HtmlHelper Helper
        {
            get { return _helper; }
        }
#else
        /// <summary>
        /// Creates one <see cref="ColumnFilterSetting"/> instance.
        /// </summary>
        public ColumnFilterSetting()
        {
            Initialize();
        }
#endif

        #region ValueFilter
        private ValueFilterSetting _valueFilter;
        private ValueFilterSetting _GetValueFilter()
        {
            if (_valueFilter == null)
            {
#if !MODEL
                _valueFilter = new ValueFilterSetting(Helper);
#else
                _valueFilter = new ValueFilterSetting();
#endif
            }
            return _valueFilter;
        }
        #endregion

        #region ConditiionFilter
        private ConditionFilterSetting _conditionFilter;
        private ConditionFilterSetting _GetConditionFilter()
        {
            if (_conditionFilter == null)
            {
#if !MODEL
                _conditionFilter = new ConditionFilterSetting(Helper);
#else
                _conditionFilter = new ConditionFilterSetting();
#endif
            }
            return _conditionFilter;
        }
        #endregion

        #region DataMap
        private DataMap _dataMap;
        private DataMap _GetDataMap()
        {
            if (_dataMap == null)
            {
#if !MODEL
                _dataMap = new DataMap(Helper);
#else
                _dataMap = new DataMap();
#endif
            }
            return _dataMap;
        }
        #endregion
    }

    partial class ValueFilterSetting
    {
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="ValueFilterSetting"/> instance.
        /// </summary>
        public ValueFilterSetting() : this(null) { }

        /// <summary>
        /// Creates one <see cref="ValueFilterSetting"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        public ValueFilterSetting(HtmlHelper helper)
        {
            _helper = helper;
            Initialize();
        }

        /// <summary>
        /// Gets or sets the htmlhelper object.
        /// </summary>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal HtmlHelper Helper
        {
            get { return _helper; }
        }
#else
        /// <summary>
        /// Creates one <see cref="ValueFilterSetting"/> instance.
        /// </summary>
        public ValueFilterSetting()
        {
            Initialize();
        }
#endif

        #region DataMap
        private DataMap _dataMap;
        private DataMap _GetDataMap()
        {
            if (_dataMap == null)
            {
#if !MODEL
                _dataMap = new DataMap(Helper);
#else
                _dataMap = new DataMap();
#endif
            }
            return _dataMap;
        }
        #endregion
    }

    partial class ConditionFilterSetting
    {
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="ConditionFilterSetting"/> instance.
        /// </summary>
        public ConditionFilterSetting() : this(null) { }

        /// <summary>
        /// Creates one <see cref="ConditionFilterSetting"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        public ConditionFilterSetting(HtmlHelper helper)
        {
            _helper = helper;
            Initialize();
        }

        /// <summary>
        /// Gets or sets the htmlhelper object.
        /// </summary>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal HtmlHelper Helper
        {
            get { return _helper; }
        }
#else
        /// <summary>
        /// Creates one <see cref="ConditionFilterSetting"/> instance.
        /// </summary>
        public ConditionFilterSetting()
        {
            Initialize();
        }
#endif

        #region DataMap
        private DataMap _dataMap;
        private DataMap _GetDataMap()
        {
            if (_dataMap == null)
            {
#if !MODEL
                _dataMap = new DataMap(Helper);
#else
                _dataMap = new DataMap();
#endif
            }
            return _dataMap;
        }
        #endregion
    }
}
