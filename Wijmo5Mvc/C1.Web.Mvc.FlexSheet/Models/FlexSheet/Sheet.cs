﻿using C1.Web.Mvc.Grid;
using System.Collections.Generic;
using System.ComponentModel;
#if MODEL
using C1.Web.Mvc.Serialization;
#endif
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc.Sheet
{
    public abstract partial class Sheet
    {
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="Sheet"/> instance.
        /// </summary>
        protected Sheet() : this(null) { }

        /// <summary>
        /// Creates one <see cref="Sheet"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        protected Sheet(HtmlHelper helper)
        {
            _helper = helper;
            Initialize();
        }

        /// <summary>
        /// Gets or sets the htmlhelper object.
        /// </summary>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal HtmlHelper Helper
        {
            get { return _helper; }
        }
#else
        /// <summary>
        /// Creates one <see cref="Sheet"/> instance.
        /// </summary>
        protected Sheet()
        {
            Initialize();
        }
#endif

        #region SelectionRanges
        private IList<CellRange> _selectionRanges;
        private IList<CellRange> _GetSelectionRanges()
        {
            return _selectionRanges ?? (_selectionRanges = new List<CellRange>());
        }
        #endregion SelectionRanges

        #region Tables
        private IList<Table> _tables;
        private IList<Table> _GetTables()
        {
            return _tables ?? (_tables = new List<Table>());
        }
        #endregion

        #region Filter
        private FilterSetting _filter;
        private FilterSetting _GetFilter()
        {
            if (_filter == null)
            {
#if !MODEL
                _filter = new FilterSetting(Helper);
#else
                _filter = new FilterSetting();
#endif
            }
            return _filter;
        }
        #endregion

        /// <summary>
        /// Gets the Sheet type, use for json serialization.
        /// </summary>
#if MODEL
        [C1Ignore]
        [Browsable(false)]
#endif
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual string Type
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}
