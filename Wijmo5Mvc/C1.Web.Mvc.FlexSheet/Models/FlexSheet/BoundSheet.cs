﻿using System.ComponentModel;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc.Sheet
{
    public partial class BoundSheet<T>
    {
#if !MODEL
        /// <summary>
        /// Creates one <see cref="BoundSheet{T}"/> instance.
        /// </summary>
        public BoundSheet() : this(null) { }

        /// <summary>
        /// Creates one <see cref="BoundSheet{T}"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        public BoundSheet(HtmlHelper helper) : base(helper)
        {
            Initialize();
        }
#else
        /// <summary>
        /// Creates one <see cref="BoundSheet{T}"/> instance.
        /// </summary>
        public BoundSheet()
        {
            Initialize();
        }
#endif

        #region ItemsSource
        private IItemsSource<T> _itemsSource;
        private IItemsSource<T> _GetItemsSource()
        {
#if !MODEL
            if (_itemsSource == null && Helper != null)
            {
                _itemsSource = new CollectionViewService<T>(Helper);
                _itemsSource.DisableServerRead = true;
            }
#else
            if (_itemsSource == null)
            {
                _itemsSource = new CollectionViewService<T>();
                _itemsSource.DisableServerRead = true;
            }
#endif
            return _itemsSource;
        }
        private void _SetItemsSource(IItemsSource<T> value)
        {
            _itemsSource = value;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        private bool _ShouldSerializeItemsSource()
        {
#if !MODEL
            return string.IsNullOrEmpty(ItemsSourceId);
#else
            return ItemsSourceBinding.IsItemsSourceDirty(DbContextType, ModelType);
#endif
        }
        #endregion ItemsSource
    }
}
