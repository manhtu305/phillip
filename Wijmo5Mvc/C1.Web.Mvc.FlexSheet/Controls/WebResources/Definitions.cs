﻿using C1.Web.Mvc.WebResources;

[assembly: AssemblyScripts(typeof(C1.Web.Mvc.Sheet.WebResources.Definitions.All))]
[assembly: AssemblyStyles(typeof(C1.Web.Mvc.Sheet.WebResources.Definitions.All))]

namespace C1.Web.Mvc.Sheet.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Definitions
    {
        [Scripts(typeof(FlexSheet), typeof(FormulaBar))]
        [Styles(typeof(FlexSheet), typeof(FormulaBar))]
        public abstract class All { }

        [Scripts(typeof(GridExDefinitions.Filter),
            FlexSheetWebResourcesHelper.WijmoJs + "wijmo.grid.sheet",
            FlexSheetWebResourcesHelper.Shared + "Grid.Sheet",
            FlexSheetWebResourcesHelper.Mvc + "Grid.Sheet",
            FlexSheetWebResourcesHelper.Mvc + "Cast.FlexSheet")]
        public abstract class FlexSheet : Mvc.WebResources.Definitions.Grid { }

        /// <summary>
        /// Defines C1 FormulaBar related js and css.
        /// </summary>
        [Scripts(FlexSheetWebResourcesHelper.Shared + "Grid.Sheet.FormulaBar",
            FlexSheetWebResourcesHelper.Mvc + "Grid.Sheet.FormulaBar")]
        public abstract class FormulaBar : Mvc.WebResources.Definitions.Extender
        {
        }
    }
}
