﻿using C1.Web.Mvc.WebResources;
using System;
using System.ComponentModel;

namespace C1.Web.Mvc.Sheet
{
    /// <summary>
    /// Defines C1 FlexSheet related js and css.
    /// </summary>
    [Scripts(typeof(WebResources.Definitions.FlexSheet))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1FlexSheet
    {
    }

    /// <summary>
    /// Defines C1 FormulaBar related js and css.
    /// </summary>
    [Scripts(typeof(WebResources.Definitions.FormulaBar))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1FormulaBar
    {
    }
}
