﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.Sheet
{
    [Obsolete("Please use Scripts control instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
#if ASPNETCORE
    public
#else
    internal 
#endif
        sealed class FlexSheetWebResourcesManager : WebResourcesManagerBase
    {
        internal static readonly IList<Type> AllControlTypes = new List<Type>(new[]
        {
            typeof(C1FlexSheet),
            typeof(C1FormulaBar)
        });

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the FlexSheetWebResourcesManager class.
        /// </summary>
        /// <param name="helper">A instance of HtmlHelper</param>
        public FlexSheetWebResourcesManager(HtmlHelper helper)
            : base(helper)
        {
        }
        #endregion Ctors

        #region Properties
        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        //protected override string WebResourcesControllerName
        //{
        //    get { return "C1WebMvcFlexSheet"; }
        //}
        #endregion Properties

        #region Methods
        internal override IList<Type> GetAllControlTypes()
        {
            return AllControlTypes;
        }
        #endregion Methods
    }
}