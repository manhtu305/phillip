﻿using C1.Web.Mvc.WebResources;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace C1.Web.Mvc.Sheet
{
    internal static class FlexSheetWebResourcesHelper
    {
        private const string FOLDER_SEPARATOR = ".";

#if !ASPNETCORE
        public const string BASE_ROOT = "C1.Web.Mvc.FlexSheet";
#else
        public const string BASE_ROOT = "C1.AspNetCore.Mvc.FlexSheet";
#endif

        public const string ROOT = BASE_ROOT + FOLDER_SEPARATOR + "Client" +
#if DEBUG
            "Debug"
#else
            "Release"
#endif
            + FOLDER_SEPARATOR;

        public const string Shared = ROOT + "Shared" + FOLDER_SEPARATOR;
        public const string Mvc = ROOT + "Mvc" + FOLDER_SEPARATOR;
        public const string Wijmo = ROOT + "Wijmo" + FOLDER_SEPARATOR;
        public const string WijmoJs = Wijmo + "controls" + FOLDER_SEPARATOR;

        public static readonly Lazy<IEnumerable<Type>> AllScriptOwnerTypes = new Lazy<IEnumerable<Type>>(
            () => WebResourcesHelper.GetAssemblyResTypes<AssemblyScriptsAttribute>(typeof(FlexSheetWebResourcesHelper).Assembly()));
    }
}