﻿using C1.JsonNet;
using System;
using System.IO;
using System.Collections;
#if ASPNETCORE
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
#else
using System.Web.Mvc;
using HttpRequest = System.Web.HttpRequestBase;
#endif

namespace C1.Web.Mvc.Sheet
{
    /// <summary>
    /// Defines the model binder for FlexSheet form-request.
    /// </summary>
    public class FlexSheetRequestModelBinder : ModelBinderBase
    {
        private const string REQUEST_KEY = "flexSheetRequest";

        /// <summary>
        /// Gets the request content which is used for deserializing.
        /// </summary>
        /// <param name="request">The http request.</param>
        /// <returns>The content.</returns>
        protected override string GetRequestContent(HttpRequest request)
        {
            var paramStrings = request.
#if ASPNETCORE
            Form
#else
            Params
#endif
            ;

            return paramStrings[REQUEST_KEY];
        }

        private static FlexSheetSaveRequest DeserializeSaveRequest(string requestContent)
        {
            JsonSetting setting = new JsonSetting { IsPropertyNameCamel = true };
            FlexSheetSaveRequest saveReq = JsonHelper.DeserializeObject<FlexSheetSaveRequest>(requestContent, setting);
            using (StringReader sr = new StringReader(requestContent.FromHexUnicode()))
            {
                JsonReader reader = new JsonReader(sr, setting);
                Hashtable result = reader.ReadValue() as Hashtable;
                if (result != null)
                {
                    string base64 = result["file"] as string;
                    if (base64 != null)
                    {
                        byte[] byteArray = Convert.FromBase64String(base64);
                        saveReq.SetFileStream(new MemoryStream(byteArray));
                    }
                }
            }

            return saveReq;
        }

        /// <summary>
        /// Deserializes the model.
        /// </summary>
        /// <param name="text">The string used to deserialize.</param>
        /// <param name="modelType">The type of the model.</param>
        /// <returns>The model.</returns>
        protected override object DeserializeModel(string text, Type modelType)
        {
            return DeserializeSaveRequest(text);
        }
    }

#if !ASPNETCORE
    /// <summary>
    /// Represents an attribute that invokes a model binder for FlexSheet request.
    /// </summary>
    public class FlexSheetRequestAttribute : CustomModelBinderAttribute
    {
        /// <summary>
        /// Retrieves the associated model binder.
        /// </summary>
        /// <returns>A reference to an object that implements the System.Web.Mvc.IModelBinder interface</returns>
        public override IModelBinder GetBinder()
        {
            return new FlexSheetRequestModelBinder();
        }
    }
#else
    /// <summary>
    /// Represents an attribute that invokes a model binder for FlexSheet request.
    /// </summary>
    public class FlexSheetRequestAttribute : ModelBinderAttribute
    {
        public FlexSheetRequestAttribute()
        {
            BinderType = typeof(FlexSheetRequestModelBinder);
        }
    }
#endif
}