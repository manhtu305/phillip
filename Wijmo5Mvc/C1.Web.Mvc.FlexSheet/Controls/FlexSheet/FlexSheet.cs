﻿using System.Linq;
using System;
using C1.Web.Mvc.WebResources;
using System.Collections.Generic;
#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
#endif

namespace C1.Web.Mvc.Sheet
{
    [Scripts(typeof(WebResources.Definitions.FlexSheet))]
    public partial class FlexSheet
    {
        private const string CLIENT_MODULE_FLEXSHEET = "grid.sheet.";

        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        internal override string ClientSubModule
        {
            get
            {
                return CLIENT_MODULE_FLEXSHEET;
            }
        }

        /// <summary>
        /// Override to render the extender startup scripts.
        /// </summary>
        /// <param name="writer">The specified writer</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            var itemSources = AppendedSheets.OfType<BoundSheet<object>>().Where(b => b.ShouldSerializeItemsSource()).Select(b => b.ItemsSource)
                .Union(AppendedSheets.SelectMany(s => s.Tables).Where(t => t.ShouldSerializeItemsSource()).Select(t => t.ItemsSource))
                .Union(AppendedSheets.SelectMany(s => s.Filter.ColumnFilters).SelectMany(f =>
                {
                    var dataMaps = new List<DataMap>();
                    dataMaps.Add(f.DataMap);
                    dataMaps.Add(f.ValueFilter.DataMap);
                    dataMaps.Add(f.ConditionFilter.DataMap);
                    return dataMaps;
                }).Where(d => d.ShouldSerializeItemsSource()).Select(d=>d.ItemsSource));

            itemSources.ToList().ForEach(itemsSource=>
            {
                var cv = itemsSource as BaseCollectionViewService<object>;
                if (cv != null)
                {
                    cv.RenderScripts(writer, false);
                }
            });

            base.RegisterStartupScript(writer);
        }

        /// <summary>
        /// Ensure that all child components have been added.
        /// </summary>
        protected override void CreateChildComponents()
        {
            AppendedSheets.OfType<BoundSheet<object>>().Where(boundSheet => boundSheet.ItemsSource != null)
                .ToList().ForEach(boundSheet =>
                {
                    var cv = boundSheet.ItemsSource as CollectionViewService<object>;
                    if (cv != null)
                    {
                        Components.Add(cv);
                    }
                });

            base.CreateChildComponents();
        }
    }
}
