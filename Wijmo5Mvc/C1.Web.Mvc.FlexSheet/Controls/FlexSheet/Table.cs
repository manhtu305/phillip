﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Sheet
{
    partial class Table : IItemsSourceContainer<object>
    {
        #region IItemsSourceContainer
        internal TDataSource GetDataSource<TDataSource>()
            where TDataSource : BaseCollectionViewService<object>
        {
            return BaseCollectionViewService<object>.GetDataSource<TDataSource>(this, _helper);
        }
        #endregion
    }
}
