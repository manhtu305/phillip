﻿using System.IO;

namespace C1.Web.Mvc.Sheet
{
    public partial class FlexSheetSaveRequest
    {
        #region FileStream
        private Stream _fileStream;
        internal void SetFileStream(Stream value) {
            if (value != null) {
                _fileStream = value;
            }
        }

        /// <summary>
        /// Gets the Stream value of xlsx file.
        /// </summary>
        /// <returns>The Stream value</returns>
        public Stream GetFileStream() {
            return _fileStream;
        }
        #endregion FileStream
    }
}
