﻿using System.IO;

namespace C1.Web.Mvc.Sheet
{
    /// <summary>
    /// Supports the load processing of the request from the client-side FlexSheet.
    /// </summary>
    public static class FlexSheetHelper
    {
        /// <summary>
        /// Load data from the FileStream instance.
        /// </summary>
        /// <param name="fileStream">the FileStream instance</param>
        /// <returns>The response data</returns>
        public static FlexSheetLoadResponse Load(Stream fileStream)
        {
            return new FlexSheetLoadResponse { FileStream = fileStream };
        }

        /// <summary>
        /// Load data from the xlsx file.
        /// </summary>
        /// <param name="filePath">The path of xlsx file</param>
        /// <returns>The response data</returns>
        public static FlexSheetLoadResponse Load(string filePath)
        {
            return new FlexSheetLoadResponse { FilePath = filePath };
        }

        /// <summary>
        /// Load data from the Workbook instance.
        /// </summary>
        /// <param name="workbook">The Workbook instance</param>
        /// <returns>The response data</returns>
        public static FlexSheetLoadResponse Load(Workbook workbook)
        {
            return new FlexSheetLoadResponse { Workbook = workbook };
        }

        /// <summary>
        /// Gets the FlexSheetSaveResponse instance with save result.
        /// </summary>
        /// <param name="success">Whether the operation is success</param>
        /// <param name="error">The error message</param>
        /// <returns>The response data</returns>
        public static FlexSheetSaveResponse Save(bool success = true, string error = "")
        {
            return new FlexSheetSaveResponse { Success = success, Error = error };
        }
    }
}
