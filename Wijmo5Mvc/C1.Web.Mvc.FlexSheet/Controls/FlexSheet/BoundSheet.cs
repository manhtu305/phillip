﻿using C1.Web.Mvc.Serialization;
using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Mvc.Sheet
{
    partial class BoundSheet<T> : IItemsSourceContainer<T>
    {
        #region SortDescriptions
        private IList<SortDescription> _sortDescriptions;
        /// <summary>
        /// Gets the sort descriptions.
        /// </summary>
        [C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual IList<SortDescription> SortDescriptions
        {
            get { return _sortDescriptions ?? (_sortDescriptions = new List<SortDescription>()); }
        }

        internal void AddSortDescriptions(IEnumerable<SortDescription> sds)
        {
            if (sds != null)
            {
                foreach (var sd in sds)
                {
                    SortDescriptions.Add(sd);
                    ItemsSource.SortDescriptions.Add(sd);
                }
            }
        }
        #endregion SortDescriptions

        #region GroupDescriptions
        private IList<GroupDescription> _groupDescriptions;
        /// <summary>
        /// Gets the group descriptions.
        /// </summary>
        [C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual IList<GroupDescription> GroupDescriptions
        {
            get { return _groupDescriptions ?? (_groupDescriptions = new List<GroupDescription>()); }
        }

        internal void AddGroupDescriptions(IEnumerable<GroupDescription> gds)
        {
            if (gds != null)
            {
                foreach (var gd in gds)
                {
                    GroupDescriptions.Add(gd);
                    ItemsSource.GroupDescriptions.Add(gd);
                }
            }
        }
        #endregion GroupDescriptions

        #region PageSize
        /// <summary>
        /// Make ItemsSource pagable with specified page size. 0 means to disable paging.
        /// </summary>
        [Serialization.C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual int PageSize
        {
            get;
            set;
        }
        #endregion PageSize

        internal TDataSource GetDataSource<TDataSource>()
            where TDataSource : BaseCollectionViewService<T>
        {
            return BaseCollectionViewService<T>.GetDataSource<TDataSource>(this, Helper);
        }
    }
}
