﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Sheet
{
    /// <summary>
    /// The response for Flexsheet remote save.
    /// </summary>
    public partial class FlexSheetSaveResponse : OperationResult
    {
        /// <summary>
        /// Initializes a new instance of <see cref="C1.Web.Mvc.Sheet.FlexSheetSaveResponse"/>
        /// </summary>
        public FlexSheetSaveResponse()
        {
        }
    }
}
