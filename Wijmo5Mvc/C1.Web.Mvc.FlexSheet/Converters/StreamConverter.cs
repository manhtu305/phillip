﻿using C1.Web.Mvc.Localization;
using System;
using System.IO;
#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.JsonNet.Converters
{
    /// <summary>
    /// Define a converter which converts the Stream to Base64String and Base64String to Stream.
    /// </summary>
    public class StreamConverter : JsonConverter
    {
        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
#if !ASPNETCORE
            return typeof(Stream).IsAssignableFrom(objectType);
#else
            return typeof(Stream).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo());
#endif
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value to be serialized.</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            if (value == null)
            {
                writer.WriteValue(null);
                return;
            }

            if (value is Stream)
            {
                var stream = value as Stream;
                WriteStream(writer, stream);
                stream.Dispose();
            }
            else
            {
                throw new FormatException(Resources.ConverterValueCanNotBeSerialized);
            }
        }

        private static void WriteStream(JsonWriter writer, Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            using (BinaryReader br = new BinaryReader(stream))
            {
                byte[] byteArray = br.ReadBytes(Convert.ToInt32(stream.Length));
                string base64String = Convert.ToBase64String(byteArray);
                writer.WriteValue(base64String);
            }
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <returns>The object value after deserializing.</returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existingValue)
        {
            if (reader.Current == null)
            {
                return null;
            }

            if (reader.Current is string)
            {
                string base64 = (string)reader.Current;
                byte[] byteArray = Convert.FromBase64String(base64);
                Stream stream = new MemoryStream(byteArray);
                return stream;
            }

            throw new FormatException(Resources.ConverterValueCanNotBeDeserialized);
        }
    }
}
