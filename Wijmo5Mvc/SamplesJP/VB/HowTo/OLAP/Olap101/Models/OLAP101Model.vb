﻿Public Class OLAP101Model
    Public Property ControlId() As String
        Get
            Return m_ControlId
        End Get
        Set
            m_ControlId = Value
        End Set
    End Property
    Private m_ControlId As String
    Public Property Settings() As IDictionary(Of String, Object())
        Get
            Return m_Settings
        End Get
        Set
            m_Settings = Value
        End Set
    End Property
    Private m_Settings As IDictionary(Of String, Object())
    Public Property DefaultValues() As IDictionary(Of String, Object)
        Get
            Return m_DefaultValues
        End Get
        Set
            m_DefaultValues = Value
        End Set
    End Property
    Private m_DefaultValues As IDictionary(Of String, Object)
    Public Property Data() As IEnumerable(Of ProductData)
        Get
            Return m_Data
        End Get
        Set
            m_Data = Value
        End Set
    End Property
    Private m_Data As IEnumerable(Of ProductData)

    Public Sub New()
    End Sub
End Class
