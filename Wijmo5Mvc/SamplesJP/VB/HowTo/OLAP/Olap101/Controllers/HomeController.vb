﻿Imports C1.Web.Mvc.Olap

Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Dim model As New OLAP101Model()
        model.Settings = GetSettings()
        model.Data = ProductData.GetData(10000)
        Return View(model)
    End Function

    Private Function GetSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
            {"RowTotals", New Object() {ShowTotals.Subtotals.ToString(), ShowTotals.None.ToString(), ShowTotals.GrandTotals.ToString()}},
            {"ColumnTotals", New Object() {ShowTotals.Subtotals.ToString(), ShowTotals.None.ToString(), ShowTotals.GrandTotals.ToString()}},
            {"ShowZeros", New Object() {False.ToString(), True.ToString()}},
            {"TotalsBeforeData", New Object() {False.ToString(), True.ToString()}},
            {"ChartType", New Object() {PivotChartType.Column.ToString(), PivotChartType.Area.ToString(), PivotChartType.Bar.ToString(), PivotChartType.Line.ToString(), PivotChartType.Pie.ToString(), PivotChartType.Scatter.ToString()}}
    }
        Return settings
    End Function

End Class
