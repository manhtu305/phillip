﻿Imports System.Web.Http
Imports System.Web.Optimization
Imports System.Web.Mvc
Imports System.Web.Routing


Public Class WebApiApplication
    Inherits System.Web.HttpApplication
    Private ReadOnly config As HttpConfiguration = GlobalConfiguration.Configuration
    Sub Application_Start()
        AreaRegistration.RegisterAllAreas()
        'WebApiConfig.RegisterWebApi(config)
        GlobalConfiguration.Configure(AddressOf WebApiConfig.Register)
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
        config.EnsureInitialized()
    End Sub
End Class
