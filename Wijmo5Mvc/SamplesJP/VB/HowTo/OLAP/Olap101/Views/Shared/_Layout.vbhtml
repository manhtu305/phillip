﻿<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <title>@ViewBag.Title</title>
    <link rel="icon" href="~/Content/images/favicon.png" type="image/png" />
    <link rel="shortcut icon" href="~/Content/images/favicon.ico" type="image/x-icon" />
    @Html.C1().Styles().Theme("Default")
    <!-- styles -->
    <link rel = "stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    <link rel = "stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="~/Content/css/app.css" />
    <link rel="stylesheet" href="~/Content/css/explorer.css" />
    @Html.C1().Scripts().Basic().Olap()

    <!-- jQuery And Bootstrap scripts -->
    <script src = "https://code.jquery.com/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src = "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- App scripts -->
    @Scripts.Render("~/bundles/app")
</head>
<body>
    <div Class="hide">
        @Html.Partial("_SiteNav", True)
    </div>
    <header>
        <div Class="hamburger-nav-btn narrow-screen"><span class="icon-bars"></span></div>
        <a href = "https://www.grapecity.co.jp/developer/componentone-studio" Class="logo-container" target="_blank">
            <img src = "/Content/images/c1icon.png" width="46" height="46" alt="ComponentOne Webサイトに移動" />
            <span Class="icon-c1text"></span>
        </a>
        <div Class="task-bar">
            <span Class="semi-bold narrow-screen">MVC OLAP 101</span>
            <span Class="semi-bold wide-screen">ASP.NET MVC Edition OLAP 101</span>
            @Html.Partial("_SiteNav", False)
        </div>
    </header>
    @RenderBody()
    <footer>
        <a href="https://www.grapecity.co.jp/developer" class="c1logo">
            <img src = "/Content/css/images/grapecityLogo.png" />
        </a>
        <p>
            © @DateTime.Now.Year GrapeCity, Inc. All Rights Reserved.<br />
            All product And company names here In may be trademarks Of their respective owners.
        </p>
        @Code
            Dim url = Request.Url
            Dim urlStr = url.OriginalString.Substring(0, url.OriginalString.Length - (If(url.Query Is Nothing, 0, url.Query.Length)))
        End Code
        <a href="https://www.facebook.com/GrapeCity.dev.JP" target="_blank">
            <img src = "/Content/css/images/icons/32/picons36.png" alt="facebook" />
        </a>
        <a href="https://twitter.com/GrapeCity_dev" target="_blank">
            <img src = "/Content/css/images/icons/32/picons33.png" alt="Twitter" />
        </a>
    </footer>
</body>
</html>
