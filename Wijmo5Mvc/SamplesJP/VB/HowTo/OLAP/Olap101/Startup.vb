﻿Imports Owin
Imports Microsoft.Owin

<Assembly: OwinStartupAttribute(GetType(Startup))>
Partial Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        app.UseDataEngineProviders().AddDataEngine("complex10", Function() ProductData.GetData(10000))
    End Sub
End Class
