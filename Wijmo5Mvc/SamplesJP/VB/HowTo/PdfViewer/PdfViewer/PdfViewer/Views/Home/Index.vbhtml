﻿@Code
    ViewBag.Title = "PdfViewer"
End Code

<div class="header ">
    <div class="container">
        <img src="@Url.Content("~/Content/c1logo.png")" alt="ComponentOne ASP.NET MVC" />
        <h1>
            PdfViewer
        </h1>
        <p>
            このページでは、ComponentOne PdfViewerでローカルPDFファイルを表示する方法を示します。
        </p>
    </div>
</div>

<div class="container">
    <div class="sample-page download-link">
        <a href="https://www.grapecity.co.jp/developer/download#net">トライアル版</a>
    </div>
    <!-- はじめに -->
    <div>
                    <h2>はじめに</h2>
        <p>
                    このアプリケーションを使用するための基本的な手順
        </p>
        <ol>
                    <li>スタートアップにディスクストレージプロバイダを登録します。</li>
            <li>ComponentOne Web APIストレージサービスで、ストレージにファイルをアップロードするためのファイル入力を追加します。</li>
            <li>PdfViewerコントロールを作成し、そのFilePathをアップロードされたファイルのパスに設定します。</li>
            <li>別のPDFファイルを表示したりブラウザウィンドウを閉じる場合は、ストレージサービスで、アップロードされたファイルを削除します。</li>
        </ol>
        <div class="row">
            <label>PDFファイルの選択: <input type="file" onchange="uploadPdf(this, '@Url.Content("~")')" accept=".pdf" /></label>
            <label id="message" class="errormessage hidden">(拡張子".pdf"のファイルを選択してください。)</label>
        </div>
        <div class="row">
            @Html.C1().PdfViewer().Id("pdfViewer").Width("100%")
        </div>
    </div>
</div>