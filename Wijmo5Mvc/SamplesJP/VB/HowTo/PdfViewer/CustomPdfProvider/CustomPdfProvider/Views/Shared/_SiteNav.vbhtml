﻿@ModelType Boolean
@Code
    Dim screenCss = If(Model, "narrow-screen", "wide-screen")
End Code
<ul class="site-nav @screenCss">
    <li>
        <a href="https://www.grapecity.co.jp/developer/componentone-studio/aspnet-mvc" target="_blank">製品情報</a>
    </li>
    <li>
        <a href="https://www.grapecity.co.jp/developer/support" target="_blank">サポート</a>
    </li>
    <li>
        <a href="https://www.grapecity.co.jp/developer/purchase" target="_blank">ご購入</a>
    </li>
    <li class="bold">
        <a href="https://www.grapecity.co.jp/developer/download#net" target="_blank">トライアル版</a>
    </li>
</ul>

