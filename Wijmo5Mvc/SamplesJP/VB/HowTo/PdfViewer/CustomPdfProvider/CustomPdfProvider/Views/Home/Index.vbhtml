@Code
    ViewBag.Title = "カスタムストレージプロバイダー"
End Code

<div Class="container">
    <!-- はじめに -->
    <div>
        <h2>はじめに</h2>
        <p>
            カスタムストレージプロバイダアプリケーションを開始する手順。
        </p>
        <ol>
            <li>ローカルPDFファイルまたはカスタマイズされたPDFドキュメントをストリームとして取得するカスタムストレージを実装します。</li>
            <li>上記のカスタムストレージを提供するカスタムストレージプロバイダを実装します。</li>
            <li>Startupにこのプロバイダを登録します。</li>
            <li>PdfViewerコントロールを作成し、FilePathプロパティを設定します。</li>
        </ol>
        <div Class="row">
            PDFファイルを選択:
            <span>
                <select id="pdfFiles">
                    <option value="CustomPdf/DefaultDocument.pdf" selected="selected">DefaultDocument.pdf</option>
                    <option value="CustomPdf/TextFlow.pdf">TextFlow.pdf</option>
                    <option value="CustomPdf/TextPosition.pdf">TextPosition.pdf</option>
                </select>
            </span>
        </div>
        <div Class="row">
            @(Html.C1().PdfViewer() _
                                .Id("PdfViewer") _
                                .Width("100%") _
                                .FilePath("CustomPdf/DefaultDocument.pdf"))
        </div>
    </div>
</div>