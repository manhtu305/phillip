﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title - ComponentOne ASP.NET MVC Project</title>
    <link rel="stylesheet" href="~/Content/css/bootstrap.min.css" />
    <link rel="stylesheet" href="~/Content/css/app.css" />
    <link rel="stylesheet" href="~/Content/css/explorer.css" />
    <link rel="stylesheet" href="~/Content/css/site.css" />
    @Html.C1().Styles()
    <!-- scripts -->
    <script src="~/Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="~/Scripts/bootstrap.min.js" type="text/javascript"></script>
    @Html.C1().Scripts().Basic(Sub(b) b.Input()).FlexViewer()
    <script src="~/Scripts/app.js" type="text/javascript"></script>
    <script src="~/Scripts/MultiLevelMenu.js" type="text/javascript"></script>
</head>
<body>
    <div class="hide">
        @Html.Partial("_SiteNav", True)
    </div>
    <header>
        <div class="hamburger-nav-btn narrow-screen"><span class="icon-bars"></span></div>
        <a href="https://www.grapecity.co.jp/developer/componentone-studio" class="logo-container" target="_blank">
            <img src="~/Content/images/c1icon.png" width="46" height="46" alt="ComponentOne Webサイトに移動" />
            <span class="icon-c1text"></span>
        </a>

        <div class="task-bar">
            <span class="semi-bold narrow-screen">カスタムストレージプロバイダー</span>
            <span class="semi-bold wide-screen">カスタムストレージプロバイダー</span>
            @Html.Partial("_SiteNav", False)
        </div>
    </header>
    <div class="container">
        @RenderBody()
    </div>

    <footer>
        <a href="https://www.grapecity.co.jp/developer" class="c1logo">
            <img src="~/Content/css/images/grapecityLogo.png" />
        </a>
        <p>
            © @DateTime.Now.Year GrapeCity, Inc. All Rights Reserved.<br />
            All product and company names here in may be trademarks of their respective owners.
        </p>
        @Code
            Dim url = Request.Url
            Dim urlStr = url.OriginalString.Substring(0, url.OriginalString.Length - (If(url.Query Is Nothing, 0, url.Query.Length)))
        End Code
        <a href="https://www.facebook.com/GrapeCity.dev.JP" target="_blank">
            <img src="~/Content/css/images/icons/32/picons36.png" alt="facebook" />
        </a>
        <a href="https://twitter.com/GrapeCity_dev" target="_blank">
            <img src="~/Content/css/images/icons/32/picons33.png" alt="Twitter" />
        </a>
    </footer>

    <!-- Google analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-208280-14', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>
