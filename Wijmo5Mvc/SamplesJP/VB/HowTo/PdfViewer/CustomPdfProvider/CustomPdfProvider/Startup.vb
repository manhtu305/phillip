﻿Imports System
Imports Owin
Imports Microsoft.Owin
Imports System.IO

<Assembly: OwinStartupAttribute(GetType(Startup))>

Partial Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        app.UseStorageProviders.Add("CustomPdf", New CustomPdfStorageProvider())
    End Sub
End Class
