﻿@Imports FlexChart101
@ModelType FlexChartModel 

@Code
    ViewBag.Title = "FlexChart入門"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="header">
    <div class="container">
        <img src="@Url.Content("~/Content/c1logo.png")" alt="Component One ASP.Net MVC" />
        <h1>
            FlexChart入門
        </h1>
        <p>
            このページでは、FlexChartコントロールの使用を開始する方法を説明します。
        </p>
    </div>
</div>

<div class="container">
    <div class="sample-page download-link">
        <a href="https://www.grapecity.co.jp/developer/download#net">トライアル版</a>
    </div>
    <!-- はじめに -->
    <div>
        <h2>はじめに</h2>
        <p>
            MVCアプリケーションでFlexChartコントロールの使用を開始する際の手順。
        </p>
        <ol>
            <li>ComponentOne ASP.NET MVCアプリケーションテンプレートを使用して、新しいMVCプロジェクトを作成します。</li>
            <li>プロジェクトに、コントローラおよび対応するビューを追加します。</li>
            <li>razor構文を使用して、ビューのChartコントロールを初期化します。</li>
            <li>（オプション）CSSを追加して、FlexChartコントロールの外観をカスタマイズします。</li>
        </ol>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#gsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#gsCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#gsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="gsHtml">

&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;

    &lt;/head&gt;
    &lt;body&gt;

        &lt;!-- this is the FlexChart --&gt;
        @@(Html.C1().FlexChart().Id("gettingStartedChart").Bind(Model.CountrySalesData).BindingX("Country") _
            .Series(Sub(sers)
                        sers.Add().Binding("Sales").Name("Sales")
                        sers.Add().Binding("Expenses").Name("Expenses")
                        sers.Add().Binding("Downloads").Name("Downloads")
                    End Sub)
        )

    &lt;/body&gt;
&lt;/html&gt;

                        </div>
                        <div class="tab-pane pane-content" id="gsCss">

.wj-flexchart {
    background-color: white;
    box-shadow: 4px 4px 10px 0px rgba(50, 50, 50, 0.75);
    height: 400px;
    margin-bottom: 12px;
    padding: 8px;
}

                        </div>
                        <div class="tab-pane pane-content" id="gsCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function    
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexChart().Id("gettingStartedChart").Bind(Model.CountrySalesData).BindingX("Country") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
            </div>
        </div>
    </div>


    <!-- chart types -->
    <div>
        <h2>チャートタイプ</h2>
        <p>FlexChartコントロールには、チャートタイプをカスタマイズするための３つのプロパティがあります。</p>
        <ol>
            <li>
                <b>ChartType</b>: すべての系列で使用されるデフォルトのチャートタイプを選択します。
                個々の系列では、これをオーバーライドできます。
            </li>
            <li>
                <b>Stacking</b>: 系列のプロット方法が個別、積層、合計が100%になる積層のどれであるかを決定します。
            </li>
            <li>
                <b>Rotated</b>: X軸とY軸を反転して、Xが縦軸、Yが横軸になるようにします。
            </li>
        </ol>
        <p>次の例は、これらのプロパティを変更するとどうなるかを示します。</p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#ctHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#ctJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#ctCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="ctHtml">

@@(Html.C1().FlexChart().Id("chartTypes").Bind(Model.CountrySalesData).BindingX("Country") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
)

&lt;label class="col-md-3 control-label"&gt;Chart Type&lt;/label&gt;
@@(Html.C1().ComboBox().Id("typeMenu").Bind(Model.Settings("ChartType")) _
    .SelectedValue("Column").OnClientSelectedIndexChanged("typeMenu_SelectedIndexChanged")
)

&lt;label class="col-md-3 control-label"&gt;Stacking&lt;/label&gt;
@@(Html.C1().ComboBox().Id("stackingMenu").Bind(Model.Settings("Stacking")) _
    .SelectedValue("None").OnClientSelectedIndexChanged("stackingMenu_SelectedIndexChanged")
)

&lt;label class="col-md-3 control-label"&gt;Rotated&lt;/label&gt;
@@(Html.C1().ComboBox().Id("rotatedMenu").Bind(Model.Settings("Rotated")) _
    .SelectedValue("False").OnClientSelectedIndexChanged("rotatedMenu_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="ctJs">

//Chart Types Module
function typeMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        var chartTypes = &lt;wijmo.chart.FlexChart&gt; wijmo.Control.getControl("#chartTypes");
        chartTypes.chartType = sender.selectedValue;
    }
}

function stackingMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        var chartTypes = &lt;wijmo.chart.FlexChart&gt; wijmo.Control.getControl("#chartTypes");
        chartTypes.stacking = parseInt(sender.selectedIndex);
    }
}

function rotatedMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        var chartTypes = &lt;wijmo.chart.FlexChart&gt;wijmo.Control.getControl("#chartTypes");
        chartTypes.rotated = sender.selectedValue == 'True' ? true : false;
    }
}


                        </div>
                        <div class="tab-pane pane-content" id="ctCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.Settings = CreateIndexSettings()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function

    Private Function CreateIndexSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
            {"ChartType", New Object() {"Column", "Bar", "Scatter", "Line", "LineSymbols", "Area",
                "Spline", "SplineSymbols", "SplineArea"}},
            {"Stacking", New Object() {"None", "Stacked", "Stacked 100%"}},
            {"Rotated", New Object() {False.ToString(), True.ToString()}}            
        }

        Return settings
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexChart().Id("chartTypes").Bind(Model.CountrySalesData).BindingX("Country") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">チャートタイプ</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("typeMenu").Bind(Model.Settings("ChartType")) _
        .SelectedValue("Column").OnClientSelectedIndexChanged("typeMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">積層</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("stackingMenu").Bind(Model.Settings("Stacking")) _
.SelectedValue("None").OnClientSelectedIndexChanged("stackingMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">回転</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("rotatedMenu").Bind(Model.Settings("Rotated")) _
.SelectedValue("False").OnClientSelectedIndexChanged("rotatedMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- mixed chart types -->
    <div>
        <h2>複合チャートタイプ</h2>
        <p>
            系列自体の<b>ChartType</b>プロパティを設定して、チャート系列ごとに異なるチャートタイプを使用できます。 これは、チャートのデフォルトのチャートタイプをオーバーライドします。
        </p>
        <p>
            次の例では、チャートの<b>ChartType</b>プロパティは<b>Column</b>に設定されていますが、<b>Downloads</b>系列がそれをオーバーライドして、<b>LineSymbols</b>チャートタイプが使用されます。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#mctHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#mctCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="mctHtml">

@@(Html.C1().FlexChart().Id("mixedTypesChart").Bind(Model.CountrySalesData).BindingX("Country") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads").ChartType(C1.Web.Mvc.Chart.ChartType.LineSymbols)
            End Sub)
            )

                        </div>
                        <div class="tab-pane pane-content" id="mctCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function    
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexChart().Id("mixedTypesChart").Bind(Model.CountrySalesData).BindingX("Country") _
        .Series(Sub(sers)
                    sers.Add().Binding("Sales").Name("Sales")
                    sers.Add().Binding("Expenses").Name("Expenses")
                    sers.Add().Binding("Downloads").Name("Downloads").ChartType(C1.Web.Mvc.Chart.ChartType.LineSymbols)
                End Sub)
                )
            </div>
        </div>
    </div>



    <!-- legend and titles -->
    <div>
        <h2>凡例とタイトル</h2>
        <p>
            チャートの汎用の外観をカスタマイズするには、<b>Legend</b>プロパティを使用します。チャートにタイトルを追加するには、<b>Header</b>、<b>Footer</b>、および軸の<b>Title</b>プロパティを使用します。
        </p>
        <p>
            CSSを使用して凡例とタイトルのスタイルを設定できます。 次のCSSタブは、凡例とタイトルの外観のカスタマイズに使用されるルールを示しています。 これらはSVG要素なので、「color」ではなく「fill」などのCSS属性を使用する必要があることに注意してください。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#ltHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#ltJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#ltCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#ltCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="ltHtml">

@@(Html.C1().FlexChart().Id("chartLegendAndTitles").Header("Sample Chart") _
    .Footer("Copyright (c) ComponentOne").Bind(Model.CountrySalesData).BindingX("Country") _
    .AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
                )
&lt;dl class="dl-horizontal"&gt;
    &lt;dt&gt;Header&lt;/dt&gt;&lt;dd&gt;&lt;input id="headerInput" class="form-control"/&gt;&lt;/dd&gt;
    &lt;dt&gt;Footer&lt;/dt&gt;&lt;dd&gt;&lt;input id="footerInput" class="form-control"/&gt;&lt;/dd&gt;
    &lt;dt&gt;X軸のタイトル&lt;/dt&gt;&lt;dd&gt;&lt;input id="xTitleInput" class="form-control"/&gt;&lt;/dd&gt;
    &lt;dt&gt;Y軸のタイトル&lt;/dt&gt;&lt;dd&gt;&lt;input id="yTitleInput" class="form-control"/&gt;&lt;/dd&gt;
    &lt;dt&gt;&lt;/dt&gt;
&lt;dd&gt;
    @@(Html.C1().ComboBox().Id("positionMenu").Bind(Model.Settings("Position")) _
        .SelectedValue("Right").OnClientSelectedIndexChanged("positionMenu_SelectedIndexChanged")
        )
&lt;/dd&gt;
&lt;/dl&gt;

                        </div>
                        <div class="tab-pane pane-content" id="ltJs">

function InitialControls() {
    //Legend & Title Module
    var ltchart = &lt;wijmo.chart.FlexChart&gt;wijmo.Control.getControl("#chartLegendAndTitles");
    var ltHeader = &lt;HTMLInputElement&gt;document.getElementById('headerInput');
    var ltFooter = &lt;HTMLInputElement&gt;document.getElementById('footerInput');
    var ltXTitle = &lt;HTMLInputElement&gt;document.getElementById('xTitleInput');
    var ltYTitle = &lt;HTMLInputElement&gt;document.getElementById('yTitleInput');

    ltHeader.value = 'Sample Chart';
    ltHeader.addEventListener('input', function () {
        ltchart.header = this.value;
    });

    ltFooter.value = 'Copyright (c) ComponentOne';
    ltFooter.addEventListener('input', function () {
        ltchart.footer = this.value;
    });

    ltXTitle.value = 'Country';
    ltXTitle.addEventListener('input', function () {
        ltchart.axisX.title = this.value;
    });

    ltYTitle.value = 'Amount';
    ltYTitle.addEventListener('input', function () {
        ltchart.axisY.title = this.value;
    });
}

//Legend and Title Module
function positionMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        var chart = &lt;wijmo.chart.FlexChart&gt; wijmo.Control.getControl("#chartLegendAndTitles");
        chart.legend.position = parseInt(sender.selectedIndex);
    }
}


                        </div>
                        <div class="tab-pane pane-content" id="ltCss">

.wj-flexchart .wj-title {
    font-weight: bold;
}
.wj-flexchart .wj-header .wj-title {
    fill: #80044d;
    font-size: 18pt;
}
.wj-flexchart .wj-footer .wj-title {
    fill: #80044d;
}
.wj-flexchart .wj-axis-x .wj-title,
.wj-flexchart .wj-axis-y .wj-title {
    font-style: italic;
}

                        </div>
                        <div class="tab-pane pane-content" id="ltCS">


Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.Settings = CreateIndexSettings()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function

    Private Function CreateIndexSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {            
            {"Position", New Object() {Position.None.ToString(), Position.Left.ToString(), Position.Top.ToString(), Position.Right.ToString(), Position.Bottom.ToString()}}            
        }

        Return settings
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexChart().Id("chartLegendAndTitles").Header("Sample Chart") _
        .Footer("Copyright (c) ComponentOne").Bind(Model.CountrySalesData).BindingX("Country") _
        .AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
                )
                <dl class="dl-horizontal">
                    <dt>ヘッダー</dt>
                    <dd><input id="headerInput" class="form-control" /></dd>
                    <dt>フッター</dt>
                    <dd><input id="footerInput" class="form-control" /></dd>
                    <dt>X軸のタイトル</dt>
                    <dd><input id="xTitleInput" class="form-control" /></dd>
                    <dt>Y軸のタイトル</dt>
                    <dd><input id="yTitleInput" class="form-control" /></dd>
                    <dt>凡例</dt>
                    <dd>
                        @(Html.C1().ComboBox().Id("positionMenu").Bind(Model.Settings("Position")) _
.SelectedValue("Right").OnClientSelectedIndexChanged("positionMenu_SelectedIndexChanged")
                        )
                    </dd>
                </dl>
            </div>
        </div>
    </div>



    <!-- tooltips -->
    <div>
        <h2>ツールチップ</h2>
        <p>
            FlexChartでは、ツールチップが組み込みでサポートされています。 ユーザーがデータポイントにタッチするか、その上にカーソルを置くと、コントロールはデフォルトでツールチップを表示します。
        </p>
        <p>
            ツールチップの内容はテンプレートを使用して生成されます。このテンプレートは、次のパラメータを含むことができます。
        </p>
        <ul>
            <li><b>SeriesName</b>: チャート要素を含む系列の名前。</li>
            <li><b>PointIndex</b>: 系列内のチャート要素のインデックス。</li>
            <li><b>AxisX</b>: チャート要素の<b>x</b>値。</li>
            <li><b>AxisY</b>: チャート要素の<b>y</b>値。</li>
        </ul>
        <p>
            デフォルトでは、ツールチップテンプレートは<code>&lt;b&gt;{seriesName}&lt;/b&gt;&lt;br/&gt;{x} {y}</code>に設定され、これがどのように機能するかは上記のチャートで確認できます。
            この例では、ツールチップテンプレートを<code>&lt;b&gt;{seriesName}&lt;/b&gt; &lt;img src='../../Content/images/{x}.png'/&gt;&lt;br/&gt;{y}</code>に設定し、国名を国旗で置き換えます。
        </p>
        <p>チャートのツールチップを無効にできます。それには、テンプレートを空の文字列に設定します。</p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tooltipHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#tooltipCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="tooltipHtml">

@@(Html.C1().FlexChart().Id("chartTooltip").Header("Sample Chart").Footer("Copyright (c) ComponentOne") _
    .Bind(Model.CountrySalesData).BindingX("Country").AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub) _
            .Tooltip(Sub(tt) tt.Content("&lt;img src='../../Content/images/{x}.png'/&gt; &lt;b&gt;{seriesName}&lt;/b&gt;&lt;br /&gt;{y}")) _
)

                        </div>
                        <div class="tab-pane pane-content" id="tooltipCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>                
                @(Html.C1().FlexChart().Id("chartTooltip").Header("Sample Chart").Footer("Copyright (c) ComponentOne") _
        .Bind(Model.CountrySalesData).BindingX("Country").AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub) _
            .Tooltip(Sub(tt) tt.Content("<img src='../../Content/images/{x}.png'/> <b>{seriesName}</b><br />{y}")) _
                )
            </div>
        </div>
    </div>


    <!-- styling series -->
    <div>
        <h2>系列のスタイル設定</h2>
        <p>
            FlexChartはデフォルトのパレットに基づいて系列ごとにカラーを自動的に選択しますが、これはオーバーライドできます。それには、<b>Palette</b>プロパティを設定します。
            しかし、デフォルト設定もオーバーライドできます。それには、任意の系列の<b>Style</b>プロパティを、<b>Fill</b>、<b>Stroke</b>、<b>StrokeThickness</b>などのSVGスタイル設定属性を指定するオブジェクトに指定します。
        </p>
        <p>
            <b>Series.Style</b>プロパティは、MVCコントロール内のすべてのスタイル設定はCSSを使用して行うという原則の例外です。 この例外は、多くのチャートには動的系列があり、事前にスタイル設定を行うのは不可能であるということに基づくものです。 たとえば株式チャートは、アプリケーションの実行中、ユーザーによって選択された系列を表示することがあります。
        </p>
        <p>
            この例のチャートは、<b>Style</b>プロパティと<b>SymbolStyle</b>プロパティを使用して、系列ごとにスタイル属性を選択します。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#seriesStyleHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#seriesStyleCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="seriesStyleHtml">

@@(Html.C1().FlexChart().Id("chartSeriesStyle").Bind(Model.CountrySalesData).BindingX("Country") _
    .AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
        sers.Add().Binding("Sales").Name("Sales").Style(Sub(ss) ss.Fill("green").Stroke("darkgreen").StrokeWidth(1))
        sers.Add().Binding("Expenses").Name("Expenses").Style(Sub(ss) ss.Fill("red").Stroke("darkred").StrokeWidth(1))
        sers.Add().Binding("Downloads").Name("Downloads").ChartType(C1.Web.Mvc.Chart.ChartType.LineSymbols) _
            .Style(Sub(ss)
                        ss.Fill("gold")
                        ss.Stroke("orange")
                        ss.StrokeWidth(5)
                    End Sub)
            End Sub) _
        .Tooltip(Sub(tt) tt.Content("&lt;img src='../../Content/images/{x}.png'/&gt; &lt;b&gt;{seriesName}&lt;/b&gt;&lt;br /&gt;{y}"))
        )

                        </div>
                        <div class="tab-pane pane-content" id="seriesStyleCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>                
                @(Html.C1().FlexChart().Id("chartSeriesStyle").Bind(Model.CountrySalesData).BindingX("Country") _
        .AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales").Style(Sub(ss) ss.Fill("green").Stroke("darkgreen").StrokeWidth(1))
                sers.Add().Binding("Expenses").Name("Expenses").Style(Sub(ss) ss.Fill("red").Stroke("darkred").StrokeWidth(1))
                sers.Add().Binding("Downloads").Name("Downloads") _
                .ChartType(C1.Web.Mvc.Chart.ChartType.LineSymbols).Style(Sub(ss)
                                                                             ss.Fill("gold")
                                                                             ss.Stroke("orange")
                                                                             ss.StrokeWidth(5)
                                                                         End Sub)
            End Sub) _
            .Tooltip(Sub(tt) tt.Content("<img src='../../Content/images/{x}.png'/> <b>{seriesName}</b><br />{y}"))
                )
            </div>
        </div>
    </div>


    <!-- customizing axes -->
    <div>
        <h2>軸のカスタマイズ</h2>
        <p>
            範囲（最小と最大）、ラベル書式、目盛りマークの間隔、グリッド線など、チャートの軸をカスタマイズするには、軸のプロパティを使用します。
        </p>
        <p>
            <b>Axis</b>クラスにはbooleanプロパティがあり、機能をオンまたはオフにできます（<b>AxisLine</b>、<b>MajorTickMarks</b>、および<b>MajorGrid</b>）。
            CSSを使用して、オンにする機能の外観のスタイルを設定できます。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#customizeAxesHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#customizeAxesCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="customizeAxesHtml">

@@(Html.C1().FlexChart().Id("chartCustomizeAxes").Bind(Model.CountrySalesData).BindingX("Country") _
    .AxisX(Sub(x) x.AxisLine(True).MajorGrid(True)).AxisY(Sub(y) y.Format("c0").Max(12000000).MajorUnit(1000000) _
        .AxisLine(True).MajorGrid(True)) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
            End Sub) _
    .Tooltip(Sub(tt) tt.Content("&lt;img src='../../Content/images/{x}.png'/&gt; &lt;b&gt;{seriesName}&lt;/b&gt;&lt;br /&gt;{y}"))
                )

                        </div>
                        <div class="tab-pane pane-content" id="customizeAxesCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>                
                @(Html.C1().FlexChart().Id("chartCustomizeAxes").Bind(Model.CountrySalesData).BindingX("Country") _
    .AxisX(Sub(x) x.AxisLine(True).MajorGrid(True)).AxisY(Sub(y) y.Format("c0").Max(12000000).MajorUnit(1000000) _
                        .AxisLine(True).MajorGrid(True)) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
            End Sub) _
            .Tooltip(Sub(tt) tt.Content("<img src='../../Content/images/{x}.png'/> <b>{seriesName}</b><br />{y}")) _
                )
            </div>
        </div>
    </div>


    <!-- theming -->
    <div>
        <h2>テーマ</h2>
        <p>
            FlexChartの外観はCSSで定義されます。 デフォルトのテーマに加えて、プロのデザインによる12種類ほどのテーマが組み込まれており、すべてのMVCコントロールの外観を統一感のある魅力的な見た目にカスタマイズできます。
        </p>
        <p>
            チャートの外観をカスタマイズするには、スタイル設定する要素を調査し、それらの要素に適用するCSSルールを作成します。
        </p>
        <p>
            たとえば、Webブラウザの開発者ツールでX軸のラベルのDOMを解析すると、それが「wj-label」クラスを持つ要素であること、「wj-axis-x」クラスを持つ要素に含まれていること、その要素は最上位のコントロール要素に含まれていること、その要素は「wj-flexchart」クラスを持つことがわかります。 この例の最初のCSSルールは、この情報を使用してXラベルをカスタマイズします。 ルールセレクタは、親要素が「wj-flexchart」クラス<b>および</b>「custom-flex-chart」クラスを持つ必要があるという追加要件を追加します。 これを使用しない場合は、そのページのすべてのチャートにルールが適用されます。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#themeHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#themeCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#themeCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="themeHtml">

@@(Html.C1().FlexChart().Id("chartTheme").CssClass("custom-flex-chart").Bind(Model.CountrySalesData).BindingX("Country") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="themeCss">

/* custom chart theme */
.custom-flex-chart.wj-flexchart .wj-axis-x .wj-label,
.custom-flex-chart.wj-flexchart .wj-legend .wj-label {
    font-family: 'Courier New', Courier, monospace;
    font-weight: bold;
}

.custom-flex-chart.wj-flexchart .wj-legend > rect,
.custom-flex-chart.wj-flexchart .wj-plot-area >  rect {
    fill: #f8f8f8;
    stroke: #c0c0c0;
}

                        </div>
                        <div id="themeCS" class="tab-pane pane-content">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ） :  </h4>
                @(Html.C1().FlexChart().Id("chartTheme").CssClass("custom-flex-chart").Bind(Model.CountrySalesData).BindingX("Country") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
            </div>
        </div>
    </div>

    <!-- selection modes -->
    <div>
                <h2>選択モード</h2>
        <p>
                FlexChartを使用すると、クリックまたはタッチすることで系列やデータポイントを選択できます。
            <b>SelectionMode</b>プロパティを使用して、系列単位で選択するか、データポイント単位で選択するか、または何も選択しないかを指定します（デフォルトでは選択はオフ）。
        </p>
        <p>
                <b>SelectionMode</b>プロパティを<b>Series</b>または<b>Point</b>に設定すると、ユーザーがマウスをクリックしたとき、FlexChartは<b>Selection</b>プロパティを更新し、選択されたチャート要素に「wj-state-selected」クラスを適用します。
        </p>
        <p>
                例に示すように、<b>Series.collectionView.currentItem</b>プロパティを使用することにより、選択された系列内で現在選択されている項目を取得します。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#selectionModeHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#selectionModeJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#selectionModeCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="selectionModeHtml">

@@(Html.C1().FlexChart().Id("chartSelectionMode").OnClientSelectionChanged("chartSelectionMode_SelectionChanged") _
    .Bind(Model.CountrySalesData).BindingX("Country") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
)

&lt;label class="col-md-3 control-label"&gt;Selection Mode&lt;/label&gt;
@@(Html.C1().ComboBox().Id("selectionModeMenu").Bind(Model.Settings("SelectionMode")) _
    .SelectedValue("None").OnClientSelectedIndexChanged("selectionModeMenu_SelectedIndexChanged")
)
&lt;label class="col-md-3 control-label"&gt;Chart Type&lt;/label&gt;
@@(Html.C1().ComboBox().Id("chartTypeMenu").Bind(Model.Settings("ChartType")) _
    .SelectedValue("Column").OnClientSelectedIndexChanged("chartTypeMenu_SelectedIndexChanged")
)

&lt;div id="seriesContainer" style="display:none"&gt;
    &lt;h4&gt;現在の選択&lt;/h4&gt;
    &lt;p&gt;系列： &lt;b id="seriesName"&gt;&lt;/b&gt;&lt;/p&gt;
    &lt;dl id="detailContainer" class="dl-horizontal" style="display:none"&gt;
        &lt;dt&gt;Country&lt;/dt&gt;&lt;dd id="seriesCountry"&gt;&lt;/dd&gt;
        &lt;dt&gt;Sales&lt;/dt&gt;&lt;dd id="seriesSales"&gt;&lt;/dd&gt;
        &lt;dt&gt;Expenses&lt;/dt&gt;&lt;dd id="seriesExpenses"&gt;&lt;/dd&gt;
        &lt;dt&gt;Downloads&lt;/dt&gt;&lt;dd id="seriesDownloads"&gt;&lt;/dd&gt;
    &lt;/dl&gt;
&lt;/div&gt;

                        </div>
                        <div class="tab-pane pane-content" id="selectionModeJs">

var chartSelectionMode = null,
    selectionModeMenu = null;

function InitialControls() {
    //選択モード Module  
    chartSelectionMode = &lt;wijmo.chart.FlexChart&gt;wijmo.Control.getControl('#chartSelectionMode'),
        typeMenu = &lt;wijmo.input.comboBox&gt;wijmo.Control.getControl('#chartTypeMenu'),
        selectionModeMenu = &lt;wijmo.input.comboBox&gt;wijmo.Control.getControl('#selectionModeMenu'),
        seriesContainer = document.getElementById('seriesContainer'),
        detailContainer = document.getElementById('detailContainer');
}

//選択モード Module
function selectionModeMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        if (!chartSelectionMode) {
            chartSelectionMode = wijmo.Control.getControl('#chartSelectionMode')
        }
        chartSelectionMode.selectionMode = parseInt(sender.selectedIndex);

        // toggle the series panel's visiblity
        if (sender.selectedIndex === 0 || !chartSelectionMode.selection) {
            if (seriesContainer)
                seriesContainer.style.display = 'none';
        }
        else {
            if (seriesContainer)
                seriesContainer.style.display = 'block';
        }

        // toggle the series panel's visiblity
        if (sender.selectedIndex !== 2 || !chartSelectionMode.selection || !chartSelectionMode.selection.collectionView.currentItem) {
            if (detailContainer)
                detailContainer.style.display = 'none';
        }
        else {
            // update the details
            setSeriesDetail(chartSelectionMode.selection.collectionView.currentItem);
        }

    }
}

function chartTypeMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        if (!chartSelectionMode) {
            chartSelectionMode = wijmo.Control.getControl('#chartSelectionMode')
        }
        chartSelectionMode.chartType = sender.selectedValue;
    }
}

// FlexChartの選択変更時に詳細を更新
function chartSelectionMode_SelectionChanged(sender) {
    var currentSelection = sender.selection,
        currentSelectItem;
    if (currentSelection) {
        var seriesContainer = document.getElementById('seriesContainer'),
            selectionModeMenu = &lt;wijmo.input.comboBox&gt; wijmo.Control.getControl('#selectionModeMenu');
        seriesContainer.style.display = 'block'; // コンテナを表示

        document.getElementById('seriesName').innerHTML = currentSelection.name;
        currentSelectItem = currentSelection.collectionView.currentItem;

        if (currentSelectItem && selectionModeMenu.selectedValue === 'Point') {
            setSeriesDetail(currentSelectItem); // 詳細を更新
        }
    }
}

// FlexChartの現在の選択の詳細を表示するヘルパーメソッド
function setSeriesDetail(currentSelectItem) {
    detailContainer.style.display = 'block';
    document.getElementById('seriesCountry').innerHTML = currentSelectItem.Country;
    document.getElementById('seriesSales').innerHTML = wijmo.Globalize.format(currentSelectItem.Sales, 'c2');
    document.getElementById('seriesExpenses').innerHTML = wijmo.Globalize.format(currentSelectItem.Expenses, 'c2');
    document.getElementById('seriesDownloads').innerHTML = wijmo.Globalize.format(currentSelectItem.Downloads, 'n0');
};


                        </div>
                        <div class="tab-pane pane-content" id="selectionModeCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.Settings = CreateIndexSettings()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function

    Private Function CreateIndexSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
            {"ChartType", New Object() {"Column", "Bar", "Scatter", "Line", "LineSymbols", "Area",
                "Spline", "SplineSymbols", "SplineArea"}},            
            {"SelectionMode", New Object() {SelectionMode.None.ToString(), SelectionMode.Series.ToString(), SelectionMode.Point.ToString()}}
        }
        Return settings
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ） :   </h4>
                @(Html.C1().FlexChart().Id("chartSelectionMode").OnClientSelectionChanged("chartSelectionMode_SelectionChanged") _
                    .Bind(Model.CountrySalesData).BindingX("Country") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">選択モード</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("selectionModeMenu").Bind(Model.Settings("SelectionMode")) _
.SelectedValue("None").OnClientSelectedIndexChanged("selectionModeMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">チャートタイプ</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("chartTypeMenu").Bind(Model.Settings("ChartType")) _
.SelectedValue("Column").OnClientSelectedIndexChanged("chartTypeMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                </div>

                <div id="seriesContainer" style="display:none">
                    <h4>現在の選択</h4>
                    <p>Series : <b id="seriesName"></b></p>
                    <dl id="detailContainer" class="dl-horizontal" style="display:none">
                        <dt>Country</dt>
                        <dd id="seriesCountry"></dd>
                                <dt>Sales</dt>
                        <dd id="seriesSales"></dd>
                                <dt>Expenses</dt>
                        <dd id="seriesExpenses"></dd>
                                <dt>Downloads</dt>
                        <dd id="seriesDownloads"></dd>
                                                                                                                                                                                                </dl>
                </div>
            </div>
        </div>
    </div>


    <!-- toggle series visibility -->
    <div>
                                <h2>系列の表示切り替え</h2>
        <p>
                                <b>Series</b>クラスには<b>Visibility</b>プロパティがあり、系列をチャート内と凡例内に表示するか、凡例内にのみ表示するか、または完全に非表示にするかを決定できます。
        </p>
        <p>
                                この例は、<b>Visibility</b>プロパティを使用して系列の表示/非表示を切り替える２つの方法を示しています。
        </p>
        <ol>
                                <li>
                                凡例エントリをクリックする。<br />
                チャートの<b>option.legendToggle</b>プロパティがtrueに設定されていると、系列の凡例エントリがクリックされたとき、その<b>Visibility</b>プロパティが切り替わります。
            </li>
            <li>
                チェックボックスを使用する。<br />
                <b>checked</b>状態が変更されると、各系列の<b>Visibility</b>プロパティがその<b>checked</b>状態によって設定されます。
            </li>
        </ol>
        <div class="row">
            <div class="col-md-6">
                <div>
                                        <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#legendToggleHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#legendToggleJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#legendToggleCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="legendToggleHtml">

@@(Html.C1().FlexChart().Id("chartLegendToggle").LegendToggle(True).Bind(Model.CountrySalesData) _
    .BindingX("Country").OnClientSeriesVisibilityChanged("chartLegendToggle_SeriesVisibilityChanged") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
)
Sales &lt;input id="cbSales" type="checkbox"/&gt;&lt;br /&gt;
Expenses &lt;input id="cbExpenses" type="checkbox"/&gt;&lt;br /&gt;
Downloads &lt;input id="cbDownloads" type="checkbox"/&gt;

                        </div>
                        <div class="tab-pane pane-content" id="legendToggleJs">

function InitialControls() {    
    var chartLegendToggle = &lt;wijmo.chart.FlexChart&gt;wijmo.Control.getControl('#chartLegendToggle'),
        cbSales = document.getElementById('cbSales'),
        cbExpenses = document.getElementById('cbExpenses'),
        cbDownloads = document.getElementById('cbDownloads');    

    // loop through custom check boxes
    ['cbSales', 'cbExpenses', 'cbDownloads'].forEach(function (item, index) {
        // update checkbox and toggle FlexChart's series visibility when clicked
        var el = &lt;HTMLInputElement&gt; document.getElementById(item);
        el.checked = chartLegendToggle.series[index].visibility === wijmo.chart.SeriesVisibility.Visible;
        el.addEventListener('click', function () {
            if (this.checked) {
                chartLegendToggle.series[index].visibility = wijmo.chart.SeriesVisibility.Visible;
            }
            else {
                chartLegendToggle.series[index].visibility = wijmo.chart.SeriesVisibility.Legend;
            }
        });
    });
}
                            
function chartLegendToggle_SeriesVisibilityChanged(sender) {
    // loop through chart series
    sender.series.forEach(function (series) {
        var seriesName = series.name,
            checked = series.visibility === wijmo.chart.SeriesVisibility.Visible;

        // update custom checkbox panel
        var cBox = &lt;HTMLInputElement&gt;document.getElementById('cb' + seriesName);
        cBox.checked = checked;
    });
}

</div>
                        <div class="tab-pane pane-content" id="legendToggleCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexChart().Id("chartLegendToggle").LegendToggle(True).Bind(Model.CountrySalesData) _
                    .BindingX("Country").OnClientSeriesVisibilityChanged("chartLegendToggle_SeriesVisibilityChanged") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
                Sales <input id="cbSales" type="checkbox" /><br />
                Expenses <input id="cbExpenses" type="checkbox" /><br />
                Downloads <input id="cbDownloads" type="checkbox" />
            </div>
        </div>
    </div>



</div>

<script type="text/javascript">
    c1.documentReady(function () {
        if (window["InitialControls"]) {
            window["InitialControls"]();
        }
    });
</script>