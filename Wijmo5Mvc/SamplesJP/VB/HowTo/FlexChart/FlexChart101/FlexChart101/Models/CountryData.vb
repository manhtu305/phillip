﻿Public Class CountryData
    Public Property ID() As Integer
        Get
            Return m_ID
        End Get
        Set
            m_ID = Value
        End Set
    End Property
    Private m_ID As Integer
    Public Property Country() As String
        Get
            Return m_Country
        End Get
        Set
            m_Country = Value
        End Set
    End Property
    Private m_Country As String
    Public Property Downloads() As Decimal
        Get
            Return m_Downloads
        End Get
        Set
            m_Downloads = Value
        End Set
    End Property
    Private m_Downloads As Decimal
    Public Property Sales() As Decimal
        Get
            Return m_Sales
        End Get
        Set
            m_Sales = Value
        End Set
    End Property
    Private m_Sales As Decimal
    Public Property Expenses() As Decimal
        Get
            Return m_Expenses
        End Get
        Set
            m_Expenses = Value
        End Set
    End Property
    Private m_Expenses As Decimal


    ' generate some random data
    Public Shared Function GetCountryData() As IEnumerable(Of CountryData)
        Dim countries = New String() {"US", "Germany", "UK", "Japan", "Italy", "Greece"}
        Dim rand = New Random(0)
        Dim baseTime = DateTime.Parse("2015-1-1")
        Dim list = countries.[Select](Function(country, i)
                                          Dim download = rand.[Next](500, 1000) * 20000
                                          Dim sale = rand.[Next](100, 1000) * 10000
                                          Dim expense = rand.[Next](100, 800) * 5000
                                          Dim CountryRec As New CountryData
                                          CountryRec.ID = i + 1
                                          CountryRec.Country = country
                                          CountryRec.Downloads = download
                                          CountryRec.Sales = sale
                                          CountryRec.Expenses = expense
                                          Return CountryRec

                                      End Function)
        Return list
    End Function

End Class
