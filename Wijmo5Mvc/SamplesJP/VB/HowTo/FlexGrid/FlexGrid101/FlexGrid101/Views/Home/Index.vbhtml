﻿@Imports MVCFlexGrid101
@ModelType MVCFlexGrid101.FlexGridModel 

@Code
    ViewBag.Title = "FlexGrid入門"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code


        <div Class="header">
    <div Class="container">
        <img src = "@Url.Content("~/Content/c1logo.png")" alt="Component One ASP.Net MVC" />
        <h1>
                FlexGrid入門
        </h1>
        <p>
                このページでは、FlexGridコントロールの使用を開始する方法を説明します。
        </p>
    </div>
</div>

<div Class="container">
    <div Class="sample-page download-link">
        <a href = "https://www.grapecity.co.jp/developer/download#net" > トライアル版</a>
    </div>
    <!-- Getting Started -->
    <div>
                        <h2> はじめに</h2>
        <p>
                        MVCアプリケーションでFlexGridコントロールの使用を開始する際の手順。
        </p>
        <ol>
                        <li> ComponentOne ASP.NET MVCアプリケーションテンプレートを使用して、新しいMVCプロジェクトを作成します。</li>
            <li> プロジェクトに、コントローラおよび対応するビューを追加します。</li>
            <li> razor構文を使用して、ビューのFlexGridコントロールを初期化します。</li>
            <li>（オプション）CSSを追加して、FlexGridコントロールの外観をカスタマイズします。</li>
        </ol>
        <p>
                        これによって作成されるFlexGridは、デフォルトの動作を行います。たとえば、列の自動生成、列のソートと並べ替え、編集、クリップボードのサポートです。
        </p>
        <div Class="row">
            <div Class="col-md-6">
                <div>
                                <ul Class="nav nav-tabs" role="tablist">
                        <li Class="active"><a href="#gsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li> <a href = "#gsCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li> <a href = "#gsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div Class="tab-content">
                        <div Class="tab-pane active pane-content" id="gsHtml">
&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;

    &lt;/head&gt;
    &lt;body&gt;
        &lt;!-- this Is the grid --&gt;
        @@(Html.C1().FlexGrid().Id("gsFlexGrid").IsReadOnly(True).SortingType(Grid.AllowSorting.SingleColumn) _
            .Bind(Model.CountryData).AutoGenerateColumns(True))

    &lt;/body&gt;
&lt;/html&gt;

                        </div>
                        <div Class="tab-pane pane-content" id="gsCss">

/* set default grid style */
.wj-flexgrid {
    height: 300px;
    background-color: white;
    box-shadow: 4px 4px 10px 0px rgba(50, 50, 50, 0.75);
    margin-bottom: 12px;
}

                        </div>
                        <div Class="tab-pane pane-content" id="gsCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function
End Class



                            </div>
                        </div>
                </div>
            </div>
            <div Class="col-md-6">
                <h4> 結果（ライブ） :              </h4>                
                    @(Html.C1().FlexGrid().Id("gsFlexGrid").IsReadOnly(True).SortingType(Grid.AllowSorting.SingleColumn) _
                .Bind(Model.CountryData).AutoGenerateColumns(True))
            </div>
        </div>
    </div>

    <!-- column definitions -->
    <div>
        <h2>列定義</h2>
        <p>
            「はじめに」の例では列を定義しなかったため、FlexGridが自動的に生成しました。
        </p>
        <p>
            この例は、FlexGridの<b>Columns</b>プロパティを使用して列を定義する方法を示します。
        </p>
        <p>
            columnsを指定すると、どの列を選択してどのような順序で表示するかを決めることができます。
            これにより、各列のWidth、Heading、Formatting、Alignmentなどのプロパティを制御することもできます。
        </p>
        <p>
            この場合、スターサイズ設定を使用して「Country」列の幅を設定します。これにより、グリッドの有効幅を埋めるように列を伸ばして空きスペースがないようにします。 「Amount」列は、formatプロパティを「n0」に設定した結果、桁区切りありで小数点以下の桁数がない数字になります。 「Discount」列は、formatプロパティを「p0」に設定した結果、パーセンテージ表記で小数点以下の桁数がない数値になります。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#cdHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#cdCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="cdHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("cdInitMethod").IsReadOnly(True).AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
        bl.Add(Sub(cb) cb.Binding("End").Header("End").Format("HH:mm"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        bl.Add(Sub(cb) cb.Binding("Amount2"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
        bl.Add(Sub(cb) cb.Binding("Active"))
    End Sub)
)
                        </div>
                        <div class="tab-pane pane-content" id="cdCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("cdInitMethod").IsReadOnly(True).AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                    .Bind(Model.CountryData).CssClass("grid") _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                                 bl.Add(Sub(cb) cb.Binding("End").Header("End").Format("HH:mm"))
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                                 bl.Add(Sub(cb) cb.Binding("Amount2"))
                                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                                 bl.Add(Sub(cb) cb.Binding("Active"))
                             End Sub)
                )
            </div>
        </div>
    </div>


    <!-- selection modes -->
    <div>
        <h2>選択モード</h2>
        <p>
            Excelと同様に、FlexGridではデフォルトでマウスまたはキーボードを使ってセルの範囲を選択できます。 <b>SelectionMode</b>プロパティを使用すると、これを変更して、Row、Range of Rows、Non-Contiguous Rows（リストボックス内など）、Single Cell、Range of Cellsを選択するか、または選択を完全に無効にできます。
        </p>
        <p>
            この例では、ComboBoxコントロールから<b>SelectionMode</b>を選択できます。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#smHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#smJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#smCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="smHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("smFlexGrid").IsReadOnly(True).AutoGenerateColumns(False) _
    .SortingType(Grid.AllowSorting.SingleColumn).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.None).Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
    End Sub)
)
&lt;br /&gt;選択モード
@@(Html.C1().ComboBox().Id("smMenu").Bind(Model.Settings("SelectionMode")) _
    .SelectedIndex(0).OnClientSelectedIndexChanged("smMenu_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="smJs">

var smFlexGrid = null;

function InitialControls() {
    //選択モード Modules
    smFlexGrid = &lt;wijmo.grid.FlexGrid&gt;wijmo.Control.getControl("#smFlexGrid");
}

//選択モード Modules
function smMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue != null && smFlexGrid != null) {
        smFlexGrid.selectionMode = sender.selectedValue;
    }
}

                        </div>
                        <div class="tab-pane pane-content" id="smCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.Settings = CreateSettings()
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function

    Private Function CreateSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
        {"SelectionMode", New Object() {SelectionMode.None.ToString(), SelectionMode.Cell.ToString(), SelectionMode.CellRange.ToString(), SelectionMode.Row.ToString(), SelectionMode.RowRange.ToString(), SelectionMode.ListBox.ToString()}},        
    }
        Return settings
    End Function
    
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ） :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("smFlexGrid").IsReadOnly(True).AutoGenerateColumns(False) _
                .SortingType(Grid.AllowSorting.SingleColumn).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.None).Bind(Model.CountryData).CssClass("grid") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
                         End Sub)
                )
                <br />選択モード
                @(Html.C1().ComboBox().Id("smMenu").Bind(Model.Settings("SelectionMode")) _
                 .SelectedIndex(0).OnClientSelectedIndexChanged("smMenu_SelectedIndexChanged")
                )
            </div>
        </div>
    </div>



    <!-- editing -->
    <div>
                <h2>編集</h2>
        <p>
                FlexGridでは、Excelにあるような高速なセル内編集が組み込みでサポートされています。 余分な列を追加して、そこに表示モードと編集モードを切り替えるための編集ボタンを置く必要はありません。
        </p>
        <p>
                ユーザーは、任意のセルでキー入力することで編集を開始できます。 これで、そのセルはクイック編集モードになります。
            このモードでは、カーソルキーを押すと編集が終了し、選択範囲が別のセルに移動します。
        </p>
        <p>
                編集を開始する別の方法としては、F2を押すかセルをダブルクリックします。 これで、そのセルは完全編集モードになります。 このモードでは、カーソルキーを押すと、セルテキスト内のカレットが移動します。
            編集を終了し、別のセルに移動するには、［Enter］、［Tab］、または［Esc］キーを押す必要があります。
        </p>
        <p>
                編集が終了すると、データは自動的に適切なタイプに変換されます。 ユーザーが無効なデータを入力した場合、編集はキャンセルされ、元のデータが保持されます。
        </p>
        <p>
                データをサーバーに対して更新するモードは２つあります。
        </p>
        <ol>
                <li>
                デフォルトでは、編集が終了すると更新操作がサーバーにコミットされます。
                <p>
                <b>注意 :        </b>
                    ユーザーが更新操作をデータソースサーバーにコミットする場合は、Updateアクション、Deleteアクション、またはCreateアクションのURLを提供する必要があります。
                    データソースを更新するために使用するコードは、対応するアクション内に記述する必要があります。
                </p>
            </li>
            <li>
                もう１つのモードはBatchEditです。 ユーザーは複数の項目を更新、作成、または削除できます。
                これらの変更は、確認後に１回だけデータソースに対してコミットされます。
                これで、これらの変更はクライアント側のCollectionViewのcommitメソッドによってコミットできます。
                ユーザーは、ソート動作、ページング動作、またはフィルタ処理動作によってこれらをコミットすることもできます。
                <p>
                <b>注意 :        </b>
                    このモードでは、BatchEditアクションURLを提供し、データソースの更新に使用される対応するコードを記述する必要があります。
                </p>
            </li>
        </ol>
        <p>
                Grid、Column、Rowオブジェクトの<b>IsReadOnly</b>プロパティを使用して、Grid、Column、またはRowレベルで編集を無効にすることができます。 この例では、ID列を読み取り専用にします。
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                        <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#eHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#eCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="eHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("eFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .CssClass("grid") _
    .Bind(Sub(bl)
            bl.Bind(Url.Action("GridRead"))
            bl.Update(Url.Action("EGridUpdate"))
        End Sub) _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*").IsReadOnly(True))
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="eCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function    

    Public Function GridRead(<C1JsonRequest> requestData As CollectionViewRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Read(requestData, FlexGridModel.Source))
    End Function

    Public Function EGridUpdate(<C1JsonRequest> requestData As CollectionViewEditRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Edit(Of Sale)(requestData, Function(sale)
            Dim [error] As String = String.Empty
            Dim success As Boolean = True
            Dim fSale = FlexGridModel.Source.Find(Function(item) item.ID = sale.ID)
            fSale.Country = sale.Country
            fSale.Amount = sale.Amount
            fSale.Discount = sale.Discount
            fSale.Active = sale.Active
            Dim RetValue = New CollectionViewItemResult(Of Sale)
            RetValue.Error = [error]
            RetValue.Success = success AndAlso ModelState.IsValid
            RetValue.Data = fSale
            Return RetValue
        End Function, Function() FlexGridModel.Source))
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ） :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("eFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                .CssClass("grid") _
                .Bind(Sub(bl)
                          bl.Bind(Url.Action("GridRead"))
                          bl.Update(Url.Action("EGridUpdate"))
                      End Sub) _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*").IsReadOnly(True))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- grouping -->
    <div>
                    <h2>
                    グループ化
        </h2>
        <p>
                    FlexGridは、クライアント側データのグループ化をサポートします。列に従ってデータをグループ化するためには、<b>GroupBy</b>プロパティを列名に設定します。
        </p>
        <p>
                    内部的に、FlexGridはクライアント側の<b>IItemsSource</b>インタフェースを使用してグループ化をサポートします。 クライアント側でグループ化を有効にするには、１つ以上の<b>GroupDescription</b>オブジェクトを<b>itemsSource.groupDescriptions</b>プロパティに追加します。 フィールドによってグリッドをグループ化するのは簡単です。対応するフィールド名を持つFlexGridBuilderのGroupByメソッドを呼び出します。 グリッドの<b>ShowGroups</b>プロパティがtrue（デフォルト値）に設定されていることを確認します。
            <b>GroupDescription</b>オブジェクトは柔軟であり、値またはグループ化関数に基づいてデータをグループ化できます。 次の例では、日付は年単位でグループ化し、金額は４つの範囲（5,000超、1,000から5,000、500から1,000、および500未満）にグループ化して、その他はすべて値でグループ化します。 メニューを使用して各グループ化の効果を確認してください。
        </p>
        <p>
                    「Amount」列のグループ行に合計が表示されることに注意してください。 これは、列の<b>Aggregate</b>プロパティを「Sum」に設定することで行われます。 列内の値を編集すると、集計は自動的に更新されます。
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                            <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#gHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#gJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#gCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="gHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("gFlexGrid").AutoGenerateColumns(False) _
    .SortingType(Grid.AllowSorting.SingleColumn).IsReadOnly(True).GroupBy("Country").Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Product").Header("Product").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Color").Header("Color").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Header("Amount").Format("n0").Width("*") _
                            .Aggregate(C1.Web.Mvc.Grid.Aggregate.Sum))
    End Sub)
                )
&lt;br /&gt;グループ化
@@(Html.C1().ComboBox().Id("gMenu").Bind(Model.Settings("GroupBy")) _
    .SelectedIndex(0).OnClientSelectedIndexChanged("gMenu_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="gJs">

//Group By Modules
function gMenu_SelectedIndexChanged(sender) {    
    var grid = &lt;wijmo.grid.FlexGrid&gt;wijmo.Control.getControl("#gFlexGrid");
    if (sender.selectedValue && grid) {
        var name = sender.selectedValue;
        var groupDescriptions = grid.collectionView.groupDescriptions;
        grid.beginUpdate();
        groupDescriptions.clear();

        if (name.indexOf("Country") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Country"));
        }

        if (name.indexOf("Product") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Product"));
        }

        if (name.indexOf("Color") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Color"));
        }

        if (name.indexOf("Start") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Start", function (item, prop) {
                var value = item[prop];
                return value.getFullYear() + "/" + value.getMonth();
            }));
        }

        if (name.indexOf("Amount") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Amount", function (item, prop) {
                var value = item[prop];
                if (value <= 500) {
                    return "<500";
                }

                if (value > 500 && value <= 1000) {
                    return "500 to 1000";
                }

                if (value > 1000 && value <= 5000) {
                    return "1000 to 5000";
                }

                return "More than 5000";
            }));
        }
        grid.endUpdate();
    }
}
                            

                        </div>
                        <div class="tab-pane pane-content" id="gCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.Settings = CreateSettings()
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function

    Private Function CreateSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {        
        {"GroupBy", New Object() {"Country", "Product", "Color", "Start", "Amount", "Country and Product",
            "Product and Color", "None"}}
    }
        Return settings
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ） :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("gFlexGrid").AutoGenerateColumns(False) _
                    .SortingType(Grid.AllowSorting.SingleColumn).IsReadOnly(True).GroupBy("Country").Bind(Model.CountryData).CssClass("grid") _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Color").Header("Color").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Header("Amount").Format("n0").Width("*") _
                                                     .Aggregate(C1.Web.Mvc.Grid.Aggregate.Sum))
                             End Sub)
                )
                <br />グループ化
                @(Html.C1().ComboBox().Id("gMenu").Bind(Model.Settings("GroupBy")) _
                    .SelectedIndex(0).OnClientSelectedIndexChanged("gMenu_SelectedIndexChanged")
                )
            </div>
        </div>
    </div>


    <!-- filtering -->
    <div>
                        <h2>フィルタ処理</h2>
        <p>
                        FlexGridは、<b>Filterable</b>プロパティを使用したフィルタ処理をサポートします。 フィルタ処理を有効にするには、<b>Filterable((Sub(fl) .ColumnFilters(Sub(cfsb)...</b>プロパティを設定します。
        </p>
        <p>
            この例では、ID、Country、Product、Color、Startに対するフィルタを作成し、入力コントロールからフィルタ値を取得します。
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#fHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#fCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="fHtml">

@@(Html.C1().FlexGrid().Id("fFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .IsReadOnly(True).Bind(Model.CountryData).CssClass("grid") _
    .Filterable(Sub(fl)
                    fl.DefaultFilterType(FilterType.None) _
                    .ColumnFilters(Sub(cfsb)
                                        For index As Int16 = 0 To Model.FilterBy.Length - 1
                                            cfsb.Add(Sub(cfb) cfb.Column(Model.FilterBy.ToList()(index)).FilterType(FilterType.Condition))
                                        Next
                                    End Sub)

                End Sub) _
.Columns(Sub(bl)
                bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="fCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        model.FilterBy = New String() {"ID", "Country", "Product", "Color", "Start"}
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ） :        </h4>
                @(Html.C1().FlexGrid().Id("fFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                    .IsReadOnly(True).Bind(Model.CountryData).CssClass("grid") _
                    .Filterable(Sub(fl)
                                    fl.DefaultFilterType(FilterType.None) _
                                    .ColumnFilters(Sub(cfsb)
                                                       For index As Int16 = 0 To Model.FilterBy.Length - 1
                                                           cfsb.Add(Sub(cfb) cfb.Column(Model.FilterBy.ToList()(index)).FilterType(FilterType.Condition))
                                                       Next
                                                   End Sub)

                                End Sub) _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                             bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                         End Sub)
                )
            </div>
        </div>
    </div>



    <!-- paging -->
    <div>
                            <h2>ページング</h2>
        <p>
                            FlexGridは、<b>Pager</b>コントロールを使用したページングをサポートします。 ページングを有効にするには、<b>PageSize</b>プロパティにページに表示する項目数を設定して、PagerコントロールをFlexGridにバインドします。
        </p>
        <p>
            この例では、<b>PageSize</b>を設定して1ページに10項目を表示します。 Pagerコントロールを追加して、<b>Owner</b>プロパティにFlexGridのIDを設定すると、ページ切り替えすることができます。
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#pHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#pCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="pHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("pFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .IsReadOnly(True).CssStyle("height", "auto").Bind(Model.CountryData).CssClass("grid") _
    .PageSize(10) _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
        bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
        bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
    End Sub)
)
@@Html.C1().Pager().Owner("pFlexGrid")

                        </div>
                        <div class="tab-pane pane-content" id="pCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("pFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                    .IsReadOnly(True).CssStyle("height", "auto").Bind(Model.CountryData).CssClass("grid") _
                    .PageSize(10) _
    .Columns(Sub(bl)
                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                 bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
             End Sub)
                )
                @Html.C1().Pager().Owner("pFlexGrid")
            </div>
        </div>
    </div>

    <!-- conditional styling -->
    <div>
        <h2>条件付きスタイル設定</h2>
        <p>
            FlexGridには、セルのコンテンツを全面的に制御できる<b>ItemFormatter</b>プロパティがあります。
        </p>
        <p>
            この例では、TypeScript関数を使用して、名前付きの色を返す値の範囲を作成します。 次に、この関数をFlexGridの<b>ItemFormatter</b>で呼び出し、セルの前景色を条件に基づいて設定するためにセルのデータを渡します。
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#csHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#csJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#csCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="csHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("csFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .IsReadOnly(True).Bind(Model.CountryData).ItemFormatter("csFlexGrid_ItemFormatter") _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="csJs">

//Conditional styling Modules
function csFlexGrid_ItemFormatter(panel, r, c, cell) {
    if (wijmo.grid.CellType.Cell == panel.cellType && panel.columns[c].binding == 'Amount') {
        var cellData = panel.getCellData(r, c);
        cell.style.color = cellData < 0 ? 'red' : cellData < 500 ? 'black' : 'green';
    }
    if (wijmo.grid.CellType.Cell == panel.cellType && panel.columns[c].binding == 'Discount') {
        var cellData = panel.getCellData(r, c);
        cell.style.color = cellData < .1 ? 'red' : cellData < .2 ? 'black' : 'green';
    }
}

                        </div>
                        <div class="tab-pane pane-content" id="csCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("csFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                .IsReadOnly(True).Bind(Model.CountryData).ItemFormatter("csFlexGrid_ItemFormatter") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- themes -->
    <div>
        <h2>テーマ</h2>
        <p>
            FlexGridの外観はCSSで定義されます。 デフォルトのテーマに加えて、プロのデザインによる12種類ほどのテーマが組み込まれており、すべてのComponentOne ASP.NET MVCコントロールの外観を統一感のある魅力的な見た目にカスタマイズできます。
        </p>
        <p>
            この例では、グリッド要素に「custom-flex-grid」クラスを追加し、CSSルールを定義して、「custom-flex-grid」を持つすべてのグリッドに対して単純な「白黒、境界線なし」のテーマを作成します。
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#tCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#tCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="tHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("tFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .IsReadOnly(True).Bind(Model.CountryData).CssClass("custom-flex-grid") _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="tCss">

.custom-flex-grid .wj-header.wj-cell {
    color: #fff;
    background-color: #000;
    border-bottom: solid 1px #404040;
    border-right: solid 1px #404040;
    font-weight: bold;
}

.custom-flex-grid .wj-cell {
    background-color: #fff;
    border: none;
}

.custom-flex-grid .wj-alt:not(.wj-state-selected):not(.wj-state-multi-selected) {
    background-color: #fff;
}

.custom-flex-grid .wj-state-selected {
    background: #000;
    color: #fff;
}

.custom-flex-grid .wj-state-multi-selected {
    background: #222;
    color: #fff;
}

                        </div>
                        <div class="tab-pane pane-content" id="tCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("tFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                .IsReadOnly(True).Bind(Model.CountryData).CssClass("custom-flex-grid") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- trees/hierarchical data -->
    <div>
        <h2>ツリーと階層化データ</h2>
        <p>
            グループ化に加えて、FlexGridは階層化データをサポートします。これは、サブ項目のリストを持つ項目があるデータのことです。 このタイプの階層構造はごく一般的なもので、通常はツリービューコントロールで表示されます。
        </p>
        <p>
            階層化データソースでFlexGridを使用するには、<b>ChildItemsPath</b>プロパティを子要素が入っているデータ要素の名前に設定します。 グリッドは自動的にデータをスキャンし、ツリーを構築します。
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tvHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#tvCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#tvCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="tvHtml">

@@(Html.C1().FlexGrid().Id("tvFlexGrid").AutoGenerateColumns(False).IsReadOnly(True) _
    .Bind(Model.TreeData).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.ListBox) _
    .AllowResizing(C1.Web.Mvc.Grid.AllowResizing.None).SortingType(Grid.AllowSorting.None).ChildItemsPath("Children") _
    .CssClass("custom-flex-grid") _
    .Columns(Sub(bl)
                bl.Add().Binding("Header").Width("*").Header("Folder/File Name")
                bl.Add().Binding("Size").Width("80").Align("center")
            End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="tvCss">

.custom-flex-grid .wj-header.wj-cell {
    color: #fff;
    background-color: #000;
    border-bottom: solid 1px #404040;
    border-right: solid 1px #404040;
    font-weight: bold;
}

.custom-flex-grid .wj-cell {
    background-color: #fff;
    border: none;
}

.custom-flex-grid .wj-alt:not(.wj-state-selected):not(.wj-state-multi-selected) {
    background-color: #fff;
}

.custom-flex-grid .wj-state-selected {
    background: #000;
    color: #fff;
}

.custom-flex-grid .wj-state-multi-selected {
    background: #222;
    color: #fff;
}

                        </div>
                        <div class="tab-pane pane-content" id="tvCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        model.TreeData = Folder.Create(Server.MapPath("~")).Children
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexGrid().Id("tvFlexGrid").AutoGenerateColumns(False).IsReadOnly(True) _
                    .Bind(Model.TreeData).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.ListBox) _
                    .AllowResizing(C1.Web.Mvc.Grid.AllowResizing.None).SortingType(Grid.AllowSorting.None).ChildItemsPath("Children") _
                    .CssClass("custom-flex-grid") _
                    .Columns(Sub(bl)
                                 bl.Add().Binding("Header").Width("*").Header("Folder/File Name")
                                 bl.Add().Binding("Size").Width("80").Align("center")
                             End Sub)
                )
            </div>
        </div>
    </div>



    <!-- handling null values -->
    <div>
        <h2>null値の処理</h2>
        <p>
            デフォルトで、FlexGridではstringタイプの列内には空の値を入力でき、その他のタイプの列には空、すなわちnull値は入力できません。
        </p>
        <p>
            この動作は、グリッド列の<b>IsRequired</b>プロパティを使用して変更できます。
            <b>IsRequired</b>プロパティをfalseに設定すると、その列のタイプにかかわらず、空の値をグリッドで入力できるようになります。 反対に、<b>IsRequired</b>プロパティをtrueに設定すると、グリッドはstring列であっても空の値を許可しません。
        </p>
        <p>
            <b>IsRequired</b>をnullに設定すると、デフォルトの動作（nullはstring列でのみ許可）に戻ります。
        </p>
        <p>
            次のグリッドでは、デフォルトの動作に戻しています。 最初の列では<b>IsRequired</b>をfalseに設定し、その他すべてではtrueに設定します。 不要なコンテンツを削除するには、空の文字列を入力するか、または単に［Delete］キーを押します。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#nvHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#nvCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="nvHtml">

@@(Html.C1().FlexGrid().Id("nvGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .Bind(Sub(bl)
            bl.Bind(Url.Action("GridRead"))
            bl.Update(Url.Action("NVGridUpdate"))
        End Sub) _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("ID").Header("ID").IsReadOnly(True))
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country").IsRequired(False))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product").IsRequired(True))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").IsRequired(True))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0").IsRequired(True))
        End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="nvCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

    Public Function GridRead(<C1JsonRequest> requestData As CollectionViewRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Read(requestData, FlexGridModel.Source))
    End Function

    Public Function NVGridUpdate(<C1JsonRequest> requestData As CollectionViewEditRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Edit(Of Sale)(requestData, Function(sale)
            Dim [error] As String = String.Empty
            Dim success As Boolean = True
            Dim fSale = FlexGridModel.Source.Find(Function(item) item.ID = sale.ID)
            fSale.Country = sale.Country
            fSale.Product = sale.Product
            fSale.Amount = sale.Amount
            fSale.Discount = sale.Discount
            Dim RetValue = New CollectionViewItemResult(Of Sale)
            RetValue.Error = [error]
            RetValue.Success = success AndAlso ModelState.IsValid
            RetValue.Data = fSale
            Return RetValue
        End Function, Function() FlexGridModel.Source))
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>結果（ライブ）:</h4>
                @(Html.C1().FlexGrid().Id("nvGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                    .Bind(Sub(bl)
                              bl.Bind(Url.Action("GridRead"))
                              bl.Update(Url.Action("NVGridUpdate"))
                          End Sub) _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID").IsReadOnly(True))
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").IsRequired(False))
                                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product").IsRequired(True))
                                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").IsRequired(True))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Format("n0").IsRequired(True))
                             End Sub)
                )
            </div>
        </div>
    </div>









    

</div>

<script type="text/javascript">
    c1.documentReady(function () {
        if (window["InitialControls"]) {
            window["InitialControls"]();
        }
    });
</script>