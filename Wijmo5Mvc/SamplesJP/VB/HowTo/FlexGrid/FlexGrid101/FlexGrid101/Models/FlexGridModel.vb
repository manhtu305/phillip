﻿Public Class FlexGridModel
    Public Property Settings() As IDictionary(Of String, Object())
        Get
            Return m_Settings
        End Get
        Set
            m_Settings = Value
        End Set
    End Property
    Private m_Settings As IDictionary(Of String, Object())
    Public Property CountryData() As IEnumerable(Of Sale)
        Get
            Return m_CountryData
        End Get
        Set
            m_CountryData = Value
        End Set
    End Property
    Private m_CountryData As IEnumerable(Of Sale)
    Public FilterBy As String()
    Public TreeData As IEnumerable(Of ITreeItem)

    'Add for editing source
    Public Shared Source As List(Of Sale) = Sale.GetData(500).ToList()


    Public Sub New()
    End Sub
End Class

