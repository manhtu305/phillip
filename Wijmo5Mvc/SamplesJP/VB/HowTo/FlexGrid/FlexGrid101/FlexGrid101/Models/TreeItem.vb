﻿Public Interface ITreeItem
    Property Header() As String
    ReadOnly Property Children() As IList(Of ITreeItem)
End Interface

Public Class Folder
    Implements ITreeItem
    Public Property Header() As String Implements ITreeItem.Header
        Get
            Return m_Header
        End Get
        Set
            m_Header = Value
        End Set
    End Property
    Private m_Header As String
    Public ReadOnly Property Children() As IList(Of ITreeItem) Implements ITreeItem.Children
        Get
            Return m_Children
        End Get
    End Property
    Private m_Children As IList(Of ITreeItem)

    Public Sub New(name As String)
        Header = name
        m_Children = New List(Of ITreeItem)()
    End Sub

    Public Shared Function Create(path As String) As Folder
        Dim folder__1 = New Folder(System.IO.Path.GetFileName(path))
        System.IO.Directory.GetDirectories(path).ToList().ForEach(Sub(d) folder__1.Children.Add(Create(d)))
        System.IO.Directory.GetFiles(path).ToList().ForEach(Sub(f) folder__1.Children.Add(File.Create(f)))
        Return folder__1
    End Function
End Class

Public Class File
    Implements ITreeItem
    Public Property Header() As String Implements ITreeItem.Header
        Get
            Return m_Header
        End Get
        Set
            m_Header = Value
        End Set
    End Property
    Private m_Header As String
    Public Property DateModified() As DateTime
        Get
            Return m_DateModified
        End Get
        Set
            m_DateModified = Value
        End Set
    End Property
    Private m_DateModified As DateTime
    Public Property Size() As Long
        Get
            Return m_Size
        End Get
        Set
            m_Size = Value
        End Set
    End Property
    Private m_Size As Long
    Public ReadOnly Property Children() As IList(Of ITreeItem) Implements ITreeItem.Children
        Get
            Return Nothing
        End Get
    End Property

    Public Sub New(name As String)
        Header = name
    End Sub

    Public Shared Function Create(path As String) As File
        Dim file = New File(System.IO.Path.GetFileName(path))
        Dim info = New System.IO.FileInfo(path)
        file.DateModified = info.LastWriteTime
        file.Size = info.Length
        Return file
    End Function
End Class
