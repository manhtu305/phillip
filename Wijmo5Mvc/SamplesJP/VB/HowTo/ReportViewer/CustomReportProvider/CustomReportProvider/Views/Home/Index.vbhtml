@Code
    ViewBag.Title = "カスタムレポートプロバイダ"
End Code

    <div Class="header ">
    <div Class="container">
        <img src = "@Url.Content("~/Content/c1logo.png")" alt="ComponentOne ASP.NET MVC" />
        <h1>
            カスタムレポートプロバイダ
        </h1>
        <p>
            このページでは、FlexReportにメモリ内のデータセットを設定する方法を示します。
        </p>
    </div>
</div>

<div Class="container">
    <div Class="sample-page download-link">
        <a href = "https://www.grapecity.co.jp/developer/download#net" > トライアル版</a>
    </div>
    <!-- はじめに -->
    <div>
                    <h2> はじめに</h2>
        <p>
                    カスタムレポートプロバイダアプリケーションを使用するための基本的な手順
        </p>
        <ol>
                    <li> FlexReportのデータソースとしてメモリ内のデータセットを設定するカスタムレポートプロバイダを実装します。</li>
            <li> スタートアップにこのレポートプロバイダを登録します。</li>
            <li> ReportViewerコントロールを作成し、そのFilePathプロパティとReportNameプロパティを設定します。</li>
            <li> クライアントのqueryLoadingDataイベントのハンドラを追加して、レポートのロード時に選択された国を提供します。</li>
        </ol>
        <div Class="row">
            国の選択:
            <span>
                @(Html.C1().ComboBox() _
                    .Id("countriesMenu") _
                    .Bind(Employees.CountryNames).SelectedIndex(0) _
                    .OnClientSelectedIndexChanged("countriesMenu_selectedIndexChanged"))
            </span>
        </div>
        <div Class="row">
            @(Html.C1().ReportViewer() _
                        .Id("reportViewer") _
                        .Width("100%") _
                        .FilePath("memoryreports/ReportsRoot/DataSourceInMemory.flxr") _
                        .ReportName("Simple List") _
                        .OnClientQueryLoadingData("reportViewer_queryLoadingData"))
        </div>
    </div>
</div>