﻿@Imports FlexSheet101
@ModelType FlexSheet101.FlexSheetModel

@Code
    ViewBag.Title = "FlexSheet入門"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code


<div Class="header">
    <div Class="container">
        <img src = "@Url.Content("~/Content/c1logo.png")" alt="Component One ASP.Net MVC" />
        <h1>
            FlexSheet入門
        </h1>
        <p>
            このページでは、FlexSheetコントロールを使用する基本的な方法を示します。
        </p>
    </div>
</div>

<div Class="container">
    <div Class="sample-page download-link">
        <a href = "https://www.grapecity.co.jp/developer/download#net" target="_blank">トライアル版</a>
    </div>
    <!-- はじめに -->
    <div>
                    <h2> はじめに</h2>
        <p>
                    MVCアプリケーションでFlexSheetコントロールの使用を開始する際の手順。
        </p>
        <ol>
                    <li> ComponentOne ASP.NET MVCアプリケーションテンプレートを使用して、新しいMVCプロジェクトを作成します。</li>
            <li> プロジェクトに、コントローラおよび対応するビューを追加します。</li>
            <li> razor構文を使用して、ビューのFlexSheetコントロールを初期化します。</li>
            <li>（オプション）CSSを追加して、FlexSheetコントロールの外観をカスタマイズします。</li>
        </ol>
        <p>
                    これによって作成されるFlexSheetは、デフォルトの動作を行います。
        </p>
        <div Class="row">
            <div Class="col-md-6">
                <div>
                            <ul Class="nav nav-tabs" role="tablist">
                        <li Class="active"><a href="#gsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#gsCSS" role="tab" data-toggle="tab">CSS</a></li>
                        <li> <a href = "#gsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div Class="tab-content">
                        <div Class="tab-pane active pane-content" id="gsHtml">
&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;

    &lt;/head&gt;
    &lt;body&gt;
        &lt;!-- this Is the FlexSheet --&gt;
        @@(Html.C1().FlexSheet().CssClass("flexSheet").Id("gsFlexSheet").SelectedSheetIndex(0).Height(300) _
                        .AddBoundSheet(Sub(sheet) _
                            sheet.Name("Country").Bind(Sub(cv) _
                                                    cv.Bind(Model.CountryData).DisableServerRead(True)))
                    )
    &lt;/body&gt;
&lt;/html&gt;

                        </div>
                        <div Class="tab-pane pane-content" id="gsCSS">

.flexSheet {
    height:     500px;
    border: 2px solid #e0e0e0;
    margin: 6px;
}

.flexSheet.wj-header - row {
    background-color: #ABD0ED !important;
    color: #000000 !important;
}

                        </div>
                        <div Class="tab-pane pane-content" id="gsCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class

                        </div>
                    </div>
                </div>
            </div>
            <div Class="col-md-6">
                <h4> 結果（ライブ） :    </h4>
                @(Html.C1().FlexSheet().CssClass("flexSheet").Id("gsFlexSheet").SelectedSheetIndex(0).Height(300) _
                        .AddBoundSheet(Sub(sheet) _
                            sheet.Name("Country").Bind(Sub(cv) _
                                                    cv.Bind(Model.CountryData).DisableServerRead(True)))
                    )

            </div>
        </div>
    </div>

    <!-- Sorting -->
    <div>
        <h2 id="sort">
            ソート
        </h2>
        <p>
            FlexSheetは任意の列でソートできます。
        </p>
        <p>
            <b>SortManager</b>を使用すると、FlexSheetでソート処理を管理できます。 次の例では、SortManagerを使用して、ソート順の指定、ソート列の追加や削除、ソート列の順序の変更を行います。
        </p>
        <div Class="row">
            <div Class="col-md-6">
                <div>
                    <ul Class="nav nav-tabs" role="tablist">
                        <li Class="active"><a href="#sortingHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li> <a href="#sortingJS" role="tab" data-toggle="tab">TS</a></li>
                        <li> <a href="#sortingCS" role="tab" data-toggle="tab">HomeController.vb</a></li>

                    </ul>
                    <div Class="tab-content">
                        <div Class="tab-pane active pane-content" id="sortingHtml">

@@(Html.C1().FlexSheet().Id("sortingFlexSheet").CssClass("flexSheet").Height(300) _
    .AddBoundSheet(Sub(Sheet) Sheet.Name("Country") _
    .Bind(Sub(cv) cv.Bind(Model.CountryData).DisableServerRead(True)))
)
&lt;div id="sortTable"&gt;
    &lt;table class="table table-bordered"&gt;
        &lt;thead&gt;
            &lt;tr&gt;
                &lt;th class="text-center"&gt;Column&lt;/th&gt;
                &lt;th class="text-center"&gt;Order&lt;/th&gt;
            &lt;/tr&gt;
        &lt;/thead&gt;
        &lt;tbody&gt;&lt;/tbody&gt;
    &lt;/table&gt;
&lt;/div&gt;
&lt;div class="btn-group"&gt;
    &lt;button type="button" class="btn btn-default" onclick="addSortLevel()"&gt;
    レベルの追加
    &lt;/button&gt;
    &lt;button type="button" class="btn btn-default" onclick="deleteSortLevel()"&gt;
    レベルの削除
    &lt;/button&gt;
    &lt;button type="button" class="btn btn-default" onclick="copySortLevel()"&gt;
    レベルのコピー
    &lt;/button&gt;
&lt;/div&gt;
&lt;div class="btn-group"&gt;
    &lt;button id="moveup" type="button" Class="btn btn-default" onclick="moveSortLevel(-1)"&gt;
        &lt;span class="glyphicon glyphicon-arrow-up"&gt;&lt;/span&gt;
    &lt;/button&gt;
    &lt;button id="movedown" type="button" Class="btn btn-default" onclick="moveSortLevel(1)"&gt;
        &lt;span class="glyphicon glyphicon-arrow-down"&gt;&lt;/span&gt;
    &lt;/button&gt;
&lt;/div&gt;
&lt;div class="btn-group"&gt;
    &lt;button type="button" class="btn btn-default" onclick="commitSort()"&gt;OK&lt;/button&gt;
    &lt;button type="button" class="btn btn-default" onclick="cancelSort()"&gt;Cancel&lt;/button&gt;
&lt;/div&gt;
                        </div>
                        <div Class="tab-pane pane-content" id="sortingJS">

// ソート
var ctxSorting = {
    flexSheet: null,
    sortManager: null,
    moveup: null,
    movedown: null,
    tbody: null,
    columns: null
};

function loadSorting() {
    ctxSorting.flexSheet = &lt;wijmo.grid.sheet.FlexSheet&gt;wijmo.Control.getControl('#sortingFlexSheet');
    ctxSorting.sortManager = ctxSorting.flexSheet.sortManager;
    ctxSorting.moveup = &lt;HTMLButtonElement&gt;document.getElementById('moveup');
    ctxSorting.movedown = &lt;HTMLButtonElement&gt;document.getElementById('movedown');
    ctxSorting.tbody = wijmo.getElement('#sortTable tbody');
    ctxSorting.columns = getColumns();
    ctxSorting.flexSheet.selectedSheetChanged.addHandler(function (sender, args) {
        ctxSorting.columns = getColumns();
        ctxSorting.sortManager = ctxSorting.flexSheet.sortManager;
        updateSortTable();
    });
    updateSortTable();
    applyDataMap(ctxSorting.flexSheet);
};

function changeBtnState() {
    ctxSorting.moveup.disabled = ctxSorting.sortManager.sortDescriptions.currentPosition &lt;= 0;
    ctxSorting.movedown.disabled = ctxSorting.sortManager.sortDescriptions.currentPosition &gt;= ctxSorting.sortManager.sortDescriptions.itemCount - 1;
}

function updateSortTable() {
    var i, j, html = '', tr, sortDescriptions = ctxSorting.sortManager.sortDescriptions,
        items = sortDescriptions.items;
    for (i = 0; i &lt; items.length; i++) {
        tr = '&lt;tr onclick="moveCurrentTo(' + i + ')" ' +
            (sortDescriptions.currentItem == items[i] ? 'class="success"' : '') + '&gt;' +
            '&lt;td&gt;' +
            '&lt;select class="form-control" onchange="columnIndexChanged(this, ' + i + ')"&gt;' +
            '&lt;option value=-1&gt;&lt;/option&gt;';

        for (j = 0; j &lt; ctxSorting.columns.length; j++) {
            tr += '&lt;option value="' + j + '" ' + (j == items[i].columnIndex ? 'selected="selected"' : '') +
                '&gt;' + ctxSorting.columns[j] + '&lt;/option&gt;';
        }

        tr += '&lt;/select&gt;&lt;/td&gt;' +
            '&lt;td&gt;' +
            '&lt;select class="form-control" onchange="ascendingChanged(this, ' + i + ')"&gt;' +
            '&lt;option value="0" ' + (items[i].ascending ? 'selected="selected"' : '') + '&gt;Ascending&lt;/option&gt;' +
            '&lt;option value="1" ' + (!items[i].ascending ? 'selected="selected"' : '') + '&gt;Descending&lt;/option&gt;' +
            '&lt;/select&gt;&lt;/td&gt;&lt;/tr&gt;';
        html += tr;
    }
    ctxSorting.tbody.innerHTML = html;
    changeBtnState();
}

function moveCurrentTo(index) {
    var items = ctxSorting.sortManager.sortDescriptions.items, i = 0;
    ctxSorting.sortManager.sortDescriptions.moveCurrentTo(items[index]);
    for (; i < ctxSorting.tbody.children.length; i++) {
        ctxSorting.tbody.children[i].className = index == i ? 'success' : '';
    }
    changeBtnState();
}

function columnIndexChanged(ele, index) {
    if (ctxSorting.sortManager.sortDescriptions.items[index] != null)
        ctxSorting.sortManager.sortDescriptions.items[index].columnIndex = +ele.value;
}

function ascendingChanged(ele, index) {
    ctxSorting.sortManager.sortDescriptions.items[index].ascending = ele.value == "0";
}

// commit the sorts
function commitSort() {
    ctxSorting.sortManager.commitSort();
};

// cancel the sorts
function cancelSort() {
    ctxSorting.sortManager.cancelSort();
};

// add new sort level
function addSortLevel() {
    ctxSorting.sortManager.addSortLevel();
    updateSortTable();
};

// delete current sort level
function deleteSortLevel() {
    ctxSorting.sortManager.deleteSortLevel();
    updateSortTable();
};

// copy a new sort level by current sort level setting.
function copySortLevel() {
    ctxSorting.sortManager.copySortLevel();
    updateSortTable();
};

// move the sort level
function moveSortLevel(offset) {
    ctxSorting.sortManager.moveSortLevel(offset);
    updateSortTable();
};
// get the columns with the column header text for the column selection for sort setting.
function getColumns() {
    var columns = [],
        flex = ctxSorting.flexSheet,
        i = 0;

    if (flex) {
        for (; i < flex.columns.length; i++) {
            columns.push('Column ' + wijmo.grid.sheet.FlexSheet.convertNumberToAlpha(i));
        }
    }

    return columns;
}

                        </div>
                        <div Class="tab-pane pane-content" id="sortingCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class


                        </div>


                    </div>
                </div>
            </div>
            <div Class="col-md-6">
                <h4> 結果（ライブ） :    </h4>
                @(Html.C1().FlexSheet().Id("sortingFlexSheet").CssClass("flexSheet").Height(300) _
                    .AddBoundSheet(Sub(Sheet) Sheet.Name("Country") _
                    .Bind(Sub(cv) cv.Bind(Model.CountryData).DisableServerRead(True)))
                )
                <div id="sortTable">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">列</th>
                                <th class="text-center">順序</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div Class="btn-group">
                    <Button type="button" Class="btn btn-default" onclick="addSortLevel()">
                        レベルの追加
                    </Button>
                    <Button type="button" Class="btn btn-default" onclick="deleteSortLevel()">
                        レベルの削除
                    </Button>
                    <Button type="button" Class="btn btn-default" onclick="copySortLevel()">
                        レベルのコピー
                    </Button>
                </div>
                <div Class="btn-group">
                    <Button id="moveup" type="button"
                            Class="btn btn-default"
                            onclick="moveSortLevel(-1)">
                        <span Class="glyphicon glyphicon-arrow-up"></span>
                    </Button>
                    <Button id="movedown" type="button"
                            Class="btn btn-default"
                            onclick="moveSortLevel(1)">
                        <span Class="glyphicon glyphicon-arrow-down"></span>
                    </Button>
                </div>
                <div Class="btn-group">
                    <Button type="button" Class="btn btn-default" onclick="commitSort()">OK</Button>
                    <Button type="button" Class="btn btn-default" onclick="cancelSort()">キャンセル</Button>
                </div>

            </div>
        </div>
    </div>

    <!-- セルの書式設定 -->
    <div>
        <h2 id="format">
            セルの書式設定
        </h2>
        <p>
            FlexSheetでは、セルごとの書式を設定できます。 書式には、フォントスタイル、セル値のデータ形式（日付/数値の形式）、セルの塗りつぶし色、水平方向の配置などの設定が含まれます。
        </p>
        <div Class="row">
            <div Class="col-md-6">
                <div>
                    <ul Class="nav nav-tabs" role="tablist">
                        <li Class="active"><a href="#formatcHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li> <a href="#formatcJS" role="tab" data-toggle="tab">TS</a></li>
                        <li> <a href="#formatcCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div Class="tab-content">
                        <div Class="tab-pane active pane-content" id="formatcHtml">

@@(Html.C1().FlexSheet().Id("fcFlexSheet").Height(300).AddUnboundSheet("Number", 20, 8) _
    .AddUnboundSheet("Date", 20, 8)
)
@@(Html.C1().ColorPicker().Id("fcColorPicker").CssStyle("display", "none") _
    .CssStyle("position", "fixed").CssStyle("z-index", "100")
)
&lt;div class="well well-lg"&gt;
    &lt;div&gt;
        Format:
        @@(Html.C1().Menu().Id("fcMenuFormat").Header("Format") _
            .OnClientSelectedIndexChanged("fcMenuFormat_Changed") _
            .MenuItems(Sub(mitem)
                            mitem.Add("10進数形式", "0")
                            mitem.Add("数値形式", "n2")
                            mitem.Add("パーセンテージ形式", "p2")
                            mitem.Add("通貨形式", "c2")
                            mitem.AddSeparator()
                            mitem.Add("短い日付", "d")
                            mitem.Add("長い日付", "D")
                            mitem.Add("完全な日時（短い時刻）", "f")
                            mitem.Add("完全な日時（長い時刻）", "F")
                        End Sub)
        &lt;/div&gt;
    &lt;div&gt;
    @@Code
        Dim fontList = ViewBag.FontList
        Dim fontSizeList = ViewBag.FontSizeList
    End Code
    フォント:
    @@(Html.C1().ComboBox(Of FontName).Id("cboFontName").Bind(fontList) _
        .SelectedIndex(0).DisplayMemberPath("Name").SelectedValuePath("Value") _
        .IsEditable(False).CssStyle("width", "120px") _
        .OnClientSelectedIndexChanged("fontChanged") _
    )
    @@(Html.C1().ComboBox(Of FontSize)().Id("cboFontSize").Bind(fontSizeList) _
        .SelectedIndex(5).DisplayMemberPath("Name").SelectedValuePath("Value") _
        .IsEditable(False).CssStyle("width", "80px").OnClientSelectedIndexChanged("fontSizeChanged")
    )
    &lt;div class="btn-group"&gt;
        &lt;button type="button" id="boldBtn" class="btn btn-default" onclick="applyBoldStyle()"&gt;Bold&lt;/button&gt;
        &lt;button type="button" id="italicBtn" class="btn btn-default" onclick="applyItalicStyle()"&gt;Italic&lt;/button&gt;
        &lt;button type="button" id="underlineBtn" class="btn btn-default" onclick="applyUnderlineStyle()"&gt;Underline&lt;/button&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;div&gt;
    色:
    &lt;div class="btn-group"&gt;
        &lt;button type="button" class="btn btn-default" onclick="showColorPicker(event, false)"&gt;Fore Color&lt;/button&gt;
        &lt;button type="button" class="btn btn-default" onclick="showColorPicker(event, true)"&gt;Fill Color&lt;/button&gt;
    &lt;/div&gt;
    配置:
    &lt;div class="btn-group"&gt;
        &lt;button type="button" id="leftBtn" class="btn btn-default active" onclick="applyCellTextAlign('left')"&gt;Left&lt;/button&gt;
        &lt;button type="button" id="centerBtn" class="btn btn-default" onclick="applyCellTextAlign('center')"&gt;Center&lt;/button&gt;
        &lt;button type="button" id="rightBtn" class="btn btn-default" onclick="applyCellTextAlign('right')"&gt;Right&lt;/button&gt;
    &lt;/div&gt;

&lt;/div&gt;
&lt;/div&gt;

                        </div>
                        <div Class="tab-pane pane-content" id="formatcJS">

// セルの書式設定
var applyFillColor = false,
    updatingSelection = false,
    formats = ['0', 'n2', 'p2', 'c2', '', 'd', 'D', 'f', 'F'],
    ctxFormatCells = {
        format: '',        
        flexSheet: null,
        cboFontName: null,
        cboFontSize: null,
        menuFormat:null,
        boldBtn: null,
        italicBtn: null,
        underlineBtn: null,
        leftBtn: null,
        centerBtn: null,
        rightBtn: null,
        colorPicker:null,
        sheetName: '',
        selectionFormatState: {
            isBold: null,
            isItalic: null,
            isUnderline: null,
            textAlign: null
        },
    };

function loadFormatCells() {
    initFlexSheet();
    initInputs();
};

function initInputs() {
    ctxFormatCells.cboFontName = <wijmo.input.ComboBox> wijmo.Control.getControl('#cboFontName');
    ctxFormatCells.cboFontSize = <wijmo.input.ComboBox>wijmo.Control.getControl('#cboFontSize');
    ctxFormatCells.menuFormat = <wijmo.input.Menu>wijmo.Control.getControl('#fcMenuFormat');
    initBtns();
    initColorPicker();
    setMenuHeader(ctxFormatCells.menuFormat);
}

function initBtns() {
    ctxFormatCells.boldBtn = wijmo.getElement('#boldBtn');
    ctxFormatCells.italicBtn = wijmo.getElement('#italicBtn');
    ctxFormatCells.underlineBtn = wijmo.getElement('#underlineBtn');
    ctxFormatCells.leftBtn = wijmo.getElement('#leftBtn');
    ctxFormatCells.centerBtn = wijmo.getElement('#centerBtn');
    ctxFormatCells.rightBtn = wijmo.getElement('#rightBtn');
}

function formatCellsUpdateBtns() {
    updateActiveState(ctxFormatCells.selectionFormatState.isBold, ctxFormatCells.boldBtn);
    updateActiveState(ctxFormatCells.selectionFormatState.isItalic, ctxFormatCells.italicBtn);
    updateActiveState(ctxFormatCells.selectionFormatState.isUnderline, ctxFormatCells.underlineBtn);
    updateActiveState(ctxFormatCells.selectionFormatState.textAlign === 'left', ctxFormatCells.leftBtn);
    updateActiveState(ctxFormatCells.selectionFormatState.textAlign === 'center', ctxFormatCells.centerBtn);
    updateActiveState(ctxFormatCells.selectionFormatState.textAlign === 'right', ctxFormatCells.rightBtn);
}

function updateActiveState(condition, btn) {
    condition ? addClass(btn, "active") : removeClass(btn, "active");
}

function initFlexSheet() {
    var sheetIdx,
        sheetName,
        colIdx,
        rowIdx,
        date,
        flexSheet;

    ctxFormatCells.flexSheet = <wijmo.grid.sheet.FlexSheet> wijmo.Control.getControl('#fcFlexSheet');
    flexSheet = ctxFormatCells.flexSheet;
    if (flexSheet) {
        flexSheet.selectionChanged.addHandler(function (sender, args) {
            updateSelection(args.range);
            ctxFormatCells.selectionFormatState = flexSheet.getSelectionFormatState();
        });

        for (sheetIdx = 0; sheetIdx < flexSheet.sheets.length; sheetIdx++) {
            flexSheet.selectedSheetIndex = sheetIdx;
            sheetName = flexSheet.selectedSheet.name;
            for (colIdx = 0; colIdx < flexSheet.columns.length; colIdx++) {
                for (rowIdx = 0; rowIdx < flexSheet.rows.length; rowIdx++) {
                    if (sheetName === 'Number') {
                        flexSheet.setCellData(rowIdx, colIdx, colIdx + rowIdx);
                    } else {
                        date = new Date(2015, colIdx, rowIdx + 1);
                        flexSheet.setCellData(rowIdx, colIdx, date);
                    }
                }
            }
        }
        flexSheet.selectedSheetIndex = 0;
        updateSelection(flexSheet.selection);
    }
};

// initialize the colorPicker control.
function initColorPicker() {
    var colorPicker = ctxFormatCells.colorPicker = <wijmo.input.ColorPicker>wijmo.Control.getControl('#fcColorPicker');
    var ua = window.navigator.userAgent,
        blurEvt;

    if (colorPicker) {
        // if the browser is firefox, we should bind the blur event.
        // if the browser is IE, we should bind the focusout event.
        blurEvt = /firefox/i.test(ua) ? 'blur' : 'focusout';
        // Hide the color picker control when it lost the focus.
        colorPicker.hostElement.addEventListener(blurEvt, function () {
            setTimeout(function () {
                if (!colorPicker.containsFocus()) {
                    applyFillColor = false;
                    colorPicker.hostElement.style.display = 'none';
                }
            }, 0);
        });

        // Initialize the value changed event handler for the color picker control.
        colorPicker.valueChanged.addHandler(function () {
            if (applyFillColor) {
                ctxFormatCells.flexSheet.applyCellsStyle({ backgroundColor: colorPicker.value });
            } else {
                ctxFormatCells.flexSheet.applyCellsStyle({ color: colorPicker.value });
            }
        });
    }
}

function fcMenuFormat_Changed(sender) {
    var flexSheet = ctxFormatCells.flexSheet,
        menu = sender;
    if (menu.selectedValue) {
        ctxFormatCells.format = menu.selectedValue.CommandParameter;
        setMenuHeader(menu);
        if (flexSheet && !updatingSelection) {
            flexSheet.applyCellsStyle({ format: ctxFormatCells.format });
        }
    }
}

function setMenuHeader(menu) {
    menu.header = "Format:<b>" + menu.selectedValue === null ? "" : menu.selectedValue.Header + "</b>";
}

function fontChanged(sender) {
    if (!updatingSelection && ctxFormatCells.flexSheet) {
        ctxFormatCells.flexSheet.applyCellsStyle({ fontFamily: ctxFormatCells.cboFontName.selectedItem.Value });
    }
}

function fontSizeChanged(sender) {
    if (!updatingSelection && ctxFormatCells.flexSheet) {
        ctxFormatCells.flexSheet.applyCellsStyle({ fontSize: ctxFormatCells.cboFontSize.selectedItem.Value });
    }
}

// apply the text alignment for the selected cells
function applyCellTextAlign(textAlign) {
    ctxFormatCells.flexSheet.applyCellsStyle({ textAlign: textAlign });
    ctxFormatCells.selectionFormatState.textAlign = textAlign;
    formatCellsUpdateBtns();
};

// apply the bold font weight for the selected cells
function applyBoldStyle() {
    ctxFormatCells.flexSheet.applyCellsStyle({ fontWeight: ctxFormatCells.selectionFormatState.isBold ? 'none' : 'bold' });
    ctxFormatCells.selectionFormatState.isBold = !ctxFormatCells.selectionFormatState.isBold;
    formatCellsUpdateBtns();
};

// apply the underline text decoration for the selected cells
function applyUnderlineStyle() {
    ctxFormatCells.flexSheet.applyCellsStyle({ textDecoration: ctxFormatCells.selectionFormatState.isUnderline ? 'none' : 'underline' });
    ctxFormatCells.selectionFormatState.isUnderline = !ctxFormatCells.selectionFormatState.isUnderline;
    formatCellsUpdateBtns();
};

// apply the italic font style for the selected cells
function applyItalicStyle() {
    ctxFormatCells.flexSheet.applyCellsStyle({ fontStyle: ctxFormatCells.selectionFormatState.isItalic ? 'none' : 'italic' });
    ctxFormatCells.selectionFormatState.isItalic = !ctxFormatCells.selectionFormatState.isItalic;
    formatCellsUpdateBtns();
};

// show the color picker control.
function showColorPicker(e, isFillColor) {
    var colorPicker = ctxFormatCells.colorPicker,
        offset = cumulativeOffset(e.target),
        winWidth = document.body.clientWidth;

    if (colorPicker) {
        colorPicker.hostElement.style.display = 'inline';
        if (offset.left + colorPicker.hostElement.clientWidth > winWidth) {
            colorPicker.hostElement.style.left = 'auto';
            colorPicker.hostElement.style.right = '0px';
        } else {
            colorPicker.hostElement.style.right = 'auto';
            colorPicker.hostElement.style.left = offset.left + 'px';
        }
        colorPicker.hostElement.style.top = (offset.top - colorPicker.hostElement.clientHeight - 5) + 'px';
        colorPicker.hostElement.focus();
    }

    applyFillColor = isFillColor;
};

// Update the selection object of the scope.
function updateSelection(sel) {
    var flexSheet = ctxFormatCells.flexSheet,
        row = flexSheet.rows[sel.row],
        rowCnt = flexSheet.rows.length,
        colCnt = flexSheet.columns.length,
        r,
        c,
        cellStyle,
        cellContent,
        cellFormat;

    updatingSelection = true;
    if (ctxFormatCells.cboFontName && sel.row > -1 && sel.col > -1 && rowCnt > 0 && colCnt > 0
        && sel.col < colCnt && sel.col2 < colCnt
        && sel.row < rowCnt && sel.row2 < rowCnt) {
        r = sel.row >= rowCnt ? rowCnt - 1 : sel.row;
        c = sel.col >= colCnt ? colCnt - 1 : sel.col;
        cellContent = flexSheet.getCellData(sel.row, sel.col);
        cellStyle = flexSheet.selectedSheet.getCellStyle(sel.row, sel.col);
        if (cellStyle) {
            ctxFormatCells.cboFontName.selectedIndex = checkFontfamily(cellStyle.fontFamily);
            ctxFormatCells.cboFontSize.selectedIndex = checkFontSize(cellStyle.fontSize);
            cellFormat = cellStyle.format;
        } else {
            ctxFormatCells.cboFontName.selectedIndex = 0;
            ctxFormatCells.cboFontSize.selectedIndex = 5;
        }

        if (!!cellFormat) {
            ctxFormatCells.format = cellFormat;
        } else {
            if (wijmo.isInt(cellContent)) {
                ctxFormatCells.format = '0';
            } else if (wijmo.isNumber(cellContent)) {
                ctxFormatCells.format = 'n2';
            } else if (wijmo.isDate(cellContent)) {
                ctxFormatCells.format = 'd';
            }
        }
        ctxFormatCells.selectionFormatState = flexSheet.getSelectionFormatState()
        ctxFormatCells.menuFormat.selectedIndex = formats.indexOf(ctxFormatCells.format);
        formatCellsUpdateBtns();
    }
    updatingSelection = false;
};

// check font family for the font name combobox of the ribbon.
function checkFontfamily(fontFamily) {
    var fonts = ctxFormatCells.cboFontName.itemsSource.items,
        fontIndex = 0,
        font;

    if (!fontFamily) {
        return fontIndex;
    }

    for (; fontIndex < fonts.length; fontIndex++) {
        font = fonts[fontIndex];

        if (font.Name === fontFamily || font.Value === fontFamily) {
            return fontIndex;
        }
    }

    return 0;
}

// check font size for the font size combobox of the ribbon.
function checkFontSize(fontSize) {
    var sizeList = ctxFormatCells.cboFontSize.itemsSource.items,
        index = 0,
        size;

    if (fontSize == undefined) {
        return 5;
    }

    for (; index < sizeList.length; index++) {
        size = sizeList[index];

        if (size.Value === fontSize || size.Name === fontSize) {
            return index;
        }
    }

    return 5;
}

// Get the absolute position of the dom element.
function cumulativeOffset(element) {
    var top = 0, left = 0, scrollTop = 0, scrollLeft = 0;

    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        scrollTop += element.scrollTop || 0;
        scrollLeft += element.scrollLeft || 0;
        element = element.offsetParent;
    } while (element);

    return {
        top: top - scrollTop,
        left: left - scrollLeft
    };
};

function hasClass(obj, cls) {
    return obj && obj.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

function addClass(obj, cls) {
    if (!this.hasClass(obj, cls)) obj.className += " " + cls;
}

function removeClass(obj, cls) {
    if (hasClass(obj, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        obj.className = obj.className.replace(reg, ' ');
    }
}


                        </div>
                        <div Class="tab-pane pane-content" id="formatcCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div Class="col-md-6">
                <h4>
                結果（ライブ） :  
                </h4>
                @(Html.C1().FlexSheet().Id("fcFlexSheet").Height(300).AddUnboundSheet("Number", 20, 8) _
                    .AddUnboundSheet("Date", 20, 8)
                )
                @(Html.C1().ColorPicker().Id("fcColorPicker").CssStyle("display", "none") _
                    .CssStyle("position", "fixed").CssStyle("z-index", "100")
                )
                <div class="well well-lg">
                    <div>
                        Format:
                        @(Html.C1().Menu().Id("fcMenuFormat").Header("Format") _
            .OnClientSelectedIndexChanged("fcMenuFormat_Changed") _
            .MenuItems(Sub(mitem)
                           mitem.Add("10進数形式", "0")
                           mitem.Add("数値形式", "n2")
                           mitem.Add("パーセンテージ形式", "p2")
                           mitem.Add("通貨形式", "c2")
                           mitem.AddSeparator()
                           mitem.Add("短い日付", "d")
                           mitem.Add("長い日付", "D")
                           mitem.Add("完全な日時（短い時刻）", "f")
                           mitem.Add("完全な日時（長い時刻）", "F")
                       End Sub)
                        )
                    </div>
                    <div>
                        @Code
                            Dim fontList = ViewBag.FontList
                            Dim fontSizeList = ViewBag.FontSizeList
                        End Code
                                フォント:
                                @(Html.C1().ComboBox(Of FontName).Id("cboFontName").Bind(fontList) _
    .SelectedIndex(0).DisplayMemberPath("Name").SelectedValuePath("Value") _
    .IsEditable(False).CssStyle("width", "120px") _
    .OnClientSelectedIndexChanged("fontChanged") _
                                )
                                @(Html.C1().ComboBox(Of FontSize)().Id("cboFontSize").Bind(fontSizeList) _
    .SelectedIndex(5).DisplayMemberPath("Name").SelectedValuePath("Value") _
    .IsEditable(False).CssStyle("width", "80px").OnClientSelectedIndexChanged("fontSizeChanged")
                                )
                                <div class="btn-group">
                                    <button type="button" id="boldBtn" class="btn btn-default" onclick="applyBoldStyle()">太字</button>
                                    <button type="button" id="italicBtn" class="btn btn-default" onclick="applyItalicStyle()">斜体</button>
                                    <button type="button" id="underlineBtn" class="btn btn-default" onclick="applyUnderlineStyle()">下線</button>
                                </div>
                    </div>
                    <div>
                        色:
                        <div class="btn-group">
                            <button type="button" class="btn btn-default" onclick="showColorPicker(event, false)">前景色</button>
                            <button type="button" class="btn btn-default" onclick="showColorPicker(event, true)">塗りつぶし色</button>
                        </div>
                        配置:
                        <div class="btn-group">
                            <button type="button" id="leftBtn" class="btn btn-default active" onclick="applyCellTextAlign('left')">左揃え</button>
                            <button type="button" id="centerBtn" class="btn btn-default" onclick="applyCellTextAlign('center')">中央揃え</button>
                            <button type="button" id="rightBtn" class="btn btn-default" onclick="applyCellTextAlign('right')">右揃え</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- セル結合 -->
    <div>
        <h2 id="cellMerge">
            セル結合
        </h2>
        <p>
            FlexSheetは、<b>mergeRange</b>メソッドの呼び出しにより、選択された複数のセルを1つのセルに結合する操作をサポートします。
        </p>
        <p>
            選択されたセルに結合されたセルが含まれている場合、mergeRangeメソッドは、結合されたセルの結合を解除します。 そうでない場合は、選択されたセルを1つのセルに結合します。
        </p>
        <p>
            FlexSheetでは、含まれているデータにかかわらず、セルを結合できます。 これは、コンテンツ依存のセル結合をサポートするFlexGridと異なります。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#cellMergingHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#cellMergingJS" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#cellMergingCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="cellMergingHtml">

@@(Html.C1().FlexSheet().CssClass("flexSheet").Id("cellMergeSheet") _
    .Height(300).AddUnboundSheet("Sheet1", 20, 8))
&lt;button type="button" class="btn btn-default" onclick="mergeCells()" id="cellMergeBtn"&gt;Merge&lt;/button&gt;

                        </div>
                        <div class="tab-pane pane-content" id="cellMergingJS">

// セル結合
var cellMergeSheet = {
    flexSheet: null,
    selectionFormatState: {
        isMergedCell: null
    },
    mergeBtn: null
};

function loadcellMerging() {
    var flexSheet;
    cellMergeSheet.flexSheet = <wijmo.grid.sheet.FlexSheet>wijmo.Control.getControl('#cellMergeSheet');
    cellMergeSheet.mergeBtn = <HTMLButtonElement> document.getElementById('cellMergeBtn');
    flexSheet = cellMergeSheet.flexSheet;
    if (flexSheet) {
        for (var colIdx = 0; colIdx < flexSheet.columns.length; colIdx++) {
            for (var rowIdx = 0; rowIdx < flexSheet.rows.length; rowIdx++) {
                flexSheet.setCellData(rowIdx, colIdx, colIdx + rowIdx);
            }
        }
        flexSheet.selectionChanged.addHandler(function () {
            cellMergeSheet.selectionFormatState = flexSheet.getSelectionFormatState();
            cellMergeUpdateBtnText();
        });
    }
};

function cellMergeUpdateBtnText() {

    var updateBtnText = cellMergeSheet.selectionFormatState.isMergedCell ? 'UnMerge' : 'Merge';
    cellMergeSheet.mergeBtn.innerText = cellMergeSheet.selectionFormatState.isMergedCell ? 'UnMerge' : 'Merge';
};

function mergeCells() {
    var flexSheet = cellMergeSheet.flexSheet;

    if (flexSheet) {
        flexSheet.mergeRange();
        cellMergeSheet.selectionFormatState = flexSheet.getSelectionFormatState();
        cellMergeUpdateBtnText();
    }
};

                        </div>
                        <div class="tab-pane pane-content" id="cellMergingCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>
                    結果（ライブ）:
                </h4>
                @(Html.C1().FlexSheet().CssClass("flexSheet").Id("cellMergeSheet") _
                    .Height(300).AddUnboundSheet("Sheet1", 20, 8))
                <button type="button" class="btn btn-default" onclick="mergeCells()" id="cellMergeBtn">セルの結合</button>
            </div>
        </div>
    </div>

    <!-- ドラッグアンドドロップ -->
    <div>
        <h2 id="dragDrop">
            ドラッグアンドドロップ
        </h2>
        <p>
            FlexSheetは、列や行を別の列や行にドラッグアンドドロップする操作をサポートします。
        </p>
        <p>
            FlexSheetでは、セルのデータがコピーまたは移動されるだけでなく、セルのスタイルもコピーまたは移動されます。
        </p>
        <p>
            キーを何も押さずに列/行をドラッグアンドドロップすると、選択された列または行がドロップ先の列または行に移動します。
        </p>
        <p>
            <b>［Ctrl］</b>キーを押しながら列/行をドラッグアンドドロップすると、選択された列または行がドロップ先の列または行にコピーされます。
        </p>
        <p>
            <b>［Shift］</b>キーを押しながら列/行をドラッグアンドドロップすると、選択された列または行の位置がドロップ先の列または行と入れ替わります。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#ddropHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#ddropJS" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#ddropCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="ddropHtml">

@@(Html.C1().FlexSheet().Id("dragDropSheet").Height(300) _
    .AddUnboundSheet("Sheet1", 20, 8))

                        </div>
                        <div class="tab-pane pane-content" id="ddropJS">

// ドラッグアンドドロップ
var ctxDragDrop = {
    flexSheet: null
};

function loadDragDrop() {
    var flexSheet;
    ctxDragDrop.flexSheet = <wijmo.grid.sheet.FlexSheet>wijmo.Control.getControl('#dragDropSheet');
    flexSheet = ctxDragDrop.flexSheet;
    if (flexSheet) {
        for (var colIdx = 0; colIdx < flexSheet.columns.length; colIdx++) {
            for (var rowIdx = 0; rowIdx < flexSheet.rows.length; rowIdx++) {
                flexSheet.setCellData(rowIdx, colIdx, colIdx + rowIdx);
            }
        }
        flexSheet.applyCellsStyle({ fontWeight: 'bold' }, [new wijmo.grid.CellRange(0, 0, 9, 0),
            new wijmo.grid.CellRange(10, 1, 19, 1)]);
        flexSheet.applyCellsStyle({ textDecoration: 'underline' }, [new wijmo.grid.CellRange(0, 2, 9, 2),
            new wijmo.grid.CellRange(10, 3, 19, 3)]);
        flexSheet.applyCellsStyle({ fontStyle: 'italic' }, [new wijmo.grid.CellRange(0, 4, 9, 4),
            new wijmo.grid.CellRange(10, 5, 19, 5)]);
        flexSheet.applyCellsStyle({ format: 'c2' }, [new wijmo.grid.CellRange(0, 0, 9, 7)]);
        flexSheet.applyCellsStyle({ backgroundColor: '#4488CC' }, [new wijmo.grid.CellRange(0, 0, 19, 0),
            new wijmo.grid.CellRange(0, 2, 19, 2), new wijmo.grid.CellRange(0, 4, 19, 4)]);
        flexSheet.applyCellsStyle({ color: '#CC8844' }, [new wijmo.grid.CellRange(0, 1, 19, 1),
            new wijmo.grid.CellRange(0, 3, 19, 3), new wijmo.grid.CellRange(0, 5, 19, 5)]);
        flexSheet.applyCellsStyle({ color: '#336699' }, [new wijmo.grid.CellRange(0, 6, 9, 7)]);
        flexSheet.applyCellsStyle({ backgroundColor: '#996633' }, [new wijmo.grid.CellRange(10, 6, 19, 7)]);
    }
};

                        </div>
                        <div class="tab-pane pane-content" id="ddropCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>
                    結果（ライブ）:
                </h4>
                @(Html.C1().FlexSheet().Id("dragDropSheet").Height(300) _
        .AddUnboundSheet("Sheet1", 20, 8))
            </div>
        </div>
    </div>

    <!-- 固定セル -->
    <div>
        <h2 id="frozenCells">
            固定セル
        </h2>
        <p>
            FlexSheetでは、<b>freezeAtCursor</b>メソッドを使用して、選択されたセルの行や列を固定することができます。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#frozenCellsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#frozenCellsJS" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#frozenCellsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="frozenCellsHtml">

@@(Html.C1().FlexSheet().Id("frozenSheet").AddUnboundSheet("Sheet1", 20, 8).Height(300) _
    .OnClientSelectedSheetChanged("frozenSheet_updateFrozenState"))
&lt;button type="button" class="btn btn-default" onclick="freezeCells()" id="frozenBtn"&gt;Freeze&lt;/button&gt;

                        </div>
                        <div class="tab-pane pane-content" id="frozenCellsJS">

// 固定セル
var ctxfrozenCells = {
    flexSheet: null,
    isFrozen: false,
    frozenBtn: null,
    mergeBtn:null
};

function loadFrozenCells() {
    var flexSheet;
    ctxfrozenCells.flexSheet = <wijmo.grid.sheet.FlexSheet>wijmo.Control.getControl('#frozenSheet');
    ctxfrozenCells.mergeBtn = wijmo.getElement('#frozenBtn');
    flexSheet = ctxfrozenCells.flexSheet;
    if (flexSheet) {
        for (var colIdx = 0; colIdx < flexSheet.columns.length; colIdx++) {
            for (var rowIdx = 0; rowIdx < flexSheet.rows.length; rowIdx++) {
                flexSheet.setCellData(rowIdx, colIdx, colIdx + rowIdx);
            }
        }
    }
};

function frozenCellsUpdateBtn() {
    ctxfrozenCells.mergeBtn.innerText = ctxfrozenCells.isFrozen ? 'UnFreeze' : 'Freeze';
}

function freezeCells() {
    var flexSheet = ctxfrozenCells.flexSheet;
    if (flexSheet) {
        flexSheet.freezeAtCursor();
        frozenSheet_updateFrozenState();
    }
}

function frozenSheet_updateFrozenState() {
    var flexSheet = ctxfrozenCells.flexSheet;
    if (flexSheet) {
        if (ctxfrozenCells.flexSheet.frozenColumns > 0 || ctxfrozenCells.flexSheet.frozenRows > 0) {
            ctxfrozenCells.isFrozen = true;
        } else {
            ctxfrozenCells.isFrozen = false;
        }

        frozenCellsUpdateBtn();
    }
}
                        </div>
                        <div class="tab-pane pane-content" id="frozenCellsCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>
                    結果（ライブ）:
                </h4>
                @(Html.C1().FlexSheet().Id("frozenSheet").AddUnboundSheet("Sheet1", 20, 8).Height(300) _
                    .OnClientSelectedSheetChanged("frozenSheet_updateFrozenState"))

                <button type="button" class="btn btn-default" onclick="freezeCells()" id="frozenBtn">固定</button>
            </div>
        </div>
    </div>

    <!-- Undo/Redo -->
    <div>
        <h2 id="undoRedo">
            元に戻す/やり直し
        </h2>
        <p>
            FlexSheetコントロールでは、次の操作を元に戻す/やり直すことができます。
        </p>
        <ol>
            <li><b>セルの編集</b></li>
            <li><b>行/列のサイズ変更</b></li>
            <li><b>行/列の追加/削除</b></li>
            <li><b>セルスタイルの変更</b></li>
            <li><b>セルの結合</b></li>
            <li><b>ソート</b></li>
            <li><b>行/列のドラッグ＆ドロップ</b></li>
        </ol>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#uredoHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#uredoJS" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#uredoCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="uredoHtml">

@@(Html.C1().FlexSheet().Id("uredoSheet") _
    .AddUnboundSheet("Sheet1", 20, 8).Height(300)
)
&lt;button id="btnUndo" type="button" class="btn btn-default" onclick="undoFunc()"&gt;Undo&lt;/button&gt;
&lt;button id="btnRedo" type="button" class="btn btn-default" onclick="redoFunc()"&gt;Redo&lt;/button&gt;

                        </div>
                        <div class="tab-pane pane-content" id="uredoJS">

// Undo/Redo
var ctxuredoSheet = {
    flexSheet: null,
    undoStack: null
};


function loadUndoRedo() {
    var flexSheet;
    ctxuredoSheet.flexSheet = <wijmo.grid.sheet.FlexSheet>wijmo.Control.getControl('#uredoSheet');
    flexSheet = ctxuredoSheet.flexSheet;
    flexSheet.deferUpdate(function () {
        var colIdx,
            rowIdx;

        ctxuredoSheet.undoStack = flexSheet.undoStack;
        // initialize the dataMap for the bound sheet.
        if (flexSheet) {
            for (colIdx = 0; colIdx < flexSheet.columns.length; colIdx++) {
                for (rowIdx = 0; rowIdx < flexSheet.rows.length; rowIdx++) {
                    flexSheet.setCellData(rowIdx, colIdx, colIdx + rowIdx);
                }
            }
        }
    });
};

// Excutes undo command.
function undoFunc() {
    if (ctxuredoSheet.flexSheet)
        ctxuredoSheet.flexSheet.undo();
};

// Excutes redo command.
function redoFunc() {
    if (ctxuredoSheet.flexSheet)
        ctxuredoSheet.flexSheet.redo();
};


                        </div>
                        <div class="tab-pane pane-content" id="uredoCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>
                    結果（ライブ）:
                </h4>
                @(Html.C1().FlexSheet().Id("uredoSheet") _
                    .AddUnboundSheet("Sheet1", 20, 8).Height(300)
                )
                <button id="btnUndo" type="button" class="btn btn-default" onclick="undoFunc()">元に戻す</button>
                <button id="btnRedo" type="button" class="btn btn-default" onclick="redoFunc()">やり直し</button>
            </div>
        </div>
    </div>


    <!-- Formulas -->
    <div>
        <h2 id="formulas">
            数式
        </h2>
        <p>
            FlexSheetコントロールは組み込みの計算エンジンを備えており、Microsoft Excelと同様の80を超える関数をサポートしています。 (<a href="http://c1.grapecity.com/help/web/aspmvc/aspmvc_helpers/In-builtFormulasSupportedinFlexSheet.html" target="_blank">完全なリストはこちら</a>)
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#formulasHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#formulasJS" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#formulasCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="formulasHtml">

@@(Html.C1().FlexSheet().Id("formulaSheet") _
    .AddUnboundSheet("Expense Report", 14, 6).Height(300)
)
&lt;div&gt;
    &lt;b&gt;セルコンテンツ: &lt;/b&gt;&lt;span id="dvCurrentCellData"&gt;&lt;/span&gt;
&lt;/div&gt;

                        </div>
                        <div class="tab-pane pane-content" id="formulasJS">

// 数式
var ctxFormulas = {
    flexSheet: null,
    currentCellData: null
};

function loadFormulasSheet() {
    ctxFormulas.flexSheet = <wijmo.grid.sheet.FlexSheet> wijmo.Control.getControl('#formulaSheet');
    var flexSheet = ctxFormulas.flexSheet;
    flexSheet.selectionChanged.addHandler(function (sender, args) {
        var selection = args.range;
        if (selection.isValid) {
            ctxFormulas.currentCellData = ctxFormulas.flexSheet.getCellData(selection.row, selection.col, true);
            document.getElementById('dvCurrentCellData').innerText = ctxFormulas.currentCellData;
        }
    });
    flexSheet.deferUpdate(function () {
        generateExpenceReport(flexSheet);
    });
};

// Set content for the use case template sheet.
function generateExpenceReport(flexSheet) {
    flexSheet.setCellData(1, 1, 'Expense Report');
    flexSheet.setCellData(3, 1, 'Date');
    flexSheet.setCellData(3, 2, 'Fuel');
    flexSheet.setCellData(3, 3, 'Parking(per hour)');
    flexSheet.setCellData(3, 4, 'Parking(hours)');
    flexSheet.setCellData(3, 5, 'Total');;
    flexSheet.setCellData(9, 1, 'Total');
    flexSheet.setCellData(10, 4, 'Subtotal');
    flexSheet.setCellData(11, 4, 'Cash Advances');
    flexSheet.setCellData(12, 4, 'Total');

    setExpenseData(flexSheet);

    applyStyleForExpenceReport(flexSheet);
}

// set expense detail data for the use case template sheet.
function setExpenseData(flexSheet) {
    var rowIndex,
        colIndex,
        value;

    for (rowIndex = 4; rowIndex <= 8; rowIndex++) {
        for (colIndex = 2; colIndex <= 5; colIndex++) {
            if (colIndex === 5) {
                flexSheet.setCellData(rowIndex, colIndex, '=C' + (rowIndex + 1) + ' + Product(C' + (rowIndex + 1) + ':D' + (rowIndex + 1) + ')');
            } else if (colIndex === 4) {
                value = parseInt((7 * Math.random()).toString()) + 1;
                flexSheet.setCellData(rowIndex, colIndex, value);
            } else if (colIndex === 3) {
                flexSheet.setCellData(rowIndex, colIndex, 3.75);
            } else {
                value = 200 * Math.random();
                flexSheet.setCellData(rowIndex, colIndex, value);
            }
        }
    }

    flexSheet.setCellData(4, 1, '2015-3-1');
    flexSheet.setCellData(5, 1, '2015-3-3');
    flexSheet.setCellData(6, 1, '2015-3-7');
    flexSheet.setCellData(7, 1, '2015-3-11');
    flexSheet.setCellData(8, 1, '2015-3-18');
    flexSheet.setCellData(9, 2, '=Sum(C5:C9)');
    flexSheet.setCellData(9, 4, '=Sum(Product(D5:E5), Product(D6:E6), Product(D7:E7), Product(D8:E8), Product(D9:E9))');
    flexSheet.setCellData(9, 5, '=Sum(F5:F9)');
    flexSheet.setCellData(10, 5, '=F13-F12');
    flexSheet.setCellData(11, 5, 800);
    flexSheet.setCellData(12, 5, '=F10');
}

// Apply styles for the use case template sheet.
function applyStyleForExpenceReport(flexSheet) {
    flexSheet.columns[0].width = 10;
    flexSheet.columns[1].width = 90;
    flexSheet.columns[2].width = 80;
    flexSheet.columns[3].width = 140;
    flexSheet.columns[4].width = 120;
    flexSheet.columns[5].width = 80;
    for (var i = 2; i <= 3; i++) {
        flexSheet.columns[i].format = 'c2';
    }
    flexSheet.columns[5].format = 'c2';
    flexSheet.rows[1].height = 45;
    flexSheet.applyCellsStyle({
        fontSize: '24px',
        fontWeight: 'bold',
        color: '#696964'
    }, [new wijmo.grid.CellRange(1, 1, 1, 3)]);
    flexSheet.mergeRange(new wijmo.grid.CellRange(1, 1, 1, 3));
    flexSheet.applyCellsStyle({
        fontWeight: 'bold',
        backgroundColor: '#FAD9CD',
    }, [new wijmo.grid.CellRange(3, 1, 3, 5),
            new wijmo.grid.CellRange(9, 1, 9, 5)]);
    flexSheet.applyCellsStyle({
        textAlign: 'center'
    }, [new wijmo.grid.CellRange(3, 1, 3, 5)]);
    flexSheet.applyCellsStyle({
        format: 'c2'
    }, [new wijmo.grid.CellRange(9, 4, 9, 4)]);
    flexSheet.applyCellsStyle({
        backgroundColor: '#F4B19B'
    }, [new wijmo.grid.CellRange(4, 1, 8, 5)]);
    flexSheet.applyCellsStyle({
        fontWeight: 'bold',
        textAlign: 'right'
    }, [new wijmo.grid.CellRange(10, 4, 12, 4)]);
}

                        </div>
                        <div class="tab-pane pane-content" id="formulasCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>
                    結果（ライブ）:
                </h4>
                @(Html.C1().FlexSheet().Id("formulaSheet") _
                    .AddUnboundSheet("Expense Report", 14, 6).Height(300)
                )
                <div><b>セルコンテンツ: </b><span id="dvCurrentCellData"></span></div>
            </div>
        </div>
    </div>

    <!-- カスタム関数 -->
    <div>
        <h2 id="customFunction">
            カスタム関数
        </h2>
        <p>
            FlexSheetで提供されている関数はほとんどの使用シナリオに対応できるはずですが、それでも場合によっては、追加の関数が必要になることがあります。
        </p>
        <p>
            FlexSheetでは、独自のカスタム関数を追加するための2つのメソッド、<b>addFunction</b>と<b>unknownFunction</b>が提供されています。
        </p>
        <p>
            <b>addFunction</b>メソッドは、カスタム関数を組み込み関数のリストに追加します。
        </p>
        <p>
            通常は、addFunctionメソッドがカスタム関数をFlexSheet計算エンジンに追加するための最適な方法です。 しかし、関数名が可変であったり、事前にわからない場合があります。 たとえば、名前付き範囲や値の辞書の場合です。
        </p>
        <p>
            このような場合は、<b>unknownFunction</b>イベントを使用して、関数の値を動的に検索することができます。 FlexSheetは、不明な関数名を検出すると、unknownFunctionイベントを発生させ、関数名とパラメータを含むパラメータを提供します。 次に、イベントハンドラで結果を計算し、値を返します。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#cFunctionHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#cFunctionJS" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#cFunctionCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="cFunctionHtml">

@@(Html.C1().FlexSheet().Id("cFunctionSheet").Height(300) _
    .AddUnboundSheet("", 25, 12).OnClientUnknownFunction("cFunctionSheet_unknownFunction"))

                        </div>
                        <div class="tab-pane pane-content" id="cFunctionJS">

// カスタム関数
var ctxcFunctionSheet = {
    flexSheet: null
};

function cFunctionSheet_unknownFunction(sender, e) {
    var result = '';
    if (e.params) {
        for (var i = 0; i < e.params.length; i++) {
            result += e.params[i];
        }
    }
    e.value = result;
};

function loadCustomFunction() {
    var flexSheet = ctxcFunctionSheet.flexSheet = <wijmo.grid.sheet.FlexSheet>wijmo.Control.getControl('#cFunctionSheet');

    flexSheet.addFunction('customSumProduct', function (range1, range2) {
        var result = 0;

        if (range1.length > 0 && range1.length === range2.length && range1[0].length === range2[0].length) {
            for (var i = 0; i < range1.length; i++) {
                for (var j = 0; j < range1[0].length; j++) {
                    result += range1[i][j] * range2[i][j];
                }
            }
        }

        return result;
    }, 'Custom SumProduct Function', 2, 2);

    for (var ri = 0; ri < flexSheet.rows.length; ri++) {
        for (var ci = 0; ci < 3; ci++) {
            flexSheet.setCellData(ri, ci, ri + ci);
        }
    }

    flexSheet.setCellData(0, 3, '=customSumProduct(A1:A10, B1:B10)');
    flexSheet.setCellData(1, 3, '=customFunc(1, "B", 3)');

};

                        </div>
                        <div class="tab-pane pane-content" id="cFunctionCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>
                    結果（ライブ）:
                </h4>
                @(Html.C1().FlexSheet().Id("cFunctionSheet").Height(300) _
                    .AddUnboundSheet("", 25, 12).OnClientUnknownFunction("cFunctionSheet_unknownFunction"))
            </div>
        </div>
    </div>

    <!-- Excel入出力 -->
    <div>
        <h2 id="excelIO">
            Excel入出力
        </h2>
        <p>
            FlexSheetは、<b>save</b>および<b>load</b>クライアントメソッドにより、xlsxファイルの保存/ロードをサポートします。
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#excelIOHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#excelIOJS" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#excelIOCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="excelIOHtml">

@@(Html.C1().FlexSheet().CssClass("flexSheet").Id("excelIOSheet").SelectedSheetIndex(0).Height(300) _
    .AddBoundSheet(Sub(sheet) sheet.Bind(Model.CountryData).Name("Country")) _
    .AddUnboundSheet("Unbound", 20, 8)
)
&lt;div class="form-inline well well-lg"&gt;
    &lt;input type="file" class="form-control" id="importFile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" /&gt;
    &lt;button class="btn btn-default" onclick="excelIOLoad()"&gt;Load&lt;/button&gt;
&lt;/div&gt;
&lt;div class="form-inline well well-lg"&gt;
    ファイル名:
    &lt;input type="text" class="form-control" id="fileName" onchange="fileNameChanged()" /&gt;
    &lt;button class="btn btn-default" onclick="excelIOSave()"&gt;Save&lt;/button&gt;
&lt;/div&gt;

                        </div>
                        <div class="tab-pane pane-content" id="excelIOJS">

// Excel入出力
var ctxcExcelIO = {
    fileName: '',
    flexSheet: null,
    fileNameInput: null,
    fileInput: null
};

function loadExcelIO() {
    var flexSheet;
    ctxcExcelIO.flexSheet = <wijmo.grid.sheet.FlexSheet>wijmo.Control.getControl('#excelIOSheet');
    ctxcExcelIO.fileNameInput = <HTMLInputElement>document.getElementById('fileName');
    ctxcExcelIO.fileInput = <HTMLInputElement>document.getElementById('importFile');
    flexSheet = ctxcExcelIO.flexSheet;
    if (flexSheet) {
        for (var sheetIdx = 0; sheetIdx < flexSheet.sheets.length; sheetIdx++) {
            flexSheet.selectedSheetIndex = sheetIdx;
            var sheetName = flexSheet.selectedSheet.name;
            if (sheetName === 'Unbound') {
                for (var colIdx = 0; colIdx < flexSheet.columns.length; colIdx++) {
                    for (var rowIdx = 0; rowIdx < flexSheet.rows.length; rowIdx++) {
                        flexSheet.setCellData(rowIdx, colIdx, colIdx + rowIdx);
                    }
                }
            } else {
                applyDataMap(flexSheet);
            }
        }
        flexSheet.selectedSheetIndex = 0;
    }
};

function excelIOLoad() {
    var flexSheet = ctxcExcelIO.flexSheet,
        fileInput = ctxcExcelIO.fileInput;
    if (flexSheet && fileInput.files[0]) {
        flexSheet.load(fileInput.files[0]);
    }
}

function excelIOSave() {
    var flexSheet = ctxcExcelIO.flexSheet,
        fileName;
    if (flexSheet) {
        if (!!ctxcExcelIO.fileName) {
            fileName = ctxcExcelIO.fileName;
        } else {
            fileName = 'FlexSheet.xlsx';
        }
        flexSheet.save(fileName);
    }
}

function fileNameChanged() {
    ctxcExcelIO.fileName = ctxcExcelIO.fileNameInput.value;
}

function applyDataMap(flexSheet) {
    var countries = ['US', 'Germany', 'UK', 'Japan', 'Italy', 'Greece'],
        products = ['Widget', 'Gadget', 'Doohickey'], column;
    // initialize the dataMap for the bound sheet.
    if (flexSheet) {
        column = flexSheet.columns.getColumn('Country');
        if (column && !column.dataMap) {
            column.dataMap = buildDataMap(countries);
        }
        column = flexSheet.columns.getColumn('Product');
        if (column && !column.dataMap) {
            column.dataMap = buildDataMap(products);
        }
    }
}

function buildDataMap(items) {
    var map = [];
    for (var i = 0; i < items.length; i++) {
        map.push({ key: i, value: items[i] });
    }
    return new wijmo.grid.DataMap(map, 'key', 'value');
}

                        </div>
                        <div class="tab-pane pane-content" id="excelIOCS">

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>
                    結果（ライブ）:
                </h4>
                @(Html.C1().FlexSheet().CssClass("flexSheet").Id("excelIOSheet").SelectedSheetIndex(0).Height(300) _
                           .AddBoundSheet(Sub(Sheet) Sheet.Bind(Model.CountryData).Name("Country")) _
                            .AddUnboundSheet("Unbound", 20, 8)
                )
                <div class="form-inline well well-lg">
                    <input type="file" class="form-control" id="importFile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                    <button class="btn btn-default" onclick="excelIOLoad()">ロード</button>
                </div>
                <div class="form-inline well well-lg">
                    ファイル名:
                    <input type="text" class="form-control" id="fileName" onchange="fileNameChanged()" />
                    <button class="btn btn-default" onclick="excelIOSave()">保存</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    c1.documentReady(function () {
        if (window["InitialControls"]) {
            window["InitialControls"]();
        }
    });
</script>