﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace PeriodicTable.Models
{
    public class DataSource
    {
        private List<Group> _groups;

        public List<Group> Groups
        {
            get
            {
                if (_groups == null)
                    _groups = new List<Group>();
                return _groups;
            }
        }

        public DataSource()
        {
            var metals = new Group("金属");
            //Add subgroups to metals
            metals.SubGroups.Add(new SubGroup("アルカリ金属") { Characteristics = "光沢,軟らか,高反応性,低融点" });
            metals.SubGroups.Add(new SubGroup("アルカリ土類金属") { Characteristics = "延性,可鍛性,低密度,高融点" });
            metals.SubGroups.Add(new SubGroup("遷移金属") { Characteristics = "高融点,高密度" });
            metals.SubGroups.Add(new SubGroup("ランタニド") { Characteristics = "可溶性,高反応性" });
            metals.SubGroups.Add(new SubGroup("アクチニド") { Characteristics = "放射性,常磁性" });
            metals.SubGroups.Add(new SubGroup("その他") { Characteristics = "脆い,貧弱な金属,低融点" });

            var nonmetals = new Group("非金属");
            //Add subgroups to non-metals
            nonmetals.SubGroups.Add(new SubGroup("ハロゲン") { Characteristics = "有毒,高反応性,低導体" });
            nonmetals.SubGroups.Add(new SubGroup("希ガス") { Characteristics = "無色,無臭,低化学反応性" });
            nonmetals.SubGroups.Add(new SubGroup("その他") { Characteristics = "揮発性,低弾性,絶縁体" });

            var others = new Group("その他");
            //Add subgroups to others
            others.SubGroups.Add(new SubGroup("メタロイド") { Characteristics = "金属のような固体,半導体" });
            others.SubGroups.Add(new SubGroup("トランスアクチニド") { Characteristics = "放射性,合成元素" });

            Groups.Add(metals);
            Groups.Add(nonmetals);
            Groups.Add(others);
            GroupElements();
            for (int gCount = 0; gCount < Groups.Count; gCount++)
            {
                for (int sgCount = 0; sgCount < Groups[gCount].SubGroups.Count; sgCount++)
                {
                    var temp = Groups[gCount].SubGroups[sgCount];
                    Groups[gCount].SubGroups[sgCount].Count = temp.Elements.Count;
                }
            }
        }

        void GroupElements()
        {
            var path = HttpContext.Current.Server.MapPath("~/Content/Periodic Table of Elements.xml");
            var elementsCollection = Utils.DeserializeXml(path);
            var metals = "アルカリ金属|アルカリ土類金属|金属|遷移金属|ランタニド|アクチニド".Split('|');
            var nonmetals = "非金属|希ガス|ハロゲン".Split('|');
            Group group;
            SubGroup sub;
            for (int i = 0; i < elementsCollection.Elements.Length; i++)
            {
                var e = elementsCollection.Elements[i];
                if (metals.Contains(e.Element.Type))
                {
                    group = Groups[0];
                    if (e.Element.Type == "金属")
                        sub = group.SubGroups.Find(s => s.SubGroupName.Equals("その他"));
                    else
                        sub = group.SubGroups.Find(s => s.SubGroupName.Equals(e.Element.Type));
                    sub.Elements.Add(e.Element);
                }
                else if (nonmetals.Contains(e.Element.Type))
                {
                    group = Groups[1];
                    if (e.Element.Type == "非金属")
                        sub = group.SubGroups.Find(s => s.SubGroupName.Equals("その他"));
                    else
                        sub = group.SubGroups.Find(s => s.SubGroupName.Equals(e.Element.Type));
                    sub.Elements.Add(e.Element);
                }
                else
                {
                    group = Groups[2];
                    if (e.Element.Type == "金属loid")
                        sub = group.SubGroups.Find(s => s.SubGroupName.Equals("メタロイド"));
                    else
                        sub = group.SubGroups.Find(s => s.SubGroupName.Equals("トランスアクチニド"));
                    sub.Elements.Add(e.Element);
                }
            }
        }
    }

    public class Group
    {
        private List<SubGroup> _subGroups;
        public string GroupName { get; set; }
        public List<SubGroup> SubGroups
        {
            get
            {
                if (_subGroups == null)
                    _subGroups = new List<SubGroup>();
                return _subGroups;
            }

        }
        public Group() { }
        public Group(string name)
        {
            GroupName = name;
        }
    }

    public class SubGroup
    {
        private List<Element> _elements;

        public string SubGroupName { get; set; }

        public string Characteristics { get; set; }

        public int Count { get; set; }

        public List<Element> Elements
        {
            get
            {
                if (_elements == null)
                    _elements = new List<Element>();
                return _elements;
            }
        }

        public SubGroup() { }

        public SubGroup(string name)
        {
            SubGroupName = name;
        }
    }
}