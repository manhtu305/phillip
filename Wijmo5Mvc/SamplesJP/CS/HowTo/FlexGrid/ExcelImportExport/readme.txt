﻿[サンプル名]ExcelImportExport
[カテゴリ]FlexGrid for ASP.NET MVC
------------------------------------------------------------------------
[概要]
FlexGridでExcelファイルをインポート／エクスポートする方法を示します。

xlsx.jsライブラリ（https://github.com/SheetJS/js-xlsx）を使用して、JavaScriptだけでExcelファイルをFlexGridにインポートしたり、FlexGridをExcelファイルにエクスポートできます。サーバー側のコードは不要です。このサンプルでは、xlsx.jsライブラリを修正したc1xlsx.jsファイル（scriptsフォルダ内）を使用してExcelの基本的なエクスポート機能とインポート機能を提供し、jszip.jsライブラリ（https://github.com/Stuk/jszip）を使用してExcelファイルを読み書きします。

■エクスポート
export関数はExcelConverterファイルで実装され、FlexGridインスタンスを入力として受け取り、そのデータと書式設定をxlsx.jsライブラリを使用してExcel形式に変換し、Excelファイルのコンテンツを含むオブジェクトを返します。
exportExcel関数はScripts\app.jsファイルで実装され、このオブジェクトを受け取り、ローカルディスク上のファイルに保存します。

■インポート
import関数はExcelConverter\ExcelConverter.jsファイルで実装され、Excelファイルの内容を入力として受け取り、xlsx.jsライブラリを使用して解析し、指定されたFlexGridインスタンスを解析データで満たします。 
importExcel関数はScripts\app.jsファイルで実装され、選択されたファイルの内容をディスクから読み取り、HTMLページで定義されたFlexGridインスタンスと一緒にExcelConverter.import関数に渡します。 

■アプリケーションへの追加方法
Excelのインポートまたはエクスポートのサポートをアプリケーションに追加するには、以下の手順に従います。
1. このサンプルから、c1xlsx.jsとExcelConverter.jsファイルを各アプリケーションに追加します。
2. Layout.cshtmlページで、次のファイルへの参照を追加します。
　・jszip.jsライブラリ（以下のCDNに置かれています）。
　　http://cdnjs.cloudflare.com/ajax/libs/jszip/2.2.1/jszip.min.js
　・c1xlsx.js
　・ExcelConverter.js
3. exportExcel関数（Exportコードの［JS］タブ）からコードを追加します。この関数はエクスポート結果をローカルファイルに保存します。
4. importExcel関数（Importコードの［JS］タブ）からコードを追加します。この関数はディスクからExcelファイルを読み取ります。
