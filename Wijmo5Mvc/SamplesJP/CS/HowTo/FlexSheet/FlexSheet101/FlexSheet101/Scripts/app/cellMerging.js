// Cell Merging
var cellMergeSheet = {
    flexSheet: null,
    selectionFormatState: {},
    mergeBtn: null
};

function loadcellMerging() {
    var flexSheet;
    cellMergeSheet.flexSheet = wijmo.Control.getControl('#cellMergeSheet');
    cellMergeSheet.mergeBtn = document.getElementById('cellMergeBtn');//wijmo.getElement('#cellMergeBtn');
    flexSheet = cellMergeSheet.flexSheet;
    if (flexSheet) {
        for (colIdx = 0; colIdx < flexSheet.columns.length; colIdx++) {
            for (rowIdx = 0; rowIdx < flexSheet.rows.length; rowIdx++) {
                flexSheet.setCellData(rowIdx, colIdx, colIdx + rowIdx);
            }
        }
        flexSheet.selectionChanged.addHandler(function () {
            cellMergeSheet.selectionFormatState = flexSheet.getSelectionFormatState();
            cellMergeUpdateBtnText();
        });
    }
};

function cellMergeUpdateBtnText() {
    var updateBtnText = cellMergeSheet.selectionFormatState.isMergedCell ? 'UnMerge' : 'Merge';
    cellMergeSheet.mergeBtn.innerText = cellMergeSheet.selectionFormatState.isMergedCell ? 'UnMerge' : 'Merge';
};

function mergeCells() {
    var flexSheet = cellMergeSheet.flexSheet;

    if (flexSheet) {
        flexSheet.mergeRange();
        cellMergeSheet.selectionFormatState = flexSheet.getSelectionFormatState();
        cellMergeUpdateBtnText();
    }
};
