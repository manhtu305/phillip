
function InitialControls() {
    loadSorting();
    loadFormatCells();
    loadcellMerging();
    loadDragDrop();
    loadFrozenCells();
    loadUndoRedo();
    loadFormulasSheet();
    loadCustomFunction();
    loadExcelIO();
};
