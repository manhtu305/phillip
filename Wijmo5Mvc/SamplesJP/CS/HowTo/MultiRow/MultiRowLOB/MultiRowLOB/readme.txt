[サンプル名]MultiRowLOB
[カテゴリ]MultiRow for ASP.NET MVC
------------------------------------------------------------------------
[概要]
MultiRowコントロールを使用して、伝票や帳票のような様々なレイアウトでデータ入力画面を作成します。