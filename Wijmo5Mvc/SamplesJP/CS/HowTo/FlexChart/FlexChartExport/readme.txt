﻿[サンプル名]FlexChartExport
[カテゴリ]FlexChart for ASP.NET MVC
------------------------------------------------------------------------
[概要]
このサンプルでは、canvgとFileSaver.jsを使用してFlexChartコントロールをPNG／JPG／BMP画像にエクスポートする方法を示します。

canvgは、SVGを解析してHTML Canvas要素として描画するJavaScriptライブラリです。
詳細な情報は、https://code.google.com/p/canvg/を参照してください。

FileSaver.jsは、クライアント側でより簡単にファイルを保存できるJavaScriptライブラリです。
詳細な情報は、https://github.com/eligrey/FileSaver.js/を参照してください。
