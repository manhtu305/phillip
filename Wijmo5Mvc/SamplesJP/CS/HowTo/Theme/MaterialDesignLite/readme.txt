﻿[サンプル名]MaterialDesignLite
[カテゴリ]Theme
------------------------------------------------------------------------
[概要]
GoogleのMaterial Design LiteでComponentOne MVC for ASP.NETを使用する方法を示します。

Material Design Lite（MDL）は、Googleがウェブデベロッパー向けにMaterial Design仕様を実装したものです。これはフレームワークにとらわれず、最新のアプリケーションに美しいレイアウトを提供します。 MDLは、私たちがMVCサンプルで広く使用するBootstrap CSSの良い代替手段です。

MDLの詳細については、次のリンクを参照してください。

https://medium.com/google-developers/introducing-material-design-lite-3ce67098c031#.hdd60hunf

https://www.getmdl.io/

このサンプルは、MDLのカスタマイズページで使用できるコントロールと同様のColorWheelコントロールを実装しています。ユーザーはホイールをクリックして、プライマリとアクセントの色を選択できます。これらは自動的にダウンロードされてページに適用されるテーマを定義します。

アプリケーションは、選択したテーマへのCDNリンクを表示し、ユーザーは縮小されたCSSをダウンロードできます。 ComponentOne MVCには、プライマリカラーとアクセントカラーのすべての有効なコンビネーションを表す286のマテリアルテーマが含まれています。

このサンプルには、「ブートストラップ」機能を提供するwijmo.material.jsファイルも含まれています。この関数は、ドキュメントの読み込みと2つのタスクの実行時に自動的に呼び出されます。

1）MDLタブの要素を監視し、ユーザーがタブを切り替えたときにComponentOne MVCコントロールを無効にします。これにより、ComponentOne MVCコントロールは、表示されるとレイアウトを更新できます。

2）MDL TextField要素に含まれるComponentOne MVC入力要素のイベントハンドラを追加して、状態属性（フォーカス、ダーティ、無効）を更新します。これにより、ネイティブ入力要素を使用するのとまったく同じ方法で、MDL TextFieldでComponentOne MVC入力要素を使用することができます。