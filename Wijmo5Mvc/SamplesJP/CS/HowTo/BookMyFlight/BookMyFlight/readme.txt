﻿[サンプル名]BookMyFlight
[カテゴリ]ASP.NET MVC用共通
------------------------------------------------------------------------
[概要]
本製品の入力コントロールを用いて作成された、飛行機予約システムのデモアプリケーションです。

※このサンプルを実行するには、開発環境にSQL Serverがインストールされている必要があります。
