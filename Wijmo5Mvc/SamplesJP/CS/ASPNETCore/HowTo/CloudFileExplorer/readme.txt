ASP.NET MVC Core クラウドファイルエクスプローラー サンプル
-------------------------------------------------------------------
このサンプルでは、Web API とクラウド API を使用して単純なファイルエクスプローラーを作成する方法を示しています。

ファイルエクスプローラーは、Azure、Amazon Web Services（AWS）、GoogleDrive、OneDrive、DropBox のクラウドストレージとの統合をサポートしています。

このサンプルを実行するには、cloudFileExplorer フォルダーの読み取り専用属性を削除し、クラウドに接続するすべての情報を Web.config ファイルに設定する必要があります。
また、「InitialPath」の値を入力する必要があります。たとえば、Dropbox  のフォルダーパスが「DropBox/C1WebApi/test1」の場合は、「DropBoxInitialPath」に「C1WebApi/test1」を設定します。

- Azure：

サンプル内の Azure Storage アカウントの接続文字列を更新するには、次の手順に従います。

手順1： Azure アカウントにログインします。

手順2： コンテナーを作成し、その接続文字列を生成します。

手順3： 手順2 で作成した接続文字列を Web.config 内の AzureStorageConnectionString キーの値として入力します。

- Amazon Web Services （AWS）：

手順1： Web.configで以下のキーの値を設定する必要があります。

AccessToken
SecretKey
BucketName

DropBox：

手順1： DropBox アカウントにログインします。

手順2： アプリを作成し、そのアクセストークンを生成します。

手順3： 手順2 で作成したアクセストークンを Web.config 内の DropBoxStorageAccessToken キーの値として入力します。

GoogleDrive：

手順1： Google ドライブにログインします。

手順2： アプリを作成し、その credentials.json ファイルを生成します。

手順3： 手順2で作成した credentials.json をサンプル内のフォルダーに配置します。


OneDrive：

手順1： OneDrive にログインします。

手順2： 次のリンクを使用してアクセス トークンを取得します。https://login.live.com/oauth20_authorize.srf?client_id=000000004C16A865&scope=onedrive.readwrite&response_type=token

手順3：手順2 で作成したアクセストークンキーを Web.config 内の OneDriveAccessToken キーの値として入力します。