﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>@ViewData("Title")</title>
    <link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
    @Html.C1().Styles()$if$ ($customthemeisset$ == True).Theme("$customtheme$")$endif$

    <script src="@Url.Content("~/Scripts/jquery-1.7.1.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/modernizr-2.5.3.js")" type="text/javascript"></script>$if$ ($refflexsheet$ == True)
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>$endif$$if$ ($enabledeferredscripts$ != True)
    @Html.C1().Scripts().Basic()$endif$$if$ ($financescript$ == True).Finance()$endif$$if$ ($flexsheetscript$ == True).FlexSheet()$endif$$if$ ($flexviewerscript$ == True).FlexViewer()$endif$$if$ ($olapscript$ == True).Olap()$endif$$if$ ($multirowscript$ == True).MultiRow()$endif$$if$ ($transposedgridscript$ == True).TransposedGrid()$endif$
</head>
<body>
    <div class="page">
        <header>
            <div id="title">
                <h1>ComponentOne MVC Application</h1>
            </div>
            <div id="logindisplay">
                @Html.Partial("_LogOnPartial")
            </div>
            <nav>
                <ul id="menu">
                    <li>@Html.ActionLink("Home", "Index", "Home")</li>
                    <li>@Html.ActionLink("About", "About", "Home")</li>
                </ul>
            </nav>
        </header>
        <section id="main">
            @RenderBody()
        </section>
        <footer></footer>
    </div>$if$ ($enabledeferredscripts$ == True)
    @Html.C1().Scripts()
    @RenderSection("scripts", false)
    @Html.C1().DeferredScripts()$endif$
</body>
</html>
