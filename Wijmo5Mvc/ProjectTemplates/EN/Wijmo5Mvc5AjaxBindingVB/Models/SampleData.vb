﻿Imports System.ComponentModel.DataAnnotations
Imports System.Collections.ObjectModel
Imports System.Threading.Tasks
Imports System.Text
Imports System.Linq
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System

Public Class Person
#Region "Fields"
    Public Class NamedCountry
        Public Property Id As String
        Public Property Name As String
    End Class

    Public Shared Countries As New List(Of NamedCountry)() From {
        New NamedCountry() With {.Id = "1", .Name = "China"},
        New NamedCountry() With {.Id = "2", .Name = "India"},
        New NamedCountry() With {.Id = "3", .Name = "United States"},
        New NamedCountry() With {.Id = "4", .Name = "Indonesia"},
        New NamedCountry() With {.Id = "5", .Name = "Brazil"},
        New NamedCountry() With {.Id = "6", .Name = "Pakistan"},
        New NamedCountry() With {.Id = "7", .Name = "Bangladesh"},
        New NamedCountry() With {.Id = "8", .Name = "Nigeria"},
        New NamedCountry() With {.Id = "9", .Name = "Russia"},
        New NamedCountry() With {.Id = "10", .Name = "Japan"}
    }

#End Region

#Region "ctor"

    Public Sub New()
    End Sub

#End Region

#Region "Object Model"

    Public Property Id As Integer
    Public ReadOnly Property Name() As String
        Get
            Return String.Format("{0} {1}", First, Last)
        End Get
    End Property
    Public Property CountryID As String
    Public Property Active As Boolean
    Public Property First As String
    Public Property Last As String
    Public Property Hired As DateTime

#End Region

    Public Sub CopyFrom(source As Person)
        CountryID = source.CountryID
        Active = source.Active
        First = source.First
        Last = source.Last
        Hired = source.Hired
    End Sub
End Class

Public Class SampleData

    Shared _rnd As New Random()
    Private Shared _firstNames As String() = "Andy|Ben|Charlie|Dan|Ed|Fred|Gil|Herb|Jack|Karl|Larry|Mark|Noah|Oprah|Paul|Quince|Rich|Steve|Ted|Ulrich|Vic|Xavier|Zeb".Split("|"c)
    Private Shared _lastNames As String() = "Ambers|Bishop|Cole|Danson|Evers|Frommer|Griswold|Heath|Jammers|Krause|Lehman|Myers|Neiman|Orsted|Paulson|Quaid|Richards|Stevens|Trask|Ulam".Split("|"c)

    Private Shared Function GetString(arr As String()) As String
        Return arr(_rnd.[Next](arr.Length))
    End Function

    Public Sub New()
    End Sub

    Public Shared Function GetData() As IEnumerable(Of Person)
        Dim list = Enumerable.Range(0, 50).[Select](Function(i)
                                                        Return New Person() With {
                                                        .Id = i,
                                                        .First = GetString(_firstNames),
                                                        .Last = GetString(_lastNames),
                                                        .CountryID = Person.Countries(_rnd.[Next](0, Person.Countries.Count)).Id,
                                                        .Active = _rnd.NextDouble() >= 0.5,
                                                        .Hired = DateTime.Today.AddDays(-_rnd.[Next](1, 365))
                                                        }
                                                    End Function)
        Return list
    End Function
End Class