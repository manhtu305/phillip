﻿Imports System.Web.Optimization$if$ ($includewebapi$ == True)
Imports System.Web.Http$endif$

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Protected Sub Application_Start()
        AreaRegistration.RegisterAllAreas()
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)$if$ ($includewebapi$ == True)
        GlobalConfiguration.Configure(AddressOf WebApiConfig.Register)$endif$
    End Sub
End Class
