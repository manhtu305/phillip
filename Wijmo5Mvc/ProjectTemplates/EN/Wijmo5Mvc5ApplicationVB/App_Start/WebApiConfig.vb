﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Reflection
Imports System.Web.Http
Imports System.Web.Http.Dispatcher$if$ ($refdataengineapi$ == True)
Imports C1.Web.Api.DataEngine$endif$
Imports C1.Web.Api.Storage

Public Module WebApiConfig
    Public Sub Register(ByVal config As HttpConfiguration)
        ' Web API configuration and services
        config.Services.Replace(GetType(IAssembliesResolver), New CustomAssembliesResolver())

        ' Web API routes
        config.MapHttpAttributeRoutes()

        config.Routes.MapHttpRoute(
            name:="DefaultApi",
            routeTemplate:="api/{controller}/{id}",
            defaults:=New With {.id = RouteParameter.Optional}
        )
    End Sub
End Module

Friend Class CustomAssembliesResolver
    Inherits DefaultAssembliesResolver

    Public Overrides Function GetAssemblies() As ICollection(Of Assembly)
        Dim assemblies = MyBase.GetAssemblies
        Dim customControllersAssembly = GetType(StorageController).Assembly
        If (Not assemblies.Contains(customControllersAssembly)) Then
            assemblies.Add(customControllersAssembly)
        End If
        $if$ ($refdataengineapi$ == True)
        customControllersAssembly = GetType(DataEngineController).Assembly
        If (Not assemblies.Contains(customControllersAssembly)) Then
            assemblies.Add(customControllersAssembly)
        End If
        $endif$
        Return assemblies
    End Function
End Class