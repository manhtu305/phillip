﻿@Code
    ViewData("Title") = "Home Page"
End Code
@ModelType IEnumerable(Of Person)

@(Html.C1().FlexGrid(Of Person)() _
                                .Id("flexGrid") _
                                .AutoGenerateColumns(False) _
                                .Bind(Model) _
                                .IsReadOnly(True) _
                                .CssClass("grid") _
                                .Columns(Sub(bl)
                                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("0.4*").IsReadOnly(True))
                                             bl.Add(Sub(cb) cb.Binding("Name").Header("Name").Width("*").Name("Name"))
                                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*").Name("Country"))
                                             bl.Add(Sub(cb) cb.Binding("Hired").Header("Hired").Width("*").Name("Hired"))
                                         End Sub)
)
