﻿Imports System.ComponentModel.DataAnnotations
Imports System.Collections.ObjectModel
Imports System.Threading.Tasks
Imports System.Text
Imports System.Linq
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System

Public Class Person
#Region "Fields"
    Friend Shared Countries As String() = "China|India|United States|Indonesia|Brazil|Pakistan|Bangladesh|Nigeria|Russia|Japan|Mexico|Philippines|Vietnam|Germany|Ethiopia|Egypt|Iran|Turkey|Congo|France|Thailand|United Kingdom|Italy|Myanmar".Split("|"c)

#End Region

#Region "ctor"

    Public Sub New()
    End Sub

#End Region

#Region "Object Model"

    Public Property ID As Integer
    Public ReadOnly Property Name() As String
        Get
            Return String.Format("{0} {1}", First, Last)
        End Get
    End Property

    Public ReadOnly Property Country() As String
        Get
            Return Countries(CountryID)
        End Get
    End Property

    Public Property CountryID As Integer
    Public Property Active As Boolean
    Public Property First As String
    Public Property Last As String
    Public Property Hired As DateTime
#End Region
End Class

Public Class SampleData

    Shared _rnd As New Random()
    Private Shared _firstNames As String() = "Andy|Ben|Charlie|Dan|Ed|Fred|Gil|Herb|Jack|Karl|Larry|Mark|Noah|Oprah|Paul|Quince|Rich|Steve|Ted|Ulrich|Vic|Xavier|Zeb".Split("|"c)
    Private Shared _lastNames As String() = "Ambers|Bishop|Cole|Danson|Evers|Frommer|Griswold|Heath|Jammers|Krause|Lehman|Myers|Neiman|Orsted|Paulson|Quaid|Richards|Stevens|Trask|Ulam".Split("|"c)

    Private Shared Function GetString(arr As String()) As String
        Return arr(_rnd.[Next](arr.Length))
    End Function

    Public Sub New()
    End Sub

    Public Shared Function GetData() As IEnumerable(Of Person)
        Dim list = Enumerable.Range(0, 50).[Select](Function(i)
                                                        Return New Person() With {
                                                        .ID = i,
                                                        .First = GetString(_firstNames),
                                                        .Last = GetString(_lastNames),
                                                        .CountryID = _rnd.[Next]() Mod Person.Countries.Length,
                                                        .Active = _rnd.NextDouble() >= 0.5,
                                                        .Hired = DateTime.Today.AddDays(-_rnd.[Next](1, 365))
                                                        }
                                                    End Function)
        Return list
    End Function
End Class