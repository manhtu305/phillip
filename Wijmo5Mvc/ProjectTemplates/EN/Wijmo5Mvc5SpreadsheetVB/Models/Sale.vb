﻿Public Class Sale
    Public Property ID() As Integer
        Get
            Return m_ID
        End Get
        Set
            m_ID = Value
        End Set
    End Property
    Private m_ID As Integer
    Public Property [Date]() As DateTime
        Get
            Return m_Date
        End Get
        Set
            m_Date = Value
        End Set
    End Property
    Private m_Date As DateTime
    Public Property Country() As String
        Get
            Return m_Country
        End Get
        Set
            m_Country = Value
        End Set
    End Property
    Private m_Country As String
    Public Property Product() As String
        Get
            Return m_Product
        End Get
        Set
            m_Product = Value
        End Set
    End Property
    Private m_Product As String
    Public Property Amount() As Double
        Get
            Return m_Amount
        End Get
        Set
            m_Amount = Value
        End Set
    End Property
    Private m_Amount As Double
    Public Property Active() As Boolean
        Get
            Return m_Active
        End Get
        Set
            m_Active = Value
        End Set
    End Property
    Private m_Active As Boolean


    ''' <summary>
    ''' Get the data.
    ''' </summary>
    ''' <param name="total"></param>
    ''' <returns></returns>
    Public Shared Function GetData(total As Integer) As IEnumerable(Of Sale)
        Dim countries = New String() {"US", "UK", "Canada", "Japan", "China", "France",
            "German", "Italy", "Korea", "Australia"}
        Dim products = New String() {"Widget", "Gadget", "Doohickey"}
        Dim rand = New Random(0)
        Dim dt = DateTime.Now
        Dim list = Enumerable.Range(0, total).[Select](Function(i)
                                                           Dim country = countries(rand.[Next](0, countries.Length - 1))
                                                           Dim product = products(rand.[Next](0, products.Length - 1))
                                                           Dim [date] = New DateTime(dt.Year, i Mod 12 + 1, 25, i Mod 24, i Mod 60, i Mod 60)
                                                           Dim SaleRec = New Sale
                                                           SaleRec.ID = i + 1
                                                           SaleRec.Country = country
                                                           SaleRec.Product = product
                                                           SaleRec.[Date] = [date]
                                                           SaleRec.Amount = rand.NextDouble() * 10000 - 5000
                                                           SaleRec.Active = (i Mod 4 = 0)
                                                           Return SaleRec
                                                           'Return New Sale() With {
                                                           '    Key.ID = i + 1,
                                                           '    Key.Country = country,
                                                           '    Key.Product = product,
                                                           '    Key.[Date] = [date],
                                                           '    Key.Amount = rand.NextDouble() * 10000 - 5000,
                                                           '    Key.Active = (i Mod 4 = 0)
                                                           '}

                                                       End Function)
        Return list
    End Function
End Class
