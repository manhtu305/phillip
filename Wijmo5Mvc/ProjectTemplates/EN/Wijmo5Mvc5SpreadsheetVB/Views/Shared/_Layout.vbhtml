﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title - ComponentOne ASP.NET MVC Project</title>
    @Styles.Render("~/Content/css")
    @Html.C1().Styles()$if$ ($customthemeisset$ == True).Theme("$customtheme$")$endif$

    @RenderSection("styles", required:=False)
    <script src="~/Scripts/app/app.js"></script>$if$ ($enabledeferredscripts$ != True)
    @Html.C1().Scripts().Basic().FlexSheet()$endif$
</head>
<body>
    @RenderBody()

    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")
    @Scripts.Render("~/Scripts/jszip.min.js")$if$ ($enabledeferredscripts$ == True)
    @Html.C1().Scripts().Basic().FlexSheet()$endif$
    @RenderSection("scripts", required:=false)$if$ ($enabledeferredscripts$ == True)
    @Html.C1().DeferredScripts()$endif$
</body>
</html>
