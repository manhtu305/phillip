﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace $safeprojectname$.Controllers
{
    public class HomeController : Controller
    {
        public static List<Person> Persons = SampleData.GetData().ToList();
        public ActionResult Index()
        {
            return View(Persons);
        }

        public ActionResult About()
        {
            ViewBag.Message = "アプリケーション説明ページ。";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "連絡先ページ。";

            return View();
        }
    }
}