﻿Imports C1.Web.Mvc
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Private Shared Persons As List(Of Person) = SampleData.GetData().ToList()
    Function Index() As ActionResult
        ViewData("Countries") = Person.Countries
        Return View()
    End Function

    Function About() As ActionResult
        ViewData("Message") = "アプリケーション説明ページ。"

        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "連絡先ページ。"

        Return View()
    End Function

    <HttpPost>
    Function ReadAction() As ActionResult
        Return Json(Persons)
    End Function

    <HttpPost>
    Public Function Save(<C1JsonRequest> requestData As CollectionViewBatchEditRequest(Of Person)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.BatchEdit(Of Person)(requestData, Function(batchData)
                                                                                    Dim itemResults = New List(Of CollectionViewItemResult(Of Person))()
                                                                                    Dim [error] As String = String.Empty
                                                                                    Dim success As Boolean = True
                                                                                    Try
                                                                                        If batchData.ItemsCreated IsNot Nothing Then
                                                                                            Dim maxId As Integer = -1
                                                                                            If Persons.Any() Then
                                                                                                maxId = Persons.Max(Function(s) s.ID)
                                                                                            End If
                                                                                            batchData.ItemsCreated.ToList().ForEach(Sub(sale)
                                                                                                                                        maxId += 1
                                                                                                                                        Dim newSale = New Person()
                                                                                                                                        newSale.ID = maxId
                                                                                                                                        newSale.CopyFrom(sale)
                                                                                                                                        Persons.Add(newSale)
                                                                                                                                        Dim itemResult = New CollectionViewItemResult(Of Person)()
                                                                                                                                        itemResult.Error = ""
                                                                                                                                        itemResult.Success = ModelState.IsValid
                                                                                                                                        itemResult.Data = sale
                                                                                                                                        itemResults.Add(itemResult)
                                                                                                                                    End Sub)
                                                                                        End If
                                                                                        If batchData.ItemsDeleted IsNot Nothing Then
                                                                                            batchData.ItemsDeleted.ToList().ForEach(Sub(sale)
                                                                                                                                        Dim result = Persons.Find(Function(s) s.ID = sale.ID)
                                                                                                                                        If result IsNot Nothing Then
                                                                                                                                            Persons.Remove(result)
                                                                                                                                        End If
                                                                                                                                        Dim itemResult = New CollectionViewItemResult(Of Person)()
                                                                                                                                        itemResult.Error = ""
                                                                                                                                        itemResult.Success = ModelState.IsValid
                                                                                                                                        itemResult.Data = sale
                                                                                                                                        itemResults.Add(itemResult)
                                                                                                                                    End Sub)
                                                                                        End If
                                                                                        If batchData.ItemsUpdated IsNot Nothing Then
                                                                                            batchData.ItemsUpdated.ToList().ForEach(Sub(sale)
                                                                                                                                        Dim result = Persons.Find(Function(s) s.ID = sale.ID)
                                                                                                                                        result.CopyFrom(sale)
                                                                                                                                        Dim itemResult = New CollectionViewItemResult(Of Person)()
                                                                                                                                        itemResult.Error = ""
                                                                                                                                        itemResult.Success = ModelState.IsValid
                                                                                                                                        itemResult.Data = sale
                                                                                                                                        itemResults.Add(itemResult)
                                                                                                                                    End Sub)
                                                                                        End If
                                                                                    Catch e As Exception
                                                                                        [error] = e.Message
                                                                                        success = False
                                                                                    End Try
                                                                                    Dim response = New CollectionViewResponse(Of Person)()
                                                                                    response.Error = [error]
                                                                                    response.Success = success
                                                                                    response.OperatedItemResults = itemResults
                                                                                    Return response
                                                                                End Function, Function() Persons))
    End Function
End Class
