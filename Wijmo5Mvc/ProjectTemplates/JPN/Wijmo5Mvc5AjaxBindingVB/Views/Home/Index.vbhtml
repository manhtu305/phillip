﻿@Code
    ViewData("Title") = "ホーム ページ"
    Dim Countries = ViewData("Countries")
End Code

<br />
<div>
    <input type="button" id="btn_save" value="Save Data" onclick="btn_save_click()" />
</div>
<br />
@(Html.C1().FlexGrid() _
        .Id("flexGrid") _
        .AutoGenerateColumns(False) _
        .Bind(Sub(bi) _
                  bi.BatchEdit(Url.Action("Save")).DisableServerRead(True)) _
        .IsReadOnly(False) _
        .AutoClipboard(True) _
        .SortingType(C1.Web.Mvc.Grid.AllowSorting.SingleColumn) _
        .AllowAddNew(True) _
        .AllowDelete(True) _
        .SelectionMode(C1.Web.Mvc.Grid.SelectionMode.Row) _
        .Height(500) _
        .Columns(Sub(bl)
                     bl.Add(Sub(cb) cb.Binding("Id").Header("ID").Width("0.4*").IsReadOnly(True))
                     bl.Add(Sub(cb) cb.Binding("First").Header("FirstName").Width("*").Name("FirstName"))
                     bl.Add(Sub(cb) cb.Binding("Last").Header("LastName").Width("*").Name("LastName"))
                     bl.Add(Sub(cb) cb.Binding("CountryID").Header("Country").Width("*").Name("Country") _
                     .DataMap(Sub(dm)
                                  dm.DisplayMemberPath("Name").DisplayMemberPath("Name").SelectedValuePath("Id").SortByDisplayValues(True).Bind(Countries)
                              End Sub))
                     bl.Add(Sub(cb) cb.Binding("Hired").Header("Hired").Format("d").Width("*").Name("Hired"))
                 End Sub)
)

@Section scripts
<script>
    function btn_save_click() {
        var flex = wijmo.Control.getControl("#flexGrid"),
        cv = flex.collectionView;
        cv.commit();
    }

    function parseDate(strDate) {
        var date = strDate.match(/\-?\d+/g);
        return new Date(parseInt(date));
    }

    c1.documentReady(function () {
        $.ajax({
            type: "POST",
            url: '@(Url.Content("~/Home/ReadAction"))',
            dataType: "json",
            success: function (result) {
                var flex = wijmo.Control.getControl("#flexGrid"),
                    cv = flex.collectionView;
                try {
                    cv._isFillingData = true;
                    cv.deferUpdate(function () {
                        cv.sourceCollection.clear();
                        result.forEach(function (item) {
                            item.Hired = parseDate(item.Hired);
                            cv.sourceCollection.push(item);
                        });
                    })
                } finally {
                    cv._isFillingData = false;
                }
            },
            error: function (err) {

            }
        });
    });
</script>
End Section