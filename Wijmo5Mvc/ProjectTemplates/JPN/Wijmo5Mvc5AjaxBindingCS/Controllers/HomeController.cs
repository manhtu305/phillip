﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C1.Web.Mvc;
using C1.Web.Mvc.Serialization;

namespace $safeprojectname$.Controllers
{
    public class HomeController : Controller
    {
        public static List<Person> Persons = SampleData.GetData().ToList();
        public ActionResult Index()
        {
            ViewBag.Countries = Person.Countries;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "アプリケーション説明ページ。";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "連絡先ページ。";

            return View();
        }

        [HttpPost]
        public ActionResult ReadAction()
        {
            return Json(Persons);
        }

        [HttpPost]
        public ActionResult Save([C1JsonRequest]CollectionViewBatchEditRequest<Person> requestData)
        {
            return this.C1Json(CollectionViewHelper.BatchEdit(requestData, batchData =>
            {
                var itemResults = new List<CollectionViewItemResult<Person>>();
                string error = string.Empty;
                bool success = true;
                try
                {
                    if (batchData.ItemsCreated != null)
                    {
                        var maxId = Persons.Any() ? Persons.Max(s => { return s.Id; }) : -1;
                        batchData.ItemsCreated.ToList().ForEach(sale =>
                        {
                            maxId++;
                            var newSale = new Person();
                            newSale.Id = maxId;
                            newSale.CopyFrom(sale);
                            Persons.Add(newSale);
                            itemResults.Add(new CollectionViewItemResult<Person>
                            {
                                Error = "",
                                Success = ModelState.IsValid,
                                Data = sale
                            });
                        });
                    }
                    if (batchData.ItemsDeleted != null)
                    {
                        batchData.ItemsDeleted.ToList().ForEach(sale =>
                        {
                            var result = Persons.Find(s => { return s.Id == sale.Id; });
                            if (result != null)
                            {
                                Persons.Remove(result);
                            }
                            itemResults.Add(new CollectionViewItemResult<Person>
                            {
                                Error = "",
                                Success = ModelState.IsValid,
                                Data = sale
                            });
                        });
                    }
                    if (batchData.ItemsUpdated != null)
                    {
                        batchData.ItemsUpdated.ToList().ForEach(sale =>
                        {
                            var result = Persons.Find(s => { return s.Id == sale.Id; });
                            result.CopyFrom(sale);
                            itemResults.Add(new CollectionViewItemResult<Person>
                            {
                                Error = "",
                                Success = ModelState.IsValid,
                                Data = sale
                            });
                        });
                    }
                }
                catch (Exception e)
                {
                    error = e.Message;
                    success = false;
                }

                return new CollectionViewResponse<Person>
                {
                    Error = error,
                    Success = success,
                    OperatedItemResults = itemResults
                };
            }, () => Persons));
        }
    }
}