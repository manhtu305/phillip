﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        ViewBag.ChartTypes =
            {"Column", "Bar", "Scatter", "Line", "LineSymbols", "Area", "Spline", "SplineSymbols", "SplineArea"}
        Return View(Sale.GetData(100))
    End Function
End Class
