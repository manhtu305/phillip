﻿@ModelType IEnumerable(Of Sale)
@Code
    Dim fontList As IEnumerable(Of FontName) = ViewBag.FontList
    Dim fontSizeList As IEnumerable(Of FontSize) = ViewBag.FontSizeList
End Code

@Section scripts
@Scripts.Render("~/bundles/excelbook")
<script>
    c1.documentReady(function () {
        initInputs();
        initTableOptions();
        initFlexSheet();
        initSort();
        updateSortTable();
        initChartEngine();
        document.body.addEventListener('keydown', function (e) {
            var lbChartTypes = ctx.lbChartTypes;
            if (lbChartTypes) {
                if (e.keyCode === 27) {
                    lbChartTypes.hostElement.style.display = 'none';
                }
                if (e.keyCode === 13 && lbChartTypes.hostElement.style.display !== 'none') {
                    addChart();
                }
            }

            hidePopup(e);
        });

        document.body.addEventListener('click', function (e) {
            var lbChartTypes = ctx.lbChartTypes;
            if (lbChartTypes) {
                lbChartTypes.hostElement.style.display = 'none';
            }

            hidePopup(e);
        }, true);
    });
</script>
End Section

<div class="excelbook" style="width:100%; height: 100%; position: relative">
    <!-- the ribbon markup -->
    <div class="row ribbon-container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group">
                    <div class="btn btn-default btn-large no-border">
                        <span class="glyphicon glyphicon-open"></span>
                        <span class="text">開く</span>
                        <input type="file" class="upload"
                               onchange="importExcel(this)"
                               accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                    </div>
                    <button class="btn btn-default btn-large no-border" onclick="exportExcel()">
                        <span class="glyphicon glyphicon-save"></span>
                        <span class="text">保存</span>
                    </button>
                    <div class="btn btn-default btn-large no-border" onclick="newFile()">
                        <span class="glyphicon glyphicon-file"></span>
                        <span class="text">新規作成</span>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">ファイル</div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group btn-group-h">
                    <button type="button" id="undoBtn" class="btn btn-default btn-small" title="元に戻す" onclick="undo()">
                        <span class="glyphicon glyphicon-share-alt flip"></span>
                        <span class="text">元に戻す</span>
                    </button>
                    <button type="button" id="redoBtn" class="btn btn-default btn-small" title="やり直し" onclick="redo()">
                        <span class="glyphicon glyphicon-share-alt"></span>
                        <span class="text">やり直し</span>
                    </button>
                </div>
            </div>
            <div class="panel-footer text-center">元に戻す／やり直し</div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group-vertical">
                    @(Html.C1().ComboBox(Of FontName).Bind(fontList).SelectedIndex(10) _
                    .DisplayMemberPath("Name").SelectedValuePath("Value") _
                    .IsEditable(False).CssStyle("width", "120px") _
                    .OnClientSelectedIndexChanged("fontChanged") _
                    .Id("cboFontName"))

                    @(Html.C1().ComboBox(Of FontSize).Bind(fontSizeList).SelectedIndex(5) _
                    .DisplayMemberPath("Name").SelectedValuePath("Value") _
                    .IsEditable(False).CssStyle("width", "80px") _
                    .OnClientSelectedIndexChanged("fontSizeChanged") _
                    .Id("cboFontSize"))

                    <div class="btn-group btn-group-h">
                        <button type="button" id="boldBtn" class="btn btn-default btn-small" title="太字" onclick="applyBoldStyle()">
                            <span class="glyphicon glyphicon-bold"></span>
                            <span class="text">太字</span>
                        </button>
                        <button type="button" id="italicBtn" class="btn btn-default btn-small" title="斜体" onclick="applyItalicStyle()">
                            <span class="glyphicon glyphicon-italic"></span>
                            <span class="text">斜体</span>
                        </button>
                        <button type="button" id="underlineBtn" class="btn btn-default btn-small" title="下線" onclick="applyUnderlineStyle()">
                            <span class="underline ribbon-icon"></span>
                            <span class="text">下線</span>
                        </button>
                        <button type="button" class="btn btn-default btn-small" title="塗りつぶしの色" onclick="showColorPicker(event, true)">
                            <span class="glyphicon glyphicon-text-background"></span>
                            <span class="text">塗りつぶし</span>
                        </button>
                        <button type="button" class="btn btn-default btn-small" title="フォントの色" onclick="showColorPicker(event, false)">
                            <span class="glyphicon glyphicon-text-color"></span>
                            <span class="text">フォント</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">フォント</div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group-vertical">
                    <div class="btn-group btn-group-h">
                        <button type="button" id="leftBtn" class="btn btn-default btn-small" title="左揃え" onclick="applyCellTextAlign('left')">
                            <span class="glyphicon glyphicon-align-left"></span>
                            <span class="text">左揃え</span>
                        </button>
                        <button type="button" id="centerBtn" class="btn btn-default btn-small" title="中央揃え" onclick="applyCellTextAlign('center')">
                            <span class="glyphicon glyphicon-align-center"></span>
                            <span class="text">中央揃え</span>
                        </button>
                        <button type="button" id="rightBtn" class="btn btn-default btn-small" title="右揃え" onclick="applyCellTextAlign('right')">
                            <span class="glyphicon glyphicon-align-right"></span>
                            <span class="text">右揃え</span>
                        </button>

                        <button type="button" id="mergeBtn" class="btn btn-default btn-small" title="セルの結合" onclick="mergeCells()" style="clear:both;">
                            <span class="glyphicon glyphicon-th-large"></span>
                            <span class="text">セルの結合</span>
                        </button>
                    </div>
                    <div class="btn-group btn-group-h" style="display:none;">
                        <button type="button" class="btn btn-default btn-small" title="上揃え" onclick="applyCellVerticalAlign('top')">
                            <span class="top ribbon-icon"></span>
                            <span class="text">上揃え</span>
                        </button>
                        <button type="button" class="btn btn-default btn-small" title="上下中央揃え" onclick="applyCellVerticalAlign('middle')">
                            <span class="middle ribbon-icon"></span>
                            <span class="text">上下中央揃え</span>
                        </button>
                        <button type="button" class="btn btn-default btn-small" title="下揃え" onclick="applyCellVerticalAlign('bottom')">
                            <span class="bottom ribbon-icon"></span>
                            <span class="text">下揃え</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">アラインメント</div>
        </div>
        <div class="panel panel-default" style="display:none">
            <div class="panel-body">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-large no-border">
                        <span class="print ribbon-icon"></span>
                        <span class="text">印刷</span>
                    </button>
                    <div class="btn-group-vertical">
                        <button type="button" class="btn btn-default btn-small no-border">
                            <span class="pagewidth ribbon-icon"></span>
                            <span class="text">ページ幅</span>
                        </button>
                        <button type="button" class="btn btn-default btn-small no-border">
                            <span class="singlepage ribbon-icon"></span>
                            <span class="text">単一ページ</span>
                        </button>
                        <button type="button" class="btn btn-default btn-small no-border">
                            <span class="selection ribbon-icon"></span>
                            <span class="text">選択</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">印刷</div>
        </div>
        <div class="panel panel-default" style="display:none">
            <div class="panel-body">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-large no-border">
                        <span class="group ribbon-icon"></span>
                        <span class="text">グループ化</span>
                    </button>
                    <button type="button" class="btn btn-default btn-large no-border">
                        <span class="ungroup ribbon-icon"></span>
                        <span class="text">グループ化解除</span>
                    </button>
                    <div class="btn-group-vertical">
                        <button type="button" class="btn btn-default btn-small no-border">
                            <span class="showdetail ribbon-icon"></span>
                            <span class="text">詳細を表示</span>
                        </button>
                        <button type="button" class="btn btn-default btn-small no-border">
                            <span class="hidedetail ribbon-icon"></span>
                            <span class="text">詳細を非表示</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">グループ化</div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group">
                    <button type="button" id="frozenBtn" class="btn btn-default btn-large no-border" onclick="freeze()">
                        <span class="glyphicon glyphicon-list"></span>
                        <span class="text">セルの固定</span>
                    </button>
                    <div class="btn-group-vertical" style="display:none">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" /> グリッド線
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" /> 見出し
                            </label>
                        </div>
                        <select class="form-control input-sm">
                            <option>Blue</option>
                            <option>Silver</option>
                            <option>Black</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">表示</div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <button type="button" class="btn btn-default btn-large no-border" data-toggle="modal" data-target="#excelStyle">
                    <span class="glyphicon glyphicon-pencil"></span>
                    <span class="text">スタイル</span>
                </button>
            </div>
            <div class="panel-footer text-center">スタイル</div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group">
                    <button type="button" id="sortBtn" class="btn btn-default btn-large no-border" data-toggle="modal" data-target="#excelSort">
                        <span class="glyphicon glyphicon-sort-by-attributes"></span>
                        <span class="text">ソート</span>
                    </button>
                    <button type="button" id="filterBtn" class="btn btn-default btn-large no-border" onclick="showFilter()">
                        <span class="glyphicon glyphicon-filter"></span>
                        <span class="text">フィルタ</span>
                    </button>
                </div>
            </div>
            <div class="panel-footer text-center">編集</div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group">
                    <button type="button" id="chartTypesBtn" class="btn btn-default btn-large no-border" onclick="showChartTypes()">
                        <span class="glyphicon glyphicon-stats"></span>
                        <span class="text">グラフを挿入</span>
                    </button>
                </div>
            </div>
            <div class="panel-footer text-center">グラフ</div>
        </div>
        <div id="tableOptionsPanel" class="panel panel-default" style="display:none;">
            <div class="panel-body">
                <div class="btn-group-vertical form-check">
                    <div>
                        <label class="form-check-label">
                            <input id="tableHeaderRow" class="form-check-input" type="checkbox">
                            見出し行
                        </label>
                    </div>
                    <div>
                        <label class="form-check-label">
                            <input id="tableTotalRow" class="form-check-input" type="checkbox">
                            集計行
                        </label>
                    </div>
                    <div>
                        <label class="form-check-label">
                            <input id="tableBandedRows" class="form-check-input" type="checkbox">
                            縞模様（行）
                        </label>
                    </div>
                </div>
                <div class="btn-group-vertical form-check">
                    <div>
                        <label class="form-check-label">
                            <input id="tableFirstColumn" class="form-check-input" type="checkbox">
                            最初の列
                        </label>
                    </div>
                    <div>
                        <label class="form-check-label">
                            <input id="tableLastColumn" class="form-check-input" type="checkbox">
                            最後の列
                        </label>
                    </div>
                    <div>
                        <label class="form-check-label">
                            <input id="tableBandedColumns" class="form-check-input" type="checkbox">
                            縞模様（列）
                        </label>
                    </div>
                </div>
                <div class="btn-group-vertical">
                    @Html.C1().ComboBox().Id("cboTableStyles").IsEditable(False)
                </div>
            </div>
            <div class="panel-footer text-center">テーブルスタイルのオプション</div>
        </div>
    </div>
    <!-- the boxes -->
    <div class="top-boxes row" id="formulaBar">
    </div>
    <!-- the flexSheet -->
    <div class="row" id="flexsheetContainer">
        @(Html.C1().FlexSheet().Id("excelLikeSheet") _
                .AddBoundSheet(Sub(sheet) _
                                   sheet.Bind(Sub(cv) _
                                                  cv.Bind(Model).DisableServerRead(True)).Name("Country")) _
                .ShowFormulaBar("#formulaBar"))
    </div>

    <!-- the status bar -->
    <div class="row status text-right" style="padding-right:10px">準備完了</div>
</div>
@(Html.C1().ColorPicker().Id("colorPicker").CssStyles(New Dictionary(Of String, String)() From {
            {"display", "none"}, {"position", "fixed"}, {"z-index", "100"}
            }))
@(Html.C1().ListBox().Id("lbChartTypes").Bind(CType(ViewBag.ChartTypes, IEnumerable(Of String))).CssStyles(New Dictionary(Of String, String)() From {
            {"display", "none"}, {"position", "fixed"}, {"z-index", "100"}
            }))
@Html.Partial("_SortDialog")
@Html.Partial("_CellStyleDialog")