﻿Imports System.ComponentModel.DataAnnotations

Public Class ExternalLoginConfirmationViewModel
    <Required>
    <EmailAddress>
    <Display(Name:="電子メール")>
    Public Property Email As String
End Class

Public Class ExternalLoginListViewModel
    Public Property Action As String
    Public Property ReturnUrl As String
End Class

Public Class ManageUserViewModel
    <Required>
    <DataType(DataType.Password)>
    <Display(Name:="現在のパスワード")>
    Public Property OldPassword As String

    <Required>
    <StringLength(100, ErrorMessage:="{0} の長さは、{2} 文字以上である必要があります。", MinimumLength:=6)>
    <DataType(DataType.Password)>
    <Display(Name:="新しいパスワード")>
    Public Property NewPassword As String

    <DataType(DataType.Password)>
    <Display(Name:="新しいパスワードの確認入力")>
    <Compare("NewPassword", ErrorMessage:="新しいパスワードと確認のパスワードが一致しません。")>
    Public Property ConfirmPassword As String
End Class

Public Class LoginViewModel
    <Required>
    <EmailAddress>
    <Display(Name:="電子メール")>
    Public Property Email As String

    <Required>
    <DataType(DataType.Password)>
    <Display(Name:="パスワード")>
    Public Property Password As String

    <Display(Name:="このアカウントを記憶する")>
    Public Property RememberMe As Boolean
End Class

Public Class RegisterViewModel
    <Required>
    <EmailAddress>
    <Display(Name:="電子メール")>
    Public Property Email As String

    <Required>
    <StringLength(100, ErrorMessage:="{0} の長さは {2} 文字以上である必要があります。", MinimumLength:=6)>
    <DataType(DataType.Password)>
    <Display(Name:="パスワード")>
    Public Property Password As String

    <DataType(DataType.Password)>
    <Display(Name:="パスワードの確認入力")>
    <Compare("Password", ErrorMessage:="パスワードと確認のパスワードが一致しません。")>
    Public Property ConfirmPassword As String
End Class

Public Class ResetPasswordViewModel
    <Required>
    <EmailAddress>
    <Display(Name:="電子メール")>
    Public Property Email() As String

    <Required> _
    <StringLength(100, ErrorMessage:="{0} の長さは {2} 文字以上である必要があります。", MinimumLength:=6)> _
    <DataType(DataType.Password)>
    <Display(Name:="パスワード")>
    Public Property Password() As String

    <DataType(DataType.Password)>
    <Display(Name:="パスワードの確認入力")>
    <Compare("Password", ErrorMessage:="パスワードと確認のパスワードが一致しません。")> _
    Public Property ConfirmPassword() As String

    Public Property Code() As String
End Class

Public Class ForgotPasswordViewModel
    <Required>
    <EmailAddress>
    <Display(Name:="電子メール")>
    Public Property Email() As String
End Class
