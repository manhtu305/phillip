﻿@Code
    ViewData("Title") = "ホーム ページ"
End Code

<div class="jumbotron">
    <h1>ASP.NET</h1>
    <p class="lead">ASP.NET は HTML、CSS、JavaScript を使用したWeb サイトやWeb アプリケーションを構築するための無料の Web フレームワークです。</p>

    <p><a href="http://asp.net" class="btn btn-primary btn-lg">詳細...</a></p>
</div>

<div class="row">
    <div class="col-md-4">
        <h2>はじめに</h2>
        <p>
            ASP.NET MVC では、強力なパターンベースの方法で動的な Web サイトを構築できます。
            この方法では、楽しく軽快な開発のために、問題点を明確に切り分け、
            マークアップを完全に制御できます。ASP.NET MVC は、最新の Web 標準を使用した高度な
            アプリケーションを作成するために、短期間での TDD フレンドリな開発を可能にする
            多くの機能を備えています。
        </p>
        <p><a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301865">詳細...</a></p>
    </div>
    <div class="col-md-4">
        <h2>NuGet パッケージを追加してコーディングを簡単に始める</h2>
        <p>NuGet により、無料のライブラリやツールのインストールと更新が簡単になります。</p>
        <p><a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301866">詳細...</a></p>
    </div>
    <div class="col-md-4">
        <h2>Web ホスティングを検索する</h2>
        <p>アプリケーションの機能と価格の適切な組み合わせを提供する Web ホスティング会社を簡単に見つけることができます。</p>
        <p><a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301867">詳細...</a></p>
    </div>
</div>
