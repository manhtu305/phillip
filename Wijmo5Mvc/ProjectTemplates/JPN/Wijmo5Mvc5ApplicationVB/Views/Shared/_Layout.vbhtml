﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title - ComponentOne ASP.NET MVC Project</title>
    @Styles.Render("~/Content/css")
    @Html.C1().Styles()$if$ ($customthemeisset$ == True).Theme("$customtheme$")$endif$

    @RenderSection("styles", required:=False)$if$ ($refflexsheet$ == True)
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>$endif$$if$ ($enabledeferredscripts$ != True)
    @Html.C1().Scripts().Basic()$endif$$if$ ($financescript$ == True).Finance()$endif$$if$ ($flexsheetscript$ == True).FlexSheet()$endif$$if$ ($flexviewerscript$ == True).FlexViewer()$endif$$if$ ($olapscript$ == True).Olap()$endif$$if$ ($multirowscript$ == True).MultiRow()$endif$
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                @Html.ActionLink("MVC Project", "Index", "Home", New With { .area = "" }, New With { .class = "navbar-brand" })
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>@Html.ActionLink("ホーム", "Index", "Home")</li>
                    <li>@Html.ActionLink("バージョン情報", "About", "Home")</li>
                    <li>@Html.ActionLink("連絡先", "Contact", "Home")</li>
                </ul>
                @Html.Partial("_LoginPartial")
            </div>
        </div>
    </div>
    <div class="container body-content">
        @RenderBody()
        <hr />
        <footer>
            <p>&copy; @DateTime.Now.Year - ComponentOne ASP.NET MVC Project</p>
        </footer>
    </div>

    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")$if$ ($enabledeferredscripts$ == True)
    @Html.C1().Scripts()$endif$
    @RenderSection("scripts", required:=false)$if$ ($enabledeferredscripts$ == True)
    @Html.C1().DeferredScripts()$endif$
</body>
</html>
