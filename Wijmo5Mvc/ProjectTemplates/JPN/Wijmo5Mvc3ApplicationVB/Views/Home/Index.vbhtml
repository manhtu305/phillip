﻿@Code
    ViewData("Title") = "ホーム ページ"
End Code

<h2>@ViewData("Message")</h2>
<p>
    ASP.NET MVC の詳細については、<a href="http://asp.net/mvc" title="ASP.NET MVC Web サイト">http://asp.net/mvc</a> を参照してください。
</p>
