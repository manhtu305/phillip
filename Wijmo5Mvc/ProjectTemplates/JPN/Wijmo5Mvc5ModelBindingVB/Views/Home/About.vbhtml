﻿@Code
    ViewData("Title") = "このサイトについて"
End Code

<h2>@ViewData("Title").</h2>
<h3>@ViewData("Message")</h3>

<p>このエリアにアプリケーション情報を記載してください。</p>
