﻿@ModelType ExternalLoginListViewModel
@Imports Microsoft.Owin.Security
@Code
    Dim loginProviders = Context.GetOwinContext().Authentication.GetExternalAuthenticationTypes()
End Code
<h4>別のサービスを使用してログインしてください。</h4>
<hr />
@If loginProviders.Count() = 0 Then
    @<div>
        <p>
            <p>外部認証サービスが構成されていません。この ASP.NET アプリケーションをセットアップして、外部サービス経由でのログインをサポートする方法の詳細については、
            <a href="http://go.microsoft.com/fwlink/?LinkId=313242">この記事</a>を参照してください。
        </p>
    </div>
Else
    @Using Html.BeginForm(Model.Action, "Account", New With {.ReturnUrl = Model.ReturnUrl}, FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
        @Html.AntiForgeryToken()
        @<div id="socialLoginList">
           <p>
               @For Each p As AuthenticationDescription In loginProviders
                   @<button type="submit" class="btn btn-default" id="@p.AuthenticationType" name="provider" value="@p.AuthenticationType" title="@p.Caption アカウントを使用してログイン">@p.AuthenticationType</button>
               Next
           </p>
        </div>
    End Using
End If
