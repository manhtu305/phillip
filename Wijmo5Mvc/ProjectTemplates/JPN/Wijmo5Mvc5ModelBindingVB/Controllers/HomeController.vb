﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Private Shared Persons = SampleData.GetData()
    Function Index() As ActionResult
        Return View(Persons)
    End Function

    Function About() As ActionResult
        ViewData("Message") = "アプリケーション説明ページ。"

        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "連絡先ページ。"

        Return View()
    End Function
End Class
