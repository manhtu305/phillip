﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Public Class UsersContext
    Inherits DbContext

    Public Sub New()
        MyBase.New("DefaultConnection")
    End Sub

    Public Property UserProfiles As DbSet(Of UserProfile)
End Class

<Table("UserProfile")>
Public Class UserProfile
    <Key()>
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)>
    Public Property UserId As Integer

    Public Property UserName As String
End Class

Public Class RegisterExternalLoginModel
    <Required()>
    <Display(Name:="ユーザー名")>
    Public Property UserName As String

    Public Property ExternalLoginData As String
End Class

Public Class LocalPasswordModel
    <Required()>
    <DataType(DataType.Password)>
    <Display(Name:="現在のパスワード")>
    Public Property OldPassword As String

    <Required()> _
    <StringLength(100, ErrorMessage:="{0} の長さは {2} 文字以上である必要があります。", MinimumLength:=6)>
    <DataType(DataType.Password)>
    <Display(Name:="新しいパスワード")>
    Public Property NewPassword As String

    <DataType(DataType.Password)>
    <Display(Name:="新しいパスワードの確認入力")>
    <System.Web.Mvc.Compare("NewPassword", ErrorMessage:="新しいパスワードと確認のパスワードが一致しません。")>
    Public Property ConfirmPassword As String
End Class

Public Class LoginModel
    <Required()>
    <Display(Name:="ユーザー名")>
    Public Property UserName As String

    <Required()>
    <DataType(DataType.Password)>
    <Display(Name:="パスワード")>
    Public Property Password As String

    <Display(Name:="このアカウントを記憶する")>
    Public Property RememberMe As Boolean
End Class

Public Class RegisterModel
    <Required()>
    <Display(Name:="ユーザー名")>
    Public Property UserName As String

    <Required()> _
    <StringLength(100, ErrorMessage:="{0} の長さは {2} 文字以上である必要があります。", MinimumLength:=6)>
    <DataType(DataType.Password)>
    <Display(Name:="パスワード")>
    Public Property Password As String

    <DataType(DataType.Password)>
    <Display(Name:="パスワードの確認入力")>
    <System.Web.Mvc.Compare("Password", ErrorMessage:="パスワードと確認のパスワードが一致しません。")>
    Public Property ConfirmPassword As String
End Class

Public Class ExternalLogin
    Public Property Provider As String
    Public Property ProviderDisplayName As String
    Public Property ProviderUserId As String
End Class
