﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        ViewData("Message") = "ASP.NET MVC アプリケーションを簡単に始めるには、このテンプレートを変更してください。"

        Return View()
    End Function

    Function About() As ActionResult
        ViewData("Message") = "アプリケーション説明ページ。"

        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "連絡先ページ。"

        Return View()
    End Function
End Class
