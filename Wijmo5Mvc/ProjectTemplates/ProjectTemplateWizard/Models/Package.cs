﻿namespace ProjectTemplateWizard.Models
{
    internal class Package
    {
        public Package(string id, string registry, string version)
        {
            Id = id;
            Registry = registry;
            Version = version;
        }

        public string Registry
        {
            get;
            private set;
        }

        public string Id
        {
            get;
            private set;
        }

        public string Version
        {
            get;
            private set;
        }
    }
}
