﻿using C1.Web.Mvc;
using ProjectTemplateWizard.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Resources = ProjectTemplateWizard.Localization.Resources;

namespace ProjectTemplateWizard.Models
{
    public enum Language
    {
        CSharp,
        Vb
    }

    public class TemplatesSettings : INotifyPropertyChanged
    {
#if GRAPECITY
        private const string CSharpIconPath = "/ProjectTemplateWizard;component/Resources/TemplateIcon_CS.ja.png";
        private const string VbIconPath = "/ProjectTemplateWizard;component/Resources/TemplateIcon_VB.ja.png";
#else
        private const string CSharpIconPath = "/ProjectTemplateWizard;component/Resources/TemplateIcon_CS.png";
        private const string VbIconPath = "/ProjectTemplateWizard;component/Resources/TemplateIcon_VB.png";
#endif
        public TemplatesSettings()
        {
            var iconPath = Language == Language.CSharp ? CSharpIconPath : VbIconPath;
            TemplateModels = new List<TemplateModel>()
            {
                new TemplateModel() { Name = "App",Text = Resources.StandardTemplateText,IconPath = iconPath },
                new TemplateModel() { Name = "AjaxBinding",Text = Resources.AjaxBindingTemplateText,IconPath = iconPath },
                new TemplateModel() { Name = "ModelBinding",Text = Resources.ModelBindingTemplateText,IconPath = iconPath },
                new TemplateModel() { Name = "Spreadsheet",Text = Resources.SpreadsheetTemplateText,IconPath = iconPath }
            };
        }

        private int _selectedTemplateIndex;
        public int SelectedTemplateIndex
        {
            get { return _selectedTemplateIndex; }
            set
            {
                if (_selectedTemplateIndex == value) return;

                _selectedTemplateIndex = value;
                RaisePropertyChanged("SelectedTemplateIndex");
                RaisePropertyChanged("SelectedTemplateImagePath");
                RaisePropertyChanged("SelectedTemplateDescription");
                RaisePropertyChanged("SelectedProjectSettings");
            }
        }
        public ProjectSettings SelectedProjectSettings { get { return TemplateModels[SelectedTemplateIndex].ProjectSettings; } }
        public string SelectedTemplateImagePath { get { return TemplateModels[SelectedTemplateIndex].ImagePath; } }
        public string SelectedTemplateDescription { get { return TemplateModels[SelectedTemplateIndex].Description; } }
        public List<TemplateModel> TemplateModels { get; private set; }
        private bool _showTemplates;
        public bool ShowTemplates
        {
            get { return _showTemplates; }
            set
            {
                if (_showTemplates == value) return;

                _showTemplates = value;
                RaisePropertyChanged("ShowTemplates");
            }
        }
        private Language _language = Language.CSharp;
        public Language Language
        {
            get { return _language; }
            set
            {
                _language = value;
                var iconPath = Language == Language.CSharp ? CSharpIconPath : VbIconPath;
                TemplateModels.ForEach(model => { model.IconPath = iconPath; });
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void RaisePropertyChanged(string propertyName)
        {
            OnPropertyChanged(propertyName);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class TemplateModel
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string TemplateFilePath { get; set; }
        public string ImagePath { get; set; }
        public string IconPath { get; set; }
        public ProjectSettings ProjectSettings { get; set; }
    }

    public class ProjectSettings
    {
        private static IList<string> _themes;
        private static IList<CoreVersion> _coreVersions;
        private static bool isAddedCore3 = false;

        private string _projectGuid;
        private string _userSecretsGuid;
        private string _currentYear;
        private string _tsFileName;
        private bool _refDataEngineApi;

        public ProjectSettings()
        {
            _projectGuid = Guid.NewGuid().ToString().ToLower();
            _userSecretsGuid = Guid.NewGuid().ToString().ToLower();
            var serverPort = (new Random()).Next(1000, 0xFFFE);
            ServerPort = serverPort.ToString();
            ServerPort1 = (serverPort + 1).ToString();
            _currentYear = DateTime.Now.Year.ToString();
        }

        public static ProjectSettings Create(Dictionary<string, string> replacementsDictionary)
        {
            var settings = new ProjectSettings();
            settings.NamePlaceholder = GetReplacementsStringValue(replacementsDictionary, "NamePlaceholder");
            settings.IsAspNetCore = GetReplacementsBoolValue(replacementsDictionary, "IsAspNetCore");
            settings.IsNetCore = GetReplacementsBoolValue(replacementsDictionary, "IsNetCore");
            settings.IsRazorPages = GetReplacementsBoolValue(replacementsDictionary, "IsRazorPages");
            settings.ShowRazorPages = GetReplacementsBoolValue(replacementsDictionary, "ShowRazorPages");
            settings.CopyET = GetReplacementsBoolValue(replacementsDictionary, "CopyET", true);
            settings.CustomTheme = GetReplacementsStringValue(replacementsDictionary, "CustomTheme", Themes.Default);
            var hideCopyET = GetReplacementsBoolValue(replacementsDictionary, "hideCopyET");
            settings.ShowCopyET = GetReplacementsBoolValue(replacementsDictionary, "ShowCopyET", !hideCopyET);
            settings.ShowWebApi = GetReplacementsBoolValue(replacementsDictionary, "ShowWebApi");
            var referLibraries = GetReplacementsStringValue(replacementsDictionary, "referLibraries");
            settings.IsStandard = GetReplacementsBoolValue(replacementsDictionary, "IsStandard", string.IsNullOrEmpty(referLibraries));
            settings.AddTsFileTemp = GetReplacementsBoolValue(replacementsDictionary, "AddTsFile");
            settings.AspNetCoreVersion = GetReplacementsStringValue(replacementsDictionary, "AspNetCoreVersion", Settings.Default.AspNetCoreVersion);
            settings.EnableDeferredScripts = GetReplacementsBoolValue(replacementsDictionary, "EnableDeferredScripts");

            settings.RefPdf = GetReplacementsBoolValue(replacementsDictionary, "RefPdf");
            settings.RefExcel = GetReplacementsBoolValue(replacementsDictionary, "RefExcel");
            settings.RefZip = GetReplacementsBoolValue(replacementsDictionary, "RefZip");
            settings.RefFinance = GetReplacementsBoolValue(replacementsDictionary, "RefFinance");
            settings.RefFlexSheet = GetReplacementsBoolValue(replacementsDictionary, "RefFlexSheet");
            settings.RefFlexViewer = GetReplacementsBoolValue(replacementsDictionary, "RefFlexViewer");
            settings.RefOlap = GetReplacementsBoolValue(replacementsDictionary, "RefOlap");
            settings.RefMultiRow = GetReplacementsBoolValue(replacementsDictionary, "RefMultiRow");
            settings.RefTransposedGrid = GetReplacementsBoolValue(replacementsDictionary, "RefTransposedGrid");
            settings.RefDataEngineApi = GetReplacementsBoolValue(replacementsDictionary, "RefDataEngineApi");
            settings.TsFileName = GetReplacementsStringValue(replacementsDictionary, "TsFileName", "app.ts");
            settings.ClientIntelliSense = GetReplacementsBoolValue(replacementsDictionary, "ClientIntelliSense");

            return settings;
        }

        private static bool GetReplacementsBoolValue(Dictionary<string, string> replacementsDictionary, string name)
        {
            return GetReplacementsBoolValue(replacementsDictionary, name, false);
        }

        private static bool GetReplacementsBoolValue(Dictionary<string, string> replacementsDictionary, string name, bool defaultValue)
        {
            string valueStr;
            string key = string.Format("${0}$", name.ToLower());
            if (replacementsDictionary.TryGetValue(key, out valueStr))
            {
                return valueStr.Equals("True", StringComparison.OrdinalIgnoreCase);
            }
            return defaultValue;
        }

        private static string GetReplacementsStringValue(Dictionary<string, string> replacementsDictionary, string name)
        {
            return GetReplacementsStringValue(replacementsDictionary, name, null);
        }

        private static string GetReplacementsStringValue(Dictionary<string, string> replacementsDictionary, string name, string defaultValue)
        {
            string valueStr;
            string key = string.Format("${0}$", name.ToLower());
            if (replacementsDictionary.TryGetValue(key, out valueStr))
            {
                return valueStr;
            }
            return defaultValue;
        }

        [ReplacementIgnoreAttribute]
        public bool ShowIntellisense
        {
            get { return Version > new Version("12.0"); }
        }

        [ReplacementIgnoreAttribute]
        public bool ShowCoreVersion
        {
            get
            {
                return IsAspNetCore && Version >= new Version("15.0") && !IsRazorPages;
            }
        }

        [ReplacementIgnoreAttribute]
        public bool ShowC1DocumentSetting
        {
            get { return !IsAspNetCore && IsStandard; }
        }

        [ReplacementIgnoreAttribute]
        public bool ShowCopyET { get; set; }
        [ReplacementIgnoreAttribute]
        public bool EnableCopyET
        {
            get
            {
                return !(ShowRazorPagesWithCoreVersion && UseRazorPages);
            }
        }
        public bool ShowWebApi { get; set; }

        [ReplacementIgnoreAttribute]
        public bool IsStandard { get; private set; }

        [ReplacementIgnoreAttribute]
        public bool AddTsFileTemp { get; set; }

        [ReplacementIgnoreAttribute]
        public IEnumerable<CoreVersion> CoreVersions
        {
            get
            {
                if (_coreVersions == null)
                {
                    _coreVersions = new List<CoreVersion>();
                    //_coreVersions.Add(new CoreVersion("1.0"));
                    //_coreVersions.Add(new CoreVersion("1.1"));
                    if (Version >= new Version("15.0"))
                    {
                        _coreVersions.Add(new CoreVersion("2.0"));
                        _coreVersions.Add(new CoreVersion("2.1"));
                    }                    
                }
                if (Version >= new Version("16.0"))
                {
                    if (isAddedCore3)
                    {
                        if (!(IsAspNetCore && IsNetCore))
                        {
                            _coreVersions.RemoveAt(_coreVersions.Count - 1);
                            if (AspNetCoreVersion == "3.0")
                                AspNetCoreVersion = "2.1";//change to update combobox
                            isAddedCore3 = false;
                        }
                    }
                    else
                    {
                        if (IsAspNetCore && IsNetCore)
                        {
                            _coreVersions.Add(new CoreVersion("3.0"));
                            isAddedCore3 = true;
                        }
                    }
                }
                
                return _coreVersions;
            }
        }

        [ReplacementIgnoreAttribute]
        public IEnumerable<string> PredefinedThemes
        {
            get
            {
                if (_themes == null)
                {
                    var fields = typeof(Themes).GetFields(BindingFlags.Static | BindingFlags.Public);
                    _themes = new List<string>();
                    foreach (var field in fields)
                    {
                        _themes.Add((string)field.GetValue(null));
                    }
                }

                return _themes;
            }
        }

        [ReplacementIgnoreAttribute]
        public string NamePlaceholder { get; private set; }

        public bool IsAspNetCore { get; private set; }
        public bool IsNetCore { get; private set; }
        public bool IsNetCore20
        {
            get { return IsAspNetCore && IsNetCore && AspNetCoreVersion == "2.0"; }
        }
        public bool IsNetCore21
        {
            get { return IsAspNetCore && IsNetCore && AspNetCoreVersion == "2.1"; }
        }
        public bool IsNetFramework20
        {
            get { return IsAspNetCore && !IsNetCore && AspNetCoreVersion == "2.0"; }
        }
        public bool IsNetFramework21
        {
            get { return IsAspNetCore && !IsNetCore && AspNetCoreVersion == "2.1"; }
        }
        public bool IsRazorPages { get; private set; }
        public bool ShowRazorPages { get; private set; }
        public bool UseRazorPages { get; set; }
        [ReplacementIgnoreAttribute]
        public bool ShowRazorPagesWithCoreVersion
        {
            get
            {
                return ShowRazorPages && (AspNetCoreVersion == "2.0" || AspNetCoreVersion == "2.1" || AspNetCoreVersion == "3.0");
            }
        }
        public string AspNetCoreVersion
        {
            get { return Settings.Default.AspNetCoreVersion; }
            set
            {
                Settings.Default.AspNetCoreVersion = value;
                Settings.Default.Save();
            }
        }
        public string CustomTheme { get; set; }
        public bool CustomThemeIsSet
        {
            get
            {
                return !string.IsNullOrEmpty(CustomTheme) && !string.Equals(CustomTheme, Themes.Default, StringComparison.OrdinalIgnoreCase);
            }
        }
        /// <summary>
        /// Copy EditorTemplate.
        /// </summary>
        public bool CopyET { get; set; }
        public bool EnableDeferredScripts { get; set; }
        public bool RefPdf { get; set; }
        public bool RefExcel { get; set; }
        public bool RefZip { get; set; }
        public bool RefFinance { get; set; }
        public bool FinanceScript { get { return RefFinance && !EnableDeferredScripts; } }
        public bool RefFlexSheet { get; set; }
        public bool FlexSheetScript { get { return RefFlexSheet && !EnableDeferredScripts; } }        
        public bool RefFlexViewer { get; set; }
        public bool FlexViewerScript { get { return RefFlexViewer && !EnableDeferredScripts; } }
        public bool RefOlap { get; set; }
        public bool OlapScript { get { return RefOlap && !EnableDeferredScripts; } }
        public bool RefMultiRow { get; set; }
        public bool MultiRowScript { get { return RefMultiRow && !EnableDeferredScripts; } }
        public bool RefTransposedGrid { get; set; }
        public bool TransposedGridScript { get { return RefTransposedGrid && !EnableDeferredScripts; } }
        public bool IncludeWebApi { get { return RefDataEngineApi; } }
        public bool RefDataEngineApi
        {
            get { return _refDataEngineApi && RefOlap; }
            set { _refDataEngineApi = value; }
        }
        public bool AddTsFile
        {
            get
            {
                if (ClientIntelliSense && AddTsFileTemp)
                {
                    return !string.IsNullOrEmpty(TsFileName);
                }
                return false;
            }
        }
        public string TsFileName
        {
            get { return _tsFileName; }
            set
            {
                var fileName = value ?? string.Empty;
                var valid = fileName.ToLower().EndsWith(".ts") && fileName.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) == -1;
                if (!valid)
                {
                    _tsFileName = "app.ts";
                    throw new ArgumentException(Localization.Resources.InvalidTsFileName);
                }

                _tsFileName = value;
            }
        }
        public string ProjectGuid { get { return _projectGuid; } }
        public string UserSecretsGuid { get { return _userSecretsGuid; } }
        public string ServerPort { get; private set; }
        public string ServerPort1 { get; private set; }
        public string CurrentYear { get { return _currentYear; } }
        public bool ClientIntelliSense { get; set; }
        public bool IntellisenseCore { get { return ClientIntelliSense; } }
        public bool IntellisenseFinance { get { return ClientIntelliSense && RefFinance; } }
        public bool IntellisenseFlexsheet { get { return ClientIntelliSense && RefFlexSheet; } }
        public bool IntellisenseFlexViewer { get { return ClientIntelliSense && RefFlexViewer; } }
        public bool IntellisenseOlap { get { return ClientIntelliSense && RefOlap; } }
        public bool IntellisenseMultiRow { get { return ClientIntelliSense && RefMultiRow; } }
        public bool IntellisenseTransposedGrid { get { return ClientIntelliSense && RefTransposedGrid; } }
        public Version Version { get; set; }

        public string LocalDbVersion
        {
            get
            {
                return Version >= new Version("14.0") ? "MSSQLLocalDB" : "v11.0";
            }
        }

        public string C1PkgSuffix
        {
            get
            {
#if GRAPECITY
                return ".ja";
#else
                return "";
#endif
            }
        }
    }

    public class CoreVersion
    {
        private const string AspNetCoreVersionPrefix = "ASP.NET Core ";

        public CoreVersion(string version)
        {
            Version = version;
        }

        public string Version { get; private set; }
        public string FullVersion { get { return AspNetCoreVersionPrefix + Version; } }
    }
}
