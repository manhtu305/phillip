﻿using System;

namespace ProjectTemplateWizard.Models
{
    [AttributeUsageAttribute(AttributeTargets.Property | AttributeTargets.Field,
    AllowMultiple = false, Inherited = true)]
    public sealed class ReplacementIgnoreAttribute : Attribute
    {
    }
}
