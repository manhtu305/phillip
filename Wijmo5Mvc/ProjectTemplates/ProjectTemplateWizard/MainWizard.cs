﻿using Microsoft.VisualStudio.TemplateWizard;
using System.Collections.Generic;
using System.Text;
using EnvDTE;
using System.IO;
using ProjectTemplateWizard.Localization;
using ProjectTemplateWizard.Models;
using System.Xml.Linq;
using System.Xml.XPath;
using System;

namespace ProjectTemplateWizard
{
    /// <summary>
    /// A wizard only for gallery templates.
    /// </summary>
    public class MainWizard : IWizard
    {
        private DTE _dte;
        private string _solutionPath;
        private string _templatePath;
        private string _projectName;
        private string _destinationPath;

        public void BeforeOpeningFile(ProjectItem projectItem)
        {
        }

        public void ProjectFinishedGenerating(Project project)
        {
        }

        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
        }

        public void RunFinished()
        {
            _dte.Solution.AddFromTemplate(_templatePath, _destinationPath, _projectName);
        }

        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            var settings = GenerateTemplatesSettings(automationObject, customParams, replacementsDictionary);
            var result = Utils.ShowDialog(settings);
            if (!result.HasValue || !result.Value)
            {
                throw new WizardCancelledException();
            }

            _dte = (DTE)automationObject;
            replacementsDictionary.TryGetValue("$safeprojectname$", out _projectName);
            replacementsDictionary.TryGetValue("$solutiondirectory$", out _solutionPath);
            replacementsDictionary.TryGetValue("$destinationdirectory$", out _destinationPath);
            _solutionPath = Path.Combine(_solutionPath, _projectName + ".sln");
            _templatePath = settings.TemplateModels[settings.SelectedTemplateIndex].TemplateFilePath;
            if (settings.SelectedProjectSettings.IsAspNetCore
                && settings.SelectedProjectSettings.Version >= new Version("15.0"))
            {
                // Template should follow this rule,
                // Base template name: AspNetCoreAppCs.
                // Core 1.0 template name: AspNetCore10AppCs.
                // Core 2.0 template name: AspNetCore20AppCs.
                // Core 2.0 (Razor Pages) template name: AspNetCore20RPAppCs
                // When searching template, replace the 'Core' part to 'Core10', 'Core20' or 'Core20RP'.
                var coreVersion = string.Format("{0}0", settings.SelectedProjectSettings.AspNetCoreVersion[0]);
                var replaceName = "Core" + coreVersion;
                if(settings.SelectedProjectSettings.ShowRazorPagesWithCoreVersion && settings.SelectedProjectSettings.UseRazorPages)
                {
                    replaceName += "RP";
                }
                _templatePath = ResolveVsTemplate(_templatePath, "Core", replaceName);
            }
            var replacements = new Dictionary<string, string>();
            //should pass $targetframeworkversion$ to exact template, otherwise will get incorrect version.
            replacements.Add("$targetframeworkversion$", replacementsDictionary["$targetframeworkversion$"]);
            replacements.Add("$hidedialog$", "True");

            string exclusiveProject = "False";
            replacementsDictionary.TryGetValue("$exclusiveproject$", out exclusiveProject);
            replacements.Add("$__exclusiveproject$", exclusiveProject);

            Utils.AddReplacements(replacements, settings.SelectedProjectSettings);
            AddTemplateParameters(replacements);
        }

        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        private static TemplatesSettings GenerateTemplatesSettings(object automationObject, object[] customParams, Dictionary<string, string> replacementsDictionary)
        {
            var settings = new TemplatesSettings();
            string hideTemplatesStr = "";
            var hideTemplates = replacementsDictionary.TryGetValue("$hidetemplates$", out hideTemplatesStr) && hideTemplatesStr.Equals("True", StringComparison.OrdinalIgnoreCase);
            settings.ShowTemplates = !hideTemplates;

            string languageStr = "";
            var language = Models.Language.CSharp;
            if (replacementsDictionary.TryGetValue("$language$", out languageStr)
                && languageStr.ToLower().Equals("vb", StringComparison.OrdinalIgnoreCase))
            {
                language = Models.Language.Vb;
            }
            settings.Language = language;

            string namePlaceholder = "";
            if (replacementsDictionary.TryGetValue("$nameplaceholder$", out namePlaceholder))
            {
                foreach (var template in settings.TemplateModels)
                {
                    template.TemplateFilePath = ResolveVsTemplate(customParams[0] as string, namePlaceholder, namePlaceholder + template.Name);
                    template.Description = GetTemplateData(template.TemplateFilePath, "Description");
                    var previewImage = GetTemplateData(template.TemplateFilePath, "PreviewImage");
                    template.ImagePath = Path.Combine(Path.GetDirectoryName(template.TemplateFilePath), previewImage);
                    template.ProjectSettings = GenerateTemplateProjectSettings(template, automationObject, replacementsDictionary);
                }
            }
            else
            {
                var projectSettings = Utils.GenerateSettings(automationObject, replacementsDictionary);
                settings.TemplateModels[0].ProjectSettings = projectSettings;
            }
            settings.SelectedTemplateIndex = 0;
            return settings;
        }

        private static Dictionary<string, string> GetTemplateParameters(string templatePath)
        {
            var parametersDic = new Dictionary<string, string>();
            var doc = XDocument.Load(templatePath);
            var parameterNodes = doc.XPathSelectElements(@"/*[local-name()='VSTemplate']/*[local-name()='TemplateContent']/*[local-name()='CustomParameters']/*[local-name()='CustomParameter']");
            foreach (XElement node in parameterNodes)
            {
                parametersDic.Add(node.Attribute("Name").Value, node.Attribute("Value").Value);
            }
            return parametersDic;
        }

        private static string GetTemplateData(string templatePath, string name)
        {
            var doc = XDocument.Load(templatePath);
            var node = doc.XPathSelectElement(string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='TemplateData']/*[local-name()='{0}']", name));
            return node.Value;
        }

        private static ProjectSettings GenerateTemplateProjectSettings(TemplateModel template, object automationObject, Dictionary<string, string> replacementsDictionary)
        {
            var parameters = GetTemplateParameters(template.TemplateFilePath);
            foreach (var key in parameters.Keys)
            {
                string value;
                if (replacementsDictionary.TryGetValue(key, out value))
                {
                    replacementsDictionary[key] = parameters[key];
                }
                else
                {
                    replacementsDictionary.Add(key, parameters[key]);
                }
            }
            var projectSettings = Utils.GenerateSettings(automationObject, replacementsDictionary);
            foreach (var key in parameters.Keys)
            {
                replacementsDictionary.Remove(key);
            }

            return projectSettings;
        }

        private static string ResolveVsTemplate(string vsTemplate, string placeholder, string replaceName)
        {
            var rootPath = Path.GetDirectoryName(vsTemplate);
            FileInfo fileInfo = new FileInfo(vsTemplate);
            var templateName = Path.GetFileNameWithoutExtension(vsTemplate);
            templateName = templateName.Replace(placeholder, replaceName) + ".vstemplate";

            var templatePath = SearchTemplate(fileInfo.Directory.Parent.FullName, templateName);
            if (string.IsNullOrEmpty(templatePath))
            {
                throw new FileNotFoundException(string.Format(Resources.TemplateNotFound, templateName));
            }

            return templatePath;
        }

        private static string SearchTemplate(string targetDir, string fileName)
        {
            foreach (string d in Directory.GetDirectories(targetDir))
            {
                var filePath = Path.Combine(d, fileName);
                if (File.Exists(filePath))
                {
                    return filePath;
                }
            }
            return string.Empty;
        }

        private void AddTemplateParameters(Dictionary<string, string> replacementsDictionary)
        {
            var builder = new StringBuilder();
            foreach (var key in replacementsDictionary.Keys)
            {
                builder.Append(string.Format("|{0}={1}", key, replacementsDictionary[key]));
            }
            _templatePath += builder.ToString();
        }
    }
}
