﻿using EnvDTE;
using EnvDTE80;
using ProjectTemplateWizard.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ProjectTemplateWizard
{
    internal abstract class WizardHelper
    {
        private const string TS_REFERENCE = "/// <reference path=\"typings/c1.mvc.{0}.lib.d.ts\" />";
        private const string C1PkgRegistryPrefix = "ComponentOne";
        private const string C1MvcPkgRegistry = C1PkgRegistryPrefix + " Mvc";
        protected const string C1WebApiPkgRegistry = C1PkgRegistryPrefix + " WebApi";

        private IEnumerable<Package> _financePackages;
        private IEnumerable<Package> _flexSheetPackages;
        private IEnumerable<Package> _flexViewerPackages;
        private IEnumerable<Package> _olapPackages;
        private IEnumerable<Package> _multiRowPackages;
        private IEnumerable<Package> _transposedGridPackages;

        private IEnumerable<Package> _webApiBasePackages;
        private IEnumerable<Package> _webApiDataEnginePackages;

        protected WizardHelper(DTE dte, ProjectSettings projectSettings, Dictionary<string, string> replacements)
        {
            Dte = dte;
            ProjectSettings = projectSettings;
            Replacements = replacements;
        }

        internal DTE Dte { get; set; }

        internal ProjectSettings ProjectSettings
        {
            get;
            set;
        }

        protected Dictionary<string, string> Replacements
        {
            get;
            set;
        }

        protected string CustomizedVsTemplate
        {
            get;
            set;
        }

        protected virtual bool SupportOfflinePackages
        {
            get
            {
                return false;
            }
        }

        private IEnumerable<Package> FinancePackages
        {
            get
            {
                if (_financePackages == null)
                {
                    _financePackages = new List<Package> { CreateMvcPackage("Finance") };
                }

                return _financePackages;
            }
        }

        private IEnumerable<Package> FlexSheetPackages
        {
            get
            {
                if (_flexSheetPackages == null)
                {
                    _flexSheetPackages = new List<Package> { CreateMvcPackage("FlexSheet") };
                }

                return _flexSheetPackages;
            }
        }

        private IEnumerable<Package> FlexViewerPackages
        {
            get
            {
                if (_flexViewerPackages == null)
                {
                    _flexViewerPackages = new List<Package> { CreateMvcPackage("FlexViewer") };
                }

                return _flexViewerPackages;
            }
        }

        private IEnumerable<Package> OlapPackages
        {
            get
            {
                if (_olapPackages == null)
                {
                    _olapPackages = new List<Package> { CreateMvcPackage("Olap") };
                }

                return _olapPackages;
            }
        }

        private IEnumerable<Package> MultiRowPackages
        {
            get
            {
                if (_multiRowPackages == null)
                {
                    _multiRowPackages = new List<Package> { CreateMvcPackage("MultiRow") };
                }

                return _multiRowPackages;
            }
        }

        private IEnumerable<Package> TransposedGridPackages
        {
            get
            {
                if (_transposedGridPackages == null)
                {
                    _transposedGridPackages = new List<Package> { CreateMvcPackage("TransposedGrid") };
                }

                return _transposedGridPackages;
            }
        }

        protected virtual IEnumerable<Package> WebApiBasePackages
        {
            get
            {
                if (_webApiBasePackages == null)
                {
                    _webApiBasePackages = new List<Package> { CreateWebApiPackage("") };
                }

                return _webApiBasePackages;
            }
        }

        protected virtual IEnumerable<Package> WebApiDataEnginePackages
        {
            get
            {
                if (_webApiDataEnginePackages == null)
                {
                    _webApiDataEnginePackages = new List<Package> { CreateWebApiPackage("DataEngine") };
                }

                return _webApiDataEnginePackages;
            }
        }

        protected abstract string C1MvcPkgPrefix { get; }

        protected abstract string C1MvcPkgMainVersion { get; }

        protected abstract string C1PkgMinorVersion { get; }

        protected abstract string C1WebApiPkgPrefix { get; }

        protected abstract string C1WebApiPkgMainVersion { get; }

        protected Package CreateMvcPackage(string moduleName)
        {
            const string c1PkgSuffix =
#if GRAPECITY
            ".ja";
#else
            "";
#endif
            return new Package(C1MvcPkgPrefix + "." + moduleName + c1PkgSuffix,
                C1MvcPkgRegistry, C1MvcPkgMainVersion + "." + C1PkgMinorVersion + AssemblyInfo.VersionSuffix);
        }

        protected Package CreateWebApiPackage(string moduleName)
        {
            const string c1PkgSuffix =
#if GRAPECITY
            ".ja";
#else
            "";
#endif
            if (moduleName.Length > 0)
            {
                moduleName = "." + moduleName;
            }
            return new Package(C1WebApiPkgPrefix + moduleName + c1PkgSuffix,
                C1WebApiPkgRegistry, C1WebApiPkgMainVersion + "." + C1PkgMinorVersion + AssemblyInfo.WebApiVersionSuffix);
        }

        public virtual void ProjectFinishedGenerating(Project project)
        {
            ProcessEditorTemplates(project);
            ProcessIntellisense(project);
            ProcessWebApi(project);
        }

        public virtual void RunStarted(object[] customParams)
        {
            if (SupportOfflinePackages)
            {
                AddPackages(customParams);
            }

            AdjustReplacements();
        }

        protected virtual void AdjustReplacements()
        {
            Utils.AddReplacements(Replacements, ProjectSettings);
        }

        public virtual void RunFinished()
        {
            if (!string.IsNullOrEmpty(CustomizedVsTemplate) && File.Exists(CustomizedVsTemplate))
            {
                File.Delete(CustomizedVsTemplate);
            }
        }

        protected void AddPackages(object[] customParams)
        {
            var vsTemplate = customParams[0] as string;
            CustomizedVsTemplate = Path.Combine(Path.GetDirectoryName(vsTemplate), Guid.NewGuid() + ".vstemplate");

            var doc = XDocument.Load(vsTemplate);
            var registryNode = doc.XPathSelectElement(string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[contains(@keyName, '{0}')]", C1PkgRegistryPrefix));
            if (registryNode == null)
            {
                return;
            }
            var nodes = doc.XPathSelectElements(
                 string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[local-name()='packages']/*[contains(@id, '{0}')]", "C1.Web.Mvc"));
            if (nodes != null)
            {
                string minorVersion = C1PkgMinorVersion;
                if (minorVersion != "5")
                {
                    foreach (XElement node in nodes)
                    {
                        foreach (XAttribute at in node.Attributes())
                        {
                            if (at.Value.StartsWith("4.5."))
                            {
                                at.SetValue(at.Value.Replace("4.5.", "4." + minorVersion + "."));
                            }
                        }
                    }
                }
            }

            var packages = PrepareExtraPackages();
            AddExtraPackages(doc, packages);
            doc.Save(CustomizedVsTemplate);

            customParams[0] = CustomizedVsTemplate;
        }

        private IEnumerable<Package> PrepareExtraPackages()
        {
            var packages = new List<Package>();
            if (ProjectSettings.RefFinance)
            {
                packages.AddRange(FinancePackages);
            }

            if (ProjectSettings.RefFlexSheet)
            {
                packages.AddRange(FlexSheetPackages);
            }

            if (ProjectSettings.RefFlexViewer)
            {
                packages.AddRange(FlexViewerPackages);
            }

            if (ProjectSettings.RefOlap)
            {
                packages.AddRange(OlapPackages);
            }

            if (ProjectSettings.RefMultiRow)
            {
                packages.AddRange(MultiRowPackages);
            }

            if (ProjectSettings.RefTransposedGrid)
            {
                packages.AddRange(TransposedGridPackages);
            }

            if (ProjectSettings.IncludeWebApi)
            {
                packages.AddRange(WebApiBasePackages);
            }

            if (ProjectSettings.RefDataEngineApi)
            {
                packages.AddRange(WebApiDataEnginePackages);
            }

            return packages;
        }

        private static void AddExtraPackages(XDocument doc, IEnumerable<Package> packages)
        {
            var repositories = new Dictionary<string, IDictionary<string, Package>>();
            foreach (var package in packages)
            {
                AddPackageToDictionary(repositories, package);
            }

            foreach (var registry in repositories.Keys)
            {
                AddPackages(doc, registry, repositories[registry].Values);
            }
        }

        private static void AddPackages(XDocument doc, string registry, IEnumerable<Package> packages)
        {
            var registryNode = doc.XPathSelectElement(string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[@keyName='{0}']", registry));
            foreach (var package in packages)
            {
                var packageNode = registryNode.XPathSelectElement(string.Format(@"*[@id='{0}']", package.Id));
                if (packageNode == null)
                {
                    packageNode = new XElement("package");
                    registryNode.Add(packageNode);
                }

                packageNode.SetAttributeValue("id", package.Id);
                packageNode.SetAttributeValue("version", package.Version);
                packageNode.SetAttributeValue("skipAssemblyReferences", "false");
            }
        }

        private static void AddPackageToDictionary(IDictionary<string, IDictionary<string, Package>> repositories, Package package)
        {
            IDictionary<string, Package> packages;
            if (!repositories.TryGetValue(package.Registry, out packages))
            {
                packages = new Dictionary<string, Package>();
                repositories.Add(package.Registry, packages);
            }

            Package existedPackage;
            if (packages.TryGetValue(package.Id, out existedPackage))
            {
                if (new Version(existedPackage.Version) >= new Version(package.Version))
                {
                    return;
                }

                packages.Remove(package.Id);
            }

            packages.Add(package.Id, package);
        }

        protected virtual string ScriptsPath
        {
            get
            {
                return "Scripts";
            }
        }

        protected virtual void RemovePath(Project project, params string[] paths)
        {
            var projectPath = Path.GetDirectoryName(project.FullName);
            var path = Path.Combine(new string[] { projectPath }.Concat(paths).ToArray());
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
                return;
            }

            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        private void ProcessIntellisense(Project project)
        {
            // Client intellisense are copied as default.
            // Remove the client intellisense if not use them.
            if (!ProjectSettings.ClientIntelliSense)
            {
                RemovePath(project, ScriptsPath, "typings");
                return;
            }

            if (!ProjectSettings.RefFinance)
            {
                RemovePath(project, ScriptsPath, "typings", "c1.mvc.finance.lib.d.ts");
            }

            if (!ProjectSettings.RefFlexSheet)
            {
                RemovePath(project, ScriptsPath, "typings", "c1.mvc.flexsheet.lib.d.ts");
            }

            if (!ProjectSettings.RefFlexViewer)
            {
                RemovePath(project, ScriptsPath, "typings", "c1.mvc.flexviewer.lib.d.ts");
            }

            if (!ProjectSettings.RefOlap)
            {
                RemovePath(project, ScriptsPath, "typings", "c1.mvc.olap.lib.d.ts");
            }

            if (!ProjectSettings.RefMultiRow)
            {
                RemovePath(project, ScriptsPath, "typings", "c1.mvc.multirow.lib.d.ts");
            }

            if (!ProjectSettings.RefTransposedGrid)
            {
                RemovePath(project, ScriptsPath, "typings", "c1.mvc.transposedgrid.lib.d.ts");
            }

            if (ProjectSettings.AddTsFile)
            {
                AddTsFile(project);
            }
        }

        protected virtual void AddTsFile(Project project)
        {
            var projectPath = Path.GetDirectoryName(project.FullName);
            string tsFile = Path.Combine(projectPath, ScriptsPath, ProjectSettings.TsFileName);
            using (var sw = File.AppendText(tsFile))
            {
                sw.WriteLine(string.Format(TS_REFERENCE, "basic"));
                if (ProjectSettings.RefFinance)
                {
                    sw.WriteLine(string.Format(TS_REFERENCE, "finance"));
                }
                if (ProjectSettings.RefFlexSheet)
                {
                    sw.WriteLine(string.Format(TS_REFERENCE, "flexsheet"));
                }
                if (ProjectSettings.RefFlexViewer)
                {
                    sw.WriteLine(string.Format(TS_REFERENCE, "flexviewer"));
                }
                if (ProjectSettings.RefOlap)
                {
                    sw.WriteLine(string.Format(TS_REFERENCE, "olap"));
                }
                if (ProjectSettings.RefMultiRow)
                {
                    sw.WriteLine(string.Format(TS_REFERENCE, "multirow"));
                }
                if (ProjectSettings.RefTransposedGrid)
                {
                    sw.WriteLine(string.Format(TS_REFERENCE, "transposedgrid"));
                }
            }
        }

        private void ProcessEditorTemplates(Project project)
        {
            // EditorTemplates are copied as default. Whether copy or not is processed by replacement
            if (ProjectSettings.CopyET)
            {
                return;
            }

            RemovePath(project, "Views", "Shared", "EditorTemplates");
        }

        protected static ProjectItem GetProjectItem(Project project, string path)
        {
            ProjectItem projectItem = null;
            foreach (var item in path.Split('\\'))
            {
                if (!string.IsNullOrEmpty(item))
                {
                    if (projectItem == null)
                    {
                        projectItem = project.ProjectItems.Item(item);
                    }
                    else
                    {
                        projectItem = projectItem.ProjectItems.Item(item);
                    }
                }
            }

            return projectItem;
        }

        protected virtual string GetFrameworkVersion()
        {
            return Replacements["$targetframeworkversion$"];
        }

        private void ProcessWebApi(Project project)
        {
            // Only Mvc5 2015 and upper version template process webapi, these templates has parameter 'ShowWebApi'.
            // WebApiConfig are copied as default. Whether copy or not is processed by replacement parameter 'IncludeWebApi'.
            if (!ProjectSettings.ShowWebApi || ProjectSettings.IncludeWebApi)
            {
                return;
            }

            RemovePath(project, "App_Start", "WebApiConfig.cs");
            RemovePath(project, "App_Start", "WebApiConfig.vb");
        }

        internal static WizardHelper Create(DTE dte, ProjectSettings projectSettings, Dictionary<string, string> replacements)
        {
            return projectSettings.IsAspNetCore ? new AspNetCoreWizardHelper(dte, projectSettings, replacements) :
                (WizardHelper)new AspNetWizardHelper(dte, projectSettings, replacements);
        }
    }

    internal class AspNetWizardHelper : WizardHelper
    {
        private IEnumerable<Package> _webApiDataEnginePackages;

        public AspNetWizardHelper(DTE dte, ProjectSettings projectSettings, Dictionary<string, string> replacements) : base(dte, projectSettings, replacements)
        { }

        protected override string C1MvcPkgPrefix
        {
            get
            {
                return "C1.Web.Mvc";
            }
        }

        protected override string C1MvcPkgMainVersion
        {
            get
            {
                return "4";
            }
        }

        private string minorVersion = "";
        protected override string C1PkgMinorVersion
        {
            get
            {
                if (minorVersion == "")
                {
                    var versionStr = Replacements["$targetframeworkversion$"];
                    var version = new Version(versionStr);
                    var minVersion = new Version("4.5.2");
                    minorVersion = version < minVersion ? "0" : "5";
                }
                return minorVersion;
            }
        }

        protected override bool SupportOfflinePackages
        {
            get
            {
                return ProjectSettings.Version >= new Version("14.0");
            }
        }

        protected override string C1WebApiPkgPrefix
        {
            get
            {
                return "C1.Web.Api";
            }
        }

        protected override string C1WebApiPkgMainVersion
        {
            get
            {
                return "4";
            }
        }

        private string winFormsVersion = "";
        internal string WinFormsVersion
        {
            get
            {
                if (winFormsVersion == "")
                {
                    Version ver = new Version(AssemblyInfo.C1WinFormsVersion);
                    winFormsVersion = string.Format("4.{0}.{1}.{2}", C1PkgMinorVersion, ver.Build, ver.Revision);
                }
                return winFormsVersion;
            }
        }

        internal string DataEngineWinFormsVersion
        {
            get
            {
                if (C1PkgMinorVersion == "0")
                {
                    return WinFormsVersion;
                }
#if GRAPECITY
                return "5.0.20202.155";
#else
                return "5.0.20202.154";
#endif
            }
        }

        private string _c1PkgSuffix
        {
            get
            {
#if GRAPECITY
                return C1PkgMinorVersion == "0" ? "" : ".Ja";
#else
                return "";
#endif
            }
        }

        protected override IEnumerable<Package> WebApiDataEnginePackages
        {
            get
            {
                if (_webApiDataEnginePackages == null)
                {
                    var aspNetDataEnginePackages = new List<Package> { new Package("C1.DataEngine" + _c1PkgSuffix, C1WebApiPkgRegistry, DataEngineWinFormsVersion) };

                    aspNetDataEnginePackages.AddRange(base.WebApiDataEnginePackages);
                    _webApiDataEnginePackages = aspNetDataEnginePackages;
                }

                return _webApiDataEnginePackages;
            }
        }

        protected override void AddTsFile(Project project)
        {
            base.AddTsFile(project);

            //Fix issue: the ts file has been created, but it shows with non-existing icon in VS Solution Explorer.
            var projectPath = Path.GetDirectoryName(project.FullName);
            string tsFile = Path.Combine(projectPath, ScriptsPath, ProjectSettings.TsFileName);
            var item = GetProjectItem(project, ScriptsPath);
            if (item != null)
            {
                item.ProjectItems.AddFromFileCopy(tsFile);
            }
        }

        protected override void AdjustReplacements()
        {
            base.AdjustReplacements();
            string nameRefFormat = "{0}, Version=4.{1}{2}, Culture=neutral, PublicKeyToken=9b75583953471eea, processorArchitecture=MSIL";
            string packageRefFormat = "..\\packages\\{0}" +
#if GRAPECITY
                ".ja" +
#endif
            ".4.{1}{2}\\lib\\net{3}\\{0}.dll";
            string mvcFrameworkname = C1PkgMinorVersion == "0" ? "40" : "45";
            //MVCs
            string[] mvcAssemblies = new string[] { "C1.Web.Mvc", "C1.Web.Mvc.Finance", "C1.Web.Mvc.FlexSheet", "C1.Web.Mvc.Olap", "C1.Web.Mvc.MultiRow", "C1.Web.Mvc.FlexViewer", "C1.Web.Mvc.TransposedGrid" };
            for (int i = 0; i < 7; i++)
            {
                Replacements[string.Format("$mvcNameRef{0}$", i)] = string.Format(nameRefFormat, mvcAssemblies[i], C1PkgMinorVersion, AssemblyInfo.VersionSuffix);
                Replacements[string.Format("$mvcPackageRef{0}$", i)] = string.Format(packageRefFormat, mvcAssemblies[i], C1PkgMinorVersion, AssemblyInfo.VersionSuffix, mvcFrameworkname);
                Replacements[string.Format("$mvcNameOnlyRef{0}$", i)] = mvcAssemblies[i];
            }

            //APIs
            string apiFormat = "{0}, Version=4.{1}{2}, Culture=neutral, PublicKeyToken=9b75583953471eea, processorArchitecture=MSIL";
            string apiPkgFormat = "..\\packages\\{0}" +
#if GRAPECITY
                ".ja" +
#endif
                ".4.{1}{2}\\lib\\net45\\{0}.dll";
            Replacements["$apiNameRef$"] = string.Format(apiFormat, "C1.Web.Api", C1PkgMinorVersion, AssemblyInfo.WebApiVersionSuffix);
            Replacements["$apiPkg$"] = string.Format(apiPkgFormat, "C1.Web.Api", C1PkgMinorVersion, AssemblyInfo.WebApiVersionSuffix);

            Replacements["$apiDataEngineRef$"] = string.Format(apiFormat, "C1.Web.Api.DataEngine", C1PkgMinorVersion, AssemblyInfo.WebApiVersionSuffix);
            Replacements["$apiDataEnginePkg$"] = string.Format(apiPkgFormat, "C1.Web.Api.DataEngine", C1PkgMinorVersion, AssemblyInfo.WebApiVersionSuffix);

            //Wins
            string windllNameSuffix = "4.5.2";
            string dataEngineWinDllNameSuffix = "";
            string pkgPdfName = "C1.C1Pdf";
            string pkgExcelName = "C1.C1Excel";
            string pkgZipName = "C1.C1Zip";
            string pkgDataEngineName = "C1.DataEngine";

#if GRAPECITY
                pkgPdfName += ".Ja";
                pkgExcelName  += ".Ja";
                pkgZipName  += ".Ja";
                pkgDataEngineName  += ".Ja";
#endif
            string pkgTemplate = "{0}.{1}, Version={2}, Culture=neutral, PublicKeyToken=79882d576c6336da, processorArchitecture=MSIL";

            string dataEnginePkgTemplate = "{0}{1}, Version={2}, Culture=neutral, PublicKeyToken=6e0c8e6954369090, processorArchitecture=MSIL";

            string winNetFolder = "52";
            if (C1PkgMinorVersion == "0")
            {
                windllNameSuffix = "4";
                dataEngineWinDllNameSuffix = ".4";
                winNetFolder = "0";
                pkgPdfName = "C1.Document";
                pkgExcelName = "C1.Excel";
                pkgZipName = "C1.Document";
                pkgDataEngineName = "C1.DataEngine";

                dataEnginePkgTemplate = "{0}{1}, Version={2}, Culture=neutral, PublicKeyToken=79882d576c6336da, processorArchitecture=MSIL";
            }

            Replacements["$pkgPdfName$"] = pkgPdfName;
            Replacements["$pdfRef$"] = string.Format(pkgTemplate, "C1.C1Pdf", windllNameSuffix, WinFormsVersion);
            Replacements["$pdfRefPkg$"] = string.Format("..\\packages\\{3}.{0}\\lib\\net4{1}\\C1.C1Pdf.{2}.dll", WinFormsVersion, winNetFolder, windllNameSuffix, pkgPdfName);
            Replacements["$pkgExcelName$"] = pkgExcelName;
            Replacements["$excelRef$"] = string.Format(pkgTemplate, "C1.C1Excel", windllNameSuffix, WinFormsVersion);
            Replacements["$excelRefPkg$"] = string.Format("..\\packages\\{3}.{0}\\lib\\net4{1}\\C1.C1Excel.{2}.dll", WinFormsVersion, winNetFolder, windllNameSuffix, pkgExcelName);
            Replacements["$pkgZipName$"] = pkgZipName;
            Replacements["$c1ZipRef$"] = string.Format(pkgTemplate, "C1.C1Zip", windllNameSuffix, WinFormsVersion);
            Replacements["$c1ZipRefPkg$"] = string.Format("..\\packages\\{3}.{0}\\lib\\net4{1}\\C1.C1Zip.{2}.dll", WinFormsVersion, winNetFolder, windllNameSuffix, pkgZipName);

            Replacements["$pkgDataEngineName$"] = pkgDataEngineName;
            Replacements["$winDataEngineRef$"] = string.Format(dataEnginePkgTemplate, "C1.DataEngine", dataEngineWinDllNameSuffix, DataEngineWinFormsVersion);
            Replacements["$winDataEnginePkg$"] = string.Format("..\\packages\\{3}.{0}\\lib\\net4{1}\\C1.DataEngine{2}.dll", DataEngineWinFormsVersion, winNetFolder, dataEngineWinDllNameSuffix, pkgDataEngineName);
            Replacements["$dataenginewinversion$"] = DataEngineWinFormsVersion;

            Replacements["$winversion$"] = WinFormsVersion;
            Replacements["$webapiversion$"] = string.Format("4.{0}{1}", C1PkgMinorVersion, AssemblyInfo.WebApiVersionSuffix);
            Replacements["$webversion$"] = string.Format("4.{0}{1}", C1PkgMinorVersion, AssemblyInfo.VersionSuffix);

            //condition to add package zip in case under net452: same name C1.Document with pdf
            bool hasRegisterPdf = Replacements["$refpdf$"] == "True";
            bool hasRegisterZip = Replacements["$refzip$"] == "True";
            bool requireZippkg = hasRegisterZip && (pkgZipName.Contains("C1.C1Zip") || !hasRegisterPdf);
            if (requireZippkg)
                Replacements["$refzip2$"] = "True";
            else Replacements["$refzip2$"] = "False";

            #region Pdf 4.0 references
            if (hasRegisterPdf && C1PkgMinorVersion == "0")
            {
                Replacements["$refpdf40$"] = "True";
                Replacements["$refpdf40c1WinC1Document$"] = "C1.Win.C1Document.4";
                Replacements["$refpdf40c1WinC1DocumentHintPath$"] = string.Format("..\\packages\\C1.Document.{0}\\lib\\net40\\C1.Win.C1Document.4.dll", WinFormsVersion);
            }
            #endregion
        }
    }

    internal class AspNetCoreWizardHelper : WizardHelper
    {
        private class DotnetSdk
        {
            public DotnetSdk(string name, Version version)
            {
                FullVersion = name;
                Version = version;
            }
            public string FullVersion { get; set; }
            public Version Version { get; set; }
        }

        private string _globalJsonPath;

        public AspNetCoreWizardHelper(DTE dte, ProjectSettings projectSettings, Dictionary<string, string> replacements) : base(dte, projectSettings, replacements)
        {
        }

        protected override string C1MvcPkgPrefix
        {
            get
            {
                return "C1.AspNetCore.Mvc";
            }
        }

        protected override string C1MvcPkgMainVersion
        {
            get
            {
                return "1";
            }
        }

        protected override string C1PkgMinorVersion
        {
            get
            {
                return "0";
            }
        }

        protected override string C1WebApiPkgPrefix
        {
            get
            {
                return "C1.AspNetCore.Api";
            }
        }

        protected override string C1WebApiPkgMainVersion
        {
            get
            {
                return "1";
            }
        }

        public override void RunStarted(object[] customParams)
        {
            base.RunStarted(customParams);
            ProcessGlobalJson();
        }

        public override void RunFinished()
        {
            base.RunFinished();

#if !GRAPECITY
            try
            {
                Dte.ExecuteCommand("Tools.GrapeCityLicenseManager");
            }
            catch { }
#endif
        }

        private void ProcessGlobalJson()
        {
            var isVs2015 = ProjectSettings.Version == new Version("14.0");
            if (!isVs2015) return;

            string solutionDirectory, projectName, destinationDirectory, exclusiveProject, __exclusiveProject;
            Replacements.TryGetValue("$solutiondirectory$", out solutionDirectory);
            Replacements.TryGetValue("$destinationdirectory$", out destinationDirectory);
            Replacements.TryGetValue("$projectname$", out projectName);
            Replacements.TryGetValue("$exclusiveproject$", out exclusiveProject);
            Replacements.TryGetValue("$__exclusiveproject$", out __exclusiveProject);

            var createDirectoryForSolution = solutionDirectory != null && destinationDirectory != null && projectName != null
                && string.Equals(Path.Combine(solutionDirectory, projectName), destinationDirectory, StringComparison.OrdinalIgnoreCase);
            var createNewSolution = __exclusiveProject == "True" || exclusiveProject == "True";
            if (!createDirectoryForSolution || !createNewSolution) return;

            var globalJsonPath = Path.Combine(solutionDirectory, "global.json");
            if (File.Exists(globalJsonPath)) return;

            var sdkVersion = GetGlobalJsonSdkVersion();
            if (string.IsNullOrEmpty(sdkVersion)) return;

            var content = GetGlobalJsonContent();
            content = content.Replace("DOTNET_SDK_VERSION", sdkVersion);
            File.WriteAllText(globalJsonPath, content);
            _globalJsonPath = globalJsonPath;
        }

        public override void ProjectFinishedGenerating(Project project)
        {
            base.ProjectFinishedGenerating(project);
            if (ProjectSettings.IsNetCore && ProjectSettings.Version >= new Version("15.0"))
            {
                RemovePath(project, "app.config");
            }

            if (!string.IsNullOrEmpty(_globalJsonPath))
            {
                var solution = project.DTE.Solution as Solution2;
                var solutionItems = GetSolutionItemsProject(solution);
                var globalJsonItem = solutionItems.ProjectItems.AddFromFile(_globalJsonPath);
                if (globalJsonItem != null && globalJsonItem.IsOpen)
                {
                    var window = globalJsonItem.Open();
                    window.Close();
                }
            }
        }

        private Project GetSolutionItemsProject(Solution2 solution)
        {
            try
            {
                return solution.Item(Constants.vsProjectKindSolutionItems);
            }
            catch (ArgumentException)
            {
                return solution.AddSolutionFolder("Solution Items");
            }
        }

        private string GetGlobalJsonContent()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ProjectTemplateWizard.Resources.global.json"))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        private string GetGlobalJsonSdkVersion()
        {
            var sdkFolder = @"c:\Program Files\dotnet\sdk\";
            if (!Directory.Exists(sdkFolder)) return null;

            var sdks = new List<DotnetSdk>();
            foreach (var folder in Directory.GetDirectories(sdkFolder))
            {
                var name = Path.GetFileName(folder);
                var index = name.IndexOf('-');
                var versionString = index > 0 ? name.Substring(0, index) : name;
                Version version;
                if (Version.TryParse(versionString, out version))
                {
                    sdks.Add(new DotnetSdk(name, version));
                }
            }

            if (sdks.Count == 0) return null;

            var sdk100Version = new Version(1, 0, 0);
            var maxSdkVersion = sdks.Max(sdk => sdk.Version);
            if (maxSdkVersion <= sdk100Version) return null;

            var sdk100s = sdks.Where(sdk => sdk.Version <= sdk100Version);
            if (!sdk100s.Any()) return null;

            return sdk100s.Max(sdk => sdk.FullVersion);
        }

        protected override string GetFrameworkVersion()
        {
            var versionStr = Replacements["$targetframeworkversion$"];
            var aspNetCoreVersion = new Version(ProjectSettings.AspNetCoreVersion);
            var version = new Version(versionStr);
            var minVersion = aspNetCoreVersion < new Version("2.0") ? new Version("4.5.2") : new Version("4.6.1");
            return version < minVersion ? minVersion.ToString() : versionStr;
        }

        private string GetFrameworkName()
        {
            return ProjectSettings.IsNetCore ? "netcoreapp" + ProjectSettings.AspNetCoreVersion : "net" + GetFrameworkVersion().Replace(".", "");
        }

        protected override void AdjustReplacements()
        {
            Replacements["$targetframeworkversion$"] = GetFrameworkVersion();
            Replacements["$targetframeworkname$"] = GetFrameworkName();
            base.AdjustReplacements();
        }

        protected override string ScriptsPath
        {
            get
            {
                return "wwwroot\\js";
            }
        }

        protected override void RemovePath(Project project, params string[] paths)
        {
            base.RemovePath(project, paths);

            if (ProjectSettings.Version >= new Version("14.0"))
            {
                //Fix issue: the non-existing file still shows with non-existing icon in VS Solution Explorer.
                var item = GetProjectItem(project, string.Join("\\", paths));
                if (item != null)
                {
                    try
                    {
                        item.Remove();
                    }
                    catch { }

                }
            }
        }
    }
}
