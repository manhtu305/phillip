﻿namespace ProjectTemplateWizard
{
    partial class WizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardForm));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbbTheme = new System.Windows.Forms.ComboBox();
            this.lblTheme = new System.Windows.Forms.Label();
            this.cbCopyET = new System.Windows.Forms.CheckBox();
            this.cbRefZip = new System.Windows.Forms.CheckBox();
            this.cbRefExcel = new System.Windows.Forms.CheckBox();
            this.cbRefPdf = new System.Windows.Forms.CheckBox();
            this.titlePic = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblRef = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnHelp = new System.Windows.Forms.Button();
            this.cbFinance = new System.Windows.Forms.CheckBox();
            this.cbFlexSheet = new System.Windows.Forms.CheckBox();
            this.cbIntelliSense = new System.Windows.Forms.CheckBox();
            this.cbAddTSFile = new System.Windows.Forms.CheckBox();
            this.txtTSFile = new System.Windows.Forms.TextBox();
            this.cbFlexViewer = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel_Settings = new System.Windows.Forms.Panel();
            this.cbCoreVersion = new System.Windows.Forms.ComboBox();
            this.panel_Theme = new System.Windows.Forms.Panel();
            this.panel_CopyTemplates = new System.Windows.Forms.Panel();
            this.cbDeferredScripts = new System.Windows.Forms.CheckBox();
            this.panel_IncludeLibs = new System.Windows.Forms.Panel();
            this.cbOlap = new System.Windows.Forms.CheckBox();
            this.cbMultiRow = new System.Windows.Forms.CheckBox();
            this.panel_Intellisense = new System.Windows.Forms.Panel();
            this.panel_RefDoclibs = new System.Windows.Forms.Panel();
            this.cboTransposedGrid = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.titlePic)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel_Settings.SuspendLayout();
            this.panel_Theme.SuspendLayout();
            this.panel_CopyTemplates.SuspendLayout();
            this.panel_IncludeLibs.SuspendLayout();
            this.panel_Intellisense.SuspendLayout();
            this.panel_RefDoclibs.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // cbbTheme
            // 
            this.cbbTheme.FormattingEnabled = true;
            resources.ApplyResources(this.cbbTheme, "cbbTheme");
            this.cbbTheme.Name = "cbbTheme";
            // 
            // lblTheme
            // 
            resources.ApplyResources(this.lblTheme, "lblTheme");
            this.lblTheme.Name = "lblTheme";
            // 
            // cbCopyET
            // 
            resources.ApplyResources(this.cbCopyET, "cbCopyET");
            this.cbCopyET.Checked = true;
            this.cbCopyET.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCopyET.Name = "cbCopyET";
            this.cbCopyET.UseVisualStyleBackColor = true;
            // 
            // cbRefZip
            // 
            resources.ApplyResources(this.cbRefZip, "cbRefZip");
            this.cbRefZip.Name = "cbRefZip";
            this.cbRefZip.UseVisualStyleBackColor = true;
            // 
            // cbRefExcel
            // 
            resources.ApplyResources(this.cbRefExcel, "cbRefExcel");
            this.cbRefExcel.Name = "cbRefExcel";
            this.cbRefExcel.UseVisualStyleBackColor = true;
            // 
            // cbRefPdf
            // 
            resources.ApplyResources(this.cbRefPdf, "cbRefPdf");
            this.cbRefPdf.Name = "cbRefPdf";
            this.cbRefPdf.UseVisualStyleBackColor = true;
            // 
            // titlePic
            // 
            resources.ApplyResources(this.titlePic, "titlePic");
            this.titlePic.Name = "titlePic";
            this.titlePic.TabStop = false;
            // 
            // lblTitle
            // 
            resources.ApplyResources(this.lblTitle, "lblTitle");
            this.lblTitle.Name = "lblTitle";
            // 
            // lblRef
            // 
            resources.ApplyResources(this.lblRef, "lblRef");
            this.lblRef.Name = "lblRef";
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // btnHelp
            // 
            resources.ApplyResources(this.btnHelp, "btnHelp");
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.MouseEnter += new System.EventHandler(this.btnHelp_MouseEnter);
            this.btnHelp.MouseLeave += new System.EventHandler(this.btnHelp_MouseLeave);
            // 
            // cbFinance
            // 
            resources.ApplyResources(this.cbFinance, "cbFinance");
            this.cbFinance.Name = "cbFinance";
            this.cbFinance.UseVisualStyleBackColor = true;
            // 
            // cbFlexSheet
            // 
            resources.ApplyResources(this.cbFlexSheet, "cbFlexSheet");
            this.cbFlexSheet.Name = "cbFlexSheet";
            this.cbFlexSheet.UseVisualStyleBackColor = true;
            // 
            // cbIntelliSense
            // 
            resources.ApplyResources(this.cbIntelliSense, "cbIntelliSense");
            this.cbIntelliSense.Name = "cbIntelliSense";
            this.cbIntelliSense.UseVisualStyleBackColor = true;
            this.cbIntelliSense.CheckedChanged += new System.EventHandler(this.cbIntelliSense_CheckedChanged);
            // 
            // cbAddTSFile
            // 
            resources.ApplyResources(this.cbAddTSFile, "cbAddTSFile");
            this.cbAddTSFile.Name = "cbAddTSFile";
            this.cbAddTSFile.UseVisualStyleBackColor = true;
            this.cbAddTSFile.CheckedChanged += new System.EventHandler(this.cbAddTSFile_CheckedChanged);
            // 
            // txtTSFile
            // 
            this.txtTSFile.BackColor = System.Drawing.SystemColors.Window;
            resources.ApplyResources(this.txtTSFile, "txtTSFile");
            this.txtTSFile.Name = "txtTSFile";
            this.txtTSFile.Validating += new System.ComponentModel.CancelEventHandler(this.txtTSFile_Validating);
            // 
            // cbFlexViewer
            // 
            resources.ApplyResources(this.cbFlexViewer, "cbFlexViewer");
            this.cbFlexViewer.Name = "cbFlexViewer";
            this.cbFlexViewer.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Controls.Add(this.titlePic);
            this.flowLayoutPanel1.Controls.Add(this.panel_Settings);
            this.flowLayoutPanel1.Controls.Add(this.cbCoreVersion);
            this.flowLayoutPanel1.Controls.Add(this.panel_Theme);
            this.flowLayoutPanel1.Controls.Add(this.panel_CopyTemplates);
            this.flowLayoutPanel1.Controls.Add(this.panel_IncludeLibs);
            this.flowLayoutPanel1.Controls.Add(this.panel_Intellisense);
            this.flowLayoutPanel1.Controls.Add(this.panel_RefDoclibs);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // panel_Settings
            // 
            this.panel_Settings.Controls.Add(this.lblTitle);
            resources.ApplyResources(this.panel_Settings, "panel_Settings");
            this.panel_Settings.Name = "panel_Settings";
            // 
            // cbCoreVersion
            // 
            this.cbCoreVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCoreVersion.FormattingEnabled = true;
            resources.ApplyResources(this.cbCoreVersion, "cbCoreVersion");
            this.cbCoreVersion.Name = "cbCoreVersion";
            // 
            // panel_Theme
            // 
            resources.ApplyResources(this.panel_Theme, "panel_Theme");
            this.panel_Theme.Controls.Add(this.lblTheme);
            this.panel_Theme.Controls.Add(this.cbbTheme);
            this.panel_Theme.Name = "panel_Theme";
            // 
            // panel_CopyTemplates
            // 
            resources.ApplyResources(this.panel_CopyTemplates, "panel_CopyTemplates");
            this.panel_CopyTemplates.Controls.Add(this.cbDeferredScripts);
            this.panel_CopyTemplates.Controls.Add(this.cbCopyET);
            this.panel_CopyTemplates.Name = "panel_CopyTemplates";
            // 
            // cbDeferredScripts
            // 
            resources.ApplyResources(this.cbDeferredScripts, "cbDeferredScripts");
            this.cbDeferredScripts.Name = "cbDeferredScripts";
            this.cbDeferredScripts.UseVisualStyleBackColor = true;
            // 
            // panel_IncludeLibs
            // 
            resources.ApplyResources(this.panel_IncludeLibs, "panel_IncludeLibs");
            this.panel_IncludeLibs.Controls.Add(this.cboTransposedGrid);
            this.panel_IncludeLibs.Controls.Add(this.cbOlap);
            this.panel_IncludeLibs.Controls.Add(this.cbFlexViewer);
            this.panel_IncludeLibs.Controls.Add(this.cbFlexSheet);
            this.panel_IncludeLibs.Controls.Add(this.cbFinance);
            this.panel_IncludeLibs.Controls.Add(this.cbMultiRow);
            this.panel_IncludeLibs.Name = "panel_IncludeLibs";
            // 
            // cbOlap
            // 
            resources.ApplyResources(this.cbOlap, "cbOlap");
            this.cbOlap.Name = "cbOlap";
            this.cbOlap.UseVisualStyleBackColor = true;
            // 
            // cbMultiRow
            // 
            resources.ApplyResources(this.cbMultiRow, "cbMultiRow");
            this.cbMultiRow.Name = "cbMultiRow";
            this.cbMultiRow.UseVisualStyleBackColor = true;
            // 
            // panel_Intellisense
            // 
            resources.ApplyResources(this.panel_Intellisense, "panel_Intellisense");
            this.panel_Intellisense.Controls.Add(this.txtTSFile);
            this.panel_Intellisense.Controls.Add(this.cbAddTSFile);
            this.panel_Intellisense.Controls.Add(this.cbIntelliSense);
            this.panel_Intellisense.Name = "panel_Intellisense";
            // 
            // panel_RefDoclibs
            // 
            resources.ApplyResources(this.panel_RefDoclibs, "panel_RefDoclibs");
            this.panel_RefDoclibs.Controls.Add(this.lblRef);
            this.panel_RefDoclibs.Controls.Add(this.cbRefZip);
            this.panel_RefDoclibs.Controls.Add(this.cbRefExcel);
            this.panel_RefDoclibs.Controls.Add(this.cbRefPdf);
            this.panel_RefDoclibs.Name = "panel_RefDoclibs";
            // 
            // cboTransposedGrid
            // 
            resources.ApplyResources(this.cboTransposedGrid, "cboTransposedGrid");
            this.cboTransposedGrid.Name = "cboTransposedGrid";
            this.cboTransposedGrid.UseVisualStyleBackColor = true;
            // 
            // WizardForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WizardForm";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.WizardForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.titlePic)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel_Settings.ResumeLayout(false);
            this.panel_Settings.PerformLayout();
            this.panel_Theme.ResumeLayout(false);
            this.panel_Theme.PerformLayout();
            this.panel_CopyTemplates.ResumeLayout(false);
            this.panel_CopyTemplates.PerformLayout();
            this.panel_IncludeLibs.ResumeLayout(false);
            this.panel_IncludeLibs.PerformLayout();
            this.panel_Intellisense.ResumeLayout(false);
            this.panel_Intellisense.PerformLayout();
            this.panel_RefDoclibs.ResumeLayout(false);
            this.panel_RefDoclibs.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cbbTheme;
        private System.Windows.Forms.Label lblTheme;
        private System.Windows.Forms.CheckBox cbCopyET;
        private System.Windows.Forms.CheckBox cbRefZip;
        private System.Windows.Forms.CheckBox cbRefExcel;
        private System.Windows.Forms.CheckBox cbRefPdf;
        private System.Windows.Forms.PictureBox titlePic;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblRef;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.CheckBox cbFinance;
        private System.Windows.Forms.CheckBox cbFlexSheet;
        private System.Windows.Forms.CheckBox cbIntelliSense;
        private System.Windows.Forms.CheckBox cbAddTSFile;
        private System.Windows.Forms.TextBox txtTSFile;
        private System.Windows.Forms.CheckBox cbFlexViewer;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel_Settings;
        private System.Windows.Forms.Panel panel_Theme;
        private System.Windows.Forms.Panel panel_CopyTemplates;
        private System.Windows.Forms.Panel panel_IncludeLibs;
        private System.Windows.Forms.Panel panel_Intellisense;
        private System.Windows.Forms.Panel panel_RefDoclibs;
        private System.Windows.Forms.CheckBox cbDeferredScripts;
        private System.Windows.Forms.CheckBox cbOlap;
        private System.Windows.Forms.CheckBox cbMultiRow;
        private System.Windows.Forms.ComboBox cbCoreVersion;
        private System.Windows.Forms.CheckBox cboTransposedGrid;
    }
}