﻿using C1Wizard;
using Microsoft.VisualStudio.TemplateWizard;
using ProjectTemplateWizard.Models;
using ProjectTemplateWizard.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ProjectTemplateWizard
{
    public static class Utils
    {
        public static void AddReplacements(IDictionary<string, string> replacements, object model)
        {
            if (replacements == null)
            {
                throw new ArgumentNullException("replacements");
            }

            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            var properties = model.GetType().GetProperties().Where(p => !p.GetCustomAttributes<ReplacementIgnoreAttribute>().Any());
            foreach (var prop in properties)
            {
                var value = prop.GetValue(model);
                if (value != null)
                {
                    replacements[string.Format("${0}$", prop.Name.ToLower())] = value.ToString();
                }
            }
        }

        public static bool? ShowDialog(TemplatesSettings settings)
        {
            var dlg = new WizardDialog();
            dlg.Title = Localization.Resources.WizardTitle;
            var content = new MvcSettings();
            content.DataContext = settings;
            dlg.DialogContent = content;
            return dlg.ShowDialog();
        }

        public static ProjectSettings GenerateSettings(object automationObject, Dictionary<string, string> replacementsDictionary)
        {
            var settings = ProjectSettings.Create(replacementsDictionary);
            settings.Version = new Version(((dynamic)automationObject).Version.ToString());

            return settings;
        }
    }
}
