﻿namespace ProjectTemplateWizard
{
    partial class HelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpForm));
            this.lbTheme = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lbEditorTemplates = new System.Windows.Forms.Label();
            this.lbDocument = new System.Windows.Forms.Label();
            this.lbFinance = new System.Windows.Forms.Label();
            this.lbFlexSheet = new System.Windows.Forms.Label();
            this.lbTs = new System.Windows.Forms.Label();
            this.lbFlexViewer = new System.Windows.Forms.Label();
            this.lbDeferredScripts = new System.Windows.Forms.Label();
            this.lbOlap = new System.Windows.Forms.Label();
            this.lbMultiRow = new System.Windows.Forms.Label();
            this.lblTransposedGrid = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbTheme
            // 
            resources.ApplyResources(this.lbTheme, "lbTheme");
            this.lbTheme.Name = "lbTheme";
            // 
            // lblTitle
            // 
            resources.ApplyResources(this.lblTitle, "lblTitle");
            this.lblTitle.Name = "lblTitle";
            // 
            // lbEditorTemplates
            // 
            resources.ApplyResources(this.lbEditorTemplates, "lbEditorTemplates");
            this.lbEditorTemplates.Name = "lbEditorTemplates";
            // 
            // lbDocument
            // 
            resources.ApplyResources(this.lbDocument, "lbDocument");
            this.lbDocument.Name = "lbDocument";
            // 
            // lbFinance
            // 
            resources.ApplyResources(this.lbFinance, "lbFinance");
            this.lbFinance.Name = "lbFinance";
            // 
            // lbFlexSheet
            // 
            resources.ApplyResources(this.lbFlexSheet, "lbFlexSheet");
            this.lbFlexSheet.Name = "lbFlexSheet";
            // 
            // lbTs
            // 
            resources.ApplyResources(this.lbTs, "lbTs");
            this.lbTs.Name = "lbTs";
            // 
            // lbFlexViewer
            // 
            resources.ApplyResources(this.lbFlexViewer, "lbFlexViewer");
            this.lbFlexViewer.Name = "lbFlexViewer";
            // 
            // lbDeferredScripts
            // 
            resources.ApplyResources(this.lbDeferredScripts, "lbDeferredScripts");
            this.lbDeferredScripts.Name = "lbDeferredScripts";
            // 
            // lbOlap
            // 
            resources.ApplyResources(this.lbOlap, "lbOlap");
            this.lbOlap.Name = "lbOlap";
            // 
            // lbMultiRow
            // 
            resources.ApplyResources(this.lbMultiRow, "lbMultiRow");
            this.lbMultiRow.Name = "lbMultiRow";
            // 
            // lblTransposedGrid
            // 
            resources.ApplyResources(this.lblTransposedGrid, "lblTransposedGrid");
            this.lblTransposedGrid.Name = "lblTransposedGrid";
            // 
            // HelpForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.lblTransposedGrid);
            this.Controls.Add(this.lbMultiRow);
            this.Controls.Add(this.lbOlap);
            this.Controls.Add(this.lbDeferredScripts);
            this.Controls.Add(this.lbFlexViewer);
            this.Controls.Add(this.lbFlexSheet);
            this.Controls.Add(this.lbFinance);
            this.Controls.Add(this.lbTs);
            this.Controls.Add(this.lbDocument);
            this.Controls.Add(this.lbEditorTemplates);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lbTheme);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HelpForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Click += new System.EventHandler(this.HelpForm_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTheme;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lbEditorTemplates;
        private System.Windows.Forms.Label lbDocument;
        private System.Windows.Forms.Label lbFinance;
        private System.Windows.Forms.Label lbFlexSheet;
        private System.Windows.Forms.Label lbTs;
        private System.Windows.Forms.Label lbFlexViewer;
        private System.Windows.Forms.Label lbDeferredScripts;
        private System.Windows.Forms.Label lbOlap;
        private System.Windows.Forms.Label lbMultiRow;
        private System.Windows.Forms.Label lblTransposedGrid;
    }
}