﻿using C1.Web.Mvc;
using ProjectTemplateWizard.Localization;
using ProjectTemplateWizard.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace ProjectTemplateWizard
{
    public partial class WizardForm : Form
    {
        private readonly string[] _themes = InitThemes();
        private HelpForm _help;
        private bool _showC1DocumentSetting = true;
        private const string AspNetCoreVersionPrefix = "ASP.NET Core ";
        private static readonly string[] AspNetCoreVersions = new[] { "1.0", "1.1" };

        public WizardForm()
        {
            InitializeComponent();
            foreach (var version in AspNetCoreVersions)
            {
                cbCoreVersion.Items.Add(AspNetCoreVersionPrefix + version);
            }
        }

        private static string[] InitThemes()
        {
            var fields = typeof(Themes).GetFields(BindingFlags.Static | BindingFlags.Public);
            var themes = new List<string>();
            foreach (var field in fields)
            {
                themes.Add((string)field.GetValue(null));
            }

            return themes.ToArray();
        }

        public bool ShowDialogWithSettings(ProjectSettings model)
        {
            if (model.Version <= new Version("12.0"))
                HideIntellisenseSettings();

            cbCoreVersion.Visible = model.IsAspNetCore && model.Version >= new Version("15.0");
            cbCoreVersion.SelectedIndex = Array.IndexOf(AspNetCoreVersions, model.AspNetCoreVersion);

            ShowC1DocumentSetting(!model.IsAspNetCore);
            if (ShowDialog() != DialogResult.OK)
                return false;

            UpdateModel(model);
            return true;
        }

        private void UpdateModel(ProjectSettings model)
        {
            model.CustomTheme = cbbTheme.Text;
            model.EnableDeferredScripts = cbDeferredScripts.Checked;
            model.CopyET = cbCopyET.Checked;
            model.RefPdf = cbRefPdf.Checked;
            model.RefExcel = cbRefExcel.Checked;
            model.RefZip = cbRefZip.Checked;
            model.RefFinance = cbFinance.Checked;
            model.RefFlexSheet = cbFlexSheet.Checked;
            model.RefFlexViewer = cbFlexViewer.Checked;
            model.RefOlap = cbOlap.Checked;
            model.RefMultiRow = cbMultiRow.Checked;
            model.AddTsFileTemp = IsAddTSFile();
            model.TsFileName = GetTSFileName();
            model.ClientIntelliSense = cbIntelliSense.Checked;
            model.AspNetCoreVersion = AspNetCoreVersions[cbCoreVersion.SelectedIndex];
        }

        private void ShowC1DocumentSetting(bool visible)
        {
            _showC1DocumentSetting = visible;
            lblRef.Visible = visible;
            cbRefExcel.Visible = visible;
            cbRefPdf.Visible = visible;
            cbRefZip.Visible = visible;
            if (_help != null)
            {
                _help.ShowC1DocumentSetting(visible);
            }
        }

        private void WizardForm_Load(object sender, EventArgs e)
        {
            cbbTheme.Items.Clear();
            Array.ForEach(_themes, theme => cbbTheme.Items.Add(theme));
            cbbTheme.SelectedIndex = Array.IndexOf(_themes, "default");
        }

        private bool IsAddTSFile()
        {
            if (cbIntelliSense.Checked && cbAddTSFile.Enabled && cbAddTSFile.Checked)
            {
                return !string.IsNullOrEmpty(txtTSFile.Text);
            }
            return false;
        }

        public string GetTSFileName()
        {
            return txtTSFile.Text;
        }

        private void btnHelp_MouseEnter(object sender, EventArgs e)
        {
            _help = new HelpForm();
            _help.Location = this.Location + new Size(btnHelp.Left, btnHelp.Top - _help.Height + 30);
            _help.ShowC1DocumentSetting(_showC1DocumentSetting);
            _help.Show();
            Activate();
        }

        private void btnHelp_MouseLeave(object sender, EventArgs e)
        {
            _help.Close();
        }

        private void cbAddTSFile_CheckedChanged(object sender, EventArgs e)
        {
            txtTSFile.Enabled = cbAddTSFile.Checked;
        }

        private void cbIntelliSense_CheckedChanged(object sender, EventArgs e)
        {
            cbAddTSFile.Enabled = cbIntelliSense.Checked;
            txtTSFile.Enabled = cbIntelliSense.Checked && cbAddTSFile.Checked;
        }

        private void txtTSFile_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var valid = txtTSFile.Text.ToLower().EndsWith(".ts") && txtTSFile.Text.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) == -1;

            if (!valid)
            {
                MessageBox.Show(Resources.InvalidTsFileName);
                txtTSFile.Text = "app.ts";
                e.Cancel = true; 
            }
        }

        private void HideIntellisenseSettings()
        {
            panel_Intellisense.Visible = false;
        }
    }
}
