﻿using ProjectTemplateWizard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectTemplateWizard.UI
{
    /// <summary>
    /// Interaction logic for MvcSettings.xaml
    /// </summary>
    public partial class MvcSettings : UserControl
    {
        public MvcSettings()
        {
            InitializeComponent();
        }

        private void CoreVersions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateUseRazorPages();
            UpdateCopyET();
        }

        private void UseRazorPages_CheckedChanged(object sender, RoutedEventArgs e)
        {
            UpdateCopyET();
        }

        private void UpdateUseRazorPages()
        {
            var expression = UseRazorPages.GetBindingExpression(CheckBox.VisibilityProperty);
            expression.UpdateTarget();
        }

        private void UpdateCopyET()
        {
            var expression = CopyET.GetBindingExpression(CheckBox.IsEnabledProperty);
            expression.UpdateTarget();
        }
    }
}
