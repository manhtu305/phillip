
@echo off
pushd "%~dp0"

if exist c1.mvc.basic.lib.d.ts del /f /q c1.mvc.basic.lib.d.ts
if exist c1.mvc.finance.lib.d.ts del /f /q c1.mvc.finance.lib.d.ts
if exist c1.mvc.flexsheet.lib.d.ts del /f /q c1.mvc.flexsheet.lib.d.ts
if exist c1.mvc.flexviewer.lib.d.ts del /f /q c1.mvc.flexviewer.lib.d.ts
if exist c1.mvc.olap.lib.d.ts del /f /q c1.mvc.olap.lib.d.ts
if exist c1.mvc.multirow.lib.d.ts del /f /q c1.mvc.multirow.lib.d.ts
if exist c1.mvc.transposedgrid.lib.d.ts del /f /q c1.mvc.transposedgrid.lib.d.ts

call merge c1.mvc.basic.list.txt c1.mvc.basic.lib.d.ts
call merge c1.mvc.finance.list.txt c1.mvc.finance.lib.d.ts
call merge c1.mvc.flexsheet.list.txt c1.mvc.flexsheet.lib.d.ts
call merge c1.mvc.flexviewer.list.txt c1.mvc.flexviewer.lib.d.ts
call merge c1.mvc.olap.list.txt c1.mvc.olap.lib.d.ts
call merge c1.mvc.multirow.list.txt c1.mvc.multirow.lib.d.ts
call merge c1.mvc.transposedgrid.list.txt c1.mvc.transposedgrid.lib.d.ts

call :copyallts ..\EN
call :copyallts ..\JPN
call :copyaspnetcorets

popd
goto :EOF

:copyallts
  call :copyts %1\Wijmo5Mvc3ApplicationCS\Scripts\typings
  call :copyts %1\Wijmo5Mvc3ApplicationVB\Scripts\typings
  call :copyts %1\Wijmo5Mvc4ApplicationCS\Scripts\typings
  call :copyts %1\Wijmo5Mvc4ApplicationVB\Scripts\typings
  call :copyts %1\Wijmo5Mvc5ApplicationCS\Scripts\typings
  call :copyts %1\Wijmo5Mvc5ApplicationVB\Scripts\typings
  call :copyts %1\Wijmo5Mvc5ModelBindingCS\Scripts\typings
  call :copyts %1\Wijmo5Mvc5ModelBindingVB\Scripts\typings
  call :copyts %1\Wijmo5Mvc5AjaxBindingCS\Scripts\typings
  call :copyts %1\Wijmo5Mvc5AjaxBindingVB\Scripts\typings
  call :copyts %1\Wijmo5Mvc5SpreadSheetCS\Scripts\typings
  call :copyts %1\Wijmo5Mvc5SpreadSheetVB\Scripts\typings
goto :EOF

:copyaspnetcorets
  call :copyts ..\AspNetCoreApp2015\wwwroot\js\typings
  call :copyts ..\AspNetCoreModelBinding2015\wwwroot\js\typings
  call :copyts ..\AspNetCoreAjaxBinding2015\wwwroot\js\typings
  call :copyts ..\AspNetCoreSpreadsheet2015\wwwroot\js\typings
  call :copyts ..\NetCoreApp2015\wwwroot\js\typings
  call :copyts ..\NetCoreModelBinding2015\wwwroot\js\typings
  call :copyts ..\NetCoreAjaxBinding2015\wwwroot\js\typings
  call :copyts ..\NetCoreSpreadsheet2015\wwwroot\js\typings
  call :copyts ..\AspNetCore10AppCs\wwwroot\js\typings
  call :copyts ..\AspNetCore20AppCs\wwwroot\js\typings
  call :copyts ..\AspNetCore20RPAppCs\wwwroot\js\typings
  call :copyts ..\AspNetCore30AppCs\wwwroot\js\typings
  call :copyts ..\AspNetCore30RPAppCs\wwwroot\js\typings
  call :copyts ..\AspNetCore10ModelBindingCs\wwwroot\js\typings
  call :copyts ..\AspNetCore20ModelBindingCs\wwwroot\js\typings
  call :copyts ..\AspNetCore30ModelBindingCs\wwwroot\js\typings
  call :copyts ..\AspNetCore10AjaxBindingCs\wwwroot\js\typings
  call :copyts ..\AspNetCore20AjaxBindingCs\wwwroot\js\typings
  call :copyts ..\AspNetCore30AjaxBindingCs\wwwroot\js\typings
  call :copyts ..\AspNetCore10SpreadsheetCs\wwwroot\js\typings
  call :copyts ..\AspNetCore20SpreadsheetCs\wwwroot\js\typings
  call :copyts ..\AspNetCore30SpreadsheetCs\wwwroot\js\typings
goto :EOF

:copyts
  if not exist %1 md %1
  if exist %1\c1.mvc.basic.lib.d.ts del /f /q %1\c1.mvc.basic.lib.d.ts
  if exist %1\c1.mvc.finance.lib.d.ts del /f /q %1\c1.mvc.finance.lib.d.ts
  if exist %1\c1.mvc.flexsheet.lib.d.ts del /f /q %1\c1.mvc.flexsheet.lib.d.ts
  if exist %1\c1.mvc.flexviewer.lib.d.ts del /f /q %1\c1.mvc.flexviewer.lib.d.ts
  if exist %1\c1.mvc.olap.lib.d.ts del /f /q %1\c1.mvc.olap.lib.d.ts
  if exist %1\c1.mvc.multirow.lib.d.ts del /f /q %1\c1.mvc.multirow.lib.d.ts
  if exist %1\c1.mvc.transposedgrid.lib.d.ts del /f /q %1\c1.mvc.transposedgrid.lib.d.ts
  
  copy c1.mvc.basic.lib.d.ts %1
  copy c1.mvc.finance.lib.d.ts %1
  copy c1.mvc.flexsheet.lib.d.ts %1
  copy c1.mvc.flexviewer.lib.d.ts %1
  copy c1.mvc.olap.lib.d.ts %1
  copy c1.mvc.multirow.lib.d.ts %1
  copy c1.mvc.transposedgrid.lib.d.ts %1
goto :EOF
