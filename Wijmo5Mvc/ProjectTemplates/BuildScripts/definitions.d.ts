declare module wijmo.chart.finance {
  /**This is declare module for typing purpose only */
  function castObj();
}
declare module wijmo.grid.multirow {
    /**This is declare module for typing purpose only */
    function castObj();
}

declare module wijmo.grid.sheet {
    /**This is declare module for typing purpose only */
    function castObj();
}
declare module wijmo.olap {
    /**This is declare module for typing purpose only */
    function castObj();
}

declare module wijmo.grid.transposed {
    /**This is declare module for typing purpose only */
    function castObj();
}