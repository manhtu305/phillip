
@echo off
setlocal enabledelayedexpansion
pushd "%~dp0"

set outputfile=%2
if exist %outputfile% del /f /q %outputfile%

@echo build %outputfile%...
@echo.> crlf.txt

set filelist=
for /f %%i in (%1) do (
  if not "%i" == "" (
    set filelist=!filelist!/b + crlf.txt/b + %%i
  )
)

copy %filelist:~5% %outputfile%

@del /f /q crlf.txt
@echo build %outputfile% completed.

popd
