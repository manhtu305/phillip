﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace $safeprojectname$
{
    public class Person
    {
        public class NamedCountry
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }

        public static List<NamedCountry> Countries = new List<NamedCountry> {
            new NamedCountry { Id="1", Name="China" },
            new NamedCountry { Id="2", Name="India" },
            new NamedCountry { Id="3", Name="United States" },
            new NamedCountry { Id="4", Name="Indonesia" },
            new NamedCountry { Id="5", Name="Brazil" },
            new NamedCountry { Id="6", Name="Pakistan" },
            new NamedCountry { Id="7", Name="Bangladesh" },
            new NamedCountry { Id="8", Name="Nigeria" },
            new NamedCountry { Id="9", Name="Russia" },
            new NamedCountry { Id="10", Name="Japan" }
        };
        #region ctor

        public Person()
        {
        }

        #endregion

        #region Object Model

        public int Id { get; set; }
        public string Name { get { return string.Format("{0} {1}", First, Last); } }
        public string CountryID { get; set; }
        public bool Active { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public DateTime Hired { get; set; }

        #endregion

        public void CopyFrom(Person source)
        {
            CountryID = source.CountryID;
            Active = source.Active;
            First = source.First;
            Last = source.Last;
            Hired = source.Hired;
        }
    }

    public class SampleData
    {
        private static Random _rnd = new Random();
        private static string[] _firstNames = "Andy|Ben|Charlie|Dan|Ed|Fred|Gil|Herb|Jack|Karl|Larry|Mark|Noah|Oprah|Paul|Quince|Rich|Steve|Ted|Ulrich|Vic|Xavier|Zeb".Split('|');
        private static string[] _lastNames = "Ambers|Bishop|Cole|Danson|Evers|Frommer|Griswold|Heath|Jammers|Krause|Lehman|Myers|Neiman|Orsted|Paulson|Quaid|Richards|Stevens|Trask|Ulam".Split('|');

        private static string GetString(string[] arr)
        {
            return arr[_rnd.Next(arr.Length)];
        }

        public static IEnumerable<Person> GetData()
        {
            var list = Enumerable.Range(0, 50).Select(i =>
            {
                return new Person()
                {
                    Id = i,
                    First = GetString(_firstNames),
                    Last = GetString(_lastNames),
                    CountryID = Person.Countries[_rnd.Next(0, Person.Countries.Count)].Id,
                    Active = _rnd.NextDouble() >= .5,
                    Hired = DateTime.Today.AddDays(-_rnd.Next(1, 365))
                };
            });
            return list;
        }
    }
}
