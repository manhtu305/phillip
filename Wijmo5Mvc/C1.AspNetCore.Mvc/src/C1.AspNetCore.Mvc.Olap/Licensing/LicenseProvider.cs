﻿using C1.Util.Licensing;

namespace C1.Web.Mvc.Olap
{
    internal class LicenseProviderAttribute : BaseLicenseProviderAttribute
    {
        public override string RunTimeKey
        {
            get
            {
                return LicenseManager.Key;
            }
        }
    }
}
