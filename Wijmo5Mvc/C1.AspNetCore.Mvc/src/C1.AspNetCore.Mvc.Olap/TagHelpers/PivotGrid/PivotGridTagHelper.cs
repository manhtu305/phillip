﻿using C1.Web.Mvc.Grid;
using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    [RestrictChildren("c1-flex-grid-cell-template")]
    partial class PivotGridTagHelper
    {
        /// <summary>
        /// Gets or sets the id of the source.
        /// </summary>
        /// <remarks>
        /// It can be the id of a <see cref="PivotPanel"/> component or a <see cref="PivotEngine"/> component.
        /// </remarks>
        public override string ItemsSourceId
        {
            get
            {
                return base.ItemsSourceId;
            }
            set
            {
                base.ItemsSourceId = value;
            }
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AllowAddNew
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AllowDelete
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override AllowDragging AllowDragging
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AutoClipboard
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AutoGenerateColumns
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string BeginningEdit
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string CellEditEnded
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string CellEditEnding
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ChildItemsPath
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ColumnFootersRowHeaderText
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ColumnLayout
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Copied
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Copying
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool DeferResizing
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DeletedRow
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DeletingRow
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DraggedColumn
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DraggedRow
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DraggingColumn
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DraggingColumnOver
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DraggingRow
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DraggingRowOver
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string GroupCollapsedChanged
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string GroupCollapsedChanging
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string GroupHeaderFormat
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ImeEnabled
        {
            get;
            set;
        }


        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool NewRowAtTop
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Pasted
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string PastedCell
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Pasting
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string PastingCell
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string PrepareCellForEdit
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool PreserveOutlineState
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool PreserveSelectedState
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string RowAdded
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string RowEditEnded
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string RowEditEnding
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string RowEditStarted
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string RowEditStarting
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowErrors
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowGroups
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowColumnFooters
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int TreeIndent
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ValidateEdits
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowAlternatingRows
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this attribute.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string GroupBy { get; set; }

        /// <summary>
        /// Overrides to hide this attribute.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OrderBy { get; set; }

    }
}
