﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    /// <summary>
    /// <see cref="ITagHelper"/> implementation for FlexPie in PivotChart.
    /// </summary>
    [HtmlTargetElement("c1-pivot-flex-pie")]
    [RestrictChildren("c1-flex-pie-datalabel", "c1-flex-chart-title-style", "c1-flex-chart-tooltip", "c1-chart-animation")]
    public class PivotFlexPieTagHelper : FlexPieTagHelper
    {
        #region Hidden Attributes
        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string BindingName
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new string Binding
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ItemsSourceId
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new string Id
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new bool IsTemplate
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new object TemplateBindings
        {
            get;
            set;
        }
        #endregion Hidden Attributes

        /// <summary>
        /// Render the startup scripts.
        /// </summary>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        protected override void Render(TagHelperOutput output)
        {
            //do nothing
        }

        /// <summary>
        /// Process the attributes set in the taghelper.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="parent">The information from the parent taghelper.</param>
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            var pivotChart = parent as PivotChart;
            if (pivotChart != null)
            {
                pivotChart._SetFlexPie(TObject);
            }
        }

        /// <summary>
        /// Gets the <see cref="PivotFlexPie"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="PivotFlexPie"/></returns>
        protected override FlexPie<object> GetObjectInstance(object parent = null)
        {
            return new PivotFlexPie(HtmlHelper);
        }
    }
}
