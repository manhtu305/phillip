﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    [RestrictChildren("c1-pivot-flex-chart", "c1-pivot-flex-pie", "c1-flex-chart-title-style")]
    partial class PivotChartTagHelper
    {
    }
}
