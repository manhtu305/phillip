﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source",
        "c1-pivot-engine", "c1-pivot-field-collection", "c1-view-field-collection")]
    partial class PivotPanelTagHelper
    {
    }
}
