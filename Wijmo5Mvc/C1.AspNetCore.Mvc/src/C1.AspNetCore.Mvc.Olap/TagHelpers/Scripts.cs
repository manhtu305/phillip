﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    /// <summary>
    /// Extends <see cref="Mvc.TagHelpers.ScriptsTagHelper"/> for olap scripts.
    /// </summary>
    [RestrictChildren("c1-olap-scripts")]
    [HtmlTargetElement("c1-scripts")]
    public class ScriptsTagHelper : Mvc.TagHelpers.ScriptsTagHelper
    {
    }
}
