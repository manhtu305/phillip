﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Reflection;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    public partial class PivotFieldBaseTagHelper<TControl>
    {
        /// <summary>
        /// Override to hide this attribute.
        /// </summary>
        public override string C1Property
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Process the child content.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            if (parent != null)
            {
                var piCollection = parent.GetType().GetProperty(CollectionName);
                if (piCollection == null)
                {
                    // Process the SubFields property of the field.
                    SetCollectionName("SubFields");
                }
            }
            base.ProcessChildContent(parent, childContent);
        }

        internal virtual void SetCollectionName(string name)
        {
            _collectionName = name;
        }
    }

    [RestrictChildren("c1-pivot-filter", "c1-pivot-field")]
    public partial class PivotFieldTagHelper
    {
        internal override void SetCollectionName(string name)
        {
            _collectionName = name;
        }
    }

    [RestrictChildren("c1-pivot-filter", "c1-cube-field")]
    public partial class CubeFieldTagHelper
    {
        internal override void SetCollectionName(string name)
        {
            _collectionName = name;
        }
    }
}
