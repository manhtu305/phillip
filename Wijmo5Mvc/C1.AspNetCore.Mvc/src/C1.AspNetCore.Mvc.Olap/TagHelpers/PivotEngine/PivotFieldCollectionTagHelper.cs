﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    [RestrictChildren("c1-pivot-field", "c1-cube-field")]
    public partial class PivotFieldCollectionTagHelper
    {
        /// <summary>
        /// Gets the fixed property name.
        /// </summary>
        public override string C1Property
        {
            get
            {
                return "Fields";
            }
        }
    }
}
