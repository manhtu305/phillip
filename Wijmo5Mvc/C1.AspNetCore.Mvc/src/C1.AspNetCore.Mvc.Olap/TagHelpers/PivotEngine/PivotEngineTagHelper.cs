﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source", "c1-cube-service",
        "c1-pivot-field-collection", "c1-view-field-collection")]
    public partial class PivotEngineTagHelper
    {
        /// <summary>
        /// Gets the customized default proeprty name.
        /// Overrides this property to customize the default property name for this tag.
        /// </summary>
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "Engine";
            }
        }
    }
}
