﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    /// <summary>
    /// <see cref="ITagHelper"/> implementation for RowFields, ColumnFields, ValueFields and FilterFields.
    /// </summary>
    /// <remarks>
    /// Represents a collection of the fields.
    /// </remarks>
    [HtmlTargetElement("c1-view-field-collection")]
    public class ViewFieldCollectionTagHelper : SettingTagHelper<PivotFieldCollection>
    {
        /// <summary>
        /// Configurates <see cref="PivotFieldCollection.MaxItems" />.
        /// Sets the maximum number of fields allowed in this collection.
        /// </summary>
        /// <remarks>
        /// This property is set to null by default, which means any number of items is allowed.
        /// </remarks>
        public int? MaxItems
        {
            get { return InnerHelper.GetPropertyValue<int?>("MaxItems"); }
            set { InnerHelper.SetPropertyValue("MaxItems", value); }
        }

        /// <summary>
        /// Gets or sets the field collection.
        /// </summary>
        /// <remarks>The text seperated with comma shows the list of the field keys.</remarks>
        public string Items
        {
            get
            {
                var items = InnerHelper.GetPropertyValue<List<PivotFieldBase>>("Items");
                return items != null ? string.Join(",", items.Select(pf => pf.Key)) : null;
            }
            set
            {
                var vfc = new ViewFieldCollection();
                vfc.SetItems(value.Split(',').Distinct().ToArray());
                InnerHelper.SetPropertyValue("Items", vfc.Items);
            }
        }
    }
}
