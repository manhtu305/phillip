﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Olap.TagHelpers
{
    [RestrictChildren("c1-flex-grid-value-filter")]
    public partial class PivotFilterTagHelper
    {
        /// <summary>
        /// Overrides to fix the property name to be "Filter" and remove this attribute in the tag.
        /// </summary>
        public override string C1Property
        {
            get
            {
                return "Filter";
            }
        }
    }
}
