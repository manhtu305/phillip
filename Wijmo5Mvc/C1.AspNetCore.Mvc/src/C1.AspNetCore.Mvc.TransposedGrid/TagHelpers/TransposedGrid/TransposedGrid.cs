﻿using C1.Web.Mvc.Grid;
using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel;

namespace C1.Web.Mvc.TransposedGrid.TagHelpers
{
    [RestrictChildren(
        "c1-flex-grid-column", "c1-flex-grid-cell-template",
        "c1-items-source", "c1-odata-source", "c1-odata-virtual-source",
        "c1-flex-grid-errortip", "c1-transposed-grid-row",
        //extenders
        "c1-flex-grid-group-panel", "c1-flex-grid-filter", "c1-flex-grid-detail"
    )]
    public partial class TransposedGridTagHelper 
    {
        #region hidden TransposedGrid method

        /// <summary>
        /// This method is useless in TransposedGrid.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AllowAddNew { get; set; }

        /// <summary>
        /// This method is useless in TransposedGrid.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AllowDelete { get; set; }
        
        /// <summary>
        /// This method is useless in TransposedGrid, using AutoGenerateRows instead.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AutoGenerateColumns { get; set; }
        
        #endregion
    }
}