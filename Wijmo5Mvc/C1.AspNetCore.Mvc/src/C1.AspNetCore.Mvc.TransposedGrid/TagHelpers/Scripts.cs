﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using C1.Web.Mvc.TagHelpers;

namespace C1.Web.Mvc.TransposedGrid.TagHelpers
{
    /// <summary>
    /// Extends <see cref="Mvc.TagHelpers.ScriptsTagHelper"/> for TransposedGrid scripts.
    /// </summary>
    [RestrictChildren("c1-transposed-grid-scripts")]
    [HtmlTargetElement("c1-scripts")]
    public class ScriptsTagHelper : Mvc.TagHelpers.ScriptsTagHelper
    {
    }
}
