﻿using C1.Web.Mvc;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Extends <see cref="IApplicationBuilder"/> for C1 MVC components.
    /// </summary>
    public static class ApplicationBuilderExtension
    {
        /// <summary>
        /// Gets the C1 MVC configuration of <see cref="Configuration"/> type.
        /// </summary>
        /// <param name="app">The application builder.</param>
        /// <returns>The C1 MVC configuration of <see cref="Configuration"/> type.</returns>
        public static Configuration C1(this IApplicationBuilder app)
        {
            return Configuration.Instance;
        }
    }
}
