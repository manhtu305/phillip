﻿using Microsoft.Extensions.Configuration;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines configuration for C1 MVC components.
    /// </summary>
    public sealed class Configuration
    {
        internal readonly static Configuration Instance = new Configuration();

        private Configuration()
        {
        }

        /// <summary>
        /// Enable deferred scripts feature.
        /// </summary>
        /// <returns><see cref="Configuration"/>.</returns>
        public Configuration EnableDeferredScripts()
        {
            DeferredScriptsManager.Global = true;
            return Instance;
        }

        /// <summary>
        /// Sets the global theme.
        /// </summary>
        /// <param name="value">The theme name.</param>
        /// <returns><see cref="Configuration"/>.</returns>
        public Configuration Theme(string value)
        {
            ThemeManager.Global = value;
            return Instance;
        }

        /// <summary>
        /// Sets the configuration of C1 MVC components according to <see cref="IConfiguration"/>.
        /// </summary>
        /// <param name="value">The application configuration of <see cref="IConfiguration"/> type.</param>
        /// <returns>The C1 MVC <see cref="Configuration"/>.</returns>
        public Configuration Configure(IConfiguration config)
        {
            var theme = config[ThemeManager.GlobalKey];
            if (theme != null)
            {
                ThemeManager.Global = config[ThemeManager.GlobalKey];
            }

            var enableDeferredScripts = config[DeferredScriptsManager.GlobalKey];
            if (enableDeferredScripts != null)
            {
                bool enableDeferredScriptsValue;
                if (bool.TryParse(enableDeferredScripts, out enableDeferredScriptsValue))
                {
                    DeferredScriptsManager.Global = enableDeferredScriptsValue;
                }
            }

            return Instance;
        }
    }
}
