﻿using Microsoft.AspNetCore.Mvc;
#if !NETCORE3
using Microsoft.AspNetCore.Mvc.Internal;
#endif
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace C1.Web.Mvc
{
    internal static class UnobtrusiveValidationHelper
    {
        internal static IDictionary<string, object> GetUnobtrusiveValidationAttributes(this IHtmlHelper helper, string name, ModelExplorer modelExplorer)
        {
            return helper.GetUnobtrusiveValidationAttributes(name, modelExplorer.Metadata);
        }

        internal static IEnumerable<ModelMetadata> GetMetadataForProperties(this IHtmlHelper helper, Type modelType)
        {
            var viewContext = helper.ViewContext;
            var modelMetadataProvider = viewContext.HttpContext?.RequestServices.GetService<IModelMetadataProvider>();
            if(modelMetadataProvider == null)
            {
                return null;
            }

            return modelMetadataProvider.GetMetadataForProperties(modelType);
        }

        // The following code is from Microsoft.AspNetCore.Mvc.ViewFeatures.DefaultHtmlGenerator, Microsoft.AspNetCore.Mvc.ViewFeatures, Version=1.0.0.0 
        internal static IDictionary<string, object> GetUnobtrusiveValidationAttributes(this IHtmlHelper helper, string name, ModelMetadata metadata)
        {
            var viewContext = helper.ViewContext;
            var optionsAccessor = viewContext.HttpContext?.RequestServices.GetService<IOptions<MvcViewOptions>>();
            var modelMetadataProvider = viewContext.HttpContext?.RequestServices.GetService<IModelMetadataProvider>();
            var clientValidatorCache = viewContext.HttpContext?.RequestServices.GetService<ClientValidatorCache>();

            if (optionsAccessor == null || modelMetadataProvider == null || clientValidatorCache == null )
            {
                return null;
            }

            var validationAttributes = new Dictionary<string, string>();
            var clientModelValidatorProvider = new CompositeClientModelValidatorProvider(optionsAccessor.Value.ClientModelValidatorProviders);
            var formContext = viewContext.ClientValidationEnabled ? viewContext.FormContext : null;

            if (formContext != null)
            {
                if (!formContext.RenderedField(name))
                {
                    formContext.RenderedField(name, true);
                    IReadOnlyList<IClientModelValidator> validators = clientValidatorCache.GetValidators(metadata, clientModelValidatorProvider);
                    if (validators.Count > 0)
                    {
                        var validationContext = new ClientModelValidationContext(viewContext, metadata, modelMetadataProvider, validationAttributes);
                        for (int i = 0; i < validators.Count; i++)
                        {
                            validators[i].AddValidation(validationContext);
                        }
                    }
                }
            }
            //TFS 413243
            var validationAttributesDic = validationAttributes.ToDictionary(i => i.Key, i => (object)i.Value);
            if (metadata.ModelType == typeof(DateTime)) {
                //get error message
                string errorMessage = string.Format("The {0} field must be a date.", name);
                if (metadata.ValidatorMetadata.Count > 0) { 
                    foreach(var item in metadata.ValidatorMetadata)
                    {
                        if (item.GetType() == typeof(DataTypeAttribute))
                        {
                            DataTypeAttribute attr = ((DataTypeAttribute)item);
                            if((attr.DataType == DataType.Date || attr.DataType == DataType.DateTime) && !string.IsNullOrEmpty(attr.ErrorMessage))
                            {
                                errorMessage = attr.ErrorMessage;
                                break;
                            }
                            
                        }
                    }
                }
                //add 'data-val-date' attribute to validationAtributes dictionary
                validationAttributesDic.Add("data-val-date", errorMessage);
            }
            return validationAttributesDic;
        }
    }
}
