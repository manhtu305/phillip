﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel;

namespace C1.Web.Mvc.TagHelpers.Input
{
    [HtmlTargetElement("c1-menu")]
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source", "c1-input-item-template", "c1-menu-item")]
    public class MenuTagHelper : MenuBaseTagHelper<object, Menu>
    {
        #region Hidden Attributes
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new string Placeholder
        {
            get;
        }
        
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new bool Required
        {
            get;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public new bool IsRequired
        {
            get;
        }


        [EditorBrowsable(EditorBrowsableState.Never)]
        public new string Text
        {
            get;
        }
        #endregion Hidden Attributes

        #region Methods
        protected override void ProcessChildContentAsync(TagHelperContext context, TagHelperOutput output, object parent)
        {
            base.ProcessChildContentAsync(context, output, parent);
            if (TObject.MenuItems != null)
            {
                var cv = TObject.GetDataSource<CollectionViewService<object>>();
                if(cv != null)
                {
                    cv.SourceCollection = TObject.MenuItems;
                }
            }
        }
        #endregion Methods
    }
}
