﻿using System;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public abstract partial class InputDateBaseTagHelper<TControl>
    {
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if (For == null)
            {
                return;
            }
            DateTime? value = null;
            if (For.Model is DateTime)
            {
                value = (DateTime)For.Model;
            }
            if (For.Metadata.ModelType == typeof(DateTime?))
            {
                IsRequired = false;
            }
            TObject.Value = value;
        }
    }
}
