﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source", "c1-input-item-template")]
    partial class MultiAutoCompleteTagHelper
    {
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if(For == null)
            {
                return;
            }

            var values = For.Model as IEnumerable;
            List<object> objectValues = null;
            if (values != null)
            {
                objectValues = new List<object>();
                foreach (var v in values)
                {
                    objectValues.Add(v);
                }
            }

            TObject.Text = null;
            TObject.SelectedValues = objectValues;
            TObject.Name = ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(For.Name);
            TObject.ValidationAttributes = HtmlHelper.GetUnobtrusiveValidationAttributes(TObject.Name, For.ModelExplorer);
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int SelectedIndex
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override object SelectedItem
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string SelectedIndexChanged
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override object SelectedValue
        {
            get;
            set;
        }
    }
}
