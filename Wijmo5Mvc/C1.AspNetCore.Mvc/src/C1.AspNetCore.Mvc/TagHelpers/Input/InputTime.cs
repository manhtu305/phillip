﻿using System;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class InputTimeTagHelper
    {
        /// <summary>
        /// Gets or sets the Name property.
        /// </summary>
        public string Name
        {
            get { return TObject.Name; }
            set { TObject.Name = value; }
        }

        /// <summary>
        /// An expression to be evaluated against the current model.
        /// </summary>
        public ModelExpression For
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Step property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the number of minutes between entries in the drop-down list.
        /// </remarks>
        public double Step
        {
            get { return TObject.Step; }
            set { TObject.Step = (int)value; }
        }

        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if (For == null)
            {
                return;
            }
            DateTime? value = null;
            if (For.Model is DateTime)
            {
                value = (DateTime)For.Model;
            }
            TObject.Value = value;
            TObject.Name = ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(For.Name);
            TObject.ValidationAttributes = HtmlHelper.GetUnobtrusiveValidationAttributes(TObject.Name, For.ModelExplorer);
        }
    }
}
