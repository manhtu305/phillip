﻿using System.Collections.Generic;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class ColorPickerTagHelper
    {
        /// <summary>
        /// Gets or Sets the currently selected color.
        /// </summary>
        public string Value
        {
            get { return TObject.Value; }
            set { TObject.Value = value; }
        }

        /// <summary>
        /// Gets or Sets the Palette property.
        /// </summary>
        public List<string> Palette
        {
            get { return TObject.Palette; }
            set { TObject.Palette = value; }
        }
    }
}
