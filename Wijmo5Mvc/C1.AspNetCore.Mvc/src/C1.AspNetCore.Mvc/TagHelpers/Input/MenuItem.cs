﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class MenuItemTagHelper
    {
        /// <summary>
        /// Configurates <see cref="MenuCommand.ExecuteCommand" /> in <see cref="MenuItem"/>.
        /// Sets the method executes the command.
        /// </summary>
        public string ExecuteCommand
        {
            get { return InnerHelper.GetPropertyValue<string>("ExecuteCommand"); }
            set { InnerHelper.SetPropertyValue("ExecuteCommand", value); }
        }

        /// <summary>
        /// Configurates <see cref="MenuCommand.CanExecuteCommand" /> in <see cref="MenuItem"/>.
        /// Sets the method returns a Boolean value that determines whether the controller can execute the command.
        /// </summary>
        public string CanExecuteCommand
        {
            get { return InnerHelper.GetPropertyValue<string>("CanExecuteCommand"); }
            set { InnerHelper.SetPropertyValue("CanExecuteCommand", value); }
        }

        /// <summary>
        /// Updates the property in TObject.
        /// It is used to update the child property manually instead of the default one.
        /// </summary>
        /// <param name="key">The property name.</param>
        /// <param name="value">The value of the property.</param>
        /// <returns>
        /// A boolean value indicates whether to use the default update.
        /// True to skip the default update.
        /// Otherwise, use the default update.
        /// </returns>
        protected override bool UpdateProperty(string key, object value)
        {
            switch (key)
            {
                case "ExecuteCommand":
                    if (TObject.Command == null)
                    {
                        TObject.Command = new MenuCommand();
                    }
                    TObject.Command.ExecuteCommand = (string)value;
                    return true;
                case "CanExecuteCommand":
                    if (TObject.Command == null)
                    {
                        TObject.Command = new MenuCommand();
                    }
                    TObject.Command.CanExecuteCommand = (string)value;
                    return true;
                default:
                    return base.UpdateProperty(key, value);
            }
        }

        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            var menu = parent as Menu;
            if (menu == null)
            {
                return;
            }
            if (menu.MenuItems == null)
            {
                menu.MenuItems = new List<MenuItem>();
            }
            menu.MenuItems.Add(TObject);
        }
    }
}
