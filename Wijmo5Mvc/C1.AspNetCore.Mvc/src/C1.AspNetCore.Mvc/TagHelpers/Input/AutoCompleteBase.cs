﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    partial class AutoCompleteBaseTagHelper<T, TControl>
    {
        /// <summary>
        /// An expression to be evaluated against the current model.
        /// </summary>
        public virtual ModelExpression For
        {
            get;
            set;
        }

        protected override void Render(TagHelperOutput output)
        {
            TObject.HtmlAttributes["type"] = "text";
            base.Render(output);
        }
    }
}
