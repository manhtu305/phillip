﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source", "c1-input-item-template")]
    public partial class AutoCompleteTagHelper
    {
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if (For == null)
            {
                return;
            }
            TObject.Values = new object[1] { For.Model };
            TObject.Name = ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(For.Name);
            TObject.ValidationAttributes = HtmlHelper.GetUnobtrusiveValidationAttributes(TObject.Name, For.ModelExplorer);
        }
    }
}
