﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class InputColorTagHelper
    {
        /// <summary>
        /// Gets or Sets the current color.
        /// </summary>
        public string Value
        {
            get { return TObject.Value; }
            set { TObject.Value = value; }
        }

        /// <summary>
        /// Configurates <see cref="InputColor.Palette"/>.
        /// Sets an array that contains the colors in the palette.
        /// </summary>
        /// <remarks>
        /// The palette contains ten colors, represented by an array with ten strings. 
        /// The first two colors are usually white and black.
        /// </remarks>
        public List<string> Palette
        {
            get { return TObject.Palette; }
            set { TObject.Palette = value; }
        }

        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if (For == null)
            {
                return;
            }
            TObject.Value = For.Model == null ? null : For.Model.ToString();
        }
    }
}
