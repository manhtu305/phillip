﻿using C1.JsonNet;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections;
using System.Collections.Generic;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source", "c1-input-item-template")]
    public partial class MultiSelectTagHelper
    {
        /// <summary>
        ///  Gets or sets the name of the MultiSelect control.
        /// </summary>
        [Ignore]
        public string Name
        {
            get { return TObject.HtmlAttributes["wj-name"]; }
            set { TObject.HtmlAttributes["wj-name"] = value; }
        }

        /// <summary>
        /// An expression to be evaluated against the current model.
        /// </summary>
        public ModelExpression For
        {
            get;
            set;
        }

        /// <summary>
        /// Render the startup scripts.
        /// </summary>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        protected override void Render(TagHelperOutput output)
        {
            TObject.HtmlAttributes["type"] = "text";
            base.Render(output);
        }

        /// <summary>
        /// Process the attributes set in the taghelper.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="parent">The information from the parent taghelper.</param>
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if (For == null)
            {
                return;
            }

            var values = For.Model as IEnumerable;
            List<object> objectValues = null;
            if (values != null)
            {
                objectValues = new List<object>();
                foreach (var v in values)
                {
                    objectValues.Add(v);
                }
            }

            TObject.CheckedValues = objectValues;
            TObject.Name = ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(For.Name);
            TObject.ValidationAttributes = HtmlHelper.GetUnobtrusiveValidationAttributes(TObject.Name, For.ModelExplorer);
        }
    }
}
