﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class InputMaskTagHelper
    {
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if (For == null)
            {
                return;
            }
            TObject.RawValue = For.Model == null ? null : For.Model.ToString();
        }
    }
}
