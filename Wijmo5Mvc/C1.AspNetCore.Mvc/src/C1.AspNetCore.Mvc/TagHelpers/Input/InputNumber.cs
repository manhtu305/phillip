﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class InputNumberTagHelper
    {
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if(For == null)
            {
                return;
            }

            var text = For.Model == null ? string.Empty : For.Model.ToString();
            double value;
            if (!double.TryParse(text, out value))
            {
                TObject.Value = null;
            }
            else
            {
                TObject.Value = value;
            }
        }
    }
}
