﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class PopupTagHelper
    {
        internal override void FillContent(TagHelperOutput output, string content)
        {
            output.PostContent.SetHtmlContent(content);
        }
    }
}
