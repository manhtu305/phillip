﻿using System;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Text.RegularExpressions;
using System.Reflection;

namespace C1.Web.Mvc.TagHelpers
{
    [HtmlTargetElement("c1-input-item-template")]
    public partial class ItemTemplateTagHelper: BaseTagHelper<object>
    {
        #region Properties
        /// <summary>
        /// Gets or sets the template's id.
        /// </summary>
        public string Id { get; set; }

        protected override object TObject
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            context.Items[C1_TEMPLATE] = true;            
        }

        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            var typeParent = parent.GetType();
            var propId = typeParent.GetProperty("ItemTemplateId");
            var propContent = typeParent.GetProperty("ItemTemplateContent");
            if (propId == null || propContent == null)
            {
                throw new ArgumentException("The parent type is invalid.", "parent");
            }
            var content = childContent.GetContent();
            var isTemplateContent = !string.IsNullOrEmpty(Regex.Replace(content, @"\s", ""));
            if (isTemplateContent)
            {
                propContent.SetValue(parent, content);
            }
            else if (!string.IsNullOrEmpty(Id))
            {
                propId.SetValue(parent, Id);
            }
        }
        #endregion Methods
    }
}
