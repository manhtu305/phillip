﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class MenuBaseTagHelper<T, TControl>
    {
        /// <summary>
        /// This method executes the command.
        /// </summary>
        public string ExecuteCommand
        {
            get { return TObject.Command.ExecuteCommand; }
            set { TObject.Command.ExecuteCommand = value; }
        }

        /// <summary>
        /// This method returns a Boolean value that determines whether the controller can execute the command.
        /// </summary>
        /// <remarks>
        /// If this method returns false, the menu option is disabled.
        /// </remarks>
        public string CanExecuteCommand
        {
            get { return TObject.Command.CanExecuteCommand; }
            set { TObject.Command.CanExecuteCommand = value; }
        }

        [HtmlAttributeNotBound]
        public override string HeaderPath
        {
            get { return base.HeaderPath; }
            set { base.HeaderPath = value; }
        }

        [HtmlAttributeNotBound]
        public override bool AutoExpandSelection
        {
            get { return base.AutoExpandSelection; }
            set { base.AutoExpandSelection = value; }
        }
    }
}
