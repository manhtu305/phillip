﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source", "c1-input-item-template")]
    public partial class ComboBoxTagHelper
    {
        /// <summary>
        ///  Gets or sets the name of the ComboBox control.
        /// </summary>
        public string Name
        {
            get { return TObject.Name; }
            set { TObject.Name = value; }
        }

        /// <summary>
        /// An expression to be evaluated against the current model.
        /// </summary>
        public ModelExpression For
        {
            get;
            set;
        }

        protected override void Render(TagHelperOutput output)
        {
            TObject.HtmlAttributes["type"] = "text";
            base.Render(output);
        }

        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            if (For == null)
            {
                return;
            }
            TObject.Values = new object[1] { For.Model };
            TObject.Name = ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(For.Name);
            TObject.ValidationAttributes = HtmlHelper.GetUnobtrusiveValidationAttributes(TObject.Name, For.ModelExplorer);
        }
    }
}
