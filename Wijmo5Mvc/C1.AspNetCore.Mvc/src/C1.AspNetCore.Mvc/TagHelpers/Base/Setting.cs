﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines the basic taghelper class of the inner taghelper for the property which type is complex.
    /// </summary>
    /// <typeparam name="T">The item type.</typeparam>
    public abstract class SettingTagHelper<T> : BaseTagHelper<T>
        where T : class
    {
        #region Fields 
        private InnerTagHelper<T> _innerHelper;
        private string _propertyName;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets or sets the property name which the taghelper is related to.
        /// </summary>
        public virtual string C1Property
        {
            get
            {
                return _propertyName ?? InnerHelper.DefaultPropertyName;
            }
            set
            {
                _propertyName = value;
            }
        }

        /// <summary>
        /// Gets the customized default proeprty name.
        /// Overrides this property to customize the default property name for this tag.
        /// </summary>
        protected virtual string CustomDefaultPropertyName
        {
            get { return null; }
        }

        /// <summary>
        /// Gests the helper for inner tag.
        /// </summary>
        internal InnerTagHelper<T> InnerHelper
        {
            get { return _innerHelper ?? (_innerHelper = InnerTagHelper<T>.CreateInstance(UpdateProperty, this, CustomDefaultPropertyName)); }
        }

        /// <summary>
        /// Overrides to redefine getter and setter.
        /// </summary>
        protected override T TObject { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Updates the property in TObject.
        /// It is used to update the child property manually instead of the default one.
        /// </summary>
        /// <param name="key">The property name.</param>
        /// <param name="value">The value of the property.</param>
        /// <returns>
        /// A boolean value indicates whether to use the default update.
        /// True to skip the default update.
        /// Otherwise, use the default update.
        /// </returns>
        protected virtual bool UpdateProperty(string key, object value)
        {
            return false;
        }

        /// <summary>
        /// Processes all attributes of current taghelper.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="parent">The parrent control instance.</param>
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            TObject = InnerHelper.GetInstance(parent, GetObjectInstance, C1Property, _propertyName != null);
            InnerHelper.Update(TObject);
        }

        /// <summary>
        /// Processes the child content.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            InnerHelper.ProcessAsCollectionItem(parent, TObject, CollectionName);
        }
        #endregion Methods
    }
}
