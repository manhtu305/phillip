﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines the basic class for C1 objects' taghelper.
    /// </summary>
    /// <typeparam name="T">The item type.</typeparam>
    public abstract class BaseTagHelper<T> : TagHelper
        where T : class
    {
        #region Fields
        private T _tObject;
        private ViewContext _viewContext;
        internal const string C1_PARENT = "C1_Parent";
        internal const string C1_TEMPLATE = "C1_Template";
        #endregion Fields

        #region Properties
        // Protected to ensure subclasses are correctly activated. Internal for ease of use when testing.
        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext
        {
            get
            {
                return _viewContext;
            }
            set
            {
                _viewContext = value;
                HtmlEncoder = value?.HttpContext?.RequestServices.GetService<HtmlEncoder>();
            }
        }

        /// <summary>
        /// Gets the <see cref="T:System.Text.Encodings.Web.HtmlEncoder"/> to be used for encoding HTML.
        /// </summary>
        internal HtmlEncoder HtmlEncoder
        {
            get;
            private set;
        }

        internal IHtmlHelper HtmlHelper
        {
            get
            {
                return new InternalHtmlHelper(ViewContext);
            }
        }

        /// <summary>
        /// Gets or sets the corresponding object.
        /// </summary>
        protected virtual T TObject
        {
            get { return _tObject ?? (_tObject = GetObjectInstance()); }
            set { _tObject = value; }
        }

        /// <summary>
        /// Gets the collection name.
        /// </summary>
        protected virtual string CollectionName
        {
            get;
        }
        #endregion Properties

        #region Methods

        /// <summary>
        /// Synchronously executes the Microsoft.AspNetCore.Razor.TagHelpers.TagHelper with
        /// the given context and output.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var parent = GetParent(context);
            if (IsTemplate(context))
            {
                ProcessAsTemplate();
            }
            ProcessAttributes(context, parent);
            ProcessChildContentAsync(context, output, parent);
        }

        /// <summary>
        /// Gets the <see cref="T"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="T"/></returns>
        protected virtual T GetObjectInstance(object parent = null)
        {
            var ctor = GetObjectConstructor(typeof(T));
            return (ctor.Invoke(null)) as T;
        }

        private static ConstructorInfo GetObjectConstructor(Type t)
        {
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

            return t.GetConstructors(bindingFlags).SingleOrDefault(c => !c.GetParameters().Any());
        }

        private object GetParent(TagHelperContext context)
        {
            if (context.Items.ContainsKey(C1_PARENT))
            {
                return context.Items[C1_PARENT];
            }
            return null;
        }

        private bool IsTemplate(TagHelperContext context)
        {
            if (context.Items.ContainsKey(C1_TEMPLATE))
            {
                var isTemplate = context.Items[C1_TEMPLATE];
                if (isTemplate == null)
                {
                    return false;
                }
                return (bool)isTemplate;
            }
            return false;
        }

        /// <summary>
        /// When the taghelper works as template, add the pre-processing here.
        /// </summary>
        protected virtual void ProcessAsTemplate()
        {
            var template = this as ITemplateTagHelper;
            if(template != null)
            {
                template.IsTemplate = true;
            }
        }

        /// <summary>
        /// Processes the attributes set in the taghelper.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="parent">The information from the parent taghelper.</param>
        protected virtual void ProcessAttributes(TagHelperContext context, object parent)
        {
        }

        /// <summary>
        /// Processes the the children taghelpers.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        /// <param name="parent">The information from the parent taghelper.</param>
        protected virtual async void ProcessChildContentAsync(TagHelperContext context, TagHelperOutput output, object parent)
        {
            context.Items[C1_PARENT] = TObject;
            var content = await output.GetChildContentAsync();
            ProcessChildContent(content, output, parent);
        }

        /// <summary>
        /// Processes the child content.
        /// </summary>
        /// <param name="childContent">The data which stands for child content.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        /// <param name="parent">The information from the parent taghelper.</param>
        internal virtual void ProcessChildContent(TagHelperContent childContent, TagHelperOutput output, object parent)
        {
            ProcessChildContent(parent, childContent);
        }

        /// <summary>
        /// Processes the child content.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected virtual void ProcessChildContent(object parent, TagHelperContent childContent)
        {
        }
        #endregion Methods

        #region InternalHtmlHelper
        /// <summary>
        /// Defines a type which implements <see cref="IHtmlHelper"/> for an internal use.
        /// </summary>
        private class InternalHtmlHelper : IHtmlHelper
        {
            private readonly ViewContext _context;
            public InternalHtmlHelper(ViewContext context)
            {
                _context = context;
            }

            public ViewContext ViewContext
            {
                get
                {
                    return _context;
                }
            }

            public Html5DateRenderingMode Html5DateRenderingMode
            {
                get
                {
                    throw new NotImplementedException();
                }

                set
                {
                    throw new NotImplementedException();
                }
            }

            public string IdAttributeDotReplacement
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public IModelMetadataProvider MetadataProvider
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public ITempDataDictionary TempData
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public UrlEncoder UrlEncoder
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public dynamic ViewBag
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public ViewDataDictionary ViewData
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public IHtmlContent ActionLink(string linkText, string actionName, string controllerName, string protocol, string hostname, string fragment, object routeValues, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent AntiForgeryToken()
            {
                throw new NotImplementedException();
            }

            public MvcForm BeginForm(string actionName, string controllerName, object routeValues, FormMethod method, bool? antiforgery, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public MvcForm BeginRouteForm(string routeName, object routeValues, FormMethod method, bool? antiforgery, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent CheckBox(string expression, bool? isChecked, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent Display(string expression, string templateName, string htmlFieldName, object additionalViewData)
            {
                throw new NotImplementedException();
            }

            public string DisplayName(string expression)
            {
                throw new NotImplementedException();
            }

            public string DisplayText(string expression)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent DropDownList(string expression, IEnumerable<SelectListItem> selectList, string optionLabel, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent Editor(string expression, string templateName, string htmlFieldName, object additionalViewData)
            {
                throw new NotImplementedException();
            }

            public string Encode(string value)
            {
                throw new NotImplementedException();
            }

            public string Encode(object value)
            {
                throw new NotImplementedException();
            }

            public void EndForm()
            {
                throw new NotImplementedException();
            }

            public string FormatValue(object value, string format)
            {
                throw new NotImplementedException();
            }

            public string GenerateIdFromName(string fullName)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<SelectListItem> GetEnumSelectList(Type enumType)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<SelectListItem> GetEnumSelectList<TEnum>() where TEnum : struct
            {
                throw new NotImplementedException();
            }

            public IHtmlContent Hidden(string expression, object value, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public string Id(string expression)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent Label(string expression, string labelText, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent ListBox(string expression, IEnumerable<SelectListItem> selectList, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public string Name(string expression)
            {
                throw new NotImplementedException();
            }

            public Task<IHtmlContent> PartialAsync(string partialViewName, object model, ViewDataDictionary viewData)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent Password(string expression, object value, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent RadioButton(string expression, object value, bool? isChecked, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent Raw(object value)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent Raw(string value)
            {
                throw new NotImplementedException();
            }

            public Task RenderPartialAsync(string partialViewName, object model, ViewDataDictionary viewData)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent RouteLink(string linkText, string routeName, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent TextArea(string expression, string value, int rows, int columns, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent TextBox(string current, object value, string format, object htmlAttributes)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent ValidationMessage(string expression, string message, object htmlAttributes, string tag)
            {
                throw new NotImplementedException();
            }

            public IHtmlContent ValidationSummary(bool excludePropertyErrors, string message, object htmlAttributes, string tag)
            {
                throw new NotImplementedException();
            }

            public string Value(string expression, string format)
            {
                throw new NotImplementedException();
            }
        }
        #endregion InternalHtmlHelper
    }
}