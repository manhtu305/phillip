﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Reflection;

namespace C1.Web.Mvc.TagHelpers
{
    abstract partial class ControlTagHelper<TControl> : ITemplateTagHelper
    {
        #region Properties
        /// <summary>
        /// Configurates <see cref="Component.Id" />.
        /// Sets the control id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return TObject.Id;
            }
            set
            {
                TObject.Id = value;
            }
        }

        [HtmlAttributeName("style")]
        /// <summary>
        /// Sets the style of the control.
        /// </summary>
        public string CssStyle
        {
            get
            {
                return TObject.HtmlAttributes["style"];
            }
            set
            {
                TObject.HtmlAttributes["style"] = value;
            }
        }

        /// <summary>
        /// Configurates <see cref="Control.TemplateBindings" />.
        /// Sets the collection of the template bindings.
        /// </summary>
        public object TemplateBindings
        {
            get
            {
                return TObject.TemplateBindings;
            }
            set
            {
                TObject.TemplateBindings.Clear();
                if(value != null)
                {
                    var pdCollection = value.GetType().GetProperties();
                    foreach(var pd in pdCollection)
                    {
                        var itemValue = (string)(pd.GetValue(value));
                        TObject.TemplateBindings.Add(pd.Name, itemValue);
                    }
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Render the startup scripts.
        /// </summary>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        protected override void Render(TagHelperOutput output)
        {
            output.TagMode = TagMode.StartTagAndEndTag;
            UpdateMarkup(output);
            UpdateCssStyle();
            base.Render(output);
        }

        /// <summary>
        /// Update the markup's attributes according to the attributes set in the taghelper.
        /// </summary>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        protected virtual void UpdateMarkup(TagHelperOutput output)
        {
            output.TagName = TObject.TagName;
            TObject.UpdateAttributes();
            TObject.Selector = "#" + Id;
            foreach (var htmlAttribute in TObject.HtmlAttributes)
            {
                output.Attributes.SetAttribute(htmlAttribute.Key, htmlAttribute.Value);
            }
        }

        /// <summary>
        /// Merge the settings for Width and Height into the style setting.
        /// </summary>
        private void UpdateCssStyle()
        {
            var appendedStyle = string.Empty; ;
            if (!string.IsNullOrEmpty(Width))
            {
                appendedStyle += string.Format("width: {0}", Width);
            }
            if(!string.IsNullOrEmpty(Height))
            {
                appendedStyle += string.Format("height: {0}", Height);
            }
            if(string.IsNullOrEmpty(appendedStyle))
            {
                return;
            }
            if(string.IsNullOrEmpty(CssStyle))
            {
                CssStyle = appendedStyle;
            }
            else
            {
                CssStyle += "; " + appendedStyle;
            }
        }
        #endregion Methods
    }
}