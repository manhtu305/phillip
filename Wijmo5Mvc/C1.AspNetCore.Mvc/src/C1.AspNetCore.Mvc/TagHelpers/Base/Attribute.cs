﻿using System.Reflection;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Linq;

namespace C1.Web.Mvc.TagHelpers
{
    // Don't use this attribute anymore because a breaking change from MS
    // https://github.com/aspnet/AspNetCore/issues/5041
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    [Obsolete("Use RestrictChildrenAttribute instead of")]
    public class C1RestrictChildrenAttribute : Attribute
    {
        #region Ctors
        [Obsolete("Use RestrictChildrenAttribute(string, string) instead of")]
        public C1RestrictChildrenAttribute(string tagName, params string[] tagNames)
            //: base (tagName, tagNames)
        {
        }

        [Obsolete("Use RestrictChildrenAttribute(string, string) instead of")]
        public C1RestrictChildrenAttribute(Type tagHelperType, params Type[] tagHelperTypes)
            //: this(GetTagHelperName(tagHelperType), tagHelperTypes.Select(tht => GetTagHelperName(tht)).ToArray())
        {
        }


        #endregion Ctors
        //private static string GetTagHelperName(Type tagHelperType)
        //{
        //    if (tagHelperType != null)
        //    {
        //        var targetElement = tagHelperType.GetTypeInfo().GetCustomAttribute(typeof(HtmlTargetElementAttribute)) as HtmlTargetElementAttribute;
        //        if (targetElement != null)
        //        {
        //            return targetElement.Tag;
        //        }
        //    }
        //    return string.Empty;
        //}
    }
}
