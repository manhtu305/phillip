﻿namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines the interface of a template tag helper.
    /// </summary>
    public interface ITemplateTagHelper
    {
        /// <summary>
        /// Gets or sets the collection of the template bindings.
        /// </summary>
        object TemplateBindings { get; set; }

        /// <summary>
        /// Gets or sets a boolean value which indicates whether transfer this control to template mode.
        /// </summary>
        bool IsTemplate { get; set; }
    }
}
