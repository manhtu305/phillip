﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;

namespace C1.Web.Mvc.TagHelpers
{
    abstract partial class ExtenderTagHelper<TControl>
    {
        private InnerTagHelper<TControl> _innerHelper;

        #region Properties
        /// <summary>
        /// Configurates <see cref="Component.Id" />.
        /// Gets or sets the component id.
        /// </summary>
        public string Id
        {
            get { return InnerHelper.GetPropertyValue<string>("Id"); }
            set { InnerHelper.SetPropertyValue("Id", value); }
        }

        /// <summary>
        /// It is useless. To keep consistent with the previous version.
        /// </summary>
        public virtual string C1Property { get; set; }

        /// <summary>
        /// Gests the helper for inner tag.
        /// </summary>
        internal InnerTagHelper<TControl> InnerHelper
        {
            get { return _innerHelper ?? (_innerHelper = InnerTagHelper<TControl>.CreateInstance(UpdateProperty, this)); }
        }

        /// <summary>
        /// Overrides to re-define getter and setter.
        /// </summary>
        protected override TControl TObject { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Updates the property in TObject.
        /// It is used to update the child property manually instead of the default one.
        /// </summary>
        /// <param name="key">The property name.</param>
        /// <param name="value">The value of the property.</param>
        /// <returns>
        /// A boolean value indicates whether to use the default update.
        /// True to skip the default update.
        /// Otherwise, use the default update.
        /// </returns>
        protected virtual bool UpdateProperty(string key, object value)
        {
            return false;
        }

        /// <summary>
        /// Overrides to get the <see cref="Extender"/> object.
        /// </summary>
        protected override TControl GetObjectInstance(object parent)
        {
            var parsCount = 0;
            var firstParamType = (parent == null ? null : parent.GetType());
            var ctor = ReflectUtils.GetConstructor(typeof(TControl), firstParamType, out parsCount);
            var objs = new List<object>();
            objs.Add(parent);
            for (var i = 0; i < parsCount - 1; i++)
            {
                objs.Add(null);
            }
            return (ctor.Invoke(objs.ToArray())) as TControl;
        }

        /// <summary>
        /// Process all attributes of current taghelper.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="parent">The parrent control instance.</param>
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            TObject = InnerHelper.GetInstance(parent, GetObjectInstance, null);
            InnerHelper.Update(TObject);
        }

        /// <summary>
        /// Process the child content.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            InnerHelper.ProcessAsCollectionItem(parent, TObject, CollectionName);
        }
        #endregion Methods
    }
}