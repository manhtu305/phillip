﻿using C1.JsonNet;
using C1.Web.Mvc.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines this helper class for service(inner tag and independent tag) and extender(inner tag).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class InnerTagHelper<T>
        where T: class
    {
        #region Fields
        private OrderedDictionary _cache;
        private bool _isIndependentTag;
        private bool _isChildProperty;
        private readonly string _typeName;
        private readonly string _customPropertyName;

        public event UpdatingCancelEventHandler Updating;
        #endregion Fields

        #region Properties
        public OrderedDictionary Cache
        {
            get
            {
                return _cache ?? (_cache = new OrderedDictionary());
            }
        }

        public string DefaultPropertyName
        {
            get { return _customPropertyName ?? _typeName; }
        }
        #endregion Properties

        public InnerTagHelper(BaseTagHelper<T> owner, string customPropertyName)
        {
            _typeName = GetTypeNameWithoutGeneric(owner);
            _customPropertyName = customPropertyName;
        }

        #region Methods
        public static InnerTagHelper<T> CreateInstance(Func<string, object, bool> updateProperty, BaseTagHelper<T> owner, string customPropertyName = null)
        {
            var innerHelper = new InnerTagHelper<T>(owner, customPropertyName);
            innerHelper.Updating += (object sender, UpdatingCancelEventArgs e) =>
            {
                e.Cancel = updateProperty(e.Key, e.Value);
            };
            return innerHelper;
        }

        /// <summary>
        /// Sets the property according to the specified property name and value.
        /// </summary>
        /// <param name="name">The name of the property.</param>
        /// <param name="value">The value to be set.</param>
        public void SetPropertyValue(string name, object value)
        {
            Cache[name] = value;
        }

        /// <summary>
        /// Gest the property value.
        /// </summary>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="name">The property name.</param>
        /// <param name="defaultValue">The default value. It is optional.</param>
        /// <returns>An instance of <see cref="TProperty"/></returns>
        public TProperty GetPropertyValue<TProperty>(string name, TProperty defaultValue = default(TProperty))
        {
            return Cache.Contains(name) ? (TProperty)Cache[name] : defaultValue;
        }

        /// <summary>
        /// GEts the instance of <see cref="T"/>.
        /// </summary>
        /// <param name="parent">The parent object of current tag.</param>
        /// <param name="createInstance">A function to create an instance of <see cref="T"/>.</param>
        /// <param name="propertyName">The name of the property which current tag stands for.</param>
        /// <param name="isProperty">Specifies wether the current tag is a property of its parent.</param>
        /// <returns></returns>
        public T GetInstance(object parent, Func<object, T> createInstance, string propertyName, bool isProperty = false)
        {
            T tObject;
            PropertyInfo propertyInfo = null;
            if(parent == null)
            {
                _isIndependentTag = true;
            }
            else if (string.IsNullOrEmpty(propertyName))
            {
                _isChildProperty = false;
            }
            else
            {
                try
                {
                    propertyInfo = parent.GetType().GetProperty(propertyName);
                }
                catch { }

                if (propertyInfo == null)
                {
                    if (isProperty)
                    {
                        throw new Exception(Resources.InvalidC1Property);
                    }
                    _isChildProperty = false;
                }
                else
                {
                    _isChildProperty = true;
                }
            }

            if (!_isIndependentTag && _isChildProperty)
            {
                tObject = propertyInfo.GetValue(parent) as T;
            }
            else
            {
                tObject = createInstance(parent);
            }

            return tObject;
        }

        /// <summary>
        /// Update the model.
        /// </summary>
        /// <param name="obj">The model instance.</param>
        public void Update(T obj)
        {
            foreach (var item in Cache)
            {
                var key = item.Key;
                var value = item.Value;
                if (!OnUpdating(key, value))
                {
                    var subPropertyInfo = obj.GetType().GetProperty(key);
                    if (subPropertyInfo != null)
                    {
                        // for getter/setter in control
                        // if no setter, the internal setter would need be added in the class definition.
                        subPropertyInfo.SetValue(obj, value);
                    }
                }
            }
        }

        private bool OnUpdating(string key, object value)
        {
            if (Updating != null)
            {
                var args = new UpdatingCancelEventArgs
                {
                    Key = key,
                    Value = value
                };
                Updating(this, args);
                return args.Cancel;
            }

            return false;
        }

        public void ProcessAsCollectionItem(object parent, T item, string collectionName)
        {
            if(_isIndependentTag || _isChildProperty)
            {
                return;
            }

            PropertyInfo piCollection = null;
            try
            {
                piCollection = parent.GetType().GetProperty(collectionName);
            }
            catch { }

            if (piCollection != null)
            {
                var collection = piCollection.GetValue(parent);
                var isList = JsonUtility.HasImplementIList(collection.GetType(), collection);
                if (isList)
                {
                    var iCollection = collection as IList;
                    iCollection.Add(item);
                    return;
                }
            }
            throw new Exception(Resources.NotSetC1Property);
        }

        private static string GetTypeNameWithoutGeneric(object value)
        {
            if(value == null)
            {
                return null;
            }

            var typeName = value.GetType().Name;
            // remove generic type
            var index = typeName.IndexOf("<");
            if(index >= 0)
            {
                typeName = typeName.Substring(0, index);
            }

            // remove TagHelper suffix
            index = typeName.LastIndexOf("TagHelper");
            if (index >= 0)
            {
                typeName = typeName.Substring(0, index);
            }

            return typeName;
        }
        #endregion Methods
    }

    #region Assistant classes
    /// <summary>
    /// Defines an ordered dictionary.
    /// </summary>
    internal sealed class OrderedDictionary: List<KeyValuePair<string, object>>
    {
        /// <summary>
        /// Tells whether the specified key is contained in the dictionary.
        /// </summary>
        /// <param name="key">The specified key</param>
        /// <returns>True if the specified key is contained. Otherwise, false.</returns>
        public bool Contains(string key)
        {
            foreach (var item in this)
            {
                if (string.Compare(item.Key, key, false) == 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets or sets the value according to the specified key.
        /// </summary>
        /// <param name="key">the specified key.</param>
        public object this[string key]
        {
            get
            {
                foreach(var item in this)
                {
                    if(string.Compare(item.Key, key, false) == 0)
                    {
                        return item.Value;
                    }
                }
                throw new ArgumentOutOfRangeException("key");
            }
            set
            {
                var item = new KeyValuePair<string, object>(key, value);
                for (var i=0; i<Count; i++)
                {
                    if (string.Compare(this[i].Key, key, false) == 0)
                    {
                        this[i] = item;
                        return;
                    }
                }
                Add(item);
            }
        }
    }

    internal delegate void UpdatingCancelEventHandler(object sender, UpdatingCancelEventArgs e);
    internal sealed class UpdatingCancelEventArgs : CancelEventArgs
    {
        public string Key { get; set; }
        public object Value { get; set; }
    }
    #endregion Assistant classes
}
