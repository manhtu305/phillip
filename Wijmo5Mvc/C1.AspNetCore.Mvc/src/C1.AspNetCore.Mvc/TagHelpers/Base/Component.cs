﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.IO;

namespace C1.Web.Mvc.TagHelpers
{
    abstract partial class ComponentTagHelper<TControl>
    {
        #region Methods
        /// <summary>
        /// Gets the <see cref="TControl"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="TControl"/></returns>
        protected override TControl GetObjectInstance(object parent = null)
        {
            return ReflectUtils.GetInstanceWithHtmlHelper(typeof(TControl), HtmlHelper) as TControl;
        }

        /// <summary>
        /// Synchronously executes the Microsoft.AspNetCore.Razor.TagHelpers.TagHelper with
        /// the given context and output.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);
            Render(output);
        }

        /// <summary>
        /// Renders the startup scripts.
        /// </summary>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        protected virtual void Render(TagHelperOutput output)
        {
            using (var sw = new StringWriter())
            {
                TObject.WriteTo(sw, HtmlEncoder);
                FillContent(output, sw.ToString());
            }
        }

        internal virtual void FillContent(TagHelperOutput output, string content)
        {
            // Removes the child taghelpers.
            output.Content.SetHtmlContent(string.Empty);
            // Appends the startup scripts following the control's host element
            // in order to advoid some potential errors 
            // when the client scripts look for the child elements in the host element.
            output.PostElement.SetHtmlContent(content);
        }
        #endregion Methods
    }
}