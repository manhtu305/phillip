﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.IO;

namespace C1.Web.Mvc.TagHelpers
{
    abstract partial class ServiceTagHelper<TControl> : ITemplateTagHelper
    {
        #region Fields
        private InnerTagHelper<TControl> _innerHelper;
        private bool _isIndependentTag;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Configurates <see cref="Component.Id"/>.
        /// Sets the id of the service.
        /// </summary>
        public virtual string Id
        {
            get { return InnerHelper.GetPropertyValue<string>("Id"); }
            set { InnerHelper.SetPropertyValue("Id", value); }
        }

        /// <summary>
        /// Gets or sets the property name which the taghelper is related to.
        /// </summary>
        private string C1Property
        {
            get
            {
                return InnerHelper.DefaultPropertyName;
            }
        }

        /// <summary>
        /// Gets the customized default proeprty name.
        /// Overrides this property to customize the default property name for this tag.
        /// </summary>
        protected virtual string CustomDefaultPropertyName
        {
            get { return null; }
        }

        /// <summary>
        /// Gests the helper for inner tag.
        /// </summary>
        /// <remarks>
        /// Notes: It can only be used in the <see cref="BaseTagHelper{TControl}.Process(TagHelperContext, TagHelperOutput)"> method.
        /// Otherwise, it could bring unexpected error.
        /// </remarks>
        internal InnerTagHelper<TControl> InnerHelper
        {
            get { return _innerHelper ?? (_innerHelper = InnerTagHelper<TControl>.CreateInstance(UpdateProperty, this, CustomDefaultPropertyName)); }
        }

        /// <summary>
        /// Overrides to re-define getter and setter.
        /// </summary>
        protected override TControl TObject { get; set; }

        #region ITemplateTagHelper
        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.IsTemplate" />.
        /// Sets a boolean value which indicates whether transfer this <see cref="CollectionViewService{T}"/> to template mode.
        /// </summary>
        public bool IsTemplate
        {
            get { return InnerHelper.GetPropertyValue<bool>("IsTemplate"); }
            set { InnerHelper.SetPropertyValue("IsTemplate", value); }
        }

        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.TemplateBindings" />.
        /// Sets the collection of the template bindings.
        /// </summary>
        public object TemplateBindings
        {
            get { return InnerHelper.GetPropertyValue<object>("TemplateBindings"); }
            set { InnerHelper.SetPropertyValue("TemplateBindings", value); }
        }
        #endregion ITemplateTagHelper
        #endregion Properties

        #region Methods
        /// <summary>
        /// Updates the sub-property specified by its name and value.
        /// </summary>
        /// <param name="propertyName">The sub-property name.</param>
        /// <param name="value">The value to be updated.</param>
        /// <returns>
        /// A <see cref="System.Boolean"/> value indicates whether to use the default updating.
        /// If you don't want the default updating(update with the value to the property which name is <paramref name="propertyName"/>),
        /// you can write customized codes in this method and return true.
        /// Otherwise, return false.
        /// </returns>
        protected virtual bool UpdateProperty(string propertyName, object value)
        {
            return false;
        }

        /// <summary>
        /// Processes all attributes of current taghelper.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="parent">The parent control instance.</param>
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            _isIndependentTag = (parent == null);
            ProcessService(parent);
            InnerHelper.Update(TObject);
        }

        /// <summary>
        /// Processes the <see cref="Service"/> instance.
        /// </summary>
        /// <param name="parent">The parent control instance.</param>
        internal virtual void ProcessService(object parent)
        {
            TObject = InnerHelper.GetInstance(parent, GetObjectInstance, C1Property, CustomDefaultPropertyName != null);
        }

        /// <summary>
        /// Renders the startup scripts.
        /// </summary>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        protected override void Render(TagHelperOutput output)
        {
            if (_isIndependentTag)
            {
                using (var sw = new StringWriter())
                {
                    TObject.WriteTo(sw, HtmlEncoder);
                    output.SuppressOutput();
                    output.Content.SetHtmlContent(sw.ToString());
                }
            }
        }
        #endregion Methods
    }
}
