﻿namespace C1.Web.Mvc.TagHelpers
{
    public partial class RangeTagHelper
    {
        /// <summary>
        /// Configurates <see cref="Range.Color" />.
        /// Sets the color used to display this range.
        /// </summary>
        public string Color
        {
            get { return InnerHelper.GetPropertyValue<string>("Color"); }
            set { InnerHelper.SetPropertyValue("Color", value); }
        }
    }
}
