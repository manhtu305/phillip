﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-gauge-range")]
    public partial class LinearGaugeTagHelper
    {
    }

    [RestrictChildren("c1-gauge-range")]
    public partial class RadialGaugeTagHelper
    {
    }

    [RestrictChildren("c1-gauge-range")]
    public partial class BulletGraphTagHelper
    {
    }
}
