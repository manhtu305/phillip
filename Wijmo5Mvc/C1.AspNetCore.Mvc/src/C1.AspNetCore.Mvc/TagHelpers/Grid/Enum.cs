﻿namespace C1.Web.Mvc.TagHelpers
{
    public enum CellType
    {
        /// <summary>
        /// Regular data cell.
        /// </summary>
        Cells = 1,

        /// <summary>
        /// Top-left cell.
        /// </summary>
        TopLeftCells = 2,

        /// <summary>
        /// Row header cell.
        /// </summary>
        RowHeaders = 4,

        /// <summary>
        /// Column header cell.
        /// </summary>
        ColumnHeaders = 8,

        /// <summary>
        /// Column footer cell.
        /// </summary>
        ColumnFooters = 16,

        /// <summary>
        /// Bottom left cell(at the intersection of the row header and column footer cells).
        /// </summary>
        BottomLeftCells = 32
    }
}
