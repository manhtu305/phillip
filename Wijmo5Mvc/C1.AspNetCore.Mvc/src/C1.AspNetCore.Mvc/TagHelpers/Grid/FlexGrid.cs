﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using C1.Web.Mvc.Grid;
using System;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren(
        "c1-flex-grid-column", "c1-flex-grid-cell-template",
        "c1-items-source", "c1-odata-source", "c1-odata-virtual-source",
        "c1-flex-grid-errortip",
        //extenders
        "c1-flex-grid-group-panel", "c1-flex-grid-filter", "c1-flex-grid-detail", "c1-flex-grid-selector", "c1-flex-grid-boolean-checker"
    )]
    public partial class FlexGridTagHelper
    {
    }

    partial class FlexGridBaseTagHelper<T, TControl>
    {
        #region Fields
        private string _orderBy;
        private string _groupBy;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets or sets the minimum number of rows and/or columns required to enable virtualization.
        /// </summary>
        /// <remarks>
        /// Setting by a comma-separated string  of minimum number of rows and columns.
        /// </remarks>
        public virtual string VirtualizationThresholds
        {
            get
            {
                return string.Format("{0},{1}", TObject.VirtualizationThresholds[0], TObject.VirtualizationThresholds[1]);
            }
            set
            {
                string[] counts = value.Split(',');
                int numberRow = 0, numberColumn = 0;
                if (counts.Length > 0) int.TryParse(counts[0], out numberRow);
                if (counts.Length > 1) int.TryParse(counts[1], out numberColumn);
                TObject.VirtualizationThresholds[0] = numberRow;
                TObject.VirtualizationThresholds[1] = numberColumn;
            }
        }

        /// <summary>
        /// Gets or sets the sort expression.
        /// </summary>
        /// <remarks>
        /// A string that contains the field name followed by "ASC" or "asc" (ascending) or "DESC" or "desc" (descending). 
        /// Fields are sorted ascending by default. 
        /// Multiple fields can be separated by commas.
        /// </remarks>
        /// <example>
        /// <c1-felx-grid order-by="State ASC, ZipCode DESC"></c1-felx-grid>
        /// </example>
        public virtual string OrderBy
        {
            get
            {
                return _orderBy;
            }
            set
            {
                _orderBy = value;
                TObject.SortDescriptions.Clear();
                if (string.IsNullOrEmpty(_orderBy))
                {
                    return;
                }
                TObject.AddSortDescriptions(CollectionViewServiceTagHelper.GetSortDesciptions(value));
            }
        }

        /// <summary>
        /// Gets or sets the group descriptions.
        /// </summary>
        /// <remarks>
        /// A string that contains the field name. 
        /// Multiple fields can be separated by commas.
        /// </remarks>
        public virtual string GroupBy
        {
            get
            {
                return _groupBy;
            }
            set
            {
                _groupBy = value;
                TObject.GroupDescriptions.Clear();
                if (string.IsNullOrEmpty(_groupBy))
                {
                    return;
                }
                TObject.AddGroupDescriptions(CollectionViewServiceTagHelper.GetGroupDescriptions(value));
            }
        }

        /// <summary>
        /// Gets the static object that defines the default width for auto-generated grid columns based on their types.
        /// </summary>
        /// <remarks>
        /// Setting by string of values separated by comma, each pair of value is (DataType, width)
        /// </remarks>
        public virtual string DefaultTypeWidth
        {
            get
            {
                var dtw = TObject.DefaultTypeWidth;
                var str = "";
                foreach (var key in dtw.Keys)
                {
                    if (str != "") str += ",";
                    str += string.Format("{0},{1}", key, dtw[key]);
                }
                return str;
            }
            set
            {
                var dtw = TObject.DefaultTypeWidth;
                string[] a = value.Split(',');
                for (var i = 0; i < a.Length - 1; i += 2)
                {
                    var keyInt = ((int)Enum.Parse(typeof(DataType), a[i])).ToString();
                    if (dtw.ContainsKey(keyInt))
                    {
                        dtw[keyInt] = int.Parse(a[i + 1]);
                    }
                    else
                    {
                        dtw.Add(keyInt, int.Parse(a[i + 1]));
                    }
                }
            }
        }
        
        #endregion Properties
    }
}
