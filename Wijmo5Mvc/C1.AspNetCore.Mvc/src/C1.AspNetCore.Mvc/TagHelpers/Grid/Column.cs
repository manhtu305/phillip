﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-flex-grid-cell-template", "c1-data-map", "c1-flex-grid-column")]
    [HtmlTargetElement("c1-transposed-grid-row")]
    public partial class ColumnTagHelper
    {
        /// <summary>
        /// Gets the <see cref="Column"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="Column"/></returns>
        protected override Column GetObjectInstance(object parent = null)
        {
            return new Column(HtmlHelper);
        }
    }
}
