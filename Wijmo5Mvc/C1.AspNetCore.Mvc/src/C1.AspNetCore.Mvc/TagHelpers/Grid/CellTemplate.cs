﻿using System;
using System.Reflection;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Text.RegularExpressions;

namespace C1.Web.Mvc.TagHelpers
{
    [HtmlTargetElement("c1-flex-grid-cell-template")]
    public partial class CellTemplateTagHelper: SettingTagHelper<CellTemplate>
    {
        #region Properties
        /// <summary>
        /// Gets or sets which area the template works for.
        /// </summary>
        public CellType CellType { get; set; }

        /// <summary>
        /// Gets or sets a boolean value which indicates whether the template works for editing or not.
        /// </summary>
        public bool IsEditing { get; set; }

        /// <summary>
        /// Gets or sets the template's id.
        /// </summary>
        public string Id { get; set; }
        #endregion Properties

        #region Methods
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            context.Items[C1_TEMPLATE] = true;
            TObject = GetTemplate(parent);
        }
        private CellTemplate GetTemplate(object parent)
        {
            if(parent is FlexGridBase<object>)
            {
                return (parent.GetType().GetProperty(CellType.ToString() + "Template").GetValue(parent)) as CellTemplate;
            }
            else if(parent is ColumnBase)
            {
                return (parent as ColumnBase).CellTemplate;
            }
            else
            {
                throw new ArgumentOutOfRangeException("parent", "The parent taghelper of this template is invalid.");
            }
        }
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            var content = childContent.GetContent();
            var isTemplateContent = !string.IsNullOrEmpty(Regex.Replace(content, @"\s", ""));
            if (isTemplateContent)
            {
                if (IsEditing)
                {
                    TObject.EditTemplateContent = content;
                }
                else
                {
                    TObject.TemplateContent = content;
                }
            } else if(!string.IsNullOrEmpty(Id))
            {
                if (IsEditing)
                {
                    TObject.EditTemplateId = Id;
                }
                else
                {
                    TObject.TemplateId = Id;
                }
            }     
        }
        #endregion Methods
    }

}
