﻿namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines the taghelper for <see cref="C1.Web.Mvc.GroupDescription"/>
    /// </summary>
    /// <typeparam name="T">The item type</typeparam>
    public abstract partial class GroupDescriptionTagHelper<T>: SettingTagHelper<T>
        where T: GroupDescription
    {
    }
}
