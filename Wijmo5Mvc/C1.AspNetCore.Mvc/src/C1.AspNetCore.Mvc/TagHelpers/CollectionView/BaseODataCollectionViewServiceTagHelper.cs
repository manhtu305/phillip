﻿namespace C1.Web.Mvc.TagHelpers
{
    partial class BaseODataCollectionViewServiceTagHelper<T, TControl>
    {
        private string _fields;
        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.Fields" />.
        /// Sets a string containing a comma-separated list which represents the names of the fields to retrieve from the data source.
        /// </summary>
        /// <remarks>
        /// If this property is not set, all fields are retrieved.
        /// </remarks>
        public string Fields
        {
            get { return _fields; }
            set
            {
                _fields = value;
                if (string.IsNullOrEmpty(_fields))
                {
                    return;
                }
                InnerHelper.SetPropertyValue("Fields", value.Split(','));
            }
        }

        private string _keys;
        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.Keys" />.
        /// Sets a string containing a comma-separated list which represents the names of the key fields. Key fields are required for update operations (add/remove/delete).
        /// </summary>
        public string Keys
        {
            get { return _keys; }
            set
            {
                _keys = value;
                if (string.IsNullOrEmpty(_keys))
                {
                    return;
                }
                InnerHelper.SetPropertyValue("Keys", value.Split(','));
            }
        }
    }
}
