﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace C1.Web.Mvc.TagHelpers
{
    partial class BaseCollectionViewServiceTagHelper<T, TControl>
    {
        #region Properties
        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.SortDescriptions" />.
        /// Sets the sort descriptions.
        /// </summary>
        /// <remarks>
        /// A string that contains the field name followed by "ASC" or "asc" (ascending) or "DESC" or "desc" (descending). 
        /// Fields are sorted ascending by default. 
        /// Multiple fields can be separated by commas.
        /// </remarks>
        /// <example>
        /// &lt;c1-items-sources order-by="State ASC, ZipCode DESC"&gt;&lt;/c1-felx-grid&gt;
        /// </example>
        public string OrderBy
        {
            get
            {
                return InnerHelper.GetPropertyValue<string>("SortDescriptions");
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    InnerHelper.SetPropertyValue("SortDescriptions", value);
                }
            }
        }

        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.GroupDescriptions" />.
        /// Sets the group descriptions.
        /// </summary>
        /// <remarks>
        /// A string that contains the field name. 
        /// Multiple fields can be separated by commas.
        /// </remarks>
        public string GroupBy
        {
            get
            {
                return InnerHelper.GetPropertyValue<string>("GroupDescriptions");
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    InnerHelper.SetPropertyValue("GroupDescriptions", value);
                }
            }
        }

        /// <summary>
        /// Gets the customized default proeprty name.
        /// Overrides this property to customize the default property name for this tag.
        /// </summary>
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "ItemsSource";
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Updates the sub-property specified by its name and value.
        /// </summary>
        /// <param name="propertyName">The sub-property name.</param>
        /// <param name="value">The value to be updated.</param>
        /// <returns>
        /// A <see cref="System.Boolean"/> value indicates whether to use the default updating.
        /// If you don't want the default updating(update with the value to the property which name is <paramref name="propertyName"/>),
        /// you can write customized codes in this method and return true.
        /// Otherwise, return false.
        /// </returns>
        protected override bool UpdateProperty(string propertyName, object value)
        {
            switch (propertyName)
            {
                case "SortDescriptions":
                    TObject.SortDescriptions.Clear();
                    var sds = GetSortDesciptions((string)value);
                    foreach(var sd in sds)
                    {
                        TObject.SortDescriptions.Add(sd);
                    }
                    return true;
                case "GroupDescriptions":
                    TObject.GroupDescriptions.Clear();
                    var gds = GetGroupDescriptions((string)value);
                    foreach (var gd in gds)
                    {
                        TObject.GroupDescriptions.Add(gd);
                    }
                    return true;
                case "TemplateBindings":
                    ApplyTemplateBindings(value);
                    return true;
                default:
                    return base.UpdateProperty(propertyName, value);
            }
        }

        /// <summary>
        /// Processes the <see cref="Service"/> instance.
        /// </summary>
        /// <param name="parent">The parent control instance.</param>
        internal override void ProcessService(object parent)
        {
            // If it is a child property of some parent, get the collectionview service from the parent.
            var sourceParent = parent as IItemsSourceContainer<T>;
            if (sourceParent != null)
            {
                TObject = BaseCollectionViewService<T>.GetDataSource<TControl>(sourceParent, HtmlHelper);
            }
            else
            {
                base.ProcessService(parent);
            }
        }

        #region Private
        private void ApplyTemplateBindings(object templateBindings)
        {
            TObject.TemplateBindings.Clear();
            if (templateBindings != null)
            {
                var pdCollection = templateBindings.GetType().GetProperties();
                foreach (var pd in pdCollection)
                {
                    var itemValue = (string)(pd.GetValue(templateBindings));
                    TObject.TemplateBindings.Add(pd.Name, itemValue);
                }
            }
        }

        internal static IEnumerable<SortDescription> GetSortDesciptions(string value)
        {
            return value.Split(',').Select(GetSortDescription).Where(sd => sd != null);
        }

        internal static IEnumerable<GroupDescription> GetGroupDescriptions(string value)
        {
            return value.Split(',')
                            .Select(n => new PropertyGroupDescription { PropertyName = n.Trim(' ') });
        }

        private static SortDescription GetSortDescription(string sortExpression)
        {
            var sortDescription = sortExpression.Trim(' ').Split(' ');
            var ascending = true;
            if (sortDescription.Length > 1)
            {
                if (string.Compare("DESC", sortDescription[1], true) == 0)
                {
                    ascending = false;
                }
            }
            return new SortDescription
            {
                Property = sortDescription[0],
                Ascending = ascending
            };
        }
        #endregion Private
        #endregion Methods
    }

}
