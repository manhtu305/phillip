﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-property-group-description", "c1-sort-description")]
    partial class ODataCollectionViewServiceTagHelper
    {
    }
}
