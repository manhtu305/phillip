﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers.CollectionView
{
    [RestrictChildren("c1-sort-description")]
    partial class ODataVirtualCollectionViewServiceTagHelper
    {
    }
}
