﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Text.RegularExpressions;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-flow-layout", "c1-auto-grid-layout",
    "c1-manual-grid-layout", "c1-split-layout")]
    partial class DashboardLayoutTagHelper
    {
    }

    partial class LayoutBaseTagHelper<TControl>
    {
        /// <summary>
        /// Gets the customized default proeprty name.
        /// Overrides this property to customize the default property name for this tag.
        /// </summary>
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "Layout";
            }
        }

        /// <summary>
        /// Processes the <see cref="Service"/> instance.
        /// </summary>
        /// <param name="parent">The parent control instance.</param>
        internal override void ProcessService(object parent)
        {
            // If it is a child property of some parent, get the collectionview service from the parent.
            var sourceParent = parent as DashboardLayout;
            if (sourceParent != null)
            {
                TObject = DashboardLayout.GetLayout<TControl>(sourceParent, HtmlHelper);
            }
            else
            {
                base.ProcessService(parent);
            }
        }
    }

    [RestrictChildren("c1-flow-tile")]
    partial class FlowLayoutTagHelper
    {
    }

    [RestrictChildren("c1-split-tile", "c1-split-group")]
    partial class SplitGroupTagHelper
    {
        /// <summary>
        /// Processes the child content.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            if(parent is LayoutBase)
            {
                _collectionName = "Items";
            } else
            {
                _collectionName = "Children";
            }
            base.ProcessChildContent(parent, childContent);
        }
    }

    partial class SplitTileTagHelper
    {
        /// <summary>
        /// Processes the child content.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            if (parent is LayoutBase)
            {
                _collectionName = "Items";
            }
            else
            {
                _collectionName = "Children";
            }
            base.ProcessChildContent(parent, childContent);
        }
    }

    [RestrictChildren("c1-auto-grid-group")]
    partial class AutoGridLayoutTagHelper
    {
    }

    [RestrictChildren("c1-auto-grid-tile")]
    partial class AutoGridGroupTagHelper
    {

    }

    [RestrictChildren("c1-manual-grid-group")]
    partial class ManualGridLayoutTagHelper
    {
    }

    [RestrictChildren("c1-manual-grid-tile")]
    partial class ManualGridGroupTagHelper
    {
    }

    partial class TileTagHelper<TControl>
    {
        internal override void ProcessChildContent(TagHelperContent childContent, TagHelperOutput output, object parent)
        {
            var htmlContent = childContent.GetContent();
            var hasContent = !string.IsNullOrEmpty(Regex.Replace(htmlContent, @"\s", ""));
            if (hasContent)
            {
                TObject.InnerContent = htmlContent;
            }
            base.ProcessChildContent(childContent, output, parent);
        }
    }
}
