﻿namespace C1.Web.Mvc.TagHelpers
{
    partial class TreeViewTagHelper
    {
        private string _childItemsPath;

        /// <summary>
        /// Gets or sets the name of the property (or properties) that contains the child items for each node. When it is not set, ['items'] will be used.
        /// </summary>
        public string ChildItemsPath
        {
            get
            {
                return _childItemsPath;
            }
            set
            {
                _childItemsPath = value;
                if (string.IsNullOrEmpty(_childItemsPath))
                {
                    return;
                }
                TObject.ChildItemsPath = value.Split(',');
            }
        }

        private string _displayMemberPath;
        /// <summary>
        /// Gets or sets the name of the property (or properties) to use as the visual representation of the nodes. When it is not set, ['header'] will be used.
        /// </summary>
        public string DisplayMemberPath
        {
            get
            {
                return _displayMemberPath;
            }
            set
            {
                _displayMemberPath = value;
                if (string.IsNullOrEmpty(_displayMemberPath))
                {
                    return;
                }
                TObject.DisplayMemberPath = value.Split(',');
            }
        }

        private string _imageMemberPath;
        /// <summary>
        /// Gets or sets the name of the property (or properties) to use as a source of images for the nodes.
        /// </summary>
        public string ImageMemberPath
        {
            get
            {
                return _imageMemberPath;
            }
            set
            {
                _imageMemberPath = value;
                if (string.IsNullOrEmpty(_imageMemberPath))
                {
                    return;
                }
                TObject.ImageMemberPath = value.Split(',');
            }
        }

        private string _checkedMemberPath;
        /// <summary>
        /// Gets or sets the name of the property (or properties) to use as a source of images for the nodes.
        /// </summary>
        public string CheckedMemberPath
        {
            get
            {
                return _checkedMemberPath;
            }
            set
            {
                _checkedMemberPath = value;
                if (string.IsNullOrEmpty(_checkedMemberPath))
                {
                    return;
                }
                TObject.CheckedMemberPath = value.Split(',');
            }
        }
    }
}
