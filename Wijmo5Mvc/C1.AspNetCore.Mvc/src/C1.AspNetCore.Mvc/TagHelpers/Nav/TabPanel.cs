﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    partial class TabPanelTagHelper
    {
        internal override void FillContent(TagHelperOutput output, string content)
        {
            // does not clear the inner content,
            // and add the script content into the post element.
            output.PostElement.SetHtmlContent(content);
        }
    }
}
