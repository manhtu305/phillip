﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="TagHelper"/> to register scripts.
    /// </summary>
    [RestrictChildren("c1-basic-scripts")]
    [HtmlTargetElement("c1-scripts")]
    public class ScriptsTagHelper : ComponentTagHelper<Component>
    {
        private static readonly string _renderedKey = typeof(ScriptsTagHelper).FullName;

        private Scripts _scripts;
        private Scripts Scripts
        {
            get
            {
                return _scripts ?? (_scripts = TObject as Scripts);
            }
        }

        /// <summary>
        /// Gets or sets the culture which is used for scripts localization.
        /// </summary>
        public string Culture
        {
            get
            {
                return Scripts.Culture;
            }
            set
            {
                Scripts.Culture = value;
            }
        }

        /// <summary>
        /// Synchronously executes the <see cref="TagHelper"/> with the given context and output.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            // <c1-scripts> may be extended in other classes. Keep the sub tags should be processed only once.
            if (context.Items.ContainsKey(_renderedKey))
            {
                return;
            }

            // Mark the sub tags processed.
            context.Items[_renderedKey] = this;

            output.SuppressOutput();
            base.Process(context, output);
        }

        /// <summary>
        /// Gets the <see cref="TControl"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="TControl"/></returns>
        protected override Component GetObjectInstance(object parent = null)
        {
            return new Scripts(HtmlHelper);
        }
    }
}
