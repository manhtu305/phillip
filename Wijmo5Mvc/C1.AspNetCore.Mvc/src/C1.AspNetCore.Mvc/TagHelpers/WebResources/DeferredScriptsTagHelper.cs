﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="TagHelper"/> to render the deferred scripts.
    /// </summary>
    [HtmlTargetElement("c1-deferred-scripts")]
    public class DeferredScriptsTagHelper : ComponentTagHelper<Component>
    {
        /// <summary>
        /// Synchronously executes the <see cref="TagHelper"/> with the given context and output.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.SuppressOutput();
            base.Process(context, output);
        }

        /// <summary>
        /// Gets the <see cref="TControl"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="TControl"/></returns>
        protected override Component GetObjectInstance(object parent = null)
        {
            return new DeferredScripts(HtmlHelper);
        }
    }
}
