﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines the base <see cref="TagHelper"/> of script bundles.
    /// </summary>
    public abstract class BundlesTagHelper : BaseTagHelper<object>
    {
        private const char _bundlesSeparator = ',';
        private const string _subClassSeparator = "+";

        /// <summary>
        /// Process the child content.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            var scripts = parent as Scripts;
            if (scripts == null)
            {
                throw new InvalidOperationException();
            }

            var types = new List<Type>();
            var initOwnerTypes = InitOwnerTypes;
            if (initOwnerTypes != null)
            {
                types.AddRange(initOwnerTypes);
            }

            var ownerTypes = GetOwnerTypes();
            if (ownerTypes != null)
            {
                types.AddRange(ownerTypes);
            }

            var defaultOwnerTypes = DefaultOwnerTypes;
            if (!types.Any() && defaultOwnerTypes != null)
            {
                types.AddRange(defaultOwnerTypes);
            }

            scripts.OwnerTypes.AddRange(types);
        }

        /// <summary>
        /// Process the the children taghelpers.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        /// <param name="parent">The information from the parent taghelper.</param>
        protected override void ProcessChildContentAsync(TagHelperContext context, TagHelperOutput output, object parent)
        {
            // The parent should be the Scripts control, set TObject to parent for sub-tag.
            TObject = parent;
            base.ProcessChildContentAsync(context, output, parent);
        }

        private IEnumerable<Type> GetOwnerTypes()
        {
            return (Bundles ?? string.Empty).Split(_bundlesSeparator)
                .Select(b => ToOwnerType(b)).Where(b => b != null);
        }

        private Type ToOwnerType(string bundle)
        {
            return Type.GetType(string.Format("{0}{1}{2}", OwnerTypePrefix, _subClassSeparator, bundle), false, true);
        }

        /// <summary>
        /// Gets or sets the string which contains bundle names.
        /// </summary>
        /// <remarks>
        /// Please use comma to separate the names.
        /// </remarks>
        public virtual string Bundles
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the initial script owner types.
        /// </summary>
        protected virtual IEnumerable<Type> InitOwnerTypes
        {
            get
            {
                return new Type[] { };
            }
        }

        /// <summary>
        /// Gets the default script owner types.
        /// </summary>
        /// <remarks>When <see cref="Bundles"/> property is empty, this property will be used.</remarks>
        protected virtual IEnumerable<Type> DefaultOwnerTypes
        {
            get
            {
                return new Type[] { };
            }
        }

        /// <summary>
        /// Gets the prefix of the script owner type.
        /// </summary>
        protected abstract string OwnerTypePrefix { get; }
    }
}
