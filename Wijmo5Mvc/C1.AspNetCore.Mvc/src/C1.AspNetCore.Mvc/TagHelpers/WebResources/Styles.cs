﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines the tag for registering the style sheets.
    /// </summary>
    [HtmlTargetElement("c1-styles")]
    public sealed class StylesTagHelper : ComponentTagHelper<Component>
    {

        private Styles _styles;
        private Styles Styles
        {
            get
            {
                return _styles ?? (_styles = TObject as Styles);
            }
        }

        /// <summary>
        /// Gets or sets the theme.
        /// </summary>
        public string Theme
        {
            get
            {
                return Styles.Theme;
            }
            set
            {
                Styles.Theme = value;
            }
        }

        /// <summary>
        /// Gets the <see cref="TControl"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="TControl"/></returns>
        protected override Component GetObjectInstance(object parent = null)
        {
            return new Styles(HtmlHelper);
        }

        /// <summary>
        /// Synchronously executes the <see cref="TagHelper"/> with the given context and output.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.SuppressOutput();
            base.Process(context, output);
        }
    }
}
