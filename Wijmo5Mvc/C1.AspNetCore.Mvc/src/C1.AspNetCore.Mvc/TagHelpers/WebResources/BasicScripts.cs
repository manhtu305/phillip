﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Razor.TagHelpers;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="TagHelper"/> to register the scripts in basic assembly.
    /// </summary>
    [RestrictChildren("c1-chart-extension-scripts", "c1-grid-extension-scripts")]
    [HtmlTargetElement("c1-basic-scripts")]
    public class BasicScriptsTagHelper : BundlesTagHelper
    {
        private readonly string _ownerTypePrefix = typeof(Definitions).FullName;

        /// <summary>
        /// Gets or sets the string which contains bundle names.
        /// </summary>
        /// <remarks>
        /// Please use comma to separate the names.
        /// The names can be following values or the combination of them:
        /// Chart, CollectionView, Gauge, Grid and Input.
        /// If this property is empty or not set, it will add all script bundles
        /// in the basic assembly.
        /// </remarks>
        public override string Bundles
        {
            get
            {
                return base.Bundles;
            }

            set
            {
                base.Bundles = value;
            }
        }

        /// <summary>
        /// Gets the default script owner types.
        /// </summary>
        /// <remarks>When <see cref="Bundles"/> property is empty, this property will be used.</remarks>
        protected override IEnumerable<Type> DefaultOwnerTypes
        {
            get
            {
                return WebResourcesHelper.BasicOwnerTypes;
            }
        }

        /// <summary>
        /// Gets the prefix of the script owner type.
        /// </summary>
        protected override string OwnerTypePrefix
        {
            get
            {
                return _ownerTypePrefix;
            }
        }
    }
}
