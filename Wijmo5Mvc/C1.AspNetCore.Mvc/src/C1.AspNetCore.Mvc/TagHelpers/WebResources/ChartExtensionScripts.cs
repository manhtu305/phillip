﻿using C1.Web.Mvc.WebResources;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="TagHelper"/> of registering chart extension script bundles.
    /// </summary>
    /// <remarks>
    /// Whatever the bundles attribute is set or not, it will register default chart script bundles of below components:
    /// FlexChart, FlexPie, Sunburst and RangeSelector.
    /// </remarks>
    [HtmlTargetElement("c1-chart-extension-scripts")]
    public class ChartExtensionScriptsTagHelper : BundlesTagHelper
    {
        private readonly string _ownerTypePrefix = typeof(ChartExDefinitions).FullName;

        /// <summary>
        /// Gets or sets the string which contains bundle names.
        /// </summary>
        /// <remarks>
        /// Please use comma to separate the names. 
        /// The names can be following values or the combination of them:
        /// Animation, Annotation and LineMarker.
        /// Whatever this attribute is set or not, it will register default chart script bundles of below components:
        /// FlexChart, FlexPie, Sunburst and RangeSelector.
        /// </remarks>
        public override string Bundles
        {
            get
            {
                return base.Bundles;
            }

            set
            {
                base.Bundles = value;
            }
        }

        /// <summary>
        /// Gets the initial script owner types.
        /// </summary>
        protected override IEnumerable<Type> InitOwnerTypes
        {
            get
            {
                return new Type[] { typeof(Definitions.Chart) };
            }
        }

        /// <summary>
        /// Gets the prefix of the script owner type.
        /// </summary>
        protected override string OwnerTypePrefix
        {
            get
            {
                return _ownerTypePrefix;
            }
        }
    }
}
