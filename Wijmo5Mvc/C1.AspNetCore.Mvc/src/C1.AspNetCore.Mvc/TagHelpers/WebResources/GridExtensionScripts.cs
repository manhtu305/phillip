﻿using C1.Web.Mvc.WebResources;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="TagHelper"/> of registering grid extension script bundles.
    /// </summary>
    /// <remarks>
    /// Whatever the bundles attribute is set or not, it will register default grid script bundle of FlexGrid control.
    /// </remarks>
    [HtmlTargetElement("c1-grid-extension-scripts")]
    public class GridExtensionScriptsTagHelper : BundlesTagHelper
    {
        private readonly string _ownerTypePrefix = typeof(GridExDefinitions).FullName;

        /// <summary>
        /// Gets or sets the string which contains bundle names.
        /// </summary>
        /// <remarks>
        /// Please use comma to separate the names.
        /// The names can be following values or the combination of them:
        /// Detail, GroupPanel and Filter.
        /// If this property is empty or not set, it will add default grid script bundle.
        /// The default grid bundle only contains the scripts of FlexGrid control.
        /// </remarks>
        public override string Bundles
        {
            get
            {
                return base.Bundles;
            }

            set
            {
                base.Bundles = value;
            }
        }

        /// <summary>
        /// Gets the initial script owner types.
        /// </summary>
        protected override IEnumerable<Type> InitOwnerTypes
        {
            get
            {
                return new Type[] { typeof(Definitions.Grid) };
            }
        }

        /// <summary>
        /// Gets the prefix of the script owner type.
        /// </summary>
        protected override string OwnerTypePrefix
        {
            get
            {
                return _ownerTypePrefix;
            }
        }
    }
}
