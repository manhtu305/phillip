﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.ComponentModel;
using System.Linq;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Define the base class for the <see cref="TagHelper"/> to manage resources.
    /// </summary>
    /// <typeparam name="T">The item type.</typeparam>
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use <c1-scripts> instead.")]
    public abstract class WebResourcesManagerBaseTagHelper<T>: ComponentTagHelper<T>
        where T : WebResourcesManagerBase
    {
        #region Properties
        /// <summary>
        /// Gets or sets the types of the controls which resources would be registered in the view.
        /// </summary>
        public string ControlTypes
        {
            get { return string.Join(",", TObject.ControlTypes.Select(t => t.Name)); }
            set { _SetControlTypes(value); }
        }
        #endregion Properties

        #region Methods
        private void _SetControlTypes(string value)
        {
            var controlTypes = TObject.ControlTypes;
            controlTypes.Clear();
            var typeNames = value.Split(',').Distinct();

            foreach (var typeName in typeNames)
            {
                controlTypes.Add(GetControlTypeFromName(typeName.Trim()));
            }
        }

        private Type GetControlTypeFromName(string typeName)
        {
            var allControlTypes = TObject.GetAllControlTypes();
            foreach (var type in allControlTypes)
            {
                if (string.Compare(type.Name, typeName, true) == 0)
                {
                    return type;
                }
            }
            throw new ArgumentOutOfRangeException("typeName");
        }

        /// <summary>
        /// Synchronously executes the <see cref="Microsoft.AspNetCore.Razor.TagHelpers.TagHelper"/> with the given context and output.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.SuppressOutput();
            base.Process(context, output);
        }
        #endregion Methods
    }
}
