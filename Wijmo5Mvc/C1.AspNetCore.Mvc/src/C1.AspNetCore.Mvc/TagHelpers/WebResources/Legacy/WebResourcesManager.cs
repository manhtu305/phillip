﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.ComponentModel;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="TagHelper"/> to manage the resources.
    /// </summary>
    [HtmlTargetElement("c1-resources")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use <c1-scripts> and <c1-style-sheets> instead.")]
    public class WebResourcesManagerTagHelper : WebResourcesManagerBaseTagHelper<WebResourcesManager>
    {
        #region Properties
        /// <summary>
        /// Gets or sets the theme.
        /// </summary>
        public string Theme
        {
            get { return TObject.Theme; }
            set { TObject.Theme = value; }
        }
        #endregion Properties
    }
}