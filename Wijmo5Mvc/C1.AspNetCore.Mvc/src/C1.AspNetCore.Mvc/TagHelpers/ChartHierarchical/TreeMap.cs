﻿using C1.Web.Mvc.Chart;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source",
        "c1-flex-chart-title-style", "c1-flex-chart-datalabel",
        "c1-flex-chart-tooltip", "c1-tree-map-item-style","c1-chart-legend")]
    public partial class TreeMapTagHelper
    {
        #region BindingName
        private string _bindingName;
        /// <summary>
        /// Configurates <see cref="TreeMap{T}.BindingName" />.
        /// Sets the name of the property containing name of the data item. It is used to show name of the node.
        /// </summary>
        public string BindingName
        {
            get
            {
                return _bindingName;
            }
            set
            {
                _bindingName = value;
                if (string.IsNullOrEmpty(_bindingName))
                {
                    return;
                }
                TObject.BindingName = value.Split(',');
            }
        }
        #endregion BindingName

        #region ChildItemsPath
        private string _childItemsPath;
        /// <summary>
        /// Configurates <see cref="TreeMap{T}.ChildItemsPath" />.
        /// Sets the name of the property (or properties) used to generate child items in hierarchical data.
        /// </summary>
        /// <remarks>
        /// Set this property to an array containing the names of the properties that contain child items at each level, when the items are child items at different levels with different names (e.g. <code>[ 'Accounts', 'Checks', 'Earnings' ]</code>).
        /// </remarks>
        public string ChildItemsPath
        {
            get
            {
                return _childItemsPath;
            }
            set
            {
                _childItemsPath = value;
                if (string.IsNullOrEmpty(_childItemsPath))
                {
                    return;
                }
                TObject.ChildItemsPath = value.Split(',');
            }
        }
        #endregion ChildItemsPath

        /// <summary>
        /// Overrides to remove this attribute.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string SelectionChanged { get; set; }

        /// <summary>
        /// Overrides to remove this attribute.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ItemFormatter { get; set; }
    }
}
