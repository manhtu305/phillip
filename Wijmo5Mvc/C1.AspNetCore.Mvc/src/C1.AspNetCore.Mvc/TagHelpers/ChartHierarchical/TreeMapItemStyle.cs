﻿using C1.Web.Mvc.Localization;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace C1.Web.Mvc.TagHelpers
{
    /// <summary>
    /// <see cref="ITagHelper"/> implementation for <see cref="TreeMapItemStyle" />.
    /// </summary>
    [HtmlTargetElement("c1-tree-map-item-style")]
    public class TreeMapItemStyleTagHelper
        : SettingTagHelper<TreeMapItemStyle>
    {
        /// <summary>
        /// Configurates <see cref="TreeMapItemStyle.TitleColor" />.
        /// Sets the title color.
        /// </summary>
        public string TitleColor
        {
            get { return InnerHelper.GetPropertyValue<string>("TitleColor"); }
            set { InnerHelper.SetPropertyValue("TitleColor", value); }
        }

        /// <summary>
        /// Configurates <see cref="TreeMapItemStyle.MaxColor" />.
        /// Sets the color of the max value.
        /// </summary>
        public string MaxColor
        {
            get { return InnerHelper.GetPropertyValue<string>("MaxColor"); }
            set { InnerHelper.SetPropertyValue("MaxColor", value); }
        }

        /// <summary>
        /// Configurates <see cref="TreeMapItemStyle.MinColor" />.
        /// Sets the color of the min value.
        /// </summary>
        public string MinColor
        {
            get { return InnerHelper.GetPropertyValue<string>("MinColor"); }
            set { InnerHelper.SetPropertyValue("MinColor", value); }
        }

        /// <summary>
        /// Overrides to specify the collection name.
        /// </summary>
        protected override string CollectionName
        {
            get
            {
                return "Palette";
            }
        }

        /// <summary>
        /// Overrides to add the style item to Palette.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            PropertyInfo piCollection = null;
            try
            {
                piCollection = parent.GetType().GetProperty(CollectionName, typeof(List<object>));
            }
            catch { }

            if (piCollection != null)
            {
                var collection = piCollection.GetValue(parent);
                var iCollection = collection as IList;
                iCollection.Add(TObject);
                return;
            }
            throw new Exception(Resources.NotSetC1Property);
        }
    }
}
