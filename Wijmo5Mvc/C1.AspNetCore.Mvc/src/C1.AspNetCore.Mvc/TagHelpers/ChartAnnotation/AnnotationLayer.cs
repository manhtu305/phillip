﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-annotation-text", "c1-annotation-rectangle", "c1-annotation-circle",
        "c1-annotation-square", "c1-annotation-line", "c1-annotation-ellipse", "c1-annotation-image",
        "c1-annotation-polygon")]
    public partial class AnnotationLayerTagHelper
    {
    }
}
