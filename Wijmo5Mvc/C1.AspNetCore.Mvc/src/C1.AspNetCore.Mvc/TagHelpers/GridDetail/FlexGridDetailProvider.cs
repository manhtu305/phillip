﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Text.RegularExpressions;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class FlexGridDetailProviderTagHelper
    {
        #region Methods
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            base.ProcessAttributes(context, parent);
            context.Items[C1_TEMPLATE] = true;
        }

        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            base.ProcessChildContent(parent, childContent);
            var content = childContent.GetContent();
            var isTemplateContent = !string.IsNullOrEmpty(Regex.Replace(content, @"\s", ""));
            if (isTemplateContent)
            {
                this.TObject.DetailRowTemplateContent = content;
            }

        }
        #endregion Methods
    }
}
