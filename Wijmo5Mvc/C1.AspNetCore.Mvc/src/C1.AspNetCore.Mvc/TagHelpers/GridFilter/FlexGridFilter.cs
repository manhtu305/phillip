﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-flex-grid-column-filter")]
    public partial class FlexGridFilterTagHelper
    {
        /// <summary>
        /// Obsoleted: Use the column-filters attribute instead of it.
        /// </summary>
        [Obsolete("Use the column-filters attribute instead of it.")]
        public object ColumnFilterTypes
        {
            get { return InnerHelper.GetPropertyValue<object>("ColumnFilterTypes"); }
            set
            {
                IDictionary<string, FilterType> dictionary = null;
                if (value is IDictionary<string, FilterType>)
                {
                    dictionary = (IDictionary<string, FilterType>)value;
                }
                else
                {
                    dictionary = new Dictionary<string, FilterType>();
                    if (value != null)
                    {
                        var pdCollection = value.GetType().GetProperties();
                        foreach (var pd in pdCollection)
                        {
                            var itemValue = (FilterType)(pd.GetValue(value));
                            dictionary.Add(pd.Name, itemValue);
                        }
                    }
                }
                InnerHelper.SetPropertyValue("ColumnFilterTypes", dictionary);
            }
        }

        protected override FlexGridFilter<object> GetObjectInstance(object parent)
        {
            if (parent != null && parent is FlexGridGroupPanel<object>)
            {
                return ((FlexGridGroupPanel<object>)parent).Filter;
            }
            return base.GetObjectInstance(parent);
        }

        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            if (parent != null && parent is FlexGridGroupPanel<object>)
            {
                return;
            }
            base.ProcessChildContent(parent, childContent);
        }
    }

    [RestrictChildren("c1-flex-grid-value-filter")]
    public partial class ColumnFilterTagHelper
    {
        [HtmlAttributeNotBound]
        public override string C1Property
        {
            get
            {
                return base.C1Property;
            }

            set
            {
                base.C1Property = value;
            }
        }
    }

    public partial class ValueFilterTagHelper
    {
        [HtmlAttributeNotBound]
        public override string C1Property
        {
            get
            {
                return base.C1Property;
            }

            set
            {
                base.C1Property = value;
            }
        }
    }
}
