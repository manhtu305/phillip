﻿using System.Collections.Generic;
using C1.Web.Mvc.Chart;

namespace C1.Web.Mvc.TagHelpers
{
    public abstract partial class FlexChartBaseTagHelper<T, TControl>
    {
        /// <summary>
        /// Configurates <see cref="FlexChartBase{T}.Palette" />.
        /// sets an array of default colors to use for displaying each series.
        /// </summary>
        /// <remarks>
        ///  There is a set of predefined palettes in the <see cref="Chart.Palettes"/> class that you can use.
        /// </remarks>
        public IEnumerable<string> Palette
        {
            get { return TObject.Palette; }
            set { TObject.Palette = value; }
        }

        /// <summary>
        /// Configurates <see cref="FlexChartBase{T}.Legend" />.
        /// Sets the enumerated value that determines whether and where the legend appears in relation to the chart.
        /// </summary>
        public Position LegendPosition
        {
            get { return TObject.Legend.Position; }
            set { TObject.Legend.Position = value; }
        }

        /// <summary>
        /// Configurates <see cref="FlexChartBase{T}.Legend" />.
        /// Sets the enumerated value that determines whether the legend orientate.
        /// </summary>
        public LegendOrientation LegendOrientation
        {
            get { return TObject.Legend.Orientation; }
            set { TObject.Legend.Orientation = value; }
        }
    }
}
