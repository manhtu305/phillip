﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-funnel-options", "c1-step-options")]
    public partial class ExtraOptionsTagHelper
        : SettingTagHelper<ExtraOptions>
    {
        /// <summary>
        /// Gets or sets the property name which the taghelper is related to.
        /// </summary>
        public override string C1Property
        {
            get
            {
                return "Options";
            }
        }
    }
}
