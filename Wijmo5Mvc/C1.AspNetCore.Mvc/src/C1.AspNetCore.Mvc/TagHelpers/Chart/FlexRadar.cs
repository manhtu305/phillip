﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren(
        "c1-flex-chart-axis", "c1-flex-chart-datalabel", 
        "c1-items-source", "c1-odata-source", "c1-odata-virtual-source",
        "c1-flex-chart-title-style", "c1-flex-radar-series", "c1-flex-chart-tooltip",
        "c1-chart-animation", "c1-chart-gestures", "c1-chart-legend"
    )]
    public partial class FlexRadarTagHelper
    {
    }
}