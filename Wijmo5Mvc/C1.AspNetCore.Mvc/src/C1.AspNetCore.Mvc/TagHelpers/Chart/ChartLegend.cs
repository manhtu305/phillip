﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class ChartLegendTagHelper
        : SettingTagHelper<ChartLegend>
    {
        /// <summary>
        /// Gets the customized default property name.
        /// </summary>
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "Legend";
            }
        }
    }
}
