﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren(
        "c1-flex-chart-axis", "c1-flex-chart-datalabel", 
        "c1-items-source", "c1-odata-source", "c1-odata-virtual-source",
        "c1-flex-chart-title-style", "c1-flex-chart-series", "c1-flex-chart-tooltip",
        //trendline
        "c1-flex-chart-trendline", "c1-flex-chart-yfunction", "c1-flex-chart-parameterfunction", "c1-flex-chart-movingaverage",
        //extenders
        "c1-line-marker", "c1-range-selector", "c1-annotation-layer",
        "c1-chart-animation", "c1-chart-gestures",
        "c1-plot-area", "c1-flex-chart-waterfall", "c1-chart-options", "c1-flex-chart-boxwhisker", "c1-flex-chart-error-bar",
        "c1-chart-legend", "c1-flex-chart-breakeven"
    )]
    public partial class FlexChartTagHelper
    {
        /// <summary>
        /// Specifies the maximum size (pixels) of symbols in the Bubble chart. The default value is 30.
        /// </summary>
        public int BubbleMaxSize
        {
            get { return TObject.Options.BubbleMaxSize; }
            set { TObject.Options.BubbleMaxSize = value; }
        }

        /// <summary>
        /// Specifies the minimum size (pixels) of symbols in the Bubble chart. The default value is 5.
        /// </summary>
        public int BubbleMinSize
        {
            get { return TObject.Options.BubbleMinSize; }
            set { TObject.Options.BubbleMinSize = value; }
        }

        /// <summary>
        /// Specifies the group width for Column charts, or the group height for Bar charts. 
        /// The group width can be specified in pixels or percent of available space. 
        /// The default value is "70%".
        /// </summary>
        public string GroupWidth
        {
            get { return TObject.Options.GroupWidth; }
            set { TObject.Options.GroupWidth = value; }
        }
    }
}