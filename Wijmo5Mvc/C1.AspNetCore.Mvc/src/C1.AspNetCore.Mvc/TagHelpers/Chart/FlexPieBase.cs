﻿namespace C1.Web.Mvc.TagHelpers
{
    public abstract partial class FlexPieBaseTagHelper<T, TControl>
        : FlexChartBaseTagHelper<T, TControl>
        where TControl : FlexPieBase<T>
    {
    }
}
