﻿namespace C1.Web.Mvc.TagHelpers
{
    public partial class StepOptionsTagHelper
        : SettingTagHelper<StepOptions>
    {
        /// <summary>
        /// Gets or sets the property name which the taghelper is related to.
        /// </summary>
        public override string C1Property
        {
            get
            {
                return "Step";
            }
        }
    }
}
