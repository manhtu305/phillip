﻿namespace C1.Web.Mvc.TagHelpers
{
    public partial class FunnelOptionsTagHelper
        : SettingTagHelper<FunnelOptions>
    {
        /// <summary>
        /// Gets or sets the property name which the taghelper is related to.
        /// </summary>
        public override string C1Property
        {
            get
            {
                return "Funnel";
            }
        }
    }
}
