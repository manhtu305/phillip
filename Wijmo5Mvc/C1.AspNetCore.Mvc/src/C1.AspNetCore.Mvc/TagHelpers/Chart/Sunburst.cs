﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source",
        "c1-flex-chart-title-style", "c1-flex-pie-datalabel", "c1-flex-chart-tooltip", "c1-chart-animation", "c1-chart-legend")]
    public partial class SunburstTagHelper
    : FlexPieBaseTagHelper<object, Sunburst<object>>
    {
    }
}
