﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source")]
    public partial class ChartAxisTagHelper
        : SettingTagHelper<ChartAxis<object>>
    {
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            var series = parent as ChartSeriesBase<object>;
            if (series != null)
            {
                if (this.C1Property == "AxisX")
                {
                    series.AxisX = new ChartAxis<object>(series._owner, true);
                    series.AxisX.Position = Chart.Position.Bottom;
                }
                if (this.C1Property == "AxisY")
                {
                    series.AxisY = new ChartAxis<object>(series._owner, false);
                    series.AxisY.Position = Chart.Position.Left;
                    series.AxisY.AxisLine = false;
                    series.AxisY.MajorGrid = false;
                }
            }
            base.ProcessAttributes(context, parent);
        }
    }
}
