﻿namespace C1.Web.Mvc.TagHelpers
{
    public partial class ChartTooltipTagHelper
        : TooltipBaseTagHelper<ChartTooltip>
    {
        /// <summary>
        /// Gets the customized default proeprty name.
        /// Overrides this property to customize the default property name for this tag.
        /// </summary>
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "Tooltip";
            }
        }
    }
}
