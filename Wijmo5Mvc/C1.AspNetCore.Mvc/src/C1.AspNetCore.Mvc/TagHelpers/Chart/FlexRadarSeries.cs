﻿using C1.Web.Mvc.Chart;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    [RestrictChildren("c1-flex-chart-axis", "c1-items-source", "c1-odata-source", "c1-odata-virtual-source")]
    public partial class FlexRadarSeriesTagHelper
    {
        /// <summary>
        /// Configurates <see cref="FlexRadarSeries{T}.ChartType" />.
        /// Sets the chart type for a specific series, overriding the chart type set on the overall chart.
        /// </summary>
        public RadarChartType ChartType
        {
            get { return InnerHelper.GetPropertyValue<RadarChartType>("ChartType"); }
            set { InnerHelper.SetPropertyValue("ChartType", value); }
        }
    }
}