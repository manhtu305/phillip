﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.TagHelpers
{
    public partial class ChartSeriesBaseTagHelper<T, TControl>
        : SettingTagHelper<TControl>
        where TControl : ChartSeriesBase<T>
    {
        protected override TControl GetObjectInstance(object parent)
        {
            TControl series = base.GetObjectInstance(parent);
            if (series._owner == null)
            {
                series._owner = parent as FlexChartCore<T>;
            }
            return series;
        }

        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            if (this.AxisX != null)
            {
                this.AxisX._owner = parent as FlexChart<T>;
            }
            if (this.AxisY != null)
            {
                this.AxisY._owner = parent as FlexChart<T>;
            }

            base.ProcessAttributes(context, parent);
        }
    }

    [RestrictChildren("c1-flex-chart-axis", "c1-items-source", "c1-odata-source", "c1-odata-virtual-source")]
    public partial class ChartSeriesTagHelper
    {
        /// <summary>
        /// Configurates <see cref="ChartSeries{T}.ChartType" />.
        /// Sets the chart type for a specific series, overriding the chart type set on the overall chart.
        /// </summary>
        public Chart.ChartType ChartType
        {
            get { return InnerHelper.GetPropertyValue<Chart.ChartType>("ChartType"); }
            set { InnerHelper.SetPropertyValue("ChartType", value); }
        }
    }

    [RestrictChildren("c1-flex-chart-axis", "c1-items-source", "c1-odata-source", "c1-odata-virtual-source")]
    public partial class FunctionSeriesTagHelper
    {
    }

    [RestrictChildren("c1-flex-chart-axis", "c1-items-source", "c1-odata-source", "c1-odata-virtual-source")]
    public partial class MovingAverageTagHelper
    {
    }

    [RestrictChildren("c1-flex-chart-axis", "c1-items-source", "c1-odata-source", "c1-odata-virtual-source")]
    public partial class TrendLineTagHelper
    {
    }
}