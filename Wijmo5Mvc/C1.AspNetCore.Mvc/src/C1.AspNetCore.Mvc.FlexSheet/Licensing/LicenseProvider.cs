﻿using C1.Util.Licensing;

namespace C1.Web.Mvc.Sheet
{
    internal class LicenseProviderAttribute : BaseLicenseProviderAttribute
    {
        public override string RunTimeKey
        {
            get
            {
                return LicenseManager.Key;
            }
        }
    }
}
