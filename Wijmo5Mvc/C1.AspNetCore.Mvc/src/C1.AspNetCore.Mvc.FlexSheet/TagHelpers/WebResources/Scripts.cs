﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Sheet.TagHelpers
{
    /// <summary>
    /// Extends <see cref="Mvc.TagHelpers.ScriptsTagHelper"/> for FlexSheet control scripts.
    /// </summary>
    [RestrictChildren("c1-flex-sheet-scripts")]
    [HtmlTargetElement("c1-scripts")]
    public class ScriptsTagHelper : Mvc.TagHelpers.ScriptsTagHelper
    {
    }
}
