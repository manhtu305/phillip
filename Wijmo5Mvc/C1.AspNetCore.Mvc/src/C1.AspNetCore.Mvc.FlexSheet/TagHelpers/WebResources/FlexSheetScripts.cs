﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel;
using C1.Web.Mvc.TagHelpers;

namespace C1.Web.Mvc.Sheet.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="TagHelper"/> of registering FlexSheet control related script bundle.
    /// This bundle contains FinancialChart control and FormulaBar extender.
    /// </summary>
    [HtmlTargetElement("c1-flex-sheet-scripts")]
    public class FlexSheetScriptsTagHelper : BundlesTagHelper
    {
        private readonly string _ownerTypePrefix = typeof(WebResources.Definitions).FullName;

        /// <summary>
        /// This property is useless in current tag.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Bundles
        {
            get
            {
                return base.Bundles;
            }

            set
            {
                base.Bundles = value;
            }
        }

        /// <summary>
        /// Gets the initial script owner types.
        /// </summary>
        protected override IEnumerable<Type> InitOwnerTypes
        {
            get
            {
                return FlexSheetWebResourcesHelper.AllScriptOwnerTypes.Value;
            }
        }

        /// <summary>
        /// Gets the prefix of the script owner type.
        /// </summary>
        protected override string OwnerTypePrefix
        {
            get
            {
                return _ownerTypePrefix;
            }
        }
    }
}
