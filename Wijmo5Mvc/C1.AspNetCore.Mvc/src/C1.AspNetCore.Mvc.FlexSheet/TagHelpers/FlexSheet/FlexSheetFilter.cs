﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Sheet.TagHelpers
{
    partial class FlexSheetFilterTagHelper
    {
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "Filter";
            }
        }
    }

    [RestrictChildren("c1-sheet-column-filter", "c1-data-map")]
    partial class FilterSettingTagHelper
    {
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "Filter";
            }
        }
    }

    [RestrictChildren("c1-sheet-value-filter", "c1-sheet-condition-filter", "c1-data-map")]
    partial class ColumnFilterSettingTagHelper
    {
        /// <summary>
        /// Gets the <see cref="ColumnFilterSetting"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="ColumnFilterSetting"/></returns>
        protected override ColumnFilterSetting GetObjectInstance(object parent = null)
        {
            return new ColumnFilterSetting(HtmlHelper);
        }
    }

    [RestrictChildren("c1-data-map")]
    partial class ValueFilterSettingTagHelper
    {
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "ValueFilter";
            }
        }
    }

    [RestrictChildren("c1-data-map")]
    partial class ConditionFilterSettingTagHelper
    {
        protected override string CustomDefaultPropertyName
        {
            get
            {
                return "ConditionFilter";
            }
        }
    }
}
