﻿using C1.Web.Mvc.Grid;
using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel;

namespace C1.Web.Mvc.Sheet.TagHelpers
{
    [RestrictChildren("c1-bound-sheet", "c1-unbound-sheet", "c1-formula-bar", "c1-defined-name", "c1-flex-sheet-filter")]
    public partial class FlexSheetTagHelper
    {
        #region hidden FlexGridTagHelper properties.

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AllowAddNew { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AllowSorting { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool? QuickAutoSize { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ChildItemsPath { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string GroupHeaderFormat { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override HeadersVisibility HeadersVisibility { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ItemFormatter { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowGroups { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool StickyHeaders { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowSort { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int? SortRowIndex { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int TreeIndent { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowColumnFooters { get; set; }

        /// <summary>
        /// This property has been deprecated in FlexSheet.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ColumnFootersRowHeaderText { get; set; }

        /// <summary>
        /// Overrides to hide this attribute.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string GroupBy { get; set; }

        /// <summary>
        /// Overrides to hide this attribute.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OrderBy { get; set; }

        #endregion
    }
}
