﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Sheet.TagHelpers
{
    /// <summary>
    /// <see cref="ITagHelper"/> implementation for <see cref="DefinedName" />.
    /// </summary>
    [HtmlTargetElement("c1-defined-name")]
    public partial class DefinedNameTagHelper : SettingTagHelper<DefinedName>
    {
        private string _collectionName = "DefinedNames";

        /// <summary>
        /// Gets the collection name.
        /// </summary>
        protected override string CollectionName
        {
            get
            {
                return _collectionName;
            }
        }

        /// <summary>
        /// Configurates <see cref="DefinedName.Name" />.
        /// Sets the name of the defined name item.
        /// </summary>
        public string Name
        {
            get { return InnerHelper.GetPropertyValue<string>("Name"); }
            set { InnerHelper.SetPropertyValue("Name", value); }
        }

        /// <summary>
        /// Configurates <see cref="DefinedName.SheetName" />.
        /// Sets the defined name item works in which sheet.
        /// If omitted, the defined name item works in workbook.
        /// </summary>
        public string SheetName
        {
            get { return InnerHelper.GetPropertyValue<string>("SheetName"); }
            set { InnerHelper.SetPropertyValue("SheetName", value); }
        }

        /// <summary>
        /// Configurates <see cref="DefinedName.Value" />.
        /// Sets the value of the defined name item.
        /// The value could be a formula, a string constant or a cell range.
        /// For e.g. "Sum(1, 2, 3)", "1.2", "\"test\"" or "sheet1!A1:B2".
        /// </summary>
        public string Value
        {
            get { return InnerHelper.GetPropertyValue<string>("Value"); }
            set { InnerHelper.SetPropertyValue("Value", value); }
        }
    }
}
