﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Sheet.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-sheet-table-column")]
    partial class TableTagHelper
    {
        /// <summary>
        /// Gets the <see cref="Table"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="Table"/></returns>
        protected override Table GetObjectInstance(object parent = null)
        {
            return new Table(HtmlHelper);
        }
    }
}
