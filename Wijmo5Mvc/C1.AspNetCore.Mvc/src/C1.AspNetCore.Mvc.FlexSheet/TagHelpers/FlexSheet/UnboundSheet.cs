﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Sheet.TagHelpers
{
    [RestrictChildren("c1-sheet-table", "c1-sheet-filter")]
    partial class UnboundSheetTagHelper
    {
        /// <summary>
        /// Gets the <see cref="UnboundSheet"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="UnboundSheet"/></returns>
        protected override UnboundSheet GetObjectInstance(object parent = null)
        {
            return new UnboundSheet(HtmlHelper);
        }
    }
}
