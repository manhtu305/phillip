﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Sheet.TagHelpers
{
    [RestrictChildren("c1-items-source", "c1-odata-source", "c1-odata-virtual-source", "c1-sheet-table", "c1-sheet-filter")]
    public partial class BoundSheetTagHelper
    {
        /// <summary>
        /// Gets the <see cref="BoundSheet{T}"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="BoundSheet{T}"/></returns>
        protected override BoundSheet<object> GetObjectInstance(object parent = null)
        {
            return new BoundSheet<object>(HtmlHelper);
        }
    }
}
