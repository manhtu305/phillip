﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using C1.Web.Mvc.TagHelpers;

namespace C1.Web.Mvc.MultiRow.TagHelpers
{
    /// <summary>
    /// Extends <see cref="Mvc.TagHelpers.ScriptsTagHelper"/> for MultiRow scripts.
    /// </summary>
    [RestrictChildren("c1-multi-row-scripts")]
    [HtmlTargetElement("c1-scripts")]
    public class ScriptsTagHelper : Mvc.TagHelpers.ScriptsTagHelper
    {
    }
}
