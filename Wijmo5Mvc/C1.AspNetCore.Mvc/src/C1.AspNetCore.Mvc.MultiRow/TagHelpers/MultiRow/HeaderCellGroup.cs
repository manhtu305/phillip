﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.MultiRow.TagHelpers
{
    [RestrictChildren("c1-multi-row-cell")]
    public partial class HeaderCellGroupTagHelper
    {
        /// <summary>
        /// Gets the <see cref="HeaderCellGroup"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="HeaderCellGroup"/></returns>
        protected override HeaderCellGroup GetObjectInstance(object parent = null)
        {
            return ReflectUtils.GetInstanceWithHtmlHelper(typeof(HeaderCellGroup), HtmlHelper) as HeaderCellGroup;
        }
    }
}
