﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.ComponentModel;

namespace C1.Web.Mvc.MultiRow.TagHelpers
{
    [RestrictChildren(
        "c1-multi-row-cell-group", "c1-flex-grid-cell-template", 
        "c1-items-source", "c1-odata-source", "c1-odata-virtual-source", "c1-multi-row-header-cell-group",
        //extenders
        "c1-flex-grid-group-panel", "c1-flex-grid-filter"
    )]
    public partial class MultiRowTagHelper
    {
        #region Properties
        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DraggingColumnOver
        {
            get;
            set;
        }

        /// <summary>
        /// This attribute is removed.
        /// </summary>
        [HtmlAttributeNotBound]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string DraggingRowOver
        {
            get;
            set;
        }
        #endregion Properties
    }
}
