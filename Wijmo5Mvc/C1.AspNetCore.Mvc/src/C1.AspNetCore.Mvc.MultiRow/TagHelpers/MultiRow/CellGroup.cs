﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.MultiRow.TagHelpers
{
    [RestrictChildren("c1-multi-row-cell")]
    public partial class CellGroupTagHelper
    {
        /// <summary>
        /// Gets the <see cref="CellGroup"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="CellGroup"/></returns>
        protected override CellGroup GetObjectInstance(object parent = null)
        {
            return ReflectUtils.GetInstanceWithHtmlHelper(typeof(CellGroup), HtmlHelper) as CellGroup;
        }
    }
}
