﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.MultiRow.TagHelpers
{
    [RestrictChildren("c1-flex-grid-cell-template", "c1-data-map")]
    public partial class CellTagHelper
    {
        /// <summary>
        /// Gets the <see cref="Cell"/> instance.
        /// </summary>
        /// <param name="parent">The parent object. It is optional.</param>
        /// <returns>An instance of <see cref="Cell"/></returns>
        protected override Cell GetObjectInstance(object parent = null)
        {
            return new Cell(HtmlHelper);
        }
    }
}
