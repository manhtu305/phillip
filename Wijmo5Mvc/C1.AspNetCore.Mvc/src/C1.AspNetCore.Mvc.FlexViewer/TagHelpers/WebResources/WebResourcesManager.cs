﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.ComponentModel;

namespace C1.Web.Mvc.Viewer.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="Microsoft.AspNetCore.Razor.TagHelpers.TagHelper"/> for FlexViewer web resources.
    /// </summary>
    [HtmlTargetElement("c1-flex-viewer-resources")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use <c1-scripts> instead.")]
    public class ViewerWebResourcesManagerTagHelper : WebResourcesManagerBaseTagHelper<ViewerWebResourcesManager>
    {
    }
}
