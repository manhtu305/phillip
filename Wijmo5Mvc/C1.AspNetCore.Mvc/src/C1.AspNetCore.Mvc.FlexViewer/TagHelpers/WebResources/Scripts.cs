﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Viewer.TagHelpers
{
    /// <summary>
    /// Extends <see cref="Mvc.TagHelpers.ScriptsTagHelper"/> for viewer related scripts.
    /// </summary>
    [RestrictChildren("c1-flex-viewer-scripts")]
    [HtmlTargetElement("c1-scripts")]
    public class ScriptsTagHelper : Mvc.TagHelpers.ScriptsTagHelper
    {
    }
}
