﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;

namespace C1.Web.Mvc.Finance.TagHelpers
{
    [RestrictChildren(        
        "c1-flex-chart-axis", "c1-flex-chart-datalabel", 
        // data source
        "c1-items-source", "c1-odata-source", "c1-odata-virtual-source",
        "c1-flex-chart-title-style", "c1-flex-chart-series", "c1-flex-chart-tooltip",
        // Financial series
        "c1-financial-chart-series", "c1-flex-chart-bollinger-bands", "c1-flex-chart-envelopes", 
        "c1-flex-chart-fibonacci", "c1-flex-chart-fibonacci-arcs", "c1-flex-chart-fibonacci-fans", "c1-flex-chart-fibonacci-time-zones",
        "c1-flex-chart-macd", "c1-flex-chart-macd-histogram", "c1-flex-chart-stochastic",
        "c1-flex-chart-rsi", "c1-flex-chart-atr", "c1-flex-chart-williams-r","c1-flex-chart-cci",
        //trendline
        "c1-flex-chart-trendline", "c1-flex-chart-yfunction", "c1-flex-chart-parameterfunction", "c1-flex-chart-movingaverage",
        //extenders
        "c1-line-marker", "c1-range-selector", "c1-annotation-layer",
        "c1-chart-animation", "c1-chart-gestures",
        "c1-plot-area", "c1-flex-chart-waterfall", "c1-chart-options", "c1-flex-chart-boxwhisker", "c1-flex-chart-error-bar",
        "c1-chart-legend")]
    public partial class FinancialChartTagHelper
    {
        /// <summary>
        /// Specifies the DataFields used for the Kagi chart. 
        /// The default value is DataFields.Close.
        /// </summary>
        public DataFields KagiFields
        {
            get { return TObject.Options.KagiFields; }
            set { TObject.Options.KagiFields = value; }
        }

        /// <summary>
        /// Specifies the RangeMode for the Kagi chart. 
        /// The default value is RangeMode.Fixed.
        /// </summary>
        public RangeMode KagiRangeMode
        {
            get { return TObject.Options.KagiRangeMode; }
            set { TObject.Options.KagiRangeMode = value; }
        }

        /// <summary>
        /// Specifies the reversal amount for the Kagi chart. 
        /// The default value is 14.
        /// </summary>
        public int KagiReversalAmount
        {
            get { return TObject.Options.KagiReversalAmount; }
            set { TObject.Options.KagiReversalAmount = value; }
        }

        /// <summary>
        /// Gets or sets the number of previous boxes that must be compared before a new box is drawn in Line Break charts. 
        /// The default value is 3.
        /// </summary>
        public int LineBreakNewLineBreaks
        {
            get { return TObject.Options.LineBreakNewLineBreaks; }
            set { TObject.Options.LineBreakNewLineBreaks = value; }
        }

        /// <summary>
        /// Specifies the DataFields used for the Renko chart. 
        /// The default value is DataFields.Close.
        /// Note:Renko doesn't support HighLow.
        /// </summary>
        public DataFields RenkoFields
        {
            get
            {
                return TObject.Options.RenkoFields;
            }
            set
            {
                if (value != DataFields.HighLow)
                {
                    TObject.Options.RenkoFields = value;
                }
                else
                {
                    throw new ArgumentException("Renko doesn't support HighLow.");
                }
            }
        }

        /// <summary>
        /// Specifies the RangeMode for the Renko chart. 
        /// The default value is RangeMode.Fixed.
        /// Note:Renko doesn't support Percentage.
        /// </summary>
        public RangeMode RenkoRangeMode
        {
            get
            {
                return TObject.Options.RenkoRangeMode;
            }
            set
            {
                if (value != RangeMode.Percentage)
                {
                    TObject.Options.RenkoRangeMode = value;
                }
                else
                {
                    throw new ArgumentException("Renko doesn't support Percentage.");
                }
            }
        }

        /// <summary>
        /// Specifies the box size for the Renko chart. 
        /// The default value is 14.
        /// </summary>
        public int RenkoBoxSize
        {
            get { return TObject.Options.RenkoBoxSize; }
            set { TObject.Options.RenkoBoxSize = value; }
        }

        /// <summary>
        /// Specifies the DataFields used for the point and figure chart.
        /// The default is Close. Only the Close and HighLow are supported.
        /// </summary>
        public DataFields PointAndFigureFields
        {
            get { return TObject.Options.PointAndFigureFields; }
            set
            {
                if (value == DataFields.Close || value == DataFields.HighLow)
                {
                    TObject.Options.PointAndFigureFields = value;
                }
                else
                {
                    throw new ArgumentException("PointAndFigure only supports Close and HighLow.");
                }
            }
        }

        /// <summary>
        /// Specifies the reversal amount for the point and figure chart. 
        /// The default is 3.
        /// </summary>
        public int PointAndFigureReversal
        {
            get { return TObject.Options.PointAndFigureReversal; }
            set { TObject.Options.PointAndFigureReversal = value; }
        }

        /// <summary>
        /// Specifies the scaling mode for point and figure chart.
        /// The default is Traditional.
        /// </summary>
        public PointAndFigureScaling PointAndFigureScaling
        {
            get { return TObject.Options.PointAndFigureScaling; }
            set { TObject.Options.PointAndFigureScaling = value; }
        }

        /// <summary>
        /// Specifies the box size for the point and figure chart. 
        /// The default is 1.
        /// </summary>
        public int PointAndFigureBoxSize
        {
            get { return TObject.Options.PointAndFigureBoxSize; }
            set { TObject.Options.PointAndFigureBoxSize = value; }
        }

        /// <summary>
        /// Specifies the ATR period of the point and figure chart.
        /// The default is 20. Must be an integer less than the length of the data and greater than one.
        /// </summary>
        public int PointAndFigurePeriod
        {
            get { return TObject.Options.PointAndFigurePeriod; }
            set { TObject.Options.PointAndFigurePeriod = value; }
        }
    }
}
