﻿namespace C1.Web.Mvc.Finance.TagHelpers
{
    public partial class FinancialSeriesTagHelper
    {
        /// <summary>
        /// Configurates <see cref="FinancialSeries{T}.ChartType" />.
        /// Sets the type of financial chart to create.
        /// </summary>
        public Chart.ChartType ChartType
        {
            get { return InnerHelper.GetPropertyValue<Chart.ChartType>("ChartType"); }
            set { InnerHelper.SetPropertyValue("ChartType", value); }
        }
    }
}
