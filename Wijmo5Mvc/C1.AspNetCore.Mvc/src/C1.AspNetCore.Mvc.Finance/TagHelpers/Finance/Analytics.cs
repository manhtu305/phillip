﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Finance.TagHelpers
{
    [RestrictChildren("c1-flex-chart-line-style")]
    public partial class StochasticTagHelper
    {
    }

    [RestrictChildren("c1-flex-chart-line-style")]
    public partial class MacdTagHelper
    {
    }
}
