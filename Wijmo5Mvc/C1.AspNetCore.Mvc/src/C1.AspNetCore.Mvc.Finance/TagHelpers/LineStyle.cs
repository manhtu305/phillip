﻿using System.Reflection;
using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Finance.TagHelpers
{
    /// <summary>
    /// <see cref="ITagHelper"/> implementation for the style setting.
    /// </summary>
    [HtmlTargetElement("c1-flex-chart-line-style")]
    public class LineStyleTagHelper: SettingTagHelper<SVGStyle>
    {
        #region Properties
        /// <summary>
        /// A value that indicates the stroke color.
        /// </summary>
        public string Stroke
        {
            get { return InnerHelper.GetPropertyValue<string>("Stroke"); }
            set { InnerHelper.SetPropertyValue("Stroke", value); }
        }
        
        /// <summary>
        /// A value that indicates the stroke width.
        /// </summary>
        public int? StrokeWidth
        {
            get { return InnerHelper.GetPropertyValue<int?>("StrokeWidth"); }
            set { InnerHelper.SetPropertyValue("StrokeWidth", value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Processes the child content.
        /// </summary>
        /// <param name="parent">The information from the parent taghelper.</param>
        /// <param name="childContent">The data which stands for child content.</param>
        protected override void ProcessChildContent(object parent, TagHelperContent childContent)
        {
            var piStyles = parent.GetType().GetProperty("Styles");
            var styles = piStyles.GetValue(parent) as IDictionary<string, SVGStyle>;
            if (styles == null)
            {
                styles = new Dictionary<string, SVGStyle>();
                piStyles.SetValue(parent, styles);
            }
            styles.Add(C1Property, TObject);
        }

        /// <summary>
        /// Overrides to create the instance without considering c1-property,
        /// because c1-property is used as a key in the dictionary instead of a sub-property name.
        /// Processes all attributes of current taghelper.
        /// </summary>
        /// <param name="context">Contains information associated with the current HTML tag.</param>
        /// <param name="parent">The parrent control instance.</param>
        protected override void ProcessAttributes(TagHelperContext context, object parent)
        {
            TObject = InnerHelper.GetInstance(parent, GetObjectInstance, C1Property);
        }
        #endregion Methods
    }
}
