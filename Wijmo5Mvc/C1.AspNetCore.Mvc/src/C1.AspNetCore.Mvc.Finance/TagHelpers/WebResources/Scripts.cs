﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace C1.Web.Mvc.Finance.TagHelpers
{
    /// <summary>
    /// Extends <see cref="Mvc.TagHelpers.ScriptsTagHelper"/> for FinancialChart control scripts.
    /// </summary>
    [RestrictChildren("c1-finance-scripts")]
    [HtmlTargetElement("c1-scripts")]
    public class ScriptsTagHelper : Mvc.TagHelpers.ScriptsTagHelper
    {
    }
}
