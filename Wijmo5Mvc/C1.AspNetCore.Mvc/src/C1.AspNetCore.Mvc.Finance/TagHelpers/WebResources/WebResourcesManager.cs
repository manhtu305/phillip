﻿using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.ComponentModel;

namespace C1.Web.Mvc.Finance.TagHelpers
{
    /// <summary>
    /// Defines a <see cref="Microsoft.AspNetCore.Razor.TagHelpers.TagHelper"/> to manage FinancialChart resources.
    /// </summary>
    [HtmlTargetElement("c1-finance-resources")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use <c1-scripts> instead.")]
    public class FinanceWebResourcesManagerTagHelper : WebResourcesManagerBaseTagHelper<FinanceWebResourcesManager>
    {
    }
}