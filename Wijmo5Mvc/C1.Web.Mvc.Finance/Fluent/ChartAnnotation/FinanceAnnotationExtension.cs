﻿using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Finance.Fluent
{
    /// <summary>
    /// Define a static class to add the AnnotationLayer extension methods for FinancialChart.
    /// </summary>
    public static class FinanceAnnotationExtension
    {
        /// <summary>
        /// Apply the AnnotationLayer extender in FinancialChart.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="financialChartBuilder">the specified financialChart builder.</param>
        /// <param name="annotationLayerBuilder">the specified annotationlayer builder</param>
        /// <returns></returns>
        public static FinancialChartBuilder<T> AddAnnotationLayer<T>(this FinancialChartBuilder<T> financialChartBuilder, Action<AnnotationLayerBuilder<T>> annotationLayerBuilder)
        {
            FinancialChart<T> financialChart = financialChartBuilder.Object;
            AnnotationLayer<T> annotationLayer = new AnnotationLayer<T>(financialChart);
            annotationLayerBuilder(new AnnotationLayerBuilder<T>(annotationLayer));
            financialChart.Extenders.Add(annotationLayer);
            return financialChartBuilder;
        }
    }
}
