﻿using C1.Web.Mvc.Fluent;
using System;

namespace C1.Web.Mvc.Finance.Fluent
{
    /// <summary>
    /// Provide extension methods for FinancialChart.
    /// </summary>
    public static class FinanceRangeSelectorExtension
    {
        /// <summary>
        /// Apply the RangeSelector extender in FinancialChart.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="financialChartBuilder">The specified FinancialchartBuilder.</param>
        /// <param name="rangeSelectorBuilder">The specified RangeselectorBuilder</param>
        /// <returns>Current builder</returns>
        public static FinancialChartBuilder<T> AddRangeSelector<T>(this FinancialChartBuilder<T> financialChartBuilder, Action<RangeSelectorBuilder<T>> rangeSelectorBuilder)
        {
            FinancialChart<T> financialChart = financialChartBuilder.Object;
            RangeSelector<T> rangeSelector = new RangeSelector<T>(financialChart);
            if (rangeSelectorBuilder != null)
            {
                rangeSelectorBuilder(new RangeSelectorBuilder<T>(rangeSelector));
            }
            financialChart.Extenders.Add(rangeSelector);
            return financialChartBuilder;
        }

        /// <summary>
        /// Apply a default RangeSelector extender in FinancialChart.
        /// </summary>
        /// <param name="financialChartBuilder">The specified FinancialchartBuilder.</param>
        /// <returns>Current builder</returns>
        public static FinancialChartBuilder<T> AddRangeSelector<T>(this FinancialChartBuilder<T> financialChartBuilder)
        {
            return AddRangeSelector(financialChartBuilder, null);
        }
    }
}