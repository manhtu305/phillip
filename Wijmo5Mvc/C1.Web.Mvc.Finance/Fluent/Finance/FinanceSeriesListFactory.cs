﻿using C1.Web.Mvc.Chart;
using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace C1.Web.Mvc.Finance.Fluent
{
    /// <summary>
    /// Define a factory to create different series.
    /// </summary>
    /// <typeparam name="T">Model type</typeparam>
    /// <typeparam name="TOwner">Chart derived from FlexChartCore</typeparam>
    /// <typeparam name="TSeries">ChartSeries derived from ChartSeriesBase</typeparam>
    /// <typeparam name="TSeriesBuilder">ChartSeriesBuilder</typeparam>
    /// <typeparam name="TChartType">ChartType</typeparam>
    public class FinanceSeriesListFactory<T, TOwner, TSeries, TSeriesBuilder, TChartType> : SeriesListFactory<T, TOwner, TSeries, TSeriesBuilder, TChartType>
        where TOwner : FlexChartCore<T>
        where TSeries : ChartSeriesBase<T>
        where TSeriesBuilder : ChartSeriesBaseBuilder<T, TSeries, TSeriesBuilder>
    {
        /// <summary>
        /// Initializes a new instance of the FinanceSeriesListFactory class.
        /// </summary>
        /// <param name="list">ChartSeries list</param>
        /// <param name="owner">FinancialChart</param>
        public FinanceSeriesListFactory(IList<ChartSeriesBase<T>> list, TOwner owner)
            : base(list,owner)
        {
        }

        #region Fibonacci

        /// <summary>
        /// Add a Fibonacci Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public FibonacciBuilder<T> AddFibonacci()
        {
            return Add<Fibonacci<T>, FibonacciBuilder<T>>
                (
                    (_owner) => (new Fibonacci<T>(_owner)),
                    (item) => (new FibonacciBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a Fibonacci Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public FibonacciBuilder<T> AddFibonacci(string name)
        {
            FibonacciBuilder<T> builder = AddFibonacci();
            builder.Name(name);
            return builder;
        }

        #endregion Fibonacci

        #region FibonacciArcs

        /// <summary>
        /// Add a FibonacciArcs Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public FibonacciArcsBuilder<T> AddFibonacciArcs()
        {
            return Add<FibonacciArcs<T>, FibonacciArcsBuilder<T>>
                (
                    (_owner) => (new FibonacciArcs<T>(_owner)),
                    (item) => (new FibonacciArcsBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a FibonacciArcs Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public FibonacciArcsBuilder<T> AddFibonacciArcs(string name)
        {
            FibonacciArcsBuilder<T> builder = AddFibonacciArcs();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region FibonacciFans

        /// <summary>
        /// Add a FibonacciFans Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public FibonacciFansBuilder<T> AddFibonacciFans()
        {
            return Add<FibonacciFans<T>, FibonacciFansBuilder<T>>
                (
                    (_owner) => (new FibonacciFans<T>(_owner)),
                    (item) => (new FibonacciFansBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a FibonacciFans Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public FibonacciFansBuilder<T> AddFibonacciFans(string name)
        {
            FibonacciFansBuilder<T> builder = AddFibonacciFans();
            builder.Name(name);
            return builder;
        }

        #endregion FibonacciFans

        #region FibonacciTimeZones

        /// <summary>
        /// Add a FibonacciTimeZones Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public FibonacciTimeZonesBuilder<T> AddFibonacciTimeZones()
        {
            return Add<FibonacciTimeZones<T>, FibonacciTimeZonesBuilder<T>>
                (
                    (_owner) => (new FibonacciTimeZones<T>(_owner)),
                    (item) => (new FibonacciTimeZonesBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a FibonacciTimeZones Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public FibonacciTimeZonesBuilder<T> AddFibonacciTimeZones(string name)
        {
            FibonacciTimeZonesBuilder<T> builder = AddFibonacciTimeZones();
            builder.Name(name);
            return builder;
        }

        #endregion FibonacciTimeZones

        #region BollingerBands

        /// <summary>
        /// Add a BollingerBands Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public BollingerBandsBuilder<T> AddBollingerBands()
        {
            return Add<BollingerBands<T>, BollingerBandsBuilder<T>>
                (
                    (_owner) => (new BollingerBands<T>(_owner)),
                    (item) => (new BollingerBandsBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a BollingerBands Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public BollingerBandsBuilder<T> AddBollingerBands(string name)
        {
            BollingerBandsBuilder<T> builder = AddBollingerBands();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region Envelopes

        /// <summary>
        /// Add an Envelopes Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public EnvelopesBuilder<T> AddEnvelopes()
        {
            return Add<Envelopes<T>, EnvelopesBuilder<T>>
                (
                    (_owner) => (new Envelopes<T>(_owner)),
                    (item) => (new EnvelopesBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add an Envelopes Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public EnvelopesBuilder<T> AddEnvelopes(string name)
        {
            EnvelopesBuilder<T> builder = AddEnvelopes();
            builder.Name(name);
            return builder;
        }


        #endregion Envelopes

        #region RSI

        /// <summary>
        /// Add a RSI Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public RSIBuilder<T> AddRSI()
        {
            return Add<RSI<T>, RSIBuilder<T>>
                (
                    (_owner) => (new RSI<T>(_owner)),
                    (item) => (new RSIBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a RSI Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public RSIBuilder<T> AddRSI(string name)
        {
            RSIBuilder<T> builder = AddRSI();
            builder.Name(name);
            return builder;
        }

        #endregion RSI

        #region WilliamsR

        /// <summary>
        /// Add a WilliamsR Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public WilliamsRBuilder<T> AddWilliamsR()
        {
            return Add<WilliamsR<T>, WilliamsRBuilder<T>>
                (
                    (_owner) => (new WilliamsR<T>(_owner)),
                    (item) => (new WilliamsRBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a WilliamsR Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public WilliamsRBuilder<T> AddWilliamsR(string name)
        {
            WilliamsRBuilder<T> builder = AddWilliamsR();
            builder.Name(name);
            return builder;
        }

        #endregion WilliamsR

        #region ATR

        /// <summary>
        /// Add an ATR Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public ATRBuilder<T> AddATR()
        {
            return Add<ATR<T>, ATRBuilder<T>>
                (
                    (_owner) => (new ATR<T>(_owner)),
                    (item) => (new ATRBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add an ATR Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public ATRBuilder<T> AddATR(string name)
        {
            ATRBuilder<T> builder = AddATR();
            builder.Name(name);
            return builder;
        }

        #endregion ATR

        #region CCI

        /// <summary>
        /// Add an CCI Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public CCIBuilder<T> AddCCI()
        {
            return Add<CCI<T>, CCIBuilder<T>>
                (
                    (_owner) => (new CCI<T>(_owner)),
                    (item) => (new CCIBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add an CCI Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public CCIBuilder<T> AddCCI(string name)
        {
            CCIBuilder<T> builder = AddCCI();
            builder.Name(name);
            return builder;
        }

        #endregion CCI

        #region Stochastic

        /// <summary>
        /// Add a Stochastic Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public StochasticBuilder<T> AddStochastic()
        {
            return Add<Stochastic<T>, StochasticBuilder<T>>
                (
                    (_owner) => (new Stochastic<T>(_owner)),
                    (item) => (new StochasticBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a Stochastic Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public StochasticBuilder<T> AddStochastic(string name)
        {
            StochasticBuilder<T> builder = AddStochastic();
            builder.Name(name);
            return builder;
        }

        #endregion Stochastic

        #region Macd

        /// <summary>
        /// Add a Macd Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public MacdBuilder<T> AddMacd()
        {
            return Add<Macd<T>, MacdBuilder<T>>
                (
                    (_owner) => (new Macd<T>(_owner)),
                    (item) => (new MacdBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a Macd Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public MacdBuilder<T> AddMacd(string name)
        {
            MacdBuilder<T> builder = AddMacd();
            builder.Name(name);
            return builder;
        }

        #endregion Macd

        #region MacdHistogram

        /// <summary>
        /// Add a MacdHistogram Series to the FinancialChart series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public MacdHistogramBuilder<T> AddMacdHistogram()
        {
            return Add<MacdHistogram<T>, MacdHistogramBuilder<T>>
                (
                    (_owner) => (new MacdHistogram<T>(_owner)),
                    (item) => (new MacdHistogramBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a MacdHistogram Series to the FinancialChart series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public MacdHistogramBuilder<T> AddMacdHistogram(string name)
        {
            MacdHistogramBuilder<T> builder = AddMacdHistogram();
            builder.Name(name);
            return builder;
        }

        #endregion MacdHistogram

    }
}
