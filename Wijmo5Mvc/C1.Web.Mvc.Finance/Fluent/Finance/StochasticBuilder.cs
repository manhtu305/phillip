﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Finance.Fluent
{
    public partial class StochasticBuilder<T>
    {
        /// <summary>
        /// Sets the k line style.
        /// </summary>
        /// <param name="stroke">The stroke</param>
        /// <param name="strokeWidth">The stroke width</param>
        /// <returns>Current builder</returns>
        public StochasticBuilder<T> SetKLineStyle(string stroke = "", int? strokeWidth = null)
        {
            UpdateStyles("kLine", stroke, strokeWidth);
            return this;
        }

        /// <summary>
        /// Sets the d line style.
        /// </summary>
        /// <param name="stroke">The stroke</param>
        /// <param name="strokeWidth">The stroke width</param>
        /// <returns>Current builder</returns>
        public StochasticBuilder<T> SetDLineStyle(string stroke = "", int? strokeWidth = null)
        {
            UpdateStyles("dLine", stroke, strokeWidth);
            return this;
        }

        private void UpdateStyles(string name, string stroke, int? strokeWidth)
        {
            if (Object.Styles == null)
            {
                Object.Styles = new Dictionary<string, SVGStyle>();
            }
            UpdateLineStyle(Object.Styles, name, stroke, strokeWidth);
        }

        internal static void UpdateLineStyle(IDictionary<string, SVGStyle> styles, string styleName, string stroke, int? strokeWidth)
        {
            SVGStyle style;
            if (!styles.TryGetValue(styleName, out style))
            {
                style = new SVGStyle();
                styles.Add(styleName, style);
            }
            style.Stroke = stroke;
            style.StrokeWidth = strokeWidth;
        }
    }
}
