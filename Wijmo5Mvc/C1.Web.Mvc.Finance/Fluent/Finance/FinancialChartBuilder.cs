﻿using System;
using System.ComponentModel;

namespace C1.Web.Mvc.Finance.Fluent
{
    /// <summary>
    /// The builder for FinancialChart control.
    /// </summary>
    public partial class FinancialChartBuilder<T>
    {
        /// <summary>
        /// Sets the Options property.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public FinancialChartBuilder<T> Options(Action<FinancialExtraOptionsBuilder> build)
        {
            build(new FinancialExtraOptionsBuilder(Object.Options));
            return this;
        }

        /// <summary>
        /// Set the FlexChart series.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public FinancialChartBuilder<T> Series(Action<FinanceSeriesListFactory<T, FinancialChart<T>, FinancialSeries<T>, FinancialSeriesBuilder<T>, ChartType>> build)
        {
            build(new FinanceSeriesListFactory<T, FinancialChart<T>, FinancialSeries<T>, FinancialSeriesBuilder<T>, ChartType>(Object.Series, Object));
            return this;
        }

        /// <summary>
        /// Add an extender.
        /// </summary>
        /// <remarks>
        /// This is no longer used, just remove it.  Please use specific extender instead.
        /// </remarks>
        /// <param name="extender">The specified extender.</param>
        /// <returns></returns>
        [Obsolete("This is no longer used, just remove it.  Please use specific extender instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public FinancialChartBuilder<T> AddExtender(Extender extender)
        {
            Object.Extenders.Add(extender);
            return this;
        }
    }
}
