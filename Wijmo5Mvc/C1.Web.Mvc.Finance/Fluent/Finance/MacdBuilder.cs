﻿using System.Collections.Generic;
namespace C1.Web.Mvc.Finance.Fluent
{
    public partial class MacdBuilder<T>
    {
        /// <summary>
        /// Sets the macd line style.
        /// </summary>
        /// <param name="stroke">The stroke</param>
        /// <param name="strokeWidth">The stroke width</param>
        /// <returns>Current builder</returns>
        public MacdBuilder<T> SetMacdLineStyle(string stroke = "", int? strokeWidth = null)
        {
            UpdateStyles("macdLine", stroke, strokeWidth);
            return this;
        }

        /// <summary>
        /// Sets the signal line style.
        /// </summary>
        /// <param name="stroke">The stroke</param>
        /// <param name="strokeWidth">The stroke width</param>
        /// <returns>Current builder</returns>
        public MacdBuilder<T> SetSignalLineStyle(string stroke = "", int? strokeWidth = null)
        {
            UpdateStyles("signalLine", stroke, strokeWidth);
            return this;
        }

        private void UpdateStyles(string name, string stroke, int? strokeWidth)
        {
            if (Object.Styles == null)
            {
                Object.Styles = new Dictionary<string, SVGStyle>();
            }
            StochasticBuilder<T>.UpdateLineStyle(Object.Styles, name, stroke, strokeWidth);
        }
    }
}
