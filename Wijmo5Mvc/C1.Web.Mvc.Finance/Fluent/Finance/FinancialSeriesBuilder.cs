﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Finance.Fluent
{
    public partial class FinancialSeriesBuilder<T>
    {
        /// <summary>
        /// Sets the ChartType property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the type of financial chart to create.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public FinancialSeriesBuilder<T> ChartType(ChartType value)
        {
            Object.ChartType = value;
            return this;
        }
    }
}
