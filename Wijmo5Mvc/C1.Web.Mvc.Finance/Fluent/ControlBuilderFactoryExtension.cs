﻿using C1.Web.Mvc.Fluent;
using System;
using System.ComponentModel;
#if ASPNETCORE
using IHtmlString = Microsoft.AspNetCore.Html.IHtmlContent;
#else
using System.Web;
#endif

namespace C1.Web.Mvc.Finance.Fluent
{
    /// <summary>
    /// Extends ControlBuilderFactory for FinancialChart related controls creation.
    /// </summary>
    public static class ControlBuilderFactoryExtension
    {
        /// <summary>
        /// Create a FinancialChartBuilder.
        /// </summary>
        /// <typeparam name="T">The data item type</typeparam>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory</param>
        /// <param name="selector">The selector</param>
        /// <returns>The FinancialChartBuilder</returns>
        public static FinancialChartBuilder<T> FinancialChart<T>(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new FinancialChartBuilder<T>(new FinancialChart<T>(controlBuilderFactory._helper, selector));
        }

        /// <summary>
        /// Create a FinancialChartBuilder.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory</param>
        /// <param name="selector">The selector</param>
        /// <returns>The builder</returns>
        public static FinancialChartBuilder<object> FinancialChart(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return controlBuilderFactory.FinancialChart<object>(selector);
        }

        /// <summary>
        /// Render the finance related js resources.
        /// </summary>
        /// <returns>The html string</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please use Scripts method instead.")]
        public static IHtmlString FinanceResources(this ControlBuilderFactory controlBuilderFactory)
        {
            var c1Resources = new FinanceWebResourcesManager(controlBuilderFactory._helper);
            return c1Resources;
        }
    }
}