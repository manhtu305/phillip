﻿using C1.Web.Mvc.Fluent;
using System;

namespace C1.Web.Mvc.Finance.Fluent
{
    /// <summary>
    /// Provide extension methods for FinancialChart.
    /// </summary>
    public static class FinanceLineMarkerExtension
    {
        /// <summary>
        /// Apply the LineMarker extender in FinancialChart.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="financialChartBuilder">The specified FinancialchartBuilder.</param>
        /// <param name="lineMarkerBuilder">The specified LinemarkerBuilder</param>
        /// <returns>Current builder</returns>
        public static FinancialChartBuilder<T> AddLineMarker<T>(this FinancialChartBuilder<T> financialChartBuilder, Action<LineMarkerBuilder<T>> lineMarkerBuilder)
        {
            FinancialChart<T> financialChart = financialChartBuilder.Object;
            LineMarker<T> lineMarker = new LineMarker<T>(financialChart);
            if (lineMarkerBuilder != null)
            {
                lineMarkerBuilder(new LineMarkerBuilder<T>(lineMarker));
            }
            financialChart.Extenders.Add(lineMarker);
            return financialChartBuilder;
        }

        /// <summary>
        /// Apply a default LineMarker extender in FinancialChart.
        /// </summary>
        /// <param name="financialChartBuilder">The specified FinancialchartBuilder.</param>
        /// <returns>Current builder</returns>
        public static FinancialChartBuilder<T> AddLineMarker<T>(this FinancialChartBuilder<T> financialChartBuilder)
        {
            return AddLineMarker(financialChartBuilder, null);
        }
    }
}