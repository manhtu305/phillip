﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.Finance.Fluent
{
    /// <summary>
    /// Extends <see cref="ScriptsBuilder"/> class for FinancialChart control.
    /// </summary>
    public static class ScriptsBuilderExtension
    {
        /// <summary>
        /// Registers the finance related script bundle. This bundle contains FinancialChart control.
        /// </summary>
        /// <param name="scriptsBuilder">The <see cref="ScriptsBuilder"/>.</param>
        /// <returns>The <see cref="ScriptsBuilder"/>.</returns>
        public static ScriptsBuilder Finance(this ScriptsBuilder scriptsBuilder)
        {
            scriptsBuilder.OwnerTypes.AddRange(FinanceWebResourcesHelper.AllScriptOwnerTypes.Value);
            return scriptsBuilder;
        }
    }
}
