﻿namespace C1.Web.Mvc.Finance
{
    public partial class Fibonacci<T>
    {
        /// <summary>
        /// Creates one <see cref="Fibonacci{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public Fibonacci(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "Fibonacci"; }
        }
    }

    public partial class FibonacciArcs<T>
    {
        /// <summary>
        /// Creates one <see cref="FibonacciArcs{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public FibonacciArcs(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "FibonacciArcs"; }
        }
    }

    public partial class FibonacciFans<T>
    {
        /// <summary>
        /// Creates one <see cref="FibonacciFans{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public FibonacciFans(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "FibonacciFans"; }
        }
    }

    public partial class FibonacciTimeZones<T>
    {
        /// <summary>
        /// Creates one <see cref="FibonacciTimeZones{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public FibonacciTimeZones(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "FibonacciTimeZones"; }
        }
    }

    public partial class BollingerBands<T>
    {
        /// <summary>
        /// Creates one <see cref="BollingerBands{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public BollingerBands(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "BollingerBands"; }
        }
    }

    public partial class Envelopes<T>
    {
        /// <summary>
        /// Creates one <see cref="Envelopes{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public Envelopes(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "Envelopes"; }
        }
    }

    public abstract partial class SingleOverlayIndicatorBase<T>
    {
        internal SingleOverlayIndicatorBase(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }
    }

    public partial class RSI<T>
    {
        /// <summary>
        /// Creates one <see cref="RSI{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public RSI(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "RSI"; }
        }
    }

    public partial class WilliamsR<T>
    {
        /// <summary>
        /// Creates one <see cref="WilliamsR{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public WilliamsR(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "WilliamsR"; }
        }
    }

    public partial class ATR<T>
    {
        /// <summary>
        /// Creates one <see cref="ATR{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public ATR(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "ATR"; }
        }
    }

    public partial class CCI<T>
    {
        /// <summary>
        /// Creates one <see cref="CCI{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public CCI(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "CCI"; }
        }
    }

    public partial class Stochastic<T>
    {
        /// <summary>
        /// Creates one <see cref="Stochastic{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public Stochastic(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "Stochastic"; }
        }
    }

    public abstract partial class MacdBase<T>
    {
        internal MacdBase(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }
    }

    public partial class Macd<T>
    {
        /// <summary>
        /// Creates one <see cref="Macd{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public Macd(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "Macd"; }
        }
    }

    public partial class MacdHistogram<T>
    {
        /// <summary>
        /// Creates one <see cref="MacdHistogram{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be derived from FlexChartCore</param>
        public MacdHistogram(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Name of this Series.
        /// </summary>
        public override string ExtraSeriesTypeName
        {
            get { return "MacdHistogram"; }
        }
    }
}
