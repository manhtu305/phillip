﻿namespace C1.Web.Mvc.Finance
{
    /// <summary>
    /// Represents the moving average type for the envelopes.
    /// </summary>
    public enum MovingAverageType
    {
        /// <summary>
        /// The simple moving average is simply the average of the last n values.
        /// </summary>
        Simple,
        /// <summary>
        /// The exponential moving average is a weighted average of the last n values, where the weighting decreases exponentially with each previous value.
        /// </summary> 
        Exponential
    }
}
