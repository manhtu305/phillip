﻿namespace C1.Web.Mvc.Finance
{
    public partial class FinancialSeries<T>
    {
        /// <summary>
        /// Creates one <see cref="FinancialSeries{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner should be FinancialChart.</param>
        public FinancialSeries(FinancialChart<T> owner)
            : base(owner)
        {
            Initialize();
        }
    }
}
