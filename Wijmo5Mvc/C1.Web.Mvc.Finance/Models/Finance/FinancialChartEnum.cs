﻿namespace C1.Web.Mvc.Finance
{
    /// <summary>
    /// Specifies the type of financial chart.
    /// </summary>
    public enum ChartType
    {
        /// <summary>
        /// Shows vertical bars and allows you to compare values of items across categories.
        /// </summary>
        Column,
        /// <summary>
        /// Uses X and Y coordinates to show patterns within the data.
        /// </summary>
        Scatter,
        /// <summary>
        /// Shows trends over a period of time or across categories.
        /// </summary>
        Line,
        /// <summary>
        ///  Shows line chart with a symbol on each data point.
        /// </summary>
        LineSymbols,
        /// <summary>
        /// Shows line chart with area below the line filled with color. 
        /// </summary>
        Area,
        /// <summary>
        /// Presents items with high, low, open, and close values.
        /// The size of the wick line is determined by the High and Low values, while
        /// the size of the bar is determined by the Open and Close values. The bar is 
        /// displayed using different colors, depending on whether the close value is 
        /// higher or lower than the open value.
        /// </summary>
        Candlestick,
        /// <summary>
        /// Displays the same information as a candlestick chart, except that opening
        /// values are displayed using lines to the left, while lines to the right 
        /// indicate closing values.
        /// </summary>
        HighLowOpenClose,
        /// <summary>
        /// Derived from the candlestick chart and uses information from the current and
        /// prior period in order to filter out the noise.
        /// These charts cannot be combined with any other series objects.
        /// </summary>
        HeikinAshi,
        /// <summary>
        /// Filters out noise by focusing exclusively on price changes. 
        /// These charts cannot be combined with any other series objects. 
        /// </summary>
        LineBreak,
        /// <summary>
        /// Ignores time and focuses on price changes that meet a specified amount. 
        /// These charts cannot be combined with any other series objects. 
        /// </summary>
        Renko,
        /// <summary>
        /// Ignores time and focuses on price action. These charts cannot be combined with any other series objects. 
        /// </summary>
        Kagi,
        /// <summary>
        /// Identical to the standard Column chart, except that the width of each bar is determined by the Volume value.
        /// </summary>
        ColumnVolume,
        /// <summary>
        /// Similar to the Candlestick chart, but shows the high and low values only. 
        /// In addition, the width of each bar is determined by Volume value. 
        /// </summary>
        EquiVolume,
        /// <summary>
        /// Identical to the standard Candlestick chart, except that the width of each bar is determined by Volume value. 
        /// </summary>
        CandleVolume,
        /// <summary>
        /// Created by Richard Arms, this chart is a combination of EquiVolume and CandleVolume chart types.
        /// </summary>
        ArmsCandleVolume,
        /// <summary>
        /// Point and figure financial chart.
        /// </summary>
        PointAndFigure
    }

    /// <summary>
    /// Specifies which fields are to be used for calculation. Applies to Renko and Kagi chart types.
    /// </summary>
    public enum DataFields
    {
        /// <summary>
        ///  Close values are used for calculations.
        /// </summary>
        Close,
        /// <summary>
        /// High values are used for calculations.
        /// </summary>
        High,
        /// <summary>
        ///  Low values are used for calculations.
        /// </summary>
        Low,
        /// <summary>
        ///  Open values are used for calculations. 
        /// </summary>
        Open,
        /// <summary>
        /// High-Low method is used for calculations. DataFields.HighLow is currently not supported with Renko chart types.
        /// </summary>
        HighLow,
        /// <summary>
        /// Average of high and low values is used for calculations.
        /// </summary>
        HL2,
        /// <summary>
        /// Average of high, low, and close values is used for calculations. 
        /// </summary>
        HLC3,
        /// <summary>
        /// Average of high, low, open, and close values is used for calculations. 
        /// </summary>
        HLOC4
    }

    /// <summary>
    /// Specifies the unit for Kagi and Renko chart types.
    /// </summary>
    public enum RangeMode
    {
        /// <summary>
        /// Uses a fixed, positive number for the Kagi chart's reversal amount or Renko chart's box size.
        /// </summary>
        Fixed,
        /// <summary>
        /// Uses the current Average True Range value for Kagi chart's reversal amount
        /// or Renko chart's box size. When ATR is used, the reversal amount or box size
        /// option of these charts must be an integer and will be used as the period for 
        /// the ATR calculation.
        /// </summary>
        ATR,
        /// <summary>
        /// Uses a percentage for the Kagi chart's reversal amount. RangeMode.Percentage
        /// is currently not supported with Renko chart types.
        /// </summary>
        Percentage
    }

    /// <summary>
    /// Specifies the scaling mode for point and figure chart.
    /// </summary>
    public enum PointAndFigureScaling
    {
        /// <summary>
        /// Traditional scaling. The box size is calculated automatically based on price range.
        /// </summary>
        Traditional,
        /// <summary>
        /// Fixed scaling. The box size is defined by boxSize property.
        /// </summary>
        Fixed,
        /// <summary>
        /// Dynamic(ATR) scaling. The box size is calculated based on ATR.
        /// </summary>
        Dynamic,
    }
}
