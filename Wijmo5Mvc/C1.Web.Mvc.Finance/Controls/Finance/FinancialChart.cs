﻿using C1.Web.Mvc.WebResources;
using System;
#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#endif

namespace C1.Web.Mvc.Finance
{
    [Scripts(typeof(WebResources.Definitions.FinancialChart))]
    public partial class FinancialChart<T>
    {

        private const string ClientModuleChartFinance = "chart.finance.";

        internal override string ClientSubModule
        {
            get { return ClientModuleChartFinance; }
        }

        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }
    }
}
