﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.Finance
{
    [Obsolete("Please use Scripts control instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
#if ASPNETCORE
    public
#else
    internal
#endif
    sealed class FinanceWebResourcesManager : WebResourcesManagerBase
    {
        #region Fields
        internal static readonly IList<Type> AllControlTypes = new List<Type>
        {
            typeof(C1FinancialChart)
        };
        #endregion Fields

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the FinanceWebResourcesManager class.
        /// </summary>
        /// <param name="helper">An instance of HtmlHelper</param>
        public FinanceWebResourcesManager(HtmlHelper helper)
            : base(helper)
        {
        }
        #endregion Ctors

        #region Properties
        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        //protected override string WebResourcesControllerName
        //{
        //    get { return "C1WebMvcFinance"; }
        //}
        #endregion Properties

        #region Methods
        internal override IList<Type> GetAllControlTypes()
        {
            return AllControlTypes;
        }
        #endregion Methods

    }
}