﻿using System;
using System.ComponentModel;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.Finance
{
    /// <summary>
    /// Defines C1 FinancialChart related js and css.
    /// </summary>
    [Scripts(typeof(WebResources.Definitions.FinancialChart))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1FinancialChart
    {
    }
}
