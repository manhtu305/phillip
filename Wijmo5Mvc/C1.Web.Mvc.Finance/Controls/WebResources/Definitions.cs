﻿using C1.Web.Mvc.WebResources;

[assembly: AssemblyScripts(typeof(C1.Web.Mvc.Finance.WebResources.Definitions.All))]
[assembly: AssemblyStyles(typeof(C1.Web.Mvc.Finance.WebResources.Definitions.All))]

namespace C1.Web.Mvc.Finance.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Definitions
    {
        [Scripts(typeof(FinancialChart))]
        [Styles(typeof(FinancialChart))]
        public abstract class All { }

        [Scripts(FinanceWebResourcesHelper.WijmoJs + "wijmo.chart.finance",
            FinanceWebResourcesHelper.WijmoJs + "wijmo.chart.finance.analytics",
            FinanceWebResourcesHelper.Shared + "Chart.Finance",
            FinanceWebResourcesHelper.Mvc + "Chart.Finance",
            FinanceWebResourcesHelper.Mvc + "Cast.Finance")]
        public abstract class FinancialChart : Mvc.WebResources.Definitions.Chart { }
    }
}
