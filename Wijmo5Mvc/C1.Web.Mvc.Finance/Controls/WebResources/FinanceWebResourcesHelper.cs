﻿using C1.Web.Mvc.WebResources;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace C1.Web.Mvc.Finance
{
    internal static class FinanceWebResourcesHelper
    {
        private const string FolderSeparator = ".";

#if !ASPNETCORE
        public const string BaseRoot = "C1.Web.Mvc.Finance";
#else
        public const string BaseRoot = "C1.AspNetCore.Mvc.Finance";
#endif

        public const string Root =
           BaseRoot + FolderSeparator + "Client" +
#if DEBUG
           "Debug"
#else
           "Release"
#endif
           + FolderSeparator;

        public const string Shared = Root + "Shared" + FolderSeparator;
        public const string Mvc = Root + "Mvc" + FolderSeparator;
        public const string Wijmo = Root + "Wijmo" + FolderSeparator;
        public const string WijmoJs = Wijmo + "controls" + FolderSeparator;

        public static readonly Lazy<IEnumerable<Type>> AllScriptOwnerTypes = new Lazy<IEnumerable<Type>>(
            () => WebResourcesHelper.GetAssemblyResTypes<AssemblyScriptsAttribute>(typeof(FinanceWebResourcesHelper).Assembly()));
    }
}