﻿using C1.JsonNet;
using C1.Web.Mvc.Localization;
using System;

namespace C1.Web.Mvc.Finance
{
    /// <summary>
    /// This converter only works the properties Start and End in the FibonacciArcs class, 
    /// Start and End in the FibonacciFans class.
    /// Because in client side it only accepts the type of wijmo.chart.DataPoint.
    /// </summary>
    internal class DataPointConvert : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DataPoint);
        }

        protected override void WriteJson(JsonWriter writer, object value)
        {
            DataPoint dpValue = value as DataPoint;
            if (dpValue != null)
            {
                writer.WriteRawValue(string.Format("new wijmo.chart.DataPoint({0},{1})", JsonHelper.SerializeObject(dpValue.X, writer.JsonSetting), JsonHelper.SerializeObject(dpValue.Y, writer.JsonSetting)));
            }
            else
            {
                writer.WriteRawValue("null");
            }
        }

        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            return existedValue as DataPoint;
        }
    }
}
