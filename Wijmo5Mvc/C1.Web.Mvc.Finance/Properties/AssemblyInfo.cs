﻿using System.Runtime.CompilerServices;
using C1.Util.Licensing;
using System.Reflection;
using System.Runtime.InteropServices;

// licensing support
// Studio Ultimate
[assembly: C1ProductInfo("SU", "757CCC59-F365-4325-A676-0674C656B7A2")]
// Studio Enterprise
[assembly: C1ProductInfo("SE", "724e8a91-af12-4a3b-9aeb-ef89612e692e")]
// C1.Web.Mvc.Finance
[assembly: C1ProductInfo("AI", "6EF7D8DF-7ACD-437F-A2BE-57AD58C06465")]

#if !ASPNETCORE
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
#if GRAPECITY
[assembly: AssemblyTitle("ComponentOne Financial Controls for ASP.NET MVC Edition JPN")]
[assembly: AssemblyDescription("ComponentOne Financial Controls for ASP.NET MVC Edition JPN")]
[assembly: AssemblyProduct("ComponentOne Financial Controls for ASP.NET MVC Edition JPN")]
#else
[assembly: AssemblyTitle("ComponentOne Financial Controls for ASP.NET MVC Edition")]
[assembly: AssemblyDescription("ComponentOne Financial Controls for ASP.NET MVC Edition")]
[assembly: AssemblyProduct("ComponentOne Financial Controls for ASP.NET MVC Edition")]
#endif

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GrapeCity, Inc.")]
[assembly: AssemblyCopyright("Copyright © GrapeCity, Inc.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("85271987-7e1e-4ebf-afb2-5dc5b5e2092c")]
#endif

#if UNITTEST
[assembly: InternalsVisibleTo("C1.Web.Mvc.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#endif

[assembly: AssemblyVersion(AssemblyInfo.Version)]
[assembly: AssemblyFileVersion(AssemblyInfo.Version)]