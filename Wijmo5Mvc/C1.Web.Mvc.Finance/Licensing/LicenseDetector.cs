﻿using System.ComponentModel;
using C1.Util.Licensing;

namespace C1.Web.Mvc.Finance
{
    /// <summary>
    /// Define a class for detecting C1.Web.Mvc.Finance license.
    /// </summary>
    [LicenseProvider]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class LicenseDetector : BaseLicenseDetector
    {
        /// <summary>
        /// The constructor.
        /// </summary>
        public LicenseDetector()
        {
        }
    }
}