﻿using System;

namespace C1.Web.Mvc.FlexViewerWizard
{
    public class ReportViewerSettings : ViewerSettingsBase
    {
        private string _ssrsServerUrl = string.Empty;
        private string _arServerUrl = string.Empty;
        private string _activeReportPath = string.Empty;
        
        private string _ssrsReportPath = string.Empty;
        private string _ssrsLoginUserName = string.Empty;
        private string _ssrsLoginPassword = string.Empty;
        private string _ssrsLoginDomain = string.Empty;
        private string _innerReportName = string.Empty;
        private string _outerReportName = string.Empty;

        public ReportViewerSettings(bool currentProjectServiceSupported = false, string wwwroot = null, string documentsFolder = null)
            : base(currentProjectServiceSupported, wwwroot, documentsFolder)
        {
            IsFlexReport = true;
        }

        protected override string DefaultDocumentsFolder
        {
            get
            {
                return "ReportsRoot";
            }
        }

        [ReplacementIgnore]
        public override bool IsValid
        {
            get
            {
                if (CurrentProjectAsService)
                {
                    return IsFlexReport
                        ? (!string.IsNullOrEmpty(OriginInnerDocumentFile) && !string.IsNullOrEmpty(InnerReportName))
                        : (!string.IsNullOrEmpty(SsrsServerUrl) && !string.IsNullOrEmpty(SsrsReportPath));
                }
                else
                {
                    return ViewWithActiveReport
                        ? !string.IsNullOrEmpty(ActiveReportServerUrl) && !string.IsNullOrEmpty(ActiveReportPath)
                        : !string.IsNullOrEmpty(ServerUrl) && !string.IsNullOrEmpty(OuterReportPath);
                }
            }
        }

        internal bool IsFlexReport { get; set; }

        internal bool IsActiveReport { get; set; }

        [ReplacementIgnore]
        public bool CurrentProjectAsServiceWithFlexReport
        {
            get { return CurrentProjectAsService && IsFlexReport; }
            set
            {
                if (value) CurrentProjectAsService = true;
                IsFlexReport = value;
            }
        }

        [ReplacementIgnore]
        public bool CurrentProjectAsServiceWithSsrsReport
        {
            get { return CurrentProjectAsService && !IsFlexReport; }
            set
            {
                if (value) CurrentProjectAsService = true;
                IsFlexReport = !value;
            }
        }

        [ReplacementIgnore]
        public bool ViewWithC1WebApi
        {
            get { return !CurrentProjectAsService && !IsActiveReport;  }
            set
            {
                if (value)
                {
                    CurrentProjectAsService = false;
                }
                IsActiveReport = !value;
            }
        }

        [ReplacementIgnore]
        public bool ViewWithActiveReport
        {
            get { return !CurrentProjectAsService && IsActiveReport;  }
            set
            {
                if (value)
                {
                    CurrentProjectAsService = false;
                }
                IsActiveReport = value;
            }
        }

        [ReplacementIgnore]
        public override bool ShouldAddDocuemntFile
        {
            get
            {
                return base.ShouldAddDocuemntFile && IsFlexReport;
            }
        }

        [ReplacementIgnore]
        public override bool ShouldAddDiskStorage
        {
            get { return CurrentProjectAsServiceWithFlexReport; }
        }

        [ReplacementIgnore]
        public string SsrsServerUrl
        {
            get { return _ssrsServerUrl; }
            set { _ssrsServerUrl = value; }
        }

        [ReplacementIgnore]
        public string ActiveReportServerUrl
        {
            get { return _arServerUrl; }
            set { _arServerUrl = value; }
        }

        [ReplacementIgnore]
        public string ActiveReportPath
        {
            get { return _activeReportPath; }
            set { _activeReportPath = value; }
        }

        [ReplacementIgnore]
        public string SsrsReportPath
        {
            get { return _ssrsReportPath; }
            set { _ssrsReportPath = value; }
        }

        /// <summary>
        /// The provider key which registers the SSRS server. 
        /// It's determined and set on updating the Startup file.
        /// </summary>
        [ReplacementIgnore]
        public string SsrsProviderKey { get; set; }

        private string SsrsDocumentPath
        {
            get
            {
                if (string.IsNullOrEmpty(SsrsProviderKey)) return SsrsReportPath.TrimStart('/');

                var path = SsrsProviderKey;
                if (!SsrsReportPath.StartsWith("/")) path += "/";
                path += SsrsReportPath;
                return path;
            }
        }

        [ReplacementIgnore]
        public string SsrsLoginUserName
        {
            get { return _ssrsLoginUserName; }
            set { _ssrsLoginUserName = value; }
        }

        [ReplacementIgnore]
        public string SsrsLoginPassword
        {
            get { return _ssrsLoginPassword; }
            set { _ssrsLoginPassword = value; }
        }

        [ReplacementIgnore]
        public string SsrsLoginDomain
        {
            get { return _ssrsLoginDomain; }
            set { _ssrsLoginDomain = value; }
        }

        [ReplacementIgnore]
        public bool HasSsrsCredential
        {
            get {
                return !string.IsNullOrEmpty(SsrsLoginUserName)
                  || !string.IsNullOrEmpty(SsrsLoginPassword)
                  || !string.IsNullOrEmpty(SsrsLoginDomain);
            }
        }

        [ReplacementIgnore]
        public string OuterReportPath
        {
            get { return System.IO.Path.Combine(OuterDocumentFile, OuterReportName).Replace("\\", "/"); }
            set
            {
                var path = value.Replace("\\", "/").TrimEnd('/');
                var parts = path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length > 1)
                {
                    var part = parts[parts.Length - 2].ToLower();
                    if(part.EndsWith(".flxr") || part.EndsWith(".xml"))
                    {
                        var index = path.LastIndexOf('/');
                        OuterDocumentFile = path.Substring(0, index);
                        OuterReportName = path.Substring(index + 1);
                        return;
                    }
                }

                OuterDocumentFile = value;
                OuterReportName = "";
            }
        }

        [ReplacementIgnore]
        public string InnerReportName
        {
            get { return _innerReportName; }
            set { _innerReportName = value; }
        }

        [ReplacementIgnore]
        public string OuterReportName
        {
            get { return _outerReportName; }
            set { _outerReportName = value; }
        }

        public override string DocumentPath
        {
            get
            {
                return CurrentProjectAsService
                    ? (IsFlexReport ? InnerDocumentFile : SsrsDocumentPath)
                    : (ViewWithActiveReport ? _activeReportPath : OuterDocumentFile);
            }
        }

        public override string ServerUrl
        {
            get
            {
                return ViewWithActiveReport ? _arServerUrl : base.ServerUrl;
            }
        }
        public string ReportName
        {
            get
            {
                return CurrentProjectAsService
                    ? (IsFlexReport ? InnerReportName : string.Empty)
                    : OuterReportName;
            }
        }

        public bool HasReportName
        {
            get { return !string.IsNullOrEmpty(ReportName); }
        }

        [ReplacementIgnore]
        public override string WebApiAssembly
        {
            get
            {
                return "C1.Web.Api.Report";
            }
        }

        [ReplacementIgnore]
        public override string WebApiLicenseDetector
        {
            get
            {
                return "C1.Web.Api.Report.LicenseDetector";
            }
        }
    }
}
