﻿using System;

namespace C1.Web.Mvc.FlexViewerWizard
{
    [AttributeUsageAttribute(AttributeTargets.Property | AttributeTargets.Field,
    AllowMultiple = false, Inherited = true)]
    public sealed class ReplacementIgnoreAttribute : Attribute
    {
    }
}
