﻿namespace C1.Web.Mvc.FlexViewerWizard
{
    public class PdfViewerSettings : ViewerSettingsBase
    {
        public PdfViewerSettings(bool currentProjectServiceSupported = false, string wwwroot = null, string documentsFolder = null)
            : base(currentProjectServiceSupported, wwwroot, documentsFolder)
        {
        }

        protected override string DefaultDocumentsFolder
        {
            get
            {
                return "PdfRoot";
            }
        }

        [ReplacementIgnore]
        public override string WebApiAssembly
        {
            get
            {
                return "C1.Web.Api.Pdf";
            }
        }

        [ReplacementIgnore]
        public override string WebApiLicenseDetector
        {
            get
            {
                return "C1.Web.Api.Pdf.LicenseDetector";
            }
        }
    }
}