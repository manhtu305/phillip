﻿using System.Collections.Generic;
using System.Reflection;
using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using System.Linq;

namespace C1.Web.Mvc.FlexViewerWizard
{
    public abstract class ViewerWizardBase : IWizard
    {
        private readonly IWizard _wizard = (IWizard)Assembly.Load("NuGet.VisualStudio.Interop, Version=1.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a").CreateInstance("NuGet.VisualStudio.TemplateWizard");
        private WizardHelperBase _wizardHelper;

        public void BeforeOpeningFile(ProjectItem projectItem)
        {
            _wizard.BeforeOpeningFile(projectItem);
        }

        public void ProjectFinishedGenerating(Project project)
        {
            _wizard.ProjectFinishedGenerating(project);
        }

        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            _wizard.ProjectItemFinishedGenerating(projectItem);
        }

        public void RunFinished()
        {
            _wizardHelper.RunFinished();
            _wizard.RunFinished();
        }
        
        internal abstract ViewerSettingsBase CreateModel(ProjectEx project);

        internal abstract StartupHelperBase StartupHelper { get; }

        public void RunStarted(object automationObject, Dictionary<string, string> replacements, WizardRunKind runKind, object[] customParams)
        {
            var project = new ProjectEx(automationObject);
            var model = CreateModel(project);
            var form = CreateSettingsWindow(model);
            if (form.ShowDialog() != true)
            {
                throw new WizardCancelledException();
            }

            _wizardHelper = WizardHelperBase.Create(project, model, StartupHelper, replacements);
            _wizardHelper.RunStarted(customParams);
            _wizard.RunStarted(automationObject, replacements, runKind, customParams);
        }

        protected abstract System.Windows.Window CreateSettingsWindow(ViewerSettingsBase model);

        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        internal bool CheckProjectServiceSupported(ProjectEx project)
        {
            return project.MvcVersion == MvcVersion.Mvc5
                || (project.MvcVersion == MvcVersion.AspNetCore &&
                !project.NetVersions.Any(v=>v.StartsWith(NetVersion.netcoreapp) || v.StartsWith(NetVersion.netstandard)));
        }
    }
}
