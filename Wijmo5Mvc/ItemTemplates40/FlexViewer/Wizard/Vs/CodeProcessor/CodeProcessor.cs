﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal abstract class CodeProcessor
    {
        public const string ParamNameRegexGroup = "paramName";
        private const string IndentGroupName = "indent";
        private static readonly Regex IndentRegex = new Regex(@"(?<" + IndentGroupName + @">\s*)(.*)");
        private static readonly Regex ArgumentsContentRegex = new Regex(@"(?<=.*\()(.*)(?=\))");

        public abstract string FileExtension { get; }

        public abstract string CommentsPrefix { get; }

        public abstract string NewInstanceKeyword { get; }

        public abstract string CreateStatement(string content);

        public abstract string CreateActionArg(string content);

        public string CreateUsing(string content)
        {
            return CreateStatement(CreateUsingCore(content));
        }

        protected abstract string CreateUsingCore(string content);

        protected abstract string CreateNoReturnMethodHeader(string startupConfigMethodName);

        public Method FindMethod(string methodName, IList<string> lines)
        {
            var method = new Method { Name = methodName };
            var methodHeader = CreateNoReturnMethodHeader(methodName);
            var scopeLevel = -1;
            for (int i = 0, length = lines.Count; i < length; i++)
            {
                var line = lines[i];
                if (method.Start == -1 && line.Contains(methodHeader))
                {
                    var match = ArgumentsContentRegex.Match(line);
                    if(match != null && !string.IsNullOrEmpty(match.Value))
                    {
                        method.Arguments = GetArguments(match.Value);
                    }

                    method.Start = i;
                }

                if (method.Start == -1)
                {
                    continue;
                }

                var start = StartScopeCount(line);
                if(scopeLevel == -1 && start == 0)
                {
                    continue;
                }

                if(scopeLevel == -1)
                {
                    scopeLevel = 0;
                }

                scopeLevel = scopeLevel + StartScopeCount(line);
                scopeLevel = scopeLevel - EndScopeCount(line);
                if (scopeLevel == 0)
                {
                    method.NestedIndent = GetNestedIndent(line, 1);
                    method.End = i;
                    break;
                }
            }

            return method;
        }

        /// <summary>
        /// Find all the statements for adding specific provider.
        /// </summary>
        /// <remarks>
        /// The statement is for adding specific provider which starts with the specified prefix.
        /// The statement has only one calling of Add***() method, and ends with ')'.
        /// </remarks>
        public IList<Statement> FindUseProvidersStatements(string prefix, IList<string> lines, Method method)
        {
            var statements = new List<Statement>();
            var startIndex = method.Start + 1;
            var endIndex = method.End - 1;
            Statement statement = FindUseProvidersStatement(prefix, lines, startIndex, endIndex);
            while (statement != null && statement.Start != -1 && statement.End != -1)
            {
                statements.Add(statement);
                startIndex = statement.End + 1;
                statement = FindUseProvidersStatement(prefix, lines, startIndex, endIndex);
            }
            return statements;
        }

        /// <summary>
        /// Find the statement for adding specific provider.
        /// </summary>
        /// <remarks>
        /// The statement is for adding specific provider which starts with the specified prefix.
        /// The statement has only one calling of Add***() method, and ends with ')'.
        /// </remarks>
        public Statement FindUseProvidersStatement(string prefix, IList<string> lines, int startIndex, int endIndex)
        {
            var statement = new Statement();
            var parenthesesLevel = -1;
            for (int i = startIndex; i <= endIndex; i++)
            {
                var line = lines[i];
                if (line.TrimStart().StartsWith(CommentsPrefix)) continue;

                if (statement.Start == -1 && line.Contains(prefix))
                {
                    statement.Start = i;
                }

                if (statement.Start == -1)
                {
                    continue;
                }

                var start = StartParenthesesCount(line);
                if (parenthesesLevel == -1 && start == 0)
                {
                    continue;
                }

                if (parenthesesLevel == -1)
                {
                    parenthesesLevel = 0;
                }

                parenthesesLevel += StartParenthesesCount(line);
                parenthesesLevel -= EndParenthesesCount(line);
                if (parenthesesLevel == 0)
                {
                    statement.End = i;
                    break;
                }
            }

            return statement;
        }

        private int StartParenthesesCount(string line)
        {
            return line.Count(c => c == '(');
        }

        private int EndParenthesesCount(string line)
        {
            return line.Count(c => c == ')');
        }

        protected abstract int EndScopeCount(string line);

        protected abstract int StartScopeCount(string line);

        protected abstract IEnumerable<Argument> GetArguments(string content);

        public static string GetNestedIndent(string line, int nestedLevel = 0)
        {
            var indent = IndentRegex.Match(line).Groups[IndentGroupName].Value;
            if (string.IsNullOrEmpty(indent) || indent[0] == '\t')
            {
                return new string('\t', nestedLevel) + indent;
            }

            return new string(' ', nestedLevel * 4) + indent;
        }

        public static CodeProcessor Create(VsLanguage vsLanguage)
        {
            return (vsLanguage == VsLanguage.Vb) ? (CodeProcessor)new VbProcessor() : new CSharpProcessor();
        }
    }
}
