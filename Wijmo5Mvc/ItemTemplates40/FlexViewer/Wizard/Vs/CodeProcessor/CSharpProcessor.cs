﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class CSharpProcessor: CodeProcessor
    {
        private const string statementSeparator = ";";

        public override string CommentsPrefix
        {
            get { return "//"; }
        }

        public override string FileExtension
        {
            get
            {
                return ".cs";
            }
        }

        public override string NewInstanceKeyword
        {
            get { return "new"; }
        }

        public override string CreateActionArg(string content)
        {
            return content;
        }

        public override string CreateStatement(string content)
        {
            if(content.EndsWith(";") || string.IsNullOrWhiteSpace(content))
            {
                return content;
            }

            return content + statementSeparator;
        }

        protected override string CreateNoReturnMethodHeader(string methodName)
        {
            return string.Format("public void {0}(", methodName);
        }

        protected override string CreateUsingCore(string content)
        {
            return "using " + content;
        }

        protected override int EndScopeCount(string line)
        {
            return line.Count(c => c == '}');
        }

        protected override IEnumerable<Argument> GetArguments(string content)
        {
            var arguments = new List<Argument>();
            content = content ?? string.Empty;
            var items = content.Split(',');
            foreach(var item in items)
            {
                var words = item.Split(' ').Where(w => !string.IsNullOrWhiteSpace(w)).ToArray();
                if(words.Length < 2)
                {
                    continue;
                }

                arguments.Add(new Argument(words[1], words[0]));
            }

            return arguments;
        }

        protected override int StartScopeCount(string line)
        {
            return line.Count(c => c == '{');
        }
    }
}
