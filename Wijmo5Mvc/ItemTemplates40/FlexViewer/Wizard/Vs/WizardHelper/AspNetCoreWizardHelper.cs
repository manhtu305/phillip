﻿using C1.Web.Mvc.FlexViewerWizard.Localization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class AspNetCoreWizardHelper : WizardHelperBase
    {
        public AspNetCoreWizardHelper(ProjectEx project, ViewerSettingsBase model, StartupHelperBase startupHelper, Dictionary<string, string> replacements)
            : base(project, model, startupHelper, replacements)
        {
            model.IsRazorPages = project.IsRazorPages;
        }

        public override string StartupConfigMethodName
        {
            get
            {
                return "Configure";
            }
        }

        public override string StartupAppTypeName
        {
            get
            {
                return "IApplicationBuilder";
            }
        }

        public override void RunFinished()
        {
            base.RunFinished();
            if (Project.VsVersion >= new Version("15.0") 
                && Model is ReportViewerSettings
                && Model.CurrentProjectAsService)
            {
                Project.AddReference("System");
            }
        }

        protected override void ConfigStartupCore()
        {
            string envParamName = string.Empty;
            Func<string> rootPathGetter = () => envParamName + ".ContentRootPath";
            Action<Method> configMethodProcess = m =>
            {
                var argument = (m.Arguments ?? new Argument[] { }).FirstOrDefault(a => a.Type.EndsWith("IHostingEnvironment"));
                envParamName = argument.Name;
            };

            ConfigStartup(rootPathGetter, configMethodProcess);
        }

        protected override void EnsureViewImports()
        {
            base.EnsureViewImports();
            EnsureTagHelperImports();
        }

        protected override void EnsureNamespaces(IEnumerable<string> namespaces)
        {
            var viewsFolder = GetViewsFolder();
            var viewImportsFile = Path.Combine(Project.ProjectFolder, viewsFolder, "_ViewImports.cshtml");
            var lines = namespaces.Select(n=> "@using " + n);
            EnsureLines(viewImportsFile, lines);
        }

        private string GetViewsFolder()
        {
            var folder = string.IsNullOrEmpty(Project.AreaName) ? "" : "Areas/" + Project.AreaName + "/";
            return Path.Combine(folder, Project.IsRazorPages ? "Pages" : "Views");
        }

        private static void EnsureLines(string path, IEnumerable<string> lines)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(Resources.FileNotFoundMessage, path);
            }

            var fileLines = File.ReadAllLines(path).ToList();
            var lineList = lines.ToList();
            foreach (var line in fileLines)
            {
                lineList.Remove(line);
            }

            foreach (var line in lineList)
            {
                fileLines.Add(line);
            }

            File.WriteAllLines(path, fileLines);
        }

        private void EnsureTagHelperImports()
        {
            var viewsFolder = GetViewsFolder();
            var viewImportsFile = Path.Combine(Project.ProjectFolder, viewsFolder, "_ViewImports.cshtml");
            var tagHelperImports = new []{ "C1.AspNetCore.Mvc", "C1.AspNetCore.Mvc.FlexViewer" };
            var lines = tagHelperImports.Select(n => "@addTagHelper *, " + n);
            EnsureLines(viewImportsFile, lines);
        }

        protected override void RemoveWebApiDependencies(XDocument doc)
        {
            var webApiNode = doc.XPathSelectElement(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[@keyName='ComponentOne WebApi']");
            if (webApiNode != null)
            {
                webApiNode.Remove();
            }
        }
    }
}
