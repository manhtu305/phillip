﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class ReportViewerStartupHelper : StartupHelperBase
    {
        private const string SsrsProviderKeyBaseName = "SSRS";
        private const string CredentialTypeFullName = "System.Net.NetworkCredential";
        private const string UseReportProvidersName = "UseReportProviders";
        private const string AddSsrsHostName = "AddSsrsReportHost";
        private const string SsrsHostTypeName = "SsrsHost";
        private const string ProviderKeyGroupName = "Key";
        private const string SsrsUrlGroupName = "Url";
        private static readonly Regex ProviderKeyRegex = new Regex(UseReportProvidersName + @"\(\).Add.+\(""(?<" + ProviderKeyGroupName + @">.*?)""");
        private static readonly Regex SsrsUrlRegex = new Regex(SsrsHostTypeName + @"\(""(?<" + SsrsUrlGroupName + @">.*?)""");
        private static readonly Regex SsrsProviderKeyUrlRegex = new Regex(AddSsrsHostName + @"\(""(?<" + ProviderKeyGroupName + @">.*?)"",\s*""(?<" + SsrsUrlGroupName + @">.*?)""");

        protected override string AddDiskStorageName
        {
            get { return "AddFlexReportDiskStorage"; }
        }

        protected override string UseProvidersName
        {
            get { return UseReportProvidersName; }
        }

        protected override void ResolveLines(IWizardHelper wizardHelper, IList<string> allLines, Method method, string rootPath, string appArgName)
        {
            base.ResolveLines(wizardHelper, allLines, method, rootPath, appArgName);
            ResolveAddSsrsHost(wizardHelper, allLines, method, appArgName);
        }

        private void ResolveAddSsrsHost(IWizardHelper wizardHelper, IList<string> allLines, Method method, string appArgName)
        {
            var model = wizardHelper.Model as ReportViewerSettings;
            if (model == null || !model.CurrentProjectAsServiceWithSsrsReport) return;

            var serverUrl = model.SsrsServerUrl;
            if (string.IsNullOrEmpty(serverUrl)) return;

            serverUrl = serverUrl.ToLower().Replace('\\', '/').TrimEnd('/');
            if (!serverUrl.StartsWith("http://") && !serverUrl.StartsWith("https://"))
            {
                serverUrl = "http://" + serverUrl;
            }

            // gets all statements starting with "app.UseReportProviders().Add***".
            var providerPrefix = string.Format("{0}.{1}().Add", appArgName, UseProvidersName);
            var statements = wizardHelper.CodeProcessor.FindUseProvidersStatements(providerPrefix, allLines, method);
            var providers = statements.Select(s => GetProviderInfo(allLines, s));

            var providerWithSameUrl = providers.FirstOrDefault(p => string.Compare(p.SsrsUrl, serverUrl, true) == 0);
            if (providerWithSameUrl != null)
            {
                model.SsrsProviderKey = providerWithSameUrl.Key;
                return;
            }

            var key = GetUniqueName(providers.Select(p => p.Key), SsrsProviderKeyBaseName);
            model.SsrsProviderKey = key;

            var credentialString = model.HasSsrsCredential
                ? string.Format(", {0} {1}(\"{2}\", \"{3}\", \"{4}\")",
                    wizardHelper.CodeProcessor.NewInstanceKeyword, CredentialTypeFullName,
                    model.SsrsLoginUserName, model.SsrsLoginPassword, model.SsrsLoginDomain)
                : "";
            var statement = wizardHelper.CodeProcessor.CreateStatement(
                string.Format("{0}{1}.{2}().{3}(\"{4}\", \"{5}\"{6})",
                    method.NestedIndent, appArgName, UseReportProvidersName, AddSsrsHostName,
                    key, serverUrl, credentialString));

            var insertIndex = method.End;
            allLines.Insert(insertIndex, statement);
            ResolveMethodRange(method, insertIndex);
        }

        private string GetUniqueName(IEnumerable<string> names, string baseName)
        {
            string name = baseName;
            int index = 1;
            while (names.Contains(name, StringComparer.InvariantCultureIgnoreCase))
            {
                name = baseName + "_" + index++;
            }

            return name;
        }

        private ProviderInfo GetProviderInfo(IList<string> allLines, Statement statement)
        {
            var provider = new ProviderInfo();
            for(int i = statement.Start; i <= statement.End; i++)
            {
                var line = allLines[i];

                var matchKeyUrl = SsrsProviderKeyUrlRegex.Match(line);
                if (matchKeyUrl != null && matchKeyUrl.Success)
                {
                    provider.Key = matchKeyUrl.Groups[ProviderKeyGroupName].Value;
                    provider.SsrsUrl = matchKeyUrl.Groups[SsrsUrlGroupName].Value.ToLower().TrimEnd('/');
                    break;
                }

                var matchKey = ProviderKeyRegex.Match(line);
                if (matchKey != null && matchKey.Success)
                {
                    provider.Key = matchKey.Groups[ProviderKeyGroupName].Value;
                }

                var matchUrl = SsrsUrlRegex.Match(line);
                if (matchUrl != null && matchUrl.Success)
                {
                    provider.SsrsUrl = matchUrl.Groups[SsrsUrlGroupName].Value.ToLower().TrimEnd('/');
                    break;
                }
            }

            return provider;
        }

        private class ProviderInfo
        {
            public ProviderInfo()
            {
                Key = string.Empty;
                SsrsUrl = string.Empty;
            }

            /// <summary>
            /// Gets or sets the key of the provider.
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// Gets or sets the url of the SSRS server.
            /// </summary>
            /// <remarks>
            /// Leave it as empty string if it's not an SSRS provider.
            /// </remarks>
            public string SsrsUrl { get; set; }
        }
    }
}
