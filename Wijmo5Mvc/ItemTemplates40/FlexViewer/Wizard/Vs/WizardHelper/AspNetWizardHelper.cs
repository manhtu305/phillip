﻿using C1.Web.Mvc.FlexViewerWizard.Localization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace C1.Web.Mvc.FlexViewerWizard
{
    internal class AspNetWizardHelper : WizardHelperBase
    {
        private const string _mvcAsm = "C1.Web.Mvc";
        private const string _mvcViewerAsm = "C1.Web.Mvc.FlexViewer";
        private const string WebFrameworksRegistry = "AspNetWebFrameworksAndTools5";
        public const string WebApiVersionKey = "$webapiversion$";
        public const string OwinVersionKey = "$owinversion$";
        private const string Vs15WebFrameworksRegistry = "WebStackVS15";

        public AspNetWizardHelper(ProjectEx project, ViewerSettingsBase model, StartupHelperBase startupHelper, Dictionary<string, string> replacements) 
            : base(project, model, startupHelper, replacements)
        {
        }

        public override string StartupConfigMethodName
        {
            get
            {
                return "Configuration";
            }
        }

        public override string StartupAppTypeName
        {
            get
            {
                return "IAppBuilder";
            }
        }

        protected override void EnsureStartupItem()
        {
            var startupItem = FindItem(Project.Project.ProjectItems, StartupItemName);
            if (string.IsNullOrEmpty(startupItem))
            {
                var fileName = StartupItemName + CodeProcessor.FileExtension;
                startupItem = Path.Combine(Project.ProjectFolder, fileName);
                CreateItemFromResource(ResPrefix + fileName, startupItem, Replacements);
                Project.AddItem(fileName);
            }
        }

        protected override void ConfigStartupCore()
        {
            Func<string> rootPathGetter = () => "AppDomain.CurrentDomain.SetupInformation.ApplicationBase";
            ConfigStartup(rootPathGetter);
        }

        protected override void AdjustWebConfig(XDocument doc)
        {
            base.AdjustWebConfig(doc);
            var configNode = doc.XPathSelectElement(@"/configuration");
            var webServerNode = configNode.XPathSelectElement(@"system.webServer");
            if (webServerNode == null)
            {
                webServerNode = new XElement("system.webServer");
                configNode.Add(webServerNode);
            }

            var handlersNode = webServerNode.XPathSelectElement(@"handlers");
            if (handlersNode == null)
            {
                handlersNode = new XElement("handlers");
                webServerNode.Add(handlersNode);
            }

            const string apiHandlerName = "api";
            if (handlersNode.XPathSelectElement(@"remove[@name='" + apiHandlerName + "']") == null)
            {
                var ele = new XElement("remove");
                ele.SetAttributeValue("name", apiHandlerName);
                handlersNode.Add(ele);
            }

            var apiHandler = handlersNode.XPathSelectElement(@"add[@name='" + apiHandlerName + "']");
            if (apiHandler == null)
            {
                apiHandler = new XElement("add");
                apiHandler.SetAttributeValue("name", apiHandlerName);
                handlersNode.Add(apiHandler);
            }

            apiHandler.SetAttributeValue("path", "api/*");
            apiHandler.SetAttributeValue("verb", "*");
            apiHandler.SetAttributeValue("type", "System.Web.Handlers.TransferRequestHandler");
            apiHandler.SetAttributeValue("preCondition", "integratedMode,runtimeVersionv4.0");
        }

        protected override void EnsureLicense()
        {
            var licxFiles = new List<string>();
            licxFiles.AddRange(Directory.GetFiles(Project.ProjectFolder, "*.licx"));
            var propertiesFolder = Path.Combine(Project.ProjectFolder, "Properties");
            if (Directory.Exists(propertiesFolder))
            {
                licxFiles.AddRange(Directory.GetFiles(propertiesFolder, "*.licx"));
            }

            const string licxPattern = "{0}, {1}";
            var getLicxContent = new Func<string, string, string>((l, asm) => string.Format(licxPattern, l, asm));

            const string mvcLicenseDetector = "C1.Web.Mvc.LicenseDetector";
            const string mvcViewerLicenseDetector = "C1.Web.Mvc.Viewer.LicenseDetector";

            string fileContainsMvc = null;
            string fileContainsMvcViewer = null;
            string fileContainsApi = null;
            bool needWebApi = Model.CurrentProjectAsService && !Model.IsBeta;
            int foundCount = 0;
            const int maxFoundCount = 3;
            foreach (var file in licxFiles)
            {
                foreach (var line in File.ReadLines(file))
                {
                    if (fileContainsMvc == null)
                    {
                        fileContainsMvc = line.Contains(mvcLicenseDetector) ? file : null;
                        if (fileContainsMvc != null)
                        {
                            foundCount++;
                        }
                    }

                    if (!Model.IsBeta && fileContainsMvcViewer == null)
                    {
                        fileContainsMvcViewer = line.Contains(mvcViewerLicenseDetector) ? file : null;
                        if (fileContainsMvcViewer != null)
                        {
                            foundCount++;
                        }
                    }

                    if (needWebApi && fileContainsApi == null)
                    {
                        fileContainsApi = line.Contains(Model.WebApiLicenseDetector) ? file : null;
                        if (fileContainsApi != null)
                        {
                            foundCount++;
                        }
                    }

                    if (foundCount >= maxFoundCount)
                    {
                        break;
                    }
                }

                if (foundCount >= maxFoundCount)
                {
                    break;
                }
            }

            var newContents = new List<string>();
            if (fileContainsMvc == null)
            {
                newContents.Add(getLicxContent(mvcLicenseDetector, _mvcAsm));
            }

            if (fileContainsMvcViewer == null && !Model.IsBeta)
            {
                newContents.Add(getLicxContent(mvcViewerLicenseDetector, _mvcViewerAsm));
            }

            if (fileContainsApi == null && needWebApi)
            {
                newContents.Add(getLicxContent(Model.WebApiLicenseDetector, Model.WebApiAssembly));
            }

            if (!newContents.Any())
            {
                return;
            }

            const string defaultLicxFileName = "licenses.licx";
            var licxFile = licxFiles.FirstOrDefault() ?? 
                Path.Combine(Project.ProjectFolder, defaultLicxFileName);
            if (File.Exists(licxFile))
            {
                var lines = File.ReadAllLines(licxFile) ?? new string[] { };
                newContents.AddRange(lines);
            }
            else
            {
                File.WriteAllText(licxFile, string.Empty);
                Project.AddItem(Path.GetFileName(licxFile));
            }

            File.WriteAllLines(licxFile, newContents);
        }

        public override void RunFinished()
        {
            if (Project.MvcVersion == MvcVersion.AspNetCore)
            {
                return;
            }

            EnsureCopyLocal(_mvcAsm);
            EnsureCopyLocal(_mvcViewerAsm);
        }

        private void EnsureCopyLocal(string name)
        {
            var mvc = Project.FindReference(name);
            if (mvc != null)
            {
                mvc.CopyLocal = true;
            }
        }

        protected override void EnsureNamespaces(IEnumerable<string> namespaces)
        {
            var viewsFolder = string.IsNullOrEmpty(Project.AreaName) ? "Views" : "Areas/" + Project.AreaName + "/Views";
            var webConfigFile = Path.Combine(Project.ProjectFolder, viewsFolder, "Web.config");
            if (!File.Exists(webConfigFile))
            {
                throw new FileNotFoundException(Resources.FileNotFoundMessage, webConfigFile);
            }

            var doc = XDocument.Load(webConfigFile);
            var namespacesNode = doc.XPathSelectElement(@"/configuration/system.web.webPages.razor/pages/namespaces");

            Action<string> ensureNamespace = ns =>
            {
                var node = namespacesNode.XPathSelectElement(string.Format("add[@namespace='{0}']", ns));
                if (node != null)
                {
                    return;
                }
                var nsNode = new XElement("add");
                nsNode.SetAttributeValue("namespace", ns);
                namespacesNode.Add(nsNode);
            };

            foreach (var ns in namespaces)
            {
                ensureNamespace(ns);
            }

            doc.Save(webConfigFile);
        }

        protected override void RunStartedCore(object[] customParams)
        {
            if (Model.CurrentProjectAsService)
            {
                EnsureStartupItem();
                EnsureWebApiConfig();
            }

            base.RunStartedCore(customParams);

            var template = customParams[0] as string;
            string currentMajorVersion = Project.VsVersion.Major.ToString();
            var newVsTemplate = Path.Combine(Path.GetDirectoryName(template),
                Path.GetFileNameWithoutExtension(template) + "." + currentMajorVersion + Path.GetExtension(template));
            if (!File.Exists(newVsTemplate))
            {
                var doc = XDocument.Load(template);
                if (Model.CurrentProjectAsService && Project.VsVersion >= new Version("15.0"))
                {
                    // VS2017(VS15.0) installs web frameworks nuget packages in WebStackVS15 repositry.
                    string currentRegistryName = Vs15WebFrameworksRegistry.Replace("15", currentMajorVersion);
                    UpdateVsWebFrameworksRegistry(doc, currentRegistryName);
                }

                UpdateWebApiVersion(doc);
                doc.Save(newVsTemplate);
            }
            customParams[0] = newVsTemplate;
        }

        private void UpdateVsWebFrameworksRegistry(XDocument doc, string VsWebFrameworksRegistry)
        {
            var registryNode = doc.XPathSelectElement(
                string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[@keyName='{0}']",
                WebFrameworksRegistry));
            if (registryNode == null)
            {
                return;
            }

            registryNode.SetAttributeValue("keyName", VsWebFrameworksRegistry);
        }

        //<package id="Microsoft.AspNet.WebApi.Client" version="5.2.3" skipAssemblyReferences="false" />
        //should dynamic change version depend on visual studio
        private void UpdateWebApiVersion(XDocument doc)
        {
            var webApiPackageNodes = doc.XPathSelectElements(
                string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[local-name()='packages']/*[@version='{0}']",
                WebApiVersionKey));

            bool isVs2019AndAbove = Project.VsVersion >= new Version("16.0");

            if (webApiPackageNodes != null)
            {
                //from VS2019(VS16.0) using package webapi 5.2.7, lower than vs version use 5.2.3
                var webApiVersion = isVs2019AndAbove ? "5.2.7" : "5.2.3";

                foreach (var node in webApiPackageNodes)
                {
                    node.SetAttributeValue("version", webApiVersion);
                }
            }

            var owinPackageNodes = doc.XPathSelectElements(
                  string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[local-name()='packages']/*[@version='{0}']",
                  OwinVersionKey));
            if (owinPackageNodes != null)
            {
                //from VS2019(VS16.0) using package aspnet.cors 4.0.0, lower than vs version use 3.0.1
                string version = isVs2019AndAbove ? "4.0.0" : "3.0.1";

                foreach (XElement node in owinPackageNodes)
                {
                    node.SetAttributeValue("version", version);
                }
            }
        }

        protected override void RemoveWebApiDependencies(XDocument doc)
        {
            var wDataNode = doc.XPathSelectElement(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']");
            if (wDataNode != null)
            {
                wDataNode.Remove();
            }
        }

        private void EnsureWebApiConfig()
        {
            var name = "WebApiConfig" + CodeProcessor.FileExtension;
            var itemName = "App_Start/" + name;
            if (!Project.ItemExists(itemName))
            {
                CreateItemFromResource(ResPrefix + name, Path.Combine(Project.ProjectFolder, itemName), Replacements);
                Project.AddItem(itemName);
                SetupGlobalWithWebApi();
            }
        }

        private void SetupGlobalWithWebApi()
        {
            var globalItem = "Global.asax" + CodeProcessor.FileExtension;
            var globalPath = Path.Combine(Project.ProjectFolder, globalItem);
            var lines = new List<string>(File.ReadAllLines(globalPath));
            lines.Insert(0, CodeProcessor.CreateUsing("System.Web.Http"));
            for (int i = 0, length = lines.Count; i < length; i++)
            {
                var line = lines[i];
                if (line.Contains("RegisterAllAreas"))
                {
                    var indent = CodeProcessor.GetNestedIndent(line);
                    var argument = CodeProcessor.CreateActionArg("WebApiConfig.Register");
                    lines.Insert(i, indent + CodeProcessor.CreateStatement("GlobalConfiguration.Configure(" + argument + ")"));
                    break;
                }
            }

            File.WriteAllLines(globalPath, lines);
        }
    }
}
