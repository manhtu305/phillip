﻿namespace C1.Web.Mvc.Viewer
{
    partial class PdfViewer
    {
        private const string _defaultServiceUrl = "~/api/pdf/";
        internal override string DefaultServiceUrl
        {
            get
            {
                return _defaultServiceUrl;
            }
        }
    }
}
