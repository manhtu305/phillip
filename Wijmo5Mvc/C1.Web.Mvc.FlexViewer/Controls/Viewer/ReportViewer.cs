﻿namespace C1.Web.Mvc.Viewer
{
    public partial class ReportViewer
    {
        private const string _defaultServiceUrl = "~/api/report/";
        internal override string DefaultServiceUrl
        {
            get
            {
                return _defaultServiceUrl;
            }
        }
    }
}
