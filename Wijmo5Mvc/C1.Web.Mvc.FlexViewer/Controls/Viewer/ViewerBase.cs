﻿using System;
using C1.Web.Mvc.WebResources;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using Microsoft.Extensions.DependencyInjection;
using UrlHelper = Microsoft.AspNetCore.Mvc.IUrlHelper;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.Viewer
{
    [Scripts(typeof(WebResources.Definitions.Viewer))]
    public partial class ViewerBase
    {
        // Valid whitespace characters defined by the HTML5 spec.
        private static readonly char[] ValidAttributeWhitespaceChars =
            new[] { '\t', '\n', '\u000C', '\r', ' ' };

        internal virtual string DefaultServiceUrl
        {
            get
            {
                return string.Empty;
            }
        }

        private static string GetFullUrl(string relativeUrl, UrlHelper urlHelper)
        {
            var trimmedUrl = relativeUrl;
            if (relativeUrl.Length >= 2 && relativeUrl[0] == '~' && relativeUrl[1] == '/')
            {
                if (urlHelper == null)
                {
                    throw new ArgumentNullException("urlHelper");
                }

                trimmedUrl = urlHelper.Content(trimmedUrl);
            }
            return trimmedUrl;
        }

        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        internal override string ClientSubModule
        {
            get
            {
                return "viewer.";
            }
        }
    }
}
