﻿using C1.Web.Mvc.WebResources;

[assembly: AssemblyScripts(typeof(C1.Web.Mvc.Viewer.WebResources.Definitions.All))]
[assembly: AssemblyStyles(typeof(C1.Web.Mvc.Viewer.WebResources.Definitions.All))]

namespace C1.Web.Mvc.Viewer.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Definitions
    {
        [Scripts(typeof(Viewer))]
        [Styles(typeof(Viewer))]
        public abstract class All { }

        /// <summary>
        /// Defines the related js and css for C1 FlexViewer controls.
        /// </summary>
        [Scripts(typeof(Mvc.WebResources.Definitions.Input),
            typeof(Mvc.WebResources.Definitions.Grid),
            ViewerWebResourcesHelper.WijmoJs + "wijmo.viewer",
            ViewerWebResourcesHelper.Shared + "Viewer",
            ViewerWebResourcesHelper.Mvc + "Viewer",
            ViewerWebResourcesHelper.Mvc + "Cast.FlexViewer")]
        public abstract class Viewer : Mvc.WebResources.Definitions.Control { }
    }
}
