﻿using C1.Web.Mvc.WebResources;
using System;
using System.ComponentModel;

namespace C1.Web.Mvc.Viewer
{
    /// <summary>
    /// Defines the related js and css for C1 FlexViewer controls.
    /// </summary>
    [Scripts(typeof(WebResources.Definitions.Viewer))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1Viewer
    {
    }
}
