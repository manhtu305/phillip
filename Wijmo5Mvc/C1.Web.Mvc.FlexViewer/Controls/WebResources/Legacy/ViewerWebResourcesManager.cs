﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.Viewer
{
    [Obsolete("Please use Scripts control instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
#if ASPNETCORE
    public
#else
    internal
#endif
    sealed class ViewerWebResourcesManager : WebResourcesManagerBase
    {
        #region Fields

        internal static readonly IList<Type> AllControlTypes = new List<Type>
        {
            typeof(C1Viewer)
        };

        #endregion Fields

        #region Properties
        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        #endregion Properties

        #region Ctors
        /// <summary>
        /// Create the resource manager for viewer related controls.
        /// </summary>
        /// <param name="helper">The specified helper.</param>
        public ViewerWebResourcesManager(HtmlHelper helper) : base(helper)
        {
        }
        #endregion Ctors

        #region Methods

        internal override IList<Type> GetAllControlTypes()
        {
            return AllControlTypes;
        }

        #endregion Methods
    }
}
