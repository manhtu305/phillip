﻿using C1.Web.Mvc.WebResources;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace C1.Web.Mvc.Viewer
{
    internal static class ViewerWebResourcesHelper
    {
        private const string FOLDER_SEPARATOR = ".";

        public const string BASE_ROOT =
#if ASPNETCORE
        "C1.AspNetCore.Mvc.FlexViewer";
#else
        "C1.Web.Mvc.Viewer";
#endif
        public const string ROOT = BASE_ROOT + FOLDER_SEPARATOR + "Client" +
#if DEBUG
            "Debug"
#else
            "Release"
#endif
            + FOLDER_SEPARATOR;

        public const string Shared = ROOT + "Shared" + FOLDER_SEPARATOR;
        public const string Mvc = ROOT + "Mvc" + FOLDER_SEPARATOR;
        public const string Wijmo = ROOT + "Wijmo" + FOLDER_SEPARATOR;
        public const string WijmoJs = Wijmo + "controls" + FOLDER_SEPARATOR;

        public static readonly Lazy<IEnumerable<Type>> AllScriptOwnerTypes = new Lazy<IEnumerable<Type>>(
            () => WebResourcesHelper.GetAssemblyResTypes<AssemblyScriptsAttribute>(typeof(ViewerWebResourcesHelper).Assembly()));
    }
}
