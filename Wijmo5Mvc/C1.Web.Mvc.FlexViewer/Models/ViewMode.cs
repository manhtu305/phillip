﻿namespace C1.Web.Mvc.Viewer
{
    /// <summary>
    /// Defines the view modes.
    /// </summary>
    public enum ViewMode
    {
        /// <summary>
        /// The single view mode.
        /// </summary>
        Single,
        /// <summary>
        /// The continuous view mode.
        /// </summary>
        Continuous
    }
}
