﻿namespace C1.Web.Mvc.Viewer
{
    public partial class ViewerBase
    {
        #region ServiceUrl
        private string _serviceUrl = null;
        private string _GetServiceUrl()
        {
#if !MODEL
            string serviceUrl = _serviceUrl;
            if (serviceUrl == null)
            {
                serviceUrl = DefaultServiceUrl;
            }
            return GetFullUrl(serviceUrl, UrlHelper);
#else
            return _serviceUrl;
#endif
        }
        private void _SetServiceUrl(string value)
        {
#if !MODEL
            if (value != null)
            {
                value = value.Trim(ValidAttributeWhitespaceChars);
            }
#endif
            _serviceUrl = value;
        }
        #endregion ServiceUrl
    }
}
