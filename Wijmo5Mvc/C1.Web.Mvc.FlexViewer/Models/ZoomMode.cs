﻿namespace C1.Web.Mvc.Viewer
{
    /// <summary>
    /// Describes the supported zoom modes of FlexViewer.
    /// </summary>
    public enum ZoomMode
    {
        /// <summary>
        /// Custom zoom mode. The actual zoom factor is determined by the value of the @see:ZoomFactor property.
        /// </summary>
        Custom,
        /// <summary>
        /// Pages are zoomed in or out as necessary to fit the page width in the view panel.
        /// </summary>
        PageWidth,
        /// <summary>
        /// Pages are zoomed in or out as necessary to fit the whole page in the view panel.
        /// </summary>
        WholePage
    }
}
