﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//----------------------------------------------------------------------------

#if ASPNETCORE
using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using C1.Web.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.IO;

namespace C1.Web.Mvc.Viewer.TagHelpers
{

#region PdfViewerTagHelper

    /// <summary>
    /// <see cref="ITagHelper"/> implementation for <see cref="PdfViewer" />.
    /// </summary>
    [HtmlTargetElement("c1-pdf-viewer")]
    public partial class PdfViewerTagHelper
        : ViewerBaseTagHelper<PdfViewer>
    {
    }

#endregion

#region ReportViewerTagHelper

    /// <summary>
    /// <see cref="ITagHelper"/> implementation for <see cref="ReportViewer" />.
    /// </summary>
    [HtmlTargetElement("c1-report-viewer")]
    public partial class ReportViewerTagHelper
        : ViewerBaseTagHelper<ReportViewer>
    {
        /// <summary>
        /// Configurates <see cref="ReportViewer.ReportName" />.
        /// Sets the report name.
        /// </summary>
        public string ReportName
        {
            get { return TObject.ReportName; }
            set { TObject.ReportName = value; }
        }

        /// <summary>
        /// Configurates <see cref="ReportViewer.Paginated" />.
        /// Sets a value indicating whether the content should be represented as set of fixed sized pages.
        /// </summary>
        /// <remarks>
        /// The default value is null, means using the default value from document source.
        /// </remarks>
        public bool? Paginated
        {
            get { return TObject.Paginated; }
            set { TObject.Paginated = value; }
        }

        /// <summary>
        /// Configurates <see cref="ReportViewer.Parameters" />.
        /// Sets a dictionary of {name: value} pairs that describe the parameters used to run the report.
        /// </summary>
        public IDictionary<string,object> Parameters
        {
            get { return TObject.Parameters; }
            set { TObject.Parameters = value; }
        }

    }

#endregion

#region ViewerBaseTagHelper<TControl>

    /// <summary>
    /// <see cref="ITagHelper"/> implementation for <see cref="ViewerBase" />.
    /// </summary>
    public abstract partial class ViewerBaseTagHelper<TControl>
        : ControlTagHelper<TControl>
        where TControl : ViewerBase
    {
        /// <summary>
        /// Configurates <see cref="ViewerBase.ViewMode" />.
        /// Sets the view mode.
        /// </summary>
        public ViewMode ViewMode
        {
            get { return TObject.ViewMode; }
            set { TObject.ViewMode = value; }
        }

        /// <summary>
        /// Configurates the <see cref="ViewerBase.OnClientPageIndexChanged" /> client event.
        /// Occurs after the page index is changed.
        /// </summary>
        public string PageIndexChanged
        {
            get { return TObject.OnClientPageIndexChanged; }
            set { TObject.OnClientPageIndexChanged = value; }
        }

        /// <summary>
        /// Configurates the <see cref="ViewerBase.OnClientViewModeChanged" /> client event.
        /// Occurs after the view mode is changed.
        /// </summary>
        public string ViewModeChanged
        {
            get { return TObject.OnClientViewModeChanged; }
            set { TObject.OnClientViewModeChanged = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.ZoomFactor" />.
        /// Sets the zoom factor.
        /// </summary>
        public double ZoomFactor
        {
            get { return TObject.ZoomFactor; }
            set { TObject.ZoomFactor = value; }
        }

        /// <summary>
        /// Configurates the <see cref="ViewerBase.OnClientZoomFactorChanged" /> client event.
        /// Occurs after the zoom factor is changed.
        /// </summary>
        public string ZoomFactorChanged
        {
            get { return TObject.OnClientZoomFactorChanged; }
            set { TObject.OnClientZoomFactorChanged = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.ServiceUrl" />.
        /// Sets the service url. It works as "~/api/" when it is not set.
        /// </summary>
        public string ServiceUrl
        {
            get { return TObject.ServiceUrl; }
            set { TObject.ServiceUrl = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.FilePath" />.
        /// Sets the document path.
        /// </summary>
        public string FilePath
        {
            get { return TObject.FilePath; }
            set { TObject.FilePath = value; }
        }

        /// <summary>
        /// Configurates the <see cref="ViewerBase.OnClientQueryLoadingData" /> client event.
        /// Queries the request data sent to the service before loading the document.
        /// </summary>
        public string QueryLoadingData
        {
            get { return TObject.OnClientQueryLoadingData; }
            set { TObject.OnClientQueryLoadingData = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.FullScreen" />.
        /// Sets whether viewer is under full screen mode.
        /// </summary>
        public bool FullScreen
        {
            get { return TObject.FullScreen; }
            set { TObject.FullScreen = value; }
        }

        /// <summary>
        /// Configurates the <see cref="ViewerBase.OnClientFullScreenChanged" /> client event.
        /// Occurs after the full screen mode is changed.
        /// </summary>
        public string FullScreenChanged
        {
            get { return TObject.OnClientFullScreenChanged; }
            set { TObject.OnClientFullScreenChanged = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.SelectMouseMode" />.
        /// Deprecated: Use MouseMode instead.
        /// </summary>
        [Obsolete("Deprecated: Use MouseMode instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool SelectMouseMode
        {
            get { return TObject.SelectMouseMode; }
            set { TObject.SelectMouseMode = value; }
        }

        /// <summary>
        /// Configurates the <see cref="ViewerBase.OnClientSelectMouseModeChanged" /> client event.
        /// Deprecated: Use OnClientMouseModeChanged instead.
        /// </summary>
        [Obsolete("Deprecated: Use OnClientMouseModeChanged instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string SelectMouseModeChanged
        {
            get { return TObject.OnClientSelectMouseModeChanged; }
            set { TObject.OnClientSelectMouseModeChanged = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.ZoomMode" />.
        /// Sets a value indicating the current zoom mode to show the document pages.
        /// </summary>
        public ZoomMode ZoomMode
        {
            get { return TObject.ZoomMode; }
            set { TObject.ZoomMode = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.ThresholdWidth" />.
        /// Sets the threshold to switch between mobile and PC template
        /// </summary>
        public int ThresholdWidth
        {
            get { return TObject.ThresholdWidth; }
            set { TObject.ThresholdWidth = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.MouseMode" />.
        /// Sets a value indicating the mouse behavior.
        /// </summary>
        public MouseMode MouseMode
        {
            get { return TObject.MouseMode; }
            set { TObject.MouseMode = value; }
        }

        /// <summary>
        /// Configurates the <see cref="ViewerBase.OnClientMouseModeChanged" /> client event.
        /// Occurs after the mouse mode is changed.
        /// </summary>
        public string MouseModeChanged
        {
            get { return TObject.OnClientMouseModeChanged; }
            set { TObject.OnClientMouseModeChanged = value; }
        }

        /// <summary>
        /// Configurates <see cref="ViewerBase.RequestHeaders" />.
        /// Sets an object containing request headers to be used when sending or requesting data. The most typical use for this property is in scenarios where authentication is required.
        /// </summary>
        public IDictionary<string,object> RequestHeaders
        {
            get { return TObject.RequestHeaders; }
            set { TObject.RequestHeaders = value; }
        }

        /// <summary>
        /// Configurates the <see cref="ViewerBase.OnClientBeforeSendRequest" /> client event.
        /// Occurs before every request sent to the server.
        /// </summary>
        public string BeforeSendRequest
        {
            get { return TObject.OnClientBeforeSendRequest; }
            set { TObject.OnClientBeforeSendRequest = value; }
        }

    }

#endregion

}
#endif

