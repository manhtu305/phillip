﻿namespace C1.Web.Mvc.Viewer
{
    /// <summary>
    /// Specifies the mouse modes, which defines the mouse behavior of viewer.
    /// </summary>
    public enum MouseMode
    {
        /// <summary>
        /// Select text.
        /// </summary>
        SelectTool,
        /// <summary>
        /// Move page.
        /// </summary>
        MoveTool,
        /// <summary>
        /// Rubberband to zoom.
        /// </summary>
        RubberbandTool,
        /// <summary>
        /// Magnifier tool.
        /// </summary>
        MagnifierTool
    }
}
