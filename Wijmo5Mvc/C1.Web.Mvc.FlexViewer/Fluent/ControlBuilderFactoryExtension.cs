﻿using C1.Web.Mvc.Fluent;
using System;
using System.ComponentModel;
using System.Linq;
#if ASPNETCORE
using IHtmlString = Microsoft.AspNetCore.Html.IHtmlContent;
#else
using System.Web;
#endif

namespace C1.Web.Mvc.Viewer.Fluent
{
    /// <summary>
    /// Extends ControlBuilderFactory for Viewer related controls creation.
    /// </summary>
    public static class ControlBuilderFactoryExtension
    {
        /// <summary>
        /// Create a ReportViewerBuilder.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <param name="selector">The selector</param>
        /// <returns>The ReportViewerBuilder</returns>
        public static ReportViewerBuilder ReportViewer(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new ReportViewerBuilder(new ReportViewer(controlBuilderFactory._helper, selector));
        }

        /// <summary>
        /// Create a PdfViewerBuilder.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <param name="selector">The selector</param>
        /// <returns>The PdfViewerBuilder</returns>
        public static PdfViewerBuilder PdfViewer(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new PdfViewerBuilder(new PdfViewer(controlBuilderFactory._helper, selector));
        }

        /// <summary>
        /// Render the css and js resources of the specified control type.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory</param>
        /// <param name="controlTypes">Specify the types of the controls</param>
        /// <returns>The html string</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please use Scripts method instead.")]
        public static IHtmlString FlexViewerResources(this ControlBuilderFactory controlBuilderFactory, params Type[] controlTypes)
        {
            var c1Resources = new ViewerWebResourcesManager(controlBuilderFactory._helper);
            controlTypes.ToList().ForEach(ct => c1Resources.ControlTypes.Add(ct));
            return c1Resources;
        }
    }
}
