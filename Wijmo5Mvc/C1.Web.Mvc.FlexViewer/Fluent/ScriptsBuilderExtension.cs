﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.Viewer.Fluent
{
    /// <summary>
    /// Extends <see cref="ScriptsBuilder"/> class for viewer scripts.
    /// </summary>
    public static class ScriptsBuilderExtension
    {
        /// <summary>
        /// Registers viewer related script bundle. This bundle contains: ReportViewer and PdfViewer controls.
        /// </summary>
        /// <param name="scriptsBuilder">The <see cref="ScriptsBuilder"/>.</param>
        /// <returns>The <see cref="ScriptsBuilder"/>.</returns>
        public static ScriptsBuilder FlexViewer(this ScriptsBuilder scriptsBuilder)
        {
            scriptsBuilder.OwnerTypes.AddRange(ViewerWebResourcesHelper.AllScriptOwnerTypes.Value);
            return scriptsBuilder;
        }
    }
}
