﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.TransposedGrid.Fluent
{
    /// <summary>
    /// Extends ControlBuilderFactory for TransposedGrid related controls creation.
    /// </summary>
    public static class ControlBuilderFactoryExtension
    {
        /// <summary>
        /// Create a TransposedGridBuilder.
        /// </summary>
        /// <typeparam name="T">The data item type.</typeparam>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <param name="selector">The selector.</param>
        /// <returns>The TransposedGridBuilder.</returns>
        public static TransposedGridBuilder<T> TransposedGrid<T>(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new TransposedGridBuilder<T>(new TransposedGrid<T>(controlBuilderFactory._helper, selector));
        }
    }
}