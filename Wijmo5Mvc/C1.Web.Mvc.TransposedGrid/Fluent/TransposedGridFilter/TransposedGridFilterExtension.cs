﻿using System;
using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.TransposedGrid.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use FlexGridFilter extender.
    /// </summary>
    public static class TransposedGridFilterExtension
    {
        /// <summary>
        /// Apply the FlexGridFilter extender in TransposedGrid.
        /// </summary>
        /// <typeparam name="T">The data record type.</typeparam>
        /// <param name="transposedGridBuilder">The specified TransposedGrid builder.</param>
        /// <param name="gridFilterBuilder">The specified FlexGridFilter builder.</param>
        /// <returns>The TransposedGrid builder.</returns>
        public static TransposedGridBuilder<T> Filterable<T>(this TransposedGridBuilder<T> transposedGridBuilder, Action<FlexGridFilterBuilder<T>> gridFilterBuilder)
        {
            var transposedGrid = transposedGridBuilder.Object;
            FlexGridFilter<T> gridFilter = new FlexGridFilter<T>(transposedGrid);
            gridFilterBuilder(new FlexGridFilterBuilder<T>(gridFilter));
            transposedGrid.Extenders.Add(gridFilter);
            return transposedGridBuilder;
        }

        /// <summary>
        /// Apply the default FlexGridFilter extender in TransposedGrid.
        /// </summary>
        /// <typeparam name="T">The data record type.</typeparam>
        /// <param name="transposedGridBuilder">The specified TransposedGrid builder.</param>
        /// <returns>The TransposedGrid filter.</returns>
        public static TransposedGridBuilder<T> Filterable<T>(this TransposedGridBuilder<T> transposedGridBuilder)
        {
            var transposedGrid = transposedGridBuilder.Object;
            FlexGridFilter<T> gridFilter = new FlexGridFilter<T>(transposedGrid);
            transposedGrid.Extenders.Add(gridFilter);
            return transposedGridBuilder;
        }

        /// <summary>
        /// Apply the FlexGridFilter extender in TransposedGrid.
        /// </summary>
        /// <param name="transposedGridBuilder">The specified TransposedGrid builder.</param>
        /// <param name="gridFilterBuilder">The specified FlexGridFilter builder.</param>
        /// <returns>The TransposedGrid builder.</returns>
        public static TransposedGridBuilder<object> Filterable(this TransposedGridBuilder<object> transposedGridBuilder, Action<FlexGridFilterBuilder<object>> gridFilterBuilder)
        {
            var transposedGrid = transposedGridBuilder.Object;
            FlexGridFilter<object> gridFilter = new FlexGridFilter<object>(transposedGrid);
            gridFilterBuilder(new FlexGridFilterBuilder<object>(gridFilter));
            transposedGrid.Extenders.Add(gridFilter);
            return transposedGridBuilder;
        }

        /// <summary>
        /// Apply the default FlexGridFilter extender in TransposedGrid.
        /// </summary>
        /// <param name="transposedGridBuilder">The specified TransposedGrid builder.</param>
        /// <returns>The TransposedGrid filter.</returns>
        public static TransposedGridBuilder<object> Filterable(this TransposedGridBuilder<object> transposedGridBuilder)
        {
            var transposedGrid = transposedGridBuilder.Object;
            FlexGridFilter<object> gridFilter = new FlexGridFilter<object>(transposedGrid);
            transposedGrid.Extenders.Add(gridFilter);
            return transposedGridBuilder;
        }

    }
}
