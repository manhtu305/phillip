﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.TransposedGrid.Fluent
{
    /// <summary>
    /// Extends <see cref="ScriptsBuilder"/> class for TransposedGrid scripts.
    /// </summary>
    public static class ScriptsBuilderExtension
    {
        /// <summary>
        /// Registers olap related script bundle.
        /// </summary>
        /// <param name="scriptsBuilder">The <see cref="ScriptsBuilder"/>.</param>
        /// <returns>The <see cref="ScriptsBuilder"/>.</returns>
        public static ScriptsBuilder TransposedGrid(this ScriptsBuilder scriptsBuilder)
        {
            scriptsBuilder.OwnerTypes.AddRange(TransposedGridWebResourcesHelper.AllScriptOwnerTypes.Value);
            return scriptsBuilder;
        }
    }
}
