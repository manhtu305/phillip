﻿using System;
using System.ComponentModel;
using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.TransposedGrid.Fluent
{
    public partial class TransposedGridBuilder<T>
    {
        /// <summary>
        /// Configure TransposedGrid.Rows"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public virtual TransposedGridBuilder<T> Rows(Action<ListItemFactory<Column, ColumnBuilder>> builder)
        {
            builder(new ListItemFactory<Column, ColumnBuilder>(Object.Columns,
                () => new Column(Object.Helper), c => new ColumnBuilder(c)));
            return this as TransposedGridBuilder<T>;
        }

        #region hidden TransposedGrid method

        /// <summary>
        /// This method is useless in TransposedGrid.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override TransposedGridBuilder<T> AllowAddNew(bool value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in TransposedGrid.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override TransposedGridBuilder<T> AllowDelete(bool value)
        {
            return this;
        }

        /// <summary>
        /// This method is useless in TransposedGrid, using AutoGenerateRows instead.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override TransposedGridBuilder<T> AutoGenerateColumns(bool value)
        {
            return this;
        }

        #endregion
    }
}
