﻿using System;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.TransposedGrid
{
    [Scripts(typeof(WebResources.Definitions.TransposedGrid))]
    public partial class TransposedGrid<T>
    {
        private const string ClientModuleTransposedGrid = "grid.transposed.";

        internal override string ClientSubModule
        {
            get { return ClientModuleTransposedGrid; }
        }

        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }
    }

}
