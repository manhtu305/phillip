﻿using C1.Web.Mvc.WebResources;

[assembly: AssemblyScripts(typeof(C1.Web.Mvc.TransposedGrid.WebResources.Definitions.All))]
[assembly: AssemblyStyles(typeof(C1.Web.Mvc.TransposedGrid.WebResources.Definitions.All))]

namespace C1.Web.Mvc.TransposedGrid.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Definitions
    {
        [Scripts(typeof(TransposedGrid))]
        [Styles(typeof(TransposedGrid))]
        public abstract class All { }
        
        [Scripts(TransposedGridWebResourcesHelper.WijmoJs + "wijmo.grid.transposed",
            TransposedGridWebResourcesHelper.Shared + "Grid.Transposed",
            TransposedGridWebResourcesHelper.Mvc + "Grid.Transposed",
            TransposedGridWebResourcesHelper.Mvc + "Cast.grid.transposed")]
        public abstract class TransposedGrid : Mvc.WebResources.Definitions.Grid { }
    }
}
