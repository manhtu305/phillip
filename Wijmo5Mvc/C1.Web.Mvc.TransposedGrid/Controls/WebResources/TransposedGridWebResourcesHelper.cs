﻿using System;
using System.Reflection;
using System.Collections.Generic;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.TransposedGrid
{
    internal static class TransposedGridWebResourcesHelper
    {
        private const string FOLDER_SEPARATOR = ".";

        public const string BASE_ROOT =
#if ASPNETCORE
        "C1.AspNetCore.Mvc.TransposedGrid";
#else
        "C1.Web.Mvc.TransposedGrid";
#endif
        public const string ROOT = BASE_ROOT + FOLDER_SEPARATOR + "Client" +
#if DEBUG
            "Debug"
#else
            "Release"
#endif
            + FOLDER_SEPARATOR;

        public const string Shared = ROOT + "Shared" + FOLDER_SEPARATOR;
        public const string Mvc = ROOT + "Mvc" + FOLDER_SEPARATOR;
        public const string Wijmo = ROOT + "Wijmo" + FOLDER_SEPARATOR;
        public const string WijmoJs = Wijmo + "controls" + FOLDER_SEPARATOR;

        public static readonly Lazy<IEnumerable<Type>> AllScriptOwnerTypes = new Lazy<IEnumerable<Type>>(
            () => WebResourcesHelper.GetAssemblyResTypes<AssemblyScriptsAttribute>(typeof(TransposedGridWebResourcesHelper).Assembly()));
    }
}
