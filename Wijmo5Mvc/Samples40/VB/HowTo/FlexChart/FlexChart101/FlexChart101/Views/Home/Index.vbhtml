﻿@Imports FlexChart101
@ModelType FlexChartModel 

@Code
    ViewBag.Title = "FlexChart Introduction"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="header">
    <div class="container">
        <a class="logo-container" href="https://www.grapecity.com/en/aspnet-mvc" target="_blank">
            <i class="gcicon-product-c1"></i>
        </a>
        <h1>
            FlexChart 101
        </h1>
        <p>
            This page shows how to get started with C1 FlexChart controls using VB.NET.
        </p>
    </div>
</div>

<div class="container">
    <div class="sample-page download-link">
        <a href="https://www.grapecity.com/en/download/componentone-studio">Download Free Trial</a>
    </div>
    <!-- Getting Started -->
    <div>
        <h2>Getting Started</h2>
        <p>
            Steps for getting started with the FlexChart control in MVC applications:
        </p>
        <ol>
            <li>Create a new MVC project using the C1 ASP.NET MVC application template.</li>
            <li>Add controller and corresponding view to the project.</li>
            <li>Initialize the Chart control in view using razor syntax.</li>
            <li>(Optional) Add some CSS to customize the FlexChart control's appearance.</li>
        </ol>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#gsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#gsCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#gsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="gsHtml">

&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;

    &lt;/head&gt;
    &lt;body&gt;

        &lt;!-- this is the FlexChart --&gt;
        @@(Html.C1().FlexChart().Id("gettingStartedChart").Bind(Model.CountrySalesData).BindingX("Country") _
            .Series(Sub(sers)
                        sers.Add().Binding("Sales").Name("Sales")
                        sers.Add().Binding("Expenses").Name("Expenses")
                        sers.Add().Binding("Downloads").Name("Downloads")
                    End Sub)
        )

    &lt;/body&gt;
&lt;/html&gt;

                        </div>
                        <div class="tab-pane pane-content" id="gsCss">

.wj-flexchart {
    background-color: white;
    box-shadow: 4px 4px 10px 0px rgba(50, 50, 50, 0.75);
    height: 400px;
    margin-bottom: 12px;
    padding: 8px;
}

                        </div>
                        <div class="tab-pane pane-content" id="gsCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function    
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexChart().Id("gettingStartedChart").Bind(Model.CountrySalesData).BindingX("Country") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
            </div>
        </div>
    </div>


    <!-- chart types -->
    <div>
        <h2>Chart Types</h2>
        <p>The FlexChart control has three properties that allow you to customize the chart type:</p>
        <ol>
            <li>
                <b>ChartType</b>: Selects the default chart type to be used for all series.
                Individual series may override this.
            </li>
            <li>
                <b>Stacking</b>: Determines whether series are plotted independently,
                stacked, or stacked so their sum is 100%.
            </li>
            <li>
                <b>Rotated</b>: Flips the X and Y axes so X becomes vertical and Y horizontal.
            </li>
        </ol>
        <p>The example below allows you to see what happens when you change these properties:</p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#ctHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#ctJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#ctCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="ctHtml">

@@(Html.C1().FlexChart().Id("chartTypes").Bind(Model.CountrySalesData).BindingX("Country") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
)

&lt;label class="col-md-3 control-label"&gt;Chart Type&lt;/label&gt;
@@(Html.C1().ComboBox().Id("typeMenu").Bind(Model.Settings("ChartType")) _
    .SelectedValue("Column").OnClientSelectedIndexChanged("typeMenu_SelectedIndexChanged")
)

&lt;label class="col-md-3 control-label"&gt;Stacking&lt;/label&gt;
@@(Html.C1().ComboBox().Id("stackingMenu").Bind(Model.Settings("Stacking")) _
    .SelectedValue("None").OnClientSelectedIndexChanged("stackingMenu_SelectedIndexChanged")
)

&lt;label class="col-md-3 control-label"&gt;Rotated&lt;/label&gt;
@@(Html.C1().ComboBox().Id("rotatedMenu").Bind(Model.Settings("Rotated")) _
    .SelectedValue("False").OnClientSelectedIndexChanged("rotatedMenu_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="ctJs">

//Chart Types Module
function typeMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        var chartTypes = &lt;wijmo.chart.FlexChart&gt; wijmo.Control.getControl("#chartTypes");
        chartTypes.chartType = sender.selectedValue;
    }
}

function stackingMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        var chartTypes = &lt;wijmo.chart.FlexChart&gt; wijmo.Control.getControl("#chartTypes");
        chartTypes.stacking = parseInt(sender.selectedIndex);
    }
}

function rotatedMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        var chartTypes = &lt;wijmo.chart.FlexChart&gt;wijmo.Control.getControl("#chartTypes");
        chartTypes.rotated = sender.selectedValue == 'True' ? true : false;
    }
}


                        </div>
                        <div class="tab-pane pane-content" id="ctCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.Settings = CreateIndexSettings()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function

    Private Function CreateIndexSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
            {"ChartType", New Object() {"Column", "Bar", "Scatter", "Line", "LineSymbols", "Area",
                "Spline", "SplineSymbols", "SplineArea"}},
            {"Stacking", New Object() {"None", "Stacked", "Stacked 100%"}},
            {"Rotated", New Object() {False.ToString(), True.ToString()}}            
        }

        Return settings
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexChart().Id("chartTypes").Bind(Model.CountrySalesData).BindingX("Country") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Chart Type</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("typeMenu").Bind(Model.Settings("ChartType")) _
        .SelectedValue("Column").OnClientSelectedIndexChanged("typeMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Stacking</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("stackingMenu").Bind(Model.Settings("Stacking")) _
.SelectedValue("None").OnClientSelectedIndexChanged("stackingMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Rotated</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("rotatedMenu").Bind(Model.Settings("Rotated")) _
.SelectedValue("False").OnClientSelectedIndexChanged("rotatedMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- mixed chart types -->
    <div>
        <h2>Mixed Chart Types</h2>
        <p>
            You can use different chart types for each chart series by setting the <b>ChartType</b>
            property on the series itself. This overrides the chart's default chart type.
        </p>
        <p>
            In the example below, the chart's <b>ChartType</b> property is set to <b>Column</b>,
            but the <b>Downloads</b> series overrides that to use the <b>LineSymbols</b> chart type:
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#mctHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#mctCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="mctHtml">

@@(Html.C1().FlexChart().Id("mixedTypesChart").Bind(Model.CountrySalesData).BindingX("Country") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads").ChartType(C1.Web.Mvc.Chart.ChartType.LineSymbols)
            End Sub)
            )

                        </div>
                        <div class="tab-pane pane-content" id="mctCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function    
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexChart().Id("mixedTypesChart").Bind(Model.CountrySalesData).BindingX("Country") _
        .Series(Sub(sers)
                    sers.Add().Binding("Sales").Name("Sales")
                    sers.Add().Binding("Expenses").Name("Expenses")
                    sers.Add().Binding("Downloads").Name("Downloads").ChartType(C1.Web.Mvc.Chart.ChartType.LineSymbols)
                End Sub)
                )
            </div>
        </div>
    </div>



    <!-- legend and titles -->
    <div>
        <h2>Legend and Titles</h2>
        <p>
            Use the <b>Legend</b> properties to customize the appearance of the chart legend, and
            the <b>Header</b>, <b>Footer</b>, and axis <b>Title</b> properties to add titles
            to your charts.
        </p>
        <p>
            You can style the legend and titles using CSS. The CSS tab below shows the rules
            used to customize the appearance of the legend and titles. Notice that these are
            SVG elements, so you have to use CSS attributes such as "fill" instead of "color."
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#ltHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#ltJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#ltCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#ltCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="ltHtml">

@@(Html.C1().FlexChart().Id("chartLegendAndTitles").Header("Sample Chart") _
    .Footer("Copyright (c) ComponentOne").Bind(Model.CountrySalesData).BindingX("Country") _
    .AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
                )
&lt;dl class="dl-horizontal"&gt;
    &lt;dt&gt;Header&lt;/dt&gt;&lt;dd&gt;&lt;input id="headerInput" class="form-control"/&gt;&lt;/dd&gt;
    &lt;dt&gt;Footer&lt;/dt&gt;&lt;dd&gt;&lt;input id="footerInput" class="form-control"/&gt;&lt;/dd&gt;
    &lt;dt&gt;X-Axis Title&lt;/dt&gt;&lt;dd&gt;&lt;input id="xTitleInput" class="form-control"/&gt;&lt;/dd&gt;
    &lt;dt&gt;Y-Axis Title&lt;/dt&gt;&lt;dd&gt;&lt;input id="yTitleInput" class="form-control"/&gt;&lt;/dd&gt;
    &lt;dt&gt;&lt;/dt&gt;
&lt;dd&gt;
    @@(Html.C1().ComboBox().Id("positionMenu").Bind(Model.Settings("Position")) _
        .SelectedValue("Right").OnClientSelectedIndexChanged("positionMenu_SelectedIndexChanged")
        )
&lt;/dd&gt;
&lt;/dl&gt;

                        </div>
                        <div class="tab-pane pane-content" id="ltJs">

function InitialControls() {
    //Legend & Title Module
    var ltchart = &lt;wijmo.chart.FlexChart&gt;wijmo.Control.getControl("#chartLegendAndTitles");
    var ltHeader = &lt;HTMLInputElement&gt;document.getElementById('headerInput');
    var ltFooter = &lt;HTMLInputElement&gt;document.getElementById('footerInput');
    var ltXTitle = &lt;HTMLInputElement&gt;document.getElementById('xTitleInput');
    var ltYTitle = &lt;HTMLInputElement&gt;document.getElementById('yTitleInput');

    ltHeader.value = 'Sample Chart';
    ltHeader.addEventListener('input', function () {
        ltchart.header = this.value;
    });

    ltFooter.value = 'Copyright (c) ComponentOne';
    ltFooter.addEventListener('input', function () {
        ltchart.footer = this.value;
    });

    ltXTitle.value = 'Country';
    ltXTitle.addEventListener('input', function () {
        ltchart.axisX.title = this.value;
    });

    ltYTitle.value = 'Amount';
    ltYTitle.addEventListener('input', function () {
        ltchart.axisY.title = this.value;
    });
}

//Legend and Title Module
function positionMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        var chart = &lt;wijmo.chart.FlexChart&gt; wijmo.Control.getControl("#chartLegendAndTitles");
        chart.legend.position = parseInt(sender.selectedIndex);
    }
}


                        </div>
                        <div class="tab-pane pane-content" id="ltCss">

.wj-flexchart .wj-title {
    font-weight: bold;
}
.wj-flexchart .wj-header .wj-title {
    fill: #80044d;
    font-size: 18pt;
}
.wj-flexchart .wj-footer .wj-title {
    fill: #80044d;
}
.wj-flexchart .wj-axis-x .wj-title,
.wj-flexchart .wj-axis-y .wj-title {
    font-style: italic;
}

                        </div>
                        <div class="tab-pane pane-content" id="ltCS">


Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.Settings = CreateIndexSettings()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function

    Private Function CreateIndexSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {            
            {"Position", New Object() {Position.None.ToString(), Position.Left.ToString(), Position.Top.ToString(), Position.Right.ToString(), Position.Bottom.ToString()}}            
        }

        Return settings
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexChart().Id("chartLegendAndTitles").Header("Sample Chart") _
        .Footer("Copyright (c) ComponentOne").Bind(Model.CountrySalesData).BindingX("Country") _
        .AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
                )
                <dl class="dl-horizontal">
                    <dt>Header</dt>
                    <dd><input id="headerInput" class="form-control" /></dd>
                    <dt>Footer</dt>
                    <dd><input id="footerInput" class="form-control" /></dd>
                    <dt>X-Axis Title</dt>
                    <dd><input id="xTitleInput" class="form-control" /></dd>
                    <dt>Y-Axis Title</dt>
                    <dd><input id="yTitleInput" class="form-control" /></dd>
                    <dt>Legend</dt>
                    <dd>
                        @(Html.C1().ComboBox().Id("positionMenu").Bind(Model.Settings("Position")) _
.SelectedValue("Right").OnClientSelectedIndexChanged("positionMenu_SelectedIndexChanged")
                        )
                    </dd>
                </dl>
            </div>
        </div>
    </div>



    <!-- tooltips -->
    <div>
        <h2>Tooltips</h2>
        <p>
            The FlexChart has built-in support for tooltips. By default, the control displays
            tooltips when the user touches or hovers the mouse on a data point.
        </p>
        <p>
            The tooltip content is generated using a template that may contain the following
            parameters:
        </p>
        <ul>
            <li><b>SeriesName</b>: The name of the series that contains the chart element.</li>
            <li><b>PointIndex</b>: The index of the chart element within the series.</li>
            <li><b>AxisX</b>: The <b>x</b> value of the chart element.</li>
            <li><b>AxisY</b>: The <b>y</b> value of the chart element.</li>
        </ul>
        <p>
            By default, the tooltip template is set to
            <code>&lt;b&gt;{seriesName}&lt;/b&gt;&lt;br/&gt;{x} {y}</code>,
            and you can see how that works in the  charts above.
            In this example, we set the tooltip template to
            <code>&lt;b&gt;{seriesName}&lt;/b&gt; &lt;img src='../../Content/images/{x}.png'/&gt;&lt;br/&gt;{y}</code>,
            which replaces the country name with the country's flag.
        </p>
        <p>You can disable the chart tooltips by setting the template to an empty string.</p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tooltipHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#tooltipCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="tooltipHtml">

@@(Html.C1().FlexChart().Id("chartTooltip").Header("Sample Chart").Footer("Copyright (c) ComponentOne") _
    .Bind(Model.CountrySalesData).BindingX("Country").AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub) _
            .Tooltip(Sub(tt) tt.Content("&lt;img src='../../Content/images/{x}.png'/&gt; &lt;b&gt;{seriesName}&lt;/b&gt;&lt;br /&gt;{y}")) _
)

                        </div>
                        <div class="tab-pane pane-content" id="tooltipCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>                
                @(Html.C1().FlexChart().Id("chartTooltip").Header("Sample Chart").Footer("Copyright (c) ComponentOne") _
        .Bind(Model.CountrySalesData).BindingX("Country").AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub) _
            .Tooltip(Sub(tt) tt.Content("<img src='../../Content/images/{x}.png'/> <b>{seriesName}</b><br />{y}")) _
                )
            </div>
        </div>
    </div>


    <!-- styling series -->
    <div>
        <h2>Styling Series</h2>
        <p>
            The FlexChart automatically picks colors for each series based on a default
            palette, which you can override by setting the <b>Palette</b> property.
            But you can also override the default settings by setting the <b>Style</b>
            property of any series to an object that specifies SVG styling attributes,
            including <b>Fill</b>, <b>Stroke</b>, <b>StrokeThickness</b>, and so on.
        </p>
        <p>
            The <b>Series.Style</b> property is an exception to the general rule that
            all styling in MVC Controls is done through CSS. The exception reflects the fact
            that many charts have dynamic series, which would be impossible to style
            in advance. For example, a stock chart may show series selected by the
            user while running the application.
        </p>
        <p>
            The chart in this example uses the <b>Style</b> and <b>SymbolStyle</b> properties
            to select style attributes for each series:
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#seriesStyleHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#seriesStyleCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="seriesStyleHtml">

@@(Html.C1().FlexChart().Id("chartSeriesStyle").Bind(Model.CountrySalesData).BindingX("Country") _
    .AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
        sers.Add().Binding("Sales").Name("Sales").Style(Sub(ss) ss.Fill("green").Stroke("darkgreen").StrokeWidth(1))
        sers.Add().Binding("Expenses").Name("Expenses").Style(Sub(ss) ss.Fill("red").Stroke("darkred").StrokeWidth(1))
        sers.Add().Binding("Downloads").Name("Downloads").ChartType(C1.Web.Mvc.Chart.ChartType.LineSymbols) _
            .Style(Sub(ss)
                        ss.Fill("gold")
                        ss.Stroke("orange")
                        ss.StrokeWidth(5)
                    End Sub)
            End Sub) _
        .Tooltip(Sub(tt) tt.Content("&lt;img src='../../Content/images/{x}.png'/&gt; &lt;b&gt;{seriesName}&lt;/b&gt;&lt;br /&gt;{y}"))
        )

                        </div>
                        <div class="tab-pane pane-content" id="seriesStyleCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>                
                @(Html.C1().FlexChart().Id("chartSeriesStyle").Bind(Model.CountrySalesData).BindingX("Country") _
        .AxisX(Sub(x) x.Title("Country")).AxisY(Sub(y) y.Title("Amount")) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales").Style(Sub(ss) ss.Fill("green").Stroke("darkgreen").StrokeWidth(1))
                sers.Add().Binding("Expenses").Name("Expenses").Style(Sub(ss) ss.Fill("red").Stroke("darkred").StrokeWidth(1))
                sers.Add().Binding("Downloads").Name("Downloads") _
                .ChartType(C1.Web.Mvc.Chart.ChartType.LineSymbols).Style(Sub(ss)
                                                                             ss.Fill("gold")
                                                                             ss.Stroke("orange")
                                                                             ss.StrokeWidth(5)
                                                                         End Sub)
            End Sub) _
            .Tooltip(Sub(tt) tt.Content("<img src='../../Content/images/{x}.png'/> <b>{seriesName}</b><br />{y}"))
                )
            </div>
        </div>
    </div>


    <!-- customizing axes -->
    <div>
        <h2>Customizing Axes</h2>
        <p>
            Use axis properties to customize the chart's axes, including ranges (minimum and maximum),
            label format, tickmark spacing, and gridlines.
        </p>
        <p>
            The <b>Axis</b> class has boolean properties that allow you to turn features on
            or off (<b>AxisLine</b>, <b>MajorTickMarks</b> and <b>MajorGrid</b>.)
            You can style the appearance of the features that are turned on using CSS.
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#customizeAxesHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#customizeAxesCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="customizeAxesHtml">

@@(Html.C1().FlexChart().Id("chartCustomizeAxes").Bind(Model.CountrySalesData).BindingX("Country") _
    .AxisX(Sub(x) x.AxisLine(True).MajorGrid(True)).AxisY(Sub(y) y.Format("c0").Max(12000000).MajorUnit(1000000) _
        .AxisLine(True).MajorGrid(True)) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
            End Sub) _
    .Tooltip(Sub(tt) tt.Content("&lt;img src='../../Content/images/{x}.png'/&gt; &lt;b&gt;{seriesName}&lt;/b&gt;&lt;br /&gt;{y}"))
                )

                        </div>
                        <div class="tab-pane pane-content" id="customizeAxesCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>                
                @(Html.C1().FlexChart().Id("chartCustomizeAxes").Bind(Model.CountrySalesData).BindingX("Country") _
    .AxisX(Sub(x) x.AxisLine(True).MajorGrid(True)).AxisY(Sub(y) y.Format("c0").Max(12000000).MajorUnit(1000000) _
                        .AxisLine(True).MajorGrid(True)) _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
            End Sub) _
            .Tooltip(Sub(tt) tt.Content("<img src='../../Content/images/{x}.png'/> <b>{seriesName}</b><br />{y}")) _
                )
            </div>
        </div>
    </div>


    <!-- theming -->
    <div>
        <h2>Theming</h2>
        <p>
            The appearance of the FlexChart is defined in CSS. In addition to the default theme, we
            include about a dozen professionally designed themes that customize the appearance of
            all MVC controls to achieve a consistent, attractive look.
        </p>
        <p>
            To customize the appearance of the chart, inspect the elements you want to style and
            create some CSS rules that apply to those elements.
        </p>
        <p>
            For example, if you right-click one of the labels on the X axis in IE or Chrome, you
            will see that it is an element with the "wj-label" class, that it is contained in an
            element with the "wj-axis-x" class, which is contained in the the top-level control
            element, which has the "wj-flexchart" class. The first CSS rule in this example uses
            this information to customize the X labels. The rule selector adds the additional
            requirement that the parent element must be have the "wj-flexchart" <b>and</b> the
            "custom-flex-chart" classes. Without this, the rule would apply to all charts on the
            page.
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#themeHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#themeCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#themeCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="themeHtml">

@@(Html.C1().FlexChart().Id("chartTheme").CssClass("custom-flex-chart").Bind(Model.CountrySalesData).BindingX("Country") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="themeCss">

/* custom chart theme */
.custom-flex-chart.wj-flexchart .wj-axis-x .wj-label,
.custom-flex-chart.wj-flexchart .wj-legend .wj-label {
    font-family: 'Courier New', Courier, monospace;
    font-weight: bold;
}

.custom-flex-chart.wj-flexchart .wj-legend > rect,
.custom-flex-chart.wj-flexchart .wj-plot-area >  rect {
    fill: #f8f8f8;
    stroke: #c0c0c0;
}

                        </div>
                        <div id="themeCS" class="tab-pane pane-content">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live) :  </h4>
                @(Html.C1().FlexChart().Id("chartTheme").CssClass("custom-flex-chart").Bind(Model.CountrySalesData).BindingX("Country") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
            </div>
        </div>
    </div>

    <!-- selection modes -->
    <div>
                <h2>Selection Modes</h2>
        <p>
                The FlexChart allows you to select series or data points by clicking or touching them.
            Use the <b>SelectionMode</b> property to specify whether you want to allow selection
            by series, by data point, or no selection at all (selection is off by default.)
        </p>
        <p>
                Setting the <b>SelectionMode</b> property to <b>Series</b> or <b>Point</b> causes
            the FlexChart to update the <b>Selection</b> property when the user clicks the
            mouse, and to apply the "wj-state-selected" class to selected chart elements.
        </p>
        <p>
                The <b>Selection</b> property returns the currently selected series. To get the
            currently selected data point, get the currently selected item within the
            selected series using the <b>Series.collectionView.currentItem</b> property
            as shown in the example.
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#selectionModeHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#selectionModeJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#selectionModeCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="selectionModeHtml">

@@(Html.C1().FlexChart().Id("chartSelectionMode").OnClientSelectionChanged("chartSelectionMode_SelectionChanged") _
    .Bind(Model.CountrySalesData).BindingX("Country") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
)

&lt;label class="col-md-3 control-label"&gt;Selection Mode&lt;/label&gt;
@@(Html.C1().ComboBox().Id("selectionModeMenu").Bind(Model.Settings("SelectionMode")) _
    .SelectedValue("None").OnClientSelectedIndexChanged("selectionModeMenu_SelectedIndexChanged")
)
&lt;label class="col-md-3 control-label"&gt;Chart Type&lt;/label&gt;
@@(Html.C1().ComboBox().Id("chartTypeMenu").Bind(Model.Settings("ChartType")) _
    .SelectedValue("Column").OnClientSelectedIndexChanged("chartTypeMenu_SelectedIndexChanged")
)

&lt;div id="seriesContainer" style="display:none"&gt;
    &lt;h4&gt;Current Selection&lt;/h4&gt;
    &lt;p&gt;Series: &lt;b id="seriesName"&gt;&lt;/b&gt;&lt;/p&gt;
    &lt;dl id="detailContainer" class="dl-horizontal" style="display:none"&gt;
        &lt;dt&gt;Country&lt;/dt&gt;&lt;dd id="seriesCountry"&gt;&lt;/dd&gt;
        &lt;dt&gt;Sales&lt;/dt&gt;&lt;dd id="seriesSales"&gt;&lt;/dd&gt;
        &lt;dt&gt;Expenses&lt;/dt&gt;&lt;dd id="seriesExpenses"&gt;&lt;/dd&gt;
        &lt;dt&gt;Downloads&lt;/dt&gt;&lt;dd id="seriesDownloads"&gt;&lt;/dd&gt;
    &lt;/dl&gt;
&lt;/div&gt;

                        </div>
                        <div class="tab-pane pane-content" id="selectionModeJs">

var chartSelectionMode = null,
    selectionModeMenu = null;

function InitialControls() {
    //Selection Modes Module  
    chartSelectionMode = &lt;wijmo.chart.FlexChart&gt;wijmo.Control.getControl('#chartSelectionMode'),
        typeMenu = &lt;wijmo.input.comboBox&gt;wijmo.Control.getControl('#chartTypeMenu'),
        selectionModeMenu = &lt;wijmo.input.comboBox&gt;wijmo.Control.getControl('#selectionModeMenu'),
        seriesContainer = document.getElementById('seriesContainer'),
        detailContainer = document.getElementById('detailContainer');
}

//Selection Modes Module
function selectionModeMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        if (!chartSelectionMode) {
            chartSelectionMode = wijmo.Control.getControl('#chartSelectionMode')
        }
        chartSelectionMode.selectionMode = parseInt(sender.selectedIndex);

        // toggle the series panel's visiblity
        if (sender.selectedIndex === 0 || !chartSelectionMode.selection) {
            if (seriesContainer)
                seriesContainer.style.display = 'none';
        }
        else {
            if (seriesContainer)
                seriesContainer.style.display = 'block';
        }

        // toggle the series panel's visiblity
        if (sender.selectedIndex !== 2 || !chartSelectionMode.selection || !chartSelectionMode.selection.collectionView.currentItem) {
            if (detailContainer)
                detailContainer.style.display = 'none';
        }
        else {
            // update the details
            setSeriesDetail(chartSelectionMode.selection.collectionView.currentItem);
        }

    }
}

function chartTypeMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue) {
        if (!chartSelectionMode) {
            chartSelectionMode = wijmo.Control.getControl('#chartSelectionMode')
        }
        chartSelectionMode.chartType = sender.selectedValue;
    }
}

// update details when the FlexChart's selection changes
function chartSelectionMode_SelectionChanged(sender) {
    var currentSelection = sender.selection,
        currentSelectItem;
    if (currentSelection) {
        var seriesContainer = document.getElementById('seriesContainer'),
            selectionModeMenu = &lt;wijmo.input.comboBox&gt; wijmo.Control.getControl('#selectionModeMenu');
        seriesContainer.style.display = 'block'; // show container

        document.getElementById('seriesName').innerHTML = currentSelection.name;
        currentSelectItem = currentSelection.collectionView.currentItem;

        if (currentSelectItem && selectionModeMenu.selectedValue === 'Point') {
            setSeriesDetail(currentSelectItem); // update details
        }
    }
}

// helper method to show details of the FlexChart's current selection
function setSeriesDetail(currentSelectItem) {
    detailContainer.style.display = 'block';
    document.getElementById('seriesCountry').innerHTML = currentSelectItem.Country;
    document.getElementById('seriesSales').innerHTML = wijmo.Globalize.format(currentSelectItem.Sales, 'c2');
    document.getElementById('seriesExpenses').innerHTML = wijmo.Globalize.format(currentSelectItem.Expenses, 'c2');
    document.getElementById('seriesDownloads').innerHTML = wijmo.Globalize.format(currentSelectItem.Downloads, 'n0');
};


                        </div>
                        <div class="tab-pane pane-content" id="selectionModeCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.Settings = CreateIndexSettings()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function

    Private Function CreateIndexSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
            {"ChartType", New Object() {"Column", "Bar", "Scatter", "Line", "LineSymbols", "Area",
                "Spline", "SplineSymbols", "SplineArea"}},            
            {"SelectionMode", New Object() {SelectionMode.None.ToString(), SelectionMode.Series.ToString(), SelectionMode.Point.ToString()}}
        }
        Return settings
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live) :   </h4>
                @(Html.C1().FlexChart().Id("chartSelectionMode").OnClientSelectionChanged("chartSelectionMode_SelectionChanged") _
                    .Bind(Model.CountrySalesData).BindingX("Country") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Selection Mode</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("selectionModeMenu").Bind(Model.Settings("SelectionMode")) _
.SelectedValue("None").OnClientSelectedIndexChanged("selectionModeMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Chart Type</label>
                        <div class="col-md-9">
                            @(Html.C1().ComboBox().Id("chartTypeMenu").Bind(Model.Settings("ChartType")) _
.SelectedValue("Column").OnClientSelectedIndexChanged("chartTypeMenu_SelectedIndexChanged")
                            )
                        </div>
                    </div>
                </div>

                <div id="seriesContainer" style="display:none">
                    <h4>Current Selection</h4>
                    <p>Series : <b id="seriesName"></b></p>
                    <dl id="detailContainer" class="dl-horizontal" style="display:none">
                        <dt>Country</dt>
                        <dd id="seriesCountry"></dd>
                                <dt>Sales</dt>
                        <dd id="seriesSales"></dd>
                                <dt>Expenses</dt>
                        <dd id="seriesExpenses"></dd>
                                <dt>Downloads</dt>
                        <dd id="seriesDownloads"></dd>
                                                                                                                                                                                                </dl>
                </div>
            </div>
        </div>
    </div>


    <!-- toggle series visibility -->
    <div>
                                <h2>Toggle Series</h2>
        <p>
                                The <b>Series</b> class has a <b>Visibility</b> property that allows you to
            determine whether a series should be shown in the chart and in the legend,
            only in the legend, or completely hidden.
        </p>
        <p>
                                This sample shows how you can use the <b>Visibility</b> property to toggle
            the visibility of a series using two methods:
        </p>
        <ol>
                                <li>
                                Clicking on legend entries:<br />
                The chart sets the chart's <b>option.legendToggle</b> property to true,
                which toggles the <b>Visibility</b> property of a series when its legend entry is
                clicked.
            </li>
            <li>
                Using checkboxes:<br />
                When the <b>checked</b> state changed, it will set the <b>Visibility</b> property of each series by the <b>checked</b> state.
            </li>
        </ol>
        <div class="row">
            <div class="col-md-6">
                <div>
                                        <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#legendToggleHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#legendToggleJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#legendToggleCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane pane-content active" id="legendToggleHtml">

@@(Html.C1().FlexChart().Id("chartLegendToggle").LegendToggle(True).Bind(Model.CountrySalesData) _
    .BindingX("Country").OnClientSeriesVisibilityChanged("chartLegendToggle_SeriesVisibilityChanged") _
    .Series(Sub(sers)
                sers.Add().Binding("Sales").Name("Sales")
                sers.Add().Binding("Expenses").Name("Expenses")
                sers.Add().Binding("Downloads").Name("Downloads")
            End Sub)
)
Sales &lt;input id="cbSales" type="checkbox"/&gt;&lt;br /&gt;
Expenses &lt;input id="cbExpenses" type="checkbox"/&gt;&lt;br /&gt;
Downloads &lt;input id="cbDownloads" type="checkbox"/&gt;

                        </div>
                        <div class="tab-pane pane-content" id="legendToggleJs">

function InitialControls() {    
    var chartLegendToggle = &lt;wijmo.chart.FlexChart&gt;wijmo.Control.getControl('#chartLegendToggle'),
        cbSales = document.getElementById('cbSales'),
        cbExpenses = document.getElementById('cbExpenses'),
        cbDownloads = document.getElementById('cbDownloads');    

    // loop through custom check boxes
    ['cbSales', 'cbExpenses', 'cbDownloads'].forEach(function (item, index) {
        // update checkbox and toggle FlexChart's series visibility when clicked
        var el = &lt;HTMLInputElement&gt; document.getElementById(item);
        el.checked = chartLegendToggle.series[index].visibility === wijmo.chart.SeriesVisibility.Visible;
        el.addEventListener('click', function () {
            if (this.checked) {
                chartLegendToggle.series[index].visibility = wijmo.chart.SeriesVisibility.Visible;
            }
            else {
                chartLegendToggle.series[index].visibility = wijmo.chart.SeriesVisibility.Legend;
            }
        });
    });
}
                            
function chartLegendToggle_SeriesVisibilityChanged(sender) {
    // loop through chart series
    sender.series.forEach(function (series) {
        var seriesName = series.name,
            checked = series.visibility === wijmo.chart.SeriesVisibility.Visible;

        // update custom checkbox panel
        var cBox = &lt;HTMLInputElement&gt;document.getElementById('cb' + seriesName);
        cBox.checked = checked;
    });
}

</div>
                        <div class="tab-pane pane-content" id="legendToggleCS">

Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.CountrySalesData = CountryData.GetCountryData()
        Return View(ModelObj)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexChart().Id("chartLegendToggle").LegendToggle(True).Bind(Model.CountrySalesData) _
                    .BindingX("Country").OnClientSeriesVisibilityChanged("chartLegendToggle_SeriesVisibilityChanged") _
                    .Series(Sub(sers)
                                sers.Add().Binding("Sales").Name("Sales")
                                sers.Add().Binding("Expenses").Name("Expenses")
                                sers.Add().Binding("Downloads").Name("Downloads")
                            End Sub)
                )
                Sales <input id="cbSales" type="checkbox" /><br />
                Expenses <input id="cbExpenses" type="checkbox" /><br />
                Downloads <input id="cbDownloads" type="checkbox" />
            </div>
        </div>
    </div>



</div>

<script type="text/javascript">
    c1.documentReady(function () {
        if (window["InitialControls"]) {
            window["InitialControls"]();
        }
    });
</script>