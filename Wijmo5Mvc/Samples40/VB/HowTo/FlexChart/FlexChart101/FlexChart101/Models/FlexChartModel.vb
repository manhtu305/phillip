﻿
Public Class FlexChartModel
    Public Property Settings() As IDictionary(Of String, Object())
        Get
            Return m_Settings
        End Get
        Set
            m_Settings = Value
        End Set
    End Property
    Private m_Settings As IDictionary(Of String, Object())

    Public Property DefaultValues() As IDictionary(Of String, Object)
        Get
            Return m_DefaultValues
        End Get
        Set
            m_DefaultValues = Value
        End Set
    End Property
    Private m_DefaultValues As IDictionary(Of String, Object)

    Public Property CountrySalesData() As IEnumerable(Of CountryData)
        Get
            Return m_CountrySalesData
        End Get
        Set
            m_CountrySalesData = Value
        End Set
    End Property
    Private m_CountrySalesData As IEnumerable(Of CountryData)


    Public Sub New()
    End Sub
End Class

