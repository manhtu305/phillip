Imports Owin
Imports Microsoft.Owin
Imports System.IO

<Assembly: OwinStartupAttribute(GetType(Startup))>

Partial Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        app.UseStorageProviders().AddDiskStorage("UploadFiles", GetFullRoot("UploadFiles"))
        app.UseStorageProviders().AddDiskStorage("~", AppDomain.CurrentDomain.SetupInformation.ApplicationBase)
        app.UseStorageProviders().AddDiskStorage("", AppDomain.CurrentDomain.SetupInformation.ApplicationBase)
    End Sub

    Private Shared Function GetFullRoot(root As String) As String
        Dim applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase
        Dim fullRoot = Path.GetFullPath(Path.Combine(applicationBase, root))
        If Not fullRoot.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal) Then
            ' When we do matches in GetFullPath, we want to only match full directory names.
            fullRoot += Path.DirectorySeparatorChar
        End If

        Return fullRoot
    End Function

End Class
