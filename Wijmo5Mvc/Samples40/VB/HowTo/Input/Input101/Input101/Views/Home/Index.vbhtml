﻿@ModelType InputModel

@Code
    ViewBag.Title = "Input Introduction"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div Class="header">
    <div Class="container">
        <a class="logo-container" href="https://www.grapecity.com/en/aspnet-mvc" target="_blank">
            <i class="gcicon-product-c1"></i>
        </a>
        <h1> Input 101</h1>
        <p>
                This page shows how To Get started With C1 Input controls using VB.NET.
        </p>
    </div>
</div>

<div Class="container">
    <div Class="sample-page download-link">
        <a href = "https://www.grapecity.com/en/download/componentone-studio" > Download Free Trial</a>
    </div>
    <!-- Getting Started -->
    <div>
                        <h2> Getting Started</h2>
        <p>
                        Steps For getting started With Input controls In ASP.NET MVC applications:
        </p>
        <ol>
                        <li> Create a New MVC project Using the C1 ASP.NET MVC application template.</li>
            <li> Add controller And corresponding  view To the project.</li>
            <li> Initialize the input control In view Using razor syntax.</li>
            <li>(Optional) Add some CSS To customize the input control's appearance.</li>
        </ol>
        <div Class="row">
            <div Class="col-md-7">
                <div>
                                <ul Class="nav nav-tabs" role="tablist">
                        <li Class="active"><a href="#gsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li> <a href = "#gsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div Class="tab-content">
                        <div Class="tab-pane active pane-content" id="gsHtml">
&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;body&gt;

        @@(Html.C1().InputNumber().Value(3.5).Step(.5).Format("n"))

    &lt;/body&gt;
&lt;/html&gt;
                        </div>
                        <div Class="tab-pane pane-content" id="gsCS">
                            
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div Class="col-md-5">
                <h4> Result(live) :  </h4>
                @(Html.C1().InputNumber().Value(3.5).Step(0.5).Format("n"))
            </div>
        </div>
    </div>






    <!-- AutoComplete -->
    <div>
        <h2>AutoComplete</h2>
        <p>
            The AutoComplete control is an auto-complete control that allows you to filter its
            item list as you type, as well as select a value directly from its drop-down list.
        </p>
        <p>
            To use the AutoComplete control, you must minimally set the <b>Bind</b>
            property to an array of data in order to populate its item list. The AutoComplete
            control also offers several other properties to alter its behavior, such as the
            <b>CssMatch</b> property. The <b>CssMatch</b> property allows you to specify the CSS
            class that is used to highlight parts of the content that match your search terms.
        </p>
        <p>
            The example below uses List of strings to populate the AutoComplete control's
            item list using the <b>Bind</b> property. To see a list of suggestions, type
            <b>"ab"</b> or <b>"za"</b> in the AutoComplete controls below.
        </p>
        <div class="row">
            <div class="col-md-7">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#acHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#acCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                        <li><a href="#acCss" role="tab" data-toggle="tab">CSS</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="acHtml">
&lt;div&gt;
    &lt;label&gt;Bind Only&lt;/label&gt;
    @@(Html.C1().AutoComplete().Bind(Model.CountryList))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Bind &amp;amp; CssMatch&lt;/label&gt;
    @@(Html.C1().AutoComplete().Bind(Model.CountryList).CssMatch("highlight"))
&lt;/div&gt;
                        </div>
                        <div class="tab-pane pane-content" id="acCS">
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class
                        </div>
                        <div class="tab-pane pane-content" id="acCss">
.highlight {
    background-color: #ff0;
    color: #000;
}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <h4>Result (live):</h4>
                <div class="app-input-group">
                    <label>Bind Only</label>
                    @(Html.C1().AutoComplete().Bind(Model.CountryList).Text(""))
                </div>
                <div class="app-input-group">
                    <label>Bind &amp; cssMatch</label>
                    @(Html.C1().AutoComplete().Bind(Model.CountryList).CssMatch("highlight").Text(""))
                </div>
            </div>
        </div>
    </div>






    <!-- ComboBox -->
    <div>
        <h2>ComboBox</h2>
        <p>
            The ComboBox control is very similar to the AutoComplete control, but rather than
            providing a list of suggestions as you type, the ComboBox will automatically complete
            and select the entry as you type.
        </p>
        <p>
            Like the AutoComplete control, you must minimally set the ComboBox's <b>Bind</b>
            property to an array of data in order to populate its item list. You may also want to
            specify whether the ComboBox is editable via the <b>IsEditable</b> property. The
            <b>IsEditable</b> property determines whether or not a user can enter values that do
            not appear in the ComboBox's item list.
        </p>
        <p>
            The example below uses two ComboBoxes bound to the same data source as the AutoComplete
            control above. The first ComboBox's <b>isEditable</b> property is set to false, while the
            second ComboBox's <b>IsEditable</b> property is set to true.
        </p>
        <div class="row">
            <div class="col-md-7">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#cbHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#cbCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="cbHtml">
&lt;div&gt;
    &lt;label&gt;Non-Editable&lt;/label&gt;
    @@(Html.C1().ComboBox().Bind(Model.CountryList).IsEditable(false))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Editable&lt;/label&gt;
    @@(Html.C1().ComboBox().Bind(Model.CountryList).IsEditable(true))
&lt;/div&gt;
                        </div>
                        <div class="tab-pane pane-content" id="cbCS">
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <h4>Result (live):</h4>
                <div class="app-input-group">
                    <label>Non-Editable</label>
                    @(Html.C1().ComboBox().Bind(Model.CountryList).IsEditable(False))
                </div>
                <div class="app-input-group">
                    <label>Editable</label>
                    @(Html.C1().ComboBox().Bind(Model.CountryList).IsEditable(True))
                </div>
            </div>
        </div>
    </div>



    <!-- InputDate and Calendar -->
    <div>
        <h2>InputDate &amp; Calendar</h2>
        <p>
            The InputDate control allows you to edit and select dates via a drop-down calendar,
            preventing you from entering an incorrect value. The InputDate's drop-down calendar
            was developed as a separate control and can be used be used independently
            from the InputDate control.
        </p>
        <p>
            Both InputDate and Calendar, specify several properties to alter the controls' behavior.
            The most commonly used properties include:
        </p>
        <ul>
            <li>
                <b>Value</b>: Specifies the date value for the control.
            </li>
            <li>
                <b>Min</b>: Specifies the minimum date value that can be entered in the control.
            </li>
            <li>
                <b>Max</b>: Specifies the maximum date value that can be entered in the control.
            </li>
        </ul>
        <p>
            The example below demonstrates how to use each of these properties.
        </p>
        <div class="row">
            <div class="col-md-7">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#idcHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#idcCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="idcHtml">
@@Code
    var today = DateTime.Today.Date
    var minDate = new DateTime(today.Year, 1, 1)
    var maxDate = new DateTime(today.Year, 12, 31)
End Code
&lt;div&gt;
    &lt;label&gt;Bound InputDate with min &amp;amp; max&lt;/label&gt;
    @@(Html.C1().InputDate().Id("idcInputDate").Value(today).Min(minDate).Max(maxDate))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Bound Calendar with min &amp;amp; max&lt;/label&gt;
    @@(Html.C1().Calendar().Id("idcCalendar").Value(today).Min(minDate).Max(maxDate).Width("300px"))
&lt;/div&gt;
&lt;p&gt;
    &lt;b&gt;Valid Range: &lt;span id="idcMinDate"&gt;@@minDate&lt;/span&gt; to &lt;span id="idcMaxDate"&gt;&lt;/span&gt;@@maxDate&lt;/b&gt;
&lt;/p&gt;

                        </div>
                        <div class="tab-pane pane-content" id="idcCS">
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                @Code
                    Dim today = DateTime.Today.Date
                    Dim minDate = New DateTime(today.Year, 1, 1)
                    Dim maxDate = New DateTime(today.Year, 12, 31)
                    Dim minTime = New DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 7, 0, 0)
                    Dim maxTime = New DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 19, 0, 0)
                    Dim format = "MMM dd, yyyy"
                End Code
                <h4>Result(live) :    </h4>
                <div Class="app-input-group">
                    <label> Bound InputDate With Min &amp; Max</label>
                    @(Html.C1().InputDate().Id("idcInputDate").Value(Today).Min(minDate).Max(maxDate))
                </div>
                <div class="app-input-group">
                    <label>Bound Calendar with Min &amp; Max</label>
                    @(Html.C1().Calendar().Id("idcCalendar").Value(Today).Min(minDate).Max(maxDate).Width("300px"))
                </div>
                <p>
                    <b>Valid Range: <span id="idcMinDate">@minDate</span> to <span id="idcMaxDate">@maxDate</span></b>
                </p>
            </div>
        </div>
    </div>





    <!-- InputDate, InputTime and InputDateTime -->
    <div>
        <h2>InputDate, InputTime and InputDateTime</h2>
        <p>
            Similar to the InputDate control, the InputTime control allows you to modify the time portion of
            a TypeScript date. The InputTime control shares many of the same properties as the InputDate control,
            including <b>Format</b>, <b>Min</b>, <b>Max</b>, and <b>Value</b>. The InputTime control also offers a
            <b>Step</b> property that allows you to specify the number of minutes between entries in its drop-down
            list.
        </p>
        <p>
            The InputDateTime control combines the InputDate and InputTime controls, allowing you to set the date
            and time portions. The InputDateTime control has two drop-downs: a Calendar
            for picking dates, and a list for picking times.
        </p>
        <p>
            The example below illustrates how to use the InputTime control in conjunction with the InputDate
            control. Notice that these controls work together to edit the same DateTime TypeScript Object
            and only update the part of the DateTime that they are responsible for.
        </p>
        <p>
            The example also shows an InputDateTime that updates both the date and time parts.
        </p>
        <div class="row">
            <div class="col-md-7">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#iditHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#iditJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#iditCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="iditHtml">
@@Code
    var today = DateTime.Today.Date
    var minDate = new DateTime(today.Year, 1, 1)
    var maxDate = new DateTime(today.Year, 12, 31)
    var minTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 7, 0, 0)
    var maxTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 19, 0, 0)
    var format = "MMM dd, yyyy"
End Code
&lt;div&gt;
    &lt;label&gt;Bound InputDate with Min, Max, &amp;amp; Format&lt;/label&gt;
    @@(Html.C1().InputDate().Id("iditInputDate").Value(today).Min(minDate).Max(maxDate).Format(format).OnClientValueChanged("inputDate_ValueChanged"))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Bound InputTime with Min, Max, &amp;amp; Step&lt;/label&gt;
    @@(Html.C1().InputTime().Id("iditInputTime").Value(today).Min(minTime).Max(maxTime).Step(15).OnClientValueChanged("inputTime_ValueChanged"))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Bound InputDateTime with Min, Max, TimeMin, TimeMax, Format, &amp;amp; TimeStep&lt;/label&gt;
    @@(Html.C1().InputDateTime().Id("iditInputDateTime").Value(today).Min(minDate).Max(maxDate).TimeMin(minTime).TimeMax(maxTime).TimeStep(15).OnClientValueChanged("inputDateTime_ValueChanged"))
&lt;/div&gt;
&lt;p&gt;
    &lt;b&gt;Selected Date &amp; Time: &lt;span id="iditSelectedValue"&gt;&lt;/span&gt;&lt;/b&gt;
&lt;/p&gt;
                        </div>
                        <div class="tab-pane pane-content" id="iditJs">
function inputTime_ValueChanged(sender) {
    var inputDateTime = &lt;wijmo.input.InputDateTime&gt;wijmo.Control.getControl("#iditInputDateTime");
    inputDateTime.value = wijmo.DateTime.fromDateTime(inputDateTime.value, sender.value);
}

function inputDate_ValueChanged(sender) {
    var inputDateTime = &lt;wijmo.input.InputDateTime&gt;wijmo.Control.getControl("#iditInputDateTime");
    inputDateTime.value = wijmo.DateTime.fromDateTime(sender.value, inputDateTime.value);
}

function inputDateTime_ValueChanged(sender) {
    var inputDate = &lt;wijmo.input.InputDate&gt;wijmo.Control.getControl("#iditInputDate");
    var inputTime = &lt;wijmo.input.InputTime&gt;wijmo.Control.getControl("#iditInputTime");
    inputDate.value = wijmo.DateTime.fromDateTime(sender.value, inputDate.value);
    inputTime.value = wijmo.DateTime.fromDateTime(inputTime.value, sender.value);
    document.getElementById('iditSelectedValue').innerHTML = wijmo.Globalize.format(sender.value, 'MMM dd, yyyy h:mm:ss tt');
}
                        </div>
                        <div class="tab-pane pane-content" id="iditCS">
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <h4>Result (live):</h4>
                <div class="app-input-group">
                    <label>Bound InputDate with Min, Max, &amp; Format</label>
                    @(Html.C1().InputDate().Id("iditInputDate").Value(Today).Min(minDate).Max(maxDate).Format(Format).OnClientValueChanged("inputDate_ValueChanged"))
                </div>
                <div class="app-input-group">
                    <label>Bound InputTime with Min, Max, &amp; Step</label>
                    @(Html.C1().InputTime().Id("iditInputTime").Value(Today).Min(minTime).Max(maxTime).Step(15).OnClientValueChanged("inputTime_ValueChanged"))
                </div>
                <div class="app-input-group">
                    <label>Bound InputDateTime with Min, Max, TimeMin, TimeMax, Format, &amp; TimeStep</label>
                    @(Html.C1().InputDateTime().Id("iditInputDateTime").Value(Today).Min(minDate).Max(maxDate).TimeStep(15).Format("MMM dd, yyyy hh:mm tt").TimeMin(minTime).TimeMax(maxTime).OnClientValueChanged("inputDateTime_ValueChanged"))
                </div>
                <p>
                    <b>Selected Date &amp; Time: <span id="iditSelectedValue"></span></b>
                </p>
            </div>
        </div>
    </div>




    <!-- ListBox -->
    <div>
        <h2>ListBox</h2>
        <p>
            The ListBox control displays a list of items and allows you to select items using your
            mouse and keyboard. Like the AutoComplete and ComboBox controls, you must specify the
            ListBox's <b>Bind</b> property in order to use the control.
        </p>
        <p>
            The example below allows you to select an item within the ListBox control, and also displays
            the control's <b>SelectedIndex</b> and <b>SelectedValue</b> properties.
        </p>
        <div class="row">
            <div class="col-md-7">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#lbHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#lbJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#lbCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="lbHtml">
&lt;div&gt;
    @@(Html.C1().ListBox().Id("lbListBox").Height("150px").Width("250px").Bind(Model.CitiesList).OnClientSelectedIndexChanged("selectedIndexChanged"))
&lt;/div&gt;
&lt;p&gt;
    &lt;b&gt;selectedIndex: &lt;span id="lbSelIdx"&gt;&lt;/span&gt;&lt;/b&gt;
&lt;/p&gt;
&lt;p&gt;
    &lt;b&gt;selectedValue: &lt;span id="lbSelVal"&gt;&lt;/span&gt;&lt;/b&gt;
&lt;/p&gt;
                        </div>
                        <div class="tab-pane pane-content" id="lbJs">
function InitialControls() {
    var ListBox = &lt;wijmo.input.ListBox&gt;wijmo.Control.getControl("#lbListBox");
    selectedIndexChanged(ListBox);
}

//selectedIndexChanged event handler
function selectedIndexChanged(sender) {
    //set selectedIndex and selectedValue text
    if (document.getElementById("lbListBox") && document.getElementById("lbSelIdx") && document.getElementById("lbSelVal")) {//if (document.readyState === "complete") {
        document.getElementById('lbSelIdx').innerHTML = sender.selectedIndex;
        document.getElementById('lbSelVal').innerHTML = sender.selectedValue;
    }
}
                        </div>
                        <div class="tab-pane pane-content" id="lbCS">
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <h4>Result (live):</h4>
                <div class="app-input-group">
                    @(Html.C1().ListBox().Id("lbListBox").Height("150px").Width("250px") _
                    .Bind(Model.CitiesList).OnClientSelectedIndexChanged("selectedIndexChanged"))
                </div>
                <p>
                    <b>selectedIndex: <span id="lbSelIdx"></span></b>
                </p>
                <p>
                    <b>selectedValue: <span id="lbSelVal"></span></b>
                </p>
            </div>
        </div>
    </div>




    <!-- InputNumber -->
    <div>
        <h2>InputNumber</h2>
        <p>
            The InputNumber control allows you to edit numbers, preventing you from entering invalid
            data and optionally formatting the numeric value as it is edited. The InputNumber can be
            used without specifying any of its properties; however, you'll typically want to bind it
            to some data using the <b>Value</b> property.
        </p>
        <p>
            In addition to the <b>value</b> property, the InputNumber control offers several other
            properties that can be used to alter its behavior, such as:
        </p>
        <ul>
            <li>
                <b>Min</b>: Specifies the minimum numeric value that can be entered.
            </li>
            <li>
                <b>Max</b>: Specifies the maximum numeric value that can be entered.
            </li>
            <li>
                <b>Step</b>: Specifies the amount to add or subtract from the current
                value when the spinner buttons are clicked.
            </li>
            <li>
                <b>Format</b>: Specifies the numeric format used to display the number being
                edited. The format property uses a .NET-style
                <a href="http://msdn.microsoft.com/en-us/library/dwhawy9k(v=vs.110).aspx" target="_blank">numeric format string</a>.
            </li>
        </ul>
        <p>
            The example below demonstrates how to use all of these properties.
        </p>
        <div class="row">
            <div class="col-md-7">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#inHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#inCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="inHtml">
&lt;div&gt;
    &lt;label&gt;Unbound with "n0" format&lt;/label&gt;
    @@(Html.C1().InputNumber().Id("inInputNumber1").Value(0).Format("n0"))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Bound with "n" format&lt;/label&gt;
    @@(Html.C1().InputNumber().Id("inInputNumber2").Value(Math.PI).Format("n"))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Bound with min (0), max (10), step, and "c2" format&lt;/label&gt;
    @@(Html.C1().InputNumber().Id("inInputNumber3").Value(3.5).Format("C2").Step(.5).Min(0).Max(10))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Unbound with placeholder and required="false"&lt;/label&gt;
    @@(Html.C1().InputNumber().Id("inInputNumber4").Placeholder("Enter a number...").Required(false).Value(Nothing))
&lt;/div&gt;
                        </div>
                        <div class="tab-pane pane-content" id="inCS">
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <h4>Result (live):</h4>
                <div class="app-input-group">
                    <label>Unbound with "n0" format</label>
                    @(Html.C1().InputNumber().Id("inInputNumber1").Value(0).Format("n0"))
                </div>
                <div class="app-input-group">
                    <label>Bound with "n" format</label>
                    @(Html.C1().InputNumber().Id("inInputNumber2").Value(Math.PI).Format("n"))
                </div>
                <div class="app-input-group">
                    <label>Bound with min (0), max (10), step, and "c2" format</label>
                    @(Html.C1().InputNumber().Id("inInputNumber3").Value(3.5).Format("C2").Step(0.5).Min(0).Max(10))
                </div>
                <div class="app-input-group">
                    <label>Unbound with placeholder and required="false"</label>
                    @(Html.C1().InputNumber().Id("inInputNumber4").Placeholder("Enter a number...").Required(False).Value(Nothing))
                </div>
            </div>
        </div>
    </div>





    <!-- InputMask -->
    <div>
        <h2>InputMask</h2>
        <p>
            The InputMask control allows you to validate and format user input as it is entered, preventing
            invalid data.  The InputMask control can be used without specifying any of its properties; however,
            you will typically set its <b>Value</b> and <b>Mask</b> properties.  Like the other MVC input
            controls, the <b>Value</b> property specifies the value for the InputMask control.  The <b>Mask</b> property
            specifies the control's mask and supports a combination of the following characters:
        </p>
        <dl class="dl-horizontal">
            <dt>0</dt>
            <dd>Digit.</dd>
            <dt>9</dt>
            <dd>Digit or space.</dd>
            <dt>#</dt>
            <dd>Digit, sign, or space.</dd>
            <dt>L</dt>
            <dd>Letter.</dd>
            <dt>l</dt>
            <dd>Letter or space.</dd>
            <dt>A</dt>
            <dd>Alphanumeric.</dd>
            <dt>a</dt>
            <dd>Alphanumeric or space.</dd>
            <dt>.</dt>
            <dd>Localized decimal point.</dd>
            <dt>,</dt>
            <dd>Localized thousand separator.</dd>
            <dt>:</dt>
            <dd>Localized time separator.</dd>
            <dt>/</dt>
            <dd>Localized date separator.</dd>
            <dt>$</dt>
            <dd>Localized currency symbol.</dd>
            <dt>&lt;</dt>
            <dd>Converts characters that follow to lowercase.</dd>
            <dt>&gt;</dt>
            <dd>Converts characters that follow to uppercase.</dd>
            <dt>|</dt>
            <dd>Disables case conversion.</dd>
            <dt>\</dt>
            <dd>Escapes any character, turning it into a literal.</dd>
            <dt>９ (\uff19)</dt>
            <dd>DBCS Digit.</dd>
            <dt>Ｊ (\uff2a)</dt>
            <dd>DBCS Hiragana.</dd>
            <dt>Ｇ (\uff27)</dt>
            <dd>DBCS big Hiragana.</dd>
            <dt>Ｋ (\uff2b)</dt>
            <dd>DBCS Katakana.</dd>
            <dt>Ｎ (\uff2e)</dt>
            <dd>DBCS big Katakana.</dd>
            <dt>K</dt>
            <dd>SBCS Katakana.</dd>
            <dt>N</dt>
            <dd>SBCS big Katakana.</dd>
            <dt>Ｚ (\uff3a)</dt>
            <dd>Any DBCS character.</dd>
            <dt>H</dt>
            <dd>Any SBCS character.</dd>
            <dt>All others</dt>
            <dd>Literals.</dd>
        </dl>
        <p>
            The examples below demonstrates how to use the <b>Value</b> and <b>Mask</b> properties with the
            InputMask, InputDate, and InputTime controls.
        </p>
        <div class="row">
            <div class="col-md-7">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#imHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#imJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#imCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="imHtml">
&lt;div&gt;
    &lt;label&gt;Social Security Number&lt;/label&gt;
    @@(Html.C1().InputMask().Id("imSocial").Mask("000-00-0000"))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Phone Number&lt;/label&gt;
    @@(Html.C1().InputMask().Id("imPhone").Mask("(999) 000-0000"))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;Try your own&lt;/label&gt;
    @@(Html.C1().InputMask().Id("imCustomInput") _
        .Placeholder("Enter an input mask...") _
        .OnClientValueChanged("MaskvalueChanged")
    )
    @@(Html.C1().InputMask().Id("imCustomTrial") _
        .Placeholder("Try your input mask...")
    )
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;InputDate with Mask&lt;/label&gt;
    @@(Html.C1().InputDate().Value(DateTime.Now) _
        .Format("MM/dd/yyyy").Mask("99/99/9999"))
&lt;/div&gt;
&lt;div&gt;
    &lt;label&gt;InputTime with Mask&lt;/label&gt;
    @@(Html.C1().InputTime().Value(DateTime.Now) _
        .Format("hh:mm tt").Mask("00:00 &gt;LL"))
&lt;/div&gt;
                        </div>
                        <div class="tab-pane pane-content" id="imJs">
// InputMask valueChanged event handler
function MaskvalueChanged(sender) {
    var customMaskTrial = &lt;wijmo.input.InputMask&gt; wijmo.Control.getControl("#imCustomTrial");
    customMaskTrial.mask = sender.value;
    customMaskTrial.hostElement.title = 'Mask: ' + (sender.value || 'N/A');
}

                        </div>
                        <div class="tab-pane pane-content" id="imCS">
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <h4>Result (live):</h4>
                <div class="app-input-group">
                    <label>Social Security Number</label>
                    @(Html.C1().InputMask().Id("imSocial").Mask("000-00-0000"))
                </div>
                <div class="app-input-group">
                    <label>Phone Number</label>
                    @(Html.C1().InputMask().Id("imPhone").Mask("(999) 000-0000"))
                </div>
                <div class="app-input-group">
                    <label>Try your own</label>
                    @(Html.C1().InputMask().Id("imCustomInput").Placeholder("Enter an input mask...") _
                        .OnClientValueChanged("MaskvalueChanged")
                    )
                    @(Html.C1().InputMask().Id("imCustomTrial") _
                .Placeholder("Try your input mask...")
                    )
                </div>
                <div class="app-input-group">
                    <label>InputDate with Mask</label>
                    @(Html.C1().InputDate().Value(DateTime.Now).Format("MM/dd/yyyy").Mask("99/99/9999"))
                </div>
                <div class="app-input-group">
                    <label>InputTime with Mask</label>
                    @(Html.C1().InputTime().Id("itMask").Value(DateTime.Now).Format("hh:mm tt").Mask("00:00 >LL"))
                </div>
            </div>
        </div>
    </div>


    <!-- Menu -->
    <div>
        <h2>Menu</h2>
        <p>
            The Menu control allows you to create a simple drop-down list with clickable items. The Menu's
            items can be defined directly or by using the <b>Bind</b> property similar to the ComboBox.
            To specify the text displayed on the Menu, you can set the <b>Header</b> property.
        </p>
        <p>
            The Menu control offers two ways to handle user selections, specifying a command on each menu item
            and the <b>ItemClicked</b> event. Unlike the <b>ItemClicked</b> event, commands are objects that
            implement two methods:
        </p>
        <ul>
            <li>
                <b>executeCommand(param)</b>: A method that executes the command.
            </li>
            <li>
                <b>canExecuteCommand(param)</b>: A method that returns a Boolean value specifying whether or
                not the command can be executed. If the return value is false, the menu item is disabled automatically.
            </li>
        </ul>
        <p>
            The example below demonstrates how to use both approaches.
        </p>
        <div class="row">
            <div class="col-md-7">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#mHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#mJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#mCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="mHtml">
&lt;div&gt;
&lt;label&gt;itemClicked Event&lt;/label&gt;

    @@(Html.C1().Menu().Id("mFileMenu").Header("File").OnClientItemClicked("itemClicked") _
        .MenuItems(Sub(items)
                        items.Add(Sub(item) item.Header("New: create a new document"))
                        items.Add(Sub(item) item.Header("Open: load an existing document from a file"))
                        items.Add(Sub(item) item.Header("Save: save the current document to a file"))
                        items.Add(Sub(item) item.IsSeparator(True))
                        items.Add(Sub(item) item.Header("Exit: save changes and exit the application"))
                    End Sub))
    @@(Html.C1().Menu().Id("mEditMenu").Header("Edit").OnClientItemClicked("itemClicked") _
        .MenuItems(Sub(items)
                        items.Add().Header("Cut: move the current selection to the clipboard")
                        items.Add().Header("Copy: copy the current selection to the clipboard")
                        items.Add().Header("Paste: insert the clipboard content at the cursor position")
                        items.Add().IsSeparator(True)
                        items.Add().Header("Find: search the current document for some text")
                        items.Add().Header("Replace: replace occurrences of a string in the current document")
                    End Sub))

&lt;/div&gt;
&lt;div class="app-input-group"&gt;
    &lt;label&gt;Commands&lt;/label&gt;
    @@(Html.C1().Menu().Header("Change Tax") _
        .Command(Sub(cmd) cmd.ExecuteCommand("execute").CanExecuteCommand("canExecute")) _
        .MenuItems(Sub(items)
            items.Add().Header("+ 25%").CommandParameter(0.25)
            items.Add().Header("+ 10%").CommandParameter(0.1)
            items.Add().Header("+ 5%").CommandParameter(0.05)
            items.Add().Header("+ 1%").CommandParameter(0.01)
            items.Add().IsSeparator(True)
            items.Add().Header("- 1%").CommandParameter(-0.01)
            items.Add().Header("- 5%").CommandParameter(-0.05)
            items.Add().Header("- 10%").CommandParameter(-0.1)
            items.Add().Header("- 25%").CommandParameter(-0.25)
        End Sub)
    )
@@(Html.C1().InputNumber().Id("mInputNumber") _
    .Value(0.07).Step(0.05).Format("p0").Min(0).Max(1)
)
&lt;/div&gt;
                        </div>
                        <div class="tab-pane pane-content" id="mJs">
function itemClicked(sender) {
    alert('You\'ve selected option ' + sender.selectedIndex + ' from the ' + sender.header + ' menu!');
}

function execute(arg) {
    var inputNumber = &lt;wijmo.input.InputNumber&gt;wijmo.Control.getControl("#mInputNumber");

    // convert argument to Number
    arg = wijmo.changeType(arg, wijmo.DataType.Number,'');

    // check if the conversion was successful
    if (wijmo.isNumber(arg)) {

        // update the value
        inputNumber.value += arg;
    }
}

function canExecute(arg) {
    var inputNumber = &lt;wijmo.input.InputNumber&gt;wijmo.Control.getControl("#mInputNumber");

    // convert argument to Number
    arg = wijmo.changeType(arg, wijmo.DataType.Number,'');

    // check if the conversion was successful
    if (wijmo.isNumber(arg)) {
        var val = inputNumber.value + arg;

        // check if the value is valid
        return val >= 0 && val <= 1;
    }

    return false;
}
                        </div>
                        <div class="tab-pane pane-content" id="mCS">
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Function Index() As ActionResult
        Return View(New InputModel())
    End Function
End Class
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <h4>Result (live):</h4>
                <div class="app-input-group">
                    <label>itemClicked Event</label>
                    @(Html.C1().Menu().Id("mFileMenu").Header("File").OnClientItemClicked("itemClicked") _
                        .MenuItems(Sub(items)
                                       items.Add(Sub(item) item.Header("New: create a new document"))
                                       items.Add(Sub(item) item.Header("Open: load an existing document from a file"))
                                       items.Add(Sub(item) item.Header("Save: save the current document to a file"))
                                       items.Add(Sub(item) item.IsSeparator(True))
                                       items.Add(Sub(item) item.Header("Exit: save changes and exit the application"))
                                   End Sub))
                    @(Html.C1().Menu().Id("mEditMenu").Header("Edit").OnClientItemClicked("itemClicked") _
                    .MenuItems(Sub(items)
                                   items.Add().Header("Cut: move the current selection to the clipboard")
                                   items.Add().Header("Copy: copy the current selection to the clipboard")
                                   items.Add().Header("Paste: insert the clipboard content at the cursor position")
                                   items.Add().IsSeparator(True)
                                   items.Add().Header("Find: search the current document for some text")
                                   items.Add().Header("Replace: replace occurrences of a string in the current document")
                               End Sub)
                    )
                </div>
                <div class="app-input-group">
                    <label>Commands</label>
                    @(Html.C1().Menu().Header("Change Tax") _
                    .Command(Sub(cmd) cmd.ExecuteCommand("execute").CanExecuteCommand("canExecute")) _
                    .MenuItems(Sub(items)
                                   items.Add().Header("+ 25%").CommandParameter(0.25)
                                   items.Add().Header("+ 10%").CommandParameter(0.1)
                                   items.Add().Header("+ 5%").CommandParameter(0.05)
                                   items.Add().Header("+ 1%").CommandParameter(0.01)
                                   items.Add().IsSeparator(True)
                                   items.Add().Header("- 1%").CommandParameter(-0.01)
                                   items.Add().Header("- 5%").CommandParameter(-0.05)
                                   items.Add().Header("- 10%").CommandParameter(-0.1)
                                   items.Add().Header("- 25%").CommandParameter(-0.25)
                               End Sub)
                    )
                    @(Html.C1().InputNumber().Id("mInputNumber") _
                       .Value(0.07).Step(0.05).Format("p0").Min(0).Max(1)
                    )
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    c1.documentReady(function () {
        if (window["InitialControls"]) {
            window["InitialControls"]();
        }
    });
</script>