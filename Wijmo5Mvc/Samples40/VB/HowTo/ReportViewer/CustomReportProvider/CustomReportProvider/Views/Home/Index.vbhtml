@Code
    ViewBag.Title = "ReportViewer Custom Report Provider"
End Code

<div class="header ">
    <div class="container">
        <a class="logo-container" href="https://www.grapecity.com/en/aspnet-mvc" target="_blank">
            <i class="gcicon-product-c1"></i>
        </a>
        <h1>
            ReportViewer Custom Report Provider
        </h1>
        <p>
            This page shows how to set the dataset in memory to FlexReport.
        </p>
    </div>
</div>

<div Class="container">
    <div Class="sample-page download-link">
        <a href = "https://www.grapecity.com/en/download/componentone-studio" > Download Free Trial</a>
    </div>
    <!-- Getting Started -->
    <div>
        <h2> Getting Started</h2>
        <p>
            Steps For getting started With the Custom Report Provider application:
        </p>
        <ol>
            <li> Implement a custom report provider which sets the FlexReport's data source with the dataset in memory.</li>
            <li> Register this report provider In Startup.</li>
            <li> Create a ReportViewer control And Set its FilePath And ReportName properties.</li>
            <li> Add handler For client queryLoadingData Event To provide selected country On loading the report.</li>
        </ol>
        <div Class="row">
            Select Case Country:
            <span>
                @(Html.C1().ComboBox() _
                    .Id("countriesMenu") _
                    .Bind(Employees.CountryNames).SelectedIndex(0) _
                    .OnClientSelectedIndexChanged("countriesMenu_selectedIndexChanged"))
            </span>
        </div>
        <div Class="row">
            @(Html.C1().ReportViewer() _
                        .Id("reportViewer") _
                        .Width("100%") _
                        .FilePath("memoryreports/ReportsRoot/DataSourceInMemory.flxr") _
                        .ReportName("Simple List") _
                        .OnClientQueryLoadingData("reportViewer_queryLoadingData"))
        </div>
    </div>
</div>