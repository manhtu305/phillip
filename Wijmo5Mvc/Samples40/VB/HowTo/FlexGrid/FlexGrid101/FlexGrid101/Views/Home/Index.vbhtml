﻿@Imports MVCFlexGrid101
@ModelType MVCFlexGrid101.FlexGridModel 

@Code
    ViewBag.Title = "FlexGrid Introduction"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code


<div Class="header">
    <div Class="container">
        <a class="logo-container" href="https://www.grapecity.com/en/aspnet-mvc" target="_blank">
            <i class="gcicon-product-c1"></i>
        </a>
        <h1>
                FlexGrid 101
        </h1>
        <p>
                This page shows how To Get started With C1 FlexGrid controls using VB.NET.
        </p>
    </div>
</div>

<div Class="container">
    <div Class="sample-page download-link">
        <a href = "https://www.grapecity.com/en/download/componentone-studio" > Download Free Trial</a>
    </div>
    <!-- Getting Started -->
    <div>
                        <h2> Getting Started</h2>
        <p>
                        Steps For getting started With the FlexGrid control In MVC applications:
        </p>
        <ol>
                        <li> Create a New MVC project Using the C1 ASP.NET MVC application template.</li>
            <li> Add controller And corresponding view To the project.</li>
            <li> Initialize the FlexGrid control In view Using razor syntax.</li>
            <li>(Optional) Add some CSS To customize the FlexGrid control's appearance.</li>
        </ol>
        <p>
                        This will create a FlexGrid With Default behavior, which includes
            automatic column generation, column sorting And reordering, editing,
            And clipboard support.
        </p>
        <div Class="row">
            <div Class="col-md-6">
                <div>
                                <ul Class="nav nav-tabs" role="tablist">
                        <li Class="active"><a href="#gsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li> <a href = "#gsCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li> <a href = "#gsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div Class="tab-content">
                        <div Class="tab-pane active pane-content" id="gsHtml">
&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;

    &lt;/head&gt;
    &lt;body&gt;
        &lt;!-- this Is the grid --&gt;
        @@(Html.C1().FlexGrid().Id("gsFlexGrid").IsReadOnly(True).AllowSorting(True) _
            .Bind(Model.CountryData).AutoGenerateColumns(True))

    &lt;/body&gt;
&lt;/html&gt;

                        </div>
                        <div Class="tab-pane pane-content" id="gsCss">

/* set default grid style */
.wj-flexgrid {
    height: 300px;
    background-color: white;
    box-shadow: 4px 4px 10px 0px rgba(50, 50, 50, 0.75);
    margin-bottom: 12px;
}

                        </div>
                        <div Class="tab-pane pane-content" id="gsCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function
End Class



                            </div>
                        </div>
                </div>
            </div>
            <div Class="col-md-6">
                <h4> Result(live) :              </h4>                
                    @(Html.C1().FlexGrid().Id("gsFlexGrid").IsReadOnly(True).AllowSorting(True) _
                .Bind(Model.CountryData).AutoGenerateColumns(True))
            </div>
        </div>
    </div>

    <!-- column definitions -->
    <div>
        <h2>Column Definitions</h2>
        <p>
            The Getting Started example did not define any columns, so FlexGrid generated them
            automatically.
        </p>
        <p>
            This example shows how you can define the columns using the FlexGrid's <b>Columns</b> property.
        </p>
        <p>
            Specifying the columns allows you to choose which columns to show, and in what order.
            This also gives you control over each column&#39;s Width, Heading, Formatting, Alignment,
            and other properties.
        </p>
        <p>
            In this case, we use star sizing to set the width of the "Country" column. This tells the
            column to stretch to fill the available width of the grid so there is no empty space. On
            the "Amount" column, we set the format property to "n0",  which results in numbers with thousand separators and no decimal digits. On
            the "Discount" column, we set the format property to "p0", which results in numbers with
            percentage and no decimal digits.
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#cdHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#cdCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="cdHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("cdInitMethod").IsReadOnly(True).AutoGenerateColumns(False).AllowSorting(True) _
    .Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
        bl.Add(Sub(cb) cb.Binding("End").Header("End").Format("HH:mm"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        bl.Add(Sub(cb) cb.Binding("Amount2"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
        bl.Add(Sub(cb) cb.Binding("Active"))
    End Sub)
)
                        </div>
                        <div class="tab-pane pane-content" id="cdCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("cdInitMethod").IsReadOnly(True).AutoGenerateColumns(False).AllowSorting(True) _
                    .Bind(Model.CountryData).CssClass("grid") _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                                 bl.Add(Sub(cb) cb.Binding("End").Header("End").Format("HH:mm"))
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                                 bl.Add(Sub(cb) cb.Binding("Amount2"))
                                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                                 bl.Add(Sub(cb) cb.Binding("Active"))
                             End Sub)
                )
            </div>
        </div>
    </div>


    <!-- selection modes -->
    <div>
        <h2>Selection Modes</h2>
        <p>
            By default, FlexGrid allows you to select a range of cells with the mouse or keyboard,
            just like Excel. The <b>SelectionMode</b> property allows you to change that so that you
            can select a Row, a Range of Rows, Non-Contiguous Rows (like in a List-Box), a Single Cell, a Range of Cells
            or disable selection altogether.
        </p>
        <p>
            This example allows you to pick the <b>SelectionMode</b> from a C1 ASP.Net MVC's ComboBox control.
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#smHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#smJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#smCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="smHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("smFlexGrid").IsReadOnly(True).AutoGenerateColumns(False) _
    .AllowSorting(True).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.None).Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
    End Sub)
)
&lt;br /&gt;Selection Mode
@@(Html.C1().ComboBox().Id("smMenu").Bind(Model.Settings("SelectionMode")) _
    .SelectedIndex(0).OnClientSelectedIndexChanged("smMenu_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="smJs">

var smFlexGrid = null;

function InitialControls() {
    //Selection Modes Modules
    smFlexGrid = &lt;wijmo.grid.FlexGrid&gt;wijmo.Control.getControl("#smFlexGrid");
}

//Selection Modes Modules
function smMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue != null && smFlexGrid != null) {
        smFlexGrid.selectionMode = sender.selectedValue;
    }
}

                        </div>
                        <div class="tab-pane pane-content" id="smCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.Settings = CreateSettings()
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function

    Private Function CreateSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
        {"SelectionMode", New Object() {SelectionMode.None.ToString(), SelectionMode.Cell.ToString(), SelectionMode.CellRange.ToString(), SelectionMode.Row.ToString(), SelectionMode.RowRange.ToString(), SelectionMode.ListBox.ToString()}},        
    }
        Return settings
    End Function
    
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live) :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("smFlexGrid").IsReadOnly(True).AutoGenerateColumns(False) _
                .AllowSorting(True).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.None).Bind(Model.CountryData).CssClass("grid") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
                         End Sub)
                )
                <br />Selection Mode
                @(Html.C1().ComboBox().Id("smMenu").Bind(Model.Settings("SelectionMode")) _
                 .SelectedIndex(0).OnClientSelectedIndexChanged("smMenu_SelectedIndexChanged")
                )
            </div>
        </div>
    </div>



    <!-- editing -->
    <div>
                <h2>Editing</h2>
        <p>
                FlexGrid has built-in support for fast, in-cell editing like you find in Excel. There is no
            need to add extra columns with Edit buttons that switch between display and edit modes.
        </p>
        <p>
                Users can start editing by typing into any cell. This puts the cell in quick-edit mode.
            In this mode, pressing a cursor key finishes the editing and moves the selection to a different cell.
        </p>
        <p>
                Another way to start editing is by pressing F2 or by clicking a cell twice. This puts the cell in
            full-edit mode. In this mode, pressing a cursor key moves the caret within the cell text.
            To finish editing and move to another cell, the user must press the Enter, Tab, or Escape key.
        </p>
        <p>
                Data is automatically coerced to the proper type when editing finishes. If the user enters invalid
            data, the edit is cancelled and the original data remains in place.
        </p>
        <p>
                There are two modes for updating the data to the server.
        </p>
        <ol>
                <li>
                By default, the update operation will be commit to the server once finishing editing.
                <p>
                <b>Notices :        </b>
                    If the user wants to commit the update operation to the datasource server, the Update, Delete or Create action url should be provided.
                    And the corresponding codes used to update the datasource should be written in the corresponding action.
                </p>
            </li>
            <li>
                The other mode is called BatchEdit. The user can update, create or remove multiple items.
                Once these modifications are confirmed, They could be commit to the data source only once.
                Now these modifications can be commit by the commit method of CollectionView in client side.
                The user can also commit them by sorting, paging or filtering behavior.
                <p>
                <b>Notices :        </b>
                    In this mode, the BatchEdit action url should be provided and the corresponding codes used to update the datasource should be written.
                </p>
            </li>
        </ol>
        <p>
                You can disable editing at the Grid, Column, or Row levels using the <b>IsReadOnly</b> property of the
            Grid, Column, or Row objects. In this example, we make the ID column read-only.
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                        <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#eHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#eCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="eHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("eFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
    .CssClass("grid") _
    .Bind(Sub(bl)
            bl.Bind(Url.Action("GridRead"))
            bl.Update(Url.Action("EGridUpdate"))
        End Sub) _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*").IsReadOnly(True))
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="eCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function    

    Public Function GridRead(<C1JsonRequest> requestData As CollectionViewRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Read(requestData, FlexGridModel.Source))
    End Function

    Public Function EGridUpdate(<C1JsonRequest> requestData As CollectionViewEditRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Edit(Of Sale)(requestData, Function(sale)
            Dim [error] As String = String.Empty
            Dim success As Boolean = True
            Dim fSale = FlexGridModel.Source.Find(Function(item) item.ID = sale.ID)
            fSale.Country = sale.Country
            fSale.Amount = sale.Amount
            fSale.Discount = sale.Discount
            fSale.Active = sale.Active
            Dim RetValue = New CollectionViewItemResult(Of Sale)
            RetValue.Error = [error]
            RetValue.Success = success AndAlso ModelState.IsValid
            RetValue.Data = fSale
            Return RetValue
        End Function, Function() FlexGridModel.Source))
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live) :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("eFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
                .CssClass("grid") _
                .Bind(Sub(bl)
                          bl.Bind(Url.Action("GridRead"))
                          bl.Update(Url.Action("EGridUpdate"))
                      End Sub) _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*").IsReadOnly(True))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- grouping -->
    <div>
                    <h2>
                    Grouping
        </h2>
        <p>
                    FlexGrid supports grouping of client side data, set the <b>GroupBy</b> property to a column name for grouping the data according to a column.
        </p>
        <p>
                    Internally, FlexGrid supports grouping through the client side <b>IItemsSource</b> interface. To enable grouping at client side, add one or more <b>GroupDescription</b> objects to the
            <b>itemsSource.groupDescriptions</b> property. It is easy to let the grid grouped by some field by calling the method GroupBy of FlexGridBuilder with the corresponding field name. And ensure that the grid's <b>ShowGroups</b> property
            is set to true (the default value).
            <b>GroupDescription</b> objects are flexible, allowing you to group data based on value or on grouping
            functions. The example below groups dates by year; amounts by range returning three ranges: over 5,000,
            1,000 to 5,000, 500 to 1,000, and under 500; and anything else by value. Use the menu to see the effects of each grouping.
        </p>
        <p>
                    Notice that the "Amount" column displays the totals in the group rows. We do this by
            setting the column's <b>Aggregate</b> property to "Sum." The aggregate is automatically
            updated when you edit the values in the column.
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                            <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#gHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#gJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#gCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="gHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("gFlexGrid").AutoGenerateColumns(False) _
    .AllowSorting(True).IsReadOnly(True).GroupBy("Country").Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Product").Header("Product").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Color").Header("Color").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Header("Amount").Format("n0").Width("*") _
                            .Aggregate(C1.Web.Mvc.Grid.Aggregate.Sum))
    End Sub)
                )
&lt;br /&gt;Group By
@@(Html.C1().ComboBox().Id("gMenu").Bind(Model.Settings("GroupBy")) _
    .SelectedIndex(0).OnClientSelectedIndexChanged("gMenu_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="gJs">

//Group By Modules
function gMenu_SelectedIndexChanged(sender) {    
    var grid = &lt;wijmo.grid.FlexGrid&gt;wijmo.Control.getControl("#gFlexGrid");
    if (sender.selectedValue && grid) {
        var name = sender.selectedValue;
        var groupDescriptions = grid.collectionView.groupDescriptions;
        grid.beginUpdate();
        groupDescriptions.clear();

        if (name.indexOf("Country") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Country"));
        }

        if (name.indexOf("Product") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Product"));
        }

        if (name.indexOf("Color") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Color"));
        }

        if (name.indexOf("Start") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Start", function (item, prop) {
                var value = item[prop];
                return value.getFullYear() + "/" + value.getMonth();
            }));
        }

        if (name.indexOf("Amount") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Amount", function (item, prop) {
                var value = item[prop];
                if (value <= 500) {
                    return "<500";
                }

                if (value > 500 && value <= 1000) {
                    return "500 to 1000";
                }

                if (value > 1000 && value <= 5000) {
                    return "1000 to 5000";
                }

                return "More than 5000";
            }));
        }
        grid.endUpdate();
    }
}
                            

                        </div>
                        <div class="tab-pane pane-content" id="gCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.Settings = CreateSettings()
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function

    Private Function CreateSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {        
        {"GroupBy", New Object() {"Country", "Product", "Color", "Start", "Amount", "Country and Product",
            "Product and Color", "None"}}
    }
        Return settings
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live) :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("gFlexGrid").AutoGenerateColumns(False) _
                    .AllowSorting(True).IsReadOnly(True).GroupBy("Country").Bind(Model.CountryData).CssClass("grid") _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Color").Header("Color").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Header("Amount").Format("n0").Width("*") _
                                                     .Aggregate(C1.Web.Mvc.Grid.Aggregate.Sum))
                             End Sub)
                )
                <br />Group By
                @(Html.C1().ComboBox().Id("gMenu").Bind(Model.Settings("GroupBy")) _
                    .SelectedIndex(0).OnClientSelectedIndexChanged("gMenu_SelectedIndexChanged")
                )
            </div>
        </div>
    </div>


    <!-- filtering -->
    <div>
                        <h2>Filtering</h2>
        <p>
                        The FlexGrid supports filtering through the <b>Filterable</b> property. To enable filtering, set the <b>Filterable((Sub(fl) .ColumnFilters(Sub(cfsb)...</b> property.
        </p>
        <p>
            In this example, we create a filter for the ID, Country, Product, Color, Start and get the filter value from the input control.
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#fHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#fCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="fHtml">

@@(Html.C1().FlexGrid().Id("fFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
    .IsReadOnly(True).Bind(Model.CountryData).CssClass("grid") _
    .Filterable(Sub(fl)
                    fl.DefaultFilterType(FilterType.None) _
                    .ColumnFilters(Sub(cfsb)
                                        For index As Int16 = 0 To Model.FilterBy.Length - 1
                                            cfsb.Add(Sub(cfb) cfb.Column(Model.FilterBy.ToList()(index)).FilterType(FilterType.Condition))
                                        Next
                                    End Sub)

                End Sub) _
.Columns(Sub(bl)
                bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="fCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        model.FilterBy = New String() {"ID", "Country", "Product", "Color", "Start"}
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live) :        </h4>
                @(Html.C1().FlexGrid().Id("fFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
                    .IsReadOnly(True).Bind(Model.CountryData).CssClass("grid") _
                    .Filterable(Sub(fl)
                                    fl.DefaultFilterType(FilterType.None) _
                                    .ColumnFilters(Sub(cfsb)
                                                       For index As Int16 = 0 To Model.FilterBy.Length - 1
                                                           cfsb.Add(Sub(cfb) cfb.Column(Model.FilterBy.ToList()(index)).FilterType(FilterType.Condition))
                                                       Next
                                                   End Sub)

                                End Sub) _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                             bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                         End Sub)
                )
            </div>
        </div>
    </div>



    <!-- paging -->
    <div>
                            <h2>Paging</h2>
        <p>
                            The FlexGrid supports paging through the <b>Pager</b> control. To enable paging, set the <b>PageSize</b> property to the number
            of items you want on each page, and use Pager control to bind this FlexGrid.
        </p>
        <p>
            In this example, we set <b>PageSize</b> to show 10 items per page. We add Pager control and set <b>Owner</b> Property to the FlexGrid id,
            then we can switch pages.
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#pHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#pCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="pHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("pFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
    .IsReadOnly(True).CssStyle("height", "auto").Bind(Model.CountryData).CssClass("grid") _
    .PageSize(10) _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
        bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
        bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
    End Sub)
)
@@Html.C1().Pager().Owner("pFlexGrid")

                        </div>
                        <div class="tab-pane pane-content" id="pCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("pFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
                    .IsReadOnly(True).CssStyle("height", "auto").Bind(Model.CountryData).CssClass("grid") _
                    .PageSize(10) _
    .Columns(Sub(bl)
                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                 bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
             End Sub)
                )
                @Html.C1().Pager().Owner("pFlexGrid")
            </div>
        </div>
    </div>

    <!-- conditional styling -->
    <div>
        <h2>Conditional Styling</h2>
        <p>
            FlexGrid has an <b>ItemFormatter</b> property that gives you complete control over
            the contents of the cells.
        </p>
        <p>
            This example uses a TypeScript function to create value ranges that return named
            colors. We then call this function in the FlexGrid's <b>ItemFormatter</b> and pass the cell's data
            in order to conditionally set the cell's foreground color.
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#csHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#csJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#csCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="csHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("csFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
    .IsReadOnly(True).Bind(Model.CountryData).ItemFormatter("csFlexGrid_ItemFormatter") _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="csJs">

//Conditional styling Modules
function csFlexGrid_ItemFormatter(panel, r, c, cell) {
    if (wijmo.grid.CellType.Cell == panel.cellType && panel.columns[c].binding == 'Amount') {
        var cellData = panel.getCellData(r, c);
        cell.style.color = cellData < 0 ? 'red' : cellData < 500 ? 'black' : 'green';
    }
    if (wijmo.grid.CellType.Cell == panel.cellType && panel.columns[c].binding == 'Discount') {
        var cellData = panel.getCellData(r, c);
        cell.style.color = cellData < .1 ? 'red' : cellData < .2 ? 'black' : 'green';
    }
}

                        </div>
                        <div class="tab-pane pane-content" id="csCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("csFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
                .IsReadOnly(True).Bind(Model.CountryData).ItemFormatter("csFlexGrid_ItemFormatter") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- themes -->
    <div>
        <h2>Themes</h2>
        <p>
            The appearance of the FlexGrid is defined in CSS. In addition to the default theme, we
            include about a dozen professionally designed themes that customize the appearance of
            all C1 ASP.Net MVC controls to achieve a consistent, attractive look.
        </p>
        <p>
            In this example, we add a "custom-flex-grid" class to the grid element and define some
            CSS rules to create a simple "black and white, no borders" theme for any grids that
            have the "custom-flex-grid" class.
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#tCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#tCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="tHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("tFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
    .IsReadOnly(True).Bind(Model.CountryData).CssClass("custom-flex-grid") _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="tCss">

.custom-flex-grid .wj-header.wj-cell {
    color: #fff;
    background-color: #000;
    border-bottom: solid 1px #404040;
    border-right: solid 1px #404040;
    font-weight: bold;
}

.custom-flex-grid .wj-cell {
    background-color: #fff;
    border: none;
}

.custom-flex-grid .wj-alt:not(.wj-state-selected):not(.wj-state-multi-selected) {
    background-color: #fff;
}

.custom-flex-grid .wj-state-selected {
    background: #000;
    color: #fff;
}

.custom-flex-grid .wj-state-multi-selected {
    background: #222;
    color: #fff;
}

                        </div>
                        <div class="tab-pane pane-content" id="tCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("tFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
                .IsReadOnly(True).Bind(Model.CountryData).CssClass("custom-flex-grid") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- trees/hierarchical data -->
    <div>
        <h2>Trees and Hierarchical Data</h2>
        <p>
            In addition to grouping, FlexGrid supports hierarchical data, that is, data with items
            that have lists of subitems. This type of hierarchical structure is very common, and is
            usually displayed in a tree-view control.
        </p>
        <p>
            To use FlexGrid with hierarchical data sources, set the <b>ChildItemsPath</b> property
            to the name of the data element that contains the child elements. The grid automatically
            scans the data and builds the tree for you.
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tvHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#tvCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#tvCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="tvHtml">

@@(Html.C1().FlexGrid().Id("tvFlexGrid").AutoGenerateColumns(False).IsReadOnly(True) _
    .Bind(Model.TreeData).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.ListBox) _
    .AllowResizing(C1.Web.Mvc.Grid.AllowResizing.None).AllowSorting(False).ChildItemsPath("Children") _
    .CssClass("custom-flex-grid") _
    .Columns(Sub(bl)
                bl.Add().Binding("Header").Width("*").Header("Folder/File Name")
                bl.Add().Binding("Size").Width("80").Align("center")
            End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="tvCss">

.custom-flex-grid .wj-header.wj-cell {
    color: #fff;
    background-color: #000;
    border-bottom: solid 1px #404040;
    border-right: solid 1px #404040;
    font-weight: bold;
}

.custom-flex-grid .wj-cell {
    background-color: #fff;
    border: none;
}

.custom-flex-grid .wj-alt:not(.wj-state-selected):not(.wj-state-multi-selected) {
    background-color: #fff;
}

.custom-flex-grid .wj-state-selected {
    background: #000;
    color: #fff;
}

.custom-flex-grid .wj-state-multi-selected {
    background: #222;
    color: #fff;
}

                        </div>
                        <div class="tab-pane pane-content" id="tvCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        model.TreeData = Folder.Create(Server.MapPath("~")).Children
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexGrid().Id("tvFlexGrid").AutoGenerateColumns(False).IsReadOnly(True) _
                    .Bind(Model.TreeData).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.ListBox) _
                    .AllowResizing(C1.Web.Mvc.Grid.AllowResizing.None).AllowSorting(False).ChildItemsPath("Children") _
                    .CssClass("custom-flex-grid") _
                    .Columns(Sub(bl)
                                 bl.Add().Binding("Header").Width("*").Header("Folder/File Name")
                                 bl.Add().Binding("Size").Width("80").Align("center")
                             End Sub)
                )
            </div>
        </div>
    </div>



    <!-- handling null values -->
    <div>
        <h2>Handling null values</h2>
        <p>
            By default, FlexGrid allows you to enter empty values in columns of type string,
            and will not allow empty/null values in columns of any other type.
        </p>
        <p>
            You can change this behavior using the <b>IsRequired</b> property on grid columns.
            If you set the <b>IsRequired</b> property to false, the grid will allow you to
            enter empty values in that column, regardless of type. Conversely, if you set
            the <b>IsRequired</b> property to true, the grid will not allow empty values
            even in string columns.
        </p>
        <p>
            Setting <b>IsRequired</b> to null reverts to the default behavior (nulls allowed
            only in string columns).
        </p>
        <p>
            The grid below reverts the default behavior. It sets <b>IsRequired</b> to false
            for the first column, and to true for all others. You can delete content that
            is not required by entering an empty string or simply by pressing the delete
            key.
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#nvHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#nvCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="nvHtml">

@@(Html.C1().FlexGrid().Id("nvGrid").AutoGenerateColumns(False).AllowSorting(True) _
    .Bind(Sub(bl)
            bl.Bind(Url.Action("GridRead"))
            bl.Update(Url.Action("NVGridUpdate"))
        End Sub) _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("ID").Header("ID").IsReadOnly(True))
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country").IsRequired(False))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product").IsRequired(True))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").IsRequired(True))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0").IsRequired(True))
        End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="nvCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

    Public Function GridRead(<C1JsonRequest> requestData As CollectionViewRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Read(requestData, FlexGridModel.Source))
    End Function

    Public Function NVGridUpdate(<C1JsonRequest> requestData As CollectionViewEditRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Edit(Of Sale)(requestData, Function(sale)
            Dim [error] As String = String.Empty
            Dim success As Boolean = True
            Dim fSale = FlexGridModel.Source.Find(Function(item) item.ID = sale.ID)
            fSale.Country = sale.Country
            fSale.Product = sale.Product
            fSale.Amount = sale.Amount
            fSale.Discount = sale.Discount
            Dim RetValue = New CollectionViewItemResult(Of Sale)
            RetValue.Error = [error]
            RetValue.Success = success AndAlso ModelState.IsValid
            RetValue.Data = fSale
            Return RetValue
        End Function, Function() FlexGridModel.Source))
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>Result (live):</h4>
                @(Html.C1().FlexGrid().Id("nvGrid").AutoGenerateColumns(False).AllowSorting(True) _
                    .Bind(Sub(bl)
                              bl.Bind(Url.Action("GridRead"))
                              bl.Update(Url.Action("NVGridUpdate"))
                          End Sub) _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID").IsReadOnly(True))
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").IsRequired(False))
                                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product").IsRequired(True))
                                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").IsRequired(True))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Format("n0").IsRequired(True))
                             End Sub)
                )
            </div>
        </div>
    </div>









    

</div>

<script type="text/javascript">
    c1.documentReady(function () {
        if (window["InitialControls"]) {
            window["InitialControls"]();
        }
    });
</script>