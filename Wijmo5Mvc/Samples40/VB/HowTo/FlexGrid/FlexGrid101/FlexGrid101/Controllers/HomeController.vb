﻿Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.Settings = CreateSettings()
        model.CountryData = Sale.GetData(500)
        model.TreeData = Folder.Create(Server.MapPath("~")).Children
        model.FilterBy = New String() {"ID", "Country", "Product", "Color", "Start"}
        Return View(model)
    End Function

    Private Function CreateSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
        {"SelectionMode", New Object() {SelectionMode.None.ToString(), SelectionMode.Cell.ToString(), SelectionMode.CellRange.ToString(), SelectionMode.Row.ToString(), SelectionMode.RowRange.ToString(), SelectionMode.ListBox.ToString()}},
        {"GroupBy", New Object() {"Country", "Product", "Color", "Start", "Amount", "Country and Product",
            "Product and Color", "None"}}
    }
        Return settings
    End Function

    Public Function GridRead(<C1JsonRequest> requestData As CollectionViewRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Read(requestData, FlexGridModel.Source))
    End Function

    Public Function EGridUpdate(<C1JsonRequest> requestData As CollectionViewEditRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Edit(Of Sale)(requestData, Function(sale)
                                                                             Dim [error] As String = String.Empty
                                                                             Dim success As Boolean = True
                                                                             Dim fSale = FlexGridModel.Source.Find(Function(item) item.ID = sale.ID)
                                                                             fSale.Country = sale.Country
                                                                             fSale.Amount = sale.Amount
                                                                             fSale.Discount = sale.Discount
                                                                             fSale.Active = sale.Active
                                                                             Dim RetValue = New CollectionViewItemResult(Of Sale)
                                                                             RetValue.Error = [error]
                                                                             RetValue.Success = success AndAlso ModelState.IsValid
                                                                             RetValue.Data = fSale
                                                                             Return RetValue
                                                                         End Function, Function() FlexGridModel.Source))
    End Function

    Public Function NVGridUpdate(<C1JsonRequest> requestData As CollectionViewEditRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Edit(Of Sale)(requestData, Function(sale)
                                                                             Dim [error] As String = String.Empty
                                                                             Dim success As Boolean = True
                                                                             Dim fSale = FlexGridModel.Source.Find(Function(item) item.ID = sale.ID)
                                                                             fSale.Country = sale.Country
                                                                             fSale.Product = sale.Product
                                                                             fSale.Amount = sale.Amount
                                                                             fSale.Discount = sale.Discount
                                                                             Dim RetValue = New CollectionViewItemResult(Of Sale)
                                                                             RetValue.Error = [error]
                                                                             RetValue.Success = success AndAlso ModelState.IsValid
                                                                             RetValue.Data = fSale
                                                                             Return RetValue
                                                                         End Function, Function() FlexGridModel.Source))
    End Function


End Class
