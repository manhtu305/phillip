﻿Public Class ProductData
    Private Shared r As New Random()

    Public Property ID() As Integer
        Get
            Return m_ID
        End Get
        Set
            m_ID = Value
        End Set
    End Property
    Private m_ID As Integer
    Public Property Product() As String
        Get
            Return m_Product
        End Get
        Set
            m_Product = Value
        End Set
    End Property
    Private m_Product As String
    Public Property Country() As String
        Get
            Return m_Country
        End Get
        Set
            m_Country = Value
        End Set
    End Property
    Private m_Country As String
    Public Property [Date]() As DateTime
        Get
            Return m_Date
        End Get
        Set
            m_Date = Value
        End Set
    End Property
    Private m_Date As DateTime
    Public Property Sales() As Integer
        Get
            Return m_Sales
        End Get
        Set
            m_Sales = Value
        End Set
    End Property
    Private m_Sales As Integer
    Public Property Downloads() As Integer
        Get
            Return m_Downloads
        End Get
        Set
            m_Downloads = Value
        End Set
    End Property
    Private m_Downloads As Integer
    Public Property Active() As Boolean
        Get
            Return m_Active
        End Get
        Set
            m_Active = Value
        End Set
    End Property
    Private m_Active As Boolean
    Public Property Discount() As Double
        Get
            Return m_Discount
        End Get
        Set
            m_Discount = Value
        End Set
    End Property
    Private m_Discount As Double

    Private Shared Function randomInt(max As Integer) As Integer
        Return CInt(Math.Floor(r.NextDouble() * (max + 1)))
    End Function

    Public Shared Function GetData(total As Integer) As IEnumerable(Of ProductData)
        Dim countries As String() = "China,India,Russia,US,Germany,UK,Japan,Italy,Greece,Spain,Portugal".Split(","c)
        Dim products As String() = "Wijmo,Aoba,Xuni,Olap".Split(","c)
        Dim list = Enumerable.Range(0, total) _
        .[Select](Function(i)
                      Dim SaleRec As New ProductData
                      SaleRec.ID = i + 1
                      SaleRec.Product = products(randomInt(products.Length - 1))
                      SaleRec.Country = countries(randomInt(countries.Length - 1))
                      SaleRec.[Date] = New DateTime(DateTime.Today.Year - 2 + randomInt(2), randomInt(11) + 1, randomInt(27) + 1)
                      SaleRec.Sales = randomInt(10000)
                      SaleRec.Downloads = randomInt(10000)
                      SaleRec.Active = If(randomInt(1) = 1, True, False)
                      SaleRec.Discount = r.NextDouble()
                      Return SaleRec
                  End Function)
        Return list
    End Function
End Class

