﻿<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <title>@ViewBag.Title</title>
    <link rel="apple-touch-icon" sizes="180x180" href="@Url.Content("~/Content/favicon/apple-touch-icon.png")">
    <link rel="icon" type="image/png" href="@Url.Content("~/Content/favicon/favicon-32x32.png")" sizes="32x32">
    <link rel="icon" type="image/png" href="@Url.Content("~/Content/favicon/favicon-16x16.png")" sizes="16x16">
    <link rel="manifest" href="@Url.Content("~/Content/favicon/manifest.json")">
    <link rel="mask-icon" href="@Url.Content("~/Content/favicon/safari-pinned-tab.svg")" color="#503b75">
    



    @Html.C1().Styles().Theme("Default")
    <!-- styles -->
    <link rel = "stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    <link rel = "stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="~/Content/css/app.css" />
    <link rel="stylesheet" href="~/Content/css/explorer.css" />
    @Html.C1().Scripts().Basic().Olap()

    <!-- jQuery And Bootstrap scripts -->
    <script src = "https://code.jquery.com/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src = "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- App scripts -->
    @Scripts.Render("~/bundles/app")
</head>
<body>


    <div Class="hide">
        @Html.Partial("_SiteNav", True)
    </div>
    <header>
        <div Class="hamburger-nav-btn narrow-screen"><span class="icon-bars"></span></div>
        <a class="logo-container" href="https://www.grapecity.com/en/aspnet-mvc" target="_blank">
            <i class="gcicon-product-c1"></i>ComponentOne
        </a>
        <div Class="task-bar">
            <span Class="semi-bold narrow-screen">MVC OLAP 101</span>
            <span Class="semi-bold wide-screen">ASP.NET MVC Edition OLAP 101</span>
            @Html.Partial("_SiteNav", False)
        </div>
    </header>
    @RenderBody()
    <footer>
        <a href="http://www.grapecity.com/">
            <i class="gcicon-logo-gc-horiz"></i>
        </a>
        <p>
            © @DateTime.Now.Year GrapeCity, Inc. All Rights Reserved.<br />
            All product And company names here In may be trademarks Of their respective owners.
        </p>
        @Code
            Dim requestUrl = Request.Url
            Dim urlStr = requestUrl.OriginalString.Substring(0, requestUrl.OriginalString.Length - (If(requestUrl.Query Is Nothing, 0, requestUrl.Query.Length)))
        End Code
        <a href = "http://www.facebook.com/sharer.php?u=@urlStr" target="_blank">
            <img src = "/Content/css/images/icons/32/picons36.png" alt="facebook" />
        </a>
        <a href = "http://twitter.com/share?text=Have you seen this? C1Studio MVC Edition Controls&url=@urlStr" target="_blank">
            <img src = "/Content/css/images/icons/32/picons33.png" alt="Twitter" />
        </a>
    </footer>
</body>
</html>
