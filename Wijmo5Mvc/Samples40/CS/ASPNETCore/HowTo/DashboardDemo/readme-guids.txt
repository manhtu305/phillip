E0471D5B-221C-4394-ACC9-4EEFA485E5D9		Common Guid shared by sample with multiple languages.
E6EE977E-A828-47B8-BFF2-293464E52C00		Unique Guid for each sample regardless of language.
<product>FlexGrid for ASP.NET MVC;ASP.NET</product>
<product>FlexChart for ASP.NET MVC;ASP.NET</product>
<product>BulletGraph for ASP.NET MVC;ASP.NET</product>
<product>PivotEngine for ASP.NET MVC;ASP.NET</product>
<product>PivotPanel for ASP.NET MVC;ASP.NET</product>
<product>PivotGrid for ASP.NET MVC;ASP.NET</product>
<product>TreeMap for ASP.NET MVC;ASP.NET</product>
<product>ReportViewer for ASP.NET MVC;ASP.NET</product>