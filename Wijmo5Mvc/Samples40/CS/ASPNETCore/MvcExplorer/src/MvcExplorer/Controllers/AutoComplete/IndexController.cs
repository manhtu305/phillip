﻿using Microsoft.AspNetCore.Mvc;

namespace MvcExplorer.Controllers
{
    public partial class AutoCompleteController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Countries = Models.Countries.GetCountries();
            return View();
        }
    }
}
