﻿using Microsoft.AspNetCore.Mvc;
using MvcExplorer.Models;
namespace MvcExplorer.Controllers
{
    public partial class MultiSelectController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Countries = Countries.GetCountries();
            return View();
        }
    }
}
