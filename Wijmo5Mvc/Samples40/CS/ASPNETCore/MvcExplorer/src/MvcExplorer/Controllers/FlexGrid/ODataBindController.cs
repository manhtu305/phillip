﻿using C1.Web.Mvc;
using C1.Web.Mvc.Serialization;
using Microsoft.AspNetCore.Mvc;
using MvcExplorer.Models;
using System.Collections.Generic;

namespace MvcExplorer.Controllers
{
    public partial class FlexGridController : Controller
    {
        // GET: ODataBind
        public ActionResult ODataBind()
        {
            ViewBag.DemoSettings = true;
            ViewBag.DemoSettingsModel = new ClientSettingsModel
            {
                Settings = new Dictionary<string, object[]>
                {
                    {"SortOnServer", new object[]{true, false}},
                    {"FilterOnServer", new object[]{true, false}}
                }
            };
#if ODATA_SERVER
            ViewBag.IsReadOnly = false;
#else
            ViewBag.IsReadOnly = true;
#endif
            return View();
        }
    }
}
