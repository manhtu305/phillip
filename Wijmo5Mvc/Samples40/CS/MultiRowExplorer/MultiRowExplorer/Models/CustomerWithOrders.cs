﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiRowExplorer.Models
{
    public class CustomerWithOrders : Customer
    {
        public List<Order> Orders { get; set; }
    }
}