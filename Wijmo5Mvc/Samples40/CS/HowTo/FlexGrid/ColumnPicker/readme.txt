﻿ASP.NET MVC Column Picker
---------------------------------------------
This sample demonstrates how to add/remove columns from FlexGrid.

The value of the column-picker directive/parameter represents an
image to be displayed at the top-right of the grid.

Clicking the image shows a drop-down with a list of check-boxes 
representing the grid columns. The user may toggle the column
visibility by clicking the check-boxes.