﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Benchmark
{
    public class TimingModule : IHttpModule
    {
        private HttpApplication _context;
        public static Dictionary<string, string> controlElapsedSeverTime = new Dictionary<string, string> {
            { "c1mvcflexgridserver", "" },
            { "devexpressgridviewserver", "" },
            { "telerikgridserver", "" },
            { "infragisticsgridserver", "" },
        };
        public static Dictionary<string, string> controlElapsedClientTime = new Dictionary<string, string> {
            { "c1mvcflexgridclient", "" },
            { "devexpressgridviewclient", "" },
            { "telerikgridclient", "" },
            { "infragisticsgridclient", "" },
        };
        public void Dispose()
        {
            //_context.BeginRequest -= OnBeginRequest;
            //_context.EndRequest -= OnEndRequest;
        }

        public void Init(HttpApplication context)
        {
            _context = context;
            _context.BeginRequest += OnBeginRequest;
            _context.EndRequest += OnEndRequest;
        }

        private string GetControlName()
        {
            string targetController = "Home/";
            string path = HttpContext.Current.Request.Path;
            var indexOfController = path.IndexOf(targetController);
            if (indexOfController < 0)
            {
                return null;
            }
            path = path.Substring(indexOfController + targetController.Length);
            return path.ToLower()+"server";
        }

        void OnBeginRequest(object sender, System.EventArgs e)
        {
            if (HttpContext.Current.Request.IsLocal
                && HttpContext.Current.IsDebuggingEnabled)
            {
                if (string.IsNullOrEmpty(GetControlName()))
                {
                    return;
                }
                var stopwatch = new Stopwatch();
                HttpContext.Current.Items["Stopwatch"] = stopwatch;
                stopwatch.Start();
            }
        }

        void OnEndRequest(object sender, System.EventArgs e)
        {
            if (HttpContext.Current.Request.IsLocal
                && HttpContext.Current.IsDebuggingEnabled)
            {
                Stopwatch stopwatch =
                  (Stopwatch)HttpContext.Current.Items["Stopwatch"];
                if (stopwatch == null)
                {
                    return;
                }
                stopwatch.Stop();

                TimeSpan ts = stopwatch.Elapsed;
                string elapsedTime = String.Format("{0} s", Math.Round(ts.TotalSeconds, 2));
                var controlName = GetControlName();
                if (!string.IsNullOrEmpty(controlName) && controlElapsedSeverTime.ContainsKey(controlName))
                {
                    controlElapsedSeverTime[controlName] = elapsedTime;
                    HttpContext.Current.Response.Write("<script> setRunningTime( '" + controlName + "','" + elapsedTime + "')</script>");
                }
            }
        }
    }
}