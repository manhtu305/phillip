﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Benchmark
{
    public class PreApplicationStartCode
    {
        public static void Start()
        {
            Microsoft.Web.Infrastructure.DynamicModuleHelper.DynamicModuleUtility.RegisterModule(typeof(TimingModule));
        }
    }
}