﻿using Benchmark.Models;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Benchmark.Controllers
{
    public class HomeController : Controller
    {
        public static int Count = 5000;
        public static bool Paging = true;
        private void HandleParams(FormCollection collection)
        {
            if (collection.AllKeys.Contains("itemCount"))
            {
                var itemCount = collection.GetValue("itemCount").ConvertTo(typeof(int));
                if (itemCount != null)
                {
                    Count = (int)itemCount;
                }
            }
            if (collection.AllKeys.Contains("paging"))
            {
                var enablePaging = collection.GetValue("paging").ConvertTo(typeof(bool));
                if (enablePaging != null)
                {
                    Paging = (bool)enablePaging;
                }
            }

            foreach (var key in collection.AllKeys)
            {
                if (TimingModule.controlElapsedClientTime.ContainsKey(key))
                {
                    var time = collection[key];
                    if (!string.IsNullOrEmpty(time))
                    {
                        TimingModule.controlElapsedClientTime[key] = time;
                    }
                }
            }
        }

        public ActionResult Index(FormCollection collection)
        {
            HandleParams(collection);
            return View();
        }

        public ActionResult C1MVCFlexGrid(FormCollection collection)
        {
            HandleParams(collection);
            var model = Sale.GetData(Count);
            return View(model);
        }

        public ActionResult DevExpressGridView(FormCollection collection)
        {
            HandleParams(collection);
            var model = Sale.GetData(Count);
            return View(model);
        }

        public ActionResult DevExpressGridViewPartial(FormCollection collection)
        {
            var model = Sale.GetData(Count);
            return PartialView("DevExpressGridViewPartial", model);
        }

        public ActionResult TelerikGrid(FormCollection collection)
        {
            HandleParams(collection);
            var model = Sale.GetData(Count);
            return View(model);
        }

        public ActionResult InfragisticsGrid(FormCollection collection)
        {
            HandleParams(collection);
            var model = Sale.GetData(Count);
            return View(model);
        }
    }
}
