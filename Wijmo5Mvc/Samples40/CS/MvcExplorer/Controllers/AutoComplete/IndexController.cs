﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C1.Web.Mvc.Serialization;

namespace MvcExplorer.Controllers
{
    public partial class AutoCompleteController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Countries = MvcExplorer.Models.Countries.GetCountries();
            return View();
        }
    }
}
