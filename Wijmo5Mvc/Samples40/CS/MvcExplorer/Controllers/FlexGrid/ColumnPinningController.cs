﻿using MvcExplorer.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MvcExplorer.Controllers
{
    public partial class FlexGridController : Controller
    {
        private readonly ControlOptions _columnPinningDataModel = new ControlOptions
        {
            Options = new OptionDictionary
            {
                {"Allow Pinning", new OptionItem {Values = new List<string> {"True", "False"}, CurrentValue = "True"}},
            }
        };

        public ActionResult ColumnPinning(FormCollection collection)
        {
            IValueProvider data = collection;
            _columnPinningDataModel.LoadPostData(data);
            var model = Sale.GetData(500);
            ViewBag.DemoOptions = _columnPinningDataModel;
            return View(model);
        }
    }
}