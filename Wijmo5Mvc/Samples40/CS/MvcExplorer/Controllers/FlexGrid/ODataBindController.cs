﻿using MvcExplorer.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MvcExplorer.Controllers
{
    partial class FlexGridController
    {
        // GET: ODataBind
        public ActionResult ODataBind()
        {
            ViewBag.DemoSettings = true;
            ViewBag.DemoSettingsModel = new ClientSettingsModel
            {
                Settings = new Dictionary<string, object[]>
                {
                    {"SortOnServer", new object[]{true, false}},
                    {"FilterOnServer", new object[]{true, false}}
                }
            };
            return View();
        }
    }
}