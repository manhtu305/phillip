﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcExplorer.Controllers
{
    public partial class ComboBoxController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Countries = MvcExplorer.Models.Countries.GetCountries();
            ViewBag.Cities = MvcExplorer.Models.Cities.GetCities();
            return View();
        }
    }
}
