﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcExplorer.Controllers
{
    public partial class ListBoxController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Cities = MvcExplorer.Models.Cities.GetCities();
            return View();
        }
    }
}
