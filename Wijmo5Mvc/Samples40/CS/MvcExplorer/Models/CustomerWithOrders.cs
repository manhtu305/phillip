﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcExplorer.Models
{
    public class CustomerWithOrders : Customer
    {
        public List<Order> Orders { get; set; }
    }
}