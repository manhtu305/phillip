﻿namespace C1.Web.Mvc
{
    public partial class FlexGridDetailProvider<T>
    {
        #region Ctors
        /// <summary>
        /// Creates one <see cref="FlexGridDetailProvider{T}"/> instance.
        /// </summary>
        /// <param name="grid">The grid owns the FlexGridDetailProvider extender.</param>
        public FlexGridDetailProvider(FlexGridBase<T> grid)
            : base(grid)
        {
            Initialize();
        }

        #endregion Ctors
    }
}
