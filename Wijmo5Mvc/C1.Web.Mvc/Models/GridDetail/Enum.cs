﻿namespace C1.Web.Mvc.GridDetail
{
    /// <summary>
    /// Specifies constants that define the action to perform when the ENTER key is pressed.
    /// </summary>
    public enum KeyAction
    {
        /// <summary>
        /// No special action (let the browser handle the key).
        /// </summary>
        None,
        /// <summary>
        /// Toggle the detail display.
        /// </summary>
        ToggleDetail
    }
}
