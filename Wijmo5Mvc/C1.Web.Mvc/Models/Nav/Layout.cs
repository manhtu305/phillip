﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    partial class LayoutBase
    {
#if !MODEL
        /// <summary>
        /// Creates one LayoutBase instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        protected LayoutBase(HtmlHelper helper) : base(helper)
#else
        protected LayoutBase()
#endif
        {
            Initialize();
        }
    }

    /// <summary>
    /// Specifies the direction in which the layout items renders in the dashboard.
    /// </summary>
    public enum LayoutOrientation
    {
        /// <summary>
        /// Shows the layout items in horizontal direction.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Shows the layout items in vertical direction.
        /// </summary>
        Vertical
    }
}
