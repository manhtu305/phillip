﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    partial class TreeView
    {
        #region Source
        private IEnumerable<object> _source;
        private IEnumerable<object> _GetSource()
        {
            return _source;
        }
        private void _SetSource(IEnumerable<object> value)
        {
            _source = value;
            _loadActionUrl = null;
        }
        #endregion Source

        #region LoadActionUrl
        private string _loadActionUrl;
        private string _GetLoadActionUrl()
        {
            return _loadActionUrl;
        }
        private void _SetLoadActionUrl(string value)
        {
            _loadActionUrl = value;
            _source = null;
        }
        #endregion LoadActionUrl
    }
}
