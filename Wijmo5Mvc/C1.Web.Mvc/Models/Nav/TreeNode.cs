﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    partial class TreeNode
    {
        #region DataItem
        private Dictionary<string, object> _dataItem;
        private Dictionary<string, object> _GetDataItem()
        {
            return _dataItem ?? (_dataItem = new Dictionary<string, object>());
        }
        #endregion DataItem
    }
}
