﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    partial class TabPanel
    {
        #region Tabs
        private IList<Tab> _tabs;
        private IList<Tab> _GetTabs()
        {
            return _tabs ?? (_tabs = new List<Tab>());
        }
        #endregion
    }
}
