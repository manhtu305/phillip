﻿using System.Collections.Generic;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    partial class ManualGridLayout
    {
#if !MODEL
        /// <summary>
        /// Creates one ManualGridLayout instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        public ManualGridLayout(HtmlHelper helper) : base(helper)
#else
        public ManualGridLayout()
#endif
        {
            Initialize();
        }

        private IList<ManualGridGroup> _items;
        private IList<ManualGridGroup> _GetItems()
        {
            return _items ?? (_items = new List<ManualGridGroup>());
        }
    }

    partial class ManualGridGroup
    {
        private IList<ManualGridTile> _children;
        private IList<ManualGridTile> _GetChildren()
        {
            return _children ?? (_children = new List<ManualGridTile>());
        }

        /// <summary>
        /// Creates one <see cref="ManualGridGroup"/> instance.
        /// </summary>
        public ManualGridGroup()
        {
            Initialize();
        }
    }

    partial class ManualGridTile
    {
        /// <summary>
        /// Creates one <see cref="ManualGridTile"/> instance.
        /// </summary>
        public ManualGridTile()
        {
            Initialize();
        }
    }
}
