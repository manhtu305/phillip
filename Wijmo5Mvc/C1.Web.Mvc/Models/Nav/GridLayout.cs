﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    partial class GridLayout
    {
#if !MODEL
        /// <summary>
        /// Creates one AutoGridLayout instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        protected GridLayout(HtmlHelper helper) : base(helper)
#else
        protected GridLayout()
#endif
        {
            Initialize();
        }
    }
}
