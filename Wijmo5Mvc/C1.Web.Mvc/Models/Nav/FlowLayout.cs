﻿using System.Collections.Generic;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    partial class FlowLayout
    {
#if !MODEL
        /// <summary>
        /// Creates one FlowLayout instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        public FlowLayout(HtmlHelper helper) : base(helper)
#else
        public FlowLayout()
#endif
        {
            Initialize();
        }

        private IList<FlowTile> _items;
        private IList<FlowTile> _GetItems()
        {
            return _items ?? (_items = new List<FlowTile>());
        }
    }

    partial class FlowTile
    {
        /// <summary>
        /// Creates one <see cref="FlowTile"/> instance.
        /// </summary>
        public FlowTile()
        {
            Initialize();
        }
    }

    /// <summary>
    /// Specifies the flow direction.
    /// </summary>
    public enum FlowDirection
    {
        /// <summary>
        /// Elements flow from the left edge of the design surface to the right.
        /// </summary>
        LeftToRight,
        /// <summary>
        /// Elements flow from the top of the design surface to the bottom.
        /// </summary>
        TopToDown,
        /// <summary>
        /// Elements flow from the right edge of the design surface to the left.
        /// </summary>
        RightToLeft,
        /// <summary>
        /// Elements flow from the bottom of the design surface to the top.
        /// </summary>
        BottomToUp
    }
}
