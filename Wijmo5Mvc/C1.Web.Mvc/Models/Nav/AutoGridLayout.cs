﻿using System.Collections.Generic;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    partial class AutoGridLayout
    {
#if !MODEL
        /// <summary>
        /// Creates one AutoGridLayout instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        public AutoGridLayout(HtmlHelper helper) : base(helper)
#else
        public AutoGridLayout()
#endif
        {
            Initialize();
        }

        private IList<AutoGridGroup> _items;
        private IList<AutoGridGroup> _GetItems()
        {
            return _items ?? (_items = new List<AutoGridGroup>());
        }
    }

    partial class AutoGridGroup
    {
        private IList<AutoGridTile> _children;
        private IList<AutoGridTile> _GetChildren()
        {
            return _children ?? (_children = new List<AutoGridTile>());
        }

        /// <summary>
        /// Creates one <see cref="AutoGridGroup"/> instance.
        /// </summary>
        public AutoGridGroup()
        {
            Initialize();
        }
    }

    partial class AutoGridTile
    {
        /// <summary>
        /// Creates one <see cref="AutoGridGroup"/> instance.
        /// </summary>
        public AutoGridTile()
        {
            Initialize();
        }
    }
}
