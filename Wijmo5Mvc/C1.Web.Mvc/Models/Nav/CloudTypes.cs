﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{

    /// <summary>
    /// Specifies type of supported cloud.
    /// </summary>
    public enum CloudTypes
    {
        /// <summary>
        /// Microsoft Azure.
        /// </summary>
        Azure,
        /// <summary>
        /// Amazon Web Services
        /// </summary>
        AWS,
        /// <summary>
        /// Dropbox.
        /// </summary>
        DropBox,
        /// <summary>
        /// Google Drive
        /// </summary>
        GoogleDrive,
        /// <summary>
        /// Microsoft OneDrive
        /// </summary>
        OneDrive,
        /// <summary>
        /// LocalStorage
        /// </summary>
        LocalStorage
    }
}
