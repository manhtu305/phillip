﻿using System.Collections.Generic;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    partial class SplitLayout
    {
#if !MODEL
        /// <summary>
        /// Creates one SplitLayout instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        public SplitLayout(HtmlHelper helper) : base(helper)
#else
        public SplitLayout()
#endif
        {
            Initialize();
        }

        private IList<ISplitLayoutItem> _items;
        private IList<ISplitLayoutItem> _GetItems()
        {
            return _items ?? (_items = new List<ISplitLayoutItem>());
        }
    }

    partial class SplitGroup : ISplitLayoutItem
    {
        private IList<ISplitLayoutItem> _children;
        private IList<ISplitLayoutItem> _GetChildren()
        {
            return _children ?? (_children = new List<ISplitLayoutItem>());
        }

        /// <summary>
        /// Creates one <see cref="SplitGroup"/> instance.
        /// </summary>
        public SplitGroup()
        {
            Initialize();
        }
    }

    partial class SplitTile : ISplitLayoutItem
    {
        /// <summary>
        /// Creates one <see cref="SplitTile"/> instance.
        /// </summary>
        public SplitTile()
        {
            Initialize();
        }

        #region Size
        private string _size;
        private void _SetSize(string value)
        {
            // value can be null or empty.
            if (string.IsNullOrEmpty(value))
            {
                _size = null;
                return;
            }

#if !MODEL
            string starSize;
            int size;
            ColumnWidthConverter.GetRealWidth(value, out starSize, out size);
#endif
            _size = value;
        }
        private string _GetSize()
        {
            return _size;
        }
        #endregion Size
    }

    /// <summary>
    /// Defines an empty interface for the items in split layout.
    /// </summary>
    public interface ISplitLayoutItem { }
}
