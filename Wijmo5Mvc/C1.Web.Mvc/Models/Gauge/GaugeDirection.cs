﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the direction in which the pointer of a LinearGauge increases.
    /// </summary>
    public enum GaugeDirection
    {
        /// <summary>
        /// Gauge value increases from left to right.
        /// </summary>
        Right,

        /// <summary>
        /// Gauge value increases from right to left.
        /// </summary>
        Left,

        /// <summary>
        /// Gauge value increases from bottom to top.
        /// </summary>
        Up,

        /// <summary>
        /// Gauge value increases from top to bottom.
        /// </summary>
        Down
    }
}
