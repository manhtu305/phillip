﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    public partial class Gauge
    {
        #region Face
        private Range _face;
        private Range _GetFace()
        {
            return _face ?? (_face = new Range());
        }
        #endregion Face

        #region Pointer
        private Range _pointer;
        private Range _GetPointer()
        {
            return _pointer ?? (_pointer = new Range());
        }
        #endregion Pointer

        #region Min
        private double _GetMin()
        {
            return Face.Min;
        }
        private void _SetMin(double value)
        {
            Face.Min = value;
        }
        #endregion Min

        #region Max
        private double _GetMax()
        {
            return Face.Max;
        }
        private void _SetMax(double value)
        {
            Face.Max = value;
        }
        #endregion Max

        #region Value
        private double _GetValue()
        {
            return Pointer.Max;
        }
        private void _SetValue(double value)
        {
            Pointer.Max = value;
        }
        #endregion Value

        #region Ranges
        private IList<Range> _ranges;
        private IList<Range> _GetRanges()
        {
            return _ranges ?? (_ranges = new List<Range>());
        }
        #endregion Ranges
    }
}
