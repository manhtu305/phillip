﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies the length of the needle element with respect to the pointer range.
    /// </summary>
    public enum NeedleLength
    {
        /// <summary>
        /// The needle element extends to the outer radius of the pointer range.
        /// </summary>
        Outer = 0,

        /// <summary>
        /// The needle element extends to the mid ponit between the inner and outer radii of the pointer range.
        /// </summary>
        Middle = 1,

        /// <summary>
        /// The needle element extends to the inner radius of the pointer range.
        /// </summary>
        Inner = 2
    }
}
