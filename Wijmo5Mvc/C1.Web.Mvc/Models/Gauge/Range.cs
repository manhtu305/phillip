﻿#if !ASPNETCORE
using System.Drawing;
#endif

namespace C1.Web.Mvc
{
    public partial class Range
    {
        /// <summary>
        /// Gets or sets the color used to display this range.
        /// </summary>
#if ASPNETCORE
        public string Color
#else
        public Color Color
#endif
        {
            get;
            set;
        }
    }
}
