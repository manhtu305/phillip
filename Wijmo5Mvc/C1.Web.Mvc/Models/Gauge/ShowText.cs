﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies which values should be displayed as text.
    /// </summary>
    public enum ShowText
    {
        /// <summary>
        /// Do not show any text in the gauge.
        /// </summary>
        None = 0,

        /// <summary>
        /// Show the gauge's value as text.
        /// </summary>
        Value = 1,

        /// <summary>
        /// Show the gauge's min and max values as text.
        /// </summary>
        MinMax = 2,

        /// <summary>
        /// Show the gauge's value, min, and max as text.
        /// </summary>
        All = 3
    }
}
