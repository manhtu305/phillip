﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies a pre-defined shape for the gauge's needle element.
    /// </summary>
    public enum NeedleShape
    {
        /// <summary>
        /// No pre-defined shape.
        /// </summary>
        None = 0,

        /// <summary>
        /// The needle element has a triangular shape.
        /// </summary>
        Triangle = 1,

        /// <summary>
        /// The needle element has a diamond shape.
        /// </summary>
        Diamond = 2,

        /// <summary>
        /// The needle element has an hexagonal shape.
        /// </summary>
        Hexagon = 3,

        /// <summary>
        /// The needle element has a rectangular shape.
        /// </summary>
        Rectangle = 4,

        /// <summary>
        /// The needle element has an arrow shape.
        /// </summary>
        Arrow = 5,

        /// <summary>
        /// The needle element has a wide arrow shape.
        /// </summary>
        WideArrow = 6,

        /// <summary>
        /// The needle element has a pointer shape.
        /// </summary>
        Pointer = 7,

        /// <summary>
        /// The needle element has a wide pointer shape.
        /// </summary>
        WidePointer = 8,

        /// <summary>
        /// The needle element has a triangular shape with an offset.
        /// </summary>
        Outer = 9
    }
}
