﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies the rate of change of a parameter over time.
    /// </summary>
    public enum Easing
    {
        /// <summary>
        /// Simple linear tweening, no easing and no acceleration.
        /// </summary>
        Linear,
        /// <summary>
        /// Easing equation for a swing easing.
        /// </summary>
        Swing,
        /// <summary>
        /// Easing equation for a quadratic easing in, accelerating from zero velocity.
        /// </summary>
        EaseInQuad,
        /// <summary>
        /// Easing equation for a quadratic easing out, decelerating to zero velocity.
        /// </summary>
        EaseOutQuad,
        /// <summary>
        /// Easing equation for a quadratic easing in and out, acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutQuad,
        /// <summary>
        /// Easing equation for a cubic easing in - accelerating from zero velocity.
        /// </summary>
        EaseInCubic,
        /// <summary>
        /// Easing equation for a cubic easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutCubic,
        /// <summary>
        /// Easing equation for a cubic easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutCubic,
        /// <summary>
        /// Easing equation for a quartic easing in - accelerating from zero velocity.
        /// </summary>
        EaseInQuart,
        /// <summary>
        /// Easing equation for a quartic easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutQuart,
        /// <summary>
        /// Easing equation for a quartic easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutQuart,
        /// <summary>
        /// Easing equation for a quintic easing in - accelerating from zero velocity.
        /// </summary>
        EaseInQuint,
        /// <summary>
        /// Easing equation for a quintic easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutQuint,
        /// <summary>
        /// Easing equation for a quintic easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutQuint,
        /// <summary>
        /// Easing equation for a sinusoidal easing in - accelerating from zero velocity.
        /// </summary>
        EaseInSine,
        /// <summary>
        /// Easing equation for a sinusoidal easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutSine,
        /// <summary>
        /// Easing equation for a sinusoidal easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutSine,
        /// <summary>
        /// Easing equation for an exponential easing in - accelerating from zero velocity.
        /// </summary>
        EaseInExpo,
        /// <summary>
        /// Easing equation for an exponential easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutExpo,
        /// <summary>
        /// Easing equation for an exponential easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutExpo,
        /// <summary>
        /// Easing equation for a circular easing in - accelerating from zero velocity.
        /// </summary>
        EaseInCirc,
        /// <summary>
        /// Easing equation for a circular easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutCirc,
        /// <summary>
        /// Easing equation for a circular easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutCirc,
        /// <summary>
        /// Easing equation for a back easing in - accelerating from zero velocity.
        /// </summary>
        EaseInBack,
        /// <summary>
        /// Easing equation for a back easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutBack,
        /// <summary>
        /// Easing equation for a back easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutBack,
        /// <summary>
        /// Easing equation for a bounce easing in - accelerating from zero velocity.
        /// </summary>
        EaseInBounce,
        /// <summary>
        /// Easing equation for a bounce easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutBounce,
        /// <summary>
        /// Easing equation for a bounce easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutBounce,
        /// <summary>
        /// Easing equation for an elastic easing in - accelerating from zero velocity.
        /// </summary>
        EaseInElastic,
        /// <summary>
        /// Easing equation for an elastic easing out - decelerating to zero velocity.
        /// </summary>
        EaseOutElastic,
        /// <summary>
        /// Easing equation for an elastic easing in and out - acceleration until halfway, then deceleration.
        /// </summary>
        EaseInOutElastic
    }
}
