﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies the animation mode whether chart should animate one point at a time, series by series, or all at once.
    /// </summary>
    public enum AnimationMode
    {
        /// <summary>
        /// All points and series are animated at once.
        /// </summary>
        All,
        /// <summary>
        /// Animation is performed point by point.
        /// Multiple series are animated simultaneously at the same time.
        /// </summary>
        Point,
        /// <summary>
        /// Animation is performed series by series.
        /// Entire series is animated at once, following the same animation as the "All" option,
        /// but just one series at a time. 
        /// </summary>
        Series
    }
}
