﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Indicates that the control supports the animation extender.
    /// </summary>
    public interface IAnimatable
    {
    }
}
