﻿namespace C1.Web.Mvc
{
    public partial class ChartAnimation<T>
    {
        /// <summary>
        /// Creates one <see cref="ChartAnimation{T}"/> instance.
        /// </summary>
        /// <param name="target">Target chart which derived from FlexChartBase</param>
        public ChartAnimation(FlexChartBase<T> target)
            : base(target)
        {
            Initialize();
        }
    }
}
