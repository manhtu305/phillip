﻿#if !MODEL
using C1.Web.Mvc.GridFilter;
using C1.Web.Mvc.Localization;
#endif
using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc
{
    public partial class SelectorBase<T>
    {
        #region Fields

        #endregion Fields
        
        #region Ctors

        /// <summary>
        /// Creates one <see cref="SelectorBase{T}"/> instance.
        /// </summary>
        /// <param name="grid">The grid which owns the extender which extends SelectorBase.</param>
        public SelectorBase(FlexGridBase<T> grid)
            : base(grid)
        {
            if (grid == null)
            {
                throw new ArgumentNullException("grid");
            }
            Initialize();
        }

        #endregion Ctors

    }
}
