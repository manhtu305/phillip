﻿#if !MODEL
using C1.Web.Mvc.GridFilter;
using C1.Web.Mvc.Localization;
#endif
using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc
{   
    public partial class BooleanChecker<T>
    {
        #region Fields

        #endregion Fields

        #region Ctors

        /// <summary>
        /// Creates one <see cref="BooleanChecker{T}"/> instance.
        /// </summary>
        /// <param name="grid">The grid which owns the BooleanChecker.</param>
        public BooleanChecker(FlexGridBase<T> grid)
            : base(grid)
        {
            if (grid == null)
            {
                throw new ArgumentNullException("grid");
            }
            Initialize();
        }

        #endregion Ctors

    }
}
