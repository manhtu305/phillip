﻿using System;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies actions that trigger showing and hiding Popup controls.
    /// </summary>
    [Flags]
    public enum PopupTrigger
    {
        /// <summary>
        /// No triggers; popups must be shown and hidden using code.
        /// </summary>
        None = 0,
        /// <summary>
        /// When the user clicks the owner element.
        /// </summary>
        ClickOwner = 1,
        /// <summary>
        /// When the user clicks the popup.
        /// </summary>
        ClickPopup = 2,
        /// <summary>
        /// When the user clicks the owner element or the popup.
        /// </summary>
        Click = ClickOwner | ClickPopup,
        /// <summary>
        /// When the owner element loses focus.
        /// </summary>
        BlurOwner = 4,
        /// <summary>
        /// When the popup loses focus.
        /// </summary>
        BlurPopup = 8,
        /// <summary>
        /// When the owner element or the popup lose focus.
        /// </summary>
        Blur = BlurOwner | BlurPopup,
        /// <summary>
        /// When the owner element or the popup are clicked or lose focus.
        /// </summary>
        ClickOrBlur = Click | Blur,
        /// <summary>
        /// When the mouse button is pressed over the owner element.
        /// </summary>
        DownOwner = 16,
        /// <summary>
        /// When the mouse button is pressed over the popup.
        /// </summary>
        DownPopup = 32,
        /// <summary>
        /// When the mouse button is pressed over the owner element or the popup.
        /// </summary>
        Down = DownOwner | DownPopup,
        /// <summary>
        /// When the mouse enters the owner element.
        /// </summary>
        EnterOwner = 64,
        /// <summary>
        /// When the mouse enters the popup.
        /// </summary>
        EnterPopup = 128,
        /// <summary>
        /// When the mouse enters the owner element or the popup.
        /// </summary>
        Enter = EnterOwner | EnterPopup,
        /// <summary>
        /// When the mouse leaves the owner element.
        /// </summary>
        LeaveOwner = 256,
        /// <summary>
        /// When the mouse leaves the popup.
        /// </summary>
        LeavePopup = 512,
        /// <summary>
        /// When the mouse leaves the owner element or the popup.
        /// </summary>
        Leave = LeaveOwner | LeavePopup
    }
}