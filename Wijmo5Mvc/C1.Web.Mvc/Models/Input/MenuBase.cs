﻿using C1.JsonNet;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    public abstract partial class MenuBase<T>
    {
        /// <summary>
        /// Overrides to remove this property.
        /// </summary>
        [Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AutoExpandSelection
        {
            get { return base.AutoExpandSelection; }
            set { base.AutoExpandSelection = value; }
        }

        /// <summary>
        /// Overrides to remove this property.
        /// </summary>
        [Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string HeaderPath
        {
            get { return base.HeaderPath; }
            set { base.HeaderPath = value; }
        }
    }
}
