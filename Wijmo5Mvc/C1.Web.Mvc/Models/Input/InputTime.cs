﻿using C1.JsonNet;
using System;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    public partial class InputTime<T>
    {
        /// <summary>
        ///  Gets or sets the name of the InputTime control.
        /// </summary>
        [Ignore]
        public string Name
        {
            get { return HtmlAttributes["wj-name"]; }
            set { HtmlAttributes["wj-name"] = value; }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether the control should automatically 
        /// expand the selection to whole words/numbers when the control is clicked.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        [DefaultValue(true)]
        public override bool AutoExpandSelection
        {
            get
            {
                return base.AutoExpandSelection;
            }
            set
            {
                base.AutoExpandSelection = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether the value should be edited.
        /// </summary>
        [DefaultValue(true)]

        public override bool IsEditable
        {
            get
            {
                return base.IsEditable;
            }

            set
            {
                base.IsEditable = value;
            }
        }

        private void CustomInitialize()
        {
            AutoExpandSelection = true;
            Value = DateTime.Today;//default value
            IsEditable = true;//default value
        }
    }
}