﻿#if !MODEL
#if ASPNETCORE
using System.Collections.Generic;
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif
using System.ComponentModel;

namespace C1.Web.Mvc
{
    /// <summary>
    /// The Menu control shows a text element with a drop-down list of commands that the user can invoke by click or touch.
    /// </summary>
    public class Menu : MenuBase<object>
    {
#if ASPNETCORE
        internal List<MenuItem> MenuItems { get; set; }
#endif

#if !MODEL
        /// <summary>
        /// Creates one <see cref="Menu"/> instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        /// <param name="selector">The selector</param>
        public Menu(HtmlHelper helper, string selector = null) : base(helper, selector)
#else
        /// <summary>
        /// Creates one <see cref="Menu"/> instance.
        /// </summary>
        public Menu()
#endif
        {
            Command = new MenuCommand();
            DisplayMemberPath = "Header";
            CommandPath = "Command";
            CommandParameterPath = "CommandParameter";
            IsContentHtml = true;
            IsRequired = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the drop-down list displays items as plain text or as HTML.
        /// </summary>
        [DefaultValue(true)]
        public override bool IsContentHtml
        {
            get
            {
                return base.IsContentHtml;
            }
            set
            {
                base.IsContentHtml = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the control value must be set to a non-null value or whether it can be set to null (by deleting the content of the control).
        /// </summary>
        [DefaultValue(false)]
        public override bool IsRequired
        {
            get
            {
                return base.IsRequired;
            }

            set
            {
                base.IsRequired = value;
            }
        }
    }
}