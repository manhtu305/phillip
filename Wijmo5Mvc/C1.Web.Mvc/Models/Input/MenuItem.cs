﻿namespace C1.Web.Mvc
{
    public partial class MenuItem
    {
        #region IsSeparator
        private const string SeparatorMarkup = "<div class='wj-separator' style='width:100%;height:1px;margin:3px 0;background-color:rgba(0,0,0,.2)'></div>";
        private bool _isSeparator = false;
        private bool _GetIsSeparator()
        {
            return _isSeparator;
        }
        private void _SetIsSeparator(bool value)
        {
            _isSeparator = value;
            if (_isSeparator)
            {
                Header = SeparatorMarkup;
            }
        }
        #endregion IsSeparator
    }
}