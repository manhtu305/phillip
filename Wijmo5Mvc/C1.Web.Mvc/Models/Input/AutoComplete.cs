﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc
{
    partial class AutoComplete<T>
    {
        // an internal property for ComboxFor and the for attribute.
        internal object[] Values
        {
            get;
            set;
        }
    }
}
