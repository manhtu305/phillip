﻿using System;

namespace C1.Web.Mvc
{
    public partial class InputDateBase
    {
        private void CustomInitialize()
        {
            Value = DateTime.Today;
        }

        private bool _GetRequired()
        {
            return IsRequired;
        }

        private void _SetRequired(bool value)
        {
            IsRequired = value;
        }
    }
}
