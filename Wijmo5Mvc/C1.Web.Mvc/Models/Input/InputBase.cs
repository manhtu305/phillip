﻿using C1.JsonNet;

namespace C1.Web.Mvc
{
    public partial class FormInputBase
    {
        /// <summary>
        ///  Gets or sets the name of the form input control.
        /// </summary>
        [Ignore]
        public string Name
        {
            get { return HtmlAttributes["wj-name"]; }
            set { HtmlAttributes["wj-name"] = value; }
        }
    }
}
