﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines an interface for DropDown.
    /// </summary>
    public interface IDropDown
    {
        /// <summary>
        /// Gets or sets a value that indicates whether the control should automatically expand
        /// the selection to whole words/numbers when the control is clicked.
        /// </summary>
        bool AutoExpandSelection
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a CSS class name to add to the control's drop-down element.
        /// </summary>
        /// <remarks>
        /// This property is useful when styling the drop-down element, because it is
        /// shown as a child of the document body rather than as a child of the control
        /// itself, which prevents using CSS selectors based on the parent control.
        /// </remarks>
        string DropDownCssClass
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value that indicates whether the control should use a fade-in animation
        /// when displaying the drop-down.
        /// </summary>
        bool IsAnimated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the drop down is currently visible.
        /// </summary>
        bool IsDroppedDown
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value that indicates whether the user can modify
        /// the control value using the mouse and keyboard.
        /// </summary>
        bool IsReadOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the control value must be a non-empty string.
        /// </summary>
        bool IsRequired
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the string shown as a hint when the control is empty.
        /// </summary>
        string Placeholder
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the control should display a drop-down button.
        /// </summary>
        bool ShowDropDownButton
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the text shown on the control.
        /// </summary>
        string Text
        {
            get;
            set;
        }

        #region Client Events
        /// <summary>
        /// Occurs before the drop down is shown or hidden.
        /// </summary>
        string OnClientIsDroppedDownChanging
        {
            get;
            set;
        }

        /// <summary>
        /// Occurs after the drop down is shown or hidden.
        /// </summary>
        string OnClientIsDroppedDownChanged
        {
            get;
            set;
        }

        /// <summary>
        /// Occurs when the value of the Text property changes.
        /// </summary>
        string OnClientTextChanged
        {
            get;
            set;
        }
        #endregion Client Events
    }
}
