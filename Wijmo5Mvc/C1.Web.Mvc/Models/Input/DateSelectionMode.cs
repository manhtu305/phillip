﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies constants that define the date selection behavior.
    /// </summary>
    public enum DateSelectionMode
    {
        /// <summary>
        /// The user cannot change the current value using the mouse or keyboard.
        /// </summary>
        None = 0,
        /// <summary>
        /// The user can select days.
        /// </summary>
        Day = 1,
        /// <summary>
        /// The user can select months.
        /// </summary>
        Month = 2
    }
}