﻿namespace C1.Web.Mvc
{
    partial class AutoCompleteBase<T>
    {
        #region Name
        private string _GetName()
        {
            return HtmlAttributes["wj-name"];
        }
        private void _SetName(string value)
        {
            HtmlAttributes["wj-name"] = value;
        }
        #endregion Name

        #region IsContentHtml
        private bool _GetIsContentHtml()
        {
            return base.IsContentHtml;
        }
        private void _SetIsContentHtml(bool value)
        {
            base.IsContentHtml = value;
        }
        #endregion IsContentHtml

        #region IsEditable
        private bool _GetIsEditable()
        {
            return base.IsEditable;
        }
        private void _SetIsEditable(bool value)
        {
            base.IsEditable = value;
        }
        #endregion IsEditable

        #region IsRequired
        private bool _GetIsRequired()
        {
            return base.IsRequired;
        }
        private void _SetIsRequired(bool value)
        {
            base.IsRequired = value;
        }
        #endregion IsRequired
    }
}
