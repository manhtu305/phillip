﻿using C1.JsonNet;
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    public partial class MultiSelect<T>
    {
        /// <summary>
        ///  Gets or sets the name of the MultiSelect control.
        /// </summary>
        [Ignore]
        public string Name
        {
            get { return HtmlAttributes["wj-name"]; }
            set { HtmlAttributes["wj-name"] = value; }
        }

        #region CheckedIndexes

        private IList<int> _checkedIndexes;
        private IList<int> _GetCheckedIndexes()
        {
            if (_checkedIndexes == null)
            {
                _checkedIndexes = new List<int>();
            }
            return _checkedIndexes;
        }

        private void _SetCheckedIndexes(IList<int> value)
        {
            _checkedIndexes = value;
        }

        #endregion CheckedIndexes

        #region CheckedValues
        private IEnumerable<object> _checkedValues;
        private IEnumerable<object> _GetCheckedValues()
        {
            if (_checkedValues == null)
            {
                _checkedValues = new List<object>();
            }
            return _checkedValues;
        }

        private void _SetCheckedValues(IEnumerable<object> value)
        {
            _checkedValues = value;
        }

        #endregion CheckedValues
    }
}