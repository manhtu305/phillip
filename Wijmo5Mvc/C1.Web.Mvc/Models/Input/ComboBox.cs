﻿using C1.JsonNet;

namespace C1.Web.Mvc
{
    public partial class ComboBoxBase<T>
    {
        #region SelectedItem
#if !MODEL
        private bool _selectedItemIsSet;
#endif
        private T _selectedItem;
        private void _SetSelectedItem(T value)
        {
            _selectedItem = value;
#if !MODEL
            _selectedItemIsSet = true;
#endif
        }
        private T _GetSelectedItem()
        {
            return _selectedItem;
        }
        #endregion SelectedItem

        /// <summary>
        /// Gets or sets the value of the SelectedValue, obtained using the SelectedValuePath.
        /// </summary>
        #region SelectedValue
        private bool _selectedValueIsSet;
        private object _selectedValue;

        private object _GetSelectedValue()
        {
            return _selectedValue;
        }

        private void _SetSelectedValue(object value)
        {
            _selectedValue = value;
            _selectedValueIsSet = true;

        }
        #endregion SelectedValue

        internal override bool _ShouldSerializeItemsSource()
        {
            return (ItemsSource.SourceCollection != null || ItemsSource.BatchEditActionUrl != null
                    || ItemsSource.CreateActionUrl != null || ItemsSource.ReadActionUrl != null || ItemsSource.UpdateActionUrl != null || ItemsSource.DeleteActionUrl != null)
                   && base._ShouldSerializeItemsSource();
        }
    }

    public partial class ComboBox<T>
    {
        /// <summary>
        ///  Gets or sets the name of the ComboBox control.
        /// </summary>
        [Ignore]
        public string Name
        {
            get { return HtmlAttributes["wj-name"]; }
            set { HtmlAttributes["wj-name"] = value; }
        }

        // an internal property for ComboxFor and the for attribute.
        internal object[] Values
        {
            get;
            set;
        }
    }
}
