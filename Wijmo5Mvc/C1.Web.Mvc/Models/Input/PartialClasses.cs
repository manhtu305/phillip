﻿using System.Collections.Generic;
#if !ASPNETCORE
using System.Drawing;
#endif

namespace C1.Web.Mvc
{
    public partial class DropDown : IDropDown
    {
    }

    public partial class DropDown<T> : IDropDown
    {
    }

    public partial class ComboBoxBase<T>
    {
        #region ItemTemplateId
        private string _itemTemplateId;
        private string _GetItemTemplateId()
        {
            return _itemTemplateId;
        }
        private void _SetItemTemplateId(string id)
        {
            _itemTemplateContent = null;
            _itemTemplateId = id;
        }
        #endregion ItemTemplateId

        #region ItemTemplateContent
        private string _itemTemplateContent;
        private string _GetItemTemplateContent()
        {
            return _itemTemplateContent;
        }
        private void _SetItemTemplateContent(string content)
        {
            _itemTemplateId = null;
            _itemTemplateContent = content;
        }
        #endregion ItemTemplateContent

        #region IsRequired
        private bool _GetRequired()
        {
            return IsRequired;
        }
        private void _SetRequired(bool value)
        {
            IsRequired = value;
        }
    #endregion IsRequired

        private bool _ShouldSerializeSelectedValue()
        {
            return (IsRequired && SelectedValue != null) || (!IsRequired && _selectedValueIsSet);
        }

    private void CustomInitialize()
        {
            DisableServerRead();
        }
    }

    public partial class ListBox<T>
    {
        #region ItemTemplateId
        private string _itemTemplateId;
        private string _GetItemTemplateId()
        {
            return _itemTemplateId;
        }
        private void _SetItemTemplateId(string id)
        {
            _itemTemplateContent = null;
            _itemTemplateId = id;
        }
        #endregion ItemTemplateId

        #region ItemTemplateContent
        private string _itemTemplateContent;
        private string _GetItemTemplateContent()
        {
            return _itemTemplateContent;
        }
        private void _SetItemTemplateContent(string content)
        {
            _itemTemplateId = null;
            _itemTemplateContent = content;
        }
        #endregion ItemTemplateContent

        private void CustomInitialize()
        {
            DisableServerRead();
        }

        #region CheckedIndexes
        private IList<int> _checkedIndexes;
        private IList<int> _GetCheckedIndexes()
        {
            return _checkedIndexes ?? (_checkedIndexes = new List<int>());
        }
        private void _SetCheckedIndexes(IList<int> value)
        {
            _checkedIndexes = value;
        }
        #endregion CheckedIndexes

        #region CheckedValues
        private IEnumerable<object> _checkedValues;
        private IEnumerable<object> _GetCheckedValues()
        {
            return _checkedValues ?? (_checkedValues = new List<object>());
        }
        private void _SetCheckedValues(IEnumerable<object> value)
        {
            _checkedValues = value;
        }
        #endregion CheckedValues
    }

    public partial class ColorPicker
    {
        /// <summary>
        /// Gets or sets the currently selected color.
        /// </summary>
#if ASPNETCORE
        public string Value
#else
        public Color Value
#endif
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets an array that contains the colors in the palette.
        /// </summary>
        /// <remarks>
        /// The palette contains ten colors, represented by an array with ten strings. The first two colors are usually white and black.
        /// </remarks>
#if ASPNETCORE
        public List<string> Palette
#else
        public List<Color> Palette
#endif
        {
            get;
            set;
        }
    }

    public partial class InputColor
    {
        /// <summary>
        /// Gets or sets the current color.
        /// </summary>
#if ASPNETCORE
        public string Value
#else
        public Color? Value
#endif
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets an array that contains the colors in the palette.
        /// </summary>
        /// <remarks>
        /// The palette contains ten colors, represented by an array with ten strings. 
        /// The first two colors are usually white and black.
        /// </remarks>
#if ASPNETCORE
        public List<string> Palette
#else
        public List<Color> Palette
#endif
        {
            get;
            set;
        }

        #region IsRequired
        private bool _GetRequired()
        {
            return IsRequired;
        }
        private void _SetRequired(bool value)
        {
            IsRequired = value;
        }
        #endregion IsRequired
    }

    public partial class InputNumber
    {
        #region IsRequired
        private bool _GetRequired()
        {
            return IsRequired;
        }
        private void _SetRequired(bool value)
        {
            IsRequired = value;
        }
        #endregion IsRequired
    }

    public partial class MultiSelectListBox<T>
    {
        private void CustomInitialize()
        {
            DisableServerRead();
        }

        #region CheckedIndexes
        private IList<int> _checkedIndexes;
        private IList<int> _GetCheckedIndexes()
        {
            return _checkedIndexes ?? (_checkedIndexes = new List<int>());
        }
        private void _SetCheckedIndexes(IList<int> value)
        {
            _checkedIndexes = value;
        }
        #endregion CheckedIndexes

        #region CheckedValues
        private IEnumerable<object> _checkedValues;
        private IEnumerable<object> _GetCheckedValues()
        {
            return _checkedValues ?? (_checkedValues = new List<object>());
        }
        private void _SetCheckedValues(IEnumerable<object> value)
        {
            _checkedValues = value;
        }
        #endregion CheckedValues
    }
}