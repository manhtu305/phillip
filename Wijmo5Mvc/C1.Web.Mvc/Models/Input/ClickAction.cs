﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies constants that define the action to perform when the user clicks the input element in the control.
    /// </summary>
    public enum ClickAction
    {
        /// <summary>
        /// Selects the input element content.
        /// </summary>
        Select = 0,
        /// <summary>
        /// Open the drop-down.
        /// </summary>
        Open = 1,
        /// <summary>
        /// Toggle the drop-down.
        /// </summary>
        Toggle = 2
    }
}
