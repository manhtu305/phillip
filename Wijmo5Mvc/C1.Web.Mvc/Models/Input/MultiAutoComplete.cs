﻿using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    partial class MultiAutoComplete<T>
    {
        #region SelectedIndexes
        private IList<int> _selectedIndexes;
        private IList<int> _GetSelectedIndexes()
        {
            return _selectedIndexes ?? (_selectedIndexes = new List<int>());
        }
        private void _SetSelectedIndexes(IList<int> value)
        {
            _selectedIndexes = value;
        }
        #endregion SelectedIndexes

        #region SelectedValues
        private IEnumerable<object> _selectedValues;
        private IEnumerable<object> _GetSelectedValues()
        {
            return _selectedValues ?? (_selectedValues = new List<object>());
        }
        private void _SetSelectedValues(IEnumerable<object> value)
        {
            _selectedValues = value;
        }
        #endregion SelectedValues

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [JsonNet.Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int SelectedIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override T SelectedItem
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [JsonNet.Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override object SelectedValue
        {
            get;
            set;
        }
    }
}
