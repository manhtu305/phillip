﻿namespace C1.Web.Mvc
{
    public partial class DataPoint
    {
        /// <summary>
        /// Creates one <see cref="DataPoint"/> instance.
        /// </summary>
        /// <param name="x">The X coordinate value of this DataPoint.</param>
        /// <param name="y">The Y coordinate value of this DataPoint.</param>
        public DataPoint(object x, object y)
        {
            Initialize();
            X = x;
            Y = y;
        }

        /// <summary>
        /// Creates one <see cref="DataPoint"/> instance.
        /// </summary>
        public DataPoint()
        {
            Initialize();
        }
    }
}
