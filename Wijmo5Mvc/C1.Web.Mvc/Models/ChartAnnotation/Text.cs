﻿namespace C1.Web.Mvc
{
    public partial class Text
    {
        /// <summary>
        /// Creates one <see cref="Text"/> instance.
        /// </summary>
        public Text()
        {
            Initialize();
            Position = AnnotationPosition.Top;
        }
    }
}
