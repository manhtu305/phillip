﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies the attachment of the annotation.
    /// </summary>
    public enum AnnotationAttachment
    {
        /// <summary>
        /// Coordinates of the annotation point are defined by the data series index and the data point index.
        /// </summary>
        DataIndex,

        /// <summary>
        /// Annotation point is specified in data coordinates.
        /// </summary>
        DataCoordinate,

        /// <summary>
        /// Annotation point is specified as a relative position inside the control where
        /// (0,0) is the top left corner and (1,1) is the bottom right corner.
        /// </summary>
        Relative,

        /// <summary>
        /// The annotation point is specified in control's pixel coordinates.
        /// </summary>
        Absolute

    }
}
