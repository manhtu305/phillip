﻿using System.Collections.Generic;
namespace C1.Web.Mvc
{
    public partial class AnnotationLayer<T>
    {
        /// <summary>
        /// Creates one <see cref="AnnotationLayer{T}"/> instance.
        /// </summary>
        /// <param name="target">A <see cref="FlexChartCore{T}"/> which owns this annotation layer.</param>
        public AnnotationLayer(FlexChartCore<T> target)
            : base(target)
        {
            Initialize();
        }

        #region Items
        private IList<AnnotationBase> _items;
        private IList<AnnotationBase> _GetItems()
        {
            return _items ?? (_items = new List<AnnotationBase>());
        }
        #endregion Items
    }
}
