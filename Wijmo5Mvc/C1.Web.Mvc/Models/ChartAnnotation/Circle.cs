﻿namespace C1.Web.Mvc
{
    public partial class Circle
    {
        /// <summary>
        /// Creates one <see cref="Circle"/> instance.
        /// </summary>
        public Circle()
        {
            Initialize();
        }
    }
}
