﻿namespace C1.Web.Mvc
{
    public partial class Line
    {
        /// <summary>
        /// Creates one <see cref=" Line"/> instance.
        /// </summary>
        public Line()
        {
            Initialize();
            Position = AnnotationPosition.Top;
        }
    }
}
