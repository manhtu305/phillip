﻿using System.ComponentModel;

namespace C1.Web.Mvc
{
    public abstract partial class AnnotationBase
    {
        /// <summary>
        /// Creates one <see cref="AnnotationBase"/> instance.
        /// </summary>
        protected AnnotationBase()
        {
            Initialize();
        }

        /// <summary>
        /// Gets the Annotation type, use for json serialization.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
#if MODEL
        [Serialization.C1Ignore]
#endif
        public virtual string Type
        {
            get
            {
                return this.GetType().Name;
            }
        }

        #region Style
        private SVGStyle _style;
        private SVGStyle _GetStyle()
        {
            return _style ?? (_style = new SVGStyle());
        }
        private void _SetStyle(SVGStyle style)
        {
            _style = style;
        }
        private bool _ShouldSerializeStyle()
        {
            return !Style.IsDefault;
        }
        #endregion Style

        private bool _ShouldSerializePosition()
        {
            if (Type.Equals("Text") || Type.Equals("Line"))
            {
                return Position != AnnotationPosition.Top;
            }
            else
            {
                return Position != AnnotationPosition.Center;
            }
        }
    }
}
