﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies the position of the annotation.
    /// </summary>
    public enum AnnotationPosition
    {
        /// <summary>
        /// The annotation appears at the Center of the target point. 
        /// </summary>
        Center = 0,

        /// <summary>
        /// The annotation appears at the Top of the target point. 
        /// </summary>
        Top = 1,

        /// <summary>
        /// The annotation appears at the Bottom of the target point. 
        /// </summary>
        Bottom = 2,

        /// <summary>
        /// The annotation appears at the Left of the target point. 
        /// </summary>
        Left = 4,

        /// <summary>
        /// The annotation appears at the Right of the target point.
        /// </summary>
        Right = 8
    }
}
