﻿namespace C1.Web.Mvc
{
    public abstract partial class Shape
    {
        /// <summary>
        /// Creates one <see cref="Shape"/> instance.
        /// </summary>
        protected Shape()
        {
            Initialize();
        }
    }
}
