﻿namespace C1.Web.Mvc
{
    public partial class FlexGridGroupPanel<T>
    {
        #region Ctors
        /// <summary>
        /// Creates one <see cref="FlexGridGroupPanel{T}"/> instance.
        /// </summary>
        /// <param name="grid">The grid which owns this group panel.</param>
        /// <param name="selector">The selector specifies the element of the group panel.</param>
        public FlexGridGroupPanel(FlexGridBase<T> grid, string selector = null)
            : base(grid)
        {
            Initialize();
            Selector = selector;
            this.grid = grid;
        }

        #endregion Ctors

        #region customize methods
        FlexGridBase<T> grid;
        FlexGridFilter<T> filter;

        private FlexGridFilter<T> _GetFilter()
        {
            if (filter == null) filter = new FlexGridFilter<T>(grid);
            return filter;
        }

        private bool _ShouldSerializeFilter()
        {
            return filter != null;
        }
        #endregion  customize methods
    }
}
