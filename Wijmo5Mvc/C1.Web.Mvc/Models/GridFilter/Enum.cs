﻿namespace C1.Web.Mvc.Grid
{
    /// <summary>
    /// Specifies filter condition operators.
    /// </summary>
    public enum Operator
    {
        /// <summary>
        /// Equals
        /// </summary>
        EQ = 0,

        /// <summary>
        /// Not equals.
        /// </summary>
        NE = 1,

        /// <summary>
        /// Greater than.
        /// </summary>
        GT = 2,

        /// <summary>
        /// Greater than or equal to.
        /// </summary>
        GE = 3,

        /// <summary>
        /// Less than.
        /// </summary>
        LT = 4,

        /// <summary>
        /// Less than or equal to.
        /// </summary>
        LE = 5,

        /// <summary>
        /// Begins with.
        /// </summary>
        BW = 6,

        /// <summary>
        /// Ends with.
        /// </summary>
        EW = 7,

        /// <summary>
        /// Contains.
        /// </summary>
        CT = 8,

        /// <summary>
        /// Does not contain.
        /// </summary>
        NC = 9 
    }
}
