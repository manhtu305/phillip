﻿namespace C1.Web.Mvc
{
    public partial class ConditionFilter
    {
        /// <summary>
        /// Gets whether the two conditions should be combined with an AND or an OR operator.
        /// </summary>
        public bool And
        {
            get;
            set;
        }

        #region Condition1
        private FilterCondition _condition1;
        private FilterCondition _GetCondition1()
        {
            return _condition1 ?? (_condition1 = new FilterCondition());
        }
        #endregion Condition1

        #region Condition2
        private FilterCondition _condition2;
        private FilterCondition _GetCondition2()
        {
            return _condition2 ?? (_condition2 = new FilterCondition());
        }
        #endregion Condition2
    }
}
