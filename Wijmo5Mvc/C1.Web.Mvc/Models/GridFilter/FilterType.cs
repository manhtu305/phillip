﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies types of column filter.
    /// </summary>
    public enum FilterType
    {
        /// <summary>
        /// No filter.
        /// </summary>
        None = 0,
        /// <summary>
        /// A filter based on two conditions.
        /// </summary>
        Condition = 1,
        /// <summary>
        /// A filter based on a set of values.
        /// </summary>
        Value = 2,
        /// <summary>
        /// A filter that combines condition and value filters.
        /// </summary>
        Both = 3
    }
}
