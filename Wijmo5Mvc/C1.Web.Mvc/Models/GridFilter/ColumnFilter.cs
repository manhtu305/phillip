﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

using System.Collections.Generic;

namespace C1.Web.Mvc
{
    public partial class ColumnFilter
    {
        #region Condition1
        private FilterCondition _GetCondition1()
        {
            return ConditionFilter.Condition1;
        }
        #endregion Condition1

        #region Condition2
        private FilterCondition _GetCondition2()
        {
            return ConditionFilter.Condition2;
        }
        #endregion Condition2

        #region And
        private bool _GetAnd()
        {
            return ConditionFilter.And;
        }
        private void _SetAnd(bool value)
        {
            ConditionFilter.And = value;
        }
        #endregion And

        #region ConditionFilter
        private ConditionFilter _conditionFilter;
        private ConditionFilter _GetConditionFilter()
        {
            return _conditionFilter ?? (_conditionFilter = new ConditionFilter());
        }
        #endregion ConditionFilter

        #region ValueFilter
        #region ShowValues
        private List<object> _GetShowValues()
        {
            return ValueFilter.ShowValues;
        }
        private void _SetShowValues(List<object> value)
        {
            ValueFilter.ShowValues = value;
        }
        #endregion ShowValues

        private ValueFilter _valueFilter;
        private ValueFilter _GetValueFilter()
        {
            return _valueFilter ?? (_valueFilter = new ValueFilter(
#if !MODEL
                _helper
#endif
                ));
        }
#endregion ValueFilter

#region Ctor
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="ColumnFilter"/> instance.
        /// </summary>
        public ColumnFilter()            
        {
            Initialize();
        }

        /// <summary>
        /// Creates one <see cref="ColumnFilter"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        internal ColumnFilter(HtmlHelper helper)            
        {
            _helper = helper;
            Initialize();
        }
#else
        public ColumnFilter()
        {
            Initialize();
        }
#endif
#endregion Ctor
    }
}
