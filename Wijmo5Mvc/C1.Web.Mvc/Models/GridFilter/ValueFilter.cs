﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

using System.Collections.Generic;

namespace C1.Web.Mvc
{
    public partial class ValueFilter
    {
        /// <summary>
        /// Gets a collection with all the formatted values that should be shown.
        /// </summary>
        /// <remarks>
        /// Null means to show all data.
        /// </remarks>
        public List<object> ShowValues
        {
            get;
            // This setter should be internal, but now we use PropertyDescriptor to Deserialize/Serialize the object in MVC3/4/5,
            // if this setter is internal, ShowValues' PropertyDescriptor will be read only, and cannot be set when deserializing, in next
            // release, we should use PropertyInfo instead.
            set;
        }

        #region Ctor
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="ValueFilter"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        internal ValueFilter(HtmlHelper helper)
        {
            _helper = helper;
            Initialize();
        }
#else       
#endif
        #endregion Ctor

        #region DataMap
        private DataMap _dataMap;
        private DataMap _GetDataMap()
        {
            if (_dataMap == null)
            {
#if !MODEL   
                _dataMap = new DataMap(_helper);
#else
                _dataMap = new DataMap();
#endif
            }
            return _dataMap;
        }

        private bool _ShouldSerializeDataMap()
        {
            return (DataMap.ShouldSerializeItemsSource() && !string.IsNullOrEmpty(DataMap.DisplayMemberPath)
                && !string.IsNullOrEmpty(DataMap.SelectedValuePath));
        }
        #endregion DataMap

    }
}
