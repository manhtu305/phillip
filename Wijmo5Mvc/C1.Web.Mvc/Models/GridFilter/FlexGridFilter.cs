﻿#if !MODEL
using C1.Web.Mvc.GridFilter;
using C1.Web.Mvc.Localization;
#endif
using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc
{
    public partial class FlexGridFilter<T>
    {
        #region Fields

#if !MODEL
        private readonly ICollectionView<T> _collectionView;
#endif

        #endregion Fields

        #region Ctors
        /// <summary>
        /// Creates one <see cref="FlexGridFilter{T}"/> instance.
        /// </summary>
        /// <param name="grid">The grid which owns the filter.</param>
        public FlexGridFilter(FlexGridBase<T> grid)
            : base(grid)
        {
            if (grid == null)
            {
                throw new ArgumentNullException("grid");
            }
#if !MODEL

            var cvService = grid.ItemsSource as CollectionViewService<T>;
            if (cvService != null)
            {
                _collectionView = cvService.CollectionView;
            }
            
#endif
            Initialize();
#if !MODEL && !ASPNETCORE
            if (cvService != null)
            {
                cvService.BeginningQuery += cvService_BeginningQuery;
            }
#endif
        }

        #endregion Ctors

        #region ColumnFilters
        private IList<ColumnFilter> _columnFilters;
        private IList<ColumnFilter> _GetColumnFilters()
        {
            return _columnFilters ?? (_columnFilters = new List<ColumnFilter>());
        }
        #endregion ColumnFilters

        #region FilterColumns
        private string[] _GetFilterColumns()
        {
            return ColumnFilters.Select(cf => cf.Column).ToArray();
        }

        private void _SetFilterColumns(string[] value)
        {
            SetColumnFilters(value);
        }
        #endregion FilterColumns

        #region Methods
#if !MODEL
        private void cvService_BeginningQuery(CollectionViewRequest<T> requestData)
        {
            if (requestData == null)
            {
                return;
            }
            FlexGridFilterHelper.ExecuteWithRequest<T>(_collectionView, requestData.ExtraRequestData);
        }
#endif

        internal void SetColumnFilters(IDictionary<string, FilterType> value)
        {
            ColumnFilters.Clear();
            foreach (var item in value)
            {
                var cf = new ColumnFilter() { Column = item.Key, FilterType = item.Value };
                ColumnFilters.Add(cf);
            }
        }

        internal void SetColumnFilters(string[] columns)
        {
            ColumnFilters.Clear();
            foreach (var item in columns)
            {
                var cf = new ColumnFilter() { Column = item, FilterType = FilterType.Condition };
                ColumnFilters.Add(cf);
            }
        }

        private IDictionary<string, FilterType> _GetColumnFilterTypes()
        {
            return ColumnFilters.Select(cf => 
                new KeyValuePair<string, FilterType>(cf.Column, cf.FilterType ?? FilterType.None))
                .ToDictionary(x => x.Key, x => x.Value);
        }

        private void _SetColumnFilterTypes(IDictionary<string, FilterType> value)
        {
            SetColumnFilters(value);
        }
        #endregion Methods

    }
}
