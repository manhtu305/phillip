﻿namespace C1.Web.Mvc
{
    public partial class BreakEvenStyles
    {
        #region FixedCost
        private SVGStyle _fixedCost;
        private SVGStyle _GetFixedCost()
        {
            return _fixedCost ?? (_fixedCost = new SVGStyle());
        }
        private void _SetFixedCost(SVGStyle value)
        {
            _fixedCost = value;
        }
        #endregion FixedCost

        #region VariableCost
        private SVGStyle _variableCost;
        private SVGStyle _GetVariableCost()
        {
            return _variableCost ?? (_variableCost = new SVGStyle());
        }
        private void _SetVariableCost(SVGStyle value)
        {
            _variableCost = value;
        }
        #endregion VariableCost

        #region TotalCost
        private SVGStyle _totalCost;
        private SVGStyle _GetTotalCost()
        {
            return _totalCost ?? (_totalCost = new SVGStyle());
        }
        private void _SetTotalCost(SVGStyle value)
        {
            _totalCost = value;
        }
        #endregion TotalCost

        #region SalesRevenue
        private SVGStyle _salesRevenue;
        private SVGStyle _GetSalesRevenue()
        {
            return _salesRevenue ?? (_salesRevenue = new SVGStyle());
        }
        private void _SetSalesRevenue(SVGStyle value)
        {
            _salesRevenue = value;
        }
        #endregion SalesRevenue

        #region SafetyMargin
        private SVGStyle _safetyMargin;
        private SVGStyle _GetSafetyMargin()
        {
            return _safetyMargin ?? (_safetyMargin = new SVGStyle());
        }
        private void _SetSafetyMargin(SVGStyle value)
        {
            _safetyMargin = value;
        }
        #endregion SafetyMargin

        #region MarginalProfit
        private SVGStyle _marginalProfit;
        private SVGStyle _GetMarginalProfit()
        {
            return _marginalProfit ?? (_marginalProfit = new SVGStyle());
        }
        private void _SetMarginalProfit(SVGStyle value)
        {
            _marginalProfit = value;
        }
        #endregion MarginalProfit

        #region BreakEven
        private SVGStyle _breakEven;
        private SVGStyle _GetBreakEven()
        {
            return _breakEven ?? (_breakEven = new SVGStyle());
        }
        private void _SetBreakEven(SVGStyle value)
        {
            _breakEven = value;
        }
        #endregion BreakEven

        /// <summary>
        /// Creates one <see cref="BreakEvenStyles"/> instance.
        /// </summary>
        public BreakEvenStyles()
        {
            Initialize();
        }
    }
}
