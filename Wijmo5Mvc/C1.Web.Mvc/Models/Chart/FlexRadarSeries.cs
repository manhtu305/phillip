﻿namespace C1.Web.Mvc
{
    public partial class FlexRadarSeries<T>
    {
        /// <summary>
        /// Creates one <see cref="FlexRadarSeries{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public FlexRadarSeries(FlexRadar<T> owner)
            : base(owner)
        {
            Initialize();
        }
    }
}
