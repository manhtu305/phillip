﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies whether and where the Series is visible.
    /// </summary>
    public enum TrendLineFitType
    {   
        /// <summary>
        /// A straight line that most closely approximates the data.  Y(x) = a * x + b.
        /// </summary> 
        Linear,
        /// <summary>
        /// Regression fit to the equation Y(x) = a * exp(b*x).
        /// </summary> 
        Exponential,
        /// <summary>
        /// Regression fit to the equation Y(x) = a * ln(x) + b.
        /// </summary> 
        Logarithmic,
        /// <summary>
        /// Regression fit to the equation Y(x) = a * pow(x, b).
        /// </summary> 
        Power,
        /// <summary>
        /// Regression fit to the equation Y(x) = a + b * cos(x) + c * sin(x) + d * cos(2*x) + e * sin(2*x) + ...
        /// </summary> 
        Fourier,
        /// <summary>
        /// Regression fit to the equation Y(x) = a * x^n + b * x^n-1 + c * x^n-2 + ... + z.
        /// </summary> 
        Polynomial,
        /// <summary>
        /// The minimum X-value. 
        /// </summary> 
        MinX,
        /// <summary>
        /// The minimum Y-value. 
        /// </summary> 
        MinY,
        /// <summary>
        /// The maximum X-value. 
        /// </summary> 
        MaxX,
        /// <summary>
        /// The maximum Y-value. 
        /// </summary> 
        MaxY,
        /// <summary>
        /// The average X-value. 
        /// </summary> 
        AverageX,
        /// <summary>
        /// The average Y-value.
        /// </summary> 
        AverageY
    }
}
