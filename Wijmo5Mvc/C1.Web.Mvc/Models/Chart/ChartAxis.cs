﻿using System;
using C1.Web.Mvc.Chart;
#if !MODEL
using C1.Web.Mvc.Localization;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents chart axis.
    /// </summary>
    public partial class ChartAxis<T>
    {
        #region Fields
        private IItemsSource<T> _itemsSource;
        internal FlexChartCore<T> _owner;
        internal bool _isHorizontal = true;
        internal double? _logBase;
        #endregion Fields

        #region Ctor
        /// <summary>
        /// Creates one <see cref="ChartAxis{T}"/> instance.
        /// </summary>
        public ChartAxis()
        {
            Initialize();
            AxisLine = true;
        }

        internal ChartAxis(FlexChartCore<T> owner, bool isHorizontal)
            :this()
        {
            _owner = owner;
            _isHorizontal = isHorizontal;
            if (isHorizontal)
            {
                Position = Position.Bottom;
                MajorTickMarks = AxisTickMark.Outside;
            }
            else
            {
                AxisLine = false;
                Position = Position.Left;
                MajorGrid = true;
            }
        }
        #endregion Ctors

        private bool _ShouldSerializePosition()
        {
            return (_isHorizontal && Position != Position.Bottom) ||
                (!_isHorizontal && Position != Position.Left);
        }

        private bool _ShouldSerializeAxisLine()
        {
            return (_isHorizontal && AxisLine != true) || 
                (!_isHorizontal && AxisLine != false);
        }

        private bool _ShouldSerializeMajorGrid()
        {
            return (_isHorizontal && MajorGrid != false) ||
                (!_isHorizontal && MajorGrid != true);
        }

        private bool _ShouldSerializeMajorTickMarks()
        {
            return (_isHorizontal && MajorTickMarks != AxisTickMark.Outside) ||
                (!_isHorizontal && MajorTickMarks != AxisTickMark.None);
        }

        private IItemsSource<T> _GetItemsSource()
        {
            if (_owner == null) 
            {
                return null;
            }
#if !MODEL
            return _itemsSource ?? (_itemsSource = new CollectionViewService<T>(_owner.Helper));
#else
            return _itemsSource ?? (_itemsSource = new CollectionViewService<T>());
#endif
        }
        private void _SetItemsSource(IItemsSource<T> value)
        {
            _itemsSource = value;
        }
        private bool _ShouldSerializeItemsSource()
        {
#if !MODEL
            return (ItemsSource != null) && !(string.IsNullOrEmpty(ItemsSource.ReadActionUrl) && ItemsSource.SourceCollection == null);
#else
            return ItemsSourceBinding.IsItemsSourceDirty(DbContextType, ModelType);
#endif
        }

        private double? _GetLogBase() 
        {
            return _logBase;
        }

        private void _SetLogBase(double? value)
        {
#if !MODEL
            if (value <= 0 || value == 1)
            {
                throw new ArgumentOutOfRangeException("value", Resources.ChartAxisInvalidLogBase);
            }
            else 
            {
                _logBase = value;
            }
#else
            _logBase = value;
#endif
        }

        internal bool IsDefault(bool isHorizontal) 
        {
            return
                ((isHorizontal
                && Position == Chart.Position.Bottom
                && MajorGrid == false && MajorTickMarks == Chart.AxisTickMark.Outside
                && AxisLine == true) ||
                (!isHorizontal
                && Position == Chart.Position.Left
                && MajorGrid == true && MinorGrid==false
                && MajorTickMarks == Chart.AxisTickMark.None
                && AxisLine == false))
                && MinorTickMarks == Chart.AxisTickMark.None
                && Min == null && Max == null
                && Reversed == false
                && MajorUnit == 0 && MinorUnit == 0
                && string.IsNullOrEmpty(Name + Title + Format + ItemFormatter + OnClientRangeChanged + Binding)
                && Labels == true && LabelAngle == null
                && Origin == null
                && OverlappingLabels == Chart.AxisOverlappingLabels.Auto
                && _logBase == null
                && PlotAreaIndex == null
                && !_ShouldSerializeItemsSource();
        }
    }
}
