﻿namespace C1.Web.Mvc
{
    public partial class PlotArea
    {
        #region Style
        private SVGStyle _style;
        private SVGStyle _GetStyle()
        {
            return _style ?? (_style = new SVGStyle());
        }
        private void _SetStyle(SVGStyle style)
        {
            _style = style;
        }
        #endregion Style

        /// <summary>
        /// Creats one <see cref="PlotArea"/> instance.
        /// </summary>
        public PlotArea()
        {
            Initialize();
        }
    }
}
