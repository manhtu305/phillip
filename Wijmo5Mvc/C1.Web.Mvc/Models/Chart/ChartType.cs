﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies which type of chart users want to create.
    /// </summary>
    public enum ChartType
    {
        /// <summary>
        /// Column charts show vertical bars and allow you to compare values of items across categories.
        /// </summary>
        Column,
        /// <summary>
        /// Bar charts show horizontal bars.
        /// </summary>
        Bar,
        /// <summary>
        /// Scatter charts use X and Y coordinates to show patterns within the data.
        /// </summary>
        Scatter,
        /// <summary>
        /// Line charts show trends over a period of time or across categories.
        /// </summary>
        Line,
        /// <summary>
        /// Line and symbol charts are line charts with a symbol on each data point.
        /// </summary>
        LineSymbols,
        /// <summary>
        /// Area charts are line charts with the area below the line filled with color.
        /// </summary>
        Area,
        /// <summary>
        /// Bubble charts are Scatter charts with a 
        /// third data value that determines the size of the symbol.
        /// </summary>
        Bubble,
        /// <summary>
        /// Candlestick charts present items with high, low, open, and close values.
        /// The size of the wick line is determined by the High and Low values, while the size of the bar is
        /// determined by the Open and Close values. The bar is displayed using different colors, depending on 
        /// whether the close value is higher or lower than the open value.
        /// </summary>
        Candlestick,
        /// <summary>
        /// High-low-open-close charts display the same information as a candlestick chart, except that opening
        /// values are displayed using lines to the left, while lines to the right indicate closing values.
        /// </summary>
        HighLowOpenClose,
        /// <summary>
        /// Spline charts are line charts that plot curves rather than angled lines through the data points.
        /// </summary>
        Spline,
        /// <summary>
        /// Spline and symbol charts are spline charts with symbols on each data point.
        /// </summary>
        SplineSymbols,
        /// <summary>
        /// Spline area charts are spline charts with the area below the line filled with color.
        /// </summary>
        SplineArea,
        /// <summary>
        /// Displays funnel chart.
        /// </summary>
        Funnel,
        /// <summary>
        /// Displays a step chart.
        /// </summary>
        Step,
        /// <summary>
        /// Displays a step chart with symbols on each data point.
        /// </summary>
        StepSymbols,
        /// <summary>
        /// Displays a step area chart.
        /// </summary>
        StepArea        
    }
}
