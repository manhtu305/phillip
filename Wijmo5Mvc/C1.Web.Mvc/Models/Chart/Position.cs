﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the position of an axis or legend on the chart.
    /// </summary>
    public enum Position
    {
        /// <summary>
        /// The item is not visible.
        /// </summary>
        None,

        /// <summary>
        /// The item appears to the left of the chart.
        /// </summary>
        Left,

        /// <summary>
        /// The item appears above the chart.
        /// </summary>
        Top,

        /// <summary>
        /// The item appears to the right of the chart.
        /// </summary>
        Right,

        /// <summary>
        /// The item appears below the chart.
        /// </summary>
        Bottom,

        /// <summary>
        /// The item is positioned automatically.
        /// </summary>
        Auto
    }
}
