﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies whether and where the Series is visible.
    /// </summary>
    public enum MovingAverageType
    {   
        /// <summary>
        /// The simple moving average is simply the average of the last n values.
        /// </summary> 
        Simple,
        /// <summary>
        /// The weighted moving average is a weighted average of the last n values, where the weighting decreases by 1 with each previous value.
        /// </summary> 
        Weighted,
        /// <summary>
        /// The exponential moving average is a weighted average of the last n values, where the weighting decreases exponentially with each previous value.
        /// </summary> 
        Exponential,
        /// <summary>
        /// The triangular moving average is a weighted average of the last n values, whose result is equivalent to a double smoothed simple moving average. 
        /// </summary> 
        Triangular
    }
}
