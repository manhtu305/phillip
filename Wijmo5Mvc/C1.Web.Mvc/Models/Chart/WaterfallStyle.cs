﻿namespace C1.Web.Mvc
{
    public partial class WaterfallStyles
    {
        #region Start
        private SVGStyle _start;
        private SVGStyle _GetStart()
        {
            return _start ?? (_start = new SVGStyle());
        }
        private void _SetStart(SVGStyle value)
        {
            _start = value;
        }
        #endregion Start

        #region Total
        private SVGStyle _total;
        private SVGStyle _GetTotal()
        {
            return _total ?? (_total = new SVGStyle());
        }
        private void _SetTotal(SVGStyle value)
        {
            _total = value;
        }
        #endregion Total

        #region IntermediateTotal
        private SVGStyle _intermediateTotal;
        private SVGStyle _GetIntermediateTotal()
        {
            return _intermediateTotal ?? (_intermediateTotal = new SVGStyle());
        }
        private void _SetIntermediateTotal(SVGStyle value)
        {
            _intermediateTotal = value;
        }
        #endregion IntermediateTotal

        #region Falling
        private SVGStyle _falling;
        private SVGStyle _GetFalling()
        {
            return _falling ?? (_falling = new SVGStyle());
        }
        private void _SetFalling(SVGStyle value)
        {
            _falling = value;
        }
        #endregion Falling

        #region Rising
        private SVGStyle _rising;
        private SVGStyle _GetRising()
        {
            return _rising ?? (_rising = new SVGStyle());
        }
        private void _SetRising(SVGStyle value)
        {
            _rising = value;
        }
        #endregion Rising

        #region ConnectorLines
        private SVGStyle _connectorLines;
        private SVGStyle _GetConnectorLines()
        {
            return _connectorLines ?? (_connectorLines = new SVGStyle());
        }
        private void _SetConnectorLines(SVGStyle value)
        {
            _connectorLines = value;
        }
        #endregion ConnectorLines

        /// <summary>
        /// Creates one <see cref="WaterfallStyles"/> instance.
        /// </summary>
        public WaterfallStyles()
        {
            Initialize();
        }
    }
}
