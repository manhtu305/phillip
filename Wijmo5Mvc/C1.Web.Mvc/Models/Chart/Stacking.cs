﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies whether and how to stack the chart's data values.
    /// </summary>
    public enum Stacking
    {
        /// <summary>
        /// No stacking. Each series object is plotted independently.
        /// </summary>
        None,
        /// <summary>
        /// Stacked charts show how each value contributes to the total.
        /// </summary>
        Stacked,
        /// <summary>
        /// 100% stacked charts show how each value contributes to the total with the relative size of 
        /// each series representing its contribution to the total.
        /// </summary>
        Stacked100pc
    }
}
