﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the type of radar chart.
    /// </summary>
    public enum RadarChartType
    {
        /// <summary>
        /// Shows vertical bars and allows you to compare values of items across categories.
        /// </summary>
        Column,
        /// <summary>
        /// Shows patterns within the data using X and Y coordinates.
        /// </summary>
        Scatter,
        /// <summary>
        /// Shows trends over a period of time or across categories.
        /// </summary>
        Line,
        /// <summary>
        /// Shows line chart with a symbol on each data point.
        /// </summary>
        LineSymbols,
        /// <summary>
        /// Shows line chart with the area below the line filled with color.
        /// </summary>
        Area
    }
}
