﻿using System;
#if !MODEL
using C1.Web.Mvc.Localization;
#endif

namespace C1.Web.Mvc
{
    public partial class FlexPieBase<T> : IAnimatable
    {
        #region InnerRadius
        private float _innerRadius;
        private float _GetInnerRadius()
        {
            return _innerRadius;
        }
        private void _SetInnerRadius(float value)
        {
#if !MODEL
            if (value < 0 || value >= 1)
            {
                throw new ArgumentOutOfRangeException("value", Resources.FlexPieInnerRadiusOutOfRangeError);
            }
#endif

            _innerRadius = value;
        }
        #endregion InnerRadius

        #region DataLabel
        private PieDataLabel _dataLabel;
        private PieDataLabel _GetDataLabel()
        {
            return _dataLabel ?? (_dataLabel = new PieDataLabel());
        }
        #endregion DataLabel

        #region InnerTextStyle
        private SVGStyle _innerTextStyle;
        private SVGStyle _GetInnerTextStyle()
        {
            return _innerTextStyle ?? (_innerTextStyle = new SVGStyle());
        }
        private void _SetInnerTextStyle(SVGStyle style)
        {
            _innerTextStyle = style;
        }
        private bool _ShouldSerializeInnerTextStyle()
        {
            return _innerTextStyle != null && !_innerTextStyle.IsDefault;
        }
        #endregion InnerTextStyle
    }
}
