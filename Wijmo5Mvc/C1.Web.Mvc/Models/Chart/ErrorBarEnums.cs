﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the error amount of the series, it can be a standard error amount, a percentage or a standard deviation.
    /// </summary>
    public enum ErrorAmount
    {
        /// <summary>
        /// Indicates the error with a fixed value.
        /// </summary>
        FixedValue,
        /// <summary>
        /// Indicates the error with a percentage value.
        /// </summary>
        Percentage,
        /// <summary>
        /// Indicates the error with the standard deviation.
        /// </summary>
        StandardDeviation,
        /// <summary>
        /// Indicates the error with the standard error.
        /// </summary>
        StandardError,
        /// <summary>
        /// Indicates the error with the custom value.
        /// </summary>
        Custom
    }

    /// <summary>
    /// Specifies the end style of the error bar.
    /// </summary>
    public enum ErrorBarEndStyle
    {
        /// <summary>
        /// Error bar with cap.
        /// </summary>
        Cap,
        /// <summary>
        /// Error bar without cap.
        /// </summary>
        NoCap
    }

    /// <summary>
    /// Specifies the direction of the error bar.
    /// </summary>
    public enum ErrorBarDirection
    {
        /// <summary>
        /// Shows both direction.
        /// </summary>
        Both,
        /// <summary>
        /// Only shows minus direction.
        /// </summary>
        Minus,
        /// <summary>
        /// Only shows plus direction.
        /// </summary>
        Plus
    }

}
