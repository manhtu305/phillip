﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies whether and where axis tick marks appear.
    /// </summary>
    public enum AxisTickMark
    {
        /// <summary>
        /// No tick marks appear.
        /// </summary>
        None,
        /// <summary>
        /// Tick marks appear outside the plot area.
        /// </summary>
        Outside,
        /// <summary>
        /// Tick marks appear inside the plot area.
        /// </summary>
        Inside,
        /// <summary>
        /// Tick marks cross the axis.
        /// </summary>
        Cross
    }
}
