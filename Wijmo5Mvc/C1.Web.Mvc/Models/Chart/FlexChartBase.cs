﻿using C1.JsonNet;
#if !ASPNETCORE
using System.Drawing;
#endif
#if !MODEL && ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
#endif
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    public partial class FlexChartBase<T>: IExtendable
    {
        #region Legend
        private ChartLegend _legend;
        private ChartLegend _GetLegend()
        {
            return _legend ?? (_legend = new ChartLegend());
        }
        private bool _ShouldSerializeLegend()
        {
            return !Legend.IsDefault;
        }
        #endregion Legend

        #region HeaderStyle
        private TitleStyle _headerStyle;
        private TitleStyle _GetHeaderStyle()
        {
            return _headerStyle ?? (_headerStyle = new TitleStyle());
        }
        #endregion HeaderStyle

        #region FooterStyle
        private TitleStyle _footerStyle;
        private TitleStyle _GetFooterStyle()
        {
            return _footerStyle ?? (_footerStyle = new TitleStyle());
        }
        #endregion FooterStyle

        #region Tooltip
        private ChartTooltip _tooltip;
        private ChartTooltip _GetTooltip()
        {
            return _tooltip ?? (_tooltip = new ChartTooltip());
        }
        #endregion Tooltip

        /// <summary>
        /// Gets or sets an array of default colors to use for displaying each series.
        /// </summary>
        /// <remarks>
        ///  There is a set of predefined palettes in the <see cref="Chart.Palettes"/> class that you can use.
        /// </remarks>
#if MODEL
        [Serialization.C1Ignore]
#endif
#if ASPNETCORE
        public virtual IEnumerable<string> Palette
#else
        public virtual IEnumerable<Color> Palette
#endif
        {
            get;
            set;
        }

        #region IExtendable
        private IList<Extender> _extenders;
        /// <summary>
        /// Gets the extender collection.
        /// </summary>
        [Ignore()]
        public IList<Extender> Extenders
        {
            get { return _extenders ?? (_extenders = new List<Extender>()); }
        }
        #endregion IExtendable
    }
}