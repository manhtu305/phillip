﻿using System.Collections.Generic;
#if !MODEL && ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
#endif

namespace C1.Web.Mvc
{
    public partial class FlexChartCore<T> : IAnimatable
    {
        #region AxisX
        private ChartAxis<T> _axisX;
        private ChartAxis<T> _GetAxisX()
        {
            return _axisX ?? (_axisX = new ChartAxis<T>(this, true));
        }
        private bool _ShouldSerializeAxisX()
        {
            return !AxisX.IsDefault(true);
        }
        #endregion AxisX

        #region AxisY
        private ChartAxis<T> _axisY;
        private ChartAxis<T> _GetAxisY()
        {
            return _axisY ?? (_axisY = new ChartAxis<T>(this, false));
        }
        private bool _ShouldSerializeAxisY()
        {
            return !AxisY.IsDefault(false);
        }
        #endregion AxisY

        #region DataLabel
        private DataLabel _dataLabel;
        private DataLabel _GetDataLabel()
        {
            return _dataLabel ?? (_dataLabel = new DataLabel());
        }
        #endregion DataLabel

        #region Series
        private List<ChartSeriesBase<T>> _series;
        private IList<ChartSeriesBase<T>> _GetSeries()
        {
            return _series ?? (_series = new List<ChartSeriesBase<T>>());
        }
        #endregion Series

        #region PlotAreas
        private List<PlotArea> _plotareas;
        private IList<PlotArea> _GetPlotAreas()
        {
            return _plotareas ?? (_plotareas = new List<PlotArea>());
        }
        #endregion PlotAreas
    }
}
