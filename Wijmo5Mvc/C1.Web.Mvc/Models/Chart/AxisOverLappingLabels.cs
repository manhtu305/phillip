﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies how to handle overlapped labels.
    /// </summary>
    public enum AxisOverlappingLabels
    {
        /// <summary>
        /// The overlapped labels are hidden.
        /// </summary>
        Auto,
        /// <summary>
        /// Show all labels, including overlapped.
        /// </summary>
        Show
    }
}
