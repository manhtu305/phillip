﻿namespace C1.Web.Mvc
{
    public partial class ChartLegend
    {
        internal const string DEFAULT_TITLE = "";
        internal const string DEFAULT_TITLE_ALIGN = "left";
        internal virtual bool IsDefault
        {
            get { return Position == Chart.Position.Right && Title == DEFAULT_TITLE && TitleAlign == DEFAULT_TITLE_ALIGN; }
        }
    }
}
