﻿namespace C1.Web.Mvc
{
    public partial class ExtraSeries<T>
    {
        /// <summary>
        /// Creates one <see cref="ExtraSeries{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        protected ExtraSeries(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        private string _GetExtraSeriesTypeName()
        {
            return "";
        }
    }

    public partial class TrendLineBase<T>
    {
        /// <summary>
        /// Creates one <see cref="TrendLineBase{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public TrendLineBase(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }
    }

    public partial class TrendLine<T>
    {
        /// <summary>
        /// Creates one <see cref="TrendLine{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public TrendLine(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the series name.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        public override string ExtraSeriesTypeName
        {
            get { return "TrendLine"; }
        }
    }

    public partial class MovingAverage<T>
    {
        /// <summary>
        /// Creates one <see cref="MovingAverage{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public MovingAverage(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the series name.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        public override string ExtraSeriesTypeName
        {
            get { return "MovingAverage"; }
        }
    }

    public partial class FunctionSeries<T>
    {
        /// <summary>
        /// Creates one <see cref="FunctionSeries{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public FunctionSeries(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }
    }

    public partial class YFunctionSeries<T>
    {
        /// <summary>
        /// Creates one <see cref="YFunctionSeries{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public YFunctionSeries(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the series name.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        public override string ExtraSeriesTypeName
        {
            get { return "YFunctionSeries"; }
        }
    }

    public partial class ParametricFunctionSeries<T>
    {
        /// <summary>
        /// Creates one <see cref="ParametricFunctionSeries{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public ParametricFunctionSeries(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the series name.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        public override string ExtraSeriesTypeName
        {
            get { return "ParametricFunctionSeries"; }
        }
    }

    public partial class Waterfall<T>
    {
        private WaterfallStyles _style;

        /// <summary>
        /// Creates one <see cref="Waterfall{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public Waterfall(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the series name.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        public override string ExtraSeriesTypeName
        {
            get { return "Waterfall"; }
        }

        private WaterfallStyles _GetStyles()
        {
            return _style ?? (_style = new WaterfallStyles());
        }
        private void _SetStyles(WaterfallStyles value)
        {
            _style = value;
        }
    }

    public partial class BoxWhisker<T>
    {
        private SVGStyle _meanLineStyle;
        private SVGStyle _meanMarkerStyle;

        /// <summary>
        /// Creates one <see cref="BoxWhisker{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public BoxWhisker(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        private SVGStyle _GetMeanLineStyle()
        {
            return _meanLineStyle ?? (_meanLineStyle = new SVGStyle());
        }

        private void _SetMeanLineStyle(SVGStyle style)
        {
            _meanLineStyle = style;
        }

        private bool _ShouldSerializeMeanLineStyle()
        {
            return _meanLineStyle != null && !_meanLineStyle.IsDefault;
        }

        private SVGStyle _GetMeanMarkerStyle()
        {
            return _meanMarkerStyle ?? (_meanMarkerStyle = new SVGStyle());
        }

        private void _SetMeanMarkerStyle(SVGStyle style)
        {
            _meanMarkerStyle = style;
        }

        private bool _ShouldSerializeMeanMarkerStyle()
        {
            return _meanMarkerStyle != null && !_meanMarkerStyle.IsDefault;
        }

        /// <summary>
        /// Gets the series name.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        public override string ExtraSeriesTypeName
        {
            get { return "BoxWhisker"; }
        }
    }

    public partial class ErrorBar<T>
    {
        private SVGStyle _errorBarStyle;

        /// <summary>
        /// Creates one <see cref="ErrorBar{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public ErrorBar(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        private SVGStyle _GetErrorBarStyle()
        {
            return _errorBarStyle ?? (_errorBarStyle = new SVGStyle());
        }

        private void _SetErrorBarStyle(SVGStyle style)
        {
            _errorBarStyle = style;
        }

        private bool _ShouldSerializeErrorBarStyle()
        {
            return _errorBarStyle != null && !_errorBarStyle.IsDefault;
        }

        /// <summary>
        /// Gets the series name.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        public override string ExtraSeriesTypeName
        {
            get { return "ErrorBar"; }
        }
    }

    public partial class BreakEven<T>
    {
        private BreakEvenStyles _style;

        /// <summary>
        /// Creates one <see cref="BreakEven{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public BreakEven(FlexChartCore<T> owner)
            : base(owner)
        {
            Initialize();
        }

        /// <summary>
        /// Gets the series name.
        /// </summary>
#if MODEL
        [Serialization.C1Ignore]
#endif
        public override string ExtraSeriesTypeName
        {
            get { return "BreakEven"; }
        }

        private BreakEvenStyles _GetStyles()
        {
            return _style ?? (_style = new BreakEvenStyles());
        }
        private void _SetStyles(BreakEvenStyles value)
        {
            _style = value;
        }
    }

}
