﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies what is selected when the user clicks the chart.
    /// </summary>
    public enum SelectionMode
    {
        /// <summary>
        /// Select neither series nor data points when the user clicks the chart.
        /// </summary>
        None,

        /// <summary>
        /// Select the whole Series when the user clicks it on the chart.​
        /// </summary>
        Series,

        /// <summary>
        /// Select the data point when the user clicks it on the chart. Since Line, Area, Spline, and SplineArea charts do not render individual data points, nothing is selected with this setting on those chart types.
        /// </summary>
        Point
    }
}