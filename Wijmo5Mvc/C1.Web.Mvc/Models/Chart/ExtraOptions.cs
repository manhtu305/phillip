﻿namespace C1.Web.Mvc
{
    public partial class ExtraOptions
    {
        internal bool IsDefault
        {
            get
            {
                return GroupWidth == "70%" && BubbleMinSize == 5 && BubbleMaxSize == 30 && !_ShouldSerializeFunnel();
            }
        }

        #region Funnel
        private FunnelOptions _funnel;
        private FunnelOptions _GetFunnel()
        {
            return _funnel ?? (_funnel = new FunnelOptions());
        }
        private bool _ShouldSerializeFunnel()
        {
            return Funnel.NeckWidth != 0.2f || Funnel.NeckHeight != 0f || Funnel.Type != FunnelType.Default;
        }
        #endregion Funnel

        #region Step
        private StepOptions _step;
        private StepOptions _GetStep()
        {
            return _step ?? (_step = new StepOptions());
        } 
        #endregion Step
    }

    /// <summary>
    /// Specifies the type of Funnel chart. It should be 'Rectangle' or 'Default'. NeckWidth and NeckHeight don't work if type is set to Rectangle.
    /// </summary>
    public enum FunnelType
    {
        /// <summary>
        /// Default
        /// </summary>
        Default,
        /// <summary>
        /// Rectangle
        /// </summary>
        Rectangle
    }

    /// <summary>
    /// Specifies the position of steps in Step chart. It should be 'Start', 'Centert or 'End'.
    /// </summary>
    public enum StepPosition
    {
        /// <summary>
        /// Start
        /// </summary>
        Start,
        /// <summary>
        /// Center
        /// </summary>
        Center,
        /// <summary>
        /// End
        /// </summary>
        End
    }
}
