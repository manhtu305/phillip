﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies whether and where the Series is visible.
    /// </summary>
    public enum SeriesVisibility
    {
        /// <summary>
        /// The series is visible on the plot and in the legend.
        /// </summary>
        Visible,
        /// <summary>
        /// The series is visible only on the plot.
        /// </summary>
        Plot,
        /// <summary>
        /// The series is visible only in the legend.
        /// </summary>
        Legend,
        /// <summary>
        /// The series is hidden.
        /// </summary>
        Hidden
    }
}
