﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the quartile calculation method of Box and Whisker chart.
    /// </summary>
    public enum QuartileCalculation
    {
        /// <summary>
        /// Include median value when calculate quartile.
        /// </summary>
        InclusiveMedian,
        /// <summary>
        /// Exclude median value when calculate quartile.
        /// </summary>
        ExclusiveMedian
    }
}
