﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the position of data labels on the chart.
    /// </summary>
    public enum LabelPosition
    {
        /// <summary>
        /// No data labels appear.
        /// </summary>
        None = 0,
        /// <summary>
        /// The labels appear to the left of the data points.
        /// </summary>
        Left = 1,
        /// <summary>
        /// The labels appear above the data points.
        /// </summary>
        Top = 2,
        /// <summary>
        /// The labels appear to the right of the data points.
        /// </summary>
        Right = 3,
        /// <summary>
        /// The labels appear below the data points.
        /// </summary>
        Bottom = 4,
        /// <summary>
        /// The labels appear centered on the data points.
        /// </summary>
        Center = 5
    }

    /// <summary>
    /// Specifies the position of data labels on the pie chart.
    /// </summary>
    public enum PieLabelPosition
    {
        /// <summary>
        /// No data labels.
        /// </summary>
        None = 0,
        /// <summary>
        /// The label appears inside the pie slice.
        /// </summary>
        Inside = 1,
        /// <summary>
        /// The item appears at the center of the pie slice.
        /// </summary>
        Center = 2,
        /// <summary>
        /// The item appears outside the pie slice.
        /// </summary>
        Outside = 3,
        /// <summary>
        /// The item appears inside the pie slice and depends of its angle.
        /// </summary>
        Radial = 4,
        /// <summary>
        /// The item appears inside the pie slice and has circular direction.
        /// </summary>
        Circular = 5
    }
}
