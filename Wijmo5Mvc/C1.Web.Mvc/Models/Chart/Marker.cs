﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the type of marker to use for the @see:symbolMarker property.
    /// Applies to Scatter, LineSymbols, and SplineSymbols chart types.
    /// </summary>
    public enum Marker
    {
        /// <summary>
        /// Uses a circle to mark each data point.
        /// </summary>
        Dot,
        /// <summary>
        /// Uses a square to mark each data point.
        /// </summary>
        Box
    }
}
