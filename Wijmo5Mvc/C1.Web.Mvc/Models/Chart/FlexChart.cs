﻿namespace C1.Web.Mvc
{
    public partial class FlexChart<T>
    {
        #region Options
        private ExtraOptions _options;
        private ExtraOptions _GetOptions()
        {
            return _options ?? (_options = new ExtraOptions());
        }
        private bool _ShouldSerializeOptions()
        {
            return !Options.IsDefault;
        }
        #endregion Options
    }
}
