﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents SVG styles.
    /// </summary>
    public partial class SVGStyle
    {
        internal bool IsDefault
        {
            get
            {
                return StyleIsDefault();
            }
        }
        
        /// <summary>
        /// Creates one <see cref="SVGStyle"/> instance.
        /// </summary>
        public SVGStyle()
        {
            Initialize();
        }

        private bool StyleIsDefault()
        {
            if (string.IsNullOrEmpty(ArrowEnd) &&
                string.IsNullOrEmpty(ClipRect) &&
                string.IsNullOrEmpty(Cursor) &&
                string.IsNullOrEmpty(Fill) &&
                string.IsNullOrEmpty(Font) &&
                string.IsNullOrEmpty(ArrowEnd) &&
                string.IsNullOrEmpty(FontFamily) &&
                string.IsNullOrEmpty(FontWeight) &&
                string.IsNullOrEmpty(Href) &&
                string.IsNullOrEmpty(Path) &&
                string.IsNullOrEmpty(R) &&
                string.IsNullOrEmpty(Src) &&
                string.IsNullOrEmpty(Stroke) &&
                string.IsNullOrEmpty(StrokeDasharray) &&
                string.IsNullOrEmpty(StrokeLinecap) &&
                string.IsNullOrEmpty(StrokeLinejoin) &&
                string.IsNullOrEmpty(Target) &&
                string.IsNullOrEmpty(Text) &&
                string.IsNullOrEmpty(TextAnchor) &&
                string.IsNullOrEmpty(Title) &&
                string.IsNullOrEmpty(Transform)&&
                Cx == null &&
                Cy == null &&
                FillOpacity == null &&
                FontSize == null &&
                Height == null &&
                Opacity == null &&
                Rx == null &&
                Ry == null &&
                StrokeMiterlimit == null &&
                StrokeOpacity == null &&
                StrokeWidth == null &&
                Width == null &&
                X == null &&
                Y == null)
            {
                return true;
            }
                return false;
        }
    }
}
