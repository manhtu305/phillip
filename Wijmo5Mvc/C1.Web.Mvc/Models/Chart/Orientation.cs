﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the element orientation.
    /// </summary>
    public enum LegendOrientation
    {
        /// <summary>
        /// Orientation is selected automatically based on element position.
        /// </summary>
        Auto = 0,

        /// <summary>
        /// Vertical orientation.
        /// </summary>
        Vertical = 1,

        /// <summary>
        /// Horizontal orientation.
        /// </summary>
        Horizontal = 2
    }
}
