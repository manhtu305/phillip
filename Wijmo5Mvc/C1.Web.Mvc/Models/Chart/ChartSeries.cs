﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents a series of data points to display in the chart.
    /// </summary>
    public partial class ChartSeriesBase<T>
    {
        #region Fields
        private SVGStyle _style;
        private SVGStyle _altStyle;
        private SVGStyle _symbolStyle;
        private IItemsSource<T> _itemsSource;
        internal FlexChartCore<T> _owner;
        #endregion Fields

        #region Ctors
        /// <summary>
        /// Creates one <see cref="ChartSeriesBase{T}"/> instance.
        /// </summary>
        /// <param name="owner">The owner which owns this series.</param>
        public ChartSeriesBase(FlexChartCore<T> owner)
        {
            _owner = owner;
            Initialize();
        }
        #endregion Ctors

        #region Style
        private SVGStyle _GetStyle()
        {
            return _style ?? (_style = new SVGStyle());
        }
        private void _SetStyle(SVGStyle style)
        {
            _style = style;
        }
        private bool _ShouldSerializeStyle()
        {
            return !Style.IsDefault;
        }
        #endregion Style

        #region AltStyle
        private SVGStyle _GetAltStyle()
        {
            return _altStyle ?? (_altStyle = new SVGStyle());
        }
        private void _SetAltStyle(SVGStyle altStyle)
        {
            _altStyle = altStyle;
        }
        private bool _ShouldSerializeAltStyle()
        {
            return !AltStyle.IsDefault;
        }
        #endregion AltStyle

        #region SymbolStyle
        private SVGStyle _GetSymbolStyle()
        {
            return _symbolStyle ?? (_symbolStyle = new SVGStyle());
        }
        private void _SetSymbolStyle(SVGStyle style)
        {
            _symbolStyle = style;
        }
        private bool _ShouldSerializeSymbolStyle()
        {
            return !SymbolStyle.IsDefault;
        }
        #endregion SymbolStyle

        #region ItemsSource
        private IItemsSource<T> _GetItemsSource()
        {
#if !MODEL
            return _itemsSource ?? (_itemsSource = new CollectionViewService<T>(_owner.Helper));
#else
            return _itemsSource ?? (_itemsSource = new CollectionViewService<T>());
#endif
        }
        private bool _ShouldSerializeItemsSource()
        {
#if !MODEL
            return !(string.IsNullOrEmpty(ItemsSource.ReadActionUrl) && ItemsSource.SourceCollection == null);
#else
            return ItemsSourceBinding.IsItemsSourceDirty(DbContextType, ModelType);
#endif
        }
        private void _SetItemsSource(IItemsSource<T> value)
        {
            _itemsSource = value;
        }
        #endregion ItemsSource

        private bool _ShouldSerializeAxisX()
        {
            return AxisX != null;
        }

        private bool _ShouldSerializeAxisY()
        {
            return AxisY != null;
        }
    }

    public partial class ChartSeries<T>
    {
        /// <summary>
        /// Creates one <see cref="ChartSeries{T}"/> instance.
        /// </summary>
        /// <param name="owner">An object which derived from <see cref="FlexChart{T}"/>.</param>
        public ChartSeries(FlexChart<T> owner)
            : base(owner)
        {
        }
    }

    
}
