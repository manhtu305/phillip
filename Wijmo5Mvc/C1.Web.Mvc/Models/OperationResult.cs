﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// The result of an operation.
    /// </summary>
    public partial class OperationResult
    {
        private bool _success = true;
        private string _error;

        private bool _GetSuccess()
        {
            return string.IsNullOrEmpty(Error) && _success;
        }

        private void _SetSuccess(bool value)
        {
            _success = value;
        }


        private string _GetError()
        {
            return _error ?? (_error = string.Empty);
        }

        private void _SetError(string value)
        {
            _error = value;
        }

    }
}