﻿using System;

namespace C1.Web.Mvc
{
    public partial class ChartGestures<T>
    {
        #region Properties
        #region PosX
        private float _posX;
        private float _GetPosX()
        {
            return _posX;
        }
        private void _SetPosX(float value)
        {
            if (value != _posX)
            {
                _posX = GetValidValue(value);
            }
        }
        #endregion PosX

        #region PosY
        private float _posY;
        private float _GetPosY()
        {
            return _posY;
        }
        private void _SetPosY(float value)
        {
            if (value != _posY)
            {
                _posY = GetValidValue(value);
            }
        }
        #endregion PosY

        #region ScaleX
        private float _scaleX = 1;
        private float _GetScaleX()
        {
            return _scaleX;
        }
        private void _SetScaleX(float value)
        {
            if(value != _scaleX)
            {
                _scaleX = GetValidValue(value);
            }
        }
        #endregion ScaleX

        #region ScaleY
        private float _scaleY = 1;
        private float _GetScaleY()
        {
            return _scaleY;
        }
        private void _SetScaleY(float value)
        {
            if(value != _scaleY)
            {
                _scaleY = GetValidValue(value);
            }
        }
        #endregion ScaleY

        private float GetValidValue(float value)
        {
            return Math.Max(0, Math.Min(1, value));
        }
        #endregion Properties

        #region Ctors
        /// <summary>
        /// Creates one <see cref="ChartGestures{T}"/> instance.
        /// </summary>
        /// <param name="target">A <see cref="FlexChartCore{T}"/> which owns this chart gestures.</param>
        public ChartGestures(FlexChartCore<T> target)
            : base(target)
        {
            Initialize();
        }
        #endregion Ctors
    }
}
