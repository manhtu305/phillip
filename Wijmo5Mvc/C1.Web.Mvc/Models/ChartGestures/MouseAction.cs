﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies the mouse action of the chart gestures.
    /// </summary>
    public enum MouseAction
    {
        /// <summary>
        /// Zoom chart by mouse.
        /// </summary>
        Zoom = 0,
        /// <summary>
        /// Pan chart by mouse.
        /// </summary>
        Pan = 1,
    }
}
