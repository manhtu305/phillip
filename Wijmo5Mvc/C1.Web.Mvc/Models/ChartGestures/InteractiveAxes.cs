﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies the interactive axes of the chart gestures.
    /// </summary>
    public enum InteractiveAxes
    {
        /// <summary>
        /// Interactive Axis X.
        /// </summary>
        X = 0,
        /// <summary>
        /// Interactive Axis Y.
        /// </summary>
        Y = 1,
        /// <summary>
        /// Interactive Both Axis X and Axis Y.
        /// </summary>
        XY = 2,
    }
}
