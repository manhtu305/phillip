﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the orientation of the range selector.
    /// </summary>
    public enum Orientation
    {
        /// <summary>
        /// Horizontal, x-data range.
        /// </summary>
        X = 0,
        /// <summary>
        /// Vertical, y-data range.
        /// </summary>
        Y = 1
    }
}
