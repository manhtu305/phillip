﻿namespace C1.Web.Mvc
{
    public partial class RangeSelector<T>
    {
        /// <summary>
        /// Creates one <see cref="RangeSelector{T}"/> instance.
        /// </summary>
        /// <param name="target">A <see cref="FlexChartCore{T}"/> which owns this range selector.</param>
        public RangeSelector(FlexChartCore<T> target)
            : base(target) 
        {
            Initialize();
        }
    }
}
