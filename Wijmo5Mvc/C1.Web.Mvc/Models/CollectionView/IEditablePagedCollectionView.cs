﻿namespace C1.Web.Mvc
{
    internal interface IEditablePagedCollectionView<T> : IEditableCollectionView<T>, IPagedCollectionView<T>
    {
    }
}
