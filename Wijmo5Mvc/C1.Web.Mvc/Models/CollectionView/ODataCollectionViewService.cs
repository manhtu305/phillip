﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    partial class ODataCollectionViewService<T>
    {
#if MODEL
        /// <summary>
        /// Creates one <see cref="ODataCollectionViewService{T}" /> instance.
        /// </summary>
        public ODataCollectionViewService()
#else
        /// <summary>
        /// Creates one <see cref="ODataCollectionViewService{T}" /> instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        internal ODataCollectionViewService(HtmlHelper helper)
            : base(helper)
#endif
        {
            Initialize();
        }
    }
}
