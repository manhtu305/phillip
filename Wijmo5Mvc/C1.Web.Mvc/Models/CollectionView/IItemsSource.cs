﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    /// <summary>
    /// The interface of items source.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public partial interface IItemsSource<T>
    {
        /// <summary>
        /// Gets and sets the url of read action.
        /// </summary>
        string ReadActionUrl { get; set; }

        /// <summary>
        /// Gets and sets the url of create action.
        /// </summary>
        string CreateActionUrl { get; set; }

        /// <summary>
        /// Gets and sets the url of update action.
        /// </summary>
        string UpdateActionUrl { get; set; }

        /// <summary>
        /// Gets and sets the url of delete action.
        /// </summary>
        string DeleteActionUrl { get; set; }

        /// <summary>
        /// Gets and sets the url of batch edit action.
        /// </summary>
        string BatchEditActionUrl { get; set; }

        /// <summary>
        /// Gets or sets the page index.
        /// </summary>
        int PageIndex { get; set; }

        /// <summary>
        /// Gets or sets the page size. When page size is 0, paging is disabled.
        /// </summary>
        int PageSize { get; set; }

        /// <summary>
        /// Gets or sets the source collection.
        /// </summary>
        IEnumerable<T> SourceCollection { get; set; }

        /// <summary>
        /// Gets the sort description collection.
        /// </summary>
        IList<SortDescription> SortDescriptions { get; }

        /// <summary>
        /// Gets the group description collection.
        /// </summary>
        IList<GroupDescription> GroupDescriptions { get; }

        /// <summary>
        /// Gets or sets a method used to determine if an item is suitable for inclusion in the view.
        /// </summary>
        Predicate<T> Filter { get; set; }

        /// <summary>
        /// Gets or sets whether to disable server-side reading.
        /// </summary>
        /// <remarks>When it is set to True, all data will be transfered to the client. 
        /// Otherwise, only part of data will be transfered.</remarks>
        bool DisableServerRead { get; set; }

        /// <summary>
        /// Gets or sets whether modifications will be sent to the server in batches or as individually requests.
        /// </summary>
        bool BatchEdit { get; set; }

        /// <summary>
        /// Gets or sets a number value which decides the count of the initial loaded items. 
        /// </summary>
        /// <remarks>
        /// It only works when the DisableServerRead property is false and no paging mode.
        /// It only accepts a number which equals or greater than 0 or null.
        /// When it is set to null, that means all the items would be loaded in the client.
        /// </remarks>
        int? InitialItemsCount { get; set; }

        /// <summary>
        /// Gets or sets a callback that determines whether a specific property of an item contains validation errors.
        /// </summary>
        string GetError { get; set; }

        /// <summary>
        /// Gets or sets a function that creates new items for the collection.
        /// </summary>
        /// <remarks>
        /// If the creator function is not supplied, try to create an uninitialized item of the appropriate type.
        /// </remarks>
        string NewItemCreator { get; set; }

        /// <summary>
        /// Occurs when the collection changes.
        /// </summary>
        string OnClientCollectionChanged { get; set; }

        /// <summary>
        /// Occurs after the current item changes.
        /// </summary>
        string OnClientCurrentChanged { get; set; }

        /// <summary>
        /// Occurs before the current item changes.
        /// </summary>
        string OnClientCurrentChanging { get; set; }

        /// <summary>
        /// Occurs after the page index changes.
        /// </summary>
        string OnClientPageChanged { get; set; }

        /// <summary>
        /// Occurs before the page index changes.
        /// </summary>
        string OnClientPageChanging { get; set; }

        /// <summary>
        /// Occurs before the sourceCollection property in client changes.
        /// </summary>
        string OnClientSourceCollectionChanging { get; set; }

        /// <summary>
        /// Occurs after the sourceCollection property in client changes.
        /// </summary>
        string OnClientSourceCollectionChanged { get; set; }

        /// <summary>
        /// Gets or sets the name JavaScript function which to collect the ajax query data.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please use OnClientQueryData instead.")]
        string OnClientCollectingQueryData { get; set; }

        /// <summary>
        /// Gets or sets the name JavaScript function which to collect the ajax query data.
        /// </summary>
        string OnClientQueryData { get; set; }

        /// <summary>
        /// Occurs when the query requests complete.
        /// </summary>
        string OnClientQueryComplete { get; set; }

        /// <summary>
        /// Occurs when there are errors from the server side.
        /// </summary>
        string OnClientError { get; set; }
    }
}