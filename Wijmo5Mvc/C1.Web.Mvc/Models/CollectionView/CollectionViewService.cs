﻿#if !MODEL
using C1.Web.Mvc.Localization;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.Mvc;
#endif
#else
using C1.Web.Mvc.Serialization;
#endif
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace C1.Web.Mvc
{
    // CollectionViewService already implements IPagedCollectionView. 
    // But CollectionViewService will be serialized as IEnumberable during serialization.
    // So remove IPagedCollectionView temporarily here.
    [EditorBrowsable(EditorBrowsableState.Never)]
    partial class CollectionViewService<T> : IItemsSource<T>
    {
        #region Fields
        private readonly IEditablePagedCollectionView<T> _collectionView;

        private bool _disableServerRead;
        #endregion Fields

#if !MODEL
        internal CollectionViewService(HtmlHelper helper, IEnumerable<T> sourceCollection = null)
            : base(helper)
        {
            Initialize();
            _collectionView = new CustomEditableCollectionView<T>(sourceCollection);
        }
#else
        public CollectionViewService(IEnumerable<T> sourceCollection = null)
        {
            Initialize();
            _collectionView = new CollectionViewModel<T>(sourceCollection);
        }
#endif

        #region DisableServerRead
        private void _SetDisableServerRead(bool value)
        {
            _disableServerRead = value;
        }
        private bool _GetDisableServerRead()
        {
#if ASPNETCORE
            if (!string.IsNullOrEmpty(ReadActionUrl) || SourceCollection == null)
            {
                return _disableServerRead;
            }

            // When CollectionView binds to a collection(not the action url), DisableServerRead works as true.
            return true;
#else
            return _disableServerRead;
#endif
        }
        #endregion DisableServerRead

        internal ICollectionView<T> CollectionView
        {
            get { return _collectionView; }
        }

        #region PageIndex
        /// <summary>
        /// Gets or sets the page index.
        /// </summary>
        public override int PageIndex
        {
            get
            {
                return _collectionView.PageIndex;
            }
            set
            {
                _collectionView.MoveToPage(value);
            }
        }
        #endregion PageIndex

        #region PageSize
        /// <summary>
        /// Gets or sets the page size. 0 means to disable paging.
        /// </summary>
        public override int PageSize
        {
            get
            {
                return _collectionView.PageSize;
            }
            set
            {
                _collectionView.PageSize = value;
            }
        }
        #endregion PageSize

        #region ReadActionUrl
        private string _readActionUrl;
        private string _GetReadActionUrl()
        {
            return _readActionUrl;
        }
        private void _SetReadActionUrl(string value)
        {
            _readActionUrl = value;
            _collectionView.SourceCollection = null;
        }
        #endregion ReadActionUrl

        #region TotalItemCount
        private int _GetTotalItemCount()
        {
            return _collectionView.TotalItemCount;
        }
        #endregion TotalItemCount

        #region InitialItemsCount
        private int? _initialItemsCount;
        private int? _GetInitialItemsCount()
        {
            return _initialItemsCount;
        }
        private void _SetInitialItemsCount(int? value)
        {
#if !MODEL
            if (value.HasValue && value < 0)
            {
                throw new ArgumentOutOfRangeException("value", Resources.CollectionViewInvalidInitialItemsCount);
            }
#endif
            _initialItemsCount = value;
        }
        #endregion InitialItemsCount

        #region SourceCollection
        private IEnumerable<T> _GetSourceCollection()
        {
            return _collectionView.SourceCollection;
        }
        private void _SetSourceCollection(IEnumerable<T> value)
        {
            _collectionView.SourceCollection = value;
            _readActionUrl = null;
        }

        private bool _ShouldSerializeSourceCollection()
        {
            return DisableServerRead;
        }
        #endregion SourceCollection

        #region SortDescriptions
        /// <summary>
        /// Gets the sort descriptions.
        /// </summary>
        public override IList<SortDescription> SortDescriptions
        {
            get
            {
                return _collectionView.SortDescriptions;
            }
        }
        #endregion SortDescriptions

        #region GroupDescriptions
        /// <summary>
        /// Gets the group descriptions.
        /// </summary>
        public override IList<GroupDescription> GroupDescriptions
        {
            get
            {
                return _collectionView.GroupDescriptions;
            }
        }
        #endregion GroupDescriptions

        #region Items
        private IEnumerable<T> _GetItems()
        {
            if (!DisableServerRead && PageSize == 0)
            {
                _collectionView.Skip = 0;
                _collectionView.Top = InitialItemsCount;
            }
            return _collectionView.ToList();
        }
        private bool _ShouldSerializeItems()
        {
            return !DisableServerRead;
        }
        #endregion Items

        #region Filter
        private Predicate<T> _GetFilter()
        {
            return _collectionView.Filter;
        }
        private void _SetFilter(Predicate<T> value)
        {
            _collectionView.Filter = value;
        }
        #endregion Filter

        #region BatchEditActionUrl
        private string _batchEditActionUrl;
        private string _GetBatchEditActionUrl()
        {
            return _batchEditActionUrl;
        }
        private void _SetBatchEditActionUrl(string value)
        {
            _batchEditActionUrl = value;
            BatchEdit = !string.IsNullOrEmpty(value);
        }
        #endregion BatchEditActionUrl
    }
}