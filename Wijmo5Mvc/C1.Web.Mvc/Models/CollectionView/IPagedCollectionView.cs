﻿namespace C1.Web.Mvc
{
    internal interface IPagedCollectionView<T> : ICollectionView<T>
    {
        int PageIndex { get; }
        int PageSize { get; set; }
        int TotalItemCount { get; }
        void MoveToPage(int pageIndex);
    }
}
