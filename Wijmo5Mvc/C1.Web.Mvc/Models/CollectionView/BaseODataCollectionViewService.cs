﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

using System.Collections.Generic;
using System;

namespace C1.Web.Mvc
{
    partial class BaseODataCollectionViewService<T>
    {
#if MODEL
        /// <summary>
        /// Creates one <see cref="BaseODataCollectionViewService{T}" /> instance.
        /// </summary>
        protected BaseODataCollectionViewService()
#else
        /// <summary>
        /// Creates one <see cref="BaseODataCollectionViewService{T}" /> instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        protected BaseODataCollectionViewService(HtmlHelper helper)
            : base(helper)
#endif
        {
            Initialize();
        }

        #region RequestHeaders
        private IDictionary<string, object> _requestHeaders;
        private IDictionary<string, object> _GetRequestHeaders()
        {
            return _requestHeaders ?? (_requestHeaders = new Dictionary<string, object>());
        }
        #endregion RequestHeaders

        #region DataTypes
        private IDictionary<string, C1.Web.Mvc.Grid.DataType> _dataTypes;
        private IDictionary<string, C1.Web.Mvc.Grid.DataType> _GetDataTypes()
        {
            return _dataTypes ?? (_dataTypes = new Dictionary<string, C1.Web.Mvc.Grid.DataType>());
        }
        #endregion DataTypes

        #region ServiceUrl
        private string _serviceUrl;
        private string _GetServiceUrl()
        {
#if !MODEL
            return Url.GetFullUrl(_serviceUrl, UrlHelper);
#else
            return _serviceUrl;
#endif
        }

        private void _SetServiceUrl(string value)
        {
#if !MODEL
            if (value != null)
            {
                value = Url.GetValidateUrl(value);
            }
#endif
            _serviceUrl = value;
        }
        #endregion ServiceUrl
    }
}
