﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies constants that define how null values are sorted.
    /// </summary>
    public enum CollectionViewSortNulls
    {
        /// <summary>
        /// Null values are sorted in natural order (first in ascending, last in descending order).
        /// </summary>
        Natural,
        /// <summary>
        /// Null values appear first (regardless of sort order).
        /// </summary>
        First,
        /// <summary>
        /// Null values appear last (regardless of sort order).
        /// </summary>
        Last
    }
}
