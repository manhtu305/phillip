﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    internal interface ICollectionView<T> : IEnumerable<T>
    {
        IEnumerable<T> Items { get; }

        IEnumerable<T> SourceCollection { get; set; }

        IList<SortDescription> SortDescriptions { get; }

        IList<GroupDescription> GroupDescriptions { get; }

        Predicate<T> Filter { get; set; }

        void Refresh();

        /// <summary>
        /// Gets or sets the skipped items' count.
        /// </summary>
        int Skip { get; set; }

        /// <summary>
        /// Gets or sets the items' count required.
        /// </summary>
        /// <remarks>
        /// Normally it is always a number equals or greater than 0. 
        /// When it is set to null, it means all the items are required.
        /// </remarks>
        int? Top { get; set; }
    }
}
