﻿namespace C1.Web.Mvc.CollectionView
{
    /// <summary>
    /// The command type of CollectionView's request.
    /// </summary>
    public enum CommandType
    {
        /// <summary>
        /// The type of reading data.
        /// </summary>
        Read,

        /// <summary>
        /// The type of creating data.
        /// </summary>
        Create,

        /// <summary>
        /// The type of updating data.
        /// </summary>
        Update,

        /// <summary>
        /// The type of deleting data.
        /// </summary>
        Delete,

        /// <summary>
        /// The type of batch modifying data.
        /// </summary>
        BatchEdit
    }
}
