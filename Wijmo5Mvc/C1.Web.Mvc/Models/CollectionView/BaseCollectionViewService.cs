﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    partial class BaseCollectionViewService<T> : IItemsSource<T>
    {
#if MODEL
        /// <summary>
        /// Creates one <see cref="BaseCollectionViewService{T}" /> instance.
        /// </summary>
        protected BaseCollectionViewService()
#else
        /// <summary>
        /// Creates one <see cref="BaseCollectionViewService{T}" /> instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        protected BaseCollectionViewService(HtmlHelper helper)
            : base(helper)
#endif
        {
            Initialize();
        }

        #region SortDescriptions
        private IList<SortDescription> _sortDescriptions;
        private IList<SortDescription> _GetSortDescriptions()
        {
            return _sortDescriptions ?? (_sortDescriptions = new List<SortDescription>());
        }
        #endregion SortDescriptions

        #region GroupDescriptions
        private IList<GroupDescription> _groupDescriptions;
        private IList<GroupDescription> _GetGroupDescriptions()
        {
            return _groupDescriptions ?? (_groupDescriptions = new List<GroupDescription>());
        }
        #endregion GroupDescriptions

        #region Obsoleted: OnClientCollectingQueryData
        private string _GetOnClientCollectingQueryData()
        {
            return OnClientQueryData;
        }
        private void _SetOnClientCollectingQueryData(string value)
        {
            OnClientQueryData = value;
        }
        #endregion Obsoleted: OnClientCollectingQueryData

    }
}
