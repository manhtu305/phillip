﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    partial class ODataVirtualCollectionViewService<T>
    {
#if MODEL
        /// <summary>
        /// Creates one <see cref="ODataVirtualCollectionViewService{T}" /> instance.
        /// </summary>
        public ODataVirtualCollectionViewService()
#else
        /// <summary>
        /// Creates one <see cref="ODataVirtualCollectionViewService{T}" /> instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        internal ODataVirtualCollectionViewService(HtmlHelper helper)
            : base(helper)
#endif
        {
            Initialize();
        }
    }
}
