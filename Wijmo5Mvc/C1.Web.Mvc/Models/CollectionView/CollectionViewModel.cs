﻿#if MODEL
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc
{
    internal class CollectionViewModel<T> : IEditablePagedCollectionView<T>
    {
        private int _pageIndex;

        public CollectionViewModel(IEnumerable<T> sourceCollection = null)
        {
            SourceCollection = sourceCollection;
            SortDescriptions = new List<SortDescription>();
            GroupDescriptions = new List<GroupDescription>();
        }

        public Func<BatchOperatingData<T>, CollectionViewResponse<T>> CustomBatchEdit { get; set; }

        public Func<T, CollectionViewItemResult<T>> CustomCreate { get; set; }

        public Func<T, CollectionViewItemResult<T>> CustomDelete { get; set; }

        public Func<T, CollectionViewItemResult<T>> CustomUpdate { get; set; }

        public Predicate<T> Filter { get; set; }

        public IList<GroupDescription> GroupDescriptions { get; private set; }

        public IEnumerable<T> Items
        {
            get
            {
                return Enumerable.Empty<T>();
            }
        }

        public int PageIndex
        {
            get
            {
                return _pageIndex;
            }
        }

        public int PageSize { get; set; }

        public int Skip { get; set; }

        public IList<SortDescription> SortDescriptions { get; private set; }

        public IEnumerable<T> SourceCollection { get; set; }

        public int? Top { get; set; }

        public int TotalItemCount
        {
            get
            {
                return 0;
            }
        }

        public CollectionViewResponse<T> BatchEdit(BatchOperatingData<T> batchData)
        {
            throw new NotImplementedException();
        }

        public CollectionViewItemResult<T> Create(T item)
        {
            throw new NotImplementedException();
        }

        public CollectionViewItemResult<T> Delete(T item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void MoveToPage(int pageIndex)
        {
            _pageIndex = pageIndex;
        }

        public void Refresh()
        {
            throw new NotImplementedException();
        }

        public CollectionViewItemResult<T> Update(T item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
#endif