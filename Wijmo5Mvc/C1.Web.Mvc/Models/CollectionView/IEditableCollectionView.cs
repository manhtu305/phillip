﻿using System;

namespace C1.Web.Mvc
{
    interface IEditableCollectionView<T> : ICollectionView<T>
    {
        Func<T, CollectionViewItemResult<T>> CustomCreate { get; set; }
        Func<T, CollectionViewItemResult<T>> CustomUpdate { get; set; }
        Func<T, CollectionViewItemResult<T>> CustomDelete { get; set; }
        Func<BatchOperatingData<T>, CollectionViewResponse<T>> CustomBatchEdit { get; set; }
        CollectionViewItemResult<T> Create(T item);
        CollectionViewItemResult<T> Update(T item);
        CollectionViewItemResult<T> Delete(T item);
        CollectionViewResponse<T> BatchEdit(BatchOperatingData<T> batchData);
    }
}
