﻿namespace C1.Web.Mvc
{
    abstract partial class ItemsBoundControl<T>
    {
        #region ItemsSource
        private IItemsSource<T> _itemsSource;
        private IItemsSource<T> _GetItemsSource()
        {
#if !MODEL
            return _itemsSource ?? (_itemsSource = new CollectionViewService<T>(Helper));
#else
            return _itemsSource ?? (_itemsSource = new CollectionViewService<T>());
#endif
        }
        private void _SetItemsSource(IItemsSource<T> value)
        {
            _itemsSource = value;
        }
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual bool _ShouldSerializeItemsSource() //Support inheriting.
        {
#if !MODEL
            return string.IsNullOrEmpty(ItemsSourceId);
#else
            return ItemsSourceBinding.IsItemsSourceDirty(DbContextType, ModelType);
#endif
        }
        #endregion ItemsSource

        internal void DisableServerRead()
        {
            var collectionViewService = ItemsSource as CollectionViewService<T>;
            if(collectionViewService != null)
            {
                collectionViewService.DisableServerRead = true;
            }
        }
    }
}
