﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the template bindings.
    /// </summary>
    /// <remarks>
    /// When creating an instance of this type, the control should be passed. The control owns an instance of <see cref="C1.Web.Mvc.TemplateBindings"/>.
    /// And it should NOT be null.Otherwise, an exception of <see cref="System.ArgumentNullException"/> would be thrown.
    /// </remarks>
    internal sealed class TemplateBindings : Dictionary<string, string>
    {
        #region Fields
        private readonly object _control;
        #endregion Fields

        #region Ctors
        public TemplateBindings(object control)
            : base()
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }
            _control = control;
        }
        #endregion Ctors

        #region Properties
        /// <summary>
        /// Gets the control which owns an instance of <see cref="C1.Web.Mvc.TemplateBindings"/>
        /// </summary>
        internal object Control
        {
            get
            {
                return _control;
            }
        }
        #endregion Properties
    }
}
