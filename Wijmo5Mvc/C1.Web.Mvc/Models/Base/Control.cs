﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    public abstract partial class Control : Component
    {
        #region CssClass
        private string _GetCssClass()
        {
            return HtmlAttributes["class"];
        }
        private void _SetCssClass(string value)
        {
            HtmlAttributes["class"] = value;
        }
        #endregion CssClass

        #region Width
        private string _GetWidth()
        {
            return CssStyles["width"];
        }
        private void _SetWidth(string value)
        {
            CssStyles["width"] = value;
        }
        #endregion Width

        #region Height
        private string _GetHeight()
        {
            return CssStyles["height"];
        }
        private void _SetHeight(string value)
        {
            CssStyles["height"] = value;
        }
        #endregion Height

        #region HtmlAttributes
        private IDictionary<string, string> _htmlAttributes;
        private IDictionary<string, string> _GetHtmlAttributes()
        {
            return _htmlAttributes ?? (_htmlAttributes = new AttrDictionary<string>());
        }
        #endregion HtmlAttributes

        #region CssStyles
        private IDictionary<string, string> _cssStyles;
        private IDictionary<string, string> _GetCssStyles()
        {
            return _cssStyles ?? (_cssStyles = new AttrDictionary<string>());
        }
        #endregion CssStyles

        #region TemplateBindings
        private IDictionary<string, string> _templateBindings;
        private IDictionary<string, string> _GetTemplateBindings()
        {
            return _templateBindings ?? (_templateBindings = new TemplateBindings(this));
        }
        #endregion TemplateBindings

        #region Obsolete
        #region Disabled
        private bool _GetDisabled()
        {
            return IsDisabled;
        }
        private void _SetDisabled(bool value)
        {
            IsDisabled = value;
        }
        #endregion Disabled
        #endregion Obsolete
    }
}
