﻿using System;

namespace C1.Web.Mvc
{
    abstract partial class Extender
    {
        #region Fields
        private readonly Component _target;
        #endregion Fields

        #region Ctors
        /// <summary>
        /// Creates one <see cref="Extender"/> instance.
        /// </summary>
        /// <param name="target">The specified component which the extender is applied on.</param>
        protected Extender(Component target)
#if !MODEL
            : base(target != null ? target.Helper : null)
#endif
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }
            _target = target;
            Initialize();
        }
        #endregion Ctors
    }
}
