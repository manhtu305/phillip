﻿namespace C1.Web.Mvc
{
    public abstract partial class Component
    {
        #region Id
#if MODEL
        private string _id;
#endif
        private string _GetId()
        {
#if !MODEL
            EnsureId();
            return InnerId;
#else
            return _id;
#endif
        }
        private void _SetId(string value)
        {
#if !MODEL
            InnerId = value;
#else
            _id = value;
#endif
        }
        #endregion Id
    }
}
