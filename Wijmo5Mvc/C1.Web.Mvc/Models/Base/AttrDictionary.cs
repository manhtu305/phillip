﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    /// <summary>
    /// This dictionary is case-insensitive. 
    /// And when invoke Items[string key] and the key doesn't exist, it will return default(TValue).
    /// </summary>
    internal class AttrDictionary<TValue> : IDictionary<string, TValue>
    {
        private readonly IDictionary<string, TValue> _innerDic = new Dictionary<string, TValue>(StringComparer.OrdinalIgnoreCase);

        public void Add(string key, TValue value)
        {
            _innerDic.Add(key, value);
        }

        public bool ContainsKey(string key)
        {
            return _innerDic.ContainsKey(key);
        }

        public ICollection<string> Keys
        {
            get
            {
                return _innerDic.Keys;
            }
        }

        public bool Remove(string key)
        {
            return _innerDic.Remove(key);
        }

        public bool TryGetValue(string key, out TValue value)
        {
            return _innerDic.TryGetValue(key, out value);
        }

        public ICollection<TValue> Values
        {
            get { return _innerDic.Values; }
        }

        public TValue this[string key]
        {
            get { return _innerDic.ContainsKey(key) ? _innerDic[key] : default(TValue); }
            set { _innerDic[key] = value; }
        }

        public void Add(KeyValuePair<string, TValue> item)
        {
            _innerDic.Add(item);
        }

        public void Clear()
        {
            _innerDic.Clear();
        }

        public bool Contains(KeyValuePair<string, TValue> item)
        {
            return _innerDic.Contains(item);
        }

        public void CopyTo(KeyValuePair<string, TValue>[] array, int arrayIndex)
        {
            _innerDic.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _innerDic.Count; }
        }

        public bool IsReadOnly
        {
            get { return _innerDic.IsReadOnly; }
        }

        public bool Remove(KeyValuePair<string, TValue> item)
        {
            return _innerDic.Remove(item);
        }

        public IEnumerator<KeyValuePair<string, TValue>> GetEnumerator()
        {
            return _innerDic.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _innerDic.GetEnumerator();
        }
    }
}
