﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    #region IExtendable
    /// <summary>
    /// Defines an interface to make a component extendable.
    /// </summary>
    internal interface IExtendable
    {
        IList<Extender> Extenders { get; }
    }
    #endregion IExtendable

    #region IExtender
    /// <summary>
    /// Defines the interface of extender.
    /// </summary>
    internal interface IExtender
    {

    }
    #endregion IExtender


    /// <summary>
    /// Defines the interface of template.
    /// </summary>
    public interface ITemplate
    {
        /// <summary>
        /// Gets the collection of the template bindings.
        /// </summary>
        IDictionary<string, string> TemplateBindings { get; }

        /// <summary>
        /// Gets or sets a boolean value which indicates whether transfer this control to template mode.
        /// </summary>
        bool IsTemplate { get; set; }
    }
}
