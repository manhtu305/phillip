﻿using System.Collections.Generic;
using C1.Web.Mvc.Chart;
using C1.Web.Mvc.Serialization;
using System.ComponentModel;
#if ASPNETCORE
using Color = System.String;
#else
using System.Drawing;
#endif

namespace C1.Web.Mvc
{
    public partial class TreeMap<T>
    {
        #region DataLabel
        private DataLabel _dataLabel;
        private DataLabel _GetDataLabel()
        {
            return _dataLabel ?? (_dataLabel = new DataLabel());
        }
        #endregion DataLabel

        #region Legend
        private ChartLegend _legend;
        private ChartLegend _GetLegend()
        {
            return _legend ?? (_legend = new TreeMapLegend());
        }
        #endregion Legend

        #region Palette
        private List<object> _palette;
        /// <summary>
        /// Gets or sets an array of default colors to be used in a tree map.
        /// </summary>
        /// <remarks>
        /// The array contains strings that represent CSS colors.
        /// The array item type could be <see cref="Color"/> or <see cref="TreeMapItemStyle"/>.
        /// </remarks>
        public new List<object> Palette
        {
            get { return _palette ?? (_palette = new List<object>()); }
        }
        #endregion Palette

        #region Removed

        /// <summary>
        /// Overrides to remove this property.
        /// </summary>
        [C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientSelectionChanged { get; set; }

        /// <summary>
        /// Overrides to remove this property.
        /// </summary>
        [C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ItemFormatter { get; set; }
        #endregion Removed
    }

    /// <summary>
    /// Defines a lengend class for TreeMap as it has different default value.
    /// </summary>
    internal class TreeMapLegend : ChartLegend
    {
        public TreeMapLegend()
        {
            Position = Position.None;
        }

        /// <summary>
        /// Gets or sets the enumerated value that determines whether and where the legend appears in relation to the chart.
        /// </summary>
        [DefaultValue(Chart.Position.None)]
        public override Position Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                base.Position = value;
            }
        }

        internal override bool IsDefault
        {
            get
            {
                return Position == Position.None && Title == DEFAULT_TITLE && TitleAlign == DEFAULT_TITLE_ALIGN && Orientation == LegendOrientation.Auto;
            }
        }
    }
}
