﻿#if ASPNETCORE
using Color = System.String;
#else
using System.Drawing;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines the colors used for an item in <see cref="TreeMap{T}"/>.
    /// </summary>
    public class TreeMapItemStyle
    {
        /// <summary>
        /// Gets or sets the title color.
        /// </summary>
        public Color TitleColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the max value.
        /// </summary>
        public Color MaxColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the min value.
        /// </summary>
        public Color MinColor { get; set; }
    }
}
