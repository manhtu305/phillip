﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies the tree map type.
    /// </summary>
    public enum TreeMapType
    {
        /// <summary>
        /// Shows squarified tree map.
        /// </summary>
        Squarified,

        /// <summary>
        /// Shows horizontal squarified tree map.
        /// </summary>
        Horizontal,

        /// <summary>
        /// Shows vertical squarified tree map.
        /// </summary>
        Vertical
    }
}
