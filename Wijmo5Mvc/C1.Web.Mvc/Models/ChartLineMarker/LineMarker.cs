﻿namespace C1.Web.Mvc
{
    public partial class LineMarker<T>
    {
        /// <summary>
        /// Creates one <see cref="LineMarker{T}"/> instance.
        /// </summary>
        /// <param name="target">A <see cref="FlexChartCore{T}"/> which owns this line marker.</param>
        public LineMarker(FlexChartCore<T> target)
            : base(target)
        {
            Initialize();
        }
    }
}
