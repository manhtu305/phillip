﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the line type for the LineMarker.
    /// </summary>
    public enum LineMarkerLines
    {
        /// <summary>
        /// Show no lines.
        /// </summary>
        None,
        /// <summary>
        /// Show a vertical line.
        /// </summary>
        Vertical,
        /// <summary>
        /// Show a horizontal line.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Show both vertical and horizontal lines.
        /// </summary>
        Both
    }
}
