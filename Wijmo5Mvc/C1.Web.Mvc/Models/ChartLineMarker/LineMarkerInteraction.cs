﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies how the LineMarker interacts with the user.
    /// </summary>
    public enum LineMarkerInteraction
    {
        /// <summary>
        /// No interaction, the user specifies the position manually.
        /// </summary>
        None,
        /// <summary>
        /// The LineMarker moves with the pointer.
        /// </summary>
        Move,
        /// <summary>
        /// The LineMarker moves when the user drags the line.
        /// </summary>
        Drag
    }
}
