﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// Specifies the alignment of the LineMarker.
    /// </summary>
    public enum LineMarkerAlignment
    {
        /// <summary>
        /// The LineMarker alignment adjusts automatically so that it stays inside the 
        /// boundaries of the plot area.
        /// </summary>
        Auto = 2,
        /// <summary>
        /// The LineMarker aligns to the right of the pointer.
        /// </summary>
        Right = 0,
        /// <summary>
        /// The LineMarker aligns to the left of the pointer.
        /// </summary>
        Left = 1,
        /// <summary>
        /// The LineMarker aligns to the bottom of the pointer.
        /// </summary>
        Bottom = 4,
        /// <summary>
        /// The LineMarker aligns to the top of the pointer.
        /// </summary>
        Top = 6
    }
}
