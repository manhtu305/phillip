﻿using C1.JsonNet;
using C1.Web.Mvc.Grid;
using System.Collections.Generic;
#if MODEL
using C1.Web.Mvc.Serialization;
#endif

namespace C1.Web.Mvc
{
    public partial class FlexGridBase<T> : IExtendable
    {
        #region virtualThreshold
        private int[] _virtualThresholds;
        private int[] _GetVirtualizationThresholds()
        {
            return _virtualThresholds ?? (_virtualThresholds = new int[] { 0, 0 });
        }
        private void _SetVirtualizationThreshold(int value)
        {
            VirtualizationThresholds[0] = value;
        }

        private int _GetVirtualizationThreshold()
        {
            return VirtualizationThresholds[0];
        }

        private bool _ShouldSerializeVirtualizationThresholds()
        {
            if (_virtualThresholds == null || _virtualThresholds.Length != 2) return false;
            return _virtualThresholds[0] != 0 || _virtualThresholds[1] != 0;
        }
        #endregion

        #region Header Template
        private C1.Web.Mvc.HeaderTemplate _headerTemplate;

        private C1.Web.Mvc.HeaderTemplate _GetHeaderTemplate()
        {
            return this._headerTemplate;
        }
        
        private void _SetHeaderTemplate(C1.Web.Mvc.HeaderTemplate value)
        {
            _headerTemplate = value;
        }

        private bool _ShouldSerializeHeaderTemplate()
        {
            return _headerTemplate != null;
        }
        #endregion

        #region Selection
        private CellRange _selection = _defaultSelection.Clone();
        private static readonly CellRange _defaultSelection = new CellRange(0, 0);
        private CellRange _GetSelection()
        {
            return _selection;
        }
        private void _SetSelection(CellRange value)
        {
            _selection = value;
        }
        private bool _ShouldSerializeSelection()
        {
            return !_defaultSelection.Equals(_selection);
        }
        #endregion Selection

        private bool _ShouldSerializeColumnLayout()
        {
            return !string.IsNullOrEmpty(ColumnLayout);
        }

        #region Columns

        private IList<Column> _columns;
        private IList<Column> _GetColumns()
        {
            if (_columns == null)
            {
                _columns = new List<Column>();
            }
            return _columns;
        }

        #endregion Columns

        #region Templates

        private CellTemplate _rowHeadersTemplate;
        private CellTemplate _GetRowHeadersTemplate()
        {
            return _rowHeadersTemplate ?? (_rowHeadersTemplate = new CellTemplate());
        }

        private CellTemplate _columnHeadersTemplate;
        private CellTemplate _GetColumnHeadersTemplate()
        {
            return _columnHeadersTemplate ?? (_columnHeadersTemplate = new CellTemplate());
        }

        private CellTemplate _cellsTemplate;
        private CellTemplate _GetCellsTemplate()
        {
            return _cellsTemplate ?? (_cellsTemplate = new CellTemplate());
        }

        private CellTemplate _topLeftCellsTemplate;
        private CellTemplate _GetTopLeftCellsTemplate()
        {
            return _topLeftCellsTemplate ?? (_topLeftCellsTemplate = new CellTemplate());
        }

        private CellTemplate _columnFootersTemplate;
        private CellTemplate _GetColumnFootersTemplate()
        {
            return _columnFootersTemplate ?? (_columnFootersTemplate = new CellTemplate());
        }

        private CellTemplate _bottomLeftCellsTemplate;
        private CellTemplate _GetBottomLeftCellsTemplate()
        {
            return _bottomLeftCellsTemplate ?? (_bottomLeftCellsTemplate = new CellTemplate());
        }
        #endregion Templates

        #region IExtendable
        private IList<Extender> _extenders;
        /// <summary>
        /// Gets the extender collection.
        /// </summary>
        [Ignore()]
        public IList<Extender> Extenders
        {
            get { return _extenders ?? (_extenders = new List<Extender>()); }
        }
        #endregion IExtendable

        #region alternatingRow
        private void _SetShowAlternatingRows(bool value)
        {
            AlternatingRowStep = value ? 1 : 0;
        }

        private bool _GetShowAlternatingRows()
        {
            return AlternatingRowStep > 0;
        }
        #endregion

        #region ErrorTip
        private ErrorTooltip _errorTip;
        private ErrorTooltip _GetErrorTip()
        {
            return _errorTip ?? (_errorTip = new ErrorTooltip());
        }
        #endregion ErrorTip

        #region DefaultTypeWidth
        private static readonly Dictionary<string, int> _defaultTypeWidth = new Dictionary<string, int>();
        private Dictionary<string, int> _GetDefaultTypeWidth()
        {
            return _defaultTypeWidth;
        }
        #endregion DefaultTypeWidth
    }
}
