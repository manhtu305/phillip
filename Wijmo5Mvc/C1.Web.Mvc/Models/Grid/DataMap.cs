﻿using C1.JsonNet;
using System.ComponentModel;
using C1.Web.Mvc.Serialization;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    public partial class DataMap
    {
        private IItemsSource<object> _itemsSource;

#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Creates one <see cref="DataMap"/> instance.
        /// </summary>
        public DataMap() : this(null) { }

        /// <summary>
        /// Creates one <see cref="DataMap"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        public DataMap(HtmlHelper helper)
        {
            Initialize();
            _helper = helper;
        }
#else
        public DataMap()
        {
            Initialize();
        }
#endif

        /// <summary>
        /// Gets the ItemsSource settings.
        /// </summary>
#if !MODEL
        [JsonConverter(typeof(ServiceConverter))]
#else
        [Browsable(false)]
        [C1HtmlHelperBuilderName("Bind")]
        [C1TagHelperIsAttribute(false)]
#endif
        public IItemsSource<object> ItemsSource
        {
            get
            {
#if !MODEL
                if (_itemsSource == null && _helper != null)
                {
                    _itemsSource = new CollectionViewService<object>(_helper);
                }
#else
                if (_itemsSource == null)
                {
                    _itemsSource = new CollectionViewService<object>();
                }
#endif
                return _itemsSource;
            }
            set
            {
                _itemsSource = value;
            }
        }

        /// <summary>
        /// Specifies whether the ItemsSource property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeItemsSource()
        {
#if !MODEL
            return ItemsSource != null && !(string.IsNullOrEmpty(ItemsSource.ReadActionUrl) &&
                ItemsSource.SourceCollection == null);
#else
            return ShouldSerializeItemsSource_Scaffolder();
#endif
        }
    }
}
