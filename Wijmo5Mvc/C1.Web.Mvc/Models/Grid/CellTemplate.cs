﻿using C1.JsonNet;
using System;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    public partial class CellTemplate
    {
        #region Removed Properties
        /// <summary>
        /// Overrides to remove this property.
        /// </summary>
        [Ignore()]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use TemplateId instead.")]
        public string Template
        {
            get { return TemplateId; }
            set { TemplateId = value; }
        }

        /// <summary>
        /// Remove this property as it is obsoleted.
        /// </summary>
        [Ignore()]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use EditTemplateId instead.")]
        public string EditTemplate
        {
            get { return EditTemplateId; }
            set { EditTemplateId = value; }
        }
        #endregion Removed Properties

        #region TemplateId
        private string _templateId;
        private void _SetTemplateId(string id)
        {
            _templateContent = null;
            _templateId = id;
        }
        private string _GetTemplateId()
        {
            return _templateId;
        }
        #endregion TemplateId

        #region TemplateContent
        private string _templateContent;
        private string _GetTemplateContent()
        {
            return _templateContent;
        }
        private void _SetTemplateContent(string content)
        {
            _templateId = null;
            _templateContent = content;
        }
        #endregion TemplateContent

        #region EditTemplateId
        private string _editTemplateId;
        private string _GetEditTemplateId()
        {
            return _editTemplateId;
        }
        private void _SetEditTemplateId(string id)
        {
            _editTemplateContent = null;
            _editTemplateId = id;
        }
        #endregion EditTemplateId

        #region EditTemplateContent
        private string _editTemplateContent;
        private string _GetEditTemplateContent()
        {
            return _editTemplateContent;
        }
        private void _SetEditTemplateContent(string content)
        {
            _editTemplateId = null;
            _editTemplateContent = content;
        }
        #endregion EditTemplateContent
    }
}
