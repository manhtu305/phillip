﻿using System.Collections.Generic;
#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc
{
    public partial class ColumnBase
    {
        #region Width
        private string _width;
        private void _SetWidth(string value)
        {
            // value can be null or empty.
            if (string.IsNullOrEmpty(value))
            {
                _width = null;
                return;
            }

#if !MODEL
            string starWidth;
            int width;
            ColumnWidthConverter.GetRealWidth(value, out starWidth, out width);
#endif
            _width = value;
        }
        private string _GetWidth()
        {
            return _width;
        }
        #endregion Width

        #region CellTemplate
        private CellTemplate _cellTemplate;
        private CellTemplate _GetCellTemplate()
        {
            return _cellTemplate ?? (_cellTemplate = new CellTemplate());
        }
        #endregion CellTemplate

        #region DataMap
        private DataMap _dataMap;
        private DataMap _GetDataMap()
        {
            if (_dataMap == null)
            {
#if !MODEL
                _dataMap = new DataMap(_helper);
#else
                _dataMap = new DataMap();
#endif
            }
            return _dataMap;
        }

        private bool _ShouldSerializeDataMap()
        {
            return (DataMap.ShouldSerializeItemsSource() && !string.IsNullOrEmpty(DataMap.DisplayMemberPath)
                && !string.IsNullOrEmpty(DataMap.SelectedValuePath));
        }
        #endregion DataMap

        #region Required
        private bool? _GetRequired()
        {
            return IsRequired;
        }

        private void _SetRequired(bool? value)
        {
            IsRequired = value;
        }
        #endregion

        #region Ctor
#if !MODEL
        private readonly HtmlHelper _helper;

        /// <summary>
        /// Gets or sets the htmlhelper object.
        /// </summary>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal HtmlHelper Helper
        {
            [SmartAssembly.Attributes.DoNotObfuscate]
            get { return _helper; }
        }

        /// <summary>
        /// Creates one <see cref="ColumnBase"/> instance.
        /// </summary>
        public ColumnBase()
        {
            Initialize();
        }

        /// <summary>
        /// Creates one <see cref="ColumnBase"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        internal ColumnBase(HtmlHelper helper)
        {
            _helper = helper;
            Initialize();
        }
#else
        internal ColumnBase()
        {
            Initialize();
        }
#endif
        #endregion Ctor

        #region Columns

        private IList<Column> _columns;
        private IList<Column> _GetColumns()
        {
            if (_columns == null)
            {
                _columns = new List<Column>();
            }
            return _columns;
        }

        #endregion Columns
    }

    public partial class Column
    {
        #region Ctor
#if !MODEL
        /// <summary>
        /// Creates one <see cref="Column"/> instance.
        /// </summary>
        public Column()
            :this(null)
        {
        }

        /// <summary>
        /// Creates one <see cref="Column"/> instance.
        /// </summary>
        /// <param name="helper">The HtmlHelper instance.</param>
        internal Column(HtmlHelper helper)
            :base(helper)
        {
            Initialize();
        }
#else
        public Column()
        {
            Initialize();
        }
#endif
        #endregion Ctor
    }    
}
