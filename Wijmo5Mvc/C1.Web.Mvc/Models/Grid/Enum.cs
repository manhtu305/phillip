﻿namespace C1.Web.Mvc.Grid
{
    /// <summary>
    /// Specifies the type of aggregate to calculate over a group of values.
    /// </summary>
    public enum Aggregate
    {
        /// <summary>
        /// No aggregate.
        /// </summary>
        None = 0,

        /// <summary>
        /// Returns the sum of the numeric values in the group.
        /// </summary>
        Sum = 1,

        /// <summary>
        /// Returns the count of non-null values in the group.
        /// </summary>
        Cnt = 2,

        /// <summary>
        /// Returns the average value of the numeric values in the group.
        /// </summary>
        Avg = 3,

        /// <summary>
        /// Returns the maximum value in the group.
        /// </summary>
        Max = 4,

        /// <summary>
        /// Returns the minimum value in the group.
        /// </summary>
        Min = 5,

        /// <summary>
        /// Returns the difference between the maximum and minimum numeric values in the group.
        /// </summary>
        Rng = 6,

        /// <summary>
        /// Returns the sample standard deviation of the numeric values in the group (uses the formula based on n-1).
        /// </summary>
        Std = 7,

        /// <summary>
        /// Returns the sample variance of the numeric values in the group (uses the formula based on n-1).
        /// </summary>
        Var = 8,

        /// <summary>
        /// Returns the population standard deviation of the values in the group (uses the formula based on n).
        /// </summary>
        StdPop = 9,

        /// <summary>
        /// Returns the population variance of the values in the group (uses the formula based on n).
        /// </summary>
        VarPop = 10,

        /// <summary>
        /// Returns the count of all values in the group (including nulls).
        /// </summary>
        CntAll = 11,

        /// <summary>
        /// Returns the first non-null value in the group.
        /// </summary>
        First = 12,

        /// <summary>
        /// Returns the last non-null value in the group.
        /// </summary>
        Last = 13
    }

    /// <summary>
    /// Specifies constants that define the row/column dragging behavior.
    /// </summary>
    public enum AllowDragging
    {
        /// <summary>
        /// The user may not drag rows or columns.
        /// </summary>
        None = 0,

        /// <summary>
        /// The user may drag columns.
        /// </summary>
        Columns = 1,

        /// <summary>
        /// The user may drag rows.
        /// </summary>
        Rows = 2,

        /// <summary>
        /// The user may drag rows and columns.
        /// </summary>
        Both = Rows | Columns

    }

    /// <summary>
    /// Specifies constants that define which areas of the grid support cell merging.
    /// </summary>
    public enum AllowMerging
    {
        /// <summary>
        /// No merging.
        /// </summary>
        None = 0,

        /// <summary>
        /// Merge scrollable cells.
        /// </summary>
        Cells = 1,

        /// <summary>
        /// Merge column headers.
        /// </summary>
        ColumnHeaders = 2,

        /// <summary>
        /// Merge row headers.
        /// </summary>
        RowHeaders = 4,

        /// <summary>
        /// Merge column and row headers.
        /// </summary>
        AllHeaders = ColumnHeaders | RowHeaders,

        /// <summary>
        /// Merge all areas.
        /// </summary>
        All = Cells | AllHeaders
    }

    /// <summary>
    /// Specifies constants that define the row/column sizing behavior.
    /// </summary>
    public enum AllowResizing
    {
        /// <summary>
        /// The user may not resize rows or columns.
        /// </summary>
        None = 0,

        /// <summary>
        /// The user may resize columns.
        /// </summary>
        Columns = 1,

        /// <summary>
        /// The user may resize rows.
        /// </summary>
        Rows = 2,

        /// <summary>
        /// The user may resize rows and columns.
        /// </summary>
        Both = Rows | Columns, //3
 
        /// <summary>
        /// The user may resize columns by dragging the edge of any cell.
        /// </summary>
        ColumnsAllCells = Columns | 4, // 5

        /// <summary>
        /// The user may resize rows by dragging the edge of any cell.
        /// </summary>
        RowsAllCells = Rows | 4, // 6

        /// <summary>
        /// The user may resize rows and columns by dragging the edge of any cell.
        /// </summary>
        BothAllCells = Both | 4 // 7
    }

    /// <summary>
    /// Specifies constants that define the row/column auto-sizing behavior.
    /// </summary>
    public enum AutoSizeMode
    {
        /// <summary>
        /// Autosizing is disabled.
        /// </summary>
        None = 0,

        /// <summary>
        /// Autosizing accounts for header cells.
        /// </summary>
        Headers = 1,

        /// <summary>
        /// Autosizing accounts for data cells.
        /// </summary>
        Cells = 2,

        /// <summary>
        /// Autosizing accounts for header and data cells.
        /// </summary>
        Both = Cells | Headers
    }

    /// <summary>
    /// Enumeration with value types.
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// Object (anything).
        /// </summary>
        Object = 0,

        /// <summary>
        /// String.
        /// </summary>
        String = 1,

        /// <summary>
        /// Number.
        /// </summary>
        Number = 2,

        /// <summary>
        /// Boolean.
        /// </summary>
        Boolean = 3,

        /// <summary>
        /// Date (date and time).
        /// </summary>
        Date = 4,

        /// <summary>
        /// Array.
        /// </summary>
        Array = 5
    }

    /// <summary>
    /// Specifies constants that specify the visibility of row and column headers.
    /// </summary>
    public enum HeadersVisibility
    {
        /// <summary>
        /// No header cells are displayed.
        /// </summary>
        None = 0,

        /// <summary>
        /// Only column header cells are displayed.
        /// </summary>
        Column = 1,

        /// <summary>
        /// Only row header cells are displayed.
        /// </summary>
        Row = 2,

        /// <summary>
        /// Both column and row header cells are displayed.
        /// </summary>
        All = Row | Column
    }

    /// <summary>
    /// Gets or sets the current selection mode.
    /// </summary>
    public enum SelectionMode
    {
        /// <summary>
        /// The user cannot select cells with the mouse or keyboard.
        /// </summary>
        None = 0,

        /// <summary>
        /// The user can select only a single cell at a time.
        /// </summary>
        Cell = 1,

        /// <summary>
        /// The user can select contiguous blocks of cells.
        /// </summary>
        CellRange = 2,

        /// <summary>
        /// The user can select a single row at a time.
        /// </summary>
        Row = 3,

        /// <summary>
        /// The user can select contiguous rows.
        /// </summary>
        RowRange = 4,

        /// <summary>
        /// The user can select non-contiguous rows by ctrl+clicking.
        /// </summary>
        ListBox = 5,

        /// <summary>
        /// The user can select multiple ranges by ctrl+clicking and dragging the mouse.
        /// </summary>
        MultiRange = 6
    }

    /// <summary>
    /// Specifies when and how the row details are displayed.
    /// </summary>
    public enum DetailVisibilityMode
    {
        /// <summary>
        /// Details are shown or hidden in code, using the showDetail and hideDetail methods.
        /// </summary>
        Code = 0,

        /// <summary>
        /// Details are shown for the row that is currently selected.
        /// </summary>
        Selection = 1,

        /// <summary>
        /// Details are shown or hidden using buttons added to the row headers. Only one row may be expanded at a time.
        /// </summary>
        ExpandSingle = 2,

        /// <summary>
        /// Details are shown or hidden using buttons added to the row headers. Multiple rows may be expanded at a time.
        /// </summary>
        ExpandMulti = 3

    }

    /// <summary>
    /// Specifies constants that define the action to perform when special keys such as ENTER and TAB are pressed.
    /// </summary>
    public enum KeyAction
    {
        /// <summary>
        /// No special action (let the browser handle the key).
        /// </summary>
        None,
        /// <summary>
        /// Move the selection to the next row.
        /// </summary>
        MoveDown,
        /// <summary>
        /// Move the selection to the next column.
        /// </summary>
        MoveAcross,
        /// <summary>
        /// Move the selection to the next column, then wrap to the next row.
        /// </summary>
        Cycle,
        /// <summary>
        /// Move the selection to the next column, then wrap to the next row, then out of the control.
        /// </summary>
        CycleOut
    }

    /// <summary>
    /// Specifies constants that define the grid's sorting capabilities.
    /// </summary>
    public enum AllowSorting
    {
        /// <summary>
        /// Users cannot sort the grid by clicking the column headers.
        /// </summary>
        None,
        /// <summary>
        /// Users may sort the grid by a single column at a time.
        /// Clicking the column header sorts the column or flips the sort direction.
        /// Ctrl+Clicking removes the sort.
        /// </summary>
        SingleColumn,
        /// <summary>
        /// Users may sort the grid by multiple columns at a time.
        /// Clicking the column header sorts the column or flips the sort direction.
        /// Ctrl+Clicking removes the sort for the clicked column.
        /// Ctrl+Shift+Clicking removes all sorts.
        /// </summary>
        MultiColumn
    }

    /// <summary>
    /// Specifies constants that define copied header cells when copying the content to the clipboard.
    /// </summary>
    public enum CopyHeader
    {
        /// <summary>
        /// Nothing copy.
        /// </summary>
        None,
        /// <summary>
        /// Copy header column.
        /// </summary>
        Column,
        /// <summary>
        /// Copy header row.
        /// </summary>
        Row,
        /// <summary>
        /// Copy both column and row
        /// </summary>
        All
    }

    /// <summary>
    /// Specifies constants that allows pinning/unpinning single columns or column ranges.
    /// </summary>
    public enum PinningType
    {
        /// <summary>
        /// Users cannot pin columns
        /// </summary>
        None = 0,
        /// <summary>
        /// Users can pin and unpin one column at a time (possibly moving them in the process).
        /// </summary>
        SingleColumn = 1,
        /// <summary>
        /// Users can pin and unpin column ranges (columns do not move when being pinned or unpinned)
        /// </summary>
        ColumnRange = 2,
        /// <summary>
        /// Users can pin and unpin single columns or column ranges (using the shift key)
        /// </summary>
        Both = 3,
    }

    /// <summary>
    /// Specifies constants that define the type of editor used with data-mapped columns.
    /// </summary>
    public enum DataMapEditor
    {
        /// <summary>
        /// Use an input element with auto-complete and validation.
        /// </summary>
        AutoComplete = 0,
        /// <summary>
        /// Use an input element with auto-complete, validation, and a drop-down list.
        /// </summary>
        DropDownList = 1,
        /// <summary>
        /// Use radio buttons with mouse and keyboard support.
        /// </summary>
        RadioButtons = 2,
    }
}
