﻿using System.ComponentModel;

namespace C1.Web.Mvc.Grid
{
    /// <summary>
    /// Represents a rectangular group of cells defined by two row indices and two column indices.
    /// </summary>
    public sealed class CellRange
    {
        private int _row = -1;
        private int _col = -1;
        private int _row2 = -1;
        private int _col2 = -1;

        #region Properties
        /// <summary>
        /// Gets or sets the index of the first row in this range.
        /// </summary>
        [DefaultValue(-1)]
        public int Row
        {
            get { return _row; }
            set { _row = value; }
        }

        /// <summary>
        /// Gets or sets the index of the first column in this range.
        /// </summary>
        [DefaultValue(-1)]
        public int Col
        {
            get { return _col; }
            set { _col = value; }
        }

        /// <summary>
        /// Gets or sets the index of the second row in this range.
        /// </summary>
        [DefaultValue(-1)]
        public int Row2
        {
            get { return _row2; }
            set { _row2 = value; }
        }

        /// <summary>
        /// Gets or sets the index of the second column in this range.
        /// </summary>
        [DefaultValue(-1)]
        public int Col2
        {
            get { return _col2; }
            set { _col2 = value; }
        }
        #endregion Properties

        #region Ctors
        /// <summary>
        /// Creates one <see cref="CellRange"/> instance.
        /// </summary>
        /// <param name="r">Index of the first row in this range.</param>
        /// <param name="c">Index of the first column in this range.</param>
        /// <param name="r2">Index of the last row in this range.</param>
        /// <param name="c2">Index of the first column in this range.</param>
        public CellRange(int r, int c, int r2, int c2)
        {
            Row = r;
            Col = c;
            Row2 = r2;
            Col2 = c2;
        }

        /// <summary>
        /// Creates one <see cref="CellRange"/> instance.
        /// </summary>
        /// <param name="r">Index of the first row in this range.</param>
        /// <param name="c">Index of the first column in this range.</param>
        public CellRange(int r, int c)
            :this(r, c, r, c)
        {
        }
        #endregion Ctors

        #region Methods
        /// <summary>
        /// Checks whether this range contains valid row and column indices (> -1).
        /// </summary>
        /// <returns>True if it is valid. Otherwise, false.</returns>
        public bool IsValid()
        {
            return Row > -1 && Col > -1 && Row2 > -1 && Col2 > -1;
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current <see cref="CellRange"/>.
        /// </summary>
        /// <param name="obj">The object to compare with the current <see cref="CellRange"/>.</param>
        /// <returns>Determines whether the specified object is equal to the current <see cref="CellRange"/>.</returns>
        public override bool Equals(object obj)
        {
            var that = obj as CellRange;
            if (that == null) return false;
            return that.Row == Row && that.Col == Col && that.Row2 == Row2 && that.Col2 == Col2;
        }

        /// <summary>
        /// Serves as a hash function for <see cref="CellRange"/>.
        /// </summary>
        /// <returns>A hash code for the current <see cref="CellRange"/>.</returns>
        public override int GetHashCode()
        {
            return Row << 6 | Col << 4 | Row2 << 2 | Col2;
        }

        /// <summary>
        /// Clones a copy of current <see cref="CellRange"/>.
        /// </summary>
        /// <returns>The new <see cref="CellRange"/> which is copied from current <see cref="CellRange"/>.</returns>
        public CellRange Clone()
        {
            return new CellRange(Row, Col, Row2, Col2);
        }
        #endregion Methods
    }
}
