﻿module wijmo.collections.CollectionView {
    /**
     * Casts the specified object to @see:wijmo.collections.CollectionView type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): CollectionView {
        return obj;
    }
}

module wijmo.collections.NotifyCollectionChangedEventArgs {
    /**
     * Casts the specified object to @see:wijmo.collections.NotifyCollectionChangedEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): NotifyCollectionChangedEventArgs {
        return obj;
    }
}

module wijmo.collections.PageChangingEventArgs {
    /**
     * Casts the specified object to @see:wijmo.collections.PageChangingEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PageChangingEventArgs {
        return obj;
    }
}

module wijmo.collections.PropertyGroupDescription {
    /**
     * Casts the specified object to @see:wijmo.collections.PropertyGroupDescription type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PropertyGroupDescription {
        return obj;
    }
}

module wijmo.collections.SortDescription {
    /**
     * Casts the specified object to @see:wijmo.collections.SortDescription type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): SortDescription {
        return obj;
    }
}

module wijmo.collections.GroupDescription {
    /**
     * Casts the specified object to @see:wijmo.collections.GroupDescription type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): GroupDescription {
        return obj;
    }
}

module wijmo.collections.ICollectionView {
    /**
     * Casts the specified object to @see:wijmo.collections.ICollectionView type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ICollectionView {
        return obj;
    }
}

module wijmo.collections.IEditableCollectionView {
    /**
     * Casts the specified object to @see:wijmo.collections.IEditableCollectionView type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): IEditableCollectionView {
        return obj;
    }
}

module wijmo.collections.IPagedCollectionView {
    /**
     * Casts the specified object to @see:wijmo.collections.IPagedCollectionView type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): IPagedCollectionView {
        return obj;
    }
}

