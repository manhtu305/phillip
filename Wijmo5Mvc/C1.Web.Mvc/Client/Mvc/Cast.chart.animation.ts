﻿module wijmo.chart.animation.ChartAnimation {
    /**
     * Casts the specified object to @see:wijmo.chart.animation.ChartAnimation type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ChartAnimation {
        return obj;
    }
}

