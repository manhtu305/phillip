﻿module wijmo.grid.filter.ConditionFilter {
    /**
     * Casts the specified object to @see:wijmo.grid.filter.ConditionFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ConditionFilter {
        return obj;
    }
}

module wijmo.grid.filter.ValueFilter {
    /**
     * Casts the specified object to @see:wijmo.grid.filter.ValueFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ValueFilter {
        return obj;
    }
}

module wijmo.grid.filter.ColumnFilter {
    /**
     * Casts the specified object to @see:wijmo.grid.filter.ColumnFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ColumnFilter {
        return obj;
    }
}

module wijmo.grid.filter.FlexGridFilter {
    /**
     * Casts the specified object to @see:wijmo.grid.filter.FlexGridFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexGridFilter {
        return obj;
    }
}

module wijmo.grid.filter.FilterCondition {
    /**
     * Casts the specified object to @see:wijmo.grid.filter.FilterCondition type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FilterCondition {
        return obj;
    }
}

module wijmo.grid.filter.ColumnFilterEditor {
    /**
     * Casts the specified object to @see:wijmo.grid.filter.ColumnFilterEditor type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ColumnFilterEditor {
        return obj;
    }
}

module wijmo.grid.filter.ConditionFilterEditor {
    /**
     * Casts the specified object to @see:wijmo.grid.filter.ConditionFilterEditor type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ConditionFilterEditor {
        return obj;
    }
}

module wijmo.grid.filter.ValueFilterEditor {
    /**
     * Casts the specified object to @see:wijmo.grid.filter.ValueFilterEditor type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ValueFilterEditor {
        return obj;
    }
}

