﻿/// <reference path="Control.ts" />
/// <reference path="../Shared/Chart.ts" />

/**
 * Defines the @see:c1.mvc.chart.hierarchical.sunburst control and its associated classes.
 */
module c1.mvc.chart.hierarchical {

    export class _SunburstWrapper extends _FlexPieWrapper {
        get _controlType(): any {
            return c1.mvc.chart.hierarchical.Sunburst;
        }
        get _initializerType(): any {
            return c1.mvc.chart.hierarchical._SunburstInitializer;
        }
    }

    export class _SunburstInitializer extends _Initializer {
        _beforeInitializeControl(options: any): void {
            var childItemsPath = options.childItemsPath;
            if(childItemsPath && childItemsPath.indexOf(',') > 0) {
                options.childItemsPath = childItemsPath.split(',');
            }
        }

        _afterInitializeControl(options: any) {
            super._afterInitializeControl(options);
            var sunburst = <Sunburst>this.control;
            sunburst._isInitialized = true;

            // refresh the control after initialize
            sunburst.invalidate();
        }
    }

    /**
     * The @see:Sunburst control displays hierarhical data as multi-level pie charts.
     */
    export class Sunburst extends c1.chart.hierarchical.Sunburst {
        _isInitialized: boolean;

        refresh(fullUpdate = true) {
            // postpone the refresh if the control is not intialized.
            if (!this._isInitialized) return;

            super.refresh(fullUpdate);
        }
    }


    export class _TreeMapWrapper extends _FlexChartBaseWrapper {
        get _controlType(): any {
            return c1.mvc.chart.hierarchical.TreeMap;
        }

        get _initializerType(): any {
            return c1.mvc.chart.hierarchical._TreeMapInitializer;
        }
    }

    export class _TreeMapInitializer extends _Initializer {
        _beforeInitializeControl(options: any): void {
            var childItemsPath = options.childItemsPath;
            if (childItemsPath && childItemsPath.length == 1) {
                options.childItemsPath = childItemsPath[0];
            }

            var bindingName = options.bindingName;
            if (bindingName && bindingName.length == 1) {
                options.bindingName = bindingName[0];
            }
        }

        _afterInitializeControl(options: any) {
            super._afterInitializeControl(options);
            var treeMap = <TreeMap>this.control;
            treeMap._isInitialized = true;

            // refresh the control after initialize
            treeMap.invalidate();
        }
    }

    /**
     * The @see:TreeMap control displays hierarchical (tree-structured) data as a set of nested rectangles.
     */
    export class TreeMap extends c1.chart.hierarchical.TreeMap {
        _isInitialized: boolean;

        refresh(fullUpdate = true) {
            // postpone the refresh if the control is not intialized.
            if (!this._isInitialized) return;

            super.refresh(fullUpdate);
        }
    }
}