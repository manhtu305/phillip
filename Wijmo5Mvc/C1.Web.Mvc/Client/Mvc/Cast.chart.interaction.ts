﻿module wijmo.chart.interaction.ChartGestures {
    /**
     * Casts the specified object to @see:wijmo.chart.interaction.ChartGestures type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ChartGestures {
        return obj;
    }
}

module wijmo.chart.interaction.RangeSelector {
    /**
     * Casts the specified object to @see:wijmo.chart.interaction.RangeSelector type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): RangeSelector {
        return obj;
    }
}

