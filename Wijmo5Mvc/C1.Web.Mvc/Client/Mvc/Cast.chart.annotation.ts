﻿module wijmo.chart.annotation.AnnotationBase {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.AnnotationBase type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): AnnotationBase {
        return obj;
    }
}

module wijmo.chart.annotation.Circle {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Circle type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Circle {
        return obj;
    }
}

module wijmo.chart.annotation.Ellipse {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Ellipse type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Ellipse {
        return obj;
    }
}

module wijmo.chart.annotation.Image {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Image type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Image {
        return obj;
    }
}

module wijmo.chart.annotation.Line {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Line type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Line {
        return obj;
    }
}

module wijmo.chart.annotation.Polygon {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Polygon type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Polygon {
        return obj;
    }
}

module wijmo.chart.annotation.Rectangle {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Rectangle type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Rectangle {
        return obj;
    }
}

module wijmo.chart.annotation.Shape {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Shape type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Shape {
        return obj;
    }
}

module wijmo.chart.annotation.Square {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Square type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Square {
        return obj;
    }
}

module wijmo.chart.annotation.Text {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.Text type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Text {
        return obj;
    }
}

module wijmo.chart.annotation.AnnotationLayer {
    /**
     * Casts the specified object to @see:wijmo.chart.annotation.AnnotationLayer type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): AnnotationLayer {
        return obj;
    }
}

