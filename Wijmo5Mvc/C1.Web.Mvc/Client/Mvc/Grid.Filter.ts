﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts" />
/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.odata.d.ts" />
/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.filter.d.ts" />
/// <reference path="Collections.ts" />
/// <reference path="../Shared/Grid.ts" />

/**
* Extension that provides an Excel-style filtering UI for @see:c1.mvc.grid.FlexGrid controls.
*/
module c1.mvc.grid.filter {

    var trueTypeShowValuesKey = '__trueTypeShowValues',
        saveTrueTypeShowValues = (columnFilter: wijmo.grid.filter.ColumnFilter, values: any[]) =>
            columnFilter[trueTypeShowValuesKey] = values,
        getTrueTypeShowValues = (columnFilter: wijmo.grid.filter.ColumnFilter): any[] =>
            columnFilter[trueTypeShowValuesKey],
        columnDataKey = '__columnData',
        saveColumnData = (columnFilter: wijmo.grid.filter.ColumnFilter, columnData: any[]) =>
            columnFilter[columnDataKey] = columnData,
        getColumnData = (columnFilter: wijmo.grid.filter.ColumnFilter): any[] =>
            columnFilter[columnDataKey] || [];

    /**
     * The @see:FlexGridFilter control extends from @see:c1.grid.filter.FlexGridFilter.
    */
    export class FlexGridFilter
        extends c1.grid.filter.FlexGridFilter {

        _flexGrid: wijmo.grid.FlexGrid & _IFlexGridMvc;
        _collectionView: c1.mvc.collections.RemoteCollectionView;

        /**
         * Initializes a new instance of the @see:FlexGridFilter.
         *
         * @param grid The @see:c1.mvc.grid.FlexGrid to filter.
         */
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc) {
            super(grid);
            this._flexGrid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);
            this._addHandlers();
            this._flexGrid.itemsSourceChanged.addHandler(() => {
                this._removeHandlers();
                this._addHandlers();
            });
        }

        private _addHandlers() {
            this._collectionView = wijmo.asType(this._flexGrid.collectionView, c1.mvc.collections.RemoteCollectionView, true);
            if (this._collectionView
                && this._collectionView._isDisableServerRead
                && !this._collectionView._isDisableServerRead()) {
                this._collectionView.queryData.addHandler(this._collectFilters, this);
            }
        }

        private _removeHandlers() {
            if (this._collectionView) {
                this._collectionView.queryData.removeHandler(this._collectFilters, this);
            }
        }

        /**
         * Initializes the @see:FlexGridFilter by copying the properties from a given object.
         *
         * This method allows you to initialize controls using plain data objects
         * instead of setting the value of each property in code.
         *
         * @param options Object that contains the initialization data.
         */
        initialize(options: any) {
            var self = this, columnFilters: any, count: number;
            if (!options) {
                return;
            }

            var columnFiltersPropName = options.columnFilters ? 'columnFilters' : 'filterColumns';
            //columnFilters = options.columnFilters;
            columnFilters = options[columnFiltersPropName];
            //delete options['columnFilters'];
            delete options[columnFiltersPropName];
            wijmo.copy(self, options);

            if (columnFilters) {
                count = columnFilters.length;
                for (var i = 0; i < count; i++) {
                    self._createColumnFilter(columnFilters[i]);
                }
            }
        }

        _createColumnFilter(opts: any): wijmo.grid.filter.ColumnFilter {
            var self = this,
                colFilter = self.getColumnFilter(self._getColumn(opts.column), true);
            colFilter.filterType = opts.filterType;
            self._updateValueFilter(colFilter.valueFilter, opts.valueFilter);
            return colFilter;
        }

        _processDataMapOptions(opts: any): boolean {
            if (!opts) {
                return;
            }

            var dataMap, mapChanged,
                dataMapOptions = opts['dataMap'];
            if (dataMapOptions) {
                dataMap = new wijmo.grid.DataMap(dataMapOptions['itemsSource'],
                    dataMapOptions['selectedValuePath'], dataMapOptions['displayMemberPath']);
                mapChanged = dataMapOptions['mapChanged'];
                if (mapChanged) {
                    dataMap.mapChanged.addHandler(mapChanged);
                }
                // default value of sortByDisplayValues is false in mvc, but is true in Wijmo 5.
                dataMap.sortByDisplayValues = dataMapOptions['sortByDisplayValues'] === true;
                opts['dataMap'] = dataMap;
            }
        }

        _updateValueFilter(valueFilter: wijmo.grid.filter.ValueFilter, opts: any) {
            if (!opts) {
                return;
            }

            this._processDataMapOptions(opts);
            wijmo.copy(valueFilter, opts);
        }

        _shouldGetAllColumnData(col: any): boolean {
            var self = this,
                cv = self._collectionView,
                cf = self.getColumnFilter(col),
                filterType = cf.filterType;
            return (filterType & wijmo.grid.filter.FilterType.Value)
                && cv && cv._isDisableServerRead && !cv._isDisableServerRead() && !cf.valueFilter.uniqueValues;
        }

        /**
         * Shows the filter editor for the given grid column.
         *
         * @param col The @see:wijmo.grid.Column that contains the filter to edit.
         * @param ht A @see:wijmo.grid.HitTestInfo object containing the range of the cell
         * that triggered the filter display.
         */
        editColumnFilter(col: any, ht: wijmo.grid.HitTestInfo, refElem?: HTMLElement) {
            var self = this, filterPopup, listboxCV: wijmo.collections.CollectionView,
                listbox: wijmo.input.ListBox, values = [], showValues,
                flt: wijmo.grid.filter.ColumnFilter, flexGridCV = self._collectionView,
                searchInput: wijmo.input.ComboBox, saveShowValues: () => void, left, saveColData: (columnData?: any[]) => void;

            //TFS 435298
            var columnFilterEditors: HTMLCollection = document.body.getElementsByClassName('wj-columnfiltereditor');
            if (columnFilterEditors.length > 0) {
                for (let i = 0; i < columnFilterEditors.length; i++) {
                    (<HTMLElement>columnFilterEditors[i]).remove();
                }
            }   

            super.editColumnFilter(col, ht, refElem);
            filterPopup = document.body.getElementsByClassName('wj-columnfiltereditor')[0];
            if (!filterPopup) {
                return;
            }

            listbox = <wijmo.input.ListBox>wijmo.Control.getControl('.wj-columnfiltereditor [wj-part="div-edt-val"] [wj-part="div-values"]');
            if (!listbox) {
                return;
            }

            listboxCV = <wijmo.collections.CollectionView>listbox.itemsSource;
            saveShowValues = () => {
                saveTrueTypeShowValues(flt, listboxCV.items);
            };
            saveColData = columnData => {
                if (columnData === undefined) {
                    columnData = [];
                    if (listboxCV.sourceCollection) {
                        (<any[]>listboxCV.sourceCollection).forEach(item => columnData.push(item.value));
                    }
                }
                saveColumnData(flt, columnData);
            };

            flt = self.getColumnFilter(col);
            searchInput = <wijmo.input.ComboBox>wijmo.Control.getControl('.wj-columnfiltereditor [wj-part="div-edt-val"] [wj-part="div-filter"]');
            if (searchInput) {
                searchInput.textChanged.addHandler(() => {
                    //In wijmo 5, search operation is preformed after 500ms. saveShowValues should after it.
                    setTimeout(saveShowValues, 500);
                });
            }

            if (!self._shouldGetAllColumnData(col)) {
                saveShowValues();
                saveColData();
                return;
            }

            if (flexGridCV && flexGridCV._isDisableServerRead && !flexGridCV._isDisableServerRead()) {
                saveShowValues();
                saveColData();
                // TFS 403869
                // return;
            }

            left = filterPopup.style.left;
            //hide the filter popup untill the data is acquired.
            filterPopup.style.left = '-9999px';
            showValues = flt.valueFilter.showValues;
            flexGridCV.getColumnData(col.binding, true, function (res: c1.mvc.collections.ICollectionViewResponse) {
                var filterKey = col.binding, _sourceCollection = this.sourceCollection;
                for (var i in res.columnData) {
                    var item = res.columnData[i];
                    var text = col.dataMap
                        ? col.dataMap.getDisplayValue(item)
                        : wijmo.Globalize.format(item, col.format),
                        value: any;

                    text = flt.valueFilter.dataMap
                        ? flt.valueFilter.dataMap.getDisplayValue(text)
                        : text;

                    value = { value: item, text: text };
                    let filterValues = _sourceCollection.filter((e) => {
                        let filterValue = e[filterKey];
                        if (filterKey.indexOf(".") > -1) {    //Multi row
                            let keys = filterKey.split(".");
                            filterValue = e[keys[0]];
                            for (let i = 1; i < keys.length; i++) {
                                filterValue = filterValue[keys[i]]
                            }
                        }
                        if (filterValue instanceof Date) {
                            return filterValue.getTime() === item.getTime();
                        }
                        return filterValue === item;
                    });
                    if (!filterValues || filterValues.length === 0) {
                        continue;
                    }
                    if (!showValues || Object.keys(showValues).length === 0) { 
                        value.show = true;
                    } else {
                        for (var key in showValues) {
                            if (value.text === key) {
                                value.show = true;
                            }
                        }
                    }
                    values.push(value);
                }
                saveColData(res.columnData);
                listboxCV.sourceCollection = values;
                saveShowValues();
                listboxCV.moveCurrentToPosition(0);
                // TFS 440605
                if (!wijmo.isFirefox())
                    filterPopup.style.left = left;
            });
        }

        _getColumn(name: string): wijmo.grid.Column {
            var self = this, columns = self._flexGrid._getBindingColumns(),
                i, colLength: number = columns.length, currentCol, name = name.toLowerCase();
            for (i = 0; i < colLength; i++) {
                currentCol = columns[i];
                if (currentCol.name && currentCol.name.toLowerCase() === name) {
                    return currentCol;
                }
            }

            for (i = 0; i < columns.length; i++) {
                currentCol = columns[i];
                if (currentCol.binding && currentCol.binding.toLowerCase() === name) {
                    return currentCol;
                }
            }
        }

        //Keep consistent with the corresponding sub-property name of ExtendedRequestData in server side.
        private _getServerComponentName(): string {
            return 'FlexGridFilter';
        }

        private _collectFilters(sender: c1.mvc.collections.RemoteCollectionView, e: c1.mvc.collections.QueryEventArgs) {
            if (e.extraRequestData == null) {
                e.extraRequestData = {};
            }
            e.extraRequestData[this._getServerComponentName()] = this._getFilterSettings();
        }

        private _getFilterSettings() {
            var self = this,
                columns = self._flexGrid._getBindingColumns(),
                columnCount: number = columns.length,
                i: number,
                filterSettings: any[] = [],
                filter: wijmo.grid.filter.ColumnFilter;
            for (i = 0; i < columnCount; i++) {
                filter = self.getColumnFilter(columns[i], false);
                if (filter && filter.isActive) {
                    filterSettings.push(self._getColumnFilterSetting(filter));
                }
            }
            return filterSettings;
        }

        private _getColumnFilterSetting(colFilter: wijmo.grid.filter.ColumnFilter) {
            var col: wijmo.grid.Column = colFilter.column, self = this, columnFilter = {};

            if (!col.binding) {
                return;
            }

            columnFilter['column'] = col.binding;
            columnFilter['format'] = col.format;

            if (colFilter.conditionFilter.isActive) {
                c1.mvc.Utils.extend(columnFilter, {
                    conditionFilter: {
                        and: colFilter.conditionFilter.and,
                        condition1: self._getConditionSetting(colFilter.conditionFilter.condition1, col),
                        condition2: self._getConditionSetting(colFilter.conditionFilter.condition2, col)
                    }
                });
            }

            if (colFilter.valueFilter.isActive) {
                c1.mvc.Utils.extend(columnFilter, {
                    valueFilter: {
                        showValues: self._getShowValues(colFilter)
                    }
                });
            }

            return columnFilter;
        }
        private _getShowValuesFromFilterDefinition(colFilter: wijmo.grid.filter.ColumnFilter): any[] {
            if (!colFilter || !colFilter.valueFilter) return [];
            if (!colFilter.valueFilter.isActive) return [];
            if (!colFilter.valueFilter.showValues) return [];

            var showValues = [];

            let colShowValues = colFilter.valueFilter.showValues;
            if (colShowValues && Object.keys(colShowValues).length > 0) {
                var colName = colFilter.column.binding;
                this._collectionView.items.forEach(item => {
                    if (item.hasOwnProperty(colName)) {
                        var colValue = item[colName];
                        var text = wijmo.Globalize.format(colValue, colFilter.column.format);
                        if (colShowValues.hasOwnProperty(text) && colShowValues[text]) {
                            showValues.push(colValue);
                        }
                    }
                });
            }

            return showValues;
        }

        private _getShowValues(colFilter: wijmo.grid.filter.ColumnFilter): any[] {
            var showValues = [], index: number, items = getTrueTypeShowValues(colFilter), currentItem;
            var needSaveValues = false;
            if (items) {
                for (index = 0; index < items.length; index++) {
                    currentItem = items[index];
                    if (currentItem && currentItem.show) {
                        showValues.push(currentItem.value);
                    }
                }
            }

            if (!showValues.length) {
                showValues = getColumnData(colFilter);
            }

            if (!items && !showValues.length) {
                showValues = this._getShowValuesFromFilterDefinition(colFilter);
                needSaveValues = true;
            }

            //TFS 382635
            if (colFilter.valueFilter && !colFilter.valueFilter.exclusiveValueSearch) {
                showValues = this._getShowValuesFromFilterDefinition(colFilter);
            }

            // add dateKind for value filter.
            var col = colFilter.column;
            if (col.dataType === wijmo.DataType.Date) {
                var dateKind = this._getDateKind(col);
                var count = showValues.length;
                for (var i = 0; i < count; i++) {
                    var date = showValues[i];
                    if (date) {
                        showValues[i]['dateKind'] = dateKind;
                    }
                }
            }

            if (needSaveValues) {
                saveColumnData(colFilter, showValues);
            }
            return showValues;
        }

        private _getDateKind(col: wijmo.grid.Column): DateKind {
            if (col.dataType === wijmo.DataType.Date) {
                var binding = col.binding ? new wijmo.Binding(col.binding) : null;
                if (binding != null) {
                    if (this._collectionView._dataInfo != null) {
                        return <DateKind>binding.getValue(this._collectionView._dataInfo);
                    }

                    return DateKind.Unspecified;
                }
            }
        }

        private _getConditionSetting(condition: wijmo.grid.filter.FilterCondition, col: wijmo.grid.Column) {
            var cValue: any = condition.value;
            // when the dataType of column is boolean, the condition value is string.
            // so here the text should be converted to boolean in order to compare correctly in server side.
            if (col.dataType === wijmo.DataType.Boolean) {
                cValue = wijmo.changeType(cValue, wijmo.DataType.Boolean, null);
            } else if (col.dataType === wijmo.DataType.Date && cValue instanceof Date) {
                // add dateKind for condition filter.
                cValue['dateKind'] = this._getDateKind(col);
            }

            return {
                operator: condition.operator,
                value: cValue
            };
        }
    }
} 