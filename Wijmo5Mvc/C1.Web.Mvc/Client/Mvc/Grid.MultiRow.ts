﻿/// <reference path="Util.ts" />
/// <reference path="Template.ts" />
/// <reference path="Control.ts" />
/// <reference path="../Shared/Grid.MultiRow.ts" />

/**
 * Defines the @see:c1.mvc.grid.multirow.MultiRow control and associated classes.
 */
module c1.mvc.grid.multirow {

    export class _MultiRowWrapper extends _ControlWrapper {

        get _controlType(): any {
            return MultiRow;
        }

        get _initializerType(): any {
            return _Initializer;
        }

        _getExtensionTypes(): any[]{
            var extensions = super._getExtensionTypes();
            extensions.push(_ItemTemplateProvider,
                _Validator,
                _SortHelper,
                _MaskLayer,
                _VirtualScrolling,
                _UpdateHelper);
            return extensions;
        }
    }

    /**
     * The @see:MultiRow control extends from @see:c1.grid.multirow.MultiRow.
     */
    export class MultiRow extends c1.grid.multirow.MultiRow implements _IFlexGridMvc {

        // _IFlexGridMvc
        _getSortDescriptorKeyMvc(): string {
            return 'MultiRowSort';
        }

        // _IFlexGridMvc
        _getFlatColumnsMvc(options): any[] {
            return this._flatColumnsMvc(options['layoutDefinition']);
        }

        private _flatColumnsMvc(groups: any[]): any[] {
            var columns = [];
            if (groups && groups.length) {
                groups.forEach(group => {
                    var cells: any[] = group.cells;
                    if (cells && cells.length) {
                        columns = columns.concat(cells);
                    }
                });
            }
            return columns;
        }

        // _IFlexGridMvc
        _getLayoutGroupsMvc(options: any): any[] {
            return options['layoutDefinition'];
        }

        // _IFlexGridMvc
        _getBindingColumns(): wijmo.grid.Column[] {
            return this._flatColumnsMvc(this._layout._bindingGroups);
        }

        // _IFlexGridMvc
		_getBindingColumnMvc(groupIndex: number, cellIndex: number): wijmo.grid.Column {
			var cellGroups = this._layout._bindingGroups,
                cell = cellGroups[groupIndex].cells[cellIndex],
                columnOffset = 0;

            for (var i = 0; i < groupIndex; i++) {
                columnOffset += cellGroups[i].colspan;
            }

            // cell._row is the row index from layout definition,
            // thre is an empty row int the MultiRow's columnHeaders,
            // so need add 1 to the row index.
            return this.getBindingColumn(this.columnHeaders, cell._row + 1, cell._col + columnOffset);
        }

        // _IFlexGridMvc
        _getBindingColumnFromCellRangeMvc(e: wijmo.grid.CellRangeEventArgs): wijmo.grid.Column {
            return this.getBindingColumn(e.panel, e.row, e.col);
        }

        // _IFlexGridMvc
        _getRowsPerItemMvc(): number {
            return this.rowsPerItem;
        }

        // _IFlexGridMvc
        _getSortCellMvc(rowIndex:number, columnIndex: number, column: wijmo.grid.Column): _IGroupCell {
			var groupIndex = -1, cellIndex = -1,
				cellGroups = this._layout._bindingGroups;

            // for MultiRow, the physic column is not the binding column
            var bindingColumn = this.getBindingColumn(this.columnHeaders, rowIndex, columnIndex);

            // there is an empty row in the column header's rows,
            // reduce 1 to get the real row index.
            rowIndex--;

            for (var i = 0; i < cellGroups.length; i++) {
                var group = cellGroups[i];
                if (columnIndex < group.colspan) {
                    // find the group
                    groupIndex = i;
                    cellIndex = group.cells.indexOf(<wijmo.grid.multirow._Cell>bindingColumn);
                    break;
                } else {
                    // go to the next group
                    columnIndex -= group.colspan;
                }
            }

            return { groupIndex: groupIndex, cellIndex: cellIndex, cell: bindingColumn };
        }
        get defaultRowSize(): number {
            return this.rows.defaultSize;
        }

        set defaultRowSize(value: number) {
            this.rows.defaultSize = value;
        }

        get defaultColumnSize(): number {
            return this.columns.defaultSize;
        }

        set defaultColumnSize(value: number) {
            this.columns.defaultSize = value;
        }
    }
}