﻿/// <reference path="Control.ts" />
/// <reference path="../Shared/Gauge.ts" /> 

/**
 * Defines the C1 MVC gauge controls.
 */
module c1.mvc.gauge {
    export class _Initializer extends c1.mvc._Initializer {

        private _rangesProps = [];

        _beforeInitializeControl(options: any) {
            var self = this, i, rangesLength, propName = "propertyChanged",
                pointerOpts = options["pointer"], faceOpts = options["face"], rangesOpts = options["ranges"];
            if (pointerOpts) {
                _addEvent(self.control["pointer"][propName], pointerOpts[propName]);
                delete pointerOpts[propName];
            }
            if (faceOpts) {
                _addEvent(self.control["face"][propName], faceOpts[propName]);
                delete faceOpts[propName];
            }
            rangesLength = rangesOpts ? rangesOpts.length : 0;
            for (i = 0; i < rangesLength; i++) {
                if (rangesOpts) {
                    self._rangesProps.push(rangesOpts[i][propName]);
                    delete rangesOpts[i][propName];
                }
            }
        }

        _afterInitializeControl(options: any) {
            var i, self = this, propName = "propertyChanged",
                rangesProps = self._rangesProps,
                length = rangesProps.length;
            for (i = 0; i < length; i++) {
                _addEvent(self.control["ranges"][i][propName], rangesProps[i]);
            }
        }
    }

    export class _GaugeWrapper extends _ControlWrapper {
        get _initializerType(): any {
            return _Initializer;
        }
    }

    export class _LinearGaugeWrapper extends _GaugeWrapper {
        get _controlType(): any {
            return LinearGauge;
        }
    }

    /**
     * The @see:LinearGauge displays a linear scale with an indicator that
     * represents a single value and optional ranges to represent reference values.
     */
    export class LinearGauge extends c1.gauge.LinearGauge {
    }

    export class _BulletGraphWrapper extends _LinearGaugeWrapper {
        get _controlType(): any {
            return BulletGraph;
        }
    }
    /**
     * The @see:BulletGraph is a type of linear gauge designed specifically 
     * for use in dashboards. It displays a single key measure along with 
     * a comparative measure and qualitative ranges to instantly signal whether
     * the measure is good, bad, or in some other state.
     */
    export class BulletGraph extends c1.gauge.BulletGraph {
    }

    export class _RadialGaugeWrapper extends _GaugeWrapper {
        get _controlType(): any {
            return RadialGauge;
        }
    }

    /**
     * The @see:RadialGauge displays a circular scale with an indicator 
     * that represents a single value and optional ranges to represent 
     * reference values.
     */
    export class RadialGauge extends c1.gauge.RadialGauge {
    }
}