﻿/// <reference path="Collections.ts" />
/// <reference path="Grid.ts" />

module c1.mvc.grid {

    // The extension which provides mask layer.
    export class _MaskLayer {
        private readonly _grid: wijmo.grid.FlexGrid;
        private _collectionView: wijmo.collections.ICollectionView;
        private _maskLayer: HTMLElement;

        // Initializes a new instance of the @see:_MaskLayer.
        // @param grid The @see:wijmo.grid.FlexGrid.
        constructor(grid: wijmo.grid.FlexGrid) {
            this._grid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);

            _overrideMethod(grid, _Initializer._INITIAL_NAME,
                null, this._afterInitialize.bind(this));
        }

        private _afterInitialize(options) {
            if (!options) {
                return;
            }

            this._addHandlers();
            this._grid.itemsSourceChanged.addHandler(() => {
                this._removeHandlers();
                this._addHandlers();
            });
        }

        private _addHandlers() {
            this._collectionView = this._grid.collectionView;
            let rcv = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(this._collectionView, c1.mvc.collections.RemoteCollectionView);
            if (rcv) {
                rcv.beginQuery.addHandler(this._updateMaskLayer, this);
                rcv.endQuery.addHandler(this._updateMaskLayer, this);
            } else {
                let odcv = <wijmo.odata.ODataCollectionView>wijmo.tryCast(this._collectionView, wijmo.odata.ODataCollectionView);
                if (odcv) {
                    odcv.loading.addHandler(this._updateMaskLayer, this);
                    odcv.loaded.addHandler(this._updateMaskLayer, this);
                }
            }
            this._updateMaskLayer();
        }

        private _removeHandlers() {
            let rcv = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(this._collectionView, c1.mvc.collections.RemoteCollectionView);
            if (rcv) {
                rcv.beginQuery.removeHandler(this._updateMaskLayer, this);
                rcv.endQuery.removeHandler(this._updateMaskLayer, this);
            } else {
                let odcv = <wijmo.odata.ODataCollectionView>wijmo.tryCast(this._collectionView, wijmo.odata.ODataCollectionView);
                if (odcv) {
                    odcv.loading.removeHandler(this._updateMaskLayer, this);
                    odcv.loaded.removeHandler(this._updateMaskLayer, this);
                }
            }
        }

        _updateMaskLayer() {
            if (DataSourceManager._isQueringData(this._grid.collectionView)) {
                this._showMaskLayer();
            } else {
                this._hideMaskLayer();
            }
        }

        _showMaskLayer() {
            if (!this._maskLayer) {
                this._maskLayer = this._createMaskLayer();
            }
            this._maskLayer.style.display = 'inline-block';
        }

        _hideMaskLayer() {
            if (this._maskLayer) {
                this._maskLayer.style.display = 'none';
            }
        }

        private _createMaskLayer(): HTMLElement {
            // add the mask layer to the sibling of cells' container.
            var parentElement: HTMLElement = this._grid.cells.hostElement.parentElement.parentElement,
                maskLayer: HTMLElement,
                parentSize: wijmo.Rect,
                maskStyle: CSSStyleDeclaration;

            if (!parentElement) {
                throw 'the grid container doesn\'t exist.';
            }
            parentSize = wijmo.getElementRect(parentElement);
            maskLayer = document.createElement('div');
            maskStyle = maskLayer.style;
            maskStyle.left = '0px';
            maskStyle.top = '0px';
            maskStyle.width = parentSize.width + 'px';
            maskStyle.height = parentSize.height + 'px';
            maskStyle.lineHeight = parentSize.height + 'px';
            maskStyle.display = 'none';
            maskLayer.innerText = 'loading...';
            parentElement.appendChild(maskLayer);
            wijmo.addClass(maskLayer, 'c1-grid-mask');
            return maskLayer;
        }
    }
}