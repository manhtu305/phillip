﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts" />

module c1.mvc {

    // Raise a callback to server.
    export class CallbackManager {
        _keyPrefix: string = 'wj-';
        _keys = {
            isCallback: this._keyPrefix + 'cbk',
            uniqueId: this._keyPrefix + 'id',
            data: this._keyPrefix + 'data'
        };

        constructor(
            public uniqueId: string) {
        }

        //
        // Begin the callback.
        //
        // @example
        //  this.getCallbackManager().doCallback({
        //       msg: 'hello {0}',
        //       obj: 'world'
        //  }, data => {
        //      alert(data.info);
        //      alert(data.msg);
        //  });
        //
        doCallback(parameters: any, complete: Action1<any>, error: Action1<ErrorEvent>,
            dataStringifying?: (e: JSONOperationEventArgs) => boolean,
            responseTextParsing?: (e: JSONOperationEventArgs) => boolean) {
            var data = {}, keys = this._keys;
            data[keys.isCallback] = 'true';
            data[keys.uniqueId] = this.uniqueId;
            data[keys.data] = Utils.jsonStringify(parameters, dataStringifying);
            Utils.ajaxPost({
                url: '',
                success: complete,
                error: error,
                data: data,
                requestDataStringifying: dataStringifying,
                responseTextParsing: responseTextParsing
            });
        }
    }

    // Manage the sources.
    export class DataSourceManager {
        static _isRemoteSource(source: wijmo.collections.ICollectionView): boolean {
            if (!source) {
                return false;
            }

            let rc = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(source, c1.mvc.collections.RemoteCollectionView);
            if (rc) {
                return rc._isRemoteMode();
            }

            let odataSource = wijmo.tryCast(source, wijmo.odata.ODataCollectionView);
            return !!odataSource;
        }

        static _isQueringData(source: wijmo.collections.ICollectionView): boolean {
            if (!source) {
                return false;
            }

            let rc = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(source, c1.mvc.collections.RemoteCollectionView);
            if (rc) {
                return rc._isQuerying && rc._isQuerying();
            }

            let odataSource = <wijmo.odata.ODataCollectionView>wijmo.tryCast(source, wijmo.odata.ODataCollectionView);
            return odataSource && odataSource._loading;
        }

        static _isDynamicalLoading(source: wijmo.collections.ICollectionView): boolean {
            if (!source) {
                return false;
            }

            let rc = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(source, c1.mvc.collections.RemoteCollectionView);
            if (rc) {
                return rc._isDynamicalLoadingEnabled && rc._isDynamicalLoadingEnabled();
            }

            let odataVirtualSource = <wijmo.odata.ODataVirtualCollectionView>wijmo.tryCast(source, wijmo.odata.ODataVirtualCollectionView);
            return !!odataVirtualSource;
        }
    }

    export interface IResult {
        success: boolean;
        value?: any;
    }

    // If use virtual scrolling the item in collectionview can be null.
    // Wijmo5 have not consider about this, so override its method.
    var bindingGetValue = wijmo.Binding.prototype["getValue"];
    wijmo.Binding.prototype["getValue"] = function (object: any): any {
        if (object == null) {
            return null;
        }
        if (bindingGetValue) {
            object = bindingGetValue.call(this, object);
        }
        return object;
    };

    /**
     * Utility functions.
     */
    export class Utils {

        private static class2type: any;
        private static dateJsonRegx = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)(:\d*)?(Z|[\+\-]\d{2}:\d{2}|)$/;
        private static requestVerificationTokenKey = "RequestVerificationToken";

        static getType(obj): string {
            var allObjects: string[],
                i: number;
            if (Utils.class2type == null) {
                allObjects = 'Boolean Number String Function Array Date RegExp Object Error'.split(' ');
                Utils.class2type = {};
                for (i = 0; i < allObjects.length; i++) {
                    Utils.class2type['[object ' + allObjects[i] + ']'] = allObjects[i].toLowerCase();
                }
            }
            if (obj == null) {
                return obj + '';
            }
            return typeof obj === 'object' || typeof obj === 'function' ?
                Utils.class2type[({}).toString.call(obj)] || 'object' :
                typeof obj;
        }

        static isEmptyContent(content: string): boolean {
            if (content == null || !content.length || !(content.replace(/\s*/g, '').length)) {
                return true;
            }
            return false;
        }

        static getService(expression: string): any {
            return new Function("return " + expression + ";").call(window);
        }

        static parseFunction(jsonValue: string): IResult {
            if (/function\(.*?\)/.test(jsonValue)) {
                return <IResult>{ value: eval(jsonValue), success: true };
            }
            var result = /^\/Function\((.*?)\)\/$/.exec(jsonValue);
            if (result) {
                return <IResult>{ value: _findFunction(result[1]), success: true };
            }
            return <IResult>{ success: false };
        }

        static parseService(jsonValue: string): IResult {
            var result = /^\/Service\((.*?)\)\/$/.exec(jsonValue);
            if (result) {
                return <IResult>{ value: Utils.getService(result[1]), success: true };
            }
            return <IResult>{ success: false };
        }

        // Parse a Date object from a json text.
        static tryDateJsonParse(text: string): Date {
            var result = Utils.dateJsonRegx.exec(text);
            if (!result) {
                return null;
            }

            var date = new Date(text);
            if (result[8] == '') {
                // fix 308980 in Android browser and Ipad safari.
                text += Utils._getLocalTimeZoneText(date);
            }

            date['dateKind'] = Utils.getDateKind(result[8]);
            return date;
        }

        // utc: "2017-01-01T00:00:00Z"
        // local: "2017-01-01T00:00:00+07:00"
        // unspecified: "2017-01-01T00:00:00"
        private static getDateKind(timeZone: string): DateKind {
            var txtTimeZone = timeZone.toLowerCase();
            if (txtTimeZone == 'z') {
                return DateKind.Utc;
            } else if (txtTimeZone.length == 0) {
                return DateKind.Unspecified;
            }

            return DateKind.Local;
        }

        static jsonParse(text: string, textParsing: (e: JSONOperationEventArgs) => boolean, reviver?: (key: any, value: any) => any) {
            return JSON.parse(text, function (key, value) {
                if (textParsing) {
                    var args = new JSONOperationEventArgs(key, value, this);
                    if (textParsing(args)) {
                        return args.result;
                    }
                }

                if (reviver) {
                    var reviverResult = reviver.call(this, key, value);
                    if (reviverResult !== value) return reviverResult;
                }

                if (typeof value === 'string') {
                    var date = Utils.tryDateJsonParse(value);
                    if (date) {
                        return date;
                    }

                    var result = Utils.parseFunction(value);
                    if (result && result.success) {
                        return result.value;
                    }

                    result = Utils.parseService(value);
                    if (result.success) {
                        return result.value;
                    }
                }

                return value;
            });
        }

        static jsonStringify(obj: any, dataStringifying: (e: JSONOperationEventArgs) => boolean, replacer?: (key: string, value: any) => any, space?: string | number) {
            return JSON.stringify(obj, function (key, value) {
                if (dataStringifying) {
                    var args = new JSONOperationEventArgs(key, value, this);
                    if (dataStringifying(args)) {
                        return args.result;
                    }
                }

                if (replacer) {
                    var result = replacer.call(this, key, value);
                    if (result !== value) return result;
                }

                var date = Utils.tryGetDate(this, key, value);
                if (date) return Utils.dateToJson(date);

                if (typeof value === 'function') return String(value);

                return value;
            }, space);
        }

        static tryGetDate(instance: any, key: string, value: any): Date {
            if (value instanceof Date) return value;

            if (typeof value === 'string') {
                var originValue = key == null ? instance : instance[key];
                if (originValue instanceof Date) return originValue;
            }

            return null;
        }

        // This method is used to serialize a Date object into a json text according to different dateKind.
        // Local:       2017-01-01T01:00:00.000+08:00
        // Unspecified: 2017-01-01T00:00:00.000
        // Utc:         2017-01-01T00:00:00.000Z
        static dateToJson(value: Date): string {
            var dateKind = value['dateKind'] || DateKind.Unspecified;
            return Utils.formatNumber(dateKind == DateKind.Utc ? value.getUTCFullYear() : value.getFullYear(), 4) + '-' +
                Utils.formatNumber((dateKind == DateKind.Utc ? value.getUTCMonth() : value.getMonth()) + 1, 2) + '-' +
                Utils.formatNumber(dateKind == DateKind.Utc ? value.getUTCDate() : value.getDate(), 2) + 'T' +
                Utils.formatNumber(dateKind == DateKind.Utc ? value.getUTCHours() : value.getHours(), 2) + ':' +
                Utils.formatNumber(dateKind == DateKind.Utc ? value.getUTCMinutes() : value.getMinutes(), 2) + ':' +
                Utils.formatNumber(dateKind == DateKind.Utc ? value.getUTCSeconds() : value.getSeconds(), 2) + '.' +
                Utils.formatNumber(dateKind == DateKind.Utc ? value.getUTCMilliseconds() : value.getMilliseconds(), 3) +
                Utils._getTimeZoneText(dateKind, value);
        }

        static _getTimeZoneText(dateKind: DateKind, currentDate?: Date): string {
            switch (dateKind) {
                case DateKind.Local:
                    return Utils._getLocalTimeZoneText(currentDate);
                case DateKind.Utc:
                    return 'Z';
                case DateKind.Unspecified:
                default:
                    return '';
            }
        }

        static _getLocalTimeZoneText(currentDate: Date = new Date()): string {
            var timeoffset = currentDate.getTimezoneOffset();
            var result = '';
            if (timeoffset > 0) {
                result += '-';
            } else {
                result += '+';
                timeoffset *= -1;
            }
            var hour = Math.floor(timeoffset / 60);
            result += Utils.formatNumber(hour, 2);
            result += ":";
            result += Utils.formatNumber(timeoffset - hour * 60, 2);
            return result;
        }

        //This method is from Wijmo 3 WebForms C1Json2.ts.
        static formatNumber(n: number, k: number) {
            // Format integers to have at least k digits.
            var text = n.toString();
            while (text.length < k) {
                text = '0' + text;
            }
            return text;
        }

        static random(min: number, max: number) {
            var rand = Math.random();
            return min + rand * (max - min);
        }

        static formatString(format: string, ...args: string[]): string {
            return format.replace(/{(\d+)}/g, (match, index) => {
                return typeof args[index] !== 'undefined'
                    ? args[index]
                    : match;
            });
        }

        static isObject(value: any): boolean {
            return value != null && typeof value === 'object' && (Utils.getType(value) !== 'date');
        }

        static copy(dst: any, src: any): any {
            var key: string, value;
            for (key in src) {
                value = src[key];
                if (!dst._copy || !dst._copy(key, value)) { // allow overrides
                    if (Utils.isObject(value) && dst[key]) {
                        Utils.copy(dst[key], value); // copy sub-objects
                    } else {
                        dst[key] = value; // assign values
                    }
                }
            }
            return dst;
        }

        static extend(dst: any, ...src: any[]): any {
            var list = src.map(s => Utils.copy({}, s)); // don't want src elements modified, so copy them
            list.unshift(dst);
            return list.reduceRight((s, d) => Utils.copy(d, s));
        }

        static formEncodedObject(src: Object): string {
            var arr = [], key: string, value;
            for (key in src) {
                value = src[key];
                if (Utils.isObject(value)) {
                    value = encodeURIComponent(JSON.stringify(value));
                } else if (value) {
                    value = encodeURIComponent(value.toString());
                }
                arr.push(key + '=' + value);
            }
            return arr.join('&');
        }

        static ajax(ajaxSettings: IAjaxSettings): XMLHttpRequest {
            var xhr = new XMLHttpRequest(),
                settings = <IAjaxSettings>Utils.copy({
                    async: true,
                    cache: false,
                    type: 'GET',
                    postType: 'form'
                }, ajaxSettings),
                params: any,
                dataKey: string,
                headerKey: string,
                url = settings.url,
                forceLoad: string;

            if (typeof settings.data === 'string'
                || settings.data instanceof Document) {
                params = settings.data;
            } else if (Utils.isObject(settings.data)) {
                if (settings.type === 'GET') {
                    url += url.indexOf('?') >= 0 ? '&' : '?';
                    url += Utils.formEncodedObject(settings.data);
                } else {
                    if (settings.postType === 'form') {
                        params = Utils.formEncodedObject(settings.data);
                        settings.contentType = 'application/x-www-form-urlencoded';
                    } else if (settings.postType === 'json') {
                        params = Utils.jsonStringify(settings.data, settings.requestDataStringifying);
                        settings.contentType = 'application/json';
                    } else if (settings.postType === 'multipart') {
                        params = new FormData();
                        for (dataKey in settings.data) {
                            params.append(dataKey, settings.data[dataKey]);
                        }
                    }
                }
            }
            xhr.onload = e => {
                var isSuccess = (xhr.status >= 200 && xhr.status < 300) || xhr.status === 304;
                if (!isSuccess) {
                    if (settings.error) {
                        settings.error.call(xhr, xhr, Utils.getTextStatus(xhr), '');
                    }
                    return;
                }

                var data;
                if (settings.dataType === 'json') {
                    data = Utils.jsonParse(xhr.responseText, settings.responseTextParsing);
                } else if (settings.dataType === 'text'
                    || settings.dataType === 'html'
                    || settings.dataType === 'script') {
                    data = xhr.responseText;
                } else if (settings.dataType === 'xml') {
                    data = xhr.responseXML;
                }
                settings.success.bind(xhr)(data);
            };
            if (settings.error) {
                xhr.onerror = e => {
                    // The intellisense says the e is type of ErrorEvent, 
                    // but it seems the ProgressEvent without valid error information.
                    // settings.error.call(xhr, xhr, Utils.getTextStatus(xhr), e.message || '');

                    // After update to typescript 3.3, e is type of ProgressEvent.
                    settings.error.call(xhr, xhr, Utils.getTextStatus(xhr), '');
                };
            }
            if (!settings.cache) {
                forceLoad = '_=' + Utils.random(1000, 9999).toFixed(0);
                url += url.indexOf('?') >= 0 ? '&' : '?';
                url += forceLoad;
            }
            xhr.open(settings.type, url, settings.async);
            if (settings.headers) {
                for (headerKey in settings.headers) {
                    xhr.setRequestHeader(headerKey, settings.headers[headerKey]);
                }
            }
            if (!settings.headers || !settings.headers[Utils.requestVerificationTokenKey]) {
                var verificationElement = <HTMLInputElement>document.querySelector('input[name=__RequestVerificationToken]');
                if (verificationElement) {
                    xhr.setRequestHeader(Utils.requestVerificationTokenKey, verificationElement.value);
                }
            }

            if (settings.contentType) {
                xhr.setRequestHeader('Content-Type', settings.contentType);
            }
            if (settings.beforeSend) {
                settings.beforeSend(xhr, settings);
            }

            xhr.send(params || null);
            return xhr;
        }

        static getTextStatus(xhr: XMLHttpRequest): string {
            return xhr.status === 0 ? '0 failed' : xhr.status + ' ' + xhr.statusText;
        }

        static beginRequest(ajaxSettings: IAjaxSettings): XMLHttpRequest {
            var settings = <IAjaxSettings>Utils.copy({
                dataType: 'json'
            }, ajaxSettings);
            return Utils.ajax(settings);
        }

        static ajaxGet(ajaxSettings: IAjaxSettings): XMLHttpRequest {
            var settings = <IAjaxSettings>Utils.copy({
                type: 'GET',
                dataType: 'json'
            }, ajaxSettings);
            return Utils.beginRequest(settings);
        }

        static ajaxPost(ajaxSettings: IAjaxSettings): XMLHttpRequest {
            var settings = <IAjaxSettings>Utils.copy({
                type: 'POST',
                dataType: 'json'
            }, ajaxSettings);
            return Utils.beginRequest(settings);
        }

        static ajaxLoad(selector: string, ajaxSettings: IAjaxSettings): XMLHttpRequest {
            var settings = <IAjaxSettings>Utils.copy({
                type: 'GET',
                dataType: 'html',
                success: res => {
                    var element = <HTMLElement>document.querySelector(selector);
                    element.innerHTML = res;
                }
            }, ajaxSettings);
            return Utils.beginRequest(settings);
        }

        // Obsolete: please use c1.documentReady.
        static documentReady(callback: Function) {
            c1.documentReady(callback);
        }

        static tryCallJQuery(callback: ($: any) => void) {
            var $: Function = window["jQuery"];
            if ($ && typeof $ === "function") {
                callback($);
            }
        }

        static forwardValidationEvents(control: wijmo.Control, input: HTMLElement, hidden: HTMLElement, isComboBox: boolean = false) {
            if (!isComboBox) {
                control.gotFocus.addHandler(() => {
                    setTimeout(() => Utils.tryCallJQuery($ => $(hidden).focus()));
                });
            }

            control.addEventListener(control.hostElement, "keyup", (e) => {
                Utils.tryCallJQuery($ => {
                    if (e.which === 9 && $(hidden).val() === "") {
                        return;
                    }
                    $(hidden).keyup();
                });
            });

            control.lostFocus.addHandler(() => {
                Utils.tryCallJQuery($ => $(hidden).blur());
            });
        }

        static triggerValidationChangeEvent(hidden: HTMLElement) {
            Utils.tryCallJQuery($ => $(hidden).keyup());
        }
    }

    // Ajax settings, compatible with jQuery ajax API.
    export interface IAjaxSettings {
        async?: boolean;
        cache?: boolean;
        data?: any;
        dataType?: string;
        type?: string;
        success?: Action1<any>;
        error?: Action1<ErrorEvent>;
        beforeSend?: Action2<XMLHttpRequest, IAjaxSettings>;
        url?: string;
        headers?: any;
        contentType?: string;
        postType?: string;
        responseTextParsing?: (e: JSONOperationEventArgs) => boolean;
        requestDataStringifying?: (e: JSONOperationEventArgs) => boolean;
    }

    // A function type: () => void
    export interface Action0 {
        (): void;
    }

    // A function type: (T) => void
    export interface Action1<T> {
        (p: T): void;
    }

    // A function type: (T1, T2) => void
    export interface Action2<T1, T2> {
        (p1: T1, p2: T2): void;
    }

    // A function type: () => TResult
    export interface Func0<TResult> {
        (): TResult;
    }

    // A function type: (T) => TResult
    export interface Func1<T, TResult> {
        (p: T): TResult;
    }

    // A function type: (T1, T2) => TResult
    export interface Func2<T1, T2, TResult> {
        (p1: T1, p2: T2): TResult;
    }

	export type Writeable<T> = { -readonly [P in keyof T]: T[P] };
	export type DeepWriteable<T> = { -readonly [P in keyof T]: DeepWriteable<T[P]> };
}