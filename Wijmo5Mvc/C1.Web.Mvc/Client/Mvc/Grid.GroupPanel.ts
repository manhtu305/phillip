﻿/// <reference path="../Shared/Grid.GroupPanel.ts" /> 
/// <reference path="./Grid.Filter.ts" />
/**
 * Extension that provides a drag and drop UI for editing groups in bound @see:c1.mvc.grid.FlexGrid controls.
 */
module c1.mvc.grid.grouppanel {
    /**
     * The @see:GroupPanel control extends from @see:c1.grid.grouppanel.GroupPanel.
    */
    export class GroupPanel extends c1.grid.grouppanel.GroupPanel {

        /**
        * Initializes a new instance of a @see:GroupPanel control.
        *
        * @param element The DOM element that hosts the control, or a selector for the host element (e.g. '#theCtrl').
        * @param grid The @see:wijmo.grid.FlexGrid that is connected to this @see:GroupPanel.
        */
        constructor(element: any, grid: wijmo.grid.FlexGrid & _IFlexGridMvc) {
            super(element);
            this.grid = grid;
        }

        /**
         * Initializes the @see:GroupPanel by copying the properties from a given object.
         *
         * This method allows you to initialize controls using plain data objects
         * instead of setting the value of each property in code.
         *
         * @param options Object that contains the initialization data.
         */
        initialize(options: any) {
            if (options) {

                if (options["selector"]) {
                    delete options["selector"];
                }

                if (options["filter"]) {
                    var filterOptions = options["filter"];
                    options["filter"] = new c1.mvc.grid.filter.FlexGridFilter(<wijmo.grid.FlexGrid & _IFlexGridMvc>this.grid);
                    options["filter"].initialize(filterOptions);
                }

                wijmo.copy(this, options);
            }
        }
    }
}