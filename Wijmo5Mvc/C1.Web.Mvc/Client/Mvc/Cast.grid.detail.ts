﻿module wijmo.grid.detail.DetailMergeManager {
    /**
     * Casts the specified object to @see:wijmo.grid.detail.DetailMergeManager type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DetailMergeManager {
        return obj;
    }
}

module wijmo.grid.detail.DetailRow {
    /**
     * Casts the specified object to @see:wijmo.grid.detail.DetailRow type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DetailRow {
        return obj;
    }
}

module wijmo.grid.detail.FlexGridDetailProvider {
    /**
     * Casts the specified object to @see:wijmo.grid.detail.FlexGridDetailProvider type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexGridDetailProvider {
        return obj;
    }
}

