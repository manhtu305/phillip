﻿/// <reference path="../Shared/Grid.Selector.ts" />
/// <reference path="../Shared/Grid.ts" />

module c1.mvc.grid.selector {

    /**
     * The @see:Selector extender extends from @see:c1.grid.selector.Selector.
    */
    export class Selector extends c1.grid.selector.Selector {

        _flexGrid: wijmo.grid.FlexGrid & _IFlexGridMvc;
       
        /**
         * Initializes a new instance of the @see:Selector.
         *
         * @param grid The {@link FlexGrid} that this {@link Selector} should customize,
         */
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc, options?: any) {
            super(grid);
            this._flexGrid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);
        }
                
        /**
         * Initializes the @see:Selector by copying the properties from a given object.
         *
         * This method allows you to initialize controls using plain data objects
         * instead of setting the value of each property in code.
         *
         * @param options Object that contains the initialization data.
         */
        initialize(options: any) {
            var self = this;
            if (!options) {
                return;
            }

            //For column selector
            let columnName = options["column"];
            if (columnName) {
                let column = self._flexGrid.getColumn(columnName);
                if (column) {
                    self.column = column;
                }
                delete options["column"];
            }
            //Other options
            wijmo.copy(self, options);
        }
    }

    /**
     * The @see:BooleanChecker extender extends from @see:c1.grid.selector.BooleanChecker.
    */
    export class BooleanChecker extends c1.grid.selector.BooleanChecker {

        _flexGrid: wijmo.grid.FlexGrid & _IFlexGridMvc;
        _disableServerRead: boolean;

        /**
         * Initializes a new instance of the @see:BooleanChecker.
         *
         * @param grid The {@link FlexGrid} that this {@link BooleanChecker} should customize,
         */
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc, options?: any) {
            super();
            this._flexGrid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);
            _overrideMethod(this, '_setRangeChecked',
                this._beforeSetRangeChecked.bind(this),
                this._afterSetRangeChecked.bind(this));
        }

        /**
         * Initializes the @see:BooleanChecker by copying the properties from a given object.
         *
         * This method allows you to initialize controls using plain data objects
         * instead of setting the value of each property in code.
         *
         * @param options Object that contains the initialization data.
         */
        initialize(options: any) {
            var self = this;
            if (!options) {
                return;
            }

            //For boolean column which bounds BooleanChecker
            let columnName = options["column"];
            if (columnName) {
                let column = self._flexGrid.getColumn(columnName);
                if (column) {
                    self.column = column;
                }
                delete options["column"];
            }
            //Other options
            wijmo.copy(self, options);
        }

        _beforeSetRangeChecked() {
            let ecv = this._isBound ? this._flexGrid['editableCollectionView'] : null;
            if (ecv) {
                this._disableServerRead = ecv['_disableServerRead']
                ecv['_disableServerRead'] = true;
            }
        }
        _afterSetRangeChecked() {
            let ecv = this._isBound ? this._flexGrid['editableCollectionView'] : null;
            if (ecv) {
                ecv['_disableServerRead'] = this._disableServerRead;
            }
        }
    }
}