﻿module wijmo.chart.FlexChartCore {
    /**
     * Casts the specified object to @see:wijmo.chart.FlexChartCore type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexChartCore {
        return obj;
    }
}

module wijmo.chart.FlexChartBase {
    /**
     * Casts the specified object to @see:wijmo.chart.FlexChartBase type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexChartBase {
        return obj;
    }
}

module wijmo.chart.FlexChart {
    /**
     * Casts the specified object to @see:wijmo.chart.FlexChart type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexChart {
        return obj;
    }
}

module wijmo.chart.Axis {
    /**
     * Casts the specified object to @see:wijmo.chart.Axis type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Axis {
        return obj;
    }
}

module wijmo.chart.DataLabelBase {
    /**
     * Casts the specified object to @see:wijmo.chart.DataLabelBase type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DataLabelBase {
        return obj;
    }
}

module wijmo.chart.DataLabel {
    /**
     * Casts the specified object to @see:wijmo.chart.DataLabel type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DataLabel {
        return obj;
    }
}

module wijmo.chart.PieDataLabel {
    /**
     * Casts the specified object to @see:wijmo.chart.PieDataLabel type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PieDataLabel {
        return obj;
    }
}

module wijmo.chart.ChartTooltip {
    /**
     * Casts the specified object to @see:wijmo.chart.ChartTooltip type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ChartTooltip {
        return obj;
    }
}

module wijmo.chart.FlexPie {
    /**
     * Casts the specified object to @see:wijmo.chart.FlexPie type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexPie {
        return obj;
    }
}

module wijmo.chart.HitTestInfo {
    /**
     * Casts the specified object to @see:wijmo.chart.HitTestInfo type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): HitTestInfo {
        return obj;
    }
}

module wijmo.chart.IRenderEngine {
    /**
     * Casts the specified object to @see:wijmo.chart.IRenderEngine type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): IRenderEngine {
        return obj;
    }
}

module wijmo.chart.Legend {
    /**
     * Casts the specified object to @see:wijmo.chart.Legend type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Legend {
        return obj;
    }
}

module wijmo.chart.LineMarker {
    /**
     * Casts the specified object to @see:wijmo.chart.LineMarker type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): LineMarker {
        return obj;
    }
}

module wijmo.chart.PlotArea {
    /**
     * Casts the specified object to @see:wijmo.chart.PlotArea type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PlotArea {
        return obj;
    }
}

module wijmo.chart.SeriesBase {
    /**
     * Casts the specified object to @see:wijmo.chart.SeriesBase type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): SeriesBase {
        return obj;
    }
}

module wijmo.chart.Series {
    /**
     * Casts the specified object to @see:wijmo.chart.Series type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Series {
        return obj;
    }
}

module wijmo.chart.SeriesEventArgs {
    /**
     * Casts the specified object to @see:wijmo.chart.SeriesEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): SeriesEventArgs {
        return obj;
    }
}

module wijmo.chart.RenderEventArgs {
    /**
     * Casts the specified object to @see:wijmo.chart.RenderEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): RenderEventArgs {
        return obj;
    }
}

module wijmo.chart.SeriesRenderingEventArgs {
    /**
     * Casts the specified object to @see:wijmo.chart.SeriesRenderingEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): SeriesRenderingEventArgs {
        return obj;
    }
}

module wijmo.chart.DataLabelRenderEventArgs {
    /**
     * Casts the specified object to @see:wijmo.chart.DataLabelRenderEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DataLabelRenderEventArgs {
        return obj;
    }
}

