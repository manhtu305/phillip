﻿/// <reference path="Collections.ts" />
/// <reference path="Grid.ts" />

module c1.mvc.grid {

    // The extension which provides virtual scrolling.
    export class _VirtualScrolling {
        private readonly _grid: wijmo.grid.FlexGrid & _IFlexGridMvc;
        private _collectionView: wijmo.collections.ICollectionView;
        private _remoteCV: c1.mvc.collections.RemoteCollectionView;

        // Initializes a new instance of the @see:_VirtualScrolling.
        // @param grid The @see:wijmo.grid.FlexGrid.
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc) {
            this._grid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);

            _overrideMethod(grid, _Initializer._INITIAL_NAME,
                null, this._afterInitialize.bind(this));

            _overrideMethod(grid, 'refresh',
                null, this._afterRefresh.bind(this));
        }

        private _afterInitialize(options) {
            if (!options) {
                return;
            }

            this._addHandlers();
            this._grid.itemsSourceChanged.addHandler(() => {
                this._removeHandlers();
                this._addHandlers();
            });
        }

        private _addHandlers() {
            this._collectionView = this._grid.collectionView;
            this._remoteCV = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(this._collectionView, c1.mvc.collections.RemoteCollectionView);

            if (DataSourceManager._isDynamicalLoading(this._collectionView)) {
                this._grid.scrollPositionChanged.addHandler(this._scrollingToCurrentView, this);

                if (this._remoteCV) {
                    this._grid.draggingRow.addHandler(this._draggingRow, this);
                    this._grid.draggedRow.addHandler(this._draggedRow, this);
                }
            }
        }

        private _removeHandlers() {
            if (DataSourceManager._isDynamicalLoading(this._collectionView)) {
                this._grid.scrollPositionChanged.removeHandler(this._scrollingToCurrentView, this);

                if (this._remoteCV) {
                    this._grid.draggingRow.removeHandler(this._draggingRow, this);
                    this._grid.draggedRow.removeHandler(this._draggedRow, this);
                }
            }
        }

        private _afterRefresh() {
            var cellRange: wijmo.grid.CellRange;

            // When collectview works in dynamical loading mode,
            // Except scrolling, here is the time for sending the request by grid.
            if (DataSourceManager._isDynamicalLoading(this._grid.collectionView)) {
                this._scrollingToCurrentView();
            }
        }

        private _draggingRow(s, e: wijmo.grid.CellRangeEventArgs) {
            if (!this._remoteCV) return;

            this._remoteCV._clearReservedItems();

            if (e.row >= 0 && e.row < this._grid.rows.length) {
                var dataItem = this._grid.rows[e.row].dataItem;
                if (dataItem) {
                    var index = this._remoteCV.sourceCollection.indexOf(dataItem);
                    if (index > -1) {
                        this._remoteCV._addReservedItem(index);
                    }
                }
            }
        }

        private _draggedRow(s, e) {
            if (!this._remoteCV) return;

            this._remoteCV._clearReservedItems();
        }

        private _scrollingToCurrentView() {
            var viewRange = this._grid.viewRange;
            this._setWindow(viewRange.row, viewRange.row2);
        }

        private _prepareRequest(rc: c1.mvc.collections.RemoteCollectionView, startRow: number, endRow: number) {
            var g = this._grid,
                currentViewRange: wijmo.grid.CellRange;

            if (!rc) {
                return;
            }

            window.setTimeout(() => {
                currentViewRange = g.viewRange;
                if (currentViewRange.row == startRow && currentViewRange.row2 == endRow) {
                    this._requestItems(rc, startRow, endRow);
                } else {
                    this._prepareRequest(rc, currentViewRange.row, currentViewRange.row2);
                }
            }, 100);
        }

        private _setWindow(startRow: number, endRow: number) {
            var rc = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(this._grid.collectionView, c1.mvc.collections.RemoteCollectionView);
            if (rc) {
                this._prepareRequest(rc, startRow, endRow);
                return;
            }

            var odvs = <wijmo.odata.ODataVirtualCollectionView>wijmo.tryCast(this._grid.collectionView, wijmo.odata.ODataCollectionView);
            if (odvs) {
                odvs.setWindow(startRow, endRow);
            }
        }

        private _requestItems(cv: c1.mvc.collections.RemoteCollectionView, startRow: number, endRow: number) {
            var rowsPerItem = this._grid._getRowsPerItemMvc(),
                startIndex = Math.floor(startRow / rowsPerItem),
                endIndex = Math.ceil(endRow / rowsPerItem);
            cv.requestItems(startIndex, endIndex);
        }
    }
}