﻿/// <reference path="Util.ts" />
/// <reference path="../Shared/Control.ts" />

/**
 * Defines C1 MVC controls, related functions and classes.
 */
module c1.mvc {

    var _OPTIONS_NAME = '_c1MvcOptions',
        _UNIQUE_ID_NAME = '_c1MvcUniqueId';

    export function _getOptions(control: Object): any {
        return control[_OPTIONS_NAME];
    }

    export function _setOptions(control: Object, options: any): void {
        control[_OPTIONS_NAME] = options;
    }

    export function _getUniqueId(control: Object): string {
        return control[_UNIQUE_ID_NAME];
    }

    export function _setUniqueId(control: Object, uniqueId: string): void {
        control[_UNIQUE_ID_NAME] = uniqueId;
    }

    export function _overrideMethod(control: Object, methodName: string, preFunc?: Function, postFunc?: Function, thisArg?) {
        var originalMethod: Function = control[methodName];
        control[methodName] = function () {
            var args = arguments;

            if (preFunc) {
                preFunc.apply(thisArg, args);
            }

            var result;
            if (originalMethod) {
                result = originalMethod.apply(control, args);
            }

            if (postFunc) {
                postFunc.apply(thisArg, args);
            }

            return result;
        };
    }

    export class _ControlWrapper extends c1._ControlWrapper {
        get _initializerType(): any {
            return _Initializer;
        }
    }

    export class _Initializer extends c1._Initializer {

        static _INITIAL_NAME = 'initialize';
        
        _beforeInitializeControl(options): void {
        }

        _afterInitializeControl(options): void {
        }

        _override(): void {
            var self = this, control = self.control;

            super._override();
            _overrideMethod(control, _Initializer._INITIAL_NAME,
                (options: any, templateScope?: any) => {
                    _Initializer.bindTemplateScope(options, templateScope);
                    _setOptions(control, options);
                    _setUniqueId(control, options.uniqueId);
                    delete options.uniqueId;
                    self._beforeInitializeControl(options);
                }, (options: any) => {
                    self._afterInitializeControl(options);
                });
        }

        static bindTemplateScope(options: any, templateScope: any): any {
            if (!options || !options.templateBindings || !templateScope) {
                return options;
            }

            for (var key in options.templateBindings) {
                options[key] = Template.getPropertyValue(options.templateBindings[key], templateScope);
            }
            delete options.templateBindings;
            return options;
        }
    }

    export class _SourceInitializer extends _Initializer {
        _beforeInitializeControl(options): void {
            super._beforeInitializeControl(options);
            if (options) {
                this._initEventsOpts(options, ['reponseTextParsing', 'requestDataStringifying']);
                if (wijmo.isString(options.itemsSource)) {
                    options.itemsSource = Utils.jsonParse(<string>options.itemsSource, this._onReponseTextParsing.bind(this));
                }
            }
        }

        private _initEventsOpts(options: any, events: string[]) {
            var self = this;
            events.forEach((ev, index, arr) => {
                var value = options[ev];
                if (self.control[ev] instanceof wijmo.Event
                    && value && wijmo.isFunction(value)) {
                    (<wijmo.Event>self.control[ev]).addHandler(value);
                }
                delete options[ev];
            });
        }

        private _onReponseTextParsing(e: JSONOperationEventArgs): boolean {
            var reponseTextParsing = <wijmo.Event>this.control['reponseTextParsing'];
            if (reponseTextParsing && reponseTextParsing instanceof wijmo.Event) {
                reponseTextParsing.raise(this.control, e);
                return e.cancel;
            }
        }

        private _onRequestDataStringifying(e: JSONOperationEventArgs): boolean {
            var requestDataStringifying = <wijmo.Event>this.control['requestDataStringifying'];
            if (requestDataStringifying && requestDataStringifying instanceof wijmo.Event) {
                requestDataStringifying.raise(this.control, e);
                return e.cancel;
            }
        }

        _ajax(setting: any) {
            var ajaxSetting = <IAjaxSettings>setting;
            ajaxSetting.requestDataStringifying = this._onRequestDataStringifying.bind(this);
            ajaxSetting.responseTextParsing = this._onReponseTextParsing.bind(this);
            return c1.mvc.Utils.ajax(ajaxSetting);
        }
    }
}