﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.xlsx.d.ts" />
/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.xlsx.d.ts" />
module wijmo.grid.xlsx.XlsxFormatItemEventArgs {
    /**
     * Casts the specified object to @see:wijmo.grid.xlsx.XlsxFormatItemEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): XlsxFormatItemEventArgs {
        return obj;
    }
}

