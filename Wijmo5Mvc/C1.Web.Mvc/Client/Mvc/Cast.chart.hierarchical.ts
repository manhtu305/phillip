﻿module wijmo.chart.hierarchical.Sunburst {
    /**
     * Casts the specified object to @see:wijmo.chart.hierarchical.Sunburst type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Sunburst {
        return obj;
    }
}

module wijmo.chart.hierarchical.TreeMap {
    /**
     * Casts the specified object to @see:wijmo.chart.hierarchical.TreeMap type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TreeMap {
        return obj;
    }
}

