﻿module wijmo.gauge.BulletGraph {
    /**
     * Casts the specified object to @see:wijmo.gauge.BulletGraph type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): BulletGraph {
        return obj;
    }
}

module wijmo.gauge.Gauge {
    /**
     * Casts the specified object to @see:wijmo.gauge.Gauge type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Gauge {
        return obj;
    }
}

module wijmo.gauge.LinearGauge {
    /**
     * Casts the specified object to @see:wijmo.gauge.LinearGauge type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): LinearGauge {
        return obj;
    }
}

module wijmo.gauge.RadialGauge {
    /**
     * Casts the specified object to @see:wijmo.gauge.RadialGauge type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): RadialGauge {
        return obj;
    }
}

module wijmo.gauge.Range {
    /**
     * Casts the specified object to @see:wijmo.gauge.Range type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Range {
        return obj;
    }
}

