﻿module wijmo.input.AutoComplete {
    /**
     * Casts the specified object to @see:wijmo.input.AutoComplete type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): AutoComplete {
        return obj;
    }
}

module wijmo.input.Calendar {
    /**
     * Casts the specified object to @see:wijmo.input.Calendar type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Calendar {
        return obj;
    }
}

module wijmo.input.ColorPicker {
    /**
     * Casts the specified object to @see:wijmo.input.ColorPicker type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ColorPicker {
        return obj;
    }
}

module wijmo.input.ComboBox {
    /**
     * Casts the specified object to @see:wijmo.input.ComboBox type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ComboBox {
        return obj;
    }
}

module wijmo.input.DropDown {
    /**
     * Casts the specified object to @see:wijmo.input.DropDown type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DropDown {
        return obj;
    }
}

module wijmo.input.InputColor {
    /**
     * Casts the specified object to @see:wijmo.input.InputColor type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): InputColor {
        return obj;
    }
}

module wijmo.input.InputDate {
    /**
     * Casts the specified object to @see:wijmo.input.InputDate type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): InputDate {
        return obj;
    }
}

module wijmo.input.InputDateTime {
    /**
     * Casts the specified object to @see:wijmo.input.InputDateTime type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): InputDateTime {
        return obj;
    }
}

module wijmo.input.InputMask {
    /**
     * Casts the specified object to @see:wijmo.input.InputMask type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): InputMask {
        return obj;
    }
}

module wijmo.input.InputNumber {
    /**
     * Casts the specified object to @see:wijmo.input.InputNumber type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): InputNumber {
        return obj;
    }
}

module wijmo.input.InputTime {
    /**
     * Casts the specified object to @see:wijmo.input.InputTime type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): InputTime {
        return obj;
    }
}

module wijmo.input.ListBox {
    /**
     * Casts the specified object to @see:wijmo.input.ListBox type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ListBox {
        return obj;
    }
}

module wijmo.input.Menu {
    /**
     * Casts the specified object to @see:wijmo.input.Menu type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Menu {
        return obj;
    }
}

module wijmo.input.MultiAutoComplete {
    /**
     * Casts the specified object to @see:wijmo.input.MultiAutoComplete type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): MultiAutoComplete {
        return obj;
    }
}

module wijmo.input.MultiSelect {
    /**
     * Casts the specified object to @see:wijmo.input.MultiSelect type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): MultiSelect {
        return obj;
    }
}

module wijmo.input.Popup {
    /**
     * Casts the specified object to @see:wijmo.input.Popup type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Popup {
        return obj;
    }
}

module wijmo.input.FormatItemEventArgs {
    /**
     * Casts the specified object to @see:wijmo.input.FormatItemEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FormatItemEventArgs {
        return obj;
    }
}

