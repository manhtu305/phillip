﻿/// <reference path="../Shared/Grid.Sheet.FormulaBar.ts" /> 

module c1.mvc.grid.sheet {
    /**
     * Define the FormulaBar class.
     */
    export class FormulaBar extends c1.grid.sheet.FormulaBar {
        /**
         * Casts the specified object to @see:c1.mvc.grid.sheet.FormulaBar type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): FormulaBar {
            return obj;
        }
    }
}