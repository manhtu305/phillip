﻿/// <reference path="Control.ts" />
/// <reference path="Chart.ts" />
/// <reference path="../Shared/Chart.ts" />

/**
 * Defines the @see:c1.mvc.chart.finance.FinancialChart control and associated classes.
 */
module c1.mvc.chart.finance {

    export class _Initializer extends _FlexChartCoreInitializer {
        _prepareExtraOptions(options: any) {
            var extraOpts = options["options"],
                newExtraOpts = {};

            if (!extraOpts) {
                return;
            }

            newExtraOpts["kagi"] = { fields: extraOpts.kagiFields, rangeMode: extraOpts.kagiRangeMode, reversalAmount: extraOpts.kagiReversalAmount };
            newExtraOpts["lineBreak"] = { newLineBreaks: extraOpts.lineBreakNewLineBreaks };
            newExtraOpts["renko"] = { fields: extraOpts.renkoFields, rangeMode: extraOpts.renkoRangeMode, boxSize: extraOpts.renkoBoxSize };
            newExtraOpts["pointAndFigure"] = { boxSize: extraOpts.pointAndFigureBoxSize, reversal: extraOpts.pointAndFigureReversal, period: extraOpts.pointAndFigurePeriod, fields: extraOpts.pointAndFigureFields, scaling: extraOpts.pointAndFigureScaling };

            options["options"] = newExtraOpts;
        }
    }

    export class _FinancialChartWrapper extends _FlexChartCoreWrapper {
        get _initializerType(): any {
            return _Initializer;
        }

        get _controlType(): any {
            return FinancialChart;
        }
    }

    /**
     * Defines the C1 MVC Financial charting control.
     */
    export class FinancialChart extends c1.chart.finance.FinancialChart {
    }
}