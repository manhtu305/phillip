﻿module wijmo.grid.multirow.MultiRow {
    /**
     * Casts the specified object to @see:wijmo.grid.multirow.MultiRow type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): MultiRow {
        return obj;
    }
}

