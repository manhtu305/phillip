﻿module wijmo.olap.PivotEngine {
    /**
     * Casts the specified object to @see:wijmo.olap.PivotEngine type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PivotEngine {
        return obj;
    }
}

module wijmo.olap.PivotField {
    /**
     * Casts the specified object to @see:wijmo.olap.PivotField type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PivotField {
        return obj;
    }
}

module wijmo.olap.CubePivotField {
    /**
     * Casts the specified object to @see:wijmo.olap.CubePivotField type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): CubePivotField {
        return obj;
    }
}

module wijmo.olap.PivotFilter {
    /**
     * Casts the specified object to @see:wijmo.olap.PivotFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PivotFilter {
        return obj;
    }
}

module wijmo.olap.PivotChart {
    /**
     * Casts the specified object to @see:wijmo.olap.PivotChart type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PivotChart {
        return obj;
    }
}

module wijmo.olap.PivotGrid {
    /**
     * Casts the specified object to @see:wijmo.olap.PivotGrid type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PivotGrid {
        return obj;
    }
}

module wijmo.olap.PivotPanel {
    /**
     * Casts the specified object to @see:wijmo.olap.PivotPanel type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PivotPanel {
        return obj;
    }
}

module wijmo.olap.DetailDialog {
    /**
     * Casts the specified object to @see:wijmo.olap.DetailDialog type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DetailDialog {
        return obj;
    }
}

module wijmo.olap.PivotCollectionView {
    /**
     * Casts the specified object to @see:wijmo.olap.PivotCollectionView type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PivotCollectionView {
        return obj;
    }
}

module wijmo.olap.PivotFieldEditor {
    /**
     * Casts the specified object to @see:wijmo.olap.PivotFieldEditor type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PivotFieldEditor {
        return obj;
    }
}

module wijmo.olap.ProgressEventArgs {
    /**
     * Casts the specified object to @see:wijmo.olap.ProgressEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ProgressEventArgs {
        return obj;
    }
}

