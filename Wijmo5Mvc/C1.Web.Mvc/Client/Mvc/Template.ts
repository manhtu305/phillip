﻿/// <reference path="Util.ts"/>

/*
 * ComponentOne ASP.NET MVC
 * Template.js
 */
module c1.mvc {

    /**
     * Define the Template class.
     * @ignore
     */
    export class Template {
        _content: string;
        _innerTemplatesContents: any[];

        // save the symbol list found in the template content 
        _symbolList: any = {};

        // Divide the content into two kinds, one is the script tag with script codes. The other is all the left.
        // It will be saved into an array in the order of the occurance in the content.
        _sections: any[] = []; /*the item of the collection would be {isScriptCode: false, data: codes/markup}*/

        // All the script tags are saved into an array in the order of the occurance in the content.
        _scripts: any[] = [];  /*the item of the collection would be {isSrc: false, data: url/codes}*/

        // the count of the script tags which have script codes.
        _scriptSnippetsCount: number = 0;

        static DATACONTEXT: string = 'dataContext';
        static UID: string = 'uid';
        private static TEMPLATE_PREFIX: string = '_c1Template_';
        private static C1_TEMPLATE_TAG: string = 'C1TemplateContent';
        private static C1_TEMPLATE_START_TAG: string = '<' + Template.C1_TEMPLATE_TAG + '>';
        private static C1_TEMPLATE_END_TAG: string = '</' + Template.C1_TEMPLATE_TAG + '>';
        private static C1_INNER_TEMPLATE_PLACEHOLDER: string = 'C1_INNER_TEMPLATE_PLACEHOLDER';

        private static ALL_SYMBOL = /{{\s*?([^\|\s\}]+?)\s*?(?:\|\s*?(\S+?))?\s*?}}/g;
        private static ONE_SYMBOL = /{{\s*?([^\|\s\}]+?)\s*?(?:\|\s*?(\S+?))?\s*?}}/;
        private static COMPLETE_SCRIPT = /<script(?:\s+[^>]+)*?\s+src\s*?=\s*?[\'\"]([^\'|\"]+?)[\'\"]\/>|(<script(?:\s+[^>]+)*?(?:\s+src\s*?=\s*?[\'\"]([^\'|\"]+?)[\'\"])?[^>]*?>)([\s\S]*?)(<\/script>)/;

        private static reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?):(\d{3})Z$/; /* the regex for the date text */
        private static C1_INNER_TEMPLATE_PLACEHOLDER_REGEXP = new RegExp('<' + Template.C1_INNER_TEMPLATE_PLACEHOLDER + ' index=(\\d+) />', 'g');

        /**
         * Initializes a new instance of a @see:Template.
         * 
         * @param content the value could be id or some content.
         */
        constructor(para: any) {
            var content: string,
                isIdSet: boolean = false;

            if (para == null) {
                return;
            }
            var paraType = Utils.getType(para);
            if (paraType === 'array') {
                content = para[0];
                isIdSet = !!para[1];
            } else if (paraType === 'string') {
                content = para;
            } else {
                throw 'Invalid parameter type.';
            }

            if (isIdSet) {
                var ele = document.getElementById(content);
                if (ele != null) {
                    content = ele.innerHTML;
                }
            } else {
                content = Template._unwrap(content);
            }

            if (Utils.isEmptyContent(content)) {
                return;
            }

            this._initContents(content);
        }

        private static _unwrap(content: string): string {
            var text = content,
                start = content.indexOf(Template.C1_TEMPLATE_START_TAG),
                end = content.lastIndexOf(Template.C1_TEMPLATE_END_TAG);
            if (start != -1 && end != -1) {
                text = content.substring(start + Template.C1_TEMPLATE_START_TAG.length, end);
            }
            return text;
        }

        private _initContents(content: string) {
            var tempContent = content,
                startTagLength = Template.C1_TEMPLATE_START_TAG.length,
                endTagLength = Template.C1_TEMPLATE_END_TAG.length,
                startIndex = 0, index = 0;
            this._innerTemplatesContents = [];
            this._content = '';
            while (tempContent.length) {
                var innerTemplate = Template._getFirstInnerTemplatePosition(tempContent);
                if (!innerTemplate) {
                    this._content += tempContent;
                    return;
                }

                this._content += tempContent.substring(0, innerTemplate.startIndex);
                this._content += '<' + Template.C1_INNER_TEMPLATE_PLACEHOLDER + ' index=' + index.toString() + ' />';
                this._innerTemplatesContents.push(tempContent.substring(innerTemplate.startIndex + startTagLength, innerTemplate.endIndex));
                startIndex = innerTemplate.endIndex + endTagLength;
                tempContent = tempContent.substr(startIndex);
                index++;
            }
        }

        private static _getFirstInnerTemplatePosition(content: string) {
            var start = content.indexOf(Template.C1_TEMPLATE_START_TAG),
                end = content.indexOf(Template.C1_TEMPLATE_END_TAG),
                startTagLength = Template.C1_TEMPLATE_START_TAG.length,
                endTagLength = Template.C1_TEMPLATE_END_TAG.length,
                index: number, tStart: number;

            if (start == -1 || end == -1) {
                return;
            }

            tStart = start + startTagLength;
            do {
                var temp = content.substring(tStart, end);
                index = temp.indexOf(Template.C1_TEMPLATE_START_TAG);
                if (index == -1) {
                    return { startIndex: start, endIndex: end };
                }
                tStart = end + endTagLength;
                end = content.indexOf(Template.C1_TEMPLATE_END_TAG, tStart);
            } while (end == -1)
        }

        /**
         * Apply the template with the data.
         * @param data the context data applied in the template.
         */
        apply(data: any): string {
            var i: number, name: string;
            data = Template._resolveData(data);
            if (!this._sections.length) {
                this._initialize();
            }

            if (this._scriptSnippetsCount > 0) {
                name = Template.getGlobalContextName(data[Template.UID]);
                window[name] = {};
                for (i = 0; i < this._scriptSnippetsCount; i++) {
                    window[name]['n' + i.toString()] = data[Template.DATACONTEXT];
                }
            }

            return this._replaceSymbols(data);
        }

        /**
         * Apply the template with the data and append the content to the specified element.
         * @param element the specified html dom element.
         * @param data the context data applied in the template.
         */
        applyTo(element: HTMLElement, data: any) {
            var i: number,
                scriptItem: any,
                scriptCode: string;
            data = Template._resolveData(data);
            element.innerHTML = this.apply(data);
            for (i = 0; i < this._scripts.length; i++) {
                scriptItem = this._scripts[i];
                if (scriptItem.isSrc) {
                    Template.excuteScriptSrc(scriptItem.data);
                } else {
                    scriptCode = this._replaceSectionSymbols(data, scriptItem.data, true);
                    Template.excuteScriptSnippets(scriptCode);
                }
            }
        }

        private static _resolveData(data: any): any {
            data = data || {};
            if (data[Template.UID] == null) {
                data[Template.UID] = '_' + new Date().getTime();
            }
            if (!data[Template.DATACONTEXT]) {
                data[Template.DATACONTEXT] = {};
            }
            return data;
        }

        private static getGlobalContextName(name: string): string {
            if (name == null) {
                name = '';
            }
            return Template.TEMPLATE_PREFIX + name;
        }

        private _initialize() {
            this._content = this._content.replace(/<\\\/script>/g, '</script>');
            this._parseSymbols();
            this._parseContent();
        }

        private _parseSymbols() {
            var symbols: any = this._symbolList;

            if (Utils.isEmptyContent(this._content)) {
                return;
            }

            var matches = this._content.match(Template.ALL_SYMBOL);
            if (matches != null && matches.length) {
                for (var i = 0; i < matches.length; i++) {
                    var templateSymbol = Template.getSymbol(matches[i]);
                    if (templateSymbol && typeof (symbols[templateSymbol.key]) === 'undefined') {
                        symbols[templateSymbol.key] = {
                            name: templateSymbol.name,
                            format: templateSymbol.format
                        };
                    }
                }
            }

            // Ensure uid should be added in the symbol list.
            var tmpStr = Template.getSymbolRegStr(Template.UID);
            if (typeof (symbols[tmpStr]) === 'undefined') {
                symbols[tmpStr] = {
                    name: Template.UID
                };
            }
        }

        private _parseContent() {
            var content = this._content,
                index = 0;

            if (Utils.isEmptyContent(content)) {
                return;
            }

            var firstScript = Template.COMPLETE_SCRIPT.exec(content);
            while (firstScript != null) {
                this._updateContentSections(false, firstScript.input.substring(0, firstScript.index));
                // judge whether <script *** src="..." *** /> is matched. 
                var isJSSrcEmpty1 = Utils.isEmptyContent(firstScript[1]);
                // judge whether <script *** src="..." *** ></script> is matched.
                var isJSSrcEmpty2 = Utils.isEmptyContent(firstScript[3]);

                // process the javascript tags which have the attribute Src.
                if (!isJSSrcEmpty1 || !isJSSrcEmpty2) {
                    var nonEmptyString = firstScript[3];
                    if (!isJSSrcEmpty1) {
                        nonEmptyString = firstScript[1];
                    }

                    this._scripts.push({ isSrc: true, data: nonEmptyString });
                    this._updateContentSections(false, firstScript[0]);
                } else if (!Utils.isEmptyContent(firstScript[4])) {
                    // process the javascript tags which have script codes as the content of the tag.
                    var contentSection = firstScript[2];
                    var scriptCode = Template.processScriptCode(firstScript[4], index);
                    this._scripts.push({ isSrc: false, data: scriptCode });
                    contentSection += scriptCode;
                    contentSection += firstScript[5];
                    this._updateContentSections(true, contentSection);
                    index = index + 1;
                } else {
                    this._updateContentSections(false, firstScript[0]);
                }
                content = firstScript.input.substr(firstScript.index + firstScript[0].length);
                firstScript = Template.COMPLETE_SCRIPT.exec(content);
            }
            this._updateContentSections(false, content);
            this._scriptSnippetsCount = index;
        }

        private _replaceSymbols(data: any): string {
            var section: any,
                updateContent: string = '';

            for (var i = 0; i < this._sections.length; i++) {
                section = this._sections[i];
                updateContent += this._replaceSectionSymbols(data, section.data, section.isScriptCode);
            }

            return updateContent;
        }

        private _updateContentSections(isScriptCode: boolean, content: string) {
            var count: number;

            if (Utils.isEmptyContent(content)) {
                return;
            }

            count = this._sections.length;

            if (!count
                || (!!this._sections[count - 1].isScriptCode === !!!isScriptCode)) {
                this._sections.push({ isScriptCode: isScriptCode, data: content });
            } else {
                this._sections[count - 1].data += content;
            }
        }

        private static excuteScriptSrc(url: string) {
            return Utils.ajax({
                url: url,
                type: 'GET',
                dataType: 'script',
                async: false
            });
        }

        private static excuteScriptSnippets(scriptCode: string) {
            (window['execScript'] || function (scriptCode) {
                window['eval'].call(window, scriptCode);
            })(scriptCode);
        }

        private static getSymbol(text: string): any {
            var result: RegExpExecArray = Template.ONE_SYMBOL.exec(text);
            if (result != null && result.length && result[1] != null) {
                return {
                    key: Template.getRegexText(Template.getSymbolRegStr(result[1], result[2])),
                    name: result[1],
                    format: result[2]
                };
            }
        }

        // convert to the regex text
        private static getRegexText(text: string) {
            return text.replace(/\./g, '\\.').replace(/\[/g, '\\[');
        }

        private static getSymbolRegStr(name: string, format?: string) {
            var pattStr: string;
            if (Utils.isEmptyContent(name)) {
                return pattStr;
            }
            pattStr = '{{\\s*?' + name + '\\s*?';
            if (format != null && !format.length) {
                pattStr += '\\|\\s*?' + format + '\\s*?';
            }
            pattStr += '}}';
            return pattStr;
        }

        private _replaceSectionSymbols(data: any, sectionContent: string, isScript:boolean = false): string {
            var newContent: string = sectionContent;

            if (Utils.isEmptyContent(sectionContent)) {
                return;
            }

            for (var rgStr in this._symbolList) {
                var replacingText: string,
                    patt = new RegExp(rgStr, 'g'),
                    value = Template.getValue(this._symbolList[rgStr].name, data);

                if (this._symbolList[rgStr].name === Template.UID) {
                    replacingText = String(value);
                } else {
                    replacingText = Template.getFormattedText(value, this._symbolList[rgStr].format, isScript);
                }

                newContent = newContent.replace(patt, replacingText);
                newContent = newContent.replace(Template.C1_INNER_TEMPLATE_PLACEHOLDER_REGEXP, (match, p1) => this._innerTemplatesContents[p1]);
            }
            return newContent;
        }

        private static getFormattedText(value: any, format: string, isScript: boolean = false): string {
            var formattedValue: any;

            if (!Utils.isEmptyContent(format)) {
                formattedValue = wijmo.Globalize.format(value, format);
            } else {
                formattedValue = value;
            }
            return Template.getJsonText(formattedValue, !isScript);
        }

        private static getJsonText(value: any, isJsonPattern: boolean = true): string {
            var text = Template.c1Stringify(value, isJsonPattern),
                vtype = Utils.getType(value);

            if (isJsonPattern && (value === null || typeof (value) === 'undefined')) {
                return '';
            }

            if (isJsonPattern && vtype === 'date'
                || vtype === 'string') {
                text = text.substr(1, text.length - 2);
                text = text.replace(/\\\\/g, "\\");
            }

            return text;
        }

        private static c1Stringify(value: any, isJson: boolean = true): string {
            return Utils.jsonStringify(value, null, (key, value) => {
                if (!isJson) {
                    var date = Utils.tryGetDate(this, key, value);
                    if (date) return 'new Date(\'' + date.toJSON() + '\')';
                }

                return value;
            });
        }

        private static getValue(key: string, data: any) {
            if (data == null || key == null || !key.length) {
                return;
            }

            if (typeof (data[key]) !== 'undefined') {
                return data[key];
            }

            if (data[Template.DATACONTEXT] == null) {
                return;
            }

            return Template.getPropertyValue(key, data[Template.DATACONTEXT]);
        }

        // get the value of a property which name could include decimal points.
        // it is copied from wijmo.Binding.
        static getPropertyValue(key: string, data: any) {
            // handle case where property name has a decimal point
            if (key && key in data) {
                return data[key];
            }

            var parts: any[] = key ? key.split('.') : [],
                i = 0;
            for (i = 0; i < parts.length; i++) {
                var part = parts[i],
                    ib = part.indexOf('['); // e.g. 'customer.balance[0]'
                if (ib > -1) {
                    var ic = part.indexOf(']', ib);
                    if (ic > -1) {
                        parts[i] = part.substr(0, ib);
                        parts.splice(++i, 0, part.substr(ib + 1, ic));
                    }
                }
            }

            var value = data;
            for (i = 0; i < parts.length && value; i++) {
                value = value[parts[i]];
            }

            return value;
        }

        private static processScriptCode(scriptCode: string, index: number): string {
            var scopeName: string = 'window[\'' + Template.getGlobalContextName('{{' + Template.UID + '}}')
                + '\'][\'n' + index + '\']',
                newScript: string = '(function($data){'
                + scriptCode + ';'
                + 'delete ' + scopeName + ';'
                + '})(' + scopeName + ');';
            return newScript;
        }
    }
}