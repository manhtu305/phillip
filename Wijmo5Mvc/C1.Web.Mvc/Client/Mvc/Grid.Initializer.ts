﻿/// <reference path="Grid.ts" />
/// <reference path="Collections.ts" />

module c1.mvc.grid {

    // The initializer extension.
    export class _Initializer extends c1.mvc._Initializer {
        private readonly _grid: wijmo.grid.FlexGrid & _IFlexGridMvc;
        private _suspendedOpts: any;
        private _isInitialized: boolean;
        private static _SUSPENDED_OPTIONS = ['scrollPosition', 'selection'];
        private _showColumnFooters: boolean;
        private _columnFootersRowHeaderText: string = '\u03A3';

        // Initializes a new instance of the @see:_Initializer.
        // @param grid The @see:wijmo.grid.FlexGrid.
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc) {
            super(grid);
            this._grid = grid;

            // the grid updates the layout based on current size.
            // the grid's size may be changed on document ready.
            // force it refresh to updating again.
            c1.documentReady(() => {
                grid.invalidate();
            });

            _overrideMethod(grid, 'refresh',
                null, this._afterRefresh.bind(this));
        }

        private _afterRefresh() {
            // set scrollPosition and selection when the grid has finished rendering
            // Otherwise, these settings wouldn't take effect.

            if (this._suspendedOpts && this._grid.collectionView) {
                // when the collectionview is bound with itemsSource, the suspended options will be processed
                // when the collectionview is bound with some action url, the suspended options should be processed after first refresh(raised by the initialize method of grid)
                if (!DataSourceManager._isRemoteSource(this._grid.collectionView) || this._isInitialized) {
                    this._processSuspendedOpts();
                } else {
                    this._isInitialized = true;
                }
            }
        }

        _beforeInitializeControl(options): void {
            if (options) {
                this._filterOptions(options);
            }
        }

        _afterInitializeControl(options): void {
            // TFS 416145
            if (this._showColumnFooters && options.itemsSource) {
                this._grid.columnFooters.rows.push(new wijmo.grid.GroupRow());
                this._grid.bottomLeftCells.setCellData(0, 0, this._columnFootersRowHeaderText || '');
            }
        }
        
        private _filterOptions(options: any) {
            this._filterPinningOptions(options);
            this._filterSortOptions(options);
            this._filterItemsSourceOptions(options);
            this._filterDataMapOptions(options);
            this._filterSuspendedOpts(options);
            this._filterColumnFootersOpts(options);
            this._filterOtherOptions(options);
            
            //deprecate ShowAlternatingRow
            if (options) {
                delete options['showAlternatingRows'];
            }
        }

        //Filter options of some other simple properties here
        private _filterOtherOptions(options: any) {
            //Proccessing static property 'defaultTypeWidth'
            var dtw = options.defaultTypeWidth;
            if (dtw) {
                for (var key in dtw) {
                    FlexGrid._defTypeWidth[key] = dtw[key];
                }
                delete options.defaultTypeWidth;
            }
        }

        private _filterSortOptions(options: any) {
            // Processing change AllowSorting type from bool to enum
            if (options.sortingType >= 0) {
                options.allowSorting = options.sortingType
                delete options.sortingType;
            } else if (options.allowSorting == false) {
                options.allowSorting = wijmo.grid.AllowSorting.None;
            } else if (options.allowSorting == true) {
                options.allowSorting = wijmo.grid.AllowSorting.SingleColumn;
            }
        }

        private _filterPinningOptions(options: any) {
            // Processing change AllowPinning type from bool to enum
            if (options.pinningType >= 0) {
                options.allowPinning = options.pinningType
                delete options.pinningType;
            } else if (options.allowPinning == false) {
                options.allowPinning = wijmo.grid.AllowPinning.None;
            } else if (options.allowPinning == true) {
                options.allowPinning = wijmo.grid.AllowPinning.SingleColumn;
            }
        }

        private _filterItemsSourceOptions(options: any) {
            // when itemsSource option is just an array, convert it to an instance of c1.mvc.CallbackCollectionView.
            // leave the unique id to be null.
            // Such scenario only happens when the grid is used as a template and the itemsSource property is bound with some array.
            if (wijmo.tryCast(options.itemsSource, wijmo.collections.CollectionView) == null
                && wijmo.isArray(options.itemsSource)) {
                options.itemsSource = new c1.mvc.collections.CallbackCollectionView({ sourceCollection: options.itemsSource, uniqueId: null, disableServerRead: true });
            } else if (options.itemsSource == null) {
                delete options['itemsSource'];
            }
        }

        private _filterDataMapOptions(options: any) {
            var columns: Array<Object>, dataMapOptions, dataMap, mapChanged, sortByDisplayValues;

            columns = this._grid._getFlatColumnsMvc(options);
            if (columns && columns.length) {
                columns.forEach(col => {
                    dataMapOptions = col['dataMap'];
                    if (dataMapOptions) {
                        mapChanged = dataMapOptions['mapChanged'];
                        sortByDisplayValues = dataMapOptions['sortByDisplayValues'];
                        dataMap = new wijmo.grid.DataMap(dataMapOptions['itemsSource'],
                            dataMapOptions['selectedValuePath'], dataMapOptions['displayMemberPath']);
                        if (mapChanged) {
                            dataMap.mapChanged.addHandler(mapChanged);
                        }
                        // default value of sortByDisplayValues is false in mvc, but is true in Wijmo 5.
                        dataMap.sortByDisplayValues = sortByDisplayValues === true;

                        col['dataMap'] = dataMap;
                    }
                });
            }
        }

        // filter the special option settings from the options.
        private _filterSuspendedOpts(options: any) {
            this._suspendedOpts = null;
            _Initializer._SUSPENDED_OPTIONS.forEach(opt => this._filterSuspendedOpt(opt, options));
        }

        private _filterSuspendedOpt(key: string, options: any) {
            if (typeof (options[key]) !== 'undefined') {
                if (!this._suspendedOpts) {
                    this._suspendedOpts = {};
                }
                this._suspendedOpts[key] = options[key];
                delete options[key];
            }
        }

        private _filterColumnFootersOpts(options: any) {
            if (typeof (options.showColumnFooters) != 'undefined') {
                this._showColumnFooters = !!options.showColumnFooters;
                delete options.showColumnFooters;
            }

            if (typeof (options.columnFootersRowHeaderText) != 'undefined') {
                this._columnFootersRowHeaderText = options.columnFootersRowHeaderText;
                delete options.columnFootersRowHeaderText;
            }
        }

        // process the special option settings.
        private _processSuspendedOpts() {
            var grid = this._grid,
                key: string,
                value: any;

            if (this._suspendedOpts) {
                for (key in this._suspendedOpts) {
                    value = this._suspendedOpts[key];
                    switch (key) {
                        case 'scrollPosition':
                            if (value) {
                                grid.scrollIntoView(value.x || 0, value.y || 0);
                            } else {
                                grid[key] = new wijmo.Point();
                            }
                            break;
                        case 'selection':
                            if (value) {
                                grid[key] = new wijmo.grid.CellRange(value.row, value.col, value.row2, value.col2);
                            } else {
                                grid[key] = new wijmo.grid.CellRange();
                            }
                            break;
                    }
                }
                this._suspendedOpts = null;
            }
        }
    }
}