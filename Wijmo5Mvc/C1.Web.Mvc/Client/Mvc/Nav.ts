﻿/// <reference path="../Shared/Nav.ts" />

/**
 * Defines the navigation components and associated classes.
 */
module c1.mvc.nav {
    /**
     * The @see:TreeView extends from @see:c1.nav.TreeView.
     */
    export class TreeView extends c1.nav.TreeView {
        _isInitialized: boolean;

        refresh(fullUpdate = true) {
            // postpone the refresh if the control is not intialized.
            if (!this._isInitialized) return;

            super.refresh(fullUpdate);
        }

        /**
         * Occurs when parsing the response text.
         */
        requestDataStringifying = new wijmo.Event();

        /**
         * Occurs when parsing the response text.
         */
        reponseTextParsing = new wijmo.Event();
    }

    export class _TreeViewWrapper extends _ControlWrapper {
        get _controlType(): any {
            return TreeView;
        }

        get _initializerType(): any {
            return _TreeViewInitializer;
        }
    }

    export class _TreeViewInitializer extends _SourceInitializer {
        private _loadActionUrl: string;
        private _lazyLoadActionUrl: string;
        private _request: XMLHttpRequest;   // current http request for loading items.
        private readonly _treeView: c1.mvc.nav.TreeView;

        constructor(control: Object) {
            super(control);
            this._treeView = <c1.mvc.nav.TreeView>this.control;
        }

        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);
            if (options) {
                if (options.lazyLoadActionUrl) {
                    this._lazyLoadActionUrl = options.lazyLoadActionUrl;
                }
                delete options.lazyLoadActionUrl;

                if (options.loadActionUrl) {
                    this._loadActionUrl = options.loadActionUrl;
                }
                delete options.loadActionUrl;
            }
        }

        _afterInitializeControl(options: any): void {
            super._afterInitializeControl(options);
            this._init();

            var treeView = <TreeView>this.control;
            treeView._isInitialized = true;

            // refresh the control after initialize
            treeView.invalidate();
        }

        private _lazyLoad(node: wijmo.nav.TreeNode, callback: (data: any[]) => void) {
            this._load(this._lazyLoadActionUrl, node, callback);
        }

        private _load(url: string, node: wijmo.nav.TreeNode, callback: (data: any[]) => void) {
            // abort the current request which is not finished.
            if (this._request && this._request.readyState != 4) {
                this._request.abort();
                this._request = null;
            }

            // send a new request for lazy loading the child nodes.
            this._request = this._ajax({
                url: url,
                data: node && {
                    level: node.level,
                    isChecked: node.isChecked,
                    dataItem: node.dataItem
                },
                async: true,
                type: 'POST',
                dataType: 'json',
                postType: 'json',
                success: callback
            });
        }

        private _init() {
            if (this._loadActionUrl) {
                // load the nodes from url.
                this._load(this._loadActionUrl, null, (data: any[]) => {
                    this._treeView.itemsSource = data;
                });
            }

            // load the child nodes from url.
            if (this._lazyLoadActionUrl) {
                this._treeView.lazyLoadFunction = this._lazyLoad.bind(this);
            }
        }
    }

    /**
     * The @see:TabPanel extends from @see:c1.nav.TabPanel.
     */
    export class TabPanel extends c1.nav.TabPanel {
    }

    export class _TabPanelWrapper extends _ControlWrapper {
        get _controlType(): any {
            return TabPanel;
        }

        get _initializerType(): any {
            return _TabPanelInitializer;
        }
    }

    export class _TabPanelInitializer extends _Initializer {
        private _tabPanel: c1.mvc.nav.TabPanel;
        private _tabs: ITabOptions[];
        private _selectedIndex: number;
        private _selectedIndexChanged: wijmo.IEventHandler;

        constructor(control: Object) {
            super(control);
            this._tabPanel = <c1.mvc.nav.TabPanel>this.control;
        }

        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);
            if (options) {
                if (options.tabs != null) {
                    this._tabs = <ITabOptions[]>options.tabs;
                    delete options.tabs;
                }

                if (options.selectedIndex != null) {
                    this._selectedIndex = options.selectedIndex;
                    delete options.selectedIndex;
                }

                if (options.selectedIndexChanged != null) {
                    this._selectedIndexChanged = options.selectedIndexChanged;
                    delete options.selectedIndexChanged;
                }
            }
        }

        _afterInitializeControl(options: any): void {
            super._afterInitializeControl(options);
            this._init();
        }

        _init(): void{
            if (this._tabs && this._tabs.length) {
                this._tabPanel.tabs.deferUpdate(() => {
                    this._tabs.forEach(item => {
                        this._tabPanel.tabs.push(this._buildTab(item));
                    });
                });
            }

            if (this._selectedIndex != null) {
                this._tabPanel.selectedIndex = this._selectedIndex;
            }

            if (this._selectedIndexChanged != null) {
                this._tabPanel.selectedIndexChanged.addHandler(this._selectedIndexChanged);
            }
        }

        _buildTab(options: ITabOptions): wijmo.nav.Tab {
            var tab = new wijmo.nav.Tab(options.header, options.pane);
            if (options.isDisabled != null) {
                tab.isDisabled = options.isDisabled;
            }
            if (options.isVisible != null) {
                tab.isVisible = options.isVisible;
            }
            return tab;
        }
    }

    export interface ITabOptions {
        isDisabled?: boolean;
        isVisible?: boolean;
        header?: string;
        pane?: string;
    }

    /**
     * The @see:DashboardLayout extends from @see:c1.nav.DashboardLayout.
     */
    export class DashboardLayout extends c1.nav.DashboardLayout {
        /**
         * Casts the specified object to @see:c1.mvc.nav.DashboardLayout type.
         * @param obj The object to cast.
         * @return The object passed in.
        */
        static cast(obj: any): DashboardLayout {
            return obj;
        }
    }

    export class _DashboardLayoutWrapper extends _ControlWrapper {
        get _controlType(): any {
            return DashboardLayout;
        }
    }

    /**
 * The @see:FileManager extends from @see:c1.nav.FileManager.
 */
    export class FileManager extends c1.nav.FileManager {
        /**
         * Casts the specified object to @see:c1.mvc.nav.FileManager type.
         * @param obj The object to cast.
         * @return The object passed in.
        */
        static cast(obj: any): FileManager {
            return obj;
        }
    }

    export class _FileManagerWrapper extends _ControlWrapper {
        get _controlType(): any {
            return FileManager;
        }
    }

    /**
     * The @see:FlowLayout extends from @see:c1.nav.flow.FlowLayout.
     */
    export class FlowLayout extends c1.nav.flow.FlowLayout {
        /**
         * Casts the specified object to @see:c1.mvc.nav.FlowLayout type.
         * @param obj The object to cast.
         * @return The object passed in.
        */
        static cast(obj: any): FlowLayout {
            return obj;
        }
    }

    /**
     * The @see:AutoGridLayout extends from @see:c1.nav.grid.AutoGridLayout.
     */
    export class AutoGridLayout extends c1.nav.grid.AutoGridLayout {
        /**
         * Casts the specified object to @see:c1.mvc.nav.AutoGridLayout type.
         * @param obj The object to cast.
         * @return The object passed in.
        */
        static cast(obj: any): AutoGridLayout {
            return obj;
        }
    }

    /**
     * The @see:ManualGridLayout extends from @see:c1.nav.grid.ManualGridLayout.
     */
    export class ManualGridLayout extends c1.nav.grid.ManualGridLayout {
        /**
         * Casts the specified object to @see:c1.mvc.nav.ManualGridLayout type.
         * @param obj The object to cast.
         * @return The object passed in.
        */
        static cast(obj: any): ManualGridLayout {
            return obj;
        }
    }

    /**
     * The @see:SplitLayout extends from @see:c1.nav.split.SplitLayout.
     */
    export class SplitLayout extends c1.nav.split.SplitLayout {
        /**
         * Casts the specified object to @see:c1.mvc.nav.SplitLayout type.
         * @param obj The object to cast.
         * @return The object passed in.
        */
        static cast(obj: any): SplitLayout {
            return obj;
        }
    }
}