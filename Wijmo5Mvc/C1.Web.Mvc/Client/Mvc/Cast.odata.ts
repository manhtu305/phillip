﻿module wijmo.odata.ODataCollectionView {
    /**
     * Casts the specified object to @see:wijmo.odata.ODataCollectionView type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ODataCollectionView {
        return obj;
    }
}

module wijmo.odata.ODataVirtualCollectionView {
    /**
     * Casts the specified object to @see:wijmo.odata.ODataVirtualCollectionView type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ODataVirtualCollectionView {
        return obj;
    }
}

