﻿module wijmo.grid.CellRange {
    /**
     * Casts the specified object to @see:wijmo.grid.CellRange type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): CellRange {
        return obj;
    }
}

module wijmo.grid.DataMap {
    /**
     * Casts the specified object to @see:wijmo.grid.DataMap type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DataMap {
        return obj;
    }
}

module wijmo.grid.FlexGrid {
    /**
     * Casts the specified object to @see:wijmo.grid.FlexGrid type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexGrid {
        return obj;
    }
}

module wijmo.grid.GridPanel {
    /**
     * Casts the specified object to @see:wijmo.grid.GridPanel type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): GridPanel {
        return obj;
    }
}

module wijmo.grid.HitTestInfo {
    /**
     * Casts the specified object to @see:wijmo.grid.HitTestInfo type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): HitTestInfo {
        return obj;
    }
}

module wijmo.grid.MergeManager {
    /**
     * Casts the specified object to @see:wijmo.grid.MergeManager type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): MergeManager {
        return obj;
    }
}

module wijmo.grid.Column {
    /**
     * Casts the specified object to @see:wijmo.grid.Column type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Column {
        return obj;
    }
}

module wijmo.grid.GroupRow {
    /**
     * Casts the specified object to @see:wijmo.grid.GroupRow type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): GroupRow {
        return obj;
    }
}

module wijmo.grid.Row {
    /**
     * Casts the specified object to @see:wijmo.grid.Row type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Row {
        return obj;
    }
}

module wijmo.grid.RowCol {
    /**
     * Casts the specified object to @see:wijmo.grid.RowCol type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): RowCol {
        return obj;
    }
}

module wijmo.grid.CellEditEndingEventArgs {
    /**
     * Casts the specified object to @see:wijmo.grid.CellEditEndingEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): CellEditEndingEventArgs {
        return obj;
    }
}

module wijmo.grid.CellRangeEventArgs {
    /**
     * Casts the specified object to @see:wijmo.grid.CellRangeEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): CellRangeEventArgs {
        return obj;
    }
}

module wijmo.grid.FormatItemEventArgs {
    /**
     * Casts the specified object to @see:wijmo.grid.FormatItemEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FormatItemEventArgs {
        return obj;
    }
}

