﻿/// <reference path="../Shared/Grid.Sheet.ts" />
declare var JSZip;
/**
 * Defines the FlexSheet control and associated classes.
 */
module c1.mvc.grid.sheet {

    export class _FlexSheetWrapper extends _FlexGridWrapper {
        get _controlType(): any {
            return FlexSheet;
        }

        get _initializerType(): any {
            return c1.mvc._Initializer;
        }

        _getExtensionTypes(): any[] {
            return [this._initializerType];
        }
    }

    /**
     * The @see:FlexSheet control extends the @see:c1.grid.sheet.FlexSheet control and provides an Excel-like functionality.
     */
    export class FlexSheet extends c1.grid.sheet.FlexSheet {
        private _loadActionUrl = '';
        private _saveActionUrl = '';
        private _saveContentType = ContentType.Xlsx;
        private _suspendedOpts = {};
        private _options = {};
        private _remoteDataLoaded = false;

        private static _SUSPENDEDOPTIONS = ['uniqueId', 'file', 'workbook', 'filter', 'appendedSheets', 'saveContentType',
            'columns', 'scrollToSelection', 'selection', 'loadActionUrl', 'saveActionUrl', 'scrollPosition',
            'sheetVisibleChanged', 'selectedSheetChanged', 'definedNames'];

        /**
         * Casts the specified object to @see:c1.mvc.grid.sheet.FlexSheet type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): FlexSheet {
            return obj;
        }

        /**
         * This method is for internal use.
         *
         * Initializes the @see:FlexSheet by copying the properties from a given object.
         *
         * This method allows you to initialize controls using plain data objects
         * instead of setting the value of each property in code.
         *
         * @param options Object that contains the initialization data.
         */
        initialize(options: any) {
            var self = this;

            if (!options) {
                return;
            }

            this._filterOptions(options);

            // TFS: 369148
            // Add property exclusiveValueSearch = true for valueFilterSetting
            // because when Json writes options, it's automatic skip rendering it by default
            const obj = this._getObject(options, "valueFilterSetting", "itemsSource");
            obj.forEach(function (val) {
                val.exclusiveValueSearch = true;
            })

            //Remove "droppingRowColumn" because Wijmo 569 remove onClientDroppingRowColumn
            if (options["droppingRowColumn"]) {
                delete options["droppingRowColumn"];
            }

            delete options["showAlternatingRows"];

            if (options['loadActionUrl'] && !self._remoteDataLoaded) {
                self._loadActionUrl = options['loadActionUrl'];
                //The "remoteLoading" event may be raised before super initializing.
                if (options["remoteLoading"]) {
                    self.remoteLoading.addHandler(options["remoteLoading"]);
                    delete options["remoteLoading"];
                }
                self._options = options;
                self._remoteLoad(true);
                return;
            }

            // We should bind some events before the super initializing
            var prioritizedEvents = ["loadingRows", "loadedRows"];
            for (var key of prioritizedEvents) {
                var value = options[key];
                if (_addEvent(this[key], value)) {
                    delete options[key];
                }
            }

            //Proccessing static property 'defaultTypeWidth'
            var dtw = options.defaultTypeWidth;
            if (dtw) {
                for (var key in dtw) {
                    FlexSheet._defTypeWidth[key] = dtw[key];
                }
                delete options.defaultTypeWidth;
            }

            self._filterOpts(options);
            self._handleSuspendedOptsBefore(() => {
                super.initialize(options);
                setTimeout(self._handleSuspendedOptsAfter.bind(self), 15);
            });
        }
        
        /**
         * Invoke this method to load data from server.
         * @param actionUrl The action url for remote saving.
         */
        remoteLoad(actionUrl?: string) {
            this._remoteLoad(false, actionUrl);
        }

        /**
         * Invoke this method to save data to server.
         * @param contentType @see:c1.mvc.grid.sheet.ContentType specify the data type.
         * @param actionUrl The action url for remote saving.
         * @param extraData The extra data.
         */
        remoteSave(contentType?: ContentType, actionUrl?: string, extraData?: any) {
            var request: IFlexSheetSaveRequest = {},
                processingQuery: c1.mvc.IAjaxSettings = {
                    async: true,
                    type: 'POST',
                    dataType: 'json',
                    postType: 'form',
                    success: this._remoteSaveSuccess.bind(this),
                    error: this._fail.bind(this)
                }, e = new wijmo.CancelEventArgs();

            actionUrl = actionUrl || this._saveActionUrl;

            if (!actionUrl) {
                return;
            }

            this.onRemoteSaving(e);

            if (e.cancel) {
                return;
            }

            if (extraData) {
                request.extraData = extraData;
            }

            processingQuery.url = actionUrl;

            contentType = contentType || this._saveContentType;

            if (contentType === ContentType.Workbook) {
                request.workbook = this.saveToWorkbookOM();
                this._processWorkbook(request.workbook);
                processingQuery.data = { "flexSheetRequest": request };
                c1.mvc.Utils.ajax(processingQuery);
            } else {
                this._saveToString((file) => {
                    request.file = file;
                    processingQuery.data = { "flexSheetRequest": request };
                    c1.mvc.Utils.ajax(processingQuery);
                });
            }
        }

        /**
         * Occurs before remote loading the data from server.
         */
        remoteLoading = new wijmo.Event();

        /**
         * Raises the remoteLoading event.
         */
        onRemoteLoading(e: wijmo.CancelEventArgs) {
            this.remoteLoading.raise(this, e);
            return !e.cancel;
        }

        /**
         * Occurs after remote loading the data from sever.
         */
        remoteLoaded = new wijmo.Event();

        /**
         * Raises the remoteLoaded event.
         */
        onRemoteLoaded(e: wijmo.EventArgs) {
            this.remoteLoaded.raise(this, e);
        }

        /**
         * Occurs before saving the data back to server.
         */
        remoteSaving = new wijmo.Event();

        /**
         * Raises the fileSaving event.
         */
        onRemoteSaving(e: wijmo.CancelEventArgs) {
            this.remoteSaving.raise(this, e);
            return !e.cancel;
        }

        /**
         * Occurs after saving the data back to sever.
         */
        remoteSaved = new wijmo.Event();

        /**
         * Raises the remoteSaved event.
         */
        onRemoteSaved(e: RemoteSavedEventArgs) {
            this.remoteSaved.raise(this, e);
        }

        private _filterOptions(options: any) {
            this._filterPinningOptions(options);
        }

        private _filterPinningOptions(options: any) {
            // Processing change AllowPinning type from bool to enum
            if (options.pinningType >= 0) {
                options.allowPinning = options.pinningType
                delete options.pinningType;
            } else if (options.allowPinning == false) {
                options.allowPinning = wijmo.grid.AllowPinning.None;
            } else if (options.allowPinning == true) {
                options.allowPinning = wijmo.grid.AllowPinning.SingleColumn;
            }
        }
        
        /**
         * Get Object by deep search
         * @param theObject The Object/ Array need to be search
         * @param value The value to get object
         * @param skip The value to skip the loop (don't need to check inside it)
         */
        private _getObject(theObject, value: string, skip?: string) {
            let objects = [];
            if (theObject instanceof Array) {
                for (let i = 0; i < theObject.length; i++) {
                    objects = objects.concat(this._getObject(theObject[i], value, skip));
                }
            }
            else {
                for (let prop in theObject) {
                    if (prop === skip) continue;
                    if (prop === value) objects.push(theObject[prop]);
                    if (theObject[prop] instanceof Object || theObject[prop] instanceof Array)
                        objects = objects.concat(this._getObject(theObject[prop], value, skip));
                }
            }
            return objects;
        }

        private _remoteSaveSuccess(res: IFlexSheetSaveResponse) {
            this.onRemoteSaved(new RemoteSavedEventArgs(res.success, res.error));
        }

        private _filterOpts(options: any) {
            var self = this;
            self._suspendedOpts = {};
            FlexSheet._SUSPENDEDOPTIONS.forEach(opt => self._filterOpt(opt, options));

            if (options['selectedSheetIndex'] !== undefined) {
                self._suspendedOpts['selectedSheetIndex'] = options['selectedSheetIndex'];
            }
        }

        private _filterOpt(key: string, options: any) {
            var self = this;
            if (typeof (options[key]) !== 'undefined') {
                self._suspendedOpts[key] = options[key];
                delete options[key];
            }
        }

        private _handleSuspendedOptsBefore(callback: Function = null): void {
            var self = this,
                fun = () => {
                if (callback) {
                    callback.apply(self);
                }
            };

            FlexSheet._SUSPENDEDOPTIONS.forEach(opt =>
                self._handleSusOptBefore(opt, self._suspendedOpts));

            if (self._suspendedOpts && self._suspendedOpts['file']) {
                self._load(self._suspendedOpts['file'], () => {
                    delete self._suspendedOpts['file'];
                    fun.apply(self);
                });
            } else {
                fun.apply(self);
            }
        }

        private _handleSuspendedOptsAfter(): void {
            FlexSheet._SUSPENDEDOPTIONS.forEach(opt =>
                this._handleSusOptAfter(opt, this._suspendedOpts));
        }

        private _handleSusOptBefore(key: string, suspendedOpts: any) {
            var self = this, value = suspendedOpts[key];
            if (!value) {
                return;
            }

            switch (key) {
                case 'saveActionUrl':
                    self._saveActionUrl = value;
                    break;
                case 'saveContentType':
                    self._saveContentType = value;
                    break;
                case 'workbook':
                    self.loadFromWorkbookOM(value);
                    break;
                case 'filter':
                    // set filter.defaultFilterType will clear the sheet column filter settings,
                    // so apply the filter before append sheets.
                    wijmo.copy(self.filter, value);
                    break;
                case 'appendedSheets':
                    self._addSheetsByOptions(value);
                    break;
                case 'definedNames':
                    value.forEach(item => {
                        var definedName = new wijmo.grid.sheet.DefinedName(self, item.name, item.value, item.sheetName);
                        self.definedNames.push(definedName);
                    });
                    break;
            }
        }

        private _handleSusOptAfter(key: string, suspendedOpts: any): void {
            var self = this, value = suspendedOpts[key];
            if (!value) {
                return;
            }

            switch (key) {
                case 'columns':
                    self._setColumns(value);
                    break;
                case 'selection':
                    self.select(self._buildCellRange(value),
                        suspendedOpts['scrollToSelection'] !== undefined ? suspendedOpts['scrollToSelection'] : true);
                    break;
                case 'scrollPosition':
                    self.scrollPosition = new wijmo.Point(value.x, value.y);
                    break;
                case 'sheetVisibleChanged':
                    if (wijmo.isFunction(value)) {
                        self.sheets.sheetVisibleChanged.addHandler(value);
                    }
                    break;
                case 'selectedSheetChanged':
                    if (wijmo.isFunction(value)) {
                        self.sheets.selectedSheetChanged.addHandler(value);
                    }
                    break;
            }
        }

        private _remoteLoad(internalLoad?: boolean, actionUrl?: string) {
            var self = this, settings = {
                url: actionUrl || self._loadActionUrl,
                async: true,
                type: 'GET',
                dataType: 'json',
                error: self._fail.bind(self),
                success: internalLoad ? self._onRemoteLoadSuccessBeforeInit.bind(self) :
                self._onRemoteLoadSuccess.bind(self)
            }, e = new wijmo.CancelEventArgs();

            self.onRemoteLoading(e);

            if (!e.cancel) {
                c1.mvc.Utils.ajax(settings);
            }
        }

        private _ensureLoadRespose(data: IFlexSheetLoadResponse): boolean {
            if (!data) {
                return false;
            }

            if (!data.success) {
                alert(data.error);
                return false;
            }

            return true;
        }

        private _onRemoteLoadSuccess(data: IFlexSheetLoadResponse) {
            if (!this._ensureLoadRespose(data)) {
                return;
            }

            if (data.workbook) {
                this.loadFromWorkbookOM(data.workbook);
                this.onRemoteLoaded(new wijmo.EventArgs());
            } else if (data.file) {
                this._load(data.file, () => {
                    this.onRemoteLoaded(new wijmo.EventArgs());
                });
            }
        }

        private _load(workbook: any, callback: Function): void {
            if (this._isAysnc()) {
                this.loadAsync(workbook, () => {
                    callback.apply(this);
                }, (reason) => {
                    console.error('The reason of load failure is ' + reason + '.');
                });
            } else {
                this.load(workbook);
                callback.apply(this);
            }
        }

        private _saveToString(callback: (base64?: string) => void) {
            var wb = this.save();
            if (this._isAysnc()) {
                wb.saveAsync('', callback, (reason) => {
                    console.error('The reason of save failure is ' + reason + '.');
                });
            } else {
                var content = wb.save();
                callback.call(this, content);
            }
        }

        // When it is not null, it uses JSZip3.0+.
        // Otherwise, we use JSZip2.5.
        // Now in JSZip3.0+, the synchronous load and save methods are not supported.
        // We need use loadAsync and saveAsync.
        private _isAysnc(): boolean {
            return JSZip.prototype.generateAsync != null;
        }

        private _onRemoteLoadSuccessBeforeInit(data: IFlexSheetLoadResponse) {
            var self = this, workbook: wijmo.xlsx.IWorkbook, file: string;

            if (!self._ensureLoadRespose(data)) {
                return;
            }

            workbook = data.workbook;
            file = data.file;

            if (workbook) {
                self._options['workbook'] = workbook;
            } else if (file) {
                self._options['file'] = file;
            }

            self._remoteDataLoaded = true;
            self.initialize(self._options);
            self.onRemoteLoaded(new wijmo.EventArgs());
        }

        // callback for query fail(network fails)
        private _fail(xhr, textStatus, errorThrown) {
            throw textStatus + errorThrown;
        }

        private _buildCellRange(options: ICellRangeOptions): wijmo.grid.CellRange {
            return new wijmo.grid.CellRange(options.row || 0, options.col || 0, options.row2, options.col2);
        }

        private _buildTable(sheet:wijmo.grid.sheet.Sheet, options: ITableOptions) {
            var name = options.name;
            var range = options.range && this._buildCellRange(options.range);
            var style = options.style && this._buildTableStyle(options.style);
            var columns: wijmo.grid.sheet.TableColumn[] = [];
            if (options.columns && options.columns.length) {
                options.columns.forEach(col => {
                    columns.push(this._buildTableColumn(col));
                });
            }

            var addTable = function (): wijmo.grid.sheet.Table {
                var table = new wijmo.grid.sheet.Table(name, range, style, columns, options);
                sheet.tables.push(table);
                return table;
            }

            var cv: wijmo.collections.ICollectionView = options.itemsSource;
            if (cv == null) {
                addTable();
            }
            else
            {
                var isAdded = false;
                var addTableByCV = ()=> {
                    if (isAdded) return;
                    isAdded = true;

                    setTimeout(function () {
                        cv.collectionChanged.removeHandler(addTableByCV);
                    });

                    if (cv.isEmpty) {
                        return addTable();
                    } else {
                        // construct properties of subset of the items source
                        var properties: string[] = null;
                        if (columns && columns.length) {
                            properties = [];
                            for (var c = 0; c < columns.length; c++) {
                                var col = columns[c];
                                if (c === 0 && col.totalRowLabel == null) {
                                    col.totalRowLabel = 'Total';
                                } else if (c === columns.length - 1 && col.totalRowFunction == null) {
                                    col.totalRowFunction = 'Sum';
                                }
                                properties.push(col.name);
                            }
                        }

                        var table = sheet.addTableFromArray(range.row, range.col, cv.items, properties, name, style, options);

                        // copy table column settings
                        if (table != null && columns && columns.length) {
                            var tableColumns = table.getColumns();
                            columns.forEach(col => {
                                var tableColumn: wijmo.grid.sheet.TableColumn = null;
                                for (var i = 0; i < tableColumns.length; i++) {
                                    if (tableColumns[i].name == col.name) {
                                        tableColumn = tableColumns[i];
                                        break;
                                    }
                                }

                                if (tableColumn != null) {
                                    tableColumn.showFilterButton = col.showFilterButton;
                                    tableColumn.totalRowFunction = col.totalRowFunction;
                                    tableColumn.totalRowLabel = col.totalRowLabel;
                                }
                            });
                        }

                        return table;
                    }
                }

                if (DataSourceManager._isRemoteSource(cv) && cv.isEmpty) {
                    // add table when data is ready
                    cv.collectionChanged.removeHandler(addTableByCV);
                    cv.collectionChanged.addHandler(addTableByCV);
                } else {
                    addTableByCV();
                }
            }
        }


        private _buildTableColumn(options: ITableColumnOptions): wijmo.grid.sheet.TableColumn {
            var name = options.name;
            delete options.name;
            var column = new wijmo.grid.sheet.TableColumn(name);
            wijmo.copy(column, options);
            return column;
        }

        private _buildTableStyle(options: ITableStyleOptions): wijmo.grid.sheet.TableStyle {
            if (!options) return null;

            if (options.name) {
                var builtInStyle = this.getBuiltInTableStyle(options.name);
                if (builtInStyle) {
                    return builtInStyle;
                }
            }

            var style = new wijmo.grid.sheet.TableStyle(options.name);
            delete options["name"];
            wijmo.copy(style, options);
            return style;
        }

        private _setColumns(columns): void {
            var self = this;
            self.columns.clear();
            columns.forEach(item => {
                var col = new wijmo.grid.Column(), dataMap = item['dataMap'], mapChanged;
                delete item['dataMap'];
                wijmo.copy(col, item);

                if (dataMap) {
                    mapChanged = dataMap['mapChanged'];
                    dataMap = new wijmo.grid.DataMap(dataMap['itemsSource'],
                        dataMap['selectedValuePath'], dataMap['displayMemberPath']);
                    if (mapChanged) {
                        dataMap.mapChanged.addHandler(mapChanged);
                    }
                    col['dataMap'] = dataMap;
                }

                self.columns.push(col);
            });
        }

        private _updateFilterSettingDataMap(filter: wijmo.grid.sheet.IFilterSetting): void {
            var columnFilters = filter.columnFilterSettings;
            if (columnFilters && columnFilters.length) {
                columnFilters.forEach(columnFilter => {
                    if (columnFilter.dataMap) {
                        columnFilter.dataMap = this._buildDataMap(columnFilter.dataMap);
                    }

                    var valueFilter = columnFilter.valueFilterSetting;
                    if (valueFilter && valueFilter.dataMap) {
                        valueFilter.dataMap = this._buildDataMap(valueFilter.dataMap);
                    }

                    var conditionFilter = columnFilter.conditionFilterSetting;
                    if (conditionFilter && conditionFilter.dataMap) {
                        conditionFilter.dataMap = this._buildDataMap(conditionFilter.dataMap);
                    }
                });
            }
        }

        private _buildDataMap(options: any): wijmo.grid.DataMap {
            if (options == null) return null;

            var dataMapOptions = <IDataMapOptions>options;
            var mapChanged = dataMapOptions.mapChanged;
            var sortByDisplayValues = dataMapOptions.sortByDisplayValues;
            var dataMap = new wijmo.grid.DataMap(dataMapOptions.itemsSource,
                dataMapOptions.selectedValuePath, dataMapOptions.displayMemberPath);
            if (mapChanged) {
                dataMap.mapChanged.addHandler(mapChanged);
            }
            // default value of sortByDisplayValues is false in mvc, but is true in Wijmo 5.
            dataMap.sortByDisplayValues = sortByDisplayValues === true;

            return dataMap;
        }

        private _addSheetsByOptions(sheets: ISheetOptions[]): void {
            var self = this, DEFAULT_ROW_COUNT = 200, DEFAULT_COLUMN_COUNT = 20;
            sheets.forEach(item=> {
                var sheet: wijmo.grid.sheet.Sheet, selectionRanges: any[],
                    name = item.name || '', tables: ITableOptions[],
                    rowCount = item.rowCount === undefined ? DEFAULT_ROW_COUNT : item.rowCount,
                    columnCount = item.columnCount === undefined ? DEFAULT_COLUMN_COUNT : item.columnCount,
                    itemsSource = item.itemsSource || null,
                    visible = item.visible === undefined ? true : item['visible'];

                if (item.type == 'UnboundSheet') {
                    sheet = self.addUnboundSheet(name, rowCount, columnCount);
                } else {
                    sheet = self.addBoundSheet(name, itemsSource);
                }

                if (item.nameChanged) {
                    sheet.nameChanged.addHandler(<wijmo.IEventHandler>item.nameChanged);
                }

                sheet.visible = visible;

                if (item.visibleChanged) {
                    sheet.visibleChanged.addHandler(<wijmo.IEventHandler>item.visibleChanged);
                }

                selectionRanges = item.selectionRanges;
                if (selectionRanges && selectionRanges.length) {
                    sheet.selectionRanges.clear(); // Clear the selection of sheet.
                    selectionRanges.forEach(item => {
                        sheet.selectionRanges.push(self._buildCellRange(item));
                    });
                }

                tables = item.tables;
                if (tables && tables.length) {
                    var tablesAdded = false;
                    var cv: wijmo.collections.CollectionView = sheet.itemsSource;
                    var buildTables = ()=> {
                        if (tablesAdded) return;
                        tablesAdded = true;

                        if (cv != null) {
                            setTimeout(function () {
                                cv.collectionChanged.removeHandler(buildTables);
                            });
                        }

                        tables.forEach(table => {
                            this._buildTable(sheet, table);
                        });
                    };

                    if (DataSourceManager._isRemoteSource(cv) && cv.isEmpty) {
                        // add tables when sheet data is ready
                        cv.collectionChanged.removeHandler(buildTables);
                        cv.collectionChanged.addHandler(buildTables);
                    } else {
                        buildTables();
                    }
                }

                var filterSetting = item.filterSetting;
                if (filterSetting) {
                    this._updateFilterSettingDataMap(filterSetting);
                    sheet.filterSetting = filterSetting;
                }
            });
        }

        // Remove some useless contents and validate some values.
        private _processWorkbook(workbook: wijmo.xlsx.IWorkbook) {
            workbook.sheets && workbook.sheets.forEach(sheet => {
                sheet.columns && sheet.columns.forEach(col => {
                    col.width && (col.width = col.width.toFixed(0));
                });
                sheet.rows && sheet.rows.forEach(row => {
                    row.height && (row.height = +row.height.toFixed(0));
                    row.cells && row.cells.forEach(cell => {
                        if (!cell) {
                            return;
                        }

                        if (cell.colSpan === 1) {
                            delete cell.colSpan;
                        }

                        if (cell.rowSpan === 1) {
                            delete cell.rowSpan;
                        }
                    });
                });
            });
        }

        get defaultRowSize(): number {
            return this.rows.defaultSize;
        }

        set defaultRowSize(value: number) {
            this.rows.defaultSize = value;
        }

        get defaultColumnSize(): number {
            return this.columns.defaultSize;
        }

        set defaultColumnSize(value: number) {
            this.columns.defaultSize = value;
        }
    }

    interface ISheetOptions {
        type: string;
        name?: string;
        rowCount?: number;
        columnCount?: number;
        itemsSource?: wijmo.collections.ICollectionView;
        nameChanged?: Function;
        visibleChanged?: Function;
        selectionRanges?: ICellRangeOptions[];
        visible?: boolean;
        tables?: ITableOptions[];
        filterSetting?: wijmo.grid.sheet.IFilterSetting;
    }

    interface ITableOptions extends wijmo.grid.sheet.ITableOptions {
        name: string;
        range: ICellRangeOptions;
        style?: ITableStyleOptions;
        columns?: ITableColumnOptions[];
        itemsSource?: any;
    }

    interface ITableColumnOptions {
        name: string;
    }

    interface ITableStyleOptions {
        name: string;
    }

    interface ICellRangeOptions {
        row: number;
        col: number;
        row2?: number;
        col2?: number;
    }

    interface IDataMapOptions {
        displayMemberPath: string;
        selectedValuePath: string;
        sortByDisplayValues?: boolean;
        isEditable?: boolean;
        itemsSource: wijmo.collections.ICollectionView;
        mapChanged?: any;
    }

    // Options that describes the remote load response.
    export interface IFlexSheetLoadResponse {
        success: boolean;
        error?: string;
        workbook?: wijmo.xlsx.IWorkbook;
        file?: string;
    }

    /**
     * Defined the content type for remote saving.
     */
    export enum ContentType {
        /**
         * The xlsx file.
         */
        Xlsx = 0,

        /**
         * The Workbook object model.
         */
        Workbook = 1
    }


    //Options that describes the save request parameters.
    export interface IFlexSheetSaveRequest {

        //The base64 string of xlsx file.
        file?: string;

        //The Workbook object model.
        workbook?: wijmo.xlsx.IWorkbook;

        //The extra data send to server.
        extraData?: any;
    }

    //Options that describes the save result.
    export interface IFlexSheetSaveResponse {
        //A boolean value indicates wheter the operation is success.
        success?: boolean;

        //The error message.
        error?: string;
    }

    /**
     * Provides arguments for the @see:FlexSheet <b>remoteSaved</b> event.
     */
    export class RemoteSavedEventArgs extends wijmo.EventArgs {
        _success: boolean;
        _error: string;

        // Initializes a new instance of a @see:RemoteSavedEventArgs.
        // @param success Specify whether the save operation is success.
        // @param error The error message.
        // @param newValue The new value of the property.
        constructor(success: boolean, error: string) {
            super();
            this._success = success;
            this._error = error;
        }

        /**
         * Casts the specified object to @see:c1.mvc.grid.sheet.RemoteSavedEventArgs type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): RemoteSavedEventArgs {
            return obj;
        }

        /**
         * Gets the value specify whether the save operation is success.
         */
        get success(): boolean {
            return this._success;
        }

        /**
         * Gets the error message.
         */
        get error(): string {
            return this._error;
        }
    }
}