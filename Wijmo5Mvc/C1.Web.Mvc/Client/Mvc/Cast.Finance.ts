﻿module wijmo.chart.finance.FinancialChart {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.FinancialChart type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FinancialChart {
        return obj;
    }
}

module wijmo.chart.finance.FinancialSeries {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.FinancialSeries type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FinancialSeries {
        return obj;
    }
}

module wijmo.chart.finance.analytics.SingleOverlayIndicatorBase {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.SingleOverlayIndicatorBase type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): SingleOverlayIndicatorBase {
        return obj;
    }
}

module wijmo.chart.finance.analytics.ATR {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.ATR type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ATR {
        return obj;
    }
}

module wijmo.chart.finance.analytics.BollingerBands {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.BollingerBands type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): BollingerBands {
        return obj;
    }
}

module wijmo.chart.finance.analytics.CCI {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.CCI type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): CCI {
        return obj;
    }
}

module wijmo.chart.finance.analytics.Envelopes {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.Envelopes type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Envelopes {
        return obj;
    }
}

module wijmo.chart.finance.analytics.Fibonacci {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.Fibonacci type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Fibonacci {
        return obj;
    }
}

module wijmo.chart.finance.analytics.FibonacciArcs {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.FibonacciArcs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FibonacciArcs {
        return obj;
    }
}

module wijmo.chart.finance.analytics.FibonacciFans {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.FibonacciFans type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FibonacciFans {
        return obj;
    }
}

module wijmo.chart.finance.analytics.FibonacciTimeZones {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.FibonacciTimeZones type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FibonacciTimeZones {
        return obj;
    }
}

module wijmo.chart.finance.analytics.Macd {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.Macd type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Macd {
        return obj;
    }
}

module wijmo.chart.finance.analytics.MacdBase {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.MacdBase type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): MacdBase {
        return obj;
    }
}

module wijmo.chart.finance.analytics.MacdHistogram {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.MacdHistogram type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): MacdHistogram {
        return obj;
    }
}

module wijmo.chart.finance.analytics.RSI {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.RSI type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): RSI {
        return obj;
    }
}

module wijmo.chart.finance.analytics.Stochastic {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.Stochastic type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Stochastic {
        return obj;
    }
}

module wijmo.chart.finance.analytics.WilliamsR {
    /**
     * Casts the specified object to @see:wijmo.chart.finance.analytics.WilliamsR type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): WilliamsR {
        return obj;
    }
}

