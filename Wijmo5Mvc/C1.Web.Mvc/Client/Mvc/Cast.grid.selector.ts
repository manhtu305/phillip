﻿module wijmo.grid.selector.Selector {
    /**
     * Casts the specified object to @see:wijmo.grid.selector.Selector type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Selector {
        return obj;
    }
}

module wijmo.grid.selector.BooleanChecker {
    /**
     * Casts the specified object to @see:wijmo.grid.selector.BooleanChecker type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): BooleanChecker {
        return obj;
    }
}

