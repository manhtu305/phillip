﻿/// <reference path="Collections.ts" />
/// <reference path="Grid.ts" />

module c1.mvc.grid {

    // The extension which provides unobtrusive validation.
    export class _Validator {
        private readonly _grid: wijmo.grid.FlexGrid & _IFlexGridMvc;
        private _validationAttributesMap: any;
        private _isInnerCheckBoxEditor: boolean;
        private _editor: HTMLInputElement;
        private _validatedEditor: any;

        // Initializes a new instance of the @see:_Validator.
        // @param grid The @see:wijmo.grid.FlexGrid.
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc) {
            this._grid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);

            _overrideMethod(grid, _Initializer._INITIAL_NAME,
                this._beforeInitialize.bind(this));

            _overrideMethod(grid, 'onPrepareCellForEdit',
                null, this._afterOnPrepareCellForEdit.bind(this));

            _overrideMethod(grid, 'onCellEditEnding',
                this._beforeOnCellEditEnding.bind(this));

            _overrideMethod(grid, 'onCellEditEnded',
                null, this._afterOnCellEditEnded.bind(this));
        }

        private _beforeInitialize(options) {
            if (options && options.validationAttributesMap) {
                this._validationAttributesMap = options.validationAttributesMap;
                delete options.validationAttributesMap;
            }
        }

        private _afterOnPrepareCellForEdit(e: wijmo.grid.CellRangeEventArgs): void {
            var editor = this._grid.activeEditor, column = this._grid._getBindingColumnFromCellRangeMvc(e),
                binding = column.binding, validationAttr;

            if (!binding || column.dataMap || column.dataType === wijmo.DataType.Boolean) {
                return;
            }

            for (var item in this._validationAttributesMap) {
                if (item.toLowerCase() == binding.toLowerCase()) {
                    validationAttr = this._validationAttributesMap[item];
                    break;
                }
            }

            if (!validationAttr) {
                return;
            }

            for (var attr in validationAttr) {
                editor.setAttribute(attr, validationAttr[attr]);
            }

            editor.setAttribute('name', binding);

            var $ = window['jQuery'];
            if ($ && $.validator && $.validator.unobtrusive) {
                //this editor is wrapped in <form> tag, it is used for validating purposes.
                $(editor).wrap('<form></form>');
                if (wijmo.isIE() && document.activeElement == null) window.focus();//372455
                this._validatedEditor = editor;

                //hide the textarea of the grid
                var textArea = $("textarea.wj-grid-editor.wj-form-control.wj-grid-ime");
                if (textArea) {
                    textArea.css('opacity', 0);
                }

                // wrap makes editor lost focus, should manually set focus.
                this._focusEditor(editor);
               
                $.validator.unobtrusive.parse(editor.form);
                var validator = $(editor.form).data('validator');
                if (validator && validator.settings) {
                    validator.settings.errorPlacement = ($error: any, $element: any) => {
                        var $cellDiv = $element.closest('div');
                        $cellDiv.removeClass(validator.settings.validClass);
                        $cellDiv.addClass(validator.settings.errorClass);
                        $cellDiv.attr('title', $error.text());
                    };
                    validator.settings.success = ($error: any, element: HTMLElement) => {
                        var $cellDiv = $(element).closest('div');
                        $cellDiv.removeClass(validator.settings.errorClass);
                        $cellDiv.addClass(validator.settings.validClass);
                        $cellDiv.attr('title', '');
                    };

                    $(editor).on('input', () => {
                        validator.element(editor);
                    });

                    //$(editor).change(() => {
                    //    validator.element(editor);
                    //});

                    setTimeout(function () {
                        validator.element(editor);
                    });
                }
            }
        }

        private _focusEditor(editor: HTMLInputElement) {
            // The condition is from _EditHandler.startEditing().
            if (editor.type == 'checkbox') {
                return;
            }

                editor.focus();

            // TFS 325130
            if (wijmo.isIE()) {
                var isKeydown, isKeypress, charCode = 0, grid = this._grid;
                if (event instanceof KeyboardEvent) {
                    var keyboardEvent = <KeyboardEvent>event;
                    isKeydown = keyboardEvent.type == 'keydown';
                    isKeypress = keyboardEvent.type == 'keypress';
                    charCode = keyboardEvent.charCode || keyboardEvent.keyCode;
                }
                
                setTimeout(function () {
                    if (document.activeElement == editor) {
                        // on pressing F2 in IE, may not select all text.
                        if (isKeydown === true && charCode == wijmo.Key.F2) {
                            if (editor instanceof HTMLInputElement && editor.value) {
                                wijmo.setSelectionRange(editor, 0, editor.value.length);
                            }
                        }
                    } else {
                        editor.focus();

                        if (editor instanceof HTMLInputElement) {
                            if (editor.value) {
                                wijmo.setSelectionRange(editor, 0, editor.value.length);
                            }

                            // following codes are copied from wijmo5 _KeyboardHandler._keypress() function,
                            // consider if enter editing by pressing character key.
                            if (isKeypress === true && charCode > wijmo.Key.Space && charCode != wijmo.Key.F2) {
                                var sel = grid.selection,
                                    txt = grid.getCellData(sel.row, sel.col, true),
                                    val = grid.getCellData(sel.row, sel.col, false);

                                editor.value = String.fromCharCode(charCode);
                                if (wijmo.isNumber(val) && txt.indexOf('%') > -1) {
                                    editor.value += '%';
                                }

                                wijmo.setSelectionRange(editor, 1);

                                var evtInput = document.createEvent('HTMLEvents');
                                evtInput.initEvent('input', true, false);
                                editor.dispatchEvent(evtInput);
                            }
                        }
                    }
                });
            }
        }

        private _beforeOnCellEditEnding(e: wijmo.grid.CellEditEndingEventArgs): void {
            var grid = this._grid;

            // if cancel is true, don't validate the input by jquery.
            if (e.cancel) {
                return;
            }

            this._isInnerCheckBoxEditor = grid.activeEditor
                && grid.activeEditor.type == 'checkbox'
                && wijmo.contains(grid.hostElement, grid.activeEditor);
            this._editor = grid.activeEditor;

            var $ = window['jQuery'];
            //validate the value of the editor that was wrapped inside <form></form>
            if ($ && this._validatedEditor) {
                var validator = $(this._validatedEditor.form).data('validator');
                if (validator) {
                    e.cancel = !(validator.element(this._validatedEditor));
                    if (!e.cancel) {
                        //if the value is valid, set the value to the active editor of the grid.
                        grid.activeEditor.value = this._validatedEditor.value;
                    }
                }
            }
        }

        private _afterOnCellEditEnded(e: wijmo.grid.CellRangeEventArgs) {
            var grid = this._grid,
                ecv: wijmo.collections.IEditableCollectionView,
                e: wijmo.grid.CellRangeEventArgs;

            this._validatedEditor = null;
            if (!this._editor || e.cancel || this._isInnerCheckBoxEditor) {
                this._isInnerCheckBoxEditor = false;
                this._editor = null;
                return;
            }

            this._editor = null;
            this._isInnerCheckBoxEditor = false;

            // TFS 431421: Wijmo 20201.663 can handle it.
            //ecv = wijmo.tryCast(grid.collectionView, 'IEditableCollectionView');
            //if (ecv && ecv.currentEditItem) {
            //    e = new wijmo.grid.CellRangeEventArgs(grid.cells, grid.selection);
            //    grid.onRowEditEnding(e);
            //    ecv.commitEdit();
            //    grid.onRowEditEnded(e);
            //}
        }   
    }
}