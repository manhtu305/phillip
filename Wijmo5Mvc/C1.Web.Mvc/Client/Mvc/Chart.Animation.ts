﻿/// <reference path="../Shared/Chart.Animation.ts" />

/**
 * Defines the @see:c1.mvc.chart.animation.ChartAnimation
 * for @see:c1.mvc.chart.FlexChart, @see:c1.mvc.chart.finance.FinancialChart and @see: c1.mvc.chart.FlexPie.
 */
module c1.mvc.chart.animation {
    /**
     * The @see:ChartAnimation control extends from @see:c1.chart.animation.ChartAnimation.
    */
    export class ChartAnimation extends c1.chart.animation.ChartAnimation {
    }
}