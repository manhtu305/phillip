﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts" />
/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.detail.d.ts" />
/// <reference path="../Shared/Grid.ts" /> 

/**
 * Extension that provides detail rows for @see:c1.mvc.grid.FlexGrid controls.
 */
module c1.mvc.grid.detail {
    /**
     * The @see:FlexGridDetailProvider control extends from @see:c1.grid.detail.FlexGridDetailProvider.
    */
    export class FlexGridDetailProvider extends c1.grid.detail.FlexGridDetailProvider {

        private template: any;
        private uniqueId: string;

        // Initializes the @see:FlexGridDetailProvider by copying the properties from a given object.
        //
        // This method allows you to initialize controls using plain data objects
        // instead of setting the value of each property in code.
        //
        // @param options Object that contains the initialization data.
        initialize(options: any) {
            var self = this;

            self.uniqueId = self.grid.hostElement.id;
            self.template = options.detailRowTemplateId || options.detailRowTemplateContent;

            if (options) {
                if (!options.createDetailCell && self.template) {
                    options.createDetailCell = function (row) {
                        var cellWrapper: HTMLElement, data = {};
                        cellWrapper = document.createElement('div');
                         // attach the wrapper to the DOM tree, make sure the control nested in the template can be initialized.
                        self.grid.hostElement.appendChild(cellWrapper);
                        //data[c1.mvc.Template.UID] = self.uniqueId + '_DetailRow_' + row.index;
                        data[c1.mvc.Template.DATACONTEXT] = row.dataItem;
                        self.template.applyTo(cellWrapper, data);
                        //remove the wrapper from DOM tree.
                        cellWrapper.parentElement.removeChild(cellWrapper);
                        return cellWrapper;
                    };
                }

                delete options.detailRowTemplateId;
                delete options.detailRowTemplateContent;
                wijmo.copy(self, options);
                if (self.detailVisibilityMode === wijmo.grid.detail.DetailVisibilityMode.Selection) self.showDetail(self.grid.selection.row, true);
            }
        }
    }
}