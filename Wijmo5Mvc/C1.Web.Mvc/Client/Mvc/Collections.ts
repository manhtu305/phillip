﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts" />

/**
 * Defines interfaces and classes related to data.
 */
module c1.mvc.collections {

    /**
     * Provides arguments for queryData events.
     */
    export class QueryEventArgs extends wijmo.EventArgs {
        /**
         * Casts the specified object to @see:c1.mvc.collections.QueryEventArgs type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): QueryEventArgs {
            return obj;
        }

        /**
         * Gets or sets the extra data for the query.
         */
        extraRequestData: any;
    }

    /**
     * Provides arguments for queryComplete events.
     */
    export class QueryCompleteEventArgs extends wijmo.EventArgs {
        private _result: IOperationResult;

        /**
         * Casts the specified object to @see:c1.mvc.collections.QueryCompleteEventArgs type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): QueryCompleteEventArgs {
            return obj;
        }

        /**
         * Initializes a new instance of the @see:QueryCompleteEventArgs class.
         *
         * @param result The operation result of the query.
         */
        constructor(result: IOperationResult) {
            super();
            this._result = result;
        }

        /**
        * Gets the operation result of the query.
        */
        get result(): IOperationResult {
            return this._result;
        }
    }

    /**
     * Provides arguments for error events.
     */
    export class ErrorEventArgs extends wijmo.CancelEventArgs {
        private _errors: string[];

        /**
         * Casts the specified object to @see:c1.mvc.collections.ErrorEventArgs type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): ErrorEventArgs {
            return obj;
        }

        /**
         * Initializes a new instance of the @see:ErrorEventArgs class.
         *
         * @param errors The errors from server side.
         */
        constructor(errors: string[]) {
            super();
            this._errors = errors;
        }

        /**
         * Gets or sets the errors from server side.
         */
        get errors(): string[] {
            return this._errors;
        }
    }

    /**
     * Extends @see:wijmo.collections.CollectionView to support remote data service.
     *
     * You can use the result objects from the data service as data sources for any Wijmo controls, 
     * and in addition to full CRUD support and real-time updates you automatically get 
     * CollectionView features including sorting, filtering, paging, grouping, and editing. 
     */
    export class RemoteCollectionView extends wijmo.collections.CollectionView {

        private _totalItemCount: number;
        private _disableServerRead: boolean = false;
        private _requestParams: ICollectionViewRequest;
        private _createActionUrl: string;
        private _readActionUrl: string;
        private _deleteActionUrl: string;
        private _updateActionUrl: string;
        _batchEditActionUrl: string;
        _batchEdit: boolean = false;
        _queryList: any[] = [];
        private _errors: string[] = [];
        private _initialItemsCount: number;
        private _skip: number = 0;
        private _top: number = 0;
        private _isInitializing: boolean = false;
        private _isFillingData: boolean = false;
        private _query: number = 0;
        private _isUpdatingItem: boolean = false;
        private _isGrouping: boolean = false;
        private _needMoveCurrentToFirst: boolean = true;
        _dataInfo : any;
        private _nullCount: number = 0;
        private newItems: any[] = [];
        private _reservedItems: _IReservedItem[] = [];
        _flexGridSearchs: any[] = [];
        _refreshOnEdit: boolean = true;
        
        /**
         * Casts the specified object to @see:c1.mvc.collections.RemoteCollectionView type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): RemoteCollectionView {
            return obj;
        }

        /**
         * Raises the @see:sourceCollectionChanged event.
         */
        onSourceCollectionChanged(e?: wijmo.EventArgs) {
            this._dataInfo = null;
            this._nullCount = 0;
            super.onSourceCollectionChanged(e);
        }

        _ensureDataInfo(data: any[]) {
            // if there is no data, don't process.
            if (!data || data.length === 0) {
                return;
            }

            if (!this._dataInfo || this._nullCount > 0) {
                // reset to default.
                if (!this._dataInfo) {
                    this._dataInfo = {};
                    this._nullCount = 0;
                }
                var index = 0;
                var rowData = data[index++];
                this._updateObjectInfo(rowData, this._dataInfo);
                while (this._nullCount > 0 && index < data.length) {
                    this._updateDataInfoForNull(this._dataInfo, data[index++]);
                }
            }
        }

        // process the item which data info is null.
        private _updateDataInfoForNull(dataInfo: any, rowData: any) {
            for (var key in dataInfo) {
                var info = dataInfo[key];
                var value = rowData[key];
                if (info == null) {
                    if (value != null) {
                        this._nullCount --;
                        this._updateDataInfo(dataInfo, key, value);
                    }
                } else if (wijmo.isObject(info)) {
                    this._updateDataInfoForNull(dataInfo[key], value);
                }
            }
        }

        // update data info
        private _updateDataInfo(dataInfo: any, key: string, value: any) {
            if (value == null) {
                dataInfo[key] = null;
                this._nullCount++;
            } else if (wijmo.isDate(value)) {
                dataInfo[key] = value['dateKind'] || DateKind.Unspecified;
            } else if (wijmo.isObject(value)) {
                if (['_rowInfo', '_bnd'].indexOf(key) >= 0) { 
                    //TransposedGrid: with Proxy item, these are unserializable objects, so, not copy but only refer to them
                    dataInfo[key] = value;
                } else {
                    dataInfo[key] = {};
                    this._updateObjectInfo(value, dataInfo[key]);
                }
            } else if (typeof dataInfo[key] !== 'undefined') {
                delete dataInfo[key];
            }
        }
        
        // update data info for plain object.
        private _updateObjectInfo(rowData: any, dataInfo: any) {
            for (var key in rowData) {
                this._updateDataInfo(dataInfo, key, rowData[key]);
            }
        }

        // add the missing dateKind for the value which is Date object.
        static _processRequestRowData(rowData: any, dataInfo: any) {
            for (var key in rowData) {
                var value = rowData[key];
                if (value == null) {
                    continue;
                }

                if (wijmo.isDate(value)) {
                    if (typeof (value['dateKind']) === 'undefined') {
                        rowData[key]['dateKind'] = (dataInfo == null || dataInfo[key] == null ? c1.mvc.DateKind.Unspecified : dataInfo[key]);
                    }
                } else if (wijmo.isObject(value)) {
                    RemoteCollectionView._processRequestRowData(value, dataInfo[key]);
                }
            }
        }

        /**
         * Initializes a new instance of a @see:RemoteCollectionView.
         *
         * @param options A @see:IRemoteCollectionViewSettings object with query options (such as take, skip, sort, etc).
         */
        constructor(options: IRemoteCollectionViewSettings) {
            super();
            var self = this;

            self._requestParams = {};
            self._applySettings(options);
            if (self._batchEdit) {
                // track the changes.
                self.trackChanges = true;
            }

            // check that groupDescriptions contains GroupDescriptions
            self.groupDescriptions.collectionChanged.removeAllHandlers();
            self.groupDescriptions.collectionChanged.addHandler(function () {
                var arr = self.groupDescriptions;
                for (var i = 0; i < arr.length; i++) {
                    var gd = wijmo.tryCast(arr[i], wijmo.collections.GroupDescription);
                    if (!gd) {
                        throw 'groupDescriptions array must contain GroupDescription objects.';
                    }
                }
                if (self.canGroup) {
                    self._isGrouping = true;
                    //self.refresh();
                    self._commitAndRefresh();
                    self._isGrouping = false;
                }
            });
        }

        /**
         * Occurs when the query requests complete.
         */
        queryComplete = new wijmo.Event();

        /**
         * Raises the <b>queryComplete</b> event.
         *
         * @param e @see:QueryCompleteEventArgs that contains the event data.
         */
        onQueryComplete(e: QueryCompleteEventArgs) {
            this.queryComplete.raise(this, e);
        }

        beginQuery = new wijmo.Event();
        onBeginQuery() {
            this._query++;
            this.beginQuery.raise(this);
        }

        endQuery = new wijmo.Event();
        onEndQuery() {
            this._query--;
            this.endQuery.raise(this);
        }

        _isQuerying(): boolean {
            return this._query > 0;
        }

        /**
         * The event fires when collect the ajax query data.
         */
        queryData = new wijmo.Event();

        /**
         * Raises the @see:queryData event.
         *
         * @param e A @see:QueryEventArgs object.
         */
        onQueryData(e: QueryEventArgs) {
            this.queryData.raise(this, e);
        }

        /**
         * The error event which raises when there are errors from the server side.
         */
        error = new wijmo.Event();

        /**
         * Raises the @see:error event.
         *
         * @param e A @see:ErrorEventArgs object.
         */
        onError(e: ErrorEventArgs) {
            this.error.raise(this, e);
            return !e.cancel;
        }

        /**
         * Occurs when parsing the response text.
         */
        reponseTextParsing = new wijmo.Event();
        /**
         * Raises the @see:reponseTextParsing event.
         *
         * @param e A @see:JSONOperationEventArgs object.
         */
        onReponseTextParsing(e: JSONOperationEventArgs) {
            this.reponseTextParsing.raise(this, e);
            return e.cancel;
        }

        /**
         * Occurs when serializing the request data.
         */
        requestDataStringifying = new wijmo.Event();
        /**
         * Raises the @see:requestDataStringifying event.
         *
         * @param e A @see:JSONOperationEventArgs object.
         */
        onRequestDataStringifying(e: JSONOperationEventArgs) {
            this.requestDataStringifying.raise(this, e);
            return e.cancel;
        }

        //Require the initial data. For internal use.
        requireInitData(options: IRemoteCollectionViewSettings) {
            var self = this;
            if (self._isDynamicalLoadingEnabled()) {
                self._sendRequest(self._readActionUrl, {
                    top: self._initialItemsCount
                });
            } else {
                self._sendRequest(self._readActionUrl);
            }
        }

        // Apply settings to the RemoteCollectionView.
        // @param options Object with query options (such as take, skip, sort, etc).
        private _applySettings(options: IRemoteCollectionViewSettings) {
            var self = this,
                groupDescription: wijmo.collections.GroupDescription,
                groupClass;

            if (!options) {
                return;
            }

            self._initialize(function () {
                if (options['getError']) {
                    self.getError = options['getError'];
                }
                self._disableServerRead = !!options.disableServerRead;
                self._pgIdx = options.pageIndex || 0;
                self.pageSize = options.pageSize || 0;

                if (options.sortDescriptions) {
                    options.sortDescriptions.forEach(sort => self.sortDescriptions.push(new wijmo.collections.SortDescription(sort.property, sort.ascending)));
                }
                if (options.groupDescriptions) {
                    options.groupDescriptions.forEach(group => {
                        groupClass = group.clientClass;
						groupDescription = groupClass ? new groupClass((<IPropertyGroupDescription>group).propertyName, (<IPropertyGroupDescription>group).converter)
							: new wijmo.collections.PropertyGroupDescription((<IPropertyGroupDescription>group).propertyName, (<IPropertyGroupDescription>group).converter);
                        self.groupDescriptions.push(groupDescription);
                    });
                }
                if (typeof options.refreshOnEdit !== 'undefined')
                    self._refreshOnEdit = options.refreshOnEdit;

                self._readActionUrl = options.readActionUrl || '';
                self._createActionUrl = options.createActionUrl || '';
                self._updateActionUrl = options.updateActionUrl || '';
                self._deleteActionUrl = options.deleteActionUrl || '';
                self._batchEditActionUrl = options.batchEditActionUrl || '';
                self._batchEdit = !!options.batchEdit;
                self._initialItemsCount = options.initialItemsCount;

                self._initEventsOpts(options);

                if (options['newItemCreator']) {
                    self.newItemCreator = options['newItemCreator'];
                }

                if (!self._disableServerRead && self.pageSize > 0 && !self._readActionUrl
                    && options.sourceCollection && options.sourceCollection.length > self.pageSize) {
                    self._disableServerRead = true;
                }

                if (options['sortNullsFirst'])
                    self.sortNullsFirst = options['sortNullsFirst'];
                if (options.hasOwnProperty('sortNulls'))
                    self.sortNulls = options['sortNulls'];
            });

            self.requireInitData(options);
        }

        private _initEventsOpts(options: any) {
            for (var key in options) {
                var value = options[key];
                if (this[key] instanceof wijmo.Event &&
                    value && wijmo.isFunction(value)) {
                    (<wijmo.Event>this[key]).addHandler(value);
                }
            }
        }

        private _initialize(fn: Function) {
            try {
                this._isInitializing = true;
                fn();
            } finally {
                this._isInitializing = false;
            }
        }

        /**
         * Force to refresh the view using the current sort, filter and group parameters.
         * When calling this method, an ajax request will be sent to server side to fecth data.
         */
        forceRefresh() {
            this._requestRead();
        }

        /**
         * Re-creates the view using the current sort, filter, and group parameters.
         * When the data is not cached in client-side, an ajax request will be sent
         * to server side to fecth data.
         */
        refresh() {
            // when some properties(like sort, page) are set in the constructor,
            // the refresh method is not expected to be called to refresh the collectionview.
            // the collectionview is expected to be called when the source data is filled.
            if (this._isInitializing) {
                return;
            }
            super.refresh();
        }

        /**
         * Commit and refresh the view using the current sort, filter, and group parameters.
         * When the data is not cached in client-side, an ajax request will be sent
         * to server side to fecth data.
         */
        _commitAndRefresh() {
            if (this._isInitializing) {
                return;
            }
            super._commitAndRefresh();
        }

        // Check if is searching then enable filter for FlexGridSearch
        _isSearching() : boolean {
            var isSearching = false;
            if (this._flexGridSearchs) {
                this._flexGridSearchs.forEach(function (gridSearch) {
                    if (gridSearch.text || gridSearch._lastText) {
                        gridSearch._lastText = gridSearch.text;
                        isSearching = true;
                    }
                });
            }
            return isSearching;
        }
        
        // Override to send the data request to server when setting filter.
        _performRefresh() {
            var self = this, canFilter = self.canFilter, canSort = self.canSort, disableServerRead = self._disableServerRead;
            if (
                //when all the data locates in the client, calling the super._performRefresh to refresh the data.
                disableServerRead
                // Hack for ts2104: https://typescript.codeplex.com/workitem/1098
                // During the period when collectview is created, the collectionChanged event will be fired 
                // and when some condition is met, the query would be send for data.
                // Some subscribers corresponding the query will be raised(like beginQuery), but these subscribers are defined after base constructor. 
                // So add the condition !this.beginQuery to ensure the subscribers defined.
                || !self.beginQuery
                // when it is updating the item, calling the super._performRefresh method to avoid send the query.
                || self._isUpdatingItem
                // when it is raised by group setting changed.
                || self._isGrouping
                // After fill data
                || self._isFillingData) {
                // If it is server read, the data is processed in server side, so don't need to filter and sort in client side again.
                var isSearching = self._isSearching();
                if (!disableServerRead) {
                    self.canFilter = isSearching;
                    self.canSort = false;
                }
                super._performRefresh();
                if (!disableServerRead) {
                    self.canFilter = canFilter;
                    self.canSort = canSort;
                }
                return;
            }

            self._requestRead();
        }

        /**
         * Raises the @see:collectionChanged event.
         *
         * @param e Contains a description of the change.
         */
        onCollectionChanged(e?: wijmo.collections.NotifyCollectionChangedEventArgs) {
            if (this._isQuerying() && !(e && e.action == wijmo.collections.NotifyCollectionChangedAction.Add)) {
                return;
            }
            if (e != null && e.action == wijmo.collections.NotifyCollectionChangedAction.Add)
              this.newItems.push(e.item);
            super.onCollectionChanged(e);
        }
                
        // Override _getPageView to get the list that corresponds to the current page
        _getPageView() {
            var self = this;
            if (self._disableServerRead) {
                return super._getPageView();
            }
            return self._view;
        }

        /**
         * Override pageCount to get the total number pages.
         */
        get pageCount(): number {
            var self = this;
            return self.pageSize ? Math.ceil(self.totalItemCount / self.pageSize) : 1;
        }

        /**
         * Raises the @see:pageChanging event.
         *
         * @param e @see:wijmo.collections.PageChangingEventArgs that contains the event data.
         */
        onPageChanging(e: wijmo.collections.PageChangingEventArgs): boolean {
            // Override to process the query when page is changing.
            // Here if it is processed in the _performRefresh method, 
            // the page setting has been changed and the time is too late to send query.
            var result = super.onPageChanging(e);
            if (this._disableServerRead) {
                return result;
            }

            if (result) {
                this._pgIdx = e.newPageIndex;
                this._idx = 0;
                this._requestRead();
                this.onPageChanged();
            }
            return false;
        }

        /**
         * Override commitNew to add the new item to the data source.
         * When @see:IRemoteCollectionViewSettings.createActionUrl or batch editing is
         * used, an ajax request will be sent to server side to update the data source.
         */
        commitNew() {
            var self = this;
            if (self.currentAddItem && self._createActionUrl) {
                self._requestWrite(CommandType.Create, [self.currentAddItem]);
            }
            self._isUpdatingItem = true;
            super.commitNew();
            self._isUpdatingItem = false;
        }

        /**
         * Override commitEdit to modify the item in the database.
         * When @see:IRemoteCollectionViewSettings.updateActionUrl or batch editing is
         * used, an ajax request will be sent to server side to update the data source.
         */
        commitEdit() {
            var self = this, sameContent = self.currentEditItem && self._sameContent(self.currentEditItem, self._edtClone);
            
            if (!sameContent && self._updateActionUrl) {
                var isExistingItem = self.currentEditItem &&
                    (self.newItems.indexOf(self.currentEditItem) == -1 && // not the current new added item at bottom
                        (self.items && self.items.indexOf(self.currentEditItem) > -1)); // make sure we have this item, not the current new added item at top.

                if (isExistingItem) {
                    self._requestWrite(CommandType.Update, [self.currentEditItem]);
                    
                    // TransposedGrid: update to server for source CollectionView under Proxy
                    let cv = self["_srcCv"];
                    if (cv instanceof c1.mvc.collections.RemoteCollectionView) {
                        cv._requestWrite(CommandType.Update, [cv._edtItem]);
                    }
                }
            }
            self._isUpdatingItem = true;
            super.commitEdit();
            self._isUpdatingItem = false;
        }

        /**
         * Override remove to remove the item from the database.
         * When @see:IRemoteCollectionViewSettings.deleteActionUrl or batch editing is
         * used, an ajax request will be sent to server side to update the data source.
         *
         * @param item The item used to be removed.
         */
        remove(item: any) {
            var self = this;

            var index = this.sourceCollection.indexOf(item);
            if (index < 0) {
                // the item does not exist or is already removed.
                return;
            }

            if (self._deleteActionUrl) {
                self._requestWrite(CommandType.Delete, [item]);
            }
            self._isUpdatingItem = true;
            super.remove(item);
            self._isUpdatingItem = false;
        }

        /**
         * Commit all the changes to the database.
         * When the editing url in @see:IRemoteCollectionViewSettings or batch editing is used,
         * an ajax request will be sent to server side to comment the changes to the data source.
         */
        commit() {
            var self = this;

            if (!self._batchEdit) {
                return;
            }

            self.commitEdit();
            self.commitNew();

            if (((self.itemsAdded && self.itemsAdded.length)
                || (self.itemsRemoved && self.itemsRemoved.length)
                || (self.itemsEdited && self.itemsEdited.length))
                && self._batchEditActionUrl) {
                self._requestWrite(CommandType.BatchEdit);
            }
        }

        // process the error information from the server side.
        private _processErrors(res: ICollectionViewResponse) {
            if (!res.success) {
                this._errors.push(res.error);
            }
            if (res.operatedItemResults) {
                res.operatedItemResults.forEach(opItemResult => {
                    if (!opItemResult.success) {
                        this._errors.push(opItemResult.error);
                    }
                });
            }
        }

        private _showErrors() {
            // show the error information for errors.
            // todo
            var self = this;
            if (!self._errors || !self._errors.length) {
                return;
            }
            if (!self.onError(new ErrorEventArgs(self._errors))) {
                return;
            }
            alert(self._errors.join(','));
        }

        // callback for query success
        private _success(res: ICollectionViewResponse) {
            var resData: any[] = res.items || [],
                self = this,
                start: number = res.skip || 0;
            this._ensureDataInfo(resData);
            self._processErrors(res);
            self.onEndQuery();
            if (self._queryList && self._queryList.length) {
                self._sendQuery();
                return;
            }
            self._showErrors();
            self._reset();
            if (!self._disableServerRead) {
            self._pgIdx = res.pageIndex || 0;
            }                                  
            self._fillData(resData, start, res.totalItemCount);
            self.onQueryComplete(new QueryCompleteEventArgs(res));
            self.newItems = [];
        }

        _isPartialDataLoaded() {
            return this._isDynamicalLoadingEnabled() || this.pageSize > 0;
        }

        _isDynamicalLoadingEnabled() {
            return !this.pageSize && !this._disableServerRead && this._initialItemsCount != null;
        }

        _isDisableServerRead() {
            return this._disableServerRead;
        }

        _fillData(items: any[], start: number, totalItemCount: number) {
            var self = this,
                isDataLoadedDanamically = self._isDynamicalLoadingEnabled();

            items = items || [];
            start = start || 0;
            self._totalItemCount = totalItemCount || 0;
            try {
                self._isFillingData = true;
                self.deferUpdate(function () {
                    self.sourceCollection.splice(0, self.sourceCollection.length);
                    if (isDataLoadedDanamically) {
                        self._skip = start;
                        self._top = items.length;
                        self.sourceCollection.length = self._totalItemCount;
                        items.forEach((item, index) => {
                            var si = index + start;
                            if (si >= self._totalItemCount || si < 0) {
                                throw 'The index of item is out of range.';
                            }
                            //override the item with the specified index.
                            if (start > 0 && index == 0) self.sourceCollection[index] = item;
                            self.sourceCollection[si] = item;
                        });

                        if (self._reservedItems && self._reservedItems.length) {
                            self._reservedItems.forEach((item) => {
                                if (item.index >= 0 && item.index < self.sourceCollection.length) {
                                    if (self.sourceCollection[item.index] == null) {
                                        self.sourceCollection[item.index] = item.data;
                                    }
                                }
                            });
                        }
                    } else {
                        items.forEach(item => {
                            self.sourceCollection.push(item);
                        });
                    }

                    if (self._needMoveCurrentToFirst && self.currentPosition < 0) {
                        self.moveCurrentToFirst();
                        self._needMoveCurrentToFirst = false;
                    }

                    self._pendingRefresh = true;
                });
            } finally {
                self._isFillingData = false;
            }
        }

        _clearReservedItems() {
            this._reservedItems.length = 0;
        }

        _addReservedItem(index: number) {
            var item = this.sourceCollection[index];
            this._reservedItems.push({ index: index, data: item });
        }

        private _reset() {
            var self = this;
            if (self.trackChanges) {
                self.clearChanges();
            }
            self._newItem = null;
            self._edtItem = null;
            self._edtClone = null;
            self._errors = [];
        }

        // callback for query fail(network fails)
        private _fail(xhr: XMLHttpRequest, textStatus, errorThrown) {
            this.onEndQuery();

            var errorMessage = '('+textStatus+') ' + errorThrown;
            this.onQueryComplete(new QueryCompleteEventArgs({ success: false, error: errorMessage }));
            throw errorMessage;
        }

        // update the query ajax settings
        private _updateAjaxSettings() {
            this._updateSortAjaxSettings();
            this._upatePageAjaxSettings();
            this._updateQueryAjaxSettings();
        }

        // update ajax setting for page
        private _upatePageAjaxSettings() {
            var self = this, params: ICollectionViewRequest;
            if (!self._requestParams) {
                self._requestParams = {};
            }
            params = self._requestParams;
            if (!self._disableServerRead && self.pageSize) {
                params.pageIndex = self.pageIndex;
                params.pageSize = self.pageSize;
            } else {
                delete params.pageIndex;
                delete params.pageSize;
            }
        }

        // update ajax setting for sort
        private _updateSortAjaxSettings() {
            var self = this, params: ICollectionViewRequest;
            if (!self._requestParams) {
                self._requestParams = {};
            }
            params = self._requestParams;
            if (self.canSort && !self._disableServerRead
                && self.sortDescriptions && self.sortDescriptions.length) {
                params.sortDescriptions = self._getSortDesByExpression();
            } else {
                delete params.sortDescriptions;
            }
        }

        private _getSortDesByExpression() {
            var sortDescriptions = this.sortDescriptions,
                sorts: Array<ISortDescription> = [],
                i = 0,
                sort: wijmo.collections.SortDescription;
            for (; i < sortDescriptions.length; i++) {
                sort = sortDescriptions[i];
                sorts.push({
                    property: sort.property,
                    ascending: sort.ascending,
                    sortNullsFirst: this.sortNullsFirst,
                    sortNulls: this.sortNulls
                });
            }
            return sorts;
        }

        private _updateQueryAjaxSettings() {
            var self = this,
                params: ICollectionViewRequest;

            if (!self._requestParams) {
                self._requestParams = {};
            }
            params = self._requestParams;
            self._collectExtraRequestData(params);

            if (self._isDynamicalLoadingEnabled()) {
                params.skip = self._skip;
                params.top = self._top;
            } else {
                delete params.skip;
                delete params.top;
            }
        }

        private _collectExtraRequestData(params: ICollectionViewRequest) {
            var eaQuery = new QueryEventArgs();
            this.onQueryData(eaQuery);
            params.extraRequestData = eaQuery.extraRequestData;
        }

        // Send ajax request.
        _ajax(settings: any /* IAjaxSettings */) {
            c1.mvc.Utils.ajax(settings);
        }

        /**
         * Get the whole column data.
         *
         * @param column The column name.
         * @param distinct A boolean value indicates whether to get the distinct data records.
         * @param success A function to be called when the response comes back.
         */
        getColumnData(column: string, distinct: boolean, success: ICollectionViewResponseHandler) {
            var self = this, requestParams: any;

            if (!self._requestParams) {
                self._requestParams = {};
            }
            requestParams = self._requestParams;
            self._collectExtraRequestData(requestParams);
            requestParams = c1.mvc.Utils.extend({}, requestParams, { column: column, distinct: distinct });
            self._sendQueryWithoutUpdate(success, requestParams);
        }

        /**
         * Get the whole data.
         *
         * @param success A function to be called when the response comes back.
         */
        getAllData(success: ICollectionViewResponseHandler) {
            var self = this,
                requestParams: any;

            self._updateAjaxSettings();
            self._requestParams.pageIndex = 0;
            self._requestParams.pageSize = self.totalItemCount;
            requestParams = c1.mvc.Utils.extend({}, self._requestParams);
            self._sendQueryWithoutUpdate(success, requestParams);
        }

        _sendQueryWithoutUpdate(success: ICollectionViewResponseHandler, requestParams: any) {
            var self = this, setting: IAjaxSettings = {};
            setting.url = self._readActionUrl;
            if (requestParams) {
                setting.data = requestParams;
            }
            setting.responseTextParsing = this.onReponseTextParsing.bind(this);
            setting.requestDataStringifying = this.onRequestDataStringifying.bind(this);
            if (success) {
                setting.success = function (res: ICollectionViewResponse) {
                    self._processErrors(res);
                    self.onEndQuery();
                    self._showErrors();
                    success.call(self, res);
                };
            }
            self._queryList.push(setting);
            self._sendQuery(false);
        }

        private _sendRequest(url: string, requestParams?, success?: Function) {
            var self = this,
                setting: IAjaxSettings = {};
            self._updateAjaxSettings();
            if (requestParams == null) {
                requestParams = c1.mvc.Utils.extend({}, self._requestParams);
            } else {
                requestParams = c1.mvc.Utils.extend({}, self._requestParams, requestParams);
            }

            setting.url = url;
            if (requestParams) {
                setting.data = requestParams;
            }
            if (success) {
                setting.success = function (res: ICollectionViewResponse) {
                    success.call(self, res);
                };
            }
            setting.responseTextParsing = this.onReponseTextParsing.bind(this);
            setting.requestDataStringifying = this.onRequestDataStringifying.bind(this);
            self._queryList.push(setting);
            self._sendQuery();
        }

        private _sendQuery(updateCollectionView: boolean = true) {
            var self = this, processingQuery: IAjaxSettings, success: Function;
            if (self._isQuerying()) {
                return;
            }
            if (!self._queryList || !self._queryList.length) {
                return;
            }

            self.onBeginQuery();

            processingQuery = self._queryList.shift();
            processingQuery = c1.mvc.Utils.extend({}, {
                async: true,
                type: 'POST',
                dataType: 'json',
                postType: 'json',
                error: self._fail.bind(self)
            }, processingQuery);
            success = processingQuery.success;
            processingQuery.success = function (res: ICollectionViewResponse) {
                if (updateCollectionView) {
                    self._success(res);
                }
                if (success) {
                    success.call(self, res);
                }
            };
            self._ajax(processingQuery);
        }

        /**
         * Prepare the items from startRow to endRow.
         *
         * @param startRow The start row index.
         * @param endRow The end row index.
         */
        requestItems(startRow: number, endRow: number) {
            var self = this,
                count: number,
                rowRange: IRowRange,
                end: number;
            if (!self._isDynamicalLoadingEnabled()) {
                //don't support loading data dynamically
                return;
            }

            // judge whether caches include startRow or EndRow
            // only three times of current view range are used to judge whether the items are queried from server side. 
            count = Math.max(0, endRow - startRow + 1);
            rowRange = self._caculateRangeRow(startRow, endRow, count);
            end = self._skip + self._top - 1;
            if (self._skip <= rowRange.start && end >= rowRange.end) {
                return;
            }
            // Add two times more for quering if the requested items are not in cache.
            rowRange = self._caculateRangeRow(rowRange.start, rowRange.end, count);
            self._skip = rowRange.start;
            self._top = Math.max(0, rowRange.end - rowRange.start + 1);
            self._sendRequest(self._readActionUrl);
        }

        private _caculateRangeRow(startRow: number, endRow: number, count: number): IRowRange {
            startRow = Math.max(0, startRow - count);
            endRow = endRow + count;
            if (endRow >= this.totalItemCount) {
                endRow = this.totalItemCount - 1;
            }
            return { start: startRow, end: endRow };
        }

        // Get Data from server side
        private _requestRead() {
            var self = this;
            if (self._batchEdit) {
                self.commit();
            }
            self._sendRequest(self._readActionUrl);
        }

        private _getWriteUrl(operationType: CommandType) {
            //CommandType[operationType]
            return this['_' + this._toCamelCase(CommandType[operationType]) + 'ActionUrl'] || '';
        }

        private _toCamelCase(text: string): string {
            var camelText: string;
            if (!text) {
                return text;
            }
            camelText = text.charAt(0).toLowerCase();
            if (text.length > 1) {
                camelText = camelText + text.substr(1);
            }
            return camelText;
        }

        private _updateOperatingItems(operatingItems: any, commandType?: string, items?: any) {
            var processedItems;
            if (!items || !items.length) {
                return operatingItems;
            }

            // Add the missing dateKind for Date object.
            processedItems = (<any[]>items).map(item => {
                RemoteCollectionView._processRequestRowData(item, this._dataInfo);
                return item;
            });
            if (commandType) {
                if (!operatingItems) {
                    operatingItems = {};
                }
                operatingItems[commandType] = processedItems;
            } else {
                operatingItems = processedItems;
            }
            return operatingItems;
        }

        /* Send write request to the data service for updating the database */
        public requestWrite(operationType: CommandType, items?: any) {
            if (!this._getWriteUrl(operationType)) { return;}
            return this._requestWrite(operationType, items);
        }
        // Send write request to the data service for updating the database
        private _requestWrite(operationType: CommandType, items?: any) {
            var self = this,
                writeUrl,
                requestParams,
                operatingItems;

            if (operationType === CommandType.BatchEdit) {
                operatingItems = self._updateOperatingItems(operatingItems, 'itemsCreated', self.itemsAdded);
                operatingItems = self._updateOperatingItems(operatingItems, 'itemsDeleted', self.itemsRemoved);
                operatingItems = self._updateOperatingItems(operatingItems, 'itemsUpdated', self.itemsEdited);
            } else {
                // commit the changes by calling the commit method when _batch is True.
                if (self._batchEdit) {
                    return;
                }
                operatingItems = self._updateOperatingItems(operatingItems, null, items);
            }

            if (operatingItems) {
                requestParams = {
                    operatingItems: operatingItems,
                    command: operationType
                };
            }
            writeUrl = self._getWriteUrl(operationType);
            self._sendRequest(writeUrl, requestParams);
        }

        /*
         * Gets the total number of items in the view before paging is applied.
         */
        get totalItemCount() {
            var self = this;
            if (self._disableServerRead) {
                return self._view ? self._view.length : 0;
            }

            return self._totalItemCount || 0;
        }

        _isRemoteMode(): boolean {
            //when the source data is obtained from the readAction Url, it works in remote mode.
            return !!this._readActionUrl;
        }
    }

    /**
     * Extends @see:RemoteCollectionView to support data service provided by control callback.
     *
     * You can use the result objects from the data service as data sources for any Wijmo controls, 
     * and in addition to full CRUD support and real-time updates you automatically get 
     * CollectionView features including sorting, filtering, paging, grouping, and editing. 
     */
    export class CallbackCollectionView extends RemoteCollectionView {

        private _uniqueId: string;
        private _cbk: CallbackManager;

        /**
         * Casts the specified object to @see:c1.mvc.collections.CallbackCollectionView type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): CallbackCollectionView {
            return obj;
        }

        // Initializes a new instance of a @see:CallbackCollectionView.
        // @param options Object with query options (such as take, skip, sort, etc).
        constructor(options: ICallBackCollectionViewSettings) {
            super(options);
            this._uniqueId = options.uniqueId;
        }

        // Require the initial data. For internal use.
        requireInitData(options: ICallBackCollectionViewSettings) {
            var self = this,
                items: any;

            if (options.readActionUrl) {
                super.requireInitData(options);
                return;
            }
            items = options.sourceCollection || options.items || [];
            if (wijmo.isString(items)) {
                // parse the json text.
                items = Utils.jsonParse(<string>items, this.onReponseTextParsing.bind(this));
            }
            this._ensureDataInfo(items);
            self._fillData(items, 0, options.totalItemCount);
        }

        // Gets a CallbackManger of the RemoteCollectionView.
        getCallbackManager(): any /* c1.mvc.CallbackManager */{
            var self = this;
            return self._cbk || (self._cbk = new CallbackManager(self._uniqueId));
        }

        // Send ajax request.
        _ajax(settings: any /* IAjaxSettings */) {
            if (settings.url) {
                super._ajax(settings);
                return;
            }

            this.getCallbackManager().doCallback(settings.data, settings.success, settings.error,
                settings.requestDataStringifying, settings.responseTextParsing);
        }
    }

    // The CallBackCollectionView startup options.
    export interface ICallBackCollectionViewSettings extends IRemoteCollectionViewSettings {
        sourceCollection?: any[];
        items?: any[];
        totalItemCount?: number;
        uniqueId: string;
    }

    /**
     * The @see:RemoteCollectionView startup options.
     */
    export interface IRemoteCollectionViewSettings {

        /**
         * The page index.
         */
        pageIndex?: number;

        /**
         * The page size.
         */
        pageSize?: number;

        /**
         * An array of @see:ISortDescription for sort setting.
         */
        sortDescriptions?: ISortDescription[];

        /**
         * An array of @see:IGroupDescription for group setting.
         */
        groupDescriptions?: IGroupDescription[];

        /**
         * A boolean value indicates whether to get and cache all server side data.
         * True means to get all server side data and cache them in client side,
         * then not to call server side to fetch data for later.
         */
        disableServerRead?: boolean;

        /**
         * The url for creating item.
         */
        createActionUrl?: string;

        /**
         * The url for reading data.
         */
        readActionUrl?: string;

        /**
         * The url for deleting item.
         */
        deleteActionUrl?: string;

        /**
         * The url for updating item.
         */
        updateActionUrl?: string;

        /**
         * The url for batch editing items.
         */
        batchEditActionUrl?: string;

        /**
         * A boolean value indicates whether to edit items in batch.
         */
        batchEdit?: boolean;

        /**
         * The count of the data which are read from server side at first time.
         */
        initialItemsCount?: number;

        /**
         * The queryData event which raises when collect the ajax query data.
         */
        queryData?: wijmo.IEventHandler;

        /**
        * The queryComplete event which raises when the query requests complete.
        */
        queryComplete?: wijmo.IEventHandler;

        /**
         * The error event which raises when there are errors from the server side.
         */
        error?: wijmo.IEventHandler;

        /**
         * The underlying (unfiltered and unsorted) collection.
         */
        sourceCollection?: any;

        /**
         * Occurs when parsing the response text.
         */
        reponseTextParsing?: wijmo.IEventHandler;

        /**
         * Occurs when serializing the request data.
         */
        requestDataStringifying?: wijmo.IEventHandler;

        /**
         * A boolean value indicates whether to refresh all cells after a cell is edited.
         */
        refreshOnEdit?: boolean;
    }

    // The data callback request parameters.
    export interface ICollectionViewRequest {
        pageIndex?: number;
        pageSize?: number;
        sortDescriptions?: ISortDescription[];
        command?: string;
        operatingItems?: any;
        extraRequestData?: any;
        skip?: number;
        top?: number;
    }

    /**
     * The data callback response messages.
     */
    export interface ICollectionViewResponse extends IOperationResult {

        /**
         * The requested data items.
         */
        items?: any[];

        /**
         * The page index.
         */
        pageIndex?: number;

        /**
         * The total item count before paging.
         */
        totalItemCount?: number;

        /**
         * The results of the data operations: create, update, delete and batch edit.
         */
        operatedItemResults?: ICollectionViewItemResult[];

        /**
         * The number of items to skip before returning the request items. 
         */
        skip?: number;

        /**
         * The request one column data.
         */
        columnData?: any[];
    }

    /**
     * Represents a handler of a @see:RemoteCollectionView response.
     */
    export interface ICollectionViewResponseHandler {
        (res: ICollectionViewResponse): void;
    }

    /**
     * The result of operating a CollectionView item.
     */
    export interface ICollectionViewItemResult extends IOperationResult {
        /**
         * The operated item.
         */
        item: any;
    }

    /**
     * The result of an operation.
     */
    export interface IOperationResult {

        /**
         * A boolean value indicates wheter the operation is success.
         */
        success?: boolean;

        /**
         * The error message.
         */
        error?: string;

    }

    /**
     * Options that describes a sort operation.
     */
    export interface ISortDescription {

        /**
         * The field name.
         */
        property: string;

        /**
         * Sort the data by ascending or descending.
         */
        ascending?: boolean;

        /**
         * Gets or sets a value that determines whether null values should appear
         * first or last when the collection is sorted (regardless of sort direction).
         */
        sortNullsFirst?: boolean;

        /**
         * Gets or sets a value that determines how null values should be sorted.
         *
         * This property is set to <b>Last</b> default, which causes null values
         * to appear last on the sorted collection, regardless of sort direction.
         * This is also the default behavior in Excel.
         */
        sortNulls: SortNulls;

    }

    /**
     * Options that describes a group operation.
     */
    export interface IGroupDescription {
        /**
         * The class which is derived from @see:wijmo.collections.GroupDescription.
         */
        clientClass: Function;
    }

    /**
     * Options that describes a group operation by property.
     */
    export interface IPropertyGroupDescription extends IGroupDescription {
        /**
         * The property name.
         */
        propertyName: string;
        /**
         * A callback function that takes an item and a property name and returns 
         * the group name. If not specified, the group name is the property value 
         * for the item.
         */
		converter?: wijmo.collections.IGroupConverter;
    }

    export interface IRowRange {
        start: number;
        end: number;
    }

    export interface _IReservedItem {
        index: number;
        data: any;
    }

    /**
     * The command type of CollectionView's request.
     */
    export enum CommandType {
        /** The type of reading data. */
        Read,

        /** The type of creating data. */
        Create,

        /** The type of updating data. */
        Update,

        /** The type of deleting data. */
        Delete,

        /** The type of batch modifying data. */
        BatchEdit
    }

    /**
     * Specifies constants that define how null values are sorted.
     */
    export enum SortNulls {
        /** Null values are sorted in natural order (first in ascending, last in descending order). */
        Natural = 0,
        /** Null values appear first (regardless of sort order). */
        First = 1,
        /** Null values appear last (regardless of sort order). */
        Last = 2
    }
}