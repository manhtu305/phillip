﻿/// <reference path="../Shared/Grid.CellMaker.ts" />
/// <reference path="../Shared/Grid.ts" />

module c1.mvc.grid.cellmaker {

    /**
     * The @see:CellMaker extends from @see:c1.grid.cellmaker.CellMaker.
    */
    export class CellMaker extends c1.grid.cellmaker.CellMaker {
    }
        
}