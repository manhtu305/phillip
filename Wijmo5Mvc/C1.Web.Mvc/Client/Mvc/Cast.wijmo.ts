﻿module wijmo.Control {
    /**
     * Casts the specified object to @see:wijmo.Control type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Control {
        return obj;
    }
}

module wijmo.CancelEventArgs {
    /**
     * Casts the specified object to @see:wijmo.CancelEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): CancelEventArgs {
        return obj;
    }
}

module wijmo.EventArgs {
    /**
     * Casts the specified object to @see:wijmo.EventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): EventArgs {
        return obj;
    }
}

module wijmo.PropertyChangedEventArgs {
    /**
     * Casts the specified object to @see:wijmo.PropertyChangedEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PropertyChangedEventArgs {
        return obj;
    }
}

module wijmo.RequestErrorEventArgs {
    /**
     * Casts the specified object to @see:wijmo.RequestErrorEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): RequestErrorEventArgs {
        return obj;
    }
}

module wijmo.Tooltip {
    /**
     * Casts the specified object to @see:wijmo.Tooltip type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Tooltip {
        return obj;
    }
}

module wijmo.TooltipEventArgs {
    /**
     * Casts the specified object to @see:wijmo.TooltipEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TooltipEventArgs {
        return obj;
    }
}

