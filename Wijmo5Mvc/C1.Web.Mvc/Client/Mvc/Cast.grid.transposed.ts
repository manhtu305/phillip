﻿module wijmo.grid.transposed.TransposedGrid {
    /**
     * Casts the specified object to @see:wijmo.grid.transposed.TransposedGrid type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TransposedGrid {
        return obj;
    }
}

