﻿module wijmo.grid.grouppanel.GroupPanel {
    /**
     * Casts the specified object to @see:wijmo.grid.grouppanel.GroupPanel type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): GroupPanel {
        return obj;
    }
}

