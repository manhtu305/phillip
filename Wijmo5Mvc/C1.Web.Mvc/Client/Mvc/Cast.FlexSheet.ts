﻿module wijmo.grid.sheet.FlexSheet {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.FlexSheet type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexSheet {
        return obj;
    }
}

module wijmo.grid.sheet.Sheet {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.Sheet type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Sheet {
        return obj;
    }
}

module wijmo.grid.sheet.FlexSheetPanel {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.FlexSheetPanel type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexSheetPanel {
        return obj;
    }
}

module wijmo.grid.sheet.HeaderRow {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.HeaderRow type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): HeaderRow {
        return obj;
    }
}

module wijmo.grid.sheet.DraggingRowColumnEventArgs {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.DraggingRowColumnEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): DraggingRowColumnEventArgs {
        return obj;
    }
}

module wijmo.grid.sheet.RowColumnChangedEventArgs {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.RowColumnChangedEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): RowColumnChangedEventArgs {
        return obj;
    }
}

module wijmo.grid.sheet.UnknownFunctionEventArgs {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.UnknownFunctionEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): UnknownFunctionEventArgs {
        return obj;
    }
}

module wijmo.grid.sheet.Table {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.Table type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Table {
        return obj;
    }
}

module wijmo.grid.sheet.TableColumn {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.TableColumn type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TableColumn {
        return obj;
    }
}

module wijmo.grid.sheet.TableStyle {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.TableStyle type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TableStyle {
        return obj;
    }
}

module wijmo.grid.sheet.ITableSectionStyle {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.ITableSectionStyle type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ITableSectionStyle {
        return obj;
    }
}

module wijmo.grid.sheet.IBandedTableSectionStyle {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.IBandedTableSectionStyle type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): IBandedTableSectionStyle {
        return obj;
    }
}

module wijmo.grid.sheet.FlexSheetFilter {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.FlexSheetFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexSheetFilter {
        return obj;
    }
}

module wijmo.grid.sheet.FlexSheetColumnFilter {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.FlexSheetColumnFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexSheetColumnFilter {
        return obj;
    }
}

module wijmo.grid.sheet.FlexSheetConditionFilter {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.FlexSheetConditionFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexSheetConditionFilter {
        return obj;
    }
}

module wijmo.grid.sheet.FlexSheetValueFilter {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.FlexSheetValueFilter type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexSheetValueFilter {
        return obj;
    }
}

module wijmo.grid.sheet.IFilterSetting {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.IFilterSetting type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): IFilterSetting {
        return obj;
    }
}

module wijmo.grid.sheet.IColumnFilterSetting {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.IColumnFilterSetting type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): IColumnFilterSetting {
        return obj;
    }
}

module wijmo.grid.sheet.IValueFiterSetting {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.IValueFiterSetting type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): IValueFiterSetting {
        return obj;
    }
}

module wijmo.grid.sheet.IConditionFilterSetting {
    /**
     * Casts the specified object to @see:wijmo.grid.sheet.IConditionFilterSetting type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): IConditionFilterSetting {
        return obj;
    }
}

