﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.pdf.d.ts" />
/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.pdf.d.ts" />
module wijmo.grid.pdf.PdfFormatItemEventArgs {
    /**
     * Casts the specified object to @see:wijmo.grid.pdf.PdfFormatItemEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PdfFormatItemEventArgs {
        return obj;
    }
}

