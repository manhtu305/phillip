﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.odata.d.ts" />

/**
 * Defines the controls used to bind with OData service.
 */
module c1.mvc.odata {
    export class ODataCollectionView extends wijmo.odata.ODataCollectionView {
        constructor(opts) {
            // TFS 371411, order pageIndex after groupDescriptions property by removing it and adding again.
            if (opts && opts.hasOwnProperty("options")) {
                var options = opts["options"];
                if (options && options.hasOwnProperty("pageIndex")) {
                    var pageIndex = options["pageIndex"];
                    delete options["pageIndex"];
                    options["pageIndex"] = pageIndex;
                }
            }
            super(opts.url, opts.tableName, opts.options);
        }

        _copy(key: string, value: any): boolean {
            return _ODataInitializer._copy(key, value, this) || super._copy(key, value);
        }
    }

    export class ODataVirtualCollectionView extends wijmo.odata.ODataVirtualCollectionView {
        constructor(opts) {
            super(opts.url, opts.tableName, opts.options);
        }

        _copy(key: string, value: any): boolean {
            return _ODataInitializer._copy(key, value, this) || super._copy(key, value);
        }
    }

    export class _ODataInitializer {
        static _copy(key: string, value: any, cv: wijmo.odata.ODataCollectionView): boolean {
            if (key == 'sortDescriptions') {
                if (value) {
                    value.forEach(sort => cv.sortDescriptions.push(new wijmo.collections.SortDescription(sort.property, sort.ascending)));
                }
                return true;
            }

            if (key == 'groupDescriptions') {
                if (value) {
                    value.forEach(group => {
                        let groupClass = group.clientClass;
                        let groupDescription = groupClass ? new groupClass(group.propertyName, group.converter)
                            : new wijmo.collections.PropertyGroupDescription(group.propertyName, group.converter);
                        cv.groupDescriptions.push(groupDescription);
                    });
                }
                return true;
            }

            if (key == 'pageIndex') {
                cv._pgIdx = Math.max(0, value);
                return true;
            }

            return false;
        }

        static _getCtorOptions(opts) {
            let serviceUrl = '',
                tableName = '';
            if (opts) {
                serviceUrl = serviceUrl || opts.serviceUrl;
                delete opts.serviceUrl;
                tableName = tableName || opts.tableName;
                delete opts.tableName;
            }

            return {
                url: serviceUrl,
                tableName: tableName,
                options: opts
            };
        }
    }


}