﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.finance.analytics.d.ts"/>
/// <reference path="Control.ts" />
/// <reference path="../Shared/Chart.ts" />

/**
 * Defines the @see:c1.mvc.chart.FlexChart control and its associated classes.
 */
module c1.mvc.chart {

    export class _FlexChartBaseInitializer extends _Initializer {
        _beforeInitializeControl(options: any): void {
            var self = this;
            if (options.itemsSource instanceof wijmo.collections.CollectionView) {
                var cv = <wijmo.collections.CollectionView>options.itemsSource;
                if (DataSourceManager._isRemoteSource(cv) && cv.isEmpty) {
                    var wjCtrl = <wijmo.chart.FlexChartBase>this.control;
                    self._beforeBeginUpdate(options);
                    wjCtrl.beginUpdate();
                    var endUpdate = () => {
                        self._beforeEndUpdate(options);
                        wjCtrl.endUpdate();
                        setTimeout(() => {
                            cv.collectionChanged.removeHandler(endUpdate);
                        }, 0);
                    };
                    cv.collectionChanged.addHandler(endUpdate);
                }
            }
        }

        _beforeBeginUpdate(options: any): void {
        }

        _beforeEndUpdate(options: any): void {
        }
    }

    export class _FlexChartBaseWrapper extends _ControlWrapper {
    }

    export class _FlexPieInitializer extends _FlexChartBaseInitializer {
        private _selectedIndex: number;

        _afterInitializeControl(options: any) {
            super._afterInitializeControl(options);
            var flexPie = <FlexPie>this.control;
            flexPie._isInitialized = true;

            // refresh the control after initialize
            flexPie.invalidate();
        }

        _beforeBeginUpdate(options: any): void {
            this._selectedIndex = -1;
            if(options && options.selectedIndex) {
                this._selectedIndex = options.selectedIndex;
                delete options.selectedIndex;
            }
        }

        _beforeEndUpdate(options: any): void {
            if(this._selectedIndex != -1)
            {
                let selectionChangedEvent = (<wijmo.chart.FlexChartBase>this.control).selectionChanged;
             //   (<wijmo.chart.FlexChartBase>this.control).selectionChanged = new wijmo.Event();
             //   (<wijmo.chart.FlexPie>this.control).selectedIndex = this._selectedIndex;
             //   (<wijmo.chart.FlexChartBase>this.control).selectionChanged = selectionChangedEvent;

				// selectionChanged was set to readonly in Wijmo 637. resolved by change control to Writeable object.
				const writeableControl = this.control as Writeable<wijmo.chart.FlexChartBase>;
				writeableControl.selectionChanged = new wijmo.Event();
				(<wijmo.chart.FlexPie>this.control).selectedIndex = this._selectedIndex;
				writeableControl.selectionChanged = selectionChangedEvent;

            }
        }
    }

    export class _FlexPieWrapper extends _FlexChartBaseWrapper {
        get _initializerType(): any {
            return c1.mvc.chart._FlexPieInitializer;
        }
		get _controlType(): any {
            return c1.mvc.chart.FlexPie;
        }
    }

    /**
     * The @see:FlexPie control provides pie and doughnut charts with selectable slices.
     */
    export class FlexPie extends c1.chart.FlexPie {
        _isInitialized: boolean;

        refresh(fullUpdate = true) {
            // postpone the refresh if the control is not intialized.
            if (!this._isInitialized) return;

            super.refresh(fullUpdate);
        }
    }

    export class _FlexChartCoreInitializer extends _FlexChartBaseInitializer {
        private _selectedIndex: number;
        private _extraSeriesList: any[] = [];// trendline,etc
        private _seriesRenderingList: Function[];
        private _seriesLength: number;

        _beforeInitializeControl(options: any): void {
            var self = this;

            super._beforeInitializeControl(options);
            self._prepareInitialOptions(options);
            self._seriesLength = options.series ? options.series.length : 0;
            self._prepareExtraSeriesList(options);
        }

        _afterInitializeControl(options: any): void {
            var self = this,
                selection,
                seriesRendering,
                wjCtrl = <wijmo.chart.FlexChartCore>self.control,
                series = wjCtrl.series,
                i: number,
                extraSeries: any;

            self._setPlotArea(options.axisX, wjCtrl.axisX);
            self._setPlotArea(options.axisY, wjCtrl.axisY);

            if (self._extraSeriesList && self._extraSeriesList.length > 0) {
                wjCtrl.beginUpdate();
                for (i = 0; i < self._seriesLength; i++) {
                    extraSeries = self._extraSeriesList[i];
                    if (extraSeries) {
                        series.splice(i, 0, extraSeries);
                    }
                }
                wjCtrl.endUpdate();
            }

            for (i = 0; i < self._seriesLength; i++) {
                seriesRendering = self._seriesRenderingList[i];
                if (seriesRendering && typeof seriesRendering === "function") {
                    series[i].rendering.addHandler(seriesRendering);
                }
                self._setPlotArea(series[i].axisX, series[i].axisX);
                self._setPlotArea(series[i].axisY, series[i].axisY);
            }

            if (self._selectedIndex == null) {
                return;
            }

            if (options.selectionMode === wijmo.chart.SelectionMode.Series &&
                series && series[self._selectedIndex]) {
                selection = series[self._selectedIndex];
                wjCtrl["selection"] = selection;
            }
        }

        _setPlotArea(opt: any, axis: any): void {
            var wjCtrl = <wijmo.chart.FlexChartCore>this.control;
            if (opt && opt._plotAreaIndex != null) {
                axis.plotArea = wjCtrl.plotAreas[opt._plotAreaIndex];
            }
        }

        _prepareInitialOptions(options: any) {
            var self = this;
            if (!options) {
                return;
            }

            self._prepareExtraOptions(options);

            self._prepareAxes(options.axisX);
            if (options.axisX) {
                _addEvent(self.control["axisX"]["rangeChanged"], options.axisX["rangeChanged"]);
                delete options.axisX["rangeChanged"];
            }

            if (options.axisY) {
                self._prepareAxes(options.axisY);
                _addEvent(self.control["axisY"]["rangeChanged"], options.axisY["rangeChanged"]);
                delete options.axisY["rangeChanged"];
            }

            self._prepareSelection(options);
            self._prepareTitleContent(options);
            self._prepareSeriesAxes(options.series);
            self._preparePloteAreas(options._plotAreas);
        }

        _preparePloteAreas(plotAreas: any): void {
            if (!plotAreas) {
                return;
            }
            var arr = wijmo.asArray(plotAreas),
                wjCtrl = <wijmo.chart.FlexChartCore>this.control;
            for (var i = 0; i < arr.length; i++) {
                var s = new wijmo.chart.PlotArea();
                wijmo.copy(s, arr[i]);
                wjCtrl.plotAreas.push(s);
            }
        }

        _prepareExtraOptions(options: any) {
            throw "Should implement this method in the derived class.";
        }

        _prepareAxes(axis) {
            if (!axis || !axis.itemsSource) {
                return;
            }
            axis.itemsSource = axis.itemsSource.items || axis.itemsSource;
        }

        _prepareSelection(opts) {
            var i = 0,
                series = opts ? opts.series : undefined;
            this._selectedIndex = opts ? opts.selectionIndex : undefined;
            delete opts.selectionIndex;

            this._seriesRenderingList = [];
            if (series && series.length) {
                for (; i < series.length; i++) {
                    this._seriesRenderingList[i] = series[i].rendering;
                    delete series[i].rendering;
                }
            }
        }

        _prepareTitleContent(opts) {
            if (opts && opts.tooltip && opts.tooltip.content) {
                var content = opts.tooltip.content;
                if (typeof window[content] === "function") {
                    opts.tooltip.content = window[content];
                }
            }
        }

        _prepareSeriesAxes(series) {
            var self = this, i, xAxis, yAxis;
            if (!series || !series.length) {
                return;
            }
            for (i = 0; i < series.length; i++) {
                xAxis = self._createAxisObj(series[i].axisX, wijmo.chart.Position.Bottom);
                if (xAxis) {
                    series[i].axisX = xAxis;
                }
                yAxis = self._createAxisObj(series[i].axisY, wijmo.chart.Position.Left);
                if (yAxis) {
                    series[i].axisY = yAxis;
                }
            }
        }

        private _createAxisObj(aOpts, defaultPosition: wijmo.chart.Position) {
            var self = this,
                axis: wijmo.chart.Axis,
                commentOptKeys = [
                // "position",
                    "min", "max", "axisLine", "format", "itemFormatter", "labelAngle",
                    "labels", "majorGrid", "minorGrid", "origin", "reversed", "majorUnit", "minorUnit",
                    "name", "title", "majorTickMarks", "minorTickMarks", "overlappingLabels", "binding", "itemsSource", "_plotAreaIndex"],
                i, value;

            if (!aOpts) {
                return undefined;
            }
            switch (aOpts["position"]) {
                case "None":
                case 0:
                    axis = new wijmo.chart.Axis(0);
                    break;
                case "Left":
                case 1:
                    axis = new wijmo.chart.Axis(1);
                    break;
                case "Top":
                case 2:
                    axis = new wijmo.chart.Axis(2);
                    break;
                case "Right":
                case 3:
                    axis = new wijmo.chart.Axis(3);
                    break;
                case "Bottom":
                case 4:
                    axis = new wijmo.chart.Axis(4);
                    break;
                default:
                    axis = new wijmo.chart.Axis(defaultPosition);
                    break;
            }

            self._prepareAxes(aOpts);

            for (i = 0; i < commentOptKeys.length; i++) {
                value = aOpts[commentOptKeys[i]];
                if (value != null) {
                    axis[commentOptKeys[i]] = value;
                }
            }

            _addEvent(axis["rangeChanged"], aOpts["rangeChanged"]);
            delete aOpts["rangeChanged"];
            return axis;
        }

        _prepareExtraSeriesList(options) {
            var self = this, i,
                seriesList = options.series ? options.series.concat() : null, series;
            if (!seriesList || !seriesList.length) {
                return;
            }

            series = [];
            for (i = 0; i < seriesList.length; i++) {
                if (seriesList[i]["extraSeriesTypeName"]) {
                    if (!seriesList[i].bindingX) {
                        seriesList[i].bindingX = options.bindingX;
                    }
                    // BreakEven in wijmo uses strokeWidth instead of stroke-width
                    // Here replace stroke-width with strokeWidth
                    if (seriesList[i]["extraSeriesTypeName"] == "BreakEven") {
                        const oldName = "stroke-width";
                        const newName = "strokeWidth";
                        this._changePropertyName(seriesList[i].style, oldName, newName);
                        this._changePropertyName(seriesList[i].altStyle, oldName, newName);
                        this._changePropertyName(seriesList[i].symbolStyle, oldName, newName);
                        for (const property in seriesList[i].styles) {
                            this._changePropertyName(seriesList[i].styles[property], oldName, newName);
                        }
                    }
                    self._extraSeriesList[i] = self._createExtraSeriesObj(seriesList[i]);
                } else {
                    series.push(seriesList[i]);
                }
            }

            options.series = series;
        }

        private _changePropertyName(obj: object, oldName: string, newName: string) {
            if (!obj || oldName === newName) return;
            if (obj.hasOwnProperty(oldName)) {
                obj[newName] = obj[oldName];
                delete obj[oldName];
            }
        }

        private _createExtraSeriesObj(series) {
            var i,
                extraSeries,
                extraSeriesTypeName = series["extraSeriesTypeName"],
                funcNames = ["func", "xFunc", "yFunc"], func;

            delete series["extraSeriesTypeName"];

            for (i = 0; i < funcNames.length; i++) {
                func = series[funcNames[i]];
                if (func && typeof window[func] === "function") {
                    series[funcNames[i]] = window[func];
                }
            }

            switch (extraSeriesTypeName) {
                case "TrendLine":
                    extraSeries = new wijmo.chart.analytics.TrendLine();
                    break;
                case "MovingAverage":
                    extraSeries = new wijmo.chart.analytics.MovingAverage();
                    break;
                case "YFunctionSeries":
                    extraSeries = new wijmo.chart.analytics.YFunctionSeries();
                    break;
                case "ParametricFunctionSeries":
                    extraSeries = new wijmo.chart.analytics.ParametricFunctionSeries();
                    break;
                case "Fibonacci":
                    extraSeries = new wijmo.chart.finance.analytics.Fibonacci();
                    break;
                case "FibonacciArcs":
                    extraSeries = new wijmo.chart.finance.analytics.FibonacciArcs();
                    break;
                case "FibonacciFans":
                    extraSeries = new wijmo.chart.finance.analytics.FibonacciFans();
                    break;
                case "FibonacciTimeZones":
                    extraSeries = new wijmo.chart.finance.analytics.FibonacciTimeZones();
                    break;
                case "BollingerBands":
                    extraSeries = new wijmo.chart.finance.analytics.BollingerBands();
                    break;
                case "ATR":
                    extraSeries = new wijmo.chart.finance.analytics.ATR();
                    break;
                case "CCI":
                    extraSeries = new wijmo.chart.finance.analytics.CCI();
                    break;
                case "Envelopes":
                    extraSeries = new wijmo.chart.finance.analytics.Envelopes();
                    break;
                case "Macd":
                    extraSeries = new wijmo.chart.finance.analytics.Macd();
                    break;
                case "MacdHistogram":
                    extraSeries = new wijmo.chart.finance.analytics.MacdHistogram();
                    break;
                case "Stochastic":
                    extraSeries = new wijmo.chart.finance.analytics.Stochastic();
                    break;
                case "WilliamsR":
                    extraSeries = new wijmo.chart.finance.analytics.WilliamsR();
                    break;
                case "RSI":
                    extraSeries = new wijmo.chart.finance.analytics.RSI();
                    break;
                case "Waterfall":
                    extraSeries = new wijmo.chart.analytics.Waterfall();
                    break;
                case "BoxWhisker":
                    extraSeries = new wijmo.chart.analytics.BoxWhisker();
                    break;
                case "ErrorBar":
                    extraSeries = new wijmo.chart.analytics.ErrorBar();
                    break;
                case "BreakEven":
                    extraSeries = new wijmo.chart.analytics.BreakEven();
                    break;
            }

            wijmo.copy(extraSeries, series);

            return extraSeries;
        }
    }

    export class _FlexChartCoreWrapper extends _FlexChartBaseWrapper {
        get _initializerType(): any {
            return c1.mvc.chart._FlexChartCoreInitializer;
        }
    }

    export class _FlexChartInitializer extends _FlexChartCoreInitializer {
        _prepareExtraOptions(options: any) {
            var extraOpts = options["options"], max, min,
                newExtraOpts = {};
            if (!extraOpts) {
                return;
            }

            max = extraOpts.bubbleMaxSize;
            min = extraOpts.bubbleMinSize;
            if (max != null || min != null) {
                newExtraOpts["bubble"] = { minSize: min, maxSize: max };
            }

            if (extraOpts.funnel != null) {
                newExtraOpts["funnel"] = extraOpts.funnel;
            }

            if (extraOpts.step != null) {
                newExtraOpts["step"] = extraOpts.step;
            }

            options["options"] = newExtraOpts;
        }
    }

    export class _FlexChartWrapper extends _FlexChartCoreWrapper {
        get _initializerType(): any {
            return c1.mvc.chart._FlexChartInitializer;
        }

        get _controlType(): any {
            return c1.mvc.chart.FlexChart;
        }
    }

    /**
     * The @see:FlexChart control provides a powerful and flexible way to visualize data.
     */
    export class FlexChart extends c1.chart.FlexChart {
    }
}