﻿/// <reference path="Collections.ts" />
/// <reference path="Grid.ts" />

module c1.mvc.grid {

    // The extension which collects extra sorting settings.
    export class _SortHelper {
        private readonly _grid: wijmo.grid.FlexGrid & _IFlexGridMvc;
        private _originalColumns: wijmo.grid.Column[];
        private _sortingOriginalColumnIndex: number;
        private _sortingRowIndex: number;
        private _collectionView: wijmo.collections.ICollectionView;

        // Initializes a new instance of the @see:_SortHelper.
        // @param grid The @see:wijmo.grid.FlexGrid.
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc) {
            this._grid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);

            var cv = this._grid.collectionView;
            if (!cv || cv instanceof wijmo.odata.ODataCollectionView) {
                return;
            }

            _overrideMethod(grid, _Initializer._INITIAL_NAME,
                null, this._afterInitialize.bind(this));

            // the columns may be changed if cannot bind the column on initializing.
            _overrideMethod(grid, '_bindColumns',
                null, this._afterBindColumns.bind(this));

            _overrideMethod(grid, 'onSortingColumn',
                null, this._afterOnSortingColumn.bind(this));
        }

        private _afterInitialize(options) {
            if (!options) {
                return;
            }

            this._storeColumns();
            this._addHandlers();
            this._grid.itemsSourceChanged.addHandler(() => {
                this._removeHandlers();
                this._addHandlers();
            });
        }

        private _addHandlers() {
            this._collectionView = this._grid.collectionView;
            if (this._isServerReadRemoteCollectionView()) {
                var cv: c1.mvc.collections.RemoteCollectionView = wijmo.tryCast(this._collectionView, c1.mvc.collections.RemoteCollectionView);
                cv.queryData.addHandler(this._collectSortSettings, this);
            }
        }

        private _removeHandlers() {
            if (this._isServerReadRemoteCollectionView()) {
                var cv: c1.mvc.collections.RemoteCollectionView = wijmo.tryCast(this._collectionView, c1.mvc.collections.RemoteCollectionView);
                cv.queryData.removeHandler(this._collectSortSettings, this);
            }
        }

        private _isServerReadRemoteCollectionView(): boolean {
            var cv: c1.mvc.collections.RemoteCollectionView = wijmo.tryCast(this._collectionView, c1.mvc.collections.RemoteCollectionView);
            return cv && cv._isDisableServerRead && !cv._isDisableServerRead(); 
        }

        private _afterBindColumns() {
            this._storeColumns();
        }

        private _storeColumns() {
            // store the columns collection to keep them in original order
            this._originalColumns = [];
            for (var i = 0; i < this._grid.columns.length; i++) {
                this._originalColumns.push(this._grid.columns[i]);
            };
        }

        private _afterOnSortingColumn(e: wijmo.grid.CellRangeEventArgs): void {
            var col: wijmo.grid.Column;

            if (!e.cancel) {
                this._sortingRowIndex = e.row;
                col = this._grid.columns[e.col];
                this._sortingOriginalColumnIndex = this._originalColumns.indexOf(col);
            }
        }

        private _collectSortSettings(sender: c1.mvc.collections.RemoteCollectionView, e: c1.mvc.collections.QueryEventArgs) {
            if (!sender.sortDescriptions || !sender.sortDescriptions.length) {
                return;
            }
            var sortSettings = this._getSortSettings();
            if (!sortSettings || !sortSettings.length) {
                return;
            }

            e.extraRequestData = e.extraRequestData || {};
            e.extraRequestData[this._grid._getSortDescriptorKeyMvc()] = sortSettings;
        }

        private _getSortSettings(): _IColumnInfo[] {
            var cell: _IGroupCell, col: wijmo.grid.Column,
                sortSettings: _IColumnInfo[] = [], columnInfo: _IColumnInfo;

            if (this._sortingOriginalColumnIndex != null) {
                cell = this._grid._getSortCellMvc(this._sortingRowIndex, this._sortingOriginalColumnIndex, this._originalColumns[this._sortingOriginalColumnIndex]);
                col = cell.cell;
                if (col && (col.sortMemberPath == null || col.sortMemberPath == col.binding)
                    && (col.dataMap && col.dataMap.sortByDisplayValues)) {
                    columnInfo = {
                        group: cell.groupIndex,
                        index: cell.cellIndex,
                        binding: col.binding,
                        dataMap: {
                            selectedValuePath: col.dataMap.selectedValuePath,
                            displayMemberPath: col.dataMap.displayMemberPath
                        }
                    };
                    sortSettings.push(columnInfo);
                }
            }

            return sortSettings;
        }
    }

    // Represents a column info that describes a column on the grid.
    // It's used to pass extra data for sorting.
    interface _IColumnInfo {
        group?: number;
        index: number;
        binding: string;
        dataMap: _IDataMapInfo;
    }

    // Represents a data map info for use with the _IColumnInfo's dataMap property.
    // It's used to pass extra data for sorting.
    interface _IDataMapInfo {
        selectedValuePath: string;
        displayMemberPath: string;
    }
}