﻿/// <reference path="Control.ts" />
/// <reference path="Template.ts" />
/// <reference path="../Shared/Input.ts" />

/*
 * Defines input controls for strings, numbers, dates, times, and colors.
 */
module c1.mvc.input {

    export class _PagerWrapper extends _ControlWrapper {
        get _controlType(): any {
            return Pager;
        }
    }

    /**
     * The @see:Pager control displays a pager navigator for the @see:wijmo.collections.CollectionView.
     */
    export class Pager extends c1.input.Pager {
        /**
         * Casts the specified object to @see:c1.mvc.input.Pager type.
         * @param obj The object to cast.
         * @return The object passed in.
        */
        static cast(obj: any): Pager {
            return obj;
        }

        initialize(options: any) {
            var owner: string,
                ownerControl: wijmo.Control,
                cv: wijmo.collections.CollectionView;

            if (options) {
                owner = options["owner"];
            } else {
                return;
            }

            if (owner) {
                ownerControl = wijmo.Control.getControl("#" + owner);
                if (ownerControl && ownerControl["collectionView"]) {
                    options["collectionView"] = ownerControl["collectionView"];
                } else {
                    cv = wijmo.tryCast(c1.getService(owner), wijmo.collections.CollectionView);
                    if (cv) {
                        options["collectionView"] = cv;
                    }
                }
            }

            delete options["owner"];
            super.initialize(options);
        }
    }

    export class _FormInputInitializer extends c1.mvc._Initializer {

        private _hidden: HTMLInputElement;

        _beforeInitializeControl(options: any) {
            var self = this,
                control = <wijmo.Control>self.control,
                textbox = control['_tbx'],
                name = textbox.getAttribute("wj-name"),
                hidden = self._hidden = document.createElement('input');

            hidden.name = name;
            hidden.type = 'text';
            hidden.style.visibility = "hidden";
            hidden.style.position = "absolute";

            if (options.validationAttributes) {
                for (var att in options.validationAttributes) {
                    hidden.setAttribute(att, options.validationAttributes[att]);
                }
                delete options.validationAttributes;
            }

            textbox.parentNode.insertBefore(hidden, textbox);
            Utils.forwardValidationEvents(control, textbox, hidden);

            if (options.isRequired != null) {
                // "isRequired" must be set before set "value".  So we set it in the first place.
                control["isRequired"] = options.isRequired;
                delete options.isRequired;
            }
        }

        _afterInitializeControl(options: any, valueName: string = 'value', valueChangedEventName: string = 'valueChanged') {
            var self = this, control = self.control, hidden = self._hidden;

            // handle hidden field
            if (hidden) {
                hidden.value = self._processInputValue(control[valueName]);
                control[valueChangedEventName].addHandler(() => {
                    hidden.value = self._processInputValue(control[valueName]);
                    Utils.triggerValidationChangeEvent(hidden);
                });
            }
        }

        private _processInputValue(value: any): any {
            return wijmo.isDate(value) ? wijmo.Globalize.formatDate(value, "yyyy/MM/dd HH:mm:ss") : value;
        }
    }

    export class _ItemsInputInitializer extends c1.mvc._Initializer {
        private _template: Template;

        _beforeInitializeControl(options: any) {
            this._template = options.itemTemplateId || options.itemTemplateContent,
            delete options.itemTemplateId;
            delete options.itemTemplateContent;
        }

        _afterInitializeControl(options: any) {
            var self = this,
                wjControl = self.control,
                lbx: wijmo.input.ListBox = wjControl["itemsChanged"] ? wjControl : wjControl["_lbx"],
                timeout = 15,
                template = this._template;

            if (template) {
                lbx.isContentHtml = true;
                lbx.itemsChanged.addHandler(() => {
                    self._transformItems(template);
                });
                // transform template both at once and at timeout.
                self._transformItems(template);
                setTimeout(() => {
                    self._transformItems(template);
                }, timeout);

                if (wjControl instanceof wijmo.input.ComboBox) {
                    wjControl.isDroppedDownChanged.addHandler(() => {
                        if ((<wijmo.input.ComboBox>wjControl).isDroppedDown) {
                            self._transformItems(template);
                        }
                    });
                }

                if (wjControl instanceof wijmo.input.ListBox) {
                    _overrideMethod(wjControl, "refresh", null, () => {
                        self._transformItems(template);
                    });
                }
            }
        }

        private _transformItems(template: Template) {
            var self = this,
                wjControl = self.control,
                lbx: wijmo.input.ListBox = wjControl["itemsChanged"] ? wjControl : wjControl["_lbx"],
                i: number,
                item: HTMLElement,
                data: any;

            for (i = 0; i < lbx.hostElement.childElementCount; i++) {
                item = <HTMLElement>lbx.hostElement.children.item(i);
                data = {};
                data[Template.UID] = _getUniqueId(self) + "_item" + i;
                data[Template.DATACONTEXT] = lbx.collectionView.items[i];
                template.applyTo(item, data);
            }
        }
    }

    export class _DropDownWrapper extends _ControlWrapper {
    }

    export class _ComboBoxWrapper extends _DropDownWrapper {
        get _initializerType(): any {
            return _ItemsInputInitializer;
        }

        get _controlType(): any {
            return ComboBox;
        }
    }

    /**
     * The @see:ComboBox control extends from @see:c1.input.ComboBox.
     */
    export class ComboBox extends c1.input.ComboBox {
        private _hiddenInput: HTMLInputElement;
        private _validationAttributes: any;

        initialize(options: any) {
            if (options) {
                if (options.validationAttributes) {
                    this._validationAttributes = options.validationAttributes;
                    delete options.validationAttributes;
                }
                // Handle initial selectedValue for RemoteCollectionView
                var cv = options.itemsSource instanceof wijmo.collections.CollectionView
                    ? <wijmo.collections.CollectionView>options.itemsSource : null;
                var selectedValue, selectedIndex, selectedItem;
                if (DataSourceManager._isRemoteSource(cv)) {
                    var needProcessSelections = false;
                    if (typeof (options.selectedValue) != 'undefined') {
                        selectedValue = options.selectedValue;
                        delete options.selectedValue;
                        needProcessSelections = true;
                    }

                    if (typeof (options.selectedItem) != 'undefined') {
                        selectedItem = options.selectedItem;
                        delete options.selectedItem;
                        needProcessSelections = true;
                    }

                    if (typeof (options.selectedIndex) != 'undefined') {
                        selectedIndex = options.selectedIndex;
                        delete options.selectedIndex;
                        needProcessSelections = true;
                    }

                    if (needProcessSelections) {
                        var processSelections = () => {
                            if (typeof (selectedValue) != 'undefined') {
                                this.selectedValue = selectedValue;
                            }
                            if (typeof (selectedItem) != 'undefined') {
                                this.selectedItem = selectedItem;
                            }
                            if (typeof (selectedIndex) != 'undefined') {
                                this.selectedIndex = selectedIndex;
                            }
                            setTimeout(() => {
                                cv.collectionChanged.removeHandler(processSelections);
                            }, 0);
                        };
                        cv.collectionChanged.addHandler(processSelections);
                    }
                }
            }

            super.initialize(options);
            // handle hidden field
            this._handleHiddenField();
        }

        /**
         * Gets or sets a value that enables or disables editing of the text 
         * in the input element of the @see:ComboBox (defaults to false).
         */
        get isEditable(): boolean {
            return this._editable;
        }
        set isEditable(value: boolean) {
            if (value !== this._editable) {
                this._editable = wijmo.asBoolean(value);
                this._updateHiddenInput();
            }
        }

        _handleHiddenField() {
            var self = this,
                name = self.hostElement.getAttribute("wj-name"),
                hidden = self._hiddenInput;

            if (!hidden) {
                hidden = document.createElement('input');
                hidden.type = 'text';
                hidden.name = name;
                hidden.style.visibility = "hidden";
                hidden.style.position = "absolute";

                self._tbx.parentNode.insertBefore(hidden, self._tbx);
                Utils.forwardValidationEvents(self, self._tbx, hidden, true);
                self._hiddenInput = hidden;
            }

            if (self._validationAttributes) {
                for (var att in self._validationAttributes) {
                    hidden.setAttribute(att, self._validationAttributes[att]);
                }
            }

            self._updateHiddenInput();
        }

        _onTextchanged() {
            var self = this;
            if (!self._hiddenInput) {
                return;
            }

            self._hiddenInput.value = self.text;
            if (self.isEditable) {
                Utils.triggerValidationChangeEvent(self._hiddenInput);
            }
        }

        _onSelectedIndexChanged() {
            var self = this;
            if (!self._hiddenInput) {
                return;
            }

            self._hiddenInput.value = self.selectedValue;
            Utils.triggerValidationChangeEvent(self._hiddenInput);
        }

        _updateHiddenInput() {
            var self = this, hidden = self._hiddenInput;

            if (!hidden) {
                return;
            }

            if (self.isEditable) {
                hidden.value = self.text;
                if (self.selectedIndexChanged.hasHandlers) {
                    self.selectedIndexChanged.removeHandler(self._onSelectedIndexChanged, self);
                }

                self.textChanged.addHandler(self._onTextchanged, self);
            } else {
                hidden.value = self.selectedValue;
                if (self.textChanged.hasHandlers) {
                    self.textChanged.removeHandler(self._onTextchanged, self);
                }

                self.selectedIndexChanged.addHandler(self._onSelectedIndexChanged, self);
            }
        }
    }

    export class _AutoCompleteWrapper extends _ComboBoxWrapper {
        get _controlType(): any {
            return AutoComplete;
        }

        get _initializerType(): any {
            return _AutoCompleteInitializer;
        }
    }

    export class _AutoCompleteInitializer extends _ItemsInputInitializer {
        _hiddenElement: HTMLElement;
        private _validationAttributes: any;
        private _textPortToSelectedValue = false;

        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);
            if (!options) {
                return;
            }

            this._processOptions(options);
            this._handleHiddenElement();
        }

        _processOptions(options: any) {
            var actionUrl = options.itemsSourceAction;
            if (actionUrl) {
                options.itemsSourceFunction = (query, max, callback) => {
                    Utils.ajaxGet({
                        url: actionUrl,
                        success: response => {
                            callback(response);
                        },
                        data: { query: query, max: max }
                    });
                };
                delete options.itemsSourceAction;
            }

            if (options.validationAttributes) {
                this._validationAttributes = options.validationAttributes;
                delete options.validationAttributes;
            }

            // the value is serialized as text if editable
            var editable = options.isEditable == undefined || options.isEditable === true;
            if (editable && options.displayMemberPath != options.selectedValuePath) {
                if (options.text != undefined) {
                    this._textPortToSelectedValue = true;
                    options.selectedValue = options.text;
                    delete options.text;
                }
            }
        }

        _createHiddenElement(): HTMLElement {
            var ele = document.createElement('input');
            ele['type'] = 'text';
            ele.name = (<wijmo.Control>this.control).hostElement.getAttribute('wj-name');
            ele.style.visibility = "hidden";
            ele.style.position = "absolute";
            return ele;
        }

        _handleHiddenElement() {
            var wijContrl = <wijmo.input.DropDown>this.control,
                controlInput = wijContrl._tbx;

            if (this._hiddenElement) {
                return;
            }

            this._hiddenElement = this._createHiddenElement();
            if (this._validationAttributes) {
                for (var att in this._validationAttributes) {
                    this._hiddenElement.setAttribute(att, this._validationAttributes[att]);
                }
            }

            controlInput.parentNode.insertBefore(this._hiddenElement, controlInput);
            Utils.forwardValidationEvents(wijContrl, controlInput, this._hiddenElement);
        }

        _updateHiddenField() {
            var input = <HTMLInputElement>this._hiddenElement,
                autoComplete = <wijmo.input.AutoComplete>this.control;

            autoComplete.selectedIndexChanged.removeHandler(this._onSelectedIndexChanged, this);
            autoComplete.textChanged.removeHandler(this._onTextchanged, this);

            if (autoComplete.isEditable) {
                input.value = autoComplete.text;
                autoComplete.selectedIndexChanged.addHandler(this._onSelectedIndexChanged, this);
                autoComplete.textChanged.addHandler(this._onTextchanged, this);
            } else {
                input.value = autoComplete.selectedValue;
                autoComplete.selectedIndexChanged.addHandler(this._onSelectedIndexChanged, this);
            }
        }

        _onTextchanged() {
            var input = <HTMLInputElement>this._hiddenElement,
                autoComplete = <wijmo.input.AutoComplete>this.control;
            if (!input) {
                return;
            }

            if (autoComplete.selectedIndex > -1) {
                if (autoComplete.text != autoComplete.getDisplayText()) {
                    // on typing
                    input.value = autoComplete.text;
                } else {
                    input.value = autoComplete.selectedValue;
                }
            } else {
                input.value = autoComplete.text;
            }
            if (autoComplete.isEditable) {
                Utils.triggerValidationChangeEvent(input);
            }
        }

        _onSelectedIndexChanged() {
            var input = <HTMLInputElement>this._hiddenElement,
                autoComplete = <wijmo.input.AutoComplete>this.control;
            if (!input) {
                return;
            }

            if (autoComplete.isEditable) {
                if (autoComplete.selectedIndex > -1) {
                    input.value = autoComplete.selectedValue;
                } else {
                    input.value = autoComplete.text;
                }
            } else {
                input.value = autoComplete.selectedValue;
            }

            Utils.triggerValidationChangeEvent(input);
        }

        _afterInitializeControl(options: any): void {
            super._afterInitializeControl(options);
            // handle hidden field
            if (this._hiddenElement) {
                this._updateHiddenField();
            }

            var autoComplete = <wijmo.input.AutoComplete>this.control;
            if (autoComplete.isEditable) {
                if (options.selectedValue != undefined && this._textPortToSelectedValue) {
                    if (autoComplete.selectedIndex == -1) {
                        autoComplete.text = options.selectedValue;
                    }
                }

                var text = autoComplete.text;
                if (autoComplete.selectedIndex == 0 && text != autoComplete.getDisplayText()) {
                    autoComplete.selectedIndex = -1;
                    autoComplete.text = text;
                }

                if (this._hiddenElement && autoComplete.selectedIndex > -1) {
                    var input = <HTMLInputElement>this._hiddenElement;
                    input.value = autoComplete.selectedValue;
                }
            }
        }
    }

    /**
     * The @see:AutoComplete control extends from @see:c1.input.AutoComplete.
     */
    export class AutoComplete extends c1.input.AutoComplete {
    }

    export class _CalendarInitializer extends _Initializer {
        _afterInitializeControl(options: any) {
            super._afterInitializeControl(options);
            var calendar = <Calendar>this.control;
            calendar._isInitialized = true;

            // refresh the control after initialize
            calendar.invalidate();
        }
    }

    export class _CalendarWrapper extends _ControlWrapper {
        get _controlType(): any {
            return Calendar;
        }

        get _initializerType(): any {
            return _CalendarInitializer;
        }
    }

    /**
     * The @see:Calendar control extends from @see:c1.input.Calendar.
     */
    export class Calendar extends c1.input.Calendar {
        _isInitialized: boolean;

        refresh(fullUpdate = true) {
            // postpone the refresh if the control is not intialized.
            if (!this._isInitialized) return;

            super.refresh(fullUpdate);
        }
    }

    export class _ColorPickerWrapper extends _ControlWrapper {
        get _controlType(): any {
            return ColorPicker;
        }
    }

    /**
     * The @see:ColorPicker control extends from @see:c1.input.ColorPicker.
     */
    export class ColorPicker extends c1.input.ColorPicker {
        initialize(options: any) {
            if (!options) {
                return;
            }

            var palette = options.palette;
            if (!!palette) {
                delete options.palette;
            }

            super.initialize(options);

            if (!!palette) {
                this.palette = palette;
            }
        }
    }

    export class _InputColorWrapper extends _DropDownWrapper {
        get _initializerType(): any {
            return _FormInputInitializer;
        }

        get _controlType(): any {
            return InputColor;
        }
    }

    /**
     * The @see:InputColor control extends from @see:c1.input.InputColor.
     */
    export class InputColor extends c1.input.InputColor {
        initialize(options: any) {
            if (!options) {
                return;
            }

            var isDroppedDown = false;
            if (options.isDroppedDown) {
                isDroppedDown = options.isDroppedDown;
                delete options.isDroppedDown;
            }

            super.initialize(options);

            if (isDroppedDown) {
                this.isDroppedDown = isDroppedDown;
            }
        }
    }

    export class _InputDateInitializer extends _FormInputInitializer {
        _beforeInitializeControl(options: any): void {
            if(options.value && options.min && options.max)
            {
                let min = wijmo.DateTime.fromDateTime(options.min, options.value);
                if (options.value < min) {
                    options.value = min;
                }

                let max = wijmo.DateTime.fromDateTime(options.max, options.value);
                if (options.value > max) {
                    options.value = max;
                }
            }
            super._beforeInitializeControl(options);
        }
        _afterInitializeControl(options: any, valueName: string = 'value', valueChangedEventName: string = 'valueChanged') {
            super._afterInitializeControl(options, valueName, valueChangedEventName);
            if (options.isDroppedDown) {
                setTimeout(() => {
                    this.control['isDroppedDown'] = true;
                }, 15);
            }
        }
    }

    export class _InputDateWrapper extends _DropDownWrapper {
        get _initializerType(): any {
            return _InputDateInitializer;
        }

        get _controlType(): any {
            return InputDate;
        }
    }

    /**
     * The @see:InputDate control extends from @see:c1.input.InputDate.
     */
    export class InputDate extends c1.input.InputDate {
        initialize(options: any) {
            if (!options) {
                return;
            }

            var refreshInterval = 15; // wijmo.Control._REFRESH_INTERVAL = 10, but is private.
            super.initialize(options);
            if (options.isDroppedDown) {
                setTimeout(() => {
                    this.isDroppedDown = true;
                }, refreshInterval);
            }
        }
    }

    export class _InputDateTimeWrapper extends _InputDateWrapper {
        get _controlType(): any {
            return InputDateTime;
        }
    }

    /**
     * The @see:InputDateTime control extends from @see:c1.input.InputDateTime.
     */
    export class InputDateTime extends c1.input.InputDateTime {
    }

    export class _InputMaskInitializer extends _FormInputInitializer {
        _afterInitializeControl(options: any) {
            super._afterInitializeControl(options, "rawValue");
        }
    }

    export class _InputMaskWrapper extends _ControlWrapper {
        get _initializerType(): any {
            return _InputMaskInitializer;
        }

        get _controlType(): any {
            return InputMask;
        }
    }

    /**
     * The @see:InputMask control extends from @see:c1.input.InputMask.
     */
    export class InputMask extends c1.input.InputMask {
    }

    export class _InputNumberWrapper extends _ControlWrapper {
        get _initializerType(): any {
            return _FormInputInitializer;
        }

        get _controlType(): any {
            return InputNumber;
        }
    }

    /**
     * The @see:InputNumber control extends from @see:c1.input.InputNumber.
     */
    export class InputNumber extends c1.input.InputNumber {
    }

    export class _InputTimeWrapper extends _ComboBoxWrapper {
        get _initializerType(): any {
            return _FormInputInitializer;
        }

        get _controlType(): any {
            return InputTime;
        }
    }

    /**
     * The @see:InputTime control extends from @see:c1.input.InputTime.
     */
    export class InputTime extends c1.input.InputTime {
        initialize(options: any) {
            if (!options) {
                return;
            }

            var self = this,
                refreshInterval = 15, // wijmo.Control._REFRESH_INTERVAL = 10, but is private.
                itemsSource = options.itemsSource,
                value = options.value;

            // The client side will call refresh method and cover itemsSource data after 10 milliseconds.
            // So we set the itemsSource property after 15 milliseconds.
            if (itemsSource) {
                delete options.itemsSource;

                if (itemsSource.length || itemsSource.itemCount || !itemsSource._disableServerRead) {
                    setTimeout(() => {
                        self.displayMemberPath = null;
                        self.selectedValuePath = null;
                        self.itemsSource = itemsSource;
                        if (value) {
                            self.value = value;
                        }
                    }, refreshInterval);
                }
            }

            super.initialize(options);
            if (options.isDroppedDown === true) {
                setTimeout(() => {
                    self.isDroppedDown = true;
                }, refreshInterval);
            }
        }
    }

    export class _ListBoxWrapper extends _ControlWrapper {
        get _initializerType(): any {
            return _ItemsInputInitializer;
        }

        get _controlType(): any {
            return ListBox;
        }
    }

    /**
     * The @see:ListBox control extends from @see:c1.input.ListBox.
     */
    export class ListBox extends c1.input.ListBox {
        initialize(options: any) {
            if (!options) {
                return;
            }

            ListBox._processCheckedFields(options, this, this);
            super.initialize(options);
        }

        static _processCheckedFields(options: any, self: any, listBox: wijmo.input.ListBox) {
            var checkedValues = options.checkedValues,
                checkedIndexes = options.checkedIndexes,
                updateCheckedItems;
            if ((checkedValues || checkedIndexes) && options.itemsSource) {
                delete options.checkedValues;
                delete options.checkedIndexes;
                if (options.itemsSource.items.length) {
                    options["checkedItems"] = checkedValues ? ListBox._valuesToItems(options, checkedValues) :
                        ListBox._indexesToItems(options.itemsSource.items, checkedIndexes);
                } else if (options.itemsSource._readActionUrl) {
                    // Enable checkedValues in remote data model.
                    updateCheckedItems = (sender) => {
                        if (sender.itemsSource.items.length) {
                            self.checkedItems = checkedValues ? ListBox._valuesToItems(options, checkedValues) :
                                ListBox._indexesToItems(options.itemsSource.items, checkedIndexes);
                            setTimeout(() => {
                                listBox.itemsChanged.removeHandler(updateCheckedItems);
                            });
                        }
                    };
                    listBox.itemsChanged.addHandler(updateCheckedItems);
                }
            }
        }

        static _valuesToItems(options: any, checkedValues: any): Array<Object> {
            var index: number, checkedItems = [], i: number,
                items = options.itemsSource.items,
                values = ListBox._getValues(items, options);
            for (i = 0; i < checkedValues.length; i++) {
                index = values.indexOf(checkedValues[i]);
                if (index != -1) {
                    checkedItems.push(items[index]);
                }
            }
            return checkedItems;
        }

        static _indexesToItems(items: Array<Object>, checkedIndexes: Array<number>) {
            var checkedItems = [];
            checkedIndexes.forEach(i=> checkedItems.push(items[i]));
            return checkedItems;
        }

        static _getValues(items: any[], options: any): any[] {
            var values: any[] = [],
                valuePath = options["selectedValuePath"] || options["displayMemberPath"];

            if (Utils.isObject(items[0])) {
                items.forEach(item=> values.push(item[valuePath]));
            } else {
                values = items.slice();
            }

            return values;
        }
    }

    export class _MenuWrapper extends _ComboBoxWrapper {
        get _controlType(): any {
            return Menu;
        }
    }

    /**
     * The @see:Menu control extends from @see:c1.input.Menu.
     */
    export class Menu extends c1.input.Menu {
        initialize(options: any) {
            if (!options) {
                return;
            }

            var owner = options["owner"],
                ownerElements: NodeList;
            if (owner) {
                ownerElements = document.querySelectorAll(owner);
                this._ValidateOwner(ownerElements);
                delete options.owner;
            }
            super.initialize(options);
        }

        _ValidateOwner(ownerElements: NodeList) {
            var self = this,
                dropDown = self.dropDown, i;
            for (i = 0; i < ownerElements.length; i++) {
                ownerElements[i].addEventListener('contextmenu', function (e) {
                    if (self && dropDown && !wijmo.closest(e.target, '[disabled]')) {
                        e.preventDefault();
                        self.owner = <HTMLElement>e.target;
                        self.selectedIndex = -1;
                        if (self.onIsDroppedDownChanging(new wijmo.CancelEventArgs())) {
                            wijmo.showPopup(dropDown, e);
                            self.onIsDroppedDownChanged();
                            dropDown.focus();
                        }
                    }
                });
            }
        }
    }

    export class _PopupWrapper extends _ControlWrapper {
        get _controlType(): any {
            return Popup;
        }
    }

    /**
     * The @see:Popup control extends from @see:c1.input.Popup.
     */
    export class Popup extends c1.input.Popup {
    }

    export class _MultiSelectWrapper extends _ComboBoxWrapper {
        get _controlType(): any {
            return MultiSelect;
        }
    }

    /**
     * The @see:MultiSelect control extends from @see:c1.input.MultiSelect.
     */
    export class MultiSelect extends c1.input.MultiSelect {
        private _hiddenSelect: HTMLSelectElement;
        private _validationAttributes: any;

        initialize(options: any) {
            if (!options) {
                return;
            }

            if (options.validationAttributes) {
                this._validationAttributes = options.validationAttributes;
                delete options.validationAttributes;
            }

            ListBox._processCheckedFields(options, this, this.listBox);
            var itemsSource = options.itemsSource;
            delete options.itemsSource;

            var isDroppedDown = false;
            if (options.isDroppedDown) {
                isDroppedDown = options.isDroppedDown;
                delete options.isDroppedDown;
            }

            super.initialize(options);

            if (itemsSource) {
                this.itemsSource = itemsSource;

                if (options.checkedItems) {
                    this.checkedItems = options.checkedItems;
                }

                if (isDroppedDown) {
                    this.isDroppedDown = isDroppedDown;
                }
            }

            this._handleHiddenField();
        }

        _handleHiddenField() {
            var self = this;
            self._updateHiddenField();
            self.checkedItemsChanged.addHandler(() => {
                self._updateHiddenField();
                Utils.triggerValidationChangeEvent(self._hiddenSelect);
            });
        }

        private _updateHiddenField() {
            // TFS 402095
            if(!this.itemsSource) return;
            var self = this, i: number,
                optionList: any,
                checkedValues: string[],
                itemsValues: string[],
                items = self.itemsSource.items,
                checkedItems = self.checkedItems,
                hiddenWraper = self._tbx.parentElement,
                name = self.hostElement.getAttribute("wj-name");

            if (!name || !items || !items.length) {
                return;
            }

            itemsValues = ListBox._getValues(items, self);
            checkedValues = ListBox._getValues(checkedItems, self);
            self._hiddenSelect = <HTMLSelectElement>hiddenWraper.querySelector("select");

            // the option's value is always string
            var checkedStringValues = [];
            checkedValues.forEach(item => {
                checkedStringValues.push(item == null ? null : '' + item);
            });

            if (!self._hiddenSelect) {
                self._hiddenSelect = self._createSelect(itemsValues, name, self._tbx);
            }

            optionList = self._hiddenSelect.querySelectorAll("option");

            for (i = 0; i < optionList.length; i++) {
                if (checkedStringValues.indexOf(optionList[i].value) != -1) {
                    optionList[i].setAttribute("selected", "selected");
                    optionList[i].selected = true;
                } else {
                    optionList[i].removeAttribute("selected");
                    optionList[i].selected = false;
                }
            }
        }

        // Create the hidden select element.
        private _createSelect(itemValues: string[], name: string, textBox: HTMLInputElement) {
            var self = this, select: HTMLSelectElement, i: number, optionItem: HTMLOptionElement;
            select = document.createElement("select");
            select.name = name;
            select.multiple = true;
            select.style.visibility = "hidden";
            select.style.position = "absolute";
            select.style.height = "0px";

            for (i = 0; i < itemValues.length; i++) {
                optionItem = document.createElement("option");
                optionItem.value = itemValues[i];
                select.appendChild(optionItem);
            }

            if (self._validationAttributes) {
                for (var att in self._validationAttributes) {
                    select.setAttribute(att, self._validationAttributes[att]);
                }
            }

            textBox.parentNode.insertBefore(select, textBox);
            Utils.forwardValidationEvents(self, textBox, select);

            return select;
        }
    }

    export class _MultiAutoCompleteWrapper extends _AutoCompleteWrapper {
        get _controlType(): any {
            return MultiAutoComplete;
        }

        get _initializerType(): any {
            return _MultiAutoCompleteInitializer;
        }
    }

    export class _MultiAutoCompleteInitializer extends _AutoCompleteInitializer {
        private _isDroppedDown: boolean;

        _createHiddenElement(): HTMLElement {
            var select = document.createElement("select");
            select.name = (<wijmo.Control>this.control).hostElement.getAttribute('wj-name');
            select.multiple = true;
            select.style.visibility = "hidden";
            select.style.position = "absolute";
            return select;
        }

        _processOptions(options: any) {
            super._processOptions(options);
            _MultiAutoCompleteInitializer._processSelectedFields(options, <wijmo.input.MultiAutoComplete>this.control);
            if (options.isDroppedDown) {
                this._isDroppedDown = options.isDroppedDown;
                delete options.isDroppedDown;
            }
        }

        _afterInitializeControl(options: any): void {
            super._afterInitializeControl(options);

            var wijContrl = <wijmo.input.DropDown>this.control;
            if (this._isDroppedDown) {
                wijContrl.isDroppedDown = true;
            }
        }

        _updateHiddenField() {
            this._updateHiddenElement();
            (<wijmo.input.MultiAutoComplete>this.control).selectedItemsChanged.addHandler(this._updateHiddenElement.bind(this));
        }

        private _updateHiddenElement() {
            var multiAutoComplete = <wijmo.input.MultiAutoComplete>this.control,
                values = ListBox._getValues(multiAutoComplete.selectedItems, multiAutoComplete),
                select = <HTMLSelectElement>this._hiddenElement;

            select.innerHTML = '';
            if (!values || !values.length) {
                return;
            }

            // the option's value is always string
            var stringValues = [];
            values.forEach(item => {
                stringValues.push(item == null ? null : '' + item);
            });

            for (var i = 0; i < stringValues.length; i++) {
                var optionItem = document.createElement("option");
                optionItem.value = stringValues[i];
                optionItem.setAttribute("selected", "selected");
                select.appendChild(optionItem);
            }
        }

        private static _getDisplayText(item: any, displayMemberPath: string) {
            var tokenTxt = item;
            if (displayMemberPath) {
                tokenTxt = item[displayMemberPath];
            }
            return tokenTxt;
        }

        static _processSelectedFields(options: any, control: wijmo.input.MultiAutoComplete) {
            var selectedValues = options.selectedValues,
                selectedIndexes = options.selectedIndexes,
                updateselectedItems;
            if ((selectedValues || selectedIndexes) && options.itemsSource) {
                delete options.selectedValues;
                delete options.selectedIndexes;
                if (options.itemsSource.items.length) {
                    options["selectedItems"] = selectedValues ? _MultiAutoCompleteInitializer._valuesToItems(options, selectedValues) :
                        _MultiAutoCompleteInitializer._indexesToItems(options.itemsSource.items, selectedIndexes);
                } else if (options.itemsSourceFunction) {
                    // Enable selectedValues in remote data model.
                    updateselectedItems = (sender) => {
                        if (sender.itemsSource.items.length && (selectedValues || selectedIndexes)) {
                            control.selectedItems = selectedValues ? _MultiAutoCompleteInitializer._valuesToItems(options, selectedValues) :
                                _MultiAutoCompleteInitializer._indexesToItems(options.itemsSource.items, selectedIndexes);
                            selectedIndexes = null;
                            selectedValues = null; 
                            control._lbx.itemsChanged.removeHandler(updateselectedItems);
                        }
                    };
                    control._lbx.itemsChanged.addHandler(updateselectedItems);
                }
            }
        }

        static _valuesToItems(options: any, selectedValues: Array<Object>): Array<Object> {
            var selectedItems = [],
                i: number,
                items = options.itemsSource.items,
                values = ListBox._getValues(items, options);

            for (i = 0; i < selectedValues.length; i++) {
                var index = values.indexOf(selectedValues[i]);
                if (index != -1) {
                    selectedItems.push(items[index]);
                }
            }
            return selectedItems;
        }

        static _indexesToItems(items: any, selectedIndexes: Array<number>) {
            var selectedItems = [];
            selectedIndexes.forEach(i => selectedItems.push(items[i]));
            return selectedItems;
        }
    }

    /**
     * The @see:MultiAutoComplete control extends from @see:c1.input.MultiAutoComplete.
     */
    export class MultiAutoComplete extends c1.input.MultiAutoComplete {
        private _isFiltering: boolean;
        private _times: number;

        protected _updateItems() {
            if (this._isFilterOnClient()) {
                super._updateItems();
                return;
            }

            // lack filter settings(bugs)
            var remoteCV = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(this.collectionView, c1.mvc.collections.RemoteCollectionView);
            if (!remoteCV) {
                // MultiAutoComplete doesn't support ODataCollectionView when its filterOnServer is true.
                return;
            }
            this._times = remoteCV._queryList.length;
            // when in batch edit mode, the changes will be commit before refresh the data.
            if (remoteCV._batchEdit && remoteCV._batchEditActionUrl
                && (remoteCV.itemsAdded && remoteCV.itemsAdded.length
                    || remoteCV.itemsEdited && remoteCV.itemsEdited.length
                    || remoteCV.itemsRemoved && remoteCV.itemsRemoved.length)) {
                this._times++;
            }

            remoteCV.beginQuery.addHandler(this._cvQueryBegin, this);
            this.collectionView.refresh();
        }

        private _isFilterOnClient(): boolean {
            var remoteCV = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(this.collectionView, c1.mvc.collections.RemoteCollectionView);
            if (remoteCV) {
                return remoteCV._isDisableServerRead();
            }

            var oDataCV = <wijmo.odata.ODataCollectionView>wijmo.tryCast(this.collectionView, wijmo.odata.ODataCollectionView);
            if (oDataCV) {
                return !oDataCV.filterOnServer;
            }

            return true;
        }

        private _cvQueryBegin() {
            if (this._times == 0) {
                var remoteCV = <c1.mvc.collections.RemoteCollectionView>this.collectionView;
                window.setTimeout(() => {
                    remoteCV.beginQuery.removeHandler(this._cvQueryBegin, this);
                });
                this._isFiltering = true;
                remoteCV.queryComplete.addHandler(this._cvQueryComplete, this);
            } else if (this._times > 0) {
                this._times--;
            } else {
                wijmo.assert(false, 'this handler should be removed.');
            }
        }

        private _cvQueryComplete() {
            var remoteCV = <c1.mvc.collections.RemoteCollectionView>this.collectionView;
            window.setTimeout(() => {
                remoteCV.queryComplete.removeHandler(this._cvQueryComplete, this);
            });
            remoteCV.moveCurrentToPosition(-1);
            this._isFiltering = false;
            // show/hide the drop-down
            this.isDroppedDown = remoteCV.items.length > 0 && this.containsFocus();
            if (remoteCV.items.length == 0 && !this.isEditable) { // honor isEditable: TFS 81936
                this.selectedIndex = -1;
            }

            // refresh to update the drop-down position
            this.refresh();
        }

        _setText(text: string) {
            // when the data is filtering, don't clear the text.
            if (this._isFiltering) {
                return;
            }

            super._setText(text);
        }
    }

    export class _CollectionViewNavigatorWrapper extends _ControlWrapper {
        get _controlType(): any {
            return CollectionViewNavigator;
        }

        get _initializerType(): any {
            return _CollectionViewNavigatorInitializer;
        }
    }

    export class _CollectionViewNavigatorInitializer extends _Initializer {
        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);

            if (options) {
                var id: string = options.itemsSourceId,
                    control: wijmo.Control,
                    cv: wijmo.collections.CollectionView,
                    sv: Object;

                if (id) {
                    sv = c1.getService(id);
                    if (sv) {
                        cv = wijmo.tryCast(sv, wijmo.collections.CollectionView);
                    } else {
                        control = wijmo.Control.getControl("#" + id);
                        if (control) {
                            cv = control["collectionView"];
                        }
                    }
                    if (cv) {
                        options.cv = cv;
                    }
                }
                delete options.itemsSourceId;
            }
        }
    }


    /**
     * The @see:CollectionViewNavigator control extends from @see:c1.input.CollectionViewNavigator.
     */
    export class CollectionViewNavigator extends c1.input.CollectionViewNavigator {
    }

    export class _MultiSelectListBoxWrapper extends _ControlWrapper {
        get _initializerType(): any {
            return _ItemsInputInitializer;
        }

        get _controlType(): any {
            return MultiSelectListBox;
        }
    }

    /**
     * The @see:MultiSelectListBox control extends from @see:c1.input.MultiSelectListBox.
     */
    export class MultiSelectListBox extends c1.input.MultiSelectListBox {
        initialize(options: any) {
            if (!options) {
                return;
            }

            MultiSelectListBox._processCheckedFields(options, this, this);
            super.initialize(options);
        }

        static _processCheckedFields(options: any, self: any, multiSelectListBox: wijmo.input.MultiSelectListBox) {
            var checkedValues = options.checkedValues,
                checkedIndexes = options.checkedIndexes,
                updateCheckedItems;
            if ((checkedValues || checkedIndexes) && options.itemsSource) {
                delete options.checkedValues;
                delete options.checkedIndexes;
                if (options.itemsSource.items.length) {
                    options["checkedItems"] = checkedValues ? MultiSelectListBox._valuesToItems(options, checkedValues) :
                        MultiSelectListBox._indexesToItems(options.itemsSource.items, checkedIndexes);
                } else if (options.itemsSource._readActionUrl) {
                    // Remote data model for checkedValues.
                    updateCheckedItems = (sender) => {
                        if (sender.itemsSource.items.length) {
                            self.checkedItems = checkedValues ? MultiSelectListBox._valuesToItems(options, checkedValues) :
                                MultiSelectListBox._indexesToItems(options.itemsSource.items, checkedIndexes);
                            setTimeout(() => {
                                multiSelectListBox.listBox.itemsChanged.removeHandler(updateCheckedItems);
                            });
                        }
                    };
                    multiSelectListBox.listBox.itemsChanged.addHandler(updateCheckedItems);
                }
            }
        }

        static _valuesToItems(options: any, checkedValues: any): Array<Object> {
            var index: number, checkedItems = [], i: number,
                items = options.itemsSource.items,
                values = MultiSelectListBox._getValues(items, options);
            for (i = 0; i < checkedValues.length; i++) {
                index = values.indexOf(checkedValues[i]);
                if (index != -1) {
                    checkedItems.push(items[index]);
                }
            }
            return checkedItems;
        }

        static _indexesToItems(items: Array<Object>, checkedIndexes: Array<number>) {
            var checkedItems = [];
            checkedIndexes.forEach(i => checkedItems.push(items[i]));
            return checkedItems;
        }

        static _getValues(items: any[], options: any): any[] {
            var values: any[] = [],
                valuePath = options["selectedValuePath"] || options["displayMemberPath"];

            if (Utils.isObject(items[0])) {
                items.forEach(item => values.push(item[valuePath]));
            } else {
                values = items.slice();
            }

            return values;
        }
        
    }
}
