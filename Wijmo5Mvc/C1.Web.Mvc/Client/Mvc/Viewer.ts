﻿/// <reference path="../Shared/Viewer.ts" />

/**
 * Defines the mvc viewer controls and their associated classes.
 */
module c1.mvc.viewer {
    export class _ReportViewerWrapper extends _ControlWrapper {
        get _controlType(): any {
            return ReportViewer;
        }
    }

    /**
     * The @see:ReportViewer control extends from @see:c1.viewer.ReportViewer.
     */
    export class ReportViewer extends c1.viewer.ReportViewer {
    }

    export class _PdfViewerWrapper extends _ControlWrapper {
        get _controlType(): any {
            return PdfViewer;
        }
    }

    /**
     * The @see:PdfViewer control extends from @see:c1.viewer.PdfViewer.
     */
    export class PdfViewer extends c1.viewer.PdfViewer {
    }
}