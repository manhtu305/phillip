﻿/// <reference path="../Shared/Grid.Transposed.ts" />

module c1.mvc.grid.transposed {

    export class _TransposedGridWrapper extends _ControlWrapper {
        get _controlType(): any {
            return TransposedGrid;
        }

        get _initializerType(): any {
            return _Initializer;
        }
    }

    export class _Initializer extends c1.mvc.grid._Initializer {
    }
    
    /**
     * The @see:TransposedGrid control extends from @see: c1.grid.transposed.TransposedGrid.
     */
    export class TransposedGrid extends c1.grid.transposed.TransposedGrid {
        
        // overridden to convert transposed CollectionView to CallbackCollectionView
        protected _getCollectionView(value: any): wijmo.collections.ICollectionView {
            let cv = value;
            value = super._getCollectionView(value);
            if (cv instanceof c1.mvc.collections.RemoteCollectionView) {
                value = new c1.mvc.collections.CallbackCollectionView({ sourceCollection: value.items, uniqueId: null, disableServerRead: true });
                value["_srcCv"] = cv;
                return wijmo.asCollectionView(value);
            } else {
                return value;
            }
        }

        // overridden to process itemsSource array is empty, in case it's ODataCollectionView
        _transposeItemsSource(arr: any[]): wijmo.collections.ObservableArray {
            if (!arr || arr.length == 0) {
                return new wijmo.collections.ObservableArray();
            } else {
                return super._transposeItemsSource(arr);
            }
        }

        // overridden to prepare for source CollectionView before editing
        onRowEditStarting(e: wijmo.grid.CellRangeEventArgs) {
            // Set edit item for source CollectionView under Proxy
            let cv = this.editableCollectionView["_srcCv"];
            if (cv instanceof c1.mvc.collections.RemoteCollectionView) {
                let item = this.rows[e.row].dataItem;
                cv._edtItem = item._arr[e.col];
            }
            super.onRowEditStarting(e)
        }

        // overridden to clear for source CollectionView after editing
        onRowEditEnded(e: wijmo.grid.CellRangeEventArgs) {
            // Remove edit item for source CollectionView under Proxy
            let cv = this.editableCollectionView["_srcCv"];
            if (cv instanceof c1.mvc.collections.RemoteCollectionView) {
                let item = this.rows[e.row].dataItem;
                cv._edtItem = null;
            }
            super.onRowEditEnded(e)
        }
        
        _getFlatColumnsMvc(options): any[] {
            return options["columns"];
        }
    }
}