﻿module wijmo.nav.TreeNode {
    /**
     * Casts the specified object to @see:wijmo.nav.TreeNode type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TreeNode {
        return obj;
    }
}

module wijmo.nav.TreeView {
    /**
     * Casts the specified object to @see:wijmo.nav.TreeView type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TreeView {
        return obj;
    }
}

module wijmo.nav.FormatNodeEventArgs {
    /**
     * Casts the specified object to @see:wijmo.nav.FormatNodeEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FormatNodeEventArgs {
        return obj;
    }
}

module wijmo.nav.TreeNodeEventArgs {
    /**
     * Casts the specified object to @see:wijmo.nav.TreeNodeEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TreeNodeEventArgs {
        return obj;
    }
}

module wijmo.nav.TreeNodeDragDropEventArgs {
    /**
     * Casts the specified object to @see:wijmo.nav.TreeNodeDragDropEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TreeNodeDragDropEventArgs {
        return obj;
    }
}

module wijmo.nav.TabPanel {
    /**
     * Casts the specified object to @see:wijmo.nav.TabPanel type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TabPanel {
        return obj;
    }
}

module wijmo.nav.Tab {
    /**
     * Casts the specified object to @see:wijmo.nav.Tab type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Tab {
        return obj;
    }
}

