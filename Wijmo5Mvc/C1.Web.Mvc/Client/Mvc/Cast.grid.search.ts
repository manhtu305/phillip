﻿module wijmo.grid.search.FlexGridSearch {
    /**
     * Casts the specified object to @see:wijmo.grid.search.FlexGridSearch type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexGridSearch {
        return obj;
    }
}

