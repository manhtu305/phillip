﻿/// <reference path="Util.ts" />
/// <reference path="Template.ts" />
/// <reference path="Control.ts" />
/// <reference path="../Shared/Grid.ts" />

/**
 * Defines the @see:c1.mvc.grid.FlexGrid control and associated classes.
 */
module c1.mvc.grid {

    export class _FlexGridWrapper extends _ControlWrapper {

        get _controlType(): any {
            return FlexGrid;
        }

        get _initializerType(): any {
            return c1.mvc.grid._Initializer;
        }

        _getExtensionTypes(): any[]{
            var extensions = super._getExtensionTypes();
            extensions.push(_ItemTemplateProvider,
                _Validator,
                _SortHelper,
                _MaskLayer,
                _VirtualScrolling,
                _UpdateHelper);
            return extensions;
        }
    }

    // the interface for common functions for FlexGrid and MultiRow.
    export interface _IFlexGridMvc {
        // gets the key for the extra request data of sort.
        _getSortDescriptorKeyMvc(): string;
        // gets the columns definition from the options.
        _getFlatColumnsMvc(options): any[];
        // gets all cell groups definition from the options.
        _getLayoutGroupsMvc(options: any): any[];
        // gets all binding columns.
        _getBindingColumns(): wijmo.grid.Column[];
        // gets a binding column by group index and cell index.
        _getBindingColumnMvc(groupIndex: number, cellIndex: number): wijmo.grid.Column;
        // gets a binding column by a cell range.
        _getBindingColumnFromCellRangeMvc(e: wijmo.grid.CellRangeEventArgs): wijmo.grid.Column;
        // gets number of rows to display one item.
        _getRowsPerItemMvc(): number;
        // gets sorting cell settings.
        _getSortCellMvc(rowIndex: number, columnIndex: number, column: wijmo.grid.Column): _IGroupCell;
    }

    // the infomation of cell on sorting.
    export interface _IGroupCell {
        // the index of the group which contains the cell.
        groupIndex: number;
        // the index of the cell within the group...
        cellIndex: number;
        // the cell on sorting.
        cell: wijmo.grid.Column;
    }

    // represent class for cell in HeaderTemplate class.
    export class HeaderTemplateCell {
        // row position.
        row: number;
        // column position.
        col: number;
        // row span value start from row property.
        rowSpan: number;
        // column span value start from column property.
        colSpan: number;
        // this text will appear in the field.
        title: string;
    }
    // represent class for HeaderTemplate.
    export class HeaderTemplate {
        // number of row(s) will appear header of table.
        rowCount: number;
        // hold all cells custom setting.
        cells: Array<HeaderTemplateCell>;
    }

    // this class hold all information about merge, it's supporter for custom header template.
    export class MVCMergeManager extends wijmo.grid.MergeManager {
        private _groupColumnsList: Array<wijmo.grid.CellRange>;
        // hold old merge manager object.
        mmOld: wijmo.grid.MergeManager;

        get groupColumnsList(): Array<wijmo.grid.CellRange> {
            return this._groupColumnsList;
        }
        set groupColumnsList(list) {
            this._groupColumnsList = list;
        }

        getMergedRange(p: wijmo.grid.GridPanel, r: number, c: number, clip?: boolean): wijmo.grid.CellRange {
            if (this._groupColumnsList && p.cellType == wijmo.grid.CellType.ColumnHeader) {
                if (r >= 0 && c >= 0) {
                    for (var i = 0; i < this._groupColumnsList.length; ++i)
                        if (this._groupColumnsList[i].contains(r, c))
                            return this._groupColumnsList[i];
                }
            }
            if(this.mmOld)
                return this.mmOld.getMergedRange(p, r, c, clip);
            return null;
        }
    }

    /**
     * The @see:FlexGrid control extends from @see:c1.grid.FlexGrid.
     */
    export class FlexGrid extends c1.grid.FlexGrid implements _IFlexGridMvc {// _IFlexGridMvc
        private _headerTemplate: HeaderTemplate;
        mergeManager: c1.mvc.grid.MVCMergeManager;

        get headerTemplate(): HeaderTemplate {
            return this._headerTemplate;
        }
        
        // Internal use only
        set headerTemplate(para: HeaderTemplate) {
            if (para == null) {
                this.collectionView.beginUpdate();
                {
                    var temp = this.columnHeaders.rows[this.columnHeaders.rows.length - 1];
                    this.columnHeaders.rows.clear();
                    this.mergeManager.mmOld = null;
                    this.mergeManager = null;
                    this.columnHeaders.rows.push(temp);
                    temp = null;
                }
                this.collectionView.endUpdate();
                this._headerTemplate = null;
                return;
            }
            /***********************************************************************************
            * if table support drag drop, it will break own custom header template.            *
            ***********************************************************************************/
            this.allowDragging = wijmo.grid.AllowDragging.None;
            
            for (var i = 0; i < para.cells.length; ++i) {
                /**********************************************************************************
                * This caused by our logic pass data from server to client.                       *
                **********************************************************************************/
                if (para.cells[i].row === undefined)
                    para.cells[i].row = 0;
                if (para.cells[i].col === undefined)
                    para.cells[i].col = 0;
                wijmo.assert(para.cells[i].col <= this.columnHeaders.columns.length - 1, "column position must not greater than last column position value = (columns length - 1)");
                para.cells[i].colSpan = Math.min(para.cells[i].colSpan, this.columnHeaders.columns.length - para.cells[i].col);
            }
            // just leave it here, if all coditions are pass, the property will be set.
            this._headerTemplate = para;

            /**********************************************************************************
            * This is the first one stuff use kind of merge manager for header table.         *
            * So it's doesn't need to concern about how many row in table exist.              *
            **********************************************************************************/
            for (var i = 0; i < para.rowCount - 1; ++i) {
                this.columnHeaders.rows.push(new wijmo.grid.Row());
            }

            /**********************************************************************************
            * Set format and data for columns header                                          *
            **********************************************************************************/
            var mm = new MVCMergeManager(this),
                listrng = new Array<wijmo.grid.CellRange>();
            for (var i = 0; i < para.cells.length; ++i) {
                this.columnHeaders.setCellData(para.cells[i].row, para.cells[i].col, para.cells[i].title);
                listrng.push(new wijmo.grid.CellRange(para.cells[i].row, para.cells[i].col,
						      para.cells[i].row + para.cells[i].rowSpan - 1,
						      para.cells[i].col + para.cells[i].colSpan - 1));
            }
            mm.groupColumnsList = listrng;
            mm.mmOld = this.mergeManager;
            
            this.mergeManager = mm;
        }

        _getSortDescriptorKeyMvc(): string {
            return 'FlexGridSort';
        }

        // _IFlexGridMvc
        _getFlatColumnsMvc(options): any[] {
            return options["columns"];
        }

        // _IFlexGridMvc
        _getLayoutGroupsMvc(options: any): any[] {
            var groups = [], group,
                columns = options.columns;

            if (columns && columns.length) {
                group = { 'cells': columns };
                groups.push(group);
            }

            return groups;
        }

        // _IFlexGridMvc
        _getBindingColumns(): wijmo.grid.Column[] {
            var columns = [];
            this.columns.forEach((col) => {
                columns.push(col);
            });
            return columns;
        }

        // _IFlexGridMvc
        _getBindingColumnMvc(groupIndex: number, cellIndex: number): wijmo.grid.Column {
            return this.columns[cellIndex];
        }

        // _IFlexGridMvc
        _getBindingColumnFromCellRangeMvc(e: wijmo.grid.CellRangeEventArgs): wijmo.grid.Column {
            return this.columns[e.col];
        }

        // _IFlexGridMvc
        _getRowsPerItemMvc(): number {
            return 1;
        }

        // _IFlexGridMvc
        _getSortCellMvc(rowIndex: number, columnIndex: number, column: wijmo.grid.Column): _IGroupCell {
            return {
                groupIndex: -1, // no group for grid
                cellIndex: columnIndex,
                cell: column
            };
        }


        getMergedRange(p: wijmo.grid.GridPanel, r: number, c: number, clip?: boolean): wijmo.grid.CellRange {

            if (p.cellType === wijmo.grid.CellType.ColumnHeader && this._headerTemplate && this._headerTemplate.cells) {

                var rng = new wijmo.grid.CellRange(r, c);

                for (var i = 0; i < this._headerTemplate.cells.length; i++) {
                    var range = new wijmo.grid.CellRange(

                        this._headerTemplate.cells[i].row,
                        this._headerTemplate.cells[i].col,
                        this._headerTemplate.cells[i].row + this._headerTemplate.cells[i].rowSpan - 1,
                        this._headerTemplate.cells[i].col + this._headerTemplate.cells[i].colSpan - 1

                    );
                    if (range.contains(rng)) {
                        return range;
                    }
                }

                return rng;
            }

            return super.getMergedRange(p, r, c, clip);
        }


        get defaultRowSize(): number {
            return this.rows.defaultSize;
        }

        set defaultRowSize(value: number) {
            this.rows.defaultSize = value;
        }

        get defaultColumnSize(): number {
            return this.columns.defaultSize;
        }

        set defaultColumnSize(value: number) {
            this.columns.defaultSize = value;
        }
    }
}
