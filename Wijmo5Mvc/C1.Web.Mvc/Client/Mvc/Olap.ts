﻿/// <reference path="../Shared/Olap.ts" />

/**
 * Defines the olap components and associated classes.
 */
module c1.mvc.olap {
    /**
     * The @see:PivotEngine extends from @see:c1.olap.PivotEngine.
     */
    export class PivotEngine extends c1.olap.PivotEngine {
        private _vdProxy: _ViewDefinitionProxy;
        private _collectionView: wijmo.collections.CollectionView;

        initialize(options: any) {
            this._processRemoteCollectionViewOpts(options);
            this._getVdProxy().initialize(options);
            if (!this._isRemoteCollectionView()) {
                this._getVdProxy()._applyViewdDefinition();
            } else {
                this._collectionView.collectionChanged.removeHandler(this._applyViewdDefinition, this);
                this._collectionView.collectionChanged.addHandler(this._applyViewdDefinition, this);
            }
        }

        private _processRemoteCollectionViewOpts(options: any) {
            if (options && options.itemsSource) {
                this._collectionView = wijmo.asType(options.itemsSource, wijmo.collections.CollectionView, true);
                if (this._isRemoteCollectionView()) {
                    delete options.itemsSource;
                }
            }
        }

        private _isRemoteCollectionView(): boolean {
            return DataSourceManager._isRemoteSource(this._collectionView);
        }

        // when the source is a remote data
        // apply the view definition when the remote data is obtained.
        private _applyViewdDefinition(sender, e: wijmo.collections.NotifyCollectionChangedEventArgs) {
            if (this._isRemoteCollectionView()
                && e.action == wijmo.collections.NotifyCollectionChangedAction.Reset) {
                this._collectionView.collectionChanged.removeHandler(this._applyViewdDefinition, this);
                this.itemsSource = this._collectionView;
                this._getVdProxy()._applyViewdDefinition();
            }
        }

        private _getVdProxy(): _ViewDefinitionProxy {
            if (!this._vdProxy) {
                this._vdProxy = new _ViewDefinitionProxy(this);
            }
            return this._vdProxy;
        }
    }

    export class _ViewDefinitionProxy {
        private _engine: wijmo.olap.PivotEngine;
        _viewDef: any;

        constructor(engine: wijmo.olap.PivotEngine) {
            this._engine = wijmo.asType(engine, wijmo.olap.PivotEngine);
        }

        initialize(options: any) {
            if (options) {
                if (options.viewDefinition) {
                    this._engine.autoGenerateFields = false;
                    delete options.fields;
                    delete options.autoGenerateFields;
                } else {
                    if (options.fields && options.fields.items && options.fields.items.length) {
                        // when fields are set in mvc control, 
                        // it means the fields would not be generated automatically.
                        this._engine.autoGenerateFields = false;
                        delete options.autoGenerateFields;
                        options.fields = options.fields.items;
                    }

                    if (options.rowFields) {
                        this._viewDef = this._viewDef || {};
                        this._viewDef['rowFields'] = options.rowFields;
                    }

                    if (options.columnFields) {
                        this._viewDef = this._viewDef || {};
                        this._viewDef['columnFields'] = options.columnFields;
                    }

                    if (options.valueFields) {
                        this._viewDef = this._viewDef || {};
                        this._viewDef['valueFields'] = options.valueFields;
                    }

                    if (options.filterFields) {
                        this._viewDef = this._viewDef || {};
                        this._viewDef['filterFields'] = options.filterFields;
                    }
                }

                delete options.rowFields;
                delete options.columnFields;
                delete options.valueFields;
                delete options.filterFields;

                this._engine.deferUpdate(() => {
                    wijmo.copy(this._engine, options);
                });
            }
        }

        // the view definiton is applied only once.
        _applyViewdDefinition() {
            var ng = this._engine;
            if (this._viewDef) {
                ng.deferUpdate(() => {
                    wijmo.copy(ng, this._viewDef);
                });

                this._viewDef = null;
            }
        }
    }

    /**
     * The @see:PivotPanel extends from @see:c1.olap.PivotPanel.
     */
    export class PivotPanel extends c1.olap.PivotPanel {
    }

    export class _PivotPanelWrapper extends _ControlWrapper {
        get _controlType(): any {
            return PivotPanel;
        }

        get _initializerType(): any {
            return _PivotPanelInitializer;
        }
    }

    // copy the engine in the options to the control.
    // process fields in the options(remove unnecessary fields for wijmo.olap.PivotEngine).
    export class _PivotPanelInitializer extends _Initializer {
        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);
            if (options) {
                var itemsSource = options.engine || c1.getService(options.itemsSourceId);

                if (itemsSource) {
                    // when binding a PivotEngine
                    if (itemsSource instanceof wijmo.olap.PivotEngine) {
                        this.control['engine'] = itemsSource;
                    } else if (itemsSource instanceof wijmo.collections.CollectionView) {
                        // Otherwise, binding a collectionview.
                        options.itemsSource = itemsSource;
                    }
                }

                delete options.engine;
                delete options.itemsSourceId;
            }
        }
    }

    /**
     * The @see:FlexPivotGrid extends from @see:c1.olap.FlexPivotGrid.
     */
    export class PivotGrid extends c1.olap.PivotGrid {
        onItemsSourceChanged() {
            super.onItemsSourceChanged();
            // fix wijmo5 bugs(if the fields is generated in pivotengine, then bind the engine to the grid and the grid show nothing.)
            this._bindGrid(true);
        }

        get defaultRowSize(): number {
            return this.rows.defaultSize;
        }

        set defaultRowSize(value: number) {
            this.rows.defaultSize = value;
        }

        get defaultColumnSize(): number {
            return this.columns.defaultSize;
        }

        set defaultColumnSize(value: number) {
            this.columns.defaultSize = value;
        }
    }

    export class _PivotGridWrapper extends _ControlWrapper {
        get _controlType(): any {
            return PivotGrid;
        }

        get _initializerType(): any {
            return _PivotInitializer;
        }
    }

    export class _PivotInitializer extends _Initializer {
        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);
            if (options) {
                if (options.itemsSourceId) {
                    options.itemsSource = c1.getService(options.itemsSourceId) || wijmo.Control.getControl('#' + options.itemsSourceId);
                }
                this._filterOptions(options);
                delete options.itemsSourceId;

                //Proccessing static property 'defaultTypeWidth'
                var dtw = options.defaultTypeWidth;
                if (dtw) {
                    for (var key in dtw) {
                        PivotGrid._defTypeWidth[key] = dtw[key];
                    }
                    delete options.defaultTypeWidth;
                }
            }
        }

        private _filterOptions(options: any) {
            this._filterSortOptions(options);
            this._filterPinningOptions(options);
        }

        private _filterSortOptions(options: any) {
            // Processing change AllowSorting type from bool to enum
            if (options.sortingType >= 0) {
                options.allowSorting = options.sortingType
                delete options.sortingType;
            } else if (options.allowSorting == false) {
                options.allowSorting = wijmo.grid.AllowSorting.None;
            } else if (options.allowSorting == true) {
                options.allowSorting = wijmo.grid.AllowSorting.SingleColumn;
            }
        }

        private _filterPinningOptions(options: any) {
            // Processing change AllowPinning type from bool to enum
            if (options.pinningType >= 0) {
                options.allowPinning = options.pinningType
                delete options.pinningType;
            } else if (options.allowPinning == false) {
                options.allowPinning = wijmo.grid.AllowPinning.None;
            } else if (options.allowPinning == true) {
                options.allowPinning = wijmo.grid.AllowPinning.SingleColumn;
            }
        }
    }

    /**
     * The @see:PivotChart extends from @see:c1.olap.PivotChart.
     */
    export class PivotChart extends c1.olap.PivotChart {
    }

    export class _PivotChartWrapper extends _ControlWrapper {
        get _controlType(): any {
            return PivotChart;
        }

        get _initializerType(): any {
            return _PivotChartInitializer;
        }
    }

    export class _PivotChartInitializer extends _PivotInitializer {
        private _flexPieOptions: any;
        private _flexChartOptions: any;
        private _showLegend: any;

        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);
            if (options) {
                if (options.showLegend != null) {
                    this._showLegend = options.showLegend;
                    delete options.showLegend;
                }

                if (options.flexPie) {
                    this._flexPieOptions = options.flexPie;
                } else if (options.flexChart) {
                    this._flexChartOptions = options.flexChart;
                }
                delete options.flexPie;
                delete options.flexChart;
            }
        }

        _afterInitializeControl(options): void {
            var pivotChart = <PivotChart>this.control;
            super._afterInitializeControl(options);
            if (this._flexPieOptions && pivotChart.flexPie) {
                pivotChart.flexPie.initialize(this._flexPieOptions);
            } else if (this._flexChartOptions && pivotChart.flexChart) {
                pivotChart.flexChart.initialize(this._flexChartOptions);
            }

            if (this._showLegend != null) {
                pivotChart.showLegend = this._showLegend;
            }
        }
    }

    /**
     * The @see:Slicer extends from @see:c1.olap.Slicer.
     */
    export class Slicer extends c1.olap.Slicer {
    }

    export class _SlicerWrapper extends _ControlWrapper {
        get _controlType(): any {
            return Slicer;
        }

        get _initializerType(): any {
            return _SlicerInitializer;
        }
    }

    // copy the field of engine in the options to the control.
    // process fields in the options(remove unnecessary fields).
    export class _SlicerInitializer extends _Initializer {
        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);
            if (options) {
                var engine = options.engine || c1.getService(options.pivotEngineId);
                var field = options.field;
                delete options.pivotEngineId;
                delete options.field;
                if (engine && field) {
                    options.field = engine.fields.getField(field);
                }
            }
        }
    }
}