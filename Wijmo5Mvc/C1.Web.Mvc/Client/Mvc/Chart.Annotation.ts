﻿/// <reference path="../Shared/Chart.Annotation.ts" />

/**
 * Defines the @see:c1.mvc.chart.annotation.AnnotationLayer and various annotations 
 * for @see:c1.mvc.chart.FlexChart and @see:c1.mvc.chart.finance.FinancialChart.
 */
module c1.mvc.chart.annotation {
    /**
     * The @see:AnnotationLayer control extends from @see:c1.chart.annotation.AnnotationLayer.
     */
    export class AnnotationLayer extends c1.chart.annotation.AnnotationLayer {
    }
} 