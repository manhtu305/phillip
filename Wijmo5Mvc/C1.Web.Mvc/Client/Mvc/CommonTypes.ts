﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts" />

/**
 * Defines common types in c1.mvc module.
 */
module c1.mvc {
    /**
         * Specifies whether a Date object represents a local time, a Coordinated
         * Universal Time (UTC), or is not specified as either local time or UTC.
         */
    export enum DateKind {
        /** The time represented is not specified as either local time or Coordinated Universal Time (UTC). */
        Unspecified = 1,
        /** The time represented is local time. */
        Local = 2,
        /** The time represented is UTC. */
        Utc = 3
    }

    /**
     * Provides arguments for json operation events.
     */
    export class JSONOperationEventArgs extends wijmo.CancelEventArgs {
        private _parent: any;
        private _key: string;
        private _value: any;
        private _result: any;

        /**
         * Casts the specified object to @see:c1.mvc.JSONOperationEventArgs type.
         * @param obj The object to cast.
         * @return The object passed in.
         */
        static cast(obj: any): JSONOperationEventArgs {
            return obj;
        }

        /**
         * Initializes a new instance of the @see:JSONOperationEventArgs class.
         *
         * @param key The name of the item.
         * @param value The value of the item.
         * @param parent The object owns the item.
         */
        constructor(key: string, value: any, parent: any) {
            super();
            this._key = key;
            this._value = value;
            this._parent = parent;
        }

        /**
        * Gets the parent object which owns the item.
        */
        get parent(): string {
            return this._parent;
        }
        /**
         * Gets the value of the item.
         */
        get value(): any {
            return this._value;
        }
        /**
         * Gets the name of the item.
         */
        get key(): any {
            return this._key;
        }
        /**
         * Gets or sets the operation result for some item.
         */
        get result(): any {
            return this._result;
        }
        set result(value: any) {
            this._result = value;
        }
    }
}