﻿/// <reference path="../Shared/Chart.LineMarker.ts" />

module c1.mvc.chart {
    /**
     * The @see:LineMarker control extends from @see:c1.chart.LineMarker.
     */
    export class LineMarker extends c1.chart.LineMarker {
    }
}