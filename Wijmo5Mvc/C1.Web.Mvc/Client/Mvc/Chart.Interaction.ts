﻿/// <reference path="../Shared/Chart.Interaction.ts" />

/**
 * Defines classes that add interactive features to charts.
 */
module c1.mvc.chart.interaction {
    /**
     * The @see:RangeSelector control extends from @see:c1.chart.interaction.RangeSelector.
     */
    export class RangeSelector extends c1.chart.interaction.RangeSelector {
    }

    /**
     * The @see:ChartGestures control extends from @see:c1.chart.interaction.ChartGestures.
     */
    export class ChartGestures extends c1.chart.interaction.ChartGestures {
    }
}