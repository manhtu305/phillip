﻿/// <reference path="Collections.ts" />
/// <reference path="Grid.ts" />

module c1.mvc.grid {

    // The extension for handling update actions (paste, delete).
    // Should notify the collection view when changing the cell value by pasting, deleting.
    export class _UpdateHelper {
        private readonly _grid: wijmo.grid.FlexGrid & _IFlexGridMvc;
        private _pastingItem;
        private _needToRemoveAllMultiRowNewRowTemplates: boolean = false;

        // Initializes a new instance of the @see:_UpdateHelper.
        // @param grid The @see:wijmo.grid.FlexGrid.
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc) {
            this._grid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);

            // handle paste
            grid.pasting.addHandler(this._onPasting, this);
            grid.pastingCell.addHandler(this._onPastingCell, this);
            grid.pasted.addHandler(this._onPasted, this);

            // handle delete
            var keyHdl = grid._keyHdl;
            if (keyHdl) {
                _overrideMethod(keyHdl, "_deleteSel",
                    this._beforeDeleteSel.bind(this), this._afterDeleteSel.bind(this));
            }
        }

        private _onPasting(sender: c1.grid.FlexGrid, e: wijmo.grid.CellRangeEventArgs) {
            this._pastingItem = null;
        }

        private _onPastingCell(sender: c1.grid.FlexGrid, e: wijmo.grid.CellRangeEventArgs) {
            var cv: any = this._grid.collectionView,
                editItem: any;

            if (e.panel.cellType === wijmo.grid.CellType.Cell && cv) {
                var itemIndex = Math.floor(e.row / this._grid._getRowsPerItemMvc());
                editItem = cv.items[itemIndex];
                if (editItem) {
                    // commit the previous pasted item.
                    if (this._pastingItem && this._pastingItem != editItem) {
                        cv.commitEdit();
                    }
                    // keep the pasting item for later commit
                    this._pastingItem = editItem;

                    // edit the pasting item.
                    cv.editItem(editItem);
                }
            }
        }

        private _onPasted(sender: c1.grid.FlexGrid, e: wijmo.grid.CellRangeEventArgs) {

            if (this._pastingItem) {
                this._pastingItem = null;
                // [371549] No need to commit current change to keep edit mode on pasted cell.
                // this._commitEdit();
            }
        }

        private _afterDeleteSel(e: KeyboardEvent) {
            // the row is still on editing mode after clear the content of the cell,
            // force commit the editing to send the update request.
            if (this._needToRemoveAllMultiRowNewRowTemplates) {
                this._updateNewRowTemplate();
                this._needToRemoveAllMultiRowNewRowTemplates = false;
            }
            this._commitEdit();
        }

        private _beforeDeleteSel(e: KeyboardEvent) {
            // # TFS 403910
            if (!this._grid.newRowAtTop || !this._grid.allowDelete || !this._grid.selection || this._grid.selectedRows.length == 0) return;
            for (let i = 0, rows = this._grid.selectedRows; i < rows.length; i++) {
                this._needToRemoveAllMultiRowNewRowTemplates = this._needToRemoveAllMultiRowNewRowTemplates || rows[i] instanceof wijmo.grid._NewRowTemplate;
                if (this._needToRemoveAllMultiRowNewRowTemplates) break;
            }
        }

        private _updateNewRowTemplate() {
            // First remove all the NewRowTemplate in the grid.
            for (let i = 0, rows = this._grid.rows; i < rows.length; i++) {
                if (rows[i] instanceof wijmo.grid._NewRowTemplate) {
                    rows.removeAt(i);
                    i--;
                }
            }
            // Update dataItem that has been cached when editing
            let _addHdl = this._grid._addHdl;
            if (_addHdl && _addHdl["_nrt"]) _addHdl["_nrt"]["dataItem"] = null;
            // Regenerate Rows template
            _addHdl.updateNewRowTemplate();
        }

        private _commitEdit() {
            var cv: any = this._grid.collectionView;
            // the endUpdate is called after all cells are pasted.
            // commit the last pasted item.
            cv.commitEdit();
        }
    }
}