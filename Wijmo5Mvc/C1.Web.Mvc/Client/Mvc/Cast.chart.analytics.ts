﻿module wijmo.chart.analytics.BoxWhisker {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.BoxWhisker type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): BoxWhisker {
        return obj;
    }
}

module wijmo.chart.analytics.ErrorBar {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.ErrorBar type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ErrorBar {
        return obj;
    }
}

module wijmo.chart.analytics.FunctionSeries {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.FunctionSeries type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FunctionSeries {
        return obj;
    }
}

module wijmo.chart.analytics.ParametricFunctionSeries {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.ParametricFunctionSeries type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ParametricFunctionSeries {
        return obj;
    }
}

module wijmo.chart.analytics.YFunctionSeries {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.YFunctionSeries type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): YFunctionSeries {
        return obj;
    }
}

module wijmo.chart.analytics.MovingAverage {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.MovingAverage type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): MovingAverage {
        return obj;
    }
}

module wijmo.chart.analytics.TrendLineBase {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.TrendLineBase type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TrendLineBase {
        return obj;
    }
}

module wijmo.chart.analytics.TrendLine {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.TrendLine type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): TrendLine {
        return obj;
    }
}

module wijmo.chart.analytics.Waterfall {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.Waterfall type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): Waterfall {
        return obj;
    }
}

module wijmo.chart.analytics.BreakEven {
    /**
     * Casts the specified object to @see:wijmo.chart.analytics.BreakEven type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): BreakEven {
        return obj;
    }
}

