﻿module wijmo.grid.cellmaker.CellMaker {
    /**
     * Casts the specified object to @see:wijmo.grid.cellmaker.CellMaker type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): CellMaker {
        return obj;
    }
}

