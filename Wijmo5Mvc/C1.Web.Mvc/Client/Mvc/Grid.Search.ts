﻿/// <reference path="../Shared/Grid.Search.ts" />
/// <reference path="Grid.ts" />

module c1.mvc.grid.search {

    export class _FlexGridSearchWrapper extends _ControlWrapper {
        get _controlType(): any {
            return FlexGridSearch;
        }

        get _initializerType(): any {
            return _FlexGridSearchInitializer;
        }
    }

    export class _FlexGridSearchInitializer extends c1.mvc._Initializer {
        _beforeInitializeControl(options: any): void {
            super._beforeInitializeControl(options);
            
            if (options) {
                var id: string = options.grid,
                    control: wijmo.Control
                    
                if (id) {
                    control = wijmo.Control.getControl("#" + id);
                    if (control) {
                        options.grid = control;
                    }
                }
            }
        }

        _afterInitializeControl(options: any) {
            super._afterInitializeControl(options);

            let gridSearch = this.control as FlexGridSearch;
            let cv = gridSearch.grid ? gridSearch.grid.collectionView : null;
            if (cv instanceof c1.mvc.collections.RemoteCollectionView) {
                cv._flexGridSearchs.push(gridSearch);
            }

            // IE: prevent applySearch when text is not changed
            var _originalApplySearch: Function = gridSearch['_applySearch'];
            gridSearch['_applySearch'] = function () {
                if ((gridSearch.text || "") != (gridSearch._lastText || "")) {
                    _originalApplySearch.apply(gridSearch, null);
                }
            }
        }
    }
    
    /**
     * The @see:FlexGridSearch control extends from @see: c1.grid.search.FlexGridSearch.
     */
    export class FlexGridSearch extends c1.grid.search.FlexGridSearch {
        _lastText: string;
    }
}