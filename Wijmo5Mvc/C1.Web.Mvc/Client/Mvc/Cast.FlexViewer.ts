﻿module wijmo.viewer.ViewerBase {
    /**
     * Casts the specified object to @see:wijmo.viewer.ViewerBase type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ViewerBase {
        return obj;
    }
}

module wijmo.viewer.ReportViewer {
    /**
     * Casts the specified object to @see:wijmo.viewer.ReportViewer type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): ReportViewer {
        return obj;
    }
}

module wijmo.viewer.PdfViewer {
    /**
     * Casts the specified object to @see:wijmo.viewer.PdfViewer type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PdfViewer {
        return obj;
    }
}

module wijmo.viewer.QueryLoadingDataEventArgs {
    /**
     * Casts the specified object to @see:wijmo.viewer.QueryLoadingDataEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): QueryLoadingDataEventArgs {
        return obj;
    }
}

module wijmo.viewer.RequestEventArgs {
    /**
     * Casts the specified object to @see:wijmo.viewer.RequestEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): RequestEventArgs {
        return obj;
    }
}

