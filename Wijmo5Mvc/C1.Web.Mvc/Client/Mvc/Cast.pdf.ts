﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.pdf.d.ts" />
module wijmo.pdf.PdfDocument {
    /**
     * Casts the specified object to @see:wijmo.pdf.PdfDocument type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PdfDocument {
        return obj;
    }
}

module wijmo.pdf.PdfPageArea {
    /**
     * Casts the specified object to @see:wijmo.pdf.PdfPageArea type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PdfPageArea {
        return obj;
    }
}

module wijmo.pdf.PdfDocumentEndedEventArgs {
    /**
     * Casts the specified object to @see:wijmo.pdf.PdfDocumentEndedEventArgs type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): PdfDocumentEndedEventArgs {
        return obj;
    }
}

