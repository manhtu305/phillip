﻿/// <reference path="Control.ts" />
/// <reference path="../Shared/Chart.ts" />

/**
 * Defines the @see:c1.mvc.chart.radar.FlexRadar control and its associated classes.
 */
module c1.mvc.chart.radar {

    export class _FlexRadarWrapper extends _FlexChartWrapper {
        get _controlType(): any {
            return c1.mvc.chart.radar.FlexRadar;
        }
    }

    /**
     * The @see:FlexRadar control displays radar chart.
     */
    export class FlexRadar extends c1.chart.radar.FlexRadar {
    }
}