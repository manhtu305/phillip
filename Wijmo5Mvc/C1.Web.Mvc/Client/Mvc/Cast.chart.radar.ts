﻿module wijmo.chart.radar.FlexRadar {
    /**
     * Casts the specified object to @see:wijmo.chart.radar.FlexRadar type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexRadar {
        return obj;
    }
}

module wijmo.chart.radar.FlexRadarAxis {
    /**
     * Casts the specified object to @see:wijmo.chart.radar.FlexRadarAxis type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexRadarAxis {
        return obj;
    }
}

module wijmo.chart.radar.FlexRadarSeries {
    /**
     * Casts the specified object to @see:wijmo.chart.radar.FlexRadarSeries type.
     * @param obj The object to cast.
     * @return The object passed in.
     */
    export function cast(obj: any): FlexRadarSeries {
        return obj;
    }
}

