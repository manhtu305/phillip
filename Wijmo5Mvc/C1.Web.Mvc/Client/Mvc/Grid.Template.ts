﻿/// <reference path="Collections.ts" />
/// <reference path="Grid.ts" />

module c1.mvc.grid {

    // The extension which provides custom item templates.
    export class _ItemTemplateProvider {
        private readonly _grid: wijmo.grid.FlexGrid & _IFlexGridMvc;
        private _collectionView: wijmo.collections.CollectionView;
        private _templates: any;
        private _activeEdt: HTMLElement;
        private _rng: wijmo.grid.CellRange;
        private _editedItemIndex: number;
        private _editedColIndex: number;
        private _celltemplates: any;
        private _windowWidth: number = window.innerWidth; //fix TFS-423975
        private _isRaiseRowEditEvent: boolean = false;

        // Initializes a new instance of the @see:_ItemTemplateProvider.
        // @param grid The @see:wijmo.grid.FlexGrid.
        constructor(grid: wijmo.grid.FlexGrid & _IFlexGridMvc) {
            this._grid = wijmo.asType(grid, wijmo.grid.FlexGrid, false);

            _overrideMethod(grid, _Initializer._INITIAL_NAME,
                this._beforeInitialize.bind(this),
                this._afterInitialize.bind(this));
        }

        private _beforeInitialize(options) {
            if (options) {
                this._processTemplates(options);
            }
        }

        private _afterInitialize(options) {
            if (!options) {
                return;
            }

            this._addHandlers();
            this._grid.itemsSourceChanged.addHandler(() => {
                this._removeHandlers();
                this._applyTemplates();
            });
        }

        private _addHandlers() {
            this._collectionView = wijmo.tryCast(this._grid.collectionView, wijmo.collections.CollectionView);
            if (this._collectionView) {
                if (this._collectionView.items
                    && this._collectionView.items.length > 0) {
                    this._applyTemplates();
                } else {
                    // remote collection view.
                    this._collectionView.collectionChanged.addHandler(this._applyTemplatesOnce, this);
                }
            }
        }

        private _applyTemplatesOnce() {
            setTimeout(() => { this._collectionView.collectionChanged.removeHandler(this._applyTemplatesOnce, this); });
            this._applyTemplates();
        }

        private _removeHandlers() {
            this._collectionView.collectionChanged.removeHandler(this._applyTemplatesOnce, this);
            if (!this._templates) {
                return;
            }
            this._removeEditTemplateHandlers();
            this._grid.formatItem.removeHandler(this._applyCellTemplates, this);
        }

        // process tempalte(rowHeaders, columnHeaders, cells, topLeftCells)
        private _processTemplates(options: any) {
            this._processPanelTemplates(options);
            this._processColumnsTempaltes(options);
        }

        // process the templates set in panels.
        private _processPanelTemplates(options: any) {
            var panelNames: string[] = ['rowHeadersTemplate', 'columnHeadersTemplate',
                'cellsTemplate', 'topLeftCellsTemplate',
                'columnFootersTemplate', 'bottomLeftCellsTemplate'],
                cellTypeName: string[] = ['RowHeader', 'ColumnHeader',
                    'Cell', 'TopLeft',
                    'ColumnFooter', 'BottomLeft'],
                count: number = panelNames.length,
                i: number,
                cellTemplate: any,
                panelName: string;

            for (i = 0; i < count; i++) {
                panelName = panelNames[i];
                cellTemplate = options[panelName];
                if (cellTemplate) {
                    delete options[panelName];
                }
                if (cellTemplate != null) {
                    this._updateTemplates(cellTypeName[i], cellTemplate);
                }
            }
        }

        // process the templates set in columns.
        private _processColumnsTempaltes(options: any, startCellIndex: number = 0) {
            var groups: any[], cells: any[], groupIndex = 0, cellIndex: number,
                cellTemplate: any, constTemplateName: string = 'cellTemplate';

            groups = this._grid._getLayoutGroupsMvc(options);
            if (groups && groups.length) {
                groups.forEach(group => {
                    cellIndex = startCellIndex;
                    cells = group.cells;
                    if (cells && cells.length) {
                        cells.forEach(cell => {
                            cellTemplate = cell[constTemplateName];
                            if (cellTemplate) {
                                delete cell[constTemplateName];
                            }
                            if (cellTemplate != null) {
                                this._updateTemplates('Cell', cellTemplate, groupIndex, cellIndex);
                            }
                            // Template property here is represented for new CellTemplate property in Wijmo 637
                            // Hereby replace Template with CellTemplate
                            if (cell['template']) {
                                cell.cellTemplate = cell['template'];
                                delete cell['template'];
                            }
                            if (cell['templateFunction']) {
                                var cellMaker = wijmo.grid.cellmaker.CellMaker;
                                cell.cellTemplate = cell['templateFunction'].call(cellMaker,cellMaker);
                                delete cell['templateFunction'];
                            }
                            // Proccess templates for child columns in case ColumnGroups
                            if (cell['columns']) {
                                this._processColumnsTempaltes(cell, cellIndex);
                            }
                            cellIndex++;
                        });
                    }
                    groupIndex++;
                });
            }
        }

        // save the templates according to its cell type and col
        // The data structure is:
        //  {
        //     the celltype name: 
        //     {
        //         // here -1 stands for the default cell template.
        //         -1: {template: Template, editTemplate: Template},
        //
        //         // here group_cell stands for the specified column cell's template.
        //         group_cell: {template: Template, editTemplate: Template},
        //     }
        //  }
        private _updateTemplates(celltypeName: string, template: any, groupIndex: number = -1, cellIndex: number = -1) {
            var typeTemplates: any,
                key: string = (groupIndex === -1 && cellIndex === -1) ? '-1' : groupIndex + '_' + cellIndex;

            if (template != null) {
                if (!this._templates) {
                    this._templates = {};
                }
                typeTemplates = this._templates[celltypeName];
                if (!typeTemplates) {
                    typeTemplates = {};
                }
                typeTemplates[key] = template;
                this._templates[celltypeName] = typeTemplates;
            }
        }

        private _applyTemplates() {
            if (!this._templates) {
                return;
            }

            this._celltemplates = {};

            Utils.copy(this._celltemplates, this._templates);

            for (var cellTypeName in this._celltemplates) {
                // {
                //   //here -1 stands for the default cell template.
                //   -1: {template: Template, editTemplate: Template},
                //
                //   // here group_cell stands for the specified column cell's template.
                //   group_cell: {template: Template, editTemplate: Template},
                //   ...
                // }
                var cellTemplate = this._celltemplates[cellTypeName];
                if (cellTypeName === 'Cell') {
                    cellTemplate = this._updateCellsTemplate(cellTypeName, cellTemplate);
                    this._celltemplates[cellTypeName] = cellTemplate;
                }
            }

            this._addEditTemplateHandlers();
            this._grid.formatItem.addHandler(this._applyCellTemplates, this);
        }

        // update the key of the cells' templates.
        private _updateCellsTemplate(cellTypeName: string, cellTemplates: any) {
            var key: string,
                updatedCellTemplates: any = {},
                cell: wijmo.grid.Column,
                grid = this._grid,
                indices: string[],
                groupIndex: number,
                cellIndex: number;

            for (key in cellTemplates) {
                if (parseInt(key) != -1) {
                    indices = key.split('_');
                    groupIndex = parseInt(indices[0]);
                    cellIndex = parseInt(indices[1]);

                    cell = this._grid._getBindingColumnMvc(groupIndex, cellIndex);
                    updatedCellTemplates[cellTypeName + cell._hash] = cellTemplates[key];

                } else {
                    updatedCellTemplates[key] = cellTemplates[key];
                }
            }
            return updatedCellTemplates;
        }

        private _applyCellTemplates(grid: any, e: wijmo.grid.FormatItemEventArgs) {
            if (!this._celltemplates) {
                return;
            }
            var cellType: wijmo.grid.CellType = e.panel.cellType;
            var isEditing = this._isEditing(cellType, e.row, e.col);
            if (cellType === wijmo.grid.CellType.Cell && isEditing) {
                return;
            }

            var cellTypeName = wijmo.grid.CellType[cellType],
                cellTemplates = this._celltemplates[cellTypeName];
            if (!cellTemplates) {
                return;
            }

            var column = this._grid._getBindingColumnFromCellRangeMvc(e),
                // {template: Template, editTemplate: Template}
                cellTemplate = this._getCellTemplate(cellTypeName, column._hash, cellTemplates);
            if (!cellTemplate) {
                return;
            }

            var template = cellTemplate.templateId
                || cellTemplate.templateContent,
                editTemplate = cellTemplate.editTemplateId
                    || cellTemplate.editTemplateContent,
                templateData = {
                    row: e.row,
                    column: e.col
                },
                visibility: string = '';

            // When auto-sizing the column, a hidden cell will be added to flexgrid to get the actual size of cell.
            // Then template in this hidden cell will be the same as the cell in flexgrid, which means there are
            // two same id in the page, should separate them.
            if (e.cell && e.cell.style && e.cell.style.visibility === 'hidden') {
                visibility = 'hidden';
            }

            templateData[c1.mvc.Template.UID] = this._getUnqiueIdentifier() + '_' + cellTypeName + '_r' + e.row + '_c' + e.col + visibility;
            templateData[c1.mvc.Template.DATACONTEXT] = (<wijmo.grid.Row>(this._grid.rows[e.row])).dataItem;
            if (!isEditing && template) {
                template.applyTo(e.cell, templateData);
            } else if (isEditing && editTemplate) {
                editTemplate.applyTo(e.cell, templateData);
            }
        }

        private _resizeWindow() {
            //fix TFS-423975
            if ((/Android/.test(navigator.userAgent))) {
                if ((this._windowWidth == window.innerWidth))
                return;                
                else this._windowWidth = window.innerWidth;
            }            
            if (!!this._activeEdt && (this._containsFocus(this._activeEdt)
                || this._containsFocus(<HTMLElement>this._activeEdt.firstElementChild))) {
                this._closeEditor(true);
                this._grid.focus();
            }
        }

        private static _hasEditTemplate(templates): boolean {
            for (var key in templates) {
                var template = templates[key];
                if (template && (template.editTemplateId || template.editTemplateContent)) {
                    return true;
                }
            }

            return false;
        }

        private _getCellTemplate(cellTypeName: string, colHash: string, cellTemplates: any) {
            var colKey: string,
                cellTypeName: string,
                cellTemplate: any;

            if (wijmo.grid.CellType[cellTypeName] == wijmo.grid.CellType.Cell) {
                colKey = cellTypeName + colHash;
                cellTemplate = cellTemplates[colKey];
            }
            if (cellTemplate == null) {
                cellTemplate = cellTemplates['-1'];
            }

            return cellTemplate;
        }

        private _addEditTemplateHandlers() {
            if (!this._celltemplates || !this._celltemplates['Cell']) {
                return;
            }
            var cellTemplates = this._celltemplates['Cell'];
            if (_ItemTemplateProvider._hasEditTemplate(cellTemplates)) {
                // close editor when user resizes the window.
                window.addEventListener('resize', this._resizeWindow.bind(this));
                this._grid.beginningEdit.addHandler(this._applyCellEditTemplate, this);
                this._grid.scrollPositionChanged.addHandler(this._scrollHandler, this);
                // add for confirming the previous editing
                this._grid.prepareCellForEdit.addHandler(this._saveEdit, this);
                // add for confirming the previous editing
                this._grid.selectionChanging.addHandler(this._saveEdit, this);
                // TFS 431421
                this._grid.selectionChanging.addHandler((s, e: wijmo.grid.CellRangeEventArgs) => {
                    if (e.row != s.selection.row) {
                        this._raiseRowEditsEvent();
                    }
                });
            }
        }

        private _removeEditTemplateHandlers() {
            window.removeEventListener('resize', this._resizeWindow.bind(this));
            this._grid.beginningEdit.removeHandler(this._applyCellEditTemplate, this);
            this._grid.scrollPositionChanged.removeHandler(this._scrollHandler, this);
            this._grid.prepareCellForEdit.removeHandler(this._saveEdit, this);
            this._grid.selectionChanging.removeHandler(this._saveEdit, this);
            // TFS 431421
            this._grid.selectionChanging.removeHandler((s, e: wijmo.grid.CellRangeEventArgs) => {
                if (e.row != s.selection.row) {
                    this._raiseRowEditsEvent();
                }
            });
        }
        private _scrollHandler(grid: wijmo.grid.FlexGrid, args: wijmo.grid.CellRangeEventArgs) {
            this._closeEditor(!args.cancel);
        }
        private _saveEdit() {
            this._closeEditor(true);
        }

        private _getUnqiueIdentifier(): string {
            return this._grid.hostElement.id || c1.mvc._getUniqueId(this._grid);
        }

        private _getCellBoundingRect(r: number, c: number): wijmo.Rect {
            //var cellEle = this._grid.cells.getCellElement(r, c);
            //if (cellEle) {
            //    var clientRect = cellEle.getBoundingClientRect();
            //    return new wijmo.Rect(clientRect.left, clientRect.top, clientRect.width, clientRect.height);
            //}

            //TFS 384087
            return this._grid.cells.getCellBoundingRect(r, c);
        }

        private _applyCellEditTemplate(grid: wijmo.grid.FlexGrid, args: wijmo.grid.CellRangeEventArgs) {
            if (args.cancel) {
                return;
            }
            // check that this is not the Delete key 
            // (which is used to clear cells and should not be messed with)
            var evt = args.data;
            if (evt && evt.keyCode == wijmo.Key.Delete) {
                return;
            }

            var column = this._grid._getBindingColumnFromCellRangeMvc(args),
                cellTemplate = this._getCellTemplate('Cell', column._hash, this._celltemplates['Cell']);
            if (!cellTemplate) {
                return;
            }

            var editTemplate = cellTemplate.editTemplateId
                || cellTemplate.editTemplateContent;
            if (!editTemplate) {
                return;
            }

            var col: number = args.col,
                row: number = args.row,
                templateData = {
                    row: row,
                    column: col
                };
            templateData[c1.mvc.Template.UID] = this._getUnqiueIdentifier() + '_' + 'Cell' + '_r' + row + '_c' + col;
            templateData[c1.mvc.Template.DATACONTEXT] = (<wijmo.grid.Row>(grid.rows[row])).dataItem;

            // cancel built-in editor
            args.cancel = true;

            var itemIndex: number = grid._getCvIndex(row);
            if (itemIndex < 0) {
                return;
            }

            // when the cell has been in editing, do nothing.
            if (this._editedItemIndex == itemIndex && this._editedColIndex == col) {
                return;
            }

            this._editedItemIndex = itemIndex;
            this._editedColIndex = col;

            // save cell being edited
            this._rng = args.range;
            this._isRaiseRowEditEvent = false;
            var edtConainter = document.createElement('div');

            // activate editor container
            document.body.appendChild(edtConainter);
            edtConainter.addEventListener('keydown', (e: KeyboardEvent) => {
                switch (e.keyCode) {
                    case wijmo.Key.Enter:
                    case wijmo.Key.Tab:
                        e.preventDefault(); // TFS 255685
                        this._closeEditor(true);
                        if (wijmo.isIE()) {
                            setTimeout(() => {
                                grid.focus();
                            }, 0);
                        }
                        else {
                            grid.focus();
                        }

                        // forward event to the grid so it will move the selection
                        var evt = document.createEvent('HTMLEvents');
                        evt.initEvent('keydown', true, true);
                        evt['ctrlKey'] = e.ctrlKey;
                        evt['shiftKey'] = e.shiftKey;
                        evt['keyCode'] = e.keyCode;
                        grid.hostElement.dispatchEvent(evt);
                        break;
                    case wijmo.Key.Escape:
                        this._closeEditor(false);
                        // 354376 : IE is taking more time to render the UI than Chrome,
                        // cause activeCell is not ready and unfocusable, instead IE force focus to grid,
                        // then grid try to set focus to it's active cell
                        // that cause infinite focus loop until resource is all out and IE crash.
                        if (wijmo.isIE()) {
                            setTimeout(() => {
                                grid.focus();
                            }, 0);
                        }
                        else {
                            grid.focus();
                        }
                        break;
                }
            });

            // initialize editor host
            var rc = this._getCellBoundingRect(row, col);
            wijmo.setCss(edtConainter, {
                position: 'absolute',
                left: rc.left - 1 + pageXOffset,
                top: rc.top - 1 + pageYOffset,
                width: rc.width + 1,
                height: grid.rows[row].renderHeight + 1,
                borderRadius: '0px',
                zIndex: "2"
            });

            editTemplate.applyTo(edtConainter, templateData);
            var editor = <HTMLElement>edtConainter.firstElementChild;
            var wijmoControl = wijmo.Control.getControl(editor);
            this._listenLostFocus(wijmoControl);
            this._activeEdt = edtConainter;

            // start editing item
            var item = grid.collectionView.items[itemIndex];
            var needUpdateInputSelection: boolean = false;
            if (grid.collectionView && item) {
                // let the item enter editing after the editor focus.
                // Otherwise, the calling of the editor's focus method will cause the grid bluring,
                // it will let the grid exit from the editing mode.
                setTimeout(() => {
                    // for 309405: only when the row is still in editing
                    if (itemIndex == this._editedItemIndex) {
                        // TFS 356699 : Fire RowEditStarting event
                        grid.onRowEditStarting(args);
                        (<wijmo.collections.CollectionView>grid.collectionView).editItem(item);
                        // TFS 356699 : Fire RowEditStarted event
                        grid.onRowEditStarted(args);
                        // TFS 342751 need update input selection after it reforcus.
                        if (needUpdateInputSelection && key) {
                            var input = <HTMLInputElement>editor.querySelector('input.wj-form-control');
                            if (!input) input = <HTMLInputElement>editor.querySelector('input');
                            if (!input && editor instanceof HTMLInputElement) input = editor;
                            if (input && input.value.toLowerCase().charCodeAt(0) == key.toLocaleLowerCase().charCodeAt(0)) wijmo.setSelectionRange(input, input.value.length == 0 ? 0 : 1, input.value.length);
                        }
                    }
                }, 210);
            }

            // get the key that triggered the editor
            var key = (evt && evt.charCode > 32)
                ? String.fromCharCode(evt.charCode)
                : null;

            // set focus for wijmo control, if it's editing by pressing normal keys (like A, B, 1, 2).
            // if does not set focus here, the char send to the editor will be selected in highlight.
            if (wijmoControl && key) {
                wijmoControl.focus();
            }

            setTimeout(() => {
                // set focus for editor
                if (!wijmoControl || !key) {
                    editor.focus();
                }

                // send key to editor
                if (key) {
                    var input = <HTMLInputElement>editor.querySelector('input.wj-form-control');
                    if (!input) {
                        input = <HTMLInputElement>editor.querySelector('input');
                    }
                    if (!input && editor instanceof HTMLInputElement) {
                        input = editor;
                    }

                    if (input instanceof HTMLInputElement) {
                        input.value = key;
                        wijmo.setSelectionRange(input, key.length, key.length);
                        var evtInput = document.createEvent('HTMLEvents');
                        evtInput.initEvent('input', true, false);
                        input.dispatchEvent(evtInput);
                        needUpdateInputSelection = true;
                    }
                } else {
                    // set the focus for wijmo control again, sometimes the focus will be catched by grid in somewhere.
                    if (wijmoControl) {
                        wijmoControl.focus();
                    }
                }
            }, 50);
        }

        private _listenLostFocus(wijmoControl: wijmo.Control) {
            var self = this;
            if (wijmoControl) {
                var lostFocusHanlder = () => {
                    setTimeout(() => { // Chrome/FireFox need a timeOut here... (TFS 138985)
                        wijmoControl.lostFocus.removeHandler(lostFocusHanlder, self);
                        if (!wijmoControl.containsFocus()
                            && self._activeEdt
                            && self._activeEdt.firstElementChild === wijmoControl.hostElement) {
                            self._closeEditor(true);
                        }
                    });
                };
                wijmoControl.lostFocus.addHandler(lostFocusHanlder, this);
            }
        }
        
        // checks whether an element contains the focus
        private _containsFocus(element: HTMLElement): boolean {
            var control = wijmo.Control.getControl(element);
            return control
                ? control.containsFocus() // controls may have popups...
                : wijmo.contains(element, document.activeElement);
        }

        private _closeEditor(saveEdits: boolean) {
            // check that the editor is active
            if (!this._activeEdt || this._editedItemIndex == null) {
                return;
            }
            var activeEdt = this._activeEdt,
                editedItemIndex = this._editedItemIndex,
                cellElement = activeEdt.parentElement;

            this._activeEdt = null;
            this._editedItemIndex = null;
            this._editedColIndex = null;
            if (cellElement) {
                var grid = this._grid,
                    e = new wijmo.grid.CellEditEndingEventArgs(grid.cells, this._rng),
                    edt = wijmo.Control.getControl(activeEdt.firstElementChild);
                e.cancel = !saveEdits;
                grid.onCellEditEnding(e);
                // TFS 356699 : Fire RowEditEnding event
                // TFS 431421: remove
                //grid.onRowEditEnding(e);

                // save editor value into grid
                if (!e.cancel) {
                    if (edt != null) {
                        if (edt instanceof wijmo.input.DropDown) {
                            var dd = <any>edt;
                            if (dd._commitText) {
                                dd._commitText();
                            }
                        }

                        var sel = grid.selection,
                            editCV = this._collectionView;
                        editCV.editItem(editCV.items[editedItemIndex]);
                        if (!wijmo.isUndefined(edt['value'])) {
                            grid.setCellData(this._rng.row, this._rng.col, edt['value']);
                        } else if (!wijmo.isUndefined(edt['text'])) {
                            grid.setCellData(this._rng.row, this._rng.col, edt['text']);
                        }
                        editCV.commitEdit();
                        editCV.commitNew();
                        // TFS: 395534
                        //grid.select(sel);
                    }
                }

                // close editor and remove it from the DOM
                if (edt instanceof wijmo.input.DropDown) {
                    (<wijmo.input.DropDown>edt).isDroppedDown = false;
                }
                cellElement.removeChild(activeEdt);
                // TFS: 395534
                this._rng = null;
                this._isRaiseRowEditEvent = true;
                grid.onCellEditEnded(e);
                // TFS 356699 : Fire RowEditEnded event
                // TFS 431421: remove
                //grid.onRowEditEnded(e);
            }
        }

        // Judge the specified cell with the specified celltype is in editing mode.
        private _isEditing(cellType: wijmo.grid.CellType, row: number, col: number): boolean {
            var isEditing: boolean = false;

            if (this._grid.isReadOnly) {
                return false;
            }

            switch (cellType) {

                // if some cell in current row is in editing mode, the mode of the rowheader cell is editing.
                case wijmo.grid.CellType.RowHeader:
                    isEditing = this._isCellEditing(row);
                    break;

                // if some cell in current column is in editing mode, the mode of the columnheader cell is editing.
                case wijmo.grid.CellType.ColumnHeader:
                    isEditing = this._isCellEditing(-1, col);
                    break;

                // judge the cell in the grid content area is in editing mode.
                case wijmo.grid.CellType.Cell:
                    isEditing = this._isCellEditing(row, col);
                    break;

                // if the grid is in editing mode, the mode of the topleft cells are in editing mode.
                case wijmo.grid.CellType.TopLeft:
                    isEditing = this._isCellEditing();
                    break;

                // for the cells in other panels, always show displayed cell template.
                default:
                    break;
            }

            return isEditing;
        }

        private _isCellEditing(row: number = -1, col: number = -1): boolean {
            var cellRange: wijmo.grid.CellRange = this._grid.editRange;

            if (cellRange == null) {
                return false;
            }

            if (row != -1 && col != -1) {
                return cellRange.contains(row, col);
            }

            if (row != -1) {
                return cellRange.containsRow(row);
            }

            if (col != -1) {
                return cellRange.containsColumn(col);
            }

            return true;
        }
        
        private _raiseRowEditsEvent() {
            var flexGrid = this._grid,
                ecv = flexGrid.editableCollectionView;

            if (ecv && this._isRaiseRowEditEvent) {
                var e = new wijmo.grid.CellEditEndingEventArgs(flexGrid.cells, flexGrid.selection);
                setTimeout(() => { // let cell edit events fire first
                    flexGrid.onRowEditEnding(e);
                    flexGrid.onRowEditEnded(e);
                    flexGrid.invalidate();
                });
            }
            this._isRaiseRowEditEvent = false;
        }
    }
}