﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.selector.d.ts"/>

/**
 * Defines the Selector extender and associated classes.
 */
module c1.grid.selector {
    /**
     * The @see:Selector extender extends the @see:wijmo.grid.selector.Selector extender
     * which adds checkboxes to cells in the first row header column of {@link FlexGrid}.
     */
    export class Selector extends wijmo.grid.selector.Selector {
    }

    /**
     * The @see:BooleanChecker extender extends the @see:wijmo.grid.selector.BooleanChecker extender
     * which adds extra checkboxes to header and group cells of
     * {@link FlexGrid} boolean columns to allow setting the values for
     * all items or groups.
     */
    export class BooleanChecker extends wijmo.grid.selector.BooleanChecker {
    }
}