﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.animation.d.ts" />

/**
 * Defines the @see:c1.chart.animation.ChartAnimation
 * for @see:c1.chart.FlexChart, @see:c1.chart.finance.FinancialChart and @see: c1.chart.FlexPie.
 */
/**
 * Defines the @see:ChartAnimation for @see:FlexChart, @see:FinancialChart and @see:FlexPie.
 */
module c1.chart.animation {
    /**
     * The @see:ChartAnimation control extends from @see:wijmo.chart.animation.ChartAnimation.
    */
    export class ChartAnimation extends wijmo.chart.animation.ChartAnimation {
    }
}