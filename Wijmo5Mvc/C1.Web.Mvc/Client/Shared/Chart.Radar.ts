﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.radar.d.ts"/>

/**
 * Defines the @see:c1.chart.radar.FlexRadar control and its associated classes.
 */
module c1.chart.radar {

    export class FlexRadar extends wijmo.chart.radar.FlexRadar {
    }

}