﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.olap.d.ts" />

/**
 * Defines the olap components and associated classes.
 */
module c1.olap {

    /**
     * The @see:PivotEngine extends from @see:wijmo.olap.PivotEngine.
     */
    export class PivotEngine extends wijmo.olap.PivotEngine {
    }

    /**
     * The @see:PivotPanel extends from @see:wijmo.olap.FlexPivotPanel.
     */
    export class PivotPanel extends wijmo.olap.PivotPanel {
    }

    /**
     * The @see:PivotGrid extends from @see:wijmo.olap.PivotGrid.
     */
    export class PivotGrid extends wijmo.olap.PivotGrid {
    }

    /**
     * The @see:PivotChart extends from @see:wijmo.olap.PivotChart.
     */
    export class PivotChart extends wijmo.olap.PivotChart {
    }

    /**
     * The @see:Slicer extends from @see:wijmo.olap.Slicer.
     */
    export class Slicer extends wijmo.olap.Slicer {
    }
}
