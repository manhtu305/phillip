﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.transposed.d.ts"/>

/**
 * Defines the TransposedGrid control and associated classes.
 */
module c1.grid.transposed {
    /**
     * The @see:TransposedGrid control extends the @see:wijmo.grid.transposed.TransposedGrid control
     * Which display data using a transposed layout, where columns represent data items and rows represent item properties.
     */
    export class TransposedGrid extends wijmo.grid.transposed.TransposedGrid {
	}
}