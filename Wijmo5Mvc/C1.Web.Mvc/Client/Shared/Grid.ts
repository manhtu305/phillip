﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.d.ts" />

/**
 * Defines the @see:c1.grid.FlexGrid control and associated classes.
 */
module c1.grid {

    /**
     * The @see:FlexGrid control extends from @see:wijmo.grid.FlexGrid.
     */
    export class FlexGrid extends wijmo.grid.FlexGrid {
    }
}
