﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.hierarchical.d.ts"/>

/**
 * Defines the @see:c1.chart.hierarchical.Sunburst control and its associated classes.
 */
module c1.chart.hierarchical {

    export class Sunburst extends wijmo.chart.hierarchical.Sunburst {
    }

    /**
     * The @see:TreeMap control extends from @see:wijmo.chart.hierarchical.TreeMap.
     */
    export class TreeMap extends wijmo.chart.hierarchical.TreeMap {
    }

}