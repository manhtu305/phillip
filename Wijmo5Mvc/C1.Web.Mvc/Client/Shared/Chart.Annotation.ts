﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.annotation.d.ts" />

/**
 * Defines the @see:c1.chart.annotation.AnnotationLayer and various annotations 
 * for @see:c1.chart.FlexChart and @see:c1.chart.finance.FinancialChart.
 */
module c1.chart.annotation {
    /**
     * The @see:AnnotationLayer control extends from @see:wijmo.chart.annotation.AnnotationLayer.
    */
    export class AnnotationLayer extends wijmo.chart.annotation.AnnotationLayer {
    }
} 