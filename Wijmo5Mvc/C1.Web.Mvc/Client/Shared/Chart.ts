﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.d.ts"/>
/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.interaction.d.ts"/>
/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.analytics.d.ts"/>

/**
 * Defines the @see:c1.chart.FlexChart control and its associated classes.
 */
module c1.chart {
    /**
     * The @see:FlexChart control extends from @see:wijmo.chart.FlexChart.
     */
    export class FlexChart extends wijmo.chart.FlexChart {
    }

    /**
     * The customized wijmo @see:FlexPie control extends from @see:wijmo.chart.FlexPie.
     */
    export class FlexPie extends wijmo.chart.FlexPie {
    }
}