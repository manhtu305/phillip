﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.cellmaker.d.ts"/>

/**
 * Defines the CellMaker and associated classes.
 */
module c1.grid.cellmaker {
    /**
     * Provides methods for creating cells with custom content such as 
     * Buttons, Hyperlinks, Images, Ratings, and Sparklines.
     * 
     * To use these methods, assign their result to a column's 
     * {@link Column.cellTemplate} property.
     */
    export class CellMaker extends wijmo.grid.cellmaker.CellMaker {
    }
}