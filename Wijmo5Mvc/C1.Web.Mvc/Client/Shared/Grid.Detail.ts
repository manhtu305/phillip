﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.detail.d.ts" />

/**
 * Extension that provides detail rows for @see:c1.grid.FlexGrid controls.
 */
module c1.grid.detail {
    /**
     * The @see:FlexGridDetailProvider control extends from @see:wijmo.grid.detail.FlexGridDetailProvider.
    */
    export class FlexGridDetailProvider extends wijmo.grid.detail.FlexGridDetailProvider {
    }
}