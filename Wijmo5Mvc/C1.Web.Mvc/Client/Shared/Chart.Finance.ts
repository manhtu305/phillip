﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.finance.d.ts"/>

/**
 * Defines the @see:c1.chart.finance.FinancialChart control and associated classes.
 */
module c1.chart.finance {
    /**
     * The @see:FinancialChart control extends from @see:wijmo.chart.finance.FinancialChart.
    */
    export class FinancialChart extends wijmo.chart.finance.FinancialChart {
    }
}