/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts"/>

/**
 * Defines the C1 controls, associated functions and classes.
 */
module c1 {

    // Client version for xmldoc.
    var _VERSION = '4.0.20202.55555';

    var _EXTENDERS_NAME = '_c1Extenders';

    /**
     * Gets the extender with specified owner and id.
     *
     * @param owner The @see:wijmo.Control who has the extender.
     * @param id The extender's id.
     */
    export function getExtender(owner: wijmo.Control, id: string): Object {
        if (!owner || id == null) {
            return null;
        }

        var extenders = owner[_EXTENDERS_NAME];
        return extenders ? extenders[id] : null;
    }

    /**
     * Gets the extenders with specified owner and type.
     *
     * For example, below code is used for getting all LineMarker extenders
     * from the chart1 which is an instance of FlexChart.
     * <pre>
     * var lineMarkers = c1.getExtenders(chart1, wijmo.chart.LineMarker);
     * </pre>
     *
     * Note: if type is not passed, this returns all extenders under the owner.
     *
     * @param owner The @see:wijmo.Control who has the extender.
     * @param type The extender's type. It is optional.
     */
    export function getExtenders(owner: wijmo.Control, type?: any): Object[] {
        var extenders: Object,
            results: Object[] = [],
            extender: Object;
        if (!owner) {
            return results;
        }

        extenders = owner[_EXTENDERS_NAME];
        if (!extenders) {
            return results;
        }

        for (var id in extenders) {
            extender = extenders[id];
            if (!type || extender instanceof type) {
                results.push(extender);
            }
        }

        return results;
    }

    export function _addExtender(owner: wijmo.Control, id: string, extender: Object) {
        if (!owner || id == null || !extender) {
            return;
        }

        var extenders = owner[_EXTENDERS_NAME];
        if (!extenders) {
            owner[_EXTENDERS_NAME] = extenders = {};
        }

        extenders[id] = extender;
    }

    export function _addEvent(prop, value): boolean {
        if (prop && value && prop instanceof wijmo.Event && wijmo.isFunction(value)) {
            //Add the function as event handler to wijmo.Event
            (<wijmo.Event>prop).addHandler(value);
            return true;
        }

        return false;
    }

    // Find the function with the specified name.
    export function _findFunction(name: string) {
        if (!name) {
            return null;
        }

        var result = window;
        name.split('.').forEach((levelName) => result = result[levelName]);
        return result;
    }

    /**
     * Specify a function to execute when the DOM is fully loaded.
     *
     * @param callback A function to execute after the DOM is ready.
     */
    export function documentReady(callback: Function) {
        document.addEventListener('DOMContentLoaded', callback.bind(window));
    }

    // get the extension with the specified owner and type.
    export function _getExtension(owner: wijmo.Control, extensionType: any) {
        if (owner && owner['extensions']) {
            var extensions = owner['extensions'];
            for (var index in extensions) {
                if (extensions[index] instanceof extensionType) {
                    return extensions[index];
                }
            }
        }
    }

    export class _Initializer {

        private _control: Object;

        constructor(control: Object) {
            if (control == null) {
                throw "control should not be null or undefined.";
            }

            this._control = control;
            this._override();
        }

        get control(): Object {
            return this._control;
        }

        _override(): void {
            _Initializer._overrideCopy(this.control);
        }

        // Customize wijmo 5 control's copy for custimizing the initializing
        private static _overrideCopy(control: Object): void {
            var proto = Object.getPrototypeOf(control), wjControlCopy = proto['_copy'];
            if (proto['_isCopyOverrided'])
                return;
            proto["_copy"] = function (key: string, value: any) {
                var prop = this[key];
                if (_addEvent(prop, value)) {
                    return true;
                }

                return wjControlCopy ? wjControlCopy.call(this, key, value) : false;
            };
            proto['_isCopyOverrided'] = true;
        }
    }

    export class _ControlWrapper {
        private _control: wijmo.Control;
        static _DATA_KEY: string = '_c1ControlWrapper';

        constructor(element: any, options?: any) {
            var self = this;
            self._beforeCreateControl(options);
            self._createControl(element);
            self._bindControl();
            self._initExtensions();
        }

        _beforeCreateControl(options?: any) {
        }

        private _createControl(element: any): void {
            var self = this, wjCtor = self._controlType;
            self._control = new wjCtor(element);
        }

        private _bindControl(): void {
            _ControlWrapper.setWrapper(this.control, this);
        }

        static setWrapper(control: wijmo.Control, wrapper: _ControlWrapper): void {
            control[_ControlWrapper._DATA_KEY] = wrapper;
        }

        static getWrapper(control: wijmo.Control): _ControlWrapper {
            return control[_ControlWrapper._DATA_KEY];
        }

        private _initExtensions(): void {
            var types = this._getExtensionTypes();

            if (types) {
                var extensions = [];
                types.reverse().forEach((extensionType) => {
                    extensions.push(new extensionType(this.control));
                });
                this.control['extensions'] = extensions;
            }
        }

        initialize(): void {
            this.control.initialize.apply(this.control, arguments);
        }

        _getExtensionTypes(): any[]{
            return [this._initializerType];
        }

        get _initializerType(): any {
            return _Initializer;
        }

        // Get the wijmo control's constructor.
        get _controlType(): any {
            throw "Should implement this property getter in the leaf control wrapper.";
        }

        // Get the wijmo control instance.
        get control(): wijmo.Control {
            return this._control;
        }
    }
}
