﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.filter.d.ts" />

/**
 * Extension that provides an Excel-style filtering UI for @see:c1.grid.FlexGrid controls.
 */
module c1.grid.filter {
    /**
     * The @see:FlexGridFilter control extends from @see:wijmo.grid.filter.FlexGridFilter.
     */
    export class FlexGridFilter extends wijmo.grid.filter.FlexGridFilter {
    }
} 