﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.search.d.ts"/>

/**
 * Defines the FlexGridSearch control and associated classes.
 */
module c1.grid.search {
    /**
     * The @see:FlexGridSearch control extends the @see:wijmo.grid.search.FlexGridSearch control
     * which allows users to quickly search the items displayed in a FlexGrid.
     */
    export class FlexGridSearch extends wijmo.grid.search.FlexGridSearch {
	}
}