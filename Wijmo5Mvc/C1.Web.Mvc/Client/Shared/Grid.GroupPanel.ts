﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.grouppanel.d.ts" />

/**
 * Extension that provides a drag and drop UI for editing groups in bound @see:c1.grid.FlexGrid controls.
 */
module c1.grid.grouppanel {
    /**
     * The @see:GroupPanel control extends from @see:wijmo.grid.grouppanel.GroupPanel.
    */
    export class GroupPanel extends wijmo.grid.grouppanel.GroupPanel {

    }
}