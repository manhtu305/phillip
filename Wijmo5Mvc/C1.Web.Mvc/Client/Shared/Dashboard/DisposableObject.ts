﻿module c1.nav {
    'use strict';

    /**
     * Base class for the disposable objects in dashboardlayout.
     *
     * The object also provides a set of methods to attach and remove the events on the dom elements.
     */
    export class DisposableObject {
        // list of event listeners attached to this control
        protected _listeners;

        /**
         * Adds an event listener to an element owned by this @see:Control.
         *
         * The control keeps a list of attached listeners and their handlers,
         * making it easier to remove them when the control is disposed (see the
         * @see:dispose and @see:removeEventListener methods).
         *
         * Failing to remove event listeners may cause memory leaks.
         *
         * @param target Target element for the event.
         * @param type String that specifies the event.
         * @param fn Function to execute when the event occurs.
         * @param capture Whether the listener is capturing.
         */
        addEventListener(target: EventTarget, type: string, fn: any, capture = false) {
            if (target) {
                target.addEventListener(type, fn, capture);
                if (this._listeners == null) {
                    this._listeners = [];
                }
                this._listeners.push({ target: target, type: type, fn: fn, capture: capture });
            }
        }
        /**
         * Removes one or more event listeners attached to elements owned by this @see:Control.
         *
         * @param target Target element for the event. If null, removes listeners attached to all targets.
         * @param type String that specifies the event. If null, removes listeners attached to all events.
         * @param fn Handler to remove. If null, removes all handlers.
         * @param capture Whether the listener is capturing. If null, removes capturing and non-capturing listeners.
         * @return The number of listeners removed.
         */
        removeEventListener(target?: EventTarget, type?: string, fn?: any, capture?: boolean): number {
            let cnt = 0;
            if (this._listeners) {
                for (let i = 0; i < this._listeners.length; i++) {
                    let l = this._listeners[i];
                    if (target == null || target == l.target) {
                        if (type == null || type == l.type) {
                            if (fn == null || fn == l.fn || // regular functions
                                (fn && l.fn && fn.toString() == l.fn.toString())) { // closures (TFS 30614)
                                if (capture == null || capture == l.capture) {
                                    l.target.removeEventListener(l.type, l.fn, l.capture);
                                    this._listeners.splice(i, 1);
                                    i--;
                                    cnt++;
                                }
                            }
                        }
                    }
                }
            }
            return cnt;
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose If true, all the related resources are disposed.
         */
        dispose(fullDispose = true) {
            if (fullDispose) {
                this.removeEventListener();
            }
        }
    }

    /**
     * Provides arguments for the @see:tileActivated event.
     */
    export class TileEventArgs extends wijmo.EventArgs {
        /**
         * Gets or sets a value that indicates the tile where this event is fired.
         */
        tile: Tile;
    }

    /**
     * Provides arguments for the @see:formatTile event.
     */
    export class TileFormattedEventArgs extends TileEventArgs {
        /**
         * Gets the element for the tile header.
         *
         * It could be used to customize the header.
         */
        headerElement: HTMLElement;

        /**
         * Gets the element for the tile content.
         *
         * It can be used to customize the content.
         */
        contentElement: HTMLElement;

        /**
         * Gets a @see:Toolbar object for the toolbar of the tile.
         *
         * You can customize the toolbar items via this object.
         * For detail, please refer to @see:Toolbar class documentation.
         */
        toolbar: Toolbar;
    }

    /**
     * Provides arguments for the @see:tileSizeChanged events.
     */
    export class TileSizeChangedEventArgs extends TileEventArgs {
        /**
         * Gets or sets a boolean value that indicates whether the tile will be maximized or restored.
         */
        isMaximum: boolean;
    }
}