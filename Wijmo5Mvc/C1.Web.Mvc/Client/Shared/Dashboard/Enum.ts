﻿module c1.nav {
    'use strict';

    /**
     * Specifies the layout orientation.
     */
    export enum LayoutOrientation {
        /** Horizontal direction. */
        Horizontal = 0,
        /** Vertical direction. */
        Vertical = 1
    }
}