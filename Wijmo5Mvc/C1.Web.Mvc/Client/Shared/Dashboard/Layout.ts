﻿module c1.nav {
    'use strict';

    /**
     * Defines the interface for the layout.
     *
     * We can define the layout by implementing this interface.
     */
    export interface ILayout {
        /**
         * Gets the active tile.
         */
        activeTile: Tile;

        /**
         * Gets or sets the current layout settings as a JSON string.
         *
         * This property is typically used to persist the layout settings.
         */
        definition: any;

        /**
         * Gets the layout item collection.
         */
        items: wijmo.collections.ObservableArray;

        /**
         * Applies this layout to the specified @see:DashboardLayout control.
         *
         * In this method, the layout is applied to some dashboard.
         * Adds the codes to implement the rendering parts for the layout.
         *
         * @param dashboard A @see:DashboardLayout control which the layout is applied to.
         */
        attachTo(dashboard: DashboardLayout);

        /**
         * Detaches this layout from the @see:DashboardLayout control.
         *
         * In this method, the resources related will be released.
         * It is often used when applying the new layout.
         * It is called to detach the old layout.
         *
         */
        detach();

        /**
         * Refreshes the layout.
         *
         * When the layout properties are changed, especially the items are updated,
         * this method should be called to redraw the layout.
         *
         * @param fullUpdate Whether to update the layout as well as the content.
         */
        refresh(fullUpdate);

        /**
         * Starts to move the tile.
         *
         * Adds the codes in this method to prepare the moving.
         * For example, saves the start moving position, the moving tile.
         *
         * @param movedTile The tile to be moved.
         * @param startPosition The started position where the tile will be moved from. It is the coordinate within the browser visible area.
         * @return A boolean value indicates whether the tile could be moved.
         */
        startMove(movedTile: Tile, startPosition: wijmo.Point): boolean;

        /**
         * Moves the tile.
         *
         * Adds the codes in this method to do somethings during moving.
         * For example, creates a special moving icon represeting the moving tile, shows some tips for moving.
         *
         * @param movedTile The tile to be moved.
         * @param position The position of the moving mouse. It is the coordinate within the browser visible area.
         */
        move(movedTile: Tile, position: wijmo.Point);

        /**
         * Ends the moving operation and exits the moving mode.
         *
         * @param movedTile The tile to be moved.
         * @param endPosition The position where the tile is moved to.
         */
        endMove(movedTile: Tile, endPosition: wijmo.Point);

        /**
         * Resizes the tile.
         *
         * Adds the codes to implement resizing in this method.
         *
         * @param resizedTile The tile to be resized.
         * @param toPosition The position where the tile is resized to. It is the coordinate within the browser visible area.
         * @return A boolean value indicates whether the resize operation succeeds.
         */
        resize(resizedTile: Tile, toPosition: wijmo.Point): boolean;

        /**
         * Toggles the size state of the tile.
         *
         * Adds the codes to implement maximizing and restoring in this method.
         *
         * @param currentTile The tile to be toggled.
         */
        toggleSize(currentTile: Tile);

        /**
         * Removes the tile.
         *
         * @param removedTile The tile to be removed.
         * @return A boolean value indicates whether the tile is removed successfully.
         */
        remove(removedTile: Tile): boolean;

        /**
         * Shows all the tiles.
         */
        showAll();
    }

    /**
     * Defines the base class for the layout.
     *
     * We can define our own layouts by implementing @see:ILayout.
     * We need write the codes for initializing, saving/loading layout, managing the tiles, painting and moving.
     * It is a little complex.
     * If extending from this class, it will be an easy thing as it helps us to consider these.
     * We only need to override some methods to implement our own customizing parts.
     *
     */
    export class LayoutBase extends DisposableObject implements ILayout {
        static _EXCEPTION_ABSTRACT = 'It belongs to an abstract object, please implement it.';
        static _TYPE_NAME = 'fullTypeName';
        static _SETTING_NAMES: string[] = null;

        private _left: number;
        private _top: number;
        private _lastX: number;
        private _lastY: number;
        private _activeTile: Tile;
        private _movingTileElement: HTMLElement;

        // the members for the properties
        private _items;
        private _dashboard: DashboardLayout;

        /**
         * Initializes a new instance of the @see:LayoutBase class.
         *
         * @param options JavaScript object containing initialization data for the layout.
         */
        constructor(options?: any) {
            super();
            this.initialize(options);
        }

        /**
         * Gets the layout item collection.
         */
        get items(): LayoutItemCollection {
            if (!this._items) {
                this._items = this._getDefaultItems();
                this._items.collectionChanged.addHandler(() => this.invalidate(), this)
            }

            return this._items;
        }

        // gets the default value of items.
        // For different layouts, we need override this method 
        // to get the default value with different types.
        _getDefaultItems(): LayoutItemCollection {
            return new LayoutItemCollection(this);
        }

        /**
         * Gets or sets the current layout settings as a JSON string.
         *
         * This property is typically used to persist the layout settings.
         * The maximum state of the tile is temporary and it will not be persisted.
         */
        get definition(): any {
            let definition = {};
            definition[LayoutBase._TYPE_NAME] = this[LayoutBase._TYPE_NAME];
            let layout = {};
            this._getNames().forEach(name => {
                layout[name] = this[name];
            });

            if (this.items && this.items.length) {
                let proxyGroups = [];
                this.items.forEach(item => {
                    let layoutitem = item as LayoutItem;
                    proxyGroups.push(layoutitem._getDefinition());
                });
                layout['items'] = proxyGroups;
            }
            definition['layout'] = layout;
            return definition;
        }
        set definition(value: any) {
            if (!value ||
                (value[LayoutBase._TYPE_NAME] != null
                && value[LayoutBase._TYPE_NAME] != this[LayoutBase._TYPE_NAME])) {
                throw 'invalid layout definition';
            }

            this.initialize(value['layout']);
        }


        /**
         * Gets the full type name of the current object.
         *
         * It should be overrided for every class which extends from this class..
         */
        get fullTypeName(): string {
            throw LayoutBase._EXCEPTION_ABSTRACT;
        }

        /**
         * Gets the isCreateGroup of the current object.
         */
        get isCreateGroup(): boolean {
            return true;
        }

        /**
         * Gets the @see:DashboardLayout control which the layout is applied in.
         */
        get dashboard(): DashboardLayout {
            return this._dashboard;
        }

        /**
         * Gets the current active tile.
         */
        get activeTile(): Tile {
            return this._activeTile;
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (LayoutBase._SETTING_NAMES) {
                return LayoutBase._SETTING_NAMES;
            }
            
            LayoutBase._SETTING_NAMES = [].concat([]);
            return LayoutBase._SETTING_NAMES;
        }

        get _allowDrag(): boolean {
            return this.dashboard ? this.dashboard.allowDrag : true;
        }

        /**
         * Applies this layout to the specified @see:DashboardLayout control.
         * @param dashboard A @see:DashboardLayout control which the layout is applied to.
         */
        attachTo(dashboard: DashboardLayout) {
            if (this._dashboard === dashboard) {
                return;
            }

            if (this._dashboard != null) {
                this.detach();
            }

            this._dashboard = dashboard;
            this._render();
        }

        /**
         * Detaches this layout from the @see:DashboardLayout control.
         *
         * It is often used when applying the new layout, the old layout should be detached.
         * When the layout is detached, the dashboard control shows nothing but a blank.
         * If you want to use the layout in the dashboard again, you need call the @see:attachTo method to apply it.
         *
         */
        detach() {
            if (!this.dashboard) {
                return;
            }

            if (this._activeTile) {
                this._activeTile.deactivate();
                this._activeTile = null;
            }

            this.dispose(true);
            this._dashboard = null;
        }

        // renders the layout.
        private _render() {
            if (!this.dashboard) {
                return;
            }

            this._initializeComponent();
            this._paint();
        }

        // Initializes the dom elements, styles and related resources for the layout.
        _initializeComponent() {
        }

        // paints the layoutitems in the layout.
        private _paint() {
            if (!this.dashboard) {
                return;
            }

            //paint all the layoutitems
            this.draw();
        }

        /**
         * Draws the layout.
         * The items should be drawn in this methods.
         */
        draw() {
            if (this.items) {
                this.items.forEach(item => {
                    let layoutItem = item as LayoutItem;
                    layoutItem.render(this._container);
                });
            }
        }

        _redraw() {
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (fullDispose) {
                // clear the moving resources.
                if (this._movingTileElement) {
                    _removeElement(this._movingTileElement);
                    this._movingTileElement = null;
                }
                this._left = null;
                this._top = null;
                this._lastX = null;
                this._lastY = null;

                // empty the container content
                this._container.innerHTML = '';
            }

            super.dispose(fullDispose);
        }


        /**
         * Refreshes the layout.
         *
         * @param fullUpdate A boolean value decides whether to update the content completely.
         * If true, the layout refreshes without keeping the current status(moving, resizing and etc.).
         * Otherwise, the status is kept after refreshing.
         */
        refresh(fullUpdate = true) {
            if (!this.dashboard) {
                return;
            }

            this.dispose(fullUpdate);
            this._paint();
        }

        /**
         * Invalidates the dashboard causing an asynchronous refresh.
         *
         * @param fullUpdate Whether to update the dashboard layout as well as the content.
         */
        invalidate(fullUpdate = true) {
            if (this.dashboard) {
                this.dashboard.invalidate(fullUpdate);
            }
        }

        /**
         * Starts to move the tile.
         *
         * @param movedTile The tile to be moved.
         * @param startPosition The started position where the tile will be moved from. It is the coordinate within the browser visible area.
         * @return A boolean value indicates whether the tile could be moved.
         */
        startMove(movedTile: Tile, startPosition: wijmo.Point): boolean {
            if (this._canMove()) {
                // save the original position before moving so that it can roll back.
                let lastPosition = this._getRelativePostion(startPosition, true);
                this._lastX = lastPosition.x;
                this._lastY = lastPosition.y;
                this._movingTileElement = this._getMovingElement(movedTile);
                this._container.appendChild(this._movingTileElement);
                this._left = parseFloat(this._movingTileElement.style.left);
                this._top = parseFloat(this._movingTileElement.style.top);
                movedTile.beginMove();
                _addClass(this.dashboard.hostElement, 'moving');
                return true;
            }
            return false;
        }
        _canMove(): boolean {
            return this._hasMultipleVisbleTiles();
        }
        private _hasMultipleVisbleTiles(): boolean {
            let count = 0,
                getVisibleTilesCount = (li: LayoutItem) => {
                    if (li instanceof Tile) {
                        return li.visible ? 1 : 0;
                    }

                    let result = 0;
                    if (li instanceof Group) {
                        li.children.forEach(child => {
                            result += getVisibleTilesCount(child);
                        })
                    }

                    return result;
                };

            if (this.items) {
                this.items.forEach(item => {
                    count += getVisibleTilesCount(item);
                });
            }

            return (count > 1);
        }

        // get the relative position of the tile to the dashboard host element.
        _getTileRelativePosition(tile: Tile, ignoreRTLDirection: boolean = false): wijmo.Point {
            let tileClientRect = tile.hostElement.getBoundingClientRect();
            return this._getRelativePostion(
                new wijmo.Point(
                    (this._dashboard.rightToLeft && !ignoreRTLDirection) ? tileClientRect.right : tileClientRect.left,
                    tileClientRect.top
                ),ignoreRTLDirection);
        }
        _getRelativePostion(position: wijmo.Point, ignoreRTLDirection: boolean = false): wijmo.Point {
            let dashboardClientRect = this._container.getBoundingClientRect(),
                x = (this._dashboard.rightToLeft && !ignoreRTLDirection)
                    ? (dashboardClientRect.right - position.x)
                    : (position.x - dashboardClientRect.left),
                y = position.y - dashboardClientRect.top;
            return new wijmo.Point(x + this._container.scrollLeft, y + this._container.scrollTop);
        }

        private _getMovingElement(tile: Tile): HTMLElement {
            let movingTileElement = tile._getMovingElement(),
                tileClientRect = tile.hostElement.getBoundingClientRect(),
                position = this._getRelativePostion(new wijmo.Point(tileClientRect.left, tileClientRect.top),true);
            movingTileElement.style.left = position.x + 'px';
            movingTileElement.style.top = position.y + 'px';
            return movingTileElement;
        }

        /**
         * Moves the tile.
         *
         * @param movedTile The tile to be moved.
         * @param position The position of the moving mouse. It is the coordinate within the browser visible area.
         */
        move(movedTile: Tile, position: wijmo.Point) {
            let newMousePosition = this._getRelativePostion(position, true);
            this._left = this._left + newMousePosition.x - this._lastX;
            this._top = this._top + newMousePosition.y - this._lastY;
            this._lastX = newMousePosition.x;
            this._lastY = newMousePosition.y;
            this._movingTileElement.style.left = this._left + 'px';
            this._movingTileElement.style.top = this._top + 'px';
            this.moveTo(movedTile, new wijmo.Point(this._left, this._top));
        }

        /**
         * Moves the tile to the specified postion.
         *
         * Adds the codes in this method to do the moving operation.
         *
         * @param tile The tile to be moved.
         * @param pt The position where the tile is moved to  in relation to the dashboard.
         */
        moveTo(tile: Tile, pt: wijmo.Point) {
        }

        /**
         * Ends moving the tile at the specified postion.
         *
         * @param movedTile The tile to be moved.
         * @param endPosition The position where the tile is moved to. It is the coordinate within the browser visible area.
         */
        endMove(movedTile: Tile, endPosition: wijmo.Point) {
            // if endPositon is null, it means the moving operation will be cancelled.
            if (!endPosition) {
                this._cancelMove(movedTile);
            }

            // remove the moving element.
            if (this._movingTileElement) {
                this._container.removeChild(this._movingTileElement);
                this._movingTileElement = null;
            }
            movedTile.endMove();
            _removeClass(this.dashboard.hostElement, 'moving');
        }


        // cancels the moving.
        _cancelMove(movedTile: Tile) {
        }

        /**
         * Resize the tile to the specified postion.
         *
         * @param resizedTile The tile to be resized.
         * @param toPosition The position where the tile is resized to. It is the coordinate within the browser visible area.
         * @return A boolean value indicates whether the resize operation succeeds.
         */
        resize(resizedTile: Tile, toPosition?: wijmo.Point): boolean {
            if (!resizedTile) {
                return;
            }

            let endPosition = this._getRelativePostion(toPosition),
                startPosition = this._getTileRelativePosition(resizedTile),
                width = endPosition.x - startPosition.x,
                height = endPosition.y - startPosition.y;

            if (width <= 0 || height <= 0) {
                return false;
            }
            if (width > resizedTile.maxResizeWidth) {
                width = resizedTile.maxResizeWidth;
            }
            if (height > resizedTile.maxResizeHeight) {
                height = resizedTile.maxResizeHeight;
            }

            let succeeded = this.resizeTo(resizedTile, new wijmo.Size(width, height));
            if (succeeded) {
                wijmo.Control.refreshAll(resizedTile.hostElement);
            } 

            return succeeded;
        }

        /**
         * Resizes the tile to the specified size.
         *
         * Adds the codes to resize the tile to the specified size by pxWidth and pxHeight.
         *
         * @param resizedTile The tile to be resized.
         * @param newSize The new size.
         * @return A boolean value indicates whether the resize operation succeeds. 
         */
        resizeTo(resizedTile: Tile, newSize: wijmo.Size): boolean {
            return false;
        }

        /**
         * Toggles the tile size.
         *
         * @param currentTile The tile to be toggled.
         */
        toggleSize(currentTile: Tile) {
            if (!currentTile) {
                return;
            }

            //this._activeTile = currentTile;
            currentTile._toggleMaximum();
        }

        /**
         * Initializes the layout by copying the properties from a given object.
         *
         * This method allows you to initialize the object using plain data objects
         * instead of setting the value of each property in code.
         *
         * @param options Object that contains the initialization data.
         */
        initialize(options: any) {
            if (options) {
                wijmo.copy(this, options);
            }
        }

        _copy(key: string, value: object) {
            if (key == 'items') {
                let items = value as Array<any>;
                if (items instanceof Array && items.length) {
                    items.forEach(liOpt => this.items.push(this._createLayoutItem(liOpt)));
                }
                return true;
            }
            return false;
        }

        // create the layout item object from the options.
        private _createLayoutItem(opts: any): LayoutItem {
            if (!opts) {
                return null;
			}

            // TFS 382827 & 364649 & 400667 & 403734
            if (opts.children || opts.hasOwnProperty("orientation")
                || (this.isCreateGroup && Object.keys(opts).length == 0)) {
                // creat group
                return this._createGroup(opts);
            } else {
                // create tile.
                return this._createTile(opts);
            }
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            throw LayoutBase._EXCEPTION_ABSTRACT;
        }

        // creates a group object from the options.
        _createGroup(groupOpts): Group {
            throw LayoutBase._EXCEPTION_ABSTRACT;
        }

        /**
         * Removes the tile from the layout.
         *
         * @param removedTile The tile to be removed.
         * @return A boolean value indicates whether the tile is removed successfully.
         */
        remove(removedTile: Tile): boolean {
            if (removedTile.remove()) {
                if (this._activeTile === removedTile) {
                    this._activeTile.deactivate();
                    this._activeTile = null;
                }

                return true;
            }

            return false;
        }

        // Creates a layout object from the specified options.
        static _createLayout(opts: string): LayoutBase {
            if (opts) {
                let objLayout = JSON.parse(opts);
                if (objLayout[LayoutBase._TYPE_NAME]) {
                    let layoutInstance = _createInstance(objLayout[LayoutBase._TYPE_NAME]);
                    layoutInstance.definition = objLayout;
                    return layoutInstance as LayoutBase;
                }
            }

            return null;
        }

        // get the container element for the layout.
        get _container(): HTMLElement {
            return this.dashboard ? this.dashboard.container : null;
        }

        // called when the tile is rendered.
        _onTileFormatted(e: TileFormattedEventArgs) {
            if (this._dashboard) {
                this._dashboard.onFormatTile(e);
            }
        }

        // called when the tile is to be activated.
        _onTileActivated(e: TileEventArgs) {
            if (this._activeTile === e.tile) {
                return;
            }

            if (this._activeTile) {
                this._activeTile.deactivate();
            }

            this._activeTile = e.tile;
            if (this._dashboard) {
                this._dashboard.onTileActivated(e);
            }
        }

        // called when the tile is toggled.
        _onTileSizeChanged(e: TileSizeChangedEventArgs) {
            if (this._dashboard) {
                this._dashboard.onTileSizeChanged(e);
            }
        }

        // called when the tile's visibility is toggled.
        _onTileVisibilityChanged(e: TileEventArgs) {
            this.refresh(false);
            if (e.tile.visible && this.activeTile === e.tile && this._dashboard) {
                this.dashboard._updateForCurrentTile();
            }
            this._onLayoutChanged();
        }

        // called when the dashboard's layout is changed.
        _onLayoutChanged() {
            this._dashboard.onLayoutChanged();
        }

        /**
         * Shows all the tiles.
         */
        showAll() {
            this.items.forEach(item => this._showLayoutItem(item));
        }
        private _showLayoutItem(item: any) {
            if (item instanceof Tile) {
                item.visible = true;
            } else if (item instanceof Group) {
                item.children.forEach(gci => this._showLayoutItem(gci));
            }
        }
    }
}