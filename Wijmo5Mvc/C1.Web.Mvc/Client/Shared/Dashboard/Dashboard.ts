﻿/**
 * Defines the @see:DashboardLayout control, different layouts and associated classes.
 */
module c1.nav {
    'use strict';

    wijmo._addCultureInfo('DashboardLayout', {
        showAll: 'Show All',
        show: 'Show',
        restore: 'Restore',
        fullScreen: 'FullScreen',
        hide: 'Hide'
    });

    /**
     * It is a consolidated display of many views and related information in a single place.
     * It is used to compare and monitor a variety of data simultaneously.
     * The different data views are displayed all at once.
     * Now DashboardLayout supports three layouts: FlowLayout, GridLayout(Auto and Manual) and SplitLayout.
     * The user can define many layouts by implementing the @see:ILayout interface,
     * or extending from the @see:LayoutBase for simple.
     */
    export class DashboardLayout extends wijmo.Control {
        //the min distance to start dragging control.
        private static _MIN_DRAG_DISTANCE = 5;
        private static _MENU_SIZE = 24;

        /* the elements for the menus.*/
        // the resize
        private _menuResize: HTMLElement;
        private _menuMove: HTMLElement;

        private _container: HTMLElement;
        private _maxTileContainer: HTMLElement;
        private _eleCalculateSize: HTMLElement;

        private _downPoint: wijmo.Point;
        private _moving = false;
        private _resizing = false;

        // the members for properties.
        private _layout: ILayout;
        private _allowDrag: boolean = true;
        private _allowResize: boolean = true;
        private _allowMaximize: boolean = true;
        private _allowHide: boolean = true;
        private _allowShowAll: boolean = true;

        /**
         * Gets or sets the template used to instantiate @see:DashboardLayout control.
         */
        static controlTemplate =
            '<div style="position:relative;overflow:hidden;width:100%;height:100%;display:block">' +
            '<div wj-part="ctr" class="wj-dl-container"></div>' +
            '<div wj-part="mtl" class="wj-dl-hidden wj-dl-max-layer"></div>' +
            '<div wj-part="cse" style="position:absolute;display:block;visibility:hidden"></div>' +
            '</div>';

        /**
         * Initializes a new instance of the @see:DashboardLayout class.
         *
         * @param element The DOM element that will host the control, or a selector for the host element (e.g. '#theCtrl').
         * @param options JavaScript object containing initialization data for the control.
         */
        constructor(element: any, options?: any) {
            super(element, options, true);
            let tpl = this.getTemplate();
            this.applyTemplate('wj-dashboard wj-dl-draggable wj-dl-resizable', tpl, {
                _container: 'ctr',
                _maxTileContainer: 'mtl',
                _eleCalculateSize: 'cse'
            });

            this._init();
            if (options) {
                this.deferUpdate(() => {
                    this.initialize(options);
                });
            }
        }

        /**
         * Gets or sets the layout applied in this @see:DashboardLayout control.
         */
        get layout(): ILayout {
            return this._layout;
        }
        set layout(value: ILayout) {
            if (value === this._layout) {
                return;
            }

            if (this._layout) {
                this._layout.detach();
            }

            this._reset();

            this._maxTileContainer.innerHTML = '';
            wijmo.addClass(this._maxTileContainer, 'wj-dl-hidden');

            this._layout = value;
            if (this._layout) {
                this._layout.attachTo(this);
            }
        }

        /**
         * Gets or sets a boolean value decides whether the tiles could be dragged.
         */
        get allowDrag(): boolean {
            return this._allowDrag;
        }
        set allowDrag(value: boolean) {
            value = !!value;
            if (this.allowDrag != value) {
                this._allowDrag = value;
                wijmo.toggleClass(this.hostElement, 'wj-dl-draggable', this.allowDrag);
            }
        }

        /**
         * Gets or sets a boolean value decides whether the tiles could be resized.
         */
        get allowResize(): boolean {
            return this._allowResize;
        }
        set allowResize(value: boolean) {
            value = !!value;
            if (this.allowResize != value) {
                this._allowResize = value;
                wijmo.toggleClass(this.hostElement, 'wj-dl-resizable', this.allowResize);
            }
        }

        /**
         * Gets or sets a boolean value decides whether the tiles could be maximized.
         */
        get allowMaximize(): boolean {
            return this._allowMaximize;
        }
        set allowMaximize(value: boolean) {
            value = !!value;
            if (this.allowMaximize != value) {
                this._allowMaximize = value;
                this._updateToolbars();
            }
        }

        private _updateToolbars() {
            let updateLIToolbar = (li: LayoutItem) => {
                if (li instanceof Tile) {
                    li._refreshToolbar();
                } else if (li instanceof Group) {
                    li.children.forEach(child => updateLIToolbar(<LayoutItem>child));
                }
            };
            if (this.layout) {
                this.layout.items.forEach(item => updateLIToolbar(<LayoutItem>item));
            }
        }

        /**
         * Gets or sets a boolean value decides whether the tiles could be hidden.
         */
        get allowHide(): boolean {
            return this._allowHide;
        }
        set allowHide(value: boolean) {
            value = !!value;
            if (this.allowHide != value) {
                this._allowHide = value;
                this._updateToolbars();
            }
        }

        /**
         * Gets or sets a boolean value decides whether the @see:DashboardLayout could show all the tiles.
         */
        get allowShowAll(): boolean {
            return this._allowShowAll;
        }
        set allowShowAll(value: boolean) {
            value = !!value;
            if (this.allowShowAll != value) {
                this._allowShowAll = value;
                this._updateToolbars();
            }
        }

        /**
         * Gets the current tile in the dashboard.
         */
        get currentTile(): Tile {
            return this._layout != null ? this._layout.activeTile : null;
        }

        /**
         * Gets the element of the dashboard container.
         */
        get container(): HTMLElement {
            return this._container;
        }

        /**
         * Occurs when an element representing a @see:Tile has been created.
         *
         * For a tile, there are three areas: Header, Toolbar and Content.
         * These areas can be customized via this event.
         * The related dom elements or objects can be obtained in the event argument(@see:TileFormattedEventArgs):
         * headerElement, toolbar and contentElement.
         *
         * For example, this code sets the backgroundColor of the tile to 'red'.
         *
         * <pre>dashboard.formatTile.addHandler(function (s, e) {
         *   // add an image after the header title.
         *   var eleInfor = document.createElement('img');
         *   eleInfor.src = '~/Content/images/down.png';
         *   e.headerElement.appendChild(eleInfor);
         *
         *   // append some text to the tile content.
         *   var spanText = document.createElement('span');
         *   spanText.innerText = 'This is the cost budgeting for the whole 2018 year!';
         *   e.contentElement.appendChild(spanText);
         *
         *   // customize the toolbar.
         *   e.toolbar.clear(); //clear the toolbar items
         *   // add a item to export the chart.
         *   toolbar.insertToolbarItem({
         *      icon: '<img src="~/Content/images/export.png" />',
         *      title: 'Export',
         *      command: function () {
         *          var selector = e.tile.content,
         *              chart = wijmo.Control.getControl(selector);
         *         chart.saveImageToFile(selector.substr(1) + '.png');
         *      }
         *  }, 0);

         *  // add a 'Delete' item in toolbar for removing the tile from the dashboard via dom.
         *  var iconClose = document.createElement('img');
         *  iconClose.style.marginLeft = '6px';
         *  iconClose.style.cursor = 'default';
         *  iconClose.src = '~/Content/images/close.png';
         *  e.toolbar.hostElement.appendChild(iconClose);
         *  iconClose.addEventListener('click', function () {
         *      // remove the tile from the dashboard.
         *      e.tile.remove();
         *  });
         * });</pre>
         */
        readonly formatTile = new wijmo.Event();
        /**
         * Raises the @see:formatTile event.
         *
         * @param e @see:TileFormattedEventArgs that contains the event data.
         */
        onFormatTile(e: TileFormattedEventArgs) {
            this.formatTile.raise(this, e);
            this._eleCalculateSize.appendChild(e.tile.hostElement);
        }

        /**
         * Occurs when the tile is activated.
         */
        readonly tileActivated = new wijmo.Event();
        /**
         * Raises the @see:tileActivated event.
         *
         * @param e @see:TileEventArgs that contains the event data.
         */
        onTileActivated(e: TileEventArgs) {
            this._updateForCurrentTile();
            this.tileActivated.raise(this, e);
        }

        /**
         * Occurs when the tile is maximized or restored.
         */
        readonly tileSizeChanged = new wijmo.Event();
        /**
         * Raises the @see:tileSizeChanged event.
         *
         * @param e @see:TileSizeChangedEventArgs that contains the event data.
         */
        onTileSizeChanged(e: TileSizeChangedEventArgs) {
            let tile = e.tile;
            if (e.isMaximum) {
                let maxTileElement = this._getMaximumTileElement(tile);
                wijmo.addClass(maxTileElement, 'maximum');
                _removeClass(maxTileElement, 'wj-tile-resizable');
                _removeClass(maxTileElement, 'wj-tile-draggable');
                _removeClass(this._maxTileContainer, 'wj-dl-hidden');
                this._maxTileContainer.appendChild(maxTileElement);
                wijmo.Control.refreshAll(maxTileElement);
                this._updateMenusLocation(new wijmo.Point(0, 0), tile, this._maxTileContainer.firstElementChild);
            } else {
                tile.hostElement.appendChild(tile._contentElement);
                wijmo.Control.refreshAll(tile._contentElement);
                wijmo.addClass(this._maxTileContainer, 'wj-dl-hidden');
                this._updateForCurrentTile();
                this._maxTileContainer.innerHTML = '';
            }
            this.tileSizeChanged.raise(this, e);
            this.onLayoutChanged();
        }

        /**
         * Occurs when the layout is changed.
         */
        readonly layoutChanged = new wijmo.Event();
        /**
         * Raises the @see:layoutChanged event.
         */
        onLayoutChanged() {
            this.layoutChanged.raise(this);
        }

        /**
         * Saves the current layout into a JSON string.
         *
         * The string represents an object with the layout type and the layout properties.
         * With the @see:loadLayout method, it can be used to persist the layouts defined
         * by the users so they are preserved across sessions,
         * and can also be used to implement undo/redo functionality in applications
         * that allows users to modify the layout.
         *
         * The tile maximum state is temporary. It will not be saved.
         *
         * For example, the text represents a @see:FlowLayout with two tiles and its direction is TopToDown.
         *
         * <pre>{
         *      "fullTypeName": "c1.nav.flow.FlowLayout",
         *      "layout": {
         *          "direction": 1,
         *          "items":[
         *              {
         *                  "headerText": "This is the header text of the first tile.",
         *                  "content": "This is the content of the first tile."
         *              },
         *              {
         *                  "headerText": "This is the header text of the second tile.",
         *                  "content": "This is the content of the second tile."
         *              }
         *          ]
         *      }
         * }</pre>
         *
         * @return A JSON text represents the layout.
         */
        saveLayout(): string {
            if (this._layout) {
                return JSON.stringify(this._layout.definition);
            }

            return '';
        }

        /**
         * Loads the layout from a JSON string.
         *
         * @param value A json string for a layout.
         */
        loadLayout(value: string) {
            this.layout = LayoutBase._createLayout(value);
        }

        /**
         * Refreshes the control.
         *
         * @param fullUpdate Whether to update the control layout as well as the content.
         */
        refresh(fullUpdate = true) {
            if (fullUpdate) {
                if (this._menuMove) {
                    this._eleCalculateSize.appendChild(this._menuMove);
                }

                if (this._menuResize) {
                    this._eleCalculateSize.appendChild(this._menuResize);
                }
            }

            super.refresh(fullUpdate);
            if (fullUpdate) {
                this._maxTileContainer.innerHTML = '';
                if (!wijmo.hasClass(this._maxTileContainer, 'wj-dl-hidden')) {
                    wijmo.addClass(this._maxTileContainer, 'wj-dl-hidden');
                }
            }

            if (this._layout) {
                this._layout.refresh(fullUpdate);
            }

            if (fullUpdate) {
                this._updateForCurrentTile();
                this.onLayoutChanged();
            }
        }

        ///**
        // * Raises the @see:lostFocus event.
        // */
        //onLostFocus(e?: wijmo.EventArgs) {
        //    super.onLostFocus(e);
        //    this._reset();
        //}

        ///**
        // * Raises the @see:gotFocus event.
        // */
        //onGotFocus(e?: wijmo.EventArgs) {
        //    super.onGotFocus(e);
        //    this._updateForCurrentTile();
        //}
        private _init() {
            this._initOperationsEvents();
            this._initResizeMenu();
            this._initMoveMenu();
            this._initContextMenu();

        }

        // Add the events for move & resize.
        private _initOperationsEvents() {
            this.addEventListener(document, 'keydown', (e: KeyboardEvent) => {
                // TFS: 341906, 400652
                if (this._moving && e.keyCode != wijmo.Key.Escape) {
                    e.preventDefault();
                }
                if (e.keyCode != wijmo.Key.Escape) {
                    return;
                }
                if (this._moving || this._downPoint) {
                    this._cancelMove();
                }
            });

            this.addEventListener(document, 'mousemove', (e: MouseEvent) => {
                let currentTile = this.currentTile;
                if (!currentTile || !this._layout) {
                    this._moving = false;
                    this._downPoint = null;
                    this._resizing = false;
                    return;
                }

                let position = new wijmo.Point(e.clientX, e.clientY);
                if (this._moving) {
                    // moving the tile
                    this._moveTo(position);
                } else if (this._resizing) {
                    // resizing the tile
                    this._resizeTo(position);
                } else if (this._downPoint) {
                    // begin to move the tile.
                    this._startMoveFrom(position);
                }
            });
            this.addEventListener(document, 'mouseup', (e: MouseEvent) => {
                this._downPoint = null;
                this._resizing = false;
                let currentTile = this.currentTile;
                if (!currentTile || !this._layout) {
                    this._moving = false;
                    return;
                }

                if (this._moving) {
                    this._endMoveAt(new wijmo.Point(e.clientX, e.clientY));
                }
            });
        }

        // resize menu
        private _initResizeMenu() {
            if (!this._menuResize) {
                this._drawResizeMenu();
            }
        }
        private _drawResizeMenu() {
            // TFS: 342058
            let btnResize;
            if (this.rightToLeft) {
                btnResize = _createSvgBtn('ResizeLeft');
                btnResize.style.cursor = "sw-resize";
                btnResize.style.left = "0px";
            } else {
                btnResize = _createSvgBtn('Resize');
                btnResize.style.cursor = "se-resize";
                btnResize.style.right = "0px";
            }
            wijmo.addClass(btnResize, 'wj-menu-resize');

            this._menuResize = btnResize;
            this._addResizingTrigger(this._menuResize);
            this._eleCalculateSize.appendChild(this._menuResize);
        }
        _addResizingTrigger(trigger: HTMLElement) {
            this.addEventListener(trigger, 'mousedown', (e: MouseEvent) => {
                if (!this.allowResize) {
                    return;
                }

                this._resizing = true;
                e.stopPropagation();
            });
        }

        // move menu
        private _initMoveMenu() {
            if (!this._menuMove) {
                this._drawMoveMenu();
            }
        }
        private _drawMoveMenu() {
            let btnMove = _createSvgBtn('MoveIndicator');
            wijmo.addClass(btnMove, 'wj-menu-move');
            this._menuMove = btnMove;
            this._addMovingTrigger(this._menuMove);
            this._eleCalculateSize.appendChild(this._menuMove);
        }
        _addMovingTrigger(trigger: HTMLElement) {
            this.addEventListener(trigger, 'mousedown', (e: MouseEvent) => {
                if (!this.allowDrag || e.button != 0
                    || (this.currentTile && (!this.currentTile.allowDrag || this.currentTile.maximum))) {
                    return;
                }

                this._downPoint = new wijmo.Point(e.clientX, e.clientY);
                e.stopPropagation();
            });
        }

        // the context menu
        private _initContextMenu() {
            let div = document.createElement('div'),
                contextMenu = new wijmo.input.Menu(div, {
                    displayMemberPath: 'header',
                    openOnHover: true,
                    isAnimated: true,
                    subItemsPath: 'items',
                    command: {
                        executeCommand: this._executeMenuCommand.bind(this),
                        canExecuteCommand: this._canExecuteMenuCommand.bind(this)
                    }
                });
            this.addEventListener(this.hostElement, 'contextmenu', (e: MouseEvent) => {
                let items = this._getMenuItems();
                e.preventDefault();
                if (items && items.length) {
                    contextMenu.initialize({
                        itemsSource: items
                    });
                    contextMenu.show(e);
                }
            }, false);
            this.addEventListener(this._maxTileContainer, 'contextmenu', (e: MouseEvent) => {
                e.preventDefault();
                e.stopPropagation();
            }, false);
        }

        private _executeMenuCommand(param: any) {
            switch (param.cmd) {
                case 'Show':
                    if (param.tile) {
                        param.tile.visible = true;
                    }
                    break;
                case 'ShowAll':
                    this.showAll();
                    break;
                default:
                    break;
            }
        }

        private _canExecuteMenuCommand(param: any) {
            switch (param.cmd) {
                case 'ShowAll':
                    let hiddenTiles = this._getHiddenTiles();
                    return !!hiddenTiles.length;
                default:
                    return true;
            }
        }

        private _getMenuItems() {
            let items = [],
                hiddenTiles = this._getHiddenTiles();

            if (hiddenTiles && hiddenTiles.length) {
                let showItems = {
                    header: wijmo.culture.DashboardLayout.show,
                    items: []
                };
                hiddenTiles.forEach(ht => {
                    let tile = ht as Tile;
                    //if (tile.headerText) { //Bug 412889
                        showItems.items.push({
                            header: tile.headerText||"Undefined",
                            cmd: 'Show',
                            tile: tile
                        });
                    //}
                });
                items.push(showItems);

                if (this.allowShowAll) {
                    items.push({
                        header: wijmo.culture.DashboardLayout.showAll,
                        cmd: 'ShowAll'
                    });
                }
            }

            return items;
        }

        private _getHiddenTiles() {
            let tiles = [];
            if (this.layout && this.layout.items) {
                this.layout.items.forEach(item => {
                    if (item instanceof Tile) {
                        if (!item.visible) {
                            tiles.push(item);
                        }
                    } else {
                        tiles = tiles.concat(this._getHiddenChildren(item as Group));
                    }
                });
            }
            return tiles;
        }

        private _getHiddenChildren(group: Group) {
            let tiles = [];
            if (group && group.children) {
                group.children.forEach(child => {
                    if (child instanceof Tile) {
                        if (!child.visible) {
                            tiles.push(child);
                        }
                    } else {
                        tiles = tiles.concat(this._getHiddenChildren(child as Group));
                    }
                });
            }
            return tiles;
        }

        private _initMenuItems(contextMenu: wijmo.input.Menu) {
            contextMenu.itemsSource = [];
            var items = [];
            if (this.allowShowAll) {
                items.push({
                })
            }
        }

        // update the menus location according to the tile host element or the specified element.
        private _updateMenusLocation(tileLocation: wijmo.Point, tile: Tile, tileElement?: Element) {
            if (!tileElement) {
                tileElement = tile._contentElement;
            }
            // the resize menu
            tileElement.appendChild(this._menuResize);

            // the move menu
            if (!tile.showHeader) {
                tileElement.appendChild(this._menuMove);
                let tileRect = tileElement.getBoundingClientRect(),
                    menuMoveRect = this._menuMove.getBoundingClientRect(),
                    left = tileRect.width / 2 - menuMoveRect.width / 2;
                this._menuMove.style.left = left + 'px';
            } else if (this._menuMove) {
                // remove the move menu from its parent element(the tile element).
                _removeElement(this._menuMove);
            }
        }

        // toggles the maximum of the tile.
        private _toggleTileMaximum(tile: Tile) {
            this._reset();
            this._layout.toggleSize(this.currentTile);
        }

        /**
         * Removes the tile.
         * @param removedTile The tile to be removed.
         */
        remove(removedTile: Tile) {
            if (!this._layout) {
                return;
            }

            this._reset();
            if (this._layout.remove(removedTile)) {
                if (removedTile.maximum) {
                    this._maxTileContainer.innerHTML = '';
                    wijmo.addClass(this._maxTileContainer, 'wj-dl-hidden');
                }

                this.refresh();
            }
        }

        /**
         * Hides the tile.
         * @param tile The tile to be hidden.
         */
        hide(tile: Tile) {
            tile.visible = false;
        }

        /**
         * Shows all the tiles.
         */
        showAll() {
            if (this.layout) {
                this.layout.showAll();
            }
        }

        // reset the DashboardLayout control.
        private _reset() {
            this._resizing = false;
            this._moving = false;
            this._downPoint = null;
        }

        // moves the tile
        private _startMoveFrom(position: wijmo.Point) {
            if (Math.max(Math.abs(position.x - this._downPoint.x), Math.abs(position.y - this._downPoint.y))
                < DashboardLayout._MIN_DRAG_DISTANCE) {
                return;
            }

            this._moving = this._layout.startMove(this.currentTile, this._downPoint);
        }
        private _moveTo(position: wijmo.Point) {
            this._layout.move(this.currentTile, position);
        }
        private _endMoveAt(position: wijmo.Point) {
            this._moving = false;
            this._layout.endMove(this.currentTile, position);
            this._updateForCurrentTile();
            this.onLayoutChanged();
        }

        private _cancelMove() {
            this._downPoint = null;
            this._endMoveAt(null);
        }

        // resizes the tile
        // Todo
        private _resizeTo(position: wijmo.Point) {
            if (this._layout.resize(this.currentTile, position)) {
                this.refresh(false);
                this._updateForCurrentTile();
                this.onLayoutChanged();
            }
        }

        private _getMaximumTileElement(tile: Tile): HTMLElement {
            let maxTileElement = tile.hostElement.cloneNode() as HTMLElement;
            maxTileElement.style.width = '100%';
            maxTileElement.style.height = '100%';
            maxTileElement.style.left = '0px';
            maxTileElement.style.top = '0px';
            maxTileElement.appendChild(tile._contentElement);
            tile._refreshToolbar();
            return maxTileElement;
        }

        // update the menus location for current tile.
        _updateForCurrentTile() {
            let currentTile = this.currentTile;
            if (currentTile && this._isInDashboard(currentTile) && currentTile.visible) {
                this._updateMenusLocation(currentTile._getPosition(), currentTile);
            }
        }

        private _isInDashboard(layoutItem: LayoutItem): boolean {
            if (!layoutItem || !this._layout
                || !this._layout.items || !this._layout.items.length) {
                return false;
            }

            let parent: any = layoutItem.parent,
                layout: any = this._layout;

            while (parent) {
                if (layout === parent) {
                    return true;
                }
                parent = parent.parent;
            }

            return false;
        }

        protected _handleResize() {
            if (this._e.parentElement) {
                let sz = new wijmo.Size(this._e.offsetWidth, this._e.offsetHeight);
                if (!sz.equals(this._szCtl)) {
                    this._szCtl = sz;
                    this.refresh(false);
                }
            }
        }
    }
}