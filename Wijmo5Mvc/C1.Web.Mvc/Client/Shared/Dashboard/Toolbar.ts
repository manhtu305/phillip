﻿module c1.nav {
    'use strict';
    /**
     * Specifies the available svg button types in dashboard.
     */
    export enum SVGButtonType {
        /** The button which is used to hide the tile. */
        Hide,
        /** The toggle button for maximizing and restore the tile.*/
        FullScreen,
        /** Show all the tiles. */
        ShowAll
    }

    /**
     * Defines a class represents the toolbar object in the tile.
     */
    export class Toolbar extends wijmo.Control {
        private static _enabledCss = 'enabled';
        private static _commandTagAttr = 'command-tag';

        private _eleFirst: HTMLElement;
        private _eleContainer: HTMLElement;
        private _eleMore: HTMLElement;

        private _toolbarItems = new wijmo.collections.ObservableArray();
        private _menuMore: wijmo.input.Menu;
        private _menuFirst: wijmo.input.Menu;
        private readonly _hostTile: Tile;

        static controlTemplate =
        '<div wj-part="container" style="display:inline-block">' +
        '<div wj-part="first"></div>' +
        '<div wj-part="more"></div>' +
        '</div>';

        /**
         * Initializes a new instance of the @see:Toolbar class and attaches it to a DOM element.
         *
         * @param element The DOM element that hosts the control, or a CSS selector for the host element (e.g. '#theCtrl').
         * @param tile The tile which owns the toolbar.
         */
        constructor(element: any, tile: Tile) {
            super(element);
            this._hostTile = tile;
            let tpl = this.getTemplate();
            this.applyTemplate('wj-tile-toolbar', tpl, {
                _eleContainer: 'container',
                _eleFirst: 'first',
                _eleMore: 'more'
            });
            this._toolbarItems.collectionChanged.addHandler(() => {
                this.invalidate();
            });
            this.invalidate();
        }

        /**
         * Refreshes the control.
         *
         * @param fullUpdate Whether to update the control layout as well as the content.
         */
        refresh(fullUpdate?: boolean) {
            super.refresh()
            this._draw();
        }

        private _toggleTileMaximum() {
            if (this._hostTile) {
                this._hostTile._toggleMaximum();
            }
        }
        //private _removeTile() {
        //    if (this._hostTile
        //        && this._hostTile.layout
        //        && this._hostTile.layout.dashboard) {
        //        this._hostTile.layout.dashboard.remove(this._hostTile);
        //    }
        //}
        private _hideTile() {
            if (this._hostTile) {
                this._hostTile.visible = false;
            }
        }

        private _showAll() {
            if (this._hostTile && this._hostTile.layout) {
                this._hostTile.layout.showAll();
            }
        }

        /**
         * Clears all the toolbar items.
         */
        clear() {
            this._toolbarItems.clear();
        }

        /**
         * Remove the toolbar item in the specified index.
         * @param index The index of the toolbar item to be deleted.
         */
        removeToolbarItemAt(index: number) {
            this._toolbarItems.removeAt(index);
        }

        /**
         * Insert a toolbar item into the specified position.
         * @param item The toolbar item which is inserted.
         * @param index The position where the item is inserted.
         */
        insertToolbarItem(item: IToolbarItem, index?: number) {
            if (index == null) {
                index = this._toolbarItems.length;
            }

            this._toolbarItems.insert(index, item);
        }

        /**
         * Insert a SVG button.
         * @param btnType The defined button types in dashboard.
         * @param index The position where the item is inserted.
         */
        insertSVGButton(btnType: SVGButtonType, index?: number) {
            btnType = wijmo.asEnum(btnType, SVGButtonType);
            if (index == null) {
                index = this._toolbarItems.length;
            }
            let toolbarItem;
            switch (btnType) {
                case SVGButtonType.FullScreen:
                    let toggledItem: IToolbarItem = {
                        icon: _createSvgBtn('Collapse').outerHTML,
                        title: wijmo.culture.DashboardLayout.restore,
                        command: this._toggleTileMaximum.bind(this),
                        isVisible: (hostTile: Tile) => hostTile._isMaximizable()
                    };
                    toolbarItem = {
                        icon: _createSvgBtn('Expand').outerHTML,
                        title: wijmo.culture.DashboardLayout.fullScreen,
                        command: this._toggleTileMaximum.bind(this),
                        condition: (hostTile: Tile) => {
                            return !hostTile.maximum;
                        },
                        toggleItem: toggledItem,
                        isVisible: (hostTile: Tile) => hostTile._isMaximizable()
                    };
                    this.insertToolbarItem(<IToolbarItem>toolbarItem, index);
                    break;
                case SVGButtonType.Hide:
                    toolbarItem = {
                        icon: _createSvgBtn('Hide').outerHTML,
                        title: wijmo.culture.DashboardLayout.hide,
                        command: this._hideTile.bind(this),
                        isVisible: (hostTile: Tile) => hostTile._couldBeHidden() && !hostTile.maximum
                    };
                    this.insertToolbarItem(toolbarItem, index);
                    break;
                case SVGButtonType.ShowAll:
                    toolbarItem = {
                        icon: _createSvgBtn('ShowAll').outerHTML,
                        title: wijmo.culture.DashboardLayout.showAll,
                        command: this._showAll.bind(this),
                        isVisible: (hostTile: Tile) => hostTile._couldShowAll() && !hostTile.maximum
                    };
                    this.insertToolbarItem(toolbarItem, index);
                    break;
                default:
                    throw 'invalid btnType!';
            }
        }

        private _draw() {
            // TFS: 342058
            if (this.rightToLeft === true) {
                this.hostElement.style.left = "5px";
            }
            else {
                this.hostElement.style.right = "5px";
            }

            if (this._menuFirst) {
                this._menuFirst.dispose();
                this._menuFirst = null;
                this._eleFirst = this.hostElement.querySelector('[wj-part="first"]') as HTMLElement;
            }
            if (this._menuMore) {
                this._menuMore.dispose();
                this._menuMore = null;
                this._eleMore = this.hostElement.querySelector('[wj-part="more"]') as HTMLElement;
            }

            this._menuFirst = new wijmo.input.Menu(this._eleFirst, {
                isButton: true
            });
            this._menuMore = new wijmo.input.Menu(this._eleMore, {
                showDropDownButton: false,
                header: _createSvgBtn('More').outerHTML
            });

            if (!this._toolbarItems.length) {
                wijmo.addClass(this._eleFirst, 'wj-dl-hidden');
                wijmo.addClass(this._eleMore, 'wj-dl-hidden');
                return;
            }

            let items = [];
            this._toolbarItems.forEach(item => {
                let toolbarItem = <IToolbarItem>item;
                if (toolbarItem.isVisible == null || toolbarItem.isVisible(this._hostTile)) {
                    items.push(this._getToolbarItem(item));
                }
            });

            let eleTile = wijmo.closest(this.hostElement, '.wj-dl-tile') as HTMLElement,
                tileRect = eleTile.getBoundingClientRect(),
                moreItems: any[];

            if (tileRect.width <= 150 && items.length > 1) {
                wijmo.addClass(this._eleFirst, 'wj-dl-hidden');
                moreItems = items;
            } else {
                if (items.length > 0) {
                    let firstItem: IToolbarItem = items[0];
                    wijmo.removeClass(this._eleFirst, 'wj-dl-hidden');
                    this._menuFirst.initialize({
                        header: firstItem.icon,
                        itemClicked: function () {
                            firstItem.command();
                        }
                    });
                }

                if (items.length > 1) {
                    moreItems = items.slice(1);
                }
            }

            if (moreItems && moreItems.length) {
                wijmo.removeClass(this._eleMore, 'wj-dl-hidden');
                this._menuMore.initialize({
                    itemsSource: moreItems,
                    displayMemberPath: 'element',
                    commandPath: 'command'
                });
            } else {
                wijmo.addClass(this._eleMore, 'wj-dl-hidden');
            }
        }

        private _getToolbarItem(item: IToolbarItem): IToolbarItem {
            let toggleToolbarItem = <IToggleToolbarItem>item;
            if (!toggleToolbarItem.condition
                || toggleToolbarItem.condition(this._hostTile)) {
                return this._getToolbarItemWithElement(item);
            } else {
                return this._getToolbarItemWithElement(toggleToolbarItem.toggleItem);
            }
        }

        private _getToolbarItemWithElement(item: IToolbarItem): IToolbarItem {
            let returnedItem: IToolbarItem = {};
            returnedItem.command = item.command;
            returnedItem.icon = item.icon;
            returnedItem.title = item.title;
            if (typeof (item.element) === 'undefined') {
                returnedItem.element = this._getItemElement(item);
            }
            return returnedItem;
        }

        private _getItemElement(item: IToolbarItem): string {
            let spanText = document.createElement('span');
            spanText.style.verticalAlign = 'middle';
            spanText.innerHTML = item.title;
            return item.icon + spanText.outerHTML;
        }
    }

    /**
     * Defines the interface for a normal toolbar item.
     */
    export interface IToolbarItem {
        /** A html fragment or svg fragment represents the toolbar icon. */
        icon?: string;
        /** A pure text or a html fragment represents the toolbar title.*/
        title?: string;
        /** A method to be called when the toolbar item is clicked. */
        command?: Function;
        /** A html fragment, text or svg fragment represents the toolbar item(icon and title) */
        element?: string;
        /**
         * A method that takes a @see:Tile object
         * and returns a boolean that decides
         * which toolbar item should be shown in the toolbar.
         * If it returns false, the item will be not shown.
         */
        isVisible?: (hostTile: Tile) => boolean;
    }

    /**
     * Defines the interface for a toggable toolbar item.
     */
    export interface IToggleToolbarItem extends IToolbarItem {
        /**
         * A method that takes a @see:Tile object
         * and returns a boolean that decides
         * which toolbar item should be used in the toolbar.
         * If it returns false, the @see:toggleItem will be used.
         * Otherwise, itself is used.
         */
        condition?: (hostTile: Tile) => boolean;
        /**
         * The toggled toobar item.
         * When the @see:condition method returns false, it will be used in the toolbar.
         */
        toggleItem?: IToolbarItem;
    }
}