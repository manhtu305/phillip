﻿/**
 * Defines the @see:SplitLayout object and associated classes.
 */
module c1.nav.split {
    'use strict';

    /**
     * Defines a class represents the layout, in which the items are splitted by splitters.
     */
    export class SplitLayout extends LayoutBase {
        static _HIDDEN_CLASS = 'wj-dl-hidden';
        static _CLASS_LAYOUT = 'wj-dl-split';
        static _SETTING_NAMES: string[] = null;

        private _orientation = LayoutOrientation.Vertical;
        private _root: _RootGroup;

        private _areaMenu: _AreaMenu;
        private _elePreviewTile: HTMLElement;
        //_eleHiddenLayer: HTMLElement;//remove code fix issue drag/drop(changeset 342592) which issue dosen't happen
        private _movingToTile: SplitTile;
        private _movedTile: SplitTile;
        private _sizeMovedTile: wijmo.Size;

        /**
         * Initializes a new instance of the @see:SplitLayout class.
         *
         * @param options JavaScript object containing initialization data for the layout.
         */
        constructor(options?: any) {
            super();
            this._initRoot(options);
            this.initialize(options);
        }

        private _initRoot(options?: any) {
            let opts = {};
            if (options && options.orientation != null) {
                opts['orientation'] = options.orientation;
            }
            this._root = new _RootGroup(opts);
            this._root._setParent(this);
        }

        /**
         * Gets or sets the orientation of the group.
         */
        get orientation(): LayoutOrientation {
            return this._root.orientation;
        }
        set orientation(value: LayoutOrientation) {
            value = wijmo.asEnum(value, LayoutOrientation);
            if (value === this.orientation) {
                return;
            }

            this._root.orientation = value;
            this.invalidate();
        }

        /**
         * Gets or sets the layout item collection.
         */
        get items(): LayoutItemCollection {
            return this._root.children;
        }

        /**
         * Gets the full type name of the current object.
         */
        get fullTypeName(): string {
            return 'c1.nav.split.SplitLayout';
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (SplitLayout._SETTING_NAMES) {
                return SplitLayout._SETTING_NAMES;
            }

            SplitLayout._SETTING_NAMES = ['orientation'].concat(super._getNames());
            return SplitLayout._SETTING_NAMES;
        }

        // Initializes the Dom elements, styles and related resources for the split layout.
        _initializeComponent() {
            super._initializeComponent();
            wijmo.addClass(this.dashboard.hostElement, SplitLayout._CLASS_LAYOUT);
            //if (!this._eleHiddenLayer) { //remove code fix issue drag/drop(changeset 342592) which issue dosen't happen
            //    let eleHiddenLayer = document.createElement('div');
            //    wijmo.addClass(eleHiddenLayer, 'wj-dl-conts-container');
            //    this.dashboard.hostElement.appendChild(eleHiddenLayer);
            //    this._eleHiddenLayer = eleHiddenLayer;
            //}
            this._initMenu();
        }
        private _initMenu() {
            let eleMenu = document.createElement('div');
			eleMenu.style.zIndex = '99999';
			this._areaMenu = new _AreaMenu(eleMenu, this.dashboard);
            this._areaMenu.itemSelected.addHandler(this._onItemSelected, this);
            this._areaMenu.itemChanged.addHandler(this._onItemChanged, this);
            this.dashboard.hostElement.appendChild(eleMenu);
        }
        // Update the preview tile position when dragging. 
        private _onItemChanged() {
            let layoutItem = this._areaMenu.selectedItem;
            if (!layoutItem) {
                if (this._elePreviewTile) {
                    wijmo.addClass(this._elePreviewTile, SplitLayout._HIDDEN_CLASS);
                }
                return;
            }

            if (!this._elePreviewTile) {
                this._elePreviewTile = document.createElement('div');
                wijmo.addClass(this._elePreviewTile, 'wj-split-preview-tile');
            }

            let left = 0, top = 0,
                rectToLI = layoutItem.hostElement.getBoundingClientRect();

            let sizeStatus = _SplitLayoutItem._getSizeStatus(this._movedTile.size),
                size: any,
                isHorizontal = this._areaMenu.selectedDirection == _Direction.Left || this._areaMenu.selectedDirection == _Direction.Right,
                sizeXName = isHorizontal ? 'width' : 'height',
                sizeYName = isHorizontal ? 'height' : 'width';
            switch (sizeStatus) {
                case _SizeStatus.Fixed:
                    size = this._movedTile.size;
                    break;
                case _SizeStatus.Auto:
                    size = this._movedTile._fullRenderSize[sizeXName];
                    break;
                case _SizeStatus.Fill:
                    size = this._sizeMovedTile[sizeXName];
                    break;
            }

            this._elePreviewTile.style[sizeXName] = size + 'px';
            this._elePreviewTile.style[sizeYName] = rectToLI[sizeYName] + 'px';
            if (this._areaMenu.selectedDirection == _Direction.Left) {
                if (this.dashboard.rightToLeft) {
                    left = rectToLI[sizeXName] - size;
                }
            }
            else if (this._areaMenu.selectedDirection == _Direction.Right) {                
                if (!this.dashboard.rightToLeft) {
                    left = rectToLI[sizeXName] - size;    
                }          
            } else if (this._areaMenu.selectedDirection == _Direction.Bottom) {
                top = rectToLI[sizeXName] - size;
            }
            this._elePreviewTile.style.left = left + 'px';
            this._elePreviewTile.style.top = top + 'px';
            layoutItem.hostElement.appendChild(this._elePreviewTile);
            wijmo.removeClass(this._elePreviewTile, SplitLayout._HIDDEN_CLASS);
        }
        private _getVisibleChildrenCount(group: SplitGroup): number {
            let count = 0;
            group.children.forEach(item => {
                if (item instanceof SplitTile) {
                    count += (item.visible ? 1 : 0);
                } else if (item instanceof SplitGroup) {
                    if (this._getVisibleChildrenCount(item)) {
                        count += 1;
                    }
                }
            });

            return count;
        }

        // confirm the position where the tile will be dropped to.
        private _onItemSelected() {
            if (!this._elePreviewTile) {
                return;
            }

            wijmo.addClass(this._elePreviewTile, SplitLayout._HIDDEN_CLASS);
            this._insertItem(this._movedTile, this._areaMenu.selectedItem, this._areaMenu.selectedDirection);
            this._movedTile = null;
            this._sizeMovedTile = null;
            this.refresh(false);
        }
        private _insertItem(newItem: SplitTile, referItem: any, direction: _Direction) {
            let newGroup = (orientation: LayoutOrientation, gSize?: any) => {
                let g = new SplitGroup();
                g.orientation = orientation;
                g.size = gSize == null ? 100 : gSize;
                return g;
            };

            let parentGroup = this._findParentGroup(referItem);
            if (!parentGroup) {
                let rootGroup = referItem as SplitGroup,
                    isInVerticalGroup = (rootGroup.orientation == LayoutOrientation.Vertical),
                    isSameDirection = (dir: _Direction, orientation: LayoutOrientation): boolean => {
                        switch (dir) {
                            case _Direction.Left:
                            case _Direction.Right:
                                return orientation === LayoutOrientation.Horizontal;
                            case _Direction.Top:
                            case _Direction.Bottom:
                                return orientation === LayoutOrientation.Vertical;
                        }
                    };
                if (isSameDirection(direction, rootGroup.orientation)) {
                    parentGroup = rootGroup;
                } else {
                    parentGroup = newGroup(isInVerticalGroup ? LayoutOrientation.Horizontal : LayoutOrientation.Vertical, '*');
                    parentGroup.children.push(rootGroup);
                    this._initRoot();
                    this.items._insertAtInternal(parentGroup);
                }
            }

            let parentOrientation = parentGroup.orientation,
                insertIndex = parentGroup.children.indexOf(referItem),
                insertSameDirection = (index: number) => {
                    parentGroup.children._insertAtInternal(newItem, index);
                },
                insertDiffDirection = (orientation: LayoutOrientation, insertFirst: boolean) => {
                    if (parentGroup.children.length == 1
                        && !(parentGroup instanceof _RootGroup)) {
                        parentGroup.orientation = orientation;
                        parentGroup.children._insertAtInternal(newItem, insertFirst ? 0 : 1);
                        return;
                    }
                    parentGroup.children._removeInternal(referItem);
                    let newG = newGroup(orientation, '*');
                    if (insertFirst) {
                        newG.children.push(newItem);
                        newG.children.push(referItem);
                    }
                    else {
                        newG.children.push(referItem);
                        newG.children.push(newItem);
                    }
                    parentGroup.children._insertAtInternal(newG, insertIndex);
                };

            switch (direction) {
                case _Direction.Left:
                    if (parentOrientation === LayoutOrientation.Vertical) {
                        insertDiffDirection(LayoutOrientation.Horizontal, true);
                    }
                    else {
                        insertSameDirection(insertIndex);
                    }
                    break;
                case _Direction.Right:
                    if (parentOrientation === LayoutOrientation.Vertical) {
                        insertDiffDirection(LayoutOrientation.Horizontal, false);
                    }
                    else {
                        insertSameDirection(insertIndex + 1);
                    }
                    break;
                case _Direction.Top:
                    if (parentOrientation === LayoutOrientation.Horizontal) {
                        insertDiffDirection(LayoutOrientation.Vertical, true);
                    }
                    else {
                        insertSameDirection(insertIndex);
                    }
                    break;
                case _Direction.Bottom:
                    if (parentOrientation === LayoutOrientation.Horizontal) {
                        insertDiffDirection(LayoutOrientation.Vertical, false);
                    }
                    else {
                        insertSameDirection(insertIndex + 1);
                    }
                    break;
            }
        }
        private _findParentGroup(item: LayoutItem): SplitGroup {
            let parent = item.parent;
            if (parent && parent instanceof SplitGroup) {
                return parent as SplitGroup;
            }

            return null;
        }

        /**
         * Detaches this layout from the @see:DashboardLayout control.
         *
         * It is often used when applying the new layout, the old layout should be detached.
         *
         */
        detach() {
            if (this._areaMenu) {
                this._areaMenu.itemSelected.removeAllHandlers();
                this._areaMenu.itemChanged.removeAllHandlers();
                this._areaMenu.disposeResources(true);
                _removeElement(this._areaMenu.hostElement);
                this._areaMenu = null;
            }

            //if (this._eleHiddenLayer) { //remove code fix issue drag/drop(changeset 342592) which issue dosen't happen
            //    _removeElement(this._eleHiddenLayer);
            //    this._eleHiddenLayer = null;
            //}

            if (this.dashboard) {
                wijmo.removeClass(this.dashboard.hostElement, SplitLayout._CLASS_LAYOUT);
            }

            super.detach();
        }

        /**
         * Draws the layout.
         */
        draw() {
            // update the available content size.
            let containerStyle = window.getComputedStyle(this._container),
                paddingLeft = parseFloat(containerStyle.getPropertyValue('padding-left')),
                paddingRight = parseFloat(containerStyle.getPropertyValue('padding-right')),
                paddingTop = parseFloat(containerStyle.getPropertyValue('padding-top')),
                paddingBottom = parseFloat(containerStyle.getPropertyValue('padding-bottom')),

                // save the original size of the container
                rectDashboard = this._container.getBoundingClientRect(),
                width = Math.floor(rectDashboard.width),
                height = Math.floor(rectDashboard.height);

            this._root.render(this._container);
            this._root.updateMinRenderSize();
            let oldWidth,
                minRenderSize = this._root._getMinRenderSize();
            do {
                oldWidth = width;
                width = width - paddingLeft - paddingRight;
                height = height - paddingTop - paddingBottom;

                if (!width || width < 0) {
                    width = this._root._fullRenderSize.width;
                }

                if (!height || height < 0) {
                    height = this._root._fullRenderSize.height;
                }

                width = Math.max(width, minRenderSize.width);
                height = Math.max(height, minRenderSize.height);

                this._root.updateRenderSize(new wijmo.Size(width, height));
                width = this._container.clientWidth;
            } while (oldWidth != width);
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (this._areaMenu) {
                this._areaMenu.disposeResources(false);
            }

            if (this._root) {
                this._root.dispose(fullDispose);
            }

            if (fullDispose) {
                //this._movingToPosition = null;
                this._movingToTile = null;
                this._sizeMovedTile = null;
                this._movedTile = null;
                if (this._elePreviewTile) {
                    _removeElement(this._elePreviewTile);
                    this._elePreviewTile = null;
                }

                //if (this._eleHiddenLayer) {//remove code fix issue drag/drop(changeset 342592) which issue dosen't happen
                //    this._eleHiddenLayer.innerHTML = '';
                //}
            }

            super.dispose(fullDispose);
        }

        /**
         * Starts to move the tile.
         *
         * @param movedTile The tile to be moved.
         * @param startPosition The started position where the tile will be moved from. It is the coordinate within the browser visible area.
         * @return A boolean value indicates whether the tile could be moved.
         */
        startMove(movedTile: Tile, startPosition: wijmo.Point): boolean {
            let result = super.startMove(movedTile, startPosition);
            if (result) {
                this._movedTile = movedTile as SplitTile;
                this._sizeMovedTile = this._movedTile._minRenderSizeAsChild;
                this._movedTile.visible = false;
            }
            return result;
        }

        /**
         * Moves the tile.
         *
         * @param movedTile The tile to be moved.
         * @param position The position of the moving mouse. It is the coordinate within the browser visible area.
         */
        move(movedTile: Tile, position: wijmo.Point) {
            super.move(movedTile, position);
            let hitTile = this._findHitTile(position);
            if (!hitTile && this._areaMenu.visible) {
                this._areaMenu.visible = false;
                this._movingToTile = null;
            } else if (hitTile && this._movingToTile != hitTile) {
                this._movingToTile = hitTile;
                this._areaMenu.show(hitTile);
            }
        }
        private _findHitTile(position: wijmo.Point): SplitTile {
            let foundedTile: SplitTile,
                rectAreaMenu;

            // TFS 403711
            if (this._areaMenu.hostElement.innerHTML == "")
                this._initMenu();
            rectAreaMenu = this._areaMenu.hostElement.getBoundingClientRect();

            // When AreaMenu is shown and the mouse is hoving on it,
            // we think the mouse works on the AreaMenu.
            if (this._areaMenu.visible
                && position.x >= rectAreaMenu.left && position.x <= rectAreaMenu.right
                && position.y >= rectAreaMenu.top && position.y <= rectAreaMenu.bottom) {
                return this._movingToTile;
            }

            let matchLayoutItem = (li: any): boolean => {
                if (!(<_ISplitLayoutItem>li)._isVisible()) {
                    return false;
                }

                let rectTile = li.hostElement.getBoundingClientRect();
                if (position.x >= rectTile.left && position.x <= rectTile.right
                    && position.y >= rectTile.top && position.y <= rectTile.bottom) {
                    return true;
                }

                return false;
            },
                matchGroup = (group: SplitGroup) => {
                    let founded = false,
                        index = 0,
                        count = group.children.length;
                    while (!founded && index < count) {
                        let child = group.children[index] as LayoutItem;
                        founded = matchLayoutItem(child);
                        if (founded) {
                            if (child instanceof SplitGroup) {
                                matchGroup(child as SplitGroup);
                            } else {
                                foundedTile = child as SplitTile;
                            }
                        }
                        index++;
                    }
                };

            matchGroup(this._root);
            return foundedTile;
        }

        /**
         * Ends moving the tile at the specified postion.
         *
         * @param movedTile The tile to be moved.
         * @param endPosition The position where the tile is moved to. It is the coordinate within the browser visible area.
         */
        endMove(movedTile: Tile, endPosition: wijmo.Point) {
            // get if any area is selected at first
            // to avoid the ui change will cause the calculation is not correct.
            let isSomeAreaSelected = this._areaMenu.hitTest(endPosition);
            super.endMove(movedTile, endPosition);
            if (isSomeAreaSelected && movedTile._remove(true)) {
                movedTile.dispose(false);
                this._onItemSelected();
            }
            movedTile.visible = true;
            this._movingToTile = null;
            this._areaMenu.visible = false;
        }

        _cancelMove(movedTile: Tile) {
            if (this._elePreviewTile && !wijmo.hasClass(this._elePreviewTile, SplitLayout._HIDDEN_CLASS)) {
                wijmo.addClass(this._elePreviewTile, SplitLayout._HIDDEN_CLASS);
            }

            this._movedTile = null;
            this._sizeMovedTile = null;
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            return new SplitTile(tileOpts);
        }

        // creates a group object from the options.
        _createGroup(groupOpts): Group {
            return new SplitGroup(groupOpts);
        }

        static _getPossibleItems(tile: SplitTile) {
            let leftItems: LayoutItem[] = [],
                rightItems: LayoutItem[] = [],
                topItems: LayoutItem[] = [],
                bottomItems: LayoutItem[] = [],
                leftBreaking = false,
                rightBreaking = false,
                topBreaking = false,
                bottomBreaking = false,
                isFirstItem = (checkedGroup: SplitGroup, child: LayoutItem): boolean => {
                    let index = checkedGroup.children.indexOf(child);
                    if (index < 0) {
                        return false;
                    }

                    for (let i = 0; i < index; i++) {
                        let item = checkedGroup.children[i];
                        if (<_ISplitLayoutItem>item._isVisible()) {
                            return false;
                        }
                    }

                    return true;
                },
                isLastItem = (checkedGroup: SplitGroup, child: LayoutItem): boolean => {
                    let index = checkedGroup.children.indexOf(child);
                    if (index < 0) {
                        return false;
                    }

                    for (let i = checkedGroup.children.length - 1; i > index; i--) {
                        let item = checkedGroup.children[i];
                        if (<_ISplitLayoutItem>item._isVisible()) {
                            return false;
                        }
                    }

                    return true;
                },
                hasOneChild = (checkedGroup: SplitGroup): boolean => {
                    let totalCount = 0;
                    checkedGroup.children.forEach(child => {
                        if ((<_ISplitLayoutItem>child)._isVisible()) {
                            totalCount++;
                        }
                    });
                    return totalCount === 1;
                },
                funCheck = (checkedGroup: SplitGroup, child: LayoutItem) => {
                    switch (checkedGroup.orientation) {
                        case LayoutOrientation.Vertical:
                            if (checkedGroup.parent instanceof SplitLayout) {
                                topBreaking = true;
                                bottomBreaking = true;

                                if (hasOneChild(checkedGroup)) {
                                    leftBreaking = true;
                                    rightBreaking = true;
                                }
                            }

                            if (!leftBreaking) {
                                leftItems.push(checkedGroup);
                            }

                            if (!rightBreaking) {
                                rightItems.push(checkedGroup);
                            }

                            if (!topBreaking && !isFirstItem(checkedGroup, child)) {
                                topBreaking = true;
                            }

                            if (!bottomBreaking && !isLastItem(checkedGroup, child)) {
                                bottomBreaking = true;
                            }
                            break;
                        case LayoutOrientation.Horizontal:
                            if (checkedGroup.parent instanceof SplitLayout) {
                                leftBreaking = true;
                                rightBreaking = true;

                                if (hasOneChild(checkedGroup)) {
                                    topBreaking = true;
                                    bottomBreaking = true;
                                }
                            }

                            if (!leftBreaking && !isFirstItem(checkedGroup, child)) {
                                leftBreaking = true;
                            }

                            if (!rightBreaking && !isLastItem(checkedGroup, child)) {
                                rightBreaking = true;
                            }

                            if (!topBreaking) {
                                topItems.push(checkedGroup);
                            }

                            if (!bottomBreaking) {
                                bottomItems.push(checkedGroup);
                            }
                            break;
                    }
                };

            leftItems.push(tile);
            rightItems.push(tile);
            topItems.push(tile);
            bottomItems.push(tile);

            let current: any = tile;
            while (current && current.parent && current.parent instanceof SplitGroup) {
                if (!hasOneChild(current.parent)) {
                    funCheck(current.parent, current);
                }

                current = current.parent;
            }

            return { left: leftItems, right: rightItems, top: topItems, bottom: bottomItems };
        }

        static _parseStarSize(value: any): number {
            if (wijmo.isString(value) && value.length > 0 && value[value.length - 1] == '*') {
                let sz = value.length == 1 ? 1 : parseInt(value.substr(0, value.length - 1)) * 1;
                if (sz > 0 && !isNaN(sz)) {
                    return sz;
                }
            }

            return null;
        }
    }

    /**
     * Defines the tile class for the @see:SplitLayout.
     */
    export class SplitTile extends Tile {
        // please keep same as the css class 
        static _MARGIN_SPACE = 3;

        static _SETTING_NAMES: string[] = null;

        // the size when rendering the content completely.
        _fullRenderSize: wijmo.Size;
        _minRenderSizeAsChild: wijmo.Size;
        _isMoving: boolean;

        private _internalSLI = new _SplitLayoutItem(this);

        private _minSize: number = LayoutItem._MIN_SIZE;

        /**
         * Initializes a new @see:SplitTile.
         *
         * @param opts JavaScript object containing initialization data for the tile.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
        }

        /**
         * Gets or sets the minimum size of the tile in pixels.
         */
        get minSize(): number {
            return this._minSize;
        }
        set minSize(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive number!');
            if (value != this._minSize) {
                this._minSize = Math.max(wijmo.asNumber(value, true, true), LayoutItem._MIN_SIZE);
            }
        }

        /**
         * Gets or sets the size of the tile.
         *
         * Tile size may be positive numbers (in pixels), null(fits the tile content)
         * or strings in the format '{number}*' (star sizing).
         *
         * The star-sizing option performs a XAML-style dynamic sizing where column 
         * widths are proportional to the number before the star. For example, if
         * a group has three tiles with sizes "100", "*", and "3*", the first column
         * will be 100 pixels size, the second will take up 1/4th of the remaining
         * space, and the last will take up the remaining 3/4ths of the remaining space.
         *
         * Star-sizing allows you to define tiles that automatically stretch to fill
         * the space available. For example, set the size of the last tile to "*"
         * and it will automatically extend to fill the entire group size so there's
         * no empty space. You may also want to set the tile's @see:minSize property
         * to prevent the tile from getting too narrow.
         */
        get size(): any {
            return this._internalSLI.size;
        }
        set size(value: any) {
            this._internalSLI.size = value;
        }

        _getSize(): any {
            return this._internalSLI.getSize();
        }

        private _getSizeStatus(): _SizeStatus {
            return this._internalSLI.sizeStatus;
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (SplitTile._SETTING_NAMES) {
                return SplitTile._SETTING_NAMES;
            }

            SplitTile._SETTING_NAMES = ['minSize', 'size'].concat(super._getNames());
            return SplitTile._SETTING_NAMES;
        }

        /**
         * Draws the tile.
         */
        draw() {
            super.draw();

            // update the full render size
            let isInVerticalGroup = this._internalSLI.isInVerticalGroup,
                currentSizeHelper = _SplitLayoutItem.getSizeHelper(isInVerticalGroup),
                minSize = this._getMinSize(),
                fullWidth = Math.max(minSize.width, this.hostElement.offsetWidth),
                fullHeight = Math.max(minSize.height, this.hostElement.offsetHeight);
            fullWidth += SplitTile._MARGIN_SPACE * 2;
            fullHeight += SplitTile._MARGIN_SPACE * 2;
            this._fullRenderSize = new wijmo.Size(fullWidth, fullHeight);
        }

        // gets the minimum size(It is the smallest one.)
        _getMinSize(): wijmo.Size {
            let sizeHelper = _SplitLayoutItem.getSizeHelper(this._internalSLI.isInVerticalGroup),
                minSize = new wijmo.Size();
            minSize[sizeHelper.xName] = this.minSize + 2 * SplitTile._MARGIN_SPACE;
            minSize[sizeHelper.yName] = LayoutItem._MIN_SIZE + 2 * SplitTile._MARGIN_SPACE;
            return minSize;
        }

        // gets/updates the minimum value of the render size.
        // It is related with the size value.
        _updateMinRenderSizeAsChild(maxStarSize: number) {
            this._minRenderSizeAsChild = this._internalSLI.getMinRenderSizeAsChild(maxStarSize);
        }

        // gets the actual render size.
        _getRenderSize(): wijmo.Size {
            return this._internalSLI.renderSize;
        }

        // updates the tile with the specified size.
        updateRenderSize(actualSize: wijmo.Size) {
            this._internalSLI.renderSize = actualSize;
            this._updateSize(new wijmo.Size(actualSize.width - SplitTile._MARGIN_SPACE * 2,
                actualSize.height - SplitTile._MARGIN_SPACE * 2));
            wijmo.Control.refreshAll(this.hostElement);
        }

        // Gets the size of the tile from the host DOM element.
        _getLayoutSize(): wijmo.Size {
            return this._internalSLI.getLayoutSize(this._hostElement);
        }

        // modify the size of the tile.
        _increaseSize(delta: number) {
            this._internalSLI.increaseSize(delta);
            this._onLayoutChanged();
        }

        _updateSizeToFixed() {
            this._internalSLI.updateSizeToFixed();
        }

        _getAvailableResizingSpace(): number {
            return this._internalSLI.getAvailableResizingSpace();
        }

        _updatePartialView(container?: HTMLElement) {
            if (container) {
                container.appendChild(this.hostElement);
            }
        }


        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (fullDispose) {
                this._fullRenderSize = null;
                this._isMoving = null;
            }
            //else if (this.layout) { //remove code fix issue drag/drop(changeset 342592) which issue dosen't happen
            //    if (this.hostElement) {
            //        let splitLayout = <SplitLayout>this.layout;
            //        splitLayout._eleHiddenLayer.appendChild(this.hostElement);
            //    }
            //}
            this._minRenderSizeAsChild = null;
            this._internalSLI.dispose();
            super.dispose(fullDispose);
        }

        /**
         * Begins the moving.
         */
        beginMove() {
            this._isMoving = true;
        }

        /**
         * End the moving.
         */
        endMove() {
            this._isMoving = false;
            super.endMove();
        }

        _isVisible(): boolean {
            return this.visible;
        }

        _onTileVisibilityChanged(e: TileEventArgs) {
            if (!e.tile.visible) {
                _removeElement(this.hostElement);
            }
            super._onTileVisibilityChanged(e);
        }
    }

    /**
     * Defines the group class for splitlayout. 
     */
    export class SplitGroup extends Group {
        static _CLASS_GROUP = 'wj-split-group';
        static _CLASS_GROUP_WRAPPER = 'wj-group-wrapper';
        static _CLASS_SPLIT_ITEM = 'wj-split-item';
        static _SETTING_NAMES: string[] = null;

        private _orientation = LayoutOrientation.Vertical;

        private _splitters: wijmo.Control[] = [];

        private _totalStarCount = 0;
        private _totalStarSize = 0;
        _maxStarUnitSize = 0;
        private _msusFullRender = 0;

        private _arrAutoSizeItems = new wijmo.collections.ObservableArray();
        private _arrStarSizeItems = new wijmo.collections.ObservableArray();
        private _internalSLI = new _SplitLayoutItem(this);

        _fullRenderSize: wijmo.Size;
        _minRenderSizeAsChild: wijmo.Size;
        private _visibleChildren: LayoutItemCollection;

        /**
         * Initializes a new @see:SplitGroup.
         *
         * @param opts JavaScript object containing initialization data for the group.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
        }

        /**
         * Gets or sets the orientation of the group.
         */
        get orientation(): LayoutOrientation {
            return this._orientation;
        }
        set orientation(value: LayoutOrientation) {
            value = wijmo.asEnum(value, LayoutOrientation);
            if (value === this._orientation) {
                return;
            }

            this._orientation = value;
        }

        /**
         * Gets or sets the size of the group.
         *
         * Tile size may be positive numbers (in pixels), null(fits the tile content)
         * or strings in the format '{number}*' (star sizing).
         *
         * The star-sizing option performs a XAML-style dynamic sizing where column 
         * widths are proportional to the number before the star. For example, if
         * a group has three tiles with sizes "100", "*", and "3*", the first column
         * will be 100 pixels size, the second will take up 1/4th of the remaining
         * space, and the last will take up the remaining 3/4ths of the remaining space.
         *
         * Star-sizing allows you to define tiles that automatically stretch to fill
         * the space available. For example, set the size of the last tile to "*"
         * and it will automatically extend to fill the entire group size so there's
         * no empty space. You may also want to set the tile's @see:minSize property
         * to prevent the tile from getting too narrow.
         */
        get size(): any {
            return this._internalSLI.size;
        }
        set size(value: any) {
            this._internalSLI.size = value;
        }

        _getSize(): any {
            return this._internalSLI.getSize();
        }

        // Overrides get the default value with the actual type.
        _getDefaultItems(): LayoutItemCollection {
            return new _SplitGroupChildren(this);
        }

        get _isVertical(): boolean {
            return this.orientation === LayoutOrientation.Vertical;
        }

        private _getSizeStatus(): _SizeStatus {
            return this._internalSLI.sizeStatus;
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (SplitGroup._SETTING_NAMES) {
                return SplitGroup._SETTING_NAMES;
            }

            SplitGroup._SETTING_NAMES = ['orientation', 'size'].concat(super._getNames());
            return SplitGroup._SETTING_NAMES;
        }

        // Initializes the dom elements, styles and related resources for the group.
        _initializeComponent() {
            super._initializeComponent();
            wijmo.addClass(this.hostElement, SplitGroup._CLASS_GROUP);
            let eleContentWrapper = document.createElement('div');
            wijmo.addClass(eleContentWrapper, 'wj-content-wrapper');
            this._contentElement.appendChild(eleContentWrapper);
            this._contentElement = eleContentWrapper;
        }

        _redraw() {
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            this._splitters.forEach(item => {
                let splitter = item as _Splitter;
                splitter.dispose();
            });
            this._splitters = [];

            this._totalStarCount = 0;
            this._maxStarUnitSize = 0;
            this._msusFullRender = 0;
            this._arrAutoSizeItems.clear();
            this._arrStarSizeItems.clear();
            this._fullRenderSize = null;
            this._minRenderSizeAsChild = null;
            this._internalSLI.dispose();
            this._visibleChildren = null;
            super.dispose(fullDispose);
            if (!fullDispose) {
                _removeElement(this._hostElement);
                this._contentElement = null;
                this._hostElement = null;
            }
        }

        private _getVisibleChildren(): LayoutItemCollection {
            let visibleChildren = this._getDefaultItems();
            this.children.forEach(child => {
                let isli = <_ISplitLayoutItem>child;
                if (isli._isVisible()) {
                    visibleChildren.push(isli);
                }
            });
            return visibleChildren;
        }

        /**
         * Draws the group.
         */
        draw() {
            this._visibleChildren = this._getVisibleChildren();
            let eleWrapper: HTMLElement,
                createWrapper = () => {
                    let wrapperElement = document.createElement('div');
                    wijmo.addClass(wrapperElement, SplitGroup._CLASS_GROUP_WRAPPER);
                    return wrapperElement;
                },
                maxStarSize = 0, groupYSize = 0,
                mssFullRender = 0,
                sizeHelper = _SplitLayoutItem.getSizeHelper(this._isVertical);

            if (!this._isVertical) {
                eleWrapper = createWrapper();
                this._contentElement.appendChild(eleWrapper);
            }

            this._totalStarCount = 0;
            this._maxStarUnitSize = 0;
            this._msusFullRender = 0;
            this._visibleChildren.forEach((child, index) => {
                let spLI = child as _ISplitLayoutItem;
                // render the child firstly.
                if (this._isVertical) {
                    eleWrapper = createWrapper();
                    this._contentElement.appendChild(eleWrapper);
                }

                let eleSplitItem = this._createSplitItem(this.orientation, index);
                eleWrapper.appendChild(eleSplitItem);
                (child as LayoutItem).render(eleSplitItem);

                // calculate the size.
                groupYSize = Math.max(groupYSize, spLI._fullRenderSize[sizeHelper.yName]);
                switch (spLI._getSizeStatus()) {
                    case _SizeStatus.Auto:
                        this._arrAutoSizeItems.push(spLI);
                        break;
                    case _SizeStatus.Fill:
                        let nStarSize = SplitLayout._parseStarSize(spLI._getSize()),
                            minSize = spLI._getMinSize();
                        this._totalStarCount += nStarSize;
                        maxStarSize = Math.max(maxStarSize, minSize[sizeHelper.xName] / nStarSize);
                        mssFullRender = Math.max(maxStarSize, spLI._fullRenderSize[sizeHelper.xName] / nStarSize);
                        this._arrStarSizeItems.push(spLI);
                        break;
                    default:
                        break;
                }
            });
            this._maxStarUnitSize = maxStarSize;
            this._msusFullRender = mssFullRender;

            // update the full render size.
            let groupXSize = 0;
            this._visibleChildren.forEach(child => {
                let spLI = child as _ISplitLayoutItem;
                spLI._updateMinRenderSizeAsChild(this._maxStarUnitSize);
                if (spLI._getSizeStatus() != _SizeStatus.Fill) {
                    groupXSize += spLI._minRenderSizeAsChild[sizeHelper.xName];
                }
            });
            groupXSize += this._msusFullRender * this._totalStarCount;
            this._fullRenderSize = new wijmo.Size();
            this._fullRenderSize[sizeHelper.xName] = groupXSize;
            this._fullRenderSize[sizeHelper.yName] = groupYSize;
        }

        private _createSplitItem(orientation: LayoutOrientation, index: number): HTMLDivElement {
            let itemElement = document.createElement('div'),
                addSplitter = (index != this._visibleChildren.length - 1);
            wijmo.addClass(itemElement, SplitGroup._CLASS_SPLIT_ITEM);

            if (addSplitter) {
                let eleSplitter = document.createElement('div');
                let splitter = new _Splitter(eleSplitter, {
                    orientation: this._isVertical ? LayoutOrientation.Horizontal : LayoutOrientation.Vertical
                });
                splitter.group = this;
                splitter['pre'] = this._visibleChildren[index];
                splitter['next'] = this._visibleChildren[index + 1];
                splitter.dragging.addHandler(this._dragging, this);
                splitter.drop.addHandler(this._drop, this);
                this._splitters.push(splitter);
                itemElement.appendChild(eleSplitter);
            }

            return itemElement;
        }
        private _dragging(sender: _Splitter, e: _DraggingEventArgs) {
            let delta = e.distance,
                preLI = <_ISplitLayoutItem>sender['pre'],
                nextLI = <_ISplitLayoutItem>sender['next'];

            if (!preLI || !nextLI) {
                e.cancel = true;
                return;
            }

            let preResizingSpace = preLI._getAvailableResizingSpace() * (-1),
                nextResizingSpace = nextLI._getAvailableResizingSpace();
            if (delta > 0 && delta > nextResizingSpace) {
                delta = nextResizingSpace;
            } else if (delta < 0 && delta < preResizingSpace) {
                delta = preResizingSpace;
            }
            e.distance = delta;
            e.cancel = !delta;
        }
        private _drop(sender: _Splitter, e: _DropEventArgs) {
            let delta = e.distance,
                preLI = sender['pre'],
                nextLI = sender['next'];

            if (delta == 0) {
                return;
            }

            let isliPre = preLI as _ISplitLayoutItem,
                isliNext = nextLI as _ISplitLayoutItem,
                ssPre = isliPre._getSizeStatus(),
                ssNext = isliNext._getSizeStatus();

            if (ssPre == _SizeStatus.Auto) {
                this._arrAutoSizeItems.remove(isliPre);
                isliPre._updateSizeToFixed();
            } else if (ssPre == _SizeStatus.Fill) {
                this._updateItemFromFillToFixed(isliPre);
            }
            isliPre._increaseSize(delta);

            if (ssNext == _SizeStatus.Auto) {
                this._arrAutoSizeItems.remove(isliNext);
                isliNext._updateSizeToFixed();
            } else if (ssNext == _SizeStatus.Fill) {
                this._updateItemFromFillToFixed(isliNext);
            }
            isliNext._increaseSize(delta * (-1));

            if (ssPre == _SizeStatus.Fill || ssNext == _SizeStatus.Fill) {
                this._msusFullRender = 0;
                if (this._arrStarSizeItems && this._arrStarSizeItems.length) {
                    // update all the fill items' minRenderSizeAsChild.
                    let sizeHelper = _SplitLayoutItem.getSizeHelper(this._isVertical);
                    this._maxStarUnitSize = 0;
                    this._arrStarSizeItems.forEach(item => {
                        let si = <_ISplitLayoutItem>item,
                            nStarSize = SplitLayout._parseStarSize(si._getSize()),
                            minSize = si._getMinSize();
                        this._maxStarUnitSize = Math.max(this._maxStarUnitSize, minSize[sizeHelper.xName] / nStarSize);
                        this._msusFullRender = Math.max(this._msusFullRender, si._fullRenderSize[sizeHelper.xName] / nStarSize);
                    });

                    this._arrStarSizeItems.forEach(item => {
                        let si = <_ISplitLayoutItem>item;
                        si._updateMinRenderSizeAsChild(this._maxStarUnitSize);
                    });
                }
            }

            // update the full render size.
            let sizeHelper = _SplitLayoutItem.getSizeHelper(this._isVertical),
                groupXSize = 0;
            this._visibleChildren.forEach(child => {
                let spLI = child as _ISplitLayoutItem;
                if (spLI._getSizeStatus() != _SizeStatus.Fill) {
                    groupXSize += spLI._minRenderSizeAsChild[sizeHelper.xName];
                }
            });
            groupXSize += this._msusFullRender * this._totalStarCount;
            this._fullRenderSize = new wijmo.Size();
            this._fullRenderSize[sizeHelper.xName] = groupXSize;
        }

        private _updateItemFromFillToFixed(item: _ISplitLayoutItem) {
            let nStarSize = SplitLayout._parseStarSize(item._getSize());
            this._totalStarCount -= nStarSize;

            let renderSize = item._getRenderSize(),
                sizeHelper = _SplitLayoutItem.getSizeHelper(this._isVertical);
            this._totalStarSize -= renderSize[sizeHelper.xName];

            this._arrStarSizeItems.remove(item);
            item._updateSizeToFixed();
        }

        _updateSizeToFixed() {
            this._internalSLI.updateSizeToFixed();
        }

        _getMinSize(): wijmo.Size {
            let sizeHelper = _SplitLayoutItem.getSizeHelper(this._isVertical),
                minSize = new wijmo.Size();

            minSize[sizeHelper.xName] = LayoutItem._MIN_SIZE + 2 * SplitTile._MARGIN_SPACE;
            minSize[sizeHelper.yName] = LayoutItem._MIN_SIZE + 2 * SplitTile._MARGIN_SPACE;
            return minSize;
        }

        _updateMinRenderSizeAsChild(maxStarSize: number) {
            this._minRenderSizeAsChild = this._internalSLI.getMinRenderSizeAsChild(maxStarSize);
        }

        _getMinRenderSize(): wijmo.Size {
            let currentSizeHelper = _SplitLayoutItem.getSizeHelper(this._isVertical),
                xSize = 0,
                ySize = 0;
            this._visibleChildren.forEach(vc => {
                let si = <_ISplitLayoutItem>vc;
                xSize += si._minRenderSizeAsChild[currentSizeHelper.xName];
                ySize = Math.max(ySize, si._minRenderSizeAsChild[currentSizeHelper.yName]);
            });

            let size = new wijmo.Size(0, 0);
            size[currentSizeHelper.xName] = xSize;
            size[currentSizeHelper.yName] = ySize;
            return size;
        }

        _getRenderSize(): wijmo.Size {
            return this._internalSLI.renderSize;
        }

        // Updates the group withe specified size.
        updateRenderSize(actualSize: wijmo.Size) {
            this.hostElement.style.width = actualSize.width + 'px';
            this.hostElement.style.height = actualSize.height + 'px';
            this._updateItemsRenderSize(actualSize);
            this._internalSLI.renderSize = actualSize;
        }

        // updates the size of all the items.
        private _updateItemsRenderSize(actualSize: wijmo.Size) {
            let oldRenderSize = this._internalSLI.renderSize,
                currentSizeHelper = _SplitLayoutItem.getSizeHelper(this._isVertical),
                minRenderSize = this._getMinRenderSize();
            if (oldRenderSize) {
                if (!this._visibleChildren.length) {
                    return;
                }

                // resizing the layout item.
                let xIncrease = actualSize[currentSizeHelper.xName] - oldRenderSize[currentSizeHelper.xName],
                    yIncrease = actualSize[currentSizeHelper.yName] - oldRenderSize[currentSizeHelper.yName];
                if (!xIncrease && !yIncrease) {
                    // nothing changed
                    return;
                }

                // if only ySize is changed, update all the children.
                if (yIncrease) {
                    this._visibleChildren.forEach(child => {
                        let splitLI = child as _ISplitLayoutItem,
                            liSize = new wijmo.Size(),
                            oldSize = splitLI._getRenderSize();
                        liSize[currentSizeHelper.yName] = actualSize[currentSizeHelper.yName];
                        liSize[currentSizeHelper.xName] = oldSize[currentSizeHelper.xName];
                        splitLI.updateRenderSize(liSize);
                    });
                    return;
                }

                // Otherwise, if xSize is changed:

                if (oldRenderSize[currentSizeHelper.xName] <= minRenderSize[currentSizeHelper.xName]) {
                    // if the old size is smaller than the min render size;
                    if (actualSize[currentSizeHelper.xName] <= minRenderSize[currentSizeHelper.xName]) {
                        // and if the new size is also smaller,
                        // do nothing to keep the scrollbar.
                        return;
                    }

                    // and if the new size is larger than the min render size,
                    // get the actual increase and adjust the child items which size status is Fill according to the increase.
                    xIncrease = actualSize[currentSizeHelper.xName] - minRenderSize[currentSizeHelper.xName];
                } else if (actualSize[currentSizeHelper.xName] <= minRenderSize[currentSizeHelper.xName]) {
                    // if the old size is larger than the min render size,
                    // and the new size is smaller than the min render size,
                    if (this._arrStarSizeItems.length) {
                        // set the min render size to this group and adjust the child items which size is 'Fill'('*', '2*') if has,
                        // according to the min render size.
                        this._totalStarSize = minRenderSize[currentSizeHelper.xName] - (oldRenderSize[currentSizeHelper.xName] - this._totalStarSize);
                        xIncrease = 0;
                    } else {
                        // otherwise, set the min render size to this group.
                        // calculate the increase from the old render size to the min render size.
                        xIncrease = minRenderSize[currentSizeHelper.xName] - oldRenderSize[currentSizeHelper.xName];
                    }
                }

                // only the children which size status is Fill need to be updated if the group has fill tiles.
                // keep the size of yName.
                if (this._arrStarSizeItems.length) {
                    // adjust the child items which size is 'Fill'('*', '2*') according to the new total star size.
                    this._totalStarSize += xIncrease;
                    this._updateStarItemsSize(oldRenderSize[currentSizeHelper.yName]);
                } else {
                    // adjust the items which size is not 'Fill' according to the increase
                    // except when the old size is larger than the min render size and the new size is smaller.
                    let si: _ISplitLayoutItem = this._arrAutoSizeItems.length ? this._arrAutoSizeItems[this._arrAutoSizeItems.length - 1] : this.children[this.children.length - 1];
                    let siRenderSize = si._getRenderSize();
                    if (siRenderSize == null) {
                        siRenderSize = si._getLayoutSize();
                    }
                    let newRenderSize = new wijmo.Size();
                    newRenderSize[currentSizeHelper.xName] = siRenderSize[currentSizeHelper.xName] + xIncrease;
                    newRenderSize[currentSizeHelper.yName] = siRenderSize[currentSizeHelper.yName];
                    si.updateRenderSize(newRenderSize);
                }

                return;
            }

            let renderSize = new wijmo.Size(
                Math.max(minRenderSize.width, actualSize.width),
                Math.max(minRenderSize.height, actualSize.height)),
                unAssignedSize = renderSize[currentSizeHelper.xName];

            this._visibleChildren.forEach((child, index) => {
                let splitLI = child as _ISplitLayoutItem;
                if (splitLI._getSizeStatus() == _SizeStatus.Fixed) {
                    let size = splitLI._getSize(),
                        liSize = new wijmo.Size();
                    if (index == this._visibleChildren.length - 1
                        && this._arrAutoSizeItems.length == 0
                        && this._arrStarSizeItems.length == 0) {
                        size = unAssignedSize;
                    }

                    liSize[currentSizeHelper.xName] = size;
                    liSize[currentSizeHelper.yName] = renderSize[currentSizeHelper.yName];
                    splitLI.updateRenderSize(liSize);
                    unAssignedSize -= size;
                }
            });

            this._arrAutoSizeItems.forEach((child, index) => {
                let splitLI = child as _ISplitLayoutItem,
                    size = splitLI._minRenderSizeAsChild[currentSizeHelper.xName],
                    liSize = new wijmo.Size();
                if (index == this._arrAutoSizeItems.length - 1
                    && this._arrStarSizeItems.length == 0) {
                    size = unAssignedSize;
                }
                liSize[currentSizeHelper.xName] = size;
                liSize[currentSizeHelper.yName] = renderSize[currentSizeHelper.yName];
                splitLI.updateRenderSize(liSize);
                unAssignedSize -= size;
            });

            this._totalStarSize = unAssignedSize;
            this._updateStarItemsSize(renderSize[currentSizeHelper.yName]);
        }

        // updates the size of the items which size status is Fill.
        private _updateStarItemsSize(ySize: number) {
            let unAssignedSize = this._totalStarSize,
                currentSizeHelper = _SplitLayoutItem.getSizeHelper(this._isVertical);
            this._arrStarSizeItems.forEach((child, index) => {
                let splitLI = child as _ISplitLayoutItem,
                    size = splitLI._getSize(),
                    liSize = new wijmo.Size();
                if (index == this._arrStarSizeItems.length - 1) {
                    size = unAssignedSize;
                } else {
                    let starSize = SplitLayout._parseStarSize(size);
                    size = Math.round(this._totalStarSize * starSize / this._totalStarCount);
                }
                liSize[currentSizeHelper.xName] = size;
                liSize[currentSizeHelper.yName] = ySize;
                splitLI.updateRenderSize(liSize);
                unAssignedSize -= size;
            });
        }

        // modify the size of the tile.
        _increaseSize(delta: number) {
            this._internalSLI.increaseSize(delta);
            this._onLayoutChanged();
        }

        _getAvailableResizingSpace(): number {
            return this._internalSLI.getAvailableResizingSpace();
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            return new SplitTile(tileOpts);
        }

        // creates a group object from the options.
        _createGroup(groupOpts): Group {
            return new SplitGroup(groupOpts);
        }

        _isVisible(): boolean {
            for (let i = 0; i < this.children.length; i++) {
                let isli = <_ISplitLayoutItem>(this.children[i]);
                if (isli._isVisible()) {
                    return true;
                }
            }

            return false;
        }
    }

    // Defines the internal interface for the layoutitem in SplitLayout.
    interface _ISplitLayoutItem {
        _fullRenderSize: wijmo.Size;
        _getMinSize(): wijmo.Size;
        _getRenderSize(): wijmo.Size;
        _getLayoutSize(): wijmo.Size;
        _minRenderSizeAsChild: wijmo.Size;

        _getSizeStatus(): _SizeStatus;

        _getSize(): any;

        _updateMinRenderSizeAsChild(maxStarSize: number);
        updateRenderSize(size: wijmo.Size);
        _updateSizeToFixed();

        _increaseSize(delta: number);
        _getAvailableResizingSpace(): number;

        _isVisible(): boolean;
    }

    interface _ISizeHelper {
        xName: string;
        yName: string;
    }

    class _SplitLayoutItem {
        private readonly _host: any;

        private _size: any;
        private _renderSize: wijmo.Size;

        constructor(host: any) {
            this._host = host;
        }

        get size(): any {
            return this._size;
        }
        set size(value: any) {
            if (value == this._size) {
                return;
            }

            if (wijmo.isNumber(value)) {
                wijmo.assert(value > 0, 'The value should be a positive number!');
                this._size = Math.max(value, LayoutItem._MIN_SIZE);
            } else {
                this._size = value;
            }
        }

        setSize(value: number) {
            let size = value;
            if (this.host instanceof Tile) {
                size -= 2 * SplitTile._MARGIN_SPACE;
            }
            this.size = size;
        }
        getSize(): any {
            let size = this.size;
            if (this.host instanceof Tile && this.sizeStatus == _SizeStatus.Fixed) {
                size += 2 * SplitTile._MARGIN_SPACE;
            }
            return size;
        }

        get renderSize(): wijmo.Size {
            return this._renderSize;
        }
        set renderSize(size: wijmo.Size) {
            this._renderSize = size;
        }

        getLayoutSize(host: HTMLElement): wijmo.Size {

            return new wijmo.Size(host.offsetWidth, host.offsetHeight);
        }

        get sizeStatus(): _SizeStatus {
            return _SplitLayoutItem._getSizeStatus(this.size);
        }

        static _getSizeStatus(size: any): _SizeStatus {
            if (wijmo.isNumber(size)) {
                return _SizeStatus.Fixed;
            }

            let nStar = SplitLayout._parseStarSize(size);
            if (wijmo.isNumber(nStar)) {
                return _SizeStatus.Fill;
            }

            return _SizeStatus.Auto;
        }

        get host(): _ISplitLayoutItem {
            return <_ISplitLayoutItem>this._host;
        }

        get isInVerticalGroup(): boolean {
            if (this._host && this._host.parent) {
                return this._host.parent.orientation == LayoutOrientation.Vertical;
            }
            return true;
        }

        increaseSize(delta: number) {
            if (!this.canIncreaseSize(delta)) {
                return;
            }
            
            if (delta == 0) {
                return;
            }

            let currentSizeHelper = _SplitLayoutItem.getSizeHelper(this.isInVerticalGroup),
                currentSize = this.renderSize[currentSizeHelper.xName],
                newSize = new wijmo.Size();

            this.setSize(currentSize + delta);
            newSize[currentSizeHelper.xName] = this.getSize();
            newSize[currentSizeHelper.yName] = this.renderSize[currentSizeHelper.yName];
            this.host.updateRenderSize(newSize);
            // at this time, the size status of the host item should be Fixed as it is resized.
            this.host._updateMinRenderSizeAsChild(0);
        }
        canIncreaseSize(delta: number): boolean {
            let currentSizeHelper = _SplitLayoutItem.getSizeHelper(this.isInVerticalGroup),
                currentSize = this.renderSize[currentSizeHelper.xName];
            return currentSize + delta >= this._getMinSize();
        }
        getAvailableResizingSpace(): number {
            let currentSizeHelper = _SplitLayoutItem.getSizeHelper(this.isInVerticalGroup),
                currentSize = this.renderSize[currentSizeHelper.xName],
                minSize = this._getMinSize();
            return currentSize - minSize;
        }
        private _getMinSize(): number {
            let minSize = this.host._getMinSize(),
                sizeHelper = _SplitLayoutItem.getSizeHelper(this.isInVerticalGroup);
            return minSize[sizeHelper.xName];
        }

        dispose() {
            this._renderSize = null;
        }

        // Calculates the min render size for the layout item.
        getMinRenderSizeAsChild(starSize: number): wijmo.Size {
            let minSize = this.host._getMinSize(),
                currentSizeHelper = _SplitLayoutItem.getSizeHelper(this.isInVerticalGroup),
                size = new wijmo.Size();

            switch (this.sizeStatus) {
                case _SizeStatus.Fixed:
                    size[currentSizeHelper.xName] = this.getSize();
                    break;
                case _SizeStatus.Auto:
                    size[currentSizeHelper.xName] = this.host._fullRenderSize[currentSizeHelper.xName];
                    break;
                case _SizeStatus.Fill:
                    let star = SplitLayout._parseStarSize(this.size);
                    size[currentSizeHelper.xName] = starSize * star;
                    break;
            }

            size[currentSizeHelper.xName] = Math.max(size[currentSizeHelper.xName], minSize[currentSizeHelper.xName]);
            size[currentSizeHelper.yName] = minSize[currentSizeHelper.yName];
            return size;
        }

        // Update the sizes when the layout item is changed from Fill to Fixed.
        updateSizeToFixed() {
            let currentSizeHelper = _SplitLayoutItem.getSizeHelper(this.isInVerticalGroup);
            this.setSize(this.renderSize[currentSizeHelper.xName]);
        }

        static getSizeHelper(isVertical: boolean): _ISizeHelper {
            let axisX = isVertical ? 'height' : 'width',
                axisY = isVertical ? 'width' : 'height',
                result = {
                    xName: axisX,
                    yName: axisY
                };
            return <_ISizeHelper>result;
        }
    }

    class _DraggingEventArgs extends wijmo.CancelEventArgs {
        distance: number;
    }

    class _DropEventArgs extends wijmo.EventArgs {
        distance: number;
    }

    class _Splitter extends wijmo.Control {
        static _SPLITTER_SIZE: number = 6;
        static _CLASS_UNSELECTABLE = 'wj-dl-unselectable';

        private _orientation: LayoutOrientation = LayoutOrientation.Vertical;
        private _startPosition = 0;
        private _barStartPosition = 0;
        private _splitBar: HTMLElement;
        private _distance: number;
        private _group: SplitGroup;

        constructor(ele: any, options?: any) {
            super(ele);
            wijmo.addClass(this.hostElement, 'wj-splitter vertical vs');
            this.initialize(options);
            this._bindEvents();
        }

        get orientation(): LayoutOrientation {
            return this._orientation;
        }
        set orientation(value: LayoutOrientation) {
            if (value === this._orientation) {
                return;
            }

            wijmo.removeClass(this.hostElement, 'horizontal hs vertical vs');
            this._orientation = value;
            wijmo.addClass(this.hostElement, this._orientation == LayoutOrientation.Vertical ? 'vertical vs' : 'horizontal hs');
        }

        get group(): SplitGroup {
            return this._group;
        }
        set group(value: SplitGroup) {
            this._group = value;
        }

        dragging = new wijmo.Event();
        onDragging(e: _DraggingEventArgs) {
            this.dragging.raise(this, e);
            return !e.cancel;
        }

        drop = new wijmo.Event();
        onDrop(e: _DropEventArgs) {
            this.drop.raise(this, e);
        }

        private _bindEvents() {
            this.addEventListener(this.hostElement, 'mousedown', (e: MouseEvent) => {
                this._dragStart(e);
                e.stopPropagation();
            });
            this.addEventListener(document, 'mousemove', (e: MouseEvent) => {
                this._dragging(e);
                e.stopPropagation();
            });

            this.addEventListener(document, 'mouseup', (e: MouseEvent) => {
                if (this._splitBar) {
                    this._dragEnd();
                    e.stopPropagation();
                }
            });
        }
        private _dragStart(e: MouseEvent) {
            this._startPosition = this._getMousePosition(e);
            this._splitBar = this._createMovingSplitBar();
            this._barStartPosition = this._getBarPosition();
            wijmo.addClass(document.body, _Splitter._CLASS_UNSELECTABLE);
        }
        private _createMovingSplitBar() {
            let splitBar = this.hostElement.cloneNode(true) as HTMLElement,
                splitterRect = this.hostElement.getBoundingClientRect(),
                position = new wijmo.Point(splitterRect.left, splitterRect.top),
                parentElement = document.body;
            if (this.group && this.group.layout) {
                let splitLayout = this.group.layout as SplitLayout;
                position = splitLayout._getRelativePostion(position);
                parentElement = splitLayout._container;
            }
            splitBar.style.left = position.x + 'px';
            splitBar.style.top = position.y + 'px';
            splitBar.style.width = splitterRect.width + 'px';
            splitBar.style.height = splitterRect.height + 'px';
            wijmo.removeClass(splitBar, 'vs hs');
            wijmo.addClass(splitBar, 'moving');
            parentElement.appendChild(splitBar);
            return splitBar;
        }
        private _getBarPosition(): number {
            if (!this._splitBar) {
                return 0;
            }

            return this.orientation == LayoutOrientation.Vertical ? parseFloat(this._splitBar.style.left) : parseFloat(this._splitBar.style.top);
        }
        private _setBarPosition(value: number) {
            if (!this._splitBar) {
                return;
            }

            if (this.orientation == LayoutOrientation.Vertical) {
                this._splitBar.style.left = value + 'px';
            } else {
                this._splitBar.style.top = value + 'px';
            }
        }
        private _dragging(e: MouseEvent) {
            if (!this._splitBar) {
                return;
            }

            let currentPosition = this._getMousePosition(e);
            if (currentPosition == this._startPosition) {
                return;
            }

            this._distance = 0;
            let eDraggingArgs = new _DraggingEventArgs();
            eDraggingArgs.distance = currentPosition - this._startPosition;
            if (this.onDragging(eDraggingArgs)) {
                this._distance = eDraggingArgs.distance;
                if (this._distance) {
                    this._setBarPosition(this._barStartPosition + this._distance);
                }
            }
        }
        private _dragEnd() {
            _removeElement(this._splitBar);
            this._splitBar = null;
            this._startPosition = null;
            wijmo.removeClass(document.body, _Splitter._CLASS_UNSELECTABLE);
            if (this._distance) {
                let eArgs = new _DropEventArgs();
                eArgs.distance = this._distance;
                this.onDrop(eArgs);
                this._distance = 0;
            }
        }
        private _getMousePosition(e: MouseEvent) {
            return this.orientation == LayoutOrientation.Vertical ? e.clientX : e.clientY;
        }

        dispose() {
            this.dragging.removeAllHandlers();
            this.drop.removeAllHandlers();
            this.removeEventListener();
            wijmo.removeClass(this.hostElement, 'wj-splitter vertical vs horizontal hs')
        }
    }

    class _AreaMenu extends wijmo.Control {
        static controlTemplate =
            '<div class="wj-am-container">' +
            '<div class="wj-am-row v">' +
            '<div class="wj-am-cell"></div>' +
            '<div wj-part="top" class="wj-am-cell"></div>' +
            '<div class="wj-am-cell"></div>' +
            '</div>' +
            '<div class="wj-am-row h">' +
            '<div wj-part="left" class="wj-am-cell">' +
            '</div>' +
            '<div wj-part="center" class="wj-am-cell"></div>' +
            '<div wj-part="right" class="wj-am-cell">' +
            '</div>' +
            '</div>' +
            '<div class="wj-am-row v">' +
            '<div class="wj-am-cell"></div>' +
            '<div wj-part="bottom" class="wj-am-cell"></div>' +
            '<div class="wj-am-cell"></div>' +
            '</div>' +
            '</div>';

        private _topWrapper: HTMLElement;
        private _leftWrapper: HTMLElement;
        private _rightWrapper: HTMLElement;
        private _bottomWrapper: HTMLElement;
        private _centerWrapper: HTMLElement;

        private _leftItems: LayoutItem[];
        private _rightItems: LayoutItem[];
        private _topItems: LayoutItem[];
        private _bottomItems: LayoutItem[];
        private _hostTile: Tile;
        private _selectedItem: LayoutItem;
		private _direction: _Direction;
		private _dashboard: DashboardLayout;

        get visible(): boolean {
            return !wijmo.hasClass(this.hostElement, SplitLayout._HIDDEN_CLASS);
        }
        set visible(value: boolean) {
            if (this.visible == !!value) {
                return;
            }

            wijmo.toggleClass(this.hostElement, SplitLayout._HIDDEN_CLASS, !value);
        }

        get selectedItem(): LayoutItem {
            return this._selectedItem;
        }

        get selectedDirection(): _Direction {
            return this._direction;
        }

        itemSelected = new wijmo.Event();
        private _onItemSelected(e?: wijmo.EventArgs) {
            this.itemSelected.raise(this, e);

            this._selectedItem = null;
            this._direction = null;
        }

        itemChanged = new wijmo.Event();
        private _onItemChanged(e?: wijmo.EventArgs) {
            this.itemChanged.raise(this, e);
        }

		constructor(element: any, dashboard?: DashboardLayout) {
			super(element);
			this._dashboard = dashboard;
            let tpl = this.getTemplate();
            this.applyTemplate('wj-area-menu', tpl, {
                _topWrapper: 'top',
                _leftWrapper: 'left',
                _rightWrapper: 'right',
                _bottomWrapper: 'bottom',
                _centerWrapper: 'center'
            });
            this._centerWrapper.appendChild(_createSvgBtn('Center'));
            wijmo.addClass(this.hostElement, SplitLayout._HIDDEN_CLASS);
			this._bindEvents();			
        }
        private _bindEvents() {
            this.addEventListener(document, 'mousemove', (e: MouseEvent) => {
                if (!this.visible) {
                    return;
                }

                let currentSelected = this.selectedItem,
                    currentDirection = this.selectedDirection;
                this.hitTest(new wijmo.Point(e.clientX, e.clientY));
                if (currentSelected != this.selectedItem
                    || currentDirection != this.selectedDirection) {
                    this._onItemChanged();
                }
            });

            this.addEventListener(document, 'mouseup', (e: MouseEvent) => {
                if (!this.visible) return;
                if (this.hitTest(new wijmo.Point(e.clientX, e.clientY))) {
                    this._onItemSelected();
                    e.stopPropagation();
                }
                this.visible = false;
            });
        }

        hitTest(position: wijmo.Point): boolean {
            let hitTestWrapper = (container: HTMLElement, wpos: wijmo.Point) => {
                if (!container || !wpos) {
                    return -1;
                }

                for (let i = 0; i < container.children.length; i++) {
                    let item = container.children[i].querySelector('.rectangle'),
                        clientRect = item.getBoundingClientRect();
                    if (wpos.x < clientRect.left || wpos.x > clientRect.right ||
                        wpos.y < clientRect.top || wpos.y > clientRect.bottom) continue;
                    return i;
                }
                return -1;
            };

            let result = hitTestWrapper(this._leftWrapper, position);
            if (result >= 0) {
                this._selectedItem = this._leftItems[this._leftItems.length - result - 1];
                this._direction = _Direction.Left;
                return true;
            }

            result = hitTestWrapper(this._topWrapper, position);
            if (result >= 0) {
                this._selectedItem = this._topItems[this._topItems.length - result - 1];
                this._direction = _Direction.Top;
                return true;
            }

            result = hitTestWrapper(this._rightWrapper, position);
            if (result >= 0) {
                this._selectedItem = this._rightItems[result];
                this._direction = _Direction.Right;
                return true;
            }

            result = hitTestWrapper(this._bottomWrapper, position);
            if (result >= 0) {
                this._selectedItem = this._bottomItems[result];
                this._direction = _Direction.Bottom;
                return true;
            }
            this._selectedItem = null;
            this._direction = null;
            return false;
        }

        show(tile: SplitTile) {
            this.visible = true;
            this._setHostTile(tile);
        }
        private _setHostTile(tile: Tile) {
            if (this._hostTile === tile) {
                return;
            }

            this._hostTile = tile;
            let items = SplitLayout._getPossibleItems(tile as SplitTile);
            this._leftItems = items.left;
            this._rightItems = items.right;
            this._topItems = items.top;
            this._bottomItems = items.bottom;
            tile.layout.dashboard.container.appendChild(this.hostElement);
            this.invalidate();
        }

        refresh(fullUpdate = true) {
            super.refresh(fullUpdate);
            if (this._hostTile && this.visible) {
                this._updateAreaItems();
                this._updatePosition();
            }
        }
        private _updateAreaItems() {
            this._renderLeftOrTop(this._leftItems, this._leftWrapper, true);
            this._renderLeftOrTop(this._topItems, this._topWrapper, false);
            this._renderRightOrBottom(this._rightItems, this._rightWrapper, true);
            this._renderRightOrBottom(this._bottomItems, this._bottomWrapper, false);
        }
        private _updatePosition() {
            let menuClientRect = this.hostElement.getBoundingClientRect(),
                tileClientRect = this._hostTile.hostElement.getBoundingClientRect(),
                menuLeft = (tileClientRect.width - menuClientRect.width) / 2,
                menuTop = (tileClientRect.height - menuClientRect.height) / 2,
                relativePostion = this._hostTile.layout._getTileRelativePosition(this._hostTile);


            menuLeft += relativePostion.x;
            menuTop += relativePostion.y;
            this.hostElement.style.top = menuTop + 'px';
            this.hostElement.style.left = menuLeft + 'px';
        }
        private _renderLeftOrTop(items: LayoutItem[], container: HTMLElement, isLeft: boolean) {
            container.innerHTML = '';
            if (!items || items.length === 0) return;

            let count = items.length,
                index = count - 1,
                isFirst = true;
            while (index-- >= 0) {
				let btnType = isLeft ? (isFirst ? 'Left' : 'LeftRight') : (isFirst ? 'Top' : 'TopBottom');
				if (this._dashboard && this._dashboard.rightToLeft && isLeft && isFirst) {
					btnType = 'Right';
				}
				isFirst = false;
                container.appendChild(_createSvgBtn(btnType));
            }
        }
        private _renderRightOrBottom(items: LayoutItem[], container: HTMLElement, isRight: boolean) {
            container.innerHTML = '';
            if (!items || items.length === 0) {
                return;
            }

            let count = items.length,
                index = 0;
            while (index++ < count) {
                let isLast = (index == count);
				let btnType = isRight ? (isLast ? 'Right' : 'LeftRight') : (isLast ? 'Bottom' : 'TopBottom');
				if (this._dashboard && this._dashboard.rightToLeft && isRight && isLast) {
					btnType = 'Left';
				}
				container.appendChild(_createSvgBtn(btnType));
            }
        }

        disposeResources(fullDispose = true) {
            if (fullDispose) {
                this.dispose();
            } else {
                this.visible = false;
                this._hostTile = null;
            }
        }
    }

    /**
     * Specified the insert direction.
     */
    enum _Direction {
        Left,
        Top,
        Right,
        Bottom
    }

    /**
     * Specified the size types.
     */
    enum _SizeStatus {
        Fixed = 0,
        Fill = 1,
        Auto = 2
    }

    /**
     * Defines a class represents the root group.
     */
    class _RootGroup extends SplitGroup {
        /**
         * Initializes a new object.
         *
         * @param opts JavaScript object containing initialization data for the group.
         */
        constructor(opts?: any) {
            super(null);
            this.size = '*';
            this.initialize(opts);
        }

        // Overrides get the default value with the actual type.
        _getDefaultItems(): LayoutItemCollection {
            return new _SplitLICollection(this);
        }

        /**
         * Updates the min render size..
         */
        updateMinRenderSize() {
            this._updateMinRenderSizeAsChild(0);
        }

    }

    /**
     * Defines a class represents the children of the root group.
     *
     * A root group has two kinds of children: SplitGroup and SplitTile.
     *
     */
    class _SplitLICollection extends LayoutItemCollection {
        static isValid(item: any): boolean {
            return item != null && (item instanceof SplitGroup || item instanceof SplitTile);
        }

        _isValidType(item: any) {
            return super._isValidType(item)
                && _SplitLICollection.isValid(item);
        }
    }

    /**
     * Defines a class represents the children of a normal group.
     *
     * A normal group has two kinds of children: SplitGroup and SplitTile.
     * When removing its child and the group has no children after removing,
     * the group should be removed. Then check whether the group parent has children.
     *
     */
    class _SplitGroupChildren extends _GroupChildren {
        _isValidType(item: any) {
            return super._isValidType(item)
                && _SplitLICollection.isValid(item);
        }
    }
}