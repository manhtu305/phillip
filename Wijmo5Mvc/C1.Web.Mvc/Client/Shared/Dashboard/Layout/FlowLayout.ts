﻿/**
 * Defines the @see:FlowLayout object and associated classes.
 */
module c1.nav.flow {
    'use strict';

    /**
     * Defines a class represents the flow layout.
     */
    export class FlowLayout extends LayoutBase {
        static _CLASS_LTR: string = 'wj-flow-ltr';
        static _CLASS_RTL: string = 'wj-flow-rtl';
        static _CLASS_TTD: string = 'wj-flow-ttd';
        static _CLASS_BTU: string = 'wj-flow-btu';
        static _CLASS_FLOW: string = 'wj-dl-flow';
        static _SETTING_NAMES: string[] = null;

        private _direction: FlowDirection = FlowDirection.LeftToRight;

        // when dragging or resizing, the dashboard always scroll to the top.
        // use these two members to save the scroll position so that the dashboard scrolls back.
        private _scrollLeft: number;
        private _scrollTop: number;

        private _oldIndex: number;

        /**
         * Initializes a new instance of the @see:FlowLayout class.
         *
         * @param options JavaScript object containing initialization data for the layout.
         */
        constructor(options?: any) {
            super(null);
            this.initialize(options);
        }

        /**
         * Gets or sets the direction the tiles renders in.
         */
        get direction(): FlowDirection {
            return this._direction;
        }
        set direction(value: FlowDirection) {
            value = wijmo.asEnum(value, FlowDirection);
            if (value == this._direction) {
                return;
            }

            this._direction = value;
            this.invalidate();
        }

        // Overrides to get the collection with the actual type.
        _getDefaultItems(): LayoutItemCollection {
            return new _FlowTileCollection(this);
        }

        /**
         * Gets the full type name of the current object.
         */
        get fullTypeName(): string {
            return 'c1.nav.flow.FlowLayout';
        }

        /**
         * Gets the isCreateGroup of the current object.
         */
        get isCreateGroup(): boolean {
            return false;
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (FlowLayout._SETTING_NAMES) {
                return FlowLayout._SETTING_NAMES;
            }

            FlowLayout._SETTING_NAMES = ['direction'].concat(super._getNames());
            return FlowLayout._SETTING_NAMES;
        }

        // Initializes the dom elements, styles and related resources for the layout.
        _initializeComponent() {
            super._initializeComponent();
            wijmo.addClass(this._container, FlowLayout._CLASS_FLOW);
        }

        /**
         * Detaches this layout from the @see:DashboardLayout control.
         *
         * It is often used when applying the new layout, the old layout should be detached.
         *
         */
        detach() {
            if (this._container) {
                wijmo.removeClass(this._container, FlowLayout._CLASS_FLOW);
            }

            super.detach();
        }

        /**
         * Draws the layout.
         */
        draw() {
            switch (this.direction) {
                case FlowDirection.LeftToRight:
                    _addClass(this._container, FlowLayout._CLASS_LTR);
                    break;
                case FlowDirection.RightToLeft:
                    _addClass(this._container, FlowLayout._CLASS_RTL);
                    break;
                case FlowDirection.TopToDown:
                    _addClass(this._container, FlowLayout._CLASS_TTD);
                    break;
                case FlowDirection.BottomToUp:
                    _addClass(this._container, FlowLayout._CLASS_BTU);
            }

            super.draw();

            // restore the scroll position to avoid the scrollbar position reset.
            if (this._scrollLeft != null) {
                this._container.scrollLeft = this._scrollLeft;
                this._scrollLeft = null;
            }
            if (this._scrollTop != null) {
                this._container.scrollTop = this._scrollTop;
                this._scrollTop = null;
            }
        }

        _redraw() {
            this.draw();
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (this.items) {
                this.items.forEach(item => {
                    let layoutItem = item as LayoutItem;
                    layoutItem.dispose(fullDispose);
                });
            }

            if (fullDispose) {
                if (this._container) {
                    wijmo.removeClass(this._container, [FlowLayout._CLASS_LTR, FlowLayout._CLASS_RTL,
                        FlowLayout._CLASS_TTD, FlowLayout._CLASS_BTU].join(' '));
                }
            }

            super.dispose(fullDispose);
        }

        /**
         * Resizes the tile to the specified size.
         *
         * Adds the codes to resize the tile to the specified size by pxWidth and pxHeight.
         *
         * @param resizedTile The tile to be resized.
         * @param newSize The new size.
         * @return A boolean value indicates whether the resize operation succeeds. 
         */
        resizeTo(resizedTile: Tile, newSize: wijmo.Size): boolean {
            let flowTile = resizedTile as FlowTile;
            flowTile.width = newSize.width;
            flowTile.height = newSize.height;
            if (this._container.scrollLeft) {
                this._scrollLeft = this._container.scrollLeft;
            }
            if (this._container.scrollTop) {
                this._scrollTop = this._container.scrollTop;
            }
            resizedTile._updateSize(new wijmo.Size(flowTile.width, flowTile.height));
            return true;
        }

        /**
         * Moves the tile to the specified postion.
         *
         * @param tile The tile to be moved.
         * @param pt The position where the tile is moved to  in relation to the dashboard.
         */
        moveTo(tile: Tile, pt: wijmo.Point) {
            // Todo: Optimize
            // How to find the tile is going to be moved to.
            super.moveTo(tile, pt);
            let minDist = Number.MAX_VALUE,
                minIndex = 0,
                oldIndex = this.items.indexOf(tile),
                oldDist = _getDistance(this._getTileRelativePosition(tile, true), pt);

            this.items._removeInternal(tile);
            for (let tileIndex = this.items.length; tileIndex >= 0; tileIndex--) {
                if (tileIndex == this.items.length) {
                    this._container.appendChild(tile.hostElement);
                } else {
                    let beforeTile: Tile = this.items[tileIndex];
                    this._container.insertBefore(tile.hostElement, beforeTile.hostElement);
                }
                let dist = _getDistance(this._getTileRelativePosition(tile, true), pt);
                if (dist < minDist) {
                    minDist = dist;
                    minIndex = tileIndex;
                }
            }

            if (oldDist == minDist) {
                minIndex = oldIndex;
            }

            if (minIndex == this.items.length) {
                this._container.appendChild(tile.hostElement);
            } else {
                let beforeTile: Tile = this.items[minIndex];
                this._container.insertBefore(tile.hostElement, beforeTile.hostElement);
            }
            this.items._insertAtInternal(tile, minIndex);
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            return new FlowTile(tileOpts);
        }

        // creates a group object from the options.
        _createGroup(groupOpts): Group {
            throw LayoutItemCollection._INVALID_ITEM_TYPE;
        }

        _onTileVisibilityChanged(e: TileEventArgs) {
            let tile = e.tile;
            wijmo.toggleClass(tile.hostElement, 'wj-dl-hidden', !tile.visible);

            super._onTileVisibilityChanged(e);
        }

        /**
         * Starts to move the tile.
         *
         * @param movedTile The tile to be moved.
         * @param startPosition The started position where the tile will be moved from. It is the coordinate within the browser visible area.
         * @return A boolean value indicates whether the tile could be moved.
         */
        startMove(movedTile: Tile, startPosition: wijmo.Point): boolean {
            this._oldIndex = this.items.indexOf(movedTile);
            return super.startMove(movedTile, startPosition);
        }

        _cancelMove(movedTile: Tile) {
            if (!movedTile) {
                return;
            }
            this.items._removeInternal(movedTile);
            let index = this._oldIndex;
            if (index == this.items.length) {
                this._container.appendChild(movedTile.hostElement);
            } else {
                let beforeTile: Tile = this.items[index];
                this._container.insertBefore(movedTile.hostElement, beforeTile.hostElement);
            }
            this.items._insertAtInternal(movedTile, index);
        }

        /**
         * Ends moving the tile at the specified postion.
         *
         * @param movedTile The tile to be moved.
         * @param endPosition The position where the tile is moved to. It is the coordinate within the browser visible area.
         */
        endMove(movedTile: Tile, endPosition: wijmo.Point) {
            super.endMove(movedTile, endPosition);
            this._oldIndex = null;
        }
    }

    /**
     * Specifies the flow direction.
     */
    export enum FlowDirection {
        /** Elements flow from the left edge of the design surface to the right. */
        LeftToRight = 0,
        /** Elements flow from the top of the design surface to the bottom. */
        TopToDown = 1,
        /** Elements flow from the right edge of the design surface to the left. */
        RightToLeft = 2,
        /** Elements flow from the bottom of the design surface to the top. */
        BottomToUp = 3
    }

    /**
     * Defines the tile class for flowlayout. 
     */
    export class FlowTile extends Tile {
        static _SETTING_NAMES: string[] = null;
        static _DEFAULT_FIREFOX_WIDTH = 334;

        private _width: number;
        private _height: number;
        private _allowResize = true;

        /**
         * Initializes a new @see:FlowTile.
         *
         * @param opts JavaScript object containing initialization data for the tile.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
        }

        /**
         * Gets or sets the tile width in pixels.
         * If it is set to null, it means the width will automatically fit the content.
         */
        get width(): number {
            return this._width;
        }
        set width(value: number) {
            if (wijmo.isNumber(value)) {
                this._width = Math.max(value, LayoutItem._MIN_SIZE);
            } else {
                this._width = value;
            }
        }

        /**
         * Gets or sets the tile height in pixels.
         * If it is set to null, it means the height will automatically fit the content.
         */
        get height(): number {
            return this._height;
        }
        set height(value: number) {
            if (wijmo.isNumber(value)) {
                this._height = Math.max(value, LayoutItem._MIN_SIZE);
            } else {
                this._height = value;
            }
        }

        /**
         * Gets or sets a boolean value decides whether the tile could be resized.
         */
        get allowResize(): boolean {
            return this._allowResize;
        }
        set allowResize(value: boolean) {
            value = !!value;
            if (this.allowResize != value) {
                this._allowResize = value;
                this._updateResizable();
            }
        }

        private _updateResizable() {
            if (this.hostElement) {
                wijmo.toggleClass(this.hostElement, 'wj-tile-resizable', this.allowResize);
            }
        }

        _initializeComponent() {
            super._initializeComponent();
            this._updateResizable();
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (FlowTile._SETTING_NAMES) {
                return FlowTile._SETTING_NAMES;
            }

            FlowTile._SETTING_NAMES = ['width', 'height'].concat(super._getNames());
            return FlowTile._SETTING_NAMES;
        }

        /**
         * Draws the tile.
         */
        draw() {
            super.draw();
            let width = this.width, height = this.height;
            if (this.width == null || this.height == null) {
                let bounds = this.hostElement.getBoundingClientRect();
                if (this.width == null) {

                    //TFS 351224
                    if (wijmo.isFirefox()) {
                        width = Math.max(FlowTile._DEFAULT_FIREFOX_WIDTH, LayoutItem._MIN_SIZE);
                    } else {
                        width = Math.max(bounds.width, LayoutItem._MIN_SIZE);
                    }


                }

                if (this.height == null) {
                    height = Math.max(bounds.height, LayoutItem._MIN_SIZE);
                }
            }
            this._updateSize(new wijmo.Size(width, height));
        }

        _appendTo(child: HTMLElement, parent: HTMLElement) {
            let flowLayout = this.layout as FlowLayout,
                items = flowLayout.items,
                index = items.indexOf(this);
            if (index != -1) {
                let count = items.length;
                if (index == count - 1) {
                    super._appendTo(child, parent);
                } else {
                    let i = index + 1;
                    for (; i < count; i++) {
                        let tile: FlowTile = items[i];
                        if (tile.hostElement && parent.contains(tile.hostElement)) {
                            parent.insertBefore(child, tile.hostElement);
                            break;
                        }
                    }

                    if (i == count) {
                        super._appendTo(child, parent);
                    }
                }
                this._refreshContent();
            }
        }
    }

    class _FlowTileCollection extends LayoutItemCollection {
        _isValidType(item: any) {
            return super._isValidType(item) && item instanceof FlowTile;
        }
    }
}