﻿/**
 * Defines the @see:AutoGridLayout object, the @see:ManualGridLayout object and associated classes.
 */
module c1.nav.grid {
    'use strict';

    /**
     * Defines the base class represents the grid layout.
     */
    export class GridLayout extends LayoutBase {
        static _CLASS_LAYOUT = 'wj-dl-grid';
        static _SETTING_NAMES: string[] = null;
        static _MARGIN_SPACE = 3;

        private _cellSize: number = LayoutItem._MIN_SIZE;
        private _cellSpacing: number = 6;
        private _orientation: LayoutOrientation = LayoutOrientation.Horizontal;
        private _maxRowsOrColumns: number = 6;
        private _groupSpacing: number = 10;

        private _eleGroupsContainer: HTMLElement;
        private _refreshStartGroupIndex: number;

        protected _mGroup: GridGroup;
        protected _oldIndex: number;

        _isRemovingMovedTile: boolean = false;
        _moveFromGroupIndex: number;
        private _moveToGroupIndex: number;

        /**
         * Initializes a new instance of the @see:GridLayout class.
         *
         * @param options JavaScript object containing initialization data for the layout.
         */
        constructor(options?: any) {
            super();
            this.initialize(options);
        }

        /**
         * Gets or sets the size of the cell in the grid layout. It is in pixels.
         */
        get cellSize(): number {
            return this._cellSize;
        }
        set cellSize(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive number!');
            let validValue = Math.max(value, LayoutItem._MIN_SIZE);
            if (this.cellSize != value) {
                this._cellSize = validValue;
                this.invalidate();
            }
        }

        /**
         * Gets or sets the spacing of the cell in the grid layout. It is in pixels.
         */
        get cellSpacing(): number {
            return this._cellSpacing;
        }
        set cellSpacing(value: number) {
            wijmo.assert(value >= 0, 'The value should NOT be a negative number!');
            if (this.cellSpacing != value) {
                this._cellSpacing = value;
                this.invalidate();
            }
        }

        /**
         * Gets or sets the spacing of the group in the grid layout. It is in pixels.
         */
        get groupSpacing(): number {
            return this._groupSpacing;
        }
        set groupSpacing(value: number) {
            wijmo.assert(value >= 0, 'The value should NOT be a negative number!');
            if (this.groupSpacing != value) {
                this._groupSpacing = value;
                this.invalidate();
            }
        }

        /**
         * Gets or sets the maximum number of the row count or the column count.
         *
         * If the tiles are laid out vertically, it is used to specify the max count of columns the layout could hold.
         * The tiles shown in the column can not exceed this value.
         * For AutoGridLayout, if there are no enough column spans to hold the next tile, it will be shown in the next row.
         * For ManualGridLayout, if the tile's Column is specified to exceed the max value, it will be shown
         * at the cell with the max column index in the row.
         *
         * If the tiles are laid out horizontally, it is used to specify the max count of rows the layout could hold.
         * For AutoGridLayout, if there are no enough row spans to hold the next tile, it will moved to next column.
         * For ManualGridLayout, if the tile's Row exceeds this value, it will be shown at the cell with the max row index in the column.
         */
        get maxRowsOrColumns(): number {
            return this._maxRowsOrColumns;
        }
        set maxRowsOrColumns(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive interger!');
            if (this.maxRowsOrColumns != value) {
                this._maxRowsOrColumns = value;
                this.invalidate();
            }
        }

        /**
         * Gets or sets the layout orientation.
         */
        get orientation(): LayoutOrientation {
            return this._orientation;
        }
        set orientation(value: LayoutOrientation) {
            value = wijmo.asEnum(value, LayoutOrientation);
            if (this.orientation != value) {
                this._orientation = value;
                this.invalidate();
            }
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (GridLayout._SETTING_NAMES) {
                return GridLayout._SETTING_NAMES;
            }

            GridLayout._SETTING_NAMES = ['cellSize', 'cellSpacing',
                'groupSpacing', 'maxRowsOrColumns', 'orientation'].concat(super._getNames());
            return GridLayout._SETTING_NAMES;
        }

        // Initializes the dom elements, styles and related resources for the layout.
        _initializeComponent() {
            super._initializeComponent();
            wijmo.addClass(this.dashboard.hostElement, GridLayout._CLASS_LAYOUT);
        }

        /**
         * Detaches this layout from the @see:DashboardLayout control.
         *
         * It is often used when applying the new layout, the old layout should be detached.
         *
         */
        detach() {
            // release/remove the resources created in _initializeComponent.
            if (this.dashboard) {
                wijmo.removeClass(this.dashboard.hostElement, GridLayout._CLASS_LAYOUT);
            }

            super.detach();
        }

        /**
         * Draws the layout.
         */
        draw() {
            if (!this._eleGroupsContainer) {
                let gsContainer = document.createElement('div');
                gsContainer.style.position = 'relative';
                this._container.appendChild(gsContainer);
                this._eleGroupsContainer = gsContainer;
            }

            let pLeft = GridLayout._MARGIN_SPACE, pTop = GridLayout._MARGIN_SPACE,
                width = 0, height = 0,
                isVertical = (this.orientation == LayoutOrientation.Vertical);

            this.items.forEach((item, index) => {
                let group = item as GridGroup;
                if (!group._hasVisibleTiles()) {
                    return;
                }

                group.render(this._eleGroupsContainer);
                if (this._refreshStartGroupIndex == null
                    || index >= this._refreshStartGroupIndex) {
                    group._setLocation(pLeft, pTop);
                }

                let groupBounds = group._getBounds();
                if (isVertical) {
                    height = groupBounds.bottom;
                    pTop = groupBounds.bottom + this.groupSpacing;
                    width = groupBounds.right;
                } else {
                    pLeft = groupBounds.right + this.groupSpacing;
                    height = groupBounds.bottom;
                    width = groupBounds.right;
                }
            });

            if (width != 0 && height != 0) {
                if (isVertical) {
                    width += GridLayout._MARGIN_SPACE;
                } else {
                    height += GridLayout._MARGIN_SPACE * 2;
                }
            }
            this._eleGroupsContainer.style.width = width + 'px';
            this._eleGroupsContainer.style.height = height + 'px';
        }

        _redraw() {
            this.draw();
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (this.items) {
                this.items.forEach(item => {
                    let layoutItem = item as LayoutItem;
                    layoutItem.dispose(fullDispose);
                });
            }

            if (fullDispose) {
                if (this._eleGroupsContainer) {
                    _removeElement(this._eleGroupsContainer);
                    this._eleGroupsContainer = null;
                }
            }
            super.dispose(fullDispose);
        }

        /**
         * Refreshes the layout.
         *
         * @param fullUpdate A boolean value decides whether to update the content completely.
         * If true, the layout refreshes without keeping the current status(moving, resizing and etc.).
         * Otherwise, the status is kept after refreshing.
         */
        refresh(fullUpdate = true) {
            super.refresh(fullUpdate);
            this._refreshStartGroupIndex = null;
        }

        /**
         * Resizes the tile to the specified size.
         *
         * Adds the codes to resize the tile to the specified size by pxWidth and pxHeight.
         *
         * @param resizedTile The tile to be resized.
         * @param newSize The new size.
         * @return A boolean value indicates whether the resize operation succeeds. 
         */
        resizeTo(resizedTile: Tile, newSize: wijmo.Size): boolean {
            let rowSpans = Math.ceil(newSize.height / this.cellSize),
                colSpans = Math.ceil(newSize.width / this.cellSize),
                gridTile = resizedTile as GridTile;

            if (gridTile.rowSpan != rowSpans || gridTile.columnSpan != colSpans) {
                gridTile.rowSpan = rowSpans;
                gridTile.columnSpan = colSpans;
                gridTile._updateSize(null);
                let group = resizedTile.parent as GridGroup;
                group._redraw();
                return true;
            }

            return false;
        }

        private _getGroupIndex(layoutItem: LayoutItem): number {
            if (!layoutItem) {
                return -1;
            }

            let liGroup = layoutItem;
            if (layoutItem instanceof Tile) {
                liGroup = layoutItem.parent;
            }
            if (!(liGroup instanceof Group)) {
                return -1;
            }

            return this._getGridGroupIndex(liGroup as GridGroup);
        }

        _getGridGroupIndex(item: GridGroup): number {
            for (let i = 0; i < this.items.length; i++) {
                let group = this.items[i] as GridGroup;
                if (item === group) {
                    return i;
                }
            }

            return -1;
        }

        _getTileIndex(tile: Tile): number {
            if (!tile || !tile.parent) {
                return -1;
            }

            let group = tile.parent as GridGroup;
            for (let i = 0; i < group.children.length; i++) {
                if (group.children[i] == tile) {
                    return i;
                }
            }

            return -1;
        }

        /**
         * Moves the tile to the specified postion.
         *
         * @param tile The tile to be moved.
         * @param pt The position where the tile is moved to  in relation to the dashboard.
         */
        moveTo(tile: Tile, pt: wijmo.Point) {
            this._moveTo(tile, null, pt);
        }

        private _moveTo(movedTile: Tile, movedToGroup: GridGroup, pt: wijmo.Point) {
            let groupIndex = this._getGroupIndex(movedTile),
                tileIndex = this._getTileIndex(movedTile);
            if (groupIndex != -1) {
                this._isRemovingMovedTile = true;
                movedTile._remove(true);
                this._isRemovingMovedTile = false;
            }

            if (pt) {
                movedToGroup = this._findHitGroup(pt);
            }

            if (!movedToGroup) {
                if (pt && groupIndex != -1 && tileIndex != -1) {
                    movedToGroup = this.items[groupIndex];
                    movedToGroup._ensureInclude(movedTile);
                    movedToGroup.children._insertAtInternal(movedTile, tileIndex);
                }
            } else {
                movedToGroup._ensureInclude(movedTile);
                let position;
                if (pt) {
                    let groupBounds = movedToGroup._getBounds();
                    position = new wijmo.Point(pt.x - groupBounds.left, pt.y - groupBounds.top)
                }
                this._moveToGroup(movedTile, movedToGroup, position);
            }

            if (movedToGroup) {
                movedToGroup.draw();
                this._moveToGroupIndex = this._getGroupIndex(movedToGroup);
            }
        }

        _moveToGroup(tile: Tile, group: Group, pt: wijmo.Point) {
        }

        private _findHitGroup(pt: wijmo.Point): GridGroup {
            let count = this.items.length;
            for (let i = 0; i < count; i++) {
                let group = this.items[i] as GridGroup,
                    bounds = group._getBounds();

                if (pt.x >= bounds.left && pt.x < bounds.right
                    && pt.y >= bounds.top && pt.y < bounds.bottom) {
                    return group;
                }
            }

            return null;
        }

        _onTileVisibilityChanged(e: TileEventArgs) {
            let tile = e.tile,
                groupIndex = this._getGroupIndex(tile),
                group: GridGroup = tile.parent;

            if (group._hasVisibleTiles()) {
                groupIndex--;
            }

            if (this._refreshStartGroupIndex == null) {
                this._refreshStartGroupIndex = groupIndex + 1;
            } else {
                this._refreshStartGroupIndex = Math.min(this._refreshStartGroupIndex, groupIndex + 1);
            }
            super._onTileVisibilityChanged(e);
        }

        /**
         * Starts to move the tile.
         *
         * @param movedTile The tile to be moved.
         * @param startPosition The started position where the tile will be moved from. It is the coordinate within the browser visible area.
         * @return A boolean value indicates whether the tile could be moved.
         */
        startMove(movedTile: Tile, startPosition: wijmo.Point): boolean {
            this._mGroup = movedTile.parent as GridGroup;
            this._oldIndex = this._getTileIndex(movedTile);
            this._moveFromGroupIndex = this._getGroupIndex(movedTile);
            return super.startMove(movedTile, startPosition);
        }

        _cancelMove(movedTile: Tile) {
            if (!movedTile || !this._mGroup) {
                return;
            }

            this._moveTo(movedTile, this._mGroup, null);
        }

        /**
         * Ends moving the tile at the specified postion.
         *
         * @param movedTile The tile to be moved.
         * @param endPosition The position where the tile is moved to. It is the coordinate within the browser visible area.
         */
        endMove(movedTile: Tile, endPosition: wijmo.Point) {
            super.endMove(movedTile, endPosition);
            this._mGroup = null;
            this._oldIndex = null;
            if (this._moveToGroupIndex != null) {
                this._refreshStartGroupIndex = Math.min(this._moveFromGroupIndex, this._moveToGroupIndex) + 1; 
                this.refresh(false);
            }
        }
    }

    /**
     * Defines a class represents the auto grid layout.
     */
    export class AutoGridLayout extends GridLayout {
        private _agItems;

        /**
         * Initializes a new instance of the @see:AutoGridLayout class.
         *
         * @param options JavaScript object containing initialization data for the layout.
         */
        constructor(options?: any) {
            super();
            this.initialize(options);
        }

        // Overrides to get the collection with the actual type.
        _getDefaultItems(): LayoutItemCollection {
            return new _AutoGridGroupCollection(this);
        }

        /**
         * Gets the full type name of the current object.
         */
        get fullTypeName(): string {
            return 'c1.nav.grid.AutoGridLayout';
        }

        _moveToGroup(tile: Tile, group: Group, pt: wijmo.Point) {
            let minIndex = 0;
            if (!pt && this._oldIndex != null) {
                // move the tile back to the group.
                minIndex = this._oldIndex;
            } else {
                let minDist = Number.MAX_VALUE,
                    insertedIndex = group.children.length,
                    gridGroup = <GridGroup>group;
                do {
                    group.children._insertAtInternal(tile, insertedIndex);
                    group.draw();
                    let dist = _getDistance(this._getPosition(tile), pt);
                    if (dist < minDist) {
                        minDist = dist;
                        minIndex = insertedIndex;
                    }
                    group.children._removeAtInternal(insertedIndex);
                } while (insertedIndex-- > 0);
            }

            group.children._insertAtInternal(tile, minIndex);
        }
        private _getPosition(tile: Tile): wijmo.Point {
            let tileClientRect = tile.hostElement.getBoundingClientRect(),
                dbClientRect = this._container.getBoundingClientRect();
            return new wijmo.Point(tileClientRect.left - dbClientRect.left,
                tileClientRect.top - dbClientRect.top);
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            return new AutoGridTile(tileOpts);
        }

        // creates a group object from the options.
        _createGroup(groupOpts): Group {
            return new AutoGridGroup(groupOpts);
        }
    }

    /**
     * Defines a class represents the manual grid layout.
     */
    export class ManualGridLayout extends GridLayout {
        private _oldRow: number;
        private _oldColumn: number;

        /**
         * Initializes a new instance of the @see:ManualGridLayout class.
         *
         * @param options JavaScript object containing initialization data for the layout.
         */
        constructor(options?: any) {
            super();
            this.initialize(options);
        }

        // Overrides to get the collection with the actual type.
        _getDefaultItems(): LayoutItemCollection {
            return new _ManualGridGroupCollection(this);
        }

        /**
         * Gets the full type name of the current object.
         */
        get fullTypeName(): string {
            return 'c1.nav.grid.ManualGridLayout';
        }

        _moveToGroup(tile: Tile, group: Group, pt: wijmo.Point) {
            let mgTile = tile as ManualGridTile;
            if (!pt && this._oldIndex != null) {
                // move the tile back to the group.
                mgTile.row = this._oldRow;
                mgTile.column = this._oldColumn;
                group.children._insertAtInternal(tile, this._oldIndex);
            } else {
                let movedToGroup = group as GridGroup,
                    movedToLocation = movedToGroup._getHitLocation(pt);
                if (mgTile.column != movedToLocation.x
                    || mgTile.row != movedToLocation.y
                    || mgTile.parent != movedToGroup) {
                    mgTile.column = movedToLocation.x;
                    mgTile.row = movedToLocation.y;
                    movedToGroup.children._insertAtInternal(mgTile);
                }
            }
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            return new ManualGridTile(tileOpts);
        }

        // creates a group object from the options.
        _createGroup(groupOpts): Group {
            return new ManualGridGroup(groupOpts);
        }

        /**
         * Starts to move the tile.
         *
         * @param movedTile The tile to be moved.
         * @param startPosition The started position where the tile will be moved from. It is the coordinate within the browser visible area.
         * @return A boolean value indicates whether the tile could be moved.
         */
        startMove(movedTile: Tile, startPosition: wijmo.Point): boolean {
            let mgTile = movedTile as ManualGridTile;
            this._oldRow = mgTile.row;
            this._oldColumn = mgTile.column;
            return super.startMove(movedTile, startPosition);
        }
        _canMove(): boolean {
            // when the max count is larger than 1 or there are multiple visible items,
            // the tile could be moved.
            return this.maxRowsOrColumns > 1 || super._canMove();
        }

        /**
         * Ends moving the tile at the specified postion.
         *
         * @param movedTile The tile to be moved.
         * @param endPosition The position where the tile is moved to. It is the coordinate within the browser visible area.
         */
        endMove(movedTile: Tile, endPosition: wijmo.Point) {
            super.endMove(movedTile, endPosition);
            this._oldRow = null;
            this._oldColumn = null;
        }
    }

    /**
     * Defines the base class represents the tile for the grid layout.
     */
    export class GridTile extends Tile {
        static _EXCEPTION_WITHOUT_LAYOUT = 'Cannot render the item without Layout!';
        static _SETTING_NAMES: string[] = null;

        private _rowSpan: number = 0;
        private _columnSpan: number = 0;
        private _allowResize = true;

        /**
         * Initializes a new @see:GridTile.
         *
         * @param opts JavaScript object containing initialization data for the tile.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
        }

        /**
         * Gets or sets the row spans occupied by this tile.
         */
        get rowSpan(): number {
            return this._rowSpan;
        }
        set rowSpan(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive interger!');
            this._rowSpan = value;
        }

        /**
         * Gets or sets the column spans occupied by this tile.
         */
        get columnSpan(): number {
            return this._columnSpan;
        }
        set columnSpan(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive interger!');
            this._columnSpan = value;
        }

        /**
         * Gets or sets a boolean value decides whether the tile could be resized.
         */
        get allowResize(): boolean {
            return this._allowResize;
        }
        set allowResize(value: boolean) {
            value = !!value;
            if (this.allowResize != value) {
                this._allowResize = value;
                this._updateResizable();
            }
        }

        private _updateResizable() {
            if (this.hostElement) {
                wijmo.toggleClass(this.hostElement, 'wj-tile-resizable', this.allowResize);
            }
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (GridTile._SETTING_NAMES) {
                return GridTile._SETTING_NAMES;
            }

            GridTile._SETTING_NAMES = ['rowSpan', 'columnSpan'].concat(super._getNames());
            return GridTile._SETTING_NAMES;
        }

        _getValidLayout(): GridLayout {
            if (!this.layout) {
                throw GridTile._EXCEPTION_WITHOUT_LAYOUT;
            }

            return this.layout as GridLayout;
        }

        _getSpans(): number {
            return this._getValidLayout().orientation == LayoutOrientation.Horizontal ? this.rowSpan : this.columnSpan;
        }

        _getCells(): number {
            return this._getValidLayout().orientation == LayoutOrientation.Horizontal ? this.columnSpan : this.rowSpan;
        }

        // the start row index(which starts from 1).
        _getStartRow(): number {
            return 1;
        }

        // the start column index(which starts from 1).
        _getStartColumn(): number {
            return 1;
        }

        /**
         * Draws the tile.
         */
        draw() {
            if (this.rowSpan == 0 || this.columnSpan == 0) {
                return;
            }
            super.draw();
            // update size
            this._updateSize(null);
        }

        _updateSize(size: wijmo.Size) {
            this._validateSettings();
            let layout = this._getValidLayout();
            let pWidth = this.columnSpan * layout.cellSize + (this.columnSpan - 1) * layout.cellSpacing,
                pHeight = this.rowSpan * layout.cellSize + (this.rowSpan - 1) * layout.cellSpacing;
            super._updateSize(new wijmo.Size(pWidth, pHeight));
        }

        _updateLoaction() {
            // update localtion
            let layout = this._getValidLayout(),
                left = (this._getStartColumn() - 1) * (layout.cellSize + layout.cellSpacing),
                top = (this._getStartRow() - 1) * (layout.cellSize + layout.cellSpacing);
            this.hostElement.style.left = left + 'px';
            this.hostElement.style.top = top + 'px';
        }

        // validate the tile settings.
        _validateSettings() {
            let layout = this._getValidLayout();
            switch (layout.orientation) {
                case LayoutOrientation.Vertical:
                    this._columnSpan = Math.min(this._columnSpan, layout.maxRowsOrColumns);
                    break;
                case LayoutOrientation.Horizontal:
                    this._rowSpan = Math.min(this._rowSpan, layout.maxRowsOrColumns);
                    break;
            }
        }

        _initializeComponent() {
            super._initializeComponent();
            this._updateResizable();
        }

        _appendTo(child: HTMLElement, parent: HTMLElement) {
            super._appendTo(child, parent);
            this._refreshContent();
        }
    }

    /**
     * Defines the class represents the tile for the manual grid layout.
     */
    export class ManualGridTile extends GridTile {
        static _SETTING_NAMES: string[] = null;

        private _column: number = 1;
        private _row: number = 1;
        private _mRowSpan: number;
        private _mColumnSpan: number;

        /**
         * Initializes a new @see:ManualGridTile.
         *
         * @param opts JavaScript object containing initialization data for the tile.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
        }

        /**
         * Gets or sets the start column index of tile.
         * The column index starts from 1.
         */
        get column(): number {
            return this._column;
        }
        set column(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive interger!');
            this._column = value;
        }

        /**
         * Gets or sets the start row index of tile.
         * The row index starts from 1.
         */
        get row(): number {
            return this._row;
        }
        set row(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive interger!');
            this._row = value;
        }

        /**
         * Gets or sets the row spans occupied by this tile.
         */
        get rowSpan(): number {
            return this._mRowSpan;
        }
        set rowSpan(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive interger!');
            this._mRowSpan = value;
        }

        /**
         * Gets or sets the row spans occupied by this tile.
         */
        get columnSpan(): number {
            return this._mColumnSpan;
        }
        set columnSpan(value: number) {
            wijmo.assert(value > 0, 'The value should be a positive interger!');
            this._mColumnSpan = value;
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (ManualGridTile._SETTING_NAMES) {
                return ManualGridTile._SETTING_NAMES;
            }

            ManualGridTile._SETTING_NAMES = ['row', 'column'].concat(super._getNames());
            return ManualGridTile._SETTING_NAMES;
        }

        _getStartRow(): number {
            return this.row;
        }

        _getStartColumn(): number {
            return this.column;
        }

        _validateSettings() {
            super._validateSettings();

            let layout = this._getValidLayout();
            switch (layout.orientation) {
                case LayoutOrientation.Vertical:
                    this._column = Math.min(this._column, layout.maxRowsOrColumns);
                    this._mColumnSpan = Math.min(this._mColumnSpan, layout.maxRowsOrColumns - this._column + 1);
                    break;
                case LayoutOrientation.Horizontal:
                    this._row = Math.min(this._row, layout.maxRowsOrColumns);
                    this._mRowSpan = Math.min(this._mRowSpan, layout.maxRowsOrColumns - this._row + 1);
                    break;
            }
        }
    }

    /**
     * Defines the class represents the tile for the auto grid layout.
     */
    export class AutoGridTile extends GridTile {
        private _startRow: number;
        private _startColumn: number;

        _getStartRow(): number {
            return this._startRow;
        }

        _getStartColumn(): number {
            return this._startColumn;
        }

        // the parameters start from 0, 
        // _startColumn/ _startRow start from 1,
        // so plus 1 before setting.
        _setLocation(startSpan: number, startCell: number) {
            let orientation = this._getValidLayout().orientation;
            startSpan++;
            startCell++;
            switch (orientation) {
                case LayoutOrientation.Vertical:
                    this._startColumn = startSpan;
                    this._startRow = startCell;
                    break;
                case LayoutOrientation.Horizontal:
                    this._startColumn = startCell;
                    this._startRow = startSpan;
                    break;
            }

            this._updateLoaction();
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (fullDispose) {
                this._startColumn = null;
                this._startRow = null;
            }
            super.dispose(fullDispose);
        }
    }

    /**
     * Defines the base class represents the group for the grid layout.
     */
    export class GridGroup extends Group {
        private _rowSpan: number;
        private _columnSpan: number;
        private _bounds = new wijmo.Rect(0, 0, 0, 0);

        /**
         * Initializes a new @see:GridGroup.
         *
         * @param opts JavaScript object containing initialization data for the group.
         */
        constructor(opts?: any) {
            super();
            this.initialize(opts);
        }

        // get valiable layout object.
        // If the layout is null, throw an exception.
        // It occurs when rendering without layout.
        _getValidLayout(): GridLayout {
            if (!this.layout) {
                throw GridTile._EXCEPTION_WITHOUT_LAYOUT;
            }

            return this.layout as GridLayout;
        }

        // Gets the bound size.
        _getBounds(): wijmo.Rect {
            return this._bounds;
        }

        private _getRowSpan(): number {
            let layout = this._getValidLayout();
            return layout.orientation === LayoutOrientation.Horizontal ? layout.maxRowsOrColumns
                : this._rowSpan;
        }
        private _getColumnSpan(): number {
            let layout = this._getValidLayout();
            return layout.orientation === LayoutOrientation.Vertical ? layout.maxRowsOrColumns
                : this._columnSpan;
        }

        /**
         * Draws the group.
         */
        draw() {
        }

        _redraw() {
            let gridLayout = <GridLayout>this.layout;
            if (gridLayout == null ||
                    (gridLayout._isRemovingMovedTile && this == gridLayout.items[gridLayout._moveFromGroupIndex]
                        && !this._hasVisibleTiles())) {
                return;
            }

            if (!this._hasVisibleTiles()) {
                _removeElement(this.hostElement);
            }

            super._redraw();
        }

        // the parameters' unit is pixed.
        // which stand for the start position and the start left of the group.
        _setLocation(pLeft: number, pTop: number) {
            this._bounds.left = pLeft;
            this._bounds.top = pTop;

            // update localtion
            this.hostElement.style.left = pLeft + 'px';
            this.hostElement.style.top = pTop + 'px';
        }

        _setSize(occupiedCells: number) {
            let layout = this._getValidLayout();
            switch (layout.orientation) {
                case LayoutOrientation.Vertical:
                    this._rowSpan = occupiedCells;
                    break;
                case LayoutOrientation.Horizontal:
                    this._columnSpan = occupiedCells;
                    break;
            }

            let wSpanCount = this._getColumnSpan(),
                hSpanCount = this._getRowSpan(),
                pWidth = wSpanCount * layout.cellSize + (wSpanCount - 1) * layout.cellSpacing,
                pHeight = hSpanCount * layout.cellSize + (hSpanCount - 1) * layout.cellSpacing;
            this.hostElement.style.width = pWidth + 'px';
            this.hostElement.style.height = pHeight + 'px';
            this._bounds.width = pWidth;
            this._bounds.height = pHeight;
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (fullDispose) {
                this._bounds = new wijmo.Rect(0, 0, 0, 0);
                this._rowSpan = null;
                this._columnSpan = null;
            }
            super.dispose(fullDispose);
        }

        _getHitLocation(pt: wijmo.Point): wijmo.Point {
            let layout = this.layout as GridLayout,
                left = Math.max(1, pt.x),
                top = Math.max(1, pt.y),
                nSingleCellSize = layout.cellSize + layout.cellSpacing;

            return new wijmo.Point(Math.ceil(left / nSingleCellSize),
                Math.ceil(top / nSingleCellSize));
        }

        // creates a group object from the options.
        _createGroup(groupOpts): Group {
            throw LayoutItemCollection._INVALID_ITEM_TYPE;
        }

        _ensureInclude(tile: Tile) {
            if (tile.hostElement && tile.hostElement.parentElement != this._contentElement) {
                this._contentElement.appendChild(tile.hostElement);
            }
        }

        _hasVisibleTiles(): boolean {
            for (let i = 0; i < this.children.length; i++) {
                let tile = this.children[i] as Tile;
                if (tile.visible) {
                    return true;
                }
            }

            return false;
        }

        _onTileVisibilityChanged(e: TileEventArgs) {
            let tile = e.tile;
            if (tile.visible) {
                this._ensureInclude(tile);
            } else {
                _removeElement(tile.hostElement);
            }
            if (this.hostElement) {
                this._redraw();
            }
            super._onTileVisibilityChanged(e);
        }

        // overrides to append the group element to the layout.
        _updatePartialView(container?: HTMLElement) {
            if (container && this.hostElement.parentElement != container) {
                container.appendChild(this.hostElement);
            }
        }
    }

    /**
     * Defines the class represents the group for the auto grid layout.
     */
    export class AutoGridGroup extends GridGroup {

        /**
         * Initializes a new @see:AutoGridGroup.
         *
         * @param opts JavaScript object containing initialization data for the group.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
        }

        // Overrides get the default value with the actual type.
        _getDefaultItems(): LayoutItemCollection {
            return new _AutoGridTileCollection(this);
        }

        /**
         * Draws the group.
         */
        draw() {
            let arrOccupyCells: number[] = [],
                maxOccupyCells: number = 0,
                layout = this._getValidLayout();

            this.children.forEach(child => {
                let gridTile = child as AutoGridTile;
                if (!gridTile.visible) {
                    return;
                }

                let tileSpans = gridTile._getSpans(),
                    leftSpans = layout.maxRowsOrColumns - tileSpans,
                    tileStartCell = maxOccupyCells,
                    tileStartSpan = 0;

                // try to find the position where tileStartSpan is the max and tileStartCell is the min.
                for (let i = 0; i <= leftSpans; i++) {
                    let occupyCells = arrOccupyCells[i] || 0;
                    // if there is space, check whether it could hold this tile.
                    if (occupyCells < tileStartCell) {
                        let foundSpace = true;
                        for (let j = 1; j < tileSpans; j++) {
                            if (arrOccupyCells[j + i] > occupyCells) {
                                foundSpace = false;
                                break;
                            }
                        }

                        if (foundSpace) {
                            tileStartCell = occupyCells;
                            tileStartSpan = i;
                        }
                    }
                }

                // update the tile location.
                let tileStartRow = tileStartSpan,
                    tileStartCol = tileStartCell;

                // update the occupied space.
                tileStartCell += gridTile._getCells();
                let endSpan = Math.min(layout.maxRowsOrColumns, tileStartSpan + tileSpans);
                for (let i = tileStartSpan; i < endSpan; i++) {
                    arrOccupyCells[i] = tileStartCell;
                }
                maxOccupyCells = Math.max(maxOccupyCells, tileStartCell);

                // render the tile
                gridTile.render(this._contentElement);
                gridTile._setLocation(tileStartRow, tileStartCol);
            });

            this._setSize(maxOccupyCells);
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            return new AutoGridTile(tileOpts);
        }
    }

    /**
     * Defines the class represents the group for the manual grid layout.
     */
    export class ManualGridGroup extends GridGroup {
        /**
         * Initializes a new @see:ManualGridGroup.
         *
         * @param opts JavaScript object containing initialization data for the group.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
        }

        // Overrides get the default value with the actual type.
        _getDefaultItems(): LayoutItemCollection {
            return new _ManualGridTileCollection(this);
        }

        /**
         * Draws the group.
         */
        draw() {
            let maxOccupyCells: number = 0,
                orientation = this._getValidLayout().orientation;
            this.children.forEach(child => {
                let li = child as ManualGridTile;
                if (!li.visible) {
                    return;
                }

                switch (orientation) {
                    case LayoutOrientation.Horizontal:
                        maxOccupyCells = Math.max(maxOccupyCells, li.column + li.columnSpan - 1);
                        break;
                    case LayoutOrientation.Vertical:
                        maxOccupyCells = Math.max(maxOccupyCells, li.row + li.rowSpan - 1);
                        break;
                }

                li.render(this._contentElement);
                li._updateLoaction();
            });

            this._setSize(maxOccupyCells);
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            return new ManualGridTile(tileOpts);
        }
    }

    class _GridGroupChildren extends _GroupChildren {
        // Resizes the group which has no child..
        _removeGroup(group: Group) {
            group.hostElement.style.width = '0px';
            group.hostElement.style.height = '0px';
        }
    }

    // Defines the collection which only accepts the item which type is AutoGridTile.
    class _AutoGridTileCollection extends _GridGroupChildren {
        _isValidType(item: any) {
            return super._isValidType(item) && item instanceof AutoGridTile;
        }
    }

    // Defines the collection which only accepts the item which type is AutoGridGroup.
    class _AutoGridGroupCollection extends LayoutItemCollection {
        _isValidType(item: any) {
            return super._isValidType(item) && item instanceof AutoGridGroup;
        }
    }

    class _ManualGridTileCollection extends _GridGroupChildren {
        _isValidType(item: any) {
            return super._isValidType(item) && item instanceof ManualGridTile;
        }
    }

    class _ManualGridGroupCollection extends LayoutItemCollection {
        _isValidType(item: any) {
            return super._isValidType(item) && item instanceof ManualGridGroup;
        }
    }
}