module c1.nav {
    'use strict';
    const _SVG_END = '</svg>';
    const _ICONS = {
        Move:
            '<g>' +
                '<rect x="11" y= "4" width="1" height="6" />' +
                '<rect x="10" y= "5" width="3" height="1" />' +
                '<rect x="9" y= "6" width="5" height="1" />' +
            '</g>' +
            '<g>' +
                '<rect x="11" y= "13" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 23 32)" width="1" height="6" />' +
                '<rect x="10" y= "17" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 23 35)" width="3" height="1" />' +
                '<rect x="9" y= "16" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 23 33)" width="5" height="1" />' +
            '</g>' +
            '<g>' +
                '<rect x="15.5" y= "8.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 27.5 -4.5)" width="1" height="6" />' +
                '<rect x="16" y= "11" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 29 -6)" width="3" height="1" />' +
                '<rect x="14" y= "11" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 28 -5)" width="5" height="1" />' +
            '</g>' +
            '<g>' +
                '<rect x="6.5" y= "8.5" transform="matrix(6.123234e-17 -1 1 6.123234e-17 -4.5 18.5)" width="1" height="6" />' +
                '<rect x="4" y= "11" transform="matrix(6.123234e-17 -1 1 6.123234e-17 -6 17)" width="3" height="1" />' +
                '<rect x="4" y= "11" transform="matrix(6.123234e-17 -1 1 6.123234e-17 -5 18)" width="5" height="1" />' +
            '</g>',
        More:
            '<rect x="10" y= "4" width="2" height="2" />' +
            '<rect x="10" y= "4" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 16 -6)" width="2" height="2" />' +
            '<rect x="10" y= "10" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 22 1.776357e-15)" width="2" height="2" />' +
            '<rect x="10" y= "16" width="2" height="2" />' +
            '<rect x="9" y= "3" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="12" y= "3" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="9" y= "6" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="12" y= "6" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="9" y= "9" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="12" y= "9" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="9" y= "12" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="12" y= "12" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="9" y= "15" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="12" y= "15" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="9" y= "18" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="12" y= "18" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="10" y= "3" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="10" y= "6" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="11.5" y= "4.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 17.5 -7.5)" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="8.5" y= "4.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 14.5 -4.5)" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="10" y= "9" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="10" y= "12" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="11.5" y= "10.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 23.5 -1.5)" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="8.5" y= "10.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 20.5 1.5)" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="10" y= "15" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="10" y= "18" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="11.5" y= "16.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 29.5 4.5)" class="wj-db-opacity08" width="2" height="1" />' +
            '<rect x="8.5" y= "16.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 26.5 7.5)" class="wj-db-opacity08" width="2" height="1" />',
        Expand:
            '<rect x="3" y= "10" width="1" height="9"/>' +
            '<rect x="7" y= "14" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 26 11)" width="1" height="9"/>' +
            '<rect x="12" y= "13" width="1" height="6"/>' +
            '<rect x="5.5" y= "6.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 15.5 3.5)" width="1" height="6"/>' +
            '<rect x="7" y= "14" width="1" height="1"/>' +
            '<rect x="8" y= "13" width="1" height="1"/>' +
            '<rect x="9" y= "12" width="1" height="1"/>' +
            '<rect x="10" y= "11" width="1" height="1"/>' +
            '<rect x="10" y= "11" width="1" height="1"/>' +
            '<rect x="11" y= "10" width="1" height="1"/>' +
            '<rect x="12" y= "9" width="1" height="1"/>' +
            '<rect x="13" y= "8" width="1" height="1"/>' +
            '<rect x="14" y= "7" class="wj-db-opacity08" width="1" height="1"/>' +
            '<rect x="3" y= "3" width="1" height="3"/>' +
            '<rect x="4" y= "2" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 8 -1)" width="1" height="3"/>' +
            '<rect x="17" y= "2" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 21 -14)" width="1" height="3"/>' +
            '<rect x="18" y= "3" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 37 9)" width="1" height="3"/>' +
            '<rect x="13" y= "4" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 20 -7)" width="1" height="5"/>' +
            '<rect x="15" y= "6" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 31 17)" width="1" height="5"/>' +
            '<rect x="18" y= "16" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 37 35)" width="1" height="3"/>' +
            '<rect x="17" y= "17" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 36 1)" width="1" height="3"/>' +
            '<rect x="3" y= "7" width="1" height="1"/>' +
            '<rect x="18" y= "7" width="1" height="2"/>' +
            '<rect x="18" y= "10" width="1" height="1"/>' +
            '<rect x="18" y= "11" width="1" height="1"/>' +
            '<rect x="18" y= "13" width="1" height="2"/>' +
            '<rect x="7" y= "3" transform="matrix(6.123234e-17 -1 1 6.123234e-17 4 11)" width="1" height="1"/>' +
            '<rect x="8" y= "3" transform="matrix(6.123234e-17 -1 1 6.123234e-17 5 12)" width="1" height="1"/>' +
            '<rect x="10.5" y= "2.5" transform="matrix(6.123234e-17 -1 1 6.123234e-17 7.5 14.5)" width="1" height="2"/>' +
            '<rect x="13.5" y= "2.5" transform="matrix(6.123234e-17 -1 1 6.123234e-17 10.5 17.5)" width="1" height="2" />' +
            '<rect x="14" y= "18" transform="matrix(6.123234e-17 -1 1 6.123234e-17 -4 33)" width="1" height="1" />',
        Collapse:
            '<rect x="10" y= "18" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 29 8)" width="1" height="1" />' +
            '<rect x="9" y= "18" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 28 9)" width="1" height="1" />' +
            '<rect x="6.5" y= "17.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 25.5 11.5)" width="1" height="2" />' +
            '<rect x="4" y= "18" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 23 14)" width="1" height="1" />' +
            '<rect x="3" y= "18" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 22 15)" width="1" height="1" />' +
            '<rect x="3" y= "17" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 21 14)" width="1" height="1" />' +
            '<rect x="2.5" y= "14.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 18.5 11.5)" width="2" height="1" />' +
            '<rect x="2.5" y= "11.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 15.5 8.5)" width="2" height="1" />' +
            '<rect x="4" y= "11" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 16 7)" width="1" height="1" />' +
            '<rect x="6.5" y= "10.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 18.5 4.5)" width="1" height="2" />' +
            '<rect x="9" y= "11" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 21 2)" width="1" height="1" />' +
            '<rect x="10" y= "11" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 22 1)" width="1" height="1" />' +
            '<rect x="10" y= "12" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 23 2)" width="1" height="1" />' +
            '<rect x="9.5" y= "14.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 25.5 4.5)" width="2" height="1" />' +
            '<rect x="10" y= "17" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 28 7)" width="1" height="1" />' +
            '<rect x="3" y= "3" width="1" height="7" />' +
            '<rect x="10" y= "-4" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 14 -7)" width="1" height="15" />' +
            '<rect x="18" y= "3" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 37 19)" width="1" height="13" />' +
            '<rect x="13" y= "8" class="wj-db-opacity08" width="1" height="1" />' +
            '<rect x="14" y= "7" width="1" height="1" />' +
            '<rect x="15" y= "6" width="1" height="1" />' +
            '<rect x="16" y= "5" width="1" height="1" />' +
            '<rect x="13.5" y= "7.5" transform="matrix(6.123234e-17 -1 1 6.123234e-17 4.5 23.5)" width="1" height="4" />' +
            '<rect x="12" y= "6" width="1" height="4" />' +
            '<rect x="18" y= "16" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 37 35)" width="1" height="3" />' +
            '<rect x="15" y= "15" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 34 3)" width="1" height="7" />',
        Hide:
            '<rect x="5" y= "5" width="1" height="3" />' +
            '<rect x="17" y= "5" width="1" height="3" />' +
            '<rect x="11" y= "1" transform="matrix(6.123234e-17 -1 1 6.123234e-17 4 19)" width="1" height="13" />' +
            '<rect x="11" y= "-1" transform="matrix(6.123234e-17 -1 1 6.123234e-17 6 17)" width="1" height="13" />' +
            '<rect x="6" y= "9" width="1" height="9" />' +
            '<rect x="16" y= "9" width="1" height="9" />' +
            '<rect x="9" y= "4" width="1" height="1" />' +
            '<rect x="13" y= "4" width="1" height="1" />' +
            '<rect x="10" y= "3" width="3" height="1" />' +
            '<rect x="8" y= "9" width="1" height="8" />' +
            '<rect x="10" y= "9" width="1" height="8" />' +
            '<rect x="12" y= "9" width="1" height="8" />' +
            '<rect x="14" y= "9" width="1" height="8" />' +
            '<rect x="6" y= "18" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="16" y= "18" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="9" y= "3" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="13" y= "3" class="wj-db-opacity02" width="1" height="1" />' +
            '<rect x="7" y= "18" width="9" height="1" />',
        Resize:
            '<rect x="0" y= "8" width="1" height="1" />' +
            '<rect x="1" y= "7" width="1" height="1" />' +
            '<rect x="2" y= "6" width="1" height="1" />' +
            '<rect x="3" y= "5" width="1" height="1" />' +
            '<rect x="4" y= "4" width="1" height="1" />' +
            '<rect x="5" y= "3" width="1" height="1" />' +
            '<rect x="6" y= "2" width="1" height="1" />' +
            '<rect x="7" y= "1" width="1" height="1" />' +
            '<rect x="2" y= "8" width="1" height="1" />' +
            '<rect x="3" y= "7" width="1" height="1" />' +
            '<rect x="4" y= "6" width="1" height="1" />' +
            '<rect x="5" y= "5" width="1" height="1" />' +
            '<rect x="6" y= "4" width="1" height="1" />' +
            '<rect x="7" y= "3" width="1" height="1" />' +
            '<rect x="4" y= "8" width="1" height="1" />' +
            '<rect x="5" y= "7" width="1" height="1" />' +
            '<rect x="6" y= "6" width="1" height="1" />' +
            '<rect x="7" y= "5" width="1" height="1" />' +
            '<rect x="6" y= "8" width="1" height="1" />' +
            '<rect x="7" y= "7" width="1" height="1" />' +
            '<rect x="8" y= "8" width="1" height="1" />' +
            '<rect x="8" y= "6" width="1" height="1" />' +
            '<rect x="8" y= "4" width="1" height="1" />' +
            '<rect x="8" y= "2" width="1" height="1" />' +
            '<g>' +
                '<rect x="1" y= "8" width="1" height="1" />' +
                '<rect x="2" y= "7" width="1" height="1" />' +
                '<rect x="3" y= "6" width="1" height="1" />' +
                '<rect x="4" y= "5" width="1" height="1" />' +
                '<rect x="5" y= "4" width="1" height="1" />' +
                '<rect x="6" y= "3" width="1" height="1" />' +
                '<rect x="7" y= "2" width="1" height="1" />' +
                '<rect x="3" y= "8" width="1" height="1" />' +
                '<rect x="4" y= "7" width="1" height="1" />' +
                '<rect x="5" y= "6" width="1" height="1" />' +
                '<rect x="6" y= "5" width="1" height="1" />' +
                '<rect x="7" y= "4" width="1" height="1" />' +
                '<rect x="5" y= "8" width="1" height="1" />' +
                '<rect x="6" y= "7" width="1" height="1" />' +
                '<rect x="7" y= "6" width="1" height="1" />' +
                '<rect x="7" y= "8" width="1" height="1" />' +
                '<rect x="8" y= "7" width="1" height="1" />' +
                '<rect x="8" y= "5" width="1" height="1" />' +
                '<rect x="8" y= "3" width="1" height="1" />' +
                '<rect x="8" y= "1" width="1" height="1" />' +
            '</g>',
        ResizeLeft:
            '<rect x="1" y = "1" width="1" height="1" />' +
            '<rect x="2" y = "2" width="1" height="1" />' +
            '<rect x="3" y = "3" width="1" height="1" />' +
            '<rect x="4" y = "4" width="1" height="1" />' +
            '<rect x="5" y = "5" width="1" height="1" />' +
            '<rect x="6" y = "6" width="1" height="1" />' +
            '<rect x="7" y = "7" width="1" height="1" />' +
            '<rect x="8" y = "8" width="1" height="1" />' +
            '<rect x="1" y = "3" width="1" height="1" />' +
            '<rect x="2" y = "4" width="1" height="1" />' +
            '<rect x="3" y = "5" width="1" height="1" />' +
            '<rect x="4" y = "6" width="1" height="1" />' +
            '<rect x="5" y = "7" width="1" height="1" />' +
            '<rect x="6" y = "8" width="1" height="1" />' +
            '<rect x="1" y = "5" width="1" height="1" />' +
            '<rect x="2" y = "6" width="1" height="1" />' +
            '<rect x="3" y = "7" width="1" height="1" />' +
            '<rect x="4" y = "8" width="1" height="1" />' +
            '<rect x="1" y = "7" width="1" height="1" />' +
            '<rect x="2" y = "8" width="1" height="1" />' +
            '<g>' +
                '<rect x="1" y = "2" width="1" height="1" />' +
                '<rect x="2" y = "3" width="1" height="1" />' +
                '<rect x="3" y = "4" width="1" height="1" />' +
                '<rect x="4" y = "5" width="1" height="1" />' +
                '<rect x="5" y = "6" width="1" height="1" />' +
                '<rect x="6" y = "7" width="1" height="1" />' +
                '<rect x="7" y = "8" width="1" height="1" />' +
                '<rect x="1" y = "4" width="1" height="1" />' +
                '<rect x="2" y = "5" width="1" height="1" />' +
                '<rect x="3" y = "6" width="1" height="1" />' +
                '<rect x="4" y = "7" width="1" height="1" />' +
                '<rect x="5" y = "8" width="1" height="1" />' +
                '<rect x="1" y = "6" width="1" height="1" />' +
                '<rect x="2" y = "7" width="1" height="1" />' +
                '<rect x="3" y = "8" width="1" height="1" />' +
                '<rect x="1" y = "8" width="1" height="1" />' +
            '</g>',
        MoveIndicator:
            '<g class="wj-svg-btn-info">' +
                '<rect x="10" y= "2" width="1" height="5"/>' +
                '<rect x="12" y= "2" width="1" height="5"/>' +
                '<rect x="14" y= "2" width="1" height="5"/>' +
                '<rect x="16" y= "2" width="1" height="5"/>' +
                '<rect x="18" y= "2" width="1" height="5"/>' +
                '<rect x="20" y= "2" width="1" height="5"/>' +
                '<rect x="22" y= "2" width="1" height="5"/>' +
                '<rect x="24" y= "2" width="1" height="5"/>' +
                '<rect x="26" y= "2" width="1" height="5"/>' +
                '<rect x="28" y= "2" width="1" height="5"/>' +
                '<rect x="30" y= "2" width="1" height="5"/>' +
            '</g>',
        Left:
            '<polygon class="border" points="18,29 6,29 6,9 18,9 18,8 5,8 5,30 18,30"/>' +
            '<rect x="6" y="9" class="gap" width="12" height="20"/>' +
            '<rect x="8.5" y="11.5" class="rectangle" width="7" height="15"/>',
        LeftRight:
            '<rect y="9" class="gap" width="12" height="20"/>' +
            '<rect x="2.5" y="11.5" class="rectangle" width="7" height="15"/>' +
            '<g>' +
                '<rect y="29" class="border" width="12" height="1"/>' +
                '<rect y="8" class="border" width="12" height="1"/>' +
            '</g>',
        Right:
            '<polygon class="border" points="0,9 12,9 12,29 0,29 0,30 13,30 13,8 0,8"/>' +
            '<rect y="9" class="gap" width="12" height="20"/>' +
            '<rect x="2.5" y="11.5" class="rectangle" width="7" height="15"/>',
        Top:
            '<polygon class="border" points="8,18 8,6 28,6 28,18 29,18 29,5 7,5 7,18"/>' +
            '<rect x="8" y="6" class="gap" width="20" height="12"/>' +
            '<rect x="10.5" y="8.5" class="rectangle" width="15" height="7"/>',
        TopBottom:
            '<rect x="8" class="gap" width="20" height="12"/>' +
            '<rect x="10.5" y="2.5" class="rectangle" width="15" height="7"/>' +
            '<g>' +
                '<rect x="28" class="border" width="1" height="12"/>' +
                '<rect x="7" class="border" width="1" height="12"/>' +
            '</g>',
        Bottom:
            '<polygon class="border" points="28,0 28,12 8,12 8,0 7,0 7,13 29,13 29,0 "/>' +
            '<rect x="8" class="gap" width="20" height="12"/>' +
            '<rect x="10.5" y="2.5" class="rectangle" width="15" height="7"/>',
        Center:
            '<polygon class="gap" points="36,9 28,9 28,0 8,0 8,9 0,9 0,29 8,29 8,36 28,36 28,29 36,29 "/>' +
            '<g>' +
                '<polygon class="border" points="36,29 28,29 28,36 29,36 29,30 36,30"/>' +
                '<polygon class="border" points="0,9 8,9 8,0 7,0 7,8 0,8"/>' +
                '<polygon class="border" points="29,0 28,0 28,9 36,9 36,8 29,8"/>' +
                '<polygon class="border" points="7,36 8,36 8,29 0,29 0,30 7,30"/>' +
            '</g>' +
            '<polygon class="arrow" points="18,1.7 15.1,6.7 20.9,6.7 "/>' +
            '<polygon class="arrow" points="35.4,19.1 30.4,16.2 30.4,22 "/>' +
            '<polygon class="arrow" points="18.1,36.4 21,31.4 15.2,31.4 "/>' +
            '<polygon class="arrow" points="0.7,19 5.7,21.9 5.7,16.1 "/>' +
            '<rect x="10.5" y="12.5" class="light rectangle" width="15" height="15"/>',
        ShowAll:
            '<rect x="3" y= "3" width="1" height="12" />' +
            '<rect x="3" y= "14" class="wj-db-opacity05" width="2" height="1" />' +
            '<rect x="3" y= "3" width="10" height="1" />' +
            '<rect x="12" y= "3" class="wj-db-opacity05" width="1" height="2" />' +
            '<rect x="5" y= "5" width="1" height="12" />' +
            '<rect x="5" y= "16" class="wj-db-opacity05" width="2" height="1" />' +
            '<rect x="5" y= "5" width="11" height="1" />' +
            '<rect x="15" y= "5" class="wj-db-opacity05" width="1" height="2" />' +
            '<rect x="7" y= "7" width="1" height="12" />' +
            '<rect x="7" y= "18" width="11" height="1" />' +
            '<rect x="7" y= "7" width="11" height="1" />' +
            '<rect x="17" y= "7" width="1" height="2" />' +
            '<path style="fill:none" d= "M11,9h8v8h-8V9z" />' +
            '<path d= "M17.3,10.9l-0.7-0.7l-3,3l0.7,0.7C14.3,13.9,17.3,10.9,17.3,10.9z M19.3,10.2l-5,5l-2-2l-0.7,0.7l2.6,2.6' +
            'l5.7-5.7L19.3, 10.2z M9, 13.9l2.6,2.6l0.7-0.7l-2.6-2.6C9.7, 13.3,9,13.9,9,13.9z"/>',
        MoveFile:
            '<path d="M14.03,4H8.24L7.28,2H1.97C1.43,2,1,2.53,1,3.18v10.65C1,14.47,1.43,15,1.96,15h12.08c0.53,0,0.96-0.53,0.96-1.18V5.29' +
	        'C15,4.64,14.57,4,14.03,4z M14,13.82c0,0.09-0.02,0.15-0.04,0.18H2.04C2.02,13.97,2,13.91,2,13.82v-2.84h4.91l-1.63,1.63l0.71,0.71' +
	        'l2.12-2.12l0.71-0.71L8.11,9.78L5.99,7.66L5.28,8.37L6.91,10H2V7h12V13.82z M14,6H2V3.18C2,3.09,2.02,3.03,2.04,3h4.61l0.69,1.43' +
            'L7.61,5h0.63h5.67C13.95,5.06,14,5.16,14,5.29V6z"/>',
        Download:
            '<polygon points="2.79,7 2,7.69 6.71,11.81 6.71,11.81 7.5,12.5 8.29,11.81 8.29,11.81 13,7.69 12.21,7 7.5,11.12 "/>' +
            '<rect x="2.5" y="6.5" transform="matrix(6.123234e-17 -1 1 6.123234e-17 0.5 14.5)" width="10" height="1"/>' +
            '<rect x="2" y="14" transform="matrix(-1 -1.224647e-16 1.224647e-16 -1 15 29)" width="11" height="1"/>',
        Upload:
            '<polygon points="12.21,10 13,9.31 8.29,5.19 8.29,5.19 7.5,4.5 6.71,5.19 6.71,5.19 2,9.31 2.79,10 7.5,5.88 "/>' +
            '<rect x="2.5" y="9.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 17.5 2.5)" width="10" height="1"/>' +
            '<rect x="2" y="2" width="11" height="1"/>',
        Delete:
            '<g>' +
            '<path d="M14,3h-2h-1V2c0-0.55-0.45-1-1-1H5C4.45,1,4,1.45,4,2v1H3H1v1h2v9c0,1.1,0.9,2,2,2h5c1.1,0,2-0.9,2-2V4h2V3z M5,2h5v1H5V2zM11,13c0,0.55-0.45,1-1,1H5c-0.55,0-1-0.45-1-1V4h7V13z"/>' +
            '<rect x="5" y="5" width="1" height="8"/>' +
            '<rect x="7" y="5" width="1" height="8"/>' +
            '<rect x="9" y="5" width="1" height="8"/>' +
            '</g>',
        ArrowUp:
            '<polygon points="12.21,7 13,6.31 8.29,2.19 8.29,2.19 7.5,1.5 6.71,2.19 6.71,2.19 2,6.31 2.79,7 7.5,2.88 "/>' +
            '<rect x="1.5" y="7.5" transform="matrix(-1.836970e-16 1 -1 -1.836970e-16 15.5 0.5)" width="12" height="1"/>',
        Folder:
            '<style type="text/css">' +
                '.st0{fill-rule:evenodd;clip-rule:evenodd;}' +
                '.st1{display:none;}' +
                '.st2{display:inline;fill-rule:evenodd;clip-rule:evenodd;}' +
                '.st3{display:inline;opacity:0.2;fill-rule:evenodd;clip-rule:evenodd;enable-background:new;}' +
                '.st4{display:inline;opacity:0.1;fill-rule:evenodd;clip-rule:evenodd;enable-background:new;}' +
            '</style>' +
            '<g>' +
            '<g>' +
                '<path d="M6.65,3l0.69,1.43L7.61,5h0.63h5.67C13.95,5.06,14,5.16,14,5.29v8.53c0,0.09-0.02,0.15-0.04,0.18H2.04' +
                'C2.02,13.97,2,13.91,2,13.82V3.18C2,3.09,2.02,3.03,2.04,3H6.65 M7.28,2H1.97C1.43,2,1,2.53,1,3.18v10.65' +
                'C1,14.47,1.43,15,1.96,15h12.08c0.53,0,0.96-0.53,0.96-1.18V5.29C15,4.64,14.57,4,14.03,4H8.24L7.28,2L7.28,2z"/>' +
            '</g>' +
            '<rect x="2" y="6" class="st0" width="12" height="1"/>' +
            '<g class="st1">' +
                '<rect x="2" y="10" class="st2" width="6" height="0.98"/>' +
                '<rect x="6" y="9" class="st2" width="1" height="1"/>' +
                '<rect x="6" y="11" class="st2" width="1" height="1"/>' +
                '<rect x="5" y="8" class="st2" width="1" height="1"/>' +
                '<rect x="5" y="12" class="st2" width="1" height="1"/>' +
                '<rect x="7" y="9" class="st3" width="1" height="1"/>' +
                '<rect x="8" y="10" class="st4" width="1" height="1"/>' +
                '<rect x="7" y="11" class="st3" width="1" height="1"/>' +
                '<rect x="6" y="8" class="st3" width="1" height="1"/>' +
                '<rect x="6" y="12" class="st3" width="1" height="1"/>' +
            '</g>' +
            '</g>',
        Triangle:
            '<polygon points="6,13 6,4 11.5,8.5"/>',
        CreateFolder:
            '<path d="M14,4H8.2L7.3,2H2C1.4,2,1,2.5,1,3.2v10.6C1,14.5,1.4,15,2,15h12c0.5,0,1-0.5,1-1.2V5.3C15,4.6,14.6,4,14,4z M14,13.8' +
            'c0,0.1,0,0.1,0,0.2H2c0,0,0-0.1,0-0.2V7h12V13.8z M14,6H2V3.2C2,3.1,2,3,2,3h4.7l0.7,1.4L7.6,5h0.6h5.7c0,0.1,0.1,0.2,0.1,0.3V6z' +
            'M5.5,10.5c0-0.2,0.2-0.4,0.4-0.4h1.7V8.4C7.6,8.2,7.8,8,8,8s0.4,0.2,0.4,0.4V10h1.7c0.2,0,0.4,0.2,0.4,0.5c0,0.2-0.2,0.4-0.4,0.4' +
            'H8.4v1.7C8.4,12.8,8.2,13,8,13s-0.4-0.2-0.4-0.4v-1.7H5.9C5.7,10.9,5.5,10.7,5.5,10.5z"/>',
        ExcelFile:
            '<path d="M14,5.3c0-0.5-0.2-1-0.6-1.4c-0.8-0.8-1.6-1.6-2.3-2.3C10.8,1.3,10.4,1.1,10,1c0,0,0,0-0.1,0C7.5,1,5.1,1,2.6,1' +
            'C2.5,1,2.4,1.1,2.3,1.1C2.1,1.3,2,1.5,2,1.7c0,0,0,0.1,0,0.1c0,4.1,0,8.2,0,12.4c0,0.3,0.1,0.6,0.4,0.7C2.5,15,2.5,15,2.6,15' +
            'c3.6,0,7.2,0,10.8,0c0.3-0.1,0.5-0.2,0.6-0.5c0-0.1,0-0.2,0-0.3C14,11.2,14,8.3,14,5.3z M10,2.1C10,2.1,10,2.1,10,2.1' +
            'c0.1,0,0.3,0,0.3,0.1c0.8,0.8,1.6,1.6,2.4,2.4c0.1,0.1,0.1,0.2,0.2,0.2c0,0.1,0,0.1-0.1,0.1c-0.1,0-0.3,0-0.4,0c-0.8,0-1.6,0-2.3,0' +
            'C10,5,10,5,10,4.9C10,4,10,3.1,10,2.1z M12.9,14c-3.2,0-6.5,0-9.7,0C3,14,3,14,3,13.9c0-2,0-3.9,0-5.9C3,6,3,4.1,3,2.2' +
            'C3,2,3,2,3.2,2c1.9,0,3.8,0,5.7,0C9,2,9,2,9,2.2c0,1,0,2,0,3C9,5.7,9.4,6,9.8,6c1,0,2,0,3,0C13,6,13,6,13,6.1c0,2.6,0,5.1,0,7.7' +
            'C13,14,13,14,12.9,14z M8.8,10.1l2.2,2.9H9.5l-1.5-2l-1.5,2H5l2.3-2.9L5.2,7.3h1.5l1.4,1.9l1.3-1.9h1.5L8.8,10.1z"/>',
        PdfFile:
            '<path d="M13.4,15c-3.6,0-7.2,0-10.8,0c-0.1,0-0.1,0-0.2-0.1C2.1,14.8,2,14.5,2,14.2c0-4.1,0-8.2,0-12.4c0,0,0-0.1,0-0.1' +
            'c0-0.2,0.1-0.4,0.3-0.5C2.4,1.1,2.5,1,2.6,1c2.4,0,4.9,0,7.3,0c0,0,0,0,0.1,0c0.4,0.1,0.8,0.3,1.1,0.6c0.8,0.8,1.6,1.6,2.3,2.3' +
            'C13.8,4.3,14,4.8,14,5.3c0,3,0,5.9,0,8.9c0,0.1,0,0.2,0,0.3C13.9,14.8,13.7,14.9,13.4,15z M3,8c0,2,0,3.9,0,5.9C3,14,3,14,3.1,14' +
            'c3.2,0,6.5,0,9.7,0c0.1,0,0.1,0,0.1-0.1c0-2.6,0-5.1,0-7.7C13,6,13,6,12.9,6c-1,0-2,0-3,0C9.4,6,9,5.7,9,5.2c0-1,0-2,0-3' +
            'C9,2,9,2,8.8,2C6.9,2,5.1,2,3.2,2C3,2,3,2,3,2.2C3,4.1,3,6,3,8z M10,2.1C10,2.1,10,2.1,10,2.1c0,1,0,1.9,0,2.8C10,5,10,5,10.1,5' +
            'c0.8,0,1.6,0,2.3,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.1-0.1c0-0.1-0.1-0.2-0.2-0.2c-0.8-0.8-1.6-1.6-2.4-2.4' +
            'C10.3,2.2,10.1,2.1,10,2.1z M11.4,10.9c-0.5,0-1-0.2-1.5-0.4c-0.3-0.1-0.5-0.2-0.8-0.1c-0.9,0.1-1.7,0.3-2.5,0.6' +
            'c-0.1,0-0.1,0.1-0.1,0.1c-0.3,0.6-0.7,1.1-1.2,1.6C5,12.8,4.8,12.9,4.6,13c-0.2,0-0.4-0.1-0.5-0.2C4,12.7,4,12.6,4,12.5' +
            'c0.1-0.4,0.4-0.6,0.7-0.9c0.2-0.2,0.5-0.4,0.8-0.5c0.1,0,0.1-0.1,0.2,0c0.1,0.1,0.1,0,0.1,0C6.4,10,6.9,8.9,7.3,7.7' +
            'c0-0.1,0-0.2,0-0.3C7.1,6.9,7,6.3,7.1,5.7c0-0.2,0-0.3,0.1-0.5C7.2,5,7.4,5,7.7,5C7.9,5,8,5.2,8,5.4C8,5.6,7.9,5.8,7.9,6' +
            'c0,0.5,0,1-0.1,1.5c0,0,0,0.1,0,0.1c0.2,0.6,0.5,1.2,1,1.7c0.2,0.2,0.5,0.4,0.8,0.5c0.1,0,0.1,0,0.2,0c0.6-0.1,1.1-0.1,1.7,0' +
            'c0.1,0,0.2,0.1,0.3,0.1c0.2,0.1,0.3,0.3,0.3,0.5c-0.1,0.3-0.2,0.4-0.5,0.4C11.5,10.9,11.4,10.9,11.4,10.9z M7.6,8.4' +
            'c-0.3,0.8-0.6,1.5-1,2.2c0.7-0.3,1.4-0.5,2.2-0.6C8.3,9.5,7.9,9,7.6,8.4z M5.5,11.4c-0.4,0.3-0.7,0.7-1,1.1' +
            'C4.9,12.3,5.2,11.9,5.5,11.4z M11.7,10.4c-0.3-0.2-0.7-0.2-1-0.2C11,10.4,11.3,10.5,11.7,10.4z M7.6,5.4c-0.1,0.3-0.1,0.6,0,1' +
            'C7.6,6,7.8,5.7,7.6,5.4z"/>',
        File:
            '<path d="M13.4,15c-3.6,0-7.2,0-10.8,0c-0.1,0-0.1,0-0.2-0.1C2.1,14.8,2,14.5,2,14.2c0-4.1,0-8.2,0-12.4c0,0,0-0.1,0-0.1' +
            'c0-0.2,0.1-0.4,0.3-0.5C2.4,1.1,2.5,1,2.6,1c2.4,0,4.9,0,7.3,0c0,0,0,0,0.1,0c0.4,0.1,0.8,0.3,1.1,0.6c0.8,0.8,1.6,1.6,2.3,2.3' +
            'C13.8,4.3,14,4.8,14,5.3c0,3,0,5.9,0,8.9c0,0.1,0,0.2,0,0.3C13.9,14.8,13.7,14.9,13.4,15z M3,8c0,2,0,3.9,0,5.9C3,14,3,14,3.1,14' +
            'c3.2,0,6.5,0,9.7,0c0.1,0,0.1,0,0.1-0.1c0-2.6,0-5.1,0-7.7C13,6,13,6,12.9,6c-1,0-2,0-3,0C9.4,6,9,5.7,9,5.2c0-1,0-2,0-3' +
            'C9,2,9,2,8.8,2C6.9,2,5.1,2,3.2,2C3,2,3,2,3,2.2C3,4.1,3,6,3,8z M10,2.1C10,2.1,10,2.1,10,2.1c0,1,0,1.9,0,2.8C10,5,10,5,10.1,5' +
            'c0.8,0,1.6,0,2.3,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.1-0.1c0-0.1-0.1-0.2-0.2-0.2c-0.8-0.8-1.6-1.6-2.4-2.4' +
            'C10.3,2.2,10.1,2.1,10,2.1z"/>',
        Refresh:
            '<path d="M1.6,12.3c0-0.5,0-1,0-1.5c0-0.1,0-0.2,0.1-0.2c1,0,2,0,3.1,0c0.1,0,0.2,0,0.3,0.1c0,0.1,0,0.2-0.1,0.3' +
            'c-0.3,0.3-0.6,0.6-0.8,0.8C4,11.9,4,11.9,4.1,12C5,12.9,6,13.4,7.3,13.5c2.9,0.4,5.5-1.5,6.2-4.1C13.5,9,13.6,8.5,13.6,8' +
            'c0-0.4,0.3-0.7,0.7-0.7c0.4,0,0.7,0.4,0.7,0.8c0,1.6-0.5,3.1-1.5,4.3c-1.1,1.3-2.5,2.2-4.2,2.5c-2.3,0.4-4.4-0.2-6.1-1.8' +
            'C3,12.9,3,12.9,2.9,13c-0.3,0.3-0.6,0.6-0.9,0.9C2,13.9,2,14,1.9,14c-0.1,0.1-0.2,0.1-0.3,0.1c-0.1,0-0.1-0.2-0.1-0.3' +
            'C1.6,13.3,1.6,12.8,1.6,12.3C1.6,12.3,1.6,12.3,1.6,12.3z M14.2,5.4c0.1,0,0.2,0,0.2-0.2c0-1,0-2,0-3c0-0.1,0-0.1,0-0.2' +
            'c0-0.2-0.2-0.3-0.3-0.1C14,2,13.9,2,13.9,2.1c-0.3,0.3-0.6,0.6-0.9,0.9C13,3,12.9,3,12.8,2.9c-0.2-0.2-0.3-0.3-0.5-0.4' +
            'c-1.6-1.2-3.4-1.7-5.4-1.4C5.1,1.4,3.7,2.2,2.6,3.6C1.5,4.9,1,6.3,1,7.9c0,0.4,0.3,0.7,0.7,0.8c0.4,0,0.7-0.3,0.7-0.7' +
            'c0-0.4,0-0.7,0.1-1.1c0.6-3.3,4-5.3,7.2-4.2c0.8,0.3,1.5,0.7,2.1,1.2c0.1,0.1,0.1,0.1,0,0.2c-0.1,0.1-0.3,0.3-0.4,0.4' +
            'C11.3,4.7,11.1,4.9,11,5c-0.1,0.1-0.1,0.1-0.1,0.2c0,0.1,0.1,0.1,0.2,0.1c0.1,0,0.1,0,0.2,0c0.5,0,1,0,1.4,0' +
            'C13.2,5.4,13.7,5.4,14.2,5.4z"/>',
        Search:
            '<path d="M12.6,6.8c0,1.3-0.4,2.4-1.1,3.4c-0.1,0.1-0.1,0.1,0,0.2c1.1,1.1,2.1,2.1,3.2,3.2c0.3,0.3,0.4,0.8,0.1,1.1' +
            'c-0.2,0.3-0.7,0.4-1,0.1c-0.1,0-0.1-0.1-0.2-0.2c-1.1-1.1-2.1-2.1-3.2-3.2c-0.1-0.1-0.1-0.1-0.3,0c-0.8,0.6-1.7,1-2.7,1.1' +
            'c-3,0.3-5.7-1.7-6.3-4.6C0.5,4.7,2.5,1.8,5.6,1.1c3.1-0.7,6.1,1.2,6.9,4.2C12.5,5.8,12.6,6.3,12.6,6.8z M2.5,6.8' +
            'c0,2.4,1.9,4.3,4.3,4.3c2.4,0,4.3-1.9,4.3-4.3c0-2.4-1.9-4.3-4.3-4.3C4.5,2.5,2.5,4.4,2.5,6.8z"/>',
        DocFile:
            '<path style="fill:#488FCC" d="M14.6,2.6H8.4v10.9h6.2c0.2,0,0.4-0.2,0.4-0.4V2.9C15,2.7,14.8,2.6,14.6,2.6z"/>' +
            '<path style="fill:#FFFFFF" d="M8.4,4.5h5.4v0.8H8.4V4.5z M8.4,6.1h5.4v0.8H8.4V6.1z M8.4,7.6h5.4v0.8H8.4V7.6z M8.4,9.2h5.4v0.8H8.4V9.2z' +
            'M8.4,10.7h5.4v0.8H8.4V10.7z"/>' +
            '<path style="fill:#17499E" d="M9.2,15L1,13.4V2.6L9.2,1V15z"/>' +
            '<path style="fill:#FFFFFF" d="M6.9,10.7H5.8L5.1,7.2c0-0.2-0.1-0.4-0.1-0.6h0C5,6.9,5,7.1,5,7.2l-0.7,3.5H3.2L2.1,5.3h1l0.6,3.6' +
            'c0,0.2,0,0.4,0.1,0.6h0c0-0.2,0-0.4,0.1-0.6l0.8-3.6h1l0.7,3.7c0,0.1,0,0.3,0.1,0.6h0c0-0.2,0-0.4,0.1-0.6l0.6-3.6h1L6.9,10.7z"/>',
        ImageFile:
            '<rect x="3.1" y="4.3" style="fill:#84D2F6" width="9.7" height="7.6"/>' +
            '<path style="fill:#4DAF4F" d="M11.4,11.9H8.6L7.1,9.8l1.3-1.6c0.1-0.1,0.2-0.1,0.2,0L11.4,11.9z"/>' +
            '<path style="fill:#556F7A" d="M14.1,2.7c0.1,0,0.2,0.1,0.2,0.2v10.2c0,0.1-0.1,0.2-0.2,0.2H1.9c-0.1,0-0.2-0.1-0.2-0.2V2.9' +
            'c0-0.1,0.1-0.2,0.2-0.2H14.1 M14.1,2H1.9C1.4,2,1,2.4,1,2.9v10.2C1,13.6,1.4,14,1.9,14h12.2c0.5,0,0.9-0.4,0.9-0.9V2.9' +
            'C15,2.4,14.6,2,14.1,2L14.1,2z"/>' +
            '<circle style="fill:#FFF379" cx="11.1" cy="6.1" r="0.7"/>' +
            '<path style="fill:#44A147" d="M8.6,11.9H3.1V8.7l1.6-1.8C4.8,6.8,4.9,6.8,5,7L8.6,11.9z"/>' +
            '<path style="fill:#67BC6B" d="M11.6,8.8l-1.4,1.5l1.2,1.6h1.4V9.7l-1.1-1C11.7,8.7,11.6,8.7,11.6,8.8z"/>',
        ThumbnaiLView:
            '<g>' +
            '<g>' +
                '<rect x="5" y="5" width="4.1" height="4.1"/>' +
                '<rect x="5" y="9.9" width="4.1" height="4.1"/>' +
                '<rect x="5" y="14.9" width="4.1" height="4.1"/>' +
            '</g>' +
            '<g>' +
                '<rect x="9.9" y="5" width="4.1" height="4.1"/>' +
                '<rect x="9.9" y="9.9" width="4.1" height="4.1"/>' +
                '<rect x="9.9" y="14.9" width="4.1" height="4.1"/>' +
            '</g>' +
            '<g>' +
                '<rect x="14.9" y="5" width="4.1" height="4.1"/>' +
                '<rect x="14.9" y="9.9" width="4.1" height="4.1"/>' +
                '<rect x="14.9" y="14.9" width="4.1" height="4.1"/>' +
            '</g>' +
            '</g>',
        ListView:
            '<g>' +
            '<g>' +
                '<rect x="4" y="5" width="4.1" height="4.1"/>' +
                '<rect x="4" y="9.9" width="4.1" height="4.1"/>' +
                '<rect x="4" y="14.9" width="4.1" height="4.1"/>' +
            '</g>' +
                '<rect x="11" y="6.6" width="5" height="1"/>' +
                '<rect x="11" y="11.5" width="9" height="1"/>' +
                '<rect x="11" y="16.4" width="6" height="1"/>' +
            '</g>',
        DetailView:
            '<g>' +
                '<rect x="3.5" y="5" width="4.1" height="4.1"/>' +
                '<rect x="3.5" y="9.9" width="4.1" height="4.1"/>' +
                '<rect x="3.5" y="14.9" width="4.1" height="4.1"/>' +
            '</g>' +
            '<rect x="9.5" y="6.6" width="3" height="1"/>' +
            '<rect x="9.5" y="11.5" width="3" height="1"/>' +
            '<rect x="9.5" y="16.4" width="3" height="1"/>' +
            '<rect x="13.4" y="6.6" width="3" height="1"/>' +
            '<rect x="13.4" y="11.5" width="3" height="1"/>' +
            '<rect x="13.4" y="16.4" width="3" height="1"/>' +
            '<rect x="17.3" y="6.6" width="3" height="1"/>' +
            '<rect x="17.3" y="11.5" width="3" height="1"/>' +
            '<rect x="17.3" y="16.4" width="3" height="1"/>',
        BigDoc:
            '<path style="fill:#2196F3" d="M410,100H250v280h160c5.5,0,10-4.5,10-10V110C420,104.5,415.5,100,410,100z"/>' +
            '<path style="fill:#FFFFFF" d="M250,150h140v20H250V150z M250,190h140v20H250V190z M250,230h140v20H250V230z M250,270h140v20H250V270z' +
            'M250,310h140v20H250V310z"/>' +
            '<path style="fill:#0D47A1" d="M270,420L60,380V100l210-40V420z"/>' +
            '<path style="fill:#FFFFFF" d="M211.7,310.1h-27.2l-18-89.9c-1-4.8-1.6-10-1.7-15.8h-0.3c-0.4,6.4-1.1,11.6-2,15.8L144,310.1h-28.3L87.1,170' +
            'h26.8l15.4,93.3c0.6,4,1.1,9.4,1.4,16.1h0.4c0.2-5,1-10.5,2.2-16.4L153,170h26.2l17.9,94c0.6,3.5,1.2,8.5,1.7,15.1h0.3' +
            'c0.2-5.1,0.7-10.4,1.6-15.6l15-93.5h24.7L211.7,310.1z"/>',
        BigExcel:
            '<path style="fill:#4CAF50" d="M410,100H250v280h160c5.5,0,10-4.5,10-10V110C420,104.5,415.5,100,410,100z"/>' +
            '<path style="fill:#FFFFFF" d="M320,150h70v30h-70V150z M320,250h70v30h-70V250z M320,300h70v30h-70V300z M320,200h70v30h-70V200z M250,150h50' +
            'v30h-50V150z M250,250h50v30h-50V250z M250,300h50v30h-50V300z M250,200h50v30h-50V200z"/>' +
            '<path style="fill:#2E7D32" d="M270,420L60,380V100l210-40V420z"/>' +
            '<path style="fill:#FFFFFF" d="M191.3,310l-24.1-45.6c-0.9-1.7-1.9-4.8-2.8-9.4H164c-0.5,2.2-1.5,5.4-3.2,9.8L136.5,310H99l44.6-70l-40.8-70' +
            'h38.4l20,42c1.6,3.3,3,7.2,4.2,11.8h0.4c0.8-2.7,2.2-6.8,4.4-12.2l22.3-41.6h35.1l-42,69.4l43.2,70.6L191.3,310L191.3,310z"/>',
        BigImage:
            '<g>' +
            '<rect x="114.7" y="144" style="fill:#81D4FA" width="249.7" height="196.1"/>' +
            '<path style="fill:#4CAF50" d="M327.5,340.1h-72.7l-38.2-53.2l34.5-42c1.3-1.6,3.9-1.6,5.2,0.1L327.5,340.1z"/>' +
            '<path style="fill:#546E7A" d="M396.7,102.9c3.5,0,6.4,2.9,6.4,6.4v261.5c0,3.5-2.9,6.4-6.4,6.4H83.8c-3.5,0-6.4-2.9-6.4-6.4V109.3' +
            'c0-3.5,2.9-6.4,6.4-6.4H396.7 M396.7,85.7H83.8c-13,0-23.5,10.5-23.5,23.5v261.5c0,13,10.5,23.5,23.5,23.5h312.9' +
            'c13,0,23.5-10.5,23.5-23.5V109.3C420.3,96.3,409.7,85.7,396.7,85.7L396.7,85.7z"/>' +
            '<circle style="fill:#FFF176" cx="319.9" cy="190.8" r="18.3"/>' +
            '<path style="fill:#43A047" d="M254.8,340.1H114.7v-81.9l40.1-45.5c2.3-2.7,6.6-2.5,8.6,0.4L254.8,340.1z"/>' +
            '<path style="fill:#66BB6A" d="M332.8,260l-35.9,39.3l30.5,40.7h36.9v-55.5l-27.7-24.8C335.6,258.7,333.8,258.8,332.8,260z"/>' +
            '</g>',
        BigPdf:
            '<path style="fill:#C74444" d="M60,0v480h360V140L280,0H60z M100,40h160v120h120v280H100V40z"/>' +
            '<path style="fill:#C74444" d="M325,287.3c-3.5-2.6-7.6-4.4-13.3-5.9c-12.4-3.3-24.4-1.7-36.7,0.4l-0.7,0.1c-2.1,0.4-2.9,0.4-3.9-0.4' +
            'c-12.5-10.9-21.5-20.9-28.1-31.4c-0.7-1.1-0.8-2-0.5-3.5c2-9.6,2.9-18.8,3.1-29.7c0.1-2.8-0.3-7.7-1.2-13c-1.4-8.7-3.2-17-9.4-23.8' +
            'c-3.3-3.7-7.7-5.5-12-4.9c-4.3,0.5-8.1,3.4-10.4,7.8c-2.3,4.3-3,8.9-3.4,13.2c-1.6,16.6,2,33.1,11.1,51.9c1.1,2.2,1.3,4,0.6,6.3' +
            'c-3.9,14.1-10.2,27.5-16.3,40.5c-1.3,2.8-2.6,5.6-3.9,8.4c-1,2.2-2.5,3.6-5.5,5c-15.9,7.5-28.7,14.7-39.1,26' +
            'c-5.8,6.3-8.8,12.2-9.6,18.7c-0.6,5.2,0.9,9.8,4.3,12.9c2.6,2.4,6.1,3.6,9.9,3.6c1.2,0,2.5-0.1,3.8-0.4c6.9-1.4,12.1-5.3,15.9-8.6' +
            'c10.6-9.2,20.4-20.8,29.9-35.5c1.8-2.7,3.7-4.4,6.5-5.5c9.3-3.7,19-6.9,28.3-10.1c3.6-1.2,7.2-2.4,10.8-3.7c8-2.8,13.7-1.7,20.3,3.7' +
            'c10.2,8.4,21.2,13.8,32.6,16.1c10.6,2.1,19.7-2.4,23.8-11.8C336.3,303.6,333.7,293.7,325,287.3z M314.6,305.8' +
            'c-0.3,1.1-0.5,1.6-3.6,0.9c-6.3-1.4-11.8-4.4-16.9-7.8c7.1-0.6,13.1,0.4,18.5,3C315,303,315.2,304.1,314.6,305.8z M249.1,287.9' +
            'c-7.3,2.1-15.5,4.7-24.4,8c3.6-7.9,7-15.6,10-23.7C239.4,277.9,243.9,283,249.1,287.9z"/>',
        BigFile:
            '<path d="M60,0v480h360V140L280,0H60z M100,40h160v120h120v280H100V40z"/>',
        BigFolder:
            '<path style="fill:#DBB065" d="M15,405V45h128.8l60,60H465v300H15z"/>' +
            '<path style="fill:#967A44" d="M137.6,60l51.2,51.2l8.8,8.8H210h240v270H30V60H137.6 M150,30H0v390h480V90H210L150,30L150,30z"/>' +
            '<g>' +
            '<path style="fill:#F5CE85" d="M15,435V135h138.5l60-30H465v330H15z"/>' +
            '<path style="fill:#967A44" d="M450,120v300H30V150h120h7.1l6.3-3.2l53.7-26.8H450 M480,90H210l-60,30H0v330h480V90L480,90z"/>' +
            '</g>',
    };

    export function _createSvgBtn(btnType: string): HTMLElement {
        if (!_ICONS[btnType]) {
            throw 'Invalid button type.';
        }
        let svgContent = _ICONS[btnType],
            btn = document.createElement('div'),
            width = 22,
            height = 22,
            bigsize = 480;
            

        switch (btnType) {
            case 'MoveIndicator':
                width = 40;
                height = 10;
                break;
            case 'Resize':
            case 'ResizeLeft':
                width = 10;
                height = 10;
                break;
            case 'Left':
            case 'Right':
                width = 18;
                height = 36;
                break;
            case 'Top':
            case 'Bottom':
                width = 36;
                height = 18;
                break;
            case 'LeftRight':
                width = 12;
                height = 36;
                break;
            case 'TopBottom':
                width = 36;
                height = 12;
                break;
            case 'Center':
                width = 36;
                height = 36;
                break;
            case 'MoveFile':
                width = 15;
                height = 16;
                break;
            case 'Download':
                width = 15;
                height = 16;
                break;
            case 'Upload':
                width = 15;
                height = 16;
                break;
            case 'Delete':
                width = 15;
                height = 15;
                break;
            case 'ArrowUp':
                width = 13;
                height = 12;
                break;
            case 'Folder':
                width = 15;
                height = 15;
                break;
            case 'Triangle':
                width = 16;
                height = 15;
                break;
            case 'CreateFolder':
                width = 15;
                height = 16;
                break;
            case 'ExcelFile':
                width = 15;
                height = 15;
                break;
            case 'ImageFile':
                width = 15;
                height = 15;
                break;
            case 'DocFile':
                width = 15;
                height = 15;
                break;
            case 'PdfFile':
                width = 15;
                height = 15;
                break;
            case 'File':
                width = 15;
                height = 15;
                break;
            case 'Refresh':
                width = 15;
                height = 15;
                break;
            case 'Search':
                width = 15;
                height = 15;
                break;
            case 'ThumbnaiLView':
                width = 30;
                height = 30;
                break;
            case 'ListView':
                width = 30;
                height = 30;
                break;
            case 'DetailView':
                width = 30;
                height = 30;
                break;
            case 'BigExcel':
                width = 72;
                height = 72;
                break;
            case 'BigImage':
                width = 72;
                height = 72;
                break;
            case 'BigPdf':
                width = 72;
                height = 72;
                break;
            case 'BigDoc':
                width = 72;
                height = 72;
                break;
            case 'BigFile':
                width = 72;
                height = 72;
                break;
            case 'BigFolder':
                width = 72;
                height = 72;
                break;                
            default:
                break;
        }
        svgContent = _getSVGStartMarkup(width, height) + svgContent + _SVG_END;
        let svg = wijmo.createElement(svgContent, btn);
        wijmo.addClass(btn, 'wj-dl-btn');
        return btn;
    }

    function _getSVGStartMarkup(width: number = 22, height: number = 22): string {
        let maxwidth = 480;
        let maxheight = 480;
        if (height != 72) {
            maxwidth = width;
            maxheight = height;
        }
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" ' +
            'xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" ' +
            'viewBox ="0 0 ' + maxwidth + ' ' + maxheight + '" ' +
            'style="enable-background:new 0 0 ' + maxwidth + ' ' + maxheight + '; ' +
            'width: ' + width + 'px; height: ' + height + 'px;" ' +
            'xml: space ="preserve" >';
    }

    export function _removeElement(e: Node) {
        return e && e.parentNode
            ? e.parentNode.removeChild(e)
            : null;
    }

    export function _getDistance(p1: wijmo.Point, p2: wijmo.Point): number {
        return Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2);
    }

    export function _createInstance(typeName: string, params ?: any[]): any {
        let stringToFunction = (str: string): any => {
            let arr = str.split("."), fn = (window || this);
            for (let i = 0, len = arr.length; i < len; i++) {
                fn = fn[arr[i]];
            }

            if (typeof fn !=="function") {
                throw new Error("function not found");
            }
            return fn;
        },
            classFunction = stringToFunction(typeName),
            instance = Object.create(classFunction.prototype);

        instance.constructor.apply(instance, params);
        return instance;
    }

    export function _addClass(e: Element, className: string) {
        if (!wijmo.hasClass(e, className)) {
            wijmo.addClass(e, className);
        }
    }

    export function _removeClass(e: Element, className: string) {
        if (wijmo.hasClass(e, className)) {
            wijmo.removeClass(e, className);
        }
    }
}