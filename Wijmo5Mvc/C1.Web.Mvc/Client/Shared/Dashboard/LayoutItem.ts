﻿module c1.nav {
    'use strict';

    /**
     * Defines the base class for the layoutitem.
     * Now there are two kinds of layoutitems: @see:Tile and @see:Group.
     */
    export class LayoutItem extends DisposableObject {
        static _CLASS_LI = 'wj-dl-li';
        static _CLASS_CONTENT = 'wj-li-content';
        static _SETTING_NAMES: string[] = null;
        static _MIN_SIZE = 100;

        private _parent: any;
        // Occurs when the tile is activated.
        private _tileActivated = new wijmo.Event();
        // Occurs when an element representing a @see:Tile has been created.
        private _tileFormatted = new wijmo.Event();
        // Occurs when the tile is maximized or restored.
        private _tileSizeChanged = new wijmo.Event();
        // Occurs when the tile's visibility is changed.
        private _tileVisibilityChanged = new wijmo.Event();
        // Occurs when the layout is changed.
        private _layoutChanged = new wijmo.Event();

        protected _hostElement: HTMLElement;
        // the element which holds the tile content.
        _contentElement: HTMLElement;
        private _layout: LayoutBase;
        private _isLayoutInitialled = false;

        /**
         * Initializes a new @see:LayoutItem.
         *
         * @param opts JavaScript object containing initialization data for the layoutitem.
         */
        constructor(opts?: any) {
            super();
            this.initialize(opts);
        }

        /**
         * Gets the parent which owns this layoutitem.
         *
         * It could be a @see:Group or a @see:LayoutBase.
         *
         */
        get parent(): any {
            return this._parent;
        }

        /**
         * Gets the layout which owns this item.
         */
        get layout(): LayoutBase {
            if (this._isLayoutInitialled) {
                return this._layout;
            }

            this._isLayoutInitialled = true;
            this._layout = LayoutItem._getLayoutFromItem(this.parent);
            return this._layout;
        }

        private static _getLayoutFromItem(item: any): LayoutBase {
            let layout;
            if (item instanceof LayoutBase) {
                layout = item;
            } else if (item instanceof Group) {
                layout = item.layout;
            }
            return layout;
        }

        /**
         * Gets the outest element which represents this layoutitem.
         */
        get hostElement(): HTMLElement {
            return this._hostElement;
        }

        /**
         * Initializes the object by copying the properties from a given object.
         *
         * @param options Object that contains the initialization data.
         */
        initialize(options: any) {
            if (options) {
                wijmo.copy(this, options);
            }
        }

        /**
         * Renders the layoutitem in the specified container.
         *
         * @param container the container element where the layoutitem renders in.
         */
        render(container?: HTMLElement): HTMLElement {
            if (this._hostElement) {
                this._updatePartialView(container);
                return this._hostElement;
            }

            this._initializeComponent();
            if (container) {
                this._appendTo(this._hostElement, container);
            }

            this.draw();

            if (container && this._hostElement.parentElement != container) {
                this._appendTo(this._hostElement, container);
            }

            return this._hostElement;
        }

        _appendTo(child: HTMLElement, parent: HTMLElement) {
            parent.appendChild(child);
        }

        // when the item has been rendered, only update the necessary layout without redrawing.
        _updatePartialView(container?: HTMLElement) {
        }

        // Initializes the dom elements, styles and related resources for the layoutitem.
        _initializeComponent() {
            this._hostElement = document.createElement('div');
            wijmo.addClass(this._hostElement, LayoutItem._CLASS_LI);
            let contentElement = document.createElement('div');
            wijmo.addClass(contentElement, LayoutItem._CLASS_CONTENT);
            this._contentElement = contentElement;
            this._hostElement.appendChild(contentElement);
        }

        /**
         * Draws the layout item.
         *
         * Adds the codes to implement the paiting parts.
         *
         */
        draw() {
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (fullDispose) {
                _removeElement(this._hostElement);
                this._hostElement = null;
                this._contentElement = null;
            }

            super.dispose(fullDispose);
        }

        _resetLayout() {
            this._isLayoutInitialled = false;
            this._layout = null;
            if (this instanceof Group) {
                this.children.forEach(item => {
                    (<LayoutItem>item)._resetLayout();
                });
            }
        }

        // Gets the definition for the settings of this layoutitem.
        _getDefinition() {
            let definition = {};
            this._getNames().forEach(name => {
                definition[name] = this[name];
            });

            return definition;
        }

        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (LayoutItem._SETTING_NAMES) {
                return LayoutItem._SETTING_NAMES;
            }

            LayoutItem._SETTING_NAMES = [].concat([]);
            return LayoutItem._SETTING_NAMES;
        }

        // set the parent of the current layoutitem.
        _setParent(parent: any) {
            if (this.parent != parent) {
                if (this.parent) {
                    this._tileActivated.removeHandler(this._activeTile, this.parent);
                    this._tileFormatted.removeHandler(this._formatTile, this.parent);
                    this._tileSizeChanged.removeHandler(this._changeTileSize, this.parent);
                    this._tileVisibilityChanged.removeHandler(this._changeTileVisibility, this.parent);
                    this._layoutChanged.removeHandler(this._changeLayout, this.parent);
                }

                if (this._isLayoutInitialled) {
                    if (this.layout != LayoutItem._getLayoutFromItem(parent)) {
                        this._resetLayout();
                    }
                }

                this._parent = parent;
                if (this.parent) {
                    this._tileActivated.addHandler(this._activeTile, this.parent);
                    this._tileFormatted.addHandler(this._formatTile, this.parent);
                    this._tileSizeChanged.addHandler(this._changeTileSize, this.parent);
                    this._tileVisibilityChanged.addHandler(this._changeTileVisibility, this.parent);
                    this._layoutChanged.addHandler(this._changeLayout, this.parent);
                }
            }
        }
        // keep this method name same with the one defined in LayoutBase.
        _onTileActivated(e: TileEventArgs) {
            this._tileActivated.raise(this, e);
        }
        private _activeTile(sender: any, e: TileEventArgs) {
            this._onTileActivated(e);
        }
        // keep this method name same with the one defined in LayoutBase.
        _onTileFormatted(e: TileFormattedEventArgs) {
            this._tileFormatted.raise(this, e);
        }
        private _formatTile(sender: any, e: TileFormattedEventArgs) {
            this._onTileFormatted(e);
        }
        // keep this method name same with the one defined in LayoutBase.
        _onTileSizeChanged(e: TileSizeChangedEventArgs) {
            this._tileSizeChanged.raise(this, e);
        }
        private _changeTileSize(sender: any, e: TileSizeChangedEventArgs) {
            this._onTileSizeChanged(e);
        }
        // keep this method name same with the one defined in LayoutBase.
        _onTileVisibilityChanged(e: TileEventArgs) {
            this._tileVisibilityChanged.raise(this, e);
        }
        private _changeTileVisibility(sender: any, e: TileEventArgs) {
            this._onTileVisibilityChanged(e);
        }
        // keep this method name same with the one defined in LayoutBase.
        _onLayoutChanged() {
            this._layoutChanged.raise(this);
        }
        private _changeLayout(sender: any) {
            this._onLayoutChanged();
        }

        /**
         * Removes the current layoutitem from its parent.
         * @return True if the layoutitem is removed successfully. Otherwise, false.
         */
        remove(): boolean {
            return this._remove(false);
        }

        _remove(internal: boolean = false): boolean {
            let parent = this.parent;
            if (!parent) {
                return false;
            }

            let succeeded = false;
            if (parent instanceof LayoutBase) {
                succeeded = !internal ? (<LayoutBase>parent).items.remove(this) : (<LayoutBase>parent).items._removeInternal(this);
            } else {
                succeeded = !internal ? (<Group>parent).children.remove(this) : (<Group>parent).children._removeInternal(this);
            }

            if (internal && succeeded) {
                // redraw the parent
                _removeElement(this.hostElement);
                parent._redraw();
            }
            
            return succeeded;
        }
    }

    /**
     * Defines a class which represents the minimum layout item. 
     */
    export class Tile extends LayoutItem {
        static _CLASS_TILE: string = 'wj-dl-tile';
        static _BORDER_WIDTH = 1;
        static _SETTING_NAMES: string[] = null;
        // when the content is set to a dom element, a selector, 
        // use this to save the parent of the element
        // So when the tile is removed, the element will be appended back to it.
        static _CONTENT_ELEMENTS_CONTAINER: HTMLElement = null;

        private _content: string;
        private _domContent: HTMLElement;
        private _domContentSelector: HTMLElement;

        private _headerText: string;
        private _showHeader = true;
        private _showToolbar = true;
        private _visible = true;
        private _allowDrag: boolean = true;
        private _allowMaximize: boolean = true;
        private _allowHide: boolean = true;
        private _maxResizeWidth: number = Number.MAX_VALUE;
        private _maxResizeHeight: number = Number.MAX_VALUE;

        private _isActived: boolean = false;

        _isMaximum = false;
        private _isPlaceHolder = false;

        private _eleContent: HTMLElement;
        private _eleHeader: HTMLElement;
        private _toolbarMenus: Toolbar;

        /**
         * Initializes a new @see:Tile.
         *
         * @param opts JavaScript object containing initialization data for the layoutitem.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
            if (!Tile._CONTENT_ELEMENTS_CONTAINER) {
                let container = document.createElement('div');
                wijmo.addClass(container, 'wj-dl-conts-container');
                document.body.appendChild(container);
                Tile._CONTENT_ELEMENTS_CONTAINER = container;
            }
        }

        /**
         * Gets or sets the tile content(text/html)
         * or the selector of the dom element which contains the content.
         */
        get content(): string {
            return this._content;
        }
        set content(value: string) {
            if (this.content != value) {
                this._saveContentSelectorElement();
                this._domContentSelector = null;
                this._content = value;
                try {
                    this._domContentSelector = wijmo.getElement(value);
                    this._domContent = this._domContentSelector;
                }
                catch (e) {
                    this._domContent = wijmo.createElement(this.content);
                }
            }
        }

        // save the content selector element to the container.
        _saveContentSelectorElement() {
            if (this._domContentSelector) {
                if (wijmo.contains(Tile._CONTENT_ELEMENTS_CONTAINER, this._domContentSelector)) {
                    return;
                }

                Tile._CONTENT_ELEMENTS_CONTAINER.appendChild(this._domContentSelector);
            }
        }

        /**
         * Gets or sets the tile header title.
         */
        get headerText(): string {
            return this._headerText;
        }
        set headerText(value: string) {
            this._headerText = value;
        }

        /**
         * Gets or sets a boolean value decides whether to show the header.
         */
        get showHeader(): boolean {
            return this._showHeader;
        }
        set showHeader(value: boolean) {
            value = !!value;
            if (this._showHeader != value) {
                this._showHeader = value;
            }
        }

        /**
         * Gets or sets a boolean value decides whether to show the toolbar.
         */
        get showToolbar(): boolean {
            return this._showToolbar;
        }
        set showToolbar(value: boolean) {
            value = !!value;
            if (this._showToolbar != value) {
                this._showToolbar = value;
                this._updateToolbar();
            }
        }

        /**
         * Gets or sets a boolean value decides whether to show the tile.
         */
        get visible(): boolean {
            return this._visible;
        }
        set visible(value: boolean) {
            value = !!value;
            if (this._visible != value) {
                this._visible = value;
                if (!this.visible && this._isMaximum) {
                    this._toggleMaximum();
                }
                let args = new TileEventArgs();
                args.tile = this;
                this._onTileVisibilityChanged(args);
            }
        }

        /**
         * Gets or sets a boolean value decides whether the tile could be dragged.
         */
        get allowDrag(): boolean {
            return this._allowDrag;
        }
        set allowDrag(value: boolean) {
            value = !!value;
            if (this.allowDrag != value) {
                this._allowDrag = value;
                this._updateDraggable();
            }
        }

        private _updateDraggable() {
            if (this.hostElement) {
                wijmo.toggleClass(this.hostElement, 'wj-tile-draggable', this.allowDrag);
            }
        }

        private _isDraggable(): boolean {
            let draggable = true;
            if (this.layout && this.layout.dashboard) {
                draggable = this.layout.dashboard.allowDrag;
            }

            return draggable && this.allowDrag;
        }

        /**
         * Gets or sets a boolean value decides whether the tiles could be maximized.
         */
        get allowMaximize(): boolean {
            return this._allowMaximize;
        }
        set allowMaximize(value: boolean) {
            value = !!value;
            if (this.allowMaximize != value) {
                this._allowMaximize = value;
                this._refreshToolbar();
            }
        }

        _isMaximizable(): boolean {
            let isMaximizable = true;
            if (this.layout && this.layout.dashboard) {
                isMaximizable = this.layout.dashboard.allowMaximize;
            }

            return isMaximizable && this.allowMaximize;
        }

        /**
         * Gets a boolean value represents whether the tile is maximized.
         */
        get maximum(): boolean {
            return !!this._isMaximum;
        }

        /**
         * Gets or sets a boolean value decides whether the tiles could be hidden.
         */
        get allowHide(): boolean {
            return this._allowHide;
        }
        set allowHide(value: boolean) {
            value = !!value;
            if (this.allowHide != value) {
                this._allowHide = value;
                this._refreshToolbar();
            }
        }

        _couldBeHidden(): boolean {
            let allowHide = true;
            if (this.layout && this.layout.dashboard) {
                allowHide = this.layout.dashboard.allowHide;
            }

            return allowHide && this.allowHide;
        }

        _couldShowAll(): boolean {
            if (this.layout && this.layout.dashboard) {
                return this.layout.dashboard.allowShowAll;
            }

            return false;
        }

        /**
         * Gets or sets the maximum values of tile's width when resizing
        */
        get maxResizeWidth(): number {
            return this._maxResizeWidth;
        }
        set maxResizeWidth(value: number) {
            this._maxResizeWidth = value;
            if (this.hostElement && parseInt(this.hostElement.style.width) > this._maxResizeWidth) this.hostElement.style.width = this._maxResizeWidth + 'px';
        }

        /**
         * Gets or sets the maximum values of tile's height when resizing
        */
        get maxResizeHeight(): number {
            return this._maxResizeHeight;
        }
        set maxResizeHeight(value: number) {
            this._maxResizeHeight = value;
            if (this.hostElement && parseInt(this.hostElement.style.height) > this._maxResizeHeight) this.hostElement.style.height = this._maxResizeHeight + 'px';
        }
        
        // Gets the names of the fields which are considered as the settings.
        _getNames(): string[] {
            if (Tile._SETTING_NAMES) {
                return Tile._SETTING_NAMES;
            }

            Tile._SETTING_NAMES = ['headerText', 'content', 'visible',
                'showHeader', 'showToolbar',
                'allowDrag', 'allowMaximize', 'allowHide', 'maxResizeWidth', 'maxResizeHeight'].concat(super._getNames());
            return Tile._SETTING_NAMES;
        }

        // Initializes the dom elements, styles and related resources for the tile.
        _initializeComponent() {
            super._initializeComponent();
            wijmo.addClass(this.hostElement, Tile._CLASS_TILE);
            this._updateDraggable();
            this.addEventListener(this.hostElement, 'contextmenu', (e: MouseEvent) => {
                e.preventDefault();
                e.stopPropagation();
            }, false);
        }

        /**
         * Renders the layoutitem in the specified container.
         *
         * @param container the container element where the layoutitem renders in.
         */
        render(container?: HTMLElement): HTMLElement {
            if (!this.visible) {
                return;
            }

            return super.render(container);
        }

        /**
         * Draws the tile.
         */
        draw() {
            super.draw();
            if (!this._isPlaceHolder) {
                this._updateAcitved();
                this._drawHeader();
                this._drawContent();
                this._drawMenus();
                this._bindEvents();
                let argsFormatted = new TileFormattedEventArgs();
                argsFormatted.tile = this;
                argsFormatted.contentElement = this._eleContent;
                argsFormatted.headerElement = this._eleHeader;
                argsFormatted.toolbar = this._toolbarMenus;
                this._onTileFormatted(argsFormatted);
            }
        }
        // draw the tile header
        private _drawHeader() {
            if (!this.showHeader) {
                return;
            }

            if (!this._eleHeader) {
                let eleHeader = document.createElement('div');
                wijmo.addClass(eleHeader, 'wj-tile-header');
                this._contentElement.appendChild(eleHeader);
                this._eleHeader = eleHeader;
            }

            let spanTitle = document.createElement('span');
            wijmo.addClass(spanTitle, 'title');
            spanTitle.innerHTML = this.headerText;
            this._eleHeader.appendChild(spanTitle);
        }
        // draws the tile content
        private _drawContent() {
            if (!this._eleContent) {
                let eleContent = document.createElement('div');
                wijmo.addClass(eleContent, 'wj-tile-content');
                this._contentElement.appendChild(eleContent);
                this._eleContent = eleContent;
            }

            if (this._domContent) {
                this._eleContent.appendChild(this._domContent);
            }
        }
        private _getContent(): HTMLElement {
            let eleContent: HTMLElement;
            try {
                eleContent = wijmo.getElement(this.content);
            }
            catch (e) { }

            if (eleContent) {
                return eleContent;
            }

            return wijmo.createElement(this.content);
        }

        _refreshContent() {
            if (this._eleContent != null) {
                wijmo.Control.refreshAll(this._eleContent);
            }
        }

        private _bindEvents() {
            this.addEventListener(this.hostElement, 'mousedown', (e: MouseEvent) => {
                if (e.button === 0) {
                    this.activate();
                }
            }, true);
            this.layout.dashboard._addMovingTrigger(this._eleHeader);
        }

        private _drawMenus() {
            if (!this.showToolbar) {
                return;
            }

            let eleMenus = document.createElement('div');
            if (this.showHeader) {
                this._eleHeader.appendChild(eleMenus);
            } else {
                this._contentElement.appendChild(eleMenus);
            }
            this._toolbarMenus = new c1.nav.Toolbar(eleMenus, this);
            this._toolbarMenus.deferUpdate(() => {
                if (this._isMaximizable()) {
                    this._toolbarMenus.insertSVGButton(SVGButtonType.FullScreen);
                }

                if (this._couldBeHidden()) {
                    this._toolbarMenus.insertSVGButton(SVGButtonType.Hide);
                }

                if (this._couldShowAll()) {
                    this._toolbarMenus.insertSVGButton(SVGButtonType.ShowAll);
                }
            });
        }
        private _updateToolbar() {
            if (this.showToolbar) {
                if (this._toolbarMenus) {
                    wijmo.removeClass(this._toolbarMenus.hostElement, 'wj-dl-hidden');
                } else {
                    this._drawMenus();
                }
            } else {
                if (this._toolbarMenus) {
                    wijmo.addClass(this._toolbarMenus.hostElement, 'wj-dl-hidden');
                }
            }
        }
        _refreshToolbar() {
            if (this._toolbarMenus) {
                this._toolbarMenus.invalidate();
            }
        }

        _updateSize(size: wijmo.Size) {
            this.hostElement.style.width = size.width + 'px';
            this.hostElement.style.height = size.height + 'px';

            if (this._eleContent) {
                let isWidthOverflow = this._eleContent.scrollWidth > this._eleContent.clientWidth,
                    isHeightOverflow = this._eleContent.scrollHeight > this._eleContent.clientHeight;

                wijmo.toggleClass(this._eleContent, "wj-width-overflow", isWidthOverflow != isHeightOverflow && isWidthOverflow);
                wijmo.toggleClass(this._eleContent, "wj-height-overflow", isWidthOverflow != isHeightOverflow && isHeightOverflow);
            }
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (fullDispose) {
                this._saveContentSelectorElement();
                this._eleContent = null;
                this._eleHeader = null;
                this._isPlaceHolder = false;
                this._isMaximum = false;
            }

            super.dispose(fullDispose);
        }

        /**
         * Acitvates the current tile.
         */
        activate() {
            if (this._isActived) {
                return;
            }

            this._setAcitved(true);
            let argsActived = new TileEventArgs();
            argsActived.tile = this;
            this._onTileActivated(argsActived);
        }

        /**
         * Deacitvates the current tile.
         */
        deactivate() {
            this._setAcitved(false);
        }

        private _setAcitved(active: boolean = true) {
            if (this._isActived == active) {
                return;
            }

            this._isActived = active;
            this._updateAcitved();
        }

        // Maximizes or restores the tile.
        _toggleMaximum() {
            if (!this.hostElement || !this._isMaximizable) {
                return;
            }

            this._isMaximum = !this._isMaximum;
            let eArgs = new TileSizeChangedEventArgs();
            eArgs.isMaximum = this._isMaximum;
            eArgs.tile = this;
            this._onTileSizeChanged(eArgs);
        }

        /**
         * Begins to be moved.
         */
        beginMove() {
            this._isPlaceHolder = true;
            wijmo.addClass(this.hostElement, 'placeholder');
        }

        /**
         * The moving is to be ended.
         */
        endMove() {
            this._isPlaceHolder = false;
            wijmo.removeClass(this.hostElement, 'placeholder');
            this.hostElement.appendChild(this._contentElement);
        }
        private _updateAcitved() {
            if (!this.hostElement) {
                return;
            }

            wijmo.toggleClass(this.hostElement, 'active', this._isActived);
        }

        // Gets the relatvie position to its parent.
        _getPosition(): wijmo.Point {
            let rectTile = this.hostElement.getBoundingClientRect(),
                parentLocation = new wijmo.Point(0, 0),
                scrollPosition = new wijmo.Point(0, 0),
                parentElement: HTMLElement;

            if (this.parent instanceof Group) {
                parentElement = (<Group>this.parent).hostElement;
            } else if (this.parent instanceof LayoutBase) {
                let dashboard = (<LayoutBase>this.parent).dashboard;
                if (dashboard) {
                    parentElement = dashboard.container;
                }
            }

            if (parentElement) {
                let parentRect = parentElement.getBoundingClientRect();
                parentLocation.x = parentRect.left;
                parentLocation.y = parentRect.top;
                scrollPosition = new wijmo.Point(parentElement.scrollLeft, parentElement.scrollTop);
            }

            return new wijmo.Point(rectTile.left - parentLocation.x + scrollPosition.x, rectTile.top - parentLocation.y + scrollPosition.y);
        }

        _getMovingElement(): HTMLElement {
            let movingTileElement = this.hostElement.cloneNode() as HTMLElement;
            wijmo.addClass(movingTileElement, 'moving');
            _removeClass(movingTileElement, 'active');
            _removeClass(movingTileElement, 'wj-tile-resizable');
            movingTileElement.appendChild(this._contentElement);
            return movingTileElement;
        }
    }

    /**
     * Defines the Group class which includes Tiles and Groups as its children.
     */
    export class Group extends LayoutItem {
        static _CLASS_GROUP: string = 'wj-dl-group';

        private _children: LayoutItemCollection;

        /**
         * Initializes a new @see:Group.
         *
         * @param opts JavaScript object containing initialization data for the group.
         */
        constructor(opts?: any) {
            super(null);
            this.initialize(opts);
        }

        /**
         * Gets the child layoutitems.
         */
        get children(): LayoutItemCollection {
            if (!this._children) {
                this._children = this._getDefaultItems();
                this._children.collectionChanged.addHandler(() => {
                    if (this.layout) {
                        this.layout.invalidate();
                    }
                }, this);
            }
            return this._children;
        }

        // gets the default value of items.
        // For different layouts, we need override this method 
        // to get the default value with different types.
        _getDefaultItems(): LayoutItemCollection {
            return new _GroupChildren(this);
        }

        // Initializes the dom elements, styles and related resources for the group.
        _initializeComponent() {
            super._initializeComponent();
            wijmo.addClass(this.hostElement, Group._CLASS_GROUP);
        }

        /**
         * Draws the group.
         */
        draw() {
            this.children.forEach(child => (<LayoutItem>child).render(this._contentElement));
        }

        _redraw() {
            this.draw();
        }

        /**
         * Disposes the object.
         *
         * @param fullDispose A boolean value decides wehter to keep the current status when disposing.
         * If true, all the current status will be cleared. Otherwise, keep the current status.
         */
        dispose(fullDispose = true) {
            if (this.children) {
                this.children.forEach(li => {
                    let liItem = li as LayoutItem;
                    liItem.dispose(fullDispose);
                });
            }

            super.dispose(fullDispose);
        }


        // Gets the definition for the settings of the group.
        _getDefinition() {
            let definition = super._getDefinition();

            if (this.children && this.children.length) {
                let children = [];
                this.children.forEach(child => {
                    let li = child as LayoutItem;
                    children.push(li._getDefinition());
                });
                definition['children'] = children;
            }

            return definition;
        }

        _copy(key: string, value: object) {
            if (key == 'children') {
                if (value) {
                    let arrLI = value as Array<any>;
                    if (arrLI instanceof Array) {
                        arrLI.forEach(li => this.children.push(this._createLayoutItem(li)));
                    }
                }
                return true;
            }

            return false;
        }

        // create the layout item object from the options.
        private _createLayoutItem(opts: any): LayoutItem {
            if (!opts) {
                return null;
            }

            if (opts.children) {
                // creat group
                return this._createGroup(opts);
            } else {
                // create tile.
                return this._createTile(opts);
            }
        }

        // creates a tile object from the options.
        _createTile(tileOpts): Tile {
            throw LayoutBase._EXCEPTION_ABSTRACT;
        }

        // creates a group object from the options.
        _createGroup(groupOpts): Group {
            throw LayoutBase._EXCEPTION_ABSTRACT;
        }
    }

    /**
     * Defines the class which represents the collection of the layoutitems.
     */
    export class LayoutItemCollection extends wijmo.collections.ObservableArray {
        static _INVALID_ITEM_TYPE = 'The child type is a invalid type!';
        private readonly _parent: any;
        private _itemType: any;
        _isInternalAddRemove: boolean;

        /**
         * Initializes a new @see:LayoutItemCollection.
         *
         * @param parent The object owns this collection.
         */
        constructor(parent?: any) {
            super();
            this._parent = parent;
        }


        /**
         * Adds one or more items to the end of the array.
         *
         * @param ...item One or more items to add to the array.
         * @return The new length of the array.
         */
        push(...item: any[]): number {
            let length = this.length;
            item.forEach(li => {
                this._preProcess(li);
                length = super.push(li);
            });

            return length;
        }

        /**
         * Removes and/or adds items to the array.
         *
         * @param index Position where items will be added or removed.
         * @param count Number of items to remove from the array.
         * @param item Item to add to the array.
         * @return An array containing the removed elements.
         */
        splice(index: number, count: number, item?: any): any[] {
            if (item) {
                this._preProcess(item);
            }

            let result = item ? super.splice(index, count, item) : super.splice(index, count);
            if (result) {
                for (let i = 0; i < result.length; i++) {
                    this._processRemoving(result[i]);
                }
            }
            return result;
        }

        /**
         * Assigns an item at a specific position in the array.
         *
         * @param index Position where the item will be assigned.
         * @param item Item to assign to the array.
         */
        setAt(index: number, item: any) {
            this._preProcess(item);
            return super.setAt(index, item);
        }

        /*
         * Removes the last item from the array.
         *
         * @return The item that was removed from the array.
         */
        pop(): any {
            let item = super.pop();
            this._processRemoving(item);
            return item;
        }


        // check whether the item is valid.
        _isValidType(item: any) {
            return item;
        }

        private _checkType(item: any) {
            if (!this._isValidType(item)) {
                throw LayoutItemCollection._INVALID_ITEM_TYPE;
            }
        }

        private _preProcess(item: LayoutItem) {
            this._checkType(item);
            if (item) {
                item._setParent(this._parent);
            }
        }

        _processRemoving(item: any) {
            let layoutItem = <LayoutItem>item;
            layoutItem._setParent(null);
            if (!this._isInternalAddRemove) {
                LayoutItemCollection._saveContentSelectorElement(layoutItem);
            }
        }

        private static _saveContentSelectorElement(item: LayoutItem) {
            if (item instanceof Tile) {
                (<Tile>item)._saveContentSelectorElement();
            } else {
                let group = item as Group;
                group.children.forEach(child => LayoutItemCollection._saveContentSelectorElement(child as LayoutItem));
            }
        }

        _removeInternal(item: any): boolean {
            this._isInternalAddRemove = true;
            let result = this.remove(item);
            this._isInternalAddRemove = false;
            return result;
        }

        _removeAtInternal(index: number) {
            this._isInternalAddRemove = true;
            this.removeAt(index);
            this._isInternalAddRemove = false;
        }

        _insertAtInternal(item: any, index?: number) {
            if (index == null) {
                index = this.length;
            }
            this._isInternalAddRemove = true;
            this.insert(index, item);
            this._isInternalAddRemove = false;
        }

        /**
         * Raises the @see:collectionChanged event.
         *
         * @param e Contains a description of the change.
         */
        onCollectionChanged(e = wijmo.collections.NotifyCollectionChangedEventArgs.reset) {
            if (this._isInternalAddRemove) {
                return;
            }
            super.onCollectionChanged(e);
        }
    }

    /**
     * Defines the class which represents the collection of the group children.
     */
    export class _GroupChildren extends LayoutItemCollection {
        // Overrides to remove the group when there is no children.
        _processRemoving(item: any) {
            let layoutItem = <LayoutItem>item;
            if (layoutItem && layoutItem.parent && layoutItem.parent instanceof Group) {
                let group = <Group>layoutItem.parent;
                if (group.children.length === 0) {
                    this._removeGroup(group);
                }
            }
            super._processRemoving(item);
        }

        // Removes the group which has no child.
        _removeGroup(group: Group) {
            group._remove(this._isInternalAddRemove);
        }
    }
}