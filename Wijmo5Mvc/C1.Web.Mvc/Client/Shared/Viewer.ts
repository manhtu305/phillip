﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.viewer.d.ts"/>

/**
 * Defines the viewer controls and associated classes.
 */
module c1.viewer {
    /**
     * The @see:ReportViewer control extends the @see:wijmo.viewer.ReportViewer control.
     */
    export class ReportViewer extends wijmo.viewer.ReportViewer {
    }

    /**
     * The @see:PdfViewer control extends the @see:wijmo.viewer.PdfViewer control.
     */
    export class PdfViewer extends wijmo.viewer.PdfViewer {
    }
}