﻿module c1.grid.sheet {
    /**
     * Define the FormulaBar class.
     */
    export class FormulaBar extends wijmo.Control {
        _txtInput: HTMLInputElement;
        _selection: HTMLElement;
        _container: HTMLElement;
        _inputContainer: HTMLElement;
        _tips: HTMLElement;
        _owner: FlexSheet;
        _pendingAction: wijmo.grid.sheet._EditAction;

        /**
         * Gets or sets the template used to instantiate @see:GroupPanel controls.
         */
        static controlTemplate = '<div wj-part="container">' +
        '<div wj-part="selection"></div>' +
        '<div wj-part="tips">fx</div>' +
        '<div wj-part="inputContainer"><input wj-part="input" type="text" /></div>' +
        '</div>';

        /**
         * Initializes a new instance of the @see:FormulaBar class.
         *
         * @param element The DOM element that hosts the control, or a selector for the host element (e.g. '#theCtrl').
         * @param owner The FlexSheet object which FormulaBar works for.
         */
        constructor(element: any, owner?: FlexSheet) {
            super(element);

            var self = this;

            // instantiate and apply template
            self.applyTemplate('wj-formulabar wj-control', self.getTemplate(), {
                _container: 'container',
                _selection: 'selection',
                _inputContainer: 'inputContainer',
                _tips: 'tips',
                _txtInput: 'input'
            });

            // events
            self.addEventListener(self._txtInput, 'keydown', self._inputKeyDown.bind(this));
            self.addEventListener(self._txtInput, 'keyup', self._inputKeyUp.bind(this));
            self.addEventListener(self._txtInput, 'focus', self._inputFocus.bind(this));

            self._applyStyle();

            if (owner) {
                self.flexSheet = owner;
            }
        }

        get flexSheet(): FlexSheet {
            return this._owner;
        }
        set flexSheet(value: FlexSheet) {
            var self = this;
            value = <FlexSheet>wijmo.asType(value, FlexSheet, true);
            if (value != self._owner) {
                if (self._owner) {
                    self._owner.selectedSheetChanged.removeHandler(self._updateSelection);
                    self._owner.selectionChanged.removeHandler(self._updateSelection);
                    self._owner.cellEditEnded.removeHandler(self._updateSelection);
                    self._owner.undoStack.undoStackChanged.removeHandler(self._updateSelection);
                }

                self._owner = value;
                if (self._owner) {
                    self._owner.selectedSheetChanged.addHandler(self._updateSelection, self);
                    self._owner.selectionChanged.addHandler(self._updateSelection, self);
                    self._owner.cellEditEnded.addHandler(self._updateSelection, self);
                    self._owner.undoStack.undoStackChanged.addHandler(self._updateSelection, self);
                }
            }

            // update the display
            self.invalidate();
        }

        refresh() {
            super.refresh();
            this._updateSelection();
            this._adjustSize();
        }

        _applyStyle() {
            var self = this;
            wijmo.addClass(self._container, 'wj-content');
            wijmo.addClass(self._selection, 'wj-selection');
            wijmo.addClass(self._tips, 'wj-tips wj-header');
            wijmo.addClass(self._inputContainer, 'wj-inputContainer');
            wijmo.addClass(self._txtInput, 'wj-txtinput');
            self._adjustSize();
        }

        _adjustSize() {
            var self = this,
                height = self._container.clientHeight,
                padding: number = 0,
                cStyle: CSSStyleDeclaration;

            // set the lineHeight style of the selection section.
            cStyle = window.getComputedStyle(self._selection);
            if (cStyle) {
                padding = self._pixelToNumber(cStyle.borderTopWidth) + self._pixelToNumber(cStyle.borderBottomWidth);
            }
            self._selection.style.lineHeight = (height - padding) + 'px';

            // set the lineHeight style of the tips section.
            padding = 0;
            cStyle = window.getComputedStyle(self._tips);
            if (cStyle) {
                padding = self._pixelToNumber(cStyle.borderTopWidth) + self._pixelToNumber(cStyle.borderBottomWidth);
            }
            self._tips.style.lineHeight = (height - padding) + 'px';
        }

        _pixelToNumber(strPixelValue: string): number {
            var nValue = 0;
            if (strPixelValue) {
                nValue = parseFloat(strPixelValue.replace('px', ''));
                if (!nValue || isNaN(nValue)) {
                    nValue = 0;
                }
            }

            return nValue;
        }

        // clear the display for formula bar.
        _reset() {
            this._txtInput.value = '';
            this._selection.innerHTML = '&nbsp;';
        }

        // update the display for formula bar.
        _updateSelection() {
            var self = this, sel: wijmo.grid.CellRange,
                flexSheet = self.flexSheet,
                rowCnt :number, colCnt: number,
                r: number, c: number;
            if (!flexSheet) {
                self._reset();
                return;
            }

            rowCnt = flexSheet.rows.length;
            colCnt = flexSheet.columns.length;
            sel = flexSheet.selection;
            if (sel.row > -1 && sel.col > -1 && rowCnt > 0 && colCnt > 0
                && sel.col < colCnt && sel.col2 < colCnt
                && sel.row < rowCnt && sel.row2 < rowCnt) {
                r = sel.row >= rowCnt ? rowCnt - 1 : sel.row;
                c = sel.col >= colCnt ? colCnt - 1 : sel.col;
                self._txtInput.value = flexSheet.getCellData(r, c, true);
                self._selection.innerText = FlexSheet.convertNumberToAlpha(sel.col) + (sel.row + 1);
            } else {
                self._reset();
            }
        }

        // handles the key down event to update the content of the selected flexSheet cell
        _inputKeyDown(e: KeyboardEvent) {
            var self = this, keyCode = e.keyCode,
                flexSheet = self._owner,
                selection: wijmo.grid.CellRange;

            if (!flexSheet) {
                return;
            }

            selection = flexSheet.selection;
            if (!selection || selection.row <= -1 || selection.col <= -1) {
                return;
            }

            if (keyCode) {
                if (flexSheet.isFunctionListOpen) {
                    switch (keyCode) {
                        case 38:
                            flexSheet.selectPreviousFunction();
                            e.preventDefault();
                            return;
                        case 40:
                            flexSheet.selectNextFunction();
                            e.preventDefault();
                            return;
                        case 9:
                        case 13:
                            flexSheet.applyFunctionToCell();
                            e.preventDefault();
                            return;
                        case 27:
                            flexSheet.hideFunctionList();
                            e.preventDefault();
                            return;
                    }
                }

                if (keyCode !== 13) {
                    return;
                }
            }

            if (flexSheet.isFunctionListOpen) {
                setTimeout(function () {
                    flexSheet.hideFunctionList();
                }, 200);
            } else {
                flexSheet.setCellData(selection.row, selection.col, self._txtInput.value, true);
                if (self._pendingAction instanceof wijmo.grid.sheet._EditAction && self._pendingAction.saveNewState()) {
                    flexSheet.undoStack._addAction(self._pendingAction);
                }
                self._pendingAction = null;
                flexSheet.refresh(false);
            }
        }

        // handles the key up event to open the function list
        _inputKeyUp(e: KeyboardEvent) {
            var keyCode = e.keyCode,
                flexSheet = this.flexSheet,
                sel: wijmo.grid.CellRange;

            if (!flexSheet) {
                return;
            }

            sel = flexSheet.selection;
            if (!sel || sel.row <= -1 || sel.col <= -1) {
                return;
            }

            if (!keyCode || keyCode > 40 || keyCode < 32 && keyCode !== 27) {
                this._owner.showFunctionList(<HTMLElement>e.target);
            }
        }

        // handles the input focus to support undo/redo for modifying the formula of the cell.
        _inputFocus() {
            var sel: wijmo.grid.CellRange,
                flexSheet = this.flexSheet;

            if (!flexSheet) {
                return;
            }

            sel = flexSheet.selection;
            if (!sel || sel.row <= -1 || sel.col <= -1) {
                return;
            }

            this._pendingAction = new wijmo.grid.sheet._EditAction(flexSheet);
        }
    }
}