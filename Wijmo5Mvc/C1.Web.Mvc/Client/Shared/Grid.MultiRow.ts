﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.multirow.d.ts" />

/**
 * Defines the @see:c1.grid.multirow.MultiRow control and associated classes.
 */
module c1.grid.multirow {

    /**
     * The @see:MultiRow control extends from @see:wijmo.grid.multirow.MultiRow.
     */
    export class MultiRow extends wijmo.grid.multirow.MultiRow {
    }
}
