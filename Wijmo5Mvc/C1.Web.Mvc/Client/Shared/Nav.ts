﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.nav.d.ts" />

/**
 * Defines the navigation components and associated classes.
 */
module c1.nav {

    /**
     * The @see:TreeView extends from @see:wijmo.nav.TreeView.
     */
    export class TreeView extends wijmo.nav.TreeView {
    }

    /**
     * The @see:TabPanel extends from @see:wijmo.nav.TabPanel.
     */
    export class TabPanel extends wijmo.nav.TabPanel {
    }
}
