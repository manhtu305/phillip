﻿module c1 {

    var services: any = {};

    // Gets the service instance according to the specified key.
    // If the service doesn't exist in the cache, it will use the provided instance(serviceCtor) and register it in the cache. 
    // Or a new instance will be created by the provided constructor(serviceCtor) and the option setting(opts). Then register the created one in the cache.
    // @param key The service key which is used to indentify in cache.
    // @param serviceCtor It could be a service instance or the constructor of a service object.
    // @param opts It is the option setting which is used to create the corresponding service instance.
    export function _registerService(key: string, serviceCtor?: any, opts?: any): any {
        var ser: any;
        // if only one argument "key" and the service cache has, use it.
        if (arguments.length === 1 && services[key] != null) {
            return services[key];
        }

        if (key.length && !serviceCtor && opts == null) {
            ser = _findFunction(key);
            if (ser == null) {
                throw "service is not registered.";
            }
            services[key] = ser;
            return ser;
        }

        if (!serviceCtor) {
            throw 'serviceCtor is null';
        }

        if (key.length && typeof (serviceCtor) === 'object' && opts == null) {
            services[key] = serviceCtor;
            return serviceCtor;
        }

        if (typeof(serviceCtor) !== 'function') {
            throw 'serviceCtor is not a service constructor.';
        }

        try {
            ser = new (serviceCtor)();
            if (typeof (ser.initialize) !== 'function') {
                ser = new (serviceCtor)(opts);
            } else {
                (<Function>ser.initialize).call(ser, opts);
            }
        } catch (e) {
            throw 'service cannot be created.';
        }

        if (key.length) {
            services[key] = ser;
        }

        return ser;
    }

    export function _disposeService(key: string) {
        delete services[key];
    }

    /**
     * Gets the service instance according to the specified key.
     * Till now two kinds of services could be returned:
     * @see:c1.mvc.collections.RemoteCollectionView and @see:wijmo.olap.PivotEngine.
     *
     * For example:
     *
     * <pre>// gets the CollectionView object which id is 'collectionview1'.
     * var cv = c1.getService('collectionview1');
     * </pre>
     *
     * @param id The service id.
     * @return A service object with the specified id.
     */
    export function getService(id: string): Object {
        return services[id];
    }
}