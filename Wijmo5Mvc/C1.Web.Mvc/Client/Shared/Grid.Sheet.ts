﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.xlsx.d.ts"/>
/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.sheet.d.ts"/>

/**
 * Defines the FlexSheet control and associated classes.
 */
module c1.grid.sheet {
    /**
     * The @see:FlexSheet control extends the @see:wijmo.grid.sheet.FlexSheet control and provides an Excel-like functionality.
     */
	export class FlexSheet extends wijmo.grid.sheet.FlexSheet {
		/*for tracking items deleted from bounded sheet only*/
		__deleteItems = [];
		/*for tracking items inserted from bounded sheet only*/
		__newItems = [];

        /**
         * This constructor is for internal use.
         *
         * Initializes a new instance of a @see:FlexSheet control.
         *
         * @param element The DOM element that will host the control, or a jQuery selector (e.g. '#theCtrl').
         * @param options JavaScript object containing initialization data for the control.
         */
		constructor(element: any, options?: any) {
			super(element, options);
		}

        /**
         * Inserts rows in the current {@link Sheet} of the <b>FlexSheet</b> control.
         *
         * @param index The position where new rows should be added. If not specified then rows will be added
         * before the first row of the current selection.
         * @param count The numbers of rows to add. If not specified then one row will be added.
         */
		insertRows(index?: number, count?: number) {
			this.__newItems = [];
            super.insertRows(index, count);
			if (this.__newItems.length > 0) {
				var remoteCV = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(this.collectionView, c1.mvc.collections.RemoteCollectionView);
				if (remoteCV) {
					// Send request write for new action
					remoteCV.requestWrite(c1.mvc.collections.CommandType.Create, this.__newItems);
					this.__newItems = [];
					return;
				}
			}
		}

        /**
        * Deletes rows from the current {@link Sheet} of the <b>FlexSheet</b> control.
        *
        * @param index The starting index of the deleting rows. If not specified then rows will be deleted
        * starting from the first row of the current selection.
        * @param count The numbers of rows to delete. If not specified then one row will be deleted.
        */
		deleteRows(indexOrRanges?: number | wijmo.grid.CellRange[], count?: number): void {
			this.__deleteItems = [];
			wijmo.isArray(indexOrRanges) ? super.deleteRows(<wijmo.grid.CellRange[]>indexOrRanges) : super.deleteRows(<number>indexOrRanges, count);
			if (this.__deleteItems.length > 0) {
				var remoteCV = <c1.mvc.collections.RemoteCollectionView>wijmo.tryCast(this.collectionView, c1.mvc.collections.RemoteCollectionView);
				if (remoteCV) {
					// Send request write for delete action
					remoteCV.requestWrite(c1.mvc.collections.CommandType.Delete, this.__deleteItems);
					this.__deleteItems = [];
					return;
				}
			}
		}	

		// Update the index of the item in the itemsSource when insert row into the bound sheet.
		_updateItemIndexForInsertingRow(items: any[], newItemIndex: number, rowCount: number) {
			if (items && newItemIndex >= 0 && newItemIndex < items.length) {
				for (let i = 0; i < rowCount; i++) {
					let idx = newItemIndex + i;
					if (idx < items.length) {
						this.__newItems.push(items[idx]);
					}
				}
			}
			super._updateItemIndexForInsertingRow(items, newItemIndex, rowCount);
		}

		// Update the index of the item in the itemsSource when remove row from the bound sheet.
		_updateItemIndexForRemovingRow(items: any[], itemIndex: number) {
			let beginIndex: number = 0;
			let rowHeader = this.rows[0];
			if (wijmo.tryCast(rowHeader, wijmo.grid.sheet.HeaderRow)) {
				beginIndex = 1;
			}
			let deletingRow = this.rows[itemIndex + beginIndex];
			if (deletingRow) {
				let deletingItem = deletingRow.dataItem;
				if (deletingItem) {
					this.__deleteItems.push(deletingItem);
				}
			}

			super._updateItemIndexForRemovingRow(items, itemIndex);

		}

	}
}