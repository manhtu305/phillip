﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.input.d.ts"/>

/*
 * Defines input controls for strings, numbers, dates, times, and colors.
 */
module c1.input {

    /**
     * The @see:c1.input.AutoComplete control extends from @see:wijmo.input.AutoComplete.
     */
    export class AutoComplete extends wijmo.input.AutoComplete {
    }

    /**
     * The @see:c1.input.Calendar control extends from @see:wijmo.input.Calendar.
     */
    export class Calendar extends wijmo.input.Calendar {
    }

    /**
     * The @see:c1.input.ColorPicker control extends from @see:wijmo.input.ColorPicker.
     */
    export class ColorPicker extends wijmo.input.ColorPicker {
    }

    /**
     * The @see:c1.input.ComboBox control extends from @see:wijmo.input.ComboBox.
     */
    export class ComboBox extends wijmo.input.ComboBox {
        /**
         * Gets the string displayed for the item at a given index (as plain text).
         *
         * @param index The index of the item to retrieve the text for.
         */
        getDisplayText(index: number): string {
            // trim space caused by HTML content
            return super.getDisplayText(index).trim();
        }
    }

    /**
     * The @see:c1.input.InputColor control extends from @see:wijmo.input.InputColor.
     */
    export class InputColor extends wijmo.input.InputColor {
    }

    /**
     * The @see:c1.input.InputDate control extends from @see:wijmo.input.InputDate.
     */
    export class InputDate extends wijmo.input.InputDate {
        constructor(element: any) {
            super(element);

            //I don't hope the mousedown event is triggerred on the elements below the
            //dropdown calendar when press mouse down on the calendar.
            this._dropDown && this._dropDown.addEventListener('mousedown', function (e) {
                e.stopPropagation();
            });
        }
    }

    /**
     * The @see:c1.input.InputDateTime control extends from @see:wijmo.input.InputDateTime.
     */
    export class InputDateTime extends wijmo.input.InputDateTime {
    }

    /**
     * The @see:c1.input.InputMask control extends from @see:wijmo.input.InputMask.
     */
    export class InputMask extends wijmo.input.InputMask {
    }

    /**
     * The @see:c1.input.InputNumber control extends from @see:wijmo.input.InputNumber.
     */
    export class InputNumber extends wijmo.input.InputNumber {
    }

    /**
     * The @see:c1.input.InputTime control extends from @see:wijmo.input.InputTime.
     */
    export class InputTime extends wijmo.input.InputTime {
    }

    /**
     * The @see:c1.input.ListBox control extends from @see:wijmo.input.ListBox.
     */
    export class ListBox extends wijmo.input.ListBox {
    }

    /**
     * The @see:c1.input.Menu control extends from @see:wijmo.input.Menu.
     */
    export class Menu extends wijmo.input.Menu {
    }

    /**
     * The @see:c1.input.Popup control extends from @see:wijmo.input.Popup.
     */
    export class Popup extends wijmo.input.Popup {
    }

    /**
     * The @see:c1.input.MultiSelect control extends from @see:wijmo.input.MultiSelect.
     */
    export class MultiSelect extends wijmo.input.MultiSelect {
    }

    /**
     * The @see:Pager control displays a pager navigator for the @see:wijmo.collections.CollectionView.
     */
    export class Pager extends wijmo.Control {

        private _btnFirst: HTMLButtonElement;
        private _btnPrev: HTMLButtonElement;
        private _btnNext: HTMLButtonElement;
        private _btnLast: HTMLButtonElement;
        private _inputPages: HTMLButtonElement;
        private _collectionView: wijmo.collections.CollectionView;
        static controlTemplate = '<div class="wj-input-group">' +
        '        <span class="wj-input-group-btn" >' +
        '            <button wj-part="btn-first" data-action="first" class="wj-btn wj-btn-default" type="button">' +
        '                <span class="wj-glyph-left" style="margin-right: -4px;"></span>' +
        '                <span class="wj-glyph-left"></span>' +
        '            </button>' +
        '        </span>' +
        '        <span class="wj-input-group-btn" >' +
        '            <button wj-part="btn-prev" data-action="prev" class="wj-btn wj-btn-default" type="button">' +
        '                <span class="wj-glyph-left"></span>' +
        '            </button>' +
        '        </span>' +
        '        <input wj-part="pages" type="text" class="wj-form-control" disabled />' +
        '        <span class="wj-input-group-btn" >' +
        '            <button wj-part="btn-next" data-action="next" class="wj-btn wj-btn-default" type="button">' +
        '                <span class="wj-glyph-right"></span>' +
        '            </button>' +
        '        </span>' +
        '        <span class="wj-input-group-btn" >' +
        '            <button wj-part="btn-last" data-action="last" class="wj-btn wj-btn-default" type="button">' +
        '                <span class="wj-glyph-right"></span>' +
        '                <span class="wj-glyph-right" style="margin-left: -4px;"></span>' +
        '            </button>' +
        '        </span>' +
        '    </div>';

        /**
         * Initializes a new instance of a @see:c1.input.Pager.
         *
         * @param element The DOM element that hosts the control, or a selector for the host element (e.g. '#theCtrl').
         * @param options The JavaScript object containing initialization data for the control.
         */
        constructor(element: any, options?) {
            super(element);
            this._initElements();
            this.initialize(options);
        }

        /**
         * Gets or sets the @see:wijmo.collections.CollectionView that own this pager.
         */
        get collectionView(): wijmo.collections.CollectionView {
            return this._collectionView;
        }
        set collectionView(value: wijmo.collections.CollectionView) {
            var cv = wijmo.tryCast(value, wijmo.collections.CollectionView),
                self = this;
            if (cv && cv !== self._collectionView) {
                if (self._collectionView) {
                    self._collectionView.collectionChanged.removeHandler(self._updatePager, self);
                }

                self._collectionView = cv;
                self._collectionView.collectionChanged.addHandler(self._updatePager, self);
                self._updatePager();
            }
        }

        /**
         * Refreshes the pager display.
         *
         */
        refresh() {
            super.refresh(true);

            this._updateContent();
        }

        private _initElements() {
            var self = this, tpl = self.getTemplate(), btns = [];
            self.applyTemplate('wj-control wj-content wj-pager', tpl, {
                _btnFirst: "btn-first",
                _btnPrev: "btn-prev",
                _btnNext: "btn-next",
                _btnLast: "btn-last",
                _inputPages: "pages"
            });
            btns.push(self._btnFirst, self._btnPrev, self._btnNext, self._btnLast);

            btns.forEach(item=> item.addEventListener('click', function () {
                self._movePage(item.getAttribute('data-action'));
            }));
        }

        private _movePage(type: string) {
            var self = this, cv = self.collectionView;
            if (!cv) {
                return;
            }

            switch (type) {
                case "first": cv.moveToFirstPage();
                    break;
                case "prev": cv.moveToPreviousPage();
                    break;
                case "next": cv.moveToNextPage();
                    break;
                case "last": cv.moveToLastPage();
                    break;
            }
        }

        private _updatePager() {
            this.invalidate();
        }

        private _updateContent() {
            var self = this, cv = self.collectionView,
                enableBackwards,
                enableForwards;
            if (!cv) {
                return;
            }

            // update the pager text
            self._inputPages.value = (cv.pageIndex + 1) + ' / ' + (cv.pageCount);

            // determine which pager buttons to enable/disable
            enableBackwards = cv.pageIndex <= 0;
            enableForwards = cv.pageIndex >= cv.pageCount - 1;

            // enable/disable pager buttons
            self._btnFirst.disabled = enableBackwards;
            self._btnPrev.disabled = enableBackwards;
            self._btnNext.disabled = enableForwards;
            self._btnLast.disabled = enableForwards;
        }
    }

    /**
     * The @see:c1.input.MultiAutoComplete control extends from @see:wijmo.input.MultiAutoComplete.
     */
    export class MultiAutoComplete extends wijmo.input.MultiAutoComplete {
    }

    /**
     * The @see:c1.input.CollectionViewNavigator control extends from @see:wijmo.input.CollectionViewNavigator.
     */
    export class CollectionViewNavigator extends wijmo.input.CollectionViewNavigator {
    }

    /**
     * The @see:c1.input.MultiSelectListBox control extends from @see:wijmo.input.MultiSelectListBox.
     */
    export class MultiSelectListBox extends wijmo.input.MultiSelectListBox {
    }
}
