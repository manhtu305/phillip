﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.d.ts" />

module c1.chart {
    /**
     * The @see:LineMarker control extends from @see:wijmo.chart.LineMarker.
     */
    export class LineMarker extends wijmo.chart.LineMarker {

        // Internal use.
        initialize(options) {
            var content = options.content;
            if (typeof window[content] === "function") {
                options.content = window[content];
            }
            wijmo.copy(this, options);
        }

        _copy(key, value) {
            if (key === "positionChanged") {
                if (typeof value === "function") {
                    this.positionChanged.addHandler(value);
                }
                return true;
            }
            return false;
        }
    }
}