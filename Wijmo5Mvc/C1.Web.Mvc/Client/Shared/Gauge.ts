﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.gauge.d.ts"/>

/**
 * Defines the C1 gauge controls.
 */
module c1.gauge {

    /**
     * The @see:BulletGraph control extends from @see:wijmo.gauge.BulletGraph.
     */
    export class BulletGraph extends wijmo.gauge.BulletGraph {
    }

    /**
     * The @see:LinearGauge control extends from @see:wijmo.gauge.LinearGauge.
     */
    export class LinearGauge extends wijmo.gauge.LinearGauge {
    }

    /**
     * The @see:RadialGauge control extends from @see:wijmo.gauge.RadialGauge.
     */
    export class RadialGauge extends wijmo.gauge.RadialGauge {
    }
}