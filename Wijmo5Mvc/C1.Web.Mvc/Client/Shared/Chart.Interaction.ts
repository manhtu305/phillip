﻿/// <reference path="../../../../Shared/WijmoMVC/dist/controls/wijmo.chart.interaction.d.ts" />

/**
 * Defines classes that add interactive features to charts.
 */
module c1.chart.interaction {
    /**
     * The @see:RangeSelector control extends from @see:wijmo.chart.interaction.RangeSelector.
     */
    export class RangeSelector extends wijmo.chart.interaction.RangeSelector {
        private __chart: wijmo.chart.FlexChartCore;

        /**
         * Initializes a new instance of the @see:RangeSelector class.
         *
         * @param chart The @see:wijmo.chart.FlexChart that displays the selected range.
         * @param options A JavaScript object containing initialization data for the control.
         */
        constructor(chart: wijmo.chart.FlexChartCore, options?) {
            super(chart, options);
            this.__chart = chart;
        }

        // Internal use.
        initialize(options: any) {
            wijmo.copy(this, options);
            // [290701] force to refresh the chart to make sure the RangeSelector updates the min/max values.
            this.__chart.refresh();
        }

        _copy(key, value) {
            if (key === "rangeChanged") {
                if (typeof value === "function") {
                    this.rangeChanged.addHandler(value);
                }
                return true;
            }
            return false;
        }
    }

    /**
     * The @see:ChartGestures control extends from @see:wijmo.chart.interaction.ChartGestures.
     */
    export class ChartGestures extends wijmo.chart.interaction.ChartGestures {
    }
}