///<reference path="../Mvc/Util.ts"/>

/**
 * Defines the @see:FileManager control, different layouts and associated classes.
 */
module c1.nav {
    'use strict';

    wijmo._addCultureInfo('FileManager', {
        upload: 'Upload',
        refresh: 'Refresh',
        create: 'New Folder',
        delete: 'Delete',
        download: 'Download',
        moveFile: 'Move File',
        close: 'Close',
        none: 'None',
        cancel: 'Cancel',
        createFolder1: 'Please enter your folder name!',
        createFolder2: 'Create folder success.',
        uploadFile1: 'Upload a file to cloud',
        uploadFile2: 'You are uploading file :',
        uploadFile3: 'Please choose a folder to upload file in.',
        uploadFile4: 'Upload success.',
        uploadFile5: 'Browse File',
        download1: 'Please select a file to download!',
        moveFile1: 'You must choose a file to move first!',
        moveFile2: 'Please choose target path to move file to!',
        moveFile3: 'Move file success',
        moveFile4: 'Select a folder to move file to',
        delete1: 'Do you want to delete this folder and all files inside?',
        delete2: 'Should choose atleast one folder or file to delete!',
        delete3: 'Delete success.',
        error1: 'Must set value for ContainerName when using Azure, OneDrive cloud or LocalStorage!',
        error2: 'Must set value for RootFolder or InitPath!',
    });

    class FolderFiles {
        Reload: boolean;
        Image: string;
        ParentPath: string = null;
        Name: string;
        Type: number;
        ItemID: string;
        ItemUrl: string;
        SubFolder: FolderFiles[] = FolderFiles[0];
        constructor() {
        }
    }

    // this code only for declaring number defined for cloudtype
    // unused code
    //========================================================
    export enum CloudName {
        DropBox = 2,
        GoogleDrive = 3,
        Azure = 0,
        AWS = 1,
        OneDrive= 4,
        LocalStorage = 5
    }
    //========================================================

    export class FileManager extends wijmo.Control {
        
        _btnRefresh: HTMLElement;
        _btnUploadData: HTMLButtonElement;
        _btnCreateFolder: HTMLElement;
        _spnFileName: HTMLElement;
        _btnConfirmMovefile: HTMLButtonElement;
        _btnConfirmUpload: HTMLElement;
        _moveFilePopup: HTMLElement;
        _uploadFilePopup: HTMLElement;
        _treeViewDiv: HTMLDivElement;
        _treeViewMoveFileDiv: HTMLDivElement;
        _listBoxDiv: HTMLDivElement;
        _navigateUL: HTMLUListElement;
        _slView: HTMLSelectElement;

        private _moveFile: HTMLElement;
        private _download: HTMLElement;
        private _upload: HTMLElement;
        private _delete: HTMLElement;
        private _btnMoveUp: HTMLElement;
        private _btnSearch: HTMLElement;
        private _txtSearch: HTMLElement;
        private _list: HTMLElement;
        private _detail: HTMLElement;
        private _detailView: HTMLElement;
        private _thumb: HTMLElement;

        _displayMode: any = 0;
        _slCloud: HTMLSelectElement;
        _selectedNode: any = null;
        _parentNode: any;
        _treeView: any;
        _listBox: any;
        _type: number;
        _parentPath: string;
        _fileName: string;
        _itemId: string;
        _uploadingFile: any;

        _initPath: string;
        _rootFolder: string;
        _containerName: string;
        _cloudName: string;
        _cloudType: CloudName;
        // cloud name + / + container name
        _rootPath: string;
        _targetUrl: string;
        // for move file
        _targetPath: string;
        _originalFileName: string;
        _downloadUrl: string;
        _id: string = '';
        _hostUrl: string; // for example http://localhost:61712
        _navigateArr: any[] = [];

        _isReadOnly: boolean = false;
        /**
        * function to return image path for each extention type
        * for example
            function fileExtentionImageFormater(fileExtention)
            {
                switch (fileExtention)
                {
                    case "xlsx":
                        return "../../Content/images/xlsx.png"
                    case "pdf":
                        return "../../Content/images/pdf.png"
                    default:
                        return "../../Content/images/folders.png"
                }
            }
        */
        _fileExtentionImageFormater: Function = null;
        /**
         * Gets or sets a string value of Host Url.
         */
        get isReadOnly(): boolean {
            return this._isReadOnly;
        }
        set isReadOnly(value: boolean) {
            this._isReadOnly = value;
        }


        /**
         * Gets or sets a string value of Host Url.
         */
        get hostUrl(): string {
            return this._hostUrl;
        }
        set hostUrl(value: string) {
            this._hostUrl = value;
        }

        /**
         * Gets or sets a string value of initpath.
         */
        get initPath(): string {
            return this._initPath;
        }
        set initPath(value: string) {
            this._initPath = value;
        }

        /**
         * Gets or sets a string value of RootFolder.
         */
        get rootFolder(): string {
            return this._rootFolder;
        }
        set rootFolder(value: string) {
            this._rootFolder = value;
        }

        /**
         * Gets or sets a string value of ContainerName.
         */
        get containerName(): string {
            return this._containerName;
        }
        set containerName(value: string) {
            this._containerName = value;
        }

        /**
         * Gets or sets a string value of cloud type.
         */
        get cloudType(): CloudName {
            return this._cloudType;
        }
        set cloudType(value: CloudName) {
            this._cloudType = value;
            this._cloudName = this.getCloudName()
        }

        /**
         * Gets or sets a string value of image formater of file extention.
         */
        get fileExtentionImageFormater(): Function {
            return this._fileExtentionImageFormater;
        }
        set fileExtentionImageFormater(value: Function) {
            if (value) {
                this._fileExtentionImageFormater = wijmo.asFunction(value);
            }
        }

        /**
         * Gets or sets the template used to instantiate @see:FileManager control.
         */
        static controlTemplate =
            '<nav class="fm-navbar pb-filemng-navbar" >' +
            '<div class="fm-container-fluid" style="margin-top:10px;margin-bottom:10px">' +
            '<div class="fm-row">' +
            '<div class="fm-col-md-12" style="padding-left:0px!important;">' +
            '<div wj-part="move-file"></div>' +
            '<div wj-part = "moveFilePopup" class="wj-dialog" > ' +
            '<div class="wj-dialog-header"> ' + wijmo.culture.FileManager.moveFile4 + '</div>' +
            '<div class="wj-dialog-body" > ' +
            //'<form name="popoverForm" > ' +
            '<div wj-part ="tree-view-move-file"></div>' +
            '<div class="wj-dialog-footer fm-dialog-footer" > ' +
            '<button class="wj-hide-ok wj-btn wj-btn-default" wj-part="btn-confirm-movefile" >OK</button>' +
            '<button class="wj-hide-cancel wj-btn wj-btn-default">' + wijmo.culture.FileManager.close + '</button>' +
            '</div>' +
            //'</form>' +
            '</div>' +
            '</div>' +
            '<div wj-part="btn-create-folder"></div>' +
            '<div wj-part="btn-refresh"></div>' +
            '<div wj-part="download"></div>' +
            '<div wj-part="up-load"></div>' +
            '<div wj-part = "upload-file-popup" class="class="wj-dialog"" style = "text-align:center;min-width:250px;" >' +
            '<div class="wj-dialog-header"> ' + wijmo.culture.FileManager.uploadFile1 + '</div>' +            
            '<div class="wj-dialog-body" >' +
            '<div class="pb-filemng-template-body" >' +
            '<span>' + wijmo.culture.FileManager.uploadFile2 + '</span></br >' +
            '<span wj-part="spn_fileName">' + wijmo.culture.FileManager.none + '</span>' +
            '<div class="wj-dialog-body" >' +
            '<span class="wj-btn-file fm-btn fm-btn-default btn-file wj-control" >' +
            wijmo.culture.FileManager.uploadFile5 + '<input wj-part="btn-upload-data" type = "file" name = "file" accept = ".xlsx" >' +
            '</span></div>' +
            '<div class="wj-dialog-footer fm-dialog-footer" >' +
            '<button type="button" class="wj-hide-ok wj-btn wj-btn-default" wj-part="btn-confirm-upload" >OK</button>' +
            '<button class="wj-hide-cancel wj-btn wj-btn-default" >' + wijmo.culture.FileManager.cancel + '</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div wj-part="delete"></div>' +

            '<div style="float:right;height:32px;width:120px;">' +

            // add later for mobile support

            //'<select wj-part="sl-view" style = "height:30px;" class="browser-default custom-select custom-select-lg  mb-3" >' +
            //'<option value = "1" selected> List View </option>' +
            //'<option value = "2" > Details View </option>' +
            //'<option value = "3" > Thumbnail View </option>' +
            //'</select>' +
            '<div wj-part="list" style="margin-left:10px;height:32px;float:left;"></div>' +
            '<div wj-part="detail" style="margin-left:10px;height:32px;float:left;"></div>' +
            '<div wj-part="thumb" style="height:32px;float:left;"></div>' +
            '</div>' +

            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</nav>' +
            '<nav class="navbar pb-filemng-navbar" style="border: 1px solid #ddd;height:100%;">' +
            '<div class="fm-container-fluid" style="margin-top:10px;margin-bottom:5px">' +
                '<div class="fm-row">' +                    
            '<div class="fm-col-md-9 fm-col-xs-12" style="padding-left:0px!important;margin-bottom:5px;">' +
            '<div style="display:flex;">' +
            '<div wj-part="btn-move-up" style="margin-right:10px"></div>' +
            '<div class="fm-nav-link wj-control" style="height:33px;border:1px solid #ccc;width:100%;" >' +
            '<ul class="fm-navbar-collapse fm-nav fm-navbar-nav fm-navbar-left" wj-part="navigate-Ul" style="padding-top:4px;list-style-type:none;"></ul>' +
            '</div>' +
            '</div>' +
            '</div>' +
                    '</div>' + 
            '<div class="fm-col-md-3 fm-col-xs-12" style="margin-bottom:5px">' +
            '<div class="fm-input-group">' +
            '<span class="fm-input-group-addon fm-search-btn wj-control fm-svg" style="height:33.1px;"><div wj-part="btn-search"></div></span>' +
            '<input type="text" class="fm-form-control fm-search-input wj-control" wj-part="search-txt" placeholder="Search..." />' +
            '</div>' +
            '</div>' +
                '</div>' + 
            '</div>' +
            '</nav>' +
            '<div class="fm-panel fm-panel-default wj-control" style="border: 1px solid #ddd;">' +
            '<div class="fm-container-fluid">' +
            '<div class="fm-row">' +
            '<div class="fm-col-sm-3 fm-col-md-4 fm-col-xs-6 pb-filemng-template-treeview" style="padding-left:0px!important;">' +
            '<div wj-part="tree-view-file-manager" style="height:365px;margin-top:10px;margin-bottom:10px"></div>' +
            '</div>' +
            '<div class="fm-col-sm-9 fm-col-md-8 fm-col-xs-6">' +
            '<div class="fm-container-fluid" style="padding-left:0px!important;width:115%;">'+
            '<div class="fm-row border fm-header-max" style="width:100.8%;" wj-part="detail-view">'+
            '<div class="fm-col-sm-4 border-left fm-header-max" >Name</div>' +
            '<div class="fm-col-sm-3 border-left fm-header-max" >Modified Date</div>' +
            '<div class="fm-col-sm-2 border-left fm-header-max" >Size</div>' +
            '<div class="fm-col-sm-2 border-left fm-header-max" > </div>' +
            '</div>' +
            '<div wj-part="list-box" style="width:100%;border:none;margin-top:10px;margin-bottom:10px;">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
            ;

        /**
         * Initializes a new instance of the @see:FileManager class.
         *
         * @param element The DOM element that will host the control, or a selector for the host element (e.g. '#theCtrl').
         * @param options JavaScript object containing initialization data for the control.
         */
        constructor(element: any, options?: any) {
            super(element, options, true);
            let tpl = this.getTemplate();
            this.applyTemplate('wj-control wj-file-manager', tpl, {
                _btnRefresh: 'btn-refresh',
                _btnUploadData: 'btn-upload-data',
                _btnCreateFolder: 'btn-create-folder',
                _spnFileName: 'spn_fileName',
                _btnConfirmMovefile: 'btn-confirm-movefile',
                _uploadFilePopup:'upload-file-popup',
                _moveFile: 'move-file',
                _download: 'download',
                _upload: 'up-load',
                _btnConfirmUpload: 'btn-confirm-upload',
                _delete: 'delete',
                _detail: 'detail',
                _list:'list',
                _thumb: 'thumb',
                _detailView:'detail-view',
                _moveFilePopup:'moveFilePopup',
                _btnMoveUp: 'btn-move-up',
                _btnSearch: 'btn-search',
                _txtSearch: 'search-txt',
                _treeViewDiv: 'tree-view-file-manager',
                _treeViewMoveFileDiv: 'tree-view-move-file',
                _listBoxDiv: 'list-box',
                _navigateUL: 'navigate-Ul',
                //_slView: 'sl-view',
            });

            let addListener = this.addEventListener.bind(this);

            addListener(this._listBoxDiv, 'click', (s, e) => { 
                var seft = this;
                if (this._listBox.itemsSource != undefined && this._listBox.itemsSource.length > 0) {
                    var itemName = undefined;
                    if (s.path != undefined)// chrome
                    {
                        itemName = s.path[0].innerText == undefined ? s.path[1].innerText : s.path[0].innerText;
                        if (itemName == undefined) {
                            for (var i = 0; i < s.path.length; i++) {
                                if (s.path[i].className == "thumb-item") {
                                    itemName = s.path[i].innerText;
                                }
                            }
                        }
                    }
                    if (s.originalTarget != undefined && itemName == undefined) // firefox
                    {
                        itemName = s.originalTarget.innerText;

                        if (itemName == undefined)
                        {
                            itemName = s.target.parentElement.innerText
                        }
                    }
                    if (s.target != undefined && itemName == undefined) // IE + Edge
                    {
                        itemName = s.target.innerText;
                        if (itemName == undefined) {
                            if (s.target.parentElement != undefined)
                            itemName = s.target.parentElement.innerText
                        }
                        if (itemName == undefined) {
                            var checkItem = s.target;
                            for (var i = 0; i < 5; i++) {// from last path of svg, maximum 5 level                                
                                if (checkItem.className == "thumb-item") {
                                    itemName = checkItem.innerText;
                                }
                                else
                                {
                                    checkItem = checkItem.parentNode;
                                }
                            }
                        }  
                    }
                    var clickedItemName = itemName;
                    if (clickedItemName != undefined) {
                        this._listBox.itemsSource.forEach(function (s) {
                            if (s.Name == clickedItemName.trim()) {
                                seft._parentPath = s.ParentPath;
                                seft._fileName = s.Name;
                                seft._type = s.Type;
                                seft._itemId = s.ItemID;
                                seft._downloadUrl = s.ItemUrl;
                                seft.showButtons();
                                if (s.Type == 0)// folder
                                {
                                    seft._selectedNode = s;
                                } else
                                {
                                    seft._selectedNode = seft._treeView._selNode; 
                                }
                            }
                        });
                    }
                }        
            });

            addListener(this._listBoxDiv, 'dblclick', (s, e) => {
                var seft = this;
                if (this._listBox.itemsSource != undefined && this._listBox.itemsSource.length > 0) {
                    var itemName = undefined;
                    if (s.path != undefined)// chrome
                    {
                        itemName = s.path[0].innerText == undefined ? s.path[1].innerText : s.path[0].innerText;
                        if (itemName == undefined) {
                            for (var i = 0; i < s.path.length; i++) {
                                if (s.path[i].className == "thumb-item") {
                                    itemName = s.path[i].innerText;
                                }
                            }
                        }
                    }
                    if (s.originalTarget != undefined && itemName == undefined) // firefox
                    {
                        itemName = s.originalTarget.innerText;
                        if(itemName == undefined)
                        {
                            itemName = s.target.parentElement.innerText
                        }
                    }
                    if (s.target != undefined && itemName == undefined) // IE + Edge
                    {
                        itemName = s.target.innerText;
                        if (itemName == undefined) {
                            if (s.target.parentElement != undefined)
                            itemName = s.target.parentElement.innerText
                        }
                        if (itemName == undefined) {
                            var checkItem = s.target;
                            for (var i = 0; i < 5; i++) {// from last path of svg, maximum 5 level                                
                                if (checkItem.className == "thumb-item") {
                                    itemName = checkItem.innerText;
                                }
                                else
                                {
                                    checkItem = checkItem.parentNode;
                                }
                            }
                        }  
                    }
                    var clickedItemName = itemName;
                    if (clickedItemName != undefined) {
                        this._listBox.itemsSource.forEach(function (s) {
                            if (s.Name == clickedItemName.trim() && s.Type == 0) {
                                seft.clickTargetFolder(s.Name);
                                seft._parentPath = s.ParentPath;
                                seft._fileName = s.Name;
                                seft._type = s.Type;
                                seft._itemId = s.ItemID;
                                seft._downloadUrl = s.ItemUrl;
                                seft.showButtons();
                                seft._navigateArr.push(s);
                                seft.addNavigateBar();
                            }
                        });
                    }
                }
            });

            addListener(this._btnRefresh, 'click', () => {
                this.refreshTree();
            });
            addListener(this._download, 'click', () => {
                this.downloadFunc();
            });
            addListener(this._btnMoveUp, 'click', () => {
                this.moveUp();
            });
            addListener(this._btnSearch, 'click', () => {
                this.search();
            });
            addListener(this._slView, 'change', () => {
                this.onSelectFunc();
            });
            addListener(this._txtSearch, 'keydown', (e: KeyboardEvent) => {
                switch (e.keyCode) {
                    case wijmo.Key.Enter:
                        this.search();
                        break;
                }
            });
            addListener(this._upload, 'click', () => {
                //donothing, the popup will be shown
            });
            addListener(this._btnUploadData, 'change', (event) => {
                var files = event.target['files'];
                if (files.length > 0) {
                    this._spnFileName.innerText = files[0].name;
                    this._uploadingFile = files;
                }
            });
            addListener(this._btnConfirmUpload, 'click', () => {
                this.uploadFunc();
            });
            addListener(this._delete, 'click', () => {
                this.deleteFunc();
            });
            addListener(this._btnCreateFolder, 'click', () => {
                this.createFolder();
            });
            addListener(this._btnConfirmMovefile, 'click', () => {
                this.confirmMoveFunc();
            });

            addListener(this._list, 'click', () => {
                // TFS 443315
                //this._listBoxDiv.className = "";
                this._listBoxDiv.className = this._listBoxDiv.className.replace(/\bwrapper\b/g, ""); // remove only class wrapper
                this._detailView.style.display = 'none';
                this._listBoxDiv.style.height = "365px";
                this._displayMode = 0;
                this.refreshSource();
                this.changeDisplayMode();
            });

            addListener(this._detail, 'click', () => {
                this._detailView.style.display = 'inline-block';
                this._listBoxDiv.style.height = "338px";
                // TFS 443315
                //this._listBoxDiv.className = "";
                this._listBoxDiv.className = this._listBoxDiv.className.replace(/\bwrapper\b/g, ""); // remove only class wrapper
                this._displayMode = 1;
                this.refreshSource();
                this.changeDisplayMode();
            });

            addListener(this._thumb, 'click', () => {
                this._detailView.style.display = 'none';
                this._listBoxDiv.style.height = "365px";
                this._listBoxDiv.className = this._listBoxDiv.className + " wrapper";
                this._displayMode = 2;
                this.refreshSource();
                this.changeDisplayMode();
            });
            
            if (options) {
                this.deferUpdate(() => {
                    this.initialize(options);
                });
            }
            c1.documentReady(() => {               
                this._initButton();
                this._init();
                this.onload();
            });
        }

        private clickTargetFolder(folderName: string)
        {
            var getNode = false;
            var forlderList = document.getElementsByClassName("wj-node") as HTMLCollectionOf<HTMLElement>;
            if (forlderList.length > 0) {
                for (var index = 0; index < forlderList.length; index++) {
                    if (forlderList[index].className.indexOf("wj-state-selected")>-1)
                    {
                        getNode = true;// if true, we will get parent folder of need click target.
                    }
                    if (getNode && forlderList[index].innerText.trim() == folderName.trim()) {
                        forlderList[index].click();
                    }
                }
            }
        }


        private removeOpacity()
        {
            this._thumb.className = this._thumb.className.replace("opa1", "").replace("opa5", "");
            this._list.className = this._list.className.replace("opa1", "").replace("opa5", "");
            this._detail.className = this._detail.className.replace("opa1", "").replace("opa5", ""); 
        }

        private changeDisplayMode()
        {
            switch (this._displayMode) {
                case 1:// detail view
                    this._detailView.style.display = 'inline-block';
                    this.removeOpacity();
                    this._thumb.className = this._thumb.className + " opa5";
                    this._list.className = this._list.className + " opa5";
                    this._detail.className = this._detail.className + " opa1";    
                    break;
                case 2:// thumb view
                    this.removeOpacity();
                    this._thumb.className = this._thumb.className + " opa1";
                    this._list.className = this._list.className + " opa5";
                    this._detail.className = this._detail.className + " opa5";  
                    break;
                default:// simple view
                    this.removeOpacity();
                    this._thumb.className = this._thumb.className + " opa5";
                    this._list.className = this._list.className + " opa1";
                    this._detail.className = this._detail.className + " opa5";  
                    break;
            }
        }

        private onSelectFunc() {  
            switch (this._slView.selectedIndex) {
                case 1:
                    this._detailView.style.display = 'inline-block';
                    this.refreshSource();
                    this._listBoxDiv.style.height = "338px";
                    this._listBoxDiv.className = "";
                    break;
                case 2:
                    this._detailView.style.display = 'none';
                    this._listBoxDiv.className = "wrapper";
                    this.refreshSource();
                    this._listBoxDiv.style.height = "365px";
                    break;
                default:
                    this._listBoxDiv.className = "";
                    this._detailView.style.display = 'none';
                    this._listBoxDiv.style.height = "365px";
                    this.refreshSource();
                    break;
            }
        }

        private _initButton() {
            this._drawMoveFile();
            this._drawCreate();
            this._drawRefresh();
            this._drawDownload();
            this._drawUpload();
            this._drawDelete();
            this._drawMoveUp();
            this._drawSearch();
            this._drawDetail();
            this._drawThumb();
            this._drawList();
        }

        private _drawMoveFile() {
            this._drawButton(this._moveFile, 'MoveFile', wijmo.culture.FileManager.moveFile);
        }

        private _drawCreate() {
            this._drawButton(this._btnCreateFolder, 'CreateFolder', wijmo.culture.FileManager.create);
        }
        
        private _drawRefresh() {
            this._drawButton(this._btnRefresh, 'Refresh', wijmo.culture.FileManager.refresh);
        }

        private _drawDownload() {
            this._drawButton(this._download, 'Download', wijmo.culture.FileManager.download);
        }

        private _drawUpload() {
            this._drawButton(this._upload, 'Upload', wijmo.culture.FileManager.upload);
        }

        private _drawDelete() {
            this._drawButton(this._delete, 'Delete', wijmo.culture.FileManager.delete);
        }

        private _drawMoveUp() {
            this._drawButton(this._btnMoveUp, 'ArrowUp');
        }

        private _drawSearch() {
            this._drawButton(this._btnSearch, 'Search');
            this._btnSearch.className = "";
        }

        private _drawList() {
            this._drawButton(this._list, 'ListView');
            this._list.className = "fm-svg opa1";
        }

        private _drawThumb() {
            this._drawButton(this._thumb, 'ThumbnaiLView');
            this._thumb.className = "fm-svg opa5";
        }

        private _drawDetail() {
            this._drawButton(this._detail, 'DetailView');
            this._detail.className = "fm-svg opa5";
        }

        private _drawButton(element: HTMLElement, svgIcon?: string, text?: string) {
            if (svgIcon) {
                let icon = _createSvgBtn(svgIcon);
                element.innerHTML = icon.outerHTML;
            }
            if (text) {
                let span = document.createElement('span');
                span.innerHTML = text;
                span.style.verticalAlign = 'middle';
                element.innerHTML += ' ' + span.outerHTML;
            }
            //element.className = "btn btn-default btn-sm";
            element.className = "wj-fm-btn fm-svg";
        }

        private _updateRootPath() {
            this._rootPath = '/' + this._cloudName + '/' + this.getContainer(this._initPath) + '/';
        }

        private _checkInitPath()
        {
            if (this._initPath == undefined)
            {
                if (this._rootFolder != undefined) {
                    var container = "null";
                    if (this._cloudName == "Azure" || this._cloudName == "OneDrive")
                    {
                        if (this._containerName == undefined)
                            throw wijmo.culture.FileManager.error1;
                        container = this._containerName;
                    }
                    this._initPath = container + "/" + this._rootFolder;
                }
                else
                {
                    throw wijmo.culture.FileManager.error2;
                }
            }
        }

        private _init() {
            var _seft = this;
            // create folder Icon
            let folderIcon = _createSvgBtn("Folder");
            let fileIcon = _createSvgBtn("File");
            let excelFileIcon = _createSvgBtn("ExcelFile");
            let pdfFileIcon = _createSvgBtn("PdfFile");
            let imageFileIcon = _createSvgBtn("ImageFile");
            let docFileIcon = _createSvgBtn("DocFile");
            let thumbnailIcon = _createSvgBtn("ThumbnaiLView");

            // large size icon

            let lFolderIcon = _createSvgBtn("BigFolder");
            let lFileIcon = _createSvgBtn("BigFile");
            let lExcelFileIcon = _createSvgBtn("BigExcel");
            let lPdfFileIcon = _createSvgBtn("BigPdf");
            let lImageFileIcon = _createSvgBtn("BigImage");
            let ldocFileIcon = _createSvgBtn("BigDoc");
            //===================
            this._cloudName = this.getCloudName();
            this.hideAllButtons();
            this._checkInitPath();
            this._updateRootPath();
            var treeView = new wijmo.nav.TreeView(this._treeViewDiv, {
                displayMemberPath: 'Name',
                childItemsPath: 'SubFolder',
                imageMemberPath: 'Image',
                autoCollapse: false,
                selectedItemChanged: function (s, e) {
                },
                itemClicked: function (s, e) {
                    var selNode = treeView.selectedNode;
                    _seft._selectedNode = selNode;
                    _seft._treeView = s;
                    if (selNode) {
                        _seft._parentPath = _seft._selectedNode.dataItem.ParentPath;
                        _seft._fileName = _seft._selectedNode.dataItem.Name;
                        _seft._parentNode = _seft._selectedNode.parentNode;
                        _seft._type = _seft._selectedNode.dataItem.Type;
                        _seft._itemId = _seft._selectedNode.dataItem.ItemID;
                        _seft._downloadUrl = _seft._selectedNode.dataItem.ItemUrl;
                        _seft._navigateArr.push(treeView.selectedItem);
                        _seft.addNavigateBar();
                        _seft.showButtons();
                            if (_seft._treeView.selectedNode != null)
                                _seft._treeView.lazyLoadFunction(_seft._treeView.selectedNode);
                        }
                },
                formatItem: function (s, e) {
                    if (e.dataItem) {
                        if (e.dataItem.Type == 0) {
                            if (_seft._fileExtentionImageFormater == null) {
                                e.element.className = e.element.className + " fm-svg";
                                e.element.innerHTML = folderIcon.innerHTML + ' ' + e.element.innerHTML;
                            }
                        } else {
                            e.element.parentNode.removeChild(e.element);
                        }
                    }
                }
            });

            var treeViewMoveFile = new wijmo.nav.TreeView(this._treeViewMoveFileDiv, {
                displayMemberPath: 'Name',
                childItemsPath: 'SubFolder',
                imageMemberPath: 'Image',
                selectedItemChanged: function (s, e) {
                    var selNode = treeViewMoveFile.selectedNode;
                    if (selNode) {
                        _seft._targetPath = selNode.dataItem.ParentPath;
                        _seft._parentPath = selNode.dataItem.ParentPath;
                        _seft._fileName = selNode.dataItem.Name;
                    }
                },
                itemClicked: function (s, e) {
                },
                formatItem: function (s, e) {
                    if (e.dataItem) {
                        if (e.dataItem.Type == 0) {
                            if (_seft._fileExtentionImageFormater == null) {
                                e.element.className = e.element.className +" fm-svg";
                                e.element.innerHTML = folderIcon.innerHTML + ' ' + e.element.innerHTML;
                            }
                        } else {
                            e.element.parentNode.removeChild(e.element);
                        }
                    }
                }
            });
            this._treeViewMoveFileDiv.style.maxHeight = "300px";

            var formatListItems = function (e)
            {
                var slMode = _seft._displayMode;
                if (_seft._slView != undefined)// todo: add code for mobile suport
                    slMode = _seft._slView.selectedIndex;
                switch (slMode) {
                    case 1:
                        formatDetailItems(e);
                        break;
                    case 2:
                        formatThumbItems(e);
                        break;
                    default:
                        formatSimpleListItem(e);
                        break;
                }
            }

            var formatSimpleListItem = function (e)
            {
                e.item.innerHTML = '<div class="fm-svg">' + getIconForSmallItem(e) + '<span class="wj-node-text"> ' + e.data.Name + '</span></div>';                    
            }

            var formatThumbItems = function (e) {
                e.item.className = e.item.className + " thumb-item-maxH";
                e.item.innerHTML = '<div class="thumb-item">' + getIconForItem(e) + '<span class="wj-node-text"> ' + e.data.Name + '</span></div>';
                
                var tooltip = new wijmo.Tooltip();
                
                var tooltipContent = '<div class="fm-row">' +
                    '<div class="fm-col-sm-12">' +
                    '<div style="width:100%;">Name: ' + e.data.Name + '</div>' +
                        '<div style="width:100%;">Last Modified Date: '+ e.data.ModifiedDate + '</div>' +
                        '<div style="width:100%;">Size: ' + e.data.Size + " kb" + '</div>' +                    
                    '</div>'+
                    '</div>'
                tooltip.setTooltip(e.item, tooltipContent);
            }
            var getIconForSmallItem = function (e) {
                if (e.data.Type == 0)
                    return folderIcon.innerHTML;
                else {
                    var fileEx = e.data.Name.split('.').pop();
                    switch (fileEx) {
                        case "pdf":
                            return pdfFileIcon.innerHTML;
                        case "docx":
                        case "doc":
                            return docFileIcon.innerHTML;
                        case "xlsx":
                        case "xls":
                            return excelFileIcon.innerHTML;
                        case "png":
                        case "tif":
                        case "gif":
                        case "jpg":
                            return imageFileIcon.innerHTML;
                        default:
                            return fileIcon.innerHTML;
                    }
                }
            }
            
            var getIconForItem = function (e)
            {
                if (e.data.Type == 0)
                    return lFolderIcon.innerHTML;
                else
                {
                    var fileEx = e.data.Name.split('.').pop();
                    switch (fileEx)
                    {
                        case "pdf":
                            return lPdfFileIcon.innerHTML;
                        case "docx":
                        case "doc":
                            return ldocFileIcon.innerHTML;
                        case "xlsx":
                        case "xls":
                            return lExcelFileIcon.innerHTML;
                        case "png":
                        case "tif":
                        case "gif":
                        case "jpg":
                            return lImageFileIcon.innerHTML;
                        default:
                            return lFileIcon.innerHTML;                            
                    }
                }
            }

            var formatDetailItems = function(e)
            {
                if (e.data.Type == 0) {
                    e.item.innerHTML = '<div class="fm-svg">' + folderIcon.innerHTML + '<span class="wj-node-text"> ' + e.data.Name + '</span></div>';
                } else
                    e.item.innerHTML = '<div class="fm-row fm-clearfix">' +
                        '<div class="fm-col-sm-4 fm-svg" style="padding-left:0px!important;">' + getIconForSmallItem(e) + '<span class="wj-node-text"> ' + e.data.Name + '</span></div>' +
                        '<div class="fm-col-sm-3">' + e.data.ModifiedDate + '</div>' +
                        '<div class="fm-col-sm-2" style="text-align:right;">' + e.data.Size + " kb" + '</div>' +
                        '<div class="fm-col-sm-2"> </div>' +
                        '</div>';
            }

            // ListBox
            this._listBox = new wijmo.input.ListBox(this._listBoxDiv, {
                formatItem: function (s, e) {
                    var textSearch = (<HTMLInputElement>_seft._txtSearch).value;
                    if (textSearch != '' && e.data.Name.toLowerCase().indexOf(textSearch.toLowerCase()) == -1) {
                        e.item.style.display = 'none';
                    }
                    else {
                        formatListItems(e);
                    }
                },
                selectedIndex: -1
            });

            this._listBoxDiv.style.height = "365px";

            var uploadBtn = new wijmo.input.Popup(this._uploadFilePopup, {
                owner: this._upload,
                showTrigger: 'Click'
            });
            var moveFileBtn = new wijmo.input.Popup(this._moveFilePopup, {
                owner: this._moveFile,
                showTrigger: 'Click',
                shown: function () {
                    _seft.onClientMoveFile();
                }
            });

            this._initContextMenu();
        }

        // The context menu
        private _initContextMenu() {
            let div = document.createElement('div'),
                contextMenu = new wijmo.input.Menu(div, {
                    displayMemberPath: 'header',
                    subItemsPath: 'items',
                    command: {
                        executeCommand: this._executeMenuCommand.bind(this),
                        canExecuteCommand: this._canExecuteMenuCommand.bind(this)
                    }
                });
            
            this.addEventListener(this._listBox.hostElement, 'contextmenu', (e: MouseEvent) => {
                let items = this._getMenuItems();
                e.preventDefault();
                if (items && items.length) {
                    contextMenu.initialize({
                        itemsSource: items
                    });
                    contextMenu._dropDown.className = contextMenu._dropDown.className + " fm-context-menu-items fm-svg";

                    contextMenu.show(e);
                }
            }, false);
        }

        private _executeMenuCommand(param: any) {
            switch (param.cmd) {
                case 'Upload':
                    //this.uploadFunc();
                    this._upload.click();
                    break;
                case 'Refresh':
                    this.refreshTree();
                    break;
                case 'Create':
                    this.createFolder();
                    break;
                case 'Delete':
                    this.deleteFunc();
                    break;
                case 'Download':
                    this.downloadFunc();
                    break;
                case 'Move':
                    //this.onClientMoveFile();
                    this._moveFile.click();
                    break;
                default:
                    break;
            }
        }

        private _canExecuteMenuCommand(param: any) {
            switch (param.cmd) {
                default:
                    return true;
            }
        }

        private _getMenuItems() {
            let items = [];

            if (this._type === 0) {
                items.push(
                    { header: this._upload.innerHTML, cmd: 'Upload' },
                    { header: this._btnRefresh.innerHTML, cmd: 'Refresh' },
                    { header: this._btnCreateFolder.innerHTML, cmd: 'Create' },
                    { header: this._delete.innerHTML, cmd: 'Delete' }
                );
            }
            else {
                items.push(
                    { header: this._download.innerHTML, cmd: 'Download' },
                    { header: this._moveFile.innerHTML, cmd: 'Move' },
                    { header: this._delete.innerHTML, cmd: 'Delete' }
                );
            }

            return items;
        }

        private onload() {
            // load treeview first time
            this.onLoadTreeView(this._treeViewDiv);
        }

        private hideAllButtons()
        {
            this._moveFile.style.display = 'none';
            this._btnCreateFolder.style.display = 'none';
            this._btnRefresh.style.display = 'none';
            this._download.style.display = 'none';
            this._upload.style.display = 'none';
            this._delete.style.display = 'none';
            this._detailView.style.display = 'none';
            this.changeDisplayMode();
        }        

        private showButtons() {
            this._detail.style.display = 'inline-block';
            this._thumb.style.display = 'inline-block';
            this._list.style.display = 'inline-block';
            this.hideAllButtons();
            if (this._type == 0) {
                this._btnCreateFolder.style.display = 'inline-block';
                this._btnRefresh.style.display = 'inline-block';
                this._upload.style.display = 'inline-block';
                this._delete.style.display = 'inline-block';
            }
            else {
                this._moveFile.style.display = 'inline-block';
                this._download.style.display = 'inline-block';
                this._delete.style.display = 'inline-block';
            }
        }

        private search() {
            this.refreshSource();     
        }

        private refreshSource()
        {
            var tempSource = [];

            if (this._listBox.itemsSource != null && this._listBox.itemsSource.length > 0) {
                this._listBox.itemsSource.forEach(function (s) {
                    tempSource.push(s);
                });
                tempSource[0].Image = '';
            }
            this._listBox.itemsSource = tempSource;
            this._listBox.selectedIndex = -1;
            this._listBox.invalidate();  
        }
    
        private moveUp() {
            if (this._treeView.selectedNode != null && this._treeView.selectedNode.parentNode != null) {
                this._treeView.selectedItem = this._treeView.selectedNode.parentNode.dataItem;
                this._treeView.invalidate();
                this._listBox.itemsSource = this._treeView.selectedNode.nodes[0].itemsSource;
                this._listBox.selectedIndex = -1;
                this._listBox.invalidate();
                this._parentPath = this._treeView.selectedItem.ParentPath;
                this._fileName = this._treeView.selectedItem.Name;
                this._type = this._treeView.selectedItem.Type;
                this._itemId = this._treeView.selectedItem.ItemID;
                this._downloadUrl = this._treeView.selectedItem.ItemUrl;
                this.addNavigateBar();
            }
        }
        
        private downloadFunc() {
            if (this._downloadUrl == '' || this._type == 0) {
                alert(wijmo.culture.FileManager.download1);
                return;
            }
            var a = document.createElement('a');
            a.style.display = 'none';
            a.href = this._downloadUrl;
            // the filename you want
            a.download = this._fileName;
            document.body.appendChild(a);
            a.click();
        }

        private addNavigateBar() {
            var _seft = this;
            var ul = this._navigateUL;
            ul.innerHTML = '';
            var pathNodes = this._parentPath.substring(this._rootPath.length).split('/');
            var className = "cloud-link";
            pathNodes.forEach(function (s) {
                var _seft2 = _seft;
                if (s != '') {
                    var li = document.createElement('li');
                    li.className = className;
                    var small = document.createElement('small');
                    var a = document.createElement('a');
                    a.className = "li-nav";
                    a.href = "#";
                    a.text = s;
                    a.onclick = function (e) {
                        var ref = this;
                        if (_seft2._navigateArr.length > 0) {
                            for (var i = 0; i < _seft2._navigateArr.length; i++) {
                                if (_seft2._navigateArr[i].Name == a.innerText) {
                                    _seft2._treeView.selectedItem = _seft2._navigateArr[i];
                                    _seft2._treeView.invalidate();
                                    if (_seft2._treeView.selectedNode.nodes[0] != undefined) {
                                        _seft2._listBox.itemsSource = _seft2._treeView.selectedNode.nodes[0].itemsSource;
                                        _seft2._listBox.selectedIndex = -1;
                                        _seft2._listBox.invalidate();
                                        _seft2._parentPath = _seft2._treeView.selectedItem.ParentPath;
                                        _seft2._fileName = _seft2._treeView.selectedItem.Name;
                                        _seft2._type = _seft2._treeView.selectedItem.Type;
                                        _seft2._itemId = _seft2._treeView.selectedItem.ItemID;
                                        _seft2._downloadUrl = _seft2._treeView.selectedItem.ItemUrl;
                                        _seft2.addNavigateBar();
                                    }
                                    return;
                                }
                            }
                        }
                    };
                    small.appendChild(a);
                    li.appendChild(small);
                    ul.appendChild(li);
                    var li2 = document.createElement('li');
                    var small2 = document.createElement('small');
                    var triangleIcon = _createSvgBtn("Triangle");
                    small2.appendChild(triangleIcon);
                    small2.className = "fm-3angels";
                    li2.appendChild(small2);
                    ul.appendChild(li2);
                    className = "";
                }
            });
            var lin = document.createElement('li');
            var an = document.createElement('a');
            an.className = "li-nav";
            an.href = "#";
            an.text = this._fileName;
            lin.appendChild(an);
            ul.appendChild(lin);
        }

        private onClientMoveFile() {
            if (this._fileName == undefined) {
                alert(wijmo.culture.FileManager.moveFile1);
                return;
            }
            this._targetUrl = this._hostUrl + '/api/storage/Move' + this._parentPath + '/?subpath=' + this._fileName;
            this._originalFileName = this._fileName;
            this._fileName = '';
            this.onLoadTreeView(this._treeViewMoveFileDiv);
        }

        private confirmMoveFunc() {
            if (this._isReadOnly)
                return;
            var _seft = this;
            if (this._targetPath == undefined) {
                alert(wijmo.culture.FileManager.moveFile2);
                return;
            }
            var url = this._targetUrl + '?targetpath=' + this.cleanRootPath(this._parentPath) + this._fileName + '/' + this._originalFileName;
            this._originalFileName = '';

            var orginalPath = (this._targetUrl.split('?')[0]).replace(this._hostUrl, "").replace("/api/storage/Move", "");
            var targetPath = this._parentPath + '/' + this._fileName + '/';
            if (orginalPath == targetPath) {
                alert(wijmo.culture.FileManager.moveFile5);
                return;
            }
            if (this._itemId != null) {
                url = url + "?itemid=" + this._itemId;
            }

            c1.mvc.Utils.ajax({
                url: encodeURI(url),
                async: true,
                cache: false,
                type: 'GET',
                success: function (result) {
                    alert(wijmo.culture.FileManager.moveFile3);
                    _seft.refreshTree();
                },
                error: function (error) {
                    alert(error);
                }
            });
        }

        private cleanRootPath(parentPath) {
            if (parentPath.length == this._rootPath.length)// AWS
                return '';
            if (this.cloudType == 4)// OneDrive
                return parentPath + '/';
            parentPath = parentPath.substring(this._rootPath.length) + '/';
            return parentPath;
        }

        private refreshTree(isDelete: boolean = false) {
            var _seft = this;
            this._treeView = wijmo.Control.getControl(this._treeViewDiv);         
          
            if (!isDelete && this._selectedNode != null)
                this._treeView.lazyLoadFunction(this._selectedNode);
            if (isDelete) {
                if (this._type == 0) {// delete folder
                    var currentParent = null;
                    if (this._treeView._selNode.dataItem.Name == this._fileName) {
                        currentParent = this._selectedNode.parentNode;
                    }
                    else {
                        currentParent = _seft._treeView._selNode;
                    }

                    this._treeView.lazyLoadFunction(currentParent);

                    setTimeout(function () {
                        if (currentParent != null && currentParent.dataItem.SubFolder.length > 0) {
                            var subFolder = currentParent.dataItem.SubFolder;
                            currentParent.dataItem.SubFolder = [];
                            for (var i = 0; i < subFolder.length; i++) {
                                if (_seft._fileName != subFolder[i].Name)
                                    currentParent.dataItem.SubFolder.push(subFolder[i]);
                            }
                        }
                        currentParent.dataItem.flag = "flip";
                        currentParent.refresh();
                        _seft._treeView.loadTree(true);
                    }, 1000, _seft);
                }
                else// delete file
                {
            if (this._selectedNode != null)
                this._treeView.lazyLoadFunction(this._selectedNode);
                }              
            }
        }

        private decorageFileExtention(fileName) {
            var fileExtention = '';
            if (fileName) {
                if (this._fileExtentionImageFormater != null) {
                    fileExtention = fileName.split('.').pop();
                    return this._fileExtentionImageFormater(fileExtention);
                }
                else
                    return '';
            }
            else
                return '';
        }

        private uploadFunc() {
            if (this._isReadOnly)
                return;
            var _seft = this;
            if (this._type == 1) {
                alert(wijmo.culture.FileManager.uploadFile3);
            }
            else {
                var files = this._uploadingFile;
                if (files && files.length > 0) {
                    var url = this._hostUrl + '/api/storage' + this._parentPath + '/?subpath=' + this._fileName + '/' + files[0].name;
                    if (this._itemId != null) {
                        url = url + "?itemid=" + this._itemId;
                    }
                    c1.mvc.Utils.ajax({
                        url: url,
                        async: true,
                        type: 'POST',
                        postType: 'multipart',
                        data: {
                            file : files[0]
                        },
                        success: function (result) {
                            _seft._spnFileName.innerText = 'none';
                            _seft._uploadingFile = null;
                            alert(wijmo.culture.FileManager.uploadFile4);
                            _seft.refreshTree();
                        },
                        error: function (error) {
                            alert(error);
                        }
                    });
                }
                else
                {
                    alert(wijmo.culture.FileManager.moveFile6);
                }
            }
        }

        private createFolder() {
            if (this._isReadOnly)
                return;
            var _seft = this;
            var newFolderName = prompt(wijmo.culture.FileManager.createFolder1, "newFolder");
            if (!newFolderName) return;
            var sameName = false;

            if (this._listBox.itemsSource.length > 0) {
                this._listBox.itemsSource.forEach(function (s) {
                    if (s.Name == newFolderName.trim()) {
                        sameName = true;
                    }
                });
            }  
            if (sameName)
            {
                alert(wijmo.culture.FileManager.createFolder3);
                return;
            }      

            var url = this._hostUrl + '/api/storage/Folder' + this._parentPath + '/?subpath=' + this._fileName + '/' + newFolderName;
            if (this._itemId != null) {
                url = url + "?itemid=" + this._itemId;
            }

            c1.mvc.Utils.ajax({
                url: url,
                async: true,
                type: 'POST',
                contentType: 'false',
                dataType: 'json',
                postType: 'json',
                success: function (result) {
                    alert(wijmo.culture.FileManager.createFolder2);
                    _seft.refreshTree();
                },
                error: function (error) {
                    alert(error);
                }
            });
        }

        private deleteFunc() {
            if (this._isReadOnly)
                return;
            if (this._type == 0) {
                if (confirm(wijmo.culture.FileManager.delete1)) {
                    this.confirmDelete();
                }
                return;
            }
            else {
                if (confirm(wijmo.culture.FileManager.delete4)) {
                    this.confirmDelete();
                }
                return;
            }
        }

        private confirmDelete() {
            var _seft = this;
            if (this._parentPath == undefined || this._fileName == undefined) {
                alert(wijmo.culture.FileManager.delete2);
                return;
            }
            var url = this._hostUrl + '/api/storage' + this._parentPath + '/?subpath=' + this._fileName;
            if (this._itemId != null) {
                url = url + "?itemid=" + this._itemId;
            }

            c1.mvc.Utils.ajax({
                url: url,
                async: true,
                cache: false,
                type: 'DELETE',
                contentType: 'false',
                success: function (result) {
                    alert(wijmo.culture.FileManager.delete3);
                    _seft.refreshTree(true);
                },
                error: function (error) {
                    alert(error);
                }
            });
        }

        private getContainer(initialPath) {
            var container = initialPath;
            if (initialPath != undefined && initialPath.indexOf('/') != -1) {
                container = initialPath.substring(0, initialPath.indexOf('/'));
            }
            return container;
        }

        private getCloudName() {
            switch (this.cloudType) {
                case 0:
                    return "Azure";
                case 1:
                    return "AWS";
                case 3: 
                    return "GoogleDrive";
                case 4:
                    return "OneDrive";
                case 5:
                    return "";
                default:
                    return "DropBox";
            }
        }

        private onLoadTreeView(div) {
            var _seft = this;
            this._treeView = null;
            this._treeView = wijmo.Control.getControl(<HTMLDivElement>div);

            var url = this._hostUrl + '/api/storage/List';
            var parentPath = "/" + this._cloudName + "/" + this._initPath;
            url = url + parentPath;
            if (this._itemId != null) {
                url = url + "?itemid=" + this._itemId;
            }

            c1.mvc.Utils.ajax({
                url: encodeURI(url),
                async: true,
                type: 'GET',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                postType: 'json',
                success: function (result) {
                    result.forEach(function (row) {
                        row.ItemUrl = url.replace("List/", "") + "/" + row.Name;
                        row.ParentPath = parentPath;
                        if (row.Type == 0) {
                            row.SubFolder = [];
                        }
                        row.Image = _seft.decorageFileExtention(row.Name);
                        row.Reload = true;
                    });
                    _seft._treeView.itemsSource = result;
                },
                error: function (error) {
                    alert(error.message);
                }
            });

            // load elements with a simulated http delay
            this._treeView.lazyLoadFunction = function (node, callback) {
                var dataItem = node.dataItem != null ? node.dataItem:node;                
                
                var url = _seft._hostUrl + '/api/storage/List';
                    url = url + "/" + dataItem.ParentPath;
                if (dataItem.Name != null)
                    url = url + "/?subpath=" + dataItem.Name;
                if (dataItem.ItemID != null) {
                        url = url + "?itemid=" + dataItem.ItemID;
                }
                var currentNode = dataItem.Name;
                var currentParentPath = dataItem.ParentPath;
                c1.mvc.Utils.ajax({
                    url: encodeURI(url),
                    async: true,
                    type: 'GET',
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    postType: 'json',
                    success: function (result) {
                        result.forEach(function (row) {
                            row.ItemUrl = url.replace("List/", "") + "/" + row.Name;
                            row.ParentPath = currentParentPath + "/" + currentNode;
                            if (row.Type == 0) {
                                row.SubFolder = [];
                            }
                            row.Image = _seft.decorageFileExtention(row.Name);
                            row.Reload = true;
                        });
                        if (callback) {
                            callback(result);
                        }
                        if (<HTMLDivElement>div == _seft._treeViewDiv) {                                        
                            _seft._listBox.itemsSource = result;   
                            _seft._listBox.selectedIndex = -1;        
                            _seft._listBox.invalidate();                            
                        }
                    },
                    error: function (error) {
                            alert(error.message);
                           
                    }
                });
            };

            // when collapsing a node with 'reload' set to true,
            // clear its contents to reload later
            this._treeView.isCollapsedChanging.addHandler(function (s, e) {
                var node = e.node,
                    tree = node.treeView;
                if (!node.isCollapsed && node.dataItem.Reload == true) {

                    // remove previous lazy-loaded items from data source
                    node.dataItem.SubFolder = [];

                    // re-bind the tree to remove the old nodes
                    tree.loadTree();
                }
            });
        }

    }
}