﻿module c1 {
    'use strict';

    wijmo.culture.DashboardLayout = window['wijmo'].culture.DashboardLayout = {
        showAll: '显示所有',
        show: '显示',
        restore: '复原',
        fullScreen: '全屏',
        hide: '隐藏'
    };
}