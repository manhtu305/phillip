﻿module c1 {
    'use strict';

    wijmo.culture.DashboardLayout = window['wijmo'].culture.DashboardLayout = {
        showAll: 'Show All',
        show: 'Show',
        restore: 'Restore',
        fullScreen: 'FullScreen',
        hide: 'Hide'
    };

    wijmo.culture.FileManager = window['wijmo'].culture.FileManager = {
        upload: 'Upload',
        refresh: 'Refresh',
        create: 'New Folder',
        delete: 'Delete',
        download: 'Download',
        moveFile: 'Move File',
        close: 'Close',
        none: 'None',
        cancel: 'Cancel',
        createFolder1: 'Please enter your folder name!',
        createFolder2: 'Create folder success.',
        createFolder3: 'Duplicate  folder name.',
        uploadFile1: 'Upload a file to cloud',
        uploadFile2: 'You are uploading file :',
        uploadFile3: 'Please choose a folder to upload file in.',
        uploadFile4: 'Upload success.',
        uploadFile5: 'Browse File',
        uploadFile6: 'Please upload a file',
        download1: 'Please select a file to download!',
        moveFile1: 'You must choose a file to move first!',
        moveFile2: 'Please choose target path to move file to!',
        moveFile3: 'Move file success',
        moveFile4: 'Select a folder to move file to',
        moveFile5: 'Target folder must differ to original folder!',
        delete1: 'Do you want to delete this folder and all files inside?',
        delete2: 'Should choose atleast one folder or file to delete!',
        delete3: 'Delete success.',
        delete4: 'Do you want to delete files?',
        block: 'In demo version, this function is not allowed!',
        error1: 'Must set value for ContainerName when using Azure or OneDrive cloud!',
        error2: 'Must set value for RootFolder or InitPath!',
    };
}