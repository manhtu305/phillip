﻿module c1 {
    'use strict';

    wijmo.culture.DashboardLayout = window['wijmo'].culture.DashboardLayout = {
        showAll: 'すべてを表示',
        show: '表示',
        restore: '全画面表示を終了',
        fullScreen: '全画面表示',
        hide: '非表示'
    };

    wijmo.culture.FileManager = window['wijmo'].culture.FileManager = {
        upload: 'アップロード',
        refresh: 'リフレッシュ',
        create: '新規フォルダー',
        delete: '削除',
        download: 'ダウンロード',
        moveFile: 'ファイルを移動',
        close: '閉じる',
        none: 'なし',
        cancel: 'キャンセル',
        createFolder1: 'フォルダー名を入力してください！',
        createFolder2: 'フォルダーの作成に成功しました。',
        createFolder3: 'フォルダー名が重複しています。',
        uploadFile1: 'ファイルをクラウドにアップロードする',
        uploadFile2: 'ファイルをアップロードしています：',
        uploadFile3: 'ファイルをアップロードするフォルダーを選択してください。',
        uploadFile4: 'アップロードに成功しました。',
        uploadFile5: '閲覧ファイル',
        uploadFile6: 'ファイルをアップロードしてください。',
        download1: 'ダウンロードするファイルを選択してください！',
        moveFile1: '最初に移動するファイルを選択する必要があります！',
        moveFile2: 'ファイルを移動するターゲットパスを選択してください！',
        moveFile3: 'ファイルの移動に成功しました。',
        moveFile4: 'ファイルを移動するフォルダーを選択します',
        moveFile5: 'ターゲットフォルダーは元のフォルダーと異なる必要があります!',
        delete1: 'このフォルダと内部のすべてのファイルを削除しますか？',
        delete2: '削除するフォルダーまたはファイルを少なくとも1つ選択する必要があります！',
        delete3: '成功に削除しました。',
        delete4: 'このファイルを削除しますか？',
        block: 'デモ版では、この機能は使用できません！',
        error1: 'Microsoft Azure または OneDrive を使用する場合は containerName に値を設定する必要があります。',
        error2: 'RootFolder または InitPath に値を設定する必要があります。',
    };
}