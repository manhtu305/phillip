set ConfigurationName=%1
set ProjectDir=%2
set IsNetCore3=%3
set IsMvc6Project=%4


cd /D %ProjectDir%

set fileExt=
if not '%ConfigurationName%' == 'Debug' set fileExt=.min

pushd ..\..\
set projFolder=%CD%\C1.Web.Mvc.FlexViewer
popd

if '%IsMvc6Project%' == 'true' (
set projFolder=..\..\C1.AspNetCore.Mvc\src\C1.AspNetCore.Mvc.FlexViewer
if '%IsNetCore3%' == 'true' set projFolder=..\..\C1.AspNetCore.Mvc30\src\C1.AspNetCore.Mvc.FlexViewer
)

pushd ..\..\..\
set WijmoRoot=%CD%\Shared\WijmoMVC\
popd

set mvcRoot=%projFolder%\ClientRelease\
if '%ConfigurationName%' == 'Debug' set mvcRoot=%projFolder%\ClientDebug\

rmdir %mvcRoot% /S /Q

set mvcWijmo=%mvcRoot%Wijmo\
set WijmoDist=%WijmoRoot%dist\

@echo Begin Copy wijmo js files
XCOPY /e /y %WijmoDist%controls\wijmo.viewer%fileExt%.js %mvcWijmo%controls\


set c1ClientRoot=%ProjectDir%

@echo C1 Codes is in "%c1ClientRoot%"

@echo Begin Copy c1 shared js files
XCOPY /e /y %c1ClientRoot%Shared\Viewer%fileExt%.js %mvcRoot%Shared\

@echo Begin Copy c1 mvc js files
XCOPY /e /y %c1ClientRoot%Mvc\Viewer%fileExt%.js %mvcRoot%Mvc\
XCOPY /e /y %c1ClientRoot%Mvc\Cast.FlexViewer%fileExt%.js %mvcRoot%Mvc\
