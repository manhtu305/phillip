set ConfigurationName=%1
set ProjectDir=%2
set IsNetCore3=%3
set IsMvc6Project=%4


cd /D %ProjectDir%

set fileExt=
if not '%ConfigurationName%' == 'Debug' set fileExt=.min

pushd ..\
set projFolder=%CD%
popd

if '%IsMvc6Project%' == 'true' (
set projFolder=..\..\C1.AspNetCore.Mvc\src\C1.AspNetCore.Mvc
if '%IsNetCore3%' == 'true' set projFolder=..\..\C1.AspNetCore.Mvc30\src\C1.AspNetCore.Mvc
)

pushd ..\..\..\
set WijmoRoot=%CD%\Shared\WijmoMVC\
popd

set mvcRoot=%projFolder%\ClientRelease\
if '%ConfigurationName%' == 'Debug' set mvcRoot=%projFolder%\ClientDebug\

@echo root %mvcRoot%

rmdir %mvcRoot% /S /Q

set mvcWijmo=%mvcRoot%Wijmo\
set WijmoDist=%WijmoRoot%dist\

@echo Begin Copy wijmo js files
XCOPY /e /y %WijmoDist%controls\*%fileExt%.js %mvcWijmo%controls\

DEL %mvcWijmo%controls\wijmo.chart.finance*
DEL %mvcWijmo%controls\wijmo.grid.sheet*
DEL %mvcWijmo%controls\wijmo.grid.multirow*
DEL %mvcWijmo%controls\wijmo.olap*
DEL %mvcWijmo%controls\wijmo.viewer*
DEL %mvcWijmo%controls\wijmo.grid.transposed*

(
pushd %mvcWijmo%controls\cultures\
ren *.js *.culture.js
ren *min.culture.js *minjs
ren *minjs *.culture.min.js
popd
)

@echo Begin Copy wijmo css files
XCOPY /e /y %WijmoDist%styles\*%fileExt%.css %mvcWijmo%styles\

set c1ClientRoot=%ProjectDir%

@echo C1 client Codes is in "%c1ClientRoot%"

@echo Begin Copy c1 Culture js files
XCOPY /e /y %c1ClientRoot%Cultures\*%fileExt%.js %mvcRoot%Cultures\

@echo Begin Copy c1 shared js files
XCOPY /e /y %c1ClientRoot%Shared\*%fileExt%.js %mvcRoot%Shared\
DEL %mvcRoot%Shared\Chart.Finance*
DEL %mvcRoot%Shared\Grid.Sheet*
DEL %mvcRoot%Shared\Grid.MultiRow*
DEL %mvcRoot%Shared\Viewer*
DEL %mvcRoot%Shared\Olap*

@echo Begin Copy c1 mvc js files
XCOPY /e /y %c1ClientRoot%Mvc\*%fileExt%.js %mvcRoot%Mvc\
DEL %mvcRoot%Mvc\Chart.Finance*
DEL %mvcRoot%Mvc\Grid.Sheet*
DEL %mvcRoot%Mvc\Grid.MultiRow*
DEL %mvcRoot%Mvc\Viewer*
DEL %mvcRoot%Mvc\Olap*
DEL %mvcRoot%Mvc\Cast.Finance*
DEL %mvcRoot%Mvc\Cast.FlexSheet*
DEL %mvcRoot%Mvc\Cast.FlexViewer*
DEL %mvcRoot%Mvc\Cast.MultiRow*
DEL %mvcRoot%Mvc\Cast.Olap*

@echo Begin Copy c1 css files
XCOPY /e /y %c1ClientRoot%Mvc\*%fileExt%.css %mvcRoot%Mvc\
XCOPY /e /y %c1ClientRoot%Shared\*%fileExt%.css %mvcRoot%Shared\

if '%ConfigurationName%' == 'Debug' (
@echo Begin remove the min files for Debug mode
pushd %mvcRoot%
forfiles /s /m *.min.js /c "cmd /c del @path"
forfiles /s /m *.min.css /c "cmd /c del @path"
popd
)

@echo Begin rename wijmo.culture js files for embedding them into the main assembly.
rem Because they are used according to Culture and resource dll is for UICulture.
rem If the resource file name ends with the culture name, it will be embedded into the resource satellite assembly.
rem pushd %mvcWijmo%controls\cultures\
rem ren *.js *.culture.js
rem ren *min.culture.js *minjs
rem ren *minjs *.culture.min.js
rem popd
