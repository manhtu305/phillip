﻿using System.ComponentModel;
using C1.Util.Licensing;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Define a class for detecting license.
    /// </summary>
    [LicenseProvider]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class LicenseDetector : BaseLicenseDetector
    {
        /// <summary>
        /// The constructor.
        /// </summary>
        public LicenseDetector()
        {
        }
    }
}