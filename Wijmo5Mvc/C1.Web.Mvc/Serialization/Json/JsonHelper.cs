﻿using System;
using System.Collections.Generic;
using System.IO;

namespace C1.JsonNet
{
    /// <summary>
    /// Defines utility methods for serialization or deserialization.
    /// </summary>
    public static class JsonHelper
    {
        /// <summary>
        /// Serializes the specified object to a JSON string using <see cref="JsonSetting"/>.
        /// </summary>
        /// <param name="obj">The object to serialize.</param>
        /// <param name="js">The <see cref="JsonSetting"/> used to serialize the object.
        /// If this is null, default serialization settings will be used.</param>
        /// <returns>
        /// A JSON string representation of the object.
        /// </returns>
        public static string SerializeObject(object obj, JsonSetting js = null)
        {
            using (StringWriter sw = new StringWriter())
            {
                JsonWriter writer = new JsonWriter(sw, js ?? new JsonSetting());
                writer.WriteValue(obj);
                return sw.ToString();
            }
        }

        /// <summary>
        ///  Serializes the specified object to a JSON string using a collection of <see cref="JsonConverter"/>.
        /// </summary>
        /// <param name="obj">The object to serialize.</param>
        /// <param name="jsCters">A collection of <see cref="JsonConverter"/></param>
        /// <returns>A JSON string representation of the object.</returns>
        public static string SerializeObject(object obj, IList<JsonConverter> jsCters)
        {
            if (jsCters == null)
            {
                return SerializeObject(obj);
            }
            JsonSetting js = new JsonSetting();
            js.Converters.Clear();
            foreach (JsonConverter jsctor in jsCters)
            {
                js.Converters.Add(jsctor);
            }
            return SerializeObject(obj, js);
        }

        /// <summary>
        /// Serializes the specified object to a JSON string using <see cref="JsonResolver"/>.
        /// </summary>
        /// <param name="obj">The object to serialize.</param>
        /// <param name="resolver">The object of <see cref="JsonResolver"/></param>
        /// <returns>A JSON string representation of the object.</returns>
        public static string SerializeObject(object obj, JsonResolver resolver)
        {
            JsonSetting js = new JsonSetting();
            js.Resolver = resolver;
            return SerializeObject(obj, js);
        }

        /// <summary>
        /// Deserializes the JSON to the specified .NET type using <see cref="JsonSetting"/>.
        /// </summary>
        /// <typeparam name="T">The type of the object to deserialize to.</typeparam>
        /// <param name="strJson">The object to deserialize.</param>
        /// <param name="js">
        /// The <see cref="JsonSetting"/> used to deserialize the object.
        /// If this is null, default serialization settings will be used.
        /// </param>
        /// <returns>The deserialized object from the JSON string.</returns>
        public static T DeserializeObject<T>(string strJson, JsonSetting js = null)
        {
            using (StringReader sr = new StringReader(strJson.FromHexUnicode()))
            {
                JsonReader reader = new JsonReader(sr, js ?? new JsonSetting());
                return reader.ReadValue<T>();
            }
        }

        /// <summary>
        /// Deserializes the JSON to the specified .NET type using <see cref="JsonSetting"/>.
        /// </summary>
        /// <param name="strJson">The object to deserialize.</param>
        /// <param name="js">
        /// The <see cref="JsonSetting"/> used to deserialize the object.
        /// If this is null, default serialization settings will be used.
        /// </param>
        /// <returns>The deserialized object from the JSON string.</returns>
        public static object DeserializeObject(string strJson, JsonSetting js = null)
        {
            return DeserializeObject(strJson, null, js);
        }

        /// <summary>
        /// Deserializes the JSON to the specified type using <see cref="JsonSetting"/>.
        /// </summary>
        /// <param name="strJson">The object to deserialize.</param>
        /// <param name="type">The type the json will be deserialized.</param>
        /// <param name="js">
        /// The <see cref="JsonSetting"/> used to deserialize the object.
        /// If this is null, default serialization settings will be used.
        /// </param>
        /// <returns>The deserialized object from the JSON string.</returns>
        public static object DeserializeObject(string strJson, Type type, JsonSetting js = null)
        {
            if (string.IsNullOrEmpty(strJson))
            {
                return null;
            }
            using (StringReader sr = new StringReader(strJson.FromHexUnicode()))
            {
                JsonReader reader = new JsonReader(sr, js ?? new JsonSetting());
                return reader.ReadValue(type);
            }
        }
    }
}
