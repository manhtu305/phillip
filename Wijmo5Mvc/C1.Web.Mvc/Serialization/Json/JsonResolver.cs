﻿using C1.Web.Mvc.Serialization;
using System;

namespace C1.JsonNet
{
    /// <summary>
    /// Defines a class for json resolver.
    /// </summary>
    public class JsonResolver : BaseResolver
    {
        #region Resolver
        /// <summary>
        /// Determines whether the object can use this resolver.
        /// </summary>
        /// <param name="name">The name of the object.</param>
        /// <param name="value">The object value.</param>
        /// <param name="type">The object type.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// A <see cref="bool"/> value indicates whether to be resovled.
        /// If true, it means the object can be resolved.
        /// Otherwise, the resolver cannot support this object.
        ///</returns>
        public override bool CanResolve(string name, object value, Type type, IContext context)
        {
            return true;
        }

        /// <summary>
        /// Gets the name after being resolved.
        /// </summary>
        /// <param name="name">The name of the object.</param>
        /// <param name="value">The object value.</param>
        /// <param name="type">The object type.</param>
        /// <param name="context">The context.</param>
        /// <returns>A string.</returns>
        public override string ResolvePropertyName(string name, object value, Type type, IContext context)
        {
            return name;
        }

        /// <summary>
        /// Gets the converter for this resolver.
        /// </summary>
        /// <param name="name">The name of the object.</param>
        /// <param name="value">The object value.</param>
        /// <param name="type">The object type.</param>
        /// <param name="context">The context.</param>
        /// <returns>A converter.</returns>
        public override BaseConverter ResolveConverter(string name, object value, Type type, IContext context)
        {
            return null;
        }
        #endregion Resolver

        #region Obsolete
        /// <summary>
        /// Gets whether the specified object can be serialized or deserialized.
        /// </summary>
        /// <param name="name">The property name.</param>
        /// <param name="value">The value of the object.</param>
        /// <param name="type">The type of the object.</param>
        /// <returns>
        /// A <see cref="bool"/> value indicates whether to be resovled.
        /// If true, it means the object can be serialized or deserialized.
        /// The object doesn't support serialization or deserialization.
        ///</returns>
        public virtual bool CanResolve(string name, object value, Type type)
        {
            return CanResolve(name, value, type, null);
        }

        internal void Deserialize(object member, object parent, JsonReader reader)
        {
            string memberName = Utility.GetName(member);
            Type memberType = Utility.GetType(member);
            object memberValue = Utility.GetValue(member, parent);
            if (CanResolve(memberName, null, memberType))
            {
                string propertyName = ResolvePropertyName(memberName);
                JsonConverter jctor = ResovleJsonConverter(memberName, memberValue, memberType);
                JsonReader.ReadMember(reader, member, propertyName, jctor, parent);
            }
        }

        /// <summary>
        /// Gets the property name for deserialization.
        /// </summary>
        /// <param name="name">The specified property name.</param>
        /// <returns></returns>
        public virtual string ResolvePropertyName(string name)
        {
            return ResolvePropertyName(name, null, null, null);
        }

        /// <summary>
        /// Gets the converter to be used for deserialization.
        /// </summary>
        /// <param name="name">The property name.</param>
        /// <param name="value">The value of the object.</param>
        /// <param name="objectType">The type of the object.</param>
        /// <returns>A converter.</returns>
        public virtual JsonConverter ResovleJsonConverter(string name, object value, Type objectType)
        {
            return ResolveConverter(name, value, objectType, null) as JsonConverter;
        }
        #endregion Obsolete
    }
}
