﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
#if !ASPNETCORE
using System.ComponentModel;
#endif
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;

namespace C1.JsonNet
{
    internal static class JsonUtility
    {
        internal static string GetSerializedName(object pd, string oldName, JsonSetting js)
        {
            var propertyName = GetAdjustedName(pd, oldName);
            if (js == null || !js.IsPropertyNameCamel)
            {
                return propertyName;
            }
            return propertyName.ToCamelCase();
        }

        internal static string ConvertFromHexUnicode(string result)
        {
            var regex = new Regex(@"\\[uU]([0-9A-F]{4})", RegexOptions.IgnoreCase);
            result = regex.Replace(result, match => ((char)int.Parse(match.Groups[1].Value, NumberStyles.HexNumber)).ToString());
            return result;
        }

        internal static string GetAdjustedName(object descriptor, string displayName = null)
        {
            var jsonAttr = Utility.GetAttribute<JsonAttribute>(descriptor);

            if (jsonAttr != null && jsonAttr.Name != null)
            {
                return jsonAttr.Name;
            }

            if (displayName == null)
            {
                displayName = Utility.GetName(descriptor);
            }

            return displayName;
        }

        

        internal static object GetEnumValue(object currentValue, Type type)
        {
            if (Utility.IsInteger(currentValue))
            {
                return Enum.ToObject(type, currentValue);
            }
            return null;
        }

        internal static string GetDateTimeText(DateTime dt, DateTimeKind? kind = null)
        {
            var jsonDateStr = FillByZero(dt.Year, 4)
               + "-" + FillByZero(dt.Month, 2)
               + "-" + FillByZero(dt.Day, 2)
               + "T" + FillByZero(dt.Hour, 2)
               + ":" + FillByZero(dt.Minute, 2)
               + ":" + FillByZero(dt.Second, 2);

            var nanoSecondText = GetNanosecondText(dt);
            if (!string.IsNullOrEmpty(nanoSecondText))
            {
                jsonDateStr += "." + nanoSecondText;
            }

            var dateKind = kind != null ? kind.Value : dt.Kind;

            switch (dateKind)
            {
                case DateTimeKind.Utc:
                    jsonDateStr += "Z";
                    break;
                case DateTimeKind.Local:
                    jsonDateStr += GetTimeOffsetText(dt);
                    break;
                case DateTimeKind.Unspecified:
                    break;
            }
            return jsonDateStr;
        }

        // get the text for the nanosecond text.
        // copy from Newtonsoft.Json.
        private static string GetNanosecondText(DateTime date)
        {
            var num2 = (int)(date.Ticks % 10000000L);
            var num3 = 7;
            if (num2 != 0)
            {
                while (num2 % 10 == 0)
                {
                    num3--;
                    num2 /= 10;
                }
                return FillByZero(num2, num3);
            }

            return string.Empty;
        }
        internal static string GetTimeOffsetText(DateTime date)
        {
            var offset = TimeZoneInfo.Local.GetUtcOffset(date);
            var result = "";
            result += ((offset.Ticks >= 0L) ? "+" : "-");
            result += FillByZero(Math.Abs(offset.Hours), 2);
            result += ":";
            result += FillByZero(Math.Abs(offset.Minutes), 2);
            return result;
        }
        private static string FillByZero(int val, int k)
        {
            var s = val.ToString();
            while (s.Length < k)
            {
                s = "0" + s;
            }
            return s;
        }

        public static void SetValue(object descriptor, object parent, object newValue)
        {
            Utility.ValidNullParameter(descriptor, "descriptor");
            Utility.ValidNullParameter(parent, "parent");
#if !ASPNETCORE
            var pd = descriptor as PropertyDescriptor;
            if (pd != null)
            {
                pd.SetValue(parent, newValue);
                return;
            }

#else
            var pi = descriptor as PropertyInfo;
            if (pi != null && pi.CanWrite)
            {
                pi.SetValue(parent, newValue);
                return;
            }
#endif
            var fi = descriptor as FieldInfo;
            if (fi != null)
            {
                fi.SetValue(parent, newValue);
            }
        }

        public static bool HasImplementIList(Type valueType, object value)
        {
            if (valueType == null)
            {
                return false;
            }
            if (typeof(IList).IsAssignableFrom(valueType))
            {
                return true;
            }
            if (value != null)
            {
                return typeof(IList).IsInstanceOfType(value);
            }
            var types = new List<Type>();
            types.Add(valueType);
            types.AddRange(valueType.GetInterfaces());
            foreach (Type type in types)
            {
                if (type.IsGenericType() && type.GetGenericTypeDefinition() == typeof(IList<>))
                {
                    return true;
                }
            }
            return false;
        }
    }

    internal class MemberComparer : IComparer<object>
    {
        public static MemberComparer CacheComparer = new MemberComparer();

        public int Compare(object x, object y)
        {
            var jsonAttr1 = Utility.GetAttribute<JsonAttribute>(x);
            var jsonAttr2 = Utility.GetAttribute<JsonAttribute>(y);
            var order1 = jsonAttr1 != null ? jsonAttr1.Order : -1;
            var order2 = jsonAttr2 != null ? jsonAttr2.Order : -1;
            return (order2 - order1);
        }
    }
}
