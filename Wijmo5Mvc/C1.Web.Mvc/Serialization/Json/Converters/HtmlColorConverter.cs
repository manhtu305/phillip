﻿#if !ASPNETCORE
using C1.Web.Mvc.Serialization;
using System;
using System.Drawing;

namespace C1.JsonNet.Converters
{
    /// <summary>
    /// The converter for converting color to html.
    /// </summary>
    public class HtmlColorConverter: JsonConverter
    {
        private static readonly HtmlColorConverter _instance = new HtmlColorConverter();

        /// <summary>
        /// Gets the instance of <see cref="HtmlColorConverter"/> object.
        /// </summary>
        public static HtmlColorConverter Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Write value to json string.
        /// </summary>
        /// <param name="writer">The json writer</param>
        /// <param name="value">The value</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            writer.WriteValue(Utility.ColorToHtml((Color)value));
        }

        /// <summary>
        /// Read value from json string.
        /// </summary>
        /// <param name="reader">The json reader</param>
        /// <param name="objectType">The object type</param>
        /// <param name="existedValue">The existed value</param>
        /// <returns></returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            var strValue = reader.ReadValue<string>();
            return string.IsNullOrEmpty(strValue) ? Color.Empty : Utility.ColorFromHtml(strValue);
        }

        /// <summary>
        /// A bool value indicates whether the type can be converted.
        /// </summary>
        /// <param name="objectType">The object type</param>
        /// <returns>A bool value indicates whether the type can be converted</returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Color);
        }
    }
}
#endif