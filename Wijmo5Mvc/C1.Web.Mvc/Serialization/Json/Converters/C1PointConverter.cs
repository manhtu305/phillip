﻿using System;
using System.Collections;
using C1.Web.Mvc.Localization;
using System.Drawing;

namespace C1.JsonNet.Converters
{
    internal class C1PointConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(PointF) || objectType == typeof(Point);
        }

        protected override void WriteJson(JsonWriter writer, object value)
        {
            if (value is PointF)
            {
                PointF pointF = (PointF)value;
                writer.WriteRawValue(string.Format("{{\"x\":{0},\"y\":{1}}}", pointF.X, pointF.Y));
                return;
            }

            if (value is Point)
            {
                Point point = (Point)value;
                writer.WriteRawValue(string.Format("{{\"x\":{0},\"y\":{1}}}", point.X, point.Y));
                return;
            }

            throw new FormatException(Resources.ConverterInvalidTypeOfValue);
        }

        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            IDictionary dic = reader.Current as IDictionary;
            if (dic != null)
            {
                if (objectType == typeof(PointF))
                {
                    PointF pf = new PointF(float.Parse(dic["x"].ToString()), float.Parse(dic["y"].ToString()));
                    return pf;
                }
                if (objectType == typeof(Point))
                {
                    Point pt = new Point((int)dic["x"], (int)dic["y"]);
                    return pt;
                }
            }

            throw new FormatException(Resources.ConverterInvalidTypeOfValue);
        }
    }
}