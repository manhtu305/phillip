﻿using C1.Web.Mvc.Localization;
using System;
using System.Text.RegularExpressions;

namespace C1.JsonNet.Converters
{
    /// <summary>
    /// Define a converter which converts the client function name to string without quote.
    /// </summary>
    public class FunctionConverter : JsonConverter
    {
        private const string STARTTEXT = @"/Function(";
        private const string ENDTEXT = @")/";

        /// <summary>
        /// Gets whether the converter is supported.
        /// </summary>
        /// <param name="objectType">The object type.</param>
        /// <returns>
        /// A bool value indicates whether the converter is supported.
        /// If true, the converter is supported.
        /// Otherwise, it is not supported.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        /// <summary>
        /// Serialize the value.
        /// </summary>
        /// <param name="writer">The serialization writer.</param>
        /// <param name="value">The value.</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            if (value == null)
            {
                writer.WriteValue(null);
                return;
            }
            if (value is string)
            {
                string content = (string)value;
                Regex rg = new Regex(@"function\(.*?\)");
                if (!rg.IsMatch(content) && !writer.JsonSetting.IsJavascriptFormat)
                {
                    content = STARTTEXT + content + ENDTEXT;
                    writer.WriteValue(content);
                }
                else
                {
                    writer.WriteRawValue(content);
                }
            }
            else
            {
                throw new FormatException(Resources.ConverterValueCanNotBeSerialized);
            }
        }

        /// <summary>
        /// Deserialize the object.
        /// </summary>
        /// <param name="reader">The deserialization reader.</param>
        /// <param name="objectType">The object type.</param>
        /// <param name="existingValue">The existed value.</param>
        /// <returns>The object after deserialization.</returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existingValue)
        {
            if (reader.Current is string)
            {
                string content = (string)reader.Current;
                if (content.StartsWith(STARTTEXT, StringComparison.Ordinal) && content.EndsWith(ENDTEXT, StringComparison.Ordinal))
                {
                    return content.Substring(STARTTEXT.Length, content.Length - STARTTEXT.Length - ENDTEXT.Length);
                }
                else
                {
                    return content;
                }
            }
            throw new FormatException(Resources.ConverterValueCanNotBeDeserialized);
        }
    }
}
