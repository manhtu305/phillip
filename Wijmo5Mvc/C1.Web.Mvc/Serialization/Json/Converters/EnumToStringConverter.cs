﻿using C1.Web.Mvc.Localization;
using System;
#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.JsonNet.Converters
{
    /// <summary>
    /// Defines a class which converts the value of Enum type to String with or without camel case
    /// </summary>
    public class EnumToStringConverter : JsonConverter
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the written enum text should be camel case.
        /// </summary>
        /// <value><c>true</c> if the written enum text will be camel case; otherwise, <c>false</c>.</value>
        public bool CamelCaseText { get; set; }
        #endregion Properties

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the EnumToStringConverter class.
        /// </summary>
        public EnumToStringConverter()
        {
            CamelCaseText = true;
        }
        #endregion Ctors

        #region Methods
        /// <summary>
        /// Gets whether the converter is supported.
        /// </summary>
        /// <param name="objectType">The object type.</param>
        /// <returns>
        /// A bool value indicates whether the converter is supported.
        /// If true, the converter is supported.
        /// Otherwise, it is not supported.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            if (typeof(Enum).IsAssignableFrom(objectType))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Deserialize the object.
        /// </summary>
        /// <param name="reader">The deserialization reader.</param>
        /// <param name="objectType">The object type.</param>
        /// <param name="existingValue">The existed value.</param>
        /// <returns>The object after deserialization.</returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existingValue)
        {
            return JsonUtility.GetEnumValue(reader.Current, objectType);
        }

        /// <summary>
        /// Serialize the value.
        /// </summary>
        /// <param name="writer">The serialization writer.</param>
        /// <param name="value">The value.</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            Enum enumValue = value as Enum;
            if (enumValue == null)
            {
                throw new FormatException(Resources.ConverterInvalidTypeOfValue);
            }
            writer.WriteValue(enumValue.ToString(CamelCaseText));
        }
        #endregion Methods
    }
}
