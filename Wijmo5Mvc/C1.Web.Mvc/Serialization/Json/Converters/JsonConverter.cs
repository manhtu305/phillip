﻿using C1.Web.Mvc.Serialization;
using System;

namespace C1.JsonNet
{
    /// <summary>
    /// Defines an abstract class for json converter.
    /// </summary>
    public abstract class JsonConverter : BaseConverter
    {
        #region Methods
        #region Read & Write
        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existedValue">The existing value of object being read.</param>
        /// <returns>The object value after deserializing.</returns>
        internal object Read(JsonReader reader, Type objectType, object existedValue)
        {
            return ReadJson(reader, objectType, existedValue);
        }
        #endregion Read & Write

        #region Json
        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value to be serialized.</param>
        protected virtual void WriteJson(JsonWriter writer, object value)
        {
            writer.WriteRawValue(ConvertTo(value));
        }

        /// <summary>
        /// Serialize the specified value to a string.
        /// </summary>
        /// <param name="value">the specified value</param>
        /// <returns>A string which the value would be serialized to.</returns>
        protected virtual string ConvertTo(object value)
        {
            return string.Empty;
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existedValue">The existing value of object being read.</param>
        /// <returns>The object value after deserializing.</returns>
        protected virtual object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            return ConvertFrom(reader.Current, existedValue);
        }

        /// <summary>
        /// Deserialize an object from the specified value.
        /// </summary>
        /// <param name="value">
        /// The specified value, which could be null or its type could be <see cref="System.Collections.ArrayList"/>,  <see cref="System.Collections.Hashtable"/>, <see cref="DateTime"/>,
        /// <see cref="String"/>, <see cref="Boolean"/>, <see cref="Int32"/>, <see cref="Double"/>.
        /// </param>
        /// <param name="existedValue">The existed value.</param>
        /// <returns>An object deserialized from the specified value.</returns>
        protected virtual object ConvertFrom(object value, object existedValue)
        {
            return existedValue;
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool CanConvert(Type objectType)
        {
            return true;
        }
        #endregion Json

        #region BaseConverter
        /// <summary>
        /// Writes the specified value.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="context">The context.</param>
        protected override void Write(BaseWriter writer, object value, IContext context)
        {
            WriteJson(writer as JsonWriter, value);
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="context">The context information.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        protected internal override bool CanConvert(Type objectType, IContext context)
        {
            return CanConvert(objectType);
        }
        #endregion BaseConverter
        #endregion Methods
    }
}
