﻿using C1.Web.Mvc.Localization;
using C1.Web.Mvc.Serialization;
#if !ASPNETCORE
using C1.JsonNet.Converters;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Globalization;

namespace C1.JsonNet
{
    /// <summary>
    /// Serialize an object to a json text.
    /// </summary>
    public sealed class JsonWriter : BaseWriter, IJsonOperation
    {
        #region Fields
        private const string NameValueSeparator = ":";
        private const string ItemSeparator = ",";
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the json settings.
        /// </summary>
        public JsonSetting JsonSetting
        {
            get
            {
                return Setting as JsonSetting;
            }
        }
        #endregion Properties

        #region Ctor
        /// <summary>
        /// Initializes a new instance of the JsonWriter class.
        /// </summary>
        /// <param name="writer">The output text writer.</param>
        /// <param name="setting">The setting for serialization.</param>
        public JsonWriter(TextWriter writer, JsonSetting setting)
            : base(new BaseContext(setting), new JsonAttributeHelper(), writer)
        {
        }
        #endregion Ctor

        #region Methods
        #region Public
        /// <summary>
        /// Serialize a text.
        /// </summary>
        /// <param name="value">The specified text.</param>
        public void WriteRawValue(string value)
        {
            WriteText(value);
        }

        /// <summary>
        /// Serialize the specified object.
        /// </summary>
        /// <param name="obj">The specified object.</param>
        /// <param name="useConverter">A <see cref="bool"/> value indicates whether to use a converter for serialization.</param>
        public void WriteValue(object obj, bool useConverter = true)
        {
            if (Scopes.Count != 0)
            {
                var scope = Scopes.Peek();
                if (scope.Type == ScopeType.IEnumerable)
                {
                    if (scope.ObjectCount > 0)
                    {
                        WriteRawText(ItemSeparator, false);
                    }

                    scope.ObjectCount++;
                }
            }

            BaseConverter cter = null;
            if (useConverter)
            {
                cter = GetConverter(null, obj);
                if(cter == null)
                {
                    cter = GetFirstConverter(null, obj);
                }
            }

            WriteMemberValueWithScope(obj, null, cter);
        }

        /// <summary>
        /// Start the scope for a new object.
        /// </summary>
        /// <param name="obj">The specified object.</param>
        public void StartObjectScope(object obj)
        {
            StartScope(obj, ScopeType.Complex);
            OnBeginningWrite();
        }

        /// <summary>
        /// Start the scope for an array.
        /// </summary>
        /// <param name="arr">The specified array.</param>
        public void StartArrayScope(object arr)
        {
            StartScope(arr, ScopeType.IEnumerable);
            OnBeginningWrite();
        }

        /// <summary>
        /// End the scope.
        /// </summary>
        public new void EndScope()
        {
            OnWriteEnded();
            base.EndScope();
        }

        /// <summary>
        /// Serialize a name for a property.
        /// </summary>
        /// <param name="name">The specified name.</param>
        public void WriteName(string name)
        {
            WriteRawName(GetFormattedMemberName(name));
        }

        /// <summary>
        /// Serialize a name for a property.
        /// </summary>
        /// <param name="name">The specified name.</param>
        public void WriteRawName(string name)
        {
            WriteMemberName(name, null, null);
            CheckScopes();
            var scope = Scopes.Peek();
            if (scope.Type == ScopeType.Complex 
                || scope.Type == ScopeType.IDictionary)
            {
                scope.ObjectCount++;
            }
        }
        #endregion Public

        #region Name
        internal override void WriteMemberName(string name, object value, object memberInfo)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            CheckScopes();

            var scope = Scopes.Peek();
            if (scope.Type != ScopeType.Complex && scope.Type != ScopeType.IDictionary)
            {
                throw new InvalidOperationException();
            }

            if (scope.ObjectCount != 0)
            {
                WriteRawText(ItemSeparator, false);
            }

            //scope.ObjectCount++;
            WriteRawText(name);
            WriteRawText(NameValueSeparator, false);
        }

        internal override string GetFormattedMemberName(string memberName)
        {
            return JsonUtility.GetSerializedName(null, memberName, JsonSetting);
        }
        #endregion Name

        #region ShouldSerialize
        internal override bool ShouldSerialize(object memberInfo, object parent)
        {
            return base.ShouldSerialize(memberInfo, parent) &&
            !CheckReferenceLoop(Utility.GetValue(memberInfo, parent), memberInfo);
        }

        /// <summary>
        /// Checks whether there is a reference loop for the specified object.
        /// </summary>
        /// <returns>true if there is a reference loop for the specified object; otherwise, false.</returns>
        private bool CheckReferenceLoop(object value, object pd = null)
        {
            if (!JsonSetting.SkipReferenceLoop
                || value == null || value is DBNull)
                return false;

            // Does not check reference loop if there is a custom json converter.
            // The custom json converter may checks the reference loop by itself and writes the object with any content.
            var jc = GetConverter(pd, value);
            if (jc != null)
            {
                return false;
            }

            // only check the reference types
            if (value.GetType().IsValueType()) return false;

            // check the scope stack
            foreach (var scope in Scopes)
            {
                if (ReferenceEquals(value, scope.Object))
                    return true;
            }

            return false;
        }

        internal override bool HasIgnoreAttribute(object memberInfo, object parent)
        {
            var result = base.HasIgnoreAttribute(memberInfo, parent);
            if (!result)
            {
                var ignoreDataMemberAttr = Utility.GetAttribute<IgnoreDataMemberAttribute>(memberInfo);
                return ignoreDataMemberAttr != null;
            }

            return result;
        }

        internal override bool ShouldSerializeDefaultValue(object memberInfo, object parent)
        {
            bool skipIfDefault;
            var jsonAttr = Utility.GetAttribute<JsonAttribute>(memberInfo);
            if (jsonAttr != null && jsonAttr.SkipIfDefault.HasValue)
            {
                skipIfDefault = jsonAttr.SkipIfDefault.Value;
            }
            else
            {
                skipIfDefault = JsonSetting == null ? true : JsonSetting.SkipIsDefault;
            }

            return !skipIfDefault || base.ShouldSerializeDefaultValue(memberInfo, parent);
        }
        #endregion ShouldSerialize

        #region Raw Values
        #region Simple
        #region Null
        // null => null
        internal override void WriteNull()
        {
            WriteRawText("null", false);
        }
        #endregion Null

        #region DBNull
        // DBNull => null
        internal override void WriteDBNull()
        {
            WriteNull();
        }
        #endregion DBNull

        #region String
        // "string"
        internal override void WriteSimpleValue(string value)
        {
            WriteRawText(Utility.QuoteJScriptString(value), true);
        }
        #endregion String

        #region Char
        // "char"
        internal override void WriteSimpleValue(char value)
        {
            WriteSimpleValue(value.ToString());
        }
        #endregion Char

        #region Int
        // 1
        internal override void WriteSimpleValue(int value)
        {
            WriteRawText(value.ToString(null, CultureInfo.InvariantCulture), false);
        }
        #endregion Int

        #region Single
        // 1f => 1.0
        // 1 => 1.0
        internal override void WriteSimpleValue(float value)
        {
            WriteRawText(Utility.EnsureDecimalPlace(value, value.ToString("R", CultureInfo.InvariantCulture)), false);
        }
        #endregion Single

        #region Double
        // 1d => 1.0
        // 1 => 1.0
        internal override void WriteSimpleValue(double value)
        {
            WriteRawText(Utility.EnsureDecimalPlace(value, value.ToString("R", CultureInfo.InvariantCulture)), false);
        }
        #endregion Double

        #region Enum
        internal override void WriteSimpleValue(Enum value)
        {
            WriteRawText(value.ToString("D"), false);
        }
        #endregion Enum

        #region DateTime
        internal override void WriteSimpleValue(DateTime value)
        {
            WriteSimpleValue(JsonUtility.GetDateTimeText(value));
        }
        #endregion DateTime

        #region Decimal
        // 1 => 1.0
        internal override void WriteSimpleValue(decimal value)
        {
            WriteRawText(Utility.EnsureDecimalPlace(value.ToString(null, CultureInfo.InvariantCulture)), false);
        }
        #endregion Decimal

        #region Boolean
        // => true
        // => false
        internal override void WriteSimpleValue(bool value)
        {
            WriteRawText(value ? "true" : "false", false);
        }
        #endregion Boolean

        #region TimeSpan
        // new TimeSpan(6, 14, 32, 17, 685) => 6.14:32:17.685
        internal override void WriteSimpleValue(TimeSpan value)
        {
            WriteRawText(value.ToString("c"), true);
        }
        #endregion TimeSpan

        #region Others
        internal override void WriteOtherSimples(object value)
        {
            WriteRawText(Convert.ToString(value, CultureInfo.InvariantCulture), false);
        }
        #endregion Others
        #endregion Simple

        #region Complex
        internal override List<object> GetMembers(object value)
        {
            var members = base.GetMembers(value);
            members.Sort(MemberComparer.CacheComparer);
            return members;
        }

        internal override void StartWriteComplex()
        {
            WriteRawText("{", false);
        }

        internal override void EndWriteComplex()
        {
            WriteRawText("}", false);
        }
        #endregion Complex

        #region Dictionary
        internal override void WriteDictionaryEntry(DictionaryEntry item, object parent, object parentMemberInfo)
        {
            var name = item.Key as string;
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException(Resources.C1JsonWriteKeyTypeIsNotSupportedInHashtable);
            }
            WriteMemberInfoWithSettings(name, item.Value, null, null);
        }

        internal override void StartWriteDictionary()
        {
            WriteRawText("{", false);
        }

        internal override void EndWriteDictionary()
        {
            WriteRawText("}", false);
        }
        #endregion Dictionary

        #region Collection
        internal override void WriteCollectionItem(object item, object collection, object collectionMemberInfo)
        {
            if (Scopes.Peek().ObjectCount != 0)
            {
                WriteRawText(ItemSeparator, false);
            }

            base.WriteCollectionItem(item, collection, collectionMemberInfo);
        }

        internal override void WriteRawCollectionItem(object item, object collection, object collectionMemberInfo)
        {
            WriteMemberValueWithScope(item, null);
        }

        internal override void StartWriteCollection()
        {
            WriteRawText("[", false);
        }

        internal override void EndWriteCollection()
        {
            WriteRawText("]", false);
        }
        #endregion Collection

#if !ASPNETCORE
        #region Color
        internal override void WriteColor(System.Drawing.Color value)
        {
            HtmlColorConverter.Instance.Serialize(this, value, null);
        }
        #endregion Color
#endif

        #region CultureInfo
        internal override void WriteCultureInfo(CultureInfo value)
        {
            WriteSimpleValue(value.Name);
        }
        #endregion CultureInfo
        #endregion Raw Values
        #endregion Methods
    }

    internal sealed class JsonAttributeHelper : AttributeHelper
    {
        public override C1ConverterAttribute GetConverterAttribute(object memberInfo, object value)
        {
            return Utility.GetAttribute<JsonConverterAttribute>(memberInfo, value);
        }

        public override C1CollectionItemConverterAttribute GetCollectionItemConverterAttribute(object memberInfo, object collection)
        {
            return Utility.GetAttribute<CollectionItemTypeAttribute>(memberInfo, collection);
        }

        public override C1NameAttribute GetNameAttribute(object memberInfo, object value)
        {
            var na = Utility.GetAttribute<JsonAttribute>(memberInfo, value);
            return na != null && !string.IsNullOrEmpty(na.Name) ? na : null;
        }
    }

    /// <summary>
    /// Defines a base class for serialization or deserialization.
    /// </summary>
    public abstract class JsonOperation : IDisposable
    {
        private JsonSetting _setting;

#region Properties
        /// <summary>
        /// Gets the json setting.
        /// </summary>
        public JsonSetting JsonSetting
        {
            get
            {
                return _setting;
            }
        }
        #endregion Properties

        /// <summary>
        /// Initializes a new instance of the JsonOperation class.
        /// </summary>
        /// <param name="setting">The json setting.</param>
        public JsonOperation(JsonSetting setting)
        {
            _setting = setting ?? new JsonSetting();
        }

        /// <summary>
        /// Dispose the current instance.
        /// </summary>
        public virtual void Dispose()
        {
            _setting = null;
        }
    }

    /// <summary>
    /// Defines interface for the json operation.
    /// </summary>
    public interface IJsonOperation
    {
        /// <summary>
        /// Gets the json settings.
        /// </summary>
        JsonSetting JsonSetting { get; }
    }
}
