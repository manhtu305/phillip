﻿using System;
using System.Text.RegularExpressions;

namespace C1.JsonNet
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal static class Extensions
    {
        #region String
        public static string ToCamelCase(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = char.ToLowerInvariant(value[0]) + value.Substring(1);
            }

            return value;
        }

        public static string ToPascalCase(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = char.ToUpperInvariant(value[0]) + value.Substring(1);
            }

            return value;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        public static string FromHexUnicode(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = JsonUtility.ConvertFromHexUnicode(value);
            }
            return value;
        }
        #endregion String

        #region Enum
        private static Regex _re = new Regex(@"^\p{Lu}|,\s*\p{Lu}");
        public static string ToString(this Enum value, bool isCamel = true)
        {
            string strValue = value.ToString();
            if (isCamel)
            {
                return _re.Replace(strValue, match => match.Value.ToLower());
            }
            return strValue;
        }
        #endregion Enum
    }
}
