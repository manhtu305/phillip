﻿using C1.Web.Mvc.Serialization;
using System.Linq;

namespace C1.JsonNet
{
    /// <summary>
    /// Represents the serialization and deserialization setting.
    /// </summary>
    public class JsonSetting : BaseSetting
    {
        #region Properties

        /// <summary>
        /// Gets or sets a boolean value indicates whether the property name should be serialzied with camel case.
        /// </summary>
        public bool IsPropertyNameCamel { get; set; }

        /// <summary>
        /// Gets or sets a value indicates how to serialize or deserialize the default value.
        /// If True, the default value will not be serialized or deserialized.
        /// Otherwise, it will be serialized or deserialized.
        /// </summary>
        public bool SkipIsDefault { get; set; }

        /// <summary>
        /// Gets or sets a value indicates whether to serialize to javascript format.
        /// </summary>
        /// <remarks>
        /// If some property type is string in server side, but its type is Function in client side,
        /// when IsJavascriptFormat is set to true, it will be serialzed to a string without quote.
        /// For example, the value of property A is "btnClick", it will be serialized to "{a: btnClick}".
        /// Otherwise, it will be serialized to "{a: 'btnClick'}". 
        /// Normally when the page is loaded in the first time and the properties are transfered to the client side, this property should be set to True.
        /// For callback, it will be set to false.
        /// </remarks>
        public bool IsJavascriptFormat { get; set; }

        /// <summary>
        /// Gets or sets a value indicates how reference loop is handled.
        /// </summary>
        /// <remarks>
        /// The object may has reference loop (e.g. a class referencing itself).
        /// The default is true which means does not serialize the member if it cause a reference loop.
        /// </remarks>
        public bool SkipReferenceLoop { get; set; }

        /// <summary>
        /// Gets or sets the resolver.
        /// </summary>
        public JsonResolver Resolver
        {
            get { return Resolvers.FirstOrDefault() as JsonResolver; }
            set
            {
                Resolvers.Clear();
                Resolvers.Add(value);
            }
        }

        #endregion Properties

        #region Ctors

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonSetting"/> class.
        /// </summary>
        public JsonSetting()
        {
            SkipIsDefault = true;
            SkipReferenceLoop = true;
        }

        #endregion Ctors
    }
}
