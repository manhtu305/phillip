﻿using C1.Web.Mvc.Serialization;
using System;
using System.ComponentModel;

namespace C1.JsonNet
{
    /// <summary>
    /// Defines the attribute for json serialization and deserialization.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class JsonAttribute : C1NameAttribute
    {
        #region Ctors
        /// <summary>
        /// Initializes a new instance of the <see cref="JsonAttribute"/> class.
        /// </summary>
        /// <param name="order">A <see cref="System.Int32"/> value indicating the order of serialization or deserialization.</param>
        public JsonAttribute(int order)
            : this(null, order, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonAttribute"/> class.
        /// </summary>
        /// <param name="skipIfDefault">A flag indicating whether the default value should be serialized or deserialized.</param>
        public JsonAttribute(bool skipIfDefault)
            : this(skipIfDefault, -1, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonAttribute"/> class.
        /// </summary>
        /// <param name="propertyName">The specified name the property would be serialized.</param>
        public JsonAttribute(string propertyName)
            : this(null, -1, propertyName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonAttribute"/> class.
        /// </summary>
        /// <param name="skipIfDefault">A flag indicating whether the default value should be serialized or deserialized.</param>
        /// <param name="order">A <see cref="System.Int32"/> value indicating the order of serialization or deserialization.</param>
        /// <param name="propertyName">The specified name the property would be serialized.</param>
        public JsonAttribute(bool skipIfDefault, int order, string propertyName)
            :base(propertyName)
        {
            SkipIfDefault = skipIfDefault;
            Order = order;
        }

        // In future this attribute will be divided into three ones. Each control one property.
        private JsonAttribute(bool? skipIfDefault, int order, string propertyName)
            :base(propertyName)
        {
            SkipIfDefault = skipIfDefault;
            Order = order;
        }
        #endregion Ctors

        #region Properties

        /// <summary>
        /// Gets a value indicates the order of the seriailization or deserialization.
        /// </summary>
        /// <remarks>
        /// The property which has greater order, it will be serialized or deserialized earlier. 
        /// </remarks>
        public int Order { get; internal set; }
        /// <summary>
        /// Gets a value indicates whether to serialize or deserialize the default value.
        /// </summary>
        public bool? SkipIfDefault { get; internal set; }

        /// <summary>
        /// Gets a value indicates the serialized property name.
        /// </summary>
        [Obsolete("Use Name instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string PropertyName
        {
            get { return Name; }
            internal set { Name = value; }
        }

        #endregion Properties

    }

    /// <summary>
    /// Defines the attribute which represents the proerty should not be serialized.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class IgnoreAttribute : C1IgnoreAttribute
    {
    }

    /// <summary>
    /// Instructs the <see cref="JsonConverterAttribute"/> to use the specified <see cref="JsonConverter"/> when serializing the member or class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface | AttributeTargets.Enum | AttributeTargets.Parameter, AllowMultiple = false)]
    public sealed class JsonConverterAttribute : C1ConverterAttribute
    {
        /// <summary>
        /// Creates an instance of <see cref="JsonConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        public JsonConverterAttribute(Type converterType)
            :base(converterType)
        {
        }

        /// <summary>
        /// Creates an instance of <see cref="JsonConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        /// <param name="converterParameters">The parameters which used to create the converter instance.</param>
        public JsonConverterAttribute(Type converterType, params object[] converterParameters)
            : base(converterType, converterParameters)
        {
        }
    }

    /// <summary>
    /// Defines the attribute which specifies the item type of a collection.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class)]
    public sealed class CollectionItemTypeAttribute : C1CollectionItemConverterAttribute
    {
        private readonly Type _collectionItemType;

        /// <summary>
        /// Gets the type of the item.
        /// </summary>
        public Type CollectionItemType
        {
            get { return _collectionItemType; }
        }

        /// <summary>
        /// Gets the converter type of item.
        /// </summary>
        public Type ItemConverterType { get; private set; }

        /// <summary>
        /// Gets parameter list to use when constructing the JsonConverter described by <see cref="ItemConverterType"/>.
        /// If null, the default constructor is used.
        /// </summary>
        public object[] ItemConverterParameters { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionItemTypeAttribute"/> class.
        /// </summary>
        /// <param name="collectionItemType">The item type</param>
        /// <param name="itemConverterType">The converter type of item</param>
        /// <param name="itemConverterParameters">The parameter list to use when constructing the item converter</param>
        public CollectionItemTypeAttribute(Type collectionItemType, Type itemConverterType = null,
           params object[] itemConverterParameters)
            : base(itemConverterType, itemConverterParameters)
        {
            if (collectionItemType == null)
                throw new ArgumentNullException("collectionItemType");

            _collectionItemType = collectionItemType;
            ItemConverterType = itemConverterType;
            ItemConverterParameters = itemConverterParameters;
        }

        /// <summary>
        /// Creates an instance of <see cref="CollectionItemTypeAttribute"/>.
        /// </summary>
        /// <param name="itemConverterType">The converter type.</param>
        /// <param name="itemConverterParameters">The parameters used to create the converter.</param>
        public CollectionItemTypeAttribute(Type itemConverterType,
           params object[] itemConverterParameters)
            : this(null, itemConverterType, itemConverterParameters)
        {
        }

        /// <summary>
        /// Creates an instance of <see cref="CollectionItemTypeAttribute"/>.
        /// </summary>
        /// <param name="itemConverterType">The converter type.</param>
        public CollectionItemTypeAttribute(Type itemConverterType)
            :this(null, itemConverterType)
        {
        }
    }
}
