﻿using C1.Web.Mvc.Localization;
using C1.Web.Mvc.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace C1.JsonNet
{
    /// <summary>
    /// Represents a reader that provides access to serialized Json data.
    /// </summary>
    public class JsonReader : BaseReader, IJsonOperation
    {
        #region Fileds
        private bool _hasContent = false;
        private static Regex DateTimeTextReg = new Regex("^(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2}(?:\\.\\d*)?)(:\\d*)?(Z|[\\+\\-]\\d{2}:\\d{2}|)$");
        #endregion Fields

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the JsonReader class.
        /// </summary>
        /// <param name="reader">The text reader.</param>
        /// <param name="setting">The json setting.</param>
        public JsonReader(TextReader reader, JsonSetting setting)
            : base(new BaseContext(setting), new JsonAttributeHelper(), reader)
        {
            try
            {
                SetCurrent(ReadJsonValue());
            }
            catch (Exception) { }
        }
        #endregion Ctors

        #region Properties
        /// <summary>
        /// Gets or sets the current value which need be deserialized.
        /// </summary>
        /// <remarks>
        /// It is maybe a <see cref="ArrayList"/>, a <see cref="Hashtable"/>, a <see cref="DateTime"/>,
        /// a <see cref="String"/>, a <see cref="Boolean"/>, a <see cref="Int32"/>, 
        /// a <see cref="Double"/> or null.
        /// </remarks>
        public object Current { get; set; }

        internal Hashtable Parent { get; set; }

        /// <summary>
        /// Gets the json settings.
        /// </summary>
        public JsonSetting JsonSetting
        {
            get
            {
                return Context != null ? Context.Setting as JsonSetting : null;
            }
        }
        #endregion Properties

        #region Methods
        #region Public
        /// <summary>
        /// Deserialize the text into an instance of the specified type which T stands.
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <returns>An instance of T</returns>
        public T ReadValue<T>()
        {
            try
            {
                return (T)ReadValue(typeof(T));
            }
            catch (Exception)
            {
                throw new FormatException(string.Format(Resources.C1JsonDeserializeToTypeError, typeof(T).FullName));
            }
        }

        /// <summary>
        ///  Deserialize the text into an instance of the specified type
        /// </summary>
        /// <param name="type">The specified type</param>
        /// <returns>An instance of the specified type.</returns>
        public object ReadValue(Type type = null)
        {
            ValidateCurrent();
            if (type == null)
            {
                return Current;
            }
            object value = ReadMemberValue(type, null);
            ResetCurrent();
            return value;
        }

        /// <summary>
        /// Read the field from the data according to the specified name
        /// </summary>
        /// <param name="name">The specified name of the field you want to read.</param>
        /// <returns>True, if the field is read successfully. Otherwise, the field doesn't exist in the data. </returns>
        public bool ReadName(string name)
        {
            ResetCurrent();
            if (Parent != null)
            {
                object current = null;
                if (Parent.ContainsKey(name))
                {
                    current = Parent[name];
                }
                else
                {
                    name = name.ToCamelCase();
                    if (Parent.ContainsKey(name))
                    {
                        current = Parent[name];
                    }
                    else
                    {
                        name = name.ToPascalCase();
                        if (Parent.ContainsKey(name))
                        {
                            current = Parent[name];
                        }
                    }
                    
                } 
                if (current != null)
                {
                    SetCurrent(current is Hashtable ? ((Hashtable)current).Clone() : current);
                }
            }
            return _hasContent;
        }
        #endregion Public

        #region The Current Management
        private void ResetCurrent()
        {
            Current = null;
            _hasContent = false;
        }

        private void SetCurrent(object content)
        {
            Current = content;
            _hasContent = true;
        }

        private void ValidateCurrent()
        {
            if (!_hasContent)
            {
                throw new Exception(Resources.C1JsonReadJsonTextIsEmpty);
            }
        }
        #endregion The Current Management

        #region Assistant
        #region Utility
        internal static void ReadMember(JsonReader reader, object pd, string name, JsonConverter jctor, object parent)
        {
            Utility.ValidNullParameter(pd, "pd");
            Utility.ValidNullParameter(parent, "parent");
            string propertyName = JsonUtility.GetSerializedName(pd, name, reader == null ? null : reader.JsonSetting);
            if (reader.ReadName(propertyName))
            {
                reader.ReadMemberValue(pd, parent, jctor);
            }
        }

        private object ReadMemberValue(Type type, JsonConverter jctor, object existedValue = null)
        {
            Utility.ValidNullParameter(type, "type");
            object value = null;
            ValidateCurrent();
            var jc = GetConverter(null, existedValue, jctor) as JsonConverter;
            if (jc != null)
            {
                value = jc.Read(this, type, existedValue);
            }
            else
            {
                value = GetValue(Current, type);
            }
            return value;
        }

        private void ReadMemberValue(object pd, object parent, JsonConverter jctor)
        {
            Utility.ValidNullParameter(pd, "pd");
            Utility.ValidNullParameter(parent, "parent");
            object existedValue = Utility.GetValue(pd, parent);
            Type memberType = Utility.GetType(pd);
            ValidateCurrent();
            var jc = GetConverter(pd, existedValue, jctor) as JsonConverter;
            if (jc != null)
            {
                object value = jc.Read(this, memberType, existedValue);
                SetValue(pd, parent, value);
            }
            else
            {
                GetValue(Current, memberType, pd, parent);
            }
        }

        private object GetValue(object currentValue, Type type, object pd = null, object parent = null)
        {
            bool isNullableType = Utility.IsNullableType(type);

            if (isNullableType)
            {
                if (currentValue == null && pd != null && parent != null)
                {
                    JsonUtility.SetValue(pd, parent, null);
                    return null;
                }
                type = Nullable.GetUnderlyingType(type);
            }

            object oldValue = null;
            if (pd != null && parent != null)
            {
                oldValue = Utility.GetValue(pd, parent);
            }

            if (typeof(IDictionary).IsAssignableFrom(type))
            {
                IDictionary existedValue = oldValue as IDictionary ?? Utility.GetInstance<IDictionary>(type, null);
                Utility.ValidNullParameter(existedValue, "type", "Cannot create an instance of " + type.FullName);
                IDictionary jsonDic = currentValue as IDictionary;
                Utility.ValidNullParameter(jsonDic, "currentValue", "Invalid type of currentValue.");
                existedValue.Clear();
                foreach (DictionaryEntry de in jsonDic)
                {
                    existedValue.Add(de.Key, de.Value);
                }
                if (oldValue == null)
                {
                    oldValue = existedValue;
                }
            }
            else if (JsonUtility.HasImplementIList(type, oldValue))
            {
                Type itemType = null;
                CollectionItemTypeAttribute citAttr = Utility.GetAttribute<CollectionItemTypeAttribute>(type);
                JsonConverter itemConverter = null;
                if (citAttr != null)
                {
                    itemType = citAttr.CollectionItemType;
                    var itemConverterType = citAttr.ItemConverterType;
                    if (itemConverterType != null)
                    {
                        itemConverter = Utility.GetInstance<JsonConverter>(itemConverterType, citAttr.ItemConverterParameters);
                    }
                }
                if (itemType == null)
                {
                    itemType = type.GetElementType();
                }
                if (itemType == null)
                {
                    itemType = type.GetGenericArguments()[0];
                }
                if (itemType == null)
                {
                    itemType = typeof(string);
                }
                var arJsonValue = currentValue as List<object>;
                Utility.ValidNullParameter(arJsonValue, "currentValue", "Invalid type of currentValue.");
                var jsItems = arJsonValue.ToList();
                IList existedValue;
                Type implementedType = typeof(List<>).MakeGenericType(itemType);
                existedValue = oldValue as IList ?? Utility.GetInstance<IList>(implementedType, null);
                Utility.ValidNullParameter(existedValue, "type", "Cannot create an instance of " + type.FullName);
                if (!existedValue.IsFixedSize)
                {
                    existedValue.Clear();
                }
                foreach (object jsItem in jsItems)
                {
                    SetCurrent(jsItem);
                    object item = ReadMemberValue(itemType, itemConverter);
                    existedValue.Add(item);
                }
                if (existedValue.IsFixedSize || type.IsArray)
                {
                    oldValue = implementedType.GetMethod("ToArray").Invoke(existedValue, null);
                } 
                else if (oldValue == null)
                {
                    oldValue = existedValue;
                }
            }
            else if (typeof(Enum).IsAssignableFrom(type))
            {
                oldValue = JsonUtility.GetEnumValue(currentValue, type);
            }
            else if (type.IsPrimitive() || typeof(string).IsAssignableFrom(type) || type == typeof(decimal))
            {
                if (typeof(char) == type)
                {
                    string str = currentValue.ToString();
                    oldValue = str.ToCharArray()[0];
                }
                try
                {
                    oldValue = Convert.ChangeType(currentValue, type);
                }
                catch (Exception)
                {
                    throw new FormatException(string.Format(Resources.C1JsonDeserializeToTypeError, type.FullName));
                }
            }
#if !ASPNETCORE
            else if (type == typeof(System.Drawing.Color))
            {
                oldValue = Converters.HtmlColorConverter.Instance.Read(this, type, oldValue);
            }
#endif
            else if(type == typeof(System.TimeSpan))
            {
                TimeSpan newValue;
                if(TimeSpan.TryParseExact((string)currentValue, "c", null, out newValue))
                {
                    oldValue = newValue;
                }
            }
            else if (type == typeof(object) || type.IsValueType())
            {
                oldValue = currentValue;
            }
            else
            {
                object existedValue = ReadCustomizeObject(currentValue, type, oldValue);
                if (oldValue == null)
                {
                    oldValue = existedValue;
                }
            }

            if (pd != null && parent != null)
            {
                JsonUtility.SetValue(pd, parent, oldValue);
            }
            return oldValue;
        }

        private object ReadCustomizeObject(object jsonValue, Type type, object parent)
        {
            object obj = parent ?? Utility.GetInstance<object>(type);

            var descriptors = new List<object>();

            // Append public fields:
            descriptors.AddRange(obj.GetType().GetFields());

            // Append public properties:
#if ASPNETCORE
            descriptors.AddRange(obj.GetType().GetProperties());
#else
            PropertyDescriptorCollection pdCollection = TypeDescriptor.GetProperties(obj);
            foreach (var pd in pdCollection)
            {
                descriptors.Add(pd);
                
            }
#endif

            descriptors.Sort(MemberComparer.CacheComparer);

            Hashtable parentJson = jsonValue as Hashtable;
            if (parentJson == null || parentJson.Count == 0)
            {
                return obj;
            }
            //backup the parent value
            Hashtable bakParent = Parent != null ? Parent.Clone() as Hashtable : null;
            Parent = parentJson.Clone() as Hashtable;

            // read the item
            foreach (var pd in descriptors)
            {
                bool processed = false;
                object val = Utility.GetValue(pd, obj);
                string memberName = Utility.GetName(pd);
                if (JsonSetting != null && JsonSetting.Resolver != null)
                {
                    if (JsonSetting.Resolver.CanResolve(memberName, val, Utility.GetType(pd)))
                    {
                        JsonSetting.Resolver.Deserialize(pd, obj, this);
                        processed = true;
                    }
                }
                if (!processed)
                {
                    ReadMember(this, pd, memberName, null, obj);
                }
            }
            //restore the parent value
            Parent = bakParent;
            return obj;
        }

        private void SetValue(object pd, object parent, object value)
        {
            Utility.ValidNullParameter(pd, "pd");
            Utility.ValidNullParameter(parent, "parent");
            Type propertyType = Utility.GetType(pd);
            if (typeof(IDictionary).IsAssignableFrom(propertyType))
            {
                SetIDictionaryValue(pd, parent, value);
            }
            else if (JsonUtility.HasImplementIList(propertyType, Utility.GetValue(pd, parent)))
            {
                SetIListValue(pd, parent, value);
            }
            else
            {
                JsonUtility.SetValue(pd, parent, value);
            }
        }

        private string ReadName(bool allowQuotes = true)
        {
            switch (PeekNextSignificantCharacter())
            {
                case '"':
                case '\'':
                    if (!allowQuotes)
                    {
                        return null;
                    }
                    return ReadString();
            }
            StringBuilder builder1 = new StringBuilder();
            while (true)
            {
                char ch1 = (char)((ushort)Reader.Peek());
                if ((ch1 != '_') && !char.IsLetterOrDigit(ch1))
                {
                    return builder1.ToString();
                }
                Reader.Read();
                builder1.Append(ch1);
            }
        }
        #endregion Utility

        #region Collection
        private static void SetIDictionaryValue(object pd, object parent, object value)
        {
            if (!(value is IDictionary))
            {
                return;
            }
            object existedValue = Utility.GetValue(pd, parent);
            IDictionary option = existedValue as IDictionary;
            Utility.ValidNullParameter(existedValue, "The value of the dictionary");
            option.Clear();
            foreach (DictionaryEntry de in value as IDictionary)
            {
                option.Add(de.Key, de.Value);
            }
        }
        private static void SetIListValue(object pd, object parent, object value)
        {
            if (!(value is IList))
            {
                return;
            }
            JsonUtility.SetValue(pd, parent, value);
        }
        #endregion Collection

        #region Json Value
        /// <summary>
        /// Read the simple value directly from the json string.
        /// </summary>
        /// <returns>Hashtable, string, number, boolean, null, array, datetime or an exception</returns>
        private object ReadJsonValue()
        {
            object obj1 = null;
            bool flag1 = false;
            char ch1 = PeekNextSignificantCharacter();
            switch (ch1)
            {
                case '[':
                    obj1 = ReadArray();
                    break;

                case '{':
                    obj1 = ReadHashtable();
                    break;

                case '\'':
                case '"':
                    {
                        string text1 = ReadString();
                        if (text1 == "@")
                        {
                            // Delta(ANSCH000091) fix:
                            obj1 = text1;
                        }

                        try
                        {
                            var matchResult = DateTimeTextReg.Match(text1);
                            if (matchResult.Success)
                            {
                                var date = DateTime.Parse(text1);
                                var timeZoneText = matchResult.Groups[8].Value.ToLower();
                                // When the text contains "z" in the end, it is a utc date.
                                // So we need convert it to Utc date manually as DateTime.Parse cannot do it.
                                if(timeZoneText == "z")
                                {
                                    date = date.ToUniversalTime();
                                }
                                obj1 = date;
                            }
                        }
                        catch (ArgumentException)
                        {
                            // Syntax error in the regular expression
                        }

                        if (obj1 == null)
                        {
                            obj1 = text1;
                        }
                        break;
                    }
                default:
                    if ((char.IsDigit(ch1) || (ch1 == '-')) || (ch1 == '.'))
                    {
                        obj1 = ReadNumber();
                    }
                    else if ((ch1 == 't') || (ch1 == 'f'))
                    {
                        obj1 = ReadBoolean();
                    }
                    else if (ch1 == 'n')
                    {
                        ReadNull();
                        flag1 = true;
                    }
                    break;
            }
            if ((obj1 == null) && !flag1)
            {
                throw new FormatException(Resources.C1JsonReadInvalidJsonText);
            }
            return obj1;
        }

        private Hashtable ReadHashtable()
        {
            Hashtable hashtable1 = new Hashtable();
            Reader.Read();
            bool recycled = true;
            while (recycled)
            {
                char ch1 = PeekNextSignificantCharacter();
                switch (ch1)
                {
                    case '\0':
                        throw new FormatException(string.Format(Resources.C1JsonReadUnterminatedLiteral, "object"));

                    case '}':
                        Reader.Read();
                        recycled = false;
                        continue;
                }
                if (hashtable1.Count != 0)
                {
                    if (ch1 != ',')
                    {
                        throw new FormatException(string.Format(Resources.C1JsonReadInvalidLiteral, "object"));
                    }
                    Reader.Read();
                }
                string text1 = ReadName(true);
                ch1 = PeekNextSignificantCharacter();
                if (ch1 != ':')
                {
                    throw new FormatException(Resources.C1JsonReadUnexpectedLiteral);
                }
                Reader.Read();
                object obj1 = ReadJsonValue();
                hashtable1[text1] = obj1;
            }
            return hashtable1;
        }

        private char PeekNextSignificantCharacter()
        {
            char ch1 = (char)((ushort)Reader.Peek());
            while ((ch1 != '\0') && char.IsWhiteSpace(ch1))
            {
                Reader.Read();
                ch1 = (char)((ushort)Reader.Peek());
            }
            return ch1;
        }

        private object ReadArray()
        {
            var list = new List<object>();
            Reader.Read();
            while (true)
            {
                char ch = PeekNextSignificantCharacter();
                switch (ch)
                {
                    case '\0':
                        throw new FormatException(string.Format(Resources.C1JsonReadUnterminatedLiteral, "array"));

                    case ']':
                        Reader.Read();
                        return list;
                }
                if (list.Count != 0)
                {
                    if (ch != ',')
                    {
                        throw new FormatException(string.Format(Resources.C1JsonReadInvalidLiteral, "array"));
                    }
                    Reader.Read();
                }
                object obj = ReadJsonValue();
                list.Add(obj);
            }
        }

        private string ReadString()
        {
            char ch2;
            StringBuilder builder1 = new StringBuilder();
            char ch1 = (char)((ushort)Reader.Read());
            bool escapeFound = false;

            while (true)
            {
                ch2 = (char)((ushort)Reader.Read());
                if (ch2 == '\0')
                {
                    throw new FormatException(string.Format(Resources.C1JsonReadUnterminatedLiteral, "string"));
                }
                if (escapeFound)
                {
                    // read \t, \n, \r as special character:
                    switch (ch2)
                    {
                        case 't':
                            builder1.Append('\t');
                            break;
                        case 'n':
                            builder1.Append('\n');
                            break;
                        case 'r':
                            builder1.Append('\r');
                            break;
                        default:
                            // by default skip escape and read next charcter:
                            builder1.Append(ch2);
                            break;
                    }
                    escapeFound = false;
                    continue;
                }
                if (ch2 == '\\')
                {
                    escapeFound = true;
                    continue;
                }
                if (ch2 == ch1)
                {
                    return builder1.ToString();
                }
                builder1.Append(ch2);
            }
        }

        private object ReadNumber()
        {
            char ch1 = (char)((ushort)Reader.Read());
            StringBuilder builder1 = new StringBuilder();
            bool isDouble = ch1 == '.';
            builder1.Append(ch1);
            while (true)
            {
                ch1 = PeekNextSignificantCharacter();
                if (!char.IsDigit(ch1) && (ch1 != '.') && ch1 != 'e' && ch1 != 'E' && ch1 != '+' && ch1 != '-')
                {
                    break;
                }
                isDouble = (isDouble || ch1 == '.');
                Reader.Read();
                builder1.Append(ch1);
            }
            string text1 = builder1.ToString();
            if (!isDouble)
            {
                int num1;
                if (int.TryParse(text1, out num1))
                {
                    return num1;
                }

                isDouble = true;
            }

            if (isDouble)
            {
                double d;
                if (double.TryParse(text1, NumberStyles.Float | NumberStyles.AllowThousands, System.Globalization.CultureInfo.InvariantCulture, out d))
                {
                    return d;
                }
                d = double.Parse(text1, System.Globalization.CultureInfo.InvariantCulture);
                return d;
            }

            throw new FormatException(string.Format(Resources.C1JsonReadInvalidLiteral, "numeric"));
        }

        private bool ReadBoolean()
        {
            string text1 = ReadName(false);
            if (text1 != null)
            {
                if (text1.Equals("true", StringComparison.Ordinal))
                {
                    return true;
                }
                if (text1.Equals("false", StringComparison.Ordinal))
                {
                    return false;
                }
            }
            throw new FormatException(string.Format(Resources.C1JsonReadInvalidLiteral, "boolean"));
        }

        private void ReadNull()
        {
            string text1 = ReadName(false);
            if ((text1 == null) || !text1.Equals("null", StringComparison.Ordinal))
            {
                throw new FormatException(string.Format(Resources.C1JsonReadInvalidLiteral, "null"));
            }
        }
        #endregion Json Value
        #endregion Assistant

        #region Dispose
        /// <summary>
        /// Dispose all the resources used.
        /// </summary>
        public override void Dispose()
        {
            Reader.Dispose();
            ResetCurrent();
            Parent = null;
            base.Dispose();
        }
        #endregion Dispose
        #endregion Methods
    }
}
