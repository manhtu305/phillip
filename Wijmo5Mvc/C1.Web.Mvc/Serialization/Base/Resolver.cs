﻿using System;

namespace C1.Web.Mvc.Serialization
{
    #region Base
    /// <summary>
    /// Defines the base class for resolver.
    /// </summary>
    public abstract class BaseResolver
    {
        /// <summary>
        /// Determines whether the object can use this resolver.
        /// </summary>
        /// <param name="name">The name of the object.</param>
        /// <param name="value">The object value.</param>
        /// <param name="type">The object type.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// A <see cref="bool"/> value indicates whether to be resovled.
        /// If true, it means the object can be resolved.
        /// Otherwise, the resolver cannot support this object.
        ///</returns>
        public virtual bool CanResolve(string name, object value, Type type, IContext context)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the converter for this resolver.
        /// </summary>
        /// <param name="name">The name of the object.</param>
        /// <param name="value">The object value.</param>
        /// <param name="type">The object type.</param>
        /// <param name="context">The context.</param>
        /// <returns>A converter.</returns>
        public virtual BaseConverter ResolveConverter(string name, object value, Type type, IContext context)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the name after being resolved.
        /// </summary>
        /// <param name="name">The name of the object.</param>
        /// <param name="value">The object value.</param>
        /// <param name="type">The object type.</param>
        /// <param name="context">The context.</param>
        /// <returns>A string.</returns>
        public virtual string ResolvePropertyName(string name, object value, Type type, IContext context)
        {
            return name;
        }

        internal void Serialize(BaseWriter writer, object memberInfo, object parent, IContext context)
        {
            var memberName = Utility.GetName(memberInfo);
            var memberValue = Utility.GetValue(memberInfo, parent);
            var memberType = Utility.GetType(memberInfo);
            if (CanResolve(memberName, memberValue, memberType, context))
            {
                memberName = ResolvePropertyName(memberName, memberValue, memberType, context);
                var converter = ResolveConverter(memberName, memberValue, memberType, context);
                writer.WriteMemberInfoWithSettings(memberName, memberValue, memberInfo, converter);
            }
        }
    }
    #endregion Base

    #region ClientEvents
    internal class C1ClientEventResolver : BaseResolver
    {
        private const string CLIENT_EVENT_PREFIX = "OnClient";

        public override bool CanResolve(string name, object value, Type type, IContext context)
        {
            return CheckOnClientEventName(name);
        }

        public override BaseConverter ResolveConverter(string name, object value, Type type, IContext context)
        {
            throw new NotImplementedException();
        }

        public override string ResolvePropertyName(string name, object value, Type type, IContext context)
        {
            return name.Substring(CLIENT_EVENT_PREFIX.Length);
        }

        private static bool CheckOnClientEventName(string name)
        {
            return !string.IsNullOrEmpty(name) && name.StartsWith(CLIENT_EVENT_PREFIX);
        }
    }
    #endregion ClientEvents

}
