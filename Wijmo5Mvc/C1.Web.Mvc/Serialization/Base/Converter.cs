﻿using System;
using System.Drawing;

namespace C1.Web.Mvc.Serialization
{
    #region Base
    /// <summary>
    /// Defines the base class for the converter.
    /// </summary>
    public abstract class BaseConverter
    {
        #region Read
        // Todo
        //public virtual bool CanRead(BaseReader reader, Type type, Context context)
        //{
        //    return CanConvert(type);
        //}
        //public abstract void Read(BaseReader reader, object memberInfo, Context context);
        //internal void Deserialize(BaseReader reader, object memberInfo, Context context)
        //{
        //    var type = Utility.GetType(memberInfo);
        //    if (CanRead(reader, type, context))
        //    {
        //        Read(reader, memberInfo, context);
        //    }
        //}
        #endregion Read

        /// <summary>
        /// Gets a value indicating whether this <see cref="BaseConverter" /> can read.
        /// </summary>
        /// <value><c>true</c> if this <see cref="BaseConverter" /> can read; otherwise, <c>false</c>.</value>
        protected internal virtual bool CanRead
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="BaseConverter" /> can write.
        /// </summary>
        /// <value><c>true</c> if this <see cref="BaseConverter" /> can write; otherwise, <c>false</c>.</value>
        protected internal virtual bool CanWrite
        {
            get { return true; }
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="context">The context information.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        protected internal abstract bool CanConvert(Type objectType, IContext context);

        /// <summary>
        /// Writes the specified value.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="context">The context.</param>
        protected abstract void Write(BaseWriter writer, object value, IContext context);

        internal void Serialize(BaseWriter writer, object value, IContext context = null)
        {
            if (CanWrite)
            {
                Write(writer, value, context);
            }
        }

    }
    #endregion Base
    /// <summary>
    /// Defines the converter to remove the inherited converter.
    /// </summary>

    #region DefaultConverter
    public class DefaultConverter : BaseConverter
    {
        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="context">The context information.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        protected internal override bool CanConvert(Type objectType, IContext context)
        {
            return true;
        }

        /// <summary>
        /// Writes the specified value.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="context">The context.</param>
        protected override void Write(BaseWriter writer, object value, IContext context)
        {
            writer.WriteObject(value);
        }
    }
    #endregion RemoveInheritedConverter

    #region Point Converter
    internal class PointConverter: BaseConverter
    {
        protected internal override bool CanConvert(Type objectType, IContext context)
        {
            return objectType == typeof(Point) || objectType == typeof(PointF);
        }

        protected override void Write(BaseWriter writer, object value, IContext context)
        {
            throw new NotImplementedException();
        }
    }
    #endregion Point Converter
}
