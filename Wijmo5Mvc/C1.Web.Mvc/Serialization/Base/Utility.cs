﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace C1.Web.Mvc.Serialization
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal static class Utility
    {
        #region Fields
        public static readonly Dictionary<Type, PrimitiveTypeCode> TypeCodeMap =
            new Dictionary<Type, PrimitiveTypeCode>
            {
                { typeof(char), PrimitiveTypeCode.Char },
                { typeof(char?), PrimitiveTypeCode.CharNullable },
                { typeof(bool), PrimitiveTypeCode.Boolean },
                { typeof(bool?), PrimitiveTypeCode.BooleanNullable },
                { typeof(sbyte), PrimitiveTypeCode.SByte },
                { typeof(sbyte?), PrimitiveTypeCode.SByteNullable },
                { typeof(short), PrimitiveTypeCode.Int16 },
                { typeof(short?), PrimitiveTypeCode.Int16Nullable },
                { typeof(ushort), PrimitiveTypeCode.UInt16 },
                { typeof(ushort?), PrimitiveTypeCode.UInt16Nullable },
                { typeof(int), PrimitiveTypeCode.Int32 },
                { typeof(int?), PrimitiveTypeCode.Int32Nullable },
                { typeof(byte), PrimitiveTypeCode.Byte },
                { typeof(byte?), PrimitiveTypeCode.ByteNullable },
                { typeof(uint), PrimitiveTypeCode.UInt32 },
                { typeof(uint?), PrimitiveTypeCode.UInt32Nullable },
                { typeof(long), PrimitiveTypeCode.Int64 },
                { typeof(long?), PrimitiveTypeCode.Int64Nullable },
                { typeof(ulong), PrimitiveTypeCode.UInt64 },
                { typeof(ulong?), PrimitiveTypeCode.UInt64Nullable },
                { typeof(float), PrimitiveTypeCode.Single },
                { typeof(float?), PrimitiveTypeCode.SingleNullable },
                { typeof(double), PrimitiveTypeCode.Double },
                { typeof(double?), PrimitiveTypeCode.DoubleNullable },
                { typeof(DateTime), PrimitiveTypeCode.DateTime },
                { typeof(DateTime?), PrimitiveTypeCode.DateTimeNullable },
#if !NET20
                { typeof(DateTimeOffset), PrimitiveTypeCode.DateTimeOffset },
                { typeof(DateTimeOffset?), PrimitiveTypeCode.DateTimeOffsetNullable },
#endif
                { typeof(decimal), PrimitiveTypeCode.Decimal },
                { typeof(decimal?), PrimitiveTypeCode.DecimalNullable },
                { typeof(Guid), PrimitiveTypeCode.Guid },
                { typeof(Guid?), PrimitiveTypeCode.GuidNullable },
                { typeof(TimeSpan), PrimitiveTypeCode.TimeSpan },
                { typeof(TimeSpan?), PrimitiveTypeCode.TimeSpanNullable },
//#if !(PORTABLE || PORTABLE40 || NET35 || NET20)
//              { typeof(BigInteger), PrimitiveTypeCode.BigInteger },
//              { typeof(BigInteger?), PrimitiveTypeCode.BigIntegerNullable },
//#endif
                { typeof(Uri), PrimitiveTypeCode.Uri },
                { typeof(string), PrimitiveTypeCode.String },
                { typeof(byte[]), PrimitiveTypeCode.Bytes },
#if !(PORTABLE || PORTABLE40 || NETFX_CORE)
                { typeof(DBNull), PrimitiveTypeCode.DBNull }
#endif
            };
        private static object _lock = new object();
        private static Dictionary<object, Dictionary<Type, Attribute>> _MemberInfoAttributesCache
            = new Dictionary<object, Dictionary<Type, Attribute>>(new PropertyComparer());
        private static Dictionary<Type, Dictionary<Type, Attribute>> _TypeAttributesCache
            = new Dictionary<Type, Dictionary<Type, Attribute>>();

        private static string CLIENT_EVENT_PREFIX = "OnClient";
        #endregion Fields

        #region ShouldSerialize
        public static bool? InvokeShouldSerializeMethod(object descriptor, object parent)
        {
            if (descriptor == null || parent == null)
            {
                return null;
            }

#if !ASPNETCORE
            PropertyDescriptor pd = descriptor as PropertyDescriptor;
            if (pd != null)
            {
                if (parent.GetType().GetMethod("ShouldSerialize" + pd.Name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance) != null)
                {
                    return pd.ShouldSerializeValue(parent);
                }
            }
#else
            var pi = descriptor as PropertyInfo;
            if (pi != null)
            {
                return InvokeShouldSerialize(parent, pi.Name);
            }
#endif

            var fi = descriptor as FieldInfo;
            if (fi != null)
            {
                return InvokeShouldSerialize(parent, fi.Name);
            }

            return null;
        }

        public static bool? InvokeShouldSerialize(object member, object name)
        {
            MethodInfo miSerialize = member.GetType().GetMethod("ShouldSerialize" + name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (miSerialize != null)
            {
                object oReturn = miSerialize.Invoke(member, null);
                if (oReturn is bool)
                {
                    return (bool)oReturn;
                }
            }

            return null;
        }
        #endregion ShouldSerialize

        #region Get Attribute
        public static T GetAttribute<T>(object memberInfo, object value) where T : Attribute
        {
            T result = null;
            if (memberInfo != null)
            {
                result = GetAttribute<T>(memberInfo);
            }

            if (result == null)
            {
                var type = (value == null ? GetType(memberInfo) : value.GetType());
                if (type != null)
                {
                    result = GetAttribute<T>(type);
                }
            }

            return result;
        }

        public static T GetAttribute<T>(object descriptor) where T : Attribute
        {
            T attr = null;
            if (descriptor == null)
            {
                return attr;
            }

            lock (_lock)
            {
                if (_MemberInfoAttributesCache.ContainsKey(descriptor))
                {
                    var attrs = _MemberInfoAttributesCache[descriptor];
                    if (attrs.ContainsKey(typeof(T)))
                    {
                        return attrs[typeof(T)] as T;
                    }
                }
                else
                {
                    _MemberInfoAttributesCache.Add(descriptor, new Dictionary<Type, Attribute>());
                }

                attr = GetMemberAttribute<T>(descriptor);

                _MemberInfoAttributesCache[descriptor].Add(typeof(T), attr);
            }

            return attr;
        }

        private static T GetMemberAttribute<T>(object descriptor) where T : Attribute
        {
#if !ASPNETCORE
            var pd = descriptor as PropertyDescriptor;
            if (pd != null)
            {
                return pd.Attributes[typeof(T)] as T;
            }
#else
            var pi = descriptor as PropertyInfo;
            if(pi != null)
            {
                return pi.GetCustomAttribute(typeof(T)) as T;
            }
#endif
            var mi = descriptor as MemberInfo;
            var tAttrs = mi.GetCustomAttributes(typeof(T), true).OfType<T>();
            return tAttrs.FirstOrDefault();
        }

        public static T GetAttribute<T>(Type type) where T : Attribute
        {
            T attr = null;
            if (type == null)
            {
                return attr;
            }

            lock (_lock)
            {
                if (_TypeAttributesCache.ContainsKey(type))
                {
                    var attrs = _TypeAttributesCache[type];
                    if (attrs.ContainsKey(typeof(T)))
                    {
                        return attrs[typeof(T)] as T;
                    }
                }
                else
                {
                    _TypeAttributesCache.Add(type, new Dictionary<Type, Attribute>());
                }

                attr = type.GetCustomAttributes(typeof(T), true).FirstOrDefault() as T;

                _TypeAttributesCache[type].Add(typeof(T), attr);
            }

            return attr;
        }
        #endregion Get Attribute

        #region Scope
        public static ScopeType GetScopeType(object value)
        {
            if (IsSimpleScope(value))
            {
                return ScopeType.Simple;
            }
#if !ASPNETCORE
            else if (value is System.Drawing.Color)
            {
                return ScopeType.SystemDrawingColor;
            }
#endif
            else if (value is CultureInfo)
            {
                return ScopeType.CultureInfo;
            }
            else if (IsDictionaryScope(value))
            {
                return ScopeType.IDictionary;
            }
            else if (value is IEnumerable)
            {
                return ScopeType.IEnumerable;
            }
            else
            {
                return ScopeType.Complex;
            }
        }

        private static bool IsSimpleScope(object value)
        {
            return value == null || value is DBNull
                || value is string
                || value is Guid
                || value is Enum
                || value is DateTime
                || value is CultureInfo
                || value is decimal
                || value.GetType().IsPrimitive()
                || value is TimeSpan;
        }

        private static bool IsDictionaryScope(object value)
        {
            return value.GetType().GetInterfaces().Any(x => x.Name == "IDictionary" || x.Name == "IDictionary`2");
        }
        #endregion Scope

        #region Descriptor
        public static List<object> GetMembers(object value)
        {
            var members = new List<object>();

            // Append public fields:
            members.AddRange(value.GetType().GetFields());

            // Append public properties:
#if ASPNETCORE
            members.AddRange(value.GetType().GetProperties());
#else
            PropertyDescriptorCollection pdCollection = TypeDescriptor.GetProperties(value);
            foreach (var pd in pdCollection)
            {
                members.Add(pd);
            }
#endif
            return members;
        }

        public static string GetName(object descriptor)
        {
            if (descriptor == null)
            {
                return null;
            }

            var pd = descriptor as
#if ASPNETCORE
                PropertyInfo;
#else
                PropertyDescriptor;
#endif

            if (pd != null)
            {
                return pd.Name;
            }

            FieldInfo fi = descriptor as FieldInfo;
            if (fi != null)
            {
                return fi.Name;
            }

            return null;
        }

        public static Type GetType(object descriptor)
        {
            if (descriptor == null)
            {
                return null;
            }

            var pd = descriptor as
#if ASPNETCORE
                PropertyInfo;
#else
                PropertyDescriptor;
#endif

            if (pd != null)
            {
                return pd.PropertyType;
            }

            FieldInfo fi = descriptor as FieldInfo;
            if (fi != null)
            {
                return fi.FieldType;
            }

            return null;
        }

        public static object GetValue(object descriptor, object parent)
        {
            if (parent == null || descriptor == null)
            {
                return null;
            }
#if !ASPNETCORE
            PropertyDescriptor pd = descriptor as PropertyDescriptor;
            if (pd != null)
            {
                return pd.GetValue(parent);
            }
#else
            PropertyInfo pi = descriptor as PropertyInfo;
            if (pi != null)
            {
                return pi.GetValue(parent);
            }
#endif
            FieldInfo fi = descriptor as FieldInfo;
            if (fi != null)
            {
                return fi.GetValue(parent);
            }
            return null;
        }
        #endregion Descriptor

        #region Create Instance
        public static T GetInstance<T>(Type type, params object[] parameters)
        {
            Func<object[], T> createor = GetCreator<T>(type);
            return createor(parameters);
        }

        /// <summary>
        /// Create a factory function that can be used to create instances of a JsonConverter described by the 
        /// argument type.  The returned function can then be used to either invoke the converter's default ctor, or any 
        /// parameterized constructors by way of an object array.
        /// </summary>
        private static Func<object[], T> GetCreator<T>(Type converterType)
        {
            Func<object> defaultConstructor = (HasDefaultConstructor(converterType, true))
                ? CreateDefaultConstructor<object>(converterType)
                : null;

            return (parameters) =>
            {
                try
                {
                    if (parameters != null && parameters.Length > 0)
                    {
                        Type[] paramTypes = parameters.Select(param => param.GetType()).ToArray();
                        ConstructorInfo parameterizedConstructorInfo = converterType.GetConstructor(paramTypes);

                        if (null != parameterizedConstructorInfo)
                        {
                            return (T)parameterizedConstructorInfo.Invoke(parameters);
                        }
                        else
                        {
                            throw new Exception("Parameterized constructor not found.");
                        }
                    }

                    if (defaultConstructor == null)
                        throw new Exception("NoParameterized constructor not found.");

                    return (T)defaultConstructor();
                }
                catch (Exception)
                {
                    throw new Exception("Creating constructor error.");
                }
            };
        }

        private static bool HasDefaultConstructor(Type t, bool nonPublic)
        {
            return (GetDefaultConstructor(t, nonPublic) != null);
        }

        private static ConstructorInfo GetDefaultConstructor(Type t)
        {
            return GetDefaultConstructor(t, false);
        }
        private static ConstructorInfo GetDefaultConstructor(Type t, bool nonPublic)
        {
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public;
            if (nonPublic)
                bindingFlags = bindingFlags | BindingFlags.NonPublic;

            return t.GetConstructors(bindingFlags).SingleOrDefault(c => !c.GetParameters().Any());
        }
        private static Func<T> CreateDefaultConstructor<T>(Type type)
        {
            if (type.IsValueType() || type.IsAbstract())
            {
                return () => (T)Activator.CreateInstance(type, true);
            }
            ConstructorInfo constructorInfo = GetDefaultConstructor(type, true);

            return () => (T)constructorInfo.Invoke(null);
        }
        #endregion Create Instance

        #region Equals
        public static bool ValueEquals(object objA, object objB)
        {
            if (objA == null && objB == null)
                return true;
            if (objA != null && objB == null)
                return false;
            if (objA == null)
                return false;

            // comparing an Int32 and Int64 both of the same value returns false
            // make types the same then compare
            if (objA.GetType() != objB.GetType())
            {
                if (IsInteger(objA) && IsInteger(objB))
                    return Convert.ToDecimal(objA, CultureInfo.CurrentCulture).Equals(Convert.ToDecimal(objB, CultureInfo.CurrentCulture));
                else if ((objA is double || objA is float || objA is decimal) && (objB is double || objB is float || objB is decimal))
                    return ApproxEquals(Convert.ToDouble(objA, CultureInfo.CurrentCulture), Convert.ToDouble(objB, CultureInfo.CurrentCulture));
                else
                    return false;
            }

            return objA.Equals(objB);
        }
        public static bool IsInteger(object value)
        {
            switch (GetTypeCode(value.GetType()))
            {
                case PrimitiveTypeCode.SByte:
                case PrimitiveTypeCode.Byte:
                case PrimitiveTypeCode.Int16:
                case PrimitiveTypeCode.UInt16:
                case PrimitiveTypeCode.Int32:
                case PrimitiveTypeCode.UInt32:
                case PrimitiveTypeCode.Int64:
                case PrimitiveTypeCode.UInt64:
                    return true;
                default:
                    return false;
            }
        }
        public static bool ApproxEquals(double d1, double d2)
        {
            const double epsilon = 2.2204460492503131E-16;

            if (d1 == d2)
                return true;

            double tolerance = ((Math.Abs(d1) + Math.Abs(d2)) + 10.0) * epsilon;
            double difference = d1 - d2;

            return (-tolerance < difference && tolerance > difference);
        }
        #endregion Equals

        #region TypeCode
        public static PrimitiveTypeCode GetTypeCode(Type t, out bool isEnum)
        {
            PrimitiveTypeCode typeCode;
            if (TypeCodeMap.TryGetValue(t, out typeCode))
            {
                isEnum = false;
                return typeCode;
            }

            if (t.IsEnum())
            {
                isEnum = true;
                return GetTypeCode(Enum.GetUnderlyingType(t));
            }

            // performance?
            if (IsNullableType(t))
            {
                Type nonNullable = Nullable.GetUnderlyingType(t);
                if (nonNullable.IsEnum())
                {
                    Type nullableUnderlyingType = typeof(Nullable<>).MakeGenericType(Enum.GetUnderlyingType(nonNullable));
                    isEnum = true;
                    return GetTypeCode(nullableUnderlyingType);
                }
            }

            isEnum = false;
            return PrimitiveTypeCode.Object;
        }

        public static PrimitiveTypeCode GetTypeCode(Type t)
        {
            bool isEnum;
            return GetTypeCode(t, out isEnum);
        }
        #endregion TypeCode

        #region Nullable
        public static bool IsNullable(Type t)
        {
            ValidNullParameter(t, "t");
            if (t.IsValueType())
            {
                return IsNullableType(t);
            }
            return true;
        }

        public static bool IsNullableType(Type t)
        {
            ArgumentNotNull(t, "t");

            return (t.IsGenericType() && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        internal static void ValidNullParameter(object para, string paraName, string msg = null)
        {
            if (para == null)
            {
                throw new ArgumentNullException(paraName, msg);
            }
        }

        internal static void ArgumentNotNull(object value, string parameterName)
        {
            if (value == null)
                throw new ArgumentNullException(parameterName);
        }
        #endregion Nullable

        internal static string QuoteJScriptString(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            StringBuilder builder1 = null;
            int num1 = 0;
            int num2 = 0;
            for (int num3 = 0; num3 < s.Length; num3++)
            {
                char ch1 = s[num3];
                if ((((ch1 == '\r') || (ch1 == '\t')) || ((ch1 == '"') || (ch1 == '\''))) || (((ch1 == '\\') || (ch1 == '\r')) || ((ch1 < ' ') || (ch1 > '\x007f'))))
                {
                    if (builder1 == null)
                    {
                        builder1 = new StringBuilder(s.Length + 6);
                    }
                    if (num2 > 0)
                    {
                        builder1.Append(s, num1, num2);
                    }
                    num1 = num3 + 1;
                    num2 = 0;
                }
                switch (ch1)
                {
                    case '\'':
                        builder1.Append("\u0027");
                        break;
                    case '\\':
                        builder1.Append(@"\\");
                        break;
                    case '\t':
                        builder1.Append(@"\t");
                        break;
                    case '\n':
                        builder1.Append(@"\n");
                        break;
                    case '\r':
                        builder1.Append(@"\r");
                        break;
                    case '"':
                        builder1.Append("\\\"");
                        break;
                    default:
                        if ((ch1 < ' ') || (ch1 > '\x007f'))
                        {
                            builder1.AppendFormat(CultureInfo.InvariantCulture, @"\u{0:x4}", new object[] { (int)ch1 });
                        }
                        else
                        {
                            num2++;
                        }
                        break;
                }
            }
            string text1 = s;
            if (builder1 == null)
            {
                return text1;
            }
            if (num2 > 0)
            {
                builder1.Append(s, num1, num2);
            }
            return builder1.ToString();
        }

        public static object GetDefaultValue(Type type)
        {
            if (!type.IsValueType() || IsNullable(type))
            {
                return null;
            }

            switch (GetTypeCode(type))
            {
                case PrimitiveTypeCode.Boolean:
                    return false;
                case PrimitiveTypeCode.Char:
                case PrimitiveTypeCode.SByte:
                case PrimitiveTypeCode.Byte:
                case PrimitiveTypeCode.Int16:
                case PrimitiveTypeCode.UInt16:
                case PrimitiveTypeCode.Int32:
                case PrimitiveTypeCode.UInt32:
                    return 0;
                case PrimitiveTypeCode.Int64:
                case PrimitiveTypeCode.UInt64:
                    return 0L;
                case PrimitiveTypeCode.Single:
                    return 0f;
                case PrimitiveTypeCode.Double:
                    return 0.0;
                case PrimitiveTypeCode.Decimal:
                    return 0m;
                case PrimitiveTypeCode.DateTime:
                    return new DateTime();
                //#if !(PORTABLE || PORTABLE40 || NET35 || NET20)
                //				case PrimitiveTypeCode.BigInteger:
                //					return new BigInteger();
                //#endif
                case PrimitiveTypeCode.Guid:
                    return new Guid();
#if !NET20
                case PrimitiveTypeCode.DateTimeOffset:
                    return new DateTimeOffset();
#endif
            }

            // possibly use IL initobj for perf here?
            return Activator.CreateInstance(type);
        }

        internal static bool IsEmptyEnumerable(IEnumerable enumerable)
        {
            if (enumerable == null)
            {
                return true;
            }
            foreach (var item in enumerable)
            {
                return false;
            }
            return true;
        }

        public static string EnsureDecimalPlace(double value, string text)
        {
            if (double.IsNaN(value) || double.IsInfinity(value) || text.IndexOf('.') != -1 || text.IndexOf('E') != -1 || text.IndexOf('e') != -1)
            {
                return text;
            }
            return text + ".0";
        }

        public static string EnsureDecimalPlace(string text)
        {
            if (text.IndexOf('.') != -1)
            {
                return text;
            }
            return text + ".0";
        }

        #region Type Extensions
        public static bool IsPrimitive(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().IsPrimitive;
#else
            return type.IsPrimitive;
#endif
        }

        public static bool IsValueType(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().IsValueType;
#else
            return type.IsValueType;
#endif
        }

        public static bool IsAbstract(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().IsAbstract;
#else
            return type.IsAbstract;
#endif
        }

        public static bool IsGenericType(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().IsGenericType;
#else
            return type.IsGenericType;
#endif
        }

        public static bool IsEnum(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().IsEnum;
#else
            return type.IsEnum;
#endif
        }
        #endregion TypeExtensions

        #region Common
        internal static bool CheckOnClientEventName(string name)
        {
            return !string.IsNullOrEmpty(name) && name.StartsWith(CLIENT_EVENT_PREFIX);
        }

#if !ASPNETCORE
        internal static string ColorToHtml(Color color)
        {
            if (color.IsEmpty) return string.Empty;
            if (color == Color.Transparent) return "Transparent";

            return color.A != 255
                ? string.Format("rgba({0},{1},{2},{3:0.###})", color.R, color.G, color.B, color.A / 255f)
                : ColorTranslator.ToHtml(color);
        }

        internal static Color ColorFromHtml(string htmlColor)
        {
            if (string.IsNullOrEmpty(htmlColor)) return Color.Empty;

            if(htmlColor.TrimStart().StartsWith("rgba(", StringComparison.OrdinalIgnoreCase))
            {
                var startIndex = htmlColor.IndexOf('(');
                var endIndex = htmlColor.IndexOf(')');
                if (endIndex == -1) return Color.Empty;

                var valuesString = htmlColor.Substring(startIndex + 1, endIndex - startIndex - 1);
                var values = valuesString.Split(',');
                var r = Convert.ToInt32(values[0]);
                var g = Convert.ToInt32(values[1]);
                var b = Convert.ToInt32(values[2]);
                var a = (int)Math.Round(Convert.ToDouble(values[3]) * 255);
                return Color.FromArgb(a, r, g, b);
            }

            return ColorTranslator.FromHtml(htmlColor);
        }
#endif
        #endregion Common
    }

    internal class PropertyComparer : IEqualityComparer<object>
    {
        public new bool Equals(object x, object y)
        {
            //Check whether the compared objects reference the same data.  
            if (Object.ReferenceEquals(x, y))
            {
                return true;
            }

            if (x == null || y == null || x.GetType() != y.GetType())
            {
                return false;
            }

#if ASPNETCORE
            if (x is PropertyInfo && y is PropertyInfo)
            {
                PropertyInfo propertyX = x as PropertyInfo;
                PropertyInfo propertyY = y as PropertyInfo;
                return (propertyX.Name == propertyY.Name &&
                    propertyX.PropertyType == propertyY.PropertyType &&
                    propertyX.DeclaringType == propertyY.DeclaringType);
            }
#else
            if (x is PropertyDescriptor && y is PropertyDescriptor)
            {
                PropertyDescriptor descriptorX = x as PropertyDescriptor;
                PropertyDescriptor descriptorY = y as PropertyDescriptor;
                return (descriptorX.Name == descriptorY.Name &&
                     descriptorX.ComponentType == descriptorY.ComponentType &&
                     descriptorX.PropertyType == descriptorY.PropertyType);
            }
#endif
            if (x is FieldInfo && y is FieldInfo)
            {
                FieldInfo fieldX = x as FieldInfo;
                FieldInfo fieldY = x as FieldInfo;
                return (fieldX.Name == fieldY.Name &&
                    fieldX.FieldType == fieldY.FieldType &&
                    fieldX.DeclaringType == fieldY.DeclaringType
                    );
            }

            return x.Equals(y);
        }

        public int GetHashCode(object obj)
        {
            return obj == null ? 0 : obj.GetHashCode();

        }
    }

    internal enum PrimitiveTypeCode
    {
        Empty,
        Object,
        Char,
        CharNullable,
        Boolean,
        BooleanNullable,
        SByte,
        SByteNullable,
        Int16,
        Int16Nullable,
        UInt16,
        UInt16Nullable,
        Int32,
        Int32Nullable,
        Byte,
        ByteNullable,
        UInt32,
        UInt32Nullable,
        Int64,
        Int64Nullable,
        UInt64,
        UInt64Nullable,
        Single,
        SingleNullable,
        Double,
        DoubleNullable,
        DateTime,
        DateTimeNullable,
#if !NET20
        DateTimeOffset,
        DateTimeOffsetNullable,
#endif
        Decimal,
        DecimalNullable,
        Guid,
        GuidNullable,
        TimeSpan,
        TimeSpanNullable,
#if !(PORTABLE || NET35 || NET20)
        BigInteger,
        BigIntegerNullable,
#endif
        Uri,
        String,
        Bytes,
        DBNull
    }
}
