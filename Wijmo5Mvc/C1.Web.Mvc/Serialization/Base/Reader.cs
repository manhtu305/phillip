﻿using System;
using System.IO;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the base class for deserializing.
    /// </summary>
    public abstract class BaseReader : Operation
    {
        private TextReader _reader;

        internal TextReader Reader
        {
            get { return _reader; }
        }

        /// <summary>
        /// Creates an instance of <see cref="BaseReader"/>.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="helper">The attribute helper.</param>
        /// <param name="reader">The text reader.</param>
        protected BaseReader(IContext context, IAttributeHelper helper, TextReader reader)
            :base(context, helper)
        {
            _reader = reader;
        }

        #region Member
        #region Converter
        internal override BaseConverter GetFirstConverter(object memberInfo, object value)
        {
            throw new NotImplementedException();
        }
        #endregion Converter
        #endregion Member
    }
}
