﻿using System;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the interface for the context information.
    /// </summary>
    public interface IContext
    {
        /// <summary>
        /// Gets the setting.
        /// </summary>
        ISetting Setting { get; }

        /// <summary>
        /// Gets the service provider.
        /// </summary>
        IServiceProvider ServiceProvider { get; }
    }

    /// <summary>
    /// Defines the class for the context.
    /// </summary>
    internal class BaseContext : IContext
    {
        #region Fields
        private ISetting _setting;
        private IServiceProvider _serviceProvider;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the setting.
        /// </summary>
        public ISetting Setting
        {
            get
            {
                return _setting ?? (_setting = new BaseSetting());
            }
        }

        /// <summary>
        /// Gets the service provider.
        /// </summary>
        public IServiceProvider ServiceProvider
        {
            get { return _serviceProvider; }
        }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Creates an instance of <see cref="IContext"/>.
        /// </summary>
        /// <param name="setting">The setting object.</param>
        /// <param name="serviceProvider">The service provider.</param>
        public BaseContext(ISetting setting = null, IServiceProvider serviceProvider = null)
        {
            _setting = setting;
            _serviceProvider = serviceProvider;
        }
        #endregion Constructors
    }
}
