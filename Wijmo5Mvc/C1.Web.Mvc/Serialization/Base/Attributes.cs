﻿using System;

namespace C1.Web.Mvc.Serialization
{
    #region Attribute Helper
    /// <summary>
    /// Defines the interface for attribute helper used to get the attribute.
    /// </summary>
    public interface IAttributeHelper
    {
        /// <summary>
        /// Gets the attribute for the converter.
        /// </summary>
        /// <param name="memberInfo">The member information.</param>
        /// <param name="value">The member value.</param>
        /// <returns>Returns an instance of <see cref="C1ConverterAttribute"/></returns>
        C1ConverterAttribute GetConverterAttribute(object memberInfo, object value);

        /// <summary>
        /// Gets the attribute for the name.
        /// </summary>
        /// <param name="memberInfo">The member information.</param>
        /// <param name="value">The member value.</param>
        /// <returns>Returns an instance of <see cref="C1NameAttribute"/>.</returns>
        C1NameAttribute GetNameAttribute(object memberInfo, object value);

        /// <summary>
        /// Gets the attribute for the collection item converter.
        /// </summary>
        /// <param name="memberInfo">The member information.</param>
        /// <param name="collection">The collection value.</param>
        /// <returns>Returns an instance of <see cref="C1CollectionItemConverterAttribute"/></returns>
        C1CollectionItemConverterAttribute GetCollectionItemConverterAttribute(object memberInfo, object collection);
    }

    internal abstract class AttributeHelper : IAttributeHelper
    {
        public abstract C1ConverterAttribute GetConverterAttribute(object memberInfo, object value);

        public abstract C1NameAttribute GetNameAttribute(object memberInfo, object value);

        public abstract C1CollectionItemConverterAttribute GetCollectionItemConverterAttribute(object memberInfo, object collection);
    }
    #endregion Attribute Helper

    /// <summary>
    /// Defines the base attribute for ignore serialization or deserialization.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class C1IgnoreAttribute: Attribute { }

    #region Name
    /// <summary>
    /// Defines the base attribute for customizing the name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct , AllowMultiple = false)]
    public abstract class C1NameAttribute: Attribute
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Creates an instance of <see cref="C1NameAttribute"/>.
        /// </summary>
        /// <param name="name">The specified name.</param>
        protected C1NameAttribute(string name)
        {
            Name = name;
        }
    }
    #endregion Name

    #region Converter
    /// <summary>
    /// Instructs the <see cref="C1ConverterAttribute"/> to use the specified <see cref="BaseConverter"/> when serializing the member or class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface | AttributeTargets.Enum | AttributeTargets.Parameter)]
    public abstract class C1ConverterAttribute : Attribute
    {
        private readonly Type _converterType;

        /// <summary>
        /// Gets the type of the converter.
        /// </summary>
        public Type ConverterType
        {
            get { return _converterType; }
        }

        /// <summary>
        /// The parameter list to use when constructing the JsonConverter described by ConverterType.  
        /// If null, the default constructor is used.
        /// </summary>
        public object[] ConverterParameters { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="C1ConverterAttribute"/> class.
        /// </summary>
        /// <param name="converterType">Type of the converter.</param>
        protected C1ConverterAttribute(Type converterType)
        {
            if (converterType == null)
                throw new ArgumentNullException("converterType");

            _converterType = converterType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="C1ConverterAttribute"/> class.
        /// </summary>
        /// <param name="converterType">Type of the converter.</param>
        /// <param name="converterParameters">Parameter list to use when constructing the JsonConverter.  Can be null.</param>
        protected C1ConverterAttribute(Type converterType, params object[] converterParameters)
            : this(converterType)
        {
            ConverterParameters = converterParameters;
        }
    }

    /// <summary>
    /// Defines the attribute to specify the collection item converter.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public abstract class C1CollectionItemConverterAttribute: C1ConverterAttribute
    {
        /// <summary>
        /// Creates an instance of <see cref="C1CollectionItemConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        protected C1CollectionItemConverterAttribute(Type converterType)
            :base(converterType)
        {
        }

        /// <summary>
        /// Creates an instance of <see cref="C1CollectionItemConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        /// <param name="converterParameters">The parameters used to create the converter instance.</param>
        protected C1CollectionItemConverterAttribute(Type converterType, params object[] converterParameters)
            :base(converterType, converterParameters)
        {
        }

    }
    #endregion Converter

}
