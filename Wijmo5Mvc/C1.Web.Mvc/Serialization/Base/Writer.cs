﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
#if MODEL
using System.Text;
#endif

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the base class for Writer.
    /// </summary>
    public abstract class BaseWriter : Operation
    {
        #region Fields
        private TextWriter _output;
        private Stack<Scope> _scopes;
        #endregion Fields

        #region Properties
        internal virtual TextWriter Output
        {
            get
            {
                return _output;
            }
        }

        internal virtual Stack<Scope> Scopes
        {
            get { return _scopes; }
        }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Creates an instance of <see cref="BaseWriter"/>
        /// </summary>
        /// <param name="context">The context object.</param>
        /// <param name="helper">The attribute helper instance.</param>
        /// <param name="writer">The writer.</param>
        protected BaseWriter(IContext context, IAttributeHelper helper, TextWriter writer)
            :base(context, helper)
        {
            Utility.ArgumentNotNull(writer, "writer");
            _output = writer;
            _scopes = new Stack<Scope>();
        }
        #endregion Constructors

        #region Methods
        #region Public
        /// <summary>
        /// Writes a text value.
        /// </summary>
        /// <param name="value">The text value.</param>
        public virtual void WriteText(string value)
        {
            WriteRawText(value, false);
        }

        /// <summary>
        /// Writes the object.
        /// </summary>
        /// <param name="value">The object to be written.</param>
        /// <param name="converter">The converter used to write.</param>
        public virtual void WriteObject(object value, BaseConverter converter = null)
        {
            WriteMemberValueWithoutScope(value, null, converter);
        }

        internal virtual void WriteRawText(string text, bool withQuotes = true)
        {
            var txt = string.Empty;
            if (withQuotes)
            {
                txt += "\"";
            }
            txt += text;
            if (withQuotes)
            {
                txt += "\"";
            }

            WritePureText(txt);
        }
        #endregion Public

        #region Member
        // Considers whether the current member should be serialized.
        internal void WriteMemberInfo(object memberInfo, object parent)
        {
            if (ShouldSerialize(memberInfo, parent))
            {
                WriteMemberInfoWithResolver(memberInfo, parent);
            }
        }

        // Considers the resolver when writing.
        internal virtual void WriteMemberInfoWithResolver(object memberInfo, object parent)
        {
            var value = Utility.GetValue(memberInfo, parent);
            var name = Utility.GetName(memberInfo);
            var type = Utility.GetType(memberInfo);
            var matchedResolver = Setting.Resolvers.FirstOrDefault(resolver => resolver.CanResolve(name, value, type, Context));
            if (matchedResolver != null)
            {
                matchedResolver.Serialize(this, memberInfo, parent, Context);
            }
            else
            {
                WriteMemberInfoWithoutResolver(name, value, memberInfo);
            }

            CheckScopes();
            var scope = Scopes.Peek();
            if (scope.Type == ScopeType.Complex)
            {
                scope.ObjectCount++;
            }
        }

        // Considers Attributes and Settings without Resolver when writing.
        internal virtual void WriteMemberInfoWithoutResolver(string memberName, object memberValue, object memberInfo)
        {
            memberName = GetName(memberInfo, memberName, memberValue);
            var cter = GetConverter(memberInfo, memberValue);
            WriteMemberInfoWithSettings(memberName, memberValue, memberInfo, cter);
        }

        // Only considers Settings when writing.
        internal virtual void WriteMemberInfoWithSettings(string memberName, object memberValue, object memberInfo, BaseConverter converter)
        {
            // Format name with Settings
            memberName = GetFormattedMemberName(memberName);
            // Consider the converters of Settings if the specified converter is null.
            var cter = converter ?? GetFirstConverter(memberInfo, memberValue);
            WriteMemberInfo(memberName, memberValue, memberInfo, cter);
        }

        internal virtual void WriteMemberInfo(string name, object value, object memberInfo, BaseConverter converter)
        {
            WriteMemberName(name, value, memberInfo);
            WriteMemberValueWithScope(value, memberInfo, converter);
        }
        #endregion Member

        #region Name
        internal abstract void WriteMemberName(string name, object value, object memberInfo);
        #endregion Name

        #region Value
        internal virtual void WriteMemberValueWithScope(object value, object memberInfo, BaseConverter converter = null, bool withoutComplexBuilderName = false)
        {
            StartScope(value, null, withoutComplexBuilderName);
            WriteMemberValueWithoutScope(value, memberInfo, converter);
            EndScope();
        }

        private void WriteMemberValueWithoutScope(object value, object memberInfo, BaseConverter converter)
        {
            if (converter != null)
            {
                WriteMemberValueWithConverter(value, memberInfo, converter);
            }
            else
            {
                WriteRawMemberValue(value, memberInfo);
            }
        }

        internal virtual void WriteMemberValueWithConverter(object value, object memberInfo, BaseConverter converter)
        {
            BaseWriter.WriteMemberValueWithConverter(this, value, memberInfo, converter, Context);
        }

        internal static void WriteMemberValueWithConverter(BaseWriter writer, object value, object memberInfo, BaseConverter converter, IContext context)
        {
            Utility.ArgumentNotNull(converter, "converter");
            converter.Serialize(writer, value, context);
        }
        #endregion Value

        #region Converter
        internal override BaseConverter GetFirstConverter(object memberInfo, object value)
        {
            return GetMatchedConverters(memberInfo, value).FirstOrDefault(cter => cter.CanWrite);
        }
        #endregion Converter

        #region ShouldSerialize
        internal virtual bool ShouldSerialize(object memberInfo, object parent)
        {
            if (HasIgnoreAttribute(memberInfo, parent))
            {
                return false;
            }

            //Invoke ShouldSerialize[Name] method if has.
            var processed = Utility.InvokeShouldSerializeMethod(memberInfo, parent);
            if (processed.HasValue)
            {
                return processed.Value;
            }

            // Check Default Value
            return ShouldSerializeDefaultValue(memberInfo, parent);
        }

        internal virtual bool HasIgnoreAttribute(object memberInfo, object parent)
        {
            // Has C1IgnoreAttribue
            var ignoreAttr = Utility.GetAttribute<C1IgnoreAttribute>(memberInfo);
            return ignoreAttr != null;

        }

        internal virtual bool ShouldSerializeDefaultValue(object memberInfo, object parent)
        {
            return !IsDefaultValue(memberInfo, parent);
        }

        private bool IsDefaultValue(object memberInfo, object parent)
        {
            DefaultValueAttribute dvAttr = Utility.GetAttribute<DefaultValueAttribute>(memberInfo);
            object objCurrent = Utility.GetValue(memberInfo, parent);
            if (dvAttr != null)
            {
                if (objCurrent == null)
                {
                    return (dvAttr.Value == null);
                }

                return Utility.ValueEquals(objCurrent, dvAttr.Value);
            }
            return IsDefaultObject(memberInfo, objCurrent);
        }

        private bool IsDefaultObject(object memberInfo, object value)
        {
            var type = Utility.GetType(memberInfo);
            if (type == null)
            {
                throw new ArgumentNullException("memberInfo");
            }
            if (value != null && type == typeof(object))
            {
                return false;
            }
            if (value == null || type.IsValueType() || type.IsPrimitive() || Utility.TypeCodeMap.ContainsKey(type))
            {
                return Utility.ValueEquals(value, Utility.GetDefaultValue(type));
            }
            if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                return Utility.IsEmptyEnumerable(value as IEnumerable);
            }
#if !ASPNETCORE
            PropertyDescriptorCollection pdCollection = TypeDescriptor.GetProperties(value);

            foreach (PropertyDescriptor pd in pdCollection)
            {
                if (ShouldSerialize(pd, value))
                {
                    return false;
                }
            }

            FieldInfo[] fis = type.GetFields();
            foreach (var fi in fis)
            {
                if (ShouldSerialize(fi, value))
                {
                    return false;
                }
            }
#else
            List<MemberInfo> mis = new List<MemberInfo>();
            mis.AddRange(value.GetType().GetProperties());
            mis.AddRange(value.GetType().GetFields());
            foreach (var mi in mis)
            {
                if (ShouldSerialize(mi, value))
                {
                    return false;
                }
            }
#endif

            return true;
        }
        #endregion ShouldSerialize

        #region Raw Values
        internal void WriteRawMemberValue(object value, object memberInfo = null)
        {
            WriteValue(value, memberInfo);
        }

        private void WriteValue(object value, object memberInfo)
        {
            switch (Utility.GetScopeType(value))
            {
                case ScopeType.IDictionary:
                    WriteDictionary(ConvertToDictionary(value), memberInfo);
                    break;
                case ScopeType.IEnumerable:
                    WriteCollection((IEnumerable)value, memberInfo);
                    break;
                case ScopeType.Simple:
                    WriteSimple(value);
                    break;
                case ScopeType.SystemDrawingColor:
#if !ASPNETCORE
                    WriteColor((System.Drawing.Color)value);
#endif
                    break;
                case ScopeType.CultureInfo:
                    WriteCultureInfo((CultureInfo)value);
                    break;
                case ScopeType.Complex:
                    WriteComplex(value, memberInfo);
                    break;
            }
            //
        }
        
        #region Simple
        internal virtual void WriteSimple(object value)
        {
            OnBeginningWrite();
            if (value == null)
            {
                WriteNull();
            }
            else if (value is DBNull)
            {
                WriteDBNull();
            }
            else if (value is Guid)
            {
                WriteSimpleValue(((Guid)value).ToString());
            }
            else if (value is bool)
            {
                WriteSimpleValue((bool)value);
            }
            else if (value is string)
            {
                WriteSimpleValue((string)value);
            }
            else if (value is char)
            {
                WriteSimpleValue((char)value);
            }
            else if (value is int)
            {
                WriteSimpleValue((int)value);
            }
            else if (value is float)
            {
                WriteSimpleValue((float)value);
            }
            else if (value is double)
            {
                WriteSimpleValue((double)value);
            }
            else if (value is Enum)
            {
                WriteSimpleValue((Enum)value);
            }
            else if (value is DateTime)
            {
                WriteSimpleValue((DateTime)value);
            }
            else if (value is decimal)
            {
                WriteSimpleValue((decimal)value);
            }
            else if (value is TimeSpan)
            {
                WriteSimpleValue((TimeSpan)value);
            }
            else
            {
                WriteOtherSimples(value);
            }

            OnWriteEnded();
        }

        internal abstract void WriteNull();
        internal abstract void WriteDBNull();
        internal abstract void WriteSimpleValue(string value);
        internal abstract void WriteSimpleValue(char value);
        internal abstract void WriteSimpleValue(int value);
        internal abstract void WriteSimpleValue(float value);
        internal abstract void WriteSimpleValue(double value);
        internal abstract void WriteSimpleValue(Enum value);
        internal abstract void WriteSimpleValue(DateTime value);
        internal abstract void WriteSimpleValue(decimal value);
        internal abstract void WriteSimpleValue(bool value);
        internal abstract void WriteSimpleValue(TimeSpan value);
        internal abstract void WriteOtherSimples(object value);
        #endregion Simple

        #region Complex
        internal virtual void WriteComplex(object value, object memberInfo, bool withoutComplexPrefix = false)
        {
            OnBeginningWrite();
            var descriptors = GetMembers(value);
            var total = descriptors.Count;
            for (var i = 0; i < total; i++)
            {
                WriteComplexSubItem(descriptors[i], value, i, total);
            }

            OnWriteEnded();
        }

        internal virtual void WriteComplexSubItem(object memberInfo, object parent, int index, int total)
        {
            WriteMemberInfo(memberInfo, parent);
        }

        internal virtual List<object> GetMembers(object value)
        {
            return Utility.GetMembers(value);
        }

        internal virtual void StartWriteComplex()
        {
        }
        internal virtual void EndWriteComplex()
        {
        }
        #endregion Complex

        #region Dictionary
        private IDictionary ConvertToDictionary(object value)
        {
            var dictionary = value as IDictionary;
            if (dictionary != null) return dictionary;

            // ExpandoObject inherits IDictionary<string, object> but not inherits IDictionary.
            // IDictionary<,> inherits IEnumerable
            var list = value as IEnumerable;
            System.Diagnostics.Debug.Assert(list != null);

            dictionary = new Dictionary<object, object>();
            foreach(dynamic item in list)
            {
                // item is KeyValuePair<,>
                dictionary.Add(item.Key, item.Value);
            }

            return dictionary;
        }

        internal virtual void WriteDictionary(IDictionary value, object collectionMemberInfo = null)
        {
            OnBeginningWrite();
            var scope = Scopes.Peek();
            foreach (DictionaryEntry item in value)
            {
                WriteDictionaryEntry(item, value, collectionMemberInfo);
                scope.ObjectCount++;
            }
            OnWriteEnded();
        }

        internal abstract void WriteDictionaryEntry(DictionaryEntry item, object parent, object parentMemberInfo);

        internal virtual void StartWriteDictionary() { }

        internal virtual void EndWriteDictionary() { }
        #endregion Dictionary

        #region Collection
        internal virtual void WriteCollection(IEnumerable value, object memberInfo)
        {
            OnBeginningWrite();
            var scope = Scopes.Peek();
            foreach (var item in value)
            {
                WriteCollectionItem(item, value, memberInfo);
                scope.ObjectCount++;
            }
            OnWriteEnded();
        }

        internal virtual void WriteCollectionItem(object item, object collection, object collectionMemberInfo)
        {
            var itemConverter = GetCollectionItemConverter(collection, item, collectionMemberInfo);
            if (itemConverter != null)
            {
                itemConverter.Serialize(this, item, Context);
            }
            else
            {
                WriteRawCollectionItem(item, collection, collectionMemberInfo);
            }
        }

        /// <summary>
        /// Write the collection item without any converter.
        /// </summary>
        internal abstract void WriteRawCollectionItem(object item, object collection, object collectionMemberInfo);

        internal virtual void StartWriteCollection()
        {
        }

        internal virtual void EndWriteCollection()
        {
        }
        #endregion Collection

#if !ASPNETCORE
        #region Color
        internal abstract void WriteColor(System.Drawing.Color value);
        #endregion Color
#endif

        #region CultureInfo
        internal abstract void WriteCultureInfo(CultureInfo value);
        #endregion CultureInfo

        #endregion Raw Values

        #region Scopes
        internal virtual void StartScope(object obj, ScopeType? scopeType = null, bool condition = false)
        {
            var st = scopeType ?? Utility.GetScopeType(obj);
            Scope parent = null;
            if(Scopes.Count > 0)
            {
                parent = Scopes.Peek();
            }

            Scopes.Push(new Scope(obj, st)
            {
                Parent = parent,
                WithoutComplexBuilderName = condition
            });
        }

        internal virtual void EndScope()
        {
            Scopes.Pop().Dispose();
        }

        internal void CheckScopes()
        {
            if(Scopes.Count == 0)
            {
                throw new InvalidOperationException();
            }
        }
        #endregion Scopes

        #region Events
        internal virtual void OnBeginningWrite()
        {
            var scope = Scopes.Peek();
            if (scope.ObjectCount == 0)
            {
                switch (scope.Type)
                {
                    case ScopeType.IEnumerable:
                        StartWriteCollection();
                        break;
                    case ScopeType.Complex:
                        StartWriteComplex();
                        break;
                    case ScopeType.IDictionary:
                        StartWriteDictionary();
                        break;
                }
            }
        }

        internal virtual void OnWriteEnded()
        {
            var scope = Scopes.Peek();
            switch (scope.Type)
            {
                case ScopeType.IEnumerable:
                    EndWriteCollection();
                    break;
                case ScopeType.Complex:
                    EndWriteComplex();
                    break;
                case ScopeType.IDictionary:
                    EndWriteDictionary();
                    break;
            }
        }
        #endregion Events

        #region Helpers
        internal virtual void WritePureText(string text)
        {
            Output.Write(text);
        }
        #endregion Helpers
        #endregion Methods
    }

    internal enum ScopeType
    {
        Simple,
        IDictionary,
        IEnumerable,
        Complex,
        SystemDrawingColor,
        CultureInfo
    }

    internal sealed class Scope: IDisposable
    {
        #region Fields
        private int _objectCount;
        private ScopeType _type;
        private object _object;
        private IList<string> _usedNames;
        #endregion Fields

        #region Properties
        public object Object
        {
            get { return _object; }
        }

        public int ObjectCount
        {
            get
            {
                return _objectCount;
            }
            set
            {
                _objectCount = value;
            }
        }

        public ScopeType Type
        {
            get
            {
                return _type;
            }
        }

        // Used for processing some special case.
        // Now it decides whether to write the complex builder name ( buildername => buildername) for HtmlHelperWriter.
        public bool WithoutComplexBuilderName { get; set; }

        public Scope Parent { get; set; }

        public IList<string> UsedNames
        {
            get { return _usedNames ?? (_usedNames = new List<string>()); }
        }

        /// <summary>
        /// Gets or sets whether to write in a new line.
        /// </summary>
        public bool NeedNewLine { get; set; }
        #endregion Properties

        #region Ctors
        public Scope(object obj, ScopeType type)
        {
            _object = obj;
            _type = type;
        }
        #endregion Ctors

        #region IDisposable
        public void Dispose()
        {
            Parent = null;
        }
        #endregion IDisposable
    }
}
