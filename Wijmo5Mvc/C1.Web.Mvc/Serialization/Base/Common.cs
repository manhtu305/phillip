﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the base class for serializing and deserializing.
    /// </summary>
    public abstract class Operation : IDisposable
    {
        #region Fields
        private readonly IContext _context;
        private readonly IAttributeHelper _helper;
        private ISetting _setting;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the context object.
        /// </summary>
        public virtual IContext Context
        {
            get
            {
                return _context;
            }
        }

        internal ISetting Setting
        {
            get
            {
                return _setting ?? (_setting = Context.Setting);
            }
        }

        internal IAttributeHelper Helper
        {
            get
            {
                return _helper;
            }
        }

        #endregion Properties

        #region Constructors
        /// <summary>
        /// Creates an instance of <see cref="Operation"/>.
        /// </summary>
        /// <param name="context">The context object.</param>
        /// <param name="helper">The attribute helper.</param>
        protected Operation(IContext context, IAttributeHelper helper)
        {
            Utility.ArgumentNotNull(helper, "helper");
            _context = context ?? new BaseContext();
            _helper = helper;
        }
        #endregion Constructors

        #region Methods
        #region Name
        // The order: NameAttribute > Member Name
        internal string GetName(object memberInfo, string memberName, object value)
        {
            var nameAttribute = Helper.GetNameAttribute(memberInfo, value);
            if (nameAttribute != null)
            {
                return nameAttribute.Name;
            }

            return memberName;
        }

        // adjust the name with Settings.
        internal virtual string GetFormattedMemberName(string memberName)
        {
            return memberName;
        }
        #endregion Name

        #region Converter
        #region Member Converter
        // The order: ConverterAttribute > Optional Converter
        internal BaseConverter GetConverter(object memberInfo, object value, BaseConverter optionalCter = null)
        {
            var converter = GetConverterFromAttribute(memberInfo, value);
            return converter ?? optionalCter;
        }

        private BaseConverter GetConverterFromAttribute(object memberInfo, object value)
        {
            var ca = Helper.GetConverterAttribute(memberInfo, value);
            return ca == null ? null
                : Utility.GetInstance<BaseConverter>(ca.ConverterType, ca.ConverterParameters);
        }

        internal abstract BaseConverter GetFirstConverter(object memberInfo, object value);

        internal IEnumerable<BaseConverter> GetMatchedConverters(object memberInfo, object value)
        {
            return Setting.Converters.Where(cter => cter.CanConvert(value != null ? value.GetType() : Utility.GetType(memberInfo), Context));
        }
        #endregion Member Converter

        #region Collection Item Converter
        // The order: CollectionItemConverter > First One of Settings' Converters
        internal BaseConverter GetCollectionItemConverter(object collection, object item, object collectionMemberInfo = null)
        {
            // Item's Converter
            var converter = GetCollectionItemConverterFromAttribute(collectionMemberInfo, collection);

            if (converter == null)
            {
                // First One of Settings' Converters
                converter = GetFirstConverter(null, item);
            }

            return converter;
        }

        private BaseConverter GetCollectionItemConverterFromAttribute(object memberInfo, object value)
        {
            var ca = Helper.GetCollectionItemConverterAttribute(memberInfo, value);
            return ca == null ? null
                : Utility.GetInstance<BaseConverter>(ca.ConverterType, ca.ConverterParameters);
        }
        #endregion Collection Item Converter
        #endregion Converter

        #region IDisposable
        /// <summary>
        /// Dispose the resources.
        /// </summary>
        public virtual void Dispose()
        {
        }
        #endregion IDisposable
        #endregion Methods
    }
}
