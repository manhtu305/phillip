﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the interface for setting.
    /// </summary>
    public interface ISetting
    {
        /// <summary>
        /// Gets the converter collection.
        /// </summary>
        IList<BaseConverter> Converters { get; }

        /// <summary>
        /// Gets the resolver collection.
        /// </summary>
        IList<BaseResolver> Resolvers { get; }
    }

    /// <summary>
    /// Defines the class for settings.
    /// </summary>
    public class BaseSetting : ISetting
    {
        private IList<BaseConverter> _converters;
        private IList<BaseResolver> _resolvers;

        /// <summary>
        /// Gets the converter collection.
        /// </summary>
        public IList<BaseConverter> Converters
        {
            get
            {
                return _converters ?? (_converters = new List<BaseConverter>());
            }
        }

        /// <summary>
        /// Gets the resolver collection.
        /// </summary>
        public IList<BaseResolver> Resolvers
        {
            get
            {
                return _resolvers ?? (_resolvers = new List<BaseResolver>());
            }
        }
    }
}
