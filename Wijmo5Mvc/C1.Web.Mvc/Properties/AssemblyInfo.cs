using System.Reflection;
using System.Runtime.InteropServices;
using C1.Util.Licensing;
using System.Runtime.CompilerServices;

// licensing support
// Studio Ultimate
[assembly: C1ProductInfo("SU", "757CCC59-F365-4325-A676-0674C656B7A2")]
// Studio Enterprise
[assembly: C1ProductInfo("SE", "724e8a91-af12-4a3b-9aeb-ef89612e692e")]
// Studio for ASP.NET
[assembly: C1ProductInfo("S9", "08f7d405-7096-4b5f-a288-f749b8c83e6a")]
// Wijmo5 Mvc
[assembly: C1ProductInfo("AH", "01667B7D-83D9-47D9-A4E0-E54FB46CA797")]

#if !ASPNETCORE
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

#if GRAPECITY
[assembly: AssemblyTitle("ComponentOne Controls for ASP.NET MVC Edition JPN")]
[assembly: AssemblyDescription("ComponentOne Controls for ASP.NET MVC Edition JPN")]
[assembly: AssemblyProduct("ComponentOne Controls for ASP.NET MVC Edition JPN")]
#else
[assembly: AssemblyTitle("ComponentOne Controls for ASP.NET MVC Edition")]
[assembly: AssemblyDescription("ComponentOne Controls for ASP.NET MVC Edition")]
[assembly: AssemblyProduct("ComponentOne Controls for ASP.NET MVC Edition")]
#endif

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GrapeCity, Inc.")]
[assembly: AssemblyCopyright("Copyright © GrapeCity, Inc.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2d364adc-4204-417d-b41f-83c510ce3757")]
#endif

[assembly: AssemblyVersion(AssemblyInfo.Version)]
[assembly: AssemblyFileVersion(AssemblyInfo.Version)]
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".Finance, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".FlexSheet, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".FlexViewer, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".Olap, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".MultiRow, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".TransposedGrid, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]

#if ASPNETCORE || EXTENSION
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".Extension, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#endif
#if UNITTEST
[assembly: InternalsVisibleTo("C1.Web.Mvc.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#endif


[SmartAssembly.Attributes.DoNotObfuscateType]
internal static class AssemblyInfo
{
    public const string Version =
#if ASPNETCORE
#if NETCORE3
        "3.0"
#else
        "1.0"
#endif
#else
#if NET45
        "4.5"
#else
        "4.0"
#endif
#endif
        + ".20202.55555";

    public const string NamePrefix =
#if ASPNETCORE
        "C1.AspNetCore.Mvc";
#else
        "C1.Web.Mvc";
#endif
}
