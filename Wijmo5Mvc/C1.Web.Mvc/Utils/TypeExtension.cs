﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace System.Reflection
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal static class TypeExtension
    {
        [SmartAssembly.Attributes.DoNotObfuscate]
        public static Assembly Assembly(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().Assembly;
#else
            return type.Assembly;
#endif
        }

        public static Type BaseType(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().BaseType;
#else
            return type.BaseType;
#endif
        }

#if NETCORE && !NETCORE3
        public static object[] GetCustomAttributes(this Type type, Type attrType, bool inherit)
        {
            var attrs = type.GetTypeInfo().GetCustomAttributes(attrType, inherit);
            return attrs.ToArray();
        }
#if !NETSTANDARD
        public static object GetCustomAttribute(this Type type, Type attrType, bool inherit)
        {
            return type.GetTypeInfo().GetCustomAttribute(attrType, inherit);
        }

        public static IEnumerable<Attribute> GetCustomAttributes(this Type type, bool inherit)
        {
            return type.GetTypeInfo().GetCustomAttributes(inherit);
        }
#endif

        public static IEnumerable<Attribute> GetCustomAttributes(this Assembly assembly, bool inherit)
        {
            return assembly.GetCustomAttributes();
        }

        public static IEnumerable<Attribute> GetCustomAttributes(this Assembly assembly, Type attrType, bool inherit)
        {
            return assembly.GetCustomAttributes(attrType);
        }
#endif

        public static INestedPropertyInfo GetNestedProperty(this Type type, string name)
        {
            if (name == null) return null;

            if (type == typeof(ExpandoObject))
            {
                return new ExpandoObjectPropertyInfo(name);
            }

            var simpleNmes = name.Split('.');
            INestedPropertyInfo property = null;
            Type currentType = type;
            foreach(var simpleName in simpleNmes)
            {
                var currentProperty = currentType.GetProperty(simpleName);
                if(currentProperty == null)
                {
                    return null;
                }

                currentType = currentProperty.PropertyType;
                property = new NestedPropertyInfo(property, currentProperty);
            }

            return property;
        }
    }

    internal interface INestedPropertyInfo
    {
        object GetValue(object obj);
    }

    internal class NestedPropertyInfo : INestedPropertyInfo
    {
        private readonly INestedPropertyInfo _parentInfo;
        private readonly PropertyInfo _currentInfo;

        public NestedPropertyInfo(INestedPropertyInfo parent, PropertyInfo current)
        {
            _parentInfo = parent;
            _currentInfo = current;
        }

        public INestedPropertyInfo Parent
        {
            get { return _parentInfo; }
        }

        public PropertyInfo Current
        {
            get { return _currentInfo; }
        }

        public object GetValue(object obj)
        {
            var currentObject = _parentInfo == null ? obj : _parentInfo.GetValue(obj);
            if (currentObject == null) return null;

            return _currentInfo.GetValue(currentObject, null);
        }
    }

    internal class ExpandoObjectPropertyInfo : INestedPropertyInfo
    {
        public ExpandoObjectPropertyInfo(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }

        public object GetValue(object obj)
        {
            //todo, get the nested value
            object value = null;
            var dic = obj as IDictionary<string, object>;
            if (dic != null)
            {
                dic.TryGetValue(Name, out value);
            }

            return value;
        }
    }


#if !ASPNETCORE
    // The following code is from Microsoft.Extensions.Internal.ClosedGenericMatcher, Microsoft.AspNetCore.Mvc.Abstractions, Version=1.0.0.0
    internal static class ClosedGenericMatcher
    {
        public static Type ExtractGenericInterface(Type queryType, Type interfaceType)
        {
            if (queryType == null)
            {
                throw new ArgumentNullException("queryType");
            }
            if (interfaceType == null)
            {
                throw new ArgumentNullException("interfaceType");
            }
            if (IsGenericInstantiation(queryType, interfaceType))
            {
                return queryType;
            }
            return GetGenericInstantiation(queryType, interfaceType);
        }

        private static Type GetGenericInstantiation(Type queryType, Type interfaceType)
        {
            Type type = null;
            foreach (Type t in queryType.GetInterfaces())
            {
                if (IsGenericInstantiation(t, interfaceType))
                {
                    if (type == null)
                    {
                        type = t;
                    }
                    else if (StringComparer.Ordinal.Compare(t.FullName, type.FullName) < 0)
                    {
                        type = t;
                    }
                }
            }
            if (type != null)
            {
                return type;
            }
            Type baseType = (queryType != null) ? queryType.BaseType : null;
            if (baseType == null)
            {
                return null;
            }
            return GetGenericInstantiation(baseType, interfaceType);
        }

        private static bool IsGenericInstantiation(Type candidate, Type interfaceType)
        {
            return (candidate.IsGenericType && (candidate.GetGenericTypeDefinition() == interfaceType));
        }
    }
#endif

}