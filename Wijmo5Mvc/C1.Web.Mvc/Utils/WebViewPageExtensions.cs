﻿using C1.Web.Mvc;
#if ASPNETCORE
using WebViewPage = Microsoft.AspNetCore.Mvc.Razor.IRazorPage;

namespace Microsoft.AspNetCore.Mvc
#else
namespace System.Web.Mvc
#endif
{
    /// <summary>
    /// Extends the <see cref="WebViewPage"/> for C1 MVC.
    /// </summary>
#if ASPNETCORE
    public static class RazorPageExtensions
#else
    public static class WebViewPageExtensions
#endif
    {
        /// <summary>
        /// Enable or disable deferred scripts feature in current view page.
        /// </summary>
        /// <param name="viewPage">The view page.</param>
        /// <param name="enable">The value.</param>
        public static void C1EnableDeferredScripts(this WebViewPage viewPage, bool enable = true)
        {
            var httpContext = viewPage.ViewContext.HttpContext;
            httpContext.SetEnableDeferredScripts(enable);
        }
    }
}
