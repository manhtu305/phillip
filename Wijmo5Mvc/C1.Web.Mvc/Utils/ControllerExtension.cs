﻿using C1.Web.Mvc;

#if ASPNETCORE
namespace Microsoft.AspNetCore.Mvc
#else
namespace System.Web.Mvc
#endif
{
    /// <summary>
    /// Extends the <see cref="Controller"/> for C1 MVC.
    /// </summary>
    public static class ControllerExtension
    {
        /// <summary>
        /// Enable or disable deferred scripts feature in current controller or current action.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="enable">The value.</param>
        public static void C1EnableDeferredScripts(this Controller controller, bool enable = true)
        {
            var httpContext = controller.ControllerContext.HttpContext;
            httpContext.SetEnableDeferredScripts(enable);
        }
    }
}
