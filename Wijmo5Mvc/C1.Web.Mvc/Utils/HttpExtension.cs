﻿using System;
using System.Collections.Generic;
#if ASPNETCORE
using System.IO;
using Writer = System.IO.TextWriter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
#else
using HttpContext = System.Web.HttpContextBase;
using Writer = System.Web.UI.HtmlTextWriter;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    internal static class HttpExtension
    {
        public static T GetContextItem<T>(this HttpContext context, object key) where T : class
        {
            return context.Items[key] as T;
        }

        public static object GetContextItem(this HttpContext context, object key)
        {
            return context.Items[key];
        }

        public static void SetContextItem(this HttpContext context, object key, object value)
        {
            context.Items[key] = value;
        }

        public static void RemoveContextItem(this HttpContext context, object key)
        {
            context.Items.Remove(key);
        }

        public static bool ContainsContextItem(this HttpContext context, object key)
        {
            return context.Items.
#if ASPNETCORE
                ContainsKey(key);
#else
                Contains(key);
#endif
        }

        public static void RenderBeginTag(this Writer tw, string tag, Action<Writer> addAttrs)
        {
#if ASPNETCORE
            tw.Write("<{0}", tag);
            if(addAttrs != null)
            {
                addAttrs(tw);
            }
            tw.Write(">\r\n");
#else
            if (addAttrs != null)
            {
                addAttrs(tw);
            }

            tw.RenderBeginTag(tag);
#endif
        }

        public static void RenderBeginTag(this Writer tw, string tag, IDictionary<string, string> attrs)
        {
            Action<Writer> addAttrs = null;
            if (attrs != null)
            {
                addAttrs = w =>
                {
                    foreach (var attr in attrs)
                    {
                        w.AddAttribute(attr.Key, attr.Value);
                    }
                };
            }

            tw.RenderBeginTag(tag, addAttrs);
        }

        public static void RenderBeginTag(this Writer tw, string tag, string key, string value)
        {
            var attrs = new Dictionary<string, string>();
            attrs.Add(key, value);
            tw.RenderBeginTag(tag, attrs);
        }

        public static void RenderEndTag(this Writer tw, string tag)
        {
#if ASPNETCORE
            tw.WriteLine("</{0}>", tag);
#else
            tw.RenderEndTag();
#endif
        }

#if ASPNETCORE

        public static T GetService<T>(this IServiceProvider sp)
        {
            if (sp == null)
            {
                return default(T);
            }

            return (T)sp.GetService(typeof(T));
        }

        public static string MapPath(this IHostingEnvironment host, string path)
        {
            path = path.Replace('/', Path.DirectorySeparatorChar).Replace('\\', Path.DirectorySeparatorChar);

            if (path.StartsWith("~"))
            {
                path = path.Substring(1);
            }

            if (path.StartsWith(Path.DirectorySeparatorChar.ToString()))
            {
                path = path.Substring(1);
            }

            return Path.Combine(host.WebRootPath, path);
        }

        public static void AddAttribute(this Writer tw, string name, string value)
        {
            tw.Write(" {0}=\"{1}\"", name, value);
        }

        public static void RenderBeginTag(this Writer tw, string tag)
        {
            tw.RenderBeginTag(tag, (Action<Writer>)null);
        }

        public static void Write(this HttpResponse response, string s)
        {
            using(var writer = new StreamWriter(response.Body))
            {
                writer.Write(s);
            }
        }

        public static void Clear(this HttpResponse response)
        {
            response.Body.Dispose();
            response.Body = new MemoryStream();
        }

        public static void End(this HttpResponse response)
        {
            response.HttpContext.Abort();
        }
#else

        public static string GetTagName(HtmlTextWriterTag tag)
        {
            return InnerHtmlTextWriter.GetTagString(tag);
        }

        private static readonly DumpHtmlTextWriter InnerHtmlTextWriter = new DumpHtmlTextWriter();

        private class DumpHtmlTextWriter : Writer {

            public DumpHtmlTextWriter() : base(null) { }

            public string GetTagString(HtmlTextWriterTag tag) {
                return GetTagName(tag);
            }
        }

#endif

    }
}