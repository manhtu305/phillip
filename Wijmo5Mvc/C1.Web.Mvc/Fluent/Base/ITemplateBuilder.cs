﻿namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the interface of the builder which can be used as a template.
    /// </summary>
    /// <typeparam name="T">The builder type.</typeparam>
    public interface ITemplateBuilder<T> where T: ITemplateBuilder<T>
    {
        /// <summary>
        /// Transfers current builder to template mode.
        /// </summary>
        /// <returns>Current builder.</returns>
        T ToTemplate();

        /// <summary>
        /// When the builder works in template mode, bind the property which name is specified 
        /// to some item which name is specified.
        /// </summary>
        /// <param name="propertyName">The specified property name.</param>
        /// <param name="boundName">The specified item name in DataContext.</param>
        /// <returns>Current builder.</returns>
        /// <remarks>
        /// It only works in template mode.
        /// </remarks>
        T TemplateBind(string propertyName, string boundName);
    }
}
