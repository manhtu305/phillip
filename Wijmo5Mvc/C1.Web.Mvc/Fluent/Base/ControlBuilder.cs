﻿using System.Collections.Generic;
#if ASPNETCORE
using Microsoft.AspNetCore.Mvc.ViewFeatures;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.Fluent
{
    abstract partial class ControlBuilder<TControl, TBuilder>
    {
        #region Ctor
        /// <summary>
        /// Creates one <see cref="ControlBuilder{TControl, TBuilder}"/> instance to configurate <paramref name="control"/>.
        /// </summary>
        /// <param name="control">The <see cref="Control"/> object to be configurated.</param>
        protected ControlBuilder(TControl control)
            : base(control)
        {
        }
        #endregion Ctor

        #region Methods

        #region Public APIs
        #region HtmlAttributes
        /// <summary>
        /// Configurates <see cref="Control.HtmlAttributes" />.
        /// Sets the HTML attributes.
        /// </summary>
        /// <param name="key">The key of the HTML attribute.</param>
        /// <param name="value">The value of the HTML attribute.</param>
        /// <returns>Current builder.</returns>
        public TBuilder HtmlAttribute(string key, string value)
        {
            Object.HtmlAttributes[key] = value;
            return this as TBuilder;
        }
        /// <summary>
        /// Configurates <see cref="Control.HtmlAttributes" />.
        /// Sets the HTML attributes.
        /// </summary>
        /// <param name="attributes">The HTML attributes.</param>
        /// <returns>Current builder.</returns>
        public TBuilder HtmlAttributes(object attributes)
        {
            return HtmlAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(attributes));
        }
        /// <summary>
        /// Configurates <see cref="Control.HtmlAttributes" />.
        /// Sets the HTML attributes.
        /// </summary>
        /// <param name="attributes">The HTML attributes.</param>
        /// <returns>Current builder.</returns>
        public TBuilder HtmlAttributes(IDictionary<string, object> attributes)
        {
            foreach (var pair in attributes)
            {
                Object.HtmlAttributes[pair.Key] = pair.Value.ToString();
            }
            return this as TBuilder;
        }
        #endregion HtmlAttributes

        #region CssStyles
        /// <summary>
        /// Configurates <see cref="Control.CssStyles" />.
        /// Sets the CSS style applied in the control.
        /// </summary>
        /// <param name="key">The key of the CSS property</param>
        /// <param name="value">The value of the CSS property</param>
        /// <returns>Current builder</returns>
        public TBuilder CssStyle(string key, string value)
        {
            Object.CssStyles[key] = value;
            return this as TBuilder;
        }
        /// <summary>
        /// Configurates <see cref="Control.CssStyles" />.
        /// Sets the CSS style applied in the control.
        /// </summary>
        /// <param name="styles">The CSS styles</param>
        /// <returns>Current builder</returns>
        public TBuilder CssStyles(IDictionary<string, string> styles)
        {
            foreach (var pair in styles)
            {
                Object.CssStyles[pair.Key] = pair.Value;
            }
            return this as TBuilder;
        }
        #endregion CssStyles

        /// <summary>
        /// Configurates the <see cref="Control.TemplateBindings" /> client event.
        /// Sets the collection of the template bindings.
        /// </summary>
        /// <remarks>
        /// When the control works in template mode, bind the property which name is specified 
        /// to some item which name is specified.
        /// </remarks>
        /// <param name="propertyName">The specified property name.</param>
        /// <param name="boundName">The specified item name in DataContext.</param>
        /// <returns>Current builder.</returns>
        public TBuilder TemplateBind(string propertyName, string boundName)
        {
            Object.TemplateBindings.Add(propertyName, boundName);
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the width of the control in pixel.
        /// </summary>
        /// <param name="width">The value.</param>
        /// <returns>Current builder.</returns>
        public TBuilder Width(int width)
        {
            return Width(width + "px");
        }

        /// <summary>
        /// Sets the height of the control in pixel.
        /// </summary>
        /// <param name="height">The value.</param>
        /// <returns>Current builder.</returns>
        public TBuilder Height(int height)
        {
            return Height(height + "px");
        }

        /// <summary>
        /// Transfer to the template mode.
        /// </summary>
        /// <returns>Current builder.</returns>
        public TBuilder ToTemplate()
        {
            Object.IsTemplate = true;
            return this as TBuilder;
        }

        /// <summary>
        /// Configurates <see cref="Component.Id"/>.
        /// Sets the id of the control.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Id(string id)
        {
            Object.Id = id;
            return this as TBuilder;
        }
        #endregion Public APIs
        #endregion Methods
    }
}