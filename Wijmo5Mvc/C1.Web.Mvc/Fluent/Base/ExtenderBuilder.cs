﻿using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    abstract partial class ExtenderBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Configurates <see cref="Component.Id"/>
        /// Sets the id of the extender.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TBuilder Id(string value)
        {
            Object.Id = value;
            return (TBuilder)this;
        }
    }
}
