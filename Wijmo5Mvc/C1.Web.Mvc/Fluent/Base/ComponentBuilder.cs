﻿#if ASPNETCORE
using System.IO;
using System.Text.Encodings.Web;
using IHtmlString = Microsoft.AspNetCore.Html.IHtmlContent;
#else
using System.Web;
#endif

namespace C1.Web.Mvc.Fluent
{
    abstract partial class ComponentBuilder<TControl, TBuilder> : IHtmlString
    {
        #region Ctor
        /// <summary>
        /// Creates one <see cref="ComponentBuilder{TControl, TBuilder}"/> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="Component"/> object to be configurated.</param>
        protected ComponentBuilder(TControl component)
            : base(component)
        {
        }
        #endregion Ctor

        #region Methods
#if ASPNETCORE
        /// <summary>
        /// Writer the process result of current instance to the writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="encoder">The Html encoder.</param>
        public virtual void WriteTo(TextWriter writer, HtmlEncoder encoder)
        {
            Object.WriteTo(writer, encoder);
        }
#else
        #region Implement the IHtmlString interface

        /// <summary>
        /// Get an Html string.
        /// </summary>
        /// <returns>An html string.</returns>
        public virtual string ToHtmlString()
        {
            return Object != null ? Object.ToHtmlString() : string.Empty;
        }

        #endregion Implement the IHtmlString interface
#endif
        #endregion Methods
    }
}
