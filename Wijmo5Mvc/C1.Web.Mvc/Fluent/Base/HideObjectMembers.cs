﻿using System;
using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines a class to remove the members from <see cref="System.Object"/> class.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class HideObjectMembers
    {
        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>A bool value.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// Overrides to remove the method.
        /// </summary>
        /// <returns>An int value.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <returns>The text.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ToString()
        {
            return base.ToString();
        }

        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <returns>The type.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new Type GetType()
        {
            return base.GetType();
        }
    }
}
