﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// The base builder for building object.
    /// </summary>
    /// <typeparam name="TObject">The object type</typeparam>
    /// <typeparam name="TBuilder">The builder type</typeparam>
    public abstract class BaseBuilder<TObject, TBuilder> : HideObjectMembers where TObject : class
    {
        /// <summary>
        /// Initializes a new instance of the BaseBuilder class.
        /// </summary>
        /// <param name="obj">The object.</param>
        protected BaseBuilder(TObject obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            Object = obj;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal TObject Object 
        {
            [SmartAssembly.Attributes.DoNotObfuscate]
            get; 
            private set; 
        }
    }
}