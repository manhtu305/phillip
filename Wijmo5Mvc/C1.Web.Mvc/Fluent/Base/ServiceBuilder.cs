﻿namespace C1.Web.Mvc.Fluent
{
    abstract partial class ServiceBuilder<TControl, TBuilder> : ITemplateBuilder<TBuilder>
    {
        /// <summary>
        /// Creates one <see cref="ServiceBuilder{TControl, TBuilder}"/> instance to configurate <paramref name="service"/>.
        /// </summary>
        /// <param name="service">The <see cref="Service"/> object to be configurated.</param>
        protected ServiceBuilder(TControl service)
            : base(service)
        {
        }

        /// <summary>
        /// Configurates <see cref="Component.Id"/>.
        /// Sets the id of the service.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Id(string id)
        {
            Object.Id = id;
            return this as TBuilder;
        }

        #region ITemplateBuilder
        /// <summary>
        /// Transfers current builder to template mode.
        /// </summary>
        /// <returns>Current builder.</returns>
        public virtual TBuilder ToTemplate()
        {
            Object.IsTemplate = true;
            return this as TBuilder;
        }

        /// <summary>
        /// When the builder works in template mode, bind the property which name is specified 
        /// to some item which name is specified.
        /// </summary>
        /// <param name="propertyName">The specified property name.</param>
        /// <param name="boundName">The specified item name in DataContext.</param>
        /// <returns>Current builder.</returns>
        /// <remarks>
        /// It only works in template mode.
        /// </remarks>
        public virtual TBuilder TemplateBind(string propertyName, string boundName)
        {
            Object.TemplateBindings.Add(propertyName, boundName);
            return this as TBuilder;
        }
        #endregion ITemplateBuilder
    }
}
