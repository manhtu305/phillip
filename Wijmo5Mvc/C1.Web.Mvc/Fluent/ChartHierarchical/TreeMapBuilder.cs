﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc.Chart;
using System.ComponentModel;
#if ASPNETCORE
using Color = System.String;
#else
using System.Drawing;
#endif

namespace C1.Web.Mvc.Fluent
{
    public partial class TreeMapBuilder<T>
    {
        /// <summary>
        /// Configurates <see cref="TreeMap{T}.ChildItemsPath" />.
        /// Sets the name of the property (or properties) used to generate child items in hierarchical data.
        /// </summary>
        /// <remarks>
        /// Set this property to an array containing the names of the properties that contain child items at each level, when the items are child items at different levels with different names (e.g. <code>[ 'Accounts', 'Checks', 'Earnings' ]</code>).
        /// </remarks>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TreeMapBuilder<T> ChildItemsPath(params string[] value)
        {
            Object.ChildItemsPath = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="TreeMap{T}.BindingName" />.
        /// Sets the name of the property containing name of the data item. It is used to show name of the node.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TreeMapBuilder<T> BindingName(params string[] value)
        {
            Object.BindingName = value;
            return this;
        }

        /// <summary>
        /// Configure <see cref="TreeMap{T}.Palette"/>.
        /// Sets an array of default colors to be used in a tree map.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public TreeMapBuilder<T> Palette(Action<ListItemFactory<TreeMapItemStyle, TreeMapItemStyleBuilder>> builder)
        {
            var palette = new List<TreeMapItemStyle>();
            builder(new ListItemFactory<TreeMapItemStyle, TreeMapItemStyleBuilder>(palette,
                () => new TreeMapItemStyle(), c => new TreeMapItemStyleBuilder(c)));
            if(Object.Palette.Any(item =>!(item is TreeMapItemStyle)))
            {
                Object.Palette.Clear();
            }
            palette.ForEach(item => Object.Palette.Add(item));
            return this;
        }

        /// <summary>
        /// Configure <see cref="TreeMap{T}.Palette"/>.
        /// Sets an array of default colors to be used in a tree map.
        /// </summary>
        /// <remarks>
        /// The array contains strings that represent CSS colors.
        /// </remarks>
        /// <param name="colors">The color collection</param>
        /// <returns>Current builder</returns>
        public override TreeMapBuilder<T> Palette(IEnumerable<Color> colors)
        {
            if (Object.Palette.Any(item => !(item is Color)))
            {
                Object.Palette.Clear();
            }

            foreach (var item in colors)
            {
                object objColor = item;
                Object.Palette.Add(objColor);
            }
            return this;
        }

        /// <summary>
        /// Overrides to remove this configuration.
        /// Configurates the <see cref="FlexChartBase{T}.OnClientSelectionChanged" /> client event.
        /// Occurs after the selection changes, whether programmatically or when the user clicks the chart.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override TreeMapBuilder<T> OnClientSelectionChanged(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to remove this configuration.
        /// Configurates <see cref="FlexChartBase{T}.ItemFormatter" />.
        /// Sets the name of the item formatter function that allows you to customize the appearance of data points.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override TreeMapBuilder<T> ItemFormatter(string value)
        {
            return base.ItemFormatter(value);
        }
    }
}
