﻿#if ASPNETCORE
using Color = System.String;
#else
using System.Drawing;
#endif

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines a builder to configurate <see cref="TreeMapItemStyle" />.
    /// </summary>
    public sealed class TreeMapItemStyleBuilder 
        : BaseBuilder<TreeMapItemStyle, TreeMapItemStyleBuilder>
    {
        /// <summary>
        /// Creates one <see cref="TreeMapItemStyle" /> instance to configurate <paramref name="obj"/>.
        /// </summary>
        /// <param name="obj">The <see cref="TreeMapItemStyle" /> object to be configurated.</param>
        public TreeMapItemStyleBuilder(TreeMapItemStyle obj) 
            : base(obj)
        {
        }

        /// <summary>
        /// Configurates <see cref="TreeMapItemStyle.TitleColor" />.
        /// Sets the title color.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TreeMapItemStyleBuilder TitleColor(Color value)
        {
            Object.TitleColor = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="TreeMapItemStyle.MaxColor" />.
        /// Sets the color of the max value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TreeMapItemStyleBuilder MaxColor(Color value)
        {
            Object.MaxColor = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="TreeMapItemStyle.MinColor" />.
        /// Sets the color of the min value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TreeMapItemStyleBuilder MinColor(Color value)
        {
            Object.MinColor = value;
            return this;
        }
    }
}
