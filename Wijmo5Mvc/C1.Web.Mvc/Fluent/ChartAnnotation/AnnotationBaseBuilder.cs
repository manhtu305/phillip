﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    public partial class AnnotationBaseBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Sets the Style property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the style of the annotation.
        /// </remarks>
        /// <param name="style">The SVGStyle instance</param>
        /// <returns>Current builder</returns>
        public TBuilder Style(SVGStyle style)
        {
            Object.Style = style;
            return this as TBuilder;
        }
    }
}
