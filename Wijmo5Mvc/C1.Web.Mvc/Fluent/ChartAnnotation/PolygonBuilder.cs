﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    public partial class PolygonBuilder
    {
        /// <summary>
        /// Add point for Polygon annotation.
        /// </summary>
        ///  <param name="point">The DataPoint instance</param>
        /// <returns>Current builder</returns>
        public PolygonBuilder AddPoint(DataPoint point)
        {
            Object.Points.Add(point);
            return this;
        }

        /// <summary>
        /// Add point for Polygon annotation.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PolygonBuilder AddPoint(Action<DataPointBuilder> build)
        {
            DataPoint point = new DataPoint();
            build(new DataPointBuilder(point));
            Object.Points.Add(point);
            return this;
        }

        /// <summary>
        /// Add point for Polygon annotation.
        /// </summary>
        /// <param name="x">The X coordinate value of this DataPoint.</param>
        /// <param name="y">The Y coordinate value of this DataPoint.</param>
        /// <returns>Current builder</returns>
        public PolygonBuilder AddPoint(object x, object y)
        {
            Object.Points.Add(new DataPoint(x, y));
            return this;
        }

        /// <summary>
        /// Add point array for Polygon annotation.
        /// </summary>
        ///  <param name="points">The DataPoint array</param>
        /// <returns>Current builder</returns>
        public PolygonBuilder AddPoints(params DataPoint[] points)
        {
            Object.Points.AddRange(points.ToList());
            return this;
        }

        /// <summary>
        /// Add point array for Polygon annotation.
        /// </summary>
        ///  <param name="points">The DataPoint list</param>
        /// <returns>Current builder</returns>
        public PolygonBuilder AddPoints(IEnumerable<DataPoint> points)
        {
            Object.Points.AddRange(points.ToList());
            return this;
        }
    }
}
