﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    public partial class AnnotationLayerBuilder<T>
    {
        #region Ctor

        /// <summary>
        /// Create one FlexGridFilterBuilder instance.
        /// </summary>
        /// <param name="extender">The FlexGridFilter extender.</param>
        public AnnotationLayerBuilder(AnnotationLayer<T> extender)
            : base(extender)
        {
        }

        #endregion Ctor

        /// <summary>
        /// Add text annotation for annotationlayer.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public AnnotationLayerBuilder<T> AddText(Action<TextBuilder> build = null)
        {
            Text text = new Text();
            if (build != null)
            {
                build(new TextBuilder(text));
            }
            Object.Items.Add(text);
            return this;
        }

        /// <summary>
        /// Add circle annotation for annotationlayer.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public AnnotationLayerBuilder<T> AddCircle(Action<CircleBuilder> build = null)
        {
            Circle circle = new Circle();
            if (build != null)
            {
                build(new CircleBuilder(circle));
            }
            Object.Items.Add(circle);
            return this;
        }

        /// <summary>
        /// Add ellipse annotation for annotationlayer.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public AnnotationLayerBuilder<T> AddEllipse(Action<EllipseBuilder> build = null)
        {
            Ellipse ellipse = new Ellipse();
            if (build != null)
            {
                build(new EllipseBuilder(ellipse));
            }
            Object.Items.Add(ellipse);
            return this;
        }

        /// <summary>
        /// Add rectangle annotation for annotationlayer.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public AnnotationLayerBuilder<T> AddRectangle(Action<RectangleBuilder> build = null)
        {
            Rectangle rectangle = new Rectangle();
            if (build != null)
            {
                build(new RectangleBuilder(rectangle));
            }
            Object.Items.Add(rectangle);
            return this;
        }

        /// <summary>
        /// Add image annotation for annotationlayer.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public AnnotationLayerBuilder<T> AddImage(Action<ImageBuilder> build = null)
        {
            Image image = new Image();
            if (build != null)
            {
                build(new ImageBuilder(image));
            }
            Object.Items.Add(image);
            return this;
        }

        /// <summary>
        /// Add line annotation for annotationlayer.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public AnnotationLayerBuilder<T> AddLine(Action<LineBuilder> build = null)
        {
            Line line = new Line();
            if (build != null)
            {
                build(new LineBuilder(line));
            }
            Object.Items.Add(line);
            return this;
        }

        /// <summary>
        /// Add polygon annotation for annotationlayer.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public AnnotationLayerBuilder<T> AddPolygon(Action<PolygonBuilder> build = null)
        {
            Polygon polygon = new Polygon();
            if (build != null)
            {
                build(new PolygonBuilder(polygon));
            }
            Object.Items.Add(polygon);
            return this;
        }

        /// <summary>
        /// Add square annotation for annotationlayer.
        /// </summary>
        ///  <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public AnnotationLayerBuilder<T> AddSquare(Action<SquareBuilder> build = null)
        {
            Square square = new Square();
            if (build != null)
            {
                build(new SquareBuilder(square));
            }
            Object.Items.Add(square);
            return this;
        }

    }
}
