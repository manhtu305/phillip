﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use AnnotationLayer extender.
    /// </summary>
    public static class AnnotationLayerExtension
    {
        /// <summary>
        /// Apply the AnnotationLayer extender in FlexChart.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="flexChartBuilder">the specified flexchart builder.</param>
        /// <param name="annotationLayerBuilder">the specified annotationlayer builder</param>
        /// <returns></returns>
        public static FlexChartBuilder<T> AddAnnotationLayer<T>(this FlexChartBuilder<T> flexChartBuilder, Action<AnnotationLayerBuilder<T>> annotationLayerBuilder)
        {
            FlexChart<T> flexChart = flexChartBuilder.Object;
            AnnotationLayer<T> annotationLayer = new AnnotationLayer<T>(flexChart);
            annotationLayerBuilder(new AnnotationLayerBuilder<T>(annotationLayer));
            flexChart.Extenders.Add(annotationLayer);
            return flexChartBuilder;
        }
    }
}
