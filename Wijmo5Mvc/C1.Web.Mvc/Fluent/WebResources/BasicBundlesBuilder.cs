﻿using C1.Web.Mvc.WebResources;
using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the builder of the script bundles of the basic assembly.
    /// </summary>
    public class BasicBundlesBuilder : BundlesBuilder
    {
        internal BasicBundlesBuilder(Scripts scripts) : base(scripts)
        {
        }

        /// <summary>
        /// Registers chart related script bundles.
        /// </summary>
        /// <param name="build">The <see cref="ChartExtensionBundlesBuilder"/>.</param>
        /// <returns>The current builder.</returns>
        public BasicBundlesBuilder Chart(Action<ChartExtensionBundlesBuilder> build = null)
        {
            Scripts.OwnerTypes.Add(typeof(Definitions.Chart));
            if (build != null)
            {
                build(new ChartExtensionBundlesBuilder(Scripts));
            }

            return this;
        }

        /// <summary>
        /// Registers grid related script bundles.
        /// </summary>
        /// <param name="build">The <see cref="GridExtensionBundlesBuilder"/>.</param>
        /// <returns>The current builder.</returns>
        public BasicBundlesBuilder Grid(Action<GridExtensionBundlesBuilder> build = null)
        {
            Scripts.OwnerTypes.Add(typeof(Definitions.Grid));
            if (build != null)
            {
                build(new GridExtensionBundlesBuilder(Scripts));
            }

            return this;
        }

        /// <summary>
        /// Registers input script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public BasicBundlesBuilder Input()
        {
            Scripts.OwnerTypes.Add(typeof(Definitions.Input));
            return this;
        }

        /// <summary>
        /// Registers gauge script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public BasicBundlesBuilder Gauge()
        {
            Scripts.OwnerTypes.Add(typeof(Definitions.Gauge));
            return this;
        }

        /// <summary>
        /// Registers collection view script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public BasicBundlesBuilder CollectionView()
        {
            Scripts.OwnerTypes.Add(typeof(Definitions.CollectionView));
            return this;
        }
    }
}