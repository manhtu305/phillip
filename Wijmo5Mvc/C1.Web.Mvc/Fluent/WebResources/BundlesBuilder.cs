﻿namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines base builder of script bundles.
    /// </summary>
    public abstract class BundlesBuilder : HideObjectMembers
    {
        internal BundlesBuilder(Scripts scripts)
        {
            Scripts = scripts;
        }

        internal Scripts Scripts
        {
            get;
            private set;
        }
    }
}
