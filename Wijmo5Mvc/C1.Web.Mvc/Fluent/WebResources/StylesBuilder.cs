﻿namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the builder of registering style sheets.
    /// </summary>
    public sealed class StylesBuilder : ComponentBuilder<Component, StylesBuilder>
    {
        internal StylesBuilder(Styles styles) : base(styles)
        {
            Styles = styles;
        }

        private Styles Styles
        {
            get;
            set;
        }

        /// <summary>
        /// Sets the theme.
        /// </summary>
        /// <param name="theme">The theme.</param>
        /// <returns>The current builder.</returns>
        public StylesBuilder Theme(string theme)
        {
            Styles.Theme = theme;
            return this;
        }
    }
}
