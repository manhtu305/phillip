﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the builder of grid extension script bundles.
    /// </summary>
    public sealed class GridExtensionBundlesBuilder : BundlesBuilder
    {
        internal GridExtensionBundlesBuilder(Scripts scripts) : base(scripts)
        {
        }

        /// <summary>
        /// Registers detail row extension script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public GridExtensionBundlesBuilder Detail()
        {
            Scripts.OwnerTypes.Add(typeof(GridExDefinitions.Detail));
            return this;
        }

        /// <summary>
        /// Registers filter extension script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public GridExtensionBundlesBuilder Filter()
        {
            Scripts.OwnerTypes.Add(typeof(GridExDefinitions.Filter));
            return this;
        }

        /// <summary>
        /// Registers group panel extension script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public GridExtensionBundlesBuilder GroupPanel()
        {
            Scripts.OwnerTypes.Add(typeof(GridExDefinitions.GroupPanel));
            return this;
        }
    }
}