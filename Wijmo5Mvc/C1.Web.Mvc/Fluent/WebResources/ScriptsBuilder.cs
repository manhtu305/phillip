﻿using C1.Web.Mvc.WebResources;
using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the builder of registering scripts.
    /// </summary>
    public class ScriptsBuilder : ComponentBuilder<Component, ScriptsBuilder>
    {
        internal ScriptsBuilder(Scripts scripts) : base(scripts)
        {
            Scripts = scripts;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal List<Type> OwnerTypes
        {
            [SmartAssembly.Attributes.DoNotObfuscate]
            get
            {
                return Scripts.OwnerTypes;
            }
        }

        private Scripts Scripts
        {
            get;
            set;
        }

        /// <summary>
        /// Registers all scripts in basic assembly.
        /// </summary>
        /// <returns>The current builder.</returns>
        public ScriptsBuilder Basic()
        {
            OwnerTypes.AddRange(WebResourcesHelper.BasicOwnerTypes);
            return this;
        }

        /// <summary>
        /// Registers the specified script bundle in basic assembly by <see cref="BasicBundlesBuilder"/>.
        /// </summary>
        /// <returns>The current builder.</returns>
        public ScriptsBuilder Basic(Action<BasicBundlesBuilder> build)
        {
            build(new BasicBundlesBuilder(Scripts));
            return this;
        }

        /// <summary>
        /// Registers the specified the globalization script with culture name.
        /// </summary>
        /// <param name="culture">The culture name.</param>
        /// <returns>The current builder.</returns>
        public ScriptsBuilder Culture(string culture)
        {
            Scripts.Culture = culture;
            return this;
        }
    }
}
