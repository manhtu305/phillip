﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the builder of chart extension script bundles.
    /// </summary>
    public sealed class ChartExtensionBundlesBuilder : BundlesBuilder
    {
        internal ChartExtensionBundlesBuilder(Scripts scripts) : base(scripts)
        {
        }

        /// <summary>
        /// Registers annotation extension script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public ChartExtensionBundlesBuilder Annotation()
        {
            Scripts.OwnerTypes.Add(typeof(ChartExDefinitions.Annotation));
            return this;
        }

        /// <summary>
        /// Registers line marker extension script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public ChartExtensionBundlesBuilder LineMarker()
        {
            Scripts.OwnerTypes.Add(typeof(ChartExDefinitions.LineMarker));
            return this;
        }

        /// <summary>
        /// Registers animation extension script bundle.
        /// </summary>
        /// <returns>The current builder.</returns>
        public ChartExtensionBundlesBuilder Animation()
        {
            Scripts.OwnerTypes.Add(typeof(ChartExDefinitions.Animation));
            return this;
        }
    }
}