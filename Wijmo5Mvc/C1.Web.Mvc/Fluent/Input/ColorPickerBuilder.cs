﻿using System.Linq;
using System.Collections.Generic;

#if !ASPNETCORE
using System.Drawing;
#endif

namespace C1.Web.Mvc.Fluent
{
    public partial class ColorPickerBuilder
    {
        /// <summary>
        /// Sets the Palette property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
#if ASPNETCORE
        public ColorPickerBuilder Paletter(params string[] value)
#else
        public ColorPickerBuilder Palette(params Color[] value)
#endif
        {
            Object.Palette = value.ToList();
            return this;
        }

        /// <summary>
        /// Sets the Palette property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
#if ASPNETCORE
        public ColorPickerBuilder Palette(List<string> value)
#else
        public ColorPickerBuilder Palette(List<Color> value)
#endif
        {
            Object.Palette = value;
            return this;
        }

        /// <summary>
        /// Sets the currently selected color.
        /// </summary>
#if ASPNETCORE
        public ColorPickerBuilder Value(string value)
#else
        public ColorPickerBuilder Value(Color value)
#endif
        {
            Object.Value = value;
            return this;
        }
    }
}
