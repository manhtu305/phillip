﻿using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    partial class MultiAutoCompleteBuilder<T>
    {
        /// <summary>
        /// Sets the SelectedIndexes property.
        /// </summary>
        /// <remarks>
        /// Gets or sets a list containing the indexes of the items that are currently selected.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public MultiAutoCompleteBuilder<T> SelectedIndexes(params int[] value)
        {
            Object.SelectedIndexes = value;
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override MultiAutoCompleteBuilder<T> SelectedIndex(int value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override MultiAutoCompleteBuilder<T> SelectedItem(T value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override MultiAutoCompleteBuilder<T> SelectedValue(object value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override MultiAutoCompleteBuilder<T> OnClientSelectedIndexChanged(string value)
        {
            return this;
        }
    }
}
