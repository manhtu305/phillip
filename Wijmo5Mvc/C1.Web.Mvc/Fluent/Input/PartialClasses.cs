﻿using System.Linq;

#if !ASPNETCORE
using System.Drawing;
#endif

namespace C1.Web.Mvc.Fluent
{
    public partial class InputColorBuilder
    {
        /// <summary>
        /// Sets the current color.
        /// </summary>
#if ASPNETCORE
        public InputColorBuilder Value(string value)
#else
        public InputColorBuilder Value(Color? value)
#endif
        {
            Object.Value = value;
            return this;
        }
    }
}
