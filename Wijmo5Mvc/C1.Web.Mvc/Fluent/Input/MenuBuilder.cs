﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// The builder for the Menu.
    /// </summary>
    public class MenuBuilder
        : MenuBaseBuilder<object, Menu, MenuBuilder>
    {
        /// <summary>
        /// Create one MenuBuilder instance.
        /// </summary>
        /// <param name="control">The Menu control.</param>
        public MenuBuilder(Menu control)
            : base(control)
        {
        }

        /// <summary>
        /// Sets the MenuItems.
        /// </summary>
        /// <param name="build">The build action</param>
        /// <returns>Current builder</returns>
        public MenuBuilder MenuItems(Action<MenuItemFactory> build)
        {
            var items = new List<MenuItem>();
            build(new MenuItemFactory(items));
            return Bind(items);
        }

        /// <summary>
        /// Sets the Command property.
        /// </summary>
        /// <param name="execute">The execute client function</param>
        /// <param name="canExecute">The canExecute client function</param>
        /// <returns>Current builder</returns>
        public MenuBuilder Command(string execute, string canExecute = null)
        {
            return Command(build => build.ExecuteCommand(execute).CanExecuteCommand(canExecute));
        }
    }
}
