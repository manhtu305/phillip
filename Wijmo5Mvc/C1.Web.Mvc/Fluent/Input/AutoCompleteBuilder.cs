﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    partial class AutoCompleteBuilder<T>
    {
        //An internal fluent command to set the internal property "Value".
        internal AutoCompleteBuilder<T> Value(object value)
        {
            Object.Values = new object[1] { value };
            return this;
        }
    }
}
