﻿
namespace C1.Web.Mvc.Fluent
{
    public partial class ComboBoxBuilder<T>
    {
        /// <summary>
        /// Sets the Name property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ComboBoxBuilder<T> Name(string value)
        {
            Object.Name = value;
            return this;
        }

        //An internal fluent command to set the internal property "Value".
        internal ComboBoxBuilder<T> Value(object value)
        {
            Object.Values = new object[1]{value};
            return this;
        }
    }
}
