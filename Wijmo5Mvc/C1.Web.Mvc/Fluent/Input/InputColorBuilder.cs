﻿using System.Linq;
using System.Collections.Generic;

#if !ASPNETCORE
using System.Drawing;
#endif

namespace C1.Web.Mvc.Fluent
{
    public partial class InputColorBuilder
    {
        /// <summary>
        /// Configurates <see cref="InputColor.Palette"/>.
        /// Sets an array that contains the colors in the palette.
        /// </summary>
        /// <remarks>
        /// The palette contains ten colors, represented by an array with ten strings. 
        /// The first two colors are usually white and black.
        /// </remarks>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
#if ASPNETCORE
        public InputColorBuilder Paletter(params string[] value)
#else
        public InputColorBuilder Palette(params Color[] value)
#endif
        {
            Object.Palette = value.ToList();
            return this;
        }

        /// <summary>
        /// Configurates <see cref="InputColor.Palette"/>.
        /// Sets an array that contains the colors in the palette.
        /// </summary>
        /// <remarks>
        /// The palette contains ten colors, represented by an array with ten strings. 
        /// The first two colors are usually white and black.
        /// </remarks>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
#if ASPNETCORE
        public InputColorBuilder Palette(List<string> value)
#else
        public InputColorBuilder Palette(List<Color> value)
#endif
        {
            Object.Palette = value;
            return this;
        }
    }
}
