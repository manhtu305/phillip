﻿namespace C1.Web.Mvc.Fluent
{
    public partial class ListBoxBuilder<T>
    {
        /// <summary>
        /// Sets the CheckedIndexes property.
        /// </summary>
        /// <remarks>
        /// Gets or sets an array containing the indexes that are currently checked.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ListBoxBuilder<T> CheckedIndexes(params int[] value)
        {
            Object.CheckedIndexes = value;
            return this;
        }

        /// <summary>
        /// Sets the CheckedValues property.
        /// </summary>
        /// <remarks>
        /// Gets or sets a list containing the values that are currently checked.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ListBoxBuilder<T> CheckedValues(params object[] value)
        {
            Object.CheckedValues = value;
            return this;
        }
    }
}
