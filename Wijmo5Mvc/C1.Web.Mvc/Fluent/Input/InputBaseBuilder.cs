﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    public partial class FormInputBaseBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Sets the Name property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TBuilder Name(string value)
        {
            Object.Name = value;
            return this as TBuilder;
        }
    }
}
