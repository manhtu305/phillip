﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// A builder factory sub class for MenuItem.
    /// </summary>
    public class MenuItemFactory : ListItemFactory<MenuItem, MenuItemBuilder>
    {
        /// <summary>
        /// Create a MenuItemFactory instance.
        /// </summary>
        /// <param name="list">The list of MenuItem</param>
        public MenuItemFactory(IList<MenuItem> list)
            : base(list, s => new MenuItemBuilder(s))
        {
        }

        /// <summary>
        /// An override factory method for adding MenuItem.
        /// </summary>
        /// <param name="header">The header of the MenuItem</param>
        /// <param name="commandParameter">The command parameter of the MenuItem</param>
        /// <returns>Current builder</returns>
        public MenuItemBuilder Add(string header, object commandParameter = null)
        {
            return Add().Header(header).CommandParameter(commandParameter);
        }

        /// <summary>
        /// Add a MenuItem that is a separator.
        /// </summary>
        /// <returns>Current builder</returns>
        public MenuItemBuilder AddSeparator()
        {
            return Add().IsSeparator(true);
        }
    }

    public partial class MenuItemBuilder
    {
        /// <summary>
        /// Sets the Command property.
        /// </summary>
        /// <param name="build">The build action</param>
        /// <returns>Current builder</returns>
        public MenuItemBuilder Command(Action<MenuCommandBuilder> build)
        {
            // We want Command property initialized only when this builder method is called.
            // Wijmo client will use item command if it has value (even an empty object, which causes error)
            if (Object.Command == null)
            {
                Object.Command = new MenuCommand();
            }
            build(new MenuCommandBuilder(Object.Command));
            return this;
        }

        /// <summary>
        /// Sets the Command property.
        /// </summary>
        /// <param name="execute">The execute client function</param>
        /// <param name="canExecute">The canExecute client function</param>
        /// <returns>Current builder</returns>
        public MenuItemBuilder Command(string execute, string canExecute = null)
        {
            return Command(build => build.ExecuteCommand(execute).CanExecuteCommand(canExecute));
        }
    }
}
