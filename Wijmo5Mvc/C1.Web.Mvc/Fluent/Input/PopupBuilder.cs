﻿using System;

namespace C1.Web.Mvc.Fluent
{
    public partial class PopupBuilder
    {
        /// <summary>
        /// Sets the Content property.
        /// </summary>
        /// <remarks>
        /// Sets the HTML content which the popup should display.
        /// </remarks>
        /// <param name="value">The string value.</param>
        /// <returns>Current builder</returns>
        public PopupBuilder Content(string value)
        {
            Object.ContentFunc = null;
            Object.Content = value;
            return this;
        }

        /// <summary>
        /// Sets the Content property.
        /// </summary>
        /// <remarks>
        /// Sets the HTML content which the popup should display.
        /// </remarks>
        /// <param name="value">The content wrapped in a regular HTML tag or text tag (Razor syntax).</param>
        /// <returns>Current builder</returns>
        public PopupBuilder Content(Func<object, object> value)
        {
            Object.Content = string.Empty;
            Object.ContentFunc = value;
            return this;
        }
    }
}
