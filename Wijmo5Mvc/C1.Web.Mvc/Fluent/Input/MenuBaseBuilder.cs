﻿using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    public partial class MenuBaseBuilder<T, TControl, TBuilder>
    {
        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override TBuilder HeaderPath(string value)
        {
            return base.HeaderPath(value);
        }

        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override TBuilder AutoExpandSelection(bool value)
        {
            return base.AutoExpandSelection(value);
        }
    }
}
