﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    public partial class InputTimeBuilder<T>
    {
        /// <summary>
        /// Sets the Name property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public InputTimeBuilder<T> Name(string value)
        {
            Object.Name = value;
            return this;
        }

        /// <summary>
        /// Sets the Step property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use Step(int value) instead.")]
        public InputTimeBuilder<T> Step(TimeSpan value)
        {
            return Step((int)value.TotalMinutes);
        }

        /// <summary>
        /// Sets the Step property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use Step(int value) instead.")]
        public InputTimeBuilder<T> Step(double value)
        {
            return Step((int)value);
        }
    }
}
