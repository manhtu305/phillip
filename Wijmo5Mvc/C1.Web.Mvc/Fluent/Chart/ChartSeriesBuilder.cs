﻿using System;
using System.Collections.Generic;
using C1.Web.Mvc.Chart;

namespace C1.Web.Mvc.Fluent
{
    public partial class ChartSeriesBaseBuilder<T, TControl, TBuilder>
    {
        /// <summary>
        /// Create one ChartSeriesBaseBuilder instance
        /// </summary>
        /// <param name="obj">The ChartSeries object.</param>
        /// <param name="Owner">The chart which owns this series.</param>
        public ChartSeriesBaseBuilder(TControl obj, FlexChart<T> Owner)
            : base(obj)
        {
            //obj._owner = Owner;
        }

        /// <summary>
        /// Sets the Visibility property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TBuilder Visibility(SeriesVisibility value)
        {
            Object.Visibility = value;
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the AxisX property.
        /// </summary>
        /// <param name="position">The position of AxisX</param>
        /// <returns>Current builder</returns>
        public TBuilder AxisX(Position position)
        {
            if (Object.AxisX == null)
            {
                Object.AxisX = new ChartAxis<T>(Object._owner, true);
            }
            Object.AxisX.Position = position;
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the AxisX property.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public TBuilder AxisX(Action<ChartAxisBuilder<T>> build)
        {
            if (Object.AxisX == null)
            {
                Object.AxisX = new ChartAxis<T>(Object._owner, true);
                Object.AxisX.Position = Position.Bottom;
            }

            build(new ChartAxisBuilder<T>(Object.AxisX));
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the AxisY property.
        /// </summary>
        /// <param name="position">The position of AxisY</param>
        /// <returns>Current builder</returns>
        public TBuilder AxisY(Position position)
        {
            if (Object.AxisY == null)
            {
                Object.AxisY = new ChartAxis<T>(Object._owner, false);
                Object.AxisY.Position = Position.Left;
                Object.AxisY.AxisLine = false;
                Object.AxisY.MajorGrid = false;
            }
            Object.AxisY.Position = position;
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the AxisY property.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public TBuilder AxisY(Action<ChartAxisBuilder<T>> build)
        {
            if (Object.AxisY == null)
            {
                // In client side the default axisY in series make the "outside" as default value of MajorTickMarkers.
                Object.AxisY = new ChartAxis<T>(Object._owner, false);
                Object.AxisY.Position = Position.Left;
                Object.AxisY.AxisLine = false;
                Object.AxisY.MajorGrid = false;
            }
            build(new ChartAxisBuilder<T>(Object.AxisY));
            return this as TBuilder;
        }

        /// <summary>
        /// Set the Symbol for Series.
        /// </summary>
        /// <param name="marker">The symbol marker type</param>
        /// <param name="symbolSize">The size of symbol</param>
        /// <returns>Current builder</returns>
        public TBuilder Symbol(Marker marker, int symbolSize)
        {
            Object.SymbolMarker = marker;
            Object.SymbolSize = symbolSize;
            return this as TBuilder;
        }

        /// <summary>
        /// Set the Symbol for Series.
        /// </summary>
        /// <param name="marker">The symbol marker type</param>
        /// <param name="symbolSize">The size of symbol</param>
        /// <param name="symbolStyleBuilder">The builder of symbol style</param>
        /// <returns>Current builder</returns>
        public TBuilder Symbol(Marker marker, int symbolSize, Action<SVGStyleBuilder> symbolStyleBuilder)
        {
            Object.SymbolMarker = marker;
            Object.SymbolSize = symbolSize;
            symbolStyleBuilder(new SVGStyleBuilder(Object.SymbolStyle));
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the SymbolMarker property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TBuilder SymbolMarker(Marker value)
        {
            Object.SymbolMarker = value;
            return this as TBuilder;
        }


        /// <summary>
        /// Bind data with the url of the action which is used for reading data.
        /// </summary>
        /// <param name="readActionUrl">The url of the action which is used for reading data</param>
        /// <returns>Current builder</returns>
        public TBuilder Bind(string readActionUrl)
        {
            var itemsSource = Object.GetDataSource<CollectionViewService<T>>();
            itemsSource.ReadActionUrl = readActionUrl;
            return this as TBuilder;
        }

        /// <summary>
        /// Bind data with specified x data field name and the url of the action which is used for reading data.
        /// </summary>
        /// <param name="xDataFieldName">Sets the name of the x data field.</param>
        /// <param name="readActionUrl">The url of the action which is used for reading data</param>
        /// <returns>Current builder</returns>
        public TBuilder Bind(string xDataFieldName, string readActionUrl)
        {
            BindingX(xDataFieldName);
            Bind(readActionUrl);
            return this as TBuilder;
        }

        /// <summary>
        /// Bind data with specified x data field name, value property and the url of the action which is used for reading data.
        /// </summary>
        /// <param name="xDataFieldName">Sets the name of the x data field.</param>
        /// <param name="valueProperty">Sets the name of the value data field.</param>
        /// <param name="readActionUrl">The url of the action which is used for reading data</param>
        /// <returns>Current builder</returns>
        public TBuilder Bind(string xDataFieldName, string valueProperty, string readActionUrl)
        {
            BindingX(xDataFieldName);
            Binding(valueProperty);
            Bind(readActionUrl);
            return this as TBuilder;
        }

        /// <summary>
        /// Bind data with a data source.
        /// </summary>
        /// <param name="sourceCollection">The data source</param>
        /// <returns>Current builder</returns>
        public TBuilder Bind(IEnumerable<T> sourceCollection)
        {
            var itemsSource = Object.GetDataSource<CollectionViewService<T>>();
            itemsSource.SourceCollection = sourceCollection;
            return this as TBuilder;
        }

        /// <summary>
        /// Bind data with specified x data field name and a data source.
        /// </summary>
        /// <param name="xDataFieldName">Sets the name of the x data field.</param>
        /// <param name="sourceCollection">The data source</param>
        /// <returns></returns>
        public TBuilder Bind(string xDataFieldName, IEnumerable<T> sourceCollection)
        {
            BindingX(xDataFieldName);
            Bind(sourceCollection);
            return this as TBuilder;
        }

        /// <summary>
        /// Bind data with specified x data field name and value property of the data source.
        /// </summary>
        /// <param name="xDataFieldName">Sets the name of the x data field.</param>
        /// <param name="valueProperty">Sets the name of the value data field.</param>
        /// <param name="sourceCollection">The data source</param>
        /// <returns>Current builder</returns>
        public TBuilder Bind(string xDataFieldName, string valueProperty, IEnumerable<T> sourceCollection)
        {
            BindingX(xDataFieldName);
            Binding(valueProperty);
            Bind(sourceCollection);
            return this as TBuilder;
        }

        /// <summary>
        /// Configurates <see cref="ChartSeriesBase{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public TBuilder BindODataSource(Action<ODataCollectionViewServiceBuilder<T>> build)
        {
            build(new ODataCollectionViewServiceBuilder<T>(Object.GetDataSource<ODataCollectionViewService<T>>()));
            return (TBuilder)this;
        }

        /// <summary>
        /// Configurates <see cref="ChartSeriesBase{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataVirtualCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder BindODataVirtualSource(Action<ODataVirtualCollectionViewServiceBuilder<T>> build)
        {
            build(new ODataVirtualCollectionViewServiceBuilder<T>(Object.GetDataSource<ODataVirtualCollectionViewService<T>>()));
            return (TBuilder)this;
        }
    }

    public partial class ChartSeriesBuilder<T>
    {
        /// <summary>
        /// Sets the ChartType property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the chart type for a specific series, overriding the chart type set on the overall chart.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ChartSeriesBuilder<T> ChartType(C1.Web.Mvc.Chart.ChartType value)
        {
            Object.ChartType = value;
            return this;
        }
    }

    public partial class FlexRadarSeriesBuilder<T>
    {
        /// <summary>
        /// Sets the ChartType property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the chart type for a specific series, overriding the chart type set on the overall chart.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public FlexRadarSeriesBuilder<T> ChartType(C1.Web.Mvc.Chart.RadarChartType value)
        {
            Object.ChartType = value;
            return this;
        }
    }


    public partial class TrendLineBuilder<T>
    {
        /// <summary>
        /// Sets the fit type of the trendline.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current Builder</returns>
        public TrendLineBuilder<T> FitType(TrendLineFitType value)
        {
            Object.FitType = value;
            return this;
        }
    }

    public partial class MovingAverageBuilder<T>
    {
        /// <summary>
        /// Sets the type of the moving average line.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current Builder</returns>
        public MovingAverageBuilder<T> Type(MovingAverageType value)
        {
            Object.Type = value;
            return this;
        }
    }
}
