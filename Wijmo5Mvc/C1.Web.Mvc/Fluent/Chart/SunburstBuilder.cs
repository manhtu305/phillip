﻿namespace C1.Web.Mvc.Fluent
{
    public partial class SunburstBuilder<T>
        : FlexPieBaseBuilder<T, Sunburst<T>, SunburstBuilder<T>>
    {
        /// <summary>
        /// Sets the BindingName property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the name of the property that contains the name of the data item. It should be an array or a string.
        /// </remarks>
        /// <param name="names">The value</param>
        /// <returns>Current builder</returns>
        public SunburstBuilder<T> BindingName(params string[] names)
        {
            return base.BindingName(string.Join(",", names));
        }
    }
}
