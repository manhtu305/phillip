﻿namespace C1.Web.Mvc.Fluent
{
    public partial class WaterfallBuilder<T>
        : ExtraSeriesBuilder<T, Waterfall<T>, WaterfallBuilder<T>>
    {
        /// <summary>
        /// Sets the IntermediateTotalPositions property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the value of the property that contains the index for positions of the intermediate total bar.The property should work with ShowIntermediateTotal and IntermediateToolLabels property.
        /// </remarks>
        /// <param name="positions">The value</param>
        /// <returns>Current builder</returns>
        public WaterfallBuilder<T> IntermediateTotalPositions(params int[] positions)
        {
            Object.IntermediateTotalPositions = positions;
            return this;
        }

        /// <summary>
        /// Sets the IntermediateTotalLabels property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the value of the property that contains the label of the intermediate total bar. The property should work with ShowIntermediateTotal and IntermediateToolPositions properties.
        /// </remarks>
        /// <param name="labels">The value</param>
        /// <returns>Current builder</returns>
        public WaterfallBuilder<T> IntermediateTotalLabels(params string[] labels)
        {
            Object.IntermediateTotalLabels = labels;
            return this;
        }

    }
}
