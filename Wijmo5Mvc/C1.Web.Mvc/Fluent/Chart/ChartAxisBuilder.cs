﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Mvc.Chart;

namespace C1.Web.Mvc.Fluent
{
    public partial class ChartAxisBuilder<T>
    {
        /// <summary>
        /// Sets the enumerated axis position.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Position(C1.Web.Mvc.Chart.Position value)
        {
            Object.Position = value;
            return this;
        }

        /// <summary>
        /// Sets the location of axis major tick marks.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> MajorTickMarks(C1.Web.Mvc.Chart.AxisTickMark value)
        {
            Object.MajorTickMarks = value;
            return this;
        }

        /// <summary>
        /// Sets the location of axis minor tick marks.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> MinorTickMarks(C1.Web.Mvc.Chart.AxisTickMark value)
        {
            Object.MinorTickMarks = value;
            return this;
        }

        /// <summary>
        /// Sets the overlapping method for axis labels.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ChartAxisBuilder<T> OverlappingLabels(AxisOverlappingLabels value)
        {
            Object.OverlappingLabels = value;
            return this;
        }

        /// <summary>
        /// Bind the axis with a special data source to customize the labels.
        /// </summary>
        /// <param name="readActionUrl">The url of the action which is used for reading data</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Bind(string readActionUrl)
        {
            var itemsSource = Object.GetDataSource<CollectionViewService<T>>();
            itemsSource.ReadActionUrl = readActionUrl;
            return this;
        }

        /// <summary>
        /// Bind the axis with a special data source to customize the labels.
        /// </summary>
        /// <param name="bindingFields">The field for binding with the data source.</param>
        /// <param name="readActionUrl">The url of the action which is used for reading data</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Bind(string bindingFields, string readActionUrl)
        {
            Binding(bindingFields);
            Bind(readActionUrl);
            return this;
        }

        /// <summary>
        /// Bind the axis with a special data source to customize the labels.
        /// </summary>
        /// <param name="sourceCollection">The data source</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Bind(IEnumerable<T> sourceCollection)
        {
            var itemsSource = Object.GetDataSource<CollectionViewService<T>>();
            itemsSource.SourceCollection = sourceCollection;
            return this;
        }

        /// <summary>
        /// Bind the axis with a special data source to customize the labels.
        /// </summary>
        /// <param name="bindingFields">The field for binding with the data source.</param>
        /// <param name="sourceCollection">The data source</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Bind(string bindingFields, IEnumerable<T> sourceCollection)
        {
            Binding(bindingFields);
            Bind(sourceCollection);
            return this;
        }

        /// <summary>
        /// Sets the Min property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the minimum value shown on the axis. If not set, the minimum is calculated automatically.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Min(double? value)
        {
            Object.Min = value;
            return this;
        }

        /// <summary>
        /// Sets the Min property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the minimum value shown on the axis. If not set, the minimum is calculated automatically.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Min(DateTime value)
        {
            Object.Min = value;
            return this;
        }

        /// <summary>
        /// Sets the Max property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the maximum value shown on the axis. If not set, the maximum is calculated automatically.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Max(double? value)
        {
            Object.Max = value;
            return this;
        }

        /// <summary>
        /// Sets the Max property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the maximum value shown on the axis. If not set, the maximum is calculated automatically.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ChartAxisBuilder<T> Max(DateTime value)
        {
            Object.Max = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="ChartAxis{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public ChartAxisBuilder<T> BindODataSource(Action<ODataCollectionViewServiceBuilder<T>> build)
        {
            build(new ODataCollectionViewServiceBuilder<T>(Object.GetDataSource<ODataCollectionViewService<T>>()));
            return this;
        }

        /// <summary>
        /// Configurates <see cref="ChartAxis{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataVirtualCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public ChartAxisBuilder<T> BindODataVirtualSource(Action<ODataVirtualCollectionViewServiceBuilder<T>> build)
        {
            build(new ODataVirtualCollectionViewServiceBuilder<T>(Object.GetDataSource<ODataVirtualCollectionViewService<T>>()));
            return this;
        }
    }
}
