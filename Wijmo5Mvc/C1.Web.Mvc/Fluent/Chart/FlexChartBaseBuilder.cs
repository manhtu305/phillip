﻿using System.Collections.Generic;
using C1.Web.Mvc.Chart;
#if ASPNETCORE
using Color = System.String;
#else
using System.Drawing;
#endif

namespace C1.Web.Mvc.Fluent
{
    public partial class FlexChartBaseBuilder<T, TControl, TBuilder>
    {
        private TBuilder CurrentBuilder
        {
            get
            {
                return (TBuilder)this;
            }
        }

        /// <summary>
        /// Configurates <see cref="TreeMap{T}.Palette"/>.
        /// Sets an array of default colors to be used in a treemap.
        /// </summary>
        /// <remarks>
        /// The array contains strings that represent CSS colors.
        /// </remarks>
        /// <param name="colors">The color array</param>
        /// <returns>Current builder</returns>
        public virtual TBuilder Palette(params Color[] colors)
        {
            return Palette((IEnumerable<Color>)colors);
        }

        /// <summary>
        /// Configurates <see cref="TreeMap{T}.Palette"/>.
        /// Sets an array of default colors to be used in a treemap.
        /// </summary>
        /// <remarks>
        /// The array contains strings that represent CSS colors.
        /// </remarks>
        /// <param name="colors">The color collection</param>
        /// <returns>Current builder</returns>
        public virtual TBuilder Palette(IEnumerable<Color> colors)
        {
            Object.Palette = colors;
            return CurrentBuilder;
        }

        /// <summary>
        /// Configurates <see cref="TreeMap{T}.Legend" />.
        /// Sets legend's position.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Legend(Position position)
        {
            Object.Legend.Position = position;
            return CurrentBuilder;
        }

        /// <summary>
        /// Configurates <see cref="TreeMap{T}.Legend" />.
        /// Sets legend's orientation.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Legend(LegendOrientation orientation)
        {
            Object.Legend.Orientation = orientation;
            return CurrentBuilder;
        }
    }
}
