﻿using System;
using C1.Web.Mvc.Chart;
using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    public partial class FlexChartBuilder<T>
    {
        /// <summary>
        /// Sets the Stacking property.
        /// </summary>
        /// <remarks>
        /// Gets or sets whether and how series objects are stacked.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public FlexChartBuilder<T> Stacking(Stacking value)
        {
            Object.Stacking = value;
            return this;
        }

        /// <summary>
        /// Sets the Options property.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public FlexChartBuilder<T> Options(Action<ExtraOptionsBuilder> build)
        {
            build(new ExtraOptionsBuilder(Object.Options));
            return this;
        }

        /// <summary>
        /// Set the FlexChart series.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public FlexChartBuilder<T> Series(Action<SeriesListFactory<T, FlexChart<T>, ChartSeries<T>, ChartSeriesBuilder<T>, ChartType>> build)
        {
            build(new SeriesListFactory<T, FlexChart<T>, ChartSeries<T>, ChartSeriesBuilder<T>, ChartType>(Object.Series, Object));
            return this;
        }

        /// <summary>
        /// Set the PlotAreas.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public FlexChartBuilder<T> PlotAreas(Action<PlotAreaListFactory<T, FlexChart<T>, PlotArea, PlotAreaBuilder>> build)
        {
            build(new PlotAreaListFactory<T, FlexChart<T>, PlotArea, PlotAreaBuilder>(Object.PlotAreas, Object));
            return this;
        }

        /// <summary>
        /// Add an extender.
        /// </summary>
        /// <remarks>
        /// This is no longer used, just remove it.  Please use specific extender instead.
        /// </remarks>
        /// <param name="extender">The specified extender.</param>
        /// <returns></returns>
        [Obsolete("This is no longer used, just remove it.  Please use specific extender instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public FlexChartBuilder<T> AddExtender(Extender extender)
        {
            Object.Extenders.Add(extender);
            return this;
        }
    }
}
