﻿using System;
using C1.Web.Mvc.Chart;

namespace C1.Web.Mvc.Fluent
{
    public partial class FlexRadarBuilder<T>
    {
        /// <summary>
        /// Set the FlexChart series.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public FlexRadarBuilder<T> Series(Action<SeriesListBaseFactory<T, FlexRadar<T>, FlexRadarSeries<T>, FlexRadarSeriesBuilder<T>, RadarChartType>> build)
        {
            build(new SeriesListBaseFactory<T, FlexRadar<T>, FlexRadarSeries<T>, FlexRadarSeriesBuilder<T>, RadarChartType>(Object.Series, Object));
            return this;
        }
    }
}
