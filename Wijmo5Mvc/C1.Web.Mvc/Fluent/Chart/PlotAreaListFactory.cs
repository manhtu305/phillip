﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines a factory to create PlotArea.
    /// </summary>
    /// <typeparam name="T">Model Type</typeparam>
    /// <typeparam name="TOwner">Chart derived from FlexChartCore</typeparam>
    /// <typeparam name="PlotArea">PlotArea</typeparam>
    /// <typeparam name="PlotAreaBuilder">PlotAreaBuilder</typeparam>
    public class PlotAreaListFactory<T, TOwner, PlotArea, PlotAreaBuilder> : 
        BaseBuilder<IList<PlotArea>, PlotAreaListFactory<T, TOwner, PlotArea, PlotAreaBuilder>>
    {
        private TOwner _owner;

        /// <summary>
        /// Initializes a new instance of the PlotAreaListFactory class.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="owner"></param>
        public PlotAreaListFactory(IList<PlotArea> list, TOwner owner)
            : base(list)
        {
            _owner = owner;
        }

        /// <summary>
        /// Add a default plotArea to the plotarea list.
        /// </summary>
        /// <returns>The plotarea builder</returns>
        public PlotAreaBuilder Add()
        {
            PlotArea plotArea = (PlotArea)Activator.CreateInstance(typeof(PlotArea));
            Object.Add(plotArea);
            return (PlotAreaBuilder)Activator.CreateInstance(typeof(PlotAreaBuilder), plotArea);
        }
    }
}
