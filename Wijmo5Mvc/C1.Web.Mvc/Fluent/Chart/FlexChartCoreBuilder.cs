﻿using System.Collections.Generic;
using C1.Web.Mvc.Chart;

namespace C1.Web.Mvc.Fluent
{
    public partial class FlexChartCoreBuilder<T, TControl, TBuilder>
    {
        private TBuilder CurrentBuilder
        {
            get
            {
                return (TBuilder)this;
            }
        }

        #region Bind
        /// <summary>
        /// Bind data with specified x data field name and the url of the action which is used for reading data.
        /// </summary>
        /// <param name="xDataFieldName">Sets the name of the x data field.</param>
        /// <param name="readActionUrl">The url of the action which is used for reading data</param>
        /// <returns>Current builder</returns>
        public TBuilder Bind(string xDataFieldName, string readActionUrl)
        {
            Object.BindingX = xDataFieldName;
            return Bind(readActionUrl);
        }

        /// <summary>
        /// Bind data with specified x data field name, value property and the url of the action which is used for reading data.
        /// </summary>
        /// <param name="xDataFieldName">Sets the name of the x data field.</param>
        /// <param name="valueProperty">Sets the name of the value data field.</param>
        /// <param name="readActionUrl">The url of the action which is used for reading data</param>
        /// <returns>Current builder</returns>
        public TBuilder Bind(string xDataFieldName, string valueProperty, string readActionUrl)
        {
            Object.BindingX = xDataFieldName;
            Object.Binding = valueProperty;
            return Bind(readActionUrl);
        }


        /// <summary>
        /// Bind data with specified x data field name and a data source.
        /// </summary>
        /// <param name="xDataFieldName">Sets the name of the x data field.</param>
        /// <param name="sourceCollection">The data source</param>
        /// <returns></returns>
        public TBuilder Bind(string xDataFieldName, IEnumerable<T> sourceCollection)
        {
            Object.BindingX = xDataFieldName;
            return Bind(sourceCollection);
        }

        /// <summary>
        /// Bind data with specified x data field name and value property of the data source.
        /// </summary>
        /// <param name="xDataFieldName">Sets the name of the x data field.</param>
        /// <param name="valueProperty">Sets the name of the value data field.</param>
        /// <param name="sourceCollection">The data source</param>
        /// <returns>Current builder</returns>
        public TBuilder Bind(string xDataFieldName, string valueProperty, IEnumerable<T> sourceCollection)
        {
            Object.BindingX = xDataFieldName;
            Object.Binding = valueProperty;
            return Bind(sourceCollection);
        }
        #endregion

        /// <summary>
        /// Sets x-axis's position.
        /// </summary>
        /// <param name="position">The position</param>
        /// <returns>Current builder</returns>
        public TBuilder AxisX(Position position)
        {
            Object.AxisX.Position = position;
            return CurrentBuilder;
        }

        /// <summary>
        /// Sets y-axis's position.
        /// </summary>
        /// <param name="position">The position</param>
        /// <returns>Current builder</returns>
        public TBuilder AxisY(Position position)
        {
            Object.AxisY.Position = position;
            return CurrentBuilder;
        }
    }
}


