﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    public partial class FlexPieBaseBuilder<T, TControl, TBuilder>
    {
        /// <summary>
        /// Bind data with specified name property, value property and the url of the action which is used for reading data.
        /// </summary>
        /// <param name="nameProperty">Gets or sets the name of the property that contains the name of the data item.</param>
        /// <param name="valueProperty">The name of the property that contains the Y values.</param>
        /// <param name="readActionUrl">The url of the action which is used for reading data</param>
        /// <returns>Current builder</returns>
        public FlexPieBaseBuilder<T, TControl, TBuilder> Bind(string nameProperty, string valueProperty, string readActionUrl) 
        {
            Object.BindingName = nameProperty;
            Object.Binding = valueProperty;
            return Bind(readActionUrl);
        }

        /// <summary>
        /// Bind data with specified name property and value property of the data source.
        /// </summary>
        /// <param name="nameProperty">Gets or sets the name of the property that contains the name of the data item.</param>
        /// <param name="valueProperty">The name of the property that contains the Y values.</param>
        /// <param name="sourceCollection">The data source</param>
        /// <returns>Current builder</returns>
        public FlexPieBaseBuilder<T, TControl, TBuilder> Bind(string nameProperty, string valueProperty, IEnumerable<T> sourceCollection = null)
        {
            Object.BindingName = nameProperty;
            Object.Binding = valueProperty;
            return Bind(sourceCollection);
        }
    }
}
