﻿using System;
using System.Collections.Generic;
#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines a factory to create different series.
    /// </summary>
    /// <typeparam name="T">Model Type</typeparam>
    /// <typeparam name="TOwner">Chart derived from FlexChartCore</typeparam>
    /// <typeparam name="TSeries">ChartSeries derived from ChartSeriesBase</typeparam>
    /// <typeparam name="TSeriesBuilder">ChartSeriesBuilder</typeparam>
    /// <typeparam name="TChartType">ChartType</typeparam>
    public class SeriesListBaseFactory<T, TOwner, TSeries, TSeriesBuilder, TChartType> : BaseBuilder<IList<ChartSeriesBase<T>>, SeriesListFactory<T, TOwner, TSeries, TSeriesBuilder, TChartType>>
        where TOwner : FlexChartCore<T>
        where TSeries : ChartSeriesBase<T>
        where TSeriesBuilder : ChartSeriesBaseBuilder<T, TSeries, TSeriesBuilder>
    {
        private TOwner _owner;

        #region Initializes a new instance of the SeriesListBaseFactory class.

        /// <summary>
        /// Initializes a new instance of the SeriesListBaseFactory class.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="owner"></param>
        public SeriesListBaseFactory(IList<ChartSeriesBase<T>> list, TOwner owner)
            : base(list)
        {
            _owner = owner;
        }

        #endregion

        #region Add a series

        /// <summary>
        /// Add a series.
        /// </summary>
        /// <typeparam name="TControl">Specifies the series type.</typeparam>
        /// <typeparam name="TBuilder">The series builder type.</typeparam>
        /// <param name="createControl">The function to create a series for the specified owner.</param>
        /// <param name="createBuilder">The builder for the series.</param>
        /// <returns>The series builder.</returns>
        protected TBuilder Add<TControl, TBuilder>(Func<TOwner, TControl> createControl, Func<TControl, TBuilder> createBuilder)
            where TControl : ChartSeriesBase<T>
            where TBuilder : ChartSeriesBaseBuilder<T, TControl, TBuilder>
        {
            var item = createControl(_owner);
            Object.Add(item);
            return createBuilder(item);
        }

        #endregion

        #region Add Normal Series

        /// <summary>
        /// Add a default Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public TSeriesBuilder Add()
        {
            var series = (TSeries)Activator.CreateInstance(typeof(TSeries), _owner);
            Object.Add(series);
            return (TSeriesBuilder)Activator.CreateInstance(typeof(TSeriesBuilder), series);
        }

        /// <summary>
        /// Add a default Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public TSeriesBuilder Add(string name)
        {
            var builder = Add();
            builder.Name(name);
            return builder;
        }

        /// <summary>
        /// Add a default Series to the series list.
        /// </summary>
        /// <param name="type">The type of series</param>
        /// <returns>The series builder</returns>
        public TSeriesBuilder Add(TChartType type)
        {
            var builder = Add();
            var series = builder.Object;
            var propInfo = series.GetType().GetProperty("ChartType");
            if (propInfo != null)
            {
                propInfo.SetValue(series, type, null);
            }
            return builder;
        }

        /// <summary>
        /// Add a default Series to the series list.
        /// </summary>
        /// <param name="type">The type of series</param>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public TSeriesBuilder Add(TChartType type, string name)
        {
            var builder = Add(name);
            var series = builder.Object;
            var propInfo = series.GetType().GetProperty("ChartType");
            if (propInfo != null)
            {
                propInfo.SetValue(series, type, null);
            }
            return builder;
        }

        #endregion
    }

    /// <summary>
    /// Define a factory to create different series.
    /// </summary>
    /// <typeparam name="T">Model Type</typeparam>
    /// <typeparam name="TOwner">Chart derived from FlexChartCore</typeparam>
    /// <typeparam name="TSeries">ChartSeries derived from ChartSeriesBase</typeparam>
    /// <typeparam name="TSeriesBuilder">ChartSeriesBuilder</typeparam>
    /// <typeparam name="TChartType">ChartType</typeparam>
    public class SeriesListFactory<T, TOwner, TSeries, TSeriesBuilder, TChartType> : SeriesListBaseFactory<T, TOwner, TSeries, TSeriesBuilder, TChartType>
        where TOwner : FlexChartCore<T>
        where TSeries : ChartSeriesBase<T>
        where TSeriesBuilder : ChartSeriesBaseBuilder<T, TSeries, TSeriesBuilder>
    {
        private TOwner _owner;

        /// <summary>
        /// Initializes a new instance of the SeriesListFactory class.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="owner"></param>
        public SeriesListFactory(IList<ChartSeriesBase<T>> list, TOwner owner)
            : base(list, owner)
        {
            _owner = owner;
        }

        #region AddTrendLine

        /// <summary>
        /// Add a TrendLine Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public TrendLineBuilder<T> AddTrendLine()
        {
            return Add<TrendLine<T>, TrendLineBuilder<T>>
                (
                    (_owner) => (new TrendLine<T>(_owner)),
                    (item) => (new TrendLineBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a TrendLine Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public TrendLineBuilder<T> AddTrendLine(string name)
        {
            var builder = AddTrendLine();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region Add MovingAverage

        /// <summary>
        /// Add a MovingAverage Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public MovingAverageBuilder<T> AddMovingAverage()
        {
            return Add<MovingAverage<T>, MovingAverageBuilder<T>>
                (
                    (_owner) => (new MovingAverage<T>(_owner)),
                    (item) => (new MovingAverageBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a MovingAverage Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public MovingAverageBuilder<T> AddMovingAverage(string name)
        {
            var builder = AddMovingAverage();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region Add YFunctionSeries

        /// <summary>
        /// Add a YFunction Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public YFunctionSeriesBuilder<T> AddYFunctionSeries()
        {
            return Add<YFunctionSeries<T>, YFunctionSeriesBuilder<T>>
                (
                    (_owner) => (new YFunctionSeries<T>(_owner)),
                    (item) => (new YFunctionSeriesBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a YFunction Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public YFunctionSeriesBuilder<T> AddYFunctionSeries(string name)
        {
            var builder = AddYFunctionSeries();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region Add ParametricFunctionSeries

        /// <summary>
        /// Add a ParametricFunction Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public ParametricFunctionSeriesBuilder<T> AddParametricFunctionSeries()
        {
            return Add<ParametricFunctionSeries<T>, ParametricFunctionSeriesBuilder<T>>
                (
                    (_owner) => (new ParametricFunctionSeries<T>(_owner)),
                    (item) => (new ParametricFunctionSeriesBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a ParametricFunction Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public ParametricFunctionSeriesBuilder<T> AddParametricFunctionSeries(string name)
        {
            var builder = AddParametricFunctionSeries();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region Add Waterfall

        /// <summary>
        /// Add a Waterfall Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public WaterfallBuilder<T> AddWaterfall()
        {
            return Add<Waterfall<T>, WaterfallBuilder<T>>
                (
                    (_owner) => (new Waterfall<T>(_owner)),
                    (item) => (new WaterfallBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a Waterfall Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public WaterfallBuilder<T> AddWaterfall(string name)
        {
            var builder = AddWaterfall();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region Add BoxWhisker

        /// <summary>
        /// Add a BoxWhisker Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public BoxWhiskerBuilder<T> AddBoxWhisker()
        {
            return Add<BoxWhisker<T>, BoxWhiskerBuilder<T>>
                (
                    (_owner) => (new BoxWhisker<T>(_owner)),
                    (item) => (new BoxWhiskerBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add a BoxWhisker Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public BoxWhiskerBuilder<T> AddBoxWhisker(string name)
        {
            var builder = AddBoxWhisker();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region Add ErrorBar

        /// <summary>
        /// Add an ErrorBar Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public ErrorBarBuilder<T> AddErrorBar()
        {
            return Add<ErrorBar<T>, ErrorBarBuilder<T>>
                (
                    (_owner) => (new ErrorBar<T>(_owner)),
                    (item) => (new ErrorBarBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add an ErrorBar Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public ErrorBarBuilder<T> AddErrorBar(string name)
        {
            var builder = AddErrorBar();
            builder.Name(name);
            return builder;
        }

        #endregion

        #region Add BreakEven

        /// <summary>
        /// Add an BreakEven Series to the series list.
        /// </summary>
        /// <returns>The series builder</returns>
        public BreakEvenBuilder<T> AddBreakEven()
        {
            return Add<BreakEven<T>, BreakEvenBuilder<T>>
                (
                    (_owner) => (new BreakEven<T>(_owner)),
                    (item) => (new BreakEvenBuilder<T>(item))
                );
        }

        /// <summary>
        /// Add an BreakEven Series to the series list.
        /// </summary>
        /// <param name="name">The name of series</param>
        /// <returns>The series builder</returns>
        public BreakEvenBuilder<T> AddBreakEven(string name)
        {
            var builder = AddBreakEven();
            builder.Name(name);
            return builder;
        }

        #endregion
    }
}
