﻿using System;
using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use FlexGridGroupPanel extender.
    /// </summary>
    public static class FlexGridGroupPanelExtension
    {
        /// <summary>
        /// Apply the FlexGridGroupPanel extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="gridGroupPanelBuilder">the specified flexgridgrouppanel builder</param>
        /// <param name="selector">The specified selector for the FlexGridGroupPanel's host element.</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> ShowGroupPanel<T>(this FlexGridBuilder<T> gridBuilder, Action<FlexGridGroupPanelBuilder<T>> gridGroupPanelBuilder, string selector = null)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            FlexGridGroupPanel<T> gridGroupPanel = new FlexGridGroupPanel<T>(grid, selector);
            gridGroupPanelBuilder(new FlexGridGroupPanelBuilder<T>(gridGroupPanel));
            grid.Extenders.Add(gridGroupPanel);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default FlexGridGroupPanel extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="selector">The specified selector for the FlexGridGroupPanel's host element.</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> ShowGroupPanel<T>(this FlexGridBuilder<T> gridBuilder, string selector = null)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            FlexGridGroupPanel<T> gridGroupPanel = new FlexGridGroupPanel<T>(grid, selector);
            grid.Extenders.Add(gridGroupPanel);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default FlexGridGroupPanel extender in FlexGrid.
        /// </summary>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="selector">The specified selector for the FlexGridGroupPanel's host element.</param>
        /// <returns></returns>
        [Obsolete("Use ShowGroupPanel<object> instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static FlexGridBuilder<object> ShowGroupPanel(this FlexGridBuilder<object> gridBuilder, string selector = null)
        {
            FlexGrid<object> grid = gridBuilder.Object;
            FlexGridGroupPanel<object> gridGroupPanel = new FlexGridGroupPanel<object>(grid, selector);
            grid.Extenders.Add(gridGroupPanel);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the FlexGridGroupPanel extender in FlexGrid.
        /// </summary>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="gridGroupPanelBuilder">the specified flexgridgrouppanel builder</param>
        /// <param name="selector">The specified selector for the FlexGridGroupPanel's host element.</param>
        /// <returns></returns>
        [Obsolete("Use ShowGroupPanel<object> instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static FlexGridBuilder<object> ShowGroupPanel(this FlexGridBuilder<object> gridBuilder, Action<FlexGridGroupPanelBuilder<object>> gridGroupPanelBuilder, string selector = null)
        {
            FlexGrid<object> grid = gridBuilder.Object;
            FlexGridGroupPanel<object> gridGroupPanel = new FlexGridGroupPanel<object>(grid, selector);
            gridGroupPanelBuilder(new FlexGridGroupPanelBuilder<object>(gridGroupPanel));
            grid.Extenders.Add(gridGroupPanel);
            return gridBuilder;
        }
    }
}
