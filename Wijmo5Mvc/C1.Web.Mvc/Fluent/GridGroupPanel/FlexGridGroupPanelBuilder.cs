﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    public partial class FlexGridGroupPanelBuilder<T> : ExtenderBuilder<FlexGridGroupPanel<T>, FlexGridGroupPanelBuilder<T>>
    {
        #region Ctor
        /// <summary>
        /// Create one FlexGridGroupPanelBuilder instance.
        /// </summary>
        /// <param name="extender">The FlexGridGroupPanel extender.</param>
        public FlexGridGroupPanelBuilder(FlexGridGroupPanel<T> extender)
            : base(extender)
        {
        }
        #endregion Ctor

        /// <summary>
        /// Sets the Selector property.
        /// </summary>
        /// <remarks>
        /// Gets or sets a value which specifies the selector.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [Obsolete("Use ShowGroupPanel(selector) instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public FlexGridGroupPanelBuilder<T> Selector(string value)
        {
            Object.Selector = value;
            return this;
        }

        /// <summary>
        /// Gets or sets the @see:grid.filter.FlexGridFilter to use for filtering the grid data.
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public FlexGridGroupPanelBuilder<T> Filter(Action<FlexGridFilterBuilder<T>> builder)
        {
            builder(new FlexGridFilterBuilder<T>( Object.Filter));
            return this;
        }
    }
}
