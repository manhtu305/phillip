﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the extensional methods for the FlexGridDetailProvider extender.
    /// </summary>
    public static class FlexGridDetailProviderExtension
    {
        /// <summary>
        ///  Apply the FlexGridDetailProvider extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified FlexGrid builder.</param>
        /// <param name="detailProviderBuilder">the specified FlexGridDetailProvider builder</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> ShowDetailRow<T>(this FlexGridBuilder<T> gridBuilder, Action<FlexGridDetailProviderBuilder<T>> detailProviderBuilder)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            FlexGridDetailProvider<T> gridDetailProvider = new FlexGridDetailProvider<T>(grid);
            detailProviderBuilder(new FlexGridDetailProviderBuilder<T>(gridDetailProvider));
            grid.Extenders.Add(gridDetailProvider);
            return gridBuilder;

        }
    }
}
