﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    public partial class FlexGridDetailProviderBuilder<T> : ExtenderBuilder<FlexGridDetailProvider<T>, FlexGridDetailProviderBuilder<T>>
    {
        #region Ctor
        /// <summary>
        /// Create one FlexGridDetailProviderBuilder instance.
        /// </summary>
        /// <param name="extender">The FlexGridDetailProvider extender.</param>
        public FlexGridDetailProviderBuilder(FlexGridDetailProvider<T> extender)
            : base(extender)
        {
        }
        #endregion Ctor
    }
}
