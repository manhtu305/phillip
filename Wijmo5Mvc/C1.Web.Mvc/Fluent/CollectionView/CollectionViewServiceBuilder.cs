﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    partial class CollectionViewServiceBuilder<T>
    {
        /// <summary>
        /// Creates one <see cref="CollectionViewServiceBuilder{T}" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="CollectionViewService{T}" /> object to be configurated.</param>
        public CollectionViewServiceBuilder(CollectionViewService<T> component)
            : base(component)
        {
        }

        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.CreateActionUrl" />.
        /// Sets the url of create action.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public CollectionViewServiceBuilder<T> Create(string value)
        {
            Object.CreateActionUrl = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.UpdateActionUrl" />.
        /// Sets the url of update action.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public CollectionViewServiceBuilder<T> Update(string value)
        {
            Object.UpdateActionUrl = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.DeleteActionUrl" />.
        /// Sets the url of delete action.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public CollectionViewServiceBuilder<T> Delete(string value)
        {
            Object.DeleteActionUrl = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.BatchEditActionUrl" />.
        /// Sets the url of batch edit action.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public CollectionViewServiceBuilder<T> BatchEdit(string value)
        {
            Object.BatchEditActionUrl = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.SourceCollection" />.
        /// Sets the source collection.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public CollectionViewServiceBuilder<T> Bind(IEnumerable<T> value)
        {
            Object.SourceCollection = value;
            return this;
        }

        /// <summary>
        /// Configurates <see cref="CollectionViewService{T}.ReadActionUrl" />.
        /// Sets the url of read action.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public CollectionViewServiceBuilder<T> Bind(string value)
        {
            Object.ReadActionUrl = value;
            return this;
        }
    }
}
