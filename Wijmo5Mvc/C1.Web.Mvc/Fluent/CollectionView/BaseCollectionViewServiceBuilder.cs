﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace C1.Web.Mvc.Fluent
{
    partial class BaseCollectionViewServiceBuilder<T, TControl, TBuilder>
    {
        /// <summary>
        /// Creates one <see cref="BaseCollectionViewServiceBuilder{T, TControl, TBuilder}" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="BaseCollectionViewService{T}" /> object to be configurated.</param>
        protected BaseCollectionViewServiceBuilder(TControl component)
            : base(component)
        {
        }

        #region PageIndex
        /// <summary>
        /// Configurates <see cref="BaseCollectionViewService{T}.PageIndex" />.
        /// Sets the page index.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder MoveToPage(int value)
        {
            Object.PageIndex = value;
            return this as TBuilder;
        }
        #endregion PageIndex

        #region GroupDescriptions
        /// <summary>
        /// Configurates <see cref="BaseCollectionViewService{T}.GroupDescriptions" />.
        /// Sets the group descriptions.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder GroupDescriptions(IEnumerable<GroupDescription> value)
        {
            if (value != null)
            {
                foreach (var groupDescription in value)
                {
                    Object.GroupDescriptions.Add(groupDescription);
                }
            }
            return this as TBuilder;
        }

        /// <summary>
        /// Configurates <see cref="BaseCollectionViewService{T}.GroupDescriptions" />.
        /// Sets the group descriptions with an action.
        /// </summary>
        /// <param name="build">An action to create the group descriptions.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder PropertyGroupDescriptions(
            Action<ListItemFactory<PropertyGroupDescription, PropertyGroupDescriptionBuilder>> build)
        {
            var propertyGroupDescriptions = new List<PropertyGroupDescription>();
            build(new ListItemFactory<PropertyGroupDescription, PropertyGroupDescriptionBuilder>
                (propertyGroupDescriptions, p => new PropertyGroupDescriptionBuilder(p)));
            GroupDescriptions(propertyGroupDescriptions);
            return this as TBuilder;
        }

        /// <summary>
        /// Configurates <see cref="BaseCollectionViewService{T}.GroupDescriptions" />.
        /// Sets the group descriptions with a name list.
        /// </summary>
        /// <param name="names">A list of the property names for grouping.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder GroupBy(params string[] names)
        {
            var propertyGroupDescriptions = names.Select(n => new PropertyGroupDescription { PropertyName = n });
            GroupDescriptions(propertyGroupDescriptions);
            return this as TBuilder;
        }
        #endregion GroupDescriptions

        #region SortDescriptions
        /// <summary>
        /// Configurates <see cref="BaseCollectionViewService{T}.SortDescriptions" />.
        /// Sets the sort descriptions with a name list in ascending order.
        /// </summary>
        /// <param name="names">A name list sorting by in ascending order.</param>
        /// <returns>Current builder.</returns>
        public TBuilder OrderBy(params string[] names)
        {
            var sortDescriptions = names.Select(n => new SortDescription { Property = n, Ascending = true });
            SortDescriptions(sortDescriptions);
            return this as TBuilder;
        }

        /// <summary>
        /// Configurates <see cref="BaseCollectionViewService{T}.SortDescriptions" />.
        /// Sets the sort descriptions with a name list in descending order.
        /// </summary>
        /// <param name="names">A name list sorting by in descending order.</param>
        /// <returns>Current builder.</returns>
        public TBuilder OrderByDescending(params string[] names)
        {
            var sortDescriptions = names.Select(n => new SortDescription { Property = n, Ascending = false });
            SortDescriptions(sortDescriptions);
            return this as TBuilder;
        }

        /// <summary>
        /// Configurates <see cref="BaseCollectionViewService{T}.SortDescriptions" />.
        /// Sets the sort descriptions.
        /// </summary>
        /// <param name="build">An action to build the sort description collection.</param>
        /// <returns>Current builder.</returns>
        public TBuilder SortDescriptions(
            Action<ListItemFactory<SortDescription, SortDescriptionBuilder>> build)
        {
            build(new ListItemFactory<SortDescription, SortDescriptionBuilder>
                (Object.SortDescriptions, s => new SortDescriptionBuilder(s)));
            return this as TBuilder;
        }

        private void SortDescriptions(IEnumerable<SortDescription> sortDescriptions)
        {
            if (sortDescriptions == null) return;
            foreach (var sortDescription in sortDescriptions)
            {
                Object.SortDescriptions.Add(sortDescription);
            }
        }
        #endregion SortDescriptions

        #region OnClientQueryData
        /// <summary>
        /// Configurates the <see cref="BaseCollectionViewService{T}.OnClientQueryData" /> client event.
        /// Obsoleted. Please use OnClientQueryData instead.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please use OnClientQueryData instead.")]
        public virtual TBuilder OnClientCollectingQueryData(string value)
        {
            return OnClientQueryData(value);
        }
        #endregion OnClientQueryData
    }
}
