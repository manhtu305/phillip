﻿namespace C1.Web.Mvc.Fluent
{
    partial class ODataCollectionViewServiceBuilder<T>
    {
        /// <summary>
        /// Creates one <see cref="ODataCollectionViewServiceBuilder{T}" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="ODataCollectionViewService{T}" /> object to be configurated.</param>
        public ODataCollectionViewServiceBuilder(ODataCollectionViewService<T> component)
            : base(component)
        {
        }
    }
}
