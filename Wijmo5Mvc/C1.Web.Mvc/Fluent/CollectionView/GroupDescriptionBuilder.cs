﻿using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// The builder for the GroupDescription.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class GroupDescriptionBuilder<TControl, TBuilder>
        : BaseBuilder<TControl, TBuilder>
        where TControl : GroupDescription
        where TBuilder : GroupDescriptionBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Create one ChartTooltipBuilder instance.
        /// </summary>
        /// <param name="control">The ChartTooltip control.</param>
        protected GroupDescriptionBuilder(TControl control)
            : base(control)
        {
        }
    }
}
