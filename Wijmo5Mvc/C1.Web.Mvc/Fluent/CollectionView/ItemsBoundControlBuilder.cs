﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    abstract partial class ItemsBoundControlBuilder<T, TControl, TBuilder>
    {
        #region CollectionViewService<T>
        /// <summary>
        /// Configurates <see cref="ItemsBoundControl{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build the items source.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Bind(Action<CollectionViewServiceBuilder<T>> build)
        {
            build(new CollectionViewServiceBuilder<T>(Object.GetDataSource<CollectionViewService<T>>()));
            return (TBuilder)this;
        }

        /// <summary>
        /// Configurates <see cref="ItemsBoundControl{T}.ItemsSource" />.
        /// Sets ItemsSource settings with a collection.
        /// </summary>
        /// <param name="value">A collection to specify the source collection.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Bind(IEnumerable<T> value)
        {
            return Bind(cvsb => cvsb.Bind(value));
        }

        /// <summary>
        /// Configurates <see cref="ItemsBoundControl{T}.ItemsSource" />.
        /// Sets ItemsSource settings with the specified action url.
        /// </summary>
        /// <param name="value">An action url to read the data.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Bind(string value)
        {
            return Bind(cvsb => cvsb.Bind(value));
        }
        #endregion CollectionViewService<T>

        #region ODataVirtualCollectionViewService
        /// <summary>
        /// Configurates <see cref="ItemsBoundControl{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataVirtualCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder BindODataVirtualSource(Action<ODataVirtualCollectionViewServiceBuilder<T>> build)
        {
            build(new ODataVirtualCollectionViewServiceBuilder<T>(Object.GetDataSource<ODataVirtualCollectionViewService<T>>()));
            return (TBuilder)this;
        }
        #endregion ODataVirtualCollectionViewService

        #region ODataCollectionViewService
        /// <summary>
        /// Configurates <see cref="ItemsBoundControl{T}.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder BindODataSource(Action<ODataCollectionViewServiceBuilder<T>> build)
        {
            build(new ODataCollectionViewServiceBuilder<T>(Object.GetDataSource<ODataCollectionViewService<T>>()));
            return (TBuilder)this;
        }
        #endregion ODataCollectionViewService
    }
}
