﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    partial class ODataVirtualCollectionViewServiceBuilder<T>
    {
        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override ODataVirtualCollectionViewServiceBuilder<T> MoveToPage(int value)
        {
            return this;
        }

        #region GroupDescriptions
        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override ODataVirtualCollectionViewServiceBuilder<T> GroupDescriptions(IEnumerable<GroupDescription> value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="build">An action to create the group descriptions.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override ODataVirtualCollectionViewServiceBuilder<T> PropertyGroupDescriptions(
            Action<ListItemFactory<PropertyGroupDescription, PropertyGroupDescriptionBuilder>> build)
        {
            return this;
        }

        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="names">A list of the property names for grouping.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override ODataVirtualCollectionViewServiceBuilder<T> GroupBy(params string[] names)
        {
            return this;
        }
        #endregion GroupDescriptions

        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override ODataVirtualCollectionViewServiceBuilder<T> OnClientPageChanged(string value)
        {
            return this;
        }


        /// <summary>
        /// Overrides to remove this method.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override ODataVirtualCollectionViewServiceBuilder<T> OnClientPageChanging(string value)
        {
            return this;
        }

        /// <summary>
        /// Creates one <see cref="ODataVirtualCollectionViewServiceBuilder{T}" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="ODataVirtualCollectionViewService{T}" /> object to be configurated.</param>
        public ODataVirtualCollectionViewServiceBuilder(ODataVirtualCollectionViewService<T> component)
            : base(component)
        {
        }
    }
}
