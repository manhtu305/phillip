﻿using System.Collections.Generic;
#if ASPNETCORE
using Microsoft.AspNetCore.Mvc.ViewFeatures;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.Fluent
{
    partial class BaseODataCollectionViewServiceBuilder<T, TControl, TBuilder>
    {
        /// <summary>
        /// Creates one <see cref="BaseODataCollectionViewServiceBuilder{T, TControl, TBuilder}" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="BaseODataCollectionViewService{T}" /> object to be configurated.</param>
        protected BaseODataCollectionViewServiceBuilder(TControl component)
            : base(component)
        {
        }

        #region RequestHeaders
        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.RequestHeaders" />.
        /// Sets an object containing request headers to be used when sending or requesting data. The most typical use for this property is in scenarios where authentication is required.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TBuilder RequestHeaders(IDictionary<string, object> value)
        {
            foreach(var item in value)
            {
                Object.RequestHeaders[item.Key] = item.Value;
            }
            return this as TBuilder;
        }

        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.RequestHeaders" />.
        /// Sets an object containing request headers to be used when sending or requesting data. The most typical use for this property is in scenarios where authentication is required.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TBuilder RequestHeaders(object value)
        {
            return RequestHeaders(HtmlHelper.AnonymousObjectToHtmlAttributes(value));
        }

        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.RequestHeaders" />.
        /// Sets an object containing request headers to be used when sending or requesting data. The most typical use for this property is in scenarios where authentication is required.
        /// </summary>
        /// <param name="key">The key of one request header.</param>
        /// <param name="value">The value of the request header.</param>
        /// <returns>Current builder.</returns>
        public TBuilder RequestHeader(string key, object value)
        {
            Object.RequestHeaders[key] = value;
            return this as TBuilder;
        }
        #endregion RequestHeaders

        #region DataTypes
        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.DataTypes" />.
        /// Sets an object to be used as a map for coercing data types when loading the data.
        /// </summary>
        /// <remarks>
        /// The object keys represent the field names and the values are <see cref="C1.Web.Mvc.Grid.DataType" /> values that indicate how the data should be coerced.
        /// </remarks>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TBuilder DataTypes(IDictionary<string, C1.Web.Mvc.Grid.DataType> value)
        {
            foreach(var item in value)
            {
                Object.DataTypes[item.Key] = item.Value;
            }
            return this as TBuilder;
        }

        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.DataTypes" />.
        /// Sets an object to be used as a map for coercing data types when loading the data.
        /// </summary>
        /// <remarks>
        /// The object keys represent the field names and the values are <see cref="C1.Web.Mvc.Grid.DataType" /> values that indicate how the data should be coerced.
        /// </remarks>
        /// <param name="fieldName">The field name.</param>
        /// <param name="dataType">The type of the field data.</param>
        /// <returns>Current builder.</returns>
        public TBuilder DataType(string fieldName, C1.Web.Mvc.Grid.DataType dataType)
        {
            Object.DataTypes[fieldName] = dataType;
            return this as TBuilder;
        }
        #endregion DataTypes

        #region Fields
        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.Fields" />.
        /// Sets an array containing the names of the fields to retrieve from the data source.
        /// </summary>
        /// <remarks>
        /// If this property is set to null or to an empty array, all fields are retrieved.
        /// </remarks>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TBuilder Fields(params string[] value)
        {
            Object.Fields = value;
            return this as TBuilder;
        }
        #endregion Fields

        #region Keys
        /// <summary>
        /// Configurates <see cref="BaseODataCollectionViewService{T}.Keys" />.
        /// Sets an array containing the names of the key fields. Key fields are required for update operations (add/remove/delete).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public TBuilder Keys(params string[] value)
        {
            Object.Keys = value;
            return this as TBuilder;
        }
        #endregion Keys
    }
}
