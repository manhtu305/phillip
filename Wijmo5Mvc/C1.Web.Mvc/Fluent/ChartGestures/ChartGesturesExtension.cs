﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use ChartGestures extender.
    /// </summary>
    public static class ChartGesturesExtension
    {
        /// <summary>
        /// Apply the ChartGestures extender in the chart which inherits from FlexChartCore.
        /// </summary>
        /// <typeparam name="T">The type of data.</typeparam>
        /// <typeparam name="TControl">The type of the chart.</typeparam>
        /// <typeparam name="TBuilder">The builder type for the chart.</typeparam>
        /// <param name="chartBuilder">The builder for the chart.</param>
        /// <param name="chartGestureBuilder">The builder for the ChartGestures object.</param>
        /// <returns>Current builder.</returns>
        public static FlexChartCoreBuilder<T, TControl, TBuilder> SupportGestures<T, TControl, TBuilder>(this FlexChartCoreBuilder<T, TControl, TBuilder> chartBuilder, Action<ChartGesturesBuilder<T>> chartGestureBuilder) 
            where TControl: FlexChartCore<T>
            where TBuilder: FlexChartCoreBuilder<T, TControl, TBuilder>
        {
            var chart = chartBuilder.Object;
            var gestures = new ChartGestures<T>(chart);
            chartGestureBuilder(new ChartGesturesBuilder<T>(gestures));
            chart.Extenders.Add(gestures);
            return chartBuilder;
        }
    }
}
