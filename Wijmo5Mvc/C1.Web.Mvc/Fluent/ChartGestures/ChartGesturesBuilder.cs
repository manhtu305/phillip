﻿namespace C1.Web.Mvc.Fluent
{
    public partial class ChartGesturesBuilder<T>
    {
        #region Ctors
        /// <summary>
        /// Create one ChartGesturesBuilder instance.
        /// </summary>
        /// <param name="extender">The ChartGestures extender</param>
        public ChartGesturesBuilder(ChartGestures<T> extender)
            :base(extender)
        {
        }
        #endregion Ctors
    }
}
