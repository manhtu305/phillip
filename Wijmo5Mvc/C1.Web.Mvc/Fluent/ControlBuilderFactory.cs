﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Collections;
#if ASPNETCORE
using Microsoft.AspNetCore.Mvc.Rendering;
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using IHtmlString = Microsoft.AspNetCore.Html.IHtmlContent;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
#if NETCORE3
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Text;
using System.Reflection;
using System.Collections.Concurrent;
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
#endif


#else
using System.Web;
using System.Web.Mvc;
using System.Drawing;
#endif

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines a factory class to create different controls' builders.
    /// </summary>
    public partial class ControlBuilderFactory : HideObjectMembers
    {
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal readonly HtmlHelper _helper;

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlBuilderFactory"/> class 
        /// by using the specified html helper instance.
        /// </summary>
        /// <param name="helper">The specified HtmlHelper object.</param>
        public ControlBuilderFactory(HtmlHelper helper)
        {
            if (helper == null)
            {
                throw new ArgumentNullException("helper");
            }

            _helper = helper;
        }

        #endregion Ctor

        #region Controls

        /// <summary>
        /// Render the css and js resources of the specified control type.
        /// </summary>
        /// <param name="controlTypes">The control types. Setting no control types means set all control types.</param>
        /// <returns>The html string</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please use Scripts method instead.")]
        public IHtmlString Resources(params Type[] controlTypes)
        {
            var c1Resources = new WebResourcesManager(_helper);

            foreach (var controlType in controlTypes)
            {
                c1Resources.ControlTypes.Add(controlType);
            }
            return c1Resources;
        }

        /// <summary>
        /// Render the css and js resources of the specified control type.
        /// </summary>
        /// <param name="theme">The theme name</param>
        /// <param name="controlTypes">The control types. Setting no control types means set all control types.</param>
        /// <returns>The html string</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please use Scripts method instead.")]
        public IHtmlString Resources(string theme, params Type[] controlTypes)
        {
            var c1Resources = (WebResourcesManager)Resources(controlTypes);
            c1Resources.Theme = theme;
            return c1Resources;
        }

        /// <summary>
        /// Render the deferred scripts.
        /// </summary>
        /// <returns>The html string.</returns>
        public IHtmlString DeferredScripts()
        {
            return new DeferredScripts(_helper);
        }

        /// <summary>
        /// Register the specified scripts.
        /// </summary>
        /// <returns>The <see cref="ScriptsBuilder"/>.</returns>
        public ScriptsBuilder Scripts()
        {
            return new ScriptsBuilder(new Scripts(_helper));
        }

        /// <summary>
        /// Register the style sheets.
        /// </summary>
        /// <returns>The <see cref="StylesBuilder"/>.</returns>
        public StylesBuilder Styles()
        {
            return new StylesBuilder(new Styles(_helper));
        }

        /// <summary>
        /// Create a Menu.
        /// </summary>
        /// <param name="selector">The selector</param>
        /// <returns>The MenuBuilder</returns>
        public MenuBuilder Menu(string selector = null)
        {
            return new MenuBuilder(new Menu(_helper, selector));
        }

        /// <summary>
        /// Create a CollectionViewServiceBuilder.
        /// </summary>
        /// <typeparam name="T">The data item type</typeparam>
        /// <param name="sourceCollection">The sourceCollection</param>
        /// <returns>The CollectionViewServiceBuilder</returns>
        public CollectionViewServiceBuilder<T> CollectionViewService<T>(IEnumerable<T> sourceCollection = null)
        {
            return new CollectionViewServiceBuilder<T>(new CollectionViewService<T>(_helper, sourceCollection));
        }

        /// <summary>
        /// Create a CollectionViewServiceBuilder.
        /// </summary>
        /// <param name="sourceCollection">The sourceCollection</param>
        /// <returns>The CollectionViewServiceBuilder</returns>
        public CollectionViewServiceBuilder<object> CollectionViewService(IEnumerable<object> sourceCollection = null)
        {
            return new CollectionViewServiceBuilder<object>(new CollectionViewService<object>(_helper, sourceCollection));
        }

        /// <summary>
        /// Create a ODataCollectionViewServiceBuilder.
        /// </summary>
        /// <returns>The ODataCollectionViewServiceBuilder</returns>
        public ODataCollectionViewServiceBuilder<object> ODataCollectionViewService()
        {
            return new ODataCollectionViewServiceBuilder<object>(new ODataCollectionViewService<object>(_helper));
        }

        /// <summary>
        /// Create a ODataVirtualCollectionViewServiceBuilder.
        /// </summary>
        /// <returns>The ODataVirtualCollectionViewServiceBuilder</returns>
        public ODataVirtualCollectionViewServiceBuilder<object> ODataVirtualCollectionViewService()
        {
            return new ODataVirtualCollectionViewServiceBuilder<object>(new ODataVirtualCollectionViewService<object>(_helper));
        }

        /// <summary>
        /// Creates a <see cref="Mvc.SplitLayout" /> service.
        /// </summary>
        /// <returns>A <see cref="SplitLayoutBuilder" />.</returns>
        public SplitLayoutBuilder SplitLayout()
        {
            return new SplitLayoutBuilder(new SplitLayout(_helper));
        }

        /// <summary>
        /// Creates a <see cref="Mvc.FlowLayout" /> service.
        /// </summary>
        /// <returns>A <see cref="FlowLayoutBuilder" />.</returns>
        public FlowLayoutBuilder FlowLayout()
        {
            return new FlowLayoutBuilder(new FlowLayout(_helper));
        }

        /// <summary>
        /// Creates a <see cref="Mvc.ManualGridLayout" /> service.
        /// </summary>
        /// <returns>A <see cref="ManualGridLayoutBuilder" />.</returns>
        public ManualGridLayoutBuilder ManualGridLayout()
        {
            return new ManualGridLayoutBuilder(new ManualGridLayout(_helper));
        }

        /// <summary>
        /// Creates a <see cref="Mvc.AutoGridLayout" /> service.
        /// </summary>
        /// <returns>A <see cref="AutoGridLayoutBuilder" />.</returns>
        public AutoGridLayoutBuilder AutoGridLayout()
        {
            return new AutoGridLayoutBuilder(new AutoGridLayout(_helper));
        }
        #endregion
    }

    /// <summary>
    /// Define a factory class to create different control builders, with model bindings.
    /// </summary>
    public class ControlBuilderFactory<TModel> : ControlBuilderFactory
    {
#if ASPNETCORE
        private readonly IHtmlHelper<TModel> _htmlHelper;
#else
        private readonly HtmlHelper<TModel> _htmlHelper;
#endif

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="C1.Web.Mvc.Fluent.ControlBuilderFactory"/> class 
        /// by using the specified html helper instance.
        /// </summary>
        /// <param name="helper">The specified HtmlHelper object.</param>
#if ASPNETCORE
        public ControlBuilderFactory(IHtmlHelper<TModel> helper)
#else
        public ControlBuilderFactory(HtmlHelper<TModel> helper)
#endif
            : base(helper)
        {
            _htmlHelper = helper;
        }

#endregion Ctor

        private string GetName(LambdaExpression expression)
        {
#if NETCORE3
            return _htmlHelper.ViewData.TemplateInfo.GetFullHtmlFieldName(GetExpressionText(expression));
#else
            return _htmlHelper.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
#endif
        }

#if NETCORE3
        private ModelExplorer FromLambdaExpression<TModel, TResult>(Expression<Func<TModel, TResult>> expression, ViewDataDictionary<TModel> viewData, IModelMetadataProvider metadataProvider)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            if (viewData == null)
            {
                throw new ArgumentNullException("viewData");
            }
            string str = null;
            Type type = null;
            bool flag = false;
            ExpressionType type2 = expression.Body.NodeType;
            if (type2 <= ((ExpressionType)((int)ExpressionType.Call)))
            {
                if (type2 == ((ExpressionType)((int)ExpressionType.ArrayIndex)))
                {
                    flag = true;
                }
                else if (type2 == ((ExpressionType)((int)ExpressionType.Call)))
                {
                    flag = IsSingleArgumentIndexer(expression.Body);
                }
            }
            else if (type2 == ((ExpressionType)((int)ExpressionType.MemberAccess)))
            {
                MemberExpression expression2 = (MemberExpression)expression.Body;
                str = (expression2.Member is PropertyInfo) ? expression2.Member.Name : null;
                if ((string.Equals(str, "Model", (StringComparison)StringComparison.Ordinal) && (expression2.Type == typeof(TModel))) && (expression2.Expression.NodeType == ((ExpressionType)((int)ExpressionType.Constant))))
                {
                    return FromModel(viewData, metadataProvider);
                }
                type = (expression2.Expression == null) ? null : expression2.Expression.Type;
                flag = true;
            }
            else if (type2 == ((ExpressionType)((int)ExpressionType.Parameter)))
            {
                return FromModel(viewData, metadataProvider);
            }
            if (!flag)
            {
                throw new InvalidOperationException("TemplateHelpers_TemplateLimitations");
            }
            Func<object, object> modelAccessor = delegate (object container)
            {
                try
                {
                    return CachedExpressionCompiler.Process<TModel, TResult>(expression)((TModel)container);
                }
                catch (NullReferenceException)
                {
                    return null;
                }
            };
            ModelMetadata metadataForType = null;
            if ((type != null) && (str != null))
            {
                metadataForType = metadataProvider.GetMetadataForType(type).Properties[str];
            }
            if (metadataForType == null)
            {
                metadataForType = metadataProvider.GetMetadataForType((Type)typeof(TResult));
            }
            return viewData.ModelExplorer.GetExplorerForExpression(metadataForType, modelAccessor);

        }
        private static ModelExplorer FromModel(ViewDataDictionary viewData, IModelMetadataProvider metadataProvider)
        {
            if (viewData == null)
            {
                throw new ArgumentNullException("viewData");
            }
            if (viewData.ModelMetadata.ModelType == typeof(object))
            {
                string model = (viewData.Model == null) ? null : Convert.ToString(viewData.Model, (IFormatProvider)CultureInfo.CurrentCulture);
                return metadataProvider.GetModelExplorerForType(typeof(string), model);
            }
            return viewData.ModelExplorer;
        }




        private string GetExpressionText(LambdaExpression expression)
        {
            string str;
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            
            
                ExpressionType type;
                bool flag = false;
                bool flag2 = false;
                int capacity = 0;
                int num2 = 0;
                int num3 = 0;
                Expression expression2 = expression.Body;
                while (expression2 != null)
                {
                    MemberExpression expression4;
                    type = expression2.NodeType;
                    if (type <= ((ExpressionType)((int)ExpressionType.Call)))
                    {
                        if (type == ((ExpressionType)((int)ExpressionType.ArrayIndex)))
                        {
                            goto Label_00AF;
                        }
                        if (type == ((ExpressionType)((int)ExpressionType.Call)))
                        {
                            goto Label_006D;
                        }
                    }
                    else
                    {
                        if (type == ((ExpressionType)((int)ExpressionType.MemberAccess)))
                        {
                            goto Label_00D9;
                        }
                        if (type == ((ExpressionType)((int)ExpressionType.Parameter)))
                        {
                            goto Label_0134;
                        }
                    }
                    goto Label_013B;
                Label_006D:
                    flag = true;
                    flag2 = false;
                    MethodCallExpression expression3 = (MethodCallExpression)expression2;
                    if (IsSingleArgumentIndexer((Expression)expression3))
                    {
                        capacity += "[99]".Length;
                        expression2 = expression3.Object;
                        num2++;
                        num3 = 0;
                    }
                    else
                    {
                        expression2 = null;
                    }
                    continue;
                Label_00AF:
                    flag = true;
                    flag2 = false;
                    capacity += "[99]".Length;
                    expression2 = ((BinaryExpression)expression2).Left;
                    num2++;
                    num3 = 0;
                    continue;
                Label_00D9:
                    expression4 = (MemberExpression)expression2;
                    string name = expression4.Member.Name;
                    if (name.Contains("__"))
                    {
                        expression2 = null;
                    }
                    else
                    {
                        flag2 = string.Equals("model", name, (StringComparison)StringComparison.OrdinalIgnoreCase);
                        capacity += name.Length + 1;
                        expression2 = expression4.Expression;
                        num2++;
                        num3++;
                    }
                    continue;
                Label_0134:
                    flag2 = false;
                    expression2 = null;
                    continue;
                Label_013B:
                    expression2 = null;
                }
                if (flag2)
                {
                    capacity -= ".model".Length;
                    num2--;
                    num3--;
                }
                if (num3 > 0)
                {
                    capacity--;
                }
                if (num2 == 0)
                {
                    return string.Empty;
                }
                StringBuilder builder = new StringBuilder(capacity);
                expression2 = expression.Body;
                while ((expression2 != null) && (num2 > 0))
                {
                    MemberExpression expression1;
                    num2--;
                    type = expression2.NodeType;
                    if (type != ((ExpressionType)((int)ExpressionType.ArrayIndex)))
                    {
                        if (type != ((ExpressionType)((int)ExpressionType.Call)))
                        {
                            if (type == ((ExpressionType)((int)ExpressionType.MemberAccess)))
                            {
                                goto Label_020D;
                            }
                        }
                        else
                        {
                            MethodCallExpression expression5 = (MethodCallExpression)expression2;
                            InsertIndexerInvocationText(builder, Enumerable.Single<Expression>((IEnumerable<Expression>)expression5.Arguments), expression);
                            expression2 = expression5.Object;
                        }
                    }
                    else
                    {
                        BinaryExpression expression6 = (BinaryExpression)expression2;
                        InsertIndexerInvocationText(builder, expression6.Right, expression);
                        expression2 = expression6.Left;
                    }
                    continue;
                Label_020D:
                    expression1 = (MemberExpression)expression2;
                    string str3 = expression1.Member.Name;
                    builder.Insert(0, str3);
                    if (num2 > 0)
                    {
                        builder.Insert(0, '.');
                    }
                    expression2 = expression1.Expression;
                }
                str = builder.ToString();
            
            return str;
        }

        public bool IsSingleArgumentIndexer(Expression expression)
        {
            MethodCallExpression expression2 = expression as MethodCallExpression;
            if ((expression2 != null) && (expression2.Arguments.Count == 1))
            {
                Type type = expression2.Method.DeclaringType;
                DefaultMemberAttribute customAttribute = CustomAttributeExtensions.GetCustomAttribute<DefaultMemberAttribute>((MemberInfo)IntrospectionExtensions.GetTypeInfo(type), true);
                if (customAttribute != null)
                {
                    foreach (PropertyInfo info in RuntimeReflectionExtensions.GetRuntimeProperties(type))
                    {
                        if (string.Equals(customAttribute.MemberName, info.Name, (StringComparison)StringComparison.Ordinal) && (info.GetMethod == expression2.Method))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void InsertIndexerInvocationText(StringBuilder builder, Expression indexExpression, LambdaExpression parentExpression)
        {
            Func<object, object> func;
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }
            if (indexExpression == null)
            {
                throw new ArgumentNullException("indexExpression");
            }
            if (parentExpression == null)
            {
                throw new ArgumentNullException("parentExpression");
            }
            if (parentExpression.Parameters == null)
            {
                throw new ArgumentException("parentExpression");
            }
            ParameterExpression expression = Expression.Parameter((Type)typeof(object), null);
            ParameterExpression[] expressionArray1 = new ParameterExpression[] { expression };
            Expression<Func<object, object>> expression2 = Expression.Lambda<Func<object, object>>((Expression)Expression.Convert(indexExpression, typeof(object)), expressionArray1);
            try
            {
                func = CachedExpressionCompiler.Process<object, object>(expression2);
            }
            catch (InvalidOperationException exception)
            {
                ParameterExpression[] expressionArray = Enumerable.ToArray<ParameterExpression>((IEnumerable<ParameterExpression>)parentExpression.Parameters);
                throw new InvalidOperationException(expressionArray[0].Name, (Exception)exception);
            }
            builder.Insert(0, ']');
            builder.Insert(0, Convert.ToString(func(null), (IFormatProvider)CultureInfo.InvariantCulture));
            builder.Insert(0, '[');
        }

        



#endif
        private string NameToId(string name)
        {
            if (string.IsNullOrEmpty(name)) return name;

            return name.Replace('.', '_').Replace('[', '_').Replace(']', '_').Replace('-', '_');
        }

#region Controls

        private
#if ASPNETCORE
            ModelExplorer
#else
            ModelMetadata
#endif
            FromLambdaExpression<TValue>(Expression<Func<TModel, TValue>> expression)
        {
#if ASPNETCORE
#if NETCORE3
            ModelExpressionProvider modelExpressionProvider = (ModelExpressionProvider)_htmlHelper.ViewContext.HttpContext.RequestServices.GetService(typeof(IModelExpressionProvider));
            var modelExplorer = modelExpressionProvider.CreateModelExpression(_htmlHelper.ViewData, expression);
            return modelExplorer.ModelExplorer;
#else
            return ExpressionMetadataProvider.FromLambdaExpression(expression, _htmlHelper.ViewData, _htmlHelper.MetadataProvider);
#endif
#else
            return ModelMetadata.FromLambdaExpression(expression, _htmlHelper.ViewData);
#endif
        }

        private InputNumberBuilder GetInputNumberBuilder<TValue>(Expression<Func<TModel, TValue>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            var name = GetName(expression);
            double? value = null;
            var model = metadata.Model as IConvertible;
            if (model != null)
            {
                value = model.ToDouble(CultureInfo.InvariantCulture);
            }
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputNumberBuilder(new InputNumber(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(value).IsRequired(Nullable.GetUnderlyingType(typeof(TValue)) == null);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, int>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, int?>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, short>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, short?>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, long>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, long?>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, double>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, double?>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, decimal>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, decimal?>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, float>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputNumberBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputNumberBuilder</returns>
        public InputNumberBuilder InputNumberFor(Expression<Func<TModel, float?>> expression)
        {
            return GetInputNumberBuilder(expression);
        }

        /// <summary>
        /// Create an InputMaskBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputMaskBuilder</returns>
        public InputMaskBuilder InputMaskFor<TObject>(Expression<Func<TModel, TObject>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            var name = GetName(expression);
            var value = metadata.Model != null ? metadata.Model.ToString() : null;
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputMaskBuilder(new InputMask(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).RawValue(value);
        }

        private DateTime? GetDateTimeValue<TValue>(Expression<Func<TModel, TValue>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            DateTime? value = null;
            if (metadata.Model is DateTime)
            {
                value = (DateTime)metadata.Model;
            }
            return value;
        }

        /// <summary>
        /// Create an InputDateBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputDateBuilder</returns>
        public InputDateBuilder InputDateFor(Expression<Func<TModel, DateTime>> expression)
        {
            var name = GetName(expression);
            var metadata = FromLambdaExpression(expression);
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputDateBuilder(new InputDate(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(GetDateTimeValue(expression));
        }

        /// <summary>
        /// Create an InputDateBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputDateBuilder</returns>
        public InputDateBuilder InputDateFor(Expression<Func<TModel, DateTime?>> expression)
        {
            var name = GetName(expression);
            var metadata = FromLambdaExpression(expression);
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputDateBuilder(new InputDate(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(GetDateTimeValue(expression)).IsRequired(false);
        }

        /// <summary>
        /// Create an InputDateTimeBuilder object.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputDateTimeBuilder object.</returns>
        public InputDateTimeBuilder InputDateTimeFor(Expression<Func<TModel, DateTime>> expression)
        {
            var name = GetName(expression);
            var metadata = FromLambdaExpression(expression);
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputDateTimeBuilder(new InputDateTime(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(GetDateTimeValue(expression));
        }

        /// <summary>
        /// Create an InputDateTimeBuilder object.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputDateTimeBuilder object.</returns>
        public InputDateTimeBuilder InputDateTimeFor(Expression<Func<TModel, DateTime?>> expression)
        {
            var name = GetName(expression);
            var metadata = FromLambdaExpression(expression);
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputDateTimeBuilder(new InputDateTime(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(GetDateTimeValue(expression)).IsRequired(false);
        }

        /// <summary>
        /// Create an InputTimeBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputTimeBuilder</returns>
        public InputTimeBuilder<object> InputTimeFor(Expression<Func<TModel, DateTime>> expression)
        {
            var name = GetName(expression);
            var metadata = FromLambdaExpression(expression);
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputTimeBuilder<object>(new InputTime<object>(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(GetDateTimeValue(expression));
        }

        /// <summary>
        /// Create an InputTimeBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputTimeBuilder</returns>
        public InputTimeBuilder<object> InputTimeFor(Expression<Func<TModel, DateTime?>> expression)
        {
            var name = GetName(expression);
            var metadata = FromLambdaExpression(expression);
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputTimeBuilder<object>(new InputTime<object>(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(GetDateTimeValue(expression)).IsRequired(false);
        }

#if ASPNETCORE
        /// <summary>
        /// Create an InputColorBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputColorBuilder</returns>
        public InputColorBuilder InputColorFor(Expression<Func<TModel, string>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            var name = GetName(expression);
            string value = metadata.Model == null ? null : metadata.Model.ToString();
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputColorBuilder(new InputColor(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(value);
        }
#else
        private InputColorBuilder GetInputColorBuilder<TValue>(Expression<Func<TModel, TValue>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            var name = GetName(expression);
            Color? value = null;
            if (metadata.Model is Color)
            {
                value = (Color)metadata.Model;
            }
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new InputColorBuilder(new InputColor(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(value).IsRequired(Nullable.GetUnderlyingType(typeof(TValue)) == null);
        }

        /// <summary>
        /// Create an InputColorBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputColorBuilder</returns>
        public InputColorBuilder InputColorFor(Expression<Func<TModel, Color>> expression)
        {
            return GetInputColorBuilder(expression);
        }

        /// <summary>
        /// Create an InputColorBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The InputColorBuilder</returns>
        public InputColorBuilder InputColorFor(Expression<Func<TModel, Color?>> expression)
        {
            return GetInputColorBuilder(expression);
        }
#endif

        /// <summary>
        /// Create a ComboBoxBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The ComboBoxBuilder</returns>
        public ComboBoxBuilder<object> ComboBoxFor<TObject>(Expression<Func<TModel, TObject>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            var name = GetName(expression);
            var value = metadata.Model != null ? metadata.Model : null;
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new ComboBoxBuilder<object>(new ComboBox<object>(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(value);
        }

        /// <summary>
        /// Create a AutoCompleteBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The AutoCompleteBuilder</returns>
        public AutoCompleteBuilder<object> AutoCompleteFor(Expression<Func<TModel, string>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            var name = GetName(expression);
            var value = metadata.Model != null ? metadata.Model.ToString() : null;
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new AutoCompleteBuilder<object>(new AutoComplete<object>(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).Value(value);
        }

        /// <summary>
        /// Create a MultiSelectBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The MultiSelectBuilder</returns>
        public MultiSelectBuilder<object> MultiSelectFor<TObject>(Expression<Func<TModel, IEnumerable<TObject>>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            var name = GetName(expression);
            var values = metadata.Model as IEnumerable;
            var objectValues = new List<object>();
            if (values != null)
            {
                foreach(var v in values)
                {
                    objectValues.Add(v);
                }
            }
            
            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new MultiSelectBuilder<object>(new MultiSelect<object>(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).CheckedValues(objectValues);
        }

        /// <summary>
        /// Create a MultiAutoCompleteBuilder.
        /// </summary>
        /// <param name="expression">The model bind expression</param>
        /// <returns>The MultiAutoCompleteBuilder</returns>
        public MultiAutoCompleteBuilder<object> MultiAutoCompleteFor<TObject>(Expression<Func<TModel, IEnumerable<TObject>>> expression)
        {
            var metadata = FromLambdaExpression(expression);
            var name = GetName(expression);
            var values = metadata.Model as IEnumerable;
            var objectValues = new List<object>();
            if (values != null)
            {
                foreach (var v in values)
                {
                    objectValues.Add(v);
                }
            }

            var unobtrusiveValidationAttributes = _htmlHelper.GetUnobtrusiveValidationAttributes(name, metadata);
            return new MultiAutoCompleteBuilder<object>(new MultiAutoComplete<object>(_htmlHelper) { ValidationAttributes = unobtrusiveValidationAttributes })
                .Id(NameToId(name)).Name(name).SelectedValues(objectValues);
        }

#endregion
    }

#if NETCORE3
    internal static class CachedExpressionCompiler
    {
        // Methods
        public static Func<TModel, TResult> Process<TModel, TResult>(Expression<Func<TModel, TResult>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            return Compiler<TModel, TResult>.Compile(expression);
        }

        // Nested Types
        private static class Compiler<TModel, TResult>
        {
            // Fields
            private static readonly ConcurrentDictionary<MemberInfo, Func<object, TResult>> _constMemberAccessCache;
            private static Func<TModel, TResult> _identityFunc;
            private static readonly ConcurrentDictionary<MemberInfo, Func<TModel, TResult>> _simpleMemberAccessCache;

            // Methods
            static Compiler()
            {
                CachedExpressionCompiler.Compiler<TModel, TResult>._simpleMemberAccessCache = new ConcurrentDictionary<MemberInfo, Func<TModel, TResult>>();
                CachedExpressionCompiler.Compiler<TModel, TResult>._constMemberAccessCache = new ConcurrentDictionary<MemberInfo, Func<object, TResult>>();
            }

            public static Func<TModel, TResult> Compile(Expression<Func<TModel, TResult>> expression)
            {
                if (expression == null)
                {
                    throw new ArgumentNullException("expression");
                }
                return (CachedExpressionCompiler.Compiler<TModel, TResult>.CompileFromIdentityFunc(expression) ?? (CachedExpressionCompiler.Compiler<TModel, TResult>.CompileFromConstLookup(expression) ?? (CachedExpressionCompiler.Compiler<TModel, TResult>.CompileFromMemberAccess(expression) ?? CachedExpressionCompiler.Compiler<TModel, TResult>.CompileSlow(expression))));
            }

            private static Func<TModel, TResult> CompileFromConstLookup(Expression<Func<TModel, TResult>> expression)
            {
                if (expression == null)
                {
                    throw new ArgumentNullException("expression");
                }
                ConstantExpression expression2 = expression.Body as ConstantExpression;
                if (expression2 != null)
                {
                    TResult constantValue = (TResult)expression2.Value;
                    return delegate (TModel _) {
                        return constantValue;
                    };
                }
                return null;
            }

            private static Func<TModel, TResult> CompileFromIdentityFunc(Expression<Func<TModel, TResult>> expression)
            {
                if (expression == null)
                {
                    throw new ArgumentNullException("expression");
                }
                if (expression.Body != expression.Parameters.FirstOrDefault())
                {
                    return null;
                }
                if (CachedExpressionCompiler.Compiler<TModel, TResult>._identityFunc == null)
                {
                    CachedExpressionCompiler.Compiler<TModel, TResult>._identityFunc = expression.Compile();
                }
                return CachedExpressionCompiler.Compiler<TModel, TResult>._identityFunc;
            }

            private static Func<TModel, TResult> CompileFromMemberAccess(Expression<Func<TModel, TResult>> expression)
            {
                if (expression == null)
                {
                    throw new ArgumentNullException("expression");
                }
                MemberExpression memberExpression = expression.Body as MemberExpression;
                if (memberExpression != null)
                {
                    if ((memberExpression.Expression == expression.Parameters.FirstOrDefault()) || (memberExpression.Expression == null))
                    {
                        return CachedExpressionCompiler.Compiler<TModel, TResult>._simpleMemberAccessCache.GetOrAdd(memberExpression.Member, delegate (MemberInfo _) {
                            return expression.Compile();
                        });
                    }
                    ConstantExpression expression2 = memberExpression.Expression as ConstantExpression;
                    if (expression2 != null)
                    {
                        Func<object, TResult> compiledExpression = CachedExpressionCompiler.Compiler<TModel, TResult>._constMemberAccessCache.GetOrAdd(memberExpression.Member, delegate (MemberInfo _) {
                            ParameterExpression expression1;
                            UnaryExpression expression2 = Expression.Convert((Expression)(expression1 = Expression.Parameter((Type)typeof(object), "capturedLocal")), memberExpression.Member.DeclaringType);
                            ParameterExpression[] expressionArray1 = new ParameterExpression[] { expression1 };
                            return Expression.Lambda<Func<object, TResult>>((Expression)memberExpression.Update((Expression)expression2), expressionArray1).Compile();
                        });
                        object capturedLocal = expression2.Value;
                        return delegate (TModel _) {
                            return compiledExpression(capturedLocal);
                        };
                    }
                }
                return null;
            }

            private static Func<TModel, TResult> CompileSlow(Expression<Func<TModel, TResult>> expression)
            {
                if (expression == null)
                {
                    throw new ArgumentNullException("expression");
                }
                return expression.Compile();
            }
        }
    }
#endif
}
