﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use ChartAnimation extender.
    /// </summary>
    public static class ChartAnimationExtension
    {
        /// <summary>
        /// Apply the ChartAnimation extender in the chart.
        /// </summary>
        /// <typeparam name="T">The type of data.</typeparam>
        /// <typeparam name="TControl">The type of the chart control which inherits from FlexChartBase.</typeparam>
        /// <typeparam name="TChartBuilder">The type of the chart builder which inherits from FlexChartBaseBuilder.</typeparam>
        /// <param name="chartBuilder">The specified chart builder.</param>
        /// <param name="animationBuilder">The action to set the ChartAnimation.</param>
        /// <returns>Current builder.</returns>
        public static TChartBuilder ShowAnimation<T, TControl, TChartBuilder>(this FlexChartBaseBuilder<T, TControl, TChartBuilder> chartBuilder, Action<ChartAnimationBuilder<T>> animationBuilder = null)
            where TControl : FlexChartBase<T>, IAnimatable
             where TChartBuilder : FlexChartBaseBuilder<T, TControl, TChartBuilder>
        {
            var chart = chartBuilder.Object;
            var animation = new ChartAnimation<T>(chart);
            if (animationBuilder != null)
            {
                animationBuilder(new ChartAnimationBuilder<T>(animation));
            }
            chart.Extenders.Add(animation);
            return chartBuilder as TChartBuilder;
        }
    }
}
