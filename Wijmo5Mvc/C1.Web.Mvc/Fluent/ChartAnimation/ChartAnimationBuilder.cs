﻿namespace C1.Web.Mvc.Fluent
{
    public partial class ChartAnimationBuilder<T>
    {
        #region Ctors
        /// <summary>
        /// Create one ChartAnimationBuilder instance.
        /// </summary>
        /// <param name="extender">The ChartAnimation extender</param>
        public ChartAnimationBuilder(ChartAnimation<T> extender)
            :base(extender)
        {
        }
        #endregion Ctors
    }
}
