﻿#if ASPNETCORE
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using IHtmlString = Microsoft.AspNetCore.Html.HtmlString;
using System.Text.Encodings.Web;
using System;
#else
using System.Web;
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Extends the standard <see cref="T:HtmlHelper"/> class.
    /// </summary>
    public static class HtmlHelperExtension
    {
        /// <summary>
        /// Create the ControlBuilderFactory instance via <see cref="T:HtmlHelper"/>.
        /// </summary>
        /// <param name="helper">The specified <see cref="T:HtmlHelper"/> object</param>
        /// <returns>An instance of ControlBuilderFactory</returns>
        public static ControlBuilderFactory C1(this HtmlHelper helper)
        {
            return new ControlBuilderFactory(helper);
        }

        /// <summary>
        /// Create the ControlBuilderFactory instance via html helper.
        /// </summary>
        /// <typeparam name="TModel">The specified model type</typeparam>
        /// <param name="helper">The specified html helper object</param>
        /// <returns>An instance of ControlBuilderFactory</returns>
        public static ControlBuilderFactory<TModel> C1<TModel>(this
#if ASPNETCORE
            IHtmlHelper
#else
            HtmlHelper 
#endif
            <TModel> helper)
        {
            return new ControlBuilderFactory<TModel>(helper);
        }
    }

    /// <summary>
    /// Define a static class to add the extension methods for <see cref="System.Web.IHtmlString"/>
    /// </summary>
    public static class IHtmlStringExtension
    {
        /// <summary>
        /// Render the html string in template mode.
        /// </summary>
        /// <param name="htmlString">The specified html string</param>
        /// <returns>An instance of html string with the template mode.</returns>
        public static IHtmlString ToTemplate(this IHtmlString htmlString)
        {
            string content = ToTemplate(htmlString.ToString());
            return
#if ASPNETCORE
                new IHtmlString(content);
#else
                MvcHtmlString.Create(content);
#endif
        }

#if ASPNETCORE

        /// <summary>
        /// Render the HelperResult in template mode.
        /// </summary>
        /// <param name="htmlContent">The specified HelperResult.</param>
        /// <returns>The HelperResult with the template mode.</returns>
        public static IHtmlContent ToTemplate(this IHtmlContent htmlContent)
        {
            return new HtmlContentWrapper((w, e) =>
            {
                using (var writer = new StringWriter())
                {
                    htmlContent.WriteTo(writer, e);
                    var content = ToTemplate(writer.ToString());
                    w.Write(content);
                }
            });
        }

        private class HtmlContentWrapper : IHtmlContent
        {
            private readonly Action<TextWriter, HtmlEncoder> _write;

            public HtmlContentWrapper(Action<TextWriter, HtmlEncoder> write)
            {
                _write = write;
            }

            public void WriteTo(TextWriter writer, HtmlEncoder encoder)
            {
                if (_write != null)
                {
                    _write(writer, encoder);
                }
            }
        }

#endif

        private static string ToTemplate(string content)
        {
            return content.Replace("</script>", "<\\/script>");
        }
    }

}
