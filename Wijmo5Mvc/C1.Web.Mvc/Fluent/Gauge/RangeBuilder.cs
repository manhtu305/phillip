﻿namespace C1.Web.Mvc.Fluent
{
    public partial class RangeBuilder
    {
        /// <summary>
        /// Sets the Color property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
#if ASPNETCORE
        public RangeBuilder Color(string value)
#else
        public RangeBuilder Color(System.Drawing.Color value)
#endif
        {
            Object.Color = value;
            return this;
        }
    }
}
