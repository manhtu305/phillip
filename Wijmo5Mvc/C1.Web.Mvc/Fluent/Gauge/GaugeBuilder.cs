﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Fluent
{
    public partial class GaugeBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Sets the Ranges.
        /// </summary>
        /// <param name="build">The build action</param>
        /// <returns>Current builder</returns>
        public GaugeBuilder<TControl, TBuilder> Ranges(Action<ListItemFactory<Range, RangeBuilder>> build)
        {
            var items = new List<Range>();
            build(new ListItemFactory<Range, RangeBuilder>(items, s => new RangeBuilder(s)));
            foreach (var item in items)
            {
                Object.Ranges.Add(item);
            }
            return this;
        }

        /// <summary>
        /// Sets the Min property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the minimum value that can be displayed on the gauge.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TBuilder Min(double value)
        {
            Object.Face.Min = value;
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the Max property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the maximum value that can be displayed on the gauge.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TBuilder Max(double value)
        {
            Object.Face.Max = value;
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the Value property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the value displayed on the gauge.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TBuilder Value(double value)
        {
            Object.Pointer.Max = value;
            return this as TBuilder;
        }
    }
}
