﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Mvc;
using C1.Web.Mvc.Localization;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Handler for header area in a table.
    /// </summary>
    public partial class HeaderTemplateBuilder
    {
        private CellTemplateHandler cellsBuilder;   

        /// <summary>
        /// Row count in header table.
        /// </summary>
        /// <param name="rowCount"></param>
        public void RowCount(int rowCount)
        {
            if(rowCount < 2)
                throw new ArgumentException(Resources.HeaderTemplateExcetionRowCount);
            Object.RowCount = rowCount;
        }

        /// <summary>
        /// Cells manager in header table.
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public Action<CellTemplateHandler> Cells(Action<CellTemplateHandler> builder)
        {
            if (Object.RowCount == 0)
                throw new ArgumentException(Resources.HeaderTemplateExceptionCells);
            else {
                if (cellsBuilder == null)
                    cellsBuilder = new CellTemplateHandler(Object);
                builder(cellsBuilder);
            }
            return builder;
        }
    }

    /// <summary>
    /// Handler for each cell unit in the header template.
    /// </summary>
    public class CellTemplateHandler : BaseBuilder<List<HeaderTemplateCell>, List<HeaderTemplateCellBuilder>>
    {
        private HeaderTemplate holder;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="holder"></param>
        public CellTemplateHandler(HeaderTemplate holder) : base(holder.Cells)
        {
            this.holder = holder;
        }

        /// <summary>
        /// Set value for properties of cell.
        /// </summary>
        /// <param name="row">row position in header table.</param>
        /// <param name="col">column position in header table.</param>
        /// <param name="rowSpan">row span in header table.</param>
        /// <param name="colSpan">column span in header table.</param>
        /// <param name="title">Text will appear in this cell.</param>
        public void Set(int row, int col, int rowSpan, int colSpan, string title)
        {
            if (row < 0)
                throw new ArgumentException(Resources.HeaderTemplateExceptionCellRow);
            if (col < 0)
                throw new ArgumentException(Resources.HeaderTemplateExceptionCellCol);
            if (rowSpan < 1)
                throw new ArgumentException(Resources.HeaderTemplateExceptionCellRowSpan);
            if (colSpan < 1)
                throw new ArgumentException(Resources.HeaderTemplateExceptionCellColSpan);
            if (row + rowSpan > holder.RowCount)
                throw new ArgumentException(Resources.HeaderTemplateExceptionCellTooHigh);

            this.Object.Add(new HeaderTemplateCell().Set(row, col, rowSpan, colSpan, title));
        }
    }
}