﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use Selector extender.
    /// </summary>
    public static class SelectorExtension
    {
        /// <summary>
        /// Apply the Selector extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="selectorBuilder">the specified Selector builder</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> Selector<T>(this FlexGridBuilder<T> gridBuilder, Action<SelectorBuilder<T>> selectorBuilder)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            Selector<T> selector = new Selector<T>(grid);
            selectorBuilder(new SelectorBuilder<T>(selector));
            grid.Extenders.Add(selector);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default Selector extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> Selector<T>(this FlexGridBuilder<T> gridBuilder)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            Selector<T> selector = new Selector<T>(grid);
            grid.Extenders.Add(selector);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default Selector extender in FlexGrid.
        /// </summary>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <returns></returns>
        public static FlexGridBuilder<object> Selector(this FlexGridBuilder<object> gridBuilder)
        {
            FlexGrid<object> grid = gridBuilder.Object;
            Selector<object> selector = new Selector<object>(grid);
            grid.Extenders.Add(selector);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the Selector extender in FlexGrid.
        /// </summary>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="selectorBuilder">the specified Selector builder</param>
        /// <returns></returns>
        public static FlexGridBuilder<object> Selector(this FlexGridBuilder<object> gridBuilder, Action<SelectorBuilder<object>> selectorBuilder)
        {
            FlexGrid<object> grid = gridBuilder.Object;
            Selector<object> selector = new Selector<object>(grid);
            selectorBuilder(new SelectorBuilder<object>(selector));
            grid.Extenders.Add(selector);
            return gridBuilder;
        }
    }
}
