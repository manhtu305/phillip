﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use BooleanChecker extender.
    /// </summary>
    public static class BooleanCheckerExtension
    {
        /// <summary>
        /// Apply the BooleanChecker extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="booleanCheckerBuilder">the specified BooleanChecker builder</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> BooleanChecker<T>(this FlexGridBuilder<T> gridBuilder, Action<BooleanCheckerBuilder<T>> booleanCheckerBuilder)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            BooleanChecker<T> booleanChecker = new BooleanChecker<T>(grid);
            booleanCheckerBuilder(new BooleanCheckerBuilder<T>(booleanChecker));
            grid.Extenders.Add(booleanChecker);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default BooleanChecker extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> BooleanChecker<T>(this FlexGridBuilder<T> gridBuilder)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            BooleanChecker<T> booleanChecker = new BooleanChecker<T>(grid);
            grid.Extenders.Add(booleanChecker);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default BooleanChecker extender in FlexGrid.
        /// </summary>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <returns></returns>
        public static FlexGridBuilder<object> BooleanChecker(this FlexGridBuilder<object> gridBuilder)
        {
            FlexGrid<object> grid = gridBuilder.Object;
            BooleanChecker<object> booleanChecker = new BooleanChecker<object>(grid);
            grid.Extenders.Add(booleanChecker);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the BooleanChecker extender in FlexGrid.
        /// </summary>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="booleanCheckerBuilder">the specified BooleanChecker builder</param>
        /// <returns></returns>
        public static FlexGridBuilder<object> BooleanChecker(this FlexGridBuilder<object> gridBuilder, Action<BooleanCheckerBuilder<object>> booleanCheckerBuilder)
        {
            FlexGrid<object> grid = gridBuilder.Object;
            BooleanChecker<object> booleanChecker = new BooleanChecker<object>(grid);
            booleanCheckerBuilder(new BooleanCheckerBuilder<object>(booleanChecker));
            grid.Extenders.Add(booleanChecker);
            return gridBuilder;
        }
    }
}
