﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the html builder for BooleanChecker.
    /// </summary>
    /// <typeparam name="T">The data item type.</typeparam>
    public partial class BooleanCheckerBuilder<T>
        : SelectorBaseBuilder<T, BooleanChecker<T>, BooleanCheckerBuilder<T>>
    {

        #region Ctor
        /// <summary>
        /// Create one BooleanCheckerBuilder instance.
        /// </summary>
        /// <param name="extender">The BooleanChecker extender.</param>
        public BooleanCheckerBuilder(BooleanChecker<T> extender)
            : base(extender)
        {
        }
        #endregion Ctor

    }
}
