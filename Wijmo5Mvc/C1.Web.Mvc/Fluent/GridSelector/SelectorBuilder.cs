﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the html builder for Selector.
    /// </summary>
    /// <typeparam name="T">The data item type.</typeparam>
    public partial class SelectorBuilder<T>
        : SelectorBaseBuilder<T, Selector<T>, SelectorBuilder<T>>
    {

        #region Ctor
        /// <summary>
        /// Create one SelectorBuilder instance.
        /// </summary>
        /// <param name="extender">The Selector extender.</param>
        public SelectorBuilder(Selector<T> extender)
            : base(extender)
        {
        }
        #endregion Ctor

    }
}
