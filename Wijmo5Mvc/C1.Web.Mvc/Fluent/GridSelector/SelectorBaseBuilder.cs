﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the html builder for SelectorBase.
    /// </summary>
    /// <typeparam name="T">The data item type.</typeparam>
    public abstract partial class SelectorBaseBuilder<T, TControl, TBuilder>
         : ExtenderBuilder<TControl, TBuilder>
    {

        #region Ctor
        /// <summary>
        /// Create one SelectorBaseBuilder instance.
        /// </summary>
        /// <param name="extender">The extender.</param>
        public SelectorBaseBuilder(TControl extender)
            : base(extender)
        {
        }
        #endregion Ctor

    }
}
