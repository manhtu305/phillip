﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the extensions for the RangeSelector extender.
    /// </summary>
    public static class RangeSelectorExtension
    {
        #region FlexChart
         /// <summary>
        /// Apply the RangeSelector extender in FlexChart.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="flexChartBuilder">the specified flexchart builder.</param>
        /// <param name="rangeSelectorBuilder">the specified rangeselector builder</param>
        /// <returns></returns>
        public static FlexChartBuilder<T> AddRangeSelector<T>(this FlexChartBuilder<T> flexChartBuilder, Action<RangeSelectorBuilder<T>> rangeSelectorBuilder)
        {
            FlexChart<T> flexChart = flexChartBuilder.Object;
            RangeSelector<T> rangeSelector = new RangeSelector<T>(flexChart);
            rangeSelectorBuilder(new RangeSelectorBuilder<T>(rangeSelector));
            flexChart.Extenders.Add(rangeSelector);
            return flexChartBuilder;
        }

        /// <summary>
        /// Apply a default RangeSelector extender in FlexChart.
        /// </summary>
        /// <param name="flexChartBuilder">the specified flexchart builder.</param>
        /// <returns></returns>
        public static FlexChartBuilder<T> AddRangeSelector<T>(this FlexChartBuilder<T> flexChartBuilder)
        {
            FlexChart<T> flexChart = flexChartBuilder.Object;
            RangeSelector<T> rangeSelector = new RangeSelector<T>(flexChart);
            flexChart.Extenders.Add(rangeSelector);
            return flexChartBuilder;
        }
        #endregion

    }
}
