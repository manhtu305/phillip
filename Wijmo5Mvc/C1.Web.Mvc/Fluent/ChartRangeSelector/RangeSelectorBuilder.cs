﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Mvc.Chart;

namespace C1.Web.Mvc.Fluent
{
    public partial class RangeSelectorBuilder<T>
    {
        #region Ctor
        /// <summary>
        /// Create one FlexGridFilterBuilder instance.
        /// </summary>
        /// <param name="extender">The FlexGridFilter extender.</param>
        public RangeSelectorBuilder(RangeSelector<T> extender)
            : base(extender)
        {
        }
        #endregion Ctor
        
        /// <summary>
        /// Sets the Orientation property.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public RangeSelectorBuilder<T> Orientation(Orientation value)
        {
            Object.Orientation = value;
            return this;
        }
    }
}
