﻿using System;
using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    public partial class CellTemplateBuilder
    {
        /// <summary>
        /// Remove this method as it is obsoleted.
        /// </summary>
        /// <param name="value">The template content.</param>
        /// <returns>The cell template builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use TemplateId instead.")]
        public CellTemplateBuilder Template(string value)
        {
            Object.Template = value;
            return this;
        }

        /// <summary>
        /// Remove this method as it is obsoleted.
        /// </summary>
        /// <param name="value">The editor template content.</param>
        /// <returns>The cell template builder.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use EditTemplateId instead.")]
        public CellTemplateBuilder EditTemplate(string value)
        {
            Object.EditTemplate = value;
            return this;
        }

    }
}
