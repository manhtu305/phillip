﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    public partial class DataMapBuilder
    {
        #region CollectionViewService
        /// <summary>
        /// Sets ItemsSource by builder.
        /// </summary>
        /// <param name="itemsSourceBuild">The build action for setting ItemsSource</param>
        /// <returns>Current builder</returns>
        public DataMapBuilder Bind(Action<CollectionViewServiceBuilder<object>> itemsSourceBuild)
        {
            itemsSourceBuild(new CollectionViewServiceBuilder<object>(Object.GetDataSource<CollectionViewService<object>>()));
            return this;
        }

        /// <summary>
        /// Bind to a collection.
        /// </summary>
        /// <param name="sourceCollection">The source collection</param>
        /// <returns>Current builder</returns>
        public DataMapBuilder Bind(IEnumerable<object> sourceCollection)
        {
            return Bind(cvsb => cvsb.Bind(sourceCollection));
        }

        /// <summary>
        /// Sets the read action url.
        /// </summary>
        /// <param name="readActionUrl">The action url.</param>
        /// <returns>Current builder</returns>
        public DataMapBuilder Bind(string readActionUrl)
        {
            return Bind(cvsb => cvsb.Bind(readActionUrl));
        }
        #endregion  CollectionViewService

        #region ODataVirtualCollectionViewService
        /// <summary>
        /// Configurates <see cref="DataMap.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataVirtualCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public DataMapBuilder BindODataVirtualSource(Action<ODataVirtualCollectionViewServiceBuilder<object>> build)
        {
            build(new ODataVirtualCollectionViewServiceBuilder<object>(Object.GetDataSource<ODataVirtualCollectionViewService<object>>()));
            return this;
        }
        #endregion ODataVirtualCollectionViewService

        #region ODataCollectionViewService
        /// <summary>
        /// Configurates <see cref="DataMap.ItemsSource" />.
        /// Sets ItemsSource settings.
        /// </summary>
        /// <param name="build">An action to build an <see cref="ODataCollectionViewService{T}"/>.</param>
        /// <returns>Current builder.</returns>
        public DataMapBuilder BindODataSource(Action<ODataCollectionViewServiceBuilder<object>> build)
        {
            build(new ODataCollectionViewServiceBuilder<object>(Object.GetDataSource<ODataCollectionViewService<object>>()));
            return this;
        }
        #endregion ODataCollectionViewService
    }
}
