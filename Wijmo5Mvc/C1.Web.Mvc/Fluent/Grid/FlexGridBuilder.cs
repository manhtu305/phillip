﻿using System;
using C1.Web.Mvc.Grid;
using System.Drawing;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    public partial class HeaderTemplateCell
    {
        /// <summary>
        /// Set value for properties of cell.
        /// </summary>
        /// <param name="row">row position in header table.</param>
        /// <param name="col">column position in header table.</param>
        /// <param name="rowSpan">row span in header table.</param>
        /// <param name="colSpan">column span in header table.</param>
        /// <param name="title">Text will appear in this cell.</param>
        public HeaderTemplateCell Set(int row, int col, int rowSpan, int colSpan, string title)
        {
            Row = row;
            Col = col;
            RowSpan = rowSpan;
            ColSpan = colSpan;
            Title = title;

            return this;
        }
    }
}

namespace C1.Web.Mvc.Fluent
{
    public partial class FlexGridBaseBuilder<T, TControl, TBuilder>
        {
        /// <summary>
        /// Scrolls the grid to bring a specific cell into view.
        /// </summary>
        /// <param name="left">the scroll left of the grid's scrollbar.</param>
        /// <param name="top">the scroll top of the grid's scrollbar.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder ScrollIntoView(int left, int top)
        {
            Object.ScrollPosition = new Point(left, top);
            return this as TBuilder;
        }

        /// <summary>
        /// Selects a cell range and optionally scrolls it into view.
        /// </summary>
        /// <param name="row">Index of the first row in this range.</param>
        /// <param name="col">Index of the first column in this range.</param>
        /// <param name="row2">Index of the last row in this range.</param>
        /// <param name="col2">Index of the last column in this range.</param>
        /// <remarks>
        /// If row2 and col2 are not set or set to Null, it means the single cell specified by row and col will be selected.
        /// </remarks>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Select(int row, int col, int? row2, int? col2)
        {
            Object.Selection = new CellRange(row, col, row2 ?? row, col2 ?? col);
            return this as TBuilder;
        }

        /// <summary>
        /// Configure <see cref="FlexGridBase{T}.Columns"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Columns(Action<ListItemFactory<Column, ColumnBuilder>> builder)
        {
            builder(new ListItemFactory<Column, ColumnBuilder>(Object.Columns,
                () => new Column(Object.Helper), c => new ColumnBuilder(c)));
            return this as TBuilder;
        }

        /// <summary>
        /// Configure <see cref="FlexGridBase{T}.Columns"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder ColumnGroups(Action<ListItemFactory<Column, ColumnBuilder>> builder)
        {
            builder(new ListItemFactory<Column, ColumnBuilder>(Object.Columns,
                () => new Column(Object.Helper), c => new ColumnBuilder(c)));
            return this as TBuilder;
        }

        /// <summary>
        /// Add a extender.
        /// </summary>
        /// <remarks>
        /// This is no longer used, just remove it.  Please use specific extender instead.
        /// </remarks>
        /// <param name="extender">The specified extender.</param>
        /// <returns>Current builder.</returns>
        [Obsolete("This is no longer used, just remove it.  Please use specific extender instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual TBuilder AddExtender(Extender extender)
        {
            Object.Extenders.Add(extender);
            return this as TBuilder;
        }

        /// <summary>
        /// Configure <see cref="PropertyGroupDescription"/>.
        /// </summary>
        /// <param name="names">The property names used for grouping.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder GroupBy(params string[] names)
        {
            var propertyGroupDescriptions = names.Select(n => new PropertyGroupDescription { PropertyName = n });
            Object.AddGroupDescriptions(propertyGroupDescriptions);
            return this as TBuilder;
        }

        /// <summary>
        /// Configure <see cref="SortDescription"/>.
        /// </summary>
        /// <param name="names">Sort by these names.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder OrderBy(params string[] names)
        {
            Object.AddSortDescriptions(names.Select(n => new SortDescription { Property = n, Ascending = true }));
            return this as TBuilder;
        }

        /// <summary>
        /// Configure <see cref="SortDescription"/>.
        /// </summary>
        /// <param name="names">Sort by these names with descending.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder OrderByDescending(params string[] names)
        {
            Object.AddSortDescriptions(names.Select(n => new SortDescription { Property = n, Ascending = false }));
            return this as TBuilder;
        }

        /// <summary>
        /// Make ItemsSource pagable with specified page size.
        /// 0 means to disable paging.
        /// </summary>
        /// <param name="pageSize">The page size.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder PageSize(int pageSize)
        {
            Object.PageSize = pageSize;
            Bind(itemsSourceBuilder => itemsSourceBuilder.PageSize(pageSize));
            return this as TBuilder;
        }

        /// <summary>
        /// Sets whether to show a group row to display the aggregates in the column footers panel.
        /// </summary>
        /// <param name="value">A bool value indicates whether to show a group row.</param>
        /// <param name="rowHeaderText">The row header text of the group row. If it is not set, it will use the default value: a sigma character.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder ShowColumnFooters(bool value = true, string rowHeaderText = null)
        {
            Object.ShowColumnFooters = value;
            Object.ColumnFootersRowHeaderText = rowHeaderText;
            return this as TBuilder;
        }

        /// <summary>
        /// Sets the minimum number of rows, columns required to enable virtualization.
        /// </summary>
        /// <param name="numberRow">The minimum number of rows.</param>
        /// <param name="numberColumn">The minimum number of columns.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder VirtualizationThresholds(int numberRow, int numberColumn) {
            Object.VirtualizationThresholds[0] = numberRow;
            Object.VirtualizationThresholds[1] = numberColumn;
            return this as TBuilder;
        }

        /// <summary>
        /// This is the builder for Custom Header Template.
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public virtual TBuilder HeaderTemplate(Action<HeaderTemplateBuilder> builder)
        {
            if (builder == null)
            {
                Object.HeaderTemplate = null;
                return this as TBuilder;
            }
            Object.HeaderTemplate = new HeaderTemplate();
            Object.HeaderTemplate.Cells = new List<HeaderTemplateCell>();
            builder(new HeaderTemplateBuilder(Object.HeaderTemplate));

            if (Object.HeaderTemplate.Cells.Count != 0)
            {
                var cells = Object.HeaderTemplate.Cells;
                for (int i = 0; i < cells.Count - 1; ++i)
                {
                    for (int j = i + 1; j < cells.Count; ++j)
                    {
                        if(
                            cells[i].Col <= cells[j].Col + cells[j].ColSpan - 1 && cells[i].Col + cells[i].ColSpan - 1 >= cells[j].Col
                            &&
                            cells[i].Row <= cells[j].Row + cells[j].RowSpan - 1 && cells[i].Row + cells[i].RowSpan - 1 >= cells[j].Row
                          )
                                throw new ArgumentException(Localization.Resources.HeaderTemplateExceptionOverlapped);
                    }
                }
            }
            
            return this as TBuilder;
        }

        /// <summary>
        /// Static object that defines the default width for auto-generated grid columns based on their types.
        /// </summary>
        /// <param name="dataType">The dataType of columns.</param>
        /// <param name="width">The default width of columns with dataType.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder DefaultTypeWidth(DataType dataType, int width)
        {
            var key = ((int)dataType).ToString();
            var dtw = Object.DefaultTypeWidth;
            if (dtw.ContainsKey(key))
            {
                dtw[key] = width;
            }
            else
            {
                dtw.Add(key, width);
            }
            return this as TBuilder;
        }
    }
}


