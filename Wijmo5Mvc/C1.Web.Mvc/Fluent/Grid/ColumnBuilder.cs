﻿using System;
using C1.Web.Mvc.Grid;
using System.Drawing;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    public partial class ColumnBaseBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Configure <see cref="ColumnBase.Columns"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder Columns(Action<ListItemFactory<Column, ColumnBuilder>> builder)
        {
            builder(new ListItemFactory<Column, ColumnBuilder>(Object.Columns,
                () => new Column(Object.Helper), c => new ColumnBuilder(c)));
            return this as TBuilder;
        }

        /// <summary>
        /// Configure <see cref="ColumnBase.Columns"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public virtual TBuilder ColumnGroups(Action<ListItemFactory<Column, ColumnBuilder>> builder)
        {
            builder(new ListItemFactory<Column, ColumnBuilder>(Object.Columns,
                () => new Column(Object.Helper), c => new ColumnBuilder(c)));
            return this as TBuilder;
        }
    }
}


