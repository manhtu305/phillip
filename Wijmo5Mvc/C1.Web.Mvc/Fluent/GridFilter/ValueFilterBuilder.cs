﻿namespace C1.Web.Mvc.Fluent
{
    public partial class ValueFilterBuilder
    {
        /// <summary>
        /// Sets the UniqueValues property.
        /// </summary>
        /// <remarks>
        /// Gets or sets an array containing the unique values to be displayed on the list.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ValueFilterBuilder UniqueValues(params object[] value)
        {
            Object.UniqueValues = value;
            return this;
        }
    }
}
