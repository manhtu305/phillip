﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use FlexGridFilter extender.
    /// </summary>
    public static class FlexGridFilterExtension
    {
        /// <summary>
        /// Apply the FlexGridFilter extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="gridFilterBuilder">the specified flexgridfilter builder</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> Filterable<T>(this FlexGridBuilder<T> gridBuilder, Action<FlexGridFilterBuilder<T>> gridFilterBuilder)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            FlexGridFilter<T> gridFilter = new FlexGridFilter<T>(grid);
            gridFilterBuilder(new FlexGridFilterBuilder<T>(gridFilter));
            grid.Extenders.Add(gridFilter);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default FlexGridFilter extender in FlexGrid.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <returns></returns>
        public static FlexGridBuilder<T> Filterable<T>(this FlexGridBuilder<T> gridBuilder)
        {
            FlexGrid<T> grid = gridBuilder.Object;
            FlexGridFilter<T> gridFilter = new FlexGridFilter<T>(grid);
            grid.Extenders.Add(gridFilter);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the default FlexGridFilter extender in FlexGrid.
        /// </summary>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <returns></returns>
        public static FlexGridBuilder<object> Filterable(this FlexGridBuilder<object> gridBuilder)
        {
            FlexGrid<object> grid = gridBuilder.Object;
            FlexGridFilter<object> gridFilter = new FlexGridFilter<object>(grid);
            grid.Extenders.Add(gridFilter);
            return gridBuilder;
        }

        /// <summary>
        /// Apply the FlexGridFilter extender in FlexGrid.
        /// </summary>
        /// <param name="gridBuilder">the specified flexgrid builder.</param>
        /// <param name="gridFilterBuilder">the specified flexgridfilter builder</param>
        /// <returns></returns>
        public static FlexGridBuilder<object> Filterable(this FlexGridBuilder<object> gridBuilder, Action<FlexGridFilterBuilder<object>> gridFilterBuilder)
        {
            FlexGrid<object> grid = gridBuilder.Object;
            FlexGridFilter<object> gridFilter = new FlexGridFilter<object>(grid);
            gridFilterBuilder(new FlexGridFilterBuilder<object>(gridFilter));
            grid.Extenders.Add(gridFilter);
            return gridBuilder;
        }
    }
}
