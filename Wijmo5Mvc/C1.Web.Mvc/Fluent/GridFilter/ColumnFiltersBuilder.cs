﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a class used to build a strongly typed list of <see cref="C1.Web.Mvc.ColumnFilter"/>s.
    /// </summary>
    public class ColumnFiltersBuilder : ListItemFactory<ColumnFilter, ColumnFilterBuilder>
    {
        #region Ctor

        /// <summary>
        /// Initializes an instance of the <see cref="Fluent.ColumnFiltersBuilder"/> class 
        /// by using the specified collection.
        /// </summary>
        /// <param name="list">The specified collection</param>
        /// <param name="createItemBuilder">The function for creating item builder.</param>
        public ColumnFiltersBuilder(IList<ColumnFilter> list, Func<ColumnFilter, ColumnFilterBuilder> createItemBuilder)
            : base(list, createItemBuilder)
        {
        }

        /// <summary>
        /// Initializes an instance of the <see cref="Fluent.ColumnFiltersBuilder"/> class 
        /// by using the specified collection.
        /// </summary>
        /// <param name="list">The specified collection</param>
        /// <param name="createItem">The function used to return a <see cref="C1.Web.Mvc.ColumnFilter"/>.</param>
        /// <param name="createItemBuilder">The function for creating item builder.</param>
        public ColumnFiltersBuilder(IList<ColumnFilter> list, Func<ColumnFilter> createItem, Func<ColumnFilter, ColumnFilterBuilder> createItemBuilder)
            : base(list, createItem, createItemBuilder)
        {
        }

        #endregion Ctor

        /// <summary>
        /// Overrides to hidden the method.
        /// </summary>
        /// <param name="item">The TItem instance.</param>
        /// <returns>The factory instance</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override ListItemFactory<ColumnFilter, ColumnFilterBuilder> Add(ColumnFilter item)
        {
            throw new NotSupportedException();
        }
    }
}
