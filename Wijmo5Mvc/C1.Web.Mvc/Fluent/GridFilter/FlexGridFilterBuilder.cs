﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Defines the html builder for FlexGridFilter.
    /// </summary>
    /// <typeparam name="T">The data item type.</typeparam>
    public partial class FlexGridFilterBuilder<T> : ExtenderBuilder<FlexGridFilter<T>, FlexGridFilterBuilder<T>>
    {

        #region Ctor
        /// <summary>
        /// Create one FlexGridFilterBuilder instance.
        /// </summary>
        /// <param name="extender">The FlexGridFilter extender.</param>
        public FlexGridFilterBuilder(FlexGridFilter<T> extender)
            : base(extender)
        {
        }
        #endregion Ctor

        /// <summary>
        /// Gets or sets a bool value that indicates whether to show the filter editing buttons in the grid's column headers.
        /// </summary>
        /// <param name="value">The bool value.</param>
        /// <returns>Current builder</returns>
        public FlexGridFilterBuilder<T> ShowFilterIcons(bool value)
        {
            Object.ShowFilterIcons = value;
            return this;
        }
        /// <summary>
        /// Configurates <see cref="ValueFilter.ExclusiveValueSearch" />.
        /// Sets a value that determines whether the filter should include only values selected by the @see:ValueFilter.filterText property.
        /// </summary>
        /// <remarks>
        /// This property is set to true by default, which matches Excel's behavior.
        /// </remarks>
        /// <param name="value">The value.</param>
        /// <returns>Current builder.</returns>
        public FlexGridFilterBuilder<T> ExclusiveValueSearch(bool value)
        {
          Object.ExclusiveValueSearch = value;
          return this;
        }

    /// <summary>
    /// Sets the default filter type to use.
    /// </summary>
    /// <param name="type">The flexgrid's filter type</param>
    /// <returns>Current builder</returns>
    public FlexGridFilterBuilder<T> DefaultFilterType(FilterType type)
        {
            Object.DefaultFilterType = type;
            return this;
        }

        /// <summary>
        /// Sets a dictionary to map the column and its FilterType.
        /// </summary>
        /// <param name="dict">The dictionary which is mapping the column and its FilterType</param>
        /// <returns>Current builder</returns>
        [Obsolete("Use ColumnFilters instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public FlexGridFilterBuilder<T> ColumnFilterTypes(IDictionary<string, FilterType> dict)
        {
            Object.SetColumnFilters(dict);
            return this;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filter editor should include sort buttons.
        /// </summary>
        /// <param name="value">The bool value.</param>
        /// <returns>Current builder</returns>
        public FlexGridFilterBuilder<T> ShowSortButtons(bool value)
        {
            Object.ShowSortButtons = value;
            return this;
        }

        /// <summary>
        /// Sets a dictionary to map the column and its FilterType.
        /// </summary>
        /// <param name="filterTypes">The dictionary which is mapping the column and its FilterType</param>
        /// <returns>Current builder</returns>
        [Obsolete("Use ColumnFilters instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public FlexGridFilterBuilder<T> ColumnFilterTypes(object filterTypes)
        {
            IDictionary<string, FilterType> dictionary = new Dictionary<string, FilterType>();
            if (filterTypes != null)
            {

                var pdCollection =
#if ASPNETCORE
                    filterTypes.GetType().GetProperties();
#else
                    TypeDescriptor.GetProperties(filterTypes).Cast<PropertyDescriptor>();
#endif

                foreach (var pd in pdCollection)
                {
                    var value = (FilterType)pd.GetValue(filterTypes);
                    dictionary.Add(pd.Name, value);
                }
            }

            Object.SetColumnFilters(dictionary);
            return this;
        }

        /// <summary>
        /// Sets the OnClientFilterChanging property.
        /// </summary>
        /// <remarks>
        /// Occurs when a column filter is about to be edited by the user.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public FlexGridFilterBuilder<T> OnClientFilterChanging(string value)
        {
            Object.OnClientFilterChanging = value;
            return this;
        }

        /// <summary>
        /// Sets the OnClientFilterChanged property.
        /// </summary>
        /// <remarks>
        /// Occurs after a column filter has been edited by the user.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public FlexGridFilterBuilder<T> OnClientFilterChanged(string value)
        {
            Object.OnClientFilterChanged = value;
            return this;
        }

        /// <summary>
        /// Sets the OnClientFilterApplied property.
        /// </summary>
        /// <remarks>
        /// Occurs after the filter is applied.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public FlexGridFilterBuilder<T> OnClientFilterApplied(string value)
        {
            Object.OnClientFilterApplied = value;
            return this;
        }

        /// <summary>
        /// Sets an array containing the names or bindings of the columns that should have filters.
        /// </summary>
        /// <param name="columns">The array indicates which columns should be filterable</param>
        /// <returns>Current builder</returns>
        [Obsolete("Use ColumnFilters instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public FlexGridFilterBuilder<T> FilterColumns(params string[] columns)
        {
            Object.SetColumnFilters(columns);
            return this;
        }

        /// <summary>
        /// Sets the ColumnFilters.
        /// </summary>
        /// <param name="builder">The builder action</param>
        /// <returns>Current builder</returns>
        public FlexGridFilterBuilder<T> ColumnFilters(Action<ColumnFiltersBuilder> builder)
        {
            builder(new ColumnFiltersBuilder(Object.ColumnFilters,
                () => new ColumnFilter(Object.Helper), c => new ColumnFilterBuilder(c)));
            return this;
        }
    }
}
