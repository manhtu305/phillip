﻿using System;

namespace C1.Web.Mvc.Fluent
{
    partial class DashboardLayoutBuilder
    {
        /// <summary>
        /// Configurates <see cref="DashboardLayout.Layout" />.
        /// Attaches a <see cref="FlowLayout" /> to the dashboard control.
        /// </summary>
        /// <param name="builder">The builder to configure the layout.</param>
        /// <returns>Current builder.</returns>
        public DashboardLayoutBuilder AttachFlowLayout(Action<FlowLayoutBuilder> builder)
        {
            builder(new FlowLayoutBuilder(DashboardLayout.GetLayout<FlowLayout>(Object, Object.Helper)));
            return this;
        }

        /// <summary>
        /// Configurates <see cref="DashboardLayout.Layout" />.
        /// Attaches a <see cref="SplitLayout" /> to the dashboard control.
        /// </summary>
        /// <param name="builder">The builder to configure the layout.</param>
        /// <returns>Current builder.</returns>
        public DashboardLayoutBuilder AttachSplitLayout(Action<SplitLayoutBuilder> builder)
        {
            builder(new SplitLayoutBuilder(DashboardLayout.GetLayout<SplitLayout>(Object, Object.Helper)));
            return this;
        }

        /// <summary>
        /// Configurates <see cref="DashboardLayout.Layout" />.
        /// Attaches a <see cref="AutoGridLayout" /> to the dashboard control.
        /// </summary>
        /// <param name="builder">The builder to configure the layout.</param>
        /// <returns>Current builder.</returns>
        public DashboardLayoutBuilder AttachAutoGridLayout(Action<AutoGridLayoutBuilder> builder)
        {
            builder(new AutoGridLayoutBuilder(DashboardLayout.GetLayout<AutoGridLayout>(Object, Object.Helper)));
            return this;
        }

        /// <summary>
        /// Configurates <see cref="DashboardLayout.Layout" />.
        /// Attaches a <see cref="ManualGridLayout" /> to the dashboard control.
        /// </summary>
        /// <param name="builder">The builder to configure the layout.</param>
        /// <returns>Current builder.</returns>
        public DashboardLayoutBuilder AttachManualGridLayout(Action<ManualGridLayoutBuilder> builder)
        {
            builder(new ManualGridLayoutBuilder(DashboardLayout.GetLayout<ManualGridLayout>(Object, Object.Helper)));
            return this;
        }
    }
}
