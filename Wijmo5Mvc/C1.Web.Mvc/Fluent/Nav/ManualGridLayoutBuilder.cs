﻿using System;

namespace C1.Web.Mvc.Fluent
{
    partial class ManualGridLayoutBuilder
    {
        /// <summary>
        /// Configure <see cref="ManualGridLayout.Items"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public ManualGridLayoutBuilder Items(Action<ListItemFactory<ManualGridGroup, ManualGridGroupBuilder>> builder)
        {
            builder(new ListItemFactory<ManualGridGroup, ManualGridGroupBuilder>(Object.Items,
                () => new ManualGridGroup(), c => new ManualGridGroupBuilder(c)));
            return this;
        }

        /// <summary>
        /// Creates one <see cref="ManualGridLayoutBuilder" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="ManualGridLayout" /> object to be configurated.</param>
        public ManualGridLayoutBuilder(ManualGridLayout component) : base(component)
        {
        }
    }

    partial class ManualGridGroupBuilder
    {
        /// <summary>
        /// Configure <see cref="ManualGridGroup.Children"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public ManualGridGroupBuilder Children(Action<ListItemFactory<ManualGridTile, ManualGridTileBuilder>> builder)
        {
            builder(new ListItemFactory<ManualGridTile, ManualGridTileBuilder>(Object.Children,
                () => new ManualGridTile(), c => new ManualGridTileBuilder(c)));
            return this;
        }
    }
}
