﻿namespace C1.Web.Mvc.Fluent
{
    partial class LayoutBaseBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Creates one <see cref="LayoutBaseBuilder{TControl, TBuilder}"/> instance to configurate <paramref name="service"/>.
        /// </summary>
        /// <param name="service">The <see cref="Service"/> object to be configurated.</param>
        protected LayoutBaseBuilder(TControl service)
            : base(service)
        {
        }
    }
}
