﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    partial class SplitLayoutBuilder
    {
        /// <summary>
        /// Configure <see cref="SplitLayout.Items"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public SplitLayoutBuilder Items(Action<SplitLayoutItemFactoryBuilder> builder)
        {
            builder(new SplitLayoutItemFactoryBuilder(Object.Items));
            return this;
        }

        /// <summary>
        /// Creates one <see cref="SplitLayoutBuilder" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="SplitLayout" /> object to be configurated.</param>
        public SplitLayoutBuilder(SplitLayout component) : base(component)
        {
        }
    }

    partial class SplitGroupBuilder
    {
        /// <summary>
        /// Configure <see cref="SplitGroup.Children"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public SplitGroupBuilder Children(Action<SplitLayoutItemFactoryBuilder> builder)
        {
            builder(new SplitLayoutItemFactoryBuilder(Object.Children));
            return this;
        }
    }

    /// <summary>
    /// Defines a builder to generate the items in the split layout.
    /// </summary>
    public class SplitLayoutItemFactoryBuilder
        : BaseBuilder<IList<ISplitLayoutItem>, SplitLayoutItemFactoryBuilder>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SplitLayoutItemFactoryBuilder"/> class.
        /// </summary>
        /// <param name="obj">The object.</param>
        public SplitLayoutItemFactoryBuilder(IList<ISplitLayoutItem> obj) 
            : base(obj)
        {
        }

        #region Add Tile
        /// <summary>
        /// Adds a <see cref="SplitTile"/> instance.
        /// </summary>
        /// <param name="builder">The builder to specify the tile to be added.</param>
        /// <returns>The current builder.</returns>
        public SplitLayoutItemFactoryBuilder AddTile(Action<SplitTileBuilder> builder)
        {
            builder(new SplitTileBuilder(AddNewTile()));
            return this;
        }

        /// <summary>
        /// Adds a <see cref="SplitTile"/> instance.
        /// </summary>
        /// <returns>A <see cref="SplitTileBuilder"/> builder to specify the tile to be added.</returns>
        public SplitTileBuilder AddTile()
        {
            return new SplitTileBuilder(AddNewTile());
        }
        private SplitTile AddNewTile()
        {
            var tile = new SplitTile();
            Object.Add(tile);
            return tile;
        }
        #endregion Add Tile

        #region Add Group
        /// <summary>
        /// Adds a <see cref="SplitGroup"/> instance.
        /// </summary>
        /// <param name="builder">The builder to specify the group to be added.</param>
        /// <returns>The current builder.</returns>
        public SplitLayoutItemFactoryBuilder AddGroup(Action<SplitGroupBuilder> builder)
        {
            builder(new SplitGroupBuilder(AddNewGroup()));
            return this;
        }

        /// <summary>
        /// Adds a <see cref="SplitGroup"/> instance.
        /// </summary>
        /// <returns>A <see cref="SplitGroupBuilder"/> builder to specify the group to be added.</returns>
        public SplitGroupBuilder AddGroup()
        {
            return new SplitGroupBuilder(AddNewGroup());
        }
        private SplitGroup AddNewGroup()
        {
            var group = new SplitGroup();
            Object.Add(group);
            return group;
        }
        #endregion Add Group
    }
}
