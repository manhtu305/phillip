﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    partial class TreeViewBuilder
    {
        /// <summary>
        ///     Sets the ChildItemsPath property.
        /// </summary>
        /// <remarks>
        ///     Gets or sets the name of the property (or properties) that contains the child items for each node. When it is not set, ['items'] will be used.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TreeViewBuilder ChildItemsPath(params string[] value)
        {
            Object.ChildItemsPath = value;
            return this;
        }

        /// <summary>
        ///     Sets the DisplayMemberPath property.
        /// </summary>
        /// <remarks>
        ///     Gets or sets the name of the property (or properties) to use as the visual representation of the nodes. When it is not set, ['header'] will be used.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TreeViewBuilder DisplayMemberPath(params string[] value)
        {
            Object.DisplayMemberPath = value;
            return this;
        }

        /// <summary>
        ///     Sets the ImageMemberPath property.
        /// </summary>
        /// <remarks>
        ///     Gets or sets the name of the property (or properties) to use as a source of images for the nodes.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public TreeViewBuilder ImageMemberPath(params string[] value)
        {
            Object.ImageMemberPath = value;
            return this;
        }

        /// <summary>
        ///     Loads the nodes from the specified data.
        /// </summary>
        /// <param name="data">
        /// The nodes data.
        /// </param>
        /// <param name="lazyLoadActionUrl">
        /// URL of the load action for lazy nodes.
        /// It is optional.
        /// When it is provided, the <see cref="TreeView"/> control works in lazy loading mode.
        /// Otherwise, all the nodes will be loaded from data.
        /// </param>
        /// <returns>
        ///     A <see cref="TreeViewBuilder"/>.
        /// </returns>
        public TreeViewBuilder Bind(IEnumerable<object> data, string lazyLoadActionUrl = null)
        {
            Object.Source = data;
            Object.LazyLoadActionUrl = lazyLoadActionUrl;
            return this;
        }

        /// <summary>
        ///     Loads the nodes from the specified url.
        /// </summary>
        /// <param name="loadActionUrl"> URL of the load action. </param>
        /// <param name="lazyLoadActionUrl">
        /// URL of the load action for lazy nodes.
        /// It is optional.
        /// When it is provided, the <see cref="TreeView"/> control works in lazy loading mode.
        /// Otherwise, all the nodes will be loaded from data.
        /// </param>
        /// <returns>
        ///     A <see cref="TreeViewBuilder"/>.
        /// </returns>
        public TreeViewBuilder Bind(string loadActionUrl, string lazyLoadActionUrl = null)
        {
            Object.LoadActionUrl = loadActionUrl;
            Object.LazyLoadActionUrl = lazyLoadActionUrl;
            return this;
        }

        #region CheckedMemberPath 
        /// <summary>
        /// Set the <see cref="TreeView.CheckedMemberPath" /> property.
        /// Gets or sets the name of the property (or properties) to bind  to the node's checked state.
        /// </summary>
        /// <param name="value">The value.</param>
        /// </param>
        /// <returns>
        ///     A <see cref="TreeViewBuilder"/>.
        /// </returns>
        public TreeViewBuilder CheckedMemberPath(params string[] value)
        {
            Object.CheckedMemberPath = value;
            return this;
        }
        #endregion CheckedMemberPath
    }
}
