﻿using System;

namespace C1.Web.Mvc.Fluent
{
    partial class AutoGridLayoutBuilder
    {
        /// <summary>
        /// Configure <see cref="AutoGridLayout.Items"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public AutoGridLayoutBuilder Items(Action<ListItemFactory<AutoGridGroup, AutoGridGroupBuilder>> builder)
        {
            builder(new ListItemFactory<AutoGridGroup, AutoGridGroupBuilder>(Object.Items,
                () => new AutoGridGroup(), c => new AutoGridGroupBuilder(c)));
            return this;
        }

        /// <summary>
        /// Creates one <see cref="AutoGridLayoutBuilder" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="AutoGridLayout" /> object to be configurated.</param>
        public AutoGridLayoutBuilder(AutoGridLayout component) : base(component)
        {
        }
    }

    partial class AutoGridGroupBuilder
    {
        /// <summary>
        /// Configure <see cref="AutoGridGroup.Children"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public AutoGridGroupBuilder Children(Action<ListItemFactory<AutoGridTile, AutoGridTileBuilder>> builder)
        {
            builder(new ListItemFactory<AutoGridTile, AutoGridTileBuilder>(Object.Children,
                () => new AutoGridTile(), c => new AutoGridTileBuilder(c)));
            return this;
        }
    }
}
