﻿using System;

namespace C1.Web.Mvc.Fluent
{
    partial class TabPanelBuilder
    {
        /// <summary>
        /// Sets the Tabs property.
        /// </summary>
        /// <param name="build">The build action.</param>
        /// <returns>Current builder.</returns>
        public virtual TabPanelBuilder Tabs(Action<ListItemFactory<Tab, TabBuilder>> build)
        {
            build(new ListItemFactory<Tab, TabBuilder>(Object.Tabs,
                () => new Tab(), t => new TabBuilder(t)));
            return this;
        }
    }
}
