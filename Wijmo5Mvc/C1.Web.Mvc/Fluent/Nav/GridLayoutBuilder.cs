﻿namespace C1.Web.Mvc.Fluent
{
    partial class GridLayoutBuilder<TControl, TBuilder>
    {
        /// <summary>
        /// Creates one <see cref="GridLayoutBuilder{TControl, TBuilder}"/> instance to configurate <paramref name="service"/>.
        /// </summary>
        /// <param name="service">The <see cref="Service"/> object to be configurated.</param>
        protected GridLayoutBuilder(TControl service)
            : base(service)
        {
        }
    }
}
