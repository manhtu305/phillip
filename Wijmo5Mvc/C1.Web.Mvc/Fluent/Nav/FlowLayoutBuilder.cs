﻿using System;

namespace C1.Web.Mvc.Fluent
{
    partial class FlowLayoutBuilder
    {
        /// <summary>
        /// Configure <see cref="FlowLayout.Items"/>.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder.</returns>
        public FlowLayoutBuilder Items(Action<ListItemFactory<FlowTile, FlowTileBuilder>> builder)
        {
            builder(new ListItemFactory<FlowTile, FlowTileBuilder>(Object.Items,
                () => new FlowTile(), c => new FlowTileBuilder(c)));
            return this;
        }

        /// <summary>
        /// Creates one <see cref="FlowLayoutBuilder" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="FlowLayout" /> object to be configurated.</param>
        public FlowLayoutBuilder(FlowLayout component) : base(component)
        {
        }
    }
}
