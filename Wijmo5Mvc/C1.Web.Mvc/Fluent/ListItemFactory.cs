﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a class used to build a strongly typed list of objects.
    /// </summary>
    /// <typeparam name="TItem">The specified type of the item</typeparam>
    /// <typeparam name="TItemBuilder">The specified type of the item builder.</typeparam>
    public class ListItemFactory<TItem, TItemBuilder> : BaseBuilder<IList<TItem>, ListItemFactory<TItem, TItemBuilder>>
        where TItem : class, new()
        where TItemBuilder : BaseBuilder<TItem, TItemBuilder>
    {
        private readonly Func<TItem> _createItem;
        private readonly Func<TItem, TItemBuilder> _createItemBuilder;

        #region Ctor

        /// <summary>
        /// Initializes an instance of the <see cref="C1.Web.Mvc.Fluent.ListItemFactory{TItem, TItemBuilder}"/> class 
        /// by using the specified collection.
        /// </summary>
        /// <param name="list">The specified collection.</param>
        /// <param name="createItemBuilder">The function for creating item builder.</param>
        public ListItemFactory(IList<TItem> list, Func<TItem, TItemBuilder> createItemBuilder)
            : base(list)
        {
            if (createItemBuilder == null)
            {
                throw new ArgumentNullException("createItemBuilder");
            }

            _createItemBuilder = createItemBuilder;
        }

        /// <summary>
        /// Initializes an instance of the <see cref="C1.Web.Mvc.Fluent.ListItemFactory{TItem, TItemBuilder}"/> class 
        /// by using the specified collection.
        /// </summary>
        /// <param name="list">The specified collection.</param>
        /// <param name="createItem">The function to create an item.</param>
        /// <param name="createItemBuilder">The builder to set an item.</param>
        public ListItemFactory(IList<TItem> list, Func<TItem> createItem, Func<TItem, TItemBuilder> createItemBuilder)
            : base(list)
        {
            if (createItem == null)
            {
                throw new ArgumentNullException("createItem");
            }
            if (createItemBuilder == null)
            {
                throw new ArgumentNullException("createItemBuilder");
            }

            _createItem = createItem;
            _createItemBuilder = createItemBuilder;
        }

        #endregion Ctor

        #region Methods
        /// <summary>
        /// Add a default TItem instance to the collection.
        /// </summary>
        /// <returns>The item builder</returns>
        public TItemBuilder Add()
        {
            TItem item;
            if (_createItem == null)
            {
                item = new TItem();
            }
            else
            {
                item = _createItem();
            }
            Object.Add(item);
            return _createItemBuilder(item);
        }

        /// <summary>
        /// Add a TItem instance updated by the specified action to the collection.
        /// </summary>
        /// <param name="build">The specified action to update TItem instance.</param>
        /// <returns>The factory instance</returns>
        public ListItemFactory<TItem, TItemBuilder> Add(Action<TItemBuilder> build)
        {
            build(Add());
            return this;
        }

        /// <summary>
        /// Add a TItem instance to the collection.
        /// </summary>
        /// <param name="item">The TItem instance.</param>
        /// <returns>The factory instance</returns>
        public virtual ListItemFactory<TItem, TItemBuilder> Add(TItem item)
        {
            Object.Add(item);
            return this;
        }
        #endregion Methods
    }
}
