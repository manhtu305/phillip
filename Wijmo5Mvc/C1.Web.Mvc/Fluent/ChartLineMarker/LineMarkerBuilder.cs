﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Mvc.Chart;

namespace C1.Web.Mvc.Fluent
{
    public partial class LineMarkerBuilder<T>
    {
        #region Ctor
        /// <summary>
        /// Create one FlexGridFilterBuilder instance.
        /// </summary>
        /// <param name="extender">The FlexGridFilter extender.</param>
        public LineMarkerBuilder(LineMarker<T> extender)
            : base(extender)
        {
        }
        #endregion Ctor
        
        /// <summary>
        /// Sets the Alignment property.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public LineMarkerBuilder<T> Alignment(LineMarkerAlignment value)
        {
            Object.Alignment = value;
            return this;
        }

        /// <summary>
        /// Sets the Lines property.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public LineMarkerBuilder<T> Lines(LineMarkerLines value)
        {
            Object.Lines = value;
            return this;
        }

        /// <summary>
        /// Sets the Interaction property.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public LineMarkerBuilder<T> Interaction(LineMarkerInteraction value)
        {
            Object.Interaction = value;
            return this;
        }
    }
}
