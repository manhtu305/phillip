﻿using System;

namespace C1.Web.Mvc.Fluent
{
    /// <summary>
    /// Define a static class to add the extension methods 
    /// for all the controls which can use FlexGridFilter extender.
    /// </summary>
    public static class LineMarkerExtension
    {

        #region FlexChart
        /// <summary>
        /// Apply the LineMarker extender in FlexChart.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="flexChartBuilder">the specified flexchart builder.</param>
        /// <param name="lineMarkerBuilder">the specified linemarker builder</param>
        /// <returns></returns>
        public static FlexChartBuilder<T> AddLineMarker<T>(this FlexChartBuilder<T> flexChartBuilder, Action<LineMarkerBuilder<T>> lineMarkerBuilder)
        {
            FlexChart<T> flexChart = flexChartBuilder.Object;
            LineMarker<T> lineMarker = new LineMarker<T>(flexChart);
            lineMarkerBuilder(new LineMarkerBuilder<T>(lineMarker));
            flexChart.Extenders.Add(lineMarker);
            return flexChartBuilder;
        }

        /// <summary>
        /// Apply a default LineMarker extender in FlexChart.
        /// </summary>
        /// <param name="flexChartBuilder">the specified flexchart builder.</param>
        /// <returns></returns>
        public static FlexChartBuilder<T> AddLineMarker<T>(this FlexChartBuilder<T> flexChartBuilder)
        {
            FlexChart<T> flexChart = flexChartBuilder.Object;
            LineMarker<T> lineMarker = new LineMarker<T>(flexChart);
            flexChart.Extenders.Add(lineMarker);
            return flexChartBuilder;
        }
        #endregion

    }
}
