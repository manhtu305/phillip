﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(Definitions.Gauge))]
    public partial class Gauge
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Gauge;
            }
        }
    }
}
