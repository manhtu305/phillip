﻿namespace C1.Web.Mvc
{
    public partial class RangeSelector<T>
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.ChartInteraction;
            }
        }
    }
}
