﻿using System;
using System.Collections.Generic;
using C1.Web.Mvc.CollectionView;
using C1.Web.Mvc.GridFilter;
using C1.Web.Mvc.Grid;
using C1.Web.Mvc.Localization;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Supports the read and write processing of the request from the client-side CollectionView.
    /// </summary>
    public static class CollectionViewHelper
    {
        /// <summary>
        /// Reads data from the request and the source collection.
        /// </summary>
        /// <typeparam name="T">The type of data record</typeparam>
        /// <param name="requestData">The request data</param>
        /// <param name="sourceCollection">The source collection</param>
        /// <param name="dataMapItemsSourceGetter">The function to get the items source for the specified column's data map.</param>
        /// <returns>The response data</returns>
        public static CollectionViewResponse<T> Read<T>(CollectionViewRequest<T> requestData, IEnumerable<T> sourceCollection, Func<ColumnInfo, IEnumerable<object>> dataMapItemsSourceGetter = null)
        {
            if (requestData.Command != CommandType.Read)
            {
                throw new ArgumentException(string.Format(Resources.CollectionViewInvalidCommand, "Read"));
            }
            ICollectionView<T> collectionView = new ReadOnlyCollectionView<T>(sourceCollection);
            ProcessExtraRequestData<T>(collectionView, requestData, dataMapItemsSourceGetter);
            return new CollectionViewProcessor<T>(collectionView).Process(requestData);
        }

        /// <summary>
        /// Processes the extra request data by the extender helpers.
        /// </summary>
        private static void ProcessExtraRequestData<T>(ICollectionView<T> collectionView, CollectionViewRequest<T> requestData, Func<ColumnInfo, IEnumerable<object>> dataMapItemsSourceGetter)
        {
            if (requestData == null || requestData.ExtraRequestData == null)
            {
                return;
            }

            //FlexGridFilter
            FlexGridFilterHelper.ExecuteWithRequest<T>(collectionView, requestData.ExtraRequestData);

            //FlexGridSort
            if (dataMapItemsSourceGetter != null)
            {
                SortHelper.ExecuteWithRequest<T>(requestData, dataMapItemsSourceGetter);
            }
        }

        /// <summary>
        /// Edits data accroding to the request and the write action.
        /// </summary>
        /// <typeparam name="T">The type of data record</typeparam>
        /// <param name="requestData">The request data</param>
        /// <param name="editAction">The edit action</param>
        /// <param name="read">The read action provides the newest data.</param>
        /// <returns>The response data</returns>
        public static CollectionViewResponse<T> Edit<T>(CollectionViewEditRequest<T> requestData, Func<T, CollectionViewItemResult<T>> editAction, Func<IEnumerable<T>> read)
        {
            if (read == null)
            {
                throw new ArgumentNullException("read");
            }

            var collectionView = new CustomEditableCollectionView<T>();

            switch (requestData.Command)
            {
                case CommandType.Create:
                    collectionView.CustomCreate = editAction;
                    break;
                case CommandType.Delete:
                    collectionView.CustomDelete = editAction;
                    break;
                case CommandType.Update:
                    collectionView.CustomUpdate = editAction;
                    break;
                default:
                    throw new ArgumentException(string.Format(Resources.CollectionViewInvalidCommand, Resources.CollectionViewEditCommand));
            }

            var editResult = new CollectionViewProcessor<T>(collectionView).Process(requestData);
            requestData.Command = CommandType.Read;
            var result = Read<T>(requestData, read());
            result.OperatedItemResults = editResult.OperatedItemResults;
            if (!result.Success)
            {
                result.Error += ";";
            }
            result.Error += editResult.Error;
            result.Success = result.Success && editResult.Success;
            return result;
        }

        /// <summary>
        /// Edits batch data accroding to the request and the edit action.
        /// </summary>
        /// <typeparam name="T">The type of data record</typeparam>
        /// <param name="requestData">The request data</param>
        /// <param name="editAction">The edit action</param>
        /// <param name="read">The read action provides the newest data.</param>
        /// <returns>The response data</returns>
        public static CollectionViewResponse<T> BatchEdit<T>(CollectionViewBatchEditRequest<T> requestData, Func<BatchOperatingData<T>, CollectionViewResponse<T>> editAction, Func<IEnumerable<T>> read)
        {
            if (requestData.Command != CommandType.BatchEdit)
            {
                throw new ArgumentException(string.Format(Resources.CollectionViewInvalidCommand, "BatchEdit"));
            }
            var collectionView = new CustomEditableCollectionView<T>();
            collectionView.CustomBatchEdit = editAction;
            var batchEditResult = new CollectionViewProcessor<T>(collectionView).Process(requestData);
            requestData.Command = CommandType.Read;
            var result = Read<T>(requestData, read());
            result.OperatedItemResults = batchEditResult.OperatedItemResults;
            if (!result.Success)
            {
                result.Error += ";";
            }
            result.Error += batchEditResult.Error;
            result.Success = result.Success && batchEditResult.Success;
            return result;
        }
    }
}
