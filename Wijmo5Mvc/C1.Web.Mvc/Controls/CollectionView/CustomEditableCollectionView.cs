﻿using C1.Web.Mvc.Localization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc
{
    /// <summary>
    /// A customizing editable collection view.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class CustomEditableCollectionView<T> : ReadOnlyCollectionView<T>, IEditablePagedCollectionView<T>
    {
        public CustomEditableCollectionView(IEnumerable<T> sourceCollection = null)
            : base(sourceCollection)
        {
        }

        public Func<T, CollectionViewItemResult<T>> CustomCreate { get; set; }

        public Func<T, CollectionViewItemResult<T>> CustomUpdate { get; set; }

        public Func<T, CollectionViewItemResult<T>> CustomDelete { get; set; }

        public Func<BatchOperatingData<T>, CollectionViewResponse<T>> CustomBatchEdit { get; set; }

        private static CollectionViewItemResult<T> CreateNoCustomActionResult(string actionName, T item)
        {
            return new CollectionViewItemResult<T>
            {
                Data = item,
                Error = string.Format(Resources.CollectionViewHandleActionTip, actionName)
            };
        }

        public CollectionViewItemResult<T> Create(T item)
        {
            return CustomCreate == null ? CreateNoCustomActionResult("CustomCreate", item) : CustomCreate(item);
        }

        public CollectionViewItemResult<T> Update(T item)
        {
            return CustomUpdate == null ? CreateNoCustomActionResult("CustomUpdate", item) : CustomUpdate(item);
        }

        public CollectionViewItemResult<T> Delete(T item)
        {
            return CustomDelete == null ? CreateNoCustomActionResult("CustomDelete", item) : CustomDelete(item);
        }

        public CollectionViewResponse<T> BatchEdit(BatchOperatingData<T> batchData)
        {
            if (CustomBatchEdit == null)
            {
                var result = new CollectionViewResponse<T>();
                IList<T> items = new List<T>();
                result.OperatedItemResults = items.Union(batchData.ItemsUpdated)
                    .Union(batchData.ItemsCreated)
                    .Union(batchData.ItemsDeleted)
                    .Select(item => CreateNoCustomActionResult("CustomBatchEdit", item));
                result.Error = string.Format(Resources.CollectionViewHandleActionTip, "CustomBatchEdit");
                result.Success = false;
                return result;
            }
            return CustomBatchEdit(batchData);
        }
    }
}