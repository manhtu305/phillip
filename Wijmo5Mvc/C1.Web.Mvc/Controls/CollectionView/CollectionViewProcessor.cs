﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using C1.Web.Mvc.CollectionView;
using C1.Web.Mvc.Localization;

namespace C1.Web.Mvc
{
    /// <summary>
    /// The class for processing the request data of CollectionView
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class CollectionViewProcessor<T>
    {
        private readonly ICollectionView<T> _collectionView;
        public CollectionViewProcessor(ICollectionView<T> collectionView)
        {
            if (collectionView == null)
            {
                throw new ArgumentNullException("collectionView");
            }

            _collectionView = collectionView;
        }

        public CollectionViewResponse<T> Process(CollectionViewRequest<T> requestData)
        {
            switch (requestData.Command)
            {
                case CommandType.Create:
                case CommandType.Update:
                case CommandType.Delete:
                    CollectionViewEditRequest<T> updateRequestData = requestData as CollectionViewEditRequest<T>;
                    return Edit(updateRequestData);
                case CommandType.Read:
                    return Read(requestData);
                case CommandType.BatchEdit:
                    CollectionViewBatchEditRequest<T> batchUpdatingData = requestData as CollectionViewBatchEditRequest<T>;
                    return BatchEdit(batchUpdatingData);
                default:
                    return new CollectionViewResponse<T> { Success = false, Error = Resources.CollectionViewUnknowCommand };
            }
        }

        private IList<object> GetColumnData(CollectionViewRequest<T> requestData)
        {
            var items = _collectionView.SourceCollection.ToList();
            var propertyInfo = typeof(T).GetNestedProperty(requestData.Column);
            var columnData = items.Select(s => 
            {
                var currentPropertyInfo = propertyInfo ?? s.GetType().GetNestedProperty(requestData.Column);
                return currentPropertyInfo == null ? null : currentPropertyInfo.GetValue(s);
            });

            if (requestData.Distinct)
            {
                columnData = columnData.Distinct();
            }

            return columnData.ToList();
        }

        private CollectionViewResponse<T> Read(CollectionViewRequest<T> requestData)
        {
            CollectionViewResponse<T> result;
            if (!string.IsNullOrEmpty(requestData.Column))
            {
                result = new CollectionViewResponse<T>
                {
                    ColumnData = GetColumnData(requestData)
                };
                return result;
            }
            var pagedCollectionView = _collectionView as IPagedCollectionView<T>;
            if (requestData.PageSize > 0)
            {
                if (pagedCollectionView == null)
                {
                    return new CollectionViewResponse<T> { Success = false, Error = Resources.CollectionViewPageNotSupported };
                }

                pagedCollectionView.PageSize = requestData.PageSize;
                pagedCollectionView.MoveToPage(requestData.PageIndex);
            }

            _collectionView.SortDescriptions.Clear();
            if (requestData.SortDescriptions != null)
            {
                foreach (var sortDescription in requestData.SortDescriptions)
                {
                    _collectionView.SortDescriptions.Add(sortDescription);
                }
            }

            if (requestData.PageSize == 0)
            {
                pagedCollectionView.Skip = requestData.Skip;
                pagedCollectionView.Top = requestData.Top;
            }

            _collectionView.Refresh();

            result = new CollectionViewResponse<T>
            {
                Items = _collectionView.ToArray()
            };

            if (pagedCollectionView != null)
            {
                result.PageIndex = pagedCollectionView.PageIndex;
                result.TotalItemCount = pagedCollectionView.TotalItemCount;
                if (requestData.PageSize == 0)
                {
                    result.Skip = pagedCollectionView.Skip;
                }
            }

            return result;
        }

        private CollectionViewResponse<T> Edit(CollectionViewEditRequest<T> requestData)
        {
            var editableCollectionView = _collectionView as IEditableCollectionView<T>;
            if (editableCollectionView == null)
            {
                return new CollectionViewResponse<T> { Success = false, Error = Resources.CollectionViewEditNotSupported };
            }

            Func<T, CollectionViewItemResult<T>> editAction = null;
            switch (requestData.Command)
            {
                case CommandType.Create:
                    editAction = editableCollectionView.Create;
                    break;
                case CommandType.Update:
                    editAction = editableCollectionView.Update;
                    break;
                case CommandType.Delete:
                    editAction = editableCollectionView.Delete;
                    break;
            }

            var itemResults = new List<CollectionViewItemResult<T>>();
            if (editAction != null)
            {
                itemResults.AddRange(requestData.OperatingItems.Select(item => editAction(item)));
            }
            return new CollectionViewResponse<T>
            {
                OperatedItemResults = itemResults
            };
        }

        private CollectionViewResponse<T> BatchEdit(CollectionViewBatchEditRequest<T> requestData)
        {
            var editableCollectionView = _collectionView as IEditableCollectionView<T>;
            if (editableCollectionView == null)
            {
                return new CollectionViewResponse<T> { Success = false, Error = Resources.CollectionViewBatchEditNotSupported };
            }
            return editableCollectionView.BatchEdit(requestData.OperatingItems);
        }
    }
}
