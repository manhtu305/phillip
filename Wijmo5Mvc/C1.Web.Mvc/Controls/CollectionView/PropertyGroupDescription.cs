﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Describes the grouping of items using a property name as the criterion.
    /// </summary>
    public partial class PropertyGroupDescription
    {
        /// <summary>
        /// Gets the client-side object's class name.
        /// </summary>
        public override string ClientClass
        {
            get { return "wijmo.collections.PropertyGroupDescription"; }
        }
    }
}
