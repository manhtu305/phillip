﻿using C1.Web.Mvc.Serialization;
using C1.JsonNet;

namespace C1.Web.Mvc
{
    partial class BaseODataCollectionViewService<T>
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.OData;
            }
        }

        /// <summary>
        /// Overrides to remove this attribute.
        /// </summary>
        [C1Ignore]
        public override string UniqueId
        {
            get
            {
                return base.UniqueId;
            }
        }

        /// <summary>
        /// Gets or sets the page size. 0 means to disable paging.
        /// </summary>
        [Json(1)]
        public override int PageSize
        {
            get
            {
                return base.PageSize;
            }
            set
            {
                base.PageSize = value;
            }
        }

        /// <summary>
        /// Overrides to add the process for the constructor parameters.
        /// </summary>
        internal override string GetSerializationOptions()
        {
            var opts = base.GetSerializationOptions();
            return ClientModule + ClientSubModule + "_ODataInitializer._getCtorOptions(" + opts + ")";
        }
    }
}
