﻿using C1.Web.Mvc.Serialization;
using C1.Web.Mvc.WebResources;
using C1.JsonNet;
#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    // CollectionViewService already implements IPagedCollectionView. 
    // But CollectionViewService will be serialized as IEnumberable during serialization.
    // So remove IPagedCollectionView temporarily here.
    [Scripts(typeof(Definitions.CollectionView))]
    partial class CollectionViewService<T>
#if !ASPNETCORE
    : ICallbackHandler
#endif
    {
#if !ASPNETCORE
        internal delegate void BeginningQueryDelegate(CollectionViewRequest<T> requestData);
        internal event BeginningQueryDelegate BeginningQuery;
#endif

        /// <summary>
        /// Forces to read data.
        /// </summary>
        public void Refresh()
        {
            _collectionView.Refresh();
        }

        /// <summary>
        /// Adds a new item.
        /// </summary>
        /// <param name="item">The item to be added.</param>
        /// <returns>A <see cref="CollectionViewItemResult{T}"/> indicates the result for the add operation.</returns>
        public CollectionViewItemResult<T> Create(T item)
        {
            return _collectionView.Create(item);
        }

        /// <summary>
        /// Deletes an item.
        /// </summary>
        /// <param name="item">The item to be deleted.</param>
        /// <returns>A <see cref="CollectionViewItemResult{T}"/> indicates the result for the delete operation.</returns>
        public CollectionViewItemResult<T> Delete(T item)
        {
            return _collectionView.Delete(item);
        }

        /// <summary>
        /// Updates an item.
        /// </summary>
        /// <param name="item">The item to be updated.</param>
        /// <returns>A <see cref="CollectionViewItemResult{T}"/> indicates the result for the update operation.</returns>
        public CollectionViewItemResult<T> Update(T item)
        {
            return _collectionView.Update(item);
        }
#if !ASPNETCORE
#region ICallbackHandler

        /// <summary>
        /// Process the callback request from client side.
        /// </summary>
        public void ProcessCallBack(CallbackManager cbm)
        {
            CollectionViewRequest<T> requestData;
            if (BatchEdit)
            {
                requestData = cbm.GetDeserializedData<CollectionViewBatchEditRequest<T>>();
            }
            else
            {
                requestData = cbm.GetDeserializedData<CollectionViewEditRequest<T>>();
            }
            if (requestData.ExtraRequestData != null)
            {
                onBeginningQuery(requestData);
            }
            var processor = new CollectionViewProcessor<T>(_collectionView);

            var result = processor.Process(requestData);
            cbm.WriteJson(result);
        }

        private void onBeginningQuery(CollectionViewRequest<T> requestData)
        {
            if (BeginningQuery != null)
            {
                BeginningQuery(requestData);
            }
        }

#endregion ICallbackHandler
#endif

        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Collections;
            }
        }

        internal override string ClientClass
        {
            get
            {
                return ClientModules.CallbackCollectionViewClass;
            }
        }
    }

    internal class ItemsSourceConverter : PascalCaseJsonConverter
    {
        protected override void WriteJson(JsonWriter writer, object value)
        {
            var txt = JsonConvertHelper.Serialize(value, false, writer.JsonSetting.IsJavascriptFormat, false, false);
            txt = Utility.QuoteJScriptString(txt);
            txt = string.Format("\"{0}\"", txt);
            writer.WriteRawValue(txt);
        }
    }
}