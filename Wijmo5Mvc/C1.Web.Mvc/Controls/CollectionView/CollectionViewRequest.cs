﻿using System.Collections;

namespace C1.Web.Mvc
{
    public partial class CollectionViewRequest<T>
    {
#if ASPNETCORE
        private System.Collections.Generic.Dictionary<string, object> _extraRequestData = new System.Collections.Generic.Dictionary<string, object>(System.StringComparer.OrdinalIgnoreCase);
#else
        private Hashtable _extraRequestData = System.Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable();
#endif

        /// <summary>
        /// Gets the extra request data.
        /// </summary>
        public
#if ASPNETCORE
        System.Collections.Generic.Dictionary<string, object>
#else
        System.Collections.Hashtable
#endif
        ExtraRequestData
        {
            get { return _extraRequestData; }
            set { _extraRequestData = value; }
        }
    }
}
