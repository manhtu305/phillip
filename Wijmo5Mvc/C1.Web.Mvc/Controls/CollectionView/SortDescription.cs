﻿using System;

namespace C1.Web.Mvc
{
    public partial class SortDescription
    {
        /// <summary>
        /// Gets or sets the sort converter used to sort mapped columns by display value.
        /// </summary>
        internal Func<object, object> SortConverter { get; set; }

        /// <summary>
        /// Gets or sets a value that determines whether null values should appear
        /// first or last when the collection is sorted(regardless of sort direction).
        /// </summary>
        public bool SortNullsFirst { get; set; }

        /// <summary>
        /// Gets or sets a value that determines how null values should be sorted.
        /// </summary>
        /// <remarks>
        /// This property is set to <b>Last</b> default, which causes null values 
        /// to appear last on the sorted collection, regardless of sort direction.
        /// This is also the default behavior in Excel.
        /// </remarks>
        public CollectionViewSortNulls SortNulls { get; set; }
    }
}
