﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace C1.Web.Mvc
{
    /// <summary>
    /// The CollectionView which cannot be edited.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class ReadOnlyCollectionView<T> : IPagedCollectionView<T>
    {
        private List<T> _items;
        private bool _requireReadDataSource = true;
        private int _totalItemCount;
        private int _pageIndex;

        public ReadOnlyCollectionView(IEnumerable<T> sourceCollection = null)
        {
            SourceCollection = sourceCollection;
            SortDescriptions = new List<SortDescription>();
            GroupDescriptions = new List<GroupDescription>();
        }

        public IEnumerable<T> SourceCollection { get; set; }
        public IList<SortDescription> SortDescriptions { get; private set; }
        public IList<GroupDescription> GroupDescriptions { get; private set; }

        public int PageIndex
        {
            get
            {
                EnsureReadDataSource();
                return _pageIndex;
            }
        }

        private int _skip;
        public int Skip
        {
            get 
            {
                return _skip; 
            }
            set 
            {
                _skip = Math.Max(0, value);
            }
        }

        public int? Top { get; set; }

        public int PageSize { get; set; }

        public Predicate<T> Filter { get; set; }

        public int TotalItemCount
        {
            get
            {
                EnsureReadDataSource();
                return _totalItemCount;
            }
        }

        public IEnumerable<T> Items
        {
            get
            {
                EnsureReadDataSource();
                return _items;
            }
        }

        public void MoveToPage(int pageIndex)
        {
            _pageIndex = pageIndex;
            Refresh();
        }

        public void Refresh()
        {
            _requireReadDataSource = true;
        }

        private void EnsureReadDataSource()
        {
            if (!_requireReadDataSource)
            {
                return;
            }

            if (_items == null)
            {
                _items = new List<T>();
            }
            else
            {
                _items.Clear();
            }

            _totalItemCount = 0;

            if (SourceCollection != null)
            {
                IEnumerable<T> items = SourceCollection.ToList();
                items = DoFilter(items);
                _totalItemCount = items.Count();
                items = DoSort(items);
                items = DoPage(items);
                items = DoPartialSelect(items);
                _items.AddRange(items);
            }

            _requireReadDataSource = false;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        private IEnumerable<T> DoPage(IEnumerable<T> items)
        {
            if (PageSize <= 0)
            {
                return items;
            }

            var pageCount = (int)Math.Ceiling((float)_totalItemCount / PageSize);
            _pageIndex = Math.Min(pageCount - 1, Math.Max(0, _pageIndex));
            return items.Skip(_pageIndex * PageSize).Take(PageSize);
        }

        private IEnumerable<T> DoSort(IEnumerable<T> items)
        {
            if (SortDescriptions != null)
            {
                //bool sortNullsFirst = true;
                SupportNullsFistComparer<object> nullsFirstComparer = new SupportNullsFistComparer<object>(true);
                SupportNullsFistComparer<object> nullsLastComparer = new SupportNullsFistComparer<object>(false);
                foreach (var sortDescription in SortDescriptions.Reverse())
                {
                    var propertyName = sortDescription.Property;
                    var propertyInfo = typeof (T).GetNestedProperty(propertyName);
                    var sortNulls = sortDescription.SortNulls;
                    Func<T, object> keySelector = i =>
                    {
                        var currentPropertyInfo = propertyInfo ?? i.GetType().GetNestedProperty(propertyName);
                        var value = currentPropertyInfo == null ? null : currentPropertyInfo.GetValue(i);
                        if (sortDescription.SortConverter != null)
                        {
                            value = sortDescription.SortConverter(value);
                        }
                        return value;
                    };
                    
                    switch (sortNulls)
                    {
                        case CollectionViewSortNulls.Natural:
                            items = sortDescription.Ascending 
                                ? items.OrderBy(keySelector, nullsFirstComparer) 
                                : items.OrderByDescending(keySelector, nullsFirstComparer);
                            break;
                        case CollectionViewSortNulls.First:
                            items = sortDescription.Ascending 
                                ? items.OrderBy(keySelector, nullsFirstComparer)
                                : items.OrderByDescending(keySelector, nullsLastComparer);
                            break;
                        default:
                            items = sortDescription.Ascending
                                ? items.OrderBy(keySelector, nullsLastComparer)
                                : items.OrderByDescending(keySelector, nullsFirstComparer);
                            break;
                    }
                }
            }

            return items;
        }

        private IEnumerable<T> DoFilter(IEnumerable<T> items)
        {
            if (Filter != null)
            {
                items = items.Where(i => Filter(i));
            }

            return items;
        }

        private IEnumerable<T> DoPartialSelect(IEnumerable<T> items)
        {
            if (!IsDynamicalLoadingEnabled())
            {
                return items;
            }
            return items.Skip(Skip).Take(Math.Max(0, Top.Value));
        }

        private bool IsDynamicalLoadingEnabled()
        {
            return PageSize == 0 && Top.HasValue;
        }
    }

    internal class SupportNullsFistComparer<T> : IComparer<T>
    {
        bool sortNullsFirst = false;
        public SupportNullsFistComparer(bool sortNullsFirst)
        {
            this.sortNullsFirst = sortNullsFirst;
        }

        public int Compare(T x, T y)
        {
            int nullsFirst = sortNullsFirst ? -1 : 1;
            if (x != null && y == null) return -1 * nullsFirst;
            if (x == null && y != null) return 1 * nullsFirst;

            //In case: object compare type is integer array
            if(x != null && y != null)
            {
                Type xType = x.GetType(), yType = y.GetType();
                if (xType.IsArray && yType.IsArray
                    && xType.GetElementType() == typeof(int)
                    && yType.GetElementType() == typeof(int))
                {
                    string xElementsString = string.Join(",", (int[])(x as Array));
                    string yElementsString = string.Join(",", (int[])(y as Array));
                    return Comparer.Default.Compare(xElementsString, yElementsString);
                }
            }   

            return Comparer.Default.Compare(x, y);
        }

    }
}