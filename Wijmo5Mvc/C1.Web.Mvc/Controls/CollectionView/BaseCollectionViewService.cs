﻿#if ASPNETCORE
using System.Reflection;
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
using System.Web.Mvc;
#endif
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    partial class BaseCollectionViewService<T>
    {
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal override bool NeedRenderEvalInfo
        {
            get
            {
                return !IsTemplate && base.NeedRenderEvalInfo;
            }
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal override bool SupportDeferredScripts
        {
            get
            {
                return !IsTemplate && base.SupportDeferredScripts;
            }
        }

        /// <summary>
        /// Gets a value which represents the unique id for the control.
        /// </summary>
#if MODEL
        [C1Ignore]
#endif
        public override string UniqueId
        {
            get
            {
                return (IsTemplate ? ClientModules.TemplateUidPrefix : "") + base.UniqueId;
            }
        }

        /// <summary>
        /// Renders the client service to the specified writer.
        /// </summary>
        /// <param name="writer">The specified writer</param>
        protected override void RenderClientService(HtmlTextWriter writer)
        {
            var options = GetSerializationOptions();
            writer.Write("var {0} = new {1}({2});", Id, ClientComponent, options);
        }

       /// <summary>
       /// Get the text for the serialized options.
       /// </summary>
       /// <returns>A text.</returns>
        internal virtual string GetSerializationOptions()
        {
            var options = SerializeOptions();
            if (IsTemplate)
            {
                options = "c1.mvc._Initializer.bindTemplateScope(" + options + ", " + ClientModules.TemplateScopeName + ")";
            }
            return options;
        }

        /// <summary>
        /// Renders the scripts.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="renderScriptTag">A boolean value indicats whether to render the script tag.</param>
        internal protected override void RenderScripts(HtmlTextWriter writer, bool renderScriptTag)
        {
            if (renderScriptTag)
            {
                if (IsTemplate)
                {
                    writer.WriteLine("<script type=\"text/javascript\">");
                }
                else
                {
                    writer.RenderBeginTag("script", "type", "text/javascript");
                }
            }

            base.RenderScripts(writer, false);

            if (renderScriptTag)
            {
                if (IsTemplate)
                {
                    writer.WriteLine("<\\/script>");
                }
                else
                {
                    writer.RenderEndTag("script");
                }
            }
        }

        /// <summary>
        /// Registers the start-up script.
        /// </summary>
        /// <param name="writer">The writer.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            if (IsTemplate && !Id.Contains(ClientModules.TemplateUidPrefix))
            {
                Id = ClientModules.TemplateUidPrefix + Id;
            }

            base.RegisterStartupScript(writer);
        }

        /// <summary>
        /// Creates an instance of <see cref="BaseCollectionViewService{T}"/>
        /// </summary>
        /// <typeparam name="TDataSource">The type of ItemsSource to be created.</typeparam>
        /// <param name="parent">The parent object.</param>
        /// <param name="helper">The helper object.</param>
        /// <returns>An instance created.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static TDataSource GetDataSource<TDataSource>(IItemsSourceContainer<T> parent, HtmlHelper helper)
    where TDataSource : BaseCollectionViewService<T>
        {
            var tDataSource = parent.ItemsSource as TDataSource;
            if (tDataSource == null)
            {
                tDataSource = ReflectUtils.GetInstanceWithHtmlHelper(typeof(TDataSource), helper) as TDataSource;
                // Initialize SortDescriptions and GroupDescriptions with the values from the parent. 
                var pType = parent.GetType();
                try
                {
                    var sdPI = pType.GetProperty("SortDescriptions");
                    if (sdPI != null)
                    {
                        var sds = sdPI.GetValue(parent, null) as IList<SortDescription>;
                        if (sds != null)
                        {
                            foreach (var sd in sds)
                            {
                                tDataSource.SortDescriptions.Add(sd);
                            }
                        }
                    }

                    var gdPI = pType.GetProperty("GroupDescriptions");
                    if (gdPI != null)
                    {
                        var gds = gdPI.GetValue(parent, null) as IList<GroupDescription>;
                        if (gds != null)
                        {
                            foreach (var gd in gds)
                            {
                                tDataSource.GroupDescriptions.Add(gd);
                            }
                        }
                    }

                    var psPI = pType.GetProperty("PageSize");
                    if(psPI != null)
                    {
                        tDataSource.PageSize = (int)psPI.GetValue(parent, null);
                    }
                }
                catch { }

                parent.ItemsSource = tDataSource;
            }
            return tDataSource;
        }
    }
}
