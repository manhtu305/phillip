﻿using C1.JsonNet;
using C1.JsonNet.Converters;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents a base class for types defining grouping conditions.
    /// </summary>
    public abstract partial class GroupDescription
    {
        /// <summary>
        /// Gets the client-side object's class name.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [JsonConverter(typeof(FunctionConverter))]
        public abstract string ClientClass { get; }
    }
}