﻿namespace C1.Web.Mvc
{
    partial class ODataVirtualCollectionViewService<T>
    {
        internal override string ClientClass
        {
            get
            {
                return ClientModules.ODataVirtualCollectionViewService;
            }
        }
    }
}
