﻿namespace C1.Web.Mvc
{
    partial class ODataCollectionViewService<T>
    {
        internal override string ClientClass
        {
            get
            {
                return ClientModules.ODataCollectionViewService;
            }
        }
    }
}
