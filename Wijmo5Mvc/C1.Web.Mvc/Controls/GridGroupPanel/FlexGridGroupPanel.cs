﻿using C1.Web.Mvc.WebResources;
#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    [Scripts(typeof(GridExDefinitions.GroupPanel))]
    public partial class FlexGridGroupPanel<T>
    {
        #region Rendering

        internal override string ClientSubModule
        {
            get { return ClientModules.GridGroupPanel; }
        }

        internal override string ClientClass
        {
            get { return ClientModules.GroupPanelClass; }
        }

        /// <summary>
        /// Render the extender instance to the writer.
        /// </summary>
        /// <param name="writer">the specified writer.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(Selector))
            {
                writer.Write(
                    "var {0} = document.createElement('div'); {1}.hostElement.parentNode.insertBefore({0}, {1}.hostElement);",
                    ClientElement, GetTargetInstance());
            }
            else
            {
                writer.Write("var {0} = '{1}';", ClientElement, Selector);
            }

            base.RegisterStartupScript(writer);
        }

        internal override string ClientConstructorArgs
        {
            get { return ClientElement + "," + GetTargetInstance(); }
        }

        private string ClientElement
        {
            get
            {
                return UniqueId + "_GroupPanel";
            }
        }

        #endregion Rendering
    }
}
