﻿using C1.JsonNet;
using System;
using System.Collections;
#if !ASPNETCORE
using System.ComponentModel;
#else
using System.Reflection;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines a converter for <see cref="C1.Web.Mvc.TemplateBindings"/>
    /// </summary>
    internal sealed class TemplateBindingsConverter : JsonConverter
    {
        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(TemplateBindings))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existedValue">The existing value of object being read.</param>
        /// <returns>The object value after deserializing.</returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            TemplateBindings bindings = existedValue as TemplateBindings;
            if (bindings == null)
            {
                throw new ArgumentNullException("existedValue");
            }
            bindings.Clear();
            Hashtable htBindings = reader.Current as Hashtable;
            if (htBindings == null || htBindings.Count == 0)
            {
                return bindings;
            }
#if !ASPNETCORE
            PropertyDescriptorCollection pdCollection = TypeDescriptor.GetProperties(bindings.Control);
            foreach (PropertyDescriptor pd in pdCollection)
#else
            var pdCollection = bindings.Control.GetType().GetProperties();
            foreach (PropertyInfo pd in pdCollection)
#endif
            {
                string serializedName = JsonUtility.GetSerializedName(pd, null, reader.JsonSetting);
                if (htBindings.ContainsKey(serializedName) && htBindings[serializedName] != null)
                {
                    bindings.Add(pd.Name, htBindings[serializedName].ToString());
                }
            }
            return bindings;
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value to be serialized.</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            TemplateBindings bindings = value as TemplateBindings;
            if (bindings == null)
            {
                writer.WriteValue(null);
                return;
            }
            writer.StartObjectScope(bindings);
#if !ASPNETCORE
            PropertyDescriptorCollection pdCollection = TypeDescriptor.GetProperties(bindings.Control);
#else
            var pdCollection = bindings.Control.GetType().GetProperties();
#endif
            foreach (var item in bindings)
            {
#if !ASPNETCORE
                PropertyDescriptor pd = pdCollection[item.Key];
#else
                PropertyInfo pd = GetProperty(item.Key, pdCollection);
#endif
                if (pd == null)
                {
                    continue;
                }
                //don't consider JsonResolver which could change the property name for serialization.
                writer.WriteRawName(JsonUtility.GetSerializedName(pd, null, writer.JsonSetting));
                writer.WriteValue(item.Value);
            }
            writer.EndScope();
        }

#if ASPNETCORE
        private PropertyInfo GetProperty(string name, PropertyInfo[] pis)
        {
            var count = 0;
            if(pis != null)
            {
                count = pis.Length;
                for(int i =0; i<count; i++) {
                    var pi = pis[i];
                    if(string.Compare(name, pi.Name, false) == 0)
                    {
                        return pi;
                    }
                }
            }
            return null;
            
        }
#endif
    }
}
