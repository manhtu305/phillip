﻿using C1.JsonNet;
#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
using System.Text.Encodings.Web;
#else
using System.Web.UI;
#endif
using System.ComponentModel;

namespace C1.Web.Mvc
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    abstract partial class Extender : IExtender
    {
        #region Properties
        #region License
        internal override bool NeedRenderEvalInfo
        {
            get { return false; }
        }
        #endregion License

        #region Virtual
        /// <summary>
        /// Overrides to remove this attribute.
        /// </summary>
        [Ignore()]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string UniqueId
        {
            get
            {
                return base.UniqueId;
            }
        }
        #endregion Virtual
        #endregion

        #region Render
        /// <summary>
        /// Renders the extender result to the writer.
        /// </summary>
        /// <param name="writer">The specified writer used to render.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
#endif
        public override void Render(HtmlTextWriter writer
#if ASPNETCORE
            , HtmlEncoder encoder
#endif
            )
        {
            base.Render(writer
#if ASPNETCORE
            , encoder
#endif
            );
        }

        /// <summary>
        /// Renders the extender instance to the writer.
        /// </summary>
        /// <param name="writer">the specified writer.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            writer.Write("var {0} = new {1}({2});", Id, ClientComponent, ClientConstructorArgs);

            if (UseClientInitialize)
            {
                writer.Write("{0}.initialize({1});", Id, SerializeOptions());
            }

            writer.Write("c1._addExtender({0}, '{1}', {1});", GetTargetInstance(), Id);
        }

        internal virtual string ClientConstructorArgs
        {
            get
            {
                return GetTargetInstance();
            }
        }

        internal virtual bool UseClientInitialize
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the target instance in client.
        /// </summary>
        /// <returns>A string indicates the target instance.</returns>
        protected virtual string GetTargetInstance()
        {
            return ClientModules.GetWrapperControl(_target.Id);
        }
        #endregion Render
    }
}
