﻿namespace C1.Web.Mvc
{
    internal static class ClientModules
    {
        public const string TemplateUidPrefix = "{{uid}}_";
        public const string TemplateScopeName = "$data";

        public const string PrivatePrefix = "_";
        public const string WrapperSuffix = "Wrapper";
        public const string WrapperPattern = PrivatePrefix + "{0}" + WrapperSuffix;
        public const string WrapperControlPropertyPattern = "{0}.control";

        public const string C1 = "c1.";
        public const string C1Mvc = C1 + "mvc.";

        public const string Chart = "chart.";
        public const string ChartAnnotation = Chart + "annotation.";
        public const string ChartInteraction = Chart + "interaction.";
        public const string ChartAnimation = Chart + "animation.";
        public const string ChartHierarchical = Chart + "hierarchical.";
        public const string ChartRadar = Chart + "radar.";

        public const string Collections = "collections.";
        public const string CallbackCollectionViewClass = "CallbackCollectionView";

        public const string OData = "odata.";
        public const string ODataCollectionViewService = "ODataCollectionView";
        public const string ODataVirtualCollectionViewService = "ODataVirtualCollectionView";

        public const string Gauge = "gauge.";

        public const string Input = "input.";

        public const string Grid = "grid.";
        public const string GridFilter = Grid + "filter.";
        public const string GridDetail = Grid + "detail.";
        public const string GridSearch = Grid + "search.";
        public const string GridSelector = Grid + "selector.";
        public const string GridCellMaker = Grid + "cellmaker.";
        public const string GridGroupPanel = Grid + "grouppanel.";
        public const string GroupPanelClass = "GroupPanel";

        public const string Nav = "nav.";

        public static string GetWrapperName(string controlName) {
            return string.Format(WrapperPattern, controlName);
        }

        public static string GetWrapperControl(string wrapperId)
        {
            return string.Format(WrapperControlPropertyPattern, wrapperId);
        }
    }
}
