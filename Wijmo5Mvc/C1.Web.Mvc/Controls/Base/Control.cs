﻿using C1.Web.Mvc.Localization;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using System.Text.Encodings.Web;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif
using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;

namespace C1.Web.Mvc
{
    abstract partial class Control : ITemplate
    {
        #region Fields
        private static readonly Regex RegId = new Regex("^[A-Za-z_][A-Za-z0-9_:\\.-]*$");
        #endregion Fields

        #region Properties
        #region License
        internal override bool NeedRenderEvalInfo
        {
            get
            {
                return !IsTemplate && base.NeedRenderEvalInfo;
            }
        }
        #endregion License

        #region Virtual
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal override bool SupportDeferredScripts
        {
            get
            {
                return !IsTemplate && base.SupportDeferredScripts;
            }
        }

        internal override string InnerId
        {
            get
            {
                return HtmlAttributes["id"];
            }
            set
            {
                HtmlAttributes["id"] = value;
            }
        }

#if ASPNETCORE
        /// <summary>
        /// Gets the tag name that corresponds to this Web
        /// server control. This property is used primarily by control developers. 
        /// </summary>
        internal protected virtual string TagName
        {
            get
            {
                return "div";
            }
        }
#else
        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
        /// server control. This property is used primarily by control developers. 
        /// </summary>
        protected virtual HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        internal string TagName
        {
            get
            {
                return HttpExtension.GetTagName(TagKey);
            }
        }
#endif

        private bool NeedRenderMarkup
        {
            get { return string.IsNullOrEmpty(Selector); }
        }

        /// <summary>
        /// Gets a value which represents the unique id for the control.
        /// </summary>
        public override string UniqueId
        {
            get
            {
                return (IsTemplate ? ClientModules.TemplateUidPrefix : "") + base.UniqueId;
            }
        }

        internal override string ClientClass
        {
            get
            {
                return ClientModules.GetWrapperName(base.ClientClass);
            }
        }
        #endregion Virtual
        #endregion Properties

        #region Ctor
        /// <summary>
        /// Creates one <see cref="C1.Web.Mvc.Control"/> instance
        /// by using the specified HtmlHelper object and selector.
        /// </summary>
        /// <param name="helper">The html helper.</param>
        /// <param name="selector">The selector used to match the dom elements which the control is attached to.</param>
        protected Control(HtmlHelper helper, string selector = null)
            : base(helper)
        {
            Selector = selector;
            Initialize();
        }
        #endregion Ctor

        #region Methods

        #region Public
        /// <summary>
        /// Transfers to template mode.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please set IsTemplate property to true instead.")]
        public void ToTemplate()
        {
            IsTemplate = true;
        }
        #endregion Public

        #region Init
        /// <summary>
        /// Gets the selector according to the settings.
        /// If the selector is not set and id is not set, create an id and use it as selector.
        /// </summary>
        /// <returns></returns>
        internal virtual string GetRenderSelector()
        {
            if (!string.IsNullOrEmpty(Selector))
            {
                return Selector;
            }

            EnsureId();
            return "#" + HtmlAttributes["id"];
        }
        #endregion Init

        #region Renders
        /// <summary>
        /// Renders the control or the callback result to the writer.
        /// </summary>
        /// <param name="writer">The Html writer.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        public override void Render(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        public override void Render(HtmlTextWriter writer)
#endif
        {
            if (NeedRenderMarkup)
            {
                RenderMarkup(writer
#if ASPNETCORE
                    , encoder
#endif
                );
            }

            base.Render(writer
#if ASPNETCORE
            , encoder
#endif
            );
        }

        /// <summary>
        /// Renders the control markup.
        /// </summary>
        /// <param name="writer">the specified writer used to write the markup.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        protected virtual void RenderMarkup(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        protected virtual void RenderMarkup(HtmlTextWriter writer)
#endif
        {
            RenderBeginTag(writer);
            RenderContent(writer
#if ASPNETCORE
                , encoder
#endif
            );
            RenderEndTag(writer);
        }

        /// <summary>
        /// Renders end tag of control.
        /// </summary>
        /// <param name="writer">The specified writer used to write the markup.</param>
        protected virtual void RenderEndTag(HtmlTextWriter writer)
        {
            writer.RenderEndTag(TagName);
        }

        /// <summary>
        /// Renders content of control.
        /// </summary>
        /// <param name="writer">The specified writer used to write the markup.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        protected virtual void RenderContent(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        protected virtual void RenderContent(HtmlTextWriter writer)
#endif
        {
        }

        /// <summary>
        /// Renders begin tag of control.
        /// </summary>
        /// <param name="writer">The specified writer used to write the markup</param>
        protected virtual void RenderBeginTag(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(TagName, RenderAttributes);
        }

        /// <summary>
        /// Renders the html attributes of control.
        /// </summary>
        /// <param name="writer">The specified writer used to write the markup</param>
        protected virtual void RenderAttributes(HtmlTextWriter writer)
        {
            UpdateAttributes();
            foreach (var htmlAttribute in HtmlAttributes)
            {
                writer.AddAttribute(htmlAttribute.Key, htmlAttribute.Value);
            }
        }

        internal void UpdateAttributes()
        {
            EnsureId();
            VerifyIdAttribute();
            EnsureStyle();
        }

        private void VerifyIdAttribute()
        {
            if (IsTemplate)
            {
                if (!Id.Contains(ClientModules.TemplateUidPrefix))
                {
                    Id = ClientModules.TemplateUidPrefix + Id;
                }
                return;
            }

            var id = HtmlAttributes.FirstOrDefault(i => string.Equals(i.Key, "id", StringComparison.OrdinalIgnoreCase)).Value;
            if (!string.IsNullOrEmpty(id) && !RegId.IsMatch(id))
            {
                throw new FormatException(string.Format(Resources.InvalidValueException, id));
            }
        }

        /// <summary>
        /// Renders the scripts.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="renderScriptTag">A boolean value indicats whether to render the script tag.</param>
        internal protected override void RenderScripts(HtmlTextWriter writer, bool renderScriptTag)
        {
            //Render the escaped script tag when it is template. And prevent rendering the tag in base.
            var templateScriptTag = renderScriptTag && IsTemplate;
            if (templateScriptTag)
            {
                writer.WriteLine("<script type=\"text/javascript\">");
            }

            base.RenderScripts(writer, renderScriptTag && !templateScriptTag);

            if (templateScriptTag)
            {
                writer.WriteLine("<\\/script>");
            }
        }

        /// <summary>
        /// Registers the startup scripts.
        /// </summary>
        /// <param name="writer">the specified writer used to write the startup scripts.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            writer.Write("var {0} = new {1}({2});", Id, ClientComponent, GetWrapperCtorOptions());
            writer.Write("{0}.initialize({1}{2});", Id, SerializeOptions(), IsTemplate ? (", " + ClientModules.TemplateScopeName) : "");
        }

        /// <summary>
        /// Gets the options for the wrapper constructor.
        /// </summary>
        /// <returns>The json text for the options.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual string GetWrapperCtorOptions()
        {
            return string.Format("'{0}'", GetRenderSelector());
        }

        /// <summary>
        /// This method is performed before rendering.
        /// </summary>
        protected override void OnPreRender()
        {
            EnsureChildComponents();
            if (IsTemplate)
            {
                foreach(var component in Components)
                {
                    var template = component as ITemplate;
                    if (template != null)
                    {
                        template.IsTemplate = true;
                    }
                }
            }

            base.OnPreRender();
        }
        #endregion Renders

        #region Assistant
        private void EnsureStyle()
        {
            if (CssStyles.Count > 0)
            {
                var style = string.Join(";", CssStyles.Select(s => string.Format("{0}:{1}", s.Key, s.Value)));
                if (HtmlAttributes.ContainsKey("style"))
                {
                    style = string.Format("{0};{1}", HtmlAttributes["style"], style);
                }
                HtmlAttributes["style"] = style;
            }
        }
        #endregion Assistant
        #endregion Methods
    }
}