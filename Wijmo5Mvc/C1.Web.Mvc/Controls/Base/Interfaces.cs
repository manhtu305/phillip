﻿using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Mvc
{
#if !ASPNETCORE
    #region ICallbackHandler
    /// <summary>
    /// Defines an interface used to process the callback request.
    /// </summary>
    internal interface ICallbackHandler
    {
        /// <summary>
        /// Handles the callback.
        /// </summary>
        /// <remarks>
        /// Only when the current request is callback and it is raised by itself, this method will be called.
        /// </remarks>
        /// <param name="cbm">the callback information</param>
        void ProcessCallBack(CallbackManager cbm);
    }
    #endregion ICallbackHandler
#endif

    #region IContainer
    /// <summary>
    /// Defines an interface which has child components.
    /// </summary>
    internal interface IComponentContainer
    {
        /// <summary>
        /// Gets the child components.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        IList<Component> Components { get; }
    }
    #endregion IContainer

    /// <summary>
    /// The interface which has items source.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal interface IItemsSourceContainer<T>
    {
        /// <summary>
        /// Gets or sets the ItemsSource settings.
        /// </summary>
        IItemsSource<T> ItemsSource { get; set; }
    }
}
