﻿#if ASPNETCORE
#else
using C1.Web.Mvc.Localization;
using System;
using System.Collections.Generic;
using System.Web.Helpers;
using System.Web.Mvc;

namespace C1.Web.Mvc
{
    /// <summary>
    /// C1AntiForgeryTokenAttribute provides support for MVC AntiForgency feature
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public sealed class C1AntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// Implement the authorizing logic base on the RequestVerificationToken value in the request header.
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException(Resources.ArgumentNullException);
            }
            var httpContext = filterContext.HttpContext;
            var cookie = httpContext.Request.Cookies[AntiForgeryConfig.CookieName];
            AntiForgery.Validate(cookie != null ? cookie.Value : null, httpContext.Request.Headers["RequestVerificationToken"]);
        }
    }
}

#endif