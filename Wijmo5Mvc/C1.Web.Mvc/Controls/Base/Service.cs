﻿#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using System.Text.Encodings.Web;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif
using System.ComponentModel;
using C1.JsonNet;
using System.Collections.Generic;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    abstract partial class Service : ITemplate
    {
        #region Fields
        private const string SERVICE = "c1._registerService";
        #endregion Fields

        #region Ctors
        /// <summary>
        /// Creates one <see cref="Service"/> instance.
        /// </summary>
        /// <param name="helper">The html helper.</param>
        protected Service(HtmlHelper helper)
            :base(helper)
        {
            Initialize();
        }
        #endregion Ctors

        #region Properties
        #region License
        internal override bool NeedRenderEvalInfo
        {
            get { return false; }
        }
        #endregion License

        #region ITemplate
        private IDictionary<string, string> _templateBindings;
        /// <summary>
        /// Gets the collection of the template bindings.
        /// </summary>
#if MODEL
        [C1Ignore]
#else
        [JsonConverter(typeof(TemplateBindingsConverter))]
#endif
        public IDictionary<string, string> TemplateBindings
        {
            get
            {
                return _templateBindings ?? (_templateBindings = new TemplateBindings(this));
            }
        }

        /// <summary>
        /// Gets or sets a boolean value which indicates whether transfer this <see cref="Service"/> to template mode.
        /// </summary>
        [C1Ignore]
        public bool IsTemplate
        {
            get;
            set;
        }
        #endregion ITemplate
        #endregion Properties

        #region Methods
        /// <summary>
        /// Renders the service result to the writer.
        /// </summary>
        /// <param name="writer">The specified writer used to render.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
#endif
        public override void Render(HtmlTextWriter writer
#if ASPNETCORE
            , HtmlEncoder encoder
#endif
            )
        {
            base.Render(writer
#if ASPNETCORE
            , encoder
#endif
            );
        }

        /// <summary>
        /// Registers the start-up script.
        /// </summary>
        /// <param name="writer">The writer.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            RenderClientService(writer);
            RegisterClientService(writer);
        }

        /// <summary>
        /// Renders the client service to the specified writer.
        /// </summary>
        /// <param name="writer">The specified writer</param>
        protected virtual void RenderClientService(HtmlTextWriter writer)
        {
            writer.Write("var {0} = new {1}();", Id, ClientComponent);
            writer.Write("{0}.initialize({1});", Id, SerializeOptions());
        }

        /// <summary>
        /// Registers the client service via the specified writer.
        /// </summary>
        /// <param name="writer">The specified writer</param>
        /// <remarks>
        /// If the key specified by keyName exists, it will not be registered.
        /// </remarks>
        protected virtual void RegisterClientService(HtmlTextWriter writer)
        {
            writer.Write(Service.GetClientServiceExpression(Id, Id) + ";");
        }

        internal static string GetClientServiceExpression(string key, string ctor = null, string opts = null)
        {
            string expression = SERVICE + "('" + key + "'";
            if (!string.IsNullOrEmpty(ctor))
            {
                expression += "," + ctor;
            }
            if (!string.IsNullOrEmpty(opts))
            {
                expression += "," + opts;
            }
            expression += ")";
            return expression;
        }
        #endregion Methods
    }
}
