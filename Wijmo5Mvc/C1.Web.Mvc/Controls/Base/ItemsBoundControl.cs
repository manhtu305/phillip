﻿#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    abstract partial class ItemsBoundControl<T> : IItemsSourceContainer<T>
    {
        /// <summary>
        /// Register the startup scripts.
        /// </summary>
        /// <param name="writer">the specified writer used to write the startup scripts.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            var collectionViewService = ItemsSource as BaseCollectionViewService<T>;
            if (collectionViewService != null)
            {
                collectionViewService.RenderScripts(writer, false);
            }

            base.RegisterStartupScript(writer);
        }

        /// <summary>
        /// Ensure the child components created.
        /// </summary>
        protected override void CreateChildComponents()
        {
            base.CreateChildComponents();
            var collectionViewService = ItemsSource as BaseCollectionViewService<T>;
            if (collectionViewService != null)
            {
                Components.Add(collectionViewService);
            }
        }

        internal TDataSource GetDataSource<TDataSource>()
            where TDataSource : BaseCollectionViewService<T>
        {
            return BaseCollectionViewService<T>.GetDataSource<TDataSource>(this, Helper);
        }
    }
}
