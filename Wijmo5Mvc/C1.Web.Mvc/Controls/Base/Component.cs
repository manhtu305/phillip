﻿using System;
using System.ComponentModel;
using C1.JsonNet;
using C1.Util.Licensing;
using C1.Web.Mvc.Localization;
using C1.Web.Mvc.Serialization;
using System.Collections.Generic;
#if ASPNETCORE
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using IHtmlString = Microsoft.AspNetCore.Html.IHtmlContent;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text.Encodings.Web;
using UrlHelper = Microsoft.AspNetCore.Mvc.IUrlHelper;
#else
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using HttpContext = System.Web.HttpContextBase;
using IHostingEnvironment = System.Web.HttpServerUtility;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines the C1 MVC component class.
    /// </summary>
    public abstract partial class Component: IHtmlString, IComponentContainer
    {
        #region Fields
        private string _uniqueId;
        private IList<Component> _components;
        private const string ComponentCountKey = "C1MVCComponentCount";
        private const string UniqueIdPrefix = "_C1MVCCtrl";
        private const string EvalInfoRenderedName = UniqueIdPrefix + "_EVAL";
        private bool? _needRenderEvalInfo;
        #endregion Fields

        #region Properties
        #region License
        internal virtual Type LicenseDetectorType
        {
            get
            {
                return typeof(LicenseDetector);
            }
        }

        internal virtual bool NeedRenderEvalInfo
        {
            get
            {
                return (bool)(_needRenderEvalInfo ?? (_needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo(LicenseDetectorType, HttpContext)));
            }
        }

        internal bool EvalInfoRendered
        {
            get
            {
                object rendered;
                if (RouteData.TryGetValue(EvalInfoRenderedName, out rendered))
                {
                    if (rendered is bool)
                    {
                        return (bool)rendered;
                    }
                }

                return false;
            }
            set { RouteData[EvalInfoRenderedName] = value; }
        }
        #endregion License

        #region Assistant
        /// <summary>
        /// Gets the url helper.
        /// </summary>
        protected UrlHelper UrlHelper
        {
            get
            {
                return
#if ASPNETCORE
                    ViewContext.HttpContext.Items[typeof(UrlHelper)] as UrlHelper;

#else
                    new UrlHelper(Helper.ViewContext.RequestContext);
#endif
            }
        }

        internal HttpContext HttpContext
        {
            get
            {
                return ViewContext == null ? null : ViewContext.HttpContext;
            }
        }

        /// <summary>
        /// Gets or sets the htmlhelper object.
        /// </summary>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal HtmlHelper Helper
        {
            [SmartAssembly.Attributes.DoNotObfuscate]
            get;
            private set;
        }

        internal ViewContext ViewContext
        {
            get { return Helper.ViewContext; }
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static IHostingEnvironment HostingEnvironment
        {
#if !ASPNETCORE
            [SmartAssembly.Attributes.DoNotObfuscate]
            get
            {
                return System.Web.HttpContext.Current.Server;
            }
#else
            get;
            set;
#endif
        }

#if ASPNETCORE
        private IDictionary<string, object> RouteData
        {
            get { return ViewContext.RouteData.Values; }
        }
#else
        private RouteValueDictionary RouteData
        {
            get { return ViewContext.RequestContext.RouteData.Values; }
        }
#endif
        #endregion Assistant

        #region Public
        /// <summary>
        /// Gets a value which represents the unique id for the control.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual string UniqueId
        {
            get 
            {
                EnsureUniqueId();
                return _uniqueId; 
            }
        }

        /// <summary>
        /// Gets the child components.
        /// </summary>
        [Ignore]
        public IList<Component> Components
        {
            get { return _components ?? (_components = new List<Component>()); }
        }
        #endregion Public

        #region Virtual
        internal virtual string InnerId
        {
            get; set;
        }

        /// <summary>
        /// Gets a value which represents the client control constructor with the complete namespace.
        /// </summary>
        internal protected virtual string ClientComponent
        {
            get
            {
                return ClientModule + ClientSubModule + ClientClass;
            }
        }

        /// <summary>
        /// The client component type, like: FlexGrid.
        /// </summary>
        internal virtual string ClientClass
        {
            get
            {
                return GetSimpleTypeName(GetType());
            }
        }

        /// <summary>
        /// The client module, like: c1.mvc.
        /// </summary>
        internal virtual string ClientModule
        {
            get
            {
                return ClientModules.C1Mvc;
            }
        }

        /// <summary>
        /// The sub module, like: chart, grid.
        /// </summary>
        internal virtual string ClientSubModule
        {
            get
            {
                return string.Empty;
            }
        }

        internal virtual bool SupportDeferredScripts
        {
            get
            {
                return HasScripts;
            }
        }

        /// <summary>
        /// Gets a boolean value indicates whether current component has the scripts which need to be rendered.
        /// </summary>
        protected virtual bool HasScripts
        {
            get
            {
                return true;
            }
        }
        #endregion Virtual
        #endregion Properties

        #region Ctors
        /// <summary>
        /// Creates one <see cref="Component"/> instance.
        /// </summary>
        /// <param name="helper">The html helper.</param>
        protected Component(HtmlHelper helper)
        {
            if (helper == null)
            {
                throw new ArgumentNullException("helper");
            }
            Helper = helper;
            Initialize();
#if ASPNETCORE
            if (HostingEnvironment == null && HttpContext != null)
            {
                HostingEnvironment = HttpContext.RequestServices.GetService(typeof(IHostingEnvironment)) as IHostingEnvironment;
            }
#endif
            EnsureUniqueId();

            var manager = HttpContext.GetDeferredScriptsManager();
            if (manager != null)
            {
                manager.Register(this);
            }
        }
        #endregion Ctors

        #region Methods
        #region Serialization
        /// <summary>
        /// Serialize the control to a Json string.
        /// </summary>
        /// <returns>A JSON string representation of the control.</returns>
        internal protected virtual string SerializeOptions()
        {
            return JsonConvertHelper.Serialize(this, true, true);
        }
        #endregion Serialization

        #region Assistant
        internal void EnsureId()
        {
            if (string.IsNullOrEmpty(InnerId))
            {
                InnerId = UniqueId;
            }
        }

        internal virtual string NewUniqueId()
        {
            var controlCount = RouteData.ContainsKey(ComponentCountKey) ? (int) RouteData[ComponentCountKey] : 0;
            RouteData[ComponentCountKey] = ++controlCount;
            return UniqueIdPrefix + controlCount;
        }

        private static string GetSimpleTypeName(Type type)
        {
            const string genericTypeSeparator = "`";
            var typeName = type.Name;
            var genericTypeSeparatorIndex = typeName.IndexOf(genericTypeSeparator);
            return genericTypeSeparatorIndex > -1 ? typeName.Substring(0, genericTypeSeparatorIndex) : typeName;
        }

        /// <summary>
        /// Ensure the unique id is created.
        /// </summary>
        /// <remarks>
        /// When the component supports callback, 
        /// this method should be called in the constructor of the component to ensure the component has the unique and fixed id.
        /// </remarks>
        internal void EnsureUniqueId()
        {
            if (_uniqueId == null)
            {
                _uniqueId = NewUniqueId();
            }
        }
        #endregion Assistant

        #region Render
        /// <summary>
        /// Render the component result to the writer.
        /// </summary>
        /// <param name="writer">The specified writer used to render.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        public virtual void Render(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        public virtual void Render(HtmlTextWriter writer)
#endif
        {
            OnPreRender();
            RenderEvalInfo(writer);
            if (HasScripts)
            {
                var manager = HttpContext.GetDeferredScriptsManager();
                if (manager != null && SupportDeferredScripts)
                {
                    return;
                }

                RenderScripts(writer, true);
            }
        }

        /// <summary>
        /// This method is performed before rendering.
        /// </summary>
        protected virtual void OnPreRender()
        {
        }

        /// <summary>
        /// Gets or sets a boolean value indicates whether child components are created.
        /// </summary>
        protected bool ChildComponentsCreated
        {
            get;
            set;
        }

        /// <summary>
        /// Render the scripts.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="renderScriptTag">A boolean value indicats whether to render the script tag.</param>
        internal protected virtual void RenderScripts(HtmlTextWriter writer, bool renderScriptTag)
        {
            if (renderScriptTag)
            {
                writer.RenderBeginTag("script", "type", "text/javascript");
            }

            RegisterStartupScript(writer);

            if (renderScriptTag)
            {
                writer.RenderEndTag("script");
            }

            var manager = HttpContext.GetDeferredScriptsManager();
            if (manager != null)
            {
                manager.Unregister(this);
            }
        }

        /// <summary>
        /// Registers the start-up script.
        /// </summary>
        /// <param name="writer">The writer.</param>
        protected virtual void RegisterStartupScript(HtmlTextWriter writer)
        {
        }

        internal void RenderEvalInfo(HtmlTextWriter writer)
        {
            if (!NeedRenderEvalInfo || EvalInfoRendered)
            {
                return;
            }

            writer.RenderBeginTag("script");
            writer.Write("alert('" + Resources.EvalInfoMessage + "');");
            writer.RenderEndTag("script");
            EvalInfoRendered = true;
        }

#if !ASPNETCORE
        /// <summary>
        /// Writer the process result of current instance to the writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        internal virtual void WriteTo(HtmlTextWriter writer)
        {
            EnsureChildComponents();
            bool isCallbackHandled = false;
            var callbackManager = new CallbackManager(ViewContext.HttpContext);
            if (callbackManager.IsCallback)
            {
                isCallbackHandled = HandleCallBack(callbackManager);
            }
            if (isCallbackHandled)
            {
                return;
            }

            Render(writer);
        }

        /// <summary>
        /// Returns an HTML-encoded string.
        /// </summary>
        /// <returns>An HTML-encoded string.</returns>
        public virtual string ToHtmlString()
        {
            using (var sw = new StringWriter())
            {
                var htw = new HtmlTextWriter(sw);
                WriteTo(htw);
                return sw.ToString();
            }
        }

        /// <summary>
        /// Handle the callback.
        /// </summary>
        /// <returns>
        /// True, if the callback is raised and processed by the component.
        /// </returns>
        internal virtual bool HandleCallBack(CallbackManager cbm)
        {
            if (IsCallBackHandledBy(cbm, this))
            {
                return true;
            }
            return false;
        }

        private bool IsCallBackHandledBy(CallbackManager cbm, Component bc)
        {
            if (bc == null)
            {
                return false;
            }
            ICallbackHandler icbComponent = bc as ICallbackHandler;
            if (icbComponent != null && cbm.UniqueId == bc.UniqueId)
            {
                icbComponent.ProcessCallBack(cbm);
                return true;
            }
            IComponentContainer container = bc as IComponentContainer;
            if (container != null && container.Components.Count != 0)
            {
                foreach (var component in container.Components)
                {
                    if (IsCallBackHandledBy(cbm, component))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
#else

        /// <summary>
        /// Writer the process result of current instance to the writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="encoder">The Html encoder.</param>
        public virtual void WriteTo(HtmlTextWriter writer, HtmlEncoder encoder)
        {
            EnsureChildComponents();
            Render(writer, encoder);
        }

#endif

        /// <summary>
        /// Ensure the child components created.
        /// </summary>
        protected virtual void EnsureChildComponents()
        {
            if (ChildComponentsCreated) return;
            CreateChildComponents();
            ChildComponentsCreated = true;
        }

        /// <summary>
        /// Creates child components.
        /// </summary>
        protected virtual void CreateChildComponents()
        {
        }
        #endregion Render
        #endregion Methods
    }
}
