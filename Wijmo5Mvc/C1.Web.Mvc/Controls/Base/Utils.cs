﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using UrlHelper = Microsoft.AspNetCore.Mvc.IUrlHelper;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc
{
    internal static class ReflectUtils
    {
        public static ConstructorInfo GetConstructor(Type t, Type firstParamType, out int parsCount)
        {
            parsCount = 0;
            var bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            var count = 0;
            var ctor = t.GetConstructors(bindingFlags).SingleOrDefault(c =>
            {
                var pars = c.GetParameters();
                if (pars.Length > 0 && pars[0].ParameterType.IsAssignableFrom(firstParamType))
                {
                    count = pars.Length;
                    return true;
                }
                return false;

            });
            parsCount = count;
            return ctor;
        }

        public static object GetInstanceWithHtmlHelper(Type objType, HtmlHelper helper)
        {
            var parsCount = 0;
            var ctor = GetConstructor(objType, typeof(HtmlHelper), out parsCount);
            var objs = new List<object>();
            objs.Add(helper);
            for (var i = 0; i < parsCount - 1; i++)
            {
                objs.Add(null);
            }
            return ctor.Invoke(objs.ToArray());
        }
    }

    /// <summary>
    /// Defines a static class to process the urls.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class Url
    {
        private static readonly char[] ValidAttributeWhitespaceChars =
    new[] { '\t', '\n', '\u000C', '\r', ' ' };

        public static string GetFullUrl(string relativeUrl, UrlHelper urlHelper)
        {
            var trimmedUrl = relativeUrl;
            if (relativeUrl != null && relativeUrl.Length >= 2
                && relativeUrl[0] == '~' && relativeUrl[1] == '/')
            {
                if (urlHelper == null)
                {
                    throw new ArgumentNullException("urlHelper");
                }

                trimmedUrl = urlHelper.Content(trimmedUrl);
            }

            return trimmedUrl;
        }
        public static string GetValidateUrl(string url)
        {
            return url.Trim(ValidAttributeWhitespaceChars);
        }
    }
}
