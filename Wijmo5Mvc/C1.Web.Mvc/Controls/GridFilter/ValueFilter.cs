﻿using C1.Web.Mvc.Localization;
using System;
using System.Globalization;

namespace C1.Web.Mvc
{
    public partial class ValueFilter
    {
        internal bool Apply(object value, string format)
        {
            if (ShowValues != null)
            {
                foreach (var item in ShowValues)
                {
                    if (Compare(item, value, format) == 0)
                    {
                        return true;
                    }
                }

                return false;
            }

            return true;
        }

        internal static int? Compare(object value1, object value2, string format)
        {
            // consider Nullable type in future.
            if ((value1 is DBNull || value1 == null) && (value2 is DBNull || value2 == null))
            {
                return 0;
            }
            // if one of these values is null or DBNull, no data should be filtered.
            if (value1 == null || value2 == null || value1 is DBNull || value2 is DBNull)
            {
                return null;
            }

            var type1 = value1.GetType();
            var type2 = value2.GetType();
            if (type1 != type2)
            {
                if (IsNumberType(value1) && IsNumberType(value2))
                {
                    double dValue1;
                    double dValue2;
                    if (Double.TryParse(value1.ToString(), out dValue1)
                        && Double.TryParse(value2.ToString(), out dValue2))
                    {
                        return dValue1.CompareTo(dValue2);
                    }
                }
                if (type1 == typeof(bool) || type2 == typeof(bool))
                {
                    bool v1 = false, v2 = false;
                    bool.TryParse(value1.ToString(), out v1);
                    bool.TryParse(value2.ToString(), out v2);
                    return v1.CompareTo(v2);
                }
                else
                {
                    //When one of the values is string, format the other one to string, then compare them.
                    var strValue = value1 as string;
                    var rawValue = value2 as IFormattable;
                    var resultFactor = 1;
                    if (strValue == null)
                    {
                        strValue = value2 as string;
                        rawValue = value1 as IFormattable;
                        resultFactor = -1;
                    }

                    if (strValue != null && rawValue != null)
                    {
                        return string.Compare(strValue, rawValue.ToString(format, CultureInfo.CurrentCulture),
                            StringComparison.CurrentCultureIgnoreCase) * resultFactor;
                    }
                }
            }
            else if (type1 == typeof(string))
            {
                return string.Compare(value1 as string, value2 as string, StringComparison.CurrentCultureIgnoreCase);
            }

            IComparable icValue1 = value1 as IComparable;
            if (icValue1 != null)
            {
                return icValue1.CompareTo(value2);
            }

            throw new InvalidOperationException(Resources.FlexGridFilterCompareError);
        }

        internal static bool IsNumberType(object value)
        {
            if (value == null)
            {
                return false;
            }
            Type vType = value.GetType();
            if (vType == typeof(float)
                || vType == typeof(int)
                || vType == typeof(uint)
                || vType == typeof(double)
                || vType == typeof(sbyte)
                || vType == typeof(byte)
                || vType == typeof(long)
                || vType == typeof(ulong)
                || vType == typeof(ushort)
                || vType == typeof(short)
                || vType == typeof(decimal))
            {
                return true;
            }
            return false;
        }
    }
}
