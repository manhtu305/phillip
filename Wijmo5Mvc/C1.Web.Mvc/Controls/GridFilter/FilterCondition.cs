﻿using System;
using System.Globalization;

namespace C1.Web.Mvc
{
    public partial class FilterCondition
    {
        // supported operators for different types:
        // string Operators: Equals, Does not equal, Begins with, Ends with, Contains, Does not contain
        // number Operators: Equals, Does not equal, Greater than, Greater than or equal to, Less than, Less than or equal to
        // date Operators: Equals, Less than, Greater than
        // bool Operators: Equals, Does not equal
        internal bool Apply(object value, string format)
        {
            if (!Operator.HasValue)
            {
                return true;
            }

            string columnLowerString = null;
            string lowerString = null;
            Action initStrings = () =>
            {
                columnLowerString = GetLowerString(value, format);
                lowerString = GetLowerString(Value, format);
            };

            switch (Operator.Value)
            {
                case Grid.Operator.BW:
                    initStrings();
                    if (columnLowerString != null && lowerString != null)
                    {
                        return columnLowerString.StartsWith(lowerString);
                    }
                    return false;
                case Grid.Operator.CT:
                    initStrings();
                    if (columnLowerString != null && lowerString != null)
                    {
                        return columnLowerString.Contains(lowerString);
                    }
                    return false;
                case Grid.Operator.EW:
                    initStrings();
                    if (columnLowerString != null && lowerString != null)
                    {
                        return columnLowerString.EndsWith(lowerString);
                    }
                    return false;
                case Grid.Operator.NC:
                    initStrings();
                    if (columnLowerString != null && lowerString != null)
                    {
                        return !columnLowerString.Contains(lowerString);
                    }
                    return false;
                case Grid.Operator.EQ:
                    return ValueFilter.Compare(value, Value, format) == 0;
                case Grid.Operator.GE:
                    if (ValueFilter.Compare(value, Value, format) >= 0)
                    {
                        return true;
                    }
                    return false;
                case Grid.Operator.GT:
                    if (ValueFilter.Compare(value, Value, format) > 0)
                    {
                        return true;
                    }
                    return false;
                case Grid.Operator.LE:
                    if (ValueFilter.Compare(value, Value, format) <= 0)
                    {
                        return true;
                    }
                    return false;
                case Grid.Operator.LT:
                    if (ValueFilter.Compare(value, Value, format) < 0)
                    {
                        return true;
                    }
                    return false;
                case Grid.Operator.NE:
                    return ValueFilter.Compare(value, Value, format) != 0;
            }
            return true;
        }

        private static string GetLowerString(object value, string format)
        {
            if (value == null)
            {
                return null;
            }

            var str = value as string;
            if (str != null)
            {
                return str.ToLower();
            }

            var formattable = value as IFormattable;
            if (formattable == null)
            {
                return null;
            }

            return formattable.ToString(format, CultureInfo.CurrentCulture).ToLower();
        }
    }
}

