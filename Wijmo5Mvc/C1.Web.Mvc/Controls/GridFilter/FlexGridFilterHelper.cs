﻿using C1.JsonNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace C1.Web.Mvc.GridFilter
{
    internal class FlexGridFilterHelper
    {
        private const string FLEXGRIDFILTER = "FlexGridFilter";

        /// <summary>
        /// Use FlexGridFilter to process the request data.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionView"></param>
        /// <param name="extraRequestData"></param>
        public static void ExecuteWithRequest<T>(ICollectionView<T> collectionView,
#if ASPNETCORE
        Dictionary<string, object>
#else
        Hashtable 
#endif
            extraRequestData = null)
        {
            if (collectionView == null)
            {
                return;
            }
            if (extraRequestData == null || !extraRequestData.ContainsKey(FLEXGRIDFILTER))
            {
                return;
            }
            string settings = JsonHelper.SerializeObject(extraRequestData[FLEXGRIDFILTER]);
            IList<ColumnFilter> filters = JsonHelper.DeserializeObject<List<ColumnFilter>>(settings);
            if (filters != null)
            {
                ExecuteWithFilters<T>(collectionView, filters);
            }
        }

        [Obsolete("Use \"ExecuteWithFilters<T>(ICollectionView<T> collectionView, IList<ColumnFilter> filters)\" instead.")]
        public static void ExecuteWithFilters<T>(ICollectionView<T> collectionView, IList<Filter> filters)
        {
            IList<ColumnFilter> columnFilters = filters.Select(filter => (ColumnFilter)filter).ToList();
            ExecuteWithFilters(collectionView, columnFilters);
        }

        /// <summary>
        /// Use FlexGridFilter with the specified filters. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionView"></param>
        /// <param name="filters"></param>
        public static void ExecuteWithFilters<T>(ICollectionView<T> collectionView, IList<ColumnFilter> filters)
        {
            if (collectionView == null || filters == null)
            {
                return;
            }

            var propertyInfos = new Dictionary<string, INestedPropertyInfo>();
            collectionView.Filter = item =>
            {
                foreach (var filter in filters)
                {
                    var propertyName = filter.Column;
                    INestedPropertyInfo propertyInfo;
                    if (!propertyInfos.TryGetValue(propertyName, out propertyInfo))
                    {
                        propertyInfo = typeof(T).GetNestedProperty(propertyName);
                        propertyInfos.Add(propertyName, propertyInfo);
                    }

                    if (propertyInfo == null)
                    {
                        propertyInfo = item.GetType().GetNestedProperty(propertyName);
                        if (propertyInfo == null)
                        {
                            continue;
                        }
                    }

                    var columnValue = propertyInfo.GetValue(item);
                    if (!filter.Apply(columnValue, filter.Format))
                    {
                        return false;
                    }
                }

                return true;
            };
        }
    }
}
