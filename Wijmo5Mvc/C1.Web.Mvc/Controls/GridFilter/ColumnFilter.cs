﻿namespace C1.Web.Mvc
{
    public partial class ColumnFilter
    {
        #region Internal Methods
        internal bool Apply(object value, string format)
        {
            return ValueFilter.Apply(value, format) && ConditionFilter.Apply(value, format);
        }
        #endregion Internal Methods
    }
}
