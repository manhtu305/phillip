﻿namespace C1.Web.Mvc
{
    public partial class ConditionFilter
    {
        internal bool Apply(object value, string format)
        {
            bool bc1 = Condition1.Apply(value, format);
            bool bc2 = Condition2.Apply(value, format);
            if (Condition1.Operator.HasValue && Condition2.Operator.HasValue)
            {
                return And ? (bc1 && bc2) : (bc1 || bc2);
            }
            if (Condition1.Operator.HasValue)
            {
                return bc1;
            }
            if (Condition2.Operator.HasValue)
            {
                return bc2;
            }
            return true;
        }
    }
}
