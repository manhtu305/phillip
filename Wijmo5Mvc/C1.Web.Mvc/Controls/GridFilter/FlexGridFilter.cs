﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(GridExDefinitions.Filter))]
    public partial class FlexGridFilter<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.GridFilter; }
        }
    }
}
