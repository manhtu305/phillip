﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(Definitions.Nav))]
    partial class TabPanel
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Nav;
            }
        }
    }
}
