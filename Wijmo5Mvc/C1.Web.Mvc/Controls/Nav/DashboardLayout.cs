﻿using C1.JsonNet;
using C1.Web.Mvc.WebResources;
using System.ComponentModel;
#if ASPNETCORE
using System;
using System.Linq;
using System.IO;
using System.Text.Encodings.Web;
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    [Scripts(typeof(Definitions.Nav))]
    partial class DashboardLayout
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Nav;
            }
        }

        /// <summary>
        /// Creates an instance of <see cref="LayoutBase"/>
        /// </summary>
        /// <typeparam name="TLayout">The type of the layout to be created.</typeparam>
        /// <param name="dashboard">The <see cref="DashboardLayout"/> control where the layout is applied.</param>
        /// <param name="helper">The helper object.</param>
        /// <returns>An instance created.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static TLayout GetLayout<TLayout>(DashboardLayout dashboard, HtmlHelper helper)
            where TLayout: LayoutBase
        {
            var layout = dashboard.Layout as TLayout;
            if(layout == null)
            {
                layout = ReflectUtils.GetInstanceWithHtmlHelper(typeof(TLayout), helper) as TLayout;
                dashboard.Layout = layout;
            }
            return layout;
        }

        /// <summary>
        /// Registers the startup scripts.
        /// </summary>
        /// <param name="writer">the specified writer used to write the startup scripts.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            var layout = Layout as LayoutBase;
            if (layout != null)
            {
                layout.RenderScripts(writer, false);
            }

            base.RegisterStartupScript(writer);
        }

        /// <summary>
        /// Ensures the child components created.
        /// </summary>
        protected override void CreateChildComponents()
        {
            base.CreateChildComponents();
            var layout = Layout as LayoutBase;
            if (layout != null)
            {
                Components.Add(layout);
            }
        }

#if ASPNETCORE
        public override void Render(HtmlTextWriter writer, HtmlEncoder encoder)
        {
            // render the content elements in layout.
            var layout = Layout as LayoutBase;
            if (layout != null)
            {
                layout.RenderMarkup(writer);
            }

            base.Render(writer, encoder);
        }
#endif
    }

    [Scripts(typeof(Definitions.Nav))]
    partial class LayoutBase
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Nav;
            }
        }

        /// <summary>
        /// Gets a value which represents the unique id for the control.
        /// </summary>
        [Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string UniqueId
        {
            get
            {
                return base.UniqueId;
            }
        }

#if ASPNETCORE
        /// <summary>
        /// Render the component result to the writer.
        /// </summary>
        /// <param name="writer">The specified writer used to render.</param>
        /// <param name="encoder">The Html encoder.</param>
        public override void Render(HtmlTextWriter writer, HtmlEncoder encoder)
        {
            RenderMarkup(writer);
            base.Render(writer, encoder);
        }

        internal void RenderMarkup(HtmlTextWriter htw)
        {
            using (var sw = new StringWriter())
            {
                WriteElements(sw);
                var content = sw.ToString();
                if (content.Length != 0)
                {
                    htw.Write(string.Format("<div class=\"wj-dl-conts-container\">{0}</div>", content));
                }
            }
        }

        internal virtual void WriteElements(StringWriter sw)
        {
        }

        internal virtual void CreateTileContentElement(HtmlTextWriter sw, Tile tile)
        {
            tile.CreateContentElement(sw, NewUniqueId);
        }
#endif
    }

#if ASPNETCORE
    partial class SplitLayout
    {
        internal override void WriteElements(StringWriter sw)
        {
            for (var i = 0; i < Items.Count; i++)
            {
                CreateContentElement(sw, Items[i]);
            }
        }

        private void CreateContentElement(HtmlTextWriter sw, ISplitLayoutItem item)
        {
            var tile = item as Tile;
            if(tile != null)
            {
                CreateTileContentElement(sw, tile);
                return;
            }

            var group = item as SplitGroup;
            if (group != null)
            {
                for(var i=0; i<group.Children.Count; i++)
                {
                    CreateContentElement(sw, group.Children[i]);
                }
            }
        }
    }

    partial class AutoGridLayout
    {
        internal override void WriteElements(StringWriter sw)
        {
            for (var i = 0; i < Items.Count; i++)
            {
                var group = Items[i];
                for (var j = 0; j < group.Children.Count; j++)
                {
                    CreateTileContentElement(sw, group.Children[j]);
                }
            }
        }

    }

    partial class ManualGridLayout
    {
        internal override void WriteElements(StringWriter sw)
        {
            for (var i = 0; i < Items.Count; i++)
            {
                var group = Items[i];
                for (var j = 0; j < group.Children.Count; j++)
                {
                    CreateTileContentElement(sw, group.Children[j]);
                }
            }
        }

    }

    partial class FlowLayout
    {
        internal override void WriteElements(StringWriter sw)
        {
            for(var i=0; i<Items.Count; i++)
            {
                CreateTileContentElement(sw, Items[i]);
            }
        }
    }

    partial class Tile
    {
        internal string InnerContent { get; set; }

        internal void CreateContentElement(HtmlTextWriter sw, Func<string> newUniqueId)
        {
            if (string.IsNullOrEmpty(Content) && !string.IsNullOrEmpty(InnerContent))
            {
                var uniqueId = newUniqueId();
                sw.WriteLine(string.Format("<div id=\"{0}\" style=\"width: 100 %;height:100%\">{1}</div>", uniqueId, InnerContent));
                InnerContent = null;
                Content = string.Format("#{0}", uniqueId);
            }
        }
    }
#endif
}
