﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(GridExDefinitions.Selector))]
    public partial class Selector<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.GridSelector; }
        }
    }
}
