﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(GridExDefinitions.BooleanChecker))]
    public partial class BooleanChecker<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.GridSelector; }
        }
    }
}
