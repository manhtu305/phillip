﻿using C1.Web.Mvc.Serialization;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(ChartExDefinitions.Annotation))]
    public partial class AnnotationLayer<T>
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.ChartAnnotation;
            }
        }
    
        internal override bool UseClientInitialize
        {
            get { return false; }
        }

        internal override string ClientConstructorArgs
        {
            get { return GetTargetInstance() + "," + SerializeItems(); }
        }

        /// <summary>
        /// Serializes the AnnotationLayer items to a Json string.
        /// </summary>
        /// <returns>A JSON string representation of the items.</returns>
        private string SerializeItems()
        {
            return JsonConvertHelper.Serialize(Items, true, true);
        }
    }
}
