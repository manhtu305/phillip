﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(GridExDefinitions.Search))]
    public partial class FlexGridSearch
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.GridSearch; }
        }
    }
}
