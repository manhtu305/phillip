﻿using System;
using C1.JsonNet;
using C1.Web.Mvc;
using C1.Web.Mvc.Localization;
#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Define the service converter.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class ServiceConverter : JsonConverter
    {
        private const string STARTTEXT = @"/Service(";
        private const string ENDTEXT = @")/";

        private readonly bool _serviceExisted;
        private readonly string _serviceCtor;
        private readonly string _serviceOpts;

        /// <summary>
        /// Create an instance of ServiceConverter
        /// </summary>
        public ServiceConverter()
            : this(true, null, null)
        {
        }

        /// <summary>
        /// Create an instance of ServiceConverter
        /// </summary>
        /// <param name="serviceExisted">Specify the service instance has been created.</param>
        public ServiceConverter(bool serviceExisted)
            : this(serviceExisted, null, null)
        {
        }

        /// <summary>
        /// Create an instance of ServiceConverter
        /// </summary>
        /// <param name="serviceExisted">Specify the service instance has been created.</param>
        /// <param name="serviceCtor">Specify the service constructor in client side.</param>
        public ServiceConverter(bool serviceExisted, string serviceCtor)
            : this(serviceExisted, serviceCtor, null)
        {
        }

        /// <summary>
        /// Create an instance of ServiceConverter
        /// </summary>
        /// <param name="serviceExisted">Specify the service instance has been created.</param>
        /// <param name="serviceCtor">Specify the service constructor in client side.</param>
        /// <param name="serviceOpts">Specify the service options.</param>
        public ServiceConverter(bool serviceExisted, string serviceCtor, string serviceOpts)
        {
            _serviceExisted = serviceExisted;
            _serviceCtor = serviceCtor;
            _serviceOpts = serviceOpts;
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(string) || typeof(Service).IsAssignableFrom(objectType))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value to be serialized.</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            string text = GetWriteText(value, !writer.JsonSetting.IsJavascriptFormat);
            if (writer.JsonSetting.IsJavascriptFormat)
            {
                writer.WriteRawValue(text);
            }
            else
            {
                writer.WriteValue(text);
            }
        }

        private string GetWriteText(object value, bool isJsonFormat = true)
        {
            string strText = isJsonFormat ? STARTTEXT : "";
            string serviceKey;
            string serviceCtor = null;
            string serviceOpts = null;
            ObtainServiceArgs(value, isJsonFormat, out serviceKey, out serviceCtor, out serviceOpts);
            strText += Service.GetClientServiceExpression(serviceKey, serviceCtor, serviceOpts);
            strText += isJsonFormat ? ENDTEXT : "";
            return strText;
        }

        protected virtual void ObtainServiceArgs(object value, bool isJsonFormat,
            out string serviceKey, out string serviceCtor, out string serviceOpts)
        {
            serviceKey = null;
            serviceCtor = null;
            serviceOpts = null;
            if (value is string)
            {
                serviceKey = (string)value;
                if (!_serviceExisted)
                {
                    serviceCtor = _serviceCtor;
                    // for template, the serviceKey(the dom element's id) is also the option setting for its constructor.
                    serviceOpts = (isJsonFormat ? _serviceOpts : (_serviceOpts ?? ("'" + serviceKey + "'")));
                }
            }
            else if (value is Service)
            {
                Service service = value as Service;
                serviceKey = service.Id;
                if (!_serviceExisted)
                {
                    serviceCtor = service.ClientComponent;
                    serviceOpts = service.SerializeOptions();
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException("value", Resources.ConverterInvalidTypeOfValue);
            }
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existedValue">The existing value of object being read.</param>
        /// <returns>The object value after deserializing.</returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            return existedValue;
        }
    }
}