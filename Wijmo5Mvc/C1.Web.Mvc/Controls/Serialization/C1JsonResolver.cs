﻿using System;
using C1.JsonNet.Converters;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// The JsonResolver for serializing options of C1 MVC controls.
    /// </summary>
    internal class C1ClientEventJsonResolver : C1ClientEventResolver
    {
        public override BaseConverter ResolveConverter(string name, object value, Type type, IContext context)
        {
            return new FunctionConverter();
        }
    }
}