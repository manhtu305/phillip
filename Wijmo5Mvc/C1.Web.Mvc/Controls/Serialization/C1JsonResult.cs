﻿#if ASPNETCORE
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Threading.Tasks;
#if NETCORE3
using Microsoft.AspNetCore.Http.Features;
#endif
#else
using System;
using C1.Web.Mvc.Localization;
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// C1 json result.
    /// </summary>
    internal class C1JsonResult : JsonResult
    {
        private bool _useCamelCasePropertyName = true;
        private const string GetHttpMethodName = "GET";
        private const string JsonContentType = "application/json";
        private static string[] DefaultMediaTypeList = new string[] { JsonContentType };
        public bool UseCamelCasePropertyName
        {
            get { return _useCamelCasePropertyName; }
            set { _useCamelCasePropertyName = value; }
        }

#if ASPNETCORE
        public C1JsonResult(object value)
            : base(value)
        {
        }

        public override Task ExecuteResultAsync(ActionContext context)
        {
// TFS 404266
#if NETCORE3
            var syncIOFeature = context.HttpContext.Features.Get<IHttpBodyControlFeature>();
            if (syncIOFeature != null)
            {
                syncIOFeature.AllowSynchronousIO = true;
            }
#endif
            // Below code is referred to "Microsoft.AspNet.Mvc.JsonResult, 
            // Microsoft.AspNet.Mvc.Formatters.Json\6.0.0-beta7\lib\dnx451\Microsoft.AspNet.Mvc.Formatters.Json.dll"
            var response = context.HttpContext.Response;          
            // ASP.NET Core 2.0 changes constructor from MediaTypeHeaderValue(String) to MediaTypeHeaderValue(StringSegment),
            // And MVC control based on core 1.0.  To fix this breaking changes, use ParseList to create instance.
            var mediaTypeHeaderValue = MediaTypeHeaderValue.ParseList(DefaultMediaTypeList)[0];
            mediaTypeHeaderValue.Encoding = Encoding.UTF8;
            response.ContentType = mediaTypeHeaderValue.ToString();
            if (StatusCode.HasValue)
            {
                response.StatusCode = StatusCode.Value;
            }

            response.Write(GetSerializationText(Value));
            return Task.FromResult(true);
        }
#else

    public override void ExecuteResult(ControllerContext context)
        {
            // Below code is referred to "System.Web.Mvc, Version=3.0.0.0"
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet && string.Equals(context.HttpContext.Request.HttpMethod, GetHttpMethodName, StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException(Resources.C1JsonResultGetIsNotAllowed);
            }

            var response = context.HttpContext.Response;
            response.ContentType = string.IsNullOrEmpty(ContentType) ? JsonContentType : ContentType;
            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data != null)
            {
                // Use C1 Json serializition
                response.Write(GetSerializationText(Data));
            }
        }
#endif

        private string GetSerializationText(object data)
        {
            return JsonConvertHelper.Serialize(data, useCamelCasePropertyName: UseCamelCasePropertyName, skipDefault: false);
        }
    }
}