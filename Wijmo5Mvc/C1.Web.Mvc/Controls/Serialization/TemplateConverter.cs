﻿using System;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the template converter.
    /// </summary>
    internal class TemplateConverter : ServiceConverter
    {
        #region Fields
        private bool _isTemplateId = false;
        private const string C1_TEMPLATE_CTOR = "c1.mvc.Template";
        private const string C1_TEMPLATE_TAG = "C1TemplateContent";
        #endregion Fields

        #region Ctors
        public TemplateConverter(bool isTemplateId)
        {
            _isTemplateId = isTemplateId;
        }

        public TemplateConverter()
            : this(false)
        {
        }
        #endregion Ctors

        #region Methods
        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(string))
            {
                return true;
            }
            return false;
        }

        protected override void ObtainServiceArgs(object value, bool isJsonFormat, 
            out string serviceKey, out string serviceCtor, out string serviceOpts)
        {
            serviceKey = string.Empty;
            serviceCtor = C1_TEMPLATE_CTOR;
            serviceOpts = null;
            var strValue = (string)value;
            if (_isTemplateId)
            {
                serviceKey = strValue;
                serviceOpts = "['" + serviceKey + "',true]";
            }
            else
            {
                serviceOpts = strValue.Replace("\\", "\\\\").Replace("\r", @"\r").Replace("\n", @"\n");
                serviceOpts = serviceOpts.Replace("'", "\\'").Replace("\"", "\\\"");
                serviceOpts = string.Format("'<{0}>{1}</{0}>'", C1_TEMPLATE_TAG, serviceOpts);
            }
        }
        #endregion Methods
    }
}
