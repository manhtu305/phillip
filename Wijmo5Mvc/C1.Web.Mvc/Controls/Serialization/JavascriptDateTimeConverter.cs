﻿using C1.JsonNet;
using C1.Web.Mvc.Localization;
using System;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Converts a <see cref="DateTime"/> or <seealso cref="DateTimeOffset"/> to a JavaScript date constructor (e.g. new Date(52231943)).
    /// </summary>
    /// <remarks>
    /// This converter is used to convert a <see cref="DateTime"/> or <seealso cref="DateTimeOffset"/> instance to a javascript date constructor.
    /// It is always used to serialize the value when the page is loaded at the first time.
    /// This converter is only used to serialize.
    /// </remarks>
    /// <summary>
    /// Define the service converter.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class JavascriptDateTimeConverter : JsonConverter
    {
        /// <summary>
        /// Gets whether an object supports serialization or deserialization with current converter.
        /// </summary>
        /// <param name="objectType">The object type.</param>
        /// <returns>If true, the converter is supported. Otherwise, it is not supported.</returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) || objectType == typeof(DateTimeOffset);
        }

        /// <summary>
        /// Serialize the object.
        /// </summary>
        /// <param name="writer">The serialization writer.</param>
        /// <param name="value">The value to be serialized.</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            if (value == null)
            {
                writer.WriteValue(null);
                return;
            }
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                writer.WriteRawValue(GetJSDateTimeText(dt));
                return;
            }

            if (value is DateTimeOffset)
            {
                DateTimeOffset dateTimeOffset = (DateTimeOffset)value;
                writer.WriteRawValue(GetJSDateTimeText(dateTimeOffset.DateTime));
                return;
            }

            throw new FormatException(Resources.ConverterInvalidTypeOfValue);
        }

        /// <summary>
        /// Overrides to not support deserialization with this converter.
        /// </summary>
        /// <param name="reader">The deserialization reader.</param>
        /// <param name="objectType">The object type.</param>
        /// <param name="existedValue">The existed value.</param>
        /// <returns>The object.</returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            // Because this converter is used to serialize in the page first load, 
            // and when postback or callback, the default converter for Date will be used,
            // the deserilzation should never be used for this converter.
            throw new NotImplementedException(Resources.ConverterDeserializationIsNotImplemented);
        }

        private string GetJSDateTimeText(DateTime dt)
        {
            var dateTimeText = JsonUtility.GetDateTimeText(dt, DateTimeKind.Unspecified);

            return string.Format("new Date(\"{0}\")", dateTimeText);
        }

    }
}
