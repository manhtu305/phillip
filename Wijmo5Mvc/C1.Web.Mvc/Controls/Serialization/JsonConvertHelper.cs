﻿using System.Text;
#if ASPNETCORE
using Microsoft.AspNetCore.Mvc;
#else
using System.Web.Mvc;
#endif
using C1.JsonNet;
using System.ComponentModel;

#region ** Obsolete this wrong namespace
namespace C1.Web.Mvc.Serializition
{
    /// <summary>
    /// The json convert helper.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class JsonConvertHelper
    {
#if ASPNETCORE
        /// <summary>
        ///  Creates a Microsoft.AspNetCore.Mvc.JsonResult object that serializes the specified
        ///  data object to JSON.
        /// </summary>
        /// <param name="controller">The specified controller.</param>
        /// <param name="data">The object to serialize.</param>
        /// <returns>
        /// The created Microsoft.AspNetCore.Mvc.JsonResult that serializes the specified data
        /// to JSON format for the response.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static JsonResult C1Json(this Controller controller, object data)
        {
            return C1.Web.Mvc.Serialization.JsonConvertHelper.C1Json(controller, data);
        }
#else
        /// <summary>
        /// Serialize data to json for C1 data.
        /// </summary>
        /// <param name="controller">The controller</param>
        /// <param name="data">The data</param>
        /// <param name="contentType">The content type</param>
        /// <param name="contentEncoding">The content encoding</param>
        /// <param name="behavior">The json request behavior</param>
        /// <returns>The json result</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static JsonResult C1Json(this Controller controller, object data, string contentType = null,
            Encoding contentEncoding = null, JsonRequestBehavior behavior = JsonRequestBehavior.DenyGet)
        {
            return C1.Web.Mvc.Serialization.JsonConvertHelper.C1Json(controller, data, contentType,
                contentEncoding, behavior);
        }
#endif
    }
}
#endregion end of ** Obsolete this wrong namespace.

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// The json convert helper.
    /// </summary>
    public static class JsonConvertHelper
    {
#if ASPNETCORE
        /// <summary>
        ///  Creates a Microsoft.AspNetCore.Mvc.JsonResult object that serializes the specified
        ///  data object to JSON.
        /// </summary>
        /// <param name="controller">The specified controller.</param>
        /// <param name="data">The object to serialize.</param>
        /// <param name="useCamelCasePropertyName">A <see cref="bool"/> value decides whether to use camel case to serialize the data.</param>
        /// <returns>
        /// The created Microsoft.AspNetCore.Mvc.JsonResult that serializes the specified data
        /// to JSON format for the response.
        /// </returns>
        public static JsonResult C1Json(this Controller controller, object data, bool useCamelCasePropertyName = true)
        {
            return C1Json(data, useCamelCasePropertyName);
        }

        /// <summary>
        ///  Creates a Microsoft.AspNetCore.Mvc.JsonResult object that serializes the specified
        ///  data object to JSON.
        /// </summary>
        /// <param name="data">The object to serialize.</param>
        /// <param name="useCamelCasePropertyName">A <see cref="bool"/> value decides whether to use camel case to serialize the data.</param>
        /// <returns>
        /// The created Microsoft.AspNetCore.Mvc.JsonResult that serializes the specified data
        /// to JSON format for the response.
        /// </returns>
        public static JsonResult C1Json(object data, bool useCamelCasePropertyName = true)
        {
            return new C1JsonResult(data)
            {
                UseCamelCasePropertyName = useCamelCasePropertyName
            };
        }
#else
        /// <summary>
        /// Serialize data to json for C1 data.
        /// </summary>
        /// <param name="controller">The controller</param>
        /// <param name="data">The data</param>
        /// <param name="contentType">The content type</param>
        /// <param name="contentEncoding">The content encoding</param>
        /// <param name="behavior">The json request behavior</param>
        /// <param name="useCamelCasePropertyName">A <see cref="bool"/> value decides whether to use camel case to serialize the data.</param>
        /// <returns>The json result</returns>
        public static JsonResult C1Json(this Controller controller, object data, string contentType = null,
            Encoding contentEncoding = null, JsonRequestBehavior behavior = JsonRequestBehavior.DenyGet, bool useCamelCasePropertyName = true)
        {
            return new C1JsonResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                UseCamelCasePropertyName = useCamelCasePropertyName
            };
        }
#endif

        internal static string Serialize(object value, bool useCamelCasePropertyName = true, bool useJavascriptFormat = false, 
            bool skipDefault = true, bool useJavascriptDateTimeConverter = true)
        {
            var settings = new JsonSetting()
            {
                SkipIsDefault = skipDefault,
                IsPropertyNameCamel = useCamelCasePropertyName,
                IsJavascriptFormat = useJavascriptFormat
            };

            settings.Resolvers.Add(new C1ClientEventJsonResolver());
            if (useJavascriptFormat && useJavascriptDateTimeConverter)
            {
                settings.Converters.Add(new JavascriptDateTimeConverter());
            }
            return JsonHelper.SerializeObject(value, settings);
        }

        internal static T Deserialize<T>(string text, bool useCamelCasePropertyName = true)
        {
            var settings = new JsonSetting()
            {
                SkipIsDefault = true,
                IsPropertyNameCamel = useCamelCasePropertyName
            };

            return JsonHelper.DeserializeObject<T>(text, settings);
        }
    }
}
