﻿using C1.JsonNet;
using C1.Web.Mvc.Localization;
using System;

namespace C1.Web.Mvc.Serialization
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class DateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) ;
        }

        protected override void WriteJson(JsonWriter writer, object value)
        {
            if (value == null)
            {
                writer.WriteValue(null);
                return;
            }
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                writer.WriteText(GetDateTimeText(dt));
                return;
            }

            throw new FormatException(Resources.ConverterInvalidTypeOfValue);
        }

        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            throw new NotImplementedException(Resources.ConverterDeserializationIsNotImplemented);
        }

        private string GetDateTimeText(DateTime dt)
        {
            return string.Format("new Date({0}, {1}, {2}, {3}, {4}, {5})", dt.Year, dt.Month-1, dt.Day, dt.Hour,dt.Minute,dt.Second);
        }
    }
}
