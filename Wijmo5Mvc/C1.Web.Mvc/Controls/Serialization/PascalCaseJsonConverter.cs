﻿using C1.JsonNet;
using System;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// The json converter with Pasal Case rule.
    /// </summary>
    internal class PascalCaseJsonConverter : JsonConverter
    {
        /// <summary>
        /// Gets whether the specified type can be converted.
        /// </summary>
        /// <param name="objectType">The object type</param>
        /// <returns>The bool value indicates whether the specified type can be converted</returns>
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        /// <summary>
        /// Read the object from json.
        /// </summary>
        /// <param name="reader">The json reader</param>
        /// <param name="objectType">The object type</param>
        /// <param name="existingValue">The existing value</param>
        /// <returns>The object which is read from json</returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existingValue)
        {
            return reader.ReadValue(objectType);
        }

        /// <summary>
        /// Write the object to json format.
        /// </summary>
        /// <param name="writer">The json writer</param>
        /// <param name="value">The object</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            writer.WriteRawValue(JsonConvertHelper.Serialize(value, false, writer.JsonSetting.IsJavascriptFormat, false));
        }
    }
}
