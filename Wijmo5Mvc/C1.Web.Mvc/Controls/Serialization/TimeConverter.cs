﻿using C1.JsonNet;
using C1.Web.Mvc.Localization;
using System;

namespace C1.Web.Mvc.Serialization
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class TimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) ;
        }

        protected override void WriteJson(JsonWriter writer, object value)
        {
            if (value == null)
            {
                writer.WriteValue(null);
                return;
            }
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                writer.WriteRawText(GetTimeText(dt));
                return;
            }

            throw new FormatException(Resources.ConverterInvalidTypeOfValue);
        }

        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            throw new NotImplementedException(Resources.ConverterDeserializationIsNotImplemented);
        }

        private string GetTimeText(DateTime dt)
        {
            return dt.ToString("HH:mm:ss");
        }
    }
}
