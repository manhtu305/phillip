﻿namespace C1.Web.Mvc
{
    public partial class TreeMap<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.ChartHierarchical; }
        }
    }
}
