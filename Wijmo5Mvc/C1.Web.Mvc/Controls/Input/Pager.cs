﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(Definitions.Input))]
    public partial class Pager
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Input;
            }
        }

    }

}
