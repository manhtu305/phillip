﻿#if !ASPNETCORE
using System.Web.UI;
#else
using HtmlTextWriter = System.IO.TextWriter;
using System.Text.Encodings.Web;
#endif

namespace C1.Web.Mvc
{
    partial class AutoComplete<T>
    {
        /// <summary>
        /// Render the control or the callback result to the writer.
        /// </summary>
        /// <param name="writer">The Html writer.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        public override void Render(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        public override void Render(HtmlTextWriter writer)
#endif
        {
            //Values != null => this value is passed from AutoCompleteFor control.
            if (Values != null && Values.Length == 1)
            {
                var val = Values[0];

                if (IsEditable)
                {
                    Text = val != null ? val.ToString() : null;
                }
                else
                {
                    SelectedValue = val;
                }
            }
            base.Render(writer
#if ASPNETCORE
                , encoder
#endif
            );
        }
    }
}
