﻿using C1.Web.Mvc.WebResources;
#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#endif

namespace C1.Web.Mvc
{
    [Scripts(typeof(Definitions.Input))]
    public partial class InputBase
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Input;
            }
        }
    }
}
