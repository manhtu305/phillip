﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(Definitions.Input))]
    public partial class DropDown<T> : IDropDown
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Input;
            }
        }
    }

    [Scripts(typeof(Definitions.Input))]
    public partial class ListBox<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.Input; }
        }
    }

    [Scripts(typeof(Definitions.Input))]
    public partial class CollectionViewNavigator
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.Input; }
        }
    }

    [Scripts(typeof(Definitions.Input))]
    public partial class MultiSelectListBox<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.Input; }
        }
    }
}