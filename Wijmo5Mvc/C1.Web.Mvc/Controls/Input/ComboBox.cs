﻿using System.Linq;
#if !ASPNETCORE
using System.Web.UI;
#else
using HtmlTextWriter = System.IO.TextWriter;
using System.Text.Encodings.Web;
#endif

namespace C1.Web.Mvc
{
    public partial class ComboBoxBase<T>
    {
        /// <summary>
        /// Render the control or the callback result to the writer.
        /// </summary>
        /// <param name="writer">The Html writer.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        public override void Render(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        public override void Render(HtmlTextWriter writer)
#endif
        {
            if (_selectedItemIsSet)
            {
                var collectionView = ItemsSource as CollectionViewService<T>;
                if (collectionView != null)
                {
                    var items = collectionView.Items;
                    if (items != null)
                    {
                        SelectedIndex = items.ToList().IndexOf(SelectedItem);
                    }
                }
            }

            base.Render(writer
#if ASPNETCORE
                , encoder
#endif
            );
        }
    }

    public partial class ComboBox<T>
    {
        /// <summary>
        /// Render the control or the callback result to the writer.
        /// </summary>
        /// <param name="writer">The Html writer.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        public override void Render(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        public override void Render(HtmlTextWriter writer)
#endif
        {
            //Values != null => this value is passed from ComboBoxFor control.
            if (Values != null && Values.Length == 1)
            {
                var val = Values[0];

                if (IsEditable)
                {
                    Text = val != null ? val.ToString() : null;
                }
                else
                {
                    SelectedValue = val;
                }
            }
            base.Render(writer
#if ASPNETCORE
                , encoder
#endif
            );
        }
    }
}
