﻿using C1.Web.Mvc.WebResources;
#if ASPNETCORE
using Microsoft.AspNetCore.Html;
using System.Text.Encodings.Web;
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.WebPages;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    [Scripts(typeof(Definitions.Input))]
    public partial class Popup
    {
        /// <summary>
        /// Renders content of control.
        /// </summary>
        /// <param name="writer">The specified writer used to write the markup.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        protected override void RenderContent(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        protected override void RenderContent(HtmlTextWriter writer)
#endif
        {
            if (!string.IsNullOrEmpty(Content))
            {
                writer.Write(Content);
            }
            else if (ContentFunc != null)
            {
                var obj = ContentFunc(new object());
#if !ASPNETCORE
                var helperResult = obj as HelperResult;
                if (helperResult != null)
                {
                    writer.Write(helperResult.ToHtmlString());
                }
#else
                var htmlContent = obj as IHtmlContent;
                if (htmlContent != null)
                {
                    htmlContent.WriteTo(writer, encoder);
                }
#endif
                else if (obj != null)
                {
                    writer.Write(obj.ToString());
                }
            }

            base.RenderContent(writer
#if ASPNETCORE
                , encoder
#endif
            );
        }

        internal override string ClientSubModule
        {
            get { return ClientModules.Input; }
        }
    }
}
