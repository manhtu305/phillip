﻿using System;
using C1.JsonNet;
using System.IO;
#if ASPNETCORE
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
#if NETCORE3
using Microsoft.AspNetCore.Http.Features;
#endif
#else
using System.Web.Mvc;
using System.Globalization;
using HttpRequest = System.Web.HttpRequestBase;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines a base class of model binder for customizing.
    /// </summary>
    public abstract class ModelBinderBase : IModelBinder
    {

#if ASPNETCORE
        /// <summary>
        /// Attempts to bind a model.
        /// </summary>
        /// <param name="bindingContext">The Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingContext.</param>
        /// <returns>
        /// A System.Threading.Tasks.Task which on completion returns a Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingResult
        /// which represents the result of the model binding process.
        /// If model binding was successful, the Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingResult
        /// should be a value created with Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingResult.Success(System.String,System.Object).
        /// If model binding failed, the Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingResult
        /// should be a value created with Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingResult.Failed(System.String).
        /// If there was no data, or this model binder cannot handle the operation, the Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingContext.Result
        /// </returns>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            object model;
            var text = GetRequestContent(bindingContext.HttpContext.Request);
            var success = BindModel(text, bindingContext.ModelName, bindingContext.ModelType, bindingContext.ModelState, out model);
            bindingContext.Model = model;
            bindingContext.Result = success ? ModelBindingResult.Success(bindingContext.Model) : ModelBindingResult.Failed();
            return Task.FromResult(0);
        }
#else
        /// <summary>
        /// Attempts to bind a model.
        /// </summary>
        /// <param name="controllerContext">The controller context.</param>
        /// <param name="bindingContext">The binding context.</param>
        /// <returns>The model.</returns>
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            object model;
            var text = GetRequestContent(controllerContext.HttpContext.Request);
            BindModel(text, bindingContext.ModelName, bindingContext.ModelType, bindingContext.ModelState, out model);
            return model;
        }
#endif

        /// <summary>
        /// Gets the request content which is used for deserializing.
        /// </summary>
        /// <param name="request">The http request.</param>
        /// <returns>The content.</returns>
        protected virtual string GetRequestContent(HttpRequest request)
        {
// TFS 404266, 404280
#if NETCORE3
            var syncIOFeature = request.HttpContext.Features.Get<IHttpBodyControlFeature>();
            if (syncIOFeature != null)
            {
                syncIOFeature.AllowSynchronousIO = true;
            }
#endif

            var stream = request.
#if ASPNETCORE
                Body
#else
                InputStream
#endif
                ;
            using (var reader = new StreamReader(stream))
            {
                if (stream.CanSeek)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                }

                return reader.ReadToEnd();
            }
        }

        private bool BindModel(string text, string modelName, Type modelType, ModelStateDictionary modelState, out object model)
        {
            try
            {
                model = DeserializeModel(text, modelType);
#if !ASPNETCORE
                var state = new ModelState() { Value = new ValueProviderResult(model, text, CultureInfo.CurrentCulture) };
                modelState.Add(modelName, state);
#else
                modelState.SetModelValue(modelName, model, text);
#endif
                return true;
            }
            catch (FormatException e)
            {
#if !ASPNETCORE
                var state = new ModelState();
                state.Errors.Add(e);
                modelState.Add(modelName, state);
#else
                modelState.AddModelError(modelName, e.Message);
#endif
                model = null;
                return false;
            }
        }

        /// <summary>
        /// Deserializes the model.
        /// </summary>
        /// <param name="text">The string used to deserialize.</param>
        /// <param name="modelType">The type of the model.</param>
        /// <returns>The model.</returns>
        protected abstract object DeserializeModel(string text, Type modelType);
    }


    /// <summary>
    /// Custom model binder for C1 json request data.
    /// </summary>
    public class C1JsonRequestModelBinder : ModelBinderBase
    {
        /// <summary>
        /// Deserializes the model.
        /// </summary>
        /// <param name="text">The string used to deserialize.</param>
        /// <param name="modelType">The type of the model.</param>
        /// <returns>The model.</returns>
        protected override object DeserializeModel(string text, Type modelType)
        {
            return JsonHelper.DeserializeObject(text, modelType, new JsonSetting { IsPropertyNameCamel = true });
        }
    }

#if !ASPNETCORE
    /// <summary>
    /// Custom model binder attribute for C1 json request data.
    /// </summary>
    public class C1JsonRequestAttribute : CustomModelBinderAttribute
    {
        /// <summary>
        /// Gets the model binder.
        /// </summary>
        /// <returns>The model binder.</returns>
        public override IModelBinder GetBinder()
        {
            return new C1JsonRequestModelBinder();
        }
    }
#else
    /// <summary>
    /// Custom model binder attribute for C1 json request data.
    /// </summary>
    public class C1JsonRequestAttribute : ModelBinderAttribute
    {
        public C1JsonRequestAttribute()
        {
            BinderType = typeof(C1JsonRequestModelBinder);
        }
    }
#endif
}