﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(ChartExDefinitions.LineMarker))]
    public partial class LineMarker<T>
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Chart;
            }
        }
    }
}
