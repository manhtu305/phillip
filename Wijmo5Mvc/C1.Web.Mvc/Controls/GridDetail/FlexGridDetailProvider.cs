﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(GridExDefinitions.Detail))]
    public partial class FlexGridDetailProvider<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.GridDetail; }
        }
    }
}
