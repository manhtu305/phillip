﻿namespace C1.Web.Mvc
{
    partial class DataMap : IItemsSourceContainer<object>
    {
        internal TDataSource GetDataSource<TDataSource>()
            where TDataSource : BaseCollectionViewService<object>
        {
            return BaseCollectionViewService<object>.GetDataSource<TDataSource>(this, _helper);
        }
    }
}
