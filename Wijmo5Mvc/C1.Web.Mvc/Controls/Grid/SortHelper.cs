﻿using C1.JsonNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc.Grid
{

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class SortHelper
    {
        private const string FlexGridSoort = "FlexGridSort";
        private static IDictionary<string, Type> _sortDescriptors = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);

        static SortHelper()
        {
            AddSortDescriptor<ColumnInfo>(FlexGridSoort);
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static void AddSortDescriptor<T>(string sortKey) where T : ColumnInfo
        {
            _sortDescriptors[sortKey] = typeof(List<T>);
        }

        /// <summary>
        /// Process the extra sort settings in reqquest data.
        /// </summary>
        public static void ExecuteWithRequest<T>(CollectionViewRequest<T> requestData, Func<ColumnInfo, IEnumerable<object>> dataMapItemsSourceGetter)
        {
            if (dataMapItemsSourceGetter == null || requestData == null
                || requestData.SortDescriptions == null || requestData.SortDescriptions.Count == 0
                || requestData.ExtraRequestData == null)
                return;

            // the FlexGridSort contains list of ColumnInfos for columns which are under sorting and have data map with SortByDisplayValues=true.
            IList sortInfos = null;
            foreach(var key in _sortDescriptors.Keys)
            {
                if (!requestData.ExtraRequestData.ContainsKey(key)) continue;

                var sortData = requestData.ExtraRequestData[key];
                if (sortData != null)
                {
                    var sortSettings = JsonHelper.SerializeObject(sortData);
                    sortInfos = JsonHelper.DeserializeObject(sortSettings, _sortDescriptors[key]) as IList;
                    break;
                }
            }

            if (sortInfos == null || sortInfos.Count == 0) return;

            var columnInfos  = sortInfos.OfType<ColumnInfo>().ToList();
            foreach (var sd in requestData.SortDescriptions)
            {
                // get the column info (with data map which SortByDisplayValues is true) for specified sort description.
                var columnInfo = columnInfos.FirstOrDefault(c => c.Binding == sd.Property);
                if (columnInfo == null) continue;

                // need sort the column with data map's display values.
                // get the items source for the data map.
                var items = dataMapItemsSourceGetter(columnInfo);

                // does not support remote bind
                if (items != null)
                {
                    // to improve the performance, fetch all items in advance (by ToList()).
                    columnInfo.DataMap.ItemsSource = items.ToList();
                    // the column is sorted by the mapped display value, 
                    // specify a converter to convert the binding value to display value.
                    sd.SortConverter = columnInfo.DataMap.GetDisplayValue;
                }
            }
        }
    }
}
