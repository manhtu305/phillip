﻿using C1.JsonNet;
using C1.Web.Mvc.Localization;
using System;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines the ColumnWidthConverter class used to serialize the different type value.(number, string)
    /// </summary>
    internal class ColumnWidthConverter : JsonConverter
    {
        protected override void WriteJson(JsonWriter writer, object value)
        {
            //base.WriteJson(writer, value);
            string txt = (string)value;
            string starWidth;
            int width;
            GetRealWidth(txt, out starWidth, out width);
            if (!string.IsNullOrEmpty(starWidth))
            {
                writer.WriteValue(starWidth);
            }
            else
            {
                writer.WriteValue(width);
            }
        }

        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            return reader.Current.ToString();
        }

        internal static void GetRealWidth(string value, out string starWidth, out int width)
        {
            starWidth = null;
            width = 0;
            string txtWidth = ParseStarSize(value);
            if (!string.IsNullOrEmpty(txtWidth))
            {
                starWidth = txtWidth;
                return;
            }
            width = ParseInterger(value);
        }

        private static string ParseStarSize(string value)
        {
            int length = value.Length;
            if (length > 0 && value[length - 1] == '*')
            {
                if (length == 1)
                {
                    return value;
                }
                double dValue;
                if (double.TryParse(value.Substring(0, length - 1), out dValue))
                {
                    if (dValue > 0)
                    {
                        return value;
                    }
                }
                throw new ArgumentOutOfRangeException("value", Resources.FlexGridInvalidColumnWidth);
            }
            return null;
        }

        private static int ParseInterger(string value)
        {
            int result;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            throw new ArgumentOutOfRangeException("value", Resources.FlexGridInvalidColumnWidth);
        }
    }
}
