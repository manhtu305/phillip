﻿using System.Collections.Generic;
using System.ComponentModel;
using C1.Web.Mvc.WebResources;
using C1.Web.Mvc.Serialization;
using System;
using System.Reflection;
#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
using Microsoft.AspNetCore.Mvc.ModelBinding;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    public partial class FlexGridBase<T> : IExtendable
    {
        #region Columns

        /// <summary>
        /// Gets all columns in the grid.
        /// </summary>
        /// <returns>Collection of columns.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual IEnumerable<ColumnBase> GetFlatColumns()
        {
            return Columns;
        }

        #endregion Columns

        #region SortDescriptions
        private IList<SortDescription> _sortDescriptions;
        /// <summary>
        /// Gets the sort descriptions.
        /// </summary>
        [C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual IList<SortDescription> SortDescriptions
        {
            get { return _sortDescriptions ?? (_sortDescriptions = new List<SortDescription>()); }
        }

        internal void AddSortDescriptions(IEnumerable<SortDescription> sds)
        {
            if(sds != null)
            {
                foreach(var sd in sds)
                {
                    SortDescriptions.Add(sd);
                    ItemsSource.SortDescriptions.Add(sd);
                }
            }
        }
        #endregion SortDescriptions

        #region GroupDescriptions
        private IList<GroupDescription> _groupDescriptions;
        /// <summary>
        /// Gets the group descriptions.
        /// </summary>
        [C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual IList<GroupDescription> GroupDescriptions
        {
            get { return _groupDescriptions ?? (_groupDescriptions = new List<GroupDescription>()); }
        }

        internal void AddGroupDescriptions(IEnumerable<GroupDescription> gds)
        {
            if(gds != null)
            {
                foreach(var gd in gds)
                {
                    GroupDescriptions.Add(gd);
                    ItemsSource.GroupDescriptions.Add(gd);
                }
            }
        }
        #endregion GroupDescriptions

        #region Unobtrusive Validation
        private IDictionary<string, IDictionary<string, object>> _validationAttributesMap;

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual bool SupportsUnobtrusiveValidation
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the map of the validation attributes.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public IDictionary<string, IDictionary<string, object>> ValidationAttributesMap
        {
            get
            {
                if (!SupportsUnobtrusiveValidation)
                {
                    return null;
                }

                if (_validationAttributesMap != null)
                {
                    return _validationAttributesMap;
                }

                var propertiesMetadata = GetMetadatas();
                if (propertiesMetadata != null)
                {
                    _validationAttributesMap = new Dictionary<string, IDictionary<string, object>>();
                    foreach (var prop in propertiesMetadata)
                    {
                        var validationAttrs = Helper.GetUnobtrusiveValidationAttributes(prop.PropertyName, prop);
                        if(validationAttrs != null && validationAttrs.Count > 0)
                        {
                            _validationAttributesMap[prop.PropertyName] = validationAttrs;
                        }
                    }
                }

                return _validationAttributesMap;
            }
        }

        private IEnumerable<ModelMetadata> GetMetadatas()
        {
            Type elementType = null;
            if (typeof(T) != typeof(object))
            {
                elementType = typeof(T);
            }
            else if (ItemsSource != null && ItemsSource.SourceCollection != null)
            {
                elementType = ItemsSource.SourceCollection.GetType().GetGenericArguments()[0];
            }

            if(elementType == null || elementType == typeof(object))
            {
                // avoid breaking changes, we still use Model.
                var modelMetadata = Helper.ViewContext.ViewData.ModelMetadata;
                if (modelMetadata == null)
                {
                    return null;
                }
#if ASPNETCORE
                if(modelMetadata.ElementMetadata != null)
                {
                    return modelMetadata.ElementMetadata.Properties;
                }
#else
                if ((modelMetadata.ModelType != typeof(string)))
                {
                    if (modelMetadata.ModelType.IsArray)
                    {
                        elementType = modelMetadata.ModelType.GetElementType();
                    }
                    else
                    {
                        var type = ClosedGenericMatcher.ExtractGenericInterface(modelMetadata.ModelType, typeof(IEnumerable<>));
                        elementType = (type != null) ? type.GetGenericArguments()[0] : null;
                        if (elementType == null)
                        {
                            elementType = typeof(object);
                        }
                    }
                }
#endif
            }

            if(elementType == null)
            {
                return null;
            }

#if ASPNETCORE
            return Helper.GetMetadataForProperties(elementType);
#else

            var provider = new DataAnnotationsModelMetadataProvider();
            return provider.GetMetadataForProperties(null, elementType);
#endif
        }
        #endregion

        #region PageSize
        /// <summary>
        /// Make ItemsSource pagable with specified page size. 0 means to disable paging.
        /// </summary>
        [Serialization.C1Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual int PageSize
        {
            get;
            set;
        }
#endregion PageSize

#region Render
        /// <summary>
        /// Override to render the extender startup scripts.
        /// </summary>
        /// <param name="writer">The specified writer</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {            
            BaseCollectionViewService<object> collectionViewService;
            foreach(var column in GetFlatColumns())
            {
                if (column.DataMap.ShouldSerializeItemsSource())
                {
                    collectionViewService = column.DataMap.ItemsSource as BaseCollectionViewService<object>;
                    if (collectionViewService != null)
                    {
                        collectionViewService.RenderScripts(writer, false);
                    }
                }
            }

            base.RegisterStartupScript(writer);
            foreach (var extender in Extenders)
            {
                FlexGridFilter<T> filter = extender as FlexGridFilter<T>;
                if (filter != null)
                {
                    foreach (var columnFilter in filter.ColumnFilters)
                    {
                        var dataMap = columnFilter.ValueFilter.DataMap;
                        if (dataMap.ShouldSerializeItemsSource())
                        {
                            collectionViewService = dataMap.ItemsSource as BaseCollectionViewService<object>;
                            if (collectionViewService != null)
                            {
                                collectionViewService.RenderScripts(writer, false);
                            }
                        }
                    }
                }
             
                extender.RenderScripts(writer, false);
            }
        }
#endregion Render

#if !ASPNETCORE
#region callback
        internal override bool HandleCallBack(CallbackManager cbm)
        {
            var cv = ItemsSource as CollectionViewService<T>;
            if (cv != null)
            {
                cv.BeginningQuery += ItemSource_BeginningQuery;
            }
            return base.HandleCallBack(cbm);
        }

        private void ItemSource_BeginningQuery(CollectionViewRequest<T> requestData)
        {
            Grid.SortHelper.ExecuteWithRequest(requestData, (columnInfo) =>
            {
                var col = GetColumn(columnInfo);
                if (col == null || col.DataMap == null || !col.DataMap.SortByDisplayValues) return null;
                return col.DataMap.ItemsSource.SourceCollection;
            });
        }

        /// <summary>
        /// Gets column by the specified infomation.
        /// </summary>
        /// <param name="columnInfo">The column info.</param>
        /// <returns>The column meets the specified column info.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual ColumnBase GetColumn(ColumnInfo columnInfo)
        {
            if (columnInfo.Index < 0 || columnInfo.Index >= Columns.Count) return null;
            return Columns[columnInfo.Index];
        }
#endregion
#endif

    }

    [Scripts(typeof(Definitions.Grid))]
    public partial class FlexGrid<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.Grid; }
        }

        internal override bool SupportsUnobtrusiveValidation
        {
            get
            {
                return true;
            }
        }
    }
}
