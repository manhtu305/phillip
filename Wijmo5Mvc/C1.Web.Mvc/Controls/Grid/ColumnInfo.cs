﻿using System;
using System.Collections.Generic;
using System.Linq;
#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents a column info that describes a column on the grid.
    /// </summary>
    public class ColumnInfo
    {
        private DataMapInfo _dataMap;

        /// <summary>
        /// Initializes a new instance of <see cref="ColumnInfo"/> object.
        /// </summary>
        public ColumnInfo()
        {
            Index = -1;
        }

        /// <summary>
        /// Gets or sets the index of the column in the grid's columns collection.
        /// </summary>
        public virtual int Index { get; set; }

        /// <summary>
        /// Gets or sets the name of the property this column is bound to.
        /// </summary>
        public string Binding { get; set; }

        /// <summary>
        /// Gets the data map info used to convert raw values into display values for the column.
        /// </summary>
        public DataMapInfo DataMap
        {
            get
            {
                return _dataMap ?? (_dataMap = new DataMapInfo());
            }
        }
    }

    /// <summary>
    /// Represents a data map info for use with the ColumnInfo's DataMap property.
    /// </summary>
    public class DataMapInfo
    {
        /// <summary>
        /// Gets or sets the name of the property to use as a key for the item (data value).
        /// </summary>
        public string SelectedValuePath { get; set; }

        /// <summary>
        /// Gets or sets the name of the property to use as the visual representation of the item.
        /// </summary>
        public string DisplayMemberPath { get; set; }

        /// <summary>
        /// For internal used. Gets or sets the items source.
        /// </summary>
        internal IList<object> ItemsSource { get; set; }

        /// <summary>
        /// Gets the display value that corresponds to a given value.
        /// </summary>
        /// <param name="value">The value of the item to retrieve.</param>
        /// <returns>The display value that corresponds the give value.</returns>
        /// <remarks>
        /// The string-based comparison of value has the same logic as Wijmo 5. 
        /// </remarks>
        internal object GetDisplayValue(object value)
        {
            if (string.IsNullOrEmpty(SelectedValuePath)
                || (ItemsSource == null || ItemsSource.Count == 0))
                return value;

            var svalue = (value == null || value == DBNull.Value) ? "" : value.ToString();

            var firstItem = ItemsSource[0];
            var valueProperty = firstItem == null ? null : firstItem.GetType().GetProperty(SelectedValuePath);
            var matchItem = ItemsSource.FirstOrDefault(item =>
            {
                if (item == null) return false;
                var currentValueProperty = valueProperty ?? item.GetType().GetProperty(SelectedValuePath);
                var currentValue = currentValueProperty == null ? null : currentValueProperty.GetValue(item, null);
                
                // first straight comparison
                if (Object.Equals(value, currentValue)) return true;
                
                // then string-based comparison (sase sensitive) 
                var scvalue = (currentValue == null || currentValue == DBNull.Value) ? "" : currentValue.ToString();
                return svalue == scvalue;
            });
            
            // cannot find the item with the specified key value.
            if (matchItem == null) return value;

            // get the display value
            var displayValueProperty = matchItem.GetType().GetProperty(DisplayMemberPath);
            var displayValue = displayValueProperty == null ? null : displayValueProperty.GetValue(matchItem, null);
            return displayValue;
        }
    }
}
