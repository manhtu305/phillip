﻿#if !ASPNETCORE
using System;
using System.Collections.Specialized;
using C1.Web.Mvc.Serialization;
using System.Web;
using System.Web.Helpers;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Callback manager.
    /// </summary>
    public sealed class CallbackManager
    {
        private readonly HttpContextBase _httpContext;
        private NameValueCollection _params;

        private const string KeyPrefix = "wj-";
        private const string IsCallbackKey = KeyPrefix + "cbk";
        private const string UniqueIdKey = KeyPrefix + "id";
        private const string DataKey = KeyPrefix + "data";

        internal CallbackManager(HttpContextBase context)
        {
            _httpContext = context;
        }

        private NameValueCollection Params
        {
            get
            {
                return _params ?? (_params = GetParams(_httpContext.Request));
            }
        }

        internal bool IsCallback
        {
            get { return IsRequestCallback(Params); }
        }

        internal string UniqueId
        {
            get
            {
                return Params[UniqueIdKey];
            }
        }

        private static NameValueCollection GetParams(HttpRequestBase request)
        {
            if (!IsPost(request))
            {
                return null;
            }

            try
            {
                return request.Params;
            }
            catch (HttpRequestValidationException)
            {
                return request.Unvalidated().Form;
            }
        }

        private static bool IsPost(HttpRequestBase request)
        {
            var method = request.HttpMethod;
            return string.Equals(method, "POST", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Get the deserialized data.
        /// </summary>
        /// <typeparam name="T">The type of the data</typeparam>
        /// <returns>The data</returns>
        public T GetDeserializedData<T>()
        {
            return JsonConvertHelper.Deserialize<T>(GetRequestRawData(Params));
        }

        private static string GetRequestRawData(NameValueCollection paramStrings)
        {
            if(paramStrings == null)
            {
                return null;
            }

            return paramStrings[DataKey];
        }

        private static bool IsRequestCallback(NameValueCollection paramStrings)
        {
            if (paramStrings == null)
            {
                return false;
            }

            return paramStrings[IsCallbackKey] == "true";
        }

        private void Write(string contentType, Action write)
        {
            _httpContext.Response.Clear();
            _httpContext.Response.ContentType = contentType;
            write();
            _httpContext.Response.End();
        }

        /// <summary>
        /// Write data as json to the response.
        /// </summary>
        /// <param name="data">The data</param>
        public void WriteJson(object data)
        {
            Write("application/json", () => _httpContext.Response.Write(JsonConvertHelper.Serialize(data)));
        }

        /// <summary>
        /// Write the content to the response.
        /// </summary>
        /// <param name="content">The content</param>
        /// <param name="contentType">The content type</param>
        public void WriteContent(string content, string contentType = "text/plain")
        {
            Write(contentType, () => _httpContext.Response.Write(content));
        }

        /// <summary>
        /// Get the deserialized data from the request.
        /// </summary>
        /// <returns>The deserialized data</returns>
        public static T GetCurrentCallbackData<T>()
        {
            return JsonConvertHelper.Deserialize<T>(GetRequestRawData(HttpContext.Current.Request.Params));
        }

        /// <summary>
        /// Gets if current request is callback.
        /// </summary>
        public static bool CurrentIsCallback
        {
            get { return IsRequestCallback(HttpContext.Current.Request.Params); }
        }

        /// <summary>
        /// Get the text data from the request.
        /// </summary>
        /// <returns>The text data</returns>
        public string GetRequestTextData()
        {
            _httpContext.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            using (var sr = new System.IO.StreamReader(_httpContext.Request.InputStream))
            {
                return sr.ReadToEnd();
            }
        }

    }
}
#endif