﻿namespace C1.Web.Mvc
{
    partial class ChartSeriesBase<T> : IItemsSourceContainer<T>
    {
        internal TDataSource GetDataSource<TDataSource>()
            where TDataSource : BaseCollectionViewService<T>
        {
            return BaseCollectionViewService<T>.GetDataSource<TDataSource>(this, _owner != null ? _owner.Helper : null);
        }
    }
}
