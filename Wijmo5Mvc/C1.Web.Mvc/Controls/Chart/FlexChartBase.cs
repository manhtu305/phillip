﻿using C1.Web.Mvc.WebResources;
#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    [Scripts(typeof(Definitions.Chart))]
    public abstract partial class FlexChartBase<T>: IExtendable
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.Chart;
            }
        }

        /// <summary>
        /// Registers the startup scripts.
        /// </summary>
        /// <param name="writer">the specified writer used to write the startup scripts.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            base.RegisterStartupScript(writer);
            foreach (var extender in Extenders)
            {
                extender.RenderScripts(writer, false);
            }
        }
    }
}