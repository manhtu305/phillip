﻿using System.Collections.Generic;
#if ASPNETCORE
using Color = System.String;
#else
using System.Linq;
using System.Drawing;
#endif

namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// A series of pre-defined palettes for chart.
    /// </summary>
    public static class Palettes
    {
        private readonly static IEnumerable<string> _standard = new[] { "#88bde6", "#fbb258", "#90cd97", "#f6aac9", "#bfa554", "#bc99c7", "#eddd46", "#f07e6e", "#8c8c8c" };

        private readonly static IEnumerable<string> _cocoa = new[] { "#466bb0", "#c8b422", "#14886e", "#b54836", "#6e5944", "#8b3872", "#73b22b", "#b87320", "#141414" };

        private readonly static IEnumerable<string> _coral = new[] { "#84d0e0", "#f48256", "#95c78c", "#efa5d6", "#ba8452", "#ab95c2", "#ede9d0", "#e96b7d", "#888888" };

        private readonly static IEnumerable<string> _dark = new[] { "#005fad", "#f06400", "#009330", "#e400b1", "#b65800", "#6a279c", "#d5a211", "#dc0127", "#000000" };

        private readonly static IEnumerable<string> _highcontrast = new[] { "#ff82b0", "#0dda2c", "#0021ab", "#bcf28c", "#19c23b", "#890d3a", "#607efd", "#1b7700", "#000000" };

        private readonly static IEnumerable<string> _light = new[] { "#ddca9a", "#778deb", "#778deb", "#b5eae2", "#7270be", "#a6c7a7", "#9e95c7", "#95b0c7", "#9b9b9b" };

        private readonly static IEnumerable<string> _midnight = new[] { "#83aaca", "#e37849", "#14a46a", "#e097da", "#a26d54", "#a584b7", "#d89c54", "#e86996", "#2c343b" };

        private readonly static IEnumerable<string> _minimal = new[] { "#92b8da", "#e2d287", "#accdb8", "#eac4cb", "#bbbb7a", "#cab1ca", "#cbd877", "#dfb397", "#c8c8c8" };

        private readonly static IEnumerable<string> _modern = new[] { "#2d9fc7", "#ec993c", "#89c235", "#e377a4", "#a68931", "#a672a6", "#d0c041", "#e35855", "#68706a" };

        private readonly static IEnumerable<string> _organic = new[] { "#9c88d9", "#a3d767", "#8ec3c0", "#e9c3a9", "#91ab36", "#d4ccc0", "#61bbd8", "#e2d76f", "#80715a" };

        private readonly static IEnumerable<string> _slate = new[] { "#7493cd", "#f99820", "#71b486", "#e4a491", "#cb883b", "#ae83a4", "#bacc5c", "#e5746a", "#505d65" };
        
        /// <summary>
        /// The standard(default) palette.
        /// </summary>
        public readonly static IEnumerable<Color> Standard;

        /// <summary>
        /// Cocoa palette.
        /// </summary>
        public readonly static IEnumerable<Color> Cocoa;

        /// <summary>
        /// Coral palette.
        /// </summary>
        public readonly static IEnumerable<Color> Coral;

        /// <summary>
        /// Dark palette.
        /// </summary>
        public readonly static IEnumerable<Color> Dark;

        /// <summary>
        /// Highcontrast palette.
        /// </summary>
        public readonly static IEnumerable<Color> Highcontrast;

        /// <summary>
        /// Light palette.
        /// </summary>
        public readonly static IEnumerable<Color> Light;

        /// <summary>
        /// Midnight palette.
        /// </summary>
        public readonly static IEnumerable<Color> Midnight;

        /// <summary>
        /// Minimal palette.
        /// </summary>
        public readonly static IEnumerable<Color> Minimal;

        /// <summary>
        /// Modern palette.
        /// </summary>
        public readonly static IEnumerable<Color> Modern;

        /// <summary>
        /// Organic palette.
        /// </summary>
        public readonly static IEnumerable<Color> Organic;

        /// <summary>
        /// Slate palette.
        /// </summary>
        public readonly static IEnumerable<Color> Slate;
        static Palettes()
        {
            Standard = GetReadOnlyValue(_standard);
            Cocoa = GetReadOnlyValue(_cocoa);
            Coral = GetReadOnlyValue(_coral);
            Dark = GetReadOnlyValue(_dark);
            Highcontrast = GetReadOnlyValue(_highcontrast);
            Light = GetReadOnlyValue(_light);
            Midnight = GetReadOnlyValue(_midnight);
            Minimal = GetReadOnlyValue(_minimal);
            Modern = GetReadOnlyValue(_modern);
            Organic = GetReadOnlyValue(_organic);
            Slate = GetReadOnlyValue(_slate);
        }

        private static IEnumerable<Color> GetReadOnlyValue(IEnumerable<string> value)
        {
            return value
#if !ASPNETCORE
                .Select(ColorTranslator.FromHtml).ToList()
#endif
                ;
        }
    }
}
