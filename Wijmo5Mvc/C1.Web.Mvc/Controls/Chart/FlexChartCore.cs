﻿#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    public abstract partial class FlexChartCore<T>
    {
        /// <summary>
        /// Registers the startup scripts.
        /// </summary>
        /// <param name="writer">the specified writer used to write the startup scripts.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            BaseCollectionViewService<T> collectionViewService;
            for (int i = 0; i < Series.Count; i++)
            {
                if (Series[i].ShouldSerializeItemsSource())
                {
                    collectionViewService = Series[i].ItemsSource as BaseCollectionViewService<T>;
                    if (collectionViewService != null)
                    {
                        collectionViewService.RenderScripts(writer, false);
                    }
                }
                if (Series[i].AxisX != null && Series[i].AxisX.ShouldSerializeItemsSource())
                {
                    collectionViewService = Series[i].AxisX.ItemsSource as BaseCollectionViewService<T>;
                    if (collectionViewService != null)
                    {
                        collectionViewService.RenderScripts(writer, false);
                    }
                }
                if (Series[i].AxisY != null && Series[i].AxisY.ShouldSerializeItemsSource())
                {
                    collectionViewService = Series[i].AxisY.ItemsSource as BaseCollectionViewService<T>;
                    if (collectionViewService != null)
                    {
                        collectionViewService.RenderScripts(writer, false);
                    }
                }
            }
            if (_axisX != null && _axisX.ShouldSerializeItemsSource())
            {
                collectionViewService = _axisX.ItemsSource as BaseCollectionViewService<T>;
                if (collectionViewService != null)
                {
                    collectionViewService.RenderScripts(writer, false);
                }
            }
            if (_axisY != null && _axisY.ShouldSerializeItemsSource())
            {
                collectionViewService = _axisY.ItemsSource as BaseCollectionViewService<T>;
                if (collectionViewService != null)
                {
                    collectionViewService.RenderScripts(writer, false);
                }
            }

            base.RegisterStartupScript(writer);

        }
    }
}
