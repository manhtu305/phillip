﻿namespace C1.Web.Mvc
{
    public partial class Sunburst<T> : FlexPieBase<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.ChartHierarchical; }
        }
    }
}
