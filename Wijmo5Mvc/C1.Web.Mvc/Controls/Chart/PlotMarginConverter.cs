﻿using C1.JsonNet;
using System;

namespace C1.Web.Mvc.Controls.Chart
{
    /// <summary>
    /// Defines the PlotMarginConverter class used to serialize the different type value.(number, string)
    /// </summary>
    internal class PlotMarginConverter : JsonConverter
    {
        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value to be serialized.</param>
        protected override void WriteJson(JsonWriter writer, object value)
        {
            if (!(value is string))
            {
                return;
            }

            var margin = (string)value;

            try
            {
                writer.WriteValue(int.Parse(margin));
            }
            catch
            {
                if (!string.IsNullOrEmpty(margin))
                {
                    writer.WriteValue(margin);
                }
            }
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existedValue">The existing value of object being read.</param>
        /// <returns>The object value after deserializing.</returns>
        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            return reader.Current.ToString();
        }
    }
}
