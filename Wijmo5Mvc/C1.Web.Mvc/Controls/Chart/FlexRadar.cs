﻿namespace C1.Web.Mvc
{
    public partial class FlexRadar<T>
    {
        internal override string ClientSubModule
        {
            get { return ClientModules.ChartRadar; }
        }
    }
}