﻿using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    [Scripts(typeof(ChartExDefinitions.Animation))]
    public partial class ChartAnimation<T>
    {
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.ChartAnimation;
            }
        }

        internal override bool UseClientInitialize
        {
            get
            {
                return false;
            }
        }

        internal override string ClientConstructorArgs
        {
            get { return GetTargetInstance() + "," + SerializeOptions(); }
        }
    }
}
