﻿namespace C1.Web.Mvc
{
    public partial class ChartGestures<T>
    {
        #region Properties
        internal override string ClientSubModule
        {
            get
            {
                return ClientModules.ChartInteraction;
            }
        }

        internal override bool UseClientInitialize
        {
            get
            {
                return false;
            }
        }

        internal override string ClientConstructorArgs
        {
            get
            {
                return GetTargetInstance() + "," + SerializeOptions();
            }
        }
        #endregion Properties
    }
}
