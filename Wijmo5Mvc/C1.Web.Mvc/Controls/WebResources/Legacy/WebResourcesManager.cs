﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines a class to manage and register the resources.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use Scripts and Styles control instead.")]
    public class WebResourcesManager : WebResourcesManagerBase
    {
        internal static readonly IList<Type> AllControlTypes = new List<Type>
        {
            typeof(C1Input),
            typeof(C1Chart),
            typeof(C1LineMarker),
            typeof(C1AnnotationLayer),
            typeof(C1ChartInteraction),
            typeof(C1ChartAnimation),
            typeof(C1Grid),
            typeof(C1GridFilter),
            typeof(C1GridGroupPanel),
            typeof(C1GridDetailProvider),
            typeof(C1Gauge),
            typeof(C1CollectionView)
        };

        /// <summary>
        /// Create an instance of <see cref="WebResourcesManager"/>.
        /// </summary>
        /// <param name="helper">The html helper.</param>
        public WebResourcesManager(HtmlHelper helper) : base(helper)
        {
        }

        /// <summary>
        /// Gets or sets the theme name to be used.
        /// </summary>
        public string Theme
        {
            get;
            set;
        }

        internal override IList<Type> GetAllControlTypes()
        {
            return AllControlTypes;
        }

        /// <summary>
        /// Creates the child components.
        /// </summary>
        protected override void CreateChildComponents()
        {
            var styles = new Styles(Helper);
            styles.Theme = Theme;
            Components.Add(styles);
            base.CreateChildComponents();
        }
    }
}