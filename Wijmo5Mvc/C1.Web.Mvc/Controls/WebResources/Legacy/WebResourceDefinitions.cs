﻿using System;
using System.ComponentModel;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines C1 CollectionViewService related js and css.
    /// </summary>
    [Scripts(typeof(Definitions.CollectionView))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1CollectionView
    {
    }

    /// <summary>
    /// Defines C1 Input Controls related js and css.
    /// </summary>
    [Scripts(typeof(Definitions.Input))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1Input
    {
    }

    /// <summary>
    /// Defines C1 Service related js and css.
    /// </summary>
    [Scripts(typeof(Definitions.Gauge))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1Gauge
    {
    }

    #region Chart

    /// <summary>
    /// Defines C1 Chart related js and css.
    /// </summary>
    [Scripts(typeof(Definitions.Chart), typeof(ChartExDefinitions.LineMarker), typeof(Definitions.ChartInteraction))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1Chart
    {
    }

    /// <summary>
    /// Defines C1 LineMarker related js and css.
    /// </summary>
    [Scripts(typeof(ChartExDefinitions.LineMarker))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1LineMarker
    {
    }

    /// <summary>
    /// Defines C1 AnnotationLayer related js and css.
    /// </summary>
    [Scripts(typeof(ChartExDefinitions.Annotation))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1AnnotationLayer
    {
    }

    /// <summary>
    /// Defines C1 RangeSelector related js and css.
    /// </summary>
    [Scripts(typeof(Definitions.ChartInteraction))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1RangeSelector
    {
    }

    /// <summary>
    /// Defines C1 ChartAnimation related js and css.
    /// </summary>
    [Scripts(typeof(ChartExDefinitions.Animation))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1ChartAnimation
    {
    }

    /// <summary>
    /// Defines C1 ChartInteractions related js and css.
    /// </summary>
    [Scripts(typeof(Definitions.ChartInteraction))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1ChartInteraction
    {
    }

    #endregion

    #region Grid

    /// <summary>
    /// Defines C1 Grid related js and css.
    /// </summary>
    [Scripts(typeof(Definitions.Grid), typeof(GridExDefinitions.Detail))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1Grid
    {
    }

    /// <summary>
    /// Defines C1 FlexGridFilter related js and css.
    /// </summary>
    [Scripts(typeof(GridExDefinitions.Filter))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1GridFilter
    {
    }

    /// <summary>
    /// Defines C1 FlexGridGroupPanel related js and css.
    /// </summary>
    [Scripts(typeof(GridExDefinitions.GroupPanel))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1GridGroupPanel
    {
    }

    /// <summary>
    /// Defines C1 FlexGridDetailProvider related js and css.
    /// </summary>
    [Scripts(typeof(GridExDefinitions.Detail))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use ScriptsBuilder instead.")]
    public abstract class C1GridDetailProvider
    {
    }

    #endregion

}
