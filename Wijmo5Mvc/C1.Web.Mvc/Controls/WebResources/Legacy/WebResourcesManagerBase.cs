﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using System.Text.Encodings.Web;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines the base class to manage and register the resources.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("Please use Scripts control instead.")]
    public abstract class WebResourcesManagerBase : Component
    {
        private IList<Type> _controlTypes;

        /// <summary>
        /// Initializes a new instance of the WebResourcesManagerBase class.
        /// </summary>
        public WebResourcesManagerBase(HtmlHelper helper): base(helper)
        {
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual IList<Type> GetAllControlTypes()
        {
            return new List<Type>();
        }

        /// <summary>
        /// Gets the type collection of the controls which resources will be registered.
        /// </summary>
        /// <remarks>
        /// When there is no item in the collection, all the resources will be registered.
        /// </remarks>
        public IList<Type> ControlTypes
        {
            get { return _controlTypes ?? (_controlTypes = new List<Type>()); }
        }

        /// <summary>
        /// Render the control to the writer.
        /// </summary>
        /// <param name="writer">The specified writer used to render.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
#endif
        public override void Render(HtmlTextWriter writer
#if ASPNETCORE
            , HtmlEncoder encoder
#endif
            )
        {
            foreach (var component in Components)
            {
                component.Render(writer
#if ASPNETCORE
                    , encoder
#endif
                    );
            }
        }

        /// <summary>
        /// Creates the child components.
        /// </summary>
        protected override void CreateChildComponents()
        {
            var scripts = new Scripts(Helper);
            var types = ControlTypes.Any() ? ControlTypes : GetAllControlTypes();
            scripts.OwnerTypes.AddRange(types);
            scripts.InAssembly = GetType().Assembly();
            Components.Add(scripts);
        }
    }
}
