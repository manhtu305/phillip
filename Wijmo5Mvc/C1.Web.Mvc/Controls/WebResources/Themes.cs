﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines the names of supported themes.
    /// </summary>
    public static class Themes
    {
        /// <summary>
        /// Default theme.
        /// </summary>
        public const string Default = "default";

        /// <summary>
        /// Cerulean theme.
        /// </summary>
        public const string Cerulean = "cerulean";

        /// <summary>
        /// Clean dark theme.
        /// </summary>
        public const string CleanDark = "cleandark";

        /// <summary>
        /// Clean light theme.
        /// </summary>
        public const string CleanLight = "cleanlight";

        /// <summary>
        /// Cocoa theme.
        /// </summary>
        public const string Cocoa = "cocoa";

        /// <summary>
        /// Coral theme.
        /// </summary>
        public const string Coral = "coral";

        /// <summary>
        /// Cyborg theme.
        /// </summary>
        public const string Cyborg = "cyborg";

        /// <summary>
        /// Dark theme.
        /// </summary>
        public const string Dark = "dark";

        /// <summary>
        /// Darkly theme.
        /// </summary>
        public const string Darkly = "darkly";

        /// <summary>
        /// Flatly theme.
        /// </summary>
        public const string Flatly = "flatly";

        /// <summary>
        /// Gray scale theme.
        /// </summary>
        public const string GrayScale = "grayscale";

        /// <summary>
        /// High contrast theme.
        /// </summary>
        public const string HighContrast = "highcontrast";

        /// <summary>
        /// Light theme.
        /// </summary>
        public const string Light = "light";

        /// <summary>
        /// Material theme.
        /// </summary>
        public const string Material = "material";

        /// <summary>
        /// Midnight theme.
        /// </summary>
        public const string Midnight = "midnight";

        /// <summary>
        /// Minimal theme.
        /// </summary>
        public const string Minimal = "minimal";

        /// <summary>
        /// Modern theme.
        /// </summary>
        public const string Modern = "modern";

        /// <summary>
        /// Office theme.
        /// </summary>
        public const string Office = "office";

        /// <summary>
        /// Organic theme.
        /// </summary>
        public const string Organic = "organic";

        /// <summary>
        /// Simplex theme.
        /// </summary>
        public const string Simplex = "simplex";

        /// <summary>
        /// Slate theme.
        /// </summary>
        public const string Slate = "slate";

        /// <summary>
        /// Super hero theme.
        /// </summary>
        public const string SuperHero = "superhero";

        /// <summary>
        /// Trust theme.
        /// </summary>
        public const string Trust = "trust";

        /// <summary>
        /// Zen theme.
        /// </summary>
        public const string Zen = "zen";
    }
}
