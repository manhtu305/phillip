﻿namespace C1.Web.Mvc
{
    internal static class ThemeManager
    {
        public const string GlobalKey = "C1:Theme";
        private static string _global;
        private static readonly object _globalLocker = new object();

        public static string Global
        {
            get
            {
                EnsureGlobal();
                return _global;
            }
            set
            {
                _global = value ?? string.Empty;
            }
        }

        private static void EnsureGlobal()
        {
            if (_global == null)
            {
                lock (_globalLocker)
                {
                    if (_global == null)
                    {
#if ASPNETCORE
                        _global = string.Empty;
#else
                        _global = System.Web.Configuration.WebConfigurationManager.AppSettings[GlobalKey] ?? string.Empty;
#endif
                    }
                }
            }
        }
    }
}