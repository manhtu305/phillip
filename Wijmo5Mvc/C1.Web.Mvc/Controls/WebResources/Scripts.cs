﻿using C1.Web.Mvc.WebResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using System.Text.Encodings.Web;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the C1 Scripts control which is used for registering scripts.
    /// </summary>
    internal class Scripts : Component
    {
        /// <summary>
        /// Create an instance of <see cref="Scripts"/> with an html helper.
        /// </summary>
        /// <param name="helper">The html helper.</param>
        public Scripts(HtmlHelper helper) : base(helper)
        {
            OwnerTypes = new List<Type>();
        }

        /// <summary>
        /// Gets or sets the culture which is used for scripts localization.
        /// </summary>
        public string Culture
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the types which need to be registered the dependent scripts.
        /// </summary>
        public List<Type> OwnerTypes
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the Assembly which the Scripts control only registers the scripts in the specified assembly.
        /// </summary>
        public Assembly InAssembly
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a boolean value indicates whether current component has the scripts which need to be rendered.
        /// </summary>
        protected override bool HasScripts
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Render the control or the callback result to the writer.
        /// </summary>
        /// <param name="writer">The specified writer used to render.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        public override void Render(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        public override void Render(HtmlTextWriter writer)
#endif
        {
            base.Render(writer
#if ASPNETCORE
                , encoder
#endif
                );

            var keys = GetOwnerTypeKeys().ToList();
            if (keys == null || !keys.Any())
            {
                return;
            }

            var cultureResKey = WebResourcesHelper.GetResKey(WebResourcesHelper.GetCultureJs(Culture));
            if (cultureResKey.HasValue)
            {
                keys.Add(cultureResKey.Value);
            }

            var mvcCultureResKey = WebResourcesHelper.GetResKey(WebResourcesHelper.GetMvcCultureJs(Culture));
            if (mvcCultureResKey.HasValue)
            {
                keys.Add(mvcCultureResKey.Value);
            }

            var resUrl = C1WebMvcControllerHelper.CreateWebResourcesUrl(keys, UrlHelper);
            writer.RenderBeginTag("script", "src", resUrl);
            writer.RenderEndTag("script");
        }

        private IEnumerable<int> GetOwnerTypeKeys()
        {
            if (OwnerTypes != null && OwnerTypes.Any())
            {
                return WebResourcesHelper.GetOwnerTypeKeys(OwnerTypes, InAssembly);
            }

            var deferredScriptsManager = HttpContext.GetDeferredScriptsManager();
            if (deferredScriptsManager != null)
            {
                return WebResourcesHelper.GetOwnerTypeKeys(deferredScriptsManager.ScriptOwnerTypes);
            }

            return new int[] { };
        }
    }
}