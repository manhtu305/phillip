﻿#if ASPNETCORE
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
#else
using HttpContext = System.Web.HttpContextBase;
#endif

namespace C1.Web.Mvc
{
    internal static class DeferredScriptsHelper
    {
        public static void SetEnableDeferredScripts(this HttpContext context, bool enable)
        {
            context.SetContextItem(DeferredScriptsManager.GlobalKey, enable);
        }

        public static bool GetEnableDeferredScripts(this HttpContext context)
        {
            if (context.ContainsContextItem(DeferredScriptsManager.GlobalKey))
            {
                var value = context.GetContextItem(DeferredScriptsManager.GlobalKey);
                if (value is bool)
                {
                    return (bool)value;
                }
            }

            return DeferredScriptsManager.Global;
        }

        public static DeferredScriptsManager GetDeferredScriptsManager(this HttpContext context)
        {
            return DeferredScriptsManager.Get(context);
        }
    }
}
