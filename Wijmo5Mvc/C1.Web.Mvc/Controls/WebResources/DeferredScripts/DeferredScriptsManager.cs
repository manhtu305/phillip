﻿using System;
using System.Collections.Generic;
using System.Linq;
#if ASPNETCORE
using Microsoft.AspNetCore.Http;
using HtmlTextWriter = System.IO.TextWriter;
#else
using HttpContext = System.Web.HttpContextBase;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    internal class DeferredScriptsManager
    {
        public const string GlobalKey = "C1:EnableDeferredScripts";
        private const string Key = "C1:DeferredScriptsManager";
        private readonly IList<Type> _scriptOwnerTypes = new List<Type>();
        private readonly HttpContext _context;
        private static bool? _global;
        private static readonly object _globalLocker = new object();
        private readonly List<Component> _components = new List<Component>();

        public DeferredScriptsManager(HttpContext context)
        {
            _context = context;
            context.SetContextItem(Key, this);
        }

        public IList<Type> ScriptOwnerTypes
        {
            get
            {
                return _scriptOwnerTypes;
            }
        }

        public void Register(Component component)
        {
            ScriptOwnerTypes.Add(component.GetType());
            _components.Add(component);
        }

        public void Unregister(Component component)
        {
            _components.Remove(component);
        }

        public void Flush(HtmlTextWriter output)
        {
            while(_components.Any())
            {
                var component = _components.First();
                if (!component.SupportDeferredScripts)
                {
                    Unregister(component);
                    continue;
                }

                component.RenderScripts(output, false);
            }

            _context.RemoveContextItem(Key);
        }

        public static DeferredScriptsManager Get(HttpContext context)
        {
            if (!context.GetEnableDeferredScripts())
            {
                return null;
            }

            DeferredScriptsManager manager = null;
            if (context.ContainsContextItem(Key))
            {
                manager = context.GetContextItem(Key) as DeferredScriptsManager;
            }

            if (manager == null)
            {
                manager = new DeferredScriptsManager(context);
            }

            return manager;
        }

        private static void EnsureGlobal()
        {
            if (!_global.HasValue)
            {
                lock (_globalLocker)
                {
                    if (!_global.HasValue)
                    {
#if ASPNETCORE
                        _global = false;
#else
                        bool config;
                        _global =
                            bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings[GlobalKey], out config)
                            ? config : false;
#endif
                    }
                }
            }
        }

        public static bool Global
        {
            get
            {
                EnsureGlobal();
                return _global.Value;
            }
            set
            {
                _global = value;
            }
        }
    }
}
