﻿#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    internal class DeferredScripts : Component
    {
        public DeferredScripts(HtmlHelper helper) : base(helper)
        {
        }

        internal override bool SupportDeferredScripts
        {
            get
            {
                return false;
            }
        }

        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            var manager = HttpContext.GetDeferredScriptsManager();
            if (manager == null)
            {
                return;
            }

            manager.Flush(writer);
        }
    }
}
