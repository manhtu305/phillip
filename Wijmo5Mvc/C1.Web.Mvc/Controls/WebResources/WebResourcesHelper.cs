﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace C1.Web.Mvc.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class WebResourcesHelper
    {
        public const string JsExt =
#if DEBUG
             ".js";
#else
            ".min.js";
#endif

        public const string CssExt =
#if DEBUG
            ".css";
#else
            ".min.css";
#endif

        public const string DefaultCulture =
#if GRAPECITY
            "ja";
#else
            "en";
#endif

        private const string FolderSeparator = ".";

        // All wijmo.culture js files are renamed for embedding them into the main assembly.
        // Because them are used according to Culture and resource dll is for UICulture.
        public const string CultureJsExt = ".culture" + JsExt;

        public const string JsContentType = "text/javascript";
        public const string CssContentType = "text/css";

#if !ASPNETCORE
        public const string BaseRoot = "C1.Web.Mvc";
#else
        public const string BaseRoot = "C1.AspNetCore.Mvc";
#endif
        public const string Root = BaseRoot + FolderSeparator + "Client" +
#if DEBUG
           "Debug"
#else
           "Release"
#endif
           + FolderSeparator;

        public const string Shared = Root + "Shared" + FolderSeparator;
        public const string Mvc = Root + "Mvc" + FolderSeparator;
        public const string Wijmo = Root + "Wijmo" + FolderSeparator;
        public const string WijmoJs = Wijmo + "controls" + FolderSeparator;
        public const string WijmoCulture = WijmoJs + "cultures" + FolderSeparator + "wijmo.culture.";
        public const string WijmoCss = Wijmo + "styles" + FolderSeparator;
        public const string WijmoTheme = WijmoCss + "themes" + FolderSeparator;
        public const string WijmoThemeFilePrefix = "wijmo.theme.";
        public const string FmThemeFilePrefix = "fm.theme.";
        public const int ResoureInfoKeyMin = 0;
        public const string MvcCulture = Root + "Cultures" + FolderSeparator + "c1.culture.";
        public const string FmTheme = Shared + "FileManagerTheme" + FolderSeparator;

        private static IDictionary<string, WebResourceInfo> _resNameDic;
        private static IDictionary<int, WebResourceInfo> _resKeyDic;
        private static readonly object ReadResInfosLocker = new object();
        private static HashSet<string> _supportedThemeResNames;
        private static HashSet<string> _supportedCultures;
        private static HashSet<string> _supportedMvcCultures;
        private static List<string> _resInfosInited = new List<string>();
        public static readonly Assembly Assembly = typeof(WebResourcesHelper).Assembly();
        private static readonly Lazy<IEnumerable<Type>> _basicOwnerTypes =
                new Lazy<IEnumerable<Type>>(() => GetAssemblyResTypes<AssemblyScriptsAttribute>());

        public static IEnumerable<Type> BasicOwnerTypes
        {
            get
            {
                return _basicOwnerTypes.Value;
            }
        }

        public static string ToThemeResName(string theme)
        {
            var res = WijmoTheme;
            var themeParts = theme.Split(FolderSeparator.ToArray()).ToArray();
            foreach (var p in themeParts.Take(themeParts.Length - 1))
            {
                res += p + FolderSeparator;
            }

            res += WijmoThemeFilePrefix + theme + CssExt;
            return res;
        }

        public static string ToThemeFmResName(string theme)
        {
            var res = FmTheme;
            var themeParts = theme.Split(FolderSeparator.ToArray()).ToArray();
            foreach (var p in themeParts.Take(themeParts.Length - 1))
            {
                res += p + FolderSeparator;
            }

            res += FmThemeFilePrefix + theme + CssExt;
            return res;
        }

        public static HashSet<string> SupportedCultures
        {
            get
            {
                EnsureResInfos();
                return _supportedCultures;
            }
        }

        public static HashSet<string> SupportedMvcCultures
        {
            get
            {
                EnsureResInfos();
                return _supportedMvcCultures;
            }
        }

        public static HashSet<string> SupportedThemeResNames
        {
            get
            {
                EnsureResInfos();
                return _supportedThemeResNames;
            }
        }

        public static IDictionary<int, WebResourceInfo> ResKeyDic(Assembly assembly = null)
        {
            EnsureResInfos(assembly);
            return _resKeyDic;
        }

        public static IDictionary<string, WebResourceInfo> ResNameDic(Assembly assembly = null)
        {
            EnsureResInfos(assembly);
            return _resNameDic;
        }

        public static string GetCultureJs(string culture)
        {
            var supportedCultures = SupportedCultures;
            var supportedCulture = GetCultureNames(culture).FirstOrDefault(c => !string.IsNullOrEmpty(c) && supportedCultures.Contains(c)) ?? DefaultCulture;

            return WijmoCulture + supportedCulture + CultureJsExt;
        }

        public static string GetMvcCultureJs(string culture)
        {
            var supportedCultures = SupportedMvcCultures;
            var supportedCulture = GetCultureNames(culture).FirstOrDefault(c => !string.IsNullOrEmpty(c) && supportedCultures.Contains(c)) ?? DefaultCulture;
            return MvcCulture + supportedCulture + CultureJsExt;
        }

        private static IEnumerable<string> GetCultureNames(string culture)
        {
            yield return culture;
            yield return CultureInfo.CurrentCulture.Name;
            yield return CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
        }

        public static WebResourceInfo GetResInfo(int key)
        {
            return _resKeyDic[key];
        }

        public static int? GetResKey(string name, Assembly assembly = null)
        {
            EnsureResInfos(assembly);
            var info = _resNameDic[name];
            return info == null ? (int?)null : info.Key;
        }

        private static void EnsureResInfos(Assembly assembly = null)
        {
            assembly = assembly ?? Assembly;
            if (_resInfosInited.Contains(assembly.FullName))
            {
                return;
            }

            lock (ReadResInfosLocker)
            {
                if (_resInfosInited.Contains(assembly.FullName))
                {
                    return;
                }

                if (_resInfosInited.Count == 0)
                {
                    _resNameDic = new Dictionary<string, WebResourceInfo>(StringComparer.OrdinalIgnoreCase);
                    _resKeyDic = new Dictionary<int, WebResourceInfo>();
                    _supportedThemeResNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                    _supportedCultures = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                    _supportedMvcCultures = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                }

                IEnumerable<string> resources;
                try
                {
                    resources = assembly.GetManifestResourceNames()
                        .Where(s => s.StartsWith(BaseRoot, StringComparison.OrdinalIgnoreCase));
                }
                catch
                {
                    _resInfosInited.Add(assembly.FullName);
                    return;
                }

                var key = _resInfosInited.Count == 0 ? ResoureInfoKeyMin : _resKeyDic.Count;
                foreach (var res in resources)
                {
                    string contentType;
                    if (res.EndsWith(JsExt, StringComparison.OrdinalIgnoreCase))
                    {
                        contentType = JsContentType;
                    }
                    else if (res.EndsWith(CssExt, StringComparison.OrdinalIgnoreCase))
                    {
                        contentType = CssContentType;
                    }
                    else
                    {
                        continue;
                    }

                    if (_resNameDic.ContainsKey(res))
                    {
                        continue;
                    }

                    var info = new WebResourceInfo(key++, res, contentType, assembly);
                    _resNameDic.Add(res, info);
                    _resKeyDic.Add(info.Key, info);

                    AddSupportedThemeResName(res, contentType);
                    AddSupportedFmThemeResName(res, contentType);
                    AddSupportedCultures(res, contentType);
                    AddSupportedMvcCultures(res, contentType);
                }


                _resInfosInited.Add(assembly.FullName);
            }
        }

        private static void AddSupportedCultures(string res, string contentType)
        {
            if (contentType != JsContentType || !res.StartsWith(WijmoCulture, StringComparison.OrdinalIgnoreCase))
                return;

            var culturePrefixLength = WijmoCulture.Length;
            var culture = res.Substring(culturePrefixLength);
            var separatorIndex = culture.IndexOf('.');
            if (separatorIndex < 0)
            {
                return;
            }

            _supportedCultures.Add(culture.Substring(0, separatorIndex));
        }

        private static void AddSupportedMvcCultures(string res, string contentType)
        {
            if (contentType != JsContentType || !res.StartsWith(MvcCulture, StringComparison.OrdinalIgnoreCase))
                return;
            
            var culture = res.Substring(MvcCulture.Length);
            var separatorIndex = culture.IndexOf('.');
            if (separatorIndex < 0)
            {
                return;
            }

            _supportedMvcCultures.Add(culture.Substring(0, separatorIndex));
        }

        private static void AddSupportedThemeResName(string res, string contentType)
        {
            if (contentType != CssContentType || !res.StartsWith(WijmoTheme, StringComparison.OrdinalIgnoreCase))
                return;

            _supportedThemeResNames.Add(res);
        }

        private static void AddSupportedFmThemeResName(string res, string contentType)
        {
            if (contentType != CssContentType || !res.StartsWith(FmTheme, StringComparison.OrdinalIgnoreCase))
                return;

            _supportedThemeResNames.Add(res);
        }

        public static Lazy<IList<Type>> GetAssemblyResTypesLazy<T>(Assembly asm) where T : AssemblyResAttribute
        {
            return new Lazy<IList<Type>>(() =>
                asm.GetCustomAttributes(typeof(T), true)
                .Cast<T>().SelectMany(s => s.Dependencies).ToList());
        }

        public static IEnumerable<Type> GetAssemblyResTypes<T>(Assembly asm = null) where T : AssemblyResAttribute
        {
            return (asm ?? Assembly).GetCustomAttributes(typeof(T), true).Cast<T>().SelectMany(s => s.Dependencies);
        }

        public static IEnumerable<int> GetAssmeblyOwnerTypeKeys(Assembly asm = null)
        {
            var types = GetAssemblyResTypes<AssemblyScriptsAttribute>(asm ?? Assembly);
            return GetOwnerTypeKeys(types);
        }

        public static IEnumerable<int> GetOwnerTypeKeys(IEnumerable<Type> resDependencies, Assembly inAsm = null)
        {
            var resources = GetTypeResources<ScriptsAttribute>(resDependencies, inAsm);
            return resources.Select(r => GetResKey(r.Resource, r.Location)).Where(i => i.HasValue).Select(i => i.Value);
        }

        public static IEnumerable<ResLocation> GetTypeResources<T>(IEnumerable<Type> resDependencies, Assembly typeInAsm = null)
            where T : ResDependenciesAttribute
        {
            return GetResources<T>(resDependencies, typeInAsm, typeInAsm != null);
        }

        private static IEnumerable<ResLocation> GetResources<T>(IEnumerable<object> resDependencies, Assembly asm = null, bool specifiedAsm = false, string extension = "")
            where T : ResDependenciesAttribute
        {
            return resDependencies.SelectMany(dependency =>
            {
                var type = dependency as Type;
                if (type == null)
                {
                    return new[] { new ResLocation(dependency.ToString() + extension, asm) };
                }

                if (specifiedAsm && type.Assembly() != asm)
                {
                    return new ResLocation[] { };
                }

                var resAttr = type.GetCustomAttributes(typeof(T), true).Cast<T>().FirstOrDefault();
                var res = (resAttr == null ? new ResLocation[] { } : GetResources<T>(resAttr.Dependencies, type.Assembly(), specifiedAsm, resAttr.ResExtension));
                var baseType = type.BaseType();
                if (baseType == null || (specifiedAsm && baseType.Assembly() != asm))
                {
                    return res;
                }

                return type.BaseType() == null ? res : GetResources<T>(new[] { type.BaseType() }, baseType.Assembly(), specifiedAsm).Concat(res);
            }).Distinct(ResLocation.IgnoreResNameCase);
        }
    }
}