﻿using System;
using System.Linq;
using C1.Web.Mvc.WebResources;
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using System.Text.Encodings.Web;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif

namespace C1.Web.Mvc
{
    internal class Styles : Component
    {
        private static readonly Lazy<int[]> _combinedStyleKeys = new Lazy<int[]>(GetCombinedStyleKeys);

        public Styles(HtmlHelper helper) : base(helper)
        {
        }

        /// <summary>
        /// Gets a boolean value indicates whether current component has the scripts which need to be rendered.
        /// </summary>
        protected override bool HasScripts
        {
            get
            {
                return false;
            }
        }

        public string Theme
        {
            get;
            set;
        }

        /// <summary>
        /// Render the control or the callback result to the writer.
        /// </summary>
        /// <param name="writer">The specified writer used to render.</param>
#if ASPNETCORE
        /// <param name="encoder">The Html encoder.</param>
        public override void Render(HtmlTextWriter writer, HtmlEncoder encoder)
#else
        public override void Render(HtmlTextWriter writer)
#endif
        {
            base.Render(writer
#if ASPNETCORE
                , encoder
#endif
                );

            var keys = _combinedStyleKeys.Value.ToList();
            var theme = GetTheme();
            if (!string.IsNullOrEmpty(theme) && !theme.Equals(Themes.Default))
            {
                var themeRes = WebResourcesHelper.ToThemeResName(theme);
                if (WebResourcesHelper.SupportedThemeResNames.Contains(themeRes))
                {
                    var themeKey = WebResourcesHelper.GetResKey(themeRes);
                    if (themeKey.HasValue) keys.Add(themeKey.Value);
                }

                var fmThemeRes = WebResourcesHelper.ToThemeFmResName(theme);
                if (WebResourcesHelper.SupportedThemeResNames.Contains(fmThemeRes))
                {
                    var fmThemeKey = WebResourcesHelper.GetResKey(fmThemeRes);
                    if (fmThemeKey.HasValue) keys.Add(fmThemeKey.Value);
                }
            }

            var combinedStyleUrl = C1WebMvcControllerHelper.CreateWebResourcesUrl(keys, UrlHelper);
            Action<HtmlTextWriter> addAttrs = w =>
            {
                w.AddAttribute("href", combinedStyleUrl);
                w.AddAttribute("rel", "stylesheet");
            };

            writer.RenderBeginTag("link", addAttrs);
            writer.RenderEndTag("link");
        }

        private string GetTheme()
        {
            if (!string.IsNullOrEmpty(Theme))
            {
                return Theme;
            }

            return ThemeManager.Global;
        }

        private static int[] GetCombinedStyleKeys()
        {
            var types = WebResourcesHelper.GetAssemblyResTypes<AssemblyStylesAttribute>();
            var resources = WebResourcesHelper.GetTypeResources<StylesAttribute>(types);
            return resources.Select(r => WebResourcesHelper.GetResKey(r.Resource, r.Location)).Where(k => k != null).Select(i => i.Value).ToArray();
        }
    }
}
