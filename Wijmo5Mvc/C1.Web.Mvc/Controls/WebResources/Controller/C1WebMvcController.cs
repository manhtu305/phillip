﻿using System.ComponentModel;

#if ASPNETCORE
using Microsoft.AspNetCore.Mvc;
using ActionResult = Microsoft.AspNetCore.Mvc.IActionResult;
#else
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc
{
    /// <summary>
    /// The controller for c1 web mvc controls.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
#if ASPNETCORE
    [Route("C1WebMvc")]
#endif
    public class C1WebMvcController : Controller
    {
        /// <summary>
        /// Gets the js and css resources for the c1 web mvc controls by the specified key.
        /// </summary>
        /// <param name="r">The key of the resources</param>
        /// <returns>The resources</returns>
#if ASPNETCORE
        [Route("WebResources")]
#endif
        public ActionResult WebResources(string r)
        {
            var keyList = C1WebMvcControllerHelper.IntsFromResourcesKey(r);
            return C1WebMvcControllerHelper.GetActionResult(keyList, HttpContext.Request);
        }
    }
}
