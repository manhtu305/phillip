﻿using System;
using System.Collections.Generic;
using System.Linq;
#if ASPNETCORE
using Microsoft.AspNetCore.Mvc;
using ControllerContext = Microsoft.AspNetCore.Mvc.ActionContext;
#else
using System.Web;
using System.Web.Mvc;
#endif

namespace C1.Web.Mvc
{
    internal class WebResourcesResult : ActionResult
    {
        private readonly List<WebResourceInfo> _resources;
        private DateTime? _resourcesModificationDate;

        public WebResourcesResult(IEnumerable<WebResourceInfo> resources = null, DateTime? resourcesModificationDate = null)
        {
            _resources = new List<WebResourceInfo>();
            if (resources != null)
            {
                _resources.AddRange(resources);
            }

            _resourcesModificationDate = resourcesModificationDate;
        }

        public IList<WebResourceInfo> Resources
        {
            get { return _resources; }
        }

        public DateTime ResourcesModificationDate
        {
            get { return (DateTime)(_resourcesModificationDate ?? (_resourcesModificationDate = C1WebMvcControllerHelper.GetResourcesModificationDate())); }
            set { _resourcesModificationDate = value; }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            if (!Resources.Any())
            {
                return;
            }

            var res = context.HttpContext.Response;
            const int expiresDays = 365;
            var expires = new TimeSpan(expiresDays, 0, 0, 0);

#if ASPNETCORE
            res.Headers["Last-Modified"] = ResourcesModificationDate.ToString("R", System.Globalization.DateTimeFormatInfo.InvariantInfo);
            res.Headers["Expires"] = ResourcesModificationDate.AddDays(expiresDays).ToString("R", System.Globalization.DateTimeFormatInfo.InvariantInfo);
            res.Headers["Cache-Control"] = string.Format("public,max-age={0}", expires.Seconds);
#else
            var cache = res.Cache;
            cache.SetCacheability(HttpCacheability.Public);
            cache.SetLastModified(ResourcesModificationDate);
            cache.SetExpires(ResourcesModificationDate.AddDays(expiresDays));
            cache.SetMaxAge(expires);
#endif

            res.ContentType = Resources.First().ContentType;
            foreach (var info in Resources)
            {
                info.WriteContent(res.
#if ASPNETCORE
                    Body
#else
                    OutputStream
#endif
                    );
            }
        }
    }
}
