﻿using C1.Web.Mvc.WebResources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Net;
#if ASPNETCORE
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ActionResult = Microsoft.AspNetCore.Mvc.IActionResult;
using HttpStatusCodeResult = Microsoft.AspNetCore.Mvc.StatusCodeResult;
using UrlHelper = Microsoft.AspNetCore.Mvc.IUrlHelper;
#else
using System.Web.Mvc;
using HttpRequest = System.Web.HttpRequestBase;
#endif

namespace C1.Web.Mvc
{
    internal class C1WebMvcControllerHelper
    {
        private const string _resActionName = "WebResources";
        private const string _controllerName = "C1WebMvc";
        private const string _resParamName = "r";
        private static bool? _useUshortTypeKey;

        private static bool UseUshortTypeKey
        {
            get
            {
                if (!_useUshortTypeKey.HasValue)
                {
                    Debug.Assert(WebResourcesHelper.ResoureInfoKeyMin == 0, "ResoureInfoKey should be zero-based.");
                    _useUshortTypeKey = WebResourcesHelper.ResKeyDic(typeof(C1WebMvcController).Assembly()).Count() - 1 > byte.MaxValue;
                }
                return _useUshortTypeKey.Value;
            }
        }

        public static string CreateWebResourcesUrl(IEnumerable<int> keys, UrlHelper urlHelper,
            string actionName = _resActionName, string controllerName = _controllerName, string paramName = _resParamName)
        {
            var resourcesKey = IntsToResourcesKey(keys);
            if (string.IsNullOrEmpty(resourcesKey))
            {
                return string.Empty;
            }

            return string.Format("{0}?{1}={2}",
                urlHelper.Action(actionName, controllerName
#if !ASPNETCORE
                , new { area = string.Empty }
#endif
                ),
                paramName, resourcesKey);
        }

        private static string IntsToResourcesKey(IEnumerable<int> keys)
        {
            if (keys == null || !keys.Any())
            {
                return string.Empty;
            }

            var bytes = new List<byte>();
            if (UseUshortTypeKey)
            {
                foreach (var keyBytes in keys.Select(k => BitConverter.GetBytes((ushort)k)))
                {
                    bytes.AddRange(keyBytes);
                }
            }
            else
            {
                bytes.AddRange(keys.Select(k => (byte)k));
            }
            var base64String = Convert.ToBase64String(bytes.ToArray());
            var urlEncodedString = Uri.EscapeDataString(base64String);
            return urlEncodedString;
        }

        public static int[] IntsFromResourcesKey(string resourcesKey)
        {
            if (string.IsNullOrEmpty(resourcesKey))
            {
                return new int[] { };
            }

            var keys = new List<int>();
            try
            {
                var bytes = Convert.FromBase64String(resourcesKey);
                if (UseUshortTypeKey)
                {
                    const int ushortTypeSize = 2;
                    for (int index = 0, length = bytes.Length; index < length; index = index + ushortTypeSize)
                    {
                        keys.Add(BitConverter.ToUInt16(bytes, index));
                    }
                }
                else
                {
                    keys.AddRange(bytes.Select(b => (int)b));
                }

                return keys.ToArray();
            }
            catch (Exception)
            {
            }
            return new int[] { };
        }

        private static IEnumerable<WebResourceInfo> GetRequestedResources(IEnumerable<int> keys)
        {
            return keys.Select(k => WebResourcesHelper.GetResInfo(k));
        }

        public static ActionResult GetActionResult(IEnumerable<int> keys, HttpRequest request)
        {
            var requestedResources = GetRequestedResources(keys).ToList();
            if (!requestedResources.Any())
            {
                return new EmptyResult();
            }

            var scriptModificationDate = GetResourcesModificationDate();
            var str = request.Headers["If-Modified-Since"];
            if (scriptModificationDate.ToString("R") == str)
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.NotModified);
            }

            try
            {
                return new WebResourcesResult(requestedResources, scriptModificationDate);
            }
#if ASPNETCORE
            catch (Exception)
#else
            catch (Exception exception)
#endif
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError
#if !ASPNETCORE
                    , exception.Message
#endif
                    );
            }
        }

#if NETCORE
        private static Version _currentAssemblyVersion;
        private static DateTime _resourcesModificationDate = DateTime.UtcNow;
        public static DateTime GetResourcesModificationDate()
        {
            var currentAssemblyVersion = WebResourcesHelper.Assembly.GetName().Version;
            if (_currentAssemblyVersion != currentAssemblyVersion)
            {
                _currentAssemblyVersion = currentAssemblyVersion;
                _resourcesModificationDate = DateTime.UtcNow;
            }

            return _resourcesModificationDate;
        }
#else
        [System.Security.SecuritySafeCritical]
        public static DateTime GetResourcesModificationDate()
        {
            var utcNow = File.GetLastWriteTime(new Uri(WebResourcesHelper.Assembly.EscapedCodeBase).LocalPath).ToUniversalTime();
            return utcNow > DateTime.UtcNow ? DateTime.UtcNow : utcNow;
        }
#endif
    }
}
