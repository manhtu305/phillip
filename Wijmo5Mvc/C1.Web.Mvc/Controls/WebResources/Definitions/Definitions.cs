﻿using C1.Web.Mvc.WebResources;

[assembly: AssemblyScripts(typeof(Definitions.All))]
[assembly: AssemblyStyles(typeof(Definitions.All))]

namespace C1.Web.Mvc.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Definitions
    {
        [Scripts(typeof(CollectionView),
            typeof(Input),
            typeof(Gauge),
            typeof(Grid),
            typeof(Chart),
            typeof(GridExDefinitions.All),
            typeof(ChartExDefinitions.All),
            typeof(Nav))]
        [Styles(typeof(Control),
            typeof(Nav))]
        public abstract class All { }

        [Styles(WebResourcesHelper.Shared + "Grid.Sheet")]
        public abstract class ExStyles { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo",
            WebResourcesHelper.Shared + "Control",
            WebResourcesHelper.Shared + "Service",
            WebResourcesHelper.Mvc + "CommonTypes",
            WebResourcesHelper.Mvc + "Util",
            WebResourcesHelper.Mvc + "Cast.wijmo",
            WebResourcesHelper.Mvc + "Cast.collections")]
        public abstract class Component { }

        public abstract class Service : Component { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.odata",
            WebResourcesHelper.Mvc + "Collections",
            WebResourcesHelper.Mvc + "OData")]
        public abstract class CollectionView : Service
        {
        }

        public abstract class Extender : Component { }

        [Styles(WebResourcesHelper.WijmoCss + "wijmo",
            WebResourcesHelper.Mvc + "Control",
            typeof(ExStyles))]
        [Scripts(WebResourcesHelper.Mvc + "Template",
            WebResourcesHelper.Mvc + "Control")]
        public abstract class Control : Component { }

        [Scripts(typeof(CollectionView),
            WebResourcesHelper.WijmoJs + "wijmo.input",
            WebResourcesHelper.Shared + "Input",
            WebResourcesHelper.Mvc + "Input",
            WebResourcesHelper.Mvc + "Cast.input")]
        public abstract class Input : Control { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.gauge",
            WebResourcesHelper.Shared + "Gauge",
            WebResourcesHelper.Mvc + "Gauge",
            WebResourcesHelper.Mvc + "Cast.gauge")]
        public abstract class Gauge : Control { }

        [Scripts(typeof(CollectionView),
            WebResourcesHelper.WijmoJs + "wijmo.chart",
            WebResourcesHelper.WijmoJs + "wijmo.chart.analytics",
            WebResourcesHelper.WijmoJs + "wijmo.chart.hierarchical",
            WebResourcesHelper.WijmoJs + "wijmo.chart.radar",
            WebResourcesHelper.WijmoJs + "wijmo.chart.render",
            WebResourcesHelper.Shared + "Chart",
            WebResourcesHelper.Mvc + "Chart",
            WebResourcesHelper.Shared + "Chart.Hierarchical",
            WebResourcesHelper.Mvc + "Chart.Hierarchical",
            WebResourcesHelper.Shared + "Chart.Radar",
            WebResourcesHelper.Mvc + "Chart.Radar",
            typeof(ChartInteraction),
            WebResourcesHelper.Mvc + "Cast.chart",
            WebResourcesHelper.Mvc + "Cast.chart.analytics",
            WebResourcesHelper.Mvc + "Cast.chart.hierarchical",
            WebResourcesHelper.Mvc + "Cast.chart.radar")]
        public abstract class Chart : Control { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.chart.interaction",
            WebResourcesHelper.Shared + "Chart.Interaction",
            WebResourcesHelper.Mvc + "Chart.Interaction",
            WebResourcesHelper.Mvc + "Cast.chart.interaction")]
        public abstract class ChartInteraction : Extender { }

        [Scripts(typeof(CollectionView),
            WebResourcesHelper.WijmoJs + "wijmo.input",
            WebResourcesHelper.WijmoJs + "wijmo.grid",
            typeof(GridXlsx),
            typeof(GridPdf),
            WebResourcesHelper.Shared + "Grid",
            WebResourcesHelper.Mvc + "Grid",
            typeof(GridExDefinitions.InnerExtensions),
            WebResourcesHelper.Mvc + "Cast.input",
            WebResourcesHelper.Mvc + "Cast.grid")]
        public abstract class Grid : Control { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.xlsx",
            WebResourcesHelper.WijmoJs + "wijmo.grid.xlsx",
            WebResourcesHelper.Mvc + "Cast.grid.xlsx")]
        public abstract class GridXlsx : Extender { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.pdf",
            WebResourcesHelper.WijmoJs + "wijmo.grid.pdf",
            WebResourcesHelper.Mvc + "Cast.pdf",
            WebResourcesHelper.Mvc + "Cast.grid.pdf")]
        public abstract class GridPdf : Extender { }

        [Styles(WebResourcesHelper.Shared + "Dashboard.Dashboard",
            WebResourcesHelper.Shared + "Filemanager")]
    [Scripts(WebResourcesHelper.WijmoJs + "wijmo.nav",
            WebResourcesHelper.WijmoJs + "wijmo.input",
            WebResourcesHelper.Shared + "Dashboard.Utils",
            WebResourcesHelper.Shared + "Dashboard.Enum",
            WebResourcesHelper.Shared + "Dashboard.DisposableObject",
            WebResourcesHelper.Shared + "Dashboard.LayoutItem",
            WebResourcesHelper.Shared + "Dashboard.Toolbar",
            WebResourcesHelper.Shared + "Dashboard.Layout",
            WebResourcesHelper.Shared + "Dashboard.Dashboard",
            WebResourcesHelper.Shared + "Filemanager",
            WebResourcesHelper.Shared + "Dashboard.Layout.FlowLayout",
            WebResourcesHelper.Shared + "Dashboard.Layout.GridLayout",
            WebResourcesHelper.Shared + "Dashboard.Layout.SplitLayout",
            WebResourcesHelper.Shared + "Nav",
            WebResourcesHelper.Mvc + "Nav",
            WebResourcesHelper.Mvc + "Cast.nav")]
        public abstract class Nav: Control { }
    }
}
