﻿namespace C1.Web.Mvc.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class GridExDefinitions
    {
        [Scripts(typeof(Filter),
            typeof(GroupPanel),
            typeof(Detail),
            typeof(Search),
            typeof(Selector),
            typeof(CellMaker))]
        public abstract class All { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.grid.filter",
            WebResourcesHelper.Shared + "Grid.Filter",
            WebResourcesHelper.Mvc + "Grid.Filter",
            WebResourcesHelper.Mvc + "Cast.grid.filter")]
        public abstract class Filter : Definitions.Extender { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.grid.grouppanel",
            WebResourcesHelper.Shared + "Grid.GroupPanel",
            WebResourcesHelper.Mvc + "Grid.GroupPanel",
            WebResourcesHelper.Mvc + "cast.grid.grouppanel")]
        public abstract class GroupPanel : Definitions.Extender { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.grid.detail",
            WebResourcesHelper.Shared + "Grid.Detail",
            WebResourcesHelper.Mvc + "Grid.Detail",
            WebResourcesHelper.Mvc + "Cast.grid.detail")]
        public abstract class Detail : Definitions.Extender { }

        [Scripts(WebResourcesHelper.Mvc + "Grid.Initializer",
            WebResourcesHelper.Mvc + "Grid.MaskLayer",
            WebResourcesHelper.Mvc + "Grid.UpdateHelper",
            WebResourcesHelper.Mvc + "Grid.Scrolling",
            WebResourcesHelper.Mvc + "Grid.Sorting",
            WebResourcesHelper.Mvc + "Grid.Template",
            WebResourcesHelper.Mvc + "Grid.Validator")]
        public abstract class InnerExtensions : Definitions.Extender { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.grid.search",
           WebResourcesHelper.Shared + "Grid.Search",
           WebResourcesHelper.Mvc + "Grid.Search",
           WebResourcesHelper.Mvc + "Cast.grid.search")]
        public abstract class Search : Definitions.Extender { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.grid.selector",
            WebResourcesHelper.Shared + "Grid.Selector",
            WebResourcesHelper.Mvc + "Grid.Selector",
            WebResourcesHelper.Mvc + "Cast.grid.selector")]
        public abstract class Selector : Definitions.Extender { }
        public abstract class BooleanChecker : Definitions.Extender { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.grid.cellmaker",
            WebResourcesHelper.Shared + "Grid.CellMaker",
            WebResourcesHelper.Mvc + "Grid.CellMaker",
            WebResourcesHelper.Mvc + "Cast.grid.cellmaker")]
        public abstract class CellMaker : Definitions.Extender { }
    }
}