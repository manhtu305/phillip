﻿namespace C1.Web.Mvc.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class ChartExDefinitions
    {
        [Scripts(typeof(Animation),
           typeof(LineMarker),
           typeof(Annotation))]
        public abstract class All { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.chart",
            WebResourcesHelper.Shared + "Chart.LineMarker",
            WebResourcesHelper.Mvc + "Chart.LineMarker",
            WebResourcesHelper.Mvc + "Cast.chart")]
        public abstract class LineMarker : Definitions.Extender { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.chart.annotation",
            WebResourcesHelper.Shared + "Chart.Annotation",
            WebResourcesHelper.Mvc + "Chart.Annotation",
            WebResourcesHelper.Mvc + "Cast.chart.annotation")]
        public abstract class Annotation : Definitions.Extender { }

        [Scripts(WebResourcesHelper.WijmoJs + "wijmo.chart.animation",
            WebResourcesHelper.Shared + "Chart.Animation",
            WebResourcesHelper.Mvc + "Chart.Animation",
            WebResourcesHelper.Mvc + "Cast.chart.animation")]
        public abstract class Animation : Definitions.Extender { }
    }
}
