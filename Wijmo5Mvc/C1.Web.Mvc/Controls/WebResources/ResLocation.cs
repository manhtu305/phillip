﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace C1.Web.Mvc.WebResources
{
    internal class ResLocation
    {
        public static readonly IEqualityComparer<ResLocation> IgnoreResNameCase = new ResLocationComparor();

        public ResLocation(string res, Assembly location = null)
        {
            if (res == null)
            {
                throw new ArgumentNullException("res");
            }

            Resource = res;
            Location = location ?? WebResourcesHelper.Assembly;
        }

        public string Resource
        {
            get;
            private set;
        }

        public Assembly Location
        {
            get;
            private set;
        }

        private class ResLocationComparor : IEqualityComparer<ResLocation>
        {
            public bool Equals(ResLocation x, ResLocation y)
            {
                if (x == y)
                {
                    return true;
                }

                if (x == null || y == null)
                {
                    return false;
                }

                return string.Equals(x.Resource, y.Resource, StringComparison.OrdinalIgnoreCase);
            }

            public int GetHashCode(ResLocation obj)
            {
                if (obj == null || obj.Resource == null) return 0;
                return obj.Resource.ToLower().GetHashCode();
            }
        }

    }
}
