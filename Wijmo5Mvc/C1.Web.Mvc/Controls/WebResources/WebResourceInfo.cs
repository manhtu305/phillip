﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace C1.Web.Mvc
{
    internal class WebResourceInfo
    {
        public WebResourceInfo(int key, string name, string contentType, Assembly currentAssembly)
        {
            Key = key;
            Name = name;
            ContentType = contentType;
            _currentAssembly = currentAssembly;
        }

        public int Key { get; private set; }
        public string Name { get; private set; }
        public string ContentType { get; private set; }

        private string _resourceContent;
        private Assembly _currentAssembly;

        public string ResourceContent
        {
            get { return _resourceContent ?? (_resourceContent = GetResourceContent()); }
        }

        public Encoding ResourceEncoding { get; private set; }

        private string GetResourceContent()
        {
            try
            {
                using (var resourceStream = _currentAssembly.GetManifestResourceStream(Name))
                {
                    if (resourceStream != null)
                    {
                        using (var reader = new StreamReader(resourceStream, true))
                        {
                            ResourceEncoding = reader.CurrentEncoding;
                            return reader.ReadToEnd() + Environment.NewLine;
                        }
                    }
                }
            }
            catch
            {
            }

            return string.Empty;
        }

        public void WriteContent(Stream stream)
        {
            if (string.IsNullOrEmpty(ResourceContent)) return;

            byte[] bytes = ResourceEncoding.GetBytes(ResourceContent);
#if NETCORE3
            stream.WriteAsync(bytes, 0, bytes.Length);
#else
            stream.Write(bytes, 0, bytes.Length);
#endif
        }
    }
}