﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.WebResources
{
    /// <summary>
    /// Determines style sheet dependencies.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscate]
    [AttributeUsage(AttributeTargets.Class)]
    internal class StylesAttribute : ResDependenciesAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StylesAttribute"/> class.
        /// </summary>
        /// <param name="dependencies">The dependencies.</param>
        public StylesAttribute(params object[] dependencies) : base(dependencies)
        {
        }

        /// <summary>
        /// A collection of style sheet dependencies.
        /// </summary>
        /// <remarks>
        ///   Example:
        ///   [Styles("jquery.wijmo.wijcalendar"]
        ///   [Styles(typeof(CalendarExtender))]
        /// </remarks>
        public override IEnumerable<object> Dependencies
        {
            get { return base.Dependencies; }
        }

        public override string ResExtension
        {
            get
            {
                return WebResourcesHelper.CssExt;
            }
        }
    }
}
