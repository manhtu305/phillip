﻿using System;

namespace C1.Web.Mvc.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    [AttributeUsage(AttributeTargets.Assembly)]
    internal class AssemblyStylesAttribute : AssemblyResAttribute
    {
        public AssemblyStylesAttribute(params Type[] dependencies) : base(dependencies)
        {
        }
    }
}
