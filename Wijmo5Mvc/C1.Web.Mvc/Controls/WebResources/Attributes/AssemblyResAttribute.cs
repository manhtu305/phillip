﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal abstract class AssemblyResAttribute : Attribute
    {
        protected AssemblyResAttribute(params Type[] dependencies)
        {
            Dependencies = dependencies ?? new Type[] { };
        }

        public IEnumerable<Type> Dependencies
        {
            get;
            private set;
        }
    }
}
