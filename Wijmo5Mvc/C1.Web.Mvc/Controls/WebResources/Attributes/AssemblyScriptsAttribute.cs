﻿using System;

namespace C1.Web.Mvc.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    [AttributeUsage(AttributeTargets.Assembly)]
    internal class AssemblyScriptsAttribute : AssemblyResAttribute
    {
        public AssemblyScriptsAttribute(params Type[] dependencies) : base(dependencies)
        {
        }
    }
}
