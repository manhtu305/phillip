﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.WebResources
{
    /// <summary>
    /// Determines web resources dependencies.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal abstract class ResDependenciesAttribute : Attribute
    {
        private readonly object[] _dependencies;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResDependenciesAttribute"/> class.
        /// </summary>
        /// <param name="dependencies">The dependencies.</param>
        protected ResDependenciesAttribute(params object[] dependencies)
        {
            _dependencies = dependencies;
        }

        /// <summary>
        /// A collection of dependencies.
        /// </summary>
        public virtual IEnumerable<object> Dependencies
        {
            get { return _dependencies; }
        }

        public abstract string ResExtension
        {
            get;
        }
    }
}