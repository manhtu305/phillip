﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.WebResources
{
    /// <summary>
    /// Determines script dependencies.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscate]
    [AttributeUsage(AttributeTargets.Class)]
    internal class ScriptsAttribute : ResDependenciesAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScriptsAttribute"/> class.
        /// </summary>
        /// <param name="dependencies">The dependencies.</param>
        public ScriptsAttribute(params object[] dependencies) : base(dependencies)
        {
        }

        /// <summary>
        /// A collection of script dependencies.
        /// </summary>
        /// <remarks>
        ///   Example:
        ///   [Scripts("jquery.wijmo.wijcalendar"]
        ///   [Scripts(typeof(CalendarExtender))]
        /// </remarks>
        public override IEnumerable<object> Dependencies
        {
            get { return base.Dependencies; }
        }

        public override string ResExtension
        {
            get
            {
                return WebResourcesHelper.JsExt;
            }
        }
    }
}
