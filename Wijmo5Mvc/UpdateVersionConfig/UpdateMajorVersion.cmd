set oldMajor=20201
set major=20202
set oldplaceHolder=20201.55555
set placeHolder=20202.55555
set tool=..\..\BuildTools\C1.Build\C1.Build.UpdateVersion.exe
%tool% SourceCode.xml -tfs-checkoutfiles true -tfs-checkinallfiles false -new %placeHolder% -savecurrent false -placeholder %oldplaceHolder%
%tool% Samples.xml -tfs-checkoutfiles true -tfs-checkinallfiles false -new %placeHolder% -placeholder %oldplaceHolder%

%tool% ..\..\WebApi\UpdateVersionConfig\SamplesMvc.xml -tfs-checkoutfiles true -tfs-checkinallfiles false -new %placeHolder% -placeholder %oldplaceHolder%

%tool% -tfs-checkoutfiles true -tfs-checkinallfiles false -files PlaceHolder.txt -placeholderfile PlaceHolder.txt -new %placeHolder%
%tool% -tfs-checkoutfiles true -tfs-checkinallfiles false -files Version.txt -placeholder %oldMajor% -new %major%