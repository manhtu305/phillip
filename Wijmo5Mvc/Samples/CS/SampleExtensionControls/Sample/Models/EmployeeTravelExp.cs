//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sample.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeTravelExp
    {
        public int Exp_ID { get; set; }
        public string EmpName { get; set; }
        public string Desc_of_Exp { get; set; }
        public Nullable<System.DateTime> Date_From { get; set; }
        public Nullable<System.DateTime> Date_To { get; set; }
        public Nullable<System.DateTime> Date_of_Submission { get; set; }
        public string Autherised_BY { get; set; }
        public Nullable<int> Airfare { get; set; }
        public Nullable<int> lodging { get; set; }
        public Nullable<int> Ground_transportation { get; set; }
        public Nullable<int> Meals_Tips { get; set; }
        public Nullable<int> Conferences_seminars { get; set; }
        public Nullable<int> Miles { get; set; }
        public Nullable<int> Mileage_Reimbursement { get; set; }
        public Nullable<int> Miscellaneous { get; set; }
        public Nullable<int> Curr_Exchange_rate { get; set; }
        public string Expence_Curr { get; set; }
        public string Dept { get; set; }
    }
}
