﻿namespace MvcExplorer.Models
{
    public class Dot
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Size { get; set; }
    }
}