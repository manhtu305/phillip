﻿using Microsoft.AspNetCore.Mvc;
using MvcExplorer.Models;

namespace MvcExplorer.Controllers
{
    public partial class ComboBoxController : Controller
    {
        private readonly C1NWindEntities _db;

        public ComboBoxController(C1NWindEntities db)
        {
            _db = db;
        }

        public ActionResult Index()
        {
            ViewBag.Countries = Countries.GetCountries();
            ViewBag.Cities = Cities.GetCities();
            return View();
        }
    }
}
