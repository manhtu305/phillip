﻿using Microsoft.AspNetCore.Mvc;
using MvcExplorer.Models;

namespace MvcExplorer.Controllers
{
    public partial class ListBoxController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Cities = Cities.GetCities();
            return View();
        }
    }
}
