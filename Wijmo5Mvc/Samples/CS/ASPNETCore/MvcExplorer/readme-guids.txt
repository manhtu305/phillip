E71B94FF-87CD-40C6-82BF-C877325388AF		Common Guid shared by sample with multiple languages.
AED0CAA4-455B-4ADE-8C01-C1CAA8966407		Unique Guid for each sample regardless of language.
<product>FlexGrid for ASP.NET MVC;ASP.NET</product>
<product>Calendar for ASP.NET MVC;ASP.NET</product>
<product>AutoComplete for ASP.NET MVC;ASP.NET</product>
<product>ColorPicker for ASP.NET MVC;ASP.NET</product>
<product>ComboBox for ASP.NET MVC;ASP.NET</product>
<product>InputColor for ASP.NET MVC;ASP.NET</product>
<product>InputDate for ASP.NET MVC;ASP.NET</product>
<product>InputMask for ASP.NET MVC;ASP.NET</product>
<product>InputNumber for ASP.NET MVC;ASP.NET</product>
<product>InputTime for ASP.NET MVC;ASP.NET</product>
<product>ListBox for ASP.NET MVC;ASP.NET</product>
<product>Menu for ASP.NET MVC;ASP.NET</product>
<product>Popup for ASP.NET MVC;ASP.NET</product>
<product>MultiSelect for ASP.NET MVC;ASP.NET</product>
<product>FlexPie for ASP.NET MVC;ASP.NET</product>
<product>FlexChart for ASP.NET MVC;ASP.NET</product>
<product>LinearGauge for ASP.NET MVC;ASP.NET</product>
<product>BulletGraph for ASP.NET MVC;ASP.NET</product>
<product>RadialGauge for ASP.NET MVC;ASP.NET</product>