﻿'use strict';

/**************************************************
    app
*/

var app = {};

// define all when page loads
onload = function () {

    // highlight code blocks 
    // (highlight.js requires <pre><code> blocks, we want just <code>)
    var code = document.querySelectorAll('code');
    for (var i = 0; i < code.length; i++) {
        hljs.highlightBlock(code[i]);
    }

    // define propertyChanged event used to implement binding
    app.propertyChanged = new wijmo.Event();
    app.setProperty = function (name, value) {
        if (app[name] !== value) {
            var oldValue = app[name];
            app[name] = value;
            app.propertyChanged.raise(app, new wijmo.PropertyChangedEventArgs(name, oldValue, value));
            //console.log('property ' + name + ' changed');
        }
    }

    //service root
    app.serviceUrl = 'http://testdemos.componentone.com/aspnet/webapi/api/dataengine/';

    // define ShowTotals options/values
    app.showTotals = [
        { name: 'None', value: wijmo.olap.ShowTotals.None },
        { name: 'Grand totals', value: wijmo.olap.ShowTotals.GrandTotals },
        { name: 'Subtotals', value: wijmo.olap.ShowTotals.Subtotals },
    ];

    // chart types
    app.chartTypes = [
        { name: 'Column', value: wijmo.olap.PivotChartType.Column },
        { name: 'Bar', value: wijmo.olap.PivotChartType.Bar },
        { name: 'Scatter', value: wijmo.olap.PivotChartType.Scatter },
        { name: 'Line', value: wijmo.olap.PivotChartType.Line },
        { name: 'Area', value: wijmo.olap.PivotChartType.Area },
        { name: 'Pie', value: wijmo.olap.PivotChartType.Pie },
    ];

    // pre-defined views
    app.cubeFields = [
        // the text fields.
        {
            header: 'Product(This is a text field collection.)', // the 1st level
            subFields: [
                {
                    dataType: 1,
                    binding: '[Product].[Product]',
                    header: 'Product',
                    key: '[Product].[Product]'
                }, // the 2nd level
                {
                    header: 'Stocking', // the 2nd level
                    subFields: [
                        {
                            dataType: 1,
                            binding: '[Product].[Class]',
                            header: 'Class',
                            key: '[Product].[Category]'
                        }, // the 3rd level
                        {
                            dataType: 1,
                            binding: '[Product].[Color]',
                            header: 'Color',
                            key: '[Product].[Category]'
                        } // the 3rd level
                    ]
                }
            ]
        },
        // the value fields
        {
            header: 'Internet Sales(This is a value field collection.)', // the 1st level
            subFields: [
                // the 2nd level fields
                {
                    dataType: 2,
                    format: 'n0',
                    aggregate: 1,
                    binding: '[Measures].[Internet Sales Amount]',
                    header: 'Internet Sales Amount',
                    key: '[Measures].[Internet Sales Amount]'
                },
                {
                    dataType: 2,
                    format: 'n0',
                    aggregate: 1,
                    binding: '[Measures].[Internet Order Quantity]',
                    header: 'Internet Order Quantity',
                    key: '[Measures].[Internet Order Quantity]'
                },
                {
                    dataType: 2,
                    format: 'n0',
                    aggregate: 1,
                    binding: '[Measures].[Internet Gross Profit]',
                    header: 'Internet Gross Profit',
                    key: '[Measures].[Internet Gross Profit]'
                }
            ]
        }
    ];

    // 1) create/bind the controls that configure the PivotPanel control
    app.cmbRowTotals = new wijmo.input.ComboBox('#cmbRowTotals', {
        itemsSource: app.showTotals,
        displayMemberPath: 'name',
        selectedValuePath: 'value',
        selectedIndexChanged: function (s, e) {
            app.setProperty('showRowTotals', s.selectedValue);
        }
    });
    app.cmbColTotals = new wijmo.input.ComboBox('#cmbColTotals', {
        itemsSource: app.showTotals,
        displayMemberPath: 'name',
        selectedValuePath: 'value',
        selectedIndexChanged: function (s, e) {
            app.setProperty('showColTotals', s.selectedValue);
        }
    });
    app.chkShowZeros = document.getElementById('chkShowZeros');
    app.chkShowZeros.addEventListener('click', function (e) {
        app.setProperty('showZeros', app.chkShowZeros.checked);
    });
    app.propertyChanged.addHandler(function (s, e) {
        switch (e.propertyName) {
            case 'showRowTotals':
                app.cmbRowTotals.selectedValue = e.newValue;
                break;
            case 'showColTotals':
                app.cmbColTotals.selectedValue = e.newValue;
                break;
            case 'showZeros':
                app.chkShowZeros.checked = e.newValue;
        }
    });

    // 2) create/bind the FlexPivotPanel, FlexPivotGrid, and FlexPivotChart controls
    app.panel = new wijmo.olap.service.FlexPivotPanel('#pivotPanel');
    // fix a problem when the column width of the field tree is not set to '*',
    // the content would be cut if it is long.
    app.panel._gridFields.initialize({
        columns: [{
            binding: 'header',
            width: '*'
        }]
    });
    var ng = app.panel.engine;
    app.propertyChanged.addHandler(function (s, e) {
        switch (e.propertyName) {
            case 'showRowTotals':
                ng.showRowTotals = e.newValue;
                break;
            case 'showColTotals':
                ng.showColumnTotals = e.newValue;
                break;
            case 'showZeros':
                ng.showZeros = e.newValue;
                break;
        }
    });
    ng.viewDefinitionChanged.addHandler(function () {
        document.getElementById('chartPanel').style.display = ng.isViewDefined ? '' : 'none';
        document.getElementById('noChartPanel').style.display = ng.isViewDefined ? 'none' : '';
    });
    app.pivotGrid = new wijmo.olap.service.FlexPivotGrid('#pivotGrid', {
        itemsSource: app.panel,
        showSelectedHeaders: 'All'
    });
    app.pivotChart = new wijmo.olap.PivotChart('#pivotChart', {
        itemsSource: app.panel
    });
    app.cmbChartType = new wijmo.input.ComboBox('#cmbChartType', {
        itemsSource: app.chartTypes,
        displayMemberPath: 'name',
        selectedValuePath: 'value',
        selectedIndexChanged: function (s, e) {
            app.pivotChart.chartType = s.selectedValue;
        }
    });

    // save/restore view definitions
    app.saveView = function () {
        if (ng.isViewDefined) {
            localStorage.viewDefinition = ng.viewDefinition;
        }
    }
    app.loadView = function () {
        if (localStorage.viewDefinition) {
            ng.viewDefinition = localStorage.viewDefinition;
            app.cmbRowTotals.selectedValue = ng.showRowTotals;
            app.cmbColTotals.selectedValue = ng.showColumnTotals;
            app.chkShowZeros.checked = ng.showZeros;
        }
    }

    // 3) export the current view to Excel
    app.export = function () {

        // create book with current view
        var book = wijmo.grid.xlsx.FlexGridXlsxConverter.save(app.pivotGrid, {
            includeColumnHeaders: true,
            includeRowHeaders: true
        });
        book.sheets[0].name = 'Main View';
        addTitleCell(book.sheets[0], getViewTitle(app.panel.engine));

        // add sheet with transposed view
        transposeView(app.panel.engine);
        var transposed = wijmo.grid.xlsx.FlexGridXlsxConverter.save(app.pivotGrid, {
            includeColumnHeaders: true,
            includeRowHeaders: true
        });
        transposed.sheets[0].name = 'Transposed View';
        addTitleCell(transposed.sheets[0], getViewTitle(app.panel.engine));
        book.sheets.push(transposed.sheets[0]);
        transposeView(app.panel.engine);

        // add sheet with raw data
        if (app.rawGrid.rows.length < 20000) {
            var raw = wijmo.grid.xlsx.FlexGridXlsxConverter.save(app.rawGrid, {
                includeColumnHeaders: true,
                includeRowHeaders: false
            });
            raw.sheets[0].name = 'Raw Data';
            book.sheets.push(raw.sheets[0]);
        }

        // save the book
        book.save('wijmo.olap.xlsx');
    }

    // initialize properties
    ng.itemsSource = app.serviceUrl;
    ng.dataSourceKey = 'cube';
    // specify the field collection provided for cube.
    ng.autoGenerateFields = false;
    ng.deferUpdate(function () {
        ng.rowFields.clear();
        ng.columnFields.clear();
        ng.valueFields.clear();
        ng.filterFields.clear();
        ng.fields.clear();
        wijmo.copy(ng, { fields: app.cubeFields });
    });
    app.setProperty('showRowTotals', wijmo.olap.ShowTotals.Subtotals);
    app.setProperty('showColTotals', wijmo.olap.ShowTotals.Subtotals);
    app.setProperty('showZeros', false);
}


/**************************************************
    utilities
*/

// save/load/transpose/export views
function transposeView(ng) {
    ng.deferUpdate(function () {

        // save row/col fields
        var rows = [],
            cols = [];
        for (var r = 0; r < ng.rowFields.length; r++) {
            rows.push(ng.rowFields[r].header);
        }
        for (var c = 0; c < ng.columnFields.length; c++) {
            cols.push(ng.columnFields[c].header);
        }

        // clear row/col fields
        ng.rowFields.clear();
        ng.columnFields.clear();

        // restore row/col fields in transposed order
        for (var r = 0; r < rows.length; r++) {
            ng.columnFields.push(rows[r]);
        }
        for (var c = 0; c < cols.length; c++) {
            ng.rowFields.push(cols[c]);
        }
    });
}

// build a title for the current view
function getViewTitle(ng) {
    var title = '';
    for (var i = 0; i < ng.valueFields.length; i++) {
        if (i > 0) title += ', ';
        title += ng.valueFields[i].header;
    }
    title += ' by ';
    if (ng.rowFields.length) {
        for (var i = 0; i < ng.rowFields.length; i++) {
            if (i > 0) title += ', ';
            title += ng.rowFields[i].header;
        }
    }
    if (ng.rowFields.length && ng.columnFields.length) {
        title += ' and by ';
    }
    if (ng.columnFields.length) {
        for (var i = 0; i < ng.columnFields.length; i++) {
            if (i > 0) title += ', ';
            title += ng.columnFields[i].header;
        }
    }
    return title;
}

// adds a title cell into an xlxs sheet
function addTitleCell(sheet, title) {

    // create cell
    var cell = new wijmo.xlsx.WorkbookCell();
    cell.value = title;
    cell.style = new wijmo.xlsx.WorkbookStyle();
    cell.style.font = new wijmo.xlsx.WorkbookFont();
    cell.style.font.bold = true;

    // create row to hold the cell
    var row = new wijmo.xlsx.WorkbookRow();
    row.cells[0] = cell;

    // and add the new row to the sheet
    sheet.rows.splice(0, 0, row);
}
