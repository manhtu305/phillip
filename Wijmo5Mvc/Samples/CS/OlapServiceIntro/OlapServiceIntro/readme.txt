﻿OlapServiceIntro
------------------------------------------------------------------------------
Shows how to get started with the controls in the wijmo.olap.service module.

The sample shows how to use the following components:

FlexPivotEngine: A class that summarizes raw data into pivot views.

FlexPivotPanel: A control that provides a UI for editing the pivot views 
by dragging fields into view lists and editing their properties.

FlexPivotGrid: A control extends the FlexGrid to display pivot tables 
with collapsible row and column groups.

PivotChart: A control that provides visual representations of 
pivot tables with hierarchical axes.
