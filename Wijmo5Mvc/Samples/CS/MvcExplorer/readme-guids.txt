91D29AAD-1AD7-413B-B5FA-0B95F1D53023		Common Guid shared by sample with multiple languages.
85F7DBDC-7083-4198-9B5F-859671CAB943		Unique Guid for each sample regardless of language.
<product>FlexGrid for ASP.NET MVC;ASP.NET</product>
<product>Calendar for ASP.NET MVC;ASP.NET</product>
<product>AutoComplete for ASP.NET MVC;ASP.NET</product>
<product>ColorPicker for ASP.NET MVC;ASP.NET</product>
<product>ComboBox for ASP.NET MVC;ASP.NET</product>
<product>InputColor for ASP.NET MVC;ASP.NET</product>
<product>InputDate for ASP.NET MVC;ASP.NET</product>
<product>InputMask for ASP.NET MVC;ASP.NET</product>
<product>InputNumber for ASP.NET MVC;ASP.NET</product>
<product>InputTime for ASP.NET MVC;ASP.NET</product>
<product>ListBox for ASP.NET MVC;ASP.NET</product>
<product>Menu for ASP.NET MVC;ASP.NET</product>
<product>Popup for ASP.NET MVC;ASP.NET</product>
<product>MultiSelect for ASP.NET MVC;ASP.NET</product>
<product>FlexPie for ASP.NET MVC;ASP.NET</product>
<product>FlexChart for ASP.NET MVC;ASP.NET</product>
<product>LinearGauge for ASP.NET MVC;ASP.NET</product>
<product>BulletGraph for ASP.NET MVC;ASP.NET</product>
<product>RadialGauge for ASP.NET MVC;ASP.NET</product>
<product>Excel for .Net;ASP.NET</product>
<product>PDF for .Net;ASP.NET</product>