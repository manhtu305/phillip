﻿using C1.Web.Mvc;
using C1.Web.Mvc.Grid;
using MvcExplorer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using C1.Web.Mvc.Serialization;

namespace MvcExplorer.Controllers
{
    public partial class FlexGridController : Controller
    {
        private static IList<ITreeItem> treeViewData = null;
        private static IEnumerable<Sale> normalData = Sale.GetData(25);
        public ActionResult PDFExport()
        {
            if (treeViewData == null)
            { 
                treeViewData = MvcExplorer.Models.Folder.Create(Server.MapPath("~")).Children;
            }

            ViewBag.GroupedFlexGridData = normalData;
            ViewBag.MergedFlexGridData = normalData;
            ViewBag.TreeViewData = treeViewData;
            return View();
        }
    }
}