﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcExplorer.Models
{
    public class NamedColor
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}