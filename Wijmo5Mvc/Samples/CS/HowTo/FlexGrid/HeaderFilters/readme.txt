﻿ASP.NET MVC Header Filters
---------------------------------------------
This sample demonstrates how to create custom filter cells in a FlexGrid.

The sample customizes the column headers by adding an extra row with merged 
cells.

An itemFormatter function is used to customize the content of some column 
header cells with filter editors. The itemFormatter is also used to customize
the display of some columns to show hyperlinks.