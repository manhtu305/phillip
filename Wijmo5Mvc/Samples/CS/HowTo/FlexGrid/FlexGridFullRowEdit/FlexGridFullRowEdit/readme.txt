﻿ASP.NET MVC Custom Editors
---------------------------------------------------------------------
This sample shows how to use full row custom editors feature of a FlexGrid and how to make its full rows editable by using ItemFormatter event of FlexGrid.

This sample shows how to use full row custom editors feature of a FlexGrid and how to make its full rows editable by using ItemFormatter event of FlexGrid.