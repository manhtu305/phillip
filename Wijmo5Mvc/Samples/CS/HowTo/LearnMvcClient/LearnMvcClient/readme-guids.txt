F6DD46CA-BAB7-4406-B064-32844F5E99A6	Common Guid shared by sample with multiple languages.
7F896BA1-5FAB-4A03-9C9F-4A1C85687522	Unique Guid for each sample regardless of language.
<product>FlexGrid for ASP.NET MVC;ASP.NET</product>
<product>Calendar for ASP.NET MVC;ASP.NET</product>
<product>AutoComplete for ASP.NET MVC;ASP.NET</product>
<product>ColorPicker for ASP.NET MVC;ASP.NET</product>
<product>ComboBox for ASP.NET MVC;ASP.NET</product>
<product>InputColor for ASP.NET MVC;ASP.NET</product>
<product>InputDate for ASP.NET MVC;ASP.NET</product>
<product>InputMask for ASP.NET MVC;ASP.NET</product>
<product>InputNumber for ASP.NET MVC;ASP.NET</product>
<product>InputTime for ASP.NET MVC;ASP.NET</product>
<product>ListBox for ASP.NET MVC;ASP.NET</product>
<product>Menu for ASP.NET MVC;ASP.NET</product>
<product>Popup for ASP.NET MVC;ASP.NET</product>
<product>MultiSelect for ASP.NET MVC;ASP.NET</product>
<product>FlexPie for ASP.NET MVC;ASP.NET</product>
<product>FlexChart for ASP.NET MVC;ASP.NET</product>
<product>LinearGauge for ASP.NET MVC;ASP.NET</product>
<product>BulletGraph for ASP.NET MVC;ASP.NET</product>
<product>RadialGauge for ASP.NET MVC;ASP.NET</product>