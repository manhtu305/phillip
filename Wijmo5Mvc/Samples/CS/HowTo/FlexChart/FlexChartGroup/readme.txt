﻿ASP.NET MVC FlexChart Group
---------------------------------------------
This sample shows how to create drill-down chart with data grouping.

This pages shows how to use MVC FlexChart and FlexPie controls and the
CollectionView's grouping capabilities to implement drill-down charts.