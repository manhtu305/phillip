﻿ASP.NET MVC Benchmark
---------------------------------------------
Benchmark sample compare the performance of grid controls.

Benchmark sample compare the performance of grid controls, this application creates an array with data objects and 
shows the data on the selected grid controls with paging enable/disable. The application reports how 
long the binding process took and shows the resulting control.