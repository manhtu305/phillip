﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Benchmark.Models
{
    public class ControlModel
    {
        public string DisplayName { get; set; }
        public string ControlName { get; set; }
        public static IList<ControlModel> GetControlModel() 
        {
            return new List<ControlModel>
            {
                new ControlModel {DisplayName="C1 MVC FlexGrid", ControlName="C1MVCFlexGrid"},
                new ControlModel {DisplayName="DevExpress GridView", ControlName="DevExpressGridView"},
                new ControlModel {DisplayName="Telerik Grid", ControlName="TelerikGrid"},
                new ControlModel {DisplayName="Infragistics Grid", ControlName="InfragisticsGrid"},
            };
        }
    }
}