﻿
function setRunningTime(controlName, elapsedTime) {
    var ele = document.getElementById(controlName);
    ele.innerHTML = "<b>" + elapsedTime + "</b>";
}

$(document).ready(function () {
    renderTiming();
});

function renderTiming() {
    $(".server-time").each(function (index, ele) {
        if (ele.innerHTML) {
            $(ele).closest(".timing").show();
        } else {
            $(ele).closest(".timing").hide();
        }
    });
}

function changePagingStats(paging) {
    if (paging) {
        var pagingMenu = document.getElementById("paging");
        pagingMenu.value = paging;
    }
    submit();
}

function changeItemCount(count) {
    if (count) {
        itemCount = document.getElementById("itemCount");
        itemCount.value = count;
    }
    submit();
}

function submit(action) {
    var form = document.forms[0];
    if (action) form.action = action;
    form.submit();
}

var startTime = null;
var docChangedTimeout = null;
var viewName = null;
function startTiming(name) {
    startTime = new Date();
    viewName = name;
}

function loadComplete() {
    if (viewName) {
        if (docChangedTimeout) clearTimeout(docChangedTimeout);
        docChangedTimeout = setTimeout(function () {
            var endTime = new Date(),
                elapsed = (endTime.getTime() - startTime.getTime()) / 1000,
                input = document.getElementById(viewName + "client");
            input.value = elapsed.toFixed(2) + ' s';
            viewName = null;
        }, 250);
    }
}