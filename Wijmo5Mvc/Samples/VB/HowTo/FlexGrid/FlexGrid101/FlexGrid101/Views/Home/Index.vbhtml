﻿@Imports MVCFlexGrid101Res
@ModelType MVCFlexGrid101.FlexGridModel 

@Code
    ViewBag.Title = "FlexGrid Introduction"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code


<div Class="header">
    <div Class="container">
        <a class="logo-container" href="https://www.grapecity.com/en/aspnet-mvc" target="_blank">
            <i class="gcicon-product-c1"></i>
        </a>
        <h1>
            @Html.Raw(Resources.FlexGrid101Res.Project_Name_Text0)
        </h1>
        <p>
                @Html.Raw(Resources.FlexGrid101Res.Project_Description_Text0)
        </p>
    </div>
</div>

<div Class="container">
    <div Class="sample-page download-link">
        <a href = "https://www.grapecity.com/en/download/componentone-studio" > Download Free Trial</a>
    </div>
    <!-- Getting Started -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Getting_Started_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Overview_Step_Title_Text0)
        </p>
        <ol>
            <li>@Html.Raw(Resources.FlexGrid101Res.Overview_Step1_Text0)</li>
            <li>@Html.Raw(Resources.FlexGrid101Res.Overview_Step2_Text0)</li>
            <li>@Html.Raw(Resources.FlexGrid101Res.Overview_Step3_Text0)</li>
            <li>@Html.Raw(Resources.FlexGrid101Res.Overview_Step4_Text0)</li>
        </ol>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Overview_Description_Text0)
        </p>
        <div Class="row">
            <div Class="col-md-6">
                <div>
                                <ul Class="nav nav-tabs" role="tablist">
                        <li Class="active"><a href="#gsHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li> <a href = "#gsCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li> <a href = "#gsCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div Class="tab-content">
                        <div Class="tab-pane active pane-content" id="gsHtml">
&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;

    &lt;/head&gt;
    &lt;body&gt;
        &lt;!-- this Is the grid --&gt;
        @@(Html.C1().FlexGrid().Id("gsFlexGrid").IsReadOnly(True).SortingType(Grid.AllowSorting.SingleColumn) _
            .Bind(Model.CountryData).AutoGenerateColumns(True))

    &lt;/body&gt;
&lt;/html&gt;

                        </div>
                        <div Class="tab-pane pane-content" id="gsCss">

/* set default grid style */
.wj-flexgrid {
    height: 300px;
    background-color: white;
    box-shadow: 4px 4px 10px 0px rgba(50, 50, 50, 0.75);
    margin-bottom: 12px;
}

                        </div>
                        <div Class="tab-pane pane-content" id="gsCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function
End Class



                            </div>
                        </div>
                </div>
            </div>
            <div Class="col-md-6">
                <h4> @Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0) :              </h4>                
                    @(Html.C1().FlexGrid().Id("gsFlexGrid").IsReadOnly(True).SortingType(Grid.AllowSorting.SingleColumn) _
.Bind(Model.CountryData).AutoGenerateColumns(True))
            </div>
        </div>
    </div>

    <!-- column definitions -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Column_Definitions_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Column_Definitions_Description_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Column_Definitions_Description_Text1)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Column_Definitions_Description_Text2)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Column_Definitions_Description_Text3)
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#cdHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#cdCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="cdHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("cdInitMethod").IsReadOnly(True).AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
        bl.Add(Sub(cb) cb.Binding("End").Header("End").Format("HH:mm"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        bl.Add(Sub(cb) cb.Binding("Amount2"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
        bl.Add(Sub(cb) cb.Binding("Active"))
    End Sub)
)
                        </div>
                        <div class="tab-pane pane-content" id="cdCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function
End Class

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0):</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("cdInitMethod").IsReadOnly(True).AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                    .Bind(Model.CountryData).CssClass("grid") _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                                 bl.Add(Sub(cb) cb.Binding("End").Header("End").Format("HH:mm"))
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                                 bl.Add(Sub(cb) cb.Binding("Amount2"))
                                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                                 bl.Add(Sub(cb) cb.Binding("Active"))
                             End Sub)
                )
            </div>
        </div>
    </div>


    <!-- selection modes -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Selection_Modes_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Selection_Modes_Description_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Selection_Modes_Description_Text1)
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#smHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#smJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#smCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="smHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("smFlexGrid").IsReadOnly(True).AutoGenerateColumns(False) _
    .SortingType(Grid.AllowSorting.SingleColumn).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.None).Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
    End Sub)
)
&lt;br /&gt;@Html.Raw(Resources.FlexGrid101Res.Selection_Modes_Text0)
@@(Html.C1().ComboBox().Id("smMenu").Bind(Model.Settings("SelectionMode")) _
    .SelectedIndex(0).OnClientSelectedIndexChanged("smMenu_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="smJs">

var smFlexGrid = null;

function InitialControls() {
    //@Html.Raw(Resources.FlexGrid101Res.Selection_Modes_Text0) Modules
    smFlexGrid = &lt;wijmo.grid.FlexGrid&gt;wijmo.Control.getControl("#smFlexGrid");
}

//@Html.Raw(Resources.FlexGrid101Res.Selection_Modes_Text0) Modules
function smMenu_SelectedIndexChanged(sender) {
    if (sender.selectedValue != null && smFlexGrid != null) {
        smFlexGrid.selectionMode = sender.selectedValue;
    }
}

                        </div>
                        <div class="tab-pane pane-content" id="smCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.Settings = CreateSettings()
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function

    Private Function CreateSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
        {"SelectionMode", New Object() {SelectionMode.None.ToString(), SelectionMode.Cell.ToString(), SelectionMode.CellRange.ToString(), SelectionMode.Row.ToString(), SelectionMode.RowRange.ToString(), SelectionMode.ListBox.ToString()}},        
    }
        Return settings
    End Function
    
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0) :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("smFlexGrid").IsReadOnly(True).AutoGenerateColumns(False) _
                .SortingType(Grid.AllowSorting.SingleColumn).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.None).Bind(Model.CountryData).CssClass("grid") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
                         End Sub)
                )
                <br />@Html.Raw(Resources.FlexGrid101Res.Selection_Modes_Text0)
                @(Html.C1().ComboBox().Id("smMenu").Bind(Model.Settings("SelectionMode")) _
                 .SelectedIndex(0).OnClientSelectedIndexChanged("smMenu_SelectedIndexChanged")
                )
            </div>
        </div>
    </div>



    <!-- editing -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Editing_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Editing_Description_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Editing_Description_Text1)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Editing_Description_Text2)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Editing_Description_Text3)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Editing_Description_Text4)
        </p>
        <ol>
            <li>
                @Html.Raw(Resources.FlexGrid101Res.Editing_Description_Text5)
            </li>
            <li>
                @Html.Raw(Resources.FlexGrid101Res.Editing_Description_Text6)
            </li>
        </ol>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Editing_Description_Text7)
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                        <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#eHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#eCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="eHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("eFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .CssClass("grid") _
    .Bind(Sub(bl)
            bl.Bind(Url.Action("GridRead"))
            bl.Update(Url.Action("EGridUpdate"))
        End Sub) _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*").IsReadOnly(True))
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
            bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="eCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()        
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function    

    Public Function GridRead(<C1JsonRequest> requestData As CollectionViewRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Read(requestData, FlexGridModel.Source))
    End Function

    Public Function EGridUpdate(<C1JsonRequest> requestData As CollectionViewEditRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Edit(Of Sale)(requestData, Function(sale)
            Dim [error] As String = String.Empty
            Dim success As Boolean = True
            Dim fSale = FlexGridModel.Source.Find(Function(item) item.ID = sale.ID)
            fSale.Country = sale.Country
            fSale.Amount = sale.Amount
            fSale.Discount = sale.Discount
            fSale.Active = sale.Active
            Dim RetValue = New CollectionViewItemResult(Of Sale)
            RetValue.Error = [error]
            RetValue.Success = success AndAlso ModelState.IsValid
            RetValue.Data = fSale
            Return RetValue
        End Function, Function() FlexGridModel.Source))
    End Function
End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0) :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("eFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                .CssClass("grid") _
                .Bind(Sub(bl)
                          bl.Bind(Url.Action("GridRead"))
                          bl.Update(Url.Action("EGridUpdate"))
                      End Sub) _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID").Width("*").IsReadOnly(True))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("c").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Active").Width("*"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- grouping -->
    <div>
        <h2>
            @Html.Raw(Resources.FlexGrid101Res.Grouping_Text0)
        </h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Grouping_Desription_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Grouping_Desription_Text1)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Grouping_Desription_Text2)
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                            <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#gHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#gJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#gCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="gHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("gFlexGrid").AutoGenerateColumns(False) _
    .SortingType(Grid.AllowSorting.SingleColumn).IsReadOnly(True).GroupBy("Country").Bind(Model.CountryData).CssClass("grid") _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Product").Header("Product").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Color").Header("Color").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Width("*"))
        bl.Add(Sub(cb) cb.Binding("Amount").Header("Amount").Format("n0").Width("*") _
                            .Aggregate(C1.Web.Mvc.Grid.Aggregate.Sum))
    End Sub)
                )
&lt;br /&gt;@Html.Raw(Resources.FlexGrid101Res.Group_By_Text0)
@@(Html.C1().ComboBox().Id("gMenu").Bind(Model.Settings("GroupBy")) _
    .SelectedIndex(0).OnClientSelectedIndexChanged("gMenu_SelectedIndexChanged")
)

                        </div>
                        <div class="tab-pane pane-content" id="gJs">

//Group By Modules
function gMenu_SelectedIndexChanged(sender) {    
    var grid = &lt;wijmo.grid.FlexGrid&gt;wijmo.Control.getControl("#gFlexGrid");
    if (sender.selectedValue && grid) {
        var name = sender.selectedValue;
        var groupDescriptions = grid.collectionView.groupDescriptions;
        grid.beginUpdate();
        groupDescriptions.clear();

        if (name.indexOf("Country") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Country"));
        }

        if (name.indexOf("Product") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Product"));
        }

        if (name.indexOf("Color") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Color"));
        }

        if (name.indexOf("Start") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Start", function (item, prop) {
                var value = item[prop];
                return value.getFullYear() + "/" + value.getMonth();
            }));
        }

        if (name.indexOf("Amount") > -1) {
            groupDescriptions.push(new wijmo.collections.PropertyGroupDescription("Amount", function (item, prop) {
                var value = item[prop];
                if (value <= 500) {
                    return "<500";
                }

                if (value > 500 && value <= 1000) {
                    return "500 to 1000";
                }

                if (value > 1000 && value <= 5000) {
                    return "1000 to 5000";
                }

                return "More than 5000";
            }));
        }
        grid.endUpdate();
    }
}
                            

                        </div>
                        <div class="tab-pane pane-content" id="gCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.Settings = CreateSettings()
        model.CountryData = Sale.GetData(500)        
        Return View(model)
    End Function

    Private Function CreateSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {        
        {"GroupBy", New Object() {"Country", "Product", "Color", "Start", "Amount", "Country and Product",
            "Product and Color", "None"}}
    }
        Return settings
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0) :        </h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("gFlexGrid").AutoGenerateColumns(False) _
                    .SortingType(Grid.AllowSorting.SingleColumn).IsReadOnly(True).GroupBy("Country").Bind(Model.CountryData).CssClass("grid") _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Color").Header("Color").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Width("*"))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Header("Amount").Format("n0").Width("*") _
                                                     .Aggregate(C1.Web.Mvc.Grid.Aggregate.Sum))
                             End Sub)
                )
                <br />@Html.Raw(Resources.FlexGrid101Res.Group_By_Text0) 
                @(Html.C1().ComboBox().Id("gMenu").Bind(Model.Settings("GroupBy")) _
                    .SelectedIndex(0).OnClientSelectedIndexChanged("gMenu_SelectedIndexChanged")
                )
            </div>
        </div>
    </div>


    <!-- filtering -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Filtering_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Filtering_Description_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Filtering_Description_Text1)
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                                <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#fHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#fCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="fHtml">

@@(Html.C1().FlexGrid().Id("fFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .IsReadOnly(True).Bind(Model.CountryData).CssClass("grid") _
    .Filterable(Sub(fl)
                    fl.DefaultFilterType(FilterType.None) _
                    .ColumnFilters(Sub(cfsb)
                                        For index As Int16 = 0 To Model.FilterBy.Length - 1
                                            cfsb.Add(Sub(cfb) cfb.Column(Model.FilterBy.ToList()(index)).FilterType(FilterType.Condition))
                                        Next
                                    End Sub)

                End Sub) _
.Columns(Sub(bl)
                bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="fCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        model.FilterBy = New String() {"ID", "Country", "Product", "Color", "Start"}
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0) :        </h4>
                @(Html.C1().FlexGrid().Id("fFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                    .IsReadOnly(True).Bind(Model.CountryData).CssClass("grid") _
                    .Filterable(Sub(fl)
                                    fl.DefaultFilterType(FilterType.None) _
                                    .ColumnFilters(Sub(cfsb)
                                                       For index As Int16 = 0 To Model.FilterBy.Length - 1
                                                           cfsb.Add(Sub(cfb) cfb.Column(Model.FilterBy.ToList()(index)).FilterType(FilterType.Condition))
                                                       Next
                                                   End Sub)

                                End Sub) _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                             bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                         End Sub)
                )
            </div>
        </div>
    </div>



    <!-- paging -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Paging_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Paging_Description_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Paging_Description_Text1)
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#pHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#pCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="pHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("pFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .IsReadOnly(True).CssStyle("height", "auto").Bind(Model.CountryData).CssClass("grid") _
    .PageSize(10) _
    .Columns(Sub(bl)
        bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
        bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
        bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
        bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
        bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
        bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
    End Sub)
)
@@Html.C1().Pager().Owner("pFlexGrid")

                        </div>
                        <div class="tab-pane pane-content" id="pCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0):</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("pFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                    .IsReadOnly(True).CssStyle("height", "auto").Bind(Model.CountryData).CssClass("grid") _
                    .PageSize(10) _
    .Columns(Sub(bl)
                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                 bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
             End Sub)
                )
                @Html.C1().Pager().Owner("pFlexGrid")
            </div>
        </div>
    </div>

    <!-- conditional styling -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Conditional_Styling_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Conditional_Styling_Description_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Conditional_Styling_Description_Text1)
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#csHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#csJs" role="tab" data-toggle="tab">TS</a></li>
                        <li><a href="#csCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="csHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("csFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .IsReadOnly(True).Bind(Model.CountryData).ItemFormatter("csFlexGrid_ItemFormatter") _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="csJs">

//Conditional styling Modules
function csFlexGrid_ItemFormatter(panel, r, c, cell) {
    if (wijmo.grid.CellType.Cell == panel.cellType && panel.columns[c].binding == 'Amount') {
        var cellData = panel.getCellData(r, c);
        cell.style.color = cellData < 0 ? 'red' : cellData < 500 ? 'black' : 'green';
    }
    if (wijmo.grid.CellType.Cell == panel.cellType && panel.columns[c].binding == 'Discount') {
        var cellData = panel.getCellData(r, c);
        cell.style.color = cellData < .1 ? 'red' : cellData < .2 ? 'black' : 'green';
    }
}

                        </div>
                        <div class="tab-pane pane-content" id="csCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0):</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("csFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                .IsReadOnly(True).Bind(Model.CountryData).ItemFormatter("csFlexGrid_ItemFormatter") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- themes -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Themes_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Themes_Description_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Themes_Description_Text0)
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#tCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#tCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="tHtml">

@@(Html.C1().FlexGrid(Of Sale)().Id("tFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .IsReadOnly(True).Bind(Model.CountryData).CssClass("custom-flex-grid") _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
        End Sub)
    )

                        </div>
                        <div class="tab-pane pane-content" id="tCss">

.custom-flex-grid .wj-header.wj-cell {
    color: #fff;
    background-color: #000;
    border-bottom: solid 1px #404040;
    border-right: solid 1px #404040;
    font-weight: bold;
}

.custom-flex-grid .wj-cell {
    background-color: #fff;
    border: none;
}

.custom-flex-grid .wj-alt:not(.wj-state-selected):not(.wj-state-multi-selected) {
    background-color: #fff;
}

.custom-flex-grid .wj-state-selected {
    background: #000;
    color: #fff;
}

.custom-flex-grid .wj-state-multi-selected {
    background: #222;
    color: #fff;
}

                        </div>
                        <div class="tab-pane pane-content" id="tCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0):</h4>
                @(Html.C1().FlexGrid(Of Sale)().Id("tFlexGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                .IsReadOnly(True).Bind(Model.CountryData).CssClass("custom-flex-grid") _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                         End Sub)
                )
            </div>
        </div>
    </div>


    <!-- trees/hierarchical data -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Trees_and_Hierarchical_Data_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Trees_and_Hierarchical_Data_Des_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Trees_and_Hierarchical_Data_Des_Text1)
        </p>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tvHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#tvCss" role="tab" data-toggle="tab">CSS</a></li>
                        <li><a href="#tvCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="tvHtml">

@@(Html.C1().FlexGrid().Id("tvFlexGrid").AutoGenerateColumns(False).IsReadOnly(True) _
    .Bind(Model.TreeData).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.ListBox) _
    .AllowResizing(C1.Web.Mvc.Grid.AllowResizing.None).SortingType(Grid.AllowSorting.None).ChildItemsPath("Children") _
    .CssClass("custom-flex-grid") _
    .Columns(Sub(bl)
                bl.Add().Binding("Header").Width("*").Header("Folder/File Name")
                bl.Add().Binding("Size").Width("80").Align("center")
            End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="tvCss">

.custom-flex-grid .wj-header.wj-cell {
    color: #fff;
    background-color: #000;
    border-bottom: solid 1px #404040;
    border-right: solid 1px #404040;
    font-weight: bold;
}

.custom-flex-grid .wj-cell {
    background-color: #fff;
    border: none;
}

.custom-flex-grid .wj-alt:not(.wj-state-selected):not(.wj-state-multi-selected) {
    background-color: #fff;
}

.custom-flex-grid .wj-state-selected {
    background: #000;
    color: #fff;
}

.custom-flex-grid .wj-state-multi-selected {
    background: #222;
    color: #fff;
}

                        </div>
                        <div class="tab-pane pane-content" id="tvCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        model.TreeData = Folder.Create(Server.MapPath("~")).Children
        Return View(model)
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0):</h4>
                @(Html.C1().FlexGrid().Id("tvFlexGrid").AutoGenerateColumns(False).IsReadOnly(True) _
                    .Bind(Model.TreeData).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.ListBox) _
                    .AllowResizing(C1.Web.Mvc.Grid.AllowResizing.None).SortingType(Grid.AllowSorting.None).ChildItemsPath("Children") _
                    .CssClass("custom-flex-grid") _
                    .Columns(Sub(bl)
                                 bl.Add().Binding("Header").Width("*").Header("Folder/File Name")
                                 bl.Add().Binding("Size").Width("80").Align("center")
                             End Sub)
                )
            </div>
        </div>
    </div>



    <!-- handling null values -->
    <div>
        <h2>@Html.Raw(Resources.FlexGrid101Res.Handling_Null_Values_Text0)</h2>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Handling_Null_Values_Des_Text0)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Handling_Null_Values_Des_Text1)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Handling_Null_Values_Des_Text2)
        </p>
        <p>
            @Html.Raw(Resources.FlexGrid101Res.Handling_Null_Values_Des_Text3)
        </p>
        <div class="row">
            <div class="col-md-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#nvHtml" role="tab" data-toggle="tab">Index.vbhtml</a></li>
                        <li><a href="#nvCS" role="tab" data-toggle="tab">HomeController.vb</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active pane-content" id="nvHtml">

@@(Html.C1().FlexGrid().Id("nvGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
    .Bind(Sub(bl)
            bl.Bind(Url.Action("GridRead"))
            bl.Update(Url.Action("NVGridUpdate"))
        End Sub) _
    .Columns(Sub(bl)
            bl.Add(Sub(cb) cb.Binding("ID").Header("ID").IsReadOnly(True))
            bl.Add(Sub(cb) cb.Binding("Country").Header("Country").IsRequired(False))
            bl.Add(Sub(cb) cb.Binding("Product").Header("Product").IsRequired(True))
            bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").IsRequired(True))
            bl.Add(Sub(cb) cb.Binding("Amount").Format("n0").IsRequired(True))
        End Sub)
)

                        </div>
                        <div class="tab-pane pane-content" id="nvCS">

Imports C1.Web.Mvc
Imports C1.Web.Mvc.Grid
Imports C1.Web.Mvc.Serialization

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexGridModel()
        model.CountryData = Sale.GetData(500)
        Return View(model)
    End Function

    Public Function GridRead(<C1JsonRequest> requestData As CollectionViewRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Read(requestData, FlexGridModel.Source))
    End Function

    Public Function NVGridUpdate(<C1JsonRequest> requestData As CollectionViewEditRequest(Of Sale)) As ActionResult
        Return Me.C1Json(CollectionViewHelper.Edit(Of Sale)(requestData, Function(sale)
            Dim [error] As String = String.Empty
            Dim success As Boolean = True
            Dim fSale = FlexGridModel.Source.Find(Function(item) item.ID = sale.ID)
            fSale.Country = sale.Country
            fSale.Product = sale.Product
            fSale.Amount = sale.Amount
            fSale.Discount = sale.Discount
            Dim RetValue = New CollectionViewItemResult(Of Sale)
            RetValue.Error = [error]
            RetValue.Success = success AndAlso ModelState.IsValid
            RetValue.Data = fSale
            Return RetValue
        End Function, Function() FlexGridModel.Source))
    End Function

End Class


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>@Html.Raw(Resources.FlexGrid101Res.Result_Live_Text0):</h4>
                @(Html.C1().FlexGrid().Id("nvGrid").AutoGenerateColumns(False).SortingType(Grid.AllowSorting.SingleColumn) _
                    .Bind(Sub(bl)
                              bl.Bind(Url.Action("GridRead"))
                              bl.Update(Url.Action("NVGridUpdate"))
                          End Sub) _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID").IsReadOnly(True))
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country").IsRequired(False))
                                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product").IsRequired(True))
                                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").IsRequired(True))
                                 bl.Add(Sub(cb) cb.Binding("Amount").Format("n0").IsRequired(True))
                             End Sub)
                )
            </div>
        </div>
    </div>









    

</div>

<script type="text/javascript">
    c1.documentReady(function () {
        if (window["InitialControls"]) {
            window["InitialControls"]();
        }
    });
</script>