Imports System.IO
Imports C1.Web.Api.Storage

Public Class StreamFileStorage
    Implements IFileStorage
    Private _stream As Stream

    Public Sub New(stream As Stream)
        _stream = stream
    End Sub

    Public Property Path() As String
        Get
            Return m_Path
        End Get
        Private Set
            m_Path = Value
        End Set
    End Property
    Private m_Path As String

    Public ReadOnly Property Exists As Boolean Implements IStorage.Exists
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property Name As String Implements IStorage.Name
        Get
            Return m_Name
        End Get
    End Property
    Private m_Name As String

    Public ReadOnly Property [ReadOnly] As Boolean Implements IFileStorage.ReadOnly
        Get
            Return True
        End Get
    End Property

    Public Sub Delete() Implements IFileStorage.Delete
    End Sub

    Public Sub Write(stream As Stream) Implements IFileStorage.Write
    End Sub

    Public Function Read() As Stream Implements IFileStorage.Read
        Return _stream
    End Function
End Class