﻿
Imports C1.Web.Mvc.Chart
Imports FlexChart101

Public Class HomeController
    Inherits Controller
    Public Function Index() As ActionResult
        Dim ModelObj As New FlexChartModel()
        ModelObj.Settings = CreateIndexSettings()
        ModelObj.CountrySalesData = CountryData.GetCountryData()

        Return View(ModelObj)
    End Function

    Private Function CreateIndexSettings() As IDictionary(Of String, Object())
        Dim settings = New Dictionary(Of String, Object())() From {
            {"ChartType", New Object() {"Column", "Bar", "Scatter", "Line", "LineSymbols", "Area",
                "Spline", "SplineSymbols", "SplineArea"}},
            {"Stacking", New Object() {"None", "Stacked", "Stacked 100%"}},
            {"Rotated", New Object() {False.ToString(), True.ToString()}},
            {"Palette", New Object() {"standard", "cocoa", "coral", "dark", "highcontrast", "light",
                "midnight", "minimal", "modern", "organic", "slate"}},
            {"GroupWidth", New Object() {"25%", "70%", "100%", "50 pixels"}},
            {"Position", New Object() {Position.None.ToString(), Position.Left.ToString(), Position.Top.ToString(), Position.Right.ToString(), Position.Bottom.ToString()}},
            {"SelectionMode", New Object() {SelectionMode.None.ToString(), SelectionMode.Series.ToString(), SelectionMode.Point.ToString()}}
        }

        Return settings
    End Function
End Class
