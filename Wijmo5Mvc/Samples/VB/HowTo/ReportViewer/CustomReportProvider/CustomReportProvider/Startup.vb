﻿Imports System
Imports Owin
Imports Microsoft.Owin

<Assembly: OwinStartupAttribute(GetType(Startup))>

Partial Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        app.UseReportProviders.Add("memoryreports", New MemoryReportsProvider())
    End Sub
End Class
