﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim model As New FlexSheetModel()
        model.CountryData = Sale.GetData(500)
        ViewBag.FontList = FontName.GetFontNameList()
        ViewBag.FontSizeList = FontSize.GetFontSizeList()
        Return View(model)
    End Function

End Class
