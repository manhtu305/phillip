﻿Public Class FontName

    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set
            m_Name = Value
        End Set
    End Property
    Private m_Name As String
    Public Property Value() As String
        Get
            Return m_Value
        End Get
        Set
            m_Value = Value
        End Set
    End Property
    Private m_Value As String

    Public Shared Function GetFontNameList() As List(Of FontName)
        Dim FontNamesList As List(Of FontName)
        FontNamesList = New List(Of FontName)
        Dim FontName As FontName

        FontName = New FontName
        FontName.Name = "Arial"
        FontName.Value = "Arial, Helvetica, sans-serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Arial Black"
        FontName.Value = """Arial Black"" Gadget, sans-serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Comic Sans MS"
        FontName.Value = """Comic Sans MS"" cursive, sans-serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Courier New"
        FontName.Value = """Courier New"" Courier, monospace"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Georgia"
        FontName.Value = "Georgia, serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Impact"
        FontName.Value = "Impact, Charcoal, sans-serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Lucida Console"
        FontName.Value = """Lucida Console"" Monaco, monospace"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Lucida Sans Unicode"
        FontName.Value = """Lucida Sans Unicode"" ""Lucida Grande"" sans-serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Palatino Linotype"
        FontName.Value = """Palatino Linotype"" ""Book Antiqua"" Palatino, serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Tahoma"
        FontName.Value = "Tahoma, Geneva, sans-serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Segoe UI"
        FontName.Value = """Segoe UI"" ""Roboto"" sans-serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Times New Roman"
        FontName.Value = """Times New Roman"" Times, serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Trebuchet MS"
        FontName.Value = """Trebuchet MS"" Helvetica, sans-serif"
        FontNamesList.Add(FontName)

        FontName = New FontName
        FontName.Name = "Verdana"
        FontName.Value = "Verdana, Geneva, sans-serif"
        FontNamesList.Add(FontName)

        Return FontNamesList
    End Function
End Class

Public Class FontSize
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set
            m_Name = Value
        End Set
    End Property
    Private m_Name As String
    Public Property Value() As String
        Get
            Return m_Value
        End Get
        Set
            m_Value = Value
        End Set
    End Property
    Private m_Value As String

    Public Shared Function GetFontSizeList() As List(Of FontSize)
        Dim FontSizesList As List(Of FontSize)
        FontSizesList = New List(Of FontSize)
        Dim FontSize As FontSize

        FontSize = New FontSize
        FontSize.Name = "8"
        FontSize.Value = "8px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "9"
        FontSize.Value = "9px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "10"
        FontSize.Value = "10px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "11"
        FontSize.Value = "11px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "12"
        FontSize.Value = "12px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "14"
        FontSize.Value = "14px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "16"
        FontSize.Value = "16px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "18"
        FontSize.Value = "18px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "20"
        FontSize.Value = "20px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "22"
        FontSize.Value = "22px"
        FontSizesList.Add(FontSize)

        FontSize = New FontSize
        FontSize.Name = "24"
        FontSize.Value = "24px"
        FontSizesList.Add(FontSize)

        Return FontSizesList
    End Function
End Class
