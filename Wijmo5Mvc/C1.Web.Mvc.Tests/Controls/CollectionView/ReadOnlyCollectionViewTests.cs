﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataItem = C1.Web.Mvc.Tests.TestHelper.TestDataItem;

namespace C1.Web.Mvc.Tests.Controls.CollectionView
{
    [TestClass]
    public class ReadOnlyCollectionViewTests : BaseTests
    {
        [TestMethod]
        public void ItemsTest()
        {
            var sourceCollection = TestHelper.GetData(10).ToArray();
            var cv = new ReadOnlyCollectionView<DataItem>
            {
                SourceCollection = sourceCollection
            };

            CollectionAssert.AreEqual(sourceCollection, cv.Items.ToArray());
        }

        [TestMethod]
        public void PagingTest()
        {
            const int count = 100;
            var sourceCollection = TestHelper.GetData(count).ToArray();
            var cv = new ReadOnlyCollectionView<DataItem>
            {
                SourceCollection = sourceCollection
            };

            Action<int, int, int> checkPage = (pageIndex, pageSize, expectedPageIndex) =>
            {
                cv.PageSize = pageSize;
                cv.MoveToPage(pageIndex);
                Assert.AreEqual(expectedPageIndex, cv.PageIndex);
                CollectionAssert.AreEqual(sourceCollection.Skip(pageSize*expectedPageIndex).Take(pageSize).ToArray(),
                    cv.Items.ToArray());
                Assert.AreEqual(count, cv.TotalItemCount);
            };

            var currentPageSize = 10;
            checkPage(0, currentPageSize, 0);
            checkPage(2, currentPageSize, 2);

            currentPageSize = 11;
            checkPage(0, currentPageSize, 0);
            checkPage(9, currentPageSize, 9);
            checkPage(10, currentPageSize, 9);

            //disable paging
            cv.PageSize = 0;
            cv.Refresh();
            CollectionAssert.AreEqual(sourceCollection, cv.Items.ToArray());
        }
    }
}
