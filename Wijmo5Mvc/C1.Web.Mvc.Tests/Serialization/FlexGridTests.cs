﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace C1.Web.Mvc.Tests.Serialization
{
    [TestClass]
    public class FlexGridTests : BaseSerializationTests
    {
        [TestMethod]
        public void SimpleTest()
        {
            var control = CreateFlexGrid();
            CheckJson(control);
        }

        [TestMethod]
        public void TestColumns()
        {
            var control = CreateFlexGrid();
            control.AutoGenerateColumns = false;
            control.Columns.Add(new Column(control.Helper) { Binding = "Id", AllowSorting = true, Format = "N", SortMemberPath = "Name" });
            control.Columns.Add(new Column(control.Helper) { Binding = "Boolean", AllowResizing = true, IsRequired = true, IsReadOnly = true });
            control.Columns.Add(new Column(control.Helper) { Binding = "Text", AllowDragging = true, Header = "Text" });
            control.Columns.Add(new Column(control.Helper) { Binding = "Double", DropDownCssClass="css", Width="*" });
            control.Columns.Add(new Column(control.Helper));

            var dataMapCol = new Column(control.Helper);
            dataMapCol.DataMap.ItemsSource.SourceCollection = TestHelper.GetData(13);
            dataMapCol.DataMap.SelectedValuePath = "Id";
            dataMapCol.DataMap.DisplayMemberPath = "Text";
            control.Columns.Add(dataMapCol);
            CheckJson(control);
        }

        [TestMethod]
        public void TestSettings()
        {
            var control = CreateFlexGrid();
            control.AllowAddNew = true;
            control.AllowDelete = true;
            control.AllowDragging = Grid.AllowDragging.Both;
            control.AllowMerging = Grid.AllowMerging.AllHeaders;
            control.AllowResizing = Grid.AllowResizing.ColumnsAllCells;
            control.SortingType = Grid.AllowSorting.SingleColumn;
            control.AutoClipboard = true;
            control.AutoSizeMode = Grid.AutoSizeMode.Cells;
            control.ChildItemsPath = "";
            control.DeferResizing = true;
            control.FrozenColumns = 1;
            control.FrozenRows = 2;
            control.ImeEnabled = false;
            control.IsReadOnly = false;
            control.ItemFormatter = "format";
            control.PreserveOutlineState = true;
            control.PreserveSelectedState = true;
            control.ScrollPosition = new Point(2, 3);
            control.Selection = new Grid.CellRange(0, 1, 2, 3);
            control.ShowColumnFooters = true;
            control.ShowErrors = true;
            control.ShowMarquee = true;
            control.SortRowIndex = 0;
            control.StickyHeaders = true;
            CheckJson(control);
        }

        [TestMethod]
        public void TestCellTemplates()
        {
            var control = CreateFlexGrid();
            Action<CellTemplate> setCellTemplate = t =>
            {
                t.EditTemplateContent = @"<input/>";
                t.EditTemplateId = @"editTemplate";
                t.TemplateContent = @"<div></div>";
                t.TemplateId = @"template";
            };
            setCellTemplate(control.CellsTemplate);
            setCellTemplate(control.TopLeftCellsTemplate);
            setCellTemplate(control.RowHeadersTemplate);
            setCellTemplate(control.ColumnHeadersTemplate);
            CheckJson(control);
        }

        private FlexGrid<TestHelper.TestDataItem> CreateFlexGrid()
        {
            var control = new FlexGrid<TestHelper.TestDataItem>(new FakeHtmlHelper());
            control.Id = "grid1";
            var rc = control.ItemsSource as CollectionViewService<TestHelper.TestDataItem>;
            Assert.IsNotNull(rc, "The ItemsSource of FlexGrid is not CollectionViewService by default.");
            rc.SourceCollection = TestHelper.GetData(100);
            return control;
        }
    }
}
