﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Mvc.Tests.Serialization
{
    [TestClass]
    public class InputNumberTests : BaseSerializationTests
    {
        [TestMethod]
        public void SimpleTest()
        {
            var control = new InputNumber(new FakeHtmlHelper());
            CheckJson(control);
        }

        [TestMethod]
        public void TestValue()
        {
            var control = new InputNumber(new FakeHtmlHelper());
            control.Value = null;
            CheckJson(control, "_null");

            control.Value = 1;
            CheckJson(control, "_1");

            control.Value = -123456.789;
            CheckJson(control, "_123456_789");
        }

        [TestMethod]
        public void TestSettings()
        {
            var control = new InputNumber(new FakeHtmlHelper());
            control.Value = 123456.789;
            control.Id = "number1";
            control.Name = "amount";
            control.IsRequired = true;
            control.Placeholder = "***";
            control.Width = "300pt";
            CheckJson(control);
        }
    }
}
