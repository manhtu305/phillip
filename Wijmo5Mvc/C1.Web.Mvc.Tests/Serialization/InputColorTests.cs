﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

namespace C1.Web.Mvc.Tests.Serialization
{
    [TestClass]
    public class InputColorTests : BaseSerializationTests
    {
        [TestMethod]
        public void SimpleTest()
        {
            var control = CreateControl();
            CheckJson(control);
        }

        [TestMethod]
        public void TestValue()
        {
            var control = CreateControl();
            control.Value = null;
            CheckJson(control, "_null");

            control.Value = new Color();
            CheckJson(control, "_Empty");

            control.Value = Color.Red;
            CheckJson(control, "_red");

            control.Value = Color.FromArgb(1, 1, 1);
            CheckJson(control, "_rgb");

            control.Value = Color.FromArgb(50, 100, 150, 200);
            CheckJson(control, "_argb");
        }

        [TestMethod]
        public void TestSettings()
        {
            var control = CreateControl();
            control.Value = Color.Blue;
            control.Id = "color1";
            control.Name = "color";
            control.IsRequired = false;
            control.Placeholder = "***";
            control.Width = "300pt";
            CheckJson(control);
        }

        private InputColor CreateControl()
        {
            return new InputColor(new FakeHtmlHelper());
        }
    }
}
