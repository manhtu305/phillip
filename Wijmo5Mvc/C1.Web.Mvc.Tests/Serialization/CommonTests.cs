﻿using C1.JsonNet;
using C1.JsonNet.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace C1.Web.Mvc.Tests.Serialization
{
    [TestClass]
    public class CommonTests : BaseSerializationTests
    {
        [TestMethod]
        public void SerializeColor()
        {
            var colorConvertor = new HtmlColorConverter();
            Assert.IsFalse(colorConvertor.CanConvert(typeof(string)));
            Assert.IsTrue(colorConvertor.CanConvert(typeof(Color)));

            Action<Color, string> test = (c, r) =>
            {
                var wrapper = new JsonWriterWrapper();
                colorConvertor.Serialize(wrapper.JsonWriter, c);
                Assert.AreEqual(r, wrapper.Content.Trim('\'', '"').ToLower(), true);
            };

            test(new Color(), "");
            test(Color.Transparent, "Transparent");
            test(Color.Red, "red");
            test(Color.FromArgb(0X11, 0X22, 0X33), "#112233");
            test(Color.FromArgb(0Xaa, 0XEE, 0), "#aaee00");
            test(Color.FromArgb(50, 100, 150, 200), "rgba(100,150,200,0.196)");
        }

        [TestMethod]
        public void DeserializeColor()
        {
            var colorConvertor = new HtmlColorConverter();
            Action<string, Color> test = (t, r) =>
            {
                var wrapper = new JsonReaderWrapper(t);
                var color = (Color)colorConvertor.Read(wrapper, typeof(Color), Color.Empty);
                Assert.AreEqual(r.A, color.A);
                Assert.AreEqual(r.R, color.R);
                Assert.AreEqual(r.G, color.G);
                Assert.AreEqual(r.B, color.B);
            };

            test("''", Color.Empty);
            test("'Transparent'", Color.Transparent);
            test("'#aa1122'", Color.FromArgb(0Xaa, 0X11, 0X22));
            test("'#112233'", Color.FromArgb(0X11, 0X22, 0X33));
            test("'red'", Color.Red);
            test("'rgba(100,150,200,0.196)'", Color.FromArgb(50, 100, 150, 200));
        }

        [TestMethod]
        public void TestOnClientEvent()
        {
            var control = new InputMask(new FakeHtmlHelper());
            control.OnClientGotFocus = "OnClientGotFocus";
            control.OnClientLostFocus = "OnClientLostFocus";
            CheckJson(control);
        }

        [TestMethod]
        public void TestTemplateBinding()
        {
            var control = new InputMask(new FakeHtmlHelper());
            control.IsTemplate = true;
            control.TemplateBindings.Add("id", "id");
            control.TemplateBindings.Add("name", "name");
            CheckJson(control);
        }

        [TestMethod]
        public void SerializeAndDeserialize()
        {
            var data = new TestHelper.TestDataItem
            {
                Id = 0,
                Boolean = true,
                DateTime = new DateTime(2017,1,1),
                Double = 100.1,
                Text = "Text"
            };

            var json = JsonHelper.SerializeObject(data);
            var result = JsonHelper.DeserializeObject<TestHelper.TestDataItem>(json);
            Assert.AreEqual(data, result);
        }

        [TestMethod]
        public void SerializeAndDeserializeCollection()
        {
            var data = TestHelper.GetData(10).ToList();
            var json = JsonHelper.SerializeObject(data);
            var result = JsonHelper.DeserializeObject<List<TestHelper.TestDataItem>>(json);
            CollectionAssert.AreEqual(data, result);
        }

        [TestMethod]
        public void SerializeAndDeserializeDictionary()
        {
            var data = new Dictionary<string, object>
            {
                { "id", 1 },
                { "name", "text"}
            };
            var json = JsonHelper.SerializeObject(data);
            var result = JsonHelper.DeserializeObject<Dictionary<string, object>>(json);
            CollectionAssert.AreEqual(data, result);
        }
    }
}
