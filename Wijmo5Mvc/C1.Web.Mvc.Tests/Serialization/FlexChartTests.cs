﻿using C1.Web.Mvc.Chart;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace C1.Web.Mvc.Tests.Serialization
{
    [TestClass]
    public class FlexChartTests : BaseSerializationTests
    {
        [TestMethod]
        public void SimpleTest()
        {
            var control = CreateControl();
            CheckJson(control);
        }

        [TestMethod]
        public void TestSeries()
        {
            var control = CreateControl();
            control.Series.Add(new ChartSeries<object>(control) { Binding = "Id", BindingX = "Text", ChartType = ChartType.Bar });
            control.Series.Add(new ChartSeries<object>(control) { Binding = "Id", BindingX = "Boolean", ChartType = ChartType.Bubble });
            control.Series.Add(new MovingAverage<object>(control) { Binding = "Id", BindingX = "Boolean", Type = MovingAverageType.Triangular, Visibility = SeriesVisibility.Legend });
            control.Series.Add(new YFunctionSeries<object>(control) { Binding = "Id", BindingX = "Boolean", Func = "func" });
            control.Series.Add(new ParametricFunctionSeries<object>(control) { Binding = "Id", BindingX = "Text", YFunc = "xfunc", XFunc = "xfunc" });
            control.Series.Add(new TrendLine<object>(control) { Binding = "Id", BindingX = "Text", FitType = TrendLineFitType.Exponential, SampleCount = 10, SymbolMarker = Marker.Dot, TrendLineOrder = 10 });
            CheckJson(control);
        }

        [TestMethod]
        public void TestStyles()
        {
            var control = CreateControl();
            control.FooterStyle.Fill = "#112233";
            control.HeaderStyle.Fill = "#112233";
            control.Series.Add(new ChartSeries<object>(control)
            {
                Binding = "Id",
                BindingX = "Text",
                ChartType = ChartType.Bar,
                Style = new SVGStyle { Cx = 12, Fill = "red", FontSize = 14, Height = 22 },
                AltStyle = new SVGStyle { Cx = 12, Fill = "red", FontSize = 14, Height = 22 }
            });
            CheckJson(control);
        }

        [TestMethod]
        public void TestAxises()
        {
            var control = CreateControl();
            Action<ChartAxis<object>> customize = a => {
                a.AxisLine = true;
                a.Binding = "test";
                a.Format = "c";
                a.ItemFormatter = "format";
                a.ItemsSource.SourceCollection = TestHelper.GetData(10);
                a.LabelAlign = "left";
                a.LabelAngle = 10;
                a.LabelPadding = 5;
                a.Labels = true;
                a.LogBase = 0.5;
                a.MajorGrid = true;
                a.MajorTickMarks = AxisTickMark.Inside;
                a.MajorUnit = 2;
                a.Max = 100.123;
                a.Min = 1.456;
                a.MinorGrid = false;
                a.MinorTickMarks = AxisTickMark.Outside;
                a.MinorUnit = 1;
                a.OnClientRangeChanged = "OnClientRangeChanged";
                a.Origin = 23.98;
                a.OverlappingLabels = AxisOverlappingLabels.Auto;
                a.PlotAreaIndex = 0;
                a.Position = Position.Left;
                a.Reversed = false;
                a.Title = "title";
            };
            customize(control.AxisX);
            customize(control.AxisY);

            var series = new ChartSeries<object>(control) { Binding = "Id", BindingX = "Text", ChartType = ChartType.Bar };
            series.AxisX = new ChartAxis<object>(control, true);
            series.AxisY = new ChartAxis<object>(control, false);
            control.Series.Add(series);
            customize(series.AxisX);
            customize(series.AxisY);

            CheckJson(control);
        }

        [TestMethod]
        public void TestPlotAreas()
        {
            var control = CreateControl();
            control.PlotAreas.Add(new PlotArea());
            control.PlotAreas.Add(new PlotArea { Column = 1, Row = 2, Width = "200px" });
            CheckJson(control);
        }

        [TestMethod]
        public void TestSettings()
        {
            var control = CreateControl();
            control.ChartType = ChartType.Funnel;
            control.DataLabel.Border = true;
            control.DataLabel.Content = "content";
            control.Footer = "footer";
            control.Header = "header";
            control.ItemFormatter = "format";
            control.Legend.Position = Position.Bottom;
            control.LegendToggle = true;
            control.Rotated = true;
            control.SelectionIndex = 1;
            control.SelectionMode = SelectionMode.None;
            control.SymbolSize = 10.3f;
            control.Tooltip.Content = @"<span></span>";
            CheckJson(control);
        }

        private FlexChart<object> CreateControl()
        {
            var control = new FlexChart<object>(new FakeHtmlHelper());
            var rc = control.ItemsSource as IItemsSource<object>;
            Assert.IsNotNull(rc, "The ItemsSource of FlexChart is not CollectionViewService by default.");
            rc.SourceCollection = TestHelper.GetData(37);
            control.Binding = "Id";
            control.BindingX = "Name";
            return control;
        }
    }
}
