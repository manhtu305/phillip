﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Mvc.Tests.Serialization
{
    [TestClass]
    public class CollectionViewServiceTests : BaseSerializationTests
    {
        [TestMethod]
        public void SimpleTest()
        {
            var control = new CollectionViewService<TestHelper.TestDataItem>(new FakeHtmlHelper());
            control.SourceCollection = TestHelper.GetData(100);
            CheckJson(control);
        }

        [TestMethod]
        public void SortTest()
        {
            var control = new CollectionViewService<TestHelper.TestDataItem>(new FakeHtmlHelper());
            control.SourceCollection = TestHelper.GetData(10);
            control.SortDescriptions.Add(new SortDescription());
            control.SortDescriptions.Add(new SortDescription { Ascending = true, Property = "Name"});
            control.SortDescriptions.Add(new SortDescription { Property = "Id" });
            CheckJson(control);
        }

        [TestMethod]
        public void GroupTest()
        {
            var control = new CollectionViewService<TestHelper.TestDataItem>(new FakeHtmlHelper());
            control.SourceCollection = TestHelper.GetData(10);
            control.GroupDescriptions.Add(new PropertyGroupDescription());
            control.GroupDescriptions.Add(new PropertyGroupDescription { PropertyName = "Id", ClientConverter = "test" });
            CheckJson(control);
        }

        [TestMethod]
        public void RemoteBindTest()
        {
            var control = new CollectionViewService<TestHelper.TestDataItem>(new FakeHtmlHelper());
            control.ReadActionUrl = "read";
            control.DeleteActionUrl = "delete";
            control.CreateActionUrl = "create";
            control.UpdateActionUrl = "update";
            CheckJson(control);
        }

        [TestMethod]
        public void VirtualScrollingTest()
        {
            var control = new CollectionViewService<TestHelper.TestDataItem>(new FakeHtmlHelper());
            control.SourceCollection = TestHelper.GetData(200);
            control.InitialItemsCount = 50;
            CheckJson(control);
        }

        [TestMethod]
        public void PagingTest()
        {
            var control = new CollectionViewService<TestHelper.TestDataItem>(new FakeHtmlHelper());
            control.SourceCollection = TestHelper.GetData(200);
            control.PageSize = 10;
            control.PageIndex = 2;
            CheckJson(control);
        }

        [TestMethod]
        public void DisableServerReadTest()
        {
            var control = new CollectionViewService<TestHelper.TestDataItem>(new FakeHtmlHelper());
            control.SourceCollection = TestHelper.GetData(200);
            control.DisableServerRead = true;
            CheckJson(control);
        }

        [TestMethod]
        public void DateTimesTest()
        {
            var control = new CollectionViewService<TestHelper.TestDateTimesItem>(new FakeHtmlHelper());
            control.SourceCollection = TestHelper.GetDateTimesData(5);
            control.DisableServerRead = true;
            CheckJson(control);
        }
    }
}
