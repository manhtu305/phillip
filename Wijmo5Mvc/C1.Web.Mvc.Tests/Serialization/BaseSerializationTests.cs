﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace C1.Web.Mvc.Tests.Serialization
{
    public abstract class BaseSerializationTests : BaseTests
    {
        protected void CheckJson(string json, string nameSuffix = null)
        {
            const string jsonEx = ".json";
            nameSuffix = (nameSuffix ?? "") + jsonEx;
            var resultFile = CurrentResultPath + nameSuffix;
            File.WriteAllText(resultFile, json);

            var expectedFile = CurrentExpectedResultPath + nameSuffix;
            Assert.IsTrue(File.Exists(expectedFile), "Cannot find expected file: " + expectedFile);

            TestHelper.CompareFile(expectedFile, resultFile);
        }

        protected void CheckJson(Component component, string nameSuffix = null)
        {
            var json = component.SerializeOptions();
            CheckJson(json, nameSuffix);
        }
    }
}
