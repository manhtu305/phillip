﻿using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Tests
{
    public static partial class TestHelper
    {
        public class TestDataItem
        {
            public int Id { get; set; }
            public double Double { get; set; }
            public string Text { get; set; }
            public bool Boolean { get; set; }
            public DateTime DateTime { get; set; }
            public decimal Decimal { get; set; }
            public float Float { get; set; }
            public short Short { get; set; }
            public long Long { get; set; }
            public uint Uint { get; set; }
            public int? NullableInt { get; set; }
            public double? NullableDouble { get; set; }
            public bool? NullableBoolean { get; set; }
            public DateTime? NullableDateTime { get; set; }
            public decimal? NullableDecimal { get; set; }
            public float? NullableFloat { get; set; }
            public short? NullableShort { get; set; }
            public long? NullableLong { get; set; }

            public override bool Equals(object obj)
            {
                var that = this as TestDataItem;
                if (that == null) return false;

                return that.Boolean == Boolean && that.Id == Id
                    && that.Double == Double && that.Text == Text
                    && that.DateTime == DateTime && that.Decimal == Decimal
                    && that.Float == Float && that.Short == Short
                    && that.Long == Long && that.Uint == Uint
                    && that.NullableInt == NullableInt && that.NullableDouble == NullableDouble
                    && that.NullableBoolean == NullableBoolean && that.NullableDateTime == NullableDateTime
                    && that.NullableDecimal == NullableDecimal && that.NullableFloat == NullableFloat
                    && that.NullableShort == NullableShort && that.NullableLong == NullableLong;
            }

            public override int GetHashCode()
            {
                var hashCode = Id.GetHashCode() ^ Double.GetHashCode() ^ Boolean.GetHashCode() ^ DateTime.GetHashCode()
                    ^ Decimal.GetHashCode() ^ Float.GetHashCode() ^ Short.GetHashCode() ^ Long.GetHashCode() ^ Uint.GetHashCode();
                if (Text != null) hashCode ^= Text.GetHashCode();
                if (NullableInt != null) hashCode ^= NullableInt.GetHashCode();
                if (NullableDouble != null) hashCode ^= NullableDouble.GetHashCode();
                if (NullableBoolean != null) hashCode ^= NullableBoolean.GetHashCode();
                if (NullableDateTime != null) hashCode ^= NullableDateTime.GetHashCode();
                if (NullableFloat != null) hashCode ^= NullableFloat.GetHashCode();
                if (NullableDecimal != null) hashCode ^= NullableDecimal.GetHashCode();
                if (NullableShort != null) hashCode ^= NullableShort.GetHashCode();
                if (NullableLong != null) hashCode ^= NullableLong.GetHashCode();
                return hashCode;
            }
        }

        public static IList<TestDataItem> GetData(int count)
        {
            var data = new List<TestDataItem>();
            for (var i = 0; i < count; i++)
            {
                data.Add(new TestDataItem
                {
                    Id = i,
                    Text = GetStringItem(i),
                    Boolean = i % 2 == 0,
                    Double = GetDoubleItem(i),
                    DateTime = GetDateTimeItem(i),
                    Decimal = i * 1.1m,
                    Float = i * 1.2f,
                    Short = (short)(i / 2),
                    Long = -i,
                    Uint = (uint)(i * 2),
                    NullableInt = i % 2 == 0 ? null : (int?)i,
                    NullableDouble = i % 4 == 0 ? null : (double?)GetDoubleItem(i),
                    NullableBoolean = i % 3 == 0 ? null : (bool?)(i % 2 == 0),
                    NullableDateTime = i % 5 == 0 ? null : (DateTime?)GetDateTimeItem(i),
                    NullableDecimal = i % 6 == 0 ? null : (decimal?)i * 1.3m,
                    NullableFloat = i % 7 == 0 ? null : (float?)i * 1.4f,
                    NullableShort = i % 8 == 0 ? null : (short?)(i / 2),
                    NullableLong = i % 9 == 0 ? null : (long?)-i,
                });
            }

            return data;
        }

        private static double GetDoubleItem(int i)
        {
            return i * i / 100d;
        }

        private static string GetStringItem(int i)
        {
            string prefix;
            switch (i % 3)
            {
                case 2:
                    prefix = "B";
                    break;
                case 1:
                    prefix = "C";
                    break;
                default:
                    prefix = "A";
                    break;
            }

            return prefix + i;
        }

        private static DateTime GetDateTimeItem(int i)
        {
            var baseDateTime = new DateTime(2015, 1, 2, 3, 4, 5, DateTimeKind.Utc);
            switch (i % 3)
            {
                case 2:
                    return baseDateTime.AddMonths(i);
                case 1:
                    return baseDateTime.AddDays(i);
                default:
                    return baseDateTime.AddHours(i);
            }
        }

        public class TestDateTimesItem
        {
            public int Id { get; set; }
            public DateTime Utc { get; set; }
            public DateTime Local { get; set; }
            public DateTime Unspecified { get; set; }
        }

        public static IList<TestDateTimesItem> GetDateTimesData(int count)
        {
            var data = new List<TestDateTimesItem>();
            for (var i = 0; i < count; i++)
            {
                data.Add(new TestDateTimesItem
                {
                    Id = i,
                    Utc = new DateTime(2015, 1, 2, 3, 4, 5, DateTimeKind.Utc),
                    Unspecified = new DateTime(2015, 1, 2, 3, 4, 5, DateTimeKind.Unspecified),
                    Local = new DateTime(2015, 1, 2, 3, 4, 5, DateTimeKind.Local)
                });
            }
            return data;
        }
    }
}
