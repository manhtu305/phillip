﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Mvc.Tests
{
    public static partial class TestHelper
    {
        public const double DoubleDelta = 0.0001;
        private static readonly object _rootDirLocker = new object();
        private static string _rootDir;
        private const string _expectedResultsDirName = "TestExpectedResults";
        private const string _samplesDirName = "TestSamples";
        // The default timezone text is +08:00, all the expected result files should use this timezone text.
        private const string ExpectedLocalTimeZoneText = "+08:00";
        private static string LocalTimeZoneText = JsonNet.JsonUtility.GetTimeOffsetText(new DateTime(2015, 1, 2, 3, 4, 5, DateTimeKind.Local));

        public static void CompareFile(string expectedFile, string currentFile)
        {
            var current = File.ReadAllText(currentFile);
            var expected = ProcessLocalDateTime(File.ReadAllText(expectedFile));
            CompareContent(expected, current, Path.GetExtension(expectedFile), string.Format("{0} is different from the expected file: {1}.", currentFile, expectedFile));
        }

        // To avoid the issue that different local timezone makes the unit test fail,
        // update the time zone text in expected result file to match the actual timezone text where the unit test runs.
        private static string ProcessLocalDateTime(string text)
        {
            return text.Replace(ExpectedLocalTimeZoneText, LocalTimeZoneText);
        }

        public static void CompareFile(string expectedFile, Stream currentFile)
        {
            var current = StreamToString(currentFile);
            var expected = File.ReadAllText(expectedFile);
            CompareContent(expected, current, Path.GetExtension(expectedFile), string.Format("{0} is different from the expected file: {1}.", "Current stream", expectedFile));
        }

        public static void CompareContent(string expected, string current, string format = null, string failMsg = null)
        {
            failMsg = failMsg ?? "Contents are different.";
            format = format ?? string.Empty;
            if (format.EndsWith("json", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    CompareJson(expected, current);
                }
                catch (AssertFailedException ex)
                {
                    throw new AssertFailedException(failMsg + ex.Message, ex);
                }

                return;
            }

            Assert.AreEqual(expected, current, failMsg);
        }

        public static string StreamToString(Stream stream)
        {
            stream.Position = 0;
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        public static string GetRootDir(this TestContext context)
        {
            if (_rootDir != null) return _rootDir;
            lock (_rootDirLocker)
            {
                if (_rootDir != null) return _rootDir;

                var testDir = context.TestRunDirectory;
                var rootDir = Path.Combine(testDir, "..", "..");
                var expectedFilesDir = Path.Combine(rootDir, _expectedResultsDirName);
                if (Directory.Exists(expectedFilesDir))
                {
                    // In tfs build:
                    return _rootDir = rootDir;
                }

                // In dev machine:
                return _rootDir = Path.Combine(rootDir, "..", typeof(TestHelper).Assembly.GetName().Name);
            }
        }

        public static string GetSamplesDir(this TestContext context, string samplePath = null)
        {
            var path = Path.Combine(context.GetRootDir(), _samplesDirName);
            return string.IsNullOrEmpty(samplePath) ? path : Path.Combine(path, samplePath);
        }

        public static string GetExpectedResultsDir(this TestContext context, string resultPath = null)
        {
            var path = Path.Combine(context.GetRootDir(), _expectedResultsDirName);
            return string.IsNullOrEmpty(resultPath) ? path : Path.Combine(path, resultPath);
        }

        public static string GetSimpleTypeName(Type type)
        {
            const string genericTypeSeparator = "`";
            var typeName = type.Name;
            var genericTypeSeparatorIndex = typeName.IndexOf(genericTypeSeparator);
            return genericTypeSeparatorIndex > -1 ? typeName.Substring(0, genericTypeSeparatorIndex) : typeName;
        }

        public static string GetLastNamespace(Type type)
        {
            var asmName = typeof(TestHelper).Assembly.GetName().Name;
            var typeNamespace = type.Namespace;
            var index = typeNamespace.LastIndexOf('.');
            if (index == -1 || index >= typeNamespace.Length) return string.Empty;

            var pre = typeNamespace.Substring(0, index);
            if(!string.Equals(pre, asmName, StringComparison.OrdinalIgnoreCase)) return string.Empty;

            return typeNamespace.Substring(index + 1);
        }

        public static void EnsureDir(string dir)
        {
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
        }

        public static bool IsNumberType(object value)
        {
            if (value == null) return false;

            var type = value.GetType();
            return (type == typeof(float)
                || type == typeof(int)
                || type == typeof(uint)
                || type == typeof(double)
                || type == typeof(sbyte)
                || type == typeof(byte)
                || type == typeof(long)
                || type == typeof(ulong)
                || type == typeof(ushort)
                || type == typeof(short)
                || type == typeof(decimal));
        }
    }
}
