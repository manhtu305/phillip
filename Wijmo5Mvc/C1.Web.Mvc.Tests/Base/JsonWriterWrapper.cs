﻿using C1.JsonNet;
using System.IO;

namespace C1.Web.Mvc.Tests
{
    public class JsonWriterWrapper
    {
        public JsonWriterWrapper()
        {
            TextWriter = new StringWriter();
            JsonWriter = new JsonWriter(TextWriter, null);
        }

        public JsonWriter JsonWriter
        {
            get;
            private set;
        }

        public StringWriter TextWriter
        {
            get;
            private set;
        }

        public string Content
        {
            get
            {
                return TextWriter.ToString();
            }
        }
    }
}
