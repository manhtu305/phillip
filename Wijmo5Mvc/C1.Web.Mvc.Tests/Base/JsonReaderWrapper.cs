﻿using C1.JsonNet;
using System.IO;

namespace C1.Web.Mvc.Tests
{
    public class JsonReaderWrapper : JsonReader
    {
        public JsonReaderWrapper(string content) : base(new StringReader(content), null)
        {
        }
    }
}
