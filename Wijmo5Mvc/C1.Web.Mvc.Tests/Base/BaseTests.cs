﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace C1.Web.Mvc.Tests
{
    public abstract class BaseTests
    {
        public TestContext TestContext { get; set; }

        protected virtual string ClassName
        {
            get
            {
                return TestHelper.GetSimpleTypeName(GetType());
            }
        }

        protected virtual string ResourceFolder
        {
            get
            {
                return TestHelper.GetLastNamespace(GetType());
            }
        }

        protected virtual string ResourceFile
        {
            get
            {
                return ClassName + "." + TestContext.TestName;
            }
        }

        protected virtual string SamplesDir
        {
            get
            {
                return TestContext.GetSamplesDir(ResourceFolder);
            }
        }

        protected virtual string CurrentSamplePath
        {
            get
            {
                return Path.Combine(SamplesDir, ResourceFile);
            }
        }

        protected virtual string ExpectedResultsDir
        {
            get
            {
                var dir = TestContext.GetExpectedResultsDir(ResourceFolder);
                TestHelper.EnsureDir(dir);
                return dir;
            }
        }

        protected virtual string CurrentExpectedResultPath
        {
            get
            {
                return Path.Combine(ExpectedResultsDir, ResourceFile);
            }
        }

        protected virtual string ResultsDir
        {
            get
            {
                var dir = string.IsNullOrEmpty(ResourceFolder) ? TestContext.TestResultsDirectory 
                    : Path.Combine(TestContext.TestResultsDirectory, ResourceFolder);
                TestHelper.EnsureDir(dir);
                return dir;
            }
        }

        protected virtual string CurrentResultPath
        {
            get
            {
                return Path.Combine(ResultsDir, ResourceFile);
            }
        }
    }
}
