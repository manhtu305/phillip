﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System;
using System.Globalization;

namespace C1.Web.Mvc.Tests
{
    public static partial class TestHelper
    {
        public static bool JsonOrderSensitive = false;

        private static void CompareJson(string expected, string current)
        {
            var s = new JsonSerializer();
            var expectedJson = (JToken)s.Deserialize(new JsonTextReaderEx(expected));
            var currentJson = (JToken)s.Deserialize(new JsonTextReaderEx(current));
            CompareJToken(expectedJson, currentJson);
        }

        private static void CompareJToken(JToken expectedJson, JToken currentJson, string path = null)
        {
            path = path ?? string.Empty;
            if (TryCompareAsJObject(expectedJson, currentJson, path))
            {
                return;
            }

            if (TryCompareAsArray(expectedJson, currentJson, path))
            {
                return;
            }

            if (TryCompareAsNumber(expectedJson, currentJson, path))
            {
                return;
            }

            Assert.IsTrue(JToken.DeepEquals(expectedJson, currentJson),
                string.Format("Path: {0}. The json content are different.", path));
        }

        private static bool TryCompareAsNumber(JToken expectedJson, JToken currentJson, string path)
        {
            var expected = expectedJson as JValue;
            var current = currentJson as JValue;
            if (expected == null || current == null )
            {
                return false;
            }

            var expectedValue = expected.Value;
            var currentValue = current.Value;
            if (expectedValue == null || currentValue == null)
            {
                return false;
            }

            if (IsNumberType(expectedValue) && IsNumberType(currentValue))
            {
                double expectedDouble = ((IConvertible)expectedValue).ToDouble(CultureInfo.CurrentCulture);
                double currentDouble = ((IConvertible)currentValue).ToDouble(CultureInfo.CurrentCulture);
                Assert.AreEqual(expectedDouble, currentDouble, DoubleDelta, string.Format("Path: {0}. Number value is not expected.", path));
                return true;
            }

            return false;
        }

        private static bool TryCompareAsArray(JToken expectedJson, JToken currentJson, string path)
        {
            var expected = expectedJson as JArray;
            var current = currentJson as JArray;
            if (expected == null || current == null)
            {
                return false;
            }

            var expectedItems = expected.Children().ToList();
            var currentItems = current.Children().ToList();

            Assert.IsTrue((expectedItems == null && currentItems == null)
                || (expectedItems != null && currentItems != null),
                string.Format("Path: \"{0}\". Json array are different.", path));

            var count = expectedItems.Count;
            Assert.AreEqual(count, currentItems.Count,
                string.Format("Path: \"{0}\". Json array's count is not expected.", path));

            for (var index = 0; index < count; index++)
            {
                CompareJToken(expectedItems[index], currentItems[index], path + "[" + index + "]");
            }

            return true;
        }

        private static bool TryCompareAsJObject(JToken expectedJson, JToken currentJson, string path)
        {
            var expected = expectedJson as JObject;
            var current = currentJson as JObject;
            if (expected == null || current == null)
            {
                return false;
            }
            var expectedProperties = expected.Properties().ToList();
            var currentProperties = current.Properties().ToList();

            Assert.IsTrue((expectedProperties == null && currentProperties == null)
                || (expectedProperties != null && currentProperties != null),
                string.Format("Path: \"{0}\". Json properties are different.", path));

            var count = expectedProperties.Count;
            Assert.AreEqual(count, currentProperties.Count,
                string.Format("Path: \"{0}\". Json properties' count is not expected.", path));

            var expectedDic = expectedProperties.ToDictionary(p => p.Name);
            for (var index = 0; index < count; index++)
            {
                JProperty ePro;
                var pro = currentProperties[index];
                if (JsonOrderSensitive)
                {
                    ePro = expectedProperties[index];
                    Assert.AreEqual(ePro.Name, pro.Name,
                        string.Format("Path: {0}. Property name is different. Maybe the properties order is different.", path));
                }
                else
                {
                    Assert.IsTrue(expectedDic.TryGetValue(pro.Name, out ePro),
                        string.Format("Path: {0}. Property {1} is not expected.", path, pro.Name));
                }

                CompareJToken(ePro.Value, pro.Value, path + "." + ePro.Name);
            }

            return true;
        }
    }
}