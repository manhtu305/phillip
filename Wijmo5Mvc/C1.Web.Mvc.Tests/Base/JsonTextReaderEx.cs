﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace C1.Web.Mvc.Tests
{
    internal class JsonTextReaderEx : JsonTextReader
    {
        private const char _spaceChar = ' ';
        private const char _tabChar = '\t';
        private const char _returnChar = '\r';
        private const char _newlineChar = '\n';
        private readonly static char[] _whiteSpaces = new[] { _spaceChar, _tabChar, _returnChar, _newlineChar };
        private const char _singleQuoteChar = '\'';
        private const char _doubleQuoteChar = '"';
        private const char _escapeChar = '\\';
        private const char _objEnd = '}';
        private const char _propertySeparator = ',';
        private readonly FieldInfo _charPosField;
        private readonly FieldInfo _charsField;
        private readonly MethodInfo _ensureCharsMethod;
        private const char _scopeStart = '(';
        private const char _scopeEnd = ')';
        private const string _cstrStart = "new";

        public JsonTextReaderEx(string content) : base(new StringReader(content))
        {
            _charPosField = typeof(JsonTextReader).GetField("_charPos", BindingFlags.NonPublic | BindingFlags.Instance);
            _charsField = typeof(JsonTextReader).GetField("_chars", BindingFlags.NonPublic | BindingFlags.Instance);
            _ensureCharsMethod = typeof(JsonTextReader).GetMethod("EnsureChars", BindingFlags.NonPublic | BindingFlags.Instance);
        }

        private bool EnsureChars(int relativePosition, bool append)
        {
            return (bool)_ensureCharsMethod.Invoke(this, new object[] { relativePosition, append });
        }

        private char[] Chars
        {
            get
            {
                return (char[])_charsField.GetValue(this);
            }
        }

        private int CharPos
        {
            get
            {
                return (int)_charPosField.GetValue(this);
            }
            set
            {
                _charPosField.SetValue(this, value);
            }
        }

        public override bool Read()
        {
            var pos = CharPos;
            if(CurrentState == State.Property && EnsureChars(_cstrStart.Length, true))
            {
                var chars = Chars;
                var str = string.Join("", chars.Skip(pos).Take(Math.Min(_cstrStart.Length, chars.Length - pos)));
                if (str == _cstrStart)
                {
                    ParseRaw();
                    return true;
                }
            }

            try
            {
                return base.Read();
            }
            catch (JsonReaderException)
            {
                if (CurrentState != State.Property)
                {
                    throw;
                }

                CharPos = pos;
                ParseRaw();
                return true;
            }
        }

        private void ParseRaw()
        {
            var current = string.Empty;
            char? currentQuoteChar = null;
            bool inEscape = false;
            int scopeLevel = 0;
            while (EnsureChars(1, true))
            {
                var currentChar = Chars[CharPos++];
                if (inEscape)
                {
                    current += currentChar;
                    inEscape = false;
                    continue;
                }

                if (currentQuoteChar.HasValue)
                {
                    if (currentChar == currentQuoteChar)
                    {
                        currentQuoteChar = null;
                    }
                    else if (currentChar == _escapeChar)
                    {
                        inEscape = true;
                    }

                    current += currentChar;
                    continue;
                }

                if (currentChar == _scopeStart)
                {
                    scopeLevel++;
                }
                else if (currentChar == _scopeEnd)
                {
                    scopeLevel--;
                }
                else if (scopeLevel == 0 && current != _cstrStart &&
                    (_whiteSpaces.Contains(currentChar) || currentChar == _objEnd || currentChar == _propertySeparator))
                {
                    CharPos--;
                    break;
                }

                current += currentChar;
            }

            SetRawToken(current);
        }

        private void SetRawToken(string current)
        {
            SetToken(JsonToken.Raw, current);
        }
    }
}