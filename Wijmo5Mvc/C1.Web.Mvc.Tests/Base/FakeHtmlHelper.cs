﻿using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace C1.Web.Mvc.Tests
{
    public class FakeHtmlHelper : HtmlHelper
    {
        public FakeHtmlHelper():base(new FakeViewContext(), new FakeViewDataContainer { ViewData = new ViewDataDictionary()})
        {
        }

        public class FakeViewDataContainer : IViewDataContainer
        {
            public ViewDataDictionary ViewData { get; set; }
        }

        public class FakeViewContext : ViewContext
        {
            public FakeViewContext()
            {
                HttpContext = new FakeHttpContext();
                ViewData = new ViewDataDictionary();
            }
        }

        public class FakeHttpContext : HttpContextBase
        {
            private IDictionary _items;

            public override IDictionary Items
            {
                get
                {
                    return _items ?? (_items = new Dictionary<object, object>());
                }
            }
        }
    }
}
