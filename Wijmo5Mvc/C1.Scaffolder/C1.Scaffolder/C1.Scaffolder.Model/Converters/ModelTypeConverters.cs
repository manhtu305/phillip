﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace C1.Web.Mvc.Converters
{
    internal abstract class ModelTypeConverterBase : TypeConverter
    {
        private const string NoneValueDisplayName = "(none)";

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var displayName = value as string;
            if (displayName != null)
            {
                if (string.IsNullOrEmpty(displayName) || displayName == NoneValueDisplayName)
                {
                    return null;
                }

                return GetModelTypes().FirstOrDefault(m => displayName == m.DisplayName);
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) && value == null)
            {
                return NoneValueDisplayName;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true;
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            var values = GetModelTypes().Select(t => t.DisplayName).ToList();
            values.Insert(0, NoneValueDisplayName);
            return new StandardValuesCollection(values);
        }

        protected abstract IList<IModelTypeDescription> GetModelTypes();
    }

    internal class DbContextTypeConverter : ModelTypeConverterBase
    {
        protected override IList<IModelTypeDescription> GetModelTypes()
        {
            var dbService = ServiceManager.DbContextProvider;
            return dbService.DbContextTypes;
        }
    }

    internal class ModelTypeConverter : ModelTypeConverterBase
    {
        protected override IList<IModelTypeDescription> GetModelTypes()
        {
            var dbService = ServiceManager.DbContextProvider;
            return dbService.ModelTypes;
        }
    }
}
