﻿using C1.Web.Mvc.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Converters
{
    public class PointFConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            var content = value as string;
            if (content == null)
                return base.ConvertFrom(context, culture, value);

            content = content.Trim();
            if (content.Length == 0) return null;

            if (culture == null)
                culture = CultureInfo.CurrentCulture;
            char separator = culture.TextInfo.ListSeparator[0];

            var strArray = content.Split(new[] { separator });
            if (strArray.Length != 2)
                throw new ArgumentException(
                    string.Format(Resources.Exception_CannotBeParsed, content));

            var numArray = new float[strArray.Length];
            for (var index = 0; index < strArray.Length; index++)
            {
                float num;
                if (!float.TryParse(strArray[index], NumberStyles.Any, culture, out num))
                    throw new ArgumentException(string.Format(Resources.Exception_NotValid, strArray[index]));
                numArray[index] = num;
            }

            return new PointF(numArray[0], numArray[1]);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string)) return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == null)
                throw new ArgumentNullException("destinationType");
            if (!(value is PointF))
                return base.ConvertTo(context, culture, value, destinationType);

            if (destinationType == typeof(string))
            {
                var point = (PointF)value;
                if (culture == null) culture = CultureInfo.CurrentCulture;
                var separator = culture.TextInfo.ListSeparator[0] + " ";
                var strArray = new string[2];
                strArray[0] = point.X.ToString(culture);
                strArray[1] = point.Y.ToString(culture);
                return string.Join(separator, strArray);
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            return TypeDescriptor.GetProperties(typeof(PointF), attributes).Sort(new[] { "X", "Y" });
        }

        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }
    }
}
