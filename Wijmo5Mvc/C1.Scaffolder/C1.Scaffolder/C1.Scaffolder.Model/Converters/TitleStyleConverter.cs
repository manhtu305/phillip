﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Converters
{
    public class TitleStyleConverter : ExpandableObjectConverter
    {
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            // remove FontSize for header section
            if (context != null && context.Instance != null && context.Instance is C1.Web.Mvc.HeaderSection<object>)
            {
                var properties = new List<PropertyDescriptor>();
                foreach (PropertyDescriptor property in base.GetProperties(context, value, attributes))
                {
                    if (property.Name == "FontSize") continue;
                    properties.Add(property);
                }

                return new PropertyDescriptorCollection(properties.ToArray());
            }

            return base.GetProperties(context, value, attributes);
        }
    }
}
