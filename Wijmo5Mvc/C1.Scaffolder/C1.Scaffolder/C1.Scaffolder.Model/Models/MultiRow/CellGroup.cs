﻿namespace C1.Web.Mvc.MultiRow
{
    partial class CellGroup
    {
        /// <summary>
        /// Convert to string.
        /// </summary>
        /// <returns>The string.</returns>
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Header))
            {
                return Header;
            }

            return GetType().Name;
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            var group = new CellGroup();

            group.Header = Header;
            group.Colspan = Colspan;

            foreach (var cell in Cells)
            {
                group.Cells.Add((Cell)cell.Clone());
            }

            return group;
        }
    }
}
