﻿using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc.MultiRow
{
    partial class MultiRow<T>
    {
        public override void InitControl()
        {
            base.InitControl();
            DesignCellGroups = new List<CellGroup>();
            AutoGenerateColumns = true;
            DesignAutoGenerateColumns = true;
        }

        public override List<ColumnBase> DesignColumns
        {
            get
            {
                if (DesignCellGroups == null) return new List<ColumnBase>();

                return DesignCellGroups.SelectMany(g => g.Cells).OfType<ColumnBase>().ToList();
            }
        }

        /// <summary>
        /// Gets the collection of cell groups for design usage.
        /// </summary>
        [C1Ignore]
        [IgnoreTemplateParameter]
        public List<CellGroup> DesignCellGroups { get; private set; }

        public override GridControlType ControlType
        {
            get { return GridControlType.MultiRow; }
        }

        internal override bool IsDesignColumnsEmpty
        {
            get
            {
                return DesignCellGroups == null || !DesignCellGroups.Any();
            }
        }

        internal override void ClearDesignColumns()
        {
            if (DesignCellGroups != null) DesignCellGroups.Clear();
        }

        internal override void FillDesignColumns(bool forUpdate = false)
        {
            if (forUpdate)
            {
                ClearDesignColumns();
                DesignGroupDescriptions.Clear();
                DesignSortDescriptions.Clear();
                DesignAutoGenerateColumns = false;
                foreach (var columnGroup in LayoutDefinition)
                {
                    columnGroup.Header = "All";
                    DesignCellGroups.Add(columnGroup);
                    foreach (var cell in columnGroup.Cells)
                    {
                        DesignGroupDescriptions.Add(new DesignPropertyGroupDescription { PropertyName = cell.Binding });
                        DesignSortDescriptions.Add(new DesignSortDescription { Property = cell.Binding, Ascending = true });
                    }
                }
                LayoutDefinition.Clear();
            }
            else
            {
                DesignCellGroups = new List<CellGroup>();

                if (SelectedModelProperties == null) return;

                var group = new CellGroup();
                group.Header = "All";
                DesignCellGroups.Add(group);

                foreach (var property in SelectedModelProperties)
                {
                    var cell = new Cell { Binding = property.Name };
                    group.Cells.Add(cell);
                }
                group.Colspan = group.Cells.Count;
            }
        }

        public override void BeforeSerialize()
        {
            base.BeforeSerialize();

            foreach (var cellGroup in DesignCellGroups)
            {                 
                LayoutDefinition.Add(cellGroup);
            }
        }

        internal override bool ShouldApplyColumns
        {
            get { return false; }
        }
    }
}
