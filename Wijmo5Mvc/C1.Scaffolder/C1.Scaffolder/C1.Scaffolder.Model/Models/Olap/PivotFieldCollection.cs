﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Olap
{
    public partial class PivotFieldCollection : IC1Property
    {
        /// <summary>
        /// Gets or sets the c1-property attribute for taghelper.
        /// </summary>
        [Browsable(false)]
        public string C1Property { get; set; }

        public bool ShouldSerializeItems()
        {
            return Items != null && Items.Count > 0;
        }
    }
}
