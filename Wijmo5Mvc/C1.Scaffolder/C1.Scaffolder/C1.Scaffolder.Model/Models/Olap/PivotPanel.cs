﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C1.Web.Mvc.Serialization;
using System.ComponentModel;

namespace C1.Web.Mvc.Olap
{
    public partial class PivotPanel : ISupportUpdater
    {
        [C1Ignore]
        [Browsable(false)]
        public MVCControl ParsedSetting { get; set; }

        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[] {
                "OnClientItemsSourceChanged",
                "OnClientViewDefinitionChanged",
                "OnClientUpdatingView",
                "OnClientUpdatedView"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }
    }
}
