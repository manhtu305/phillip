﻿using C1.Web.Mvc.Serialization;
using System.ComponentModel;
namespace C1.Web.Mvc
{
    partial class Line
    {
        [C1Description("Line_Start")]
        [DisplayName("Start")]
        [C1Ignore]
        public DataPoint DesignStart
        {
            get
            {
                return Start ?? (Start = new DataPoint());
            }
        }

        [C1Description("Line_End")]
        [DisplayName("End")]
        [C1Ignore]
        public DataPoint DesignEnd
        {
            get
            {
                return End ?? (End = new DataPoint());
            }
        }

        /// <summary>
        /// Specifies whether the Start property should be serialized.
        /// </summary>
        public bool ShouldSerializeStart()
        {
            return Start != null && Start.X != null && Start.Y != null;
        }

        /// <summary>
        /// Specifies whether the End property should be serialized.
        /// </summary>
        public bool ShouldSerializeEnd()
        {
            return End != null && End.X != null && End.Y != null;
        }
    }
}
