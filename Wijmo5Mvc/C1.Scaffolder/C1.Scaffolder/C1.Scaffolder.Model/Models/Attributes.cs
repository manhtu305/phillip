﻿using System;
using System.ComponentModel;
using C1.Web.Mvc.Localization;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Specifies whether a property should be ignored when passing parameters to T4 template.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class IgnoreTemplateParameter : Attribute
    {
    }

    /// <summary>
    /// Specifies a description for the property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class C1DescriptionAttribute : DescriptionAttribute
    {
        private readonly string _description;

        /// <summary>
        /// Initializes a new instance of <see cref="C1DescriptionAttribute"/> object
        /// </summary>
        /// <param name="resourceKey">The resource key to get the description.</param>
        public C1DescriptionAttribute(string resourceKey)
        {
            _description = Resources.ResourceManager.GetString(resourceKey);
        }

        /// <summary>
        /// Gets the localized description.
        /// </summary>
        public override string Description
        {
            get
            {
                return _description;
            }
        }
    }
}
