﻿using System;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represent a class as a Point for internal use.
    /// </summary>
    public class C1Point
    {
        private int _x;
        private int _y;

        /// <summary>
        /// Occurs when one of the property value is changed.
        /// </summary>
        public event EventHandler<PropertyChangedEventArgs> PropertyChanged;

        /// <summary>
        /// Gets or sets the x-coordinate of this Point.
        /// </summary>
        public int X
        {
            get
            {
                return _x;
            }
            set
            {
                if (_x == value) return;
                _x = value;
                OnPropertyChanged("X");
            }
        }

        /// <summary>
        /// Gets or sets the y-coordinate of this Point.
        /// </summary>
        public int Y
        {
            get
            {
                return _y;
            }
            set
            {
                if (_y == value) return;
                _y = value;
                OnPropertyChanged("Y");
            }
        }

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
