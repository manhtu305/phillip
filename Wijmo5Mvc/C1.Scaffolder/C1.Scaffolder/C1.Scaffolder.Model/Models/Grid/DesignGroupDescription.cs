﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents a class for types defining grouping conditions.
    /// </summary>
    public class DesignPropertyGroupDescription : PropertyGroupDescription
    {
        /// <summary>
        /// Gets or sets whether property is grouped.
        /// </summary>
        public bool Enabled
        {
            get;
            set;
        }
    }
}
