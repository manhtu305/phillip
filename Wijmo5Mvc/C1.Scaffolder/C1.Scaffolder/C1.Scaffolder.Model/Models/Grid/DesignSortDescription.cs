﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Sort description.
    /// </summary>
    public class DesignSortDescription : SortDescription
    {
        /// <summary>
        /// Gets or sets whether property is sorted.
        /// </summary>
        public bool Enabled
        {
            get;
            set;
        }
    }
}
