﻿using C1.Web.Mvc.Serialization;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc.Sheet
{
    partial class FlexSheet
    {
        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[] { 
                "OnClientAutoSizedColumn",
                "OnClientAutoSizedRow",
                "OnClientAutoSizingColumn",
                "OnClientAutoSizingRow",
                "OnClientBeginningEdit",
                "OnClientCellEditEnded",
                "OnClientCellEditEnding",
                "OnClientCopied",
                "OnClientCopying",
                "OnClientDeletingRow",
                "OnClientDraggedColumn",
                "OnClientDraggedRow",
                "OnClientDraggingColumn",
                "OnClientDraggingRow",
                "OnClientDraggingRowColumn",
                "OnClientBeginDroppingRowColumn",
                "OnClientEndDroppingRowColumn",
                "OnClientFormatItem",
                "OnClientGotFocus",
                "OnClientGroupCollapsedChanged",
                "OnClientGroupCollapsedChanging",
                "OnClientLoaded",
                "OnClientLoadedRows",
                "OnClientLoadingRows",
                "OnClientLostFocus",
                "OnClientPasted",
                "OnClientPasting",
                "OnClientPrepareCellForEdit",
                "OnClientRemoteLoading",
                "OnClientRemoteLoaded",
                "OnClientRemoteSaving",
                "OnClientRemoteSaved",
                "OnClientResizedColumn",
                "OnClientResizedRow",
                "OnClientResizingColumn",
                "OnClientResizingRow",
                "OnClientRowAdded",
                "OnClientRowEditEnded",
                "OnClientRowEditEnding",
                "OnClientScrollPositionChanged",
                "OnClientSelectionChanged",
                "OnClientSelectionChanging",
                "OnClientSelectedSheetChanged",
                "OnClientSheetVisibleChanged",
                "OnClientUnknownFunction",
                "OnClientColumnChanged",
                "OnClientPrepareChangingColumn",
                "OnClientPrepareChangingRow",
                "OnClientRowChanged"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }

        /// <summary>
        /// Gets or sets the path for saving the FlexSheet.
        /// </summary>
        [C1Ignore]
        public string SavePath { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether it's loaded in remote mode.
        /// </summary>
        [C1Ignore]
        public bool UseRemoteLoad { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether it's saved in remote mode.
        /// </summary>
        [C1Ignore]
        public bool UseRemoteSave { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the formula bar is visibled.
        /// </summary>
        [C1Ignore]
        public bool ShowFormulaBar { get; set; }

        [IgnoreTemplateParameter]
        public override FlexGridFilter<object> FilterExtension
        {
            get
            {
                return base.FilterExtension;
            }
        }

        private FormulaBar _formulaBar;
        /// <summary>
        /// Gets the formula bar extension assicated with the sheet.
        /// </summary>
        [C1HtmlHelperBuilderNameAttribute("ShowFormulaBar")]
        [C1TagHelperIsAttribute(false)]
        public FormulaBar FormularBar { get { return _formulaBar ?? (_formulaBar = new FormulaBar(this)); } }

        /// <summary>
        /// Specifies whether the FormularBar property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeFormularBar()
        {
            return ShowFormulaBar;
        }

        /// <summary>
        /// Specifies whether the LoadActionUrl property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeLoadActionUrl()
        {
            return !string.IsNullOrEmpty(FilePath) && UseRemoteLoad;
        }

        /// <summary>
        /// Specifies whether the FilePath property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeFilePath()
        {
            return !string.IsNullOrEmpty(FilePath) && !UseRemoteLoad;
        }

        /// <summary>
        /// Specifies whether the SaveActionUrl property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeSaveActionUrl()
        {
            return UseRemoteSave && (!string.IsNullOrEmpty(SavePath) || !string.IsNullOrEmpty(FilePath));
        }

        internal override void FillDesignColumns(bool forUpdate = false)
        {
            if (forUpdate)
            {
                //Des
                //if ( != null) return;//already filled.
                //DesignColumns = new List<ColumnBase>();
                if (forUpdate)
                {
                    foreach (var column in Columns)
                    {
                        DesignColumns.Add(column);
                    }
                }
            }
            else
            {
                base.FillDesignColumns(forUpdate);
            }
        }
    }
}
