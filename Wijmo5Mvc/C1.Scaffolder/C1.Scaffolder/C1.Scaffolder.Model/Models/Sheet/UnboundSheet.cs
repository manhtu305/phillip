﻿namespace C1.Web.Mvc.Sheet
{
    partial class UnboundSheet
    {
        /// <summary>
        /// Gets a value indicating whether the sheet is in bound mode.
        /// </summary>
        public override bool IsBound { get { return false; } }

        /// <summary>
        /// Creates a new instance of <see cref="UnboundSheet"/> object.
        /// </summary>
        /// <returns>An <see cref="UnboundSheet"/> object.</returns>
        public static UnboundSheet CreateNew()
        {
            return new UnboundSheet();
        }
    }
}
