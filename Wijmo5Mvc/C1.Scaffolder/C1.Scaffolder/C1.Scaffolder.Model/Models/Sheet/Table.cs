﻿using C1.Web.Mvc.Converters;
using C1.Web.Mvc.Serialization;
using System.ComponentModel;
namespace C1.Web.Mvc.Sheet
{
    partial class Table : IBindingHolder
    {
        /// <summary>
        /// Gets and sets the source of Table.
        /// </summary>
        [DisplayName("ModelClass")]
        [C1Description("Table_ModelType")]
        [TypeConverter(typeof(ModelTypeConverter))]
        [C1Ignore]
        public IModelTypeDescription ModelType { get; set; }

        /// <summary>
        /// Gets and sets the source of Table.
        /// </summary>
        [DisplayName("DataContext")]
        [C1Description("Table_DbContextType")]
        [TypeConverter(typeof(DbContextTypeConverter))]
        [C1Ignore]
        public IModelTypeDescription DbContextType { get; set; }
    }
}
