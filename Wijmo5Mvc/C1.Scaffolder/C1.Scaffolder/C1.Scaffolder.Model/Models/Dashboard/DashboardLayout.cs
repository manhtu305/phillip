﻿using C1.Web.Mvc.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace C1.Web.Mvc
{
    partial class DashboardLayout : ISupportUpdater
    {
        #region some properties use for designer
        IList _items = null;
        [C1Ignore]
        public IList LayoutItems
        {
            get
            {
                if (_items == null) InitLayout();
                return _items;
            }
        }

        private LayoutType _layoutType = LayoutType.Auto;

        [C1Ignore]
        public LayoutType LayoutType
        {
            get
            {
                return _layoutType;
            }
            set
            {
                _layoutType = value;
                InitLayout();
            }
        }

        [C1Ignore]
        [Browsable(false)]
        public MVCControl ParsedSetting { get; set; }

        public DashboardLayout(MVCControl settings) : this()
        {
            this.ParsedSetting = settings;
        }

        private void EnsureInitLayout()
        {
            if (_items == null) InitLayout();
        }

        private void InitLayout()
        {
            switch (_layoutType)
            {
                case LayoutType.Auto:
                    Layout = new AutoGridLayout();
                    _items = ((AutoGridLayout)Layout).Items as IList;
                    break;
                case LayoutType.Split:
                    Layout = new SplitLayout();
                    _items = ((SplitLayout)Layout).Items as IList;
                    break;
                case LayoutType.Flow:
                    Layout = new FlowLayout();
                    _items = ((FlowLayout)Layout).Items as IList;
                    break;
                default:
                    Layout = new ManualGridLayout();
                    _items = ((ManualGridLayout)Layout).Items as IList;
                    break;
            }
        }

        [C1Ignore]
        public int CellSize
        {
            get
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        return gridLayout.CellSize;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        return manualGridLayout.CellSize;
                    }
                }
                return -1;
            }
            set
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        gridLayout.CellSize = value;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        manualGridLayout.CellSize = value;
                    }
                }
            }
        }

        [C1Ignore]
        public int CellSpacing
        {
            get
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        return gridLayout.CellSpacing;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        return manualGridLayout.CellSpacing;
                    }
                }
                return -1;
            }
            set
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        gridLayout.CellSpacing = value;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        manualGridLayout.CellSpacing = value;
                    }
                }
            }
        }

        [C1Ignore]
        public int GroupSpacing
        {
            get
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        return gridLayout.GroupSpacing;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        return manualGridLayout.GroupSpacing;
                    }
                }
                return -1;
            }
            set
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        gridLayout.GroupSpacing = value;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        manualGridLayout.GroupSpacing = value;
                    }
                }
            }
        }

        [C1Ignore]
        public int MaxRowsOrColumns
        {
            get
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        return gridLayout.MaxRowsOrColumns;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        return manualGridLayout.MaxRowsOrColumns;
                    }
                }
                return -1;
            }
            set
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        gridLayout.MaxRowsOrColumns = value;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        manualGridLayout.MaxRowsOrColumns = value;
                    }
                }
            }
        }

        [C1Ignore]
        public LayoutOrientation Orientation
        {
            get
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        return gridLayout.Orientation;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        return manualGridLayout.Orientation;
                    }
                }
                else if (_layoutType == LayoutType.Split)
                {
                    SplitLayout sLayout = Layout as SplitLayout;
                    if (sLayout != null)
                    {
                        return sLayout.Orientation;
                    }
                }
                return LayoutOrientation.Horizontal;
            }
            set
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Auto)
                {
                    AutoGridLayout gridLayout = Layout as AutoGridLayout;
                    if (gridLayout != null)
                    {
                        gridLayout.Orientation = value;
                    }
                }
                else if (_layoutType == LayoutType.Manual)
                {
                    ManualGridLayout manualGridLayout = Layout as ManualGridLayout;
                    if (manualGridLayout != null)
                    {
                        manualGridLayout.Orientation = value;
                    }
                }
                else if (_layoutType == LayoutType.Split)
                {
                    SplitLayout sLayout = Layout as SplitLayout;
                    if (sLayout != null)
                    {
                        sLayout.Orientation = value;
                    }
                }
            }
        }

        [C1Ignore]
        public FlowDirection Direction
        {
            get
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Flow)
                {
                    FlowLayout flowLayout = Layout as FlowLayout;
                    if (flowLayout != null)
                    {
                        return flowLayout.Direction;
                    }
                }

                return FlowDirection.LeftToRight;
            }
            set
            {
                EnsureInitLayout();
                if (_layoutType == LayoutType.Flow)
                {
                    FlowLayout flowLayout = Layout as FlowLayout;
                    if (flowLayout != null)
                    {
                        flowLayout.Direction = value;
                    }
                }
            }
        }
        #endregion  designer's properties

        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[] {
                "OnClientFormatTile",
                "OnClientTileActivated",
                "OnClientTileSizeChanged"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }
    }

    /// <summary>
    /// Grid layout type
    /// </summary>
    public enum LayoutType
    {
        Auto,
        Split,
        Flow,
        Manual
    }
}
