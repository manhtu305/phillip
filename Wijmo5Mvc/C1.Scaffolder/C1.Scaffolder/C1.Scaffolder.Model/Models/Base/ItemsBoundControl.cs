﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    partial class ItemsBoundControl<T> : IBindingHolder
    {
        #region SelectedModelTypeChangedEvent

        /// <summary>
        /// Occurs when the <see cref="ModelType"/> property value is changed.
        /// </summary>
        public event EventHandler SelectedModelTypeChanged;

        /// <summary>
        /// Raises the <see cref="SelectedModelTypeChanged"/> event.
        /// </summary>
        /// <param name="args"></param>
        protected virtual void OnSelectedModelTypeChanged(EventArgs args)
        {
            if (SelectedModelTypeChanged != null)
            {
                SelectedModelTypeChanged(this, args);
            }
        }
        #endregion

        #region SelectedDbContextTypeChanged

        /// <summary>
        /// Occurs when the <see cref="DbContextType"/> property value is changed.
        /// </summary>
        public event EventHandler SelectedDbContextTypeChanged;

        /// <summary>
        /// Raises the <see cref="SelectedDbContextTypeChanged"/> event.
        /// </summary>
        /// <param name="args"></param>
        protected virtual void OnSelectedDbContextTypeChanged(EventArgs args)
        {
            if (SelectedDbContextTypeChanged != null)
            {
                SelectedDbContextTypeChanged(this, args);
            }
        }
        #endregion

        private bool _isBound;
        private string _entitySetName;

        /// <summary>
        /// Occurs when the <see cref="IsBound"/> property value is changed.
        /// </summary>
        public event EventHandler IsBoundChanged;

        /// <summary>
        /// Raises the <see cref="IsBoundChanged"/> event.
        /// </summary>
        protected virtual void OnIsBoundChanged()
        {
            if (IsBoundChanged != null)
            {
                IsBoundChanged(this, new EventArgs());
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the control is in bound mode.
        /// </summary>
        [IgnoreTemplateParameter]
        [C1Ignore]
        public bool IsBound
        {
            get
            {
                return _isBound;
            }
            set
            {
                if (_isBound == value)
                {
                    return;
                }

                _isBound = value;
                OnIsBoundChanged();
                OnBindingInfoChanged();
            }
        }

        [IgnoreTemplateParameter]
        [C1Ignore]
        public IModelTypeDescription DbContextType
        {
            get
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                return dbService == null ? null : dbService.DbContextType;
            }
            set
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                if (dbService != null && dbService.DbContextType != value)
                {
                    dbService.DbContextType = value;
                    OnSelectedDbContextTypeChanged(EventArgs.Empty);
                    IsBound = GetIsBound();
                }
            }
        }

        /// <summary>
        /// Gets the selected model type.
        /// </summary>
        [IgnoreTemplateParameter]
        [C1Ignore]
        public virtual IModelTypeDescription ModelType
        {
            get
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                return dbService==null?null:dbService.ModelType;
            }
            set
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                if (dbService != null && dbService.ModelType != value)
                {
                    dbService.ModelType = value;
                    OnSelectedModelTypeChanged(EventArgs.Empty);
                    IsBound = GetIsBound();
                }
            }
        }

        [C1Ignore]
        public virtual IList<PropertyDescription> SelectedModelProperties
        {
            get
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                return (dbService == null || dbService.ModelType == null) ? null : dbService.ModelType.Properties;
            }
        }

        [C1Ignore]
        public virtual IDictionary<string, string> PrimaryKeys
        {
            get
            {
                var dbService = ServiceManager.DefaultDbContextProvider;
                return dbService == null ? null : dbService.PrimaryKeys;
            }
        }

        [C1Ignore]
        public virtual string EntitySetName
        {
            get
            {
                return _entitySetName ?? (_entitySetName = ServiceManager.DbContextProvider.GetEntitySetName(DbContextType, ModelType));
            }
        }

        private bool GetIsBound()
        {
            return DbContextType != null && ModelType != null;
        }
    }
}
