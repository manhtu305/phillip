﻿using C1.Web.Mvc.Serialization;
namespace C1.Web.Mvc
{
    partial class Control
    {
        /// <summary>
        /// Gets or sets the width for design.
        /// </summary>
        [C1Ignore]
        public string DesignWidth
        {
            get { return Width; }
            set { Width = GetOutputSizeValue(value); }
        }

        /// <summary>
        /// Gets or sets the height for design.
        /// </summary>
        [C1Ignore]
        public string DesignHeight
        {
            get { return Height; }
            set { Height = GetOutputSizeValue(value); }
        }

        private static string GetOutputSizeValue(string strValue)
        {
            float value;
            if (!float.TryParse(strValue, out value))
            {
                return strValue;
            }

            const string defaultUnit = "px";
            return value + defaultUnit;
        }
    }
}
