﻿using System.ComponentModel;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class RangeSelector<T>
    {
        /// <summary>
        /// Gets or sets a value indicating whether add handler for OnClientRangeChanged event.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        public bool ListenRangeChangedEvent { get; set; }
    }
}
