﻿using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class BaseCollectionViewService<T>
    {
        /// <summary>
        /// Adds for the serialization of SourceCollection in View.
        /// </summary>
        [C1HtmlHelperBuilderName("Bind")]
        [C1TagHelperName("SourceCollection")]
        [C1HtmlHelperConverter(typeof(SourceCollectionConverter))]
        [C1TagHelperConverter(typeof(SourceCollectionConverter))]
        public string Source { get; set; }

        public bool ShouldSerializeBatchEditActionUrl()
        {
            return BatchEdit;
        }
    }

    partial interface IItemsSource<T>
    {
        string Source { get; set; }
    }

}
