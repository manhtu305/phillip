﻿using System.Collections.Generic;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class ComboBoxBase<T> : IInputValueBindingHolder
    {
        private InputValueBinding _valueBinding;

        /// <summary>
        /// Resets the value while value binding is valid.
        /// </summary>
        public virtual void ResetValue()
        {
            Text = null;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the control is bound with remote data.
        /// </summary>
        [C1Ignore]
        public bool UseRemoteBinding { get; set; }

        /// <summary>
        /// Gets the value binding settings.
        /// </summary>
        [C1Ignore]
        public InputValueBinding ValueBinding
        {
            get { return _valueBinding ?? (_valueBinding = new InputValueBinding()); }
        }

        /// <summary>
        /// Gets or sets the content for xxxxFor() builder.
        /// </summary>
        [C1Ignore]
        public string BuilderFor { get; set; }

        /// <summary>
        /// Gets or sets the content for tag helper "for" attribute.
        /// </summary>
        [C1TagHelperName("For")]
        public string TagHelperFor { get; set; }

        /// <summary>
        /// Invoked before serialize this component.
        /// </summary>
        public override void BeforeSerialize()
        {
            base.BeforeSerialize();

            this.SetupForProperties();

            if (!string.IsNullOrEmpty(EntitySetName) && UseRemoteBinding)
            {
                ItemsSource.ReadActionUrl = Id + "_Read";
            }
        }

        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEvents = base.CreateClientEvents();
            clientEvents.Add(new ClientEvent("OnClientFormatItem"));
            return clientEvents;
        }
    }
}
