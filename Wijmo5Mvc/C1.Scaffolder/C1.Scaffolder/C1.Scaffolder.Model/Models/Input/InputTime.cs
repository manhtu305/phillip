﻿using System;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class InputTime<T>
    {
        /// <summary>
        /// Resets the value while value binding is valid.
        /// </summary>
        public override void ResetValue()
        {
            base.ResetValue();
            Value = null;
        }

        /// <summary>
        /// For designer used. Gets or sets the time part of the value.
        /// </summary>
        [C1Ignore]
        public DateTime TimeValue
        {
            get { return Value.HasValue ? Value.Value : DateTime.Now; }
            set
            {
                var date = Value.HasValue ? Value.Value : DateTime.Now.Date;
                Value = new DateTime(date.Year, date.Month, date.Day, value.Hour, value.Minute, value.Second);
            }
        }
    }
}
