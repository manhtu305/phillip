﻿using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc
{
    partial class DropDown
    {
        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[] {
                "OnClientIsDroppedDownChanging",
                "OnClientIsDroppedDownChanged"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }

        /// <summary>
        /// Resets the value while value binding is valid.
        /// </summary>
        public override void ResetValue()
        {
            base.ResetValue();
            Text = null;
        }
    }

    partial class DropDown<T>
    {
        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[] {
                "OnClientIsDroppedDownChanging",
                "OnClientIsDroppedDownChanged"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }
    }
}
