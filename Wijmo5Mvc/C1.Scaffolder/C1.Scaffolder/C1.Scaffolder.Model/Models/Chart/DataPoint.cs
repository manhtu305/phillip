﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    partial class DataPoint
    {
        private AxisData _designX;
        [DisplayName("X")]
        [C1Description("DataPoint_DesignX")]
        [C1Ignore]
        public AxisData DesignX
        {
            get 
            {
                if (_designX == null)
                {
                    _designX = new AxisData();
                    _designX.ValueChanged += (sender, e) =>
                    {
                        X = _designX.GetValue();
                    };
                }
                return _designX;
            }
            set { _designX = value; }
        }

        private AxisData _designY;
        [DisplayName("Y")]
        [C1Description("DataPoint_DesignY")]
        [C1Ignore]
        public AxisData DesignY
        {
            get
            {
                if (_designY == null)
                {
                    _designY = new AxisData();
                    _designY.ValueChanged += (sender, e) =>
                    {
                        Y = _designY.GetValue();
                    };
                }
                return _designY;
            }
            set { _designY = value; }
        }
    }
}