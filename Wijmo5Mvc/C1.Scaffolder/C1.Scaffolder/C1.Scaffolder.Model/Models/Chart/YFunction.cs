﻿using C1.Web.Mvc.Serialization;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    partial class YFunctionSeries<T>
    {
        [Serialization.C1Ignore]
        public override ChartSeriesType SeriesType
        {
            get { return ChartSeriesType.YFunctionSeries; }
        }

        [C1Ignore]
        [Browsable(false)]
        public object AddYFunctionSeries { get; set; }
    }
}
