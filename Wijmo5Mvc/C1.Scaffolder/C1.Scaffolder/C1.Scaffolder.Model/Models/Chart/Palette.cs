﻿namespace C1.Web.Mvc.Chart
{
    /// <summary>
    /// A series of pre-defined palettes for chart.
    /// </summary>
    public enum Palettes
    {
        Standard,
        Cocoa,
        Coral,
        Dark,
        Highcontrast,
        Light,
        Midnight,
        Minimal,
        Modern,
        Organic,
        Slate
    }
}
