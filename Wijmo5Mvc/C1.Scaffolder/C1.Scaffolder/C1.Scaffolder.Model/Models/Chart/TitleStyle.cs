﻿using C1.Web.Mvc.Serialization;
using System;
using System.ComponentModel;
using C1.Web.Mvc.Converters;

namespace C1.Web.Mvc
{
    [TypeConverter(typeof(TitleStyleConverter))]
    partial class TitleStyle : IC1Property
    {
        #region IC1Property
        /// <summary>
        /// Add this property for TagHelper serialization.
        /// </summary>
        [Browsable(false)]
        public string C1Property { get; set; }
        #endregion IC1Property

        private HalignEnum _designHalign = HalignEnum.Center;
        /// <summary>
        /// Get or set the horizontal alignment of the title.
        /// </summary>
        [C1Description("TitleStyle_Halign")]
        [DisplayName("Halign")]
        [DefaultValue(HalignEnum.Center)]
        [C1Ignore]
        public HalignEnum DesignHalign 
        {
            get
            {
                return _designHalign;
            }
            set
            {
                _designHalign = value;
                Halign = Enum.GetName(typeof(HalignEnum), DesignHalign).ToLower();
            }
        }

        /// <summary>
        /// Specifies whether the Halign property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeHalign()
        {
            return !string.IsNullOrEmpty(Halign) && Halign != Enum.GetName(typeof(HalignEnum), HalignEnum.Center).ToLower();
        }
    }

    /// <summary>
    /// Specifies the horizontal alignment.
    /// </summary>
    public enum HalignEnum
    {
        Left,
        Center,
        Right
    }
}
