﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using C1.Web.Mvc.Chart;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class FlexChartBase<T>
    {
        public override void InitControl()
        {
            base.InitControl();
            HeaderSection = new HeaderSection<T>(this);
            FooterSection = new FooterSection<T>(this);
        }

        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[]
            {
                "ItemFormatter",
                "OnClientSelectionChanged",
                "OnClientGotFocus",
                "OnClientLostFocus",
                "OnClientRendered",
                "OnClientRendering"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }

        /// <summary>
        /// Gets or sets an array of default colors to use for displaying each series.
        /// </summary>
        [DefaultValue(Palettes.Standard)]
        [DisplayName("Palette")]
        [C1HtmlHelperConverter(typeof(PalettesConverter))]
        [C1TagHelperConverter(typeof(PalettesConverter))]
        [C1HtmlHelperBuilderName("Palette")]
        [C1TagHelperName("Palette")]
        public Palettes PredefinedPalette { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the chart is bound with remote data.
        /// </summary>
        [C1Ignore]
        public bool UseRemoteBind { get; set; }

        private List<string> _fieldNames;
        protected override void OnSelectedModelTypeChanged(EventArgs args)
        {
            base.OnSelectedModelTypeChanged(args);
            _fieldNames = new List<string>();
            if (SelectedModelProperties != null)
            {
                SelectedModelProperties.ToList().ForEach(p => _fieldNames.Add(p.Name));
            }
        }

        /// <summary>
        /// Gets the collection of field names.
        /// </summary>
        [C1Ignore]
        public List<string> FieldNames
        {
            get
            {
                return _fieldNames;
            }
        }

        /// <summary>
        /// Gets or sets the selected index for design usage.
        /// </summary>
        [C1Ignore]
        public abstract int DesignSelectionIndex { get; set; }

        /// <summary>
        /// Gets the kind of the chart control.
        /// </summary>
        [C1Ignore]
        public abstract ChartControlType ControlType { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the animation is enabled.
        /// </summary>
        [C1Ignore]
        public bool ShowAnimation { get; set; }

        private ChartAnimation<T> _animation;
        /// <summary>
        /// Gets the animation extension for this chart.
        /// </summary>
        [C1HtmlHelperBuilderName("ShowAnimation")]
        [C1TagHelperIsAttribute(false)]
        public ChartAnimation<T> Animation
        {
            get { return _animation ?? (_animation = new ChartAnimation<T>(this)); }
        }

        /// <summary>
        /// Gets the settings for the header of the chart.
        /// </summary>
        [C1Ignore]
        public Section<T> HeaderSection { get; private set; }

        /// <summary>
        /// Gets the settings for the footer of the chart.
        /// </summary>
        [C1Ignore]
        public Section<T> FooterSection { get; private set; }

        /// <summary>
        /// Adds for the Animation serialization.
        /// </summary>
        public bool ShouldSerializeAnimation()
        {
            return ShowAnimation;
        }

        public override void BeforeSerialize()
        {
            base.BeforeSerialize();
            if (!string.IsNullOrEmpty(EntitySetName) && UseRemoteBind)
            {
                ItemsSource.ReadActionUrl = Id + "_Read";
            }
            
        }
    }
}
