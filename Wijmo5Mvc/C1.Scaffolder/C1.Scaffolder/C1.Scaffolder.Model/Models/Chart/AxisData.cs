﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Represents the AxisData class which contains all of the settings for the axis series data.
    /// </summary>
    [DefaultProperty("DoubleValue")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class AxisData
    {

        #region ** fields
        private double? _doubleValue = null;
        private DateTime? _dateTimeValue = null;
        #endregion ** end of fields.

        #region ** constructors
        /// <summary>
        /// Initializes a new instance of the AxisData class.
        /// </summary>
        public AxisData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the AxisData class.
        /// </summary>
        /// <param name="doubleValue">
        /// A double? value is set to DoubleValue property.
        /// </param>
        public AxisData(double? doubleValue)
        {
            this.DoubleValue = doubleValue;
        }

        /// <summary>
        /// Initializes a new instance of the AxisData class.
        /// </summary>
        /// <param name="dateTimeValue">
        /// A DateTime? value is set to DateTimeValue property.
        /// </param>
        public AxisData(DateTime? dateTimeValue)
        {
            this.DateTimeValue = dateTimeValue;
        }

        #endregion ** end of constructors.

        #region ** properties
        /// <summary>
        /// A double? value specifies the data point of a series.
        /// </summary>
        [C1Description("AxisData_DoubleValue")]
        [DefaultValue(null)]
        [NotifyParentProperty(true)]
        [C1Ignore]
        public double? DoubleValue
        {
            get
            {
                return this._doubleValue;
            }
            set
            {
                this.ResetProperties();
                this._doubleValue = value;
                this.OnValueChanged();
            }
        }

        /// <summary>
        /// A DateTime? value specifies the data point of a series.
        /// </summary>
        [DefaultValue(null)]
        [NotifyParentProperty(true)]
        [C1Description("AxisData_DateTimeValue")]
        [Editor(typeof(System.ComponentModel.Design.DateTimeEditor), typeof(UITypeEditor))]
        [C1Ignore]
        public virtual DateTime? DateTimeValue
        {
            get
            {
                return this._dateTimeValue;
            }
            set
            {
                this.ResetProperties();
                this._dateTimeValue = value;
                this.OnValueChanged();
            }
        }

        [Browsable(false)]
        [C1Ignore]
        public bool IsNull
        {
            get
            {
                return this.DoubleValue == null && this.DateTimeValue == null;
            }
        }
        #endregion ** end of properties.

        [Browsable(false)]
        public event EventHandler ValueChanged;

        #region ** methods
        protected void OnValueChanged()
        {
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }

        internal virtual void ResetProperties()
        {
            this._doubleValue = null;
            this._dateTimeValue = null;
        }
        public override string ToString()
        {
            if (DateTimeValue.HasValue)
            {
                return DateTimeValue.ToString();
            }
            if (DoubleValue.HasValue)
            {
                return DoubleValue.ToString();
            }
            return string.Empty;
        }
        #endregion ** end of methods.

        /// <summary>
        /// Get actual data value.
        /// </summary>
        /// <returns></returns>
        public object GetValue()
        {
            if (DoubleValue.HasValue) return DoubleValue.Value;
            if (DateTimeValue.HasValue) return DateTimeValue.Value;
            
            return null;
        }
    }
}
