﻿using C1.Web.Mvc.Serialization;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    partial class BoxWhisker<T>
    {
        [Serialization.C1Ignore]
        public override ChartSeriesType SeriesType
        {
            get { return ChartSeriesType.BoxWhiskerSeries; }
        }

        [C1Ignore]
        [Browsable(false)]
        public object AddBoxWhisker { get; set; }
    }
}
