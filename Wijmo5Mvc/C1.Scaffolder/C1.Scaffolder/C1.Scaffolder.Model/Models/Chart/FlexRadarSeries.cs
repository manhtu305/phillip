﻿namespace C1.Web.Mvc
{
    partial class FlexRadarSeries<T>
    {
        [Serialization.C1Ignore]
        public override ChartSeriesType SeriesType
        {
            get { return ChartSeriesType.RadarSeries; }
        }
    }
}
