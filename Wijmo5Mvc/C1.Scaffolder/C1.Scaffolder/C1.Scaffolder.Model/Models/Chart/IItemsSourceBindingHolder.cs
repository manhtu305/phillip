﻿namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines the interface to process the ItemsSource property 
    /// of the Axis object and the Series object for Chart.
    /// </summary>
    public interface IItemsSourceBindingHolder<T> : IBindingHolder
    {
        /// <summary>
        /// Gets the source.
        /// </summary>
        IItemsSource<T> ItemsSource { get; }
    }

    internal class ItemsSourceBinding
    {
        public static bool IsItemsSourceDirty(IModelTypeDescription dbContextType, IModelTypeDescription modelType)
        {
            return ServiceManager.IsDesignTime ? (dbContextType != null || modelType != null)
                : !string.IsNullOrEmpty(ServiceManager.DbContextProvider.GetEntitySetName(dbContextType, modelType));
        }
    }
}
