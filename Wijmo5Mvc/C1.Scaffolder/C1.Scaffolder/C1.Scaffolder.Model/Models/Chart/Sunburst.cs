﻿namespace C1.Web.Mvc
{
    partial class Sunburst<T>
    {
        [Serialization.C1Ignore]
        public override ChartControlType ControlType
        {
            get
            {
                return ChartControlType.Sunburst;
            }
        }
    }
}
