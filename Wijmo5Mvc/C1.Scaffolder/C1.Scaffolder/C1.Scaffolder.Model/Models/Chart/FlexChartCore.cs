﻿using C1.Web.Mvc.Services;
namespace C1.Web.Mvc
{
    partial class FlexChartCore<T>
    {
        /// <summary>
        /// Gets a value indicating whether the settings of <see cref="AxisX"/> is dirty.
        /// </summary>
        [Serialization.C1Ignore]
        public bool IsAxisXDirty{get { return ShouldSerializeAxisX(); }}

        /// <summary>
        /// Gets a value indicating whether the settings of <see cref="AxisY"/> is dirty.
        /// </summary>
        [Serialization.C1Ignore]
        public bool IsAxisYDirty{get { return ShouldSerializeAxisY(); }}

        /// <summary>
        /// Gets or sets the name of the property that contains Y values, for design usage.
        /// </summary>
        [Serialization.C1Ignore]
        public virtual string DesignBinding
        {
            get
            {
                return Binding;
            }
            set
            {
                if (value == Binding)
                {
                    return;
                }
                Binding = value;
                OnBindingInfoChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected index for design usage.
        /// </summary>
        [Serialization.C1Ignore]
        public override int DesignSelectionIndex
        {
            get
            {
                return SelectionIndex.HasValue ? SelectionIndex.Value : 0;
            }
            set
            {
                SelectionIndex = (value == 0) ? (int?)null : value;
            }
        }

        /// <summary>
        /// Adds for the Binding serialization.
        /// </summary>
        public bool ShouldSerializeBinding()
        {
            if (ServiceManager.IsDesignTime)
            {
                return !string.IsNullOrEmpty(Binding);
            }

            return !string.IsNullOrEmpty(Binding) && !string.IsNullOrEmpty(EntitySetName);
        }

        /// <summary>
        /// Adds for the BindingX serialization.
        /// </summary>
        public bool ShouldSerializeBindingX()
        {
            if (ServiceManager.IsDesignTime)
            {
                return !string.IsNullOrEmpty(BindingX);
            }

            return !string.IsNullOrEmpty(BindingX) && !string.IsNullOrEmpty(EntitySetName);
        }
    }
}
