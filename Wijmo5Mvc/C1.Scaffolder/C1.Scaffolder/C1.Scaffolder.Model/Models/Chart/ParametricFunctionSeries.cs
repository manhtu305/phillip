﻿using C1.Web.Mvc.Serialization;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    partial class ParametricFunctionSeries<T>
    {
        [Serialization.C1Ignore]
        public override ChartSeriesType SeriesType
        {
            get { return ChartSeriesType.ParametricFunctionSeries; }
        }

        [C1Ignore]
        [Browsable(false)]
        public object AddParametricFunctionSeries { get; set; }
    }
}
