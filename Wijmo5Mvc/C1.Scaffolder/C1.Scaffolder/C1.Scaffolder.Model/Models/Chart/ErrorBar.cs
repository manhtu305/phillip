﻿using C1.Web.Mvc.Serialization;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    partial class ErrorBar<T>
    {
        [Serialization.C1Ignore]
        public override ChartSeriesType SeriesType
        {
            get { return ChartSeriesType.Normal; }
        }

        [C1Ignore]
        [Browsable(false)]
        public object AddErrorBar { get; set; }
    }
}
