﻿using C1.Web.Mvc.Serialization;
using System.ComponentModel;

namespace C1.Web.Mvc
{
    partial class Waterfall<T>
    {
        [Serialization.C1Ignore]
        public override ChartSeriesType SeriesType
        {
            get { return ChartSeriesType.WaterfallSeries; }
        }

        [C1Ignore]
        [Browsable(false)]
        public object AddWaterfall { get; set; }
    }
}
