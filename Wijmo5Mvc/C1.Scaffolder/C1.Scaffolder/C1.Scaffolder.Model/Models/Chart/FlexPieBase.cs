﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    partial class FlexPieBase<T>
    {
        [Serialization.C1Ignore]
        public override ChartControlType ControlType
        {
            get
            {
                return ChartControlType.FlexPie;
            }
        }

        [Serialization.C1Ignore]
        public override int DesignSelectionIndex
        {
            get
            {
                return SelectedIndex;
            }
            set
            {
                SelectedIndex = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the property that contains chart values, for design usage.
        /// </summary>
        [Serialization.C1Ignore]
        public string DesignBinding
        {
            get { return Binding; }
            set
            {
                if (Binding == value) return;
                Binding = value;
                OnBindingInfoChanged();
            }
        }

        /// <summary>
        /// Gets or sets the name of the property that contains the name of the data item, for design usage.
        /// </summary>
        [Serialization.C1Ignore]
        public string DesignBindingName
        {
            get
            {
                return BindingName;
            }
            set
            {
                if (value == BindingName)
                {
                    return;
                }
                BindingName = value;
                OnBindingInfoChanged();
            }
        }
    }
}
