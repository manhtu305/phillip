﻿using System.ComponentModel;
using C1.Web.Mvc.Converters;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    internal interface IChartSeries
    {
    }

    partial class ChartSeriesBase<T> : IItemsSourceBindingHolder<T>, IChartSeries
    {
        [C1Ignore]
        [Browsable(false)]
        public virtual ChartSeriesType SeriesType
        {
            get { return ChartSeriesType.Normal; }
        }

        /// <summary>
        /// Gets or sets the x-axis for the series.
        /// </summary>
        [C1HtmlHelperBuilderName("AxisX")]
        [C1TagHelperIsAttribute(false)]
        [C1Description("ChartSeriesBase_AxisX")]
        [DisplayName("AxisX")]
        public ChartAxis<T> DesignAxisX
        {
            get
            {
                return AxisX;
            }
        }

        /// <summary>
        /// Gets or sets the y-axis for the series.
        /// </summary>
        [C1HtmlHelperBuilderName("AxisY")]
        [C1TagHelperIsAttribute(false)]
        [C1Description("ChartSeriesBase_AxisY")]
        [DisplayName("AxisY")]
        public ChartAxis<T> DesignAxisY
        {
            get
            {
                return AxisY;
            }
        }

        #region Data Binding

        /// <summary>
        /// Gets and sets the source of Series.
        /// </summary>
        [TypeConverter(typeof(ModelTypeConverter))]
        [C1Description("ChartSeriesBase_ModelType")]
        [C1Ignore]
        [DisplayName("ModelClass")]
        public IModelTypeDescription ModelType { get; set; }

        /// <summary>
        /// Gets and sets the source of Series.
        /// </summary>
        [TypeConverter(typeof(DbContextTypeConverter))]
        [C1Description("ChartSeriesBase_DbContextType")]
        [C1Ignore]
        [DisplayName("DataContext")]
        public IModelTypeDescription DbContextType { get; set; }

        #endregion
    }
}
