﻿using System.Collections.Generic;

namespace C1.Web.Mvc
{
    partial class FlexRadar<T>
    {
        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEvents = base.CreateClientEvents();
            clientEvents.Add(new ClientEvent("OnClientSeriesVisibilityChanged"));
            return clientEvents;
        }

        [Serialization.C1Ignore]
        public override int DesignSelectionIndex
        {
            get
            {
                return SelectionIndex.HasValue ? SelectionIndex.Value : 0;
            }
            set
            {
                SelectionIndex = (value == 0) ? (int?)null : value;
            }
        }

        [Serialization.C1Ignore]
        public override ChartControlType ControlType
        {
            get
            {
                return ChartControlType.FlexRadar;
            }
        }

        public override void BeforeSerialize()
        {
            base.BeforeSerialize();

            if (!string.IsNullOrEmpty(EntitySetName)
    && Series.Count == 0)
            {
                Series.Add(new FlexRadarSeries<T>());
            }
        }
    }
}
