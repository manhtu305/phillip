﻿using System.ComponentModel;
using C1.Web.Mvc.Converters;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    partial class ChartAxis<T> : IItemsSourceBindingHolder<T>, IC1Property
    {
        #region Data Binding

        /// <summary>
        /// Gets and sets the source of Axis.
        /// </summary>
        [TypeConverter(typeof(ModelTypeConverter))]
        [C1Description("ChartAxis_ModelType")]
        [C1Ignore]
        [DisplayName("ModelClass")]
        public IModelTypeDescription ModelType { get; set; }

        /// <summary>
        /// Gets and sets the source of Axis.
        /// </summary>
        [TypeConverter(typeof(DbContextTypeConverter))]
        [C1Description("ChartAxis_DbContextType")]
        [C1Ignore]
        [DisplayName("DataContext")]
        public IModelTypeDescription DbContextType { get; set; }
        #endregion

        #region IC1Property
        /// <summary>
        /// Gets or sets the c1-property attribute for taghelper.
        /// </summary>
        [Browsable(false)]
        public string C1Property { get; set; }
        #endregion IC1Property

        private AxisData _designMin;
        [DisplayName("Min")]
        [C1Ignore]
        public AxisData DesignMin
        {
            get
            {
                if (_designMin == null)
                {
                    _designMin = new AxisData();
                    _designMin.ValueChanged += (sender, e) =>
                    {
                        Min = _designMin.GetValue();
                    };
                }
                return _designMin;
            }
            set 
            {
                _designMin = value;
            }
        }

        private AxisData _designMax;
        [DisplayName("Max")]
        [C1Ignore]
        public AxisData DesignMax 
        {
            get
            {
                if (_designMax == null)
                {
                    _designMax = new AxisData();
                    _designMax.ValueChanged += (sender, e) =>
                    {
                        Max = _designMax.GetValue();
                    };
                }
                return _designMax;
            }
            set
            {
                _designMax = value;
            }
        }

        /// <summary>
        /// Create a <see cref="ChartAxis{T}"/> instance.
        /// </summary>
        /// <param name="owner">The chart which owns this axis.</param>
        /// <param name="isHorizontal">A bool value indicates whether it is an x axis or a y axis.</param>
        /// <returns>A <see cref="ChartAxis{T}"/></returns>
        public static ChartAxis<T> CreateAxis(FlexChartCore<T> owner, bool isHorizontal)
        {
            return new ChartAxis<T>(owner, isHorizontal);
        }

        /// <summary>
        /// Specifies whether the Min property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeMin()
        {
            return Min != null;
        }

        /// <summary>
        /// Specifies whether the Max property should be serialized.
        /// </summary>
        /// <returns>
        /// If true, it will be serialized.
        /// Otherwise, it will not be serialized.
        /// </returns>
        public bool ShouldSerializeMax()
        {
            return Max != null;
        }
    }
}
