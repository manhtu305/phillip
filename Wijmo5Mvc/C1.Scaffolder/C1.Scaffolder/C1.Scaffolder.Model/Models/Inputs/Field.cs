﻿using System.ComponentModel;
namespace C1.Web.Mvc
{
    /// <summary>
    /// Defines the Field class for data binding.
    /// </summary>
    public class Field
    {
        private DataMap _dataMap;

        /// <summary>
        /// Gets the DataMap used to convert raw values into display values.
        /// </summary>
        [C1Description("Field_DataMap")]
        public DataMap DataMap
        {
            get { return _dataMap ?? (_dataMap = CreateDataMap()); }
        }

        protected virtual DataMap CreateDataMap()
        {
            return new DataMap();
        }

        /// <summary>
        /// Gets or sets the format string used to convert raw values into display values(see Globalize).
        /// </summary>
        [C1Description("Field_Format")]
        public string Format
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the text displayed in the header.
        /// </summary>
        [C1Description("Field_Header")]
        public string Header
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets whether the field can be edited in the edit view.
        /// </summary>
        [C1Description("Field_IsReadOnly")]
        public virtual bool IsReadOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the property that this is bound to.
        /// </summary>
        [C1Description("Field_Binding")]
        [TypeConverter(typeof(C1.Web.Mvc.Converters.BindingConverter))]
        public string Binding
        {
            get;
            set;
        }

        /// <summary>
        /// Convert to string.
        /// </summary>
        /// <returns>The string.</returns>
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Header))
            {
                return Header;
            }

            if (!string.IsNullOrEmpty(Binding))
            {
                return Binding;
            }

            return GetType().Name;
        }
    }
}
