﻿using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    public partial class TabPanel : ISupportUpdater, IControlValue
    {
        private string _controlValue;

        [C1Ignore]
        [Browsable(false)]
        public MVCControl ParsedSetting { get; set; }
        [C1Ignore]
        [Browsable(false)]
        public string ControlValue
        {
            get { return this._controlValue; }
            set
            {
                if (ParsedSetting == null || (ParsedSetting != null && ParsedSetting.CodeType != BlockCodeType.HTMLNode))
                {
                    if (value == string.Empty)
                        this._controlValue = string.Empty;
                    else if (value.Length >= 2 && value.StartsWith("\"") && value.EndsWith("\""))
                        this._controlValue = value.Substring(1, value.Length-2);
                    else
                        this._controlValue = value;
                }
                else
                    this._controlValue = value;
            }
        }

        /// <summary>
        /// It's look like stupid way, but anyway we must support
        /// </summary>
        public override string Id
        {
            get
            {
                if (ParsedSetting != null && ParsedSetting.CodeType == BlockCodeType.HTMLNode) return this.ControlValue;
                return base.Id;
            }
            set
            {
                this.ControlValue = base.Id = value;
            }
        }

        public TabPanel(MVCControl settings)
        {
            this.Initialize();
            ParsedSetting = settings;
        }

        protected override List<ClientEvent> CreateClientEvents()
        {
            var clientEventNames = new[] {
                "OnClientSelectedIndexChanged"
            };

            return clientEventNames.Select(e => new ClientEvent(e)).ToList();
        }

        public void CorrectIdForAspnetCore()
        {
            if (this.ControlValue == null)
                Id = string.Empty;
            else
                Id = this.ControlValue;
        }
    }
}
