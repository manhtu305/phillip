﻿using System.ComponentModel;
using C1.Web.Mvc.Serialization;

namespace C1.Web.Mvc
{
    partial class LineMarker<T>
    {
        /// <summary>
        /// Gets or sets the content function that allows to customize the text content of the LineMarker.
        /// </summary>
        [Serialization.C1Ignore]
        public bool ShowContent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether add handler for OnClientPositionChanged event.
        /// </summary>
        [Browsable(false)]
        [C1Ignore]
        public bool ListenPositionChangedEvent { get; set; }
    }
}
