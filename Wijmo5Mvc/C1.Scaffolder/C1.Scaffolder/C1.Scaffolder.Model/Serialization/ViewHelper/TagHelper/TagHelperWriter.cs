﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using C1.Web.Mvc.Services;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the taghelper writer.
    /// </summary>
    public sealed class TagHelperWriter : ViewWriter
    {
        #region Fields
        private TagHelperWriter _nestedWriter;
        private Stack<string> _tagNames;
        private Stack<Scope> _scopes;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the nested writer.
        /// It is used for nested tags.
        /// </summary>
        private TagHelperWriter NestedWriter
        {
            get
            {
                if(_nestedWriter == null)
                {
                    _nestedWriter = new TagHelperWriter(Context, new StringWriter(), Scopes);
                    _nestedWriter.Indent = Indent;
                    _nestedWriter.IncreaseIndent();
                }
                return _nestedWriter;
            }
        }

        internal Stack<string> TagNames
        {
            get { return _tagNames; }
        }

        internal override Stack<Scope> Scopes
        {
            get
            {
                return _scopes;
            }
        }

        internal override string TemplateFileSuffix
        {
            get
            {
                return ".Core.cs" + base.TemplateFileSuffix;
            }
        }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Creates an instance of <see cref="TagHelperWriter"/>.
        /// </summary>
        /// <param name="context">The context object.</param>
        /// <param name="writer">The text writer.</param>
        public TagHelperWriter(IContext context, TextWriter writer)
            : base(context, new TagHelperAttributeHelper(), writer)
        {
            _tagNames = new Stack<string>();
            _scopes = base.Scopes;
        }

        /// <summary>
        /// Create an instance of <see cref="TagHelperWriter"/>.
        /// </summary>
        /// <param name="context">The context object.</param>
        /// <param name="writer">The text writer.</param>
        /// <param name="scopes">The scopes.</param>
        internal TagHelperWriter(IContext context, TextWriter writer, Stack<Scope> scopes)
            : this(context, writer)
        {
            _scopes = scopes;
        }
        #endregion Constructors

        #region Methods
        #region Public
        /// <summary>
        /// Returns a string that represents the content.
        /// </summary>
        /// <returns>A string that represents the content.</returns>
        public override string ToString()
        {
            var content = Output.ToString();
            Dispose();
            return content;
        }
        #endregion Public

        #region Control
        internal override void WriteControl(object control)
        {
            base.WriteControl(control);
            WriteMemberValueWithScope(control, null, GetControlConverter(control));
        }
        #endregion Control

        #region Name
        internal override void WriteMemberName(string name, object value, object memberInfo)
        {
            // If nested tag, write nothing.
            var currentMemberScopeType = Utility.GetScopeType(value);
            if ((currentMemberScopeType == ScopeType.Complex
                || currentMemberScopeType == ScopeType.IEnumerable) && !IsAttribute(memberInfo))
            {
                return;
            }

            // Otherwise, write the attribute name.
            StartWriteNewMember();
            var scope = Scopes.Peek();
            if (scope.Type == ScopeType.Complex)
            {
                WriteRawText(" ", false);
            }

            WriteRawText(string.Format("{0}=", name), false);
        }

        internal override string GetFormattedMemberName(string memberName)
        {
            //Todo: PromptChar => prompt-char
            return GetFormattedName(memberName);
        }
        #endregion Name

        #region Value
        internal override void WriteMemberValueWithScope(object value, object memberInfo, BaseConverter converter = null, bool WithoutComplexBuilderName = false)
        {
            // If nested tag, don't write the value in "".
            var isNestedTag = IsTag(value, memberInfo);
            if (!isNestedTag)
            {
                WriteRawText("\"", false);
            }
            
            base.WriteMemberValueWithScope(value, memberInfo, converter);

            if (!isNestedTag)
            {
                WriteRawText("\"", false);
            }
        }

        internal override void WriteMemberValueWithConverter(object value, object memberInfo, BaseConverter converter)
        {
            var isNestedTag = IsTag(value, memberInfo);
            if (isNestedTag)
            {
                WriteMemberValueWithConverter(NestedWriter, value, memberInfo, converter, Context);
            }
            else
            {
                base.WriteMemberValueWithConverter(value, memberInfo, converter);
            }
        }

        private bool IsTag(object value, object memberInfo)
        {
            var currentMemberScopeType = Utility.GetScopeType(value);
            return !IsAttribute(memberInfo) && (currentMemberScopeType == ScopeType.Complex
                || currentMemberScopeType == ScopeType.IEnumerable);
        }
        #endregion Value

        #region Raw Values
        #region Simple
        internal override void WriteSimpleValue(char value)
        {
            WriteRawText(string.Format("'{0}'", value.ToString()), false);
        }

        internal override void WriteSimpleValue(Enum value)
        {
            WriteRawText(value.ToString(), false);
        }

        internal override void WriteSimpleValue(bool value)
        {
            WriteRawText(value ? "true" : "false", false);
        }

        internal override void WriteSimpleValue(DateTime value)
        {
            WriteRawText(string.Format("@DateTime.Parse(\"{0}\")", value.ToString()), false);
        }
        #endregion Simple

        #region Complex
        //  <c1-flex-grid-column binding="ID"></c1-flex-grid-column>
        internal override void WriteComplex(object value, object memberInfo, bool condition)
        {
            var tagName = GetTagName(value, memberInfo);
            //  <c1-flex-grid-column binding="ID"></c1-flex-grid-column>
            //nested element that unsupport by update feature
            MVCControl ctrlSetting = null;
            if (value is ISupportUpdater && (ctrlSetting= ((ISupportUpdater)value).ParsedSetting) != null)
            {
                //TODO: not good code, special case for FlexGrid:
                //itemsSource have setting but Not serialize when update
                // need recheck if have time
                if (ctrlSetting.Properties.ContainsKey("Sources") && value is ItemsBoundControl<object>)
                {
                    ctrlSetting.Properties["Sources"].IsIgnored = !((ItemsBoundControl<object>)value).ShouldSerializeItemsSource();
                }

                foreach (var item in ctrlSetting.Properties)
                {
                    if ( item.Value.Value is MVCControlCollection 
                        && item.Value.IsIgnored)
                    {
                        NestedWriter.WriteText(item.Value.RenderedText);
                        NestedWriter.WriteNewLine();
                    }
                }
            }
            if (memberInfo == null)
            {
                WriteTag(tagName, value, condition);
            }
            else
            {
                // Write as a nested tag.
                NestedWriter.WriteTag(tagName, value, condition);
            }
        }

        internal void WriteTag(string tagName, object value, bool condition)
        {
            TagNames.Push(tagName);
            base.WriteComplex(value, null, condition);
            WriteNewLine();
            TagNames.Pop();
        }

        // <c1-flex-grid-column
        internal override void StartWriteComplex()
        {
            WriteRawText(string.Format("<{0}", TagNames.Peek()), false);
        }

        //      >
        //      [nested content]
        //      </c1-flex-grid-column>
        internal override void EndWriteComplex()
        {
            WriteRawText(">", false);
            WriteNestedContent();
            WriteRawText(string.Format("</{0}>", TagNames.Peek()), false);
        }
        #endregion Complex

        #region Dictionary
        //template-bindings="@(new {ItemsSource="Trends"})"
        internal override void WriteDictionary(IDictionary value, object collectionMemberInfo = null)
        {
            WriteRawText("@(new{", false);
            base.WriteDictionary(value, collectionMemberInfo);
            WriteRawText("})", false);
        }

        // [,]ItemsSource="Trends"
        internal override void WriteDictionaryEntry(DictionaryEntry item, object parent, object parentMemberInfo)
        {
            var scope = Scopes.Peek();
            if(scope.ObjectCount > 0)
            {
                WriteRawText(",", false);
            }
            WriteRawText((string)item.Key + "=", false);
            using(var sw = new StringWriter())
            {
                var csWriter = new CSHtmlHelperWriter(null, sw);
                csWriter.WriteObject(item.Value);
                WriteRawText(sw.ToString(), false);
            }
        }
        #endregion Dictionary

        #region Collection
        //<c1-flex-grid-column binding="ID"></c1-flex-grid-column>
        //<c1-flex-grid-column binding = "Country"></c1-flex-grid-column>
        internal override void WriteCollection(IEnumerable value, object memberInfo)
        {
            if (memberInfo == null)
            {
                base.WriteCollection(value, memberInfo);
            }
            else
            {
                NestedWriter.InnerWriteCollection(value, memberInfo);
            }
        }

        /// <summary>
        /// Write the collection directly, do not create a nested writer.
        /// </summary>
        private void InnerWriteCollection(IEnumerable value, object memberInfo)
        {
            base.WriteCollection(value, memberInfo);
        }

        //<c1-flex-grid-column binding = "Country"></c1-flex-grid-column>
        internal override void WriteRawCollectionItem(object item, object collection, object collectionMemberInfo)
        {
            WriteMemberValueWithScope(item, null);
        }
        #endregion Collection

        #region Color
        internal override string GetColorText(Color value)
        {
            return Utility.ColorToHtml(value);
        }
        #endregion Color
        #endregion Raw Values

        #region IDisposable
        /// <summary>
        /// Releases all resources used.
        /// </summary>
        public override void Dispose()
        {
            if(_nestedWriter != null)
            {
                _nestedWriter.Dispose();
                _nestedWriter = null;
            }

            Output.Dispose();
        }
        #endregion IDisposable

        #region Helpers
        /// <summary>
        /// Writes a new line.
        /// </summary>
        /// <param name="connected">A boolean value indicates whether to write a connected new line.</param>
        public override void WriteNewLine(bool connected = false)
        {
            Output.WriteLine();
            base.WriteNewLine(connected);
        }

        private static bool IsAttribute(object memberInfo)
        {
            if(memberInfo == null)
            {
                return false;
            }

            var attribute = Utility.GetAttribute<C1TagHelperIsAttributeAttribute>(memberInfo) as C1TagHelperIsAttributeAttribute;
            return attribute == null || attribute.IsAttribute;
        }

        // [MemberInfo's C1TagHelperNameAttribute] > [Type's C1TagHelperNameAttribute]
        private static string GetTagName(object value, object memberInfo)
        {
            C1TagHelperNameAttribute attr = null;
            if (memberInfo != null)
            {
                attr = Utility.GetAttribute<C1TagHelperNameAttribute>(memberInfo) as C1TagHelperNameAttribute;
            }

            var type = value != null ? value.GetType() : Utility.GetType(memberInfo);
            if (attr == null)
            {
                attr = Utility.GetAttribute<C1TagHelperNameAttribute>(type) as C1TagHelperNameAttribute;
            }

            if(attr == null)
            {
                return GetDefaultTagName(GetFormattedName(ViewUtility.GetNameWithoutGeneric(type.Name)));
            }
            else
            {
                return attr.Name;
            }
        }

        private static string GetFormattedName(string name)
        {
            if (string.IsNullOrEmpty(name) || name.Length == 0)
            {
                throw new ArgumentNullException("name");
            }

            var parts = new List<string>();
            var part = new List<char>();
            foreach(var c in name)
            {
                if (char.IsLower(c) || !char.IsLetter(c))
                {
                    part.Add(c);
                }
                else
                {
                    if(part.Count > 0)
                    {
                        parts.Add(new string(part.ToArray()));
                        part.Clear();
                    }
                    part.Add(char.ToLower(c));
                }
            }

            if(part.Count > 0)
            {
                parts.Add(new string(part.ToArray()));
            }

            return string.Join("-", parts);
        }

        private static string GetDefaultTagName(string name)
        {
            return "c1-" + name;
        }

        private void WriteNestedContent()
        {
            if(_nestedWriter != null)
            {
                var nestedText = _nestedWriter.ToString();
                _nestedWriter = null;
                if (nestedText.Length != 0)
                {
                    WriteNewLine();
                    Output.Write(nestedText);
                }
            }
            
        }

        internal override string GetCodeText(string value)
        {
            if (value.Contains("@"))
                return value;
            return value.Replace("\"", "@(\"\\\"\")");
        }
        #endregion Helpers
        #endregion Methods
    }
}
