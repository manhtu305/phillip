﻿using System;

namespace C1.Web.Mvc.Serialization
{
    #region Attributes
    /// <summary>
    /// Defines the attribute to set the name for some property.
    /// If this property is an attribute, you can customize the attribute name by this attribute.
    /// Otherwise, it will use the property name as the attribute name.
    /// 
    /// If a tag will represent the property, you can specify the tag name for this property. 
    /// Otherwise, it will use the type's tag name as its tag name.
    /// 
    /// For example, as we know, a tag will be shown for the ItemsSource property:
    ///     &lt;c1-items-source initial-items-count="100" source-collection="Model"&gt;&lt;/c1-items-source&gt;
    /// 
    /// In order to write the property into a tag, we need add the following above the ItemsSource property.
    ///     [C1TagHelperName("c1-items-source")]
    ///     public virtual IItemsSource&lt;T&gt; ItemsSource
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class, AllowMultiple = false)]
    public class C1TagHelperNameAttribute : C1NameAttribute
    {
        public C1TagHelperNameAttribute(string name)
            : base(name)
        {
        }
    }

    /// <summary>
    /// Defines the attribute to determine whether a property is an attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class C1TagHelperIsAttributeAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of <see cref="C1TagHelperIsAttributeAttribute"/> object.
        /// </summary>
        /// <param name="isAttribute">Whether the property is an attribute.</param>
        public C1TagHelperIsAttributeAttribute(bool isAttribute)
        {
            IsAttribute = isAttribute;
        }

        /// <summary>
        /// Gets a value indicating whether the property is an attribute.
        /// </summary>
        public bool IsAttribute { get; private set; }
    }

    /// <summary>
    /// Defines the attribute to set the converter which is used for writer and reader.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface | AttributeTargets.Enum | AttributeTargets.Parameter)]
    public sealed class C1TagHelperConverterAttribute : C1ConverterAttribute
    {
        /// <summary>
        /// Create an instance of <see cref="C1TagHelperConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        public C1TagHelperConverterAttribute(Type converterType)
            : base(converterType)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="C1ConverterAttribute"/> class.
        /// </summary>
        /// <param name="converterType">Type of the converter.</param>
        /// <param name="converterParameters">Parameter list to use when constructing the JsonConverter. Can be null.</param>
        public C1TagHelperConverterAttribute(Type converterType, params object[] converterParameters)
            :base(converterType, converterParameters)
        {

        }
    }

    /// <summary>
    /// Defines the attribute to set the collection item converter.
    /// </summary>
    public sealed class C1TagHelperCollectionItemConverterAttribute : C1CollectionItemConverterAttribute
    {
        /// <summary>
        /// Creates an instance of <see cref="C1TagHelperCollectionItemConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        public C1TagHelperCollectionItemConverterAttribute(Type converterType)
            : base(converterType)
        {
        }

        /// <summary>
        /// Creates an instance of <see cref="C1TagHelperCollectionItemConverterAttribute"/>.
        /// </summary>
        /// <param name="converterType">The converter type.</param>
        /// <param name="converterParameters">The parameters used to create the converter.</param>
        public C1TagHelperCollectionItemConverterAttribute(Type converterType, params object[] converterParameters)
            : base(converterType, converterParameters)
        {
        }
    }
    #endregion Attributes

    #region Helper
    internal sealed class TagHelperAttributeHelper : AttributeHelper
    {
        public override C1CollectionItemConverterAttribute GetCollectionItemConverterAttribute(object memberInfo, object collection)
        {
            return Utility.GetAttribute<C1TagHelperCollectionItemConverterAttribute>(memberInfo, collection);
        }

        public override C1ConverterAttribute GetConverterAttribute(object memberInfo, object value)
        {
            return Utility.GetAttribute<C1TagHelperConverterAttribute>(memberInfo, value);
        }

        public override C1NameAttribute GetNameAttribute(object memberInfo, object value)
        {
            return Utility.GetAttribute<C1TagHelperNameAttribute>(memberInfo, value);
        }
    }
    #endregion Helper
}
