﻿namespace C1.Web.Mvc.Serialization
{
    internal sealed class NullableEnumConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            writer.WriteText(value.GetType().FullName + "." + value.ToString());
        }
    }
}
