﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Serialization
{
    internal sealed class StringArrayTagHelperConverter : StringArrayConverter
    {
        private readonly string _name;

        public StringArrayTagHelperConverter(string name)
        {
            _name = name;
        }

        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var name = string.IsNullOrEmpty(_name) ? "temp" : _name;
            var arrCodeSnippets = GetStringArrayCodeSnippets((string[])value);
            writer.View.AddCode(string.Format("var {0} = new string[]{{{1}}};", name, arrCodeSnippets));
            writer.WriteText(string.Format("@{0}", name));
        }
    }
}
