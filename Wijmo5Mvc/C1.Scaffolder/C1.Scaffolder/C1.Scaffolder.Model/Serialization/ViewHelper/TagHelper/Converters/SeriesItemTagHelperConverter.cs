﻿namespace C1.Web.Mvc.Serialization
{
    internal sealed class SeriesItemTagHelperConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            ChartIItemsSourceBindingHolderConverter.ConvertBindingHolder(writer, value, context);

            var tw = writer as TagHelperWriter;
            tw.WriteRawCollectionItem(value, null, null);
        }
    }
}
