﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Serialization
{
    internal sealed class StringArrayWithSeparatorTagHelperConverter : StringArrayConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var strArray = value as string[];
            if(strArray == null)
            {
                return;
            }

            StringBuilder builder = new StringBuilder();
            for (int idx = 0; idx < strArray.Length; idx++)
            {
                builder.Append(strArray[idx].Trim());
                if (idx != strArray.Length - 1)
                {
                    builder.Append(',');
                }
            }

            writer.WriteText(builder.ToString());
        }
    }
}
