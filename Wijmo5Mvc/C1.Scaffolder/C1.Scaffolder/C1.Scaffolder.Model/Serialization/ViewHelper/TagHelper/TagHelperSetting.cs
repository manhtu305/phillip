﻿namespace C1.Web.Mvc.Serialization
{
    internal class TagHelperSetting : ViewSetting
    {
        public TagHelperSetting()
        {
            Resolvers.Add(new ViewClientEventResolver());
            Resolvers.Add(new AxisResolver());
            Resolvers.Add(new TitleStyleResolver());
            Resolvers.Add(new PivotFieldCollectionResolver());
        }
    }
}
