﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Serialization
{
    internal class PointHtmlHelperConverter : BaseConverter
    {
        protected internal override bool CanConvert(Type objectType, IContext context)
        {
            return objectType == typeof(Point);
        }

        protected override void Write(BaseWriter writer, object value, IContext context)
        {
            if (value is Point)
            {
                var pf = (Point)value;
                writer.WriteText(string.Format("{0}, {1}", pf.X, pf.Y));
                return;
            }

            throw new NotSupportedException();
        }
    }
}
