﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace C1.Web.Mvc.Serialization
{
    class SplitGroupConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            (writer as HtmlHelperWriter).WriteCollectionItem(value,
                (value is SplitGroup) ? "Group" : "Tile"
                );
        }
    }

    class SplitTileConverter : ViewConverter
    {
        internal void GetBuilderName(string outText, out string builderName, out string spaces)
        {
            #region findout spaces
            {
                int i = outText.LastIndexOf(".AddGroup");
                int j = outText.LastIndexOf(".Children");
                if (i == -1 || j == -1)
                {
                    builderName = spaces = null;
                    return;
                }
                spaces = new string(' ', j - i - 13); // 13 unit for .AddGroup()\r\n
            }
            #endregion

            #region findout builder name
            {
                string[] handleNumber = null;
                string[] arrSplit = Regex.Split(outText, @"[^a-zA-Z0-9-]+");
                int i = Array.FindLastIndex(arrSplit, x => x.Equals("AddGroup"));
                builderName = arrSplit[i - 1];
                arrSplit = null;
                handleNumber = Regex.Split(builderName, @"[a-zA-Z]+");
                handleNumber = handleNumber.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                if (handleNumber.Count() == 0)
                {
                    builderName += "1";
                }
                else
                {
                    builderName = Regex.Split(builderName, @"\D+").Where(x => !string.IsNullOrEmpty(x)).ToArray().ElementAt(0);
                    builderName += (int.Parse(handleNumber[0]) + 1);
                }
            }
            #endregion
        }

        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var list = value as List<ISplitLayoutItem>;

            string builderName = null, spaces = null;
            GetBuilderName(writer.Output.ToString(), out builderName, out spaces);

            // it's only happen when logic sealize before this Tile is wrong.
            if (builderName == null || string.IsNullOrEmpty(builderName))
                return;

            writer.OnBeginningWrite();

            if (writer is VBHtmlHelperWriter)
            {
                writer.WriteRawText(string.Format("\r\n{0}Sub({1})\r\n", spaces, builderName), false);
                foreach (var x in list)
                {
                    writer.WriteRawText(string.Format("{0}\t{1}.AddTile()", spaces, builderName), false);
                    // write properties
                    writer.WriteComplex(x, null, false);
                    // end of line
                    writer.WriteRawText("\r\n", false);
                }
                writer.WriteRawText(string.Format("{0}End Sub", spaces), false);
            }
            else
            {
                writer.WriteRawText(string.Format("{0} =>\r\n{1}{{\r\n", builderName, spaces), false);
                foreach (var x in list)
                {
                    writer.WriteRawText(string.Format("{0}\t{1}.AddTile()", spaces, builderName), false);
                    // write properties
                    writer.WriteComplex(x, null, false);
                    // end of line
                    writer.WriteRawText(";\r\n", false);
                }
                writer.WriteRawText(string.Format("{0}}}", spaces), false);
            }

            writer.OnWriteEnded();
            builderName = spaces = null;
        }
    }
}
