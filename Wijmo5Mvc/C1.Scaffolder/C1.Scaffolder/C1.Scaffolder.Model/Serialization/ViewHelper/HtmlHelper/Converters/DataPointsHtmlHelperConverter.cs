﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Serialization
{
    internal sealed class DataPointsHtmlHelperConverter : ViewConverter
    {
        protected internal override bool CanConvertWhole
        {
            get
            {
                return true;
            }
        }

        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var points = value as List<DataPoint>;
            if(points != null)
            {
                foreach(var point in points)
                {
                    writer.WriteText(".AddPoint(");
                    writer.WriteMemberValue(point, new DataPointConverter());
                    writer.WriteText(")");
                }
            }
            
        }
    }
}
