﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc.Serialization
{
    internal class ChartIItemsSourceBindingHolderItemsSourceHtmlHelperConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var itemsSource = value as IItemsSource<object>;
            writer.WriteRawText(itemsSource.Source, false);
        }
    }
}
