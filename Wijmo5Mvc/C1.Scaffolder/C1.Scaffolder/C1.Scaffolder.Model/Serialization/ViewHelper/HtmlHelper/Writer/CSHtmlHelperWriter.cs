﻿using System.CodeDom.Compiler;
using System.IO;
using C1.Web.Mvc.Services;
using System.Collections.Generic;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Serializes an object to an htmlhelper in C# language.
    /// </summary>
    public sealed class CSHtmlHelperWriter : HtmlHelperWriter
    {
        #region Properties
        internal override string ItemSeperator
        {
            get
            {
                return ";";
            }
        }

        internal override string TemplateFileSuffix
        {
            get
            {
                return ".cs" + base.TemplateFileSuffix;
            }
        }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Creates an instance of <see cref="CSHtmlHelperWriter"/>
        /// </summary>
        /// <param name="context">The context object.</param>
        /// <param name="writer">The writer.</param>
        public CSHtmlHelperWriter(IContext context, TextWriter writer)
            : base(context, writer)
        {
        }

        #endregion Constructors

        #region Methods
        #region Control
        internal override string GetGenericType()
        {
            var currentDbContext = Context.ServiceProvider.GetService(typeof(IDefaultDbContextProvider)) as IDefaultDbContextProvider;
            return currentDbContext.ModelType == null
                ? string.Empty
                : string.Format("<{0}>", currentDbContext.ModelType.TypeName);
        }
        #endregion Control

        #region Raw Values
        #region Simple
        internal override void WriteSimpleValue(char value)
        {
            WriteRawText(string.Format("'{0}'", value.ToString()), false);
        }

        internal override void WriteSimpleValue(bool value)
        {
            WriteRawText(value ? "true" : "false", false);
        }
        #endregion Simple

        #region Complex
        internal override void WriteComplexBuilderName(string name)
        {
            WriteRawText(string.Format("{0}=>{0}", name), false);
        }


        internal override void WriteComplex(object value, object memberInfo, bool withoutComplexPrefix = false)
        {
            base.WriteComplex(value, memberInfo, withoutComplexPrefix);
        }

                
        #endregion Complex

        #region Collection
        internal override void WriteCollectionPrefix()
        {
            var collectionBuilderName = CollectionBuilderNames.Peek();
            WriteRawText(string.Format("{0}=>", collectionBuilderName), false);
            WriteViewLine();
            WriteRawText("{", false);
            WriteViewLine();
        }

        internal override void WriteCollectionSuffix()
        {
            WriteRawText("}", false);
        }
        #endregion Collection
        #endregion Raw Values

        #region Helpers
        /// <summary>
        /// Writes a new line.
        /// </summary>
        /// <param name="connected">A boolean value indicates whether to write a connected new line.</param>
        public override void WriteNewLine(bool connected = false)
        {
            Output.WriteLine();
            base.WriteNewLine(connected);
        }

        internal override CodeDomProvider GetCodeDomProvider()
        {
            return new Microsoft.CSharp.CSharpCodeProvider();
        }
        #endregion Helpers
        #endregion Methods
    }
}
