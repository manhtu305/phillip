﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using C1.Web.Mvc.Services;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the base class for htmlhelper writer: CS and VB.
    /// </summary>
    public abstract class HtmlHelperWriter : ViewWriter
    {
        #region Fields
        private const string DotConnector = ".";
        private Stack<string> _collectionBuilderNames;
        #endregion Fields

        #region Properties
        internal abstract string ItemSeperator { get; }

        internal Stack<string> CollectionBuilderNames
        {
            get { return _collectionBuilderNames; }
        }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Creates an instance of <see cref="HtmlHelperWriter"/>
        /// </summary>
        /// <param name="context">The context object.</param>
        /// <param name="writer">The text writer.</param>
        protected HtmlHelperWriter(IContext context, TextWriter writer)
            : base(context, new HtmlHelperAttributeHelper(), writer)
        {
            _collectionBuilderNames = new Stack<string>();
        }
        #endregion Constructors

        #region Methods
        #region Public
        public string GetUniqueBuilderName(string name)
        {
            CheckScopes();
            var scope = Scopes.Peek();
            var parent = scope.Parent;
            var result = name;
            var usedNames = new List<string>();

            while (parent != null)
            {
                usedNames.AddRange(parent.UsedNames);
                parent = parent.Parent;
            }

            var viewSnippets = GetViewSnippets();
            if(viewSnippets != null)
            {
                result = viewSnippets.GetBuilderVariableName(usedNames, name);
            }
            else
            {
                result = GetUniqueBuilderName(name, 0, usedNames);
            }

            scope.UsedNames.Add(result);
            return result;
        }

        private string GetUniqueBuilderName(string name, int index, IList<string> usedNames)
        {
            var bName = index == 0 ? name : name + index.ToString();
            if (usedNames.Contains(bName))
            {
                return GetUniqueBuilderName(name, ++index, usedNames);
            }

            return bName;
        }
        #endregion Public

        #region Control
        internal override void WriteControl(object control)
        {
            base.WriteControl(control);
            WriteRawText(string.Format("Html.C1().{0}", GetControlBuilder(control)), false);
            WriteViewLine(true);
            WriteMemberValueWithScope(control, null, GetControlConverter(control), true);
        }

        private string GetControlBuilder(object control)
        {
            var type = control.GetType();
            var name = GetName(null, type.Name, control);
            var controlName = type.IsGenericType ? ViewUtility.GetNameWithoutGeneric(name) : name;
            var controlBuilderValueText = "";
            // Add for InputFor
            var inputForControl = control as IInputValueBindingHolder;
            if(inputForControl != null && !string.IsNullOrEmpty(inputForControl.BuilderFor))
            {
                controlName = controlName + "For";
                controlBuilderValueText = inputForControl.BuilderFor;
            }
            else if (type.IsGenericType)
            {
                var scaffolder = Context.ServiceProvider.GetService(typeof(IScaffolderDescriptor)) as IScaffolderDescriptor;
                if (scaffolder.ShouldSerializeGenericParameter)
                {
                    controlName = controlName + GetGenericType();
                }
            }

            if (control is IControlValue)
            {
                if ((control as IControlValue).ControlValue != null)
                {
                    var value = (control as IControlValue).ControlValue;
                    if(value.Length >= 2 && value.StartsWith("\"") && value.EndsWith("\""))
                        controlBuilderValueText = value;
                    else
                        controlBuilderValueText = "\"" + value + "\"";
                }
            }

            return string.Format("{0}({1})", controlName, controlBuilderValueText);
        }

        internal abstract string GetGenericType();
        #endregion Control

        #region Member
        internal override void WriteMemberInfoWithResolver(object memberInfo, object parent)
        {
            CheckScopes();
            var scope = Scopes.Peek();
            if (scope.Type == ScopeType.Complex 
                && scope.ObjectCount == 0 && !scope.WithoutComplexBuilderName)
            {
                WriteComplexPrefix(parent);
            }

            base.WriteMemberInfoWithResolver(memberInfo, parent);
        }
        #endregion Member

        #region Name
        internal override void WriteMemberName(string name, object value, object memberInfo)
        {
            base.WriteMemberName(name, value, memberInfo);
            var hasCreatedNewLine = IsNewLine;
            if (!hasCreatedNewLine && Scopes.Count != 0)
            {
                var scope = Scopes.Peek();
                if (scope.NeedNewLine)
                {
                    WriteViewLine(true);
                    hasCreatedNewLine = true;
                    scope.NeedNewLine = false;
                }
            }
            

            var currentMemberScopeType = Utility.GetScopeType(value);
            switch (currentMemberScopeType)
            {
                // If the current member is IDictionary, don't write the member name.
                case ScopeType.IDictionary:
                    return;
                // If the current member is collection or complex object, write them in a new line.
                case ScopeType.IEnumerable:
                case ScopeType.Complex:
                    if (!hasCreatedNewLine)
                    {
                        WriteViewLine(true);
                    }
                    break;
            }

            WriteRawText(DotConnector, false);
            WriteRawText(name, false);
        }
        #endregion Name

        #region Value
        internal override void WriteMemberValueWithScope(object value, object memberInfo, BaseConverter converter = null, bool withoutComplexBuilderName = false)
        {
            var currentMemberScopeType = Utility.GetScopeType(value);
            var hasParenthesis = currentMemberScopeType != ScopeType.IDictionary && !withoutComplexBuilderName;
            if(hasParenthesis)
            {
                WriteRawText("(", false);
            }

            base.WriteMemberValueWithScope(value, memberInfo, converter, withoutComplexBuilderName);

            if(hasParenthesis)
            {
                WriteRawText(")", false);
                if(currentMemberScopeType == ScopeType.IEnumerable 
                    || currentMemberScopeType == ScopeType.Complex)
                {
                    if(Scopes.Count > 0)
                    {
                        var scope = Scopes.Peek();
                        scope.NeedNewLine = true;
                    }
                }
            }
        }
        #endregion Value

        #region Raw Values
        #region Simple
        internal override void WriteSimpleValue(Enum value)
        {
            WriteRawText(value.GetType().FullName + DotConnector + value.ToString(), false);
        }

        internal override void WriteSimpleValue(DateTime value)
        {
            WriteRawText(string.Format("DateTime.Parse(\"{0}\")", value.ToString()), false);
        }
        #endregion Simple

        #region Complex
        internal abstract void WriteComplexBuilderName(string name);

        internal virtual void WriteComplexPrefix(object value)
        {
            var name = GetDefaultMemberName(value.GetType());
            name = GetUniqueBuilderName(name);
            WriteComplexBuilderName(name);
        }

        private string GetDefaultMemberName(Type memberType)
        {
            return GetUpperLetter(memberType.Name).ToLower() + "b";
        }
        #endregion Complex

        #region Dictionary
        // HtmlAttributes =>
        //  .HtmlAttribute(key, value)
        internal override void WriteDictionaryEntry(DictionaryEntry item, object parent, object parentMemberInfo)
        {
            var name = GetDictionaryEntryName(parentMemberInfo, parent);
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException();
            }

            WriteRawText(DotConnector + name + "(", false);
            WriteRawText((string)item.Key);
            WriteRawText(",", false);
            WriteMemberValue(item.Value); // the type of item.Value is string
            WriteRawText(")", false);
        }

        private string GetDictionaryEntryName(object memberInfo, object value)
        {
            var na = Utility.GetAttribute<C1HtmlHelperDictionaryEntryNameAttribute>(memberInfo, value);
            return na != null ? na.Name : null;
        }
        #endregion Dictionary

        #region Collection
        internal override void WriteCollectionItem(object item, object collection, object collectionMemberInfo)
        {
            if (Scopes.Peek().ObjectCount == 0)
            {
                StartWriteFirstCollectionItem(collection);
            }

            StartWriteNewMember();

            base.WriteCollectionItem(item, collection, collectionMemberInfo);
        }

        internal override void EndWriteCollection()
        {
            if (Scopes.Peek().ObjectCount > 0)
            {
                FinishWriteLastCollectionItem();
            }
        }

        internal override void WriteRawCollectionItem(object item, object collection, object collectionMemberInfo)
        {
            var itemRealType = item.GetType();
            var itemType = Utility.GetType(collectionMemberInfo).GetGenericArguments().Single();
            var itemName = itemRealType != itemType ? ViewUtility.GetNameWithoutGeneric(itemRealType.Name) : string.Empty;
            WriteCollectionItem(item, itemName);
        }

        internal void WriteCollectionItem(object item, object itemName)
        {
            var builderName = CollectionBuilderNames.Peek();
            WriteRawText(string.Format("{0}.Add{1}()", builderName, itemName), false);
            WriteMemberValueWithScope(item, null, null, true);
            WriteRawText(ItemSeperator, false);
            WriteNewLine();
        }

        internal virtual void StartWriteFirstCollectionItem(object collection)
        {
            var name = GetDefaultCollectionBuilderName(collection.GetType());
            name = GetUniqueBuilderName(name);
            CollectionBuilderNames.Push(name);
            WriteCollectionPrefix();
            IncreaseIndent();
        }

        internal virtual void FinishWriteLastCollectionItem()
        {
            WriteCollectionSuffix();
            CollectionBuilderNames.Pop();
            DecreaseIndent();
        }

        internal abstract void WriteCollectionPrefix();

        internal abstract void WriteCollectionSuffix();

        private string GetDefaultCollectionBuilderName(Type collectionType)
        {
            var name = collectionType.Name;
            if (collectionType.IsGenericType)
            {
                name = collectionType.GetGenericArguments().Single().Name;
            }

            name = GetUpperLetter(name);

            return name.ToLower() + "sb";
        }
        #endregion Collection

        #region Color
        internal override string GetColorText(Color value)
        {
            if (value.IsNamedColor)
            {
                return string.Format("System.Drawing.Color.{0}", value.Name);
            }
            return string.Format("System.Drawing.Color.FromArgb({0},{1},{2},{3})", value.A, value.R, value.G, value.B);
        }
        #endregion Color
        #endregion Raw Values

        #region Helpers
        private static string GetUpperLetter(string name)
        {
            var temp = new List<char>();
            foreach (var c in name)
            {
                if (char.IsUpper(c))
                {
                    temp.Add(c);
                }
            }
            return new string(temp.ToArray());
        }

        internal override string GetCodeText(string value)
        {
            using (var provider = GetCodeDomProvider())
            {
                var sb = new System.Text.StringBuilder();
                provider.GenerateCodeFromExpression(new CodePrimitiveExpression(value),
                    new StringWriter(sb), new CodeGeneratorOptions());
                return sb.ToString();
            }
        }

        internal abstract CodeDomProvider GetCodeDomProvider();
        #endregion Helpers

        #region Scopes
        internal override void StartScope(object obj, ScopeType? scopeType = default(ScopeType?), bool condition = false)
        {
            base.StartScope(obj, scopeType, condition);
            var scope = Scopes.Peek();
            if (scope.Type == ScopeType.Complex)
            {
                IncreaseIndent();
            }
        }

        internal override void EndScope()
        {
            var scope = Scopes.Peek();
            if (scope.Type == ScopeType.Complex)
            {
                DecreaseIndent();
            }
            base.EndScope();
        }
        #endregion Scopes
        #endregion Methods
    }
}
