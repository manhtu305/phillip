﻿using C1.Web.Mvc.Olap;
using C1.Web.Mvc.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace C1.Web.Mvc.Serialization
{
    #region Public
    /// <summary>
    /// Defines the base converter for view: CSHtmlHelper, VBHtmlHelper and TagHelper.
    /// </summary>
    public abstract class ViewConverter : BaseConverter
    {
        /// <summary>
        /// Writes the specified value into the view.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="context">The context.</param>
        protected abstract void WriteView(ViewWriter writer, object value, IContext context);

        /// <summary>
        /// Gets a value indicating whether this <see cref="ViewWriter" /> works for the whole member.
        /// </summary>
        /// <value>If <c>true</c>, it converts the whole member: name and value. Otherwise, it only converts the value.</value>
        internal protected virtual bool CanConvertWhole
        {
            get { return false; }
        }

        protected string ResolveHandler(string url, string prefix, bool ignoreCase = false)
        {
            if (string.IsNullOrEmpty(prefix)) return url;

            if (url.StartsWith(prefix, ignoreCase, System.Globalization.CultureInfo.InvariantCulture))
            {
                return url.Substring(prefix.Length);
            }

            return url;
        }

        protected string ResolveUrl(string url, string prefix, bool ignoreCase = true)
        {
            var handler = url;
            if (url.StartsWith(prefix, ignoreCase, System.Globalization.CultureInfo.InvariantCulture))
            {
                handler = url.Substring(prefix.Length);
            }

            return prefix + ResolvePascalName(handler);
        }

        private static string ResolvePascalName(string name)
        {
            if (string.IsNullOrEmpty(name)) return name;
            return Char.ToUpper(name[0]) + name.Substring(1);
        }

        #region BaseConverter
        /// <summary>
        /// Writes the specified value.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="context">The context.</param>
        protected override void Write(BaseWriter writer, object value, IContext context)
        {
            var vw = writer as ViewWriter;
            if (vw == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            WriteView(vw, value, context);
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="context">The context information.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        protected internal override bool CanConvert(Type objectType, IContext context)
        {
            return true;
        }
        #endregion BaseConverter
    }
    #endregion Public

    #region Common
    internal class ViewPointConverter : PointConverter
    {
        protected override void Write(BaseWriter writer, object value, IContext context)
        {
            var proj = ViewUtility.GetProjectInfo(context);
            var newKey = proj.IsAspNetCore || proj.IsCs ? "new" : "New";
            if (value is PointF)
            {
                var pf = (PointF)value;
                var parameters = new string[] { string.Format("{0}F", pf.X), string.Format("{0}F", pf.Y) };
                writer.WriteText(string.Format("{0} {1}({2})", newKey, typeof(PointF).FullName, string.Join(",", parameters)));
                return;
            }

            if (value is Point)
            {
                var pf = (Point)value;
                var parameters = new string[] { string.Format("{0}", pf.X), string.Format("{0}", pf.Y) };
                writer.WriteText(string.Format("{0} {1}({2})", newKey, typeof(Point).FullName, string.Join(",", parameters)));
                return;
            }

            throw new NotSupportedException();
        }
    }

    internal class ViewFunctionConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var eventHandlerName = (string)value;
            writer.View.AddClientEventScript(eventHandlerName);
            writer.WriteObject(eventHandlerName);
        }
    }

    internal class ChartItemFormatterConverter : ViewFunctionConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var eventHandlerName = (string)value;
            string eventBody = "    defaultFormatter();";
            writer.View.AddFunctionScript(eventHandlerName, eventBody, "engine", "hitTestInfo", "defaultFormatter");
            writer.WriteObject(eventHandlerName);
        }
    }

    internal class CollectionViewServiceConverter : ViewConverter
    {

        private static bool IsBoundMode(IDictionary<string, object> templatePars)
        {
            if (!templatePars.ContainsKey("ModelTypeName") || !templatePars.ContainsKey("DbContextTypeName"))
            {
                return false;
            }

            if (templatePars["ModelTypeName"] == null || string.IsNullOrEmpty(templatePars["ModelTypeName"].ToString()))
            {
                return false;
            }

            if (templatePars["DbContextTypeName"] == null || string.IsNullOrEmpty(templatePars["DbContextTypeName"].ToString()))
            {
                return false;
            }

            return true;
        }

        private void WriteActionUrlsInUnboundMode(ViewWriter writer, IItemsSource<object> collectionView,
            IScaffolderDescriptor scaffolderDescriptor, IMvcProject project)
        {
            if (string.IsNullOrEmpty(collectionView.CreateActionUrl) &&
                string.IsNullOrEmpty(collectionView.UpdateActionUrl) &&
                string.IsNullOrEmpty(collectionView.DeleteActionUrl) &&
                string.IsNullOrEmpty(collectionView.BatchEditActionUrl))
            {
                return;
            }

            // <#= ActionsHolder #>:
            var parameters = new Dictionary<string, object>();
            var actionPrefix = project.IsRazorPages ? "OnPost" : "";
            var c1JsonContext = project.IsRazorPages ? "JsonConvertHelper" : (project.IsCs ? "this" : "Me");

            // cv["CreateActionUrl"]
            // CreateMethodName = Create
            if (!string.IsNullOrEmpty(collectionView.CreateActionUrl))
            {
                string resolvedUrl = ResolveUrl(collectionView.CreateActionUrl, actionPrefix);
                if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                {
                    collectionView.CreateActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                    parameters.Add("CreateActionUrl", collectionView.CreateActionUrl);
                    parameters.Add("CreateMethodName", writer.Controller.GetMemberName("Create"));
                }
            }

            // cv["UpdateActionUrl"]
            // UpdateMethodName = "Update"
            if (!string.IsNullOrEmpty(collectionView.UpdateActionUrl))
            {
                string resolvedUrl = ResolveUrl(collectionView.UpdateActionUrl, actionPrefix);
                if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                {
                    collectionView.UpdateActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                    parameters.Add("UpdateActionUrl", collectionView.UpdateActionUrl);
                    parameters.Add("UpdateMethodName", writer.Controller.GetMemberName("Update"));
                }
            }

            // cv["DeleteActionUrl"]
            // DeleteMethodName = Delete
            if (!string.IsNullOrEmpty(collectionView.DeleteActionUrl))
            {
                string resolvedUrl = ResolveUrl(collectionView.DeleteActionUrl, actionPrefix);
                if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                {
                    collectionView.DeleteActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                    parameters.Add("DeleteActionUrl", collectionView.DeleteActionUrl);
                    parameters.Add("DeleteMethodName", writer.Controller.GetMemberName("Delete"));
                }
            }

            // cv["BatchEditActionUrl"]
            if (collectionView.BatchEdit && !string.IsNullOrEmpty(collectionView.BatchEditActionUrl))
            {
                string resolvedUrl = ResolveUrl(collectionView.BatchEditActionUrl, actionPrefix);
                if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                {
                    collectionView.BatchEditActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                    parameters.Add("BatchEditActionUrl", collectionView.BatchEditActionUrl);

                    if (project.IsAspNetCore)
                    {
                        var efcVersion = project.EntityFrameworkCoreVersion;
                        var reloadOnError = efcVersion != null && efcVersion >= new Version("1.1");
                        parameters.Add("ReloadOnBatchError", reloadOnError);
                    }
                }
            }

            if (project.IsAspNetCore)
            {
                // GetSqlExceptionMethodName
                parameters.Add("GetSqlExceptionMethodName", writer.Controller.GetMemberName("GetSqlException"));
                // GetExceptionMessageMethodName
                parameters.Add("GetExceptionMessageMethodName", writer.Controller.GetMemberName("GetExceptionMessage"));

                // ArrayToStringMethodName
                var arrayToStringMethodName = "";
                if (!string.IsNullOrEmpty(collectionView.DeleteActionUrl) || !string.IsNullOrEmpty(collectionView.BatchEditActionUrl))
                {
                    arrayToStringMethodName = writer.Controller.GetMemberName("ArrayToString");
                }
                parameters.Add("ArrayToStringMethodName", arrayToStringMethodName);

            }

            if (parameters.Count == 0)
            {
                return;
            }
            string controllerContent = writer.GetContentFromTemplate("Controllers\\CV\\Edit", parameters);
            writer.Controller.AddAction(controllerContent);
        }

        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var project = context.ServiceProvider.GetService(typeof(IMvcProject)) as IMvcProject;
            var currentDbContext = context.ServiceProvider.GetService(typeof(IDefaultDbContextProvider)) as IDefaultDbContextProvider;
            var scaffolderDescriptor = context.ServiceProvider.GetService(typeof(IScaffolderDescriptor)) as IScaffolderDescriptor;
            var cv = value as IItemsSource<object>;
            var component = scaffolderDescriptor.TemplateParameters["Component"] as Dictionary<string, object>;

            //is not bound mode
            if (!IsBoundMode(scaffolderDescriptor.TemplateParameters))
            {
                //try
                //{
                //    WriteActionUrlsInUnboundMode(writer, cv, scaffolderDescriptor, project);
                //}
                //catch (Exception ex)
                //{
                //    //if exception happens, the following logic is not interrupted.
                //}
                writer.WriteObject(cv);
                return;
            }

            // <#= DbContextsHolder #>
            // DbContextName
            var dbContextName = writer.Controller.AddDbContext("db", currentDbContext.DbContextType.TypeName);
            // ViewDataTypeName
            var viewDataTypeName = currentDbContext.ModelType.TypeName;
            // EntitySetName
            var dbContextProvider = context.ServiceProvider.GetService(typeof(IDbContextProvider)) as IDbContextProvider;
            var entitySetName = dbContextProvider.GetEntitySetName(currentDbContext.DbContextType, currentDbContext.ModelType);


            var actionPrefix = project.IsRazorPages ? "OnPost" : "";
            var c1JsonContext = project.IsRazorPages ? "JsonConvertHelper" : (project.IsCs ? "this" : "Me");

            // 1. Controller
            string controllerContent;
            // If ReadActionUrl is not set:
            if (string.IsNullOrEmpty(cv.ReadActionUrl))
            {
                // If insert the control, add ViewBag
                if (scaffolderDescriptor.IsInsert || project.IsRazorPages)
                {
                    var viewBagName = writer.Controller.AddViewBagItem(entitySetName, dbContextName, entitySetName);
                    cv.Source = ViewUtility.ConverViewBagToModelType(viewBagName, viewDataTypeName, context);
                }
                else
                {
                    cv.Source = "Model";
                }
            }

            // <#= ActionsHolder #>:
            var parameters = new Dictionary<string, object>();

            // Read
            // >>>>Columns
            var columns = component.ContainsKey("Columns") ? component["Columns"] : new List<dynamic>();
            parameters.Add("Columns", columns);

            if (!string.IsNullOrEmpty(cv.ReadActionUrl))
            {
                string resolvedUrl = ResolveUrl(cv.ReadActionUrl, actionPrefix);
                if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                {
                    parameters.Add("C1JsonContext", c1JsonContext);
                    parameters.Add("ViewDataTypeName", viewDataTypeName);
                    parameters.Add("DbContextName", dbContextName);
                    parameters.Add("EntitySetName", entitySetName);

                    cv.ReadActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                    parameters.Add("ReadActionUrl", cv.ReadActionUrl);
                    controllerContent = writer.GetContentFromTemplate("Controllers\\CV\\Read", parameters);
                    writer.Controller.AddAction(controllerContent);
                }
            }

            // Edit
            if (!string.IsNullOrEmpty(cv.CreateActionUrl) || !string.IsNullOrEmpty(cv.UpdateActionUrl)
                || !string.IsNullOrEmpty(cv.DeleteActionUrl) || !string.IsNullOrEmpty(cv.BatchEditActionUrl))
            {
                parameters = new Dictionary<string, object>();
                // PrimaryKeys
                var pk = "";
                var primaryKeys = currentDbContext.PrimaryKeys;
                if (primaryKeys != null && primaryKeys.Count > 0)
                {
                    pk = string.Join(", ", primaryKeys.Select(p => "item." + p.Key));
                }
                parameters.Add("C1JsonContext", c1JsonContext);
                parameters.Add("PrimaryKeys", pk);
                parameters.Add("ViewDataTypeName", viewDataTypeName);
                parameters.Add("DbContextName", dbContextName);
                parameters.Add("EntitySetName", entitySetName);

                // cv["CreateActionUrl"]
                // CreateMethodName = Create
                if (!string.IsNullOrEmpty(cv.CreateActionUrl))
                {
                    string resolvedUrl = ResolveUrl(cv.CreateActionUrl, actionPrefix);
                    if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                    {
                        cv.CreateActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                        parameters.Add("CreateActionUrl", cv.CreateActionUrl);
                        parameters.Add("CreateMethodName", writer.Controller.GetMemberName("Create"));
                    }
                }

                // cv["UpdateActionUrl"]
                // UpdateMethodName = "Update"
                if (!string.IsNullOrEmpty(cv.UpdateActionUrl))
                {
                    string resolvedUrl = ResolveUrl(cv.UpdateActionUrl, actionPrefix);
                    if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                    {
                        cv.UpdateActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                        parameters.Add("UpdateActionUrl", cv.UpdateActionUrl);
                        parameters.Add("UpdateMethodName", writer.Controller.GetMemberName("Update"));
                    }
                }

                // cv["DeleteActionUrl"]
                // DeleteMethodName = Delete
                if (!string.IsNullOrEmpty(cv.DeleteActionUrl))
                {
                    string resolvedUrl = ResolveUrl(cv.DeleteActionUrl, actionPrefix);
                    if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                    {
                        cv.DeleteActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                        parameters.Add("DeleteActionUrl", cv.DeleteActionUrl);
                        parameters.Add("DeleteMethodName", writer.Controller.GetMemberName("Delete"));
                    }
                }

                // cv["BatchEditActionUrl"]
                if (cv.BatchEdit && !string.IsNullOrEmpty(cv.BatchEditActionUrl))
                {
                    string resolvedUrl = ResolveUrl(cv.BatchEditActionUrl, actionPrefix);
                    if (!scaffolderDescriptor.IsUpdate || !writer.Controller.IsExistMemberName(resolvedUrl))
                    {
                        cv.BatchEditActionUrl = writer.Controller.GetMemberName(resolvedUrl);
                        parameters.Add("BatchEditActionUrl", cv.BatchEditActionUrl);

                        if (project.IsAspNetCore)
                        {
                            var efcVersion = project.EntityFrameworkCoreVersion;
                            var reloadOnError = efcVersion != null && efcVersion >= new Version("1.1");
                            parameters.Add("ReloadOnBatchError", reloadOnError);
                        }
                    }
                }

                var proj = context.ServiceProvider.GetService(typeof(IMvcProject)) as IMvcProject;
                if (proj != null && proj.IsAspNetCore)
                {
                    // GetSqlExceptionMethodName
                    parameters.Add("GetSqlExceptionMethodName", writer.Controller.GetMemberName("GetSqlException"));
                    // GetExceptionMessageMethodName
                    parameters.Add("GetExceptionMessageMethodName", writer.Controller.GetMemberName("GetExceptionMessage"));

                    // ArrayToStringMethodName
                    var arrayToStringMethodName = "";
                    if (!string.IsNullOrEmpty(cv.DeleteActionUrl) || !string.IsNullOrEmpty(cv.BatchEditActionUrl))
                    {
                        arrayToStringMethodName = writer.Controller.GetMemberName("ArrayToString");
                    }
                    parameters.Add("ArrayToStringMethodName", arrayToStringMethodName);

                }

                controllerContent = writer.GetContentFromTemplate("Controllers\\CV\\Edit", parameters);
                writer.Controller.AddAction(controllerContent);
            }

            // 2. View
            writer.WriteObject(cv);
        }
    }

    internal class GroupDescriptionsConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var gps = value as IList<GroupDescription>;
            var proj = ViewUtility.GetProjectInfo(context);
            var isTagHelper = (proj != null && proj.IsAspNetCore);
            writer.WriteRawText(string.Join(",", gps.OfType<PropertyGroupDescription>().Select(pgd => isTagHelper ? pgd.PropertyName : string.Format("\"{0}\"", pgd.PropertyName))), false);
        }
    }

    internal sealed class ActionUrlConverter : ViewConverter
    {
        private string _pageMethodPrefix = "OnPost";

        public ActionUrlConverter()
        {
        }

        public ActionUrlConverter(string pageMethodPrefix)
        {
            _pageMethodPrefix = pageMethodPrefix;
        }

        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var proj = ViewUtility.GetProjectInfo(context);
            string format;
            var url = (string)value;

            if (proj == null || !proj.IsAspNetCore)
            {
                format = "Url.Action(\"{0}\")";
            }
            else
            {
                if (proj.IsRazorPages)
                {
                    var controller = context.ServiceProvider.GetService(typeof(IControllerSnippets)) as IControllerSnippets;
                    format = "@Url.Page(\"" + controller.ControllerName + "\", \"{0}\")";
                    url = ResolveHandler(url, _pageMethodPrefix);
                }
                else
                {
                    format = "@Url.Action(\"{0}\")";
                }
            }
            writer.WriteText(string.Format(format, url));
        }
    }

    internal sealed class SourceCollectionConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            writer.WriteText((string)value);
        }
    }
    #endregion Common

    #region Grid
    internal sealed class DataMapConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var dataMap = value as DataMap;

            // 1. Controller
            if (dataMap.IsValid)
            {
                var project = context.ServiceProvider.GetService(typeof(IMvcProject)) as IMvcProject;
                var currentDbContext = context.ServiceProvider.GetService(typeof(IDefaultDbContextProvider)) as IDefaultDbContextProvider;
                // DbContextName
                var dbContextName = writer.Controller.AddDbContext("db", currentDbContext.DbContextType.TypeName);
                if (dbContextName == null)
                {
                    throw new InvalidOperationException();
                }

                // DataTypeName
                var dataTypeName = dataMap.ModelType.TypeName;

                // EntitySetName
                var entitySetName = dataMap.EntitySetName;

                if (dataMap.UseRemoteBinding)
                {
                    // <#= ActionsHolder #>:
                    var parameters = new Dictionary<string, object>();
                    parameters.Add("IsValid", dataMap.IsValid);
                    parameters.Add("UseRemoteBinding", dataMap.UseRemoteBinding);
                    parameters.Add("DataTypeName", dataTypeName);
                    parameters.Add("DbContextName", dbContextName);
                    parameters.Add("EntitySetName", entitySetName);

                    var c1JsonContext = project.IsRazorPages ? "JsonConvertHelper" : (project.IsCs ? "this" : "Me");
                    parameters.Add("C1JsonContext", c1JsonContext);

                    var actionPrefix = project.IsRazorPages ? "OnPost" : "";
                    var readUrl = ResolveUrl("DataMap_" + entitySetName + "_" + dataMap.DisplayMemberPath + "_" + dataMap.SelectedValuePath + "_Read", actionPrefix);
                    var readActionUrl = writer.Controller.GetMemberName(readUrl);
                    dataMap.ItemsSource.ReadActionUrl = readActionUrl;
                    parameters.Add("ReadActionUrl", readActionUrl);

                    var controllerContent = writer.GetContentFromTemplate("Controllers\\DataMap\\Action", parameters);
                    writer.Controller.AddAction(controllerContent);
                }
                else
                {
                    // <#= ViewBagsHolder #>
                    var viewBagName = writer.Controller.AddViewBagItem(entitySetName, dbContextName, entitySetName);
                    dataMap.ItemsSource.Source = ViewUtility.ConverViewBagToModelType(viewBagName, dataTypeName, context);
                }
            }

            // 2. View
            writer.WriteObject(dataMap);
        }
    }
    #endregion Grid

    #region Sheet
    internal abstract class RemoteBaseConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            BeforeWrite(writer);
            var scaffolderDescriptor = context.ServiceProvider.GetService(typeof(IScaffolderDescriptor)) as IScaffolderDescriptor;
            var component = scaffolderDescriptor.TemplateParameters["Component"] as Dictionary<string, object>;

            var parameters = new Dictionary<string, object>();
            parameters.Add("FilePath", component["FilePath"] ?? string.Empty);
            var proj = ViewUtility.GetProjectInfo(context);
            if (proj != null && proj.IsAspNetCore)
            {
                var webRootPathName = writer.Controller.AddWebRoot("_webRootPath");
                parameters.Add("WebRootPathName", webRootPathName);
            }

            var c1JsonContext = (proj != null && proj.IsRazorPages) ? "JsonConvertHelper" : ((proj == null || proj.IsCs) ? "this" : "Me");
            parameters.Add("C1JsonContext", c1JsonContext);

            AddParameters(parameters, component);

            var actionContent = writer.GetContentFromTemplate(TemplateName, parameters);
            writer.Controller.AddAction(actionContent);

            var actionUrlConverter = new ActionUrlConverter(PageModelPrefix);
            actionUrlConverter.Serialize(writer, ActionName, context);
        }

        protected abstract string PageModelPrefix { get; }
        protected abstract void BeforeWrite(ViewWriter writer);
        protected abstract void AddParameters(Dictionary<string, object> parameters, Dictionary<string, object> component);

        protected abstract string TemplateName { get; }
        protected abstract string ActionName { get; }
    }

    internal sealed class RemoteLoadConverter : RemoteBaseConverter
    {
        private string _actionName;
        protected override string ActionName
        {
            get
            {
                return _actionName;
            }
        }
        protected override string TemplateName
        {
            get
            {
                return "Controllers\\FlexSheet\\RemoteLoadFileAction";
            }
        }

        protected override string PageModelPrefix
        {
            get { return "OnGet"; }
        }

        protected override void BeforeWrite(ViewWriter writer)
        {
            var project = writer.Context.ServiceProvider.GetService(typeof(IMvcProject)) as IMvcProject;
            var actionPrefix = project.IsRazorPages ? PageModelPrefix : "";
            _actionName = writer.Controller.GetMemberName(ResolveUrl("RemoteLoadFile", actionPrefix));
        }

        protected override void AddParameters(Dictionary<string, object> parameters, Dictionary<string, object> component)
        {
            parameters.Add("RemoteLoadFileActionName", ActionName);
        }
    }

    internal sealed class RemoteSaveConverter : RemoteBaseConverter
    {
        private string _actionName;
        protected override string ActionName
        {
            get
            {
                return _actionName;
            }
        }
        protected override string TemplateName
        {
            get
            {
                return "Controllers\\FlexSheet\\RemoteSaveFileAction";
            }
        }

        protected override string PageModelPrefix
        {
            get { return "OnPost"; }
        }

        protected override void BeforeWrite(ViewWriter writer)
        {
            var project = writer.Context.ServiceProvider.GetService(typeof(IMvcProject)) as IMvcProject;
            var actionPrefix = project.IsRazorPages ? PageModelPrefix : "";
            _actionName = writer.Controller.GetMemberName(ResolveUrl("RemoteSaveFile", actionPrefix));
        }

        protected override void AddParameters(Dictionary<string, object> parameters, Dictionary<string, object> component)
        {
            parameters.Add("RemoteSaveFileActionName", ActionName);
            parameters.Add("SavePath", component["SavePath"] ?? string.Empty);
        }
    }

    internal abstract class SheetsConverterBase : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var proj = ViewUtility.GetProjectInfo(context);
            var proxyCreationEnabled = proj.IsAspNetCore;

            var sheets = (IList<Sheet.Sheet>)value;
            foreach (var sheet in sheets)
            {
                if (!sheet.IsBound)
                {
                    WriteSheet(writer, sheet);
                    continue;
                }

                var boundSheet = sheet as C1.Web.Mvc.Sheet.BoundSheet<object>;
                if (boundSheet == null)
                    continue;

                var dbContextName = writer.Controller.AddDbContext("db", boundSheet.DbContextType.TypeName);
                var dbContextProvider = context.ServiceProvider.GetService(typeof(IDbContextProvider)) as IDbContextProvider;
                var entitySetName = dbContextProvider.GetEntitySetName(boundSheet.DbContextType, boundSheet.ModelType);

                var viewBagName = writer.Controller.AddViewBagItem("Sheet_" + entitySetName, dbContextName, entitySetName, proxyCreationEnabled);
                boundSheet.ItemsSource.Source = ViewUtility.ConverViewBagToModelType(viewBagName, boundSheet.ModelType.TypeName, context);
                //Set DisableServerRead to default value.
                //It will always be true for BoundSheet, so don't need to serialize this property.
                boundSheet.ItemsSource.DisableServerRead = false;

                WriteSheet(writer, sheet);
            }
        }

        protected abstract void WriteSheet(ViewWriter writer, Sheet.Sheet sheet);
    }

    internal sealed class HtmlHelperSheetsConverter : SheetsConverterBase
    {
        protected internal override bool CanConvertWhole
        {
            get
            {
                return true;
            }
        }

        protected override void WriteSheet(ViewWriter writer, Sheet.Sheet sheet)
        {
            var memberName = sheet.IsBound ? "AddBoundSheet" : "AddUnboundSheet";
            writer.WriteMember(memberName, sheet);
        }
    }

    internal sealed class TagHelperSheetsConverter : SheetsConverterBase
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            base.WriteView(writer, value, context);

            writer.WriteObject(value);
        }

        protected override void WriteSheet(ViewWriter writer, Sheet.Sheet sheet)
        {

        }
    }
    #endregion Sheet

    #region Chart
    internal sealed class ChartIItemsSourceBindingHolderConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            ConvertBindingHolder(writer, value, context);
            writer.WriteObject(value);
        }

        public static void ConvertBindingHolder(ViewWriter writer, object value, IContext context)
        {
            var bindingHolder = value as IItemsSourceBindingHolder<object>;
            if (bindingHolder != null)
            {
                if (bindingHolder.DbContextType != null && bindingHolder.ModelType != null)
                {
                    // <#= DbContextsHolder #>
                    // DbContextName
                    var dbContextName = writer.Controller.AddDbContext("db", bindingHolder.DbContextType.TypeName);
                    // DataTypeName
                    var dataTypeName = bindingHolder.ModelType.TypeName;
                    // EntitySetName
                    var dbContextProvider = context.ServiceProvider.GetService(typeof(IDbContextProvider)) as IDbContextProvider;
                    var entitySetName = dbContextProvider.GetEntitySetName(bindingHolder.DbContextType, bindingHolder.ModelType);
                    var viewBagName = writer.Controller.AddViewBagItem(entitySetName, dbContextName, entitySetName);
                    bindingHolder.ItemsSource.Source = ViewUtility.ConverViewBagToModelType(viewBagName, dataTypeName, context);
                }
            }
        }
    }

    internal sealed class PalettesConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            writer.WriteText("C1.Web.Mvc.Chart." + value.GetType().Name + "." + value.ToString());
        }
    }

    internal sealed class DataPointConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var dataPoint = value as DataPoint;
            var proj = ViewUtility.GetProjectInfo(context);
            var newKey = proj.IsAspNetCore || proj.IsCs ? "new" : "New";
            if (dataPoint != null)
            {
                writer.WriteText(string.Format("{0} {1}(", newKey, typeof(DataPoint).FullName));
                writer.WriteMemberValue(dataPoint.X);
                writer.WriteText(",");
                writer.WriteMemberValue(dataPoint.Y);
                writer.WriteText(")");
            }
        }

        protected internal override bool CanConvert(Type objectType, IContext context)
        {
            if (typeof(DataPoint).IsAssignableFrom(objectType))
            {
                return true;
            }
            return false;
        }
    }

    internal abstract class StringArrayConverter : ViewConverter
    {
        protected virtual string GetStringArrayCodeSnippets(string[] value)
        {
            return "\"" + string.Join("\",\"", value) + "\"";
        }
    }

    internal sealed class ChartFunctionConverter : ViewConverter
    {
        private readonly string _parameters;
        private readonly string _body;
        public ChartFunctionConverter(string parameters, string body)
            : this(parameters)
        {
            _body = body;
        }

        public ChartFunctionConverter(string parameters)
        {
            _parameters = parameters;
        }

        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            var funcName = (string)value;
            var funcBody = string.IsNullOrEmpty(_body) ? "" : _body;
            var funcParameters = string.IsNullOrEmpty(_parameters) ? "" : _parameters;
            // Add the function to the view.
            writer.View.AddFunctionScript(funcName, funcBody, funcParameters.Split(','));
            // Write the member.
            writer.WriteObject(funcName);
        }
    }
    #endregion Chart

    #region DashboardLayout
    internal abstract class LayoutConverterBase : ViewConverter
    {
        protected internal override bool CanConvertWhole
        {
            get
            {
                return true;
            }
        }
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            WriteLayout(writer, value as LayoutBase);
        }

        protected abstract void WriteLayout(ViewWriter writer, LayoutBase layout);
    }

    internal sealed class HtmlHelperLayoutConverter : LayoutConverterBase
    {
        protected override void WriteLayout(ViewWriter writer, LayoutBase layout)
        {
            var memberName = "AttachAutoGridLayout";
            if (layout is FlowLayout)
            {
                memberName = "AttachFlowLayout";
            }
            else if (layout is SplitLayout)
            {
                memberName = "AttachSplitLayout";
            }
            else if (layout is ManualGridLayout)
            {
                memberName = "AttachManualGridLayout";
            }
            writer.WriteMember(memberName, layout);
        }
    }

    #endregion DashboardLayout

    #region Pivot
    internal sealed class PivotFieldCollectionItemsConverter : ViewConverter
    {
        protected override void WriteView(ViewWriter writer, object value, IContext context)
        {
            IList<PivotFieldBase> fieldCollection = value as IList<PivotFieldBase>;
            if (value == null)
            {
                return;
            }

            int count = fieldCollection.Count;
            StringBuilder builder = new StringBuilder();
            var proj = ViewUtility.GetProjectInfo(context);
            var isTagHelper = (proj != null && proj.IsAspNetCore);

            for (int idx = 0; idx < count; idx++)
            {
                if (isTagHelper)
                {
                    builder.AppendFormat("{0}", fieldCollection[idx].Key.Trim());
                }
                else
                {
                    builder.AppendFormat("\"{0}\"", fieldCollection[idx].Key.Trim());
                }

                if (idx != count - 1)
                {
                    builder.Append(',');
                }
            }

            if (isTagHelper)
            {
                writer.WriteMemberValue(builder.ToString());
            }
            else
            {
                writer.WriteRawText(builder.ToString(), false);
            }

        }
    }
    #endregion
}
