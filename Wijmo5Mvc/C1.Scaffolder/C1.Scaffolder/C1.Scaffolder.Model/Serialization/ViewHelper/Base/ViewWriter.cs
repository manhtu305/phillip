﻿using C1.Web.Mvc.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;

namespace C1.Web.Mvc.Serialization
{
    /// <summary>
    /// Defines the base class for the view writer: CSHtmlHelper, VBHtmlHelper and TagHelper.
    /// </summary>
    public abstract class ViewWriter : BaseWriter
    {
        #region Fields
        private IViewSnippets _view;
        private IControllerSnippets _controller;
        private int _freeSpaces;
        private bool _isNewLine = false;
        private readonly int _defaultIndent;
        private readonly int _maxLineLength;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets the view.
        /// </summary>
        public IViewSnippets View
        {
            get
            {
                if (Context == null || Context.ServiceProvider == null)
                {
                    return null;
                }

                return _view ?? (_view = GetViewSnippets());
            }
        }

        /// <summary>
        /// Gets the controller.
        /// </summary>
        public IControllerSnippets Controller
        {
            get
            {
                if (Context == null || Context.ServiceProvider == null)
                {
                    return null;
                }

                return _controller ?? (_controller = GetControllerSnippets());
            }
        }

        internal int Indent { get; set; }

        internal bool IsNewLine
        {
            get
            {
                return _isNewLine;
            }
        }

        internal virtual string TemplateFileSuffix
        {
            get 
            {
                return ".include.t4";
            }
        }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Creates an instance of <see cref="ViewWriter"/>.
        /// </summary>
        /// <param name="context">The context object.</param>
        /// <param name="helper">The attribute helper.</param>
        /// <param name="writer">The text writer.</param>
        protected ViewWriter(IContext context, IAttributeHelper helper, TextWriter writer)
            : base(context, helper, writer)
        {
            var viewSetting = Setting as ViewSetting;
            _defaultIndent = viewSetting.DefaultIndent;
            _maxLineLength = viewSetting.MaxLineLength;
            _freeSpaces = _maxLineLength;
        }
        #endregion Constructors

        #region Methods
        #region Public
        /// <summary>
        /// Writes the member with the specified name and value.
        /// </summary>
        /// <param name="name">Specifies the name.</param>
        /// <param name="value">The value.</param>
        public void WriteMember(string name, object value)
        {
            WriteMemberInfo(name, value, null, null);
        }

        /// <summary>
        /// Writes the member with the member information and its parent value.
        /// </summary>
        /// <param name="memberInfo">The member information.</param>
        /// <param name="parent">The parent value of the member.</param>
        public void WriteMember(object memberInfo, object parent)
        {
            WriteMemberInfoWithResolver(memberInfo, parent);
        }

        /// <summary>
        /// Writes the member with the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        public void WriteMember(object value)
        {
            WriteMemberInfoWithoutResolver(null, value, null);
        }

        /// <summary>
        /// Writes the member value.
        /// </summary>
        /// <param name="value">The member value.</param>
        /// <param name="converter">The converter used to write.</param>
        public void WriteMemberValue(object value, BaseConverter converter = null)
        {
            StartScope(value);
            WriteObject(value, converter);
            EndScope();
        }
        #endregion Public

        #region Member
        /// <summary>
        /// Overrides to convert the whole member: name and value.
        /// </summary>
        internal override void WriteMemberInfo(string name, object value, object memberInfo, BaseConverter converter)
        {
            var viewConverter = converter as ViewConverter;
            if (viewConverter != null && viewConverter.CanConvertWhole)
            {
                viewConverter.Serialize(this, value, Context);
            }
            else
            {
                base.WriteMemberInfo(name, value, memberInfo, converter);
            }
        }
        #endregion Member

        #region Control
        internal virtual void WriteControl(object control)
        {
            Utility.ArgumentNotNull(control, "control");
        }

        internal BaseConverter GetControlConverter(object control)
        {
            var cter = GetConverter(null, control);
            if (cter == null)
            {
                cter = GetFirstConverter(null, control);
            }
            return cter;
        }
        #endregion Control

        #region Name
        internal override void WriteMemberName(string name, object value, object memberInfo)
        {
            StartWriteNewMember();
        }
        #endregion Name

        #region Raw Values
        #region Simple
        internal override void WriteNull()
        {
            WriteRawText("null", false);
        }
        internal override void WriteDBNull()
        {
            WriteRawText("DBNull.Value", false);
        }

        internal override void WriteSimpleValue(string value)
        {
            var text = GetCodeText(value);
            WriteRawText(text, false);
        }
        internal abstract string GetCodeText(string value);

        internal override void WriteSimpleValue(char value)
        {
            throw new NotImplementedException();
        }
        internal override void WriteSimpleValue(int value)
        {
            WriteRawText(value.ToString(null, CultureInfo.InvariantCulture), false);
        }
        internal override void WriteSimpleValue(float value)
        {
            WriteRawText(Utility.EnsureDecimalPlace(value, value.ToString("R", CultureInfo.InvariantCulture)) + "F", false);
        }
        internal override void WriteSimpleValue(double value)
        {
            WriteRawText(Utility.EnsureDecimalPlace(value, value.ToString("R", CultureInfo.InvariantCulture)), false);
        }
        internal override void WriteSimpleValue(Enum value)
        {
            throw new NotImplementedException();
        }
        internal override void WriteSimpleValue(DateTime value)
        {
            throw new NotImplementedException();
        }
        internal override void WriteSimpleValue(decimal value)
        {
            WriteRawText(Utility.EnsureDecimalPlace(value.ToString(null, CultureInfo.InvariantCulture)), false);
        }
        internal override void WriteSimpleValue(bool value)
        {
            throw new NotImplementedException();
        }
        internal override void WriteOtherSimples(object value)
        {
            WriteRawText(Convert.ToString(value, CultureInfo.InvariantCulture), false);
        }
        internal override void WriteSimpleValue(TimeSpan value)
        {
            throw new NotImplementedException();
        }
        #endregion Simple

        #region Complex
        internal override void WriteComplex(object value, object memberInfo, bool withoutComplexPrefix = false)
        {
            OnBeginningWrite();
            var descriptors = GetMembers(value);
            var total = descriptors.Count;
            MVCControl setting = value is ISupportUpdater ? ((ISupportUpdater)value).ParsedSetting : null;
            Dictionary<string, string> alreadyWroteProperties = new Dictionary<string, string>();
            for (var i = 0; i < total; i++)
            {
                PropertyDescriptor pd = (PropertyDescriptor)descriptors[i];
                MVCProperty p;
                if (setting != null && pd != null && setting.Properties.ContainsKey(pd.Name) &&
                    (!((p = setting.Properties[pd.Name]).Value is MVCControlCollection) && (p.NeedTextEditor || p.IsIgnored)))
                {
                    alreadyWroteProperties.Add(pd.Name, p.RenderedText);
                }
                else
                {
                    WriteComplexSubItem(pd, value, i, total);
                }
            }
            if (setting != null)
            {
                foreach(var item in setting.Properties)
                {
                    if (setting.CodeType == BlockCodeType.HTMLNode && item.Key == "DashboardLayout")
                        continue;

                    //add support OData
                    if ((!(item.Value.Value is MVCControlCollection) || (item.Key.Equals("BindODataSource") || item.Key.Equals("BindODataVirtualSource")))
                        && (item.Value.IsIgnored || item.Value.NeedTextEditor) && !alreadyWroteProperties.ContainsKey(item.Key))
                    {
                        alreadyWroteProperties.Add(item.Key, item.Value.RenderedText);
                    }
                }
            }
            
            if (alreadyWroteProperties.Count > 0)//write all remain properties (ignored and input text value)
            {
                foreach (var item in alreadyWroteProperties)
                {
                    WriteNewLine(true);
                    WriteRawText(item.Value, false);
                }
            }
            alreadyWroteProperties = null;
            OnWriteEnded();
        }
        #endregion
        #region Dictionary
        internal override void WriteDictionaryEntry(DictionaryEntry item, object parent, object parentMemberInfo)
        {
            throw new NotImplementedException();
        }
        #endregion Dictionary

        #region Collection
        internal override void WriteRawCollectionItem(object item, object collection, object collectionMemberInfo)
        {
            throw new NotImplementedException();
        }
        #endregion Collection

        #region Color
        // using string
        internal override void WriteColor(Color value)
        {
            WriteRawText(GetColorText(value), false);
        }

        internal abstract string GetColorText(Color value);
        #endregion Color

        #region CultureInfo
        internal override void WriteCultureInfo(CultureInfo value)
        {
            throw new NotImplementedException();
        }
        #endregion CultureInfo
        #endregion Raw Values

        #region Helpers
        internal virtual void WriteViewLine(bool connected = false)
        {
            if (_freeSpaces <= 0)
            {
                return;
            }

            WriteNewLine(connected);
        }

        /// <summary>
        /// Writes a new line.
        /// </summary>
        /// <param name="connected">A boolean value indicates whether to write a connected new line.</param>
        public virtual void WriteNewLine(bool connected = false)
        {
            _isNewLine = true;
            _freeSpaces = _maxLineLength;
        }

        internal override void WritePureText(string text)
        {
            if (_isNewLine)
            {
                _isNewLine = false;
                WritePureText("".PadLeft(Indent));
            }

            base.WritePureText(text);
            _freeSpaces = _freeSpaces - text.Length;
        }

        internal IViewSnippets GetViewSnippets()
        {
            if(Context != null)
            {
                return Context.ServiceProvider.GetService(typeof(IViewSnippets)) as IViewSnippets;
            }

            return null;
        }

        private IControllerSnippets GetControllerSnippets()
        {
            return Context.ServiceProvider.GetService(typeof(IControllerSnippets)) as IControllerSnippets;
        }

        internal string GetContentFromTemplate(string templateName, Dictionary<string, object> parameters = null)
        {
            var tp = Context.ServiceProvider.GetService(typeof(ITemplateProvider)) as ITemplateProvider;
            var filePath = tp.Get(templateName + TemplateFileSuffix);
            return tp.TransformTemplate(filePath, parameters);
        }

        internal void DecreaseIndent()
        {
            Indent -= _defaultIndent;
        }

        internal void IncreaseIndent()
        {
            Indent += _defaultIndent;
        }

        internal void StartWriteNewMember()
        {
            if (_freeSpaces <= 0)
            {
                WriteNewLine(true);
            }
        }
        #endregion Helpers
        #endregion Methods
    }
}
