﻿using C1.Web.Mvc;
using C1.Web.Mvc.Sheet;
using C1.Web.Mvc.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    internal class FlexSheetBinder : BaseControlBinder
    {
        public FlexSheetBinder()
        {
            base.MappedProperties.Add("RemoteLoad", "LoadActionUrl");
            base.MappedProperties.Add("RemoteSave", "SaveActionUrl");
            base.MappedProperties.Add("Load", "FilePath");

            #region dotnet core client events
            base.MappedProperties.Add("AutoSizedColumn", "OnClientAutoSizedColumn");
            base.MappedProperties.Add("AutoSizedRow", "OnClientAutoSizedRow");
            base.MappedProperties.Add("AutoSizingColumn", "OnClientAutoSizingColumn");
            base.MappedProperties.Add("AutoSizingRow", "OnClientAutoSizingRow");
            base.MappedProperties.Add("BeginningEdit", "OnClientBeginningEdit");
            base.MappedProperties.Add("CellEditEnded", "OnClientCellEditEnded");
            base.MappedProperties.Add("CellEditEnding", "OnClientCellEditEnding");
            base.MappedProperties.Add("Copied", "OnClientCopied");
            base.MappedProperties.Add("Copying", "OnClientCopying");
            base.MappedProperties.Add("DeletingRow", "OnClientDeletingRow");
            base.MappedProperties.Add("DraggedColumn", "OnClientDraggedColumn");
            base.MappedProperties.Add("DraggedRow", "OnClientDraggedRow");
            base.MappedProperties.Add("DraggingColumn", "OnClientDraggingColumn");
            base.MappedProperties.Add("DraggingRow", "OnClientDraggingRow");
            base.MappedProperties.Add("DraggingRowColumn", "OnClientDraggingRowColumn");
            base.MappedProperties.Add("BeginDroppingRowColumn", "OnClientBeginDroppingRowColumn");
            base.MappedProperties.Add("EndDroppingRowColumn", "OnClientEndDroppingRowColumn");
            base.MappedProperties.Add("FormatItem", "OnClientFormatItem");
            base.MappedProperties.Add("GotFocus", "OnClientGotFocus");
            base.MappedProperties.Add("GroupCollapsedChanged", "OnClientGroupCollapsedChanged");
            base.MappedProperties.Add("GroupCollapsedChanging", "OnClientGroupCollapsedChanging");
            base.MappedProperties.Add("Loaded", "OnClientLoaded");
            base.MappedProperties.Add("LoadedRows", "OnClientLoadedRows");
            base.MappedProperties.Add("LoadingRows", "OnClientLoadingRows");
            base.MappedProperties.Add("LostFocus", "OnClientLostFocus");
            base.MappedProperties.Add("Pasted", "OnClientPasted");
            base.MappedProperties.Add("Pasting", "OnClientPasting");
            base.MappedProperties.Add("PrepareCellForEdit", "OnClientPrepareCellForEdit");
            base.MappedProperties.Add("RemoteLoading", "OnClientRemoteLoading");
            base.MappedProperties.Add("RemoteLoaded", "OnClientRemoteLoaded");
            base.MappedProperties.Add("RemoteSaving", "OnClientRemoteSaving");
            base.MappedProperties.Add("RemoteSaved", "OnClientRemoteSaved");
            base.MappedProperties.Add("ResizedColumn", "OnClientResizedColumn");
            base.MappedProperties.Add("ResizedRow", "OnClientResizedRow");
            base.MappedProperties.Add("ResizingColumn", "OnClientResizingColumn");
            base.MappedProperties.Add("ResizingRow", "OnClientResizingRow");
            base.MappedProperties.Add("RowAdded", "OnClientRowAdded");
            base.MappedProperties.Add("RowEditEnded", "OnClientRowEditEnded");
            base.MappedProperties.Add("RowEditEnding", "OnClientRowEditEnding");
            base.MappedProperties.Add("ScrollPositionChanged", "OnClientScrollPositionChanged");
            base.MappedProperties.Add("SelectionChanged", "OnClientSelectionChanged");
            base.MappedProperties.Add("SelectionChanging", "OnClientSelectionChanging");
            base.MappedProperties.Add("SelectedSheetChanged", "OnClientSelectedSheetChanged");
            base.MappedProperties.Add("SheetVisibleChanged", "OnClientSheetVisibleChanged");
            base.MappedProperties.Add("UnknownFunction", "OnClientUnknownFunction");
            base.MappedProperties.Add("ColumnChanged", "OnClientColumnChanged");
            base.MappedProperties.Add("PrepareChangingColumn", "OnClientPrepareChangingColumn");
            base.MappedProperties.Add("PrepareChangingRow", "OnClientPrepareChangingRow");
            #endregion
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            string propertyName = property.Name;
            bool ret = false;
            var propertyValue = settings[realPropName].Value;

            ret = base.BindComplexProperty(property, settings, realPropName, instance);
            if (ret) return true;

            if (propertyName.Equals("AppendedSheets"))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                if (builder != null)
                {
                    FlexSheet flexSheet = instance as FlexSheet;

                    for (int j = 0; j < builder.Items.Count; j++)
                    {
                        MVCControl sheetSetting = builder.Items[j];
                        Sheet.Sheet sheet;
                        if (sheetSetting.Name == "BoundSheet")
                        {
                            sheet = new BoundSheet<object>();
                        }
                        else
                        {
                            sheet = new UnboundSheet();
                        }

                        if (sheetSetting.Properties.Count == 1 && sheetSetting.Properties.Keys.ElementAt(0) == "Add")
                        {
                            MVCControlCollection cellBuilder = sheetSetting.Properties["Add"].Value as MVCControlCollection;
                            if (cellBuilder != null)
                            {
                                BindControlProperties(cellBuilder.Items[0], sheet);
                            }
                        }
                        else  //case cb.Add().IsReadOnly(true)....
                        {
                            BindControlProperties(sheetSetting, sheet);
                        }

                        flexSheet.AppendedSheets.Add(sheet);
                    }
                    ret = true;
                }
            }
            else if (propertyName.Equals("ItemsSource"))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                BoundSheet<object> boundSheet = instance as BoundSheet<object>;
                if (builder.Items.Count == 1)
                {
                    BindControlProperties(builder.Items[0], boundSheet.ItemsSource);
                    ret = true;
                }
            }
            else if (propertyName.Equals("LoadActionUrl"))
            {
                FlexSheet flexSheet = instance as FlexSheet;
                flexSheet.LoadActionUrl = GetUrlAction(propertyValue);
                flexSheet.UseRemoteLoad = true;
                ret = true;
            }
            else if (propertyName.Equals("SaveActionUrl"))
            {
                FlexSheet flexSheet = instance as FlexSheet;
                flexSheet.SaveActionUrl = GetUrlAction(propertyValue);
                flexSheet.UseRemoteSave = true;
                ret = true;
            }
            else if (propertyName.Equals("ShowFormulaBar"))
            {
                FlexSheet flexSheet = instance as FlexSheet;
                flexSheet.ShowFormulaBar = true;
                ret = true;
            }
            return ret;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return property.Name.Equals("LoadActionUrl")
                || property.Name.Equals("SaveActionUrl")
                || property.Name.Equals("ShowFormulaBar")
                || base.IsRequireBindComplex(property, propertyValue);
        }
    }
}