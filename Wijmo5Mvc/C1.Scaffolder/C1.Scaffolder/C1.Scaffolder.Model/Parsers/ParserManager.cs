﻿using C1.Web.Mvc.Services;
using EnvDTE;
using System;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using System.IO;
using System.Collections.Generic;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Manage all tasks of parser
    /// </summary>
    public class ParserManager
    {
        public ParserManager()
        {
        }
        internal BaseHtmlParser CreateParser(BlockCodeType type)
        {
            switch (type)
            {
                case BlockCodeType.CSharp:
                    return new CSHtmlParser();
                case BlockCodeType.VB:
                    return new VBHtmlParser();
                case BlockCodeType.HTMLNode:
                    return new CoreHtmlParser();
            }
            return null;
        }
        /// <summary>
        /// Find and return a MVC control
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="projectInfo"></param>
        /// <returns></returns>
        public MVCControl GetMVCControl(IServiceProvider serviceProvider, IMvcProject projectInfo) {

            DTE dte = (DTE)serviceProvider.GetService(typeof(DTE));

            if (dte.ActiveDocument != null)
            {
                var textDocument = (TextDocument)dte.ActiveDocument.Object("TextDocument");
                var editPoint = textDocument.StartPoint.CreateEditPoint();
                string textContent = editPoint.GetText(textDocument.EndPoint.CreateEditPoint());
                string fileName = dte.ActiveDocument.FullName;
                fileName = fileName.ToLowerInvariant();
                if (!(fileName.EndsWith(".cshtml") || fileName.EndsWith(".vbhtml"))) return null;
                MVCControl mvcControl = null;

                BlockCodeType codeType = BlockCodeType.VB;
                if (projectInfo != null)
                {
                    if (projectInfo.IsAspNetCore)
                    {
                        codeType = BlockCodeType.HTMLNode;
                    }
                    else if (projectInfo.IsCs)
                    {
                        codeType = BlockCodeType.CSharp;
                    }
                }
            
                RazorParser razorParser = new RazorParser(textContent, codeType);
                try
                {
                    ControlBlockCode block = razorParser.GetControlBlockAt(textDocument.Selection.ActivePoint.CreateEditPoint());
                    if (block != null)
                    {
                        BaseHtmlParser htmlParser = CreateParser(block.CodeType);
                        if (htmlParser != null)
                            mvcControl = htmlParser.GetControlSetting(block.Content);

                        if (mvcControl != null)
                        {
                            mvcControl.StartIndex = block.StartIndex;
                            mvcControl.EndIndex = block.EndIndex;

                            return mvcControl;
                        }
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return null;
        }
        private string GetFileName(ITextBuffer buffer)
        {
            Microsoft.VisualStudio.TextManager.Interop.IVsTextBuffer bufferAdapter;
            buffer.Properties.TryGetProperty(typeof(Microsoft.VisualStudio.TextManager.Interop.IVsTextBuffer), out bufferAdapter);
            if (bufferAdapter != null)
            {
                var persistFileFormat = bufferAdapter as Microsoft.VisualStudio.Shell.Interop.IPersistFileFormat;
                string ppzsFilename = null;
                uint frmIndex;
                if (persistFileFormat != null) persistFileFormat.GetCurFile(out ppzsFilename, out frmIndex);
                return ppzsFilename;
            }
            else return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="textBuffer"></param>
        /// <param name="textView"></param>
        /// <returns></returns>
        public MVCControl GetMVCControl(ITextBuffer textBuffer, ITextView textView)
        {
            ITextCaret caret = textView.Caret;
            string fileName = GetFileName(textBuffer).ToLowerInvariant();
            MVCControl control = null;
            if (fileName.EndsWith(".cshtml") || fileName.EndsWith(".vbhtml"))
            {
                if (caret.Position.BufferPosition > 0)
                {
                    int currentPosition = caret.Position.BufferPosition.Position;
                    string html = string.Empty;
                    bool isCSharp = fileName.EndsWith(".cshtml");
                    StreamReader sr = new StreamReader(fileName);
                    html = sr.ReadToEnd();
                    RazorParser parser = new RazorParser(html, isCSharp ? BlockCodeType.CSharp : BlockCodeType.VB);
                    List<ControlBlockCode> list = parser.GetControlBlocks();
                    for (int i = 0; i < list.Count; i++)
                    {
                        ControlBlockCode block = list[i];
                        if (block.StartIndex <= currentPosition && block.EndIndex >= currentPosition)
                        {
                            ParserManager pm = new ParserManager();
                            BaseHtmlParser htmlParser = pm.CreateParser(block.CodeType);
                            control = htmlParser.GetControlSetting(block.Content);
                        }
                    }
                }
            }
            return control;
        }
    }
}
