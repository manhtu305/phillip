﻿using C1.Web.Mvc;
using C1.Web.Mvc.Sheet;
using C1.Web.Mvc.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    internal class FlexChartBinder : BaseControlBinder
    {
        public FlexChartBinder()
        {
            base.MappedProperties.Add("Areas", "PlotAreas");
            base.MappedProperties.Add("DotnetCoreSeries", "Series");
            base.MappedProperties.Add("Tooltips", "Tooltip");
            base.MappedProperties.Add("Layers", "AnnotationLayer");
            base.MappedProperties.Add("AddAnnotationLayer", "AnnotationLayer");
            base.MappedProperties.Add("Palette", "PredefinedPalette");
            base.MappedProperties.Add("AddLineMarker", "UseLineMarker");
            base.MappedProperties.Add("AddRangeSelector", "UseRangeSelector");
            base.MappedProperties.Add("Datalabels", "DataLabel");//for dotnet core
            base.MappedProperties.Add("Legends", "Legend");//for dotnet core
            base.MappedProperties.Add("Selectors", "UseRangeSelector");//for dotnet core
            base.MappedProperties.Add("Markers", "UseLineMarker");//for dotnet core
            base.MappedProperties.Add("Animations", "ShowAnimation");//for dotnet core
            base.MappedProperties.Add("Axis", "NetCoreAxis");//for dotnet core

            #region dotnet core client events
            base.MappedProperties.Add("SelectionChanged", "OnClientSelectionChanged");
            base.MappedProperties.Add("GotFocus", "OnClientGotFocus");
            base.MappedProperties.Add("LostFocus", "OnClientLostFocus");
            base.MappedProperties.Add("Rendered", "OnClientRendered");
            base.MappedProperties.Add("Rendering", "OnClientRendering");
            #endregion
        }

        private List<KeyValuePair<string, MVCControl>> SeriesGetValues(List<MVCControl> items)
        {
            List<KeyValuePair<string, MVCControl>> result = null;
            if (items.Count == 0) return null;
            result = new List<KeyValuePair<string, MVCControl>>(items.Count);
            if (items[0].Name.StartsWith("FlexChart"))
            {
                foreach (var item in items)
                    result.Add(new KeyValuePair<string, MVCControl>(item.Name.Remove(0, 9), item));
            }
            else
            {
                foreach (var item in items)
                {
                    if (item.Properties.Count == 0 || !item.Properties.ElementAt(0).Key.Contains("Add"))
                        result.Add(new KeyValuePair<string, MVCControl>("Series", item));
                    else
                        result.Add(new KeyValuePair<string, MVCControl>(item.Properties.ElementAt(0).Key.Remove(0, 3), item));
                }
            }

            return result;
        }

        // type == 0 meaning for header
        // type == 1 meaning for footer
        internal void HandleStyles(object propertyValue, object instance, int type)
        {
            if (propertyValue == null || (type != 0 && type != 1)) return;

            TitleStyle style = null;
            if (type == 0)
                style = (instance as FlexChartBase<object>).HeaderStyle;
            else if (type == 1)
                style = (instance as FlexChartBase<object>).FooterStyle;

            BindControlProperties(propertyValue as MVCControl, style);
            switch (style.Halign)
            {
                case "left":
                    style.DesignHalign = HalignEnum.Left;
                    break;
                case "right":
                    style.DesignHalign = HalignEnum.Right;
                    break;
                default:
                    break;
            }
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            string propertyName = property.Name;
            bool ret = false;
            var propertyValue = settings[realPropName].Value;

            ret = base.BindComplexProperty(property, settings, realPropName, instance);
            if (ret) return true;

            MVCControlCollection builder = propertyValue as MVCControlCollection;
            FlexChartBase<object> chart = instance as FlexChartBase<object>;

            if (propertyName.Equals("Legend"))
            {
                if (builder != null && builder.Items.Count == 1 && chart != null)
                {
                    BindControlProperties(builder.Items[0], chart.Legend);
                    ret = true;
                }
            }
            else if (propertyName.Equals("Tooltip"))
            {
                if (builder != null && builder.Items.Count == 1 && chart != null)
                    BindControlProperties(builder.Items[0], chart.Tooltip);
                ret = true;
            }
            else if (propertyName.Equals("Series"))
            {
                if (builder == null || builder.Items.Count == 0) return true;
                IList<ChartSeriesBase<object>> series;
                if (!(instance is FlexChartCore<object>))
                {
                    return false;
                }

                var chartCore = chart as FlexChartCore<object>;
                series = chartCore.Series;
                var result = SeriesGetValues(builder.Items);
                ChartSeriesBase<object> dest = null;

                if (result != null)
                {
                    var seriesBinder = new ChartSeriesBinder();
                    foreach (var pair in result)
                    {
                        switch (pair.Key.ToLowerInvariant())
                        {
                            case "series":
                                dest = CreateDefaultSeries();
                                break;
                            case "trendline":
                                dest = new TrendLine<object>();
                                break;
                            case "movingaverage":
                                dest = new MovingAverage<object>();
                                break;
                            case "yfunction":
                            case "yfunctionseries":
                                dest = new YFunctionSeries<object>();
                                break;
                            case "parameterfunction":
                            case "parametricfunctionseries":
                                dest = new ParametricFunctionSeries<object>();
                                break;
                            case "waterfall":
                                dest = new Waterfall<object>();
                                break;
                            case "boxwhisker":
                                dest = new BoxWhisker<object>();
                                break;
                            case "errorbar":
                                dest = new ErrorBar<object>();
                                break;
                            default: continue;
                        }

                        seriesBinder.BindControlProperties(pair.Value, dest);
                        series.Add(dest);
                    }
                    result = null;
                }

                return true;
            }
            #region AnnotationLayer
            else if (propertyName.Equals("AnnotationLayer"))
            {
                if (builder != null && builder.Items.Count != 0)
                {
                    var layers = (chart as FlexChart<object>).AnnotationLayer;
                    AnnotationBase dest = null;

                    if (builder.Items[0].Properties.ElementAt(0).Key.StartsWith("Add"))
                    {
                        foreach (var item in builder.Items)
                        {
                            if (item.Properties.ElementAt(0).Key.StartsWith("Add"))
                                dest = (AnnotationBase)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance("C1.Web.Mvc." + item.Properties.ElementAt(0).Key.Remove(0, 3));
                            if (dest != null)
                            {
                                BindControlProperties(item, dest);
                                layers.Items.Add(dest);
                                dest = null;
                            }
                        }
                    }
                    else
                    {
                        var subPro = builder.Items[0].Properties.Select(o => o).ToList();

                        foreach (var item in subPro)
                        {
                            dest = (AnnotationBase)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance("C1.Web.Mvc." + item.Key.Remove(item.Key.Length - 1));
                            if (dest != null)
                            {
                                BindControlProperties((item.Value.Value as C1.Web.Mvc.MVCControlCollection).Items[0], dest);
                                layers.Items.Add(dest);
                                dest = null;
                            }
                        }

                        subPro = null;
                    }
                }

                ret = true;
            }
            #endregion
            else if (propertyName.Equals("HeaderStyle"))
            {
                HandleStyles(builder.Items[0], instance, 0);
                ret = true;
            }
            else if (propertyName.Equals("FooterStyle"))
            {
                HandleStyles(builder.Items[0], instance, 1);
                ret = true;
            }
            else if (propertyName.Equals("ShowAnimation"))
            {
                if (builder != null && builder.Items.Count == 1 && chart != null)
                {
                    chart.ShowAnimation = true;
                    BindControlProperties(builder.Items[0], chart.Animation);
                    ret = true;
                }
            }
            else if (propertyName.Equals("UseLineMarker"))
            {
                FlexChart<object> flexChart = chart as FlexChart<object>;
                if (builder != null && builder.Items.Count == 1 && flexChart != null)
                {
                    flexChart.UseLineMarker = true;
                    MVCControl lineSetting = builder.Items[0];
                    BindControlProperties(lineSetting, flexChart.LineMarker);

                    if (lineSetting.Properties.ContainsKey("Content") && ((string)lineSetting.Properties["Content"].Value) != "")
                        flexChart.LineMarker.ShowContent = true;
                    if (lineSetting.Properties.ContainsKey("OnClientPositionChanged") && ((string)lineSetting.Properties["OnClientPositionChanged"].Value) != "")
                        flexChart.LineMarker.ListenPositionChangedEvent = true;
                    ret = true;
                }
            }
            else if (propertyName.Equals("UseRangeSelector"))
            {
                FlexChart<object> flexChart = chart as FlexChart<object>;
                if (builder != null && builder.Items.Count == 1 && flexChart != null)
                {
                    flexChart.UseRangeSelector = true;
                    MVCControl rangeSetting = builder.Items[0];
                    BindControlProperties(rangeSetting, flexChart.RangeSelector);

                    if (
                        (rangeSetting.Properties.ContainsKey("OnClientRangeChanged") && ((string)rangeSetting.Properties["OnClientRangeChanged"].Value) != "")  // normal dotnet
                        ||
                        (rangeSetting.Properties.ContainsKey("RangeChanged") && ((string)rangeSetting.Properties["RangeChanged"].Value) != "")                  // dotnet core
                       )
                        flexChart.RangeSelector.ListenRangeChangedEvent = true;
                    ret = true;
                }
            }
            else if (propertyName.Equals("DataLabel"))
            {
                if (builder != null && builder.Items.Count == 1)
                {
                    dynamic chartCore = instance as FlexChartBase<object>;

                    if (chart != null)
                    {
                        BindControlProperties(builder.Items[0], chartCore.DataLabel);
                        ret = true;
                    }
                    else
                        ret = BindSpecifyProperties(property, propertyValue, instance);
                }
            }
            else if (propertyName.Equals("Options"))
            {
                if (builder != null && builder.Items.Count == 1)
                {
                    FlexChart<object> flexChart = instance as FlexChart<object>;
                    if (chart != null)
                    {
                        BindControlProperties(builder.Items[0], flexChart.Options);
                        ret = true;
                    }
                }
            }
            else if (propertyName.Equals("Funnel"))
            {
                if (builder != null && builder.Items.Count == 1)
                {
                    ExtraOptions option = instance as ExtraOptions;
                    if (option != null)
                    {
                        BindControlProperties(builder.Items[0], option.Funnel);
                        ret = true;
                    }
                }
            }
            else if (propertyName.Equals("ItemsSource"))
            {
                //TODO: check whether chart contains source
                ret = false;
            }
            else if (propertyName.Equals("ItemFormatter"))
            {
                if (!(instance is ItemsBoundControl<object>))
                    return false;

                foreach (var x in ((ItemsBoundControl<object>)instance).ClientEvents)
                {
                    if (!x.Handled && x.Name.Equals(propertyName))
                    {
                        x.Handled = true;
                        return true;
                    }
                }
            }
            else if (propertyName.Equals("PlotAreas"))
            {
                if (builder == null || builder.Items.Count == 0) return true;
                FlexChartCore<object> chartCore = instance as FlexChartCore<object>;
                if (instance == null) return false;
                var plotAreas = chartCore.PlotAreas;

                foreach (var item in builder.Items)
                {
                    var temp = new PlotArea();
                    BindControlProperties(item, temp);
                    plotAreas.Add(temp);
                }

                return true;
            }
            else if (propertyName.Contains("Axis"))
            {
                ChartAxis<object> axis = null;
                FlexChartCore<object> chartCore = instance as FlexChartCore<object>;
                if (instance == null) return false;
                if (chartCore != null)
                {
                    if (propertyName.Equals("AxisX"))
                        axis = chartCore.AxisX;
                    else
                        axis = chartCore.AxisY;

                    if (builder.Items.Count == 1)
                        BindControlProperties(builder.Items[0], axis);
                    else if (1 < builder.Items.Count)
                    {
                        foreach (var item in builder.Items)
                        {
                            if (item.Properties.ContainsKey("Property") && item.Properties["Property"].Value.ToString().Equals(propertyName))
                            {
                                BindControlProperties(item, axis);
                                return true;
                            }
                        }
                    }

                    return true;
                }
                else if ((axis = instance as ChartAxis<object>) != null)
                {
                    if (propertyName.Equals("AxisLine"))
                    {
                        axis.AxisLine = (bool)propertyValue;
                        return true;
                    }
                }
                else if (instance is C1.Web.Mvc.ChartAnimation<object>)
                {
                    var animation = instance as C1.Web.Mvc.ChartAnimation<object>;
                    switch (propertyName)
                    {
                        case "AxisAnimation":
                            animation.AxisAnimation = (bool)propertyValue;
                            break;
                    }
                    animation = null;
                    return true;
                }

                return false;
            }

            return ret;
        }

        internal virtual ChartSeriesBase<object> CreateDefaultSeries()
        {
            return new ChartSeries<object>();
        }

        internal virtual bool BindSpecifyProperties(PropertyInfo property, object propertyValue, object instance)
        {
            return true;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return
                property.Name.Equals("ItemFormatter") ||
                property.Name.Equals("Series") ||
                property.Name.Equals("AnnotationLayer") ||
                property.Name.Contains("Axis") ||
                base.IsRequireBindComplex(property, propertyValue);
        }
    }

    internal class FlexPieChartBinder : FlexChartBinder
    {
        internal override bool BindSpecifyProperties(PropertyInfo property, object propertyValue, object instance)
        {
            string propertyName = property.Name;
            bool ret = false;
            if (propertyName.Equals("DataLabel"))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                if (builder != null && builder.Items.Count == 1)
                {
                    FlexPie<object> chart = instance as FlexPie<object>;
                    if (chart != null)
                    {
                        BindControlProperties(builder.Items[0], chart.DataLabel);
                        ret = true;
                    }
                }
            }
            return ret;
        }

        internal override ChartSeriesBase<object> CreateDefaultSeries()
        {
            return null;
        }
    }

    internal class FlexRadarChartBinder : FlexChartBinder
    {
        public FlexRadarChartBinder()
        {
            base.MappedProperties.Add("SeriesVisibilityChanged", "OnClientSeriesVisibilityChanged");
        }

        internal override bool BindSpecifyProperties(PropertyInfo property, object propertyValue, object instance)
        {
            string propertyName = property.Name;
            bool ret = false;
            if (propertyName.Equals("DataLabel"))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                if (builder != null && builder.Items.Count == 1)
                {
                    FlexRadar<object> chart = instance as FlexRadar<object>;
                    if (chart != null)
                    {
                        BindControlProperties(builder.Items[0], chart.DataLabel);
                        ret = true;
                    }
                }
            }
            else if (propertyName.Equals("Series"))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                if (builder != null)
                {
                    FlexRadar<object> chart = instance as FlexRadar<object>;
                    if (chart != null)
                    {

                        for (int j = 0; j < builder.Items.Count; j++)
                        {
                            MVCControl seriesSetting = builder.Items[j];
                            FlexRadarSeries<object> series = new FlexRadarSeries<object>();
                            if (seriesSetting.Properties.Count == 1 && seriesSetting.Properties.Keys.ElementAt(0) == "Add")
                            {
                                MVCControlCollection columnBuilder = seriesSetting.Properties["Add"].Value as MVCControlCollection;
                                if (columnBuilder != null)
                                {
                                    BindControlProperties(columnBuilder.Items[0], series);
                                }
                            }
                            else  //case cb.Add().IsReadOnly(true)....
                            {
                                BindControlProperties(seriesSetting, series);
                            }

                            chart.Series.Add(series);
                        }
                        ret = true;
                    }
                }
            }
            return ret;
        }

        internal override ChartSeriesBase<object> CreateDefaultSeries()
        {
            return new FlexRadarSeries<object>();
        }
    }

    internal sealed class ChartSeriesBinder : BaseControlBinder
    {
        private static string[] PropNames = new string[] { "AddWaterfall", "AddTrendLine", "AddBoxWhisker", "AddYFunctionSeries",
            "AddParametricFunctionSeries", "AddMovingAverage", "AddErrorBar", "Add" };

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return base.IsRequireBindComplex(property, propertyValue) ||
               PropNames.Any(prop => prop.Equals(property.Name, StringComparison.OrdinalIgnoreCase));
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            base.BindComplexProperty(property, settings, realPropName, instance);
            return true;
        }
    }
}