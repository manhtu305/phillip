﻿using C1.Web.Mvc;
using C1.Web.Mvc.Sheet;
using C1.Web.Mvc.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    internal class InputBaseBinder : BaseControlBinder
    {
        public InputBaseBinder()
        {
            #region dotnet core client events
            base.MappedProperties.Add("IsDroppedDownChanged", "OnClientIsDroppedDownChanged");
            base.MappedProperties.Add("IsDroppedDownChanging", "OnClientIsDroppedDownChanging");
            base.MappedProperties.Add("FormatItem", "OnClientFormatItem");
            #endregion
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            var propertyValue = settings[realPropName].Value;
            if (base.BindComplexProperty(property, settings, realPropName, instance))
                return true;
            switch (property.Name)
            {
                case "Min":
                case "Max":
                case "TimeMin":
                case "TimeMax":
                case "Value":
                    if (instance is InputDateBase || instance is InputTime<object>)
                    {
                        object ctrl = instance;
                        var instanceProperty = ctrl.GetType().GetProperty(property.Name);
                        int start = propertyValue.ToString().IndexOf('\"'),
                            end = propertyValue.ToString().LastIndexOf('\"');
                        instanceProperty.SetValue(instance, DateTime.Parse(propertyValue.ToString().Substring(start + 1, end - start - 1)));
                        instanceProperty = null;
                        return true;
                    }
                    else if(instance is InputNumber)
                    {
                        if (property.Name.Equals("Min"))
                            (instance as InputNumber).Min = (double)propertyValue;
                        else
                            (instance as InputNumber).Max = (double)propertyValue;
                        return true;
                    }

                    return true;
            }
            return false;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return
                property.Name.Equals("Min") || property.Name.Equals("Max") ||
                property.Name.Equals("TimeMin") || property.Name.Equals("TimeMax") ||
                property.Name.Equals("Value") ||
                base.IsRequireBindComplex(property, propertyValue);
        }
    }

    internal class InputColorBinder : InputBaseBinder
    {
        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            var propertyValue = settings[realPropName].Value;
            if (base.BindComplexProperty(property, settings, realPropName, instance))
                return true;

            switch (property.Name)
            {
                // I don't know all case has property name is 'Value' but this property name make me feel like Invisible Bug.
                // so I decided to check property.ReflectedType.Name too.
                case "Value":
                    if (!property.ReflectedType.Name.Equals("InputColor")) return false;
                    string value = propertyValue.ToString();
                    InputColor inputColor = instance as InputColor;
                    if (value.StartsWith("System.Drawing.Color."))
                    {
                        value = value.Replace("System.Drawing.Color.", "");
                    }
                    else if (value.StartsWith("Color."))
                    {
                        value = value.Replace("Color.", "");
                    }
                    inputColor.Value = System.Drawing.Color.FromName(value);
                    return (inputColor.Value.Value.IsKnownColor);
                
                default:
                    return false;
            }
        }
    }

}
