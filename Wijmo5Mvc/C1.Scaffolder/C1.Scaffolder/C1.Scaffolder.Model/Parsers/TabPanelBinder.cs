﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    class TabPanelBinder : BaseControlBinder
    {
        public TabPanelBinder()
        {
            //client events(dotnet core)
            base.MappedProperties.Add("SelectedIndexChanged", "OnClientSelectedIndexChanged");
        }

        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            var propertyValue = settings[realPropName].Value;

            if (property.Name.Equals("Id"))
            {
                var tabPanel = instance as TabPanel;
                settings[realPropName].NeedTextEditor = false;
                tabPanel.ControlValue = tabPanel.Id  =  propertyValue.ToString();
                return true;
            }

            if (base.BindComplexProperty(property, settings, realPropName, instance))
                return true;

            return false;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return base.IsRequireBindComplex(property, propertyValue) ||
                property.Name.Equals("Id");
        }
    }
}
