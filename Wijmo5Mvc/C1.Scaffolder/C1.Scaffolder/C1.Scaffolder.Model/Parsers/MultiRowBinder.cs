﻿using C1.Web.Mvc;
using C1.Web.Mvc.MultiRow;
using C1.Web.Mvc.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    internal class MultiRowBinder : FlexGridBinder
    {
        public override bool BindComplexProperty(PropertyInfo property, Dictionary<string, MVCProperty> settings, string realPropName, object instance)
        {
            string propertyName = property.Name;
            bool ret = false;
            var propertyValue = settings[realPropName].Value;
            if (BindClientEventProperties(property, instance)) return true;
            if (propertyName.Equals("LayoutDefinition") || propertyName.Equals("Groups"))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                if (builder != null)
                {
                    MultiRow<object> multiRow = instance as MultiRow<object>;

                    for (int j = 0; j < builder.Items.Count; j++)
                    {
                        MVCControl groupSetting = builder.Items[j];
                        CellGroup cellGroups = new CellGroup();

                        if (groupSetting.Properties.Count == 1 && groupSetting.Properties.Keys.ElementAt(0) == "Add")
                        {
                            MVCControlCollection cellBuilder = groupSetting.Properties["Add"].Value as MVCControlCollection;
                            if (cellBuilder != null)
                            {
                                BindControlProperties(cellBuilder.Items[0], cellGroups);
                            }
                        }
                        else  //case cb.Add().IsReadOnly(true)....
                        {
                            BindControlProperties(groupSetting, cellGroups);
                        }

                        multiRow.LayoutDefinition.Add(cellGroups);
                    }
                    multiRow.FillDesignColumns(true);
                    ret = true;
                }
            }
            else if (propertyName.Equals("Cells"))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                if (builder != null)
                {
                    CellGroup cellGroup = instance as CellGroup;
                    for (int j = 0; j < builder.Items.Count; j++)
                    {
                        MVCControl cellSetting = builder.Items[j];
                        Cell cell = new Cell();
                        if (cellSetting.Properties.Count == 1 && cellSetting.Properties.Keys.ElementAt(0) == "Add")
                        {
                            MVCControlCollection cellBuilder = cellSetting.Properties["Add"].Value as MVCControlCollection;
                            if (cellBuilder != null)
                            {
                                BindControlProperties(cellBuilder.Items[0], cell);
                            }
                        }
                        else  //case cb.Add().IsReadOnly(true)....
                        {
                            BindControlProperties(cellSetting, cell);
                        }
                        cellGroup.Cells.Add(cell);
                        ret = true;
                    }
                }
            }
            else if (propertyName.Equals("DataMap"))
            {
                MVCControlCollection builder = propertyValue as MVCControlCollection;
                if (builder != null && builder.Items.Count == 1)
                {
                    MVCControl setting = builder.Items[0];
                    BindControlProperties(setting, ((Cell)instance).DataMap);
                    ret = true;
                }
            }
            else
            {
                ret = base.BindComplexProperty(property, settings, realPropName, instance);
            }
            return ret;
        }

        protected override bool IsRequireBindComplex(PropertyInfo property, object propertyValue)
        {
            return
                property.Name.Equals("AllowFiltering") ||
                base.IsRequireBindComplex(property, propertyValue);
        }
    }
}