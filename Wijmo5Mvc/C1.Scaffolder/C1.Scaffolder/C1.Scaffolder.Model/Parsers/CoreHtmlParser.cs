﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Parse html code page of the core project.
    /// </summary>
    internal class CoreHtmlParser : BaseHtmlParser
    {
        /// <summary>
        /// Get the parsed setting of control that generated in the html code
        /// </summary>
        /// <param name="textHtml"></param>
        /// <returns></returns>
        public override MVCControl GetControlSetting(string textHtml)
        {
            Reader = new StringReader(textHtml);
            string tempString = "";
            char c;
            c = (char)((ushort)Reader.Peek());
            string originText = string.Empty;
            MVCControl control = null;
            if (c == '<')
            {
                Reader.Read();//'<'
                tempString = ReadTag();
                if (!string.IsNullOrEmpty(tempString))
                {
                    originText = "<" + tempString;
                    control = new MVCControl(BlockCodeType.HTMLNode);
                    control.Name = GetPropertyName(tempString);
                    control = ReadAControl(tempString, ref originText);
                }
            }
            return control;
        }

        private MVCControl ReadAControl(string tagName, ref string originText)
        {
            MVCControl control = new MVCControl(BlockCodeType.HTMLNode);
            control.Name = GetPropertyName(tagName);
            char c;
            string tempString = "";
            string propName = "";
            bool isCSharp = false;
            MVCControlCollection lambda = null;
            string propertyOriginText = "";
            string controlOriginText = "";
            string lambdaOriginText = "";

            while ((c = (char)((ushort)Reader.Peek())) != '\uffff')
            {
                switch (c)
                {
                    case '<'://start open tag
                        Reader.Read();
                        tempString = ReadTag();
                        if (!string.IsNullOrEmpty(tempString))
                        {
                            originText += controlOriginText;
                            controlOriginText = "<" + tempString;
                            if (lambda != null && !lambda.VarialbeName.Equals(tempString))
                            {
                                propName = GetCollectionsName(lambda.VarialbeName);
                                if (propName.Equals("AppendedSheets") || propName.Equals("DotnetCoreSeries") || propName.Equals("SplitTiles") || propName.Equals("SplitGroups"))
                                {
                                    if (control.Properties.ContainsKey(propName))
                                    {
                                        MVCControlCollection collection = control.Properties[propName].Value as MVCControlCollection;
                                        control.Properties[propName].RenderedText += "\r\n" + lambdaOriginText;
                                        collection.Items.AddRange(lambda.Items);
                                    }
                                    else
                                    {
                                        MVCProperty propertyVal = new MVCProperty(propName, lambda, lambdaOriginText);
                                        control.Properties.Add(propName, propertyVal);
                                    }
                                }
                                else
                                {
                                    MVCProperty propertyVal = new MVCProperty(propName, lambda, lambdaOriginText);
                                    if (control.Properties.ContainsKey(propName))
                                        control.Properties.Remove(propName);
                                    control.Properties.Add(propName, propertyVal);
                                }
                                //control.Properties.Add(propName, lambda);
                                lambda = null;
                            }
                            if (lambda == null)
                            {
                                lambda = new MVCControlCollection();
                                lambda.VarialbeName = tempString;
                                lambda.Items = new List<MVCControl>();
                                lambdaOriginText = "";
                            }
                            MVCControl item = ReadAControl(tempString, ref controlOriginText);
                            if (item != null)
                            {
                                originText += controlOriginText;
                                if (lambda.Items.Count > 0) lambdaOriginText += "\r\n";
                                lambdaOriginText += controlOriginText;
                                controlOriginText = "";
                                lambda.Items.Add(item);
                            }
                        }
                        else
                        {
                            controlOriginText += "<";
                        }
                        break;
                    case '>':
                        tempString = "";
                        Reader.Read();
                        controlOriginText += ">" + ReadContent();
                        break;
                    case '=':
                    case ' ':
                        Reader.Read();
                        break;
                    case '"':
                        propName = GetPropertyName(tempString);
                        propertyOriginText = " " + tempString + " = \"";
                        Reader.Read();//ignore first '"'
                        tempString = ReadAttributeValue(ref isCSharp);
                        propertyOriginText += tempString + "\"";
                        Reader.Read();//ignore last '"'
                        object val = TryGetCorrectType(tempString);
                        if (propName.Equals("For"))//create binding value
                        {
                            string[] parts = val.ToString().Split('.');
                            if (parts.Length == 2)
                            {
                                control.Binding = new MVCProperty(parts[0], parts[1], propertyOriginText);
                            }
                            else if (parts.Length == 1)
                            {
                                control.Binding = new MVCProperty("", parts[0], propertyOriginText);
                            }
                        }
                        else
                        {
                            MVCProperty property = new MVCProperty(propName, val, propertyOriginText);
                            if (control.Properties.ContainsKey(propName))
                                control.Properties.Remove(propName);
                            control.Properties.Add(propName, property);
                        }
                        controlOriginText += propertyOriginText;//add to local control text                        
                        propertyOriginText = "";
                        tempString = "";
                        propName = "";
                        break;
                    case '/'://read close tag
                        Reader.Read();
                        controlOriginText += c;
                        tempString = ReadTag();
                        bool isSelfCloseTag = false;
                        if (tempString == "")//self close tag
                        {
                            c = (char)((ushort)Reader.Peek());
                            if (c == '>') isSelfCloseTag = true;
                        }
                        if (GetPropertyName(tempString) == control.Name || isSelfCloseTag)
                        {
                            controlOriginText += tempString + ">";
                            if (lambda != null)
                            {
                                propName = GetCollectionsName(lambda.VarialbeName);
                                if (propName.Equals("AppendedSheets") || propName.Equals("DotnetCoreSeries") || propName.Equals("SplitTiles") || propName.Equals("SplitGroups"))
                                {
                                    if (control.Properties.ContainsKey(propName))
                                    {
                                        MVCControlCollection collection = control.Properties[propName].Value as MVCControlCollection;
                                        control.Properties[propName].RenderedText += lambdaOriginText;
                                        collection.Items.AddRange(lambda.Items);
                                    }
                                    else
                                    {
                                        MVCProperty propertyVal = new MVCProperty(propName, lambda, lambdaOriginText);
                                        control.Properties.Add(propName, propertyVal);
                                    }
                                }

                                else
                                {
                                    MVCProperty propertyVal = new MVCProperty(propName, lambda, lambdaOriginText);
                                    if (control.Properties.ContainsKey(propName))
                                        control.Properties.Remove(propName);
                                    control.Properties.Add(propName, propertyVal);
                                }
                            }
                            Reader.Read();//'>'    
                            originText += controlOriginText;
                            controlOriginText = "";
                            tempString = "";
                            control.RenderedText = originText;
                            return control;
                        }
                        break;
                    default:
                        tempString = ReadTag();
                        if (tempString.Equals("\r\n"))
                        {
                            controlOriginText += tempString;
                        }
                        break;
                }
            }

            return null;
        }

        private string GetCollectionsName(string tag)
        {
            //==>special case: -> TODO: need refactor later
            if (tag.Equals("c1-multi-row-cell-group")) return "LayoutDefinition";
            else if (tag.Equals("c1-bound-sheet") || tag.Equals("c1-unbound-sheet")) return "AppendedSheets";
            else if (tag.Equals("c1-odata-source")) return "OdataSources";
            else if (tag.Equals("c1-odata-virtual-source")) return "OdataVirtualSources";
            else if (tag.Equals("c1-funnel-options")) return "Funnel";
            else if (tag.Equals("c1-flow-tile")) return "FlowTiles";
            else if (tag.Equals("c1-split-tile")) return "SplitTiles";
            else if (tag.Equals("c1-split-group")) return "SplitGroups";
            else if (
                tag.Equals("c1-flex-chart-series") ||
                tag.Equals("c1-flex-chart-trendline") ||
                tag.Equals("c1-flex-chart-movingaverage") ||
                tag.Equals("c1-flex-chart-yfunction") ||
                tag.Equals("c1-flex-chart-parameterfunction") ||
                tag.Equals("c1-flex-chart-waterfall") ||
                tag.Equals("c1-flex-chart-boxwhisker") ||
                tag.Equals("c1-flex-chart-error-bar")
                )
                return "DotnetCoreSeries";
            //<== special case
            string[] parts = tag.Split('-');
            string name = GetCamelString(parts[parts.Length - 1]);
            if (!name.EndsWith("s")) name += "s";
            return name;
        }

        private string GetPropertyName(string tag)
        {
            //special cases
            if (tag == "class") return "CssClass";//special case for class
            string[] parts = tag.Split('-');
            string propName = "";
            if (parts.Length > 0)
            {
                if (parts[0] != "c1")
                {
                    propName = GetCamelString(parts[0]);
                }
                for (int i = 1; i < parts.Length; i++)
                {
                    propName += GetCamelString(parts[i]);
                }
            }
            return propName;
        }

        private string GetCamelString(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                return name[0].ToString().ToUpper() + name.Substring(1);
            }
            return name;
        }

        private string ReadAttributeValue(ref bool isCSharpExp)
        {
            StringBuilder builder = new StringBuilder();
            char ch1 = (char)((ushort)Reader.Peek());//'@'
            if (ch1 == '@')
            {
                isCSharpExp = true;
                return ReadCSharpExp();
            }
            else if (ch1 == '"') //empty string
            {
                return string.Empty;
            }
            else
            {
                while ('\uffff' != ch1)
                {
                    builder.Append(ch1);
                    Reader.Read();
                    ch1 = (char)((ushort)Reader.Peek());
                    if (ch1 == '"') break;
                }
                return builder.ToString();
            }
        }

        private string ReadCSharpExp()
        {
            StringBuilder builder = new StringBuilder();
            char ch1 = (char)((ushort)Reader.Peek());//'@'
            Reader.Read();
            bool startWithBrace = false;
            builder.Append(ch1);
            ch1 = (char)((ushort)Reader.Peek());
            if (ch1 == '(') startWithBrace = true;
            int squareBraceLevel = 0;
            int braceLevel = 0;
            while ('\uffff' != ch1)
            {
                switch (ch1)
                {
                    case '(':
                        braceLevel++;
                        builder.Append(ch1);
                        Reader.Read();
                        break;
                    case ')':
                        Reader.Read();
                        builder.Append(ch1);
                        braceLevel--;
                        if (braceLevel == 0 && startWithBrace)
                        {
                            return builder.ToString();
                        }
                        break;
                    case '[':
                        squareBraceLevel++;
                        Reader.Read();
                        builder.Append(ch1);
                        break;
                    case ']':
                        squareBraceLevel--;
                        Reader.Read();
                        builder.Append(ch1);
                        break;
                    case '"':
                        if (squareBraceLevel == 0 && braceLevel == 0)//end of attribute
                        {
                            return builder.ToString();
                        }
                        builder.Append(ch1);
                        Reader.Read();
                        break;
                    default:
                        builder.Append(ch1);
                        Reader.Read();
                        break;
                }

                ch1 = (char)((ushort)Reader.Peek());
            }
            return builder.ToString();
        }

        private string ReadTag()
        {
            StringBuilder builder = new StringBuilder();
            char ch1;// = (char)((ushort)Reader.Peek());
            while ('\uffff' != (ch1 = (char)((ushort)Reader.Peek())))
            {
                switch (ch1)
                {
                    case '=':
                    case ' ':
                    case '>':
                    case '<':
                        return builder.ToString();
                    case '/':
                        return string.Empty;
                    default:
                        builder.Append(ch1);
                        Reader.Read();
                        break;
                }
            }
            return builder.ToString();
        }

        private string ReadContent()
        {
            StringBuilder builder = new StringBuilder();
            char c;
            while ('\uffff' != (c = (char)((ushort)Reader.Peek())) && c != '<')
            {
                builder.Append(c);
                Reader.Read();
            }
            return builder.ToString();
        }
    }
}
