﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    internal class CSHtmlParser : BaseMvcHtmlParser
    {
        public override MVCControl GetControlSetting(string textHtml)
        {
            if (textHtml.StartsWith("Html.C1()."))
            {
                textHtml = textHtml.Replace("Html.C1().", "");

                //find control name
                MVCControl setting = new MVCControl();
                Reader = new StringReader(textHtml);
                bool isModel = false;
                string tempString = "";
                string propName = "";
                string propValue = "";
                char c;
                string originText = "";
                bool foundComplex = false;
                // flag for set control value
                bool ctrlWaitToSet = false;

                while ((c = (char)((ushort)Reader.Peek())) != '\uffff')
                {
                    switch (c)
                    {
                        case '<'://start model
                            if (!string.IsNullOrEmpty(tempString))
                            {
                                if (string.IsNullOrEmpty(setting.Name))
                                {
                                    isModel = true;
                                    setting.Name = tempString;
                                    Reader.Read();
                                }
                                else
                                {
                                    //TFS 382505
                                    propName = tempString;
                                    object val = ReadParams(ref originText);
                                    MVCProperty property = new MVCProperty(propName, val, originText);
                                    //case user add more time, replace old propName
                                    if (setting.Properties.ContainsKey(propName))
                                    {
                                        setting.Properties.Remove(propName);
                                    }

                                    setting.Properties.Add(propName, property);
                                }
                                tempString = "";
                                originText = "";
                            }
                            break;
                        case '>':
                            if (isModel)
                            {
                                setting.ModelType = tempString;
                                tempString = "";
                                originText = "";
                                isModel = false;
                            }
                            Reader.Read();
                            break;
                        case '(':
                            if (setting.Name == null)
                            {
                                if (tempString.EndsWith("For"))//case binding value
                                {
                                    setting.Name = tempString.Substring(0, tempString.Length - 3);
                                    propName = tempString;
                                    MVCControlCollection val = ReadParams(ref originText) as MVCControlCollection;
                                    if (val != null && val.Items.Count == 1)
                                    {
                                        MVCControl item = val.Items[0];
                                        setting.Binding = new MVCProperty(item.Name,
                                            item.Properties.Count > 0 ? item.Properties.Keys.ElementAt(0).ToString() : "", originText);
                                    }
                                }
                                else
                                {
                                    if (!ctrlWaitToSet)
                                    {
                                        propName = tempString;
                                        ctrlWaitToSet = true;
                                    }
                                    setting.Name = tempString;
                                    Reader.Read();
                                }
                            }
                            else
                            {
                                if (tempString != "")
                                {
                                    propName = tempString;
                                    //special case for FlexSheet
                                    if (propName.Equals("AddUnboundSheet") || propName.Equals("AddBoundSheet"))
                                    {
                                        object val = ReadParams(ref originText);
                                        MVCControl sheet = new MVCControl();
                                        if (val is Array)
                                        {
                                            object[] parameters = val as object[];
                                            if (parameters.Length == 3)
                                            {
                                                sheet.Properties["Name"] = new MVCProperty("Name", parameters[0], "");
                                                sheet.Properties["RowCount"] = new MVCProperty("RowCount", parameters[1], "");
                                                sheet.Properties["ColumnCount"] = new MVCProperty("ColumnCount", parameters[2], "");
                                            }
                                        }
                                        else
                                            if (val is MVCControlCollection)
                                        {
                                            sheet = ((MVCControlCollection)val).Items[0];
                                        }
                                        sheet.Name = propName.Remove(0, 3);//remove "Add"
                                        MVCControlCollection sheets;
                                        string appendSheetsName = "AppendedSheets";
                                        //case user add more time, replace old propName
                                        if (setting.Properties.ContainsKey(appendSheetsName))
                                        {
                                            sheets = setting.Properties[appendSheetsName].Value as MVCControlCollection;
                                            setting.Properties[appendSheetsName].RenderedText += originText;
                                        }
                                        else
                                        {
                                            sheets = new MVCControlCollection();
                                            setting.Properties[appendSheetsName] = new MVCProperty(appendSheetsName, sheets, originText);
                                        }
                                        sheets.Items.Add(sheet);
                                    }
                                    else
                                    {
                                        object val = ReadParams(ref originText);
                                        MVCProperty property = new MVCProperty(propName, val, originText);
                                        //case user add more time, replace old propName
                                        if (setting.Properties.ContainsKey(propName))
                                        {
                                            setting.Properties.Remove(propName);
                                        }

                                        setting.Properties.Add(propName, property);
                                    }
                                }
                                else//case () of Name, don't have setting
                                {
                                    Reader.Read();
                                }
                            }
                            originText = "";
                            tempString = "";
                            break;
                        case ')':
                            if (tempString != "")
                            {
                                //found value of param
                                propValue = tempString;
                                if (
                                    !propName.Equals(string.Empty)
                                        &&
                                    !propValue.Equals(string.Empty)
                                   )
                                {
                                    // if the flag is turned on
                                    if (ctrlWaitToSet)
                                    {
                                        setting.ControlValue = propValue;
                                        ctrlWaitToSet = false;
                                    }

                                    //setting.Properties.Add(propName, propValue);
                                    propName = "";
                                    propValue = "";
                                }
                            }
                            Reader.Read();
                            originText = "";
                            tempString = "";
                            break;
                        case '\"':
                        case '\'':
                            tempString = ReadText();
                            originText += tempString;
                            break;
                        case ' ':
                        case '\r':
                        case '\n':
                        case '\t':
                            ReadIgnoreCharacter();
                            break;
                        case '.':
                            Reader.Read();
                            originText += c;
                            break;
                        default:
                            tempString = ReadStringItem(ref foundComplex);
                            originText += tempString;
                            break;
                    }
                }

                return setting;
            }
            return null;
        }

        /// <summary>
        /// Read a continuos string until meet next special character
        /// </summary>
        /// <returns></returns>
        private string ReadStringItem(ref bool needParseAsComplex, bool supportExpressionParam = false)
        {
            StringBuilder builder = new StringBuilder();
            char ch1 = (char)((ushort)Reader.Read());
            bool startQuote = false;
            int braceLevel = ch1 == '(' ? 1 : 0;
            needParseAsComplex = false;
            while ('\uffff' != ch1)
            {
                if (IsStartCommentLine(ch1))
                {
                    Reader.ReadLine();
                    return builder.ToString();
                }
                builder.Append(ch1);
                char ch2 = (char)((ushort)Reader.Peek());
                if (ch2 == '\0')
                {
                    throw new FormatException("Invalid setting text is input.");
                }

                switch (ch2)
                {
                    case '\"':
                        startQuote = !startQuote;
                        break;
                    //case ' ':
                    //    if (!startQuote && braceLevel == 0)
                    //        return builder.ToString().Trim();
                    //    break;
                    case ',':
                        if (!startQuote && braceLevel == 0)
                            return builder.ToString().Trim();
                        break;
                    case '(':
                        if (supportExpressionParam)
                            braceLevel++;
                        else
                            return builder.ToString().Trim();
                        break;
                    case ')':
                        if (--braceLevel < 0)
                        {
                            return builder.ToString().Trim();
                        }
                        break;
                    case '.':
                    case '<':
                        if (!supportExpressionParam)
                        {
                            return builder.ToString().Trim();
                        }
                        break;
                    case '>':
                        if (ch1 != '=' && braceLevel == 0)
                            return builder.ToString().Trim();
                        break;
                    case '=':
                        ch1 = (char)((ushort)Reader.Read());//'='
                        ch2 = (char)((ushort)Reader.Peek());
                        if (ch2 == '>') //lambda
                        {
                            needParseAsComplex = true;
                            return builder.ToString().Trim();
                        }
                        else
                        {
                            builder.Append(ch1);
                        }
                        break;
                }
                ch1 = (char)((ushort)Reader.Read());
            }
            return builder.ToString().Trim();
        }

        private object ReadParams(ref string originText)
        {
            List<object> paramsValue = new List<object>();
            string tempString = string.Empty;
            int bracketLevel = 0;
            bool found = false;
            string localOriginText = string.Empty;
            bool foundComplex = false;
            while (true)
            {
                char c = (char)((ushort)Reader.Peek());
                switch (c)
                {
                    case '(':
                        bracketLevel++;
                        if (bracketLevel > 1)
                        {
                            tempString = ReadStringItem(ref foundComplex, true);
                            localOriginText += tempString;
                            bracketLevel--;
                        }
                        else
                        {
                            Reader.Read();
                            localOriginText += c;
                        }
                        break;
                    case '\"':
                    case '\'':
                        tempString = ReadText();
                        localOriginText += tempString;
                        break;
                    case ',':
                    case ')':
                        if (tempString != "")
                        {
                            paramsValue.Add(TryGetCorrectType(tempString));
                            Reader.Read();
                            localOriginText += c;
                            if (c == ',')
                            {
                                tempString = "";
                            }
                            else
                            {
                                originText += localOriginText;
                                localOriginText = "";
                                found = (--bracketLevel == 0);
                            }
                        }
                        else if (c == ')')
                        {
                            Reader.Read();
                            originText += localOriginText;
                            localOriginText = "";
                            originText += c;
                            found = (--bracketLevel == 0);
                        }
                        break;
                    case '>'://lambda expression
                        Reader.Read();//'>'
                        if (foundComplex)
                        {
                            localOriginText += " => ";
                            paramsValue.Add(ReadComplexSettings(tempString, ref localOriginText));
                            tempString = "";
                            Reader.Read();//')'
                            originText += localOriginText + ")";
                            localOriginText = "";
                            found = (--bracketLevel == 0);
                            foundComplex = false;
                        }
                        else
                        {
                            localOriginText += ">";
                        }
                        break;
                    case '=':
                        localOriginText += "=";
                        Reader.Read();
                        break;
                    case ' ':
                    case '\t':
                        ReadIgnoreCharacter();
                        localOriginText += c;//keep 1 character
                        break;
                    case '\r':
                    case '\n':
                        ReadIgnoreCharacter();
                        localOriginText += "\r\n";//keep 1 pair characters
                        break;
                    default:
                        tempString = ReadStringItem(ref foundComplex, true);
                        localOriginText += tempString;
                        break;
                }

                if (found) break;
            }
            if (paramsValue.Count == 0) return null;
            if (paramsValue.Count == 1) return paramsValue[0];
            return paramsValue.ToArray();
        }

        private MVCControlCollection ReadComplexSettings(string variableName, ref string originText)
        {
            StringBuilder builder = new StringBuilder();
            string tempString = string.Empty;
            List<object> paramsValue = new List<object>();

            MVCControlCollection lambda = new MVCControlCollection();
            MVCControl control = null;
            lambda.VarialbeName = variableName;
            string propName = string.Empty;
            int bracket = 0;
            string localOriginText = "";
            bool foundComplex = false;
            while (true)
            {
                char c = (char)((ushort)Reader.Peek());
                switch (c)
                {
                    case '{':
                    case '}':
                        Reader.Read();
                        //originText += c;
                        localOriginText += c;
                        break;
                    case ';'://end item
                        if (control != null)
                        {
                            lambda.Items.Add(control);
                            control = null;
                        }
                        Reader.Read();
                        originText += localOriginText;
                        originText += c;
                        localOriginText = "";
                        break;
                    case '(':
                        bracket++;
                        if (tempString != "")
                        {
                            propName = tempString;
                            object val = ReadParams(ref localOriginText);
                            if (control != null)
                            {
                                if (propName.Equals("Add"))
                                {
                                    MVCControlCollection child = val as MVCControlCollection;
                                    if (child != null)
                                    {
                                        lambda.Items.Add(child.Items[0]);
                                        control = null;
                                    }
                                    else if (val != null)
                                    {
                                        MVCProperty property = new MVCProperty(propName, val, localOriginText);
                                        if (control.Properties.ContainsKey(propName))
                                            control.Properties.Remove(propName);//case user input duplicated propertyname
                                        control.Properties.Add(propName, property);
                                    }
                                }
                                else
                                {
                                    MVCProperty property = new MVCProperty(propName, val, localOriginText);
                                    if (control.Properties.ContainsKey(propName))
                                        control.Properties.Remove(propName);//case user input duplicated propertyname
                                    control.Properties.Add(propName, property);
                                }
                            }
                            originText += localOriginText;
                            localOriginText = "";
                            bracket--;
                        }
                        else
                        {
                            Reader.Read();
                            localOriginText += c;
                        }
                        break;
                    case ')':
                        bracket--;
                        if (bracket < 0)
                        {
                            if (control != null)
                            {
                                if (!string.IsNullOrEmpty(tempString) && !control.Properties.ContainsKey(tempString))
                                {
                                    control.Properties.Add(tempString, new MVCProperty(tempString, null, localOriginText));
                                }
                                lambda.Items.Add(control);
                            }
                            originText += localOriginText;
                            return lambda;
                        }
                        Reader.Read();
                        localOriginText += c;
                        break;
                    case ' ':
                    case '\r':
                    case '\n':
                    case '\t':
                        ReadIgnoreCharacter();
                        break;
                    case '.':
                        if (tempString.Equals(variableName))
                        {
                            tempString = "";
                            control = new MVCControl();//next control setting
                            control.Name = variableName;//lambda control
                        }
                        Reader.Read();
                        originText += localOriginText;
                        localOriginText = "" + c;
                        break;
                    default:
                        tempString = ReadStringItem(ref foundComplex);
                        localOriginText += tempString;
                        break;
                }
            }

        }
    }
}
