﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    internal class VBHtmlParser : BaseMvcHtmlParser
    {
        public override MVCControl GetControlSetting(string textHtml)
        {
            if (textHtml.StartsWith("Html.C1()."))
            {
                textHtml = textHtml.Replace("Html.C1().", "");

                //find control name
                MVCControl setting = new MVCControl(BlockCodeType.VB);
                Reader = new StringReader(textHtml);
                bool canBeModel = false;
                string tempString = "";
                string propName = "";
                string originText = "";
                char c;
                // flag for set control value
                bool ctrlWaitToSet = false;

                while ((c = (char)((ushort)Reader.Peek())) != '\uffff')
                {
                    switch (c)
                    {
                        case '(':
                            if (setting.Name == null)
                            {
                                if (tempString.EndsWith("For"))//case binding value
                                {
                                    setting.Name = tempString.Substring(0, tempString.Length - 3);
                                    propName = tempString;
                                    MVCControlCollection val = ReadParams(ref originText) as MVCControlCollection;
                                    if (val != null && val.Items.Count == 1)
                                    {
                                        MVCControl item = val.Items[0];
                                        setting.Binding = new MVCProperty(item.Name,
                                            item.Properties.Count > 0 ? item.Properties.Keys.ElementAt(0).ToString() : "", originText);
                                    }
                                }
                                else
                                {
                                    setting.Name = tempString;
                                    if (!ctrlWaitToSet)
                                    {
                                        setting.ControlValue = string.Empty;
                                        propName = tempString;
                                        ctrlWaitToSet = true;
                                    }
                                    canBeModel = true;
                                    Reader.Read();
                                }
                            }
                            else
                            {
                                if (tempString != "")
                                {
                                    propName = tempString;
                                    //special case for FlexSheet
                                    if (propName.Equals("AddUnboundSheet") || propName.Equals("AddBoundSheet"))
                                    {
                                        object val = ReadParams(ref originText);
                                        MVCControl sheet = new MVCControl(BlockCodeType.VB);
                                        if (val is Array)
                                        {
                                            object[] parameters = val as object[];
                                            if (parameters.Length == 3)
                                            {
                                                sheet.Properties["Name"] = new MVCProperty("Name", parameters[0], "");
                                                sheet.Properties["RowCount"] = new MVCProperty("RowCount", parameters[1], "");
                                                sheet.Properties["ColumnCount"] = new MVCProperty("ColumnCount", parameters[2], "");
                                            }
                                        }
                                        else
                                            if (val is MVCControlCollection)
                                        {
                                            sheet = ((MVCControlCollection)val).Items[0];
                                        }
                                        sheet.Name = propName.Remove(0, 3);//remove "Add"
                                        
                                        MVCControlCollection sheets;
                                        string appendSheetsName = "AppendedSheets";
                                        //case user add more time, replace old propName
                                        if (setting.Properties.ContainsKey(appendSheetsName))
                                        {
                                            sheets = setting.Properties[appendSheetsName].Value as MVCControlCollection;
                                            setting.Properties[appendSheetsName].RenderedText += originText;
                                        }
                                        else
                                        {
                                            sheets = new MVCControlCollection();
                                            setting.Properties[appendSheetsName] = new MVCProperty(appendSheetsName, sheets, originText);
                                        }
                                        sheets.Items.Add(sheet);
                                    }
                                    else
                                    {
                                        object val = ReadParams(ref originText);
                                        MVCProperty property = new MVCProperty(propName, val, originText);
                                        if (setting.Properties.ContainsKey(propName))
                                            setting.Properties.Remove(propName);
                                        setting.Properties.Add(propName, property);
                                    }
                                }
                                else//case () of Name
                                {
                                    Reader.Read();
                                }
                            }
                            
                            originText = "";
                            tempString = "";
                            break;
                        case ')':
                            // if the flag is turned on
                            if (ctrlWaitToSet)
                            {
                                setting.ControlValue = tempString;
                                ctrlWaitToSet = false;
                            }
                            Reader.Read();
                            tempString = "";
                            break;
                        case '\"':
                            tempString = ReadText(false);
                            originText += tempString;
                            break;
                        case ' ':
                            if (canBeModel && tempString.Equals("Of", StringComparison.Ordinal))
                            {
                                tempString = ReadStringItem(true);
                                setting.ModelType = tempString.Trim();
                                tempString = "";
                                originText = "";
                                canBeModel = false;
                                ctrlWaitToSet = false;
                            }
                            else
                            {
                                Reader.Read();//' '
                                originText += c;
                            }
                            break;
                        case '_'://continous in next row character
                            Reader.Read();
                            break;
                        case '\r':
                        case '\n':
                        case '\t':
                            ReadIgnoreCharacter();
                            break;
                        case '.':
                            Reader.Read();
                            originText += c;
                            break;
                        default:
                            tempString = ReadStringItem();
                            originText += tempString;
                            break;
                    }
                }

                return setting;
            }
            return null;
        }

/// <summary>
        /// Read a continuos string until meet next special character
        /// </summary>
        /// <returns></returns>
        private string ReadStringItem(bool supportExpressionParam = false) {
            StringBuilder builder = new StringBuilder();
            char ch1 = (char)((ushort)Reader.Read());
            bool startQuote = false;
            int braceLevel = ch1 == '(' ? 1 : 0;
            while ('\uffff' != ch1)
            {                
                if (IsStartCommentLine(ch1))
                {
                    Reader.ReadLine();
                    return "";
                }
                builder.Append(ch1);
                char ch2 = (char)((ushort)Reader.Peek());
                if (ch2 == '\0')
                {
                    throw new FormatException("Invalid setting text is input.");
                }

                switch (ch2)
                {
                    case '\"':
                        startQuote = !startQuote;
                        break;
                    case ' ':
                        if (!startQuote && braceLevel == 0)
                            return builder.ToString().Trim();
                        break;
                    case ',':
                        if (!startQuote && braceLevel == 0)
                            return builder.ToString().Trim();
                        break;
                    case '(':
                        if (supportExpressionParam && !builder.ToString().Equals("Sub") && !builder.ToString().Equals("Function"))
                            braceLevel++;
                        else 
                            return builder.ToString().Trim();
                        break;
                    case ')':
                        if (--braceLevel < 0)
                        {
                            return builder.ToString().Trim();
                        }
                        break;
                    case '.':
                    case '<':
                        if (!supportExpressionParam)
                        {
                            return builder.ToString().Trim();
                        }
                        break;
                    case '>':
                        if (ch1 != '=' && braceLevel == 0)
                            return builder.ToString().Trim();
                        break;
                    case '=':
                        ch1 = (char)((ushort)Reader.Read());//'='
                        ch2 = (char)((ushort)Reader.Peek());
                        if (ch2 == '>') //lambda
                        {
                            return builder.ToString().Trim();
                        }
                        else
                        {
                            builder.Append(ch1);
                        }
                        break;
                }
                ch1 = (char)((ushort)Reader.Read());
            }
            return builder.ToString().Trim();
        }

        private object ReadParams(ref string originText)
        {
            List<object> paramsValue = new List<object>();
            string tempString = string.Empty;
            int bracketLevel = 0;
            bool found = false;
            while (true)
            {
                char c = (char)((ushort)Reader.Peek());
                switch (c)
                {
                    case '(':
                        bracketLevel++;
                        if (bracketLevel > 1 && (tempString.Equals("Sub") || tempString.Equals("Function")))// start Sub
                        {
                            Reader.Read();//'('
                            originText += c;
                            tempString = ReadStringItem(true);
                            originText += tempString;
                            Reader.Read();//')'
                            originText += ")";
                            bracketLevel--;
                            paramsValue.Add(ReadComplexSettings(tempString, ref originText));
                            tempString = "";
                            Reader.Read();//')'
                            originText += ")";
                            found = (--bracketLevel == 0);
                        }
                        else
                        {
                            Reader.Read();
                            originText += c;
                        }
                        break;
                    case '\"':
                        tempString = ReadText(false);
                        originText += tempString;
                        break;
                    case ',':
                    case ')':
                        if (tempString!= "")
                        {
                            paramsValue.Add(TryGetCorrectType(tempString));
                            Reader.Read();
                            originText += c;
                            if (c == ',')
                            {
                                tempString = "";
                            }
                            else
                            {
                                found = (--bracketLevel ==0 );
                            }
                        }
                        else if (c == ')')
                        {
                            Reader.Read();
                            originText += c;
                            found = (--bracketLevel ==0 );
                        }
                        break;
                    case ' ':
                    case '\r':
                    case '\n':
                    case '\t':
                        ReadIgnoreCharacter();
                        break;
                    default:
                        tempString = ReadStringItem(true);
                        originText += tempString;
                        break;
                }

                if (found) break;
            }

            if (paramsValue.Count == 0) return null;
            if (paramsValue.Count == 1) return paramsValue[0];
            return paramsValue.ToArray();
        }

        private MVCControlCollection ReadComplexSettings(string variableName, ref string originText)
        {
            StringBuilder builder = new StringBuilder();
            string tempString = string.Empty;
            List<object> paramsValue = new List<object>();

            MVCControlCollection lambda = new MVCControlCollection();
            MVCControl control = null;// new MVCControl("-" +variableName +"-");
            lambda.VarialbeName = variableName;
            string propName = string.Empty;
            int bracket = 0;
            string localOriginText = "";
            while (true)
            {
                char c = (char)((ushort)Reader.Peek());
                switch (c)
                {
                    case '(':
                        bracket++;
                        if (tempString != "")
                        {
                            propName = tempString;
                            object val = ReadParams(ref localOriginText);
                            if (propName.Equals("Add"))
                            {
                                MVCControlCollection child = val as MVCControlCollection;
                                if (child != null)
                                {
                                    lambda.Items.Add(child.Items[0]);
                                    control = null;
                                }
                                else if (val != null)
                                {
                                    MVCProperty property = new MVCProperty(propName, val, localOriginText);
                                    if (control.Properties.ContainsKey(propName))
                                        control.Properties.Remove(propName);
                                    control.Properties.Add(propName, property);
                                }
                            }
                            else
                            {
                                MVCProperty property = new MVCProperty(propName, val, localOriginText);
                                if (control.Properties.ContainsKey(propName))
                                    control.Properties.Remove(propName);
                                control.Properties.Add(propName, property);
                            }
                            
                            originText += localOriginText;
                            localOriginText = "";
                            bracket--;
                        }
                        else
                        {
                            Reader.Read();
                            localOriginText += c;
                        }
                        break;
                    case ')':
                        bracket--;
                        if (bracket < 0)
                        {
                            if (control != null)
                            {
                                // !tempString.Equals("Sub") meaning for end of sub case
                                if (!string.IsNullOrEmpty(tempString) && !tempString.Equals("Sub") && !control.Properties.ContainsKey(tempString))
                                {
                                    control.Properties.Add(tempString, new MVCProperty(tempString, null, localOriginText));
                                }
                                lambda.Items.Add(control);
                            }
                            originText += localOriginText;
                            return lambda;
                        }
                        Reader.Read();
                        localOriginText += c;
                        break;
                    case '_'://continous in next row character
                        //Reader.Read();
                        //break;
                    case ' ':
                    case '\r':
                    case '\n':
                    case '\t':
                       // ReadIgnoreCharacter();
                       Reader.Read();
                        localOriginText += c;
                        break;
                    case '.':
                        if (tempString.Equals(variableName))
                        {
                            if (control != null)
                            {                                
                                lambda.Items.Add(control);
                                control = null;
                            }
                            tempString = "";
                            control =  new MVCControl(BlockCodeType.VB);//next control setting
                            control.Name = variableName;//
                        }
                        Reader.Read();
                        originText += localOriginText;
                        localOriginText = "" + c;
                        break;
                    default:
                        tempString = ReadStringItem();
                        localOriginText += tempString;
                        break;
                }
            }
        }
        
        protected override bool IsStartCommentLine(char c)
        {
            return c == '\'';
        }
        protected override bool TryGetBoolean(string value, out bool boolValue)
        {
            if (value.Equals("True", StringComparison.Ordinal))
            {
                boolValue = true;
                return true;
            }
            else if (value.Equals("False", StringComparison.Ordinal))
            {
                boolValue = false;
                return true;
            }
            boolValue = false;
            return false;
        }
    }
}
