﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Mvc
{
    /// <summary>
    /// Parse a html text to MVC control model.
    /// </summary>
    internal abstract class BaseHtmlParser
    {
        public StringReader Reader;
        public abstract MVCControl GetControlSetting(string textHtml);

        public BaseHtmlParser()
        {
        }

        protected object TryGetCorrectType(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                char c1 = value[0];
                bool booleanValue;
                if (char.IsDigit(c1) || c1 == '-' || c1 == '.')
                {
                    if (value.Contains(".") || value.Contains("e") || value.Contains("E") || value.Contains("+"))
                    {
                        double d;
                        if (double.TryParse(value, out d))
                        {
                            return d;
                        }
                    }
                    else
                    {
                        int n;
                        if (int.TryParse(value, out n))
                        {
                            return n;
                        }
                    }
                }
                else if (TryGetBoolean(value, out booleanValue))
                {
                    return booleanValue;
                }
            }
            return value;
        }
        protected virtual bool TryGetBoolean(string value, out bool boolValue)
        {
            if (value.Equals("true", StringComparison.Ordinal))
            {
                boolValue = true;
                return true;
            }
            else if (value.Equals("false", StringComparison.Ordinal))
            {
                boolValue = false;
                return true;
            }
            boolValue = false;
            return false;
        }
    }

    /// <summary>
    /// Parse a html text to MVC control model.
    /// </summary>
    internal abstract class BaseMvcHtmlParser : BaseHtmlParser
    {
        protected void ReadIgnoreCharacter()
        {
            while (true)
            {
                char c = (char)((ushort)Reader.Peek());
                if (c == ' ' || c == '\r' || c == '\n' || c == '\t') Reader.Read();
                else break;
            }
        }

        protected string ReadText(bool shouldEscape = true)
        {
            char ch2;
            StringBuilder builder1 = new StringBuilder();
            char ch1 = (char)((ushort)Reader.Read());
            bool escapeFound = false;
            builder1.Append(ch1);
            while (true)
            {
                ch2 = (char)((ushort)Reader.Read());
                if (ch2 == '\0')
                {
                    throw new FormatException("Invalid setting text is input.");
                }
                if (escapeFound)
                {
                    // read \t, \n, \r as special character:
                    switch (ch2)
                    {
                        case 't':
                            builder1.Append('\t');
                            break;
                        case 'n':
                            builder1.Append('\n');
                            break;
                        case 'r':
                            builder1.Append('\r');
                            break;
                        default:
                            // by default skip escape and read next charcter:
                            builder1.Append(ch2);
                            break;
                    }
                    escapeFound = false;
                    continue;
                }
                if (ch2 == '\\' && shouldEscape)
                {
                    escapeFound = true;
                    continue;                    
                }
                if (ch2 == ch1)//char /"
                {
                    builder1.Append(ch1);
                    return builder1.ToString();
                }
                builder1.Append(ch2);
            }
        }

        protected virtual bool IsStartCommentLine(char c)
        {
            char c2 = (char)((ushort)Reader.Peek());
            if (c == '/' && c2 == '/')
            {

                return true;
            }
            return false;
        }
    }
}
