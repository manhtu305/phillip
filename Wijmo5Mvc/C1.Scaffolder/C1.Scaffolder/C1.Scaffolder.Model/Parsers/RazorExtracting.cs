﻿using EnvDTE;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Razor;
using System.Web.Razor.Parser.SyntaxTree;

namespace C1.Web.Mvc
{
    internal class RazorParser
    {
        private string html;
        private BlockCodeType codeType;
        private bool isRazorComment;
        public RazorParser(string htmlCode, BlockCodeType type)
        {
            this.html = htmlCode;
            this.codeType = type;
        }

        /// <summary>
        /// Get all block code that contains defined of C1 MVC Control
        /// </summary>
        /// <returns>List of <see cref="ControlBlockCode"/> in page.</returns>
        public List<ControlBlockCode> GetControlBlocks() {
            /*
             * Because user can input both HTMLTagHelper and TagTagHelper in one Cshtml file
             * So we must change logic to support this case
             */
            List<ControlBlockCode> list = new List<ControlBlockCode>();
            if (codeType == BlockCodeType.VB)
            {
                RazorEngineHost _host = new RazorEngineHost(new VBRazorCodeLanguage());
                RazorTemplateEngine templateEngine = new RazorTemplateEngine(_host);
                ParserResults parserResult = templateEngine.ParseTemplate(new StringReader(html));
                if (parserResult.Success)
                {
                    list = GetControlBlocks(parserResult.Document as Block, codeType);
                }
            }
            else  //CS and HTML Node
            {
                //try with both CS code language and Node
                RazorEngineHost _host = new RazorEngineHost(new CSharpRazorCodeLanguage());
                RazorTemplateEngine templateEngine = new RazorTemplateEngine(_host);
                ParserResults parserResult = templateEngine.ParseTemplate(new StringReader(html));
                if (parserResult.Success)
                {
                    list = GetControlBlocks(parserResult.Document as Block, BlockCodeType.CSharp);
                }

                //html Node
                
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);
                if (htmlDoc.DocumentNode != null)
                {
                    List<ControlBlockCode> list2 = GetControlBlocks(htmlDoc.DocumentNode, BlockCodeType.HTMLNode);
                    if (list2.Count > 0)
                    {
                        list.AddRange(list2);
                    }
                }
            }

            return list;
        }

        private bool IsRazorCommentOpened(string html, bool IsOpened = false)
        {
            if (IsOpened)
            {
                var p = html.IndexOf("*@");
                return !(p >= 0 && html.IndexOf("@*", 0, p) < 0);
            }
            else
            {
                var p = html.LastIndexOf("@*");
                return (p >= 0 && html.LastIndexOf("*@", p + 1) < 0);
            }
        }
        
        private List<ControlBlockCode> GetControlBlocks(HtmlNode node, BlockCodeType type) {
            List<ControlBlockCode> lst = new List<ControlBlockCode>();
            if (node != null)
            {
                isRazorComment = node.ParentNode == null ? false : isRazorComment;
                if (node.Name.StartsWith("c1-") && !isRazorComment)
                {
                    lst.Add(new ControlBlockCode(node, BlockCodeType.HTMLNode));
                }
                else
                {
                    if (node.ChildNodes.Count == 0)
                    {
                        isRazorComment = IsRazorCommentOpened(node.InnerHtml, isRazorComment);
                    }
                    else
                    {
                        foreach (var child in node.ChildNodes)
                        {
                            lst.AddRange(GetControlBlocks(child as HtmlNode, type));
                        }
                    }
                }
            }
            return lst;
        }
        private List<ControlBlockCode> GetControlBlocks(Block block, BlockCodeType type) {
            List<ControlBlockCode> lst = new List<ControlBlockCode>();
            if (block != null)
            {
                if (block.Type == BlockType.Expression)
                {
                    string content = string.Empty;
                    foreach (var child in block.Children)
                    {
                        Span childSpan = child as Span;
                        if (childSpan != null && childSpan.Kind == SpanKind.Code)
                        {
                            content += childSpan.Content;
                        }
                        if (content.StartsWith("Html.C1()."))
                        {
                            lst.Add(new ControlBlockCode(block, type));
                            break;
                        }
                    }
                }
                else
                {
                    foreach (var child in block.Children)
                    {
                        lst.AddRange(GetControlBlocks(child as Block, type));
                    }
                }
            }
            return lst;
        }
        /// <summary>
        /// Get a block code of C1 MVC control at specify point
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public ControlBlockCode GetControlBlockAt(EditPoint point)
        {
            //need adjust position, because when count AbsoluteCharOffset each '\r\n' will be count as 1 character new line only
            int position = point.AbsoluteCharOffset;// + point.Line - 2;//and index base 1.
            
            List<ControlBlockCode> list = GetControlBlocks();
            if (list.Count <= 0) return null;
            foreach(ControlBlockCode block in list)
            {
                if (block.StartIndex <= position && block.EndIndex >= position) return block;
            }
            return null;
        }
    }
    /// <summary>
    /// Each control is render as a block of code in page, this class present that block.
    /// </summary>
    internal class ControlBlockCode {
        /// <summary>
        /// Constructor with a Block, result parsed of Razor engine.
        /// </summary>
        /// <param name="baseBlock"></param>
        /// <param name="type"></param>
        public ControlBlockCode(Block baseBlock, BlockCodeType type)
        {
            this.CodeType = type;
            ParseBlock(baseBlock);
        }

        /// <summary>
        /// Constructor with a HtmlNode, result parsed of HtmlAgilityPack engine.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="type"></param>
        public ControlBlockCode(HtmlNode node, BlockCodeType type)
        {
            this.CodeType = type;
            ParseFromHtmlNode(node);
        }
        
        /// <summary>
        /// Start position character in page.
        /// </summary>
        public int StartIndex { get; private set; }

        /// <summary>
        /// End position character in page.
        /// </summary>
        public int EndIndex { get; private set; }

        /// <summary>
        /// Raw text content in page.
        /// </summary>
        public string Content { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public BlockCodeType CodeType { get; set; }
        private void ParseFromHtmlNode(HtmlNode node)
        {
            Content = node.OwnerDocument.Text.Substring(node.StreamPosition, node.OuterLength);
            //because when count AbsoluteCharOffset each '\r\n' will be count as 1 character new line only
            StartIndex = node.StreamPosition -(node.Line - 2);
            HtmlNode nextNode = node.NextSibling;
            if (nextNode != null)
            {
                EndIndex = nextNode.StreamPosition - (nextNode.Line - 2);
            }
            else //control belong to last node
            {
                EndIndex = StartIndex + node.OuterLength -1;
            }
        }
        private void ParseBlock(Block baseBlock)
        {
            if (baseBlock == null) return;
            //start is '@'
            StartIndex = baseBlock.Start.AbsoluteIndex - (baseBlock.Start.LineIndex -1);
            
            foreach (var child in baseBlock.Children)
            {
                Span childSpan = child as Span;
                if (childSpan != null && childSpan.Kind == SpanKind.Code)
                {
                    Content += childSpan.Content;
                }
            }

            //end is last ')'
            SyntaxTreeNode lastNode = baseBlock.Children.Last();
            EndIndex = lastNode.Start.AbsoluteIndex - (lastNode.Start.LineIndex -1) + lastNode.Length;
        }
    }

    /// <summary>
    /// Type of control block code.
    /// </summary>
    public enum BlockCodeType
    {
        /// <summary>
        /// Block code is rendered in C#.
        /// </summary>
        CSharp = 0,
        /// <summary>
        /// Block code is rendered in VB.
        /// </summary>
        VB,
        /// <summary>
        /// Block code is a Html node.
        /// </summary>
        HTMLNode
    }
}
