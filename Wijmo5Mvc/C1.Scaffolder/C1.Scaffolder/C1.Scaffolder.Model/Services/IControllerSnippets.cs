﻿namespace C1.Web.Mvc.Services
{
    /// <summary>
    /// Represents the snippets collection for controller.
    /// </summary>
    public interface IControllerSnippets
    {
        /// <summary>
        /// Gets the name of the controller.
        /// </summary>
        string ControllerName { get; }
        /// <summary>
        /// Adds an import snippet.
        /// </summary>
        /// <param name="ns">The namespace to import.</param>
        void AddImport(string ns);
        /// <summary>
        /// Add a field to hold the web root from IHostingEnvironment.
        /// </summary>
        /// <remarks>
        /// Only for ASP.NET Core.
        /// </remarks>
        /// <param name="name">The default field name.</param>
        /// <returns>The real field name.</returns>
        string AddWebRoot(string name);
        /// <summary>
        /// Gets the name of the database context snippet.
        /// </summary>
        /// <param name="typeName">The name of the database context type.</param>
        /// <returns>The name of the field if existing, otherwise, null.</returns>
        string GetDbContextName(string typeName);
        /// <summary>
        /// Adds a database context declaration snippet.
        /// </summary>
        /// <param name="name">The default name of field.</param>
        /// <param name="typeName">The name of the database context type.</param>
        /// <returns>The real name of the field.</returns>
        string AddDbContext(string name, string typeName);
        /// <summary>
        /// Adds a viewbag item snippet.
        /// </summary>
        /// <param name="name">The default name of the viewbag item.</param>
        /// <param name="dbName">The field name of the database context.</param>
        /// <param name="entitySetName">The name of the entity set.</param>
        /// <param name="proxyCreationEnabled">The value of db's Configuration.ProxyCreationEnabled property.</param>
        /// <returns>The real name of the viewbag item.</returns>
        string AddViewBagItem(string name, string dbName, string entitySetName, bool proxyCreationEnabled = true);
        /// <summary>
        /// Adds an action snippet.
        /// </summary>
        /// <param name="content">The content of the action.</param>
        void AddAction(string content);
        /// <summary>
        /// Gets an unique member name.
        /// </summary>
        /// <param name="defaultName">The default name.</param>
        /// <returns>The unique member name.</returns>
        string GetMemberName(string defaultName);
        /// <summary>
        /// Gets an unique variable name.
        /// </summary>
        /// <param name="defaultName">The default name.</param>
        /// <returns>The unique variable name.</returns>
        string GetVariableName(string defaultName);
        /// <summary>
        /// Gets an unique view bag name.
        /// </summary>
        /// <param name="defaultName">The default name.</param>
        /// <returns>The unique view bag name.</returns>
        string GetViewBagName(string defaultName);
        /// <summary>
        /// Check whether the name is exist in list of members
        /// </summary>
        /// <param name="memberName">The member name</param>
        /// <returns>True if the name is exist.</returns>
        bool IsExistMemberName(string memberName);
    }
}
