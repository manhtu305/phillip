﻿//------------------------------------------------------------------------------
// <copyright file="UpdateMvcControlCommand.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.Design;
using Microsoft.VisualStudio.Shell;
namespace C1.Scaffolder
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class AsyncUpdateCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x101;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("dd4299f1-33ca-4dc8-901c-070e41eeed56");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly AsyncPackage package;

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncUpdateCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private AsyncUpdateCommand(AsyncPackage package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package");
            }

            this.package = package;            
        }

        C1ControlUpdater codeUpdater;
        private void UpdateCmdItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;
            codeUpdater = new C1ControlUpdater(AsyncPackage as Package);            
            if (null != menuCommand)
            {
                menuCommand.Enabled = codeUpdater.Control != null;
            }  
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static AsyncUpdateCommand Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private AsyncPackage AsyncPackage
        {
            get
            {
                return this.package;
            }
        }
        

    /// <summary>
    /// Initializes the instance of the command.
    /// </summary>
    /// <param name="asyncPackage"></param>
    public static async System.Threading.Tasks.Task InitinalizeAsync(AsyncPackage asyncPackage)
    {
        Instance = new AsyncUpdateCommand(asyncPackage);
        OleMenuCommandService commandService = await Instance.AsyncPackage.GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService;
        if (commandService != null)
        {
            var menuCommandID = new CommandID(CommandSet, CommandId);
            OleMenuCommand updateCmdItem = new OleMenuCommand(new EventHandler(Instance.MenuItemCallback), menuCommandID);
            updateCmdItem.BeforeQueryStatus += Instance.UpdateCmdItem_BeforeQueryStatus;
            commandService.AddCommand(updateCmdItem);
        }
        }

    /// <summary>
    /// This function is the callback used to execute the command when the menu item is clicked.
    /// See the constructor to see how the menu item is associated with this function using
    /// OleMenuCommandService service and MenuCommand class.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Event args.</param>
    private void MenuItemCallback(object sender, EventArgs e)
        {
            if (codeUpdater == null)
                codeUpdater = new C1ControlUpdater(AsyncPackage as Package);
            codeUpdater.ShowForm();
        }
    }
}
