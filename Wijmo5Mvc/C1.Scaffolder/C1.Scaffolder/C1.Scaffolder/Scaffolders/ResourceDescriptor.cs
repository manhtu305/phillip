﻿using System.Collections.Generic;

namespace C1.Scaffolder.Scaffolders
{
    internal class MetadataDescriptor
    {
        public MetadataDescriptor()
        {
            Namespaces = new List<string>();
            ControllerNamespaces = new List<string>();
            Scripts = new List<string>();
            Packages = new List<string>();
        }

        public IList<string> Namespaces { get; private set; }

        public IList<string> ControllerNamespaces { get; private set; }

        public IList<string> Scripts { get; private set; }

        public IList<string> Packages { get; private set; }
    }

    internal partial class ControlScaffolderBase
    {
        MetadataDescriptor descriptor;
        internal virtual MetadataDescriptor GetDescriptor()
        {
            if (descriptor == null)
            {
                descriptor = new MetadataDescriptor();
                
                descriptor.ControllerNamespaces.Add("System");
                descriptor.ControllerNamespaces.Add("System.Collections.Generic");
                descriptor.ControllerNamespaces.Add("System.Linq");
                descriptor.ControllerNamespaces.Add("C1.Web.Mvc");
                descriptor.ControllerNamespaces.Add("C1.Web.Mvc.Serialization");
                if (OptionsModel.IsAspNetCore)
                {
                    descriptor.Namespaces.Add("C1.AspNetCore.Mvc");
                    descriptor.Scripts.Add("<c1-basic-scripts />");

                    descriptor.ControllerNamespaces.Add("Microsoft.AspNetCore.Mvc");
                    descriptor.ControllerNamespaces.Add("Microsoft.EntityFrameworkCore");
                    descriptor.ControllerNamespaces.Add("System.Data.SqlClient");

                    descriptor.Packages.Add("C1.AspNetCore.Mvc");
                }
                else
                {
                    descriptor.Namespaces.Add("C1.Web.Mvc");
                    descriptor.Namespaces.Add("C1.Web.Mvc.Fluent");
                    descriptor.Scripts.Add("Basic()");

                    descriptor.ControllerNamespaces.Add("System.Data");
                    descriptor.ControllerNamespaces.Add("System.Data.Entity");
                    descriptor.ControllerNamespaces.Add("System.Data.Entity.Validation");
                    descriptor.ControllerNamespaces.Add("System.Web");
                    descriptor.ControllerNamespaces.Add("System.Web.Mvc");

                    descriptor.Packages.Add("C1.Web.Mvc");
                }
            }
            return descriptor;
        }
    }

    internal partial class FlexGridScaffolder
    {
        internal override MetadataDescriptor GetDescriptor()
        {
            MetadataDescriptor descriptor = base.GetDescriptor();
            if (!OptionsModel.IsAspNetCore)
                descriptor.Namespaces.Add("C1.Web.Mvc.Grid");
            return descriptor;
        }
    }

    internal partial class FlexSheetScaffolder
    {
        internal override MetadataDescriptor GetDescriptor()
        {
            MetadataDescriptor descriptor = base.GetDescriptor();
            descriptor.ControllerNamespaces.Add("C1.Web.Mvc.Sheet");
            descriptor.ControllerNamespaces.Add("System.IO"); 
            if (OptionsModel.IsAspNetCore)
            {
                descriptor.ControllerNamespaces.Add("Microsoft.AspNetCore.Hosting");
                descriptor.Namespaces.Add("C1.AspNetCore.Mvc.FlexSheet");
                descriptor.Scripts.Add("<c1-flex-sheet-scripts />");
                descriptor.Packages.Add("C1.AspNetCore.Mvc.FlexSheet");
            }
            else
            {
                descriptor.Namespaces.Add("C1.Web.Mvc.Grid");
                descriptor.Namespaces.Add("C1.Web.Mvc.Sheet");
                descriptor.Namespaces.Add("C1.Web.Mvc.Sheet.Fluent");

                descriptor.Packages.Add("C1.Web.Mvc.FlexSheet");
                descriptor.Scripts.Add("FlexSheet()");
            }
            return descriptor;
        }
    }

    internal partial class MultiRowScaffolder
    {
        internal override MetadataDescriptor GetDescriptor()
        { 
            MetadataDescriptor descriptor = base.GetDescriptor();
            if (OptionsModel.IsAspNetCore)
            {
                descriptor.Namespaces.Add("C1.AspNetCore.Mvc.MultiRow");
                descriptor.Scripts.Add("<c1-multi-row-scripts />");
                descriptor.Packages.Add("C1.AspNetCore.Mvc.MultiRow");
            }
            else
            {
                descriptor.Namespaces.Add("C1.Web.Mvc.MultiRow");
                descriptor.Namespaces.Add("C1.Web.Mvc.MultiRow.Fluent");

                descriptor.Packages.Add("C1.Web.Mvc.MultiRow");
                descriptor.Scripts.Add("MultiRow()");
            }
            return descriptor;
        }
    }

    internal partial class PivotScaffolder
    {
        internal override MetadataDescriptor GetDescriptor()
        {
            MetadataDescriptor descriptor = base.GetDescriptor();
            if (OptionsModel.IsAspNetCore)
            {
                descriptor.Namespaces.Add("C1.AspNetCore.Mvc.Olap");
                descriptor.Scripts.Add("<c1-olap-scripts />");
                descriptor.Packages.Add("C1.AspNetCore.Mvc.Olap");
            }
            else
            {
                descriptor.Namespaces.Add("C1.Web.Mvc.Olap");
                descriptor.Namespaces.Add("C1.Web.Mvc.Olap.Fluent");

                descriptor.Packages.Add("C1.Web.Mvc.Olap");
                descriptor.Scripts.Add("Olap()");
            }
            return descriptor;
        }
    }

    internal partial class PivotGridScaffolder
    {
        internal override MetadataDescriptor GetDescriptor()
        {
            MetadataDescriptor descriptor = base.GetDescriptor();
            
            if (OptionsModel.IsAspNetCore)
            {
                descriptor.Namespaces.Add("C1.AspNetCore.Mvc.Olap");
                descriptor.Scripts.Add("<c1-olap-scripts />");
                descriptor.Packages.Add("C1.AspNetCore.Mvc.Olap");
            }
            else
            {
                descriptor.Namespaces.Add("C1.Web.Mvc.Olap");
                descriptor.Namespaces.Add("C1.Web.Mvc.Olap.Fluent");

                descriptor.Packages.Add("C1.Web.Mvc.Olap");
                descriptor.Scripts.Add("Olap()");
            }
            return descriptor;
        }
    }

    internal partial class ChartBaseScaffolder
    {
        internal override MetadataDescriptor GetDescriptor()
        {
            MetadataDescriptor descriptor = base.GetDescriptor();
            descriptor.ControllerNamespaces.Add("System.IO");

            if (OptionsModel.IsAspNetCore)
            {
                descriptor.ControllerNamespaces.Add("Microsoft.AspNetCore.Hosting");
            }
            else
            {
                descriptor.Namespaces.Add("C1.Web.Mvc.Chart");
                descriptor.Namespaces.Add("System.Drawing");
            }
            return descriptor;
        }
    }

    internal partial class PivotChartScaffolder
    {
        internal override MetadataDescriptor GetDescriptor()
        {
            MetadataDescriptor descriptor = base.GetDescriptor();
            descriptor.ControllerNamespaces.Add("System.IO");

            if (OptionsModel.IsAspNetCore)
            {
                descriptor.ControllerNamespaces.Add("Microsoft.AspNetCore.Hosting");
            }
            else
            {
                descriptor.Namespaces.Add("C1.Web.Mvc.Chart");
                descriptor.Namespaces.Add("System.Drawing");
            }

            return descriptor;
        }
    }
}
