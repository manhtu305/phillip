﻿using C1.Scaffolder.Models.Olap;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder.Scaffolders
{
    internal partial class PivotGridScaffolder : FlexGridScaffolder
    {
        private readonly PivotGridOptionsModel _pivotGridOptionsModel;

        public PivotGridScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new PivotGridOptionsModel(serviceProvider, new PivotGrid()))
        {

        }

        public PivotGridScaffolder(IServiceProvider serviceProvider, PivotGridOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _pivotGridOptionsModel = optionsModel;
        }

        public PivotGridScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new PivotGridOptionsModel(serviceProvider, new PivotGrid(), settings))
        {

        }
    }
}
