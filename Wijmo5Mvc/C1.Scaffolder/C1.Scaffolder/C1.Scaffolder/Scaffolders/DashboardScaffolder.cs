﻿using System;
using C1.Web.Mvc;
using C1.Scaffolder.Models.Dashboard;

namespace C1.Scaffolder.Scaffolders
{
    internal class DashboardScaffolder : ControlScaffolderBase
    {
        private readonly DashboardOptionsModel _dashboardOptionsModel;

        public DashboardScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new DashboardOptionsModel(serviceProvider))
        {
        }

        public DashboardScaffolder(IServiceProvider serviceProvider, DashboardOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _dashboardOptionsModel = optionsModel;
        }
        public DashboardScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this (serviceProvider, new DashboardOptionsModel(serviceProvider, settings))
        {

        }
    }
}
