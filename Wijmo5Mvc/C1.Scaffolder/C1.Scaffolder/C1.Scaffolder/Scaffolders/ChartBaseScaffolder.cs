﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Scaffolder.Models.Chart;

namespace C1.Scaffolder.Scaffolders
{
    internal partial class ChartBaseScaffolder : DataBoundScaffolder
    {
        private readonly ChartBaseOptionsModel _chartBaseOptionsModel;

        public ChartBaseScaffolder(IServiceProvider serviceProvider, ChartBaseOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _chartBaseOptionsModel = optionsModel;
        }

    }
}
