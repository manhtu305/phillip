﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C1.Scaffolder.Models;
using C1.Scaffolder.Models.Olap;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;

namespace C1.Scaffolder.Scaffolders
{
    internal partial class PivotScaffolder : ControlScaffolderBase
    {
        public PivotScaffolder(IServiceProvider serviceProvider, ControlOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
        }

    }

    internal partial class PivotEngineScaffolder : PivotScaffolder
    {
        private readonly PivotEngineOptionsModel _pivotEngineOptionsModel;

        public PivotEngineScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new PivotEngineOptionsModel(serviceProvider, new PivotEngine()))
        {

        }

        public PivotEngineScaffolder(IServiceProvider serviceProvider, PivotEngineOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _pivotEngineOptionsModel = optionsModel;
        }

        public PivotEngineScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new PivotEngineOptionsModel(serviceProvider, new PivotEngine(), settings))
        {

        }

        protected override Dictionary<string, object> GetTemplateParameters()
        {
            //Because ReadActionUrl and ServiceUrl will be overriden by CubeService initialization (inside PivotEngine control)
            //we need to backup these values.
            PivotEngine pivotEngine = OptionsModel.Component as PivotEngine;
            string serviceUrl = pivotEngine.ServiceUrl ?? null;
            string readUrlAction = pivotEngine.ItemsSource.ReadActionUrl ?? null;

            var parameters = base.GetTemplateParameters();

            if (!string.IsNullOrEmpty(serviceUrl))
            {
                pivotEngine.ServiceUrl = serviceUrl;
            }

            if (!string.IsNullOrEmpty(readUrlAction))
            {
                pivotEngine.ItemsSource.ReadActionUrl = readUrlAction;
            }

            var dbService = ServiceManager.DefaultDbContextProvider;
            if (dbService != null)
            {
                parameters.Add("DbContextTypeName",
                    dbService.DbContextType == null
                        ? string.Empty
                        : dbService.DbContextType.TypeName);
                parameters.Add("ViewDataTypeName",
                    dbService.ModelType == null ? string.Empty : dbService.ModelType.TypeName);
                parameters.Add("ModelTypeName",
                    dbService.ModelType == null
                        ? string.Empty
                        : dbService.ModelType.ShortTypeName);
                parameters.Add("PrimaryKeys", dbService.PrimaryKeys);
            }

            return parameters;
        }
    }
}
