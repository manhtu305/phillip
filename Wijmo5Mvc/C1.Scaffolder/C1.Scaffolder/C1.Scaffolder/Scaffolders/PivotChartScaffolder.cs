﻿using C1.Scaffolder.Models.Olap;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder.Scaffolders
{
    internal partial class PivotChartScaffolder : PivotScaffolder
    {
        private readonly PivotChartOptionsModel _pivotChartOptionsModel;

        public PivotChartScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new PivotChartOptionsModel(serviceProvider, new PivotChart()))
        {
        }

        public PivotChartScaffolder(IServiceProvider serviceProvider, PivotChartOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _pivotChartOptionsModel = optionsModel;
        }

        public PivotChartScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this(serviceProvider, new PivotChartOptionsModel(serviceProvider, new PivotChart(), settings))
        {
        }

    }
}
