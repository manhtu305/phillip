﻿using System;
using C1.Scaffolder.Models.Chart;
using C1.Web.Mvc;

namespace C1.Scaffolder.Scaffolders
{
    class FlexChartScaffolder : ChartBaseScaffolder
    {
        public FlexChartScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new FlexChartOptionsModel(serviceProvider))
        {
        }

        public FlexChartScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new FlexChartOptionsModel(serviceProvider, controlSettings))
        {
        }

        public FlexChartScaffolder(IServiceProvider serviceProvider, ChartCoreOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
        }
    }
}
