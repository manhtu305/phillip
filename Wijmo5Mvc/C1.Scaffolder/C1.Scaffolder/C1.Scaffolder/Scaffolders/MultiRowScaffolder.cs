﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Scaffolder.Models.MultiRow;
using C1.Web.Mvc;

namespace C1.Scaffolder.Scaffolders
{
    internal partial class MultiRowScaffolder : FlexGridScaffolder
    {
        public MultiRowScaffolder(IServiceProvider serviceProvider)
            : base(serviceProvider, new MultiRowOptionsModel(serviceProvider))
        {
        }

        public MultiRowScaffolder(IServiceProvider serviceProvider, MVCControl settings) 
            : base(serviceProvider, new MultiRowOptionsModel(serviceProvider, settings))
        {
        }
    }
}
