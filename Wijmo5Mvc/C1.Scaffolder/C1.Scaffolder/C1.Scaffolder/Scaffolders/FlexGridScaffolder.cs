﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Scaffolder.Models.Grid;
using C1.Web.Mvc;
using C1.Web.Mvc.Serialization;
using C1.Web.Mvc.Services;

namespace C1.Scaffolder.Scaffolders
{
    internal partial class FlexGridScaffolder : DataBoundScaffolder
    {
        private readonly FlexGridOptionsModel _flexGridOptionsModel;

        public FlexGridScaffolder(IServiceProvider serviceProvider)
            : this(serviceProvider, new FlexGridOptionsModel(serviceProvider))
        {
        }

        public FlexGridScaffolder(IServiceProvider serviceProvider, FlexGridOptionsModel optionsModel)
            : base(serviceProvider, optionsModel)
        {
            _flexGridOptionsModel = optionsModel;
        }
        public FlexGridScaffolder(IServiceProvider serviceProvider, MVCControl settings)
            : this (serviceProvider, new FlexGridOptionsModel(serviceProvider, settings))
        {

        }

        public override void AfterSerializeComponent()
        {
            base.AfterSerializeComponent();

            if (_flexGridOptionsModel.Grid.AllowPaging)
            {
                var viewSnippets = ServiceProvider.GetService(typeof(IViewSnippets)) as IViewSnippets;
                if (viewSnippets != null)
                {
                    var pager = new Pager {Owner = _flexGridOptionsModel.Grid.Id};
                    var pagerHtml = ViewSerializer.SerializeComponent(pager, ServiceManager.Instance, null);
                    viewSnippets.AddComponent(pagerHtml);
                }
            }
        }
    }
}
