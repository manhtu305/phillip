﻿using System;
using C1.Scaffolder.Models.Chart;
using C1.Web.Mvc;

namespace C1.Scaffolder.Scaffolders
{
    class FlexRadarScaffolder : FlexChartScaffolder
    {

        public FlexRadarScaffolder(IServiceProvider serviceProvider)
            : base(serviceProvider, new FlexRadarOptionsModel(serviceProvider))
        {
        }

        public FlexRadarScaffolder(IServiceProvider serviceProvider, MVCControl controlSettings)
            : base(serviceProvider, new FlexRadarOptionsModel(serviceProvider, controlSettings))
        {
        }
    }
}
