﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using C1.Scaffolder.Scaffolders;
using C1.Scaffolder.VS;
using C1.Web.Mvc;
using C1.Web.Mvc.Services;
using EnvDTE;
using Microsoft.AspNet.Scaffolding;

namespace C1.Scaffolder.Models
{
    internal abstract class ComponentOptionsModel
    {
        private const string ControllerSuffix = "Controller";
        private const string PageModelSuffix = "Model";

        private OptionsCategory[] _categoryPages;
        private bool _isValid;
        private string _controllerName;
        private string _viewName;
        private IEnumerable<IControllerDescriptor> _controllers;
        private IControllerDescriptor _selectedController;
        private IActionDescriptor _selectedAction;
        private bool _isSupportEF = true;
        private bool _isBoundMode;//in bound mode, control required setting ModelType, datacontext

        protected ComponentOptionsModel(IServiceProvider serviceProvider, Component component)
        {
            ServiceProvider = serviceProvider;
            Context = serviceProvider.GetService(typeof(CodeGenerationContext)) as CodeGenerationContext;
            Project = serviceProvider.GetService(typeof(IMvcProject)) as MvcProject;

            var scaffolderDescriptor = serviceProvider.GetService(typeof (IScaffolderDescriptor)) as ScaffolderDescriptor;
            IsInsertOnCodePage = scaffolderDescriptor.IsInsert;
            IsUpdate = scaffolderDescriptor.IsUpdate;

            // find default controller and view action.
            if (IsInsertOnCodePage)
            {
                IActionDescriptor action;
                SelectedController = Project.GetController(Project.ActiveItem.ViewDescriptor, out action);
                SelectedAction = action;
            }

            Component = component;

            component.BindingInfoChanged += OnBindingInfoChanged;
            component.InitControl();

            ViewName = "Index";
            _isValid = GetIsValid();
            _isSupportEF =  Services.DbContextProvider.IsProjectSupportEF(Project, serviceProvider);
        }

        public bool IsProjectSupportEF
        {
            get { return _isSupportEF; }
            private set { }
        }

        public bool IsBoundMode
        {
            get { return _isBoundMode; }
            set
            {
                if (_isBoundMode != value)
                {
                    _isBoundMode = value;
                    OnBoundModeChanged(EventArgs.Empty);
                }
                IsValid = GetIsValid();
            }
        }

        public event EventHandler BoundModeChanged;
        private void OnBoundModeChanged(EventArgs eArgs)
        {
            if (BoundModeChanged != null)
                BoundModeChanged(this, eArgs);
        }

        public IEnumerable<IControllerDescriptor> Controllers
        {
            get
            {
                if (_controllers == null)
                {
                    var activeItem = Project.ActiveItem;
                    if (activeItem != null)
                    {
                        var view = activeItem.ViewDescriptor;
                        if (view != null)
                        {
                            _controllers = Project.GetControllerDescriptors(view.AreaName);
                        }
                    }

                    if (_controllers == null)
                    {
                        _controllers = Enumerable.Empty<ControllerDescriptor>();
                    }
                }

                return _controllers;
            }
        }

        public event EventHandler SelectedControllerChanged;

        private void OnSelectedControllerChanged(EventArgs eArgs)
        {
            if (SelectedControllerChanged != null)
            {
                SelectedControllerChanged(this, eArgs);
            }
        }

        public IControllerDescriptor SelectedController
        {
            get { return _selectedController; }
            set
            {
                if (_selectedController == value) return;

                _selectedController = value;
                IsValid = GetIsValid();
            }
        }

        public event EventHandler SelectedActionChanged;

        private void OnSelectedActionChanged(EventArgs eArgs)
        {
            if (SelectedActionChanged != null)
            {
                SelectedActionChanged(this, eArgs);
            }
        }

        public IActionDescriptor SelectedAction
        {
            get
            {
                return _selectedAction;
            }
            set
            {
                if (_selectedAction == value) return;

                _selectedAction = value;
                OnSelectedActionChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets the MVC component.
        /// </summary>
        public Component Component { get; private set; }

        public bool IsInsertOnCodePage { get; private set; }
        public bool IsUpdate { get; private set; }

        private void OnBindingInfoChanged(object sender, EventArgs e)
        {
            IsValid = GetIsValid();
        }

        /// <summary>
        /// Gets the display name of the scaffolded control.
        /// </summary>
        [IgnoreTemplateParameter]
        public virtual string ControlName
        {
            get
            {
                return "ComponentOne ASP.NET MVC Control";
            }
        }

        /// <summary>
        /// Gets the options dialog title for the scaffolded control.
        /// </summary>
        [IgnoreTemplateParameter]
        public string Title
        {
            get
            {
                var title = ControlName;
                if (IsRazorPages)
                {
                    title += " (" + Localization.Resources.RazorPages + ")";
                }
                return title;
            }
        }

        #region Valid

        /// <summary>
        /// Occurs when the <see cref="IsValid"/> property value is changed.
        /// </summary>
        public event EventHandler IsValidChanged;

        /// <summary>
        /// Gets whether the settings of options model is valid of not.
        /// </summary>
        [IgnoreTemplateParameter]
        public virtual bool IsValid
        {
            get
            {
                return _isValid;
            }
            protected set
            {
                if (value == _isValid)
                {
                    return;
                }

                _isValid = value;
                OnIsValidChanged();
            }
        }

        protected virtual void OnIsValidChanged()
        {
            if (IsValidChanged != null)
            {
                IsValidChanged(this, new EventArgs());
            }
        }

        #endregion

        internal IServiceProvider ServiceProvider { get; private set; }

        /// <summary>
        /// Gets the context for code generation.
        /// </summary>
        [IgnoreTemplateParameter]
        public CodeGenerationContext Context { get; private set; }

        [IgnoreTemplateParameter]
        public MvcProject Project { get; private set; }

        /// <summary>
        /// Gets or sets the name of the controller will be added.
        /// </summary>
        [IgnoreTemplateParameter]
        public string ControllerName
        {
            get
            {
                return _controllerName;
            }
            set
            {
                if (_controllerName == value)
                {
                    return;
                }

                _controllerName = value;
                IsValid = GetIsValid();
            }
        }

        /// <summary>
        /// Gets or sets the name of the view will be added.
        /// </summary>
        [IgnoreTemplateParameter]
        public string ViewName
        {
            get
            {
                return _viewName;
            }
            set
            {
                if (_viewName == value)
                {
                    return;
                }

                _viewName = value;
                IsValid = GetIsValid();
            }
        }

        /// <summary>
        /// Gets the array of the category pages will be shown in the wizard.
        /// </summary>
        [IgnoreTemplateParameter]
        public OptionsCategory[] CategoryPages { get { return _categoryPages ?? (_categoryPages = CreateCategoryPages()); } }

        /// <summary>
        /// Gets a value indicating whether the project is a ASP.NET Core project.
        /// </summary>
        [IgnoreTemplateParameter]
        public bool IsAspNetCore { get { return Project.IsAspNetCore; } }

        [IgnoreTemplateParameter]
        public bool IsRazorPages { get { return Project.IsRazorPages; } }

        /// <summary>
        /// Gets a value indicating whether should add the generic parameter to the control builder.
        /// </summary>
        [IgnoreTemplateParameter]
        public virtual bool ShouldSerializeGenericParameter
        {
            get { return true; }
        }

        protected abstract OptionsCategory[] CreateCategoryPages();

        /// <summary>
        /// Checks whether the controller exists in the project.
        /// </summary>
        /// <returns></returns>
        public bool ControllerExist()
        {
            return ControllerExist(ControllerName, false);
        }

        private bool ControllerExist(string controllerName, bool checkView = true)
        {
            var areaName = Project.SelectedAreaName;
            ProjectItems items = Project.Source.ProjectItems;
            if (!string.IsNullOrEmpty(areaName))
            {
                items = items.GetProjectItem(string.Format("Areas\\{0}\\", Project.SelectedAreaName)).ProjectItems;
            }

            var controllersFolder = Project.IsRazorPages ? Path.Combine("Pages", Project.SelectedPageFolder) : "Controllers";
            var controllers = MvcProject.FindProjectItem(Project.GetProjectItems(items), controllersFolder.Split('/', '\\'));
            var controllerItemName = GetControllerItemName(controllerName);
            if (controllers != null && controllers.Source.ProjectItems.Cast<ProjectItem>()
                .Any(item => string.Equals(item.Name, controllerItemName,
                    StringComparison.OrdinalIgnoreCase)))
            {
                return true;
            }

            if (!checkView) return false;

            var viewsFolder = Project.IsRazorPages ? Path.Combine("Pages", Project.SelectedPageFolder) : "Views";
            var views = MvcProject.FindProjectItem(Project.GetProjectItems(items), viewsFolder.Split('/', '\\'));
            var viewItemName = GetViewItemName(controllerName);
            if(views != null && views.Source.ProjectItems.Cast<ProjectItem>()
                .Any(item => string.Equals(item.Name, viewItemName, StringComparison.OrdinalIgnoreCase)))
            {
                return true;
            }

            return false;
        }

        protected virtual string GetControllerItemName(string controllerName)
        {
            var shortName = GetControllerShortName(controllerName);
            var controllerFileExtension = Project.IsCs ? ".cs" : ".vb";
            var controllerItemName = IsRazorPages
                ? shortName + controllerFileExtension + "html"
                : controllerName;
            controllerItemName += controllerFileExtension;

            return controllerItemName;
        }

        protected virtual string GetViewItemName(string controllerName)
        {
            var shortName = GetControllerShortName(controllerName);
            var controllerFileExtension = Project.IsCs ? ".cs" : ".vb";
            var viewItemName = IsRazorPages
                ? shortName + controllerFileExtension + "html"
                : shortName;

            return viewItemName;
        }

        /// <summary>
        /// Gets the default controller name. 
        /// </summary>
        /// <remarks>Add suffix-number for controller name if it exist.</remarks>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetDefaultControllerName(string name)
        {
            int controllerIndex = 0;
            var suffix = Project.IsRazorPages ? PageModelSuffix : ControllerSuffix;

            string newName = name + suffix;
            while (ControllerExist(newName))
            {
                controllerIndex++;
                newName = name + controllerIndex + suffix;
            }

            return newName;
        }

        internal string GetControllerShortName(string name)
        {
            if (string.IsNullOrEmpty(name)) return string.Empty;

            var suffix = Project.IsRazorPages ? PageModelSuffix : ControllerSuffix;
            return name.EndsWith(suffix)
                ? name.Remove(name.Length - suffix.Length, suffix.Length)
                : name;
        }

        protected virtual bool GetIsValid()
        {
            return IsInsertOnCodePage
                ? (SelectedController != null && SelectedAction != null)
                : (!string.IsNullOrEmpty(ControllerName) && !string.IsNullOrEmpty(ViewName));
        }
    }
}
