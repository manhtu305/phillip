﻿using System;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;
using System.Collections.Generic;
using System.IO;
using MvcInputs = C1.Web.Mvc.Inputs;

namespace C1.Scaffolder.Models.Inputs
{
    internal class InputsOptionsModel : DataBoundOptionsModel
    {
        public InputsOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new MvcInputs())
        {
        }

        public InputsOptionsModel(IServiceProvider serviceProvider, MvcInputs inputs)
            : this(serviceProvider, inputs, null)
        {
            //Inputs = inputs;
            //ControllerName = GetDefaultControllerName("Input");
        }

        public InputsOptionsModel(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new MvcInputs(), controlSettings)
        {
        }

        public InputsOptionsModel(IServiceProvider serviceProvider, MvcInputs inputs, MVCControl controlSettings)
            : base(serviceProvider, inputs, controlSettings)
        {
            Inputs = inputs;
            ControllerName = GetDefaultControllerName("Input");
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Inputs", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Fields, Path.Combine("Inputs", "Fields.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }

        [IgnoreTemplateParameter]
        public MvcInputs Inputs { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.InputModelName; }
        }

        protected override bool GetIsValid()
        {
            return !IsBoundMode || !IsProjectSupportEF || (!string.IsNullOrEmpty(ControllerName) && Inputs.IsBound);
        }

        protected override string GetControllerItemName(string controllerName)
        {
            if (!Project.IsRazorPages)
            {
                return base.GetControllerItemName(controllerName);
            }

            // for Inputs, put 5 pages in the new folder
            var shortName = GetControllerShortName(controllerName);
            return shortName;
        }

        protected override string GetViewItemName(string controllerName)
        {
            if (!Project.IsRazorPages)
            {
                return base.GetViewItemName(controllerName);
            }

            // for Inputs, put 5 pages in the new folder
            var shortName = GetControllerShortName(controllerName);
            return shortName;
        }
    }
}
