﻿using C1.Scaffolder.Localization;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using C1.Web.Mvc.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Scaffolder.Models.Olap
{
    internal class PivotEngineOptionsModel : ControlOptionsModel
    {
        private static int _counter;
        internal PivotEngineOptionsModel(IServiceProvider serviceProvider, PivotEngine pivotEngine)
            : base(serviceProvider, pivotEngine)
        {
            Pivot = pivotEngine;
            Pivot.Id = string.Format("pivotEngine{0}", ++_counter);
            ControllerName = GetDefaultControllerName("PivotEngine");
            InitFieldsItems();
        }

        internal PivotEngineOptionsModel(IServiceProvider serviceProvider, PivotEngine pivotEngine, MVCControl settings)
            : base(serviceProvider, pivotEngine)
        {
            Pivot = pivotEngine;
            Pivot.ParsedSetting = settings;
            ControllerName = GetDefaultControllerName("PivotEngine");
            InitFieldsItems();

            if (settings != null)
            {
                InitControlValues(settings);
            }
        }

        private void InitFieldsItems()
        {
            Fields = new ObservableCollection<PivotFieldBase>();
            Pivot.RowFields.Items = new ObservableCollection<PivotFieldBase>();
            Pivot.ColumnFields.Items = new ObservableCollection<PivotFieldBase>();
            Pivot.ValueFields.Items = new ObservableCollection<PivotFieldBase>();
            Pivot.FilterFields.Items = new ObservableCollection<PivotFieldBase>();
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            return new OptionsCategory[]
            {
                new OptionsCategory(Resources.PivotEngineOptionsTab_General,
                    Path.Combine("PivotEngine", "General.xaml")),
                new OptionsCategory(Resources.PivotEngineOptionsTab_ClientEvents,
                    Path.Combine("PivotEngine", "ClientEvents.xaml")),
                //new OptionsCategory(Resources.PivotEngineOptionsTab_Fields,
                //    Path.Combine("PivotEngine", "Fields.xaml")),
                new OptionsCategory(Resources.FlexGridOptionsTab_DataBinding,
                    Path.Combine("PivotEngine", "DataBinding.xaml"))
            };
        }

        [IgnoreTemplateParameter]
        public PivotEngine Pivot { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get
            {
                return Resources.PivotEngineModelName;
            }
        }

        [IgnoreTemplateParameter]
        public IEnumerable<IModelTypeDescription> ModelTypes
        {
            get { return ServiceManager.DbContextProvider.ModelTypes; }
        }

        [IgnoreTemplateParameter]
        public IEnumerable<IModelTypeDescription> DbContextTypes
        {
            get { return ServiceManager.DbContextProvider.DbContextTypes; }
        }

        /// <summary>
        /// ModelType.Properties to IList<PivotFieldBase> mapping.
        /// </summary>
        [IgnoreTemplateParameter]
        public ObservableCollection<PivotFieldBase> Fields { get; set; }

        /// <summary>
        /// Only enable ReadActionUrl when the ModelType is selected and Project supports EF
        /// </summary>
        public bool ShouldEnableReadActionUrl
        {
            get
            {
                if (IsUpdate)
                {
                    return true;
                }

                return Pivot.ModelType != null && IsProjectSupportEF;
            }
        }

        /// <summary>
        /// "Add" button will be enabled when both ReadActionURL and DbContextType are filled or cleared.
        /// </summary>
        protected override bool GetIsValid()
        {
            if (IsUpdate)
            {
                return base.GetIsValid();
            }

            //defaut = true
            if(Pivot == null || Pivot.ItemsSource == null)
            {
                return true;
            }

            return !(string.IsNullOrEmpty(Pivot.ItemsSource.ReadActionUrl) ^ (Pivot.DbContextType == null));
        }
    }
}
