﻿using System;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;
using System.Collections.Generic;
using System.IO;

namespace C1.Scaffolder.Models.Chart
{
    internal abstract class ChartBaseOptionsModel : DataBoundOptionsModel
    {
        protected ChartBaseOptionsModel(IServiceProvider serviceProvider, FlexChartBase<object> chart)
            : base(serviceProvider, chart)
        {
            Chart = chart;
        }

        public ChartBaseOptionsModel(IServiceProvider serviceProvider, FlexChartBase<object> chart, MVCControl controlSettings) 
            : base(serviceProvider, chart, controlSettings)
        {
            Chart = chart;
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.FlexChartOptionsTab_General,
                    Path.Combine("FlexChart", "General.xaml")),
                new OptionsCategory(Resources.FlexChartOptionsTab_Appearance,
                    Path.Combine("FlexChart", "Appearance.xaml"))
            };

            return categories.ToArray();
        }

        [IgnoreTemplateParameter]
        public FlexChartBase<object> Chart { get; private set; }
    }
}
