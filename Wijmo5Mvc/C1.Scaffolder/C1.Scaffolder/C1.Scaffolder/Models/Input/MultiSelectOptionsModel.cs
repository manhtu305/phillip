﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal class MultiSelectOptionsModel : ComboBoxOptionsModel
    {
        private static int _counter = 1;

        public MultiSelectOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new MultiSelect<object>())
        {
        }

        public MultiSelectOptionsModel(IServiceProvider serviceProvider, MultiSelect<object> multiSelect)
            : this(serviceProvider, multiSelect, null)
        {
            //MultiSelect = multiSelect;

            multiSelect.Id = "multiSelect";
            if (IsInsertOnCodePage) multiSelect.Id += _counter++;
        }

        public MultiSelectOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            :this (serviceProvider, new MultiSelect<object>(), settings)
        {
        }

        public MultiSelectOptionsModel(IServiceProvider serviceProvider, MultiSelect<object> multiSelect, MVCControl settings)
            : base(serviceProvider, multiSelect, settings)
        {
            MultiSelect = multiSelect;
        }

        [IgnoreTemplateParameter]
        public MultiSelect<object> MultiSelect { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.MultiSelectModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "MultiSelect", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "MultiSelect", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ClientEvents,
                    Path.Combine("Controls", "ClientEvents.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "MultiSelect", "Misc.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }
    }
}
