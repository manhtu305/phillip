﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal class AutoCompleteOptionsModel : ComboBoxOptionsModel
    {
        private static int _counter = 1;

        public AutoCompleteOptionsModel(IServiceProvider serviceProvider)
            : this(serviceProvider, new AutoComplete<object>())
        {
        }

        public AutoCompleteOptionsModel(IServiceProvider serviceProvider, AutoCompleteBase<object> autoComplete)
            : this(serviceProvider, autoComplete, null)
        {
            //AutoComplete = autoComplete;

            autoComplete.Id = "autoComplete";
            if (IsInsertOnCodePage) autoComplete.Id += _counter++;
        }

        public AutoCompleteOptionsModel(IServiceProvider serviceProvider, MVCControl settings) 
            : this(serviceProvider, new AutoComplete<object>(), settings)
        {
        }
        public AutoCompleteOptionsModel(IServiceProvider serviceProvider, AutoCompleteBase<object> autoComplete, MVCControl settings) 
            : base(serviceProvider, autoComplete, settings)
        {
            AutoComplete = autoComplete;
        }

        [IgnoreTemplateParameter]
        public AutoCompleteBase<object> AutoComplete { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.AutoCompleteModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "AutoComplete", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "ComboBox", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ClientEvents,
                    Path.Combine("Controls", "ClientEvents.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "AutoComplete", "Misc.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }
    }
}
