﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal class InputDateOptionsModel : DropDownOptionsModel
    {
        private static int _counter = 1;

        public InputDateOptionsModel(IServiceProvider serviceProvider)
            :this(serviceProvider, new InputDate())
        {
        }

        public InputDateOptionsModel(IServiceProvider serviceProvider, InputDateBase inputDate)
            : this(serviceProvider, inputDate, null)
        {
            //InputDate = inputDate;

            inputDate.Id = "inputDate";
            if (IsInsertOnCodePage) inputDate.Id += _counter++;
        }
        public InputDateOptionsModel(IServiceProvider serviceProvider, MVCControl controlSettings)
            : this(serviceProvider, new InputDate(), controlSettings)
        {
        }

        public InputDateOptionsModel(IServiceProvider serviceProvider, InputDateBase inputDate, MVCControl controlSettings) 
            : base(serviceProvider, inputDate, controlSettings)
        {
            InputDate = inputDate;
        }



        [IgnoreTemplateParameter]
        public InputDateBase InputDate { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.InputDateModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "InputDate", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ClientEvents,
                    Path.Combine("Controls", "ClientEvents.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "InputDate", "Misc.xaml")),
            };

            return categories.ToArray();
        }
    }
}
