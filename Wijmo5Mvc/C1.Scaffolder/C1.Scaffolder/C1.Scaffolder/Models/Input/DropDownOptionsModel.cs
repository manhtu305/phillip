﻿using System;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal abstract class DropDownOptionsModel : FormInputOptionsModel
    {
        protected DropDownOptionsModel(IServiceProvider serviceProvider, DropDown dropDown)
            : this(serviceProvider, dropDown, null)
        {
            //DropDown = dropDown;
        }

        public DropDownOptionsModel(IServiceProvider serviceProvider, DropDown dropDown, MVCControl controlSettings)
            : base(serviceProvider, dropDown, controlSettings)
        {
            DropDown = dropDown;
        }

        [IgnoreTemplateParameter]
        public DropDown DropDown { get; private set; }
    }
}
