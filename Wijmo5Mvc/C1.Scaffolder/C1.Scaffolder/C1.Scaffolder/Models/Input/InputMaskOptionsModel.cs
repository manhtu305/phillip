﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    class InputMaskOptionsModel : FormInputOptionsModel
    {
        private static int _counter = 1;

        public InputMaskOptionsModel(IServiceProvider serviceProvider)
            :this(serviceProvider, new InputMask())
        {
        }

        public InputMaskOptionsModel(IServiceProvider serviceProvider, InputMask inputMask)
            : this(serviceProvider, inputMask, null)
        {
            //InputMask = inputMask;

            inputMask.Id = "inputMask";
            if (IsInsertOnCodePage) inputMask.Id += _counter++;
        }

        public InputMaskOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            :this (serviceProvider, new InputMask(), settings)
        {
        }

        public InputMaskOptionsModel(IServiceProvider serviceProvider, InputMask inputMask, MVCControl settings)
            :base (serviceProvider, inputMask, settings)
        {
            InputMask = inputMask;
        }

        [IgnoreTemplateParameter]
        public InputMask InputMask { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.InputMaskModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "InputMask", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "InputMask", "Misc.xaml")),
            };

            return categories.ToArray();
        }
    }
}
