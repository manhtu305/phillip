﻿using System;
using System.Collections.Generic;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal abstract class FormInputOptionsModel : ControlOptionsModel
    {
        protected FormInputOptionsModel(IServiceProvider serviceProvider, FormInputBase input)
            : this(serviceProvider, input, null)
        {
        }


        protected FormInputOptionsModel(IServiceProvider serviceProvider, FormInputBase input, MVCControl settings)
            : base (serviceProvider, input)
        {
            Input = input;
            if (settings != null)
                InitControlValues(settings);
        }

        [IgnoreTemplateParameter]
        public FormInputBase Input { get; private set; }

        [IgnoreTemplateParameter]
        public IEnumerable<IModelTypeDescription> ModelTypes
        {
            get { return ServiceManager.DbContextProvider.ModelTypes; }
        }
    }
}
