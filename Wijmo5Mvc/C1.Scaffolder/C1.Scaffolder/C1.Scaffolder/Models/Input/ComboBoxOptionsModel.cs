﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Scaffolder.Localization;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models.Input
{
    internal class ComboBoxOptionsModel : DropDownBoundOptionsModel
    {
        private static int _counter = 1;

        public ComboBoxOptionsModel(IServiceProvider serviceProvider)
            :this(serviceProvider, new ComboBox<object>())
        {
        }

        public ComboBoxOptionsModel(IServiceProvider serviceProvider, ComboBoxBase<object> comboBox)
            :this(serviceProvider, comboBox, null)
        {
            //ComboBox = comboBox;

            comboBox.Id = "comboBox";
            if (IsInsertOnCodePage) comboBox.Id += _counter++;
        }
        public ComboBoxOptionsModel(IServiceProvider serviceProvider, MVCControl settings)
            :this (serviceProvider, new ComboBox<object>(), settings)
        {
        }
        public ComboBoxOptionsModel(IServiceProvider serviceProvider, ComboBoxBase<object> comboBox, MVCControl settings)
            :base(serviceProvider, comboBox, settings)
        {
            ComboBox = comboBox;
        }
        [IgnoreTemplateParameter]
        public ComboBoxBase<object> ComboBox { get; private set; }

        [IgnoreTemplateParameter]
        public override string ControlName
        {
            get { return Resources.ComboBoxModelName; }
        }

        protected override OptionsCategory[] CreateCategoryPages()
        {
            var categories = new List<OptionsCategory>
            {
                new OptionsCategory(Resources.InputOptionsTab_General,
                    Path.Combine("Input", "ComboBox", "General.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ValueBinding,
                    Path.Combine("Input", "ComboBox", "Value.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_ClientEvents,
                    Path.Combine("Controls", "ClientEvents.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_HtmlAttributes,
                    Path.Combine("Controls", "HtmlAttributes.xaml")),
                new OptionsCategory(Resources.InputOptionsTab_Misc,
                    Path.Combine("Input", "ComboBox", "Misc.xaml"))
            };

            UpdateCategoryPagesEnabled(categories);
            return categories.ToArray();
        }
    }
}
