﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Scaffolder.Services;
using C1.Web.Mvc;

namespace C1.Scaffolder.Models
{
    internal abstract class DataBoundOptionsModel : ControlOptionsModel
    {
        protected DataBoundOptionsModel(IServiceProvider serviceProvider, ItemsBoundControl<object> control)
            : base(serviceProvider, control)
        {
            ItemsBoundControl = control;
            control.IsBoundChanged += OnIsBoundChanged;
        }

        protected DataBoundOptionsModel(IServiceProvider serviceProvider, ItemsBoundControl<object> control, MVCControl settings)
            : base(serviceProvider, control)
        {
            ItemsBoundControl = control;
            BoundModeChanged += DataBoundOptionsModel_BoundModeChanged;
            if (settings!= null)
            {
                if (!string.IsNullOrEmpty(settings.ModelType))
                {
                    ItemsBoundControl.ModelType = ModelTypes.Where(c => (c.ShortTypeName.Equals(settings.ModelType) || c.TypeName.Equals(settings.ModelType))).First();
                    ItemsBoundControl.DbContextType = DbContextTypes.FirstOrDefault(ct => (true));//select first dbcontext
                    IsBoundMode = true;
                }
                else
                    IsBoundMode = false;

                InitControlValues(settings);
            }
            else
            {
                if (!IsInsertOnCodePage) IsBoundMode = true;
            }
                
            control.IsBoundChanged += OnIsBoundChanged;
        }

        private void DataBoundOptionsModel_BoundModeChanged(object sender, EventArgs e)
        {
            if (!IsBoundMode)
            {
                ItemsBoundControl.ModelType = null;
                ItemsBoundControl.DbContextType = null;
            }
        }

        private void OnIsBoundChanged(object sender, EventArgs e)
        {
            UpdateCategoryPagesEnabled(CategoryPages);
        }

        [IgnoreTemplateParameter]
        public ItemsBoundControl<object> ItemsBoundControl { get; private set; }

        [IgnoreTemplateParameter]
        public IEnumerable<IModelTypeDescription> DbContextTypes
        {
            get { return ServiceManager.DbContextProvider.DbContextTypes; }
        }

        [IgnoreTemplateParameter]
        public IEnumerable<IModelTypeDescription> ModelTypes
        {
            get { return ServiceManager.DbContextProvider.ModelTypes; }
        }

        protected override bool GetIsValid()
        {
           return !IsBoundMode || !IsProjectSupportEF || (ItemsBoundControl != null && ItemsBoundControl.IsBound);
        }

        protected void UpdateCategoryPagesEnabled(IEnumerable<OptionsCategory> categoryPages)
        {
            if (ItemsBoundControl == null) return;

            // Skip the first page which is used to set the binding
            //categoryPages.Skip(1).ToList().ForEach(o => o.IsEnabled = ItemsBoundControl.IsBound);
        }
    }
}
