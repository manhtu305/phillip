﻿using C1.Scaffolder.Localization;

namespace C1.Scaffolder.UI
{
    internal static class ControlList
    {
        public static readonly ControlDescription[] DataControls =
        {
            new ControlDescription(Resources.FlexGridName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexGrid.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexGridLarge.png",
                Resources.FlexGridDescription, FlexGrid),
            new ControlDescription(Resources.MultiRowName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/MultiRow.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/MultiRowLarge.png",
                Resources.MultiRowDescription, MultiRow),
            new ControlDescription(Resources.InputName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/Input.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputLarge.png",
                Resources.InputDescription, Input),
            new ControlDescription(Resources.FlexSheetName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexSheet.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexSheetLarge.png",
                Resources.FlexSheetDescription, FlexSheet),
            new ControlDescription(Resources.FlexChartName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexChart.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexChartLarge.png",
                Resources.FlexChartDescription, FlexChart),
            new ControlDescription(Resources.FlexPieName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexPie.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexPieLarge.png",
                Resources.FlexPieDescription, FlexPie),
            new ControlDescription(Resources.SunburstName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/Sunburst.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/SunburstLarge.png",
                Resources.SunburstDescription, Sunburst),
            new ControlDescription(Resources.FlexRadarName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexRadar.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/FlexRadarLarge.png",
                Resources.FlexRadarDescription, FlexRadar),            
            new ControlDescription(Resources.TabPanelName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/TabPanel.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/TabPanelLarge.png",
                Resources.TabPanelDescription, TabPanel),
            new ControlDescription(Resources.DashboardName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/Dashboard.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/DashboardLarge.png",
                Resources.DashboardDescription, Dashboard)
        };

        public static readonly ControlDescription[] InputControls =
        {
            new ControlDescription(Resources.InputDateName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputDate.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputDateLarge.png",
                Resources.InputDateDescription, InputDate),
            new ControlDescription(Resources.InputDateTimeName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputDateTime.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputDateTimeLarge.png",
                Resources.InputDateTimeDescription, InputDateTime),
            new ControlDescription(Resources.InputTimeName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputTime.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputTimeLarge.png",
                Resources.InputTimeDescription, InputTime),
            new ControlDescription(Resources.InputNumberName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputNumber.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputNumberLarge.png",
                Resources.InputNumberDescription, InputNumber),
            new ControlDescription(Resources.InputMaskName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputMask.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputMaskLarge.png",
                Resources.InputMaskDescription, InputMask),
            new ControlDescription(Resources.InputColorName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputColor.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/InputColorLarge.png",
                Resources.InputColorDescription, InputColor),
            new ControlDescription(Resources.ComboBoxName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/ComboBox.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/ComboBoxLarge.png",
                Resources.ComboBoxDescription, ComboBox),
            new ControlDescription(Resources.MultiSelectName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/MultiSelect.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/MultiSelectLarge.png",
                Resources.MultiSelectDescription, MultiSelect),
            new ControlDescription(Resources.AutoCompleteName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/AutoComplete.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/AutoCompleteLarge.png",
                Resources.AutoCompleteDescription, AutoComplete),
            new ControlDescription(Resources.MultiAutoCompleteName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/MultiAutoComplete.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/MultiAutoCompleteLarge.png",
                Resources.MultiAutoCompleteDescription, MultiAutoComplete)
        };

        public static readonly ControlDescription[] OlapControls =
        {
            new ControlDescription(Resources.PivotEngineName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/PivotEngine.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/PivotEngineLarge.png",
                Resources.PivotEngineDescription, PivotEngine),
           new ControlDescription(Resources.PivotPanelName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/PivotPanel.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/PivotPanelLarge.png",
                Resources.PivotPanelDescription, PivotPanel),
            new ControlDescription(Resources.PivotChartName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/PivotChart.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/PivotChartLarge.png",
                Resources.PivotChartDescription, PivotChart),
            new ControlDescription(Resources.PivotGridName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/PivotGrid.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/PivotGridLarge.png",
                Resources.PivotGridDescription, PivotGrid),
            new ControlDescription(Resources.SlicerName,
                "/C1.Scaffolder.4.5.2;component/Resources/Images/Slicer.png",
                "/C1.Scaffolder.4.5.2;component/Resources/Images/SlicerLarge.png",
                Resources.SlicerDescription, Slicer)
        };

        public const string FlexGrid = "FlexGrid";
        public const string MultiRow = "MultiRow";
        public const string Input = "Input";
        public const string FlexSheet = "FlexSheet";
        public const string FlexChart = "FlexChart";
        public const string FlexPie = "FlexPie";
        public const string Sunburst = "Sunburst";
        public const string FlexRadar = "FlexRadar";
        public const string InputDate = "InputDate";
        public const string InputDateTime = "InputDateTime";
        public const string InputTime = "InputTime";
        public const string InputNumber = "InputNumber";
        public const string InputMask = "InputMask";
        public const string InputColor = "InputColor";
        public const string ComboBox = "ComboBox";
        public const string AutoComplete = "AutoComplete";
        public const string MultiAutoComplete = "MultiAutoComplete";
        public const string MultiSelect = "MultiSelect";
        public const string PivotEngine = "PivotEngine";
        public const string PivotPanel = "PivotPanel";
        public const string PivotChart = "PivotChart";
        public const string PivotGrid = "PivotGrid";
        public const string Slicer = "Slicer";
        public const string TabPanel = "TabPanel";
        public const string Dashboard = "DashboardLayout";
    }
}
