﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model = C1.Scaffolder.Models.Chart;
using loc = C1.Scaffolder.Localization.Resources;

namespace C1.Scaffolder.UI.FlexChart
{
    /// <summary>
    /// Interaction logic for HeaderFooter.xaml
    /// </summary>
    public partial class HeaderFooter : UserControl
    {
        public HeaderFooter()
        {
            InitializeComponent();
        }

        private void lstSections_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var data = DataContext as Model.ChartBaseOptionsModel;
            var chart = data.Chart;
            if((lstSections.SelectedItem as ListBoxItem).Content.ToString() == loc.HeaderText)
            {
                SectionEditor.SelectedObject = chart.HeaderSection;
            }
            else
            {
                SectionEditor.SelectedObject = chart.FooterSection;
            }
        }
    }
}
