﻿using System.Diagnostics;
using C1.Scaffolder.Models.Chart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using C1.Web.Mvc;
using Model = C1.Scaffolder.Models.Chart;
using loc = C1.Scaffolder.Localization.Resources;

namespace C1.Scaffolder.UI.FlexChart
{
    /// <summary>
    /// Interaction logic for Axes.xaml
    /// </summary>
    public partial class Axes : UserControl
    {
        public Axes()
        {
            InitializeComponent();
        }

        private void lstAxis_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var model = DataContext as Model.ChartBaseOptionsModel;
            var chart = model.Chart;
            Debug.Assert(model != null);
            var isAxisX = (lstAxis.SelectedItem as ListBoxItem).Content.ToString() == loc.AxisXText;
            if(chart.ControlType == ChartControlType.FlexChart)
            {
                var data = model as Model.FlexChartOptionsModel;
                var flexChart = data.FlexChart;
                AxisEditor.SelectedObject = isAxisX ? flexChart.AxisX : flexChart.AxisY;
            }
            else if (chart.ControlType == ChartControlType.FlexRadar)
            {
                var data = model as Model.FlexRadarOptionsModel;
                var flexRadar = data.FlexRadar;
                AxisEditor.SelectedObject = isAxisX ? flexRadar.AxisX : flexRadar.AxisY;
            }
        }
    }
}
