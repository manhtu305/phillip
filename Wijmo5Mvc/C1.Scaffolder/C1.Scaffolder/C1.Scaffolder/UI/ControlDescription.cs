﻿namespace C1.Scaffolder.UI
{
    internal class ControlDescription
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string IconPath { get; private set; }
        public string ThumbnailPath { get; private set; }
        public string ControlType { get; private set; }

        public ControlDescription(string name, string iconPath, string thumbnailPath, string description, string controlType)
        {
            Name = name;
            IconPath = iconPath;
            ThumbnailPath = thumbnailPath;
            Description = description;
            ControlType = controlType;
        }
    }
}
