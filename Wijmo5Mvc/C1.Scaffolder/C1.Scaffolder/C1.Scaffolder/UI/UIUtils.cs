﻿using C1.Scaffolder.Localization;
using C1.Scaffolder.Models;
using C1.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup.Primitives;
using System.Windows.Media;

namespace C1.Scaffolder.UI
{
    public static class DependencyObjectHelper
    {
        public static List<DependencyObject> GetControlsBinding(DependencyObject dependencyObject, List<DependencyObject> sources)
        {
            if (dependencyObject.GetType().Name.Equals("ComboBox"))
            {
                sources.Add(dependencyObject);
            }
            else
            {
                var dependencyProperties = new List<DependencyProperty>();
                dependencyProperties.AddRange(MarkupWriter.GetMarkupObjectFor(dependencyObject).Properties.Where(x => x.DependencyProperty != null).Select(x => x.DependencyProperty).ToList());
                dependencyProperties.AddRange(
                    MarkupWriter.GetMarkupObjectFor(dependencyObject).Properties.Where(x => x.IsAttached && x.DependencyProperty != null).Select(x => x.DependencyProperty).ToList());

                var bindings = dependencyProperties.Select(x => BindingOperations.GetBindingBase(dependencyObject, x)).Where(x => x != null).ToList();

                if (bindings != null && bindings.Count > 0)
                {
                    sources.Add(dependencyObject);
                }
            }

            var children = LogicalTreeHelper.GetChildren(dependencyObject).OfType<DependencyObject>().ToList();
            if (children.Count == 0)
                return sources.ToList();

            foreach (var child in children)
            {
                sources.AddRange(GetControlsBinding(child, new List<DependencyObject>()));
            }

            return sources.ToList();
        }

        public static DependencyObject Filter(List<DependencyObject> source, string propertyName)
        {
            Predicate<Binding> isBindCheckingProperty = binding => binding != null && binding.Path.Path.Contains("." + propertyName);

            Predicate<ComboBox> cbbForCheckingProperty = cbb => cbb != null && cbb.ItemsSource != null && cbb.ItemsSource.GetType().FullName.Contains("." + propertyName + "[]");

            for (int i = 0; i < source.Count; ++i)
            {
                var dependencyObject = source[i];
                if (dependencyObject is ComboBox)
                {
                    if (cbbForCheckingProperty(dependencyObject as ComboBox)) return dependencyObject;
                }
                else
                {
                    var dependencyProperties = new List<DependencyProperty>();
                    dependencyProperties.AddRange(MarkupWriter.GetMarkupObjectFor(dependencyObject).Properties.Where(x => x.DependencyProperty != null).Select(x => x.DependencyProperty).ToList());
                    dependencyProperties.AddRange(
                        MarkupWriter.GetMarkupObjectFor(dependencyObject).Properties.Where(x => x.IsAttached && x.DependencyProperty != null).Select(x => x.DependencyProperty).ToList());
                    var bindings = dependencyProperties.Select(x => BindingOperations.GetBindingBase(dependencyObject, x)).Where(x => x != null).ToList();
                    foreach (var bindingBase in bindings)
                    {
                        if (bindingBase is Binding)
                        {
                            if (isBindCheckingProperty(bindingBase as Binding))
                                return dependencyObject;
                        }
                    }
                }
            }

            return null;
        }
    }

    internal class UIUpdateUtils
    {
        public UIUpdateUtils()
        {
        }

        internal void AdjustContentControl(ContentControl contentControl, MVCControl controlSettings)
        {
            if (contentControl != null && controlSettings != null)
            {
                StackPanel sp = contentControl.Content as StackPanel;
                if (sp != null)
                {
                    List<DependencyObject> controls = controls = DependencyObjectHelper.GetControlsBinding(sp, new List<DependencyObject>()); ;
                    List<KeyValuePair < string, MVCProperty>> properties = controlSettings.Properties.Where(p => p.Value.NeedTextEditor).ToList();

                    for(int i = 0; i < properties.Count; ++i)
                    {
                        StackPanel parent;
                        int idx;
                        UIElement ctrl;
                        StackPanel childStack;

                        ctrl = (UIElement)DependencyObjectHelper.Filter(controls, properties.ElementAt(i).Key);
                        if (ctrl == null) continue;
                        
                        childStack = new StackPanel();
                        childStack.Orientation = Orientation.Horizontal;
                        // this trick is not good but it's a litebit faster than below checking logic.
                        if (!ctrl.IsEnabled)
                            continue;
                        
                        parent = (StackPanel)VisualTreeHelper.GetParent(ctrl);
                        if (parent == null) continue;
                        idx = parent.Children.IndexOf(ctrl);

                        // should keep this in here until some thing will break above checking logic

                        //if (idx == -1) continue;
                        //if (parent.Children.Count > idx + 1) {
                        //    Button x = null;
                        //    if ((x = (Button)parent.Children[idx + 1]) != null)
                        //    {
                        //        // if it's already contain red button.
                        //        if (x.Content.ToString() == "(!)")
                        //            continue;
                        //    }
                        //}

                        ctrl.IsEnabled = false;
                        parent.Children.RemoveAt(idx);
                        parent.Children.Insert(idx, childStack);
                        childStack.Children.Add(ctrl);

                        Button editButton = new Button();
                        editButton.Content = "(!)";
                        editButton.Margin = new Thickness(4, 0, 0, 0);
                        editButton.ToolTip = Resources.ExpressionValueTooltip;
                        editButton.Tag = properties.ElementAt(i).Value;
                        editButton.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 0, 0));
                        editButton.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
                        editButton.BorderThickness = new Thickness(0);

                        editButton.Click += EditButton_Click;
                        childStack.Children.Add(editButton);
                    }
                }
            }
        }
        
        private static void EditButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                MVCProperty property = button.Tag as MVCProperty;
                var inputWindow = new PromptDialog(property.Value.ToString());
                var dialogResult = inputWindow.ShowDialog();
                if (dialogResult.HasValue && (bool)dialogResult)
                {
                    property.AdjustRenderedText(inputWindow.EditedValue);
                }
            }
        }
    }
}
