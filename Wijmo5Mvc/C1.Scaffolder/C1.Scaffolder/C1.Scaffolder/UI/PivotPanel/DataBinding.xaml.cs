﻿using C1.Scaffolder.Models.Olap;
using C1.Web.Mvc;
using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;


namespace C1.Scaffolder.UI.PivotPanel
{
    /// <summary>
    /// Interaction logic for Fields.xaml
    /// </summary>
    public partial class DataBinding : UserControl
    {
        private Point _startPoint;

        public DataBinding()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!cbModels.IsLoaded)
            {
                return;
            }

            _startPoint = new Point();
            InitializeListViews();
        }

        private void InitializeListViews()
        {
            var peOptionsModel = DataContext as PivotPanelOptionsModel;
            peOptionsModel.Fields.Clear();
            if (peOptionsModel.PivotPanel.Engine.ModelType != null)
            {
                foreach (var item in peOptionsModel.PivotPanel.Engine.ModelType.Properties)
                {
                    peOptionsModel.Fields.Add(new PivotField()
                    {
                        KeyWrapper = item.Name
                    });
                }
            }

            peOptionsModel.PivotPanel.Engine.RowFields.Items.Clear();
            peOptionsModel.PivotPanel.Engine.ColumnFields.Items.Clear();
            peOptionsModel.PivotPanel.Engine.ValueFields.Items.Clear();
            peOptionsModel.PivotPanel.Engine.FilterFields.Items.Clear();

            tbCollectionViewReadActionUrl.GetBindingExpression(TextBox.IsEnabledProperty).UpdateTarget();
            cbxDbContexTypes.GetBindingExpression(ComboBox.IsEnabledProperty).UpdateTarget();
        }

        private void lstView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Get current mouse position
            _startPoint = e.GetPosition(null);
        }

        // Helper to search up the VisualTree
        private static T FindAnchestor<T>(DependencyObject current)
            where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }

        private void lstView_MouseMove(object sender, MouseEventArgs e)
        {
            // Get the current mouse position
            Point mousePos = e.GetPosition(null);
            Vector diff = _startPoint - mousePos;

            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                       Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                // Get the dragged ListViewItem
                ListView sourceListView = sender as ListView;
                ListViewItem listViewItem = FindAnchestor<ListViewItem>((DependencyObject)e.OriginalSource);
                if (listViewItem == null)
                {
                    return;
                }

                // Find the data behind the ListViewItem
                PivotField prop = (PivotField)sourceListView.ItemContainerGenerator.ItemFromContainer(listViewItem);
                if (prop == null)
                {
                    return;
                }

                // Initialize the drag & drop operation
                DataObject dragData = new DataObject("WorkItem", new PivotDragEventData
                {
                    Data = prop,
                    Item = listViewItem,
                    Source = sourceListView
                });

                DragDrop.DoDragDrop(listViewItem, dragData, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }

        private void lstView_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("WorkItem"))
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void lstView_Drop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("WorkItem"))
            {
                return;
            }

            ListView targetListView = sender as ListView;
            object eventData = e.Data.GetData("WorkItem");

            if (eventData == null || !(eventData is PivotDragEventData))
            {
                // Abort
                e.Effects = DragDropEffects.None;
                return;
            }

            PivotDragEventData dragData = eventData as PivotDragEventData;
            if (!dragData.Valid)
            {
                // Abort
                e.Effects = DragDropEffects.None;
                return;
            }

            e.Effects = DragDropEffects.Move;
            PivotFieldBase prop = dragData.Data;
            GetItemsSource(targetListView).Add(prop);
            GetItemsSource(dragData.Source).Remove(prop);
        }

        private ObservableCollection<PivotFieldBase> GetItemsSource(ListView listView)
        {
            return "lvFields".Equals(listView.Name) ?
                (DataContext as PivotPanelOptionsModel).Fields :
                (listView.ItemsSource as ObservableCollection<PivotFieldBase>);
        }
    }
}
