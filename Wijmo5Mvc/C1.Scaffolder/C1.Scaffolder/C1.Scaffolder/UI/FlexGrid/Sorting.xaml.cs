﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using C1.Web.Mvc;

namespace C1.Scaffolder.UI.FlexGrid
{
    /// <summary>
    /// Interaction logic for Sorting.xaml
    /// </summary>
    public partial class Sorting : UserControl
    {
        delegate Point GetPositionDelegate(IInputElement element);

        public Sorting()
        {
            InitializeComponent();
        }

        private void UpButton_Click(object sender, RoutedEventArgs e)
        {
            var oldSelectedIndex = lvSortDescriptions.SelectedIndex;
            if (oldSelectedIndex == 0)
            {
                return;
            }
            DownButton.IsEnabled = true;
            var itemsSource = lvSortDescriptions.ItemsSource as IList;
            (itemsSource as ObservableCollection<DesignSortDescription>).Move(oldSelectedIndex, oldSelectedIndex - 1);
        }

        private void DownButton_Click(object sender, RoutedEventArgs e)
        {
            var oldSelectedIndex = lvSortDescriptions.SelectedIndex;
            if (oldSelectedIndex >= lvSortDescriptions.Items.Count - 1)
            {
                return;
            }
            UpButton.IsEnabled = true;
            var itemsSource = lvSortDescriptions.ItemsSource as IList;
            (itemsSource as ObservableCollection<DesignSortDescription>).Move(oldSelectedIndex, oldSelectedIndex + 1);
        }

        private void lvSortDescriptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listview = sender as ListView;
            var selectedIndex = listview.SelectedIndex;
            UpButton.IsEnabled = selectedIndex > 0;
            DownButton.IsEnabled = selectedIndex > -1 && selectedIndex < (listview.Items.Count - 1);
        }
    }
}
