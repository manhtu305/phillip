﻿using System.Windows;
using System.Windows.Controls;
using C1.Scaffolder.Models;
using C1.Scaffolder.Models.Grid;
using C1.Web.Mvc;

namespace C1.Scaffolder.UI.FlexGrid
{
    /// <summary>
    /// Interaction logic for Columns.xaml
    /// </summary>
    public partial class Columns
    {
        public Columns()
        {
            InitializeComponent();
        }

        private void AutoGenerateColumnsCkb_OnChecked(object sender, RoutedEventArgs e)
        {
            var expression = ColumnsEditor.GetBindingExpression(CollectionEditor.ItemsSourceProperty);
            if (expression != null)
            {
                expression.UpdateTarget();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ColumnsEditor.AddButton.Visibility = Visibility.Collapsed;
            var btn = new Button
            {
                Content = ColumnsEditor.AddButton.Content,
                MinHeight = 24,
                HorizontalAlignment = HorizontalAlignment.Right
            };
            btn.Click += (s, ev) => AddColumn();
            ColumnsEditor.Toolbar.Children.Add(btn);
            
        }

        private void AddColumn()
        {
            var col = new Column();
            ColumnsEditor.ItemsSource.Add(col);
            ColumnsEditor._showList.Add(new ListBoxItem { Content = col.GetType().Name });
            ColumnsEditor.ItemsSelector.SelectedIndex = ColumnsEditor.ItemsSource.Count - 1;
        }
    }
}
