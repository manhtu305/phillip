﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace C1.Scaffolder.UI.Input
{
    /// <summary>
    /// Interaction logic for TimeEditor.xaml
    /// </summary>
    public partial class TimeEditor : UserControl
    {
        public TimeEditor()
        {
            InitializeComponent();
            valueHolder.DataContext = this;
        }

        public static readonly DependencyProperty ValueProperty=DependencyProperty.Register("Value", typeof(DateTime), typeof(TimeEditor));

        public DateTime Value
        {
            get { return (DateTime)this.GetValue(ValueProperty); }
            set { this.SetValue(ValueProperty, value); }
        }

        private void ValueHolder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsLoaded) return;
            var date = Value;
            var hour = cmbHour.SelectedValue == null ? 0 : (int)cmbHour.SelectedValue;
            var minute = cmbMinute.SelectedValue == null ? 0 : (int)cmbMinute.SelectedValue;
            var second = cmbSecond.SelectedValue == null ? 0 : (int)cmbSecond.SelectedValue;
            Value = new DateTime(date.Year, date.Month, date.Day, hour, minute, second);
        }
    }
}
