﻿using C1.Scaffolder.Models.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace C1.Scaffolder.UI.Input
{
    /// <summary>
    /// Interaction logic for ValueBinding.xaml
    /// </summary>
    public partial class ValueBinding : UserControl
    {
        public ValueBinding()
        {
            InitializeComponent();

            cbbModelType.SelectionChanged += CbbModelType_SelectionChanged;
        }

        private void CbbModelType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool needResetPath = true;
            FormInputOptionsModel optionModel = DataContext as FormInputOptionsModel;
            Web.Mvc.InputValueBinding valueBinding= null;
            if (optionModel != null && optionModel.Input != null && (valueBinding = optionModel.Input.ValueBinding) != null )
            {
                string currentValuePath = cbbValuePath.Text;
                if (!string.IsNullOrEmpty(currentValuePath))
                {
                    foreach (var item in valueBinding.ModelType.Properties)
                    {
                        if (item.Name.Equals(currentValuePath))
                        {
                            valueBinding.ValuePath = currentValuePath;
                            needResetPath = false;
                        }
                    }
                }
            }
            if (needResetPath)
            {
                cbbValuePath.SelectedIndex = -1;
                cbbValuePath.Text = string.Empty;
            }
        }
    }
}
