﻿using C1.Web.Mvc.Olap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace C1.Scaffolder.UI
{
    internal class PivotDragEventData
    {
        public ListViewItem Item { get; set; }
        public PivotFieldBase Data { get; set; }
        public ListView Source { get; set; }

        public bool Valid
        {
            get { return Item != null && Data != null && Source != null; }
        }
    }
}
