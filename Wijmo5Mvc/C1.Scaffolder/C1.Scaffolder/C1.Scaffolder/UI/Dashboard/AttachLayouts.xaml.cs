﻿using C1.Scaffolder.Models;
using C1.Scaffolder.Models.Dashboard;
using C1.Web.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace C1.Scaffolder.UI.Dashboard
{
    /// <summary>
    /// Interaction logic for AttachLayouts.xaml
    /// </summary>
    public partial class AttachLayouts : UserControl
    {
        private ListItemsSplit listItemsSplit;
        public AttachLayouts()
        {
            InitializeComponent();
            DataContextChanged += AttachLayouts_DataContextChanged;

            listItemsSplit = new ListItemsSplit();
            
            ItemsSelector.Loaded += ItemsSelector_Loaded;
        }

        private void ItemsSelector_Loaded(object sender, RoutedEventArgs e)
        {
            CheckLayoutType();
        }

        private void AttachLayouts_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            BindLayoutItems();
        }

        private IList layoutItems;
        private void BindLayoutItems()
        {
            DashboardOptionsModel optionModel = DataContext as DashboardOptionsModel;
            layoutItems = optionModel.Dashboard.LayoutItems;
            cbbLayoutType.SelectionChanged -= cbbLayoutType_SelectionChanged;
            if (layoutItems != null)
            {
                LayoutType layoutType = optionModel.Dashboard.LayoutType;
                switch (layoutType)
                {
                    case LayoutType.Split:
                        {
                            foreach (ISplitLayoutItem group in layoutItems)
                            {
                                AddSplitChildItems(group, listItemsSplit);
                            }
                        }
                        break;
                    case LayoutType.Flow:
                        {
                            foreach (FlowTile tile in layoutItems)
                            {
                                var tileWrapper = new TileWrapper(null, tile);
                                _showList.Add(tileWrapper);
                            }
                        }
                        break;
                    case LayoutType.Auto:
                        {
                            foreach (AutoGridGroup group in layoutItems)
                            {
                                var groupWrapper = new GroupWrapper(group);
                                _showList.Add(groupWrapper);

                                foreach (AutoGridTile tile in group.Children)
                                {
                                    var tileWrapper = new TileWrapper(groupWrapper, tile);
                                    groupWrapper.Children.Add(tileWrapper);
                                }
                            }
                        }
                        break;
                    case LayoutType.Manual:
                        {
                            foreach (ManualGridGroup group in layoutItems)
                            {
                                var groupWrapper = new GroupWrapper(group);
                                _showList.Add(groupWrapper);

                                foreach (ManualGridTile tile in group.Children)
                                {
                                    var tileWrapper = new TileWrapper(groupWrapper, tile);
                                    groupWrapper.Children.Add(tileWrapper);
                                }
                            }
                        }
                        break;
                }
                

                cbbLayoutType.SelectedItem = layoutType;
            }

            ItemsSelector.ItemsSource = _showList;
            cbbLayoutType.SelectionChanged += cbbLayoutType_SelectionChanged;
            EnableControlsSetting();
        }

        private void AddSplitChildItems(ISplitLayoutItem layoutItem, GroupWrapper parent)
        {
            if (layoutItem is SplitGroup)
            {
                SplitGroup sGroup = layoutItem as SplitGroup;
                var groupWrapper = new GroupWrapper(sGroup);
                foreach (ISplitLayoutItem splitLayoutItem in sGroup.Children)
                {
                    AddSplitChildItems(splitLayoutItem, groupWrapper);
                }

                parent.Children.Add(groupWrapper);
            }
            else if (layoutItem is SplitTile)
            {
                TileWrapper tWrapper = new TileWrapper(parent, layoutItem as SplitTile);
                parent.Children.Add(tWrapper);
            }
        }

        private void EnableControlsSetting()
        {
            if (cbbLayoutType.SelectedIndex == -1)
                return;

            gridLayouts.IsEnabled = true;
            ItemEditor.IsEnabled = true;

            LayoutType layoutType = (LayoutType)cbbLayoutType.SelectedItem;
            AddGroupButton.IsEnabled = layoutType != LayoutType.Flow;
            if (layoutType == LayoutType.Flow)
            {
                cbbDirection.Visibility = Visibility.Visible;
                labelDirection.Visibility = Visibility.Visible;
                cbbOrientation.Visibility = Visibility.Collapsed;
                labelOrientation.Visibility = Visibility.Collapsed;
            }
            else
            {
                cbbDirection.Visibility = Visibility.Collapsed;
                labelDirection.Visibility = Visibility.Collapsed;
                cbbOrientation.Visibility = Visibility.Visible;
                labelOrientation.Visibility = Visibility.Visible;
            }
            gridSizes.Visibility = (layoutType == LayoutType.Auto || layoutType == LayoutType.Manual) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void CheckLayoutType()
        {
            DashboardOptionsModel optionModel = DataContext as DashboardOptionsModel;
            switch (optionModel.Dashboard.LayoutType)
            {
                case LayoutType.Auto:
                case LayoutType.Manual:
                    AddGroupButton.IsEnabled = true;
                    AddTileButton.IsEnabled  = false;
                    break;
                case LayoutType.Split:
                    if(!_showList.Contains(listItemsSplit))
                        _showList.Add(listItemsSplit);
                    AddGroupButton.IsEnabled = true;
                    AddTileButton.IsEnabled  = true;
                    break;
                case LayoutType.Flow:
                    AddGroupButton.IsEnabled = false;
                    AddTileButton.IsEnabled  = true;
                    break;
            }
        }

        private void cbbLayoutType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _showList.Clear();//clear all current list of groups and tiles
            layoutItems = (DataContext as DashboardOptionsModel).Dashboard.LayoutItems;
            EnableControlsSetting();

            if(listItemsSplit.Children.Count != 0)
                listItemsSplit.Children.Clear();
            CheckLayoutType();
        }

        #region collection editor
        private readonly ObservableCollection<IGroupTileWrapper> _showList = new ObservableCollection<IGroupTileWrapper>();

        /// <summary>
        /// Occurs after an item is created.
        /// </summary>
        public event EventHandler<ItemCreatedEventArgs> ItemCreated;

        private void ItemsSelector_SelectionChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var wrapper = ItemsSelector.SelectedItem as IGroupTileWrapper;
            ItemEditor.SelectedObject = wrapper == null ? null : wrapper.Object;

            if(!AddTileButton.IsEnabled)
                AddTileButton.IsEnabled = true;
        }


        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var wrapper = ItemsSelector.SelectedItem as IGroupTileWrapper;
            if (wrapper == null)
                return;
            else if (wrapper == listItemsSplit)
            {
                if (listItemsSplit.Children.Count != 0)
                {
                    listItemsSplit.Children.Clear();
                    layoutItems.Clear();
                }

                return;
            }

            IList<IGroupTileWrapper> wrapperContainer = null;
            IList itemContainer = null;

            if ((DataContext as DashboardOptionsModel).Dashboard.LayoutType == LayoutType.Split)
            {
                if (wrapper is GroupWrapper || (wrapper is TileWrapper && (wrapper as TileWrapper).Parent == listItemsSplit))
                {
                    wrapperContainer = listItemsSplit.Children;
                    itemContainer = layoutItems;
                }
                else
                {
                    wrapperContainer = wrapper.Parent.Children;
                    itemContainer = wrapperContainer.Select(x => x.Object).ToList();
                }
            }
            else
            {
                wrapperContainer = GetWrappersContainer(wrapper);
                itemContainer = GetItemsContainer(wrapper);
            }

            var index = wrapperContainer.IndexOf(wrapper);
            // select next item.
            var nextIndex = index + 1;
            // select the previous item if remove the last item.
            if (nextIndex >= wrapperContainer.Count)
            {
                nextIndex = index - 1;
            }

            // get the next item to be selected
            var nextItem = nextIndex < 0 ? wrapper.Parent : wrapperContainer[nextIndex];

            // remove the item
            itemContainer.RemoveAt(index);

            // sync with wrapper
            wrapperContainer.RemoveAt(index);
            wrapper.IsSelected = false;

            if (nextItem != null)
            {
                nextItem.IsSelected = true;

                DashboardOptionsModel optionModel = DataContext as DashboardOptionsModel;
                switch (optionModel.Dashboard.LayoutType)
                {
                    case LayoutType.Auto:
                    case LayoutType.Manual:
                    case LayoutType.Split:
                        AddGroupButton.IsEnabled = true;
                        AddTileButton.IsEnabled = true;
                        break;
                    case LayoutType.Flow:
                        AddGroupButton.IsEnabled = false;
                        AddTileButton.IsEnabled = true;
                        break;
                }
            }
            else
            {
                CheckLayoutType();
            }
        }

        private void UpButton_Click(object sender, RoutedEventArgs e)
        {
            MoveSelectedItem(-1);
        }

        private void DownButton_Click(object sender, RoutedEventArgs e)
        {
            MoveSelectedItem(1);
        }

        private void AddGroupButton_Click(object sender, RoutedEventArgs e)
        {
            GroupWrapper groupWrapper;
            var wrapper = ItemsSelector.SelectedItem as IGroupTileWrapper;
            if (wrapper != null)
            {
                wrapper.IsSelected = false;
            }
            switch((LayoutType)cbbLayoutType.SelectedItem)
            {
                case LayoutType.Auto:
                case LayoutType.Manual:
                    groupWrapper = CreateGroup();
                    groupWrapper.IsSelected = true;
                    break;

                case LayoutType.Split:
                    var group = new SplitGroup();
                    layoutItems.Add(group);
                    OnItemCreated(group);
                    groupWrapper = new GroupWrapper(group);
                    listItemsSplit.Children.Add(groupWrapper);
                    groupWrapper.IsSelected = true;
                    break;
            }

        }

        private void AddTileButton_Click(object sender, RoutedEventArgs e)
        {
            var wrapper = ItemsSelector.SelectedItem as IGroupTileWrapper;
            if (wrapper != null)
            {
                wrapper.IsSelected = false;//unselect current
            }

            Tile tile = null;
            GroupWrapper groupWrapper = null;
            TileWrapper tileWrapper = null;
            LayoutType layoutType = (LayoutType)cbbLayoutType.SelectedItem;

            if (layoutType == LayoutType.Flow)
            {
                tile = new FlowTile();
                TileWrapper flowTileWrapper = new TileWrapper(null, tile);
                OnItemCreated(tile);
                flowTileWrapper.IsSelected = true;
                layoutItems.Add(tile);
                _showList.Add(flowTileWrapper);
                return;
            }
            if (layoutType != LayoutType.Split)
                groupWrapper = wrapper == null ? CreateGroup() : (GroupWrapper)(wrapper.Parent ?? wrapper);

            switch (layoutType)
            {
                case LayoutType.Split:
                    {
                        TileWrapper splitTileWrapper;
                        tile = new SplitTile();
                        OnItemCreated(tile);

                        if (wrapper == listItemsSplit || wrapper == null || (wrapper is TileWrapper && wrapper.Parent == listItemsSplit))
                        {
                            groupWrapper = listItemsSplit;
                            layoutItems.Add(tile);
                        }
                        else
                        {
                            if (wrapper is TileWrapper)
                                groupWrapper = (wrapper as TileWrapper).Parent as GroupWrapper;
                            else
                                groupWrapper = wrapper as GroupWrapper;

                            (groupWrapper.Group as SplitGroup).Children.Add(tile as SplitTile);
                        }

                        splitTileWrapper = new TileWrapper(groupWrapper, tile);
                        groupWrapper.Children.Add(splitTileWrapper);
                        splitTileWrapper.IsSelected = true;
                    }
                    return;
                case LayoutType.Auto:
                    tile = CreateAutoTile(groupWrapper);
                    break;
                case LayoutType.Manual:
                    tile = CreateManualTile(groupWrapper);
                    break;
            }

            OnItemCreated(tile);
            tileWrapper = new TileWrapper(groupWrapper, tile);
            groupWrapper.Children.Add(tileWrapper);
            tileWrapper.IsSelected = true;
        }

        private Tile CreateAutoTile(GroupWrapper groupWrapper)
        {
            // add new tile
            AutoGridTile tile = new AutoGridTile();
            ((AutoGridGroup)groupWrapper.Group).Children.Add(tile);

            return tile;
        }

        private Tile CreateManualTile(GroupWrapper groupWrapper)
        {
            // add new tile
            ManualGridTile tile = new ManualGridTile();
            ((ManualGridGroup)groupWrapper.Group).Children.Add(tile);

            return tile;
        }

        private Tile CreateSplitTile(GroupWrapper groupWrapper)
        {
            // add new tile
            SplitTile tile = new SplitTile();
            if (groupWrapper != null && groupWrapper.Group is SplitGroup)
            {
                ((SplitGroup)groupWrapper.Group).Children.Add(tile);
            }
            return tile;
        }

        protected virtual void OnItemCreated(object item)
        {
            if (ItemCreated != null)
            {
                ItemCreated(this, new ItemCreatedEventArgs(item));
            }
        }


        #region drag drop

        private IGroupTileWrapper _draggedWrapper;
        private IGroupTileWrapper _droppedWrapper;
        private System.Windows.Point _startPoint;

        private void ItemsSelector_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _startPoint = e.GetPosition(ItemsSelector);
            var item = GetNearestContainer(e.OriginalSource as UIElement);
            _draggedWrapper = item == null ? null : GetWrapper(item);
            _droppedWrapper = null;
        }

        private void ItemsSelector_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            var pos = e.GetPosition(ItemsSelector);
            if (e.LeftButton == MouseButtonState.Pressed && _draggedWrapper != null
                && CheckDragDistance(_startPoint - pos))
            {
                DragDrop.DoDragDrop(ItemsSelector, _draggedWrapper, DragDropEffects.Move | DragDropEffects.Copy);
            }
        }

        private void ItemsSelector_DragOver(object sender, DragEventArgs e)
        {
            ClearDroppedWrapper();
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            var pos = e.GetPosition(ItemsSelector);
            if (!CheckDragDistance(_startPoint - pos)) return;

            var item = GetNearestContainer(e.OriginalSource as UIElement);
            var wrapper = GetWrapper(item);
            if (wrapper != null && CheckDropTarget(wrapper))
            {
                var isCopy = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
                e.Effects = isCopy ? DragDropEffects.Copy : DragDropEffects.Move;
                _droppedWrapper = wrapper;
                _droppedWrapper.IsDropTarget = true;
            }
        }

        private void ItemsSelector_DragLeave(object sender, DragEventArgs e)
        {
            ClearDroppedWrapper();
        }

        private void ItemsSelector_Drop(object sender, DragEventArgs e)
        {
            ClearDroppedWrapper();
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            var item = GetNearestContainer(e.OriginalSource as UIElement);
            var targetWrapper = GetWrapper(item);
            if (targetWrapper != null && _draggedWrapper != null && CheckDropTarget(targetWrapper))
            {
                var isCopy = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
                if (isCopy)
                {
                    CopyItem(_draggedWrapper, targetWrapper);
                }
                else
                {
                    MoveItem(_draggedWrapper, targetWrapper);
                }
            }
        }

        private void CopyItem(IGroupTileWrapper srcWrapper, IGroupTileWrapper desWrapper)
        {
            // copy group
            var srcGroupLayoutWrapper = srcWrapper as GroupWrapper;
            if (srcGroupLayoutWrapper != null)
            {
                var desGroupLayoutWrapper = desWrapper as GroupWrapper;
                // can only copy the group next to another group
                if (desGroupLayoutWrapper == null) return;

                var desIndex = _showList.IndexOf(desGroupLayoutWrapper) + 1;

                return;
            }

            // copy cell
            var srcCellWrapper = srcWrapper as CellWrapper;
            if (srcCellWrapper != null)
            {
                var desGroupLayoutWrapper = desWrapper as GroupWrapper;
                int desIndex;

                if (desGroupLayoutWrapper == null)
                {
                    // des is cell
                    var desCellWrapper = desWrapper as CellWrapper;
                    if (desCellWrapper == null) return;

                    desGroupLayoutWrapper = desCellWrapper.Parent as GroupWrapper;
                    if (desGroupLayoutWrapper == null) return;

                    //desIndex = desGroupLayoutWrapper.Children.IndexOf(desCellWrapper) + 1;
                }
                else
                {
                    // des is group, add to the end of children
                    desIndex = desGroupLayoutWrapper.Children.Count;
                }

                // adjust selection
                srcCellWrapper.IsSelected = false;
            }
        }

        private void MoveItem(IGroupTileWrapper srcWrapper, IGroupTileWrapper desWrapper)
        {
            // move the group
            var srcGroupLayoutWrapper = srcWrapper as GroupWrapper;
            if (srcGroupLayoutWrapper != null)
            {
                var desGroupLayoutWrapper = desWrapper as GroupWrapper;
                // can only move the group next to another group
                if (desGroupLayoutWrapper == null) return;

                var srcIndex = _showList.IndexOf(srcGroupLayoutWrapper);

                // sync with wrapper
                _showList.RemoveAt(srcIndex);

                var desIndex = _showList.IndexOf(desGroupLayoutWrapper) + 1;

                // sync with wrapper
                _showList.Insert(desIndex, srcGroupLayoutWrapper);

                // adjust selection
                srcGroupLayoutWrapper.IsSelected = true;

                return;
            }

            // move the cell
            var srcCellWrapper = srcWrapper as CellWrapper;
            if (srcCellWrapper != null)
            {
                srcGroupLayoutWrapper = srcCellWrapper.Parent as GroupWrapper;
                if (srcGroupLayoutWrapper == null) return;

                var desGroupLayoutWrapper = desWrapper as GroupWrapper;
                var desCellWrapper = desWrapper as CellWrapper;

                // des is cell
                if (desGroupLayoutWrapper == null)
                {
                    if (desCellWrapper == null) return;

                    desGroupLayoutWrapper = desCellWrapper.Parent as GroupWrapper;
                    if (desGroupLayoutWrapper == null) return;
                }
            }
        }

        private bool CheckDragDistance(Vector diff)
        {
            return Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance
                   || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance;
        }

        private bool CheckDropTarget(IGroupTileWrapper targetWrapper)
        {
            if (targetWrapper == null) return false;

            if (_draggedWrapper is GroupWrapper)
            {
                // drag the group, can only be dropped to other groups.
                var targetGroupLayoutWrapper = targetWrapper as GroupWrapper;
                return targetGroupLayoutWrapper != null && _draggedWrapper != targetGroupLayoutWrapper;
            }

            // drag the cell, can be dropped to other cells and groups.
            return _draggedWrapper != targetWrapper;
        }

        private void ClearDroppedWrapper()
        {
            if (_droppedWrapper != null)
            {
                _droppedWrapper.IsDropTarget = false;
                _droppedWrapper = null;
            }
        }

        private IGroupTileWrapper GetWrapper(TreeViewItem item)
        {
            if (item == null) return null;

            var wrapper = item.Header as IGroupTileWrapper;
            if (wrapper == null)
            {
                var parent = FindVisualParent<ItemsControl>(item);
                if (parent != null)
                {
                    wrapper = parent.ItemContainerGenerator.ItemFromContainer(item) as IGroupTileWrapper;
                }
            }

            return wrapper;
        }

        private TreeViewItem GetNearestContainer(UIElement element)
        {
            var container = element as TreeViewItem;
            while (container == null && element != null)
            {
                element = VisualTreeHelper.GetParent(element) as UIElement;
                container = element as TreeViewItem;
            }

            return container;
        }

        private T FindVisualParent<T>(UIElement child) where T : UIElement
        {
            if (child == null) return null;

            var parent = VisualTreeHelper.GetParent(child) as UIElement;
            while (parent != null)
            {
                var found = parent as T;
                if (found != null) return found;

                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }

            return null;
        }

        #endregion

        private GroupWrapper CreateGroup()
        {
            Group group = null;
            LayoutType layoutType = (LayoutType)cbbLayoutType.SelectedItem;
            // create a new group
            switch (layoutType)
            {
                case LayoutType.Auto:
                    group = new AutoGridGroup();
                    break;
                case LayoutType.Split:
                    group = new SplitGroup();
                    break;
                case LayoutType.Flow:
                    return null;
                case LayoutType.Manual:
                default:
                    group = new ManualGridGroup();
                    break;
            }

            layoutItems.Add(group);
            if (group != null) OnItemCreated(group);

            // sync with wrapper
            var groupWrapper = new GroupWrapper(group);
            _showList.Add(groupWrapper);
            return groupWrapper;
        }

        private IList<IGroupTileWrapper> GetWrappersContainer(IGroupTileWrapper wrapper)
        {
            return wrapper.Parent == null ? _showList : wrapper.Parent.Children;
        }

        private IList GetItemsContainer(IGroupTileWrapper wrapper)
        {
            if (wrapper.Parent == null) return layoutItems;

            switch ((LayoutType)cbbLayoutType.SelectedItem)
            {
                case LayoutType.Auto:
                    AutoGridGroup aggroup = (AutoGridGroup)wrapper.Parent.Object;
                    return new ListWrapper<AutoGridTile>(aggroup.Children);
                case LayoutType.Split:
                    SplitGroup sgroup = (SplitGroup)wrapper.Parent.Object;
                    return new ListWrapper<ISplitLayoutItem>(sgroup.Children);
                case LayoutType.Flow:
                    return layoutItems;
                case LayoutType.Manual:
                    ManualGridGroup mggroup = (ManualGridGroup)wrapper.Parent.Object;
                    return new ListWrapper<ManualGridTile>(mggroup.Children);
            }
            return null;
        }

        private void MoveSelectedItem(int delta)
        {
            var wrapper = ItemsSelector.SelectedItem as IGroupTileWrapper;
            if (wrapper == null || wrapper == listItemsSplit) return;
            IList<IGroupTileWrapper> wrapperContainer = null;
            IList itemContainer = null;

            if ((DataContext as DashboardOptionsModel).Dashboard.LayoutType == LayoutType.Split)
            {
                if (wrapper is GroupWrapper || (wrapper is TileWrapper && (wrapper as TileWrapper).Parent == listItemsSplit))
                {
                    wrapperContainer = listItemsSplit.Children;
                    itemContainer = layoutItems;
                }
                else
                {
                    if (wrapper.Parent == null) return;
                    wrapperContainer = wrapper.Parent.Children;
                    itemContainer = wrapperContainer.Select(x => x.Object).ToList();
                }
            }
            else
            {
                wrapperContainer = GetWrappersContainer(wrapper);
                itemContainer = GetItemsContainer(wrapper);
            }

            var index = wrapperContainer.IndexOf(wrapper);
            var newIndex = index + delta;
            if (newIndex < 0 || newIndex >= wrapperContainer.Count)
                return;

            // move the item
            itemContainer.RemoveAt(index);
            itemContainer.Insert(newIndex, wrapper.Object);

            // sync with wrapper
            wrapperContainer.RemoveAt(index);
            wrapperContainer.Insert(newIndex, wrapper);
        }

        /// <summary>
        /// The event arguments which contains the created item.
        /// </summary>
        public class ItemCreatedEventArgs
        {
            /// <summary>
            /// The constructor.
            /// </summary>
            /// <param name="item">The created item.</param>
            public ItemCreatedEventArgs(object item)
            {
                Item = item;
            }

            /// <summary>
            /// Gets the created item.
            /// </summary>
            public object Item
            {
                get;
                private set;
            }
        }

        #endregion collectionEditor
    }


    internal class ListWrapper<T> : IList
    {
        private readonly IList<T> _list;
        private readonly object _locker = new object();

        public ListWrapper(IList<T> list)
        {
            _list = list;
        }

        public IEnumerator GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            for (int i = 0; i < Count; i++)
            {
                array.SetValue(_list[i], index + i);
            }
        }

        public int Count { get { return _list.Count; } }
        public object SyncRoot { get { return _locker; } }
        public bool IsSynchronized { get { return false; } }
        public int Add(object value)
        {
            _list.Add((T)value);
            return _list.Count - 1;
        }

        public bool Contains(object value)
        {
            return _list.Contains((T)value);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public int IndexOf(object value)
        {
            return _list.IndexOf((T)value);
        }

        public void Insert(int index, object value)
        {
            _list.Insert(index, (T)value);
        }

        public void Remove(object value)
        {
            _list.Remove((T)value);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        public object this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = (T)value; }
        }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }
        public bool IsFixedSize { get { return !_list.IsReadOnly; } }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal interface IGroupTileWrapper : INotifyPropertyChanged
    {
        IGroupTileWrapper Parent { get; }
        object Object { get; }
        bool IsSelected { get; set; }
        bool IsDropTarget { get; set; }
        string DisplayText { get; }
        ObservableCollection<IGroupTileWrapper> Children { get; }
        void OnPropertyChanged(string propertyName);
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal abstract class GroupTileWrapper : IGroupTileWrapper
    {
        private readonly IGroupTileWrapper _parent;
        private readonly object _obj;
        private bool _isSelected;
        private bool _isDropTarget;
        private readonly ObservableCollection<IGroupTileWrapper> _items = new ObservableCollection<IGroupTileWrapper>();

        protected GroupTileWrapper(IGroupTileWrapper parent, object obj)
        {
            _parent = parent;
            _obj = obj;
        }

        public IGroupTileWrapper Parent
        {
            get { return _parent; }
        }

        public object Object
        {
            get { return _obj; }
        }

        public ObservableCollection<IGroupTileWrapper> Children
        {
            get { return _items; }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value) return;

                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public bool IsDropTarget
        {
            get { return _isDropTarget; }
            set
            {
                if (_isDropTarget == value) return;

                _isDropTarget = value;
                OnPropertyChanged("IsDropTarget");
            }
        }

        public virtual string DisplayText
        {
            get
            {
                return _obj.ToString().Replace("C1.Web.Mvc.", "");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class TileWrapper : GroupTileWrapper
    {
        private readonly Tile _tile;

        public TileWrapper(IGroupTileWrapper parent, Tile tile)
            : base(parent, tile)
        {
            _tile = tile;
        }

        public Tile Tile
        {
            get { return _tile; }
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class GroupWrapper : GroupTileWrapper
    {
        private readonly Group _group;

        public GroupWrapper(Group group)
            : base(null, group)
        {
            _group = group;
        }

        public Group Group
        {
            get { return _group; }
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class DropTargetBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isTarget = (bool)value;
            return isTarget ? System.Drawing.Brushes.LightGray : System.Drawing.Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }


    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class ListItemsSplit : GroupWrapper
    {
        public ListItemsSplit()
            : base(null)
        {
        }

        public override string DisplayText
        {
            get
            {
                return "List Items";
            }
        }
    }
}
