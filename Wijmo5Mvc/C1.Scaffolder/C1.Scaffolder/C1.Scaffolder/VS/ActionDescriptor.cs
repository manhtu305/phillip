﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EnvDTE;

namespace C1.Scaffolder.VS
{
    internal class ActionDescriptor : IActionDescriptor
    {
        public ActionDescriptor(CodeFunction codeFunction, MvcProject project)
        {
            CodeFunction = codeFunction;
            Project = project;
            Name = CodeFunction.Name;
            CodeClass = codeFunction.Parent as CodeClass;
        }

        public MvcProject Project
        {
            get;
            private set;
        }

        public CodeFunction CodeFunction
        {
            get;
            private set;
        }

        public MvcProjectItem ProjectItem
        {
            get
            {
                return MvcProjectItem.Get(CodeFunction.ProjectItem, Project);
            }
        }

        public string Name { get; private set; }

        public CodeClass CodeClass { get; private set; }

        private IList<string> _viewBagNames;
        public IEnumerable<string> ViewBagNames
        {
            get
            {
                if (_viewBagNames == null)
                {
                    InitViewBagNames();
                }
                return _viewBagNames;
            }
        }

        private void InitViewBagNames()
        {
            _viewBagNames=new List<string>();
            var text = CodeFunction.GetBodyText();

            var regString = Project.IsRazorPages
                ? @"ViewData\[""(?<Name>.+?)""\]\s*="
                : @"ViewBag.(?<Name>.+?)\s*=";
            var reg = new Regex(regString);
            foreach (Match match in reg.Matches(text))
            {
                _viewBagNames.Add(match.Groups["Name"].Value);
            }
        }
    }
}
