﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Scaffolder.CodeBuilder;
using C1.Scaffolder.Extensions;
using C1.Scaffolder.Scaffolders;
using C1.Web.Mvc.Services;
using EnvDTE;

namespace C1.Scaffolder.VS
{
    internal class ControllerEditor
    {
        public static void ReplaceHolders(IServiceProvider serviceProvider, ProjectItem projectItem, IDictionary<string, string> replaces)
        {
            if (replaces == null || replaces.Count == 0) return;

            projectItem.SafeOpen();

            var generator = serviceProvider.GetService(typeof(ICodeBuilder)) as ICodeBuilder;
            foreach (var keypair in replaces)
            {
                projectItem.Document.ReplaceText(generator.CommentSuffix + keypair.Key, keypair.Value);
            }

            var selection = projectItem.Document.Selection as TextSelection;
            var line = selection == null ? 0 : selection.TopLine;

            projectItem.FormatSelection(-1, -1);

            if (selection != null)
            {
                selection.GotoLine(line);
            }
        }

        public static void AddImport(IServiceProvider serviceProvider, string ns)
        {
            var codeClass = GetCodeClass(serviceProvider);
            if (codeClass == null) return;

            var projectItem = codeClass.ProjectItem;
            projectItem.SafeOpen();

            codeClass.AddImport(ns);
        }

        public static void AddContent(IServiceProvider serviceProvider, string content)
        {
            var codeClass = GetCodeClass(serviceProvider);
            if (codeClass == null) return;

            var projectItem = codeClass.ProjectItem;
            projectItem.SafeOpen();

            var editPoint = codeClass.GetEndPoint(vsCMPart.vsCMPartBody).CreateEditPoint();
            editPoint.InsertAndFormat(projectItem, content);
        }

        public static void AddWebRootPath(IServiceProvider serviceProvider, string name)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var codeClass = scaffolder.Action.CodeClass;
            if (codeClass == null) return;

            var projectItem = codeClass.ProjectItem;
            projectItem.SafeOpen();

            var project = serviceProvider.GetService(typeof(IMvcProject)) as MvcProject;
            if (project.IsAspNetCore)
            {
                codeClass.AddVariable(name, vsCMTypeRef.vsCMTypeRefString);
                string parameterTypeName = "IHostingEnvironment";
                string parameterName = "hostingEnvironment";
                var codeConstructor = GetOrCreateConstructor(scaffolder.Controller, codeClass);
                var codeParameter = GetParameter(codeConstructor, parameterTypeName);

                var generator = serviceProvider.GetService(typeof(ICodeBuilder)) as ICodeBuilder;
                var text = generator.GenerateFieldAssignment(name, (codeParameter == null ? parameterName : codeParameter.Name) + ".WebRootPath") + Environment.NewLine;
                ActionEditor.AddContent(codeConstructor, text);

                // need create parameter on the last, 
                // it may throw exception (infact parameter is created), and cannot add content into it anymore.
                if (codeParameter == null)
                {
                    GetOrCreateParameter(codeConstructor, parameterName, parameterTypeName);
                }
            }
        }

        public static void AddDbContext(IServiceProvider serviceProvider, string name, string typeName)
        {
            var scaffolder = serviceProvider.GetService(typeof (IScaffolderDescriptor)) as ScaffolderDescriptor;
            var codeClass = scaffolder.Action.CodeClass;
            if (codeClass == null) return;

            var projectItem = codeClass.ProjectItem;
            projectItem.SafeOpen();

            var generator = serviceProvider.GetService(typeof(ICodeBuilder)) as ICodeBuilder;
            var project = serviceProvider.GetService(typeof (IMvcProject)) as MvcProject;
            if (project.IsAspNetCore)
            {
                codeClass.AddVariable(name, typeName);
                var codeConstructor = GetOrCreateConstructor(scaffolder.Controller, codeClass);
                var codeParameter = GetParameter(codeConstructor, typeName);

                var text = generator.GenerateFieldAssignment(name, codeParameter == null ? name : codeParameter.Name) + Environment.NewLine;
                ActionEditor.AddContent(codeConstructor, text);

                // need create parameter on the last, 
                // it may throw exception (infact parameter is created), and cannot add content into it anymore.
                if (codeParameter == null)
                {
                    GetOrCreateParameter(codeConstructor, name, typeName);
                }
            }
            else
            {
                var text = generator.GenerateFieldDeclare(typeName, name, true) + Environment.NewLine;
                var editPoint = codeClass.GetStartPoint(vsCMPart.vsCMPartBody).CreateEditPoint();
                editPoint.InsertAndFormat(projectItem, text);
            }
        }

        private static CodeClass GetCodeClass(IServiceProvider serviceProvider)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            if (scaffolder.Action == null) return null;
            return scaffolder.Action.CodeClass;
        }

        private static IControllerDescriptor GetController(IServiceProvider serviceProvider)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            return scaffolder.Controller;
        }

        private static CodeFunction GetOrCreateConstructor(IControllerDescriptor controller, CodeClass codeClass)
        {
            var action = controller.Actions.FirstOrDefault(
                a => a.CodeFunction.Access.HasFlag(vsCMAccess.vsCMAccessPublic) &&
                          a.CodeFunction.FunctionKind == vsCMFunction.vsCMFunctionConstructor);
            CodeFunction codeConstructor;
            if (action == null)
            {
                var suffix = controller.Project.IsRazorPages ? "Model" : "Controller";
                codeConstructor = codeClass.AddFunction(controller.Name + suffix, vsCMFunction.vsCMFunctionConstructor,
                    vsCMTypeRef.vsCMTypeRefVoid, 0, vsCMAccess.vsCMAccessPublic);
                controller.AddAction(new ActionDescriptor(codeConstructor, controller.Project));
            }
            else
            {
                codeConstructor = action.CodeFunction;
            }

            return codeConstructor;
        }

        private static CodeParameter GetParameter(CodeFunction codeFunction, string typeName)
        {
            var codeParameter = codeFunction.Parameters.OfType<CodeParameter>()
                .FirstOrDefault(p => string.Equals(p.Type.AsFullName, typeName, StringComparison.OrdinalIgnoreCase)
                                     || p.Type.AsFullName.EndsWith(typeName, StringComparison.OrdinalIgnoreCase));

            return codeParameter;
        }

        private static CodeParameter GetOrCreateParameter(CodeFunction codeFunction, string name, string typeName)
        {
            var codeParameter = GetParameter(codeFunction, typeName);
            if (codeParameter == null)
            {
                try
                {
                    codeParameter = codeFunction.AddParameter(name, typeName);
                }
                catch
                {
                    // may throw exception "Key already exists in table: ......",
                    // but the parameter is added actually.
                }
            }

            return codeParameter;
        }
    }
}
