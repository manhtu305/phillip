﻿using System.Collections.Generic;
using EnvDTE;

namespace C1.Scaffolder.VS
{
    internal class ControllerDescriptor : IControllerDescriptor
    {
        private readonly IList<CodeType> _types = new List<CodeType>();
        private IList<IActionDescriptor> _actions;
        private IList<string> _memberNames;
        private IDictionary<string, string> _variables;

        public ControllerDescriptor(CodeType type, MvcProject project)
        {
            _types.Add(type);
            var suffix = project.IsRazorPages ? "Model" : "Controller";
            Name = type.Name.Substring(0, type.Name.Length - suffix.Length);
            FullName = type.FullName;
            Project = project;
        }

        public MvcProject Project
        {
            get;
            private set;
        }

        internal void AddCodeType(CodeType type)
        {
            if (_types.Contains(type)) return;
            _types.Add(type);
            _actions = null;
            _memberNames = null;
            _variables = null;
        }

        public void AddAction(IActionDescriptor action)
        {
            if (_actions != null)
            {
                _actions.Add(action);
            }
        }

        public IEnumerable<string> MemberNames
        {
            get
            {
                if (_memberNames == null)
                {
                    InitMembers();
                }

                return _memberNames;
            }
        }

        public IDictionary<string, string> Variables
        {
            get
            {
                if (_variables == null)
                {
                    InitMembers();
                }

                return _variables;
            }
        }

        public IEnumerable<IActionDescriptor> Actions
        {
            get
            {
                if (_actions == null)
                {
                    InitMembers();
                }

                return _actions;
            }
        }

        private void InitMembers()
        {
            _actions = new List<IActionDescriptor>();
            _memberNames = new List<string>();
            _variables = new Dictionary<string,string>();
            foreach (var type in _types)
            {
                foreach (CodeElement member in type.Members)
                {
                    _memberNames.Add(member.Name);

                    var codeFunction = member as CodeFunction;
                    if (codeFunction != null && codeFunction.Access.HasFlag(vsCMAccess.vsCMAccessPublic))
                    {
                        _actions.Add(new ActionDescriptor(codeFunction, Project));
                    }

                    var codeVariable = member as CodeVariable;
                    if (codeVariable != null)
                    {
                        try
                        {
                            var typeName = codeVariable.Type.AsFullName;
                            _variables[member.Name] = typeName;
                        }
                        catch
                        {
                            // do nothing
                        }
                    }
                }
            }
        }

        public string Name { get; private set; }

        public string FullName { get; private set; }
    }
}
