﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using EnvDTE;
using C1.Scaffolder.Extensions;
using C1.Web.Mvc.Services;
using C1.Scaffolder.Scaffolders;
using System.Linq;
using System.Xml;
using Microsoft.VisualStudio.ComponentModelHost;
using NuGet.VisualStudio;

namespace C1.Scaffolder.VS
{
    /// <summary>
    /// The editor contains some methods allow add extra information into a file.
    /// </summary>
    internal class FileEditor
    {
        /// <summary>
        /// Add packages information into a file.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="packages"></param>
        public static void AddPackages(IServiceProvider serviceProvider, IEnumerable<string> packages)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var mvcProject = scaffolder.View.ProjectItem.Project;
            //add package info
            var packageConfigFilePath = "packages.config";
            var projectItemPackageConfig = mvcProject.Source.ProjectItems.GetProjectItem(packageConfigFilePath);
            if (projectItemPackageConfig != null)
            {
                var reader = new StringReader(projectItemPackageConfig.GetAllText());
                var doc = XDocument.Load(reader);

                var packagesNode = doc.XPathSelectElement(@"/packages");
                var version = ProjectFileEditor.GetCurrentVersion(false);
                var netFrw = GetTargetFrameworkName(mvcProject.Source);
                Action<string> ensurePackage = pk =>
                {
#if GRAPECITY
                    pk += ".ja";
#endif
                    var node = packagesNode.XPathSelectElement(string.Format("package[@id='{0}']", pk));
                    if (node != null)//ignore the package already registered
                    {
                        return;
                    }
                    var pkNode = new XElement("package");
                    pkNode.SetAttributeValue("id", pk);
                    pkNode.SetAttributeValue("version", version);
                    pkNode.SetAttributeValue("targetFramework", netFrw);
                    packagesNode.Add(pkNode);
                };

                foreach (var p in packages)
                {
                    ensurePackage(p);
                }

                var writer = new EncodedStringWriter(doc.Declaration.Encoding);
                doc.Save(writer);
                projectItemPackageConfig.ReplaceAllText(writer.ToString());
            }
        }

        private static string GetTargetFrameworkName(Project project)
        {
            string fullName = project.Properties.Item("TargetFrameworkMoniker").Value.ToString();
            if (fullName.StartsWith(".NETFramework"))
            {
                fullName = fullName.Replace(".NETFramework,Version=v", "");
                return "net" + fullName.Replace(".", "");
            }
            return "net452";
        }
        /// <summary>
        /// Insert all 'lines' strings into a file 'item', ignore existed lines
        /// </summary>
        /// <param name="item"></param>
        /// <param name="lines"></param>
        public static void EnsureLines(ProjectItem item, IEnumerable<string> lines)
        {
            var fileLines = ReadLines(item.GetAllText()).ToList();
            var lineList = lines.ToList();
            foreach (var line in fileLines)
            {
                lineList.Remove(line);
            }

            fileLines.AddRange(lineList);
            item.ReplaceAllText(string.Join(Environment.NewLine, fileLines));
        }

        public static void ReplaceAllText(string filePath, string newContentText)
        {
            EnsureFileWriteable(filePath);
            File.WriteAllText(filePath, newContentText);
        }

        private static void EnsureFileWriteable(string filePath)
        {
            if (File.Exists(filePath))
            {
                FileAttributes attr = File.GetAttributes(filePath);
                attr &= ~FileAttributes.ReadOnly;
                File.SetAttributes(filePath, attr);
            }
        }

        public static IEnumerable<string> ReadLines(string text)
        {
            using (var reader = new StringReader(text))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    yield return line;
                }
            }
        }

        /// <summary>
        /// Return a ProjectItem located at 'path', if not exist, create new item then return
        /// </summary>
        /// <param name="project"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static ProjectItem EnsureViewImportsFile(MvcProject project, string path)
        {
            var filePath = Path.Combine(project.ProjectFolder, path);
            var projectItem = project.Source.ProjectItems.GetProjectItem(path);
            if (projectItem == null)
            {
                if (!File.Exists(filePath)) ReplaceAllText(filePath, string.Empty);
                projectItem = project.Source.ProjectItems.AddFromFile(filePath);
            }

            return projectItem;
        }
    }

    /// <summary>
    /// The editor allow edit information in License.licx file
    /// </summary>
    internal class LicenseEditor
    {
        public static void AddLicenseslicx(IServiceProvider serviceProvider, IEnumerable<string> licenses)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var project = scaffolder.View.ProjectItem.Project;
            if (project.IsAspNetCore) return;
            ProjectItem licenseItem = GetFileLicenselicxItem(serviceProvider);
            var lines = licenses.Select(l => string.Format("{0}.LicenseDetector, {1}", l.Replace("FlexSheet", "Sheet"), l));
            FileEditor.EnsureLines(licenseItem, lines);
        }

        private static ProjectItem GetFileLicenselicxItem(IServiceProvider serviceProvider)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var view = scaffolder.View;
            var project = view.ProjectItem.Project;
            string fileName = "licenses.licx";
            var viewsFolder = project.IsCs ? "Properties" : "";
            var path = Path.Combine(viewsFolder, fileName);

            var fullPath = Path.Combine(project.ProjectFolder, path);
            var projectItem = project.Source.ProjectItems.GetProjectItem(path);
            if (projectItem == null)
            {
                if (!File.Exists(fullPath))
                {
                    FileEditor.ReplaceAllText(fullPath, string.Empty);
                }
                //ProjectFileEditor.AddLicenselicxFile(project, path);
                projectItem = project.Source.ProjectItems.AddFromFile(fullPath);
            }

            return projectItem;
        }
    }

    /// <summary>
    /// The editor edit project file (*.vbproj && *.csproj)
    /// </summary>
    internal class ProjectFileEditor
    {
        /// <summary>
        /// Add assembly references for the project file.
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="assemblies"></param>
        public static void AddReferenceAssembly(IServiceProvider serviceProvider, IEnumerable<string> assemblies)
        {
            var scaffolder = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            var project = scaffolder.View.ProjectItem.Project;
            //bool installedPkg = true;
            try
            {
                var componentModel = (IComponentModel)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SComponentModel));
                IVsPackageInstallerServices installerServices = componentModel.GetService<IVsPackageInstallerServices>();
                foreach (string ns in assemblies)
                {
                    string strNS = ns;
#if GRAPECITY
                    strNS += ".ja";
#endif
                    if (!installerServices.IsPackageInstalled(project.Source, strNS))
                    {
                        var installer = componentModel.GetService<IVsPackageInstaller>();
                        installer.InstallPackage(null, project.Source, strNS, (System.Version)null, false);
                    }
                }

                if (project.IsAspNetCore && project.EntityFrameworkCoreVersion >= new Version("3.0"))
                {
                    string strPackage = "System.Data.SqlClient";
                    if (!installerServices.IsPackageInstalled(project.Source, strPackage))
                    {
                        var installer = componentModel.GetService<IVsPackageInstaller>();
                        installer.InstallPackage(null, project.Source, strPackage, (System.Version)null, false);
                    }
                }
            }

            catch (Exception ex)
            {
                //installedPkg = false;
                // Log the failure
            }
        }

        /// <summary>
        /// Add assembly references for the project file.
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="assemblies"></param>
        public static void AddLicenselicxFile(MvcProject project, string licenseFilePath)
        {
            var reader = new StreamReader(project.ProjectFile);
            XDocument doc = XDocument.Load(reader);

            reader.Close();
            //find the parent node of reference
            XElement parentReference = doc.XPathSelectElement(@"/child::node()[local-name()='Project']");
            if (parentReference != null)
            {
                //add reference dll
                XElement itemGroup = new XElement("ItemGroup");
                itemGroup.Name = parentReference.Name.Namespace + itemGroup.Name.LocalName;//for ignore write empty namespace
                parentReference.Add(itemGroup);
                XElement contentNode = new XElement("Content",
                new XAttribute("Include", licenseFilePath));
                contentNode.Name = itemGroup.Name.Namespace + contentNode.Name.LocalName;//for ignore write empty namespace
                itemGroup.Add(contentNode);
                var writer = new EncodedStringWriter(doc.Declaration != null ? doc.Declaration.Encoding : null);
                doc.Save(writer);
                FileEditor.ReplaceAllText(project.ProjectFile, writer.ToString());
            }
        }

        /// <summary>
        /// Get current version. Temporary use version of scaffolder and set for MVC control's dll
        /// </summary>
        /// <param name="isAspNetCore"></param>
        /// <returns></returns>
        public static string GetCurrentVersion(bool isAspNetCore)
        {
            Version scaffolderVersion = new Version(AssemblyInfo.Version);
#if DEBUG
            return isAspNetCore ? "1.0.20192.228": "4.5.20192.228";
#else
            Version mvcVersion = new Version(isAspNetCore ? 1 : 4, scaffolderVersion.Minor, scaffolderVersion.Build, scaffolderVersion.Revision);
            return mvcVersion.ToString();
#endif
        }

    }
}
