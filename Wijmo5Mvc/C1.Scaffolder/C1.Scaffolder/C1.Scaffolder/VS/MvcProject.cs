﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using C1.Scaffolder.Extensions;
using C1.Web.Mvc.Services;
using EnvDTE;
using EnvDTE80;

namespace C1.Scaffolder.VS
{
    internal class MvcProject : IMvcProject
    {
        private readonly Project _source;
        private IEnumerable<CodeType> _codeTypes;
        private readonly bool _isInsert;
        private bool? _isRazorPages;
        private string _area;
        private string _selectedPagefolder;
        private MvcProjectItem _activeItem;

        public MvcProject(Project source, bool isInsert)
        {
            _source = source;
            _isInsert = isInsert;
        }

        public bool IsAspNetCore
        {
            get { return _source.IsAspNetCore(); }
        }

        public bool IsRazorPages
        {
            get
            {
                if (_isRazorPages == null)
                {
                    _isRazorPages = GetIsRazorPages();
                }
                return _isRazorPages.Value;
            }
        }

        private bool GetIsRazorPages()
        {
            var pagesFolder = string.IsNullOrEmpty(SelectedAreaName) ? "Pages" : "Areas/" + SelectedAreaName + "/Pages";
            bool hasPagesFolder = ItemExists(pagesFolder);
            return hasPagesFolder && IsAspNetCore && !GetIsInControllersViews();
        }

        private bool GetIsInControllersViews()
        {
            var projectItem = GetSelectedProjectItem();
            if (projectItem == null) return false;

            var relativeName = projectItem.RelativeName;
            if (string.IsNullOrEmpty(relativeName)) return false;

            var names = relativeName.Split('/', '\\');
            var folder = names[0];
            if (names.Length > 2 && string.Equals(names[0], "areas", StringComparison.OrdinalIgnoreCase))
            {
                folder = names[2];
            }

            if (string.Equals(folder, "views", StringComparison.OrdinalIgnoreCase)
                || string.Equals(folder, "controllers", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        private MvcProjectItem GetSelectedProjectItem()
        {
            var dte2 = Source.DTE as DTE2;
            if (dte2 == null) return null;

            var selectedItems = dte2.ToolWindows.SolutionExplorer.SelectedItems as Array;
            if (selectedItems == null || selectedItems.Length != 1) return null;

            var selectedItem = selectedItems.GetValue(0) as UIHierarchyItem;
            if (selectedItem == null) return null;

            var projectItem = selectedItem.Object as ProjectItem;
            if (projectItem == null) return null;

            return MvcProjectItem.Get(projectItem, this);
        }

        public Version EntityFrameworkCoreVersion
        {
            get { return _source.EntityFrameworkCoreVersion(); }
        }

        public bool IsCs
        {
            get { return _source.IsCs(); }
        }

        public VsLanguage VsLanguage
        {
            get
            {
                if (ProjectFile.EndsWith(".vbproj", StringComparison.OrdinalIgnoreCase))
                {
                    return VsLanguage.Vb;
                }

                return VsLanguage.CSharp;
            }
        }

        public string ProjectFolder
        {
            get
            {
                try
                {
                    return Source.Properties.Item("FullPath").Value as string;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public string ProjectFile
        {
            get
            {
                try
                {
                    return Source.FileName;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        public bool ItemExists(string name)
        {
            var path = Path.Combine(ProjectFolder, name);
            return Directory.Exists(path) || File.Exists(path);
        }

        public Project Source
        {
            get
            {
                return _source;
            }
        }

        public string SelectedAreaName
        {
            get
            {
                return _area ?? (_area = GetSelectedArea());
            }
        }

        private String GetSelectedArea()
        {
            var mvcProjectItem = _isInsert ? ActiveItem : GetSelectedProjectItem();
            if (mvcProjectItem == null) return string.Empty;

            if (mvcProjectItem.RelativeName.StartsWith("areas", StringComparison.OrdinalIgnoreCase))
            {
                return mvcProjectItem.RelativeName.Split('/', '\\')[1];
            }

            return string.Empty;
        }

        public string SelectedPageFolder
        {
            get { return _selectedPagefolder ?? (_selectedPagefolder = GetSelectedPageFolder()); }
        }

        private String GetSelectedPageFolder()
        {
            var mvcProjectItem = _isInsert ? ActiveItem : GetSelectedProjectItem();
            if (mvcProjectItem == null) return string.Empty;

            var relativeName = mvcProjectItem.RelativeName;
            var names = relativeName.Split('/', '\\');
            if (names.Length > 1 && string.Equals("areas", names[0], StringComparison.OrdinalIgnoreCase))
            {
                names = names.Skip(2).ToArray();
            }

            if (names.Length > 0 && string.Equals("pages", names[0], StringComparison.OrdinalIgnoreCase))
            {
                var folders = names.Skip(1).ToArray();
                if (mvcProjectItem.IsFile && folders.Length > 0)
                {
                    folders = folders.Take(folders.Length - 1).ToArray();
                }
                return Path.Combine(folders);
            }

            return string.Empty;
        }

        internal MvcProjectItem ActiveItem
        {
            get
            {
                if (_activeItem == null)
                {
                    var item = Source.DTE.ActiveDocument == null ? null : Source.DTE.ActiveDocument.ProjectItem;
                    if (item == null) return null;
                    _activeItem = MvcProjectItem.Get(item, this);
                }
                return _activeItem;
            }
        }

        private readonly IDictionary<ProjectItem, MvcProjectItem> _projectItemsCache = new Dictionary<ProjectItem, MvcProjectItem>();

        public IDictionary<ProjectItem, MvcProjectItem> ItemsCache
        {
            get
            {
                return _projectItemsCache;
            }
        }

        public IEnumerable<CodeType> CodeTypes
        {
            get { return _codeTypes ?? (_codeTypes = GetCodeTypes(Items)); }
        }

        private IEnumerable<MvcProjectItem> _items;
        public IEnumerable<MvcProjectItem> Items
        {
            get { return _items ?? (_items = GetProjectItems(Source.ProjectItems)); }
        }

        public static IEnumerable<CodeType> GetCodeTypes(IEnumerable<MvcProjectItem> items)
        {
            if (items == null) yield break;
            foreach (var item in items)
            {
                var subItems = GetCodeTypes(item.Items);
                foreach (var subItem in subItems)
                {
                    yield return subItem;
                }

                if (!item.IsCode) continue;

                FileCodeModel fileCodeModel;
                try
                {
                    fileCodeModel = item.Source.FileCodeModel;
                }
                catch (Exception)
                {
                    continue;
                }

                if (fileCodeModel == null)
                    continue;

                foreach (CodeElement codeElement in fileCodeModel.CodeElements)
                {
                    if (codeElement.IsCodeType)
                    {
                        var codeType = codeElement as CodeType;
                        if (codeType == null) continue;
                        yield return codeType;
                    }

                    if (codeElement.Kind == vsCMElement.vsCMElementNamespace)
                    {
                        var ns = codeElement as CodeNamespace;
                        if (ns == null) continue;

                        foreach (CodeElement member in ns.Members)
                        {
                            if (!member.IsCodeType) continue;

                            var codeType = member as CodeType;
                            if (codeType == null) continue;
                            yield return codeType;
                        }
                    }
                }
            }
        }

        public static MvcProjectItem FindProjectItem(IEnumerable<MvcProjectItem> items, params string[] nodes)
        {
            var item = items.FirstOrDefault(i => i.FileName.Equals(nodes[0], StringComparison.OrdinalIgnoreCase));
            if (item == null) return null;
            var restNodes = nodes.Skip(1).ToArray();
            return restNodes.Any() ? FindProjectItem(item.Items, restNodes) : item;
        }

        public IEnumerable<MvcProjectItem> GetProjectItems(ProjectItems projectItems, bool recursive = false)
        {
            if (projectItems == null) yield break;

            foreach (ProjectItem item in projectItems)
            {
                var mvcItem = MvcProjectItem.Get(item, this);
                if (mvcItem == null) continue;

                yield return mvcItem;

                if (recursive)
                {
                    foreach (var childItem in mvcItem.Items)
                        yield return childItem;
                }
            }
        }

        public IControllerDescriptor GetController(IViewDescriptor viewDescriptor, out IActionDescriptor currentAction)
        {
            return GetController(viewDescriptor.ControllerName, viewDescriptor.ActionName, viewDescriptor.AreaName, out currentAction);
        }

        private IControllerDescriptor GetController(string name, string actionName, string areaName, out IActionDescriptor currentAction)
        {
            currentAction = null;
            var currentController = GetControllerDescriptor(name, areaName);
            if (currentController == null) return null;
            currentAction = currentController.Actions
                .FirstOrDefault(a => string.Equals(a.Name, actionName, StringComparison.OrdinalIgnoreCase));
            return currentController;
        }

        private IControllerDescriptor GetControllerDescriptor(string name, string areaName)
        {
            return GetControllerDescriptors(areaName)
                .FirstOrDefault(i => string.Equals(i.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        private readonly IDictionary<string, IEnumerable<ControllerDescriptor>> _controllerDescripterCache =
            new Dictionary<string, IEnumerable<ControllerDescriptor>>(StringComparer.OrdinalIgnoreCase);

        public IEnumerable<IControllerDescriptor> GetControllerDescriptors(string areaName = null)
        {
            areaName = areaName ?? string.Empty;
            IEnumerable<ControllerDescriptor> result;
            if (!_controllerDescripterCache.TryGetValue(areaName, out result))
            {
                var areaItem = FindProjectItem(Items, "areas", areaName);
                IEnumerable<CodeType> codeTypes = areaItem != null ? areaItem.CodeTypes : CodeTypes;

                var types = codeTypes.Where(c => IsRazorPages ? c.IsPageModel() : c.IsController());
                var controllerDescriptors =
                    new Dictionary<string, ControllerDescriptor>(StringComparer.OrdinalIgnoreCase);
                foreach (var type in types)
                {
                    ControllerDescriptor controllerDescriptor;
                    if (!controllerDescriptors.TryGetValue(type.FullName, out controllerDescriptor))
                    {
                        controllerDescriptor = new ControllerDescriptor(type, this);
                        controllerDescriptors[type.FullName] = controllerDescriptor;
                    }
                    else
                    {
                        controllerDescriptor.AddCodeType(type);
                    }
                }

                result = controllerDescriptors.Values;
                _controllerDescripterCache.Add(areaName, result);
            }

            return result;
        }
    }

    internal class CodeTypeComparer : IEqualityComparer<CodeType>
    {
        public bool Equals(CodeType x, CodeType y)
        {
            if (x == null) return y == null;
            if (y == null) return false;

            return x.Name == y.Name
                   && GetNamespace(x) == GetNamespace(y);
        }

        public int GetHashCode(CodeType obj)
        {
            return obj.Name.GetHashCode();
        }

        private string GetNamespace(CodeType codeType)
        {
            return (codeType == null || codeType.Namespace == null || string.IsNullOrEmpty(codeType.Namespace.FullName))
                ? string.Empty
                : codeType.Namespace.FullName;
        }
    }
}
