﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using EnvDTE;
using C1.Scaffolder.Extensions;

namespace C1.Scaffolder.VS
{
    internal class WebConfigEditor
    {
        public static void AddImports(ProjectItem item, IEnumerable<string> imports)
        {
            var reader = new StringReader(item.GetAllText());
            var doc = XDocument.Load(reader);
            AddImports(doc, imports);
            var writer = new EncodedStringWriter(doc.Declaration.Encoding);
            doc.Save(writer);
            item.ReplaceAllText(writer.ToString());
        }

        private static void AddImports(XDocument doc, IEnumerable<string> imports)
        {
            var namespacesNode = doc.XPathSelectElement(@"/configuration/system.web.webPages.razor/pages/namespaces");

            Action<string> ensureNamespace = ns =>
            {
                var node = namespacesNode.XPathSelectElement(string.Format("add[@namespace='{0}']", ns));
                if (node != null)
                {
                    return;
                }
                var nsNode = new XElement("add");
                nsNode.SetAttributeValue("namespace", ns);
                namespacesNode.Add(nsNode);
            };

            foreach (var ns in imports)
            {
                ensureNamespace(ns);
            }
        }
    }

    internal class EncodedStringWriter : StringWriter
    {
        private System.Text.Encoding _encoding;

        public EncodedStringWriter(System.Text.Encoding encoding)
        {
            _encoding = encoding;
        }

        public EncodedStringWriter(string encoding)
        {
            if (!string.IsNullOrEmpty(encoding))
            {
                try
                {
                    _encoding = System.Text.Encoding.GetEncoding(encoding);
                }
                catch
                {
                    _encoding = null;
                }
            }
        }

        public override System.Text.Encoding Encoding
        {
            get
            {
                return _encoding;
            }
        }
    }
}
