﻿using System;
using System.Collections.Generic;
using System.IO;
using EnvDTE;

namespace C1.Scaffolder.VS
{
    internal class MvcProjectItem
    {
        private readonly ProjectItem _source;
        private ViewDescriptor _viewDescriptor;

        public static MvcProjectItem Get(ProjectItem source, MvcProject project)
        {
            if (source == null) return null;

            MvcProjectItem item;
            if (!project.ItemsCache.TryGetValue(source, out item))
            {
                item = new MvcProjectItem(source, project);
                project.ItemsCache.Add(source, item);
            }

            return item;
        }

        public bool IsFile
        {
            get
            {
                return File.Exists(FullName);
            }
        }

        private MvcProjectItem(ProjectItem source, MvcProject project)
        {
            _source = source;
            Project = project;
        }


        private IEnumerable<MvcProjectItem> _items;
        public IEnumerable<MvcProjectItem> Items
        {
            get
            {
                return _items ?? (_items = Project.GetProjectItems(
                    Source.SubProject != null ? Source.SubProject.ProjectItems : Source.ProjectItems));
            }
        }

        public MvcProject Project
        {
            get;
            private set;
        }

        public ProjectItem Source
        {
            get
            {
                return _source;
            }
        }

        public string FileName
        {
            get
            {
                return Path.GetFileName(Source.Name);
            }
        }

        public string FullName
        {
            get
            {
                return (string)Source.Properties.Item("FullPath").Value;
            }
        }

        public string RelativeName
        {
            get
            {
                if (string.IsNullOrEmpty(FullName)) return string.Empty;

                return FullName.Substring(Project.ProjectFolder.TrimEnd('/', '\\').Length).TrimStart('/', '\\');
            }
        }

        public bool IsView
        {
            get
            {
                if (!IsFile) return false;
                var extesion = Project.VsLanguage == VsLanguage.CSharp ? "cshtml" : "vbhtml";
                return FileName.EndsWith(extesion, StringComparison.OrdinalIgnoreCase);
            }
        }

        public bool IsCode
        {
            get
            {
                if (!IsFile) return false;
                return FileName.EndsWith(".cs", StringComparison.InvariantCultureIgnoreCase)
                        || FileName.EndsWith(".vb", StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public IViewDescriptor ViewDescriptor
        {
            get
            {
                if (!IsView) return null;

                return _viewDescriptor ?? (_viewDescriptor = new ViewDescriptor(this));
            }
        }

        private IEnumerable<CodeType> _codeTypes;
        public IEnumerable<CodeType> CodeTypes
        {
            get
            {
                if (_codeTypes == null)
                {
                    var items = new List<MvcProjectItem>();
                    if (!IsFile)
                    {
                        items.AddRange(Items);
                    }
                    else if (IsCode)
                    {
                        items.Add(this);
                    }

                    _codeTypes = MvcProject.GetCodeTypes(items);
                }

                return _codeTypes;
            }
        }
    }
}
