﻿using System;
using System.IO;
using System.Linq;
using C1.Scaffolder.Extensions;
using EnvDTE;
using EnvDTE80;

namespace C1.Scaffolder.VS
{
    internal static class Extensions
    {
        /// <summary>
        /// Returns true if given <paramref name="fullTypeName"/> is a matching type name
        ///             or one of the base type names in the hierarchy of the .NET type represented by
        ///             <paramref name="codeType"/>.
        ///             This does not account for language-specific type representations,
        ///             so will not be accurate for types with language-specific short names, such as List(Of Integer).
        /// 
        /// </summary>
        /// <param name="codeType">Code type which needs to be determined if it's a derived type.</param><param name="fullTypeName">The fullTypeName provided must be language representation
        ///             of the type but not the Type.FullName from reflection. For example, the fullTypeName
        ///             passed in VB must be "System.Data.Entity.DbSet(Of MyNamespace.MyModel)" but not
        ///             System.Data.Entity.DbSet`1[MyNamespace.MyModel].</param>
        public static bool IsDerivedType(this CodeType codeType, string fullTypeName)
        {
            if (codeType == null)
                throw new ArgumentNullException("codeType");
            if (!codeType.IsCodeType)
                return false;
            if (string.Equals(codeType.FullName, fullTypeName, StringComparison.OrdinalIgnoreCase))
                return true;
            Func<CodeType, bool> predicate = type => type.IsDerivedType(fullTypeName);
            var codeClass2 = codeType as CodeClass2;
            if (codeClass2 != null && codeClass2.ImplementedInterfaces.OfType<CodeType>().Any(predicate))
                return true;

            return codeType.Bases.OfType<CodeType>().Any(predicate);
        }

        public static bool IsController(this CodeType codeType)
        {
            if (!codeType.Name.EndsWith("Controller")) return false;

            return codeType.IsDerivedType("Microsoft.AspNetCore.Mvc.Controller") ||
                   codeType.IsDerivedType("System.Web.Mvc.Controller");
        }

        public static bool IsPageModel(this CodeType codeType)
        {
            if (!codeType.Name.EndsWith("Model")) return false;

            return codeType.IsDerivedType("Microsoft.AspNetCore.Mvc.RazorPages.PageModel");
        }

        public static ProjectItem GetOrCreateFolder(this ProjectItems projectItems, string relativePath)
        {
            string firstPart, remainParts;
            SeperatePath(relativePath, out firstPart, out remainParts);
            if (string.IsNullOrEmpty(firstPart)) return null;

            var projectItem = projectItems.GetOrCreateFolderByName(firstPart);
            if (projectItem == null) return null;

            return string.IsNullOrEmpty(remainParts)
                ? projectItem
                : projectItem.ProjectItems.GetOrCreateFolder(remainParts);
        }

        public static ProjectItem GetOrCreateFolderByName(this ProjectItems projectItems, string name)
        {
            var projectItem = projectItems.GetProjectItemByName(name);
            if (projectItems == null)
            {
                projectItem = projectItems.AddFolder(name);
            }
            return projectItem;
        }

        public static ProjectItem GetProjectItem(this ProjectItems projectItems, string relativePath)
        {
            string firstPart, remainParts;
            SeperatePath(relativePath, out firstPart, out remainParts);
            if (string.IsNullOrEmpty(firstPart)) return null;

            var projectItem = GetProjectItemByName(projectItems, firstPart);
            if (projectItem == null) return null;

            return string.IsNullOrEmpty(remainParts) ? projectItem : GetProjectItem(projectItem.ProjectItems, remainParts);
        }

        public static ProjectItem GetProjectItemByName(this ProjectItems projectItems, string name)
        {
            return projectItems.OfType<ProjectItem>()
                .FirstOrDefault(item => string.Equals(item.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        private static void SeperatePath(string relativePath, out string firstPart, out string remainParts)
        {
            relativePath = relativePath.Trim(Path.DirectorySeparatorChar);
            firstPart = remainParts = string.Empty;
            if (string.IsNullOrEmpty(relativePath)) return;

            var index = relativePath.IndexOf(Path.DirectorySeparatorChar);
            if (index > 0)
            {
                firstPart = relativePath.Substring(0, index);
                remainParts = relativePath.Substring(index + 1).TrimStart(Path.DirectorySeparatorChar);
            }
            else
            {
                firstPart = relativePath;
            }
        }

        public static void AddImport(this CodeClass codeClass, string ns)
        {
            codeClass.ProjectItem.SafeOpen();

            var codeNamespace = codeClass.Namespace as CodeNamespace;
            if (codeNamespace != null && codeNamespace.HasImport(ns)) return;

            FileCodeModel2 fileCodeModel;
            try
            {
                fileCodeModel = codeClass.ProjectItem.FileCodeModel as FileCodeModel2;
            }
            catch (Exception)
            {
                return;
            }

            if (fileCodeModel == null) return;

            if (!fileCodeModel.HasImport(ns))
            {
                fileCodeModel.AddImport(ns);
            }
        }

        private static bool HasImport(this CodeNamespace codeNamespace, string ns)
        {
            foreach (CodeElement element in codeNamespace.Members)
            {
                if (element.Kind == vsCMElement.vsCMElementImportStmt)
                {
                    var codeImport = (CodeImport) element;
                    if (string.Equals(codeImport.Namespace, ns, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }
            }

            var parentNamespace = codeNamespace.Parent as CodeNamespace;
            if (parentNamespace != null)
            {
                return parentNamespace.HasImport(ns);
            }

            return false;
        }

        private static bool HasImport(this FileCodeModel codeModel, string ns)
        {
            foreach (CodeElement element in codeModel.CodeElements)
            {
                if (element.Kind == vsCMElement.vsCMElementImportStmt)
                {
                    var codeImport = (CodeImport)element;
                    if (string.Equals(codeImport.Namespace, ns, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static void FormatSelection(this ProjectItem projectItem, int startLine, int endLine)
        {
            projectItem.Open().Activate();
            var textSelection = projectItem.Document.Selection as TextSelection;
            if (textSelection == null) return;

            if (startLine == -1 && endLine == -1)
            {
                textSelection.SelectAll();
            }
            else
            {
                textSelection.GotoLine(startLine, false);
                textSelection.LineDown(true, endLine - startLine);
            }

            try
            {
                projectItem.DTE.ExecuteCommand("Edit.FormatSelection");
            }
            catch
            {
                // do nothing
            }
        }

        public static void InsertAndFormat(this EditPoint editPoint, ProjectItem projectItem, string text)
        {
            var line = editPoint.Line;
            editPoint.Insert(text);
            projectItem.FormatSelection(line, editPoint.Line);
        }

        public static string GetBodyText(this CodeFunction codeFunction)
        {
            var projectItem = codeFunction.ProjectItem;
            if (projectItem == null) return string.Empty;
            projectItem.Open();

            var selection = projectItem.Document.Selection as TextSelection;
            if (selection == null) return string.Empty;

            selection.MoveToPoint(codeFunction.GetStartPoint(vsCMPart.vsCMPartBody));
            selection.MoveToPoint(codeFunction.GetEndPoint(vsCMPart.vsCMPartBody), true);
            return selection.Text;
        }
    }
}
