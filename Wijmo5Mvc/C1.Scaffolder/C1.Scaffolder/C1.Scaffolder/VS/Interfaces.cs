﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnvDTE;

namespace C1.Scaffolder.VS
{
    internal interface IControllerDescriptor
    {
        MvcProject Project { get; }
        IEnumerable<string> MemberNames { get; }
        IDictionary<string, string> Variables { get; }
        IEnumerable<IActionDescriptor> Actions { get; }
        string Name { get; }
        string FullName { get; }
        void AddAction(IActionDescriptor action);
    }

    internal interface IActionDescriptor
    {
        MvcProject Project { get; }
        CodeFunction CodeFunction { get; }
        MvcProjectItem ProjectItem { get; }
        string Name { get; }
        CodeClass CodeClass { get; }
        IEnumerable<string> ViewBagNames { get; }
    }

    internal interface IViewDescriptor
    {
        string Name { get; }
        MvcProjectItem ProjectItem { get; }
        string ControllerName { get; }
        string ActionName { get; }
        string AreaName { get; }
    }
}
