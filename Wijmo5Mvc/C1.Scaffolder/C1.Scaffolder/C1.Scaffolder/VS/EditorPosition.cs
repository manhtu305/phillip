﻿namespace C1.Scaffolder.VS
{
    enum EditorPosition
    {
        Current,
        StartOfDocument,
        EndOfDocument
    }
}
