﻿using System;
using System.Collections.Generic;
using EnvDTE;
using System.Linq;
using Microsoft.AspNet.Scaffolding;
using VSLangProj;

namespace C1.Scaffolder.Extensions
{
    /// <summary>
    /// Defines extensions for <see cref="Project"/>.
    /// </summary>
    public static class ProjectExtensions
    {
        /// <summary>
        /// Gets the default namespace of the project.
        /// </summary>
        /// <param name="project">The <see cref="Project"/>.</param>
        /// <returns>The default namespace of the project.</returns>
        public static string GetDefaultNamespace(this Project project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project");
            }

            var defNamespace = project.GetPropertyValue<string>("DefaultNamespace");
            if (!string.IsNullOrEmpty(defNamespace))
            {
                return defNamespace;
            }

            return project.GetPropertyValue<string>("RootNamespace");
        }

        /// <summary>
        /// Gets the property value of the project.
        /// </summary>
        /// <typeparam name="TValue">The type of the property value.</typeparam>
        /// <param name="project">The <see cref="Project"/>.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>The value of the property defined in the project.</returns>
        public static TValue GetPropertyValue<TValue>(this Project project, string propertyName)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project");
            }
            try
            {
                var property = project.Properties.Item(propertyName);
                if (property != null)
                {
                    return (TValue)property.Value;
                }
            }
            catch (ArgumentException)
            {
            }
            return default(TValue);
        }

        /// <summary>
        /// Gets a value indicating whether the project is ASP.NET Core or not.
        /// </summary>
        /// <param name="project">The <see cref="Project"/>.</param>
        /// <returns>true if it's an ASP.NET Core project; otherwise, false.</returns>
        public static bool IsAspNetCore(this Project project)
        {
            // for VS2015, the AspNet5 (core) project file is "*.xproj".
            if (project.FullName.ToLower().EndsWith(".xproj"))
                return true;

            // for VS2017, the AspNet5 (core) project file changes to "*.csproj".
            // We think it's an AspNet5 (core) project if it has the reference "Microsoft.AspNetCore.*".
            var vsproject = project.Object as VSProject;
            if (vsproject != null)
            {
                return vsproject.References.Cast<Reference>()
                    .Any(reference => reference.Name.ToLower().StartsWith("microsoft.aspnetcore."));
            }

            return false;
        }

        /// <summary>
        /// Gets the version of Microsoft.EntityFrameworkCore in AspNet Core project.
        /// </summary>
        /// <param name="project">The <see cref="Project"/>.</param>
        /// <returns>The version of Microsoft.EntityFrameworkCore. null if not found.</returns>
        public static Version EntityFrameworkCoreVersion(this Project project)
        {
            var vsproject = project.Object as VSProject;
            if (vsproject == null) return null;

            var efc = vsproject.References.Cast<Reference>()
                .Where(reference => reference.Name.ToLower().StartsWith("microsoft.entityframeworkcore"))
                .FirstOrDefault();

            return efc == null ? null : new Version(efc.Version);
        }

        /// <summary>
        /// Gets a value indicating whether the project is a CSharp project or not.
        /// </summary>
        /// <param name="project">The <see cref="Project"/>.</param>
        /// <returns>true if it's a CSharp project; otherwise, false.</returns>
        public static bool IsCs(this Project project)
        {
            return project.CodeModel.Language == CodeModelLanguageConstants.vsCMLanguageCSharp;
        }

        /// <summary>
        /// Gets all code types in the project.
        /// </summary>
        /// <param name="project">The <see cref="Project"/>.</param>
        /// <param name="serviceProvider">The service provider.</param>
        /// <returns>The collection of all code types in the project.</returns>
        public static IEnumerable<CodeType> GetAllCodeTypes(this Project project, IServiceProvider serviceProvider)
        {
            var vsVersion = project.DTE.Version;
            // VS2017 returns few classes by ICodeTypeService, try to get all classes from files.
            if (serviceProvider == null || vsVersion.StartsWith("15.0"))
            {
                try
                {
                    return GetAllCodeTypesByFiles(project).ToArray();
                }
                catch
                {
                    // cannot get code types by files, try ICodeTypeService.
                }
            }

            var codeTypeService = (ICodeTypeService) serviceProvider.GetService(typeof (ICodeTypeService));
            return codeTypeService.GetAllCodeTypes(project);
        }

        private static IEnumerable<CodeType>  GetAllCodeTypesByFiles(Project project)
        {
            var allCodeItems = GetProjectItems(project.ProjectItems).Where(IsCodeItem).ToArray();

            foreach (var codeItem in allCodeItems)
            {
                FileCodeModel fileCodeModel;
                try
                {
                    fileCodeModel = codeItem.FileCodeModel;
                }
                catch (Exception)
                {
                    continue;
                }
                if (fileCodeModel == null)
                    continue;

                foreach (CodeElement codeElement in fileCodeModel.CodeElements)
                {
                    if (codeElement.IsCodeType)
                    {
                        var codeType = codeElement as CodeType;
                        if (codeType == null) continue;
                        yield return codeType;
                    }

                    if (codeElement.Kind == vsCMElement.vsCMElementNamespace)
                    {
                        var ns = codeElement as CodeNamespace;
                        if (ns == null) continue;

                        foreach (CodeElement member in ns.Members)
                        {
                            if (!member.IsCodeType) continue;

                            var codeType = member as CodeType;
                            if (codeType == null) continue;
                            yield return codeType;
                        }
                    }
                }
            }
        }

        private static IEnumerable<ProjectItem> GetProjectItems(ProjectItems projectItems)
        {
            foreach (ProjectItem item in projectItems)
            {
                yield return item;

                if (item.SubProject != null)
                {
                    foreach (ProjectItem childItem in GetProjectItems(item.SubProject.ProjectItems))
                        yield return childItem;
                }
                else
                {
                    foreach (ProjectItem childItem in GetProjectItems(item.ProjectItems))
                        yield return childItem;
                }
            }
        }

        private static bool IsCodeItem(ProjectItem item)
        {
            return item != null &&
                (item.Name.EndsWith(".cs", StringComparison.InvariantCultureIgnoreCase)
                || item.Name.EndsWith(".vb", StringComparison.InvariantCultureIgnoreCase));
        }

        public static void SafeOpen(this ProjectItem item)
        {
            try
            {
                item.Open().Activate();
            }
            catch
            {
            }
        }

        public static void SafeSave(this ProjectItem item, bool ignoreReadOnly = false)
        {
            if (item.Saved) return;

            if(ignoreReadOnly && item.Document.ReadOnly) return;

            try
            {
                item.Document.Save();
            }
            catch
            {
            }
        }

        public static string GetAllText(this ProjectItem item)
        {
            item.SafeOpen();
            var textDocument = item.Document.Object("TextDocument") as TextDocument;
            var editStart = textDocument.StartPoint.CreateEditPoint();
            return editStart.GetText(textDocument.EndPoint);
        }

        public static void ReplaceAllText(this ProjectItem item, string text)
        {
            item.SafeOpen();
            var textDocument = item.Document.Object("TextDocument") as TextDocument;
            var editStart = textDocument.StartPoint.CreateEditPoint();
            editStart.ReplaceText(textDocument.EndPoint, text, -1);
            item.SafeSave();
        }
    }

    public class CodeTypeComparer : IEqualityComparer<CodeType>
    {
        public bool Equals(CodeType x, CodeType y)
        {
            if (x == null) return y == null;
            if (y == null) return false;

            return x.Name == y.Name
                   && GetNamespace(x) == GetNamespace(y);
        }

        public int GetHashCode(CodeType obj)
        {
            return obj.Name.GetHashCode();
        }

        private string GetNamespace(CodeType codeType)
        {
            return (codeType == null || codeType.Namespace == null || string.IsNullOrEmpty(codeType.Namespace.FullName))
                ? string.Empty
                : codeType.Namespace.FullName;
        }
    }
}
