﻿using System.ComponentModel;
using System.ComponentModel.Design;
using C1.Util.Licensing;

namespace C1.Scaffolder
{
    internal static class LicenseHelper
    {
        private static readonly LicenseContext DesigntimeLicenseContext = new DesigntimeLicenseContext();
        private static readonly MvcLicenseDetector MvcLicenseDetector = new MvcLicenseDetector();

        public static void Validate()
        {
            var originLicenseContext = LicenseManager.CurrentContext;
            try
            {
                LicenseManager.CurrentContext = DesigntimeLicenseContext;
                ProviderInfo.Validate(typeof(MvcLicenseDetector), MvcLicenseDetector);
            }
            finally
            {
                LicenseManager.CurrentContext = originLicenseContext;
            }
        }
    }
}
