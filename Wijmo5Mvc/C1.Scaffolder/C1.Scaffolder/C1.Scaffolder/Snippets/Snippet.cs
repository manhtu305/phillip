﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Scaffolder.Snippets
{
    internal abstract class Snippet
    {
        public abstract void Apply(IServiceProvider serviceProvider);
    }

    internal abstract class ContentSnippet : Snippet
    {
        public abstract string GetContent(IServiceProvider serviceProvider);
    }

    internal class SnippetsCollection<T> : Snippet where T : Snippet
    {
        private readonly IList<T> _snippets = new List<T>();

        public IList<T> Snippets { get { return _snippets; } }

        public void Add(T snippet)
        {
            Snippets.Add(snippet);
        }

        public override void Apply(IServiceProvider serviceProvider)
        {
            foreach (var snippet in Snippets)
            {
                snippet.Apply(serviceProvider);
            }
        }
    }

    internal abstract class ContentSnippetsCollection<T> : ContentSnippet where T : ContentSnippet
    {
        private readonly IList<T> _snippets = new List<T>();

        public IList<T> Snippets { get { return _snippets; } }

        public void Add(T snippet)
        {
            Snippets.Add(snippet);
        }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            var sb = new StringBuilder();
            foreach (var snippet in Snippets)
            {
                sb.AppendLine(snippet.GetContent(serviceProvider));
            }
            return sb.ToString();
        }
    }
}
