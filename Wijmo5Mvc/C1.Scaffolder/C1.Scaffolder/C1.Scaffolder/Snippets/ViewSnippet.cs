﻿using System;
using System.Collections.Generic;
using System.Linq;
using C1.Scaffolder.VS;
using C1.Web.Mvc.Services;

namespace C1.Scaffolder.Snippets
{
    internal class ViewSnippet : SnippetsCollection<Snippet>, IViewSnippets
    {
        private ScriptHtmlSnippetsCollection _scripts;
        private ViewCodeHtmlSnippetsCollection _codes;
        private HtmlSnippetsCollection _contents;
        private SnippetsCollection<ImportHtmlSnippet> _imports;
        private SnippetsCollection<ImportTagHtmlSnippet> _importTags;
        private SnippetsCollection<SimpleHtmlSnippet> _resources;

        private SnippetsCollection<ImportHtmlSnippet> _packages;


        private readonly IList<string> _memberNames = new List<string>();
        private readonly IList<string> _builderVariableNames = new List<string>();

        public ViewSnippet()
        {
            InitSnippets();
        }

        private void InitSnippets()
        {
            Add(_scripts = new ScriptHtmlSnippetsCollection());
            Add(_codes = new ViewCodeHtmlSnippetsCollection());
            Add(_contents = new HtmlSnippetsCollection());

            _imports = new SnippetsCollection<ImportHtmlSnippet>();
            _importTags = new SnippetsCollection<ImportTagHtmlSnippet>();
            _resources = new SnippetsCollection<SimpleHtmlSnippet>();
            _packages = new SnippetsCollection<ImportHtmlSnippet>();
        }

        public override void Apply(IServiceProvider serviceProvider)
        {
            if (_packages.Snippets.Count > 0)//369163, ensure edit project file before others process add item to project
            {
                var packages = _packages.Snippets.Select(s => s.Namespace);
                
                ProjectFileEditor.AddReferenceAssembly(serviceProvider, packages);

                FileEditor.AddPackages(serviceProvider, packages);

                LicenseEditor.AddLicenseslicx(serviceProvider, packages);
            }

            base.Apply(serviceProvider);

            if (_imports.Snippets.Count > 0)
            {
                var namespaces = _imports.Snippets.Select(s => s.Namespace);
                ViewEditor.AddImports(serviceProvider, namespaces);
            }

            if (_resources.Snippets.Count > 0)
            {
                var scripts = _resources.Snippets.Select(s => s.Content);
                ViewEditor.AddWebResources(serviceProvider, scripts);
            }

            if (_importTags.Snippets.Count > 0)
            {
                var tags = _importTags.Snippets.Select(s => s.Tag);
                ViewEditor.AddImportTags(serviceProvider, tags);
            }
        }

        public void AddScript(string content)
        {
            _scripts.Add(new SimpleHtmlSnippet(content));
        }

        public void AddFunctionScript(string name, string body, params string[] parameters)
        {
            _scripts.Add(new FunctionScriptHtmlSnippet(name, parameters, body));
        }

        public void AddClientEventScript(string eventName)
        {
            _scripts.Add(new ClientEventScriptHtmlSnippet(eventName));
        }

        public void AddCode(string code)
        {
            _codes.Add(new SimpleHtmlSnippet(code));
        }

        public void AddHtml(string content)
        {
            _contents.Add(new SimpleHtmlSnippet(content));
        }

        public void AddComponent(string content)
        {
            _contents.Add(new ComponentHtmlSnippet(content));
        }

        public void AddComponent(SimpleHtmlSnippet componentContent)
        {
            _contents.Add(componentContent);
        }

        public void AddImport(string ns)
        {
            _imports.Add(new ImportHtmlSnippet(ns));
        }

        public void AddImportTag(string tag)
        {
            _importTags.Add(new ImportTagHtmlSnippet(tag));
        }

        public void AddResourceScript(string script)
        {
            _resources.Add(new SimpleHtmlSnippet(script));
        }

        public void AddPackage(string package)
        {
            _packages.Add(new ImportHtmlSnippet(package));
        }

        public string GetBuilderVariableName(IEnumerable<string> names, string defaultName)
        {
            var realName = SnippetUtils.GetUniqueName(names.Concat(_memberNames), defaultName);
            if (!_builderVariableNames.Contains(realName))
            {
                _builderVariableNames.Add(realName);
            }
            return realName;
        }

        public string GetMemberName(string defaultName)
        {
            var realName = SnippetUtils.GetUniqueName(_memberNames.Concat(_builderVariableNames), defaultName);
            _memberNames.Add(realName);
            return realName;
        }
    }
}
