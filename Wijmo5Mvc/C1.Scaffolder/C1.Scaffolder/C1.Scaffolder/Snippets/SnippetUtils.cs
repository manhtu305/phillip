﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace C1.Scaffolder.Snippets
{
    internal static class SnippetUtils
    {
        public static string GetUniqueName(IEnumerable<string> names, string defaultName)
        {
            var oldNames = names.ToArray();
            var name = defaultName;
            var index = 0;
            while (oldNames.Contains(name, StringComparer.OrdinalIgnoreCase))
            {
                index++;
                name = defaultName + index;
            }

            return name;
        }
    }
}
