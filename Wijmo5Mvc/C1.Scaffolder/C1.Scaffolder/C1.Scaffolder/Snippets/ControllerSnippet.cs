﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Scaffolder.CodeBuilder;
using C1.Scaffolder.Scaffolders;
using C1.Scaffolder.VS;
using C1.Web.Mvc.Services;

namespace C1.Scaffolder.Snippets
{
    internal class ControllerSnippet : SnippetsCollection<Snippet>, IControllerSnippets
    {
        public const string DbContextsHolder = "[DbContextsHolder]";
        public const string ViewBagsHolder = "[ViewBagsHolder]";
        public const string ActionsHolder = "[ActionsHolder]";

        private SnippetsCollection<ImportCodeSnippet> _imports;
        private CodeSnippetsCollection _actions;
        private SnippetsCollection<DbContextCodeSnippet> _dbs;
        private WebRootPathCodeSnippet _webroot;
        private ActionCodeSnippetsCollection _viewBags;

        private readonly ScaffolderDescriptor _scaffolder;
        private readonly IList<string> _memberNames = new List<string>();
        private readonly IList<string> _viewBagNames = new List<string>();
        private readonly IList<string> _proxyCreationDisabledDbNames = new List<string>();

        public ControllerSnippet(ScaffolderDescriptor scaffolder)
        {
            _scaffolder = scaffolder;
             InitSnippets();
        }

        private void InitSnippets()
        {
            Add(_imports = new SnippetsCollection<ImportCodeSnippet>());
            Add(_actions = new CodeSnippetsCollection());
            Add(_dbs = new SnippetsCollection<DbContextCodeSnippet>());
            Add(_viewBags = new ActionCodeSnippetsCollection());
        }

        #region IControllerSnippets
        public string ControllerName
        {
            get
            {
                return _scaffolder.Controller == null
                    ? _scaffolder.View.ControllerName
                    : _scaffolder.Controller.Name;
            }
        }

        public void AddImport(string ns)
        {
            _imports.Add(new ImportCodeSnippet(ns));
        }

        public string AddWebRoot(string name)
        {
            if (_webroot == null)
            {
                var realName = GetMemberName(name);
                _webroot = new WebRootPathCodeSnippet(realName);
            }
            
            return _webroot.Name;
        }

        public string GetDbContextName(string typeName)
        {
            // find the existing db context in the controller
            if (_scaffolder.Controller != null)
            {
                foreach (var p in _scaffolder.Controller.Variables)
                {
                    if (string.Equals(p.Value, typeName, StringComparison.OrdinalIgnoreCase))
                    {
                        return p.Key;
                    }
                }
            }

            // find the db context added before
            var db = _dbs.Snippets.FirstOrDefault(s => string.Equals(s.TypeName, typeName, StringComparison.OrdinalIgnoreCase));
            return db == null ? null : db.Name;
        }

        public string AddDbContext(string name, string typeName)
        {
            var realName = GetDbContextName(typeName);
            if (string.IsNullOrEmpty(realName))
            {
                realName = GetMemberName(name);
                _dbs.Add(new DbContextCodeSnippet(realName, typeName));
            }
            return realName;
        }

        public string AddViewBagItem(string name, string dbName, string entitySetName, bool proxyCreationEnabled = true)
        {
            var realName = GetViewBagName(name);
            if (!proxyCreationEnabled && !_proxyCreationDisabledDbNames.Contains(dbName))
            {
                _proxyCreationDisabledDbNames.Add(dbName);
                _viewBags.Add(new DbProxyCreationDisabledCodeSnippet(dbName));
            }
            _viewBags.Add(new ViewBagCodeSnippet(realName, dbName, entitySetName));
            return realName;
        }

        public void AddAction(string content)
        {
            _actions.Add(new SimpleCodeSnippet(content));
        }

        public string GetMemberName(string defaultName)
        {
            IEnumerable<string> names = _memberNames;
            if (_scaffolder.Controller != null)
            {
                names = names.Concat(_scaffolder.Controller.MemberNames);
            }
            var name = SnippetUtils.GetUniqueName(names, defaultName);
            _memberNames.Add(name);
            return name;
        }

        public bool IsExistMemberName(string memberName)
        {
            IEnumerable<string> names = _memberNames;
            if (_scaffolder.Controller != null)
            {
                names = names.Concat(_scaffolder.Controller.MemberNames);
            }
            var oldNames = names.ToArray();
            return oldNames.Contains(memberName, StringComparer.OrdinalIgnoreCase);
        }
        public string GetVariableName(string defaultName)
        {
            // todo
            return defaultName;
        }

        public string GetViewBagName(string defaultName)
        {
            IEnumerable<string> names = _viewBagNames;
            if (_scaffolder.Action != null)
            {
                names = names.Concat(_scaffolder.Action.ViewBagNames);
            }
            var name = SnippetUtils.GetUniqueName(names, defaultName);
            _viewBagNames.Add(name);
            return name;
        }
        #endregion

        public override void Apply(IServiceProvider serviceProvider)
        {
            var scaffoler = serviceProvider.GetService(typeof(IScaffolderDescriptor)) as ScaffolderDescriptor;
            Dictionary<string, string> replaces;
            if (scaffoler.Controller != null || scaffoler.Action != null)
            {
                if (_webroot != null)
                {
                    _webroot.Apply(serviceProvider);
                }

                base.Apply(serviceProvider);

                replaces = new Dictionary<string, string>
                {
                    {DbContextsHolder, ""},
                    {ViewBagsHolder, ""},
                    {ActionsHolder, ""}
                };
            }
            else
            {
                replaces = new Dictionary<string, string>
                {
                    {DbContextsHolder, GetDbContextsContent(serviceProvider)},
                    {ViewBagsHolder, GetViewBagsContent(serviceProvider)},
                    {ActionsHolder, GetActionsContent(serviceProvider)}
                };
            }

            ControllerEditor.ReplaceHolders(serviceProvider, scaffoler.ControllerItem.Source, replaces);
        }

        #region get contents

        public string GetDbContextsContent(IServiceProvider serviceProvider)
        {
            if (_webroot == null && _dbs.Snippets.Count == 0) return string.Empty;

            var scaffoler = serviceProvider.GetService(typeof (IControlScaffolder)) as IControlScaffolder;
            var generator = serviceProvider.GetService(typeof(ICodeBuilder)) as ICodeBuilder;
            if (scaffoler == null || generator == null) return string.Empty;

            var sb = new StringBuilder();
            var paramsList = new List<string>();

            foreach (var db in _dbs.Snippets)
            {
                sb.AppendLine(generator.GenerateFieldDeclare(db.TypeName, db.Name));
                paramsList.Add(generator.GenerateParameter(db.TypeName, db.Name));
            }

            if (_webroot != null)
            {
                sb.AppendLine(generator.GenerateFieldDeclare("String", _webroot.Name));
                paramsList.Add(generator.GenerateParameter("IHostingEnvironment", "hostingEnvironment"));
            }

            var sbBody = new StringBuilder();
            foreach (var db in _dbs.Snippets)
            {
                sbBody.AppendLine(generator.GenerateFieldAssignment(db.Name, db.Name));
            }

            if (_webroot != null)
            {
                sbBody.AppendLine(generator.GenerateFieldAssignment(_webroot.Name, "hostingEnvironment.WebRootPath"));
            }

            if (paramsList.Count > 0 || sbBody.Length > 0)
            {
                sb.AppendLine(generator.GenerateConstructor(scaffoler.OptionsModel.ControllerName,
                    string.Join(", ", paramsList), sbBody.ToString()));
            }

            return sb.ToString();
        }

        public string GetViewBagsContent(IServiceProvider serviceProvider)
        {
            return _viewBags.GetContent(serviceProvider);
        }

        public string GetActionsContent(IServiceProvider serviceProvider)
        {
            return _actions.GetContent(serviceProvider);
        }
        #endregion
    }
}
