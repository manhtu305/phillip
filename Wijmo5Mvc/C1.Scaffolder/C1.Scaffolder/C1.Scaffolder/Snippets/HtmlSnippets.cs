﻿using System;
using System.Collections.Generic;
using System.Text;
using C1.Scaffolder.CodeBuilder;
using C1.Scaffolder.VS;
using C1.Web.Mvc.Services;

namespace C1.Scaffolder.Snippets
{
    internal abstract class HtmlSnippet : ContentSnippet
    {
        public override void Apply(IServiceProvider serviceProvider)
        {
            ViewEditor.AddContent(serviceProvider, GetContent(serviceProvider));
        }
    }

    internal class ImportHtmlSnippet : ContentSnippet
    {
        public ImportHtmlSnippet(string ns)
        {
            Namespace = ns;
        }

        public string Namespace { get; private set; }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            throw new NotImplementedException();
        }

        public override void Apply(IServiceProvider serviceProvider)
        {
            throw new NotImplementedException();
        }
    }

    internal class ImportTagHtmlSnippet : ContentSnippet
    {
        public ImportTagHtmlSnippet(string tag)
        {
            Tag = tag;
        }

        public string Tag { get; private set; }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            throw new NotImplementedException();
        }

        public override void Apply(IServiceProvider serviceProvider)
        {
            throw new NotImplementedException();
        }
    }

    internal class HtmlSnippetsCollection : ContentSnippetsCollection<HtmlSnippet>
    {
        public override void Apply(IServiceProvider serviceProvider)
        {
            ViewEditor.AddContent(serviceProvider, GetContent(serviceProvider));
        }
    }

    internal class ViewCodeHtmlSnippetsCollection : HtmlSnippetsCollection
    {
        public override string GetContent(IServiceProvider serviceProvider)
        {
            if (Snippets.Count == 0) return string.Empty;

            var builder = serviceProvider.GetService(typeof(ICodeBuilder)) as ICodeBuilder;
            return builder.GenerateViewCodeBlock(base.GetContent(serviceProvider));
        }
    }

    internal class SimpleHtmlSnippet : HtmlSnippet
    {
        public SimpleHtmlSnippet(string content)
        {
            Content = content;
        }

        public string Content { get; private set; }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            return Content;
        }
    }

    internal class ComponentHtmlSnippet : SimpleHtmlSnippet
    {
        public ComponentHtmlSnippet(string content) : base(content)
        {
        }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            var project = serviceProvider.GetService(typeof (IMvcProject)) as IMvcProject;
            return project.IsAspNetCore
                ? base.GetContent(serviceProvider)
                : string.Format("@({0})", base.GetContent(serviceProvider));
        }
    }

    internal class CustomComponentHtmlSnippet : SimpleHtmlSnippet
    {
        private bool isCoreProject = false;
        public CustomComponentHtmlSnippet(string content, bool isCore)
            : base(content)
        {
            isCoreProject = isCore;
        }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            return isCoreProject
                ? base.GetContent(serviceProvider)
                : string.Format("@({0})", base.GetContent(serviceProvider));
        }
    }

    internal class FunctionScriptHtmlSnippet : HtmlSnippet
    {
        public FunctionScriptHtmlSnippet(string name, IEnumerable<string> parameters, string body)
            : this(name, parameters == null ? string.Empty : string.Join(", ", parameters), body)
        {
        }

        public FunctionScriptHtmlSnippet(string name, string parameters, string body)
        {
            Name = name;
            Parameters = parameters;
            Body = body;
        }

        public string Name { get; private set; }
        public string Body { get; private set; }
        public string Parameters { get; private set; }

        public override string GetContent(IServiceProvider serviceProvider)
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("function {0}({1}){{", Name, Parameters));
            sb.AppendLine(Body);
            sb.AppendLine("}");
            return sb.ToString();
        }
    }

    internal class ClientEventScriptHtmlSnippet : FunctionScriptHtmlSnippet
    {
        public ClientEventScriptHtmlSnippet(string name)
            : base(name, new[] { "sender", "e" }, GetClientEventBody(name))
        {
        }

        private static string GetClientEventBody(string eventName)
        {
            return string.Format("    // Implement the event handler for {0}.", eventName);
        }
    }

    internal class ScriptHtmlSnippetsCollection : HtmlSnippetsCollection
    {
        public override string GetContent(IServiceProvider serviceProvider)
        {
            if (Snippets.Count == 0) return string.Empty;

            var sb = new StringBuilder();
            sb.AppendLine("<script>");
            sb.Append(base.GetContent(serviceProvider));
            sb.AppendLine("</script>");
            return sb.ToString();
        }
    }
}
