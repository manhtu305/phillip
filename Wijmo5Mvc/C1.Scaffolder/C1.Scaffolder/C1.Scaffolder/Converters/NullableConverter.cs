﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace C1.Scaffolder.Converters
{
    /// <summary>
    /// Converters for type of Nullable.
    /// </summary>
    public class NullableConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType.Name != "Nullable`1") return value;

            return value == null ? string.Empty : value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType.Name != "Nullable`1") return value;

            var s = value as string;
            if (string.IsNullOrEmpty(s)) return null;

            var gType = targetType.GenericTypeArguments[0];
            if (!gType.IsPrimitive) return value;

            try
            {
                return System.Convert.ChangeType(s, gType);
            }
            catch
            {
                return null;
            }
        }
    }
}
