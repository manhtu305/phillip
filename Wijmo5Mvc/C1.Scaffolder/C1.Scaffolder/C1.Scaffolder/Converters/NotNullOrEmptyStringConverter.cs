﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace C1.Scaffolder.Converters
{
    /// <summary>
    /// Validate a string should not be null or empty
    /// </summary>
    public class NotNullOrEmptyStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value == null)
            {
                return false;
            }

            return !string.IsNullOrEmpty(((string)value).Trim());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value == null)
            {
                return false;
            }

            return !string.IsNullOrEmpty(((string)value).Trim());
        }
    }
}
