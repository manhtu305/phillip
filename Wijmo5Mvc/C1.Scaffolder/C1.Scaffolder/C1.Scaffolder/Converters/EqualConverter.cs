﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Globalization;

namespace C1.Scaffolder.Converters
{
    public class EqualConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return EqualityCheck(value, parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static bool EqualityCheck(object value, object parameter)
        {
            var v = value as IComparable;
            var p = parameter as IComparable;

            if (v != null && p != null)
            {
                return (v.CompareTo(p) == 0);
            }
            return false;
        }
    }
}
