﻿namespace C1.Scaffolder.CodeBuilder
{
    internal interface ICodeBuilder
    {
        string CommentSuffix { get; }
        string KeywordFalse { get; }
        string KeywordThis { get; }
        string GenerateFieldDeclare(string typeName, string fieldName, bool initialize=false);
        string GenerateParameter(string typeName, string parameterName);
        string GenerateConstructor(string functionName, string parameters, string body);
        string GenerateViewCodeBlock(string code);
        string GenerateAssignment(string left, string right);
        string GenerateMemberRef(string instance, string propertyName);
        string GenerateIndexRef(string instance, string indexer);
    }

    internal static class CodeBuilderExtension
    {
        public static string GenerateFieldAssignment(this ICodeBuilder builder, string fieldName, string value)
        {
            var field = builder.GenerateMemberRef(builder.KeywordThis, fieldName);
            return builder.GenerateAssignment(field, value);
        }

        public static string GenerateViewBagAssignment(this ICodeBuilder builder, string propertyName, string dbName, string modelName)
        {
            var left = builder.GenerateMemberRef("ViewBag", propertyName);
            var right = builder.GenerateMemberRef(dbName, modelName);
            return builder.GenerateAssignment(left, right);
        }

        public static string GenerateViewDataAssignment(this ICodeBuilder builder, string propertyName, string dbName, string modelName)
        {
            var left = builder.GenerateIndexRef("ViewData", propertyName);
            var right = builder.GenerateMemberRef(dbName, modelName);
            return builder.GenerateAssignment(left, right);
        }
    }
}
