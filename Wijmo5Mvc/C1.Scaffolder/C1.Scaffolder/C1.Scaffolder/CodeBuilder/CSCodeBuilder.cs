﻿using System.Text;

namespace C1.Scaffolder.CodeBuilder
{
    internal class CsCodeBuilder : ICodeBuilder
    {
        public string CommentSuffix { get { return "//"; } }

        public string KeywordFalse { get { return "false"; } }

        public string KeywordThis { get { return "this"; } }

        public string GenerateMemberRef(string instance, string propertyName)
        {
            return string.Format("{0}.{1}", instance, propertyName);
        }
        
        public string GenerateIndexRef(string instance, string indexer)
        {
            return string.Format("{0}[\"{1}\"]", instance, indexer);
        }

        public string GenerateFieldDeclare(string typeName, string fieldName, bool initialize = false)
        {
            return initialize
                ? string.Format("{0} {1} = new {0}();", typeName, fieldName)
                : string.Format("{0} {1};", typeName, fieldName);
        }

        public string GenerateParameter(string typeName, string parameterName)
        {
            return string.Format("{0} {1}", typeName, parameterName);
        }

        public string GenerateConstructor(string functionName, string parameters, string body)
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("public {0}({1})\r\n{{", functionName, parameters));
            sb.AppendLine(body);
            sb.AppendLine("}");
            return sb.ToString();
        }

        public string GenerateAssignment(string left, string right)
        {
            return string.Format("{0} = {1};", left, right);
        }

        public string GenerateViewCodeBlock(string code)
        {
            var sb = new StringBuilder();
            sb.AppendLine("@{");
            sb.AppendLine(code);
            sb.AppendLine("}");
            return sb.ToString();
        }
    }
}
