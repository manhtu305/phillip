﻿using System.Text;

namespace C1.Scaffolder.CodeBuilder
{
    internal class VbCodeBuilder : ICodeBuilder
    {
        public string CommentSuffix { get { return "'"; } }

        public string KeywordFalse { get { return "False"; } }

        public string KeywordThis { get { return "Me"; } }

        public string GenerateMemberRef(string instance, string propertyName)
        {
            return string.Format("{0}.{1}", instance, propertyName);
        }

        public string GenerateIndexRef(string instance, string indexer)
        {
            return string.Format("{0}.Item(\"{1}\")", instance, indexer);
        }

        public string GenerateFieldDeclare(string typeName, string fieldName, bool initialize = false)
        {
            return initialize
                ? string.Format("Dim {0} as New {1}()", fieldName, typeName)
                : string.Format("Dim {0} as {1}", fieldName, typeName);
        }

        public string GenerateParameter(string typeName, string parameterName)
        {
            return string.Format("{0} As {1}", parameterName, typeName);
        }

        public string GenerateConstructor(string functionName, string parameters, string body)
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("Public Sub New({0})", parameters));
            sb.AppendLine(body);
            sb.AppendLine("End Sub");
            return sb.ToString();
        }

        public string GenerateAssignment(string left, string right)
        {
            return string.Format("{0} = {1}", left, right);
        }

        public string GenerateViewCodeBlock(string code)
        {
            var sb = new StringBuilder();
            sb.AppendLine("@Code");
            sb.AppendLine(code);
            sb.AppendLine("End Code");
            return sb.ToString();
        }
    }
}
