﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C1.Web.Mvc.Services;

namespace C1.Scaffolder.Services
{
    internal class DesignHelper : IDesignHelper
    {
        public bool IsDesignTime { get; set; }
    }
}
