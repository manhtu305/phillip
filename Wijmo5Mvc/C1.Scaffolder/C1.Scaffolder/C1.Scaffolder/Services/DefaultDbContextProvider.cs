﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using C1.Scaffolder.Extensions;
using C1.Scaffolder.Models;
using C1.Web.Mvc;
using C1.Web.Mvc.Services;
using EnvDTE;
using Microsoft.AspNet.Scaffolding;

namespace C1.Scaffolder.Services
{
    internal class DefaultDbContextProvider : IDefaultDbContextProvider
    {
        public DefaultDbContextProvider(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        #region SelectedModelTypeChangedEvent
        public event EventHandler SelectedModelTypeChanged;

        /// <summary>
        /// To fire SelectedModelTypeChanged event.
        /// </summary>
        /// <param name="args"></param>
        protected virtual void OnSelectedModelTypeChanged(EventArgs args)
        {
            if (SelectedModelTypeChanged != null)
            {
                SelectedModelTypeChanged(this, args);
            }
        }
        #endregion

        #region SelectedDbContextTypeChanged
        public event EventHandler SelectedDbContextTypeChanged;

        /// <summary>
        /// Fire the SelectedDbContextTypeChanged event.
        /// </summary>
        /// <param name="args"></param>
        protected virtual void OnSelectedDbContextTypeChanged(EventArgs args)
        {
            if (SelectedDbContextTypeChanged != null)
            {
                SelectedDbContextTypeChanged(this, args);
            }
        }
        #endregion

        public IServiceProvider ServiceProvider { get; private set; }

        private IModelTypeDescription _dbContextType;
        public IModelTypeDescription DbContextType
        {
            get { return _dbContextType; }
            set
            {
                if (_dbContextType == value) return;
                _dbContextType = value;
                OnSelectedDbContextTypeChanged(EventArgs.Empty);
            }
        }

        private IModelTypeDescription _modelType;
        public IModelTypeDescription ModelType
        {
            get { return _modelType; }
            set
            {
                if (_modelType == value) return;
                _modelType = value;
                _primaryKeys = null;
                OnSelectedModelTypeChanged(EventArgs.Empty);
            }
        }

        private IDictionary<string, string> _primaryKeys;
        public IDictionary<string, string> PrimaryKeys
        {
            get { return _primaryKeys ?? (_primaryKeys = GetPrimaryKeys()); }
        }

        private IDictionary<string, string> GetPrimaryKeys()
        {
            var primaryKeys = new Dictionary<string, string>();
            var modelType = _modelType as ModelType;
            if (modelType == null) return primaryKeys;

            var project = ServiceProvider.GetService(typeof (IMvcProject)) as IMvcProject;
            var codeGeneration = ServiceProvider.GetService(typeof (CodeGenerationContext)) as CodeGenerationContext;
            if (!project.IsAspNetCore && codeGeneration != null)
            {
                try
                {
                    var efMetadata = Models.ModelType.GetEfMetadata(codeGeneration, DbContextType.TypeName,
                        modelType.TypeName);
                    efMetadata.PrimaryKeys.ToList().ForEach(key => primaryKeys.Add(key.PropertyName, key.ShortTypeName));
                    return primaryKeys;
                }
                catch (Exception e)
                {
                    if (e is InvalidOperationException && e.InnerException != null)
                    {
                        System.Windows.Forms.MessageBox.Show(e.InnerException.Message);
                        return primaryKeys;
                    }
                }
            }

            // fallback
            var properties = modelType.CodeType.Children.Cast<CodeElement>();
            foreach (var property in properties)
            {
                var isPrimaryKey = false;
                var codeProperty = property as CodeProperty;

                if (codeProperty != null)
                {
                    if (string.Equals(property.Name, "id", StringComparison.InvariantCultureIgnoreCase))
                    {
                        isPrimaryKey = true;
                    }
                    else if (string.Equals(property.Name, ModelType.ShortTypeName + "id",
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        isPrimaryKey = true;
                    }
                    else
                    {
                        isPrimaryKey = codeProperty.Attributes.Cast<CodeElement>().Any(attr => attr.Name == "Key");
                    }
                }

                if (isPrimaryKey)
                {
                    primaryKeys.Add(property.Name, codeProperty.Type.AsString);
                }
            }

            return primaryKeys;
        }
    }
}
