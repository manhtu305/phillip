﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using C1.Scaffolder.Scaffolders;
using C1.Web.Mvc.Services;
using Microsoft.VisualStudio.TextTemplating;
using Microsoft.VisualStudio.TextTemplating.VSHost;
using Shell = Microsoft.VisualStudio.Shell;

namespace C1.Scaffolder.Services
{
    internal class TemplateManager : ITemplateProvider
    {
        private const string ScaffolderStr = "Scaffolder";

        public TemplateManager(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;

            var mvcProject = serviceProvider.GetService(typeof (IMvcProject)) as IMvcProject;
            IsAspNetCore = mvcProject.IsAspNetCore;
            IsRazorPages = mvcProject.IsRazorPages;
            IsCs = mvcProject.IsCs;

            var scaffolder = serviceProvider.GetService(typeof (IControlScaffolder)) as IControlScaffolder;
            ScaffolderType = scaffolder.GetType();
        }

        public string TemplatesFolder
        {
            get
            {
                return Path.Combine(Path.GetDirectoryName(GetType().Assembly.Location), "Templates");
            }
        }

        public IServiceProvider ServiceProvider { get; private set; }
        public bool IsAspNetCore { get; private set; }
        public bool IsRazorPages { get; private set; }
        public bool IsCs { get; private set; }
        public Type ScaffolderType { get; private set; }

        #region Get Templates

        /// <summary>
        /// Gets the template path for specified path.
        /// </summary>
        /// <param name="path">The relative path of the template file.</param>s
        /// <returns>The full path of the template file if existing.</returns>
        public string Get(string path)
        {
            var file = Path.Combine(TemplatesFolder, path);
            return File.Exists(file) ? file : null;
        }

        /// <summary>
        /// Gets the controller template path for current scaffolder.
        /// </summary>
        /// <param name="name">The name of the template file. Default will search the empty string or "Index".</param>
        /// <returns>The full path of the template file if existing.</returns>
        public string GetController(string name = null)
        {
            var suffix = GetLanguage() + ".t4";
            if (IsRazorPages)
            {
                suffix = (IsCs ? "cshtml." : "vbhtml.") + suffix;
            }
            var controllersFolder = IsRazorPages ? "Pages" : "Controllers";
            return Get(controllersFolder, name, suffix);
        }

        /// <summary>
        /// Gets the view template path for current scaffolder.
        /// </summary>
        /// <param name="name">The name of the template file. Default will search the empty string or "Index".</param>
        /// <returns>The full path of the template file if existing.</returns>
        public string GetView(string name = null)
        {
            var suffix = GetLanguage() + "html.t4";
            var viewsFolder = IsRazorPages ? "Pages" : "Views";
            return Get(viewsFolder,  name, suffix);
        }

        private string Get(string area, string name, string suffix)
        {
            var folder = string.IsNullOrEmpty(name) ? null : GetControlName(ScaffolderType);
            return GetFilePaths(GetSearchFolders(area, folder), name, suffix).FirstOrDefault(File.Exists);
        }

        private IEnumerable<string> GetSearchFolders(string area, string searchFolder)
        {
            var rootPath = Path.Combine(TemplatesFolder, area);
            if (!string.IsNullOrEmpty(searchFolder))
            {
                yield return Path.Combine(rootPath, searchFolder);
                yield break;
            }

            var type = ScaffolderType;
            while (type != null && type != typeof (object))
            {
                yield return Path.Combine(rootPath, GetControlName(type));
                type = type.BaseType;
            }

            yield return Path.Combine(rootPath, "Shared");
        }

        private IEnumerable<string> GetSearchNames(string searchName, string folderName)
        {
            var names = string.IsNullOrEmpty(searchName)
                ? new[] {GetControlName(ScaffolderType), folderName, "Index", ""}
                : new[] {searchName};
            foreach (var name in names)
            {
                if (IsAspNetCore)
                {
                    yield return string.IsNullOrEmpty(name) ? "Core" : name + ".Core";
                }

                yield return name;
            }
        }

        private IEnumerable<string> GetFilePaths(IEnumerable<string> searchFolders, string searchName, string suffix)
        {
            foreach (var folder in searchFolders)
            {
                if (!Directory.Exists(folder)) continue;

                var folderName = Path.GetFileName(folder.TrimEnd('\\'));
                var names = GetSearchNames(searchName, folderName);

                foreach (var name in names)
                {
                    yield return Path.Combine(folder,
                        string.Format("{0}{1}{2}", name, string.IsNullOrEmpty(name) ? "" : ".", suffix));
                }
            }
        }

        private string GetControlName(Type type)
        {
            var name = type.Name;
            if (name.EndsWith(ScaffolderStr))
            {
                name = name.Substring(0, name.Length - ScaffolderStr.Length);
            }

            return name;
        }

        private string GetLanguage()
        {
            return IsCs ? "cs" : "vb";
        }

        #endregion

        #region transform

        /// <summary>
        /// Transform the template file.
        /// </summary>
        /// <param name="templatePath">The full path of the template file.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>The transformed content.</returns>
        public string TransformTemplate(string templatePath, Dictionary<string, object> parameters=null)
        {
            var  templating = ServiceProvider.GetService(typeof(STextTemplating)) as ITextTemplating;
            if (templating == null)
            {
                //when using LightBulb to update a control, STextTemplating object is not injected into ServiceProvider
                //So we have to get this object from the VS Shell.
                templating = (ITextTemplating)Shell.Package.GetGlobalService(typeof(STextTemplating));
                if(templating == null)
                {
                    return string.Empty;
                }
            }

            if (parameters != null)
            {
                var sessionHost = templating as ITextTemplatingSessionHost;
                if (sessionHost == null) return string.Empty;

                sessionHost.Session = sessionHost.CreateSession();
                foreach (var parameter in parameters)
                {
                    sessionHost.Session[parameter.Key] = parameter.Value;
                }
            }

            var result = templating.ProcessTemplate(templatePath, File.ReadAllText(templatePath), new TemplatingCallback());

            return result;
        }
        #endregion
    }

    internal class TemplatingCallback : ITextTemplatingCallback
    {
        public void ErrorCallback(bool warning, string message, int line, int column)
        {
            if (!warning)
            {
                throw new Exception(string.Format("Line: {0}, Column {1}:\r\n{2}", line, column, message));
            }
        }

        public void SetFileExtension(string extension)
        {
            // do nothing
        }

        public void SetOutputEncoding(Encoding encoding, bool fromOutputDirective)
        {
            // do nothing
        }
    }
}
