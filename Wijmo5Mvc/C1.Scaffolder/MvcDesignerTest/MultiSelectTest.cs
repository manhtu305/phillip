﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class MultiSelectTest : Base.BaseTest
    {
        string CShaprFileName = "multiselect.cshtml";
        string VBFileName = "multiselect.vbhtml";
        string CoreFileName = "multiselect_core.cshtml";

        #region Mvc project's files.
        [TestMethod]
        public void GetMultiSelect()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiSelect", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "Name", "DisplayMemberPath", "SelectedValuePath", "CheckedMemberPath"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
         [TestMethod]
        public void GetMultiSelect1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiSelect", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Name", "Id", "Bind", "Placeholder", "HeaderFormat", "OnClientCheckedItemsChanged"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
         [TestMethod]
        public void GetMultiSelect2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiSelect", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "Placeholder", "DropDownCssClass"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
       
#endregion

        #region vbhtml
        [TestMethod]
        public void GetMultiSelectVb()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiSelect", control.Name, "Wrong name of control");
            Assert.AreEqual("MVCFlexGrid101.Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "CheckedMemberPath", "HeaderFormat", "DisplayMemberPath", "SelectedValuePath", "ShowDropDownButton", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        
        #endregion

        #region for core project
        [TestMethod]
        public void GetMultiSelectCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiSelect", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "DisplayMemberPath", "SelectedValuePath",  "CheckedMemberPath", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        
        [TestMethod]
        public void GetMultiSelectCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiSelect", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "Placeholder",  "HeaderFormat", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        [TestMethod]
        public void GetMultiSelectCore2()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("MultiSelect", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "Id", "Placeholder", "HeaderFormat", "CheckedItemsChanged", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion coreproject

    }
}
