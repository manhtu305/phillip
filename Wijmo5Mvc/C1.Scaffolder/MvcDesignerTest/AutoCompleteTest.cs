﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class AutoCompleteTest : Base.BaseTest
    {
        string CShaprFileName = "AutoComplete.cshtml";
        string VBFileName = "AutoComplete.vbhtml";
        string CoreFileName = "AutoComplete_core.cshtml";

        #region Mvc project's files.
        [TestMethod]
        public void GetAutoComplete()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("AutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "DisplayMemberPath", "SelectedValuePath"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetAutoComplete1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("AutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "MaxItems", "CssMatch", "ItemsSourceAction"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetAutoComplete2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("AutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "CssMatch"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
         
#endregion

        #region vbhtml
        [TestMethod]
        public void GetMultiAutoCompleteVb()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("AutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual("MVCFlexGrid101.Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "MaxItems", "IsEditable", "IsRequired", "SelectedValuePath", "HeaderPath", "OnClientFormatItem", "CssClass", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
             [TestMethod]
        public void GetMultiAutoCompleteVb1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("AutoComplete", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "MaxItems", "IsEditable", "IsRequired", "OnClientFormatItem", "CssClass", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        
        #endregion

        #region for core project
        [TestMethod]
        public void GetAutoCompleteCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("AutoComplete", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "DisplayMemberPath", "SelectedValuePath",  "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        [TestMethod]
        public void GetAutoCompleteCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("AutoComplete", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "MaxItems", "CssMatch",  "ItemsSourceAction"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        [TestMethod]
        public void GetAutoCompleteCore2()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("AutoComplete", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "CssMatch", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion coreproject

    }
}
