﻿<!-- 0 -->
@(Html.C1().MultiRow(Of MVCFlexGrid101.Sale)() _
                            .SelectionMode(C1.Web.Mvc.Grid.SelectionMode.RowRange).CssClass("multiRow").Id("multirow1"))
<!--1-->
@(Html.C1().MultiRow(Of MVCFlexGrid101.MonthData)() _
                            .AutoSizeMode(C1.Web.Mvc.Grid.AutoSizeMode.Headers).OnClientRowAdded("multirow2_RowAdded").OnClientRowEditEnded("multirow2_RowEditEnded") _
                            .Width("500px").Id("multirow2"))