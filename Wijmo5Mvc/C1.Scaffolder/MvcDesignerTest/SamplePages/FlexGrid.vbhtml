﻿<!-- 0 -->
@(Html.C1().FlexGrid(Of Sale)().Id("cdInitMethod").IsReadOnly(True).AutoGenerateColumns(False).AllowSorting(True) _
                .Bind(Model.CountryData).CssClass("grid") _
                .Columns(Sub(bl)
                             'bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                             bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                             bl.Add(Sub(cb) cb.Binding("End").Header("End").Format("HH:mm"))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").Width("*"))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("n0"))
                             bl.Add(Sub(cb) cb.Binding("Amount2"))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                             bl.Add(Sub(cb) cb.Binding("Active"))
                         End Sub)
)
<!--1-->
@(Html.C1().FlexGrid().Id("nvGrid").AutoGenerateColumns(False).AllowSorting(True) _
                .Bind(Sub(bl)
                          bl.Bind(Url.Action("GridRead"))
                          bl.Update(Url.Action("NVGridUpdate"))
                      End Sub) _
                .Columns(Sub(bl)
                             bl.Add(Sub(cb) cb.Binding("ID").Header("ID").IsReadOnly(True))
                             bl.Add(Sub(cb) cb.Binding("Country").Header("Country").IsRequired(False))
                             bl.Add(Sub(cb) cb.Binding("Product").Header("Product").IsRequired(True))
                             bl.Add(Sub(cb) cb.Binding("Discount").Format("p0").IsRequired(True))
                             bl.Add(Sub(cb) cb.Binding("Amount").Format("n0").IsRequired(True))
                         End Sub)
)
<!--2-->
@(Html.C1().FlexGrid().Id("fFlexGrid").AutoGenerateColumns(False).AllowSorting(True) _
                        .IsReadOnly(True).Bind(Model.CountryData).CssClass("grid") _
                        .Filterable(Sub(fl)
                                        fl.DefaultFilterType(FilterType.None) _
                                        .ColumnFilters(Sub(cfsb)
                                                           For index As Int16 = 0 To Model.FilterBy.Length - 1
                                                               cfsb.Add(Sub(cfb) cfb.Column(Model.FilterBy.ToList()(index)).FilterType(FilterType.Condition))
                                                           Next
                                                       End Sub)

                                    End Sub) _
                    .Columns(Sub(bl)
                                 bl.Add(Sub(cb) cb.Binding("ID").Header("ID"))
                                 bl.Add(Sub(cb) cb.Binding("Country").Header("Country"))
                                 bl.Add(Sub(cb) cb.Binding("Product").Header("Product"))
                                 bl.Add(Sub(cb) cb.Binding("Color").Header("Color"))
                                 bl.Add(Sub(cb) cb.Binding("Start").Header("Start").Format("MMM d yy"))
                                 bl.Add(Sub(cb) cb.Binding("Discount").Format("p0"))
                             End Sub)
)
<!--3-->
@(Html.C1().FlexGrid().Id("tvFlexGrid").AutoGenerateColumns(False).IsReadOnly(True) _
                                      .Bind(Model.TreeData).SelectionMode(C1.Web.Mvc.Grid.SelectionMode.ListBox) _
                                      .AllowResizing(C1.Web.Mvc.Grid.AllowResizing.None).AllowSorting(False).ChildItemsPath("Children") _
                                      .CssClass("custom-flex-grid") _
                                      .Columns(Sub(bl)
                                                   bl.Add().Binding("Header").Width("*").Header("Folder/File Name")
                                                   bl.Add().Binding("Size").Width("80").Align("center")
                                               End Sub)
)

<!--4-->
@(Html.C1().FlexGrid(Of MVCFlexGrid101.Sale)() _
                            .AllowSorting(False).AutoGenerateColumns(False).IsReadOnly(True) _
                            .Columns(Sub(csb)
                                         csb.Add().Binding("ID")
                                         csb.Add().Binding("Start")
                                         csb.Add().Binding("[End]")
                                         csb.Add().Binding("Country")
                                         csb.Add().Binding("Product")
                                         csb.Add().Binding("Color")
                                         csb.Add().Binding("Amount")
                                         csb.Add().Binding("Amount2")
                                         csb.Add().Binding("Discount")
                                         csb.Add().Binding("Active")
                                         csb.Add().Binding("Trends")
                                         csb.Add().Binding("Rank")
                                     End Sub) _
                            .Bind(Sub(cvsb) cvsb.Bind(DirectCast(ViewBag.Sales, IEnumerable(Of MVCFlexGrid101.Sale)))) _
                            .Height("800px").Id("flexgrid1"))