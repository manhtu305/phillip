﻿<!-- 0 -->
@(Html.C1().FlexChart(Of Input101.InputModel)() _
                            .AddAnnotationLayer(Sub(alb)
                                                    alb.AddEllipse(Sub(eb) eb.Height(80.0F).Width(100.0F))
                                                End Sub) _
                            .Height("300px").Id("flexchart1"))


<!--1-->
@(Html.C1().FlexChart(Of ModelStyle).Id("chartTypes").Bind(Model.CountrySalesData).BindingX("Country") _
                                                        .Series(Sub(sers)
                                                                    sers.Add().Binding("Sales").Name("Sales")
                                                                    sers.Add().Binding("Expenses").Name("Expenses")
                                                                    sers.Add().Binding("Downloads").Name("Downloads")
                                                                End Sub)
)