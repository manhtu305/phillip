﻿<!--0-->
@(Html.C1().InputNumber().Id("inInputNumber1").Value(0).Format("n0"))
<!--1-->
@(Html.C1().InputNumber().Id("inInputNumber3").Value(3.5).Format("C2").Step(0.5).Min(0).Max(10))
<!--2-->
@(Html.C1().InputNumber().Id("inInputNumber4").Placeholder("Enter a number...").Required(False).Value(Nothing))
<!--3-->
@(Html.C1().InputTime().Id("itMask").Value(DateTime.Now).Format("hh:mm tt").Mask("00:00 >LL"))
<!--4-->
@(Html.C1().InputTime().Id("iditInputTime").Value(Today).Min(minTime).Max(maxTime).Step(15).OnClientValueChanged("inputTime_ValueChanged"))
<!--5-->
@(Html.C1().InputDate().Id("iditInputDate").Value(Today).Min(minDate).Max(maxDate).Format(Format).OnClientValueChanged("inputDate_ValueChanged"))
<!--6-->
@(Html.C1().InputDateTime().Id("iditInputDateTime").Value(Today).Min(minDate).Max(maxDate).TimeStep(15).Format("MMM dd, yyyy hh:mm tt").TimeMin(minTime).TimeMax(maxTime).OnClientValueChanged("inputDateTime_ValueChanged"))
<!--7-->
@(Html.C1().InputMask().Id("imCustomInput").Placeholder("Enter an input mask...") _
                                    .OnClientValueChanged("MaskvalueChanged")
)
<!--8-->
@(Html.C1().InputMask().Id("imPhone").Mask("(999) 000-0000"))
<!--9-->
@(Html.C1().InputNumberFor(Function(model) model.Start) _
                            .Id("inputNumber1"))
<!---->10-->
@(Html.C1().InputDate() _
                        .Value(DateTime.Parse("2018/11/12 13:21:14")).Min(DateTime.Parse("2010/01/01 0:00:00")).Max(DateTime.Parse("2020/01/01 0:00:00")) _
                        .Id("inputDate2"))