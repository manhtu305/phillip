﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class FlexPieParseTest : Base.BaseTest
    {
        string CShaprFileName = "FlexPie/FlexPie.cshtml";
        string VBFileName = "FlexPie/FlexPie.vbhtml";
        string CoreFileName = "FlexPie/FlexPie_core.cshtml";

        #region Mvc project's files.

        [TestMethod]
        public void FlexPie0()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexPie", control.Name, "Wrong name of control");
            Assert.AreEqual("CustomerOrder", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            {"Id", "Header", "Bind", "InnerRadius", "DataLabel", "ShowAnimation"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void FlexPie1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexPie", control.Name, "Wrong name of control");
            Assert.AreEqual("CustomerOrder", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            {"Id", "Header", "Bind", "SelectionMode", "IsAnimated", "SelectedItemPosition"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion

        #region vbhtml
        
        [TestMethod]
        public void FlexPieVB0()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexPie", control.Name, "Wrong name of control");
            Assert.AreEqual("FlexPie.Sale", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "SelectionMode", "CssClass", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void FlexPieVB1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexPie", control.Name, "Wrong name of control");
            Assert.AreEqual("FlexPie.Style", control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Height", "Width", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion

        #region for core project

        [TestMethod]
        public void GetMultiRowCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexPie", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "Id", "Header", "BindingName", "Binding", "Sources", "Datalabels", "Animations" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void FlexPieCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexPie", control.Name, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "Id", "Header",  "InnerRadius", "BindingName", "Binding", "Sources", "Datalabels" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        #endregion coreproject
    }
}
