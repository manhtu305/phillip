﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class FlexSheetParseTest : Base.BaseTest
    {
        string CShaprFileName = "FlexSheet/FlexSheet.cshtml";
        string VBFileName = "FlexSheet/FlexSheet.vbhtml";
        string CoreFileName = "FlexSheet/FlexSheet_core.cshtml";

        protected override string[] GetListComplexProperties()
        {
            return new string[] { "AppendedSheets"};
        }
        #region Mvc project's files.
        [TestMethod]
        public void GetFlexSheet()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexSheet", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "CssClass", "Id", "AppendedSheets"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

          [TestMethod]
        public void GetFlexSheet1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexSheet", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "CssClass", "Id", "AppendedSheets"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

           [TestMethod]
        public void GetFlexSheet2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexSheet", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Id", "CssClass", "Load"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
#endregion

        #region vbhtml
        [TestMethod]
        public void GetFlexSheetVB()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexSheet", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "AppendedSheets", "SelectionMode", "Height", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }
        


        #endregion

        #region for core project
        [TestMethod]
        public void GetFlexSheetCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexSheet", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "CssClass", "Id", "SelectedSheetIndex",  "AppendedSheets" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
       
        [TestMethod]
        public void GetFlexSheetCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexSheet", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Id", "CssClass", "UnknownFunction",  "AppendedSheets" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetFlexSheetCore2()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexSheet", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "CssClass", "Id", "LoadActionUrl",  "SaveActionUrl", "RemoteSaved" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
         [TestMethod]
        public void GetFlexSheetCore3()
        {
            int index = 3;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("FlexSheet", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "Height", "Id", "AppendedSheets"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion coreproject
    }
}
