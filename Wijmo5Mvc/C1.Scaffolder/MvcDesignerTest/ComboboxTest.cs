﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class ComboboxTest : Base.BaseTest
    {
        string CShaprFileName = "combobox.cshtml";
        string VBFileName = "combobox.vbhtml";
        string CoreFileName = "combobox_core.cshtml";

        #region Mvc project's files.
        [TestMethod]
        public void GetCombobox()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("ComboBox", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "SelectedIndex", "IsEditable"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

       [TestMethod]
        public void GetCombobox1()
        {
            int index = 1;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("ComboBox", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "DisplayMemberPath", "SelectedValuePath", "ItemTemplateId", "Width"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetCombobox2()
        {
            int index = 2;
            MVCControl control = GetControlSetting(index, CShaprFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("ComboBox", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[] 
            { "Bind", "SelectedIndex", "IsEditable", "DropDownCssClass"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
#endregion

        #region vbhtml
        [TestMethod]
        public void GetComboboxVb()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("ComboBox", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "IsEditable", "IsRequired", "Placeholder", "Id"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

        }

        
        #endregion

        #region for core project
        [TestMethod]
        public void GetComboboxCore()
        {
            int index = 0;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("ComboBox", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "DisplayMemberPath", "SelectedValuePath",  "SelectedIndex", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        
        [TestMethod]
        public void GetComboboxCore1()
        {
            int index = 1;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("ComboBox", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "DisplayMemberPath", "SelectedValuePath",  "Templates", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }

        [TestMethod]
        public void GetComboboxCore2()
        {
            int index = 2;
            MVCControl control = GetControlSettingInCore(index, CoreFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("ComboBox", control.Name, "Wrong name of control");
            //Assert.AreEqual("Category", control.Model, "Wrong model name of control");
            string[] expectedProperties = new string[]
            { "SelectedIndex", "IsEditable", "DropDownCssClass", "Sources"};
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));
        }
        #endregion coreproject

    }
}
