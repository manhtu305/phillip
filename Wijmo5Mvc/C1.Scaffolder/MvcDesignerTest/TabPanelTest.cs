﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using C1.Web.Mvc;

namespace C1.Scaffolder.UnitTest
{
    [TestClass]
    public class TabPanelTest : Base.BaseTest
    {
        string VBFileName = "TabPanel/Index.vbhtml";
        
        [TestMethod]
        public void FirstCaseTest()
        {
            int index = 0;
            MVCControl control = GetControlSetting(index, VBFileName);

            Assert.IsTrue(control != null, "must exist control");
            Assert.AreEqual("TabPanel", control.Name, "Wrong name of control");
            Assert.AreEqual(null, control.ModelType, "Wrong model name of control");
            Assert.AreEqual("\"#asdasd\"", control.ControlValue, "Wrong name of control");
            string[] expectedProperties = new string[]
            { "AutoSwitch", "IsAnimated", "SelectedIndex" };
            Assert.IsTrue(CheckSameProperties(expectedProperties, control.Properties));

            
        }
    }
}
