﻿namespace C1.Web.Mvc.Olap
{
    partial class Slicer
    {
        #region Field
        private PivotField _field;
        private PivotField _GetField()
        {

            return _field ?? (_field = new PivotField());
        }
        private bool _ShouldSerializeField()
        {
            return _field != null;
        }
        #endregion Field

    }
}
