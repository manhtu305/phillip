﻿namespace C1.Web.Mvc.Olap
{
    /// <summary>
    /// Specifies constants that define the chart type.
    /// </summary>
    public enum PivotChartType
    {
        /// <summary>
        /// Shows vertical bars and allows you to compare values of items across categories.
        /// </summary>
        Column,
        /// <summary>
        /// Shows horizontal bars.
        /// </summary>
        Bar,
        /// <summary>
        /// Shows patterns within the data using X and Y coordinates.
        /// </summary>
        Scatter,
        /// <summary>
        /// Shows trends over a period of time or across categories.
        /// </summary>
        Line,
        /// <summary>
        /// Shows line chart with the area below the line filled with color.
        /// </summary>
        Area,
        /// <summary>
        /// Shows pie chart.
        /// </summary>
        Pie
    }
}
