﻿namespace C1.Web.Mvc.Olap
{
    partial class PivotPanel
    {
        #region Engine
        private PivotEngine _engine;
        private PivotEngine _GetEngine()
        {
#if !MODEL
            return _engine ?? (_engine = new PivotEngine(Helper));
#else
            return _engine ?? (_engine = new PivotEngine());
#endif
        }
        private bool _ShouldSerializeEngine()
        {
            return string.IsNullOrEmpty(ItemsSourceId);
        }
        #endregion Engine

        private PivotFieldCollection _GetFields()
        {
            return Engine.Fields;
        }

        private PivotFieldCollection _GetRowFields()
        {
            return Engine.RowFields;
        }

        private PivotFieldCollection _GetColumnFields()
        {
            return Engine.ColumnFields;
        }

        private PivotFieldCollection _GetValueFields()
        {
            return Engine.ValueFields;
        }

        private PivotFieldCollection _GetFilterFields()
        {
            return Engine.FilterFields;
        }

        private IItemsSource<object> _GetItemsSource()
        {
            return Engine.ItemsSource;
        }

        #region ViewDefinition
        private string _GetViewDefinition()
        {
            return Engine.ViewDefinition;
        }
        private void _SetViewDefinition(string value)
        {
            Engine.ViewDefinition = value;
        }
        #endregion ViewDefinition
    }
}
