﻿using C1.Web.Mvc.Grid;
using System.ComponentModel;
using System.Collections.Generic;
#if !MODEL
using C1.JsonNet;
using C1.Web.Mvc.Serialization;
#endif

namespace C1.Web.Mvc.Olap
{
    partial class PivotGrid
    {
        /// <summary>
        /// Gets or sets the id of the source.
        /// </summary>
        /// <remarks>
        /// It can be the id of a <see cref="PivotPanel"/> component or a <see cref="PivotEngine"/> component.
        /// </remarks>
        #if !MODEL
        [Json(true)]
        [JsonConverter(typeof(DefaultConverter))]
        #endif
        public override string ItemsSourceId
        {
            get
            {
                return base.ItemsSourceId;
            }
            set
            {
                base.ItemsSourceId = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the user can modify cell values using the mouse and keyboard. Default value is True
        /// </summary>
        [DefaultValue(true)]
        public override bool IsReadOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override AllowDragging AllowDragging
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool DeferResizing
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowAlternatingRows
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AutoGenerateColumns
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AllowAddNew
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AllowDelete
        {
            get;
            set;
        }    

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool AutoClipboard
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ChildItemsPath
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ColumnFootersRowHeaderText
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override CellTemplate ColumnFootersTemplate
        {
            get
            {
                return base.ColumnFootersTemplate;
            }
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override CellTemplate BottomLeftCellsTemplate
        {
            get
            {
                return base.BottomLeftCellsTemplate;
            }
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ColumnLayout
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override IList<Column> Columns
        {
            get
            {
                return base.Columns;
            }
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string GroupHeaderFormat
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ImeEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override IItemsSource<object> ItemsSource
        {
            get
            {
                return base.ItemsSource;
            }
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool NewRowAtTop
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientBeginningEdit
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientCellEditEnded
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientCellEditEnding
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientCopied
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientCopying
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDeletedRow
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDeletingRow
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDraggedColumn
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDraggingColumn
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDraggedRow
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDraggingColumnOver
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDraggingRow
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientDraggingRowOver
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientGroupCollapsedChanged
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientGroupCollapsedChanging
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientPasted
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientPastedCell
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientPasting
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientPastingCell
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientPrepareCellForEdit
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientRowAdded
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientRowEditEnded
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientRowEditEnding
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientRowEditStarted
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string OnClientRowEditStarting
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool PreserveOutlineState
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowColumnFooters
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowErrors
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ShowGroups
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int TreeIndent
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides to hide this property.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool ValidateEdits
        {
            get;
            set;
        }
    }
}
