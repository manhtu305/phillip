﻿namespace C1.Web.Mvc.Olap
{
    /// <summary>
    /// Specifies constants that define when the chart legend should be displayed.
    /// </summary>
    public enum LegendVisibility
    {
        /// <summary>
        /// Always show the legend.
        /// </summary>
        Always,
        /// <summary>
        /// Never show the legend.
        /// </summary>
        Never,
        /// <summary>
        /// Show the legend if the chart has more than one series.
        /// </summary>
        Auto
    }
}
