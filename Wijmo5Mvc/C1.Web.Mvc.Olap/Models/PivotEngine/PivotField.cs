﻿using System.Collections.Generic;

namespace C1.Web.Mvc.Olap
{
    public partial class PivotFieldBase
    {
        private PivotFilter _filter;
        private PivotFilter _GetFilter()
        {
            return _filter ?? (_filter = new PivotFilter());
        }
    }

    public partial class PivotField
    {
        private IList<PivotField> _subFields;
        private IList<PivotField> _GetSubFields()
        {
            return _subFields ?? (_subFields = new List<PivotField>());
        }
    }

    public partial class CubeField
    {
        private IList<CubeField> _subFields;
        private IList<CubeField> _GetSubFields()
        {
            return _subFields ?? (_subFields = new List<CubeField>());
        }
    }
}
