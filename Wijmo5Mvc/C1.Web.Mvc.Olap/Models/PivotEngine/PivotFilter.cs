﻿namespace C1.Web.Mvc.Olap
{
    public partial class PivotFilter
    {
        #region ValueFilter
        private ValueFilter _valueFilter;
        private ValueFilter _GetValueFilter()
        {
            return _valueFilter ?? (_valueFilter = new ValueFilter());
        }
        #endregion ValueFilter

        #region ConditionFilter
        private ConditionFilter _conditionFilter;
        private ConditionFilter _GetConditionFilter()
        {
            return _conditionFilter ?? (_conditionFilter = new ConditionFilter());
        }
        #endregion ConditionFilter
    }
}
