﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using UrlHelper = Microsoft.AspNetCore.Mvc.IUrlHelper;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif
#endif

namespace C1.Web.Mvc.Olap
{
    public partial class PivotEngine
    {
        private IItemsSource<object> _itemsSource;
        private PivotFieldCollection _fields;
        private PivotFieldCollection _rowFields;
        private PivotFieldCollection _columnFields;
        private PivotFieldCollection _valueFields;
        private PivotFieldCollection _filterFields;
        private string _itemsSourceId;
        private string _serviceUrl;
        private CubeService _cubeService;

#if !MODEL
        /// <summary>
        /// Creates one PivotEngine instance.
        /// </summary>
        /// <param name="helper">The html helper</param>
        public PivotEngine(HtmlHelper helper) : base(helper)
#else
        public PivotEngine()
#endif
        {
            Initialize();
        }

        private PivotFieldCollection _GetFields()
        {
            return _fields ?? (_fields = new PivotFieldCollection());
        }

        private PivotFieldCollection _GetRowFields()
        {
            return _rowFields ?? (_rowFields = new ViewFieldCollection());
        }

        private PivotFieldCollection _GetColumnFields()
        {
            return _columnFields ?? (_columnFields = new ViewFieldCollection());
        }

        private PivotFieldCollection _GetValueFields()
        {
            return _valueFields ?? (_valueFields = new ViewFieldCollection());
        }

        private PivotFieldCollection _GetFilterFields()
        {
            return _filterFields ?? (_filterFields = new ViewFieldCollection());
        }

        private IItemsSource<object> _GetItemsSource()
        {
            if (_itemsSource == null)
            {
                _itemsSourceId = null;
                _serviceUrl = null;
                _cubeService = null;

#if !MODEL
                _itemsSource = new CollectionViewService<object>(Helper);
#else
                _itemsSource = new CollectionViewService<object>();
#endif
                _itemsSource.DisableServerRead = true;
            }

            return _itemsSource;
        }

        private string _GetItemsSourceId()
        {
            return _itemsSourceId;
        }

        private void _SetItemsSourceId(string value)
        {
            _itemsSource = null;
            _serviceUrl = null;
            _cubeService = null;
            _itemsSourceId = value;
        }

        private string _GetServiceUrl()
        {
#if !MODEL
            return Url.GetFullUrl(_serviceUrl, UrlHelper);
#else
            return _serviceUrl;
#endif
        }

        private void _SetServiceUrl(string value)
        {
            _itemsSource = null;
            _itemsSourceId = null;
            _cubeService = null;
#if !MODEL
            if (value != null)
            {
                value = Url.GetValidateUrl(value);
            }
#endif
            _serviceUrl = value;
        }

        private CubeService _GetCubeService()
        {
            if (_cubeService == null)
            {
                _itemsSource = null;
                _itemsSourceId = null;
                _serviceUrl = null;
                _cubeService = new CubeService();
            }
            return _cubeService;
        }

        private bool _ShouldSerializeItemsSource()
        {
            return _itemsSource != null;
        }

        private bool _ShouldSerializeItemsSourceId()
        {
            return _itemsSource == null && !string.IsNullOrEmpty(ItemsSourceId);
        }

        private bool _ShouldSerializeCubeService()
        {
            return _cubeService != null;
        }
    }
}
