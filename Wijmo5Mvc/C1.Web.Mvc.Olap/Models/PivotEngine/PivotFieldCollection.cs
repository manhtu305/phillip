﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace C1.Web.Mvc.Olap
{
    public partial class PivotFieldCollection
    {
        #region Items
        private IList<PivotFieldBase> _items;
        private IList<PivotFieldBase> _GetItems()
        {
            return _items ?? (_items = new PivotFieldBaseCollection());
        }
        private void _SetItems(IList<PivotFieldBase> value)
        {
            _items = value;
        }
        #endregion Items

        private class PivotFieldBaseCollection : IList<PivotFieldBase>, IList
        {

            private List<PivotFieldBase> _collection;
            private bool? _isCubeFieldCollection;

            public PivotFieldBaseCollection()
            {
                _collection = new List<PivotFieldBase>();
            }

            public PivotFieldBase this[int index]
            {
                get
                {
                    return ((IList<PivotFieldBase>)_collection)[index];
                }

                set
                {
                    ((IList<PivotFieldBase>)_collection)[index] = value;
                }
            }

            object IList.this[int index]
            {
                get
                {
                    return ((IList)_collection)[index];
                }

                set
                {
                    ((IList)_collection)[index] = value;
                }
            }

            public int Count
            {
                get
                {
                    return ((IList<PivotFieldBase>)_collection).Count;
                }
            }

            public bool IsFixedSize
            {
                get
                {
                    return ((IList)_collection).IsFixedSize;
                }
            }

            public bool IsReadOnly
            {
                get
                {
                    return ((IList<PivotFieldBase>)_collection).IsReadOnly;
                }
            }

            public bool IsSynchronized
            {
                get
                {
                    return ((IList)_collection).IsSynchronized;
                }
            }

            public object SyncRoot
            {
                get
                {
                    return ((IList)_collection).SyncRoot;
                }
            }

            public int Add(object value)
            {
                var count = Count;
                InsertItem(count, value as PivotFieldBase);
                if(count + 1 == Count)
                {
                    return Count;
                }
                return -1;
            }

            public void Add(PivotFieldBase item)
            {
                InsertItem(Count, item);
            }

            public void Clear()
            {
                ((IList<PivotFieldBase>)_collection).Clear();
            }

            public bool Contains(object value)
            {
                return Contains(value as PivotFieldBase);
            }

            public bool Contains(PivotFieldBase item)
            {
                return ((IList<PivotFieldBase>)_collection).Contains(item);
            }

            public void CopyTo(Array array, int index)
            {
                ((IList)_collection).CopyTo(array, index);
            }

            public void CopyTo(PivotFieldBase[] array, int arrayIndex)
            {
                ((IList<PivotFieldBase>)_collection).CopyTo(array, arrayIndex);
            }

            public IEnumerator<PivotFieldBase> GetEnumerator()
            {
                return ((IList<PivotFieldBase>)_collection).GetEnumerator();
            }

            public int IndexOf(object value)
            {
                return IndexOf(value as PivotFieldBase);
            }

            public int IndexOf(PivotFieldBase item)
            {
                return ((IList<PivotFieldBase>)_collection).IndexOf(item);
            }

            public void Insert(int index, object value)
            {
                Insert(index, value as PivotFieldBase);
            }

            public void Insert(int index, PivotFieldBase item)
            {
                InsertItem(index, item);
            }

            public void Remove(object value)
            {
                Remove(value as PivotFieldBase);
            }

            public bool Remove(PivotFieldBase item)
            {
                return ((IList<PivotFieldBase>)_collection).Remove(item);
            }

            public void RemoveAt(int index)
            {
                ((IList<PivotFieldBase>)_collection).RemoveAt(index);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return ((IList<PivotFieldBase>)_collection).GetEnumerator();
            }

            private void InsertItem(int index, PivotFieldBase item)
            {
                if (_isCubeFieldCollection.HasValue)
                {
                    if (item is CubeField ^ _isCubeFieldCollection.Value)
                    {
                        throw new InvalidOperationException("Cannot append both cube field and non-cube field in the list.");
                    }
                }
                else
                {
                    _isCubeFieldCollection = item is CubeField;
                }
                _collection.Insert(index, item);
            }
        }
    }

    internal class ViewFieldCollection : PivotFieldCollection
    {
        public void SetItems(params string[] fieldKeys)
        {
            foreach (var fk in fieldKeys)
            {
                if (!Items.Any(item => item.Key == fk))
                {
                    Items.Add(new PivotField
                    {
                        Key = fk
                    });
                }
            }
        }
    }
}
