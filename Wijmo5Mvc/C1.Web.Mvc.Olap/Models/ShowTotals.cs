﻿namespace C1.Web.Mvc.Olap
{
    /// <summary>
    /// Specifies constants that define whether to include totals in the output table.
    /// </summary>
    public enum ShowTotals
    {
        /// <summary>
        /// Do not show any totals.
        /// </summary>
        None,
        /// <summary>
        /// Show grand totals.
        /// </summary>
        GrandTotals,
        /// <summary>
        /// Show subtotals and grand totals.
        /// </summary>
        Subtotals
    }
}
