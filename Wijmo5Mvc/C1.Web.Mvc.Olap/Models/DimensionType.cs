﻿namespace C1.Web.Mvc.Olap
{
    /// <summary>
    /// Defines the dimension types of a field.
    /// </summary>
    public enum DimensionType
    {
        /// <summary>
        /// A group of fields.
        /// </summary>
        Dimension,
        /// <summary>
        /// An aggregatable field.
        /// </summary>
        Measure,
        /// <summary>
        /// A field used to gauge performance of some value.
        /// </summary>
        Kpi,
        /// <summary>
        /// Multidimensional Expression (MDX) that returns a set of dimension members.
        /// </summary>
        NameSet,
        /// <summary>
        /// Provide supplementary information about dimension members.
        /// </summary>
        Attribute,
        /// <summary>
        /// A folder field.
        /// </summary>
        Folder,
        /// <summary>
        /// A discrete field.
        /// </summary>
        Hierarchy,
        /// <summary>
        /// Dimension with time-based levels of granularity for analysis and reporting.
        /// </summary>
        Date,
        /// <summary>
        /// Dimension whose attributes represent a list of currencies for financial reporting purposes.
        /// </summary>
        Currency
    }
}
