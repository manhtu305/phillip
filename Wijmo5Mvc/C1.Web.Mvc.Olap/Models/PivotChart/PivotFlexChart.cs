﻿#if !MODEL
#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Web.Mvc;
#endif
#endif

namespace C1.Web.Mvc.Olap
{
    internal partial class PivotFlexChart: FlexChart<object>
    {
#if !MODEL
        public PivotFlexChart(HtmlHelper helper, string selector = null) 
            : base(helper, selector)
        {
        }
#endif
        internal override bool _ShouldSerializeItemsSource()
        {
            return false;
        }
    }
}
