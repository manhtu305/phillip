﻿namespace C1.Web.Mvc.Olap
{
    partial class PivotChart
    {
        #region HeaderStyle
        private TitleStyle _headerStyle;
        private TitleStyle _GetHeaderStyle()
        {
            return _headerStyle ?? (_headerStyle = new TitleStyle());
        }
        #endregion HeaderStyle

        #region FooterStyle
        private TitleStyle _footerStyle;
        private TitleStyle _GetFooterStyle()
        {
            return _footerStyle ?? (_footerStyle = new TitleStyle());
        }
        #endregion FooterStyle

        #region FlexChart
        private FlexChart<object> _flexChart;
        private FlexChart<object> _GetFlexChart()
        {
            if(ChartType != PivotChartType.Pie && _flexChart == null)
            {
#if !MODEL
                _flexChart = new PivotFlexChart(Helper);
#else
                _flexChart = new PivotFlexChart();
#endif
            }
            return _flexChart;
        }
        // internal set for taghelper
        internal void _SetFlexChart(FlexChart<object> value)
        {
            if(ChartType == PivotChartType.Pie)
            {
                ChartType = PivotChartType.Column;
            }
            _flexChart = value;
        }
        private bool _ShouldSerializeFlexChart()
        {
            return ChartType != PivotChartType.Pie;
        }
        #endregion FlexChart

        #region FlexPie
        private FlexPie<object> _flexPie;
        private FlexPie<object> _GetFlexPie()
        {
            if (ChartType == PivotChartType.Pie && _flexPie == null)
            {
#if !MODEL
                _flexPie = new PivotFlexPie(Helper);
#else
                _flexPie = new PivotFlexPie();
#endif
            }
            return _flexPie;
        }
        // internal set for taghelper
        internal void _SetFlexPie(FlexPie<object> value)
        {
            ChartType = PivotChartType.Pie;
            _flexPie = value;
        }
        private bool _ShouldSerializeFlexPie()
        {
            return ChartType == PivotChartType.Pie;
        }
        #endregion FlexPie
    }
}
