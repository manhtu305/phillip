﻿namespace C1.Web.Mvc.Olap
{
    /// <summary>
    /// Specifies constants that define calculations to be applied to cells in the output view.
    /// </summary>
    public enum ShowAs
    {
        /// <summary>
        /// Show plain aggregated values.
        /// </summary>
        NoCalculation,
        /// <summary>
        /// Show differences between each item and the item in the previous row.
        /// </summary>
        DiffRow,
        /// <summary>
        /// Show differences between each item and the item in the previous row as a percentage.
        /// </summary>
        DiffRowPct,
        /// <summary>
        /// Show differences between each item and the item in the previous column.
        /// </summary>
        DiffCol,
        /// <summary>
        /// Show differences between each item and the item in the previous column as a percentage.
        /// </summary>
        DiffColPct,
        /// <summary>
        /// Show values as a percentage of the grand totals for the field.
        /// </summary>
        PctGrand,
        /// <summary>
        /// Show values as a percentage of the row totals for the field.
        /// </summary>
        PctRow,
        /// <summary>
        /// Show values as a percentage of the column totals for the field.
        /// </summary>
        PctCol,
        /// <summary>
        /// Show values as running totals.
        /// </summary>
        RunTot,
        /// <summary>
        /// Show values as percentage running totals.
        /// </summary>
        RunTotPct
    }
}
