﻿using C1.Util.Licensing;
using System.ComponentModel;

namespace C1.Web.Mvc.Olap
{
    /// <summary>
    /// Define a class for detecting C1.Web.Mvc.Olap license.
    /// </summary>
    [LicenseProvider]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class LicenseDetector : BaseLicenseDetector
    {
        /// <summary>
        /// The constructor.
        /// </summary>
        public LicenseDetector()
        {
        }
    }
}
