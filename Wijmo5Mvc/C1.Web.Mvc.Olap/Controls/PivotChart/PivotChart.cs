﻿using C1.Web.Mvc.WebResources;
using System;

namespace C1.Web.Mvc.Olap
{
    [Scripts(typeof(WebResources.Definitions.Olap))]
    partial class PivotChart
    {
        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        internal override string ClientSubModule
        {
            get
            {
                return "olap.";
            }
        }
    }
}
