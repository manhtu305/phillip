﻿#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
#endif
using System;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.Olap
{
    [Scripts(typeof(WebResources.Definitions.Olap))]
    partial class PivotPanel
    {
        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        internal override string ClientSubModule
        {
            get
            {
                return "olap.";
            }
        }

        /// <summary>
        /// Register the startup scripts.
        /// </summary>
        /// <param name="writer">the specified writer used to write the startup scripts.</param>
        protected override void RegisterStartupScript(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(ItemsSourceId))
            {
                Engine.RenderScripts(writer, false);
            }
            base.RegisterStartupScript(writer);
        }
    }
}
