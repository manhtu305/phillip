﻿using System;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.Olap
{
    [Scripts(typeof(WebResources.Definitions.Olap))]
    partial class PivotGrid
    {
        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        internal override string ClientSubModule
        {
            get
            {
                return "olap.";
            }
        }
    }
}
