﻿#if ASPNETCORE
using HtmlTextWriter = System.IO.TextWriter;
#else
using System.Web.UI;
#endif
using System;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.Olap
{
    [Scripts(typeof(WebResources.Definitions.Olap))]
    partial class Slicer
    {
        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        internal override string ClientSubModule
        {
            get
            {
                return "olap.";
            }
        }
    }
}
