﻿#if ASPNETCORE
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
using HtmlTextWriter = System.IO.TextWriter;
using UrlHelper = Microsoft.AspNetCore.Mvc.IUrlHelper;
#else
using System.Web.Mvc;
using System.Web.UI;
#endif
using System;
using C1.JsonNet;
using System.ComponentModel;
using C1.Web.Mvc.WebResources;

namespace C1.Web.Mvc.Olap
{
    [Scripts(typeof(WebResources.Definitions.Olap))]
    public partial class PivotEngine
    {
        internal override Type LicenseDetectorType
        {
            get { return typeof(LicenseDetector); }
        }

        /// <summary>
        /// Gets a value which represents the unique id for the control.
        /// </summary>
        [Ignore]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string UniqueId
        {
            get
            {
                return base.UniqueId;
            }
        }

        /// <summary>
        /// Render the client service to the specified writer.
        /// </summary>
        /// <param name="writer">The specified writer</param>
        protected override void RenderClientService(HtmlTextWriter writer)
        {
            RenderChildComponents(writer);
            base.RenderClientService(writer);
        }

        private void RenderChildComponents(HtmlTextWriter writer)
        {
            EnsureChildComponents();
            foreach (var c in Components)
            {
                var service = c as Service;
                if (service != null)
                {
                    service.RenderScripts(writer, false);
                }
            }
        }

        internal override string ClientSubModule
        {
            get
            {
                return "olap.";
            }
        }

        /// <summary>
        /// Ensure the child components created.
        /// </summary>
        protected override void CreateChildComponents()
        {
            if(Components.Count != 0)
            {
                return;
            }
            base.CreateChildComponents();
            if (_ShouldSerializeItemsSource())
            {
                Components.Add(_itemsSource as Component);
            }
        }
    }
}
