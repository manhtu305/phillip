﻿using C1.Web.Mvc.WebResources;

[assembly: AssemblyScripts(typeof(C1.Web.Mvc.Olap.WebResources.Definitions.All))]
[assembly: AssemblyStyles(typeof(C1.Web.Mvc.Olap.WebResources.Definitions.All))]

namespace C1.Web.Mvc.Olap.WebResources
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Definitions
    {
        [Scripts(typeof(Olap))]
        [Styles(typeof(Olap))]
        public abstract class All { }

        [Scripts(typeof(Mvc.WebResources.Definitions.CollectionView),
            typeof(Mvc.WebResources.Definitions.Input),
            typeof(Mvc.WebResources.Definitions.Grid),
            typeof(Mvc.WebResources.Definitions.Chart),
            typeof(GridExDefinitions.Filter),
            OlapWebResourcesHelper.WijmoJs + "wijmo.olap",
            OlapWebResourcesHelper.Shared + "Olap",
            OlapWebResourcesHelper.Mvc + "Olap",
            OlapWebResourcesHelper.Mvc + "Cast.Olap")]
        public abstract class Olap
        {
        }
    }
}
