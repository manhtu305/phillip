﻿using C1.Util.Licensing;
using System.Reflection;
using System.Runtime.InteropServices;

// licensing support
// Studio Ultimate
[assembly: C1ProductInfo("SU", "757CCC59-F365-4325-A676-0674C656B7A2")]
// Studio Enterprise
[assembly: C1ProductInfo("SE", "724e8a91-af12-4a3b-9aeb-ef89612e692e")]

#if !ASPNETCORE
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
#if GRAPECITY
[assembly: AssemblyTitle("ComponentOne Olap Control for ASP.NET MVC Edition JPN")]
[assembly: AssemblyDescription("ComponentOne Olap Control for ASP.NET MVC Edition JPN")]
[assembly: AssemblyProduct("ComponentOne Olap Control for ASP.NET MVC Edition JPN")]
#else
[assembly: AssemblyTitle("ComponentOne Olap Control for ASP.NET MVC Edition")]
[assembly: AssemblyDescription("ComponentOne Olap Control for ASP.NET MVC Edition")]
[assembly: AssemblyProduct("ComponentOne Olap Control for ASP.NET MVC Edition")]
#endif

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GrapeCity, Inc.")]
[assembly: AssemblyCopyright("Copyright © GrapeCity, Inc.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7912560c-c21d-40e1-96de-b4b00fa2e590")]
#endif

[assembly: AssemblyVersion(AssemblyInfo.Version)]
[assembly: AssemblyFileVersion(AssemblyInfo.Version)]