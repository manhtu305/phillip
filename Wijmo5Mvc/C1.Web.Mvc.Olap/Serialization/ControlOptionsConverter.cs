﻿using System;
using C1.JsonNet;

namespace C1.Web.Mvc.Olap
{
    /// <summary>
    /// Define a converter which converts the control to the json string of its properties.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class ControlOptionsConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        protected override object ReadJson(JsonReader reader, Type objectType, object existedValue)
        {
            return existedValue;
        }

        protected override void WriteJson(JsonWriter writer, object value)
        {
            var control = value as Control;
            if(control != null)
            {
                writer.WriteRawValue(control.SerializeOptions());
            }
        }
    }
}
