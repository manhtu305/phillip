﻿using System;
using C1.JsonNet;

namespace C1.Web.Mvc.Olap
{
    /// <summary>
    /// Define a converter which converts the view field collection.
    /// </summary>
    internal class ViewFieldCollectionConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        protected override void WriteJson(JsonWriter writer, object value)
        {
            if (value == null)
            {
                writer.WriteValue(null);
                return;
            }
            if (value is PivotFieldCollection)
            {
                PivotFieldCollection fields = (PivotFieldCollection)value;
                writer.StartObjectScope(fields);
                if (fields.MaxItems.HasValue)
                {
                    writer.WriteName("MaxItems");
                    writer.WriteValue(fields.MaxItems.Value);
                }
                if(fields.Items != null && fields.Items.Count > 0)
                {
                    writer.WriteName("Items");
                    writer.StartArrayScope(fields.Items);
                    foreach(var item in fields.Items)
                    {
                        writer.WriteValue(item.Key);
                    }
                    writer.EndScope();
                }
                writer.EndScope();
                return;
            }
            throw new InvalidCastException();
        }

        protected override object ReadJson(JsonReader reader, Type objectType, object existingValue)
        {
            //ReadJson is not supported.
            throw new NotSupportedException();
        }
    }
}
