﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.Olap.Fluent
{
    /// <summary>
    /// Extends <see cref="ScriptsBuilder"/> class for olap scripts.
    /// </summary>
    public static class ScriptsBuilderExtension
    {
        /// <summary>
        /// Registers olap related script bundle.
        /// </summary>
        /// <param name="scriptsBuilder">The <see cref="ScriptsBuilder"/>.</param>
        /// <returns>The <see cref="ScriptsBuilder"/>.</returns>
        public static ScriptsBuilder Olap(this ScriptsBuilder scriptsBuilder)
        {
            scriptsBuilder.OwnerTypes.AddRange(OlapWebResourcesHelper.AllScriptOwnerTypes.Value);
            return scriptsBuilder;
        }
    }
}
