﻿using C1.Web.Mvc.Fluent;
using System;

namespace C1.Web.Mvc.Olap.Fluent
{
    partial class PivotChartBuilder
    {
        /// <summary>
        /// Sets the FlexChart property.
        /// </summary>
        /// <remarks>
        /// Gets the inner <see cref="PivotFlexChart" /> object to show the data.
        /// </remarks>
        /// <param name="build">The builder for <see cref="PivotFlexChart"/></param>
        /// <returns>Current builder</returns>
        public PivotChartBuilder ShowFlexChart(Action<PivotFlexChartBuilder> build)
        {
            if(Object.ChartType == PivotChartType.Pie)
            {
                Object.ChartType = PivotChartType.Column;
            }
            build(new PivotFlexChartBuilder(Object.FlexChart));
            return this;
        }

        /// <summary>
        /// Sets the FlexPie property.
        /// </summary>
        /// <remarks>
        /// Gets the inner <see cref="PivotFlexPie" /> object to show the data.
        /// </remarks>
        /// <param name="build">The builder for <see cref="PivotFlexPie"/></param>
        /// <returns>Current builder</returns>
        public PivotChartBuilder ShowFlexPie(Action<PivotFlexPieBuilder> build)
        {
            Object.ChartType = PivotChartType.Pie;
            build(new PivotFlexPieBuilder(Object.FlexPie));
            return this;
        }
    }
}
