﻿using C1.Web.Mvc.Fluent;
using System.Collections.Generic;
using System.ComponentModel;
using System;

namespace C1.Web.Mvc.Olap.Fluent
{
    /// <summary>
    /// The builder for the FlexPie in PivotChart.
    /// </summary>
    public class PivotFlexPieBuilder : FlexPieBaseBuilder<object, FlexPie<object>, PivotFlexPieBuilder>
    {
        /// <summary>
        /// Create one PivotFlexPieBuilder instance.
        /// </summary>
        /// <param name="control">The FlexPie control.</param>
        public PivotFlexPieBuilder(FlexPie<object> control) : base(control)
        {
        }

        #region Hidden
        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexPieBuilder Bind(string nameProperty, string valueProperty, IEnumerable<object> sourceCollection = null)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexPieBuilder Bind(string nameProperty, string valueProperty, string readActionUrl)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotFlexPieBuilder Bind(Action<CollectionViewServiceBuilder<object>> itemsSourceBuild)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotFlexPieBuilder Bind(IEnumerable<object> sourceCollection)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotFlexPieBuilder Bind(string readActionUrl)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotFlexPieBuilder ItemsSourceId(string Id)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexPieBuilder Id(string id)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexPieBuilder TemplateBind(string propertyName, string boundName)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexPieBuilder ToTemplate()
        {
            return this;
        }
        #endregion Hidden
    }
}
