﻿using C1.Web.Mvc.Chart;
using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Mvc.Olap.Fluent
{
    /// <summary>
    /// The builder for the FlexChart in PivotChart.
    /// </summary>
    public class PivotFlexChartBuilder : FlexChartCoreBuilder<object, FlexChart<object>, PivotFlexChartBuilder>
    {
        /// <summary>
        /// Create one PivotFlexChartBuilder instance.
        /// </summary>
        /// <param name="control">The FlexChart control.</param>
        public PivotFlexChartBuilder(FlexChart<object> control) : base(control)
        {
        }

        /// <summary>
        /// Sets the GroupWidth property.
        /// </summary>
        /// <remarks>
        /// Specifies the group width for Column charts, or the group height for Bar charts.The group width can be specified in pixels or percent of available space.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public PivotFlexChartBuilder GroupWidth(string value)
        {
            Object.Options.GroupWidth = value;
            return this;
        }

        /// <summary>
        /// Sets the Stacking property.
        /// </summary>
        /// <remarks>
        /// Gets or sets whether and how series objects are stacked.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public PivotFlexChartBuilder Stacking(Stacking value)
        {
            Object.Stacking = value;
            return this;
        }

        #region Hidden
        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder AxisX(Action<ChartAxisBuilder<object>> build)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder AxisY(Action<ChartAxisBuilder<object>> build)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder BindingX(string value)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder SymbolSize(float value)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder SelectionIndex(int? value)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder Bind(string xDataFieldName, string readActionUrl)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder Bind(string xDataFieldName, string valueProperty, string readActionUrl)
        {
            return this;
        }


        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder Bind(string xDataFieldName, IEnumerable<object> sourceCollection)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder Bind(string xDataFieldName, string valueProperty, IEnumerable<object> sourceCollection)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder AxisX(Position position)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder AxisY(Position position)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotFlexChartBuilder Bind(Action<CollectionViewServiceBuilder<object>> itemsSourceBuild)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotFlexChartBuilder Bind(IEnumerable<object> sourceCollection)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotFlexChartBuilder Bind(string readActionUrl)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotFlexChartBuilder ItemsSourceId(string Id)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder Id(string id)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder TemplateBind(string propertyName, string boundName)
        {
            return this;
        }

        /// <summary>
        /// The builder is hidden.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new PivotFlexChartBuilder ToTemplate()
        {
            return this;
        }
        #endregion Hidden
    }
}
