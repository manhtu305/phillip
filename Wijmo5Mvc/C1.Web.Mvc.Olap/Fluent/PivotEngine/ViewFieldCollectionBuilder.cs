﻿using C1.Web.Mvc.Fluent;

namespace C1.Web.Mvc.Olap.Fluent
{
    /// <summary>
    /// Defines a builder for RowFields,ColumnFields, ValueFields and FilterFields.
    /// </summary>
    public class ViewFieldCollectionBuilder : BaseBuilder<PivotFieldCollection, ViewFieldCollectionBuilder>
    {
        /// <summary>
        /// Create one ViewFieldCollectionBuilder instance.
        /// </summary>
        /// <param name="obj">The PivotFieldCollection object.</param>
        public ViewFieldCollectionBuilder(PivotFieldCollection obj) : base(obj)
        {
        }

        /// <summary>
        /// Configure the field collection according to the specified field key list.
        /// </summary>
        /// <param name="fieldKeys">The field key list.</param>
        /// <returns>The builder.</returns>
        public ViewFieldCollectionBuilder Items(params string[] fieldKeys)
        {
            var viewFieldCollection = Object as ViewFieldCollection;
            if (viewFieldCollection != null)
            {
                viewFieldCollection.SetItems(fieldKeys);
            }

            return this;
        }

        /// <summary>
        /// Sets the MaxItems property.
        /// </summary>
        /// <remarks>
        /// Gets or sets the maximum number of fields allowed in this collection.
        /// </remarks>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public ViewFieldCollectionBuilder MaxItems(int? value)
        {
            Object.MaxItems = value;
            return this;
        }
    }
}
