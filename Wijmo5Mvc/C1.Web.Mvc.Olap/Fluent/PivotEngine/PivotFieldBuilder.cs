﻿using C1.Web.Mvc.Fluent;
using System;

namespace C1.Web.Mvc.Olap.Fluent
{
    partial class PivotFieldBuilder
    {
        /// <summary>
        /// Configure <see cref="PivotField"/>.
        /// </summary>
        /// <param name="build">The build action.</param>
        /// <returns>Current builder.</returns>
        public PivotFieldBuilder SubFields(Action<ListItemFactory<PivotField, PivotFieldBuilder>> build)
        {
            build(new ListItemFactory<PivotField, PivotFieldBuilder>(Object.SubFields,
                () => new PivotField(), c => new PivotFieldBuilder(c)));
            return this;
        }
    }

    partial class CubeFieldBuilder
    {
        /// <summary>
        /// Configure <see cref="CubeField"/>.
        /// </summary>
        /// <param name="build">The build action.</param>
        /// <returns>Current builder.</returns>
        public CubeFieldBuilder SubFields(Action<ListItemFactory<CubeField, CubeFieldBuilder>> build)
        {
            build(new ListItemFactory<CubeField, CubeFieldBuilder>(Object.SubFields,
                () => new CubeField(), c => new CubeFieldBuilder(c)));
            return this;
        }
    }
}
