﻿using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Olap.Fluent
{
    public partial class PivotFieldCollectionBuilder
    {
        /// <summary>
        /// Configure the field collection.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>The builder.</returns>
        public PivotFieldCollectionBuilder Items(Action<FieldItemFactory> builder)
        {
            builder(new FieldItemFactory(Object.Items));
            return this;
        }
    }

    /// <summary>
    /// Defines a factory to create different fields.
    /// </summary>
    public class FieldItemFactory : BaseBuilder<IList<PivotFieldBase>, FieldItemFactory>
    {
        #region Ctor
        /// <summary>
        /// Initializes an instance of the <see cref="FieldItemFactory"/> class 
        /// by using the specified collection.
        /// </summary>
        /// <param name="list">The specified collection.</param>
        public FieldItemFactory(IList<PivotFieldBase> list)
            : base(list)
        {
        }
        #endregion Ctor

        #region Methods
        /// <summary>
        /// Add a <see cref="PivotField"/> instance updated by the specified action to the collection.
        /// </summary>
        /// <param name="build">The specified action to update TItem instance.</param>
        /// <returns>The factory instance</returns>
        public FieldItemFactory AddPivotField(Action<PivotFieldBuilder> build)
        {
            var field = new PivotField();
            Object.Add(field);
            build(new PivotFieldBuilder(field));
            return this;
        }

        /// <summary>
        /// Add a <see cref="PivotField"/> instance to the collection.
        /// </summary>
        /// <param name="item">The <see cref="PivotField"/> instance.</param>
        /// <returns>The factory instance</returns>
        public virtual FieldItemFactory AddPivotField(PivotField item)
        {
            Object.Add(item);
            return this;
        }

        /// <summary>
        /// Add a <see cref="CubeField"/> instance updated by the specified action to the collection.
        /// </summary>
        /// <param name="build">The specified action to update TItem instance.</param>
        /// <returns>The factory instance</returns>
        public FieldItemFactory AddCubeField(Action<CubeFieldBuilder> build)
        {
            var field = new CubeField();
            Object.Add(field);
            build(new CubeFieldBuilder(field));
            return this;
        }

        /// <summary>
        /// Add a <see cref="CubeField"/> instance to the collection.
        /// </summary>
        /// <param name="item">The <see cref="CubeField"/> instance.</param>
        /// <returns>The factory instance</returns>
        public virtual FieldItemFactory AddCubeField(CubeField item)
        {
            Object.Add(item);
            return this;
        }
        #endregion Methods
    }
}
