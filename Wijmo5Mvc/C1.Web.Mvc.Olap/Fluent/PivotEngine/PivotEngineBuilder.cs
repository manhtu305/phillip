﻿using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Olap.Fluent
{
    public partial class PivotEngineBuilder
    {
        /// <summary>
        /// Binds the service.
        /// </summary>
        /// <param name="serviceUrl">The service url.</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder BindService(string serviceUrl)
        {
            Object.ServiceUrl = serviceUrl;
            return this;
        }

        /// <summary>
        /// Bind to a collection.
        /// </summary>
        /// <param name="sourceCollection">The source collection.</param>
        /// <returns>Current builder.</returns>
        public PivotEngineBuilder Bind(IEnumerable<object> sourceCollection)
        {
            return Bind(isb => isb.Bind(sourceCollection));
        }

        /// <summary>
        /// Sets ItemsSource by builder.
        /// </summary>
        /// <param name="itemsSourceBuild">The build action for setting ItemsSource.</param>
        /// <returns>Current builder.</returns>
        public PivotEngineBuilder Bind(Action<CollectionViewServiceBuilder<object>> itemsSourceBuild)
        {
            itemsSourceBuild(new CollectionViewServiceBuilder<object>(Object.ItemsSource as CollectionViewService<object>));
            return this;
        }

        /// <summary>
        /// Sets the read action url.
        /// </summary>
        /// <param name="readActionUrl">The action url.</param>
        /// <returns>Current builder.</returns>
        public PivotEngineBuilder Bind(string readActionUrl)
        {
            return Bind(isb => isb.Bind(readActionUrl));
        }

        /// <summary>
        /// Sets CubeService by builder.
        /// </summary>
        /// <param name="url">The url of the SSAS server.</param>
        /// <param name="cube">The OLAP cube in the SSAS server.</param>
        /// <param name="catalog">The catalog name</param>
        /// <param name="user">The username in the SSAS server for windows authentication</param>
        /// <param name="password">The password in the SSAS server for windows authentication</param>
        /// <returns>Current builder.</returns>
        public PivotEngineBuilder BindCubeService(string url, string cube, string catalog = null, string user = null, string password = null)
        {
            var cubeService = Object.CubeService;
            cubeService.Url = url;
            cubeService.Cube = cube;
            if (!string.IsNullOrEmpty(catalog))
            {
                cubeService.Catalog = catalog;
            }
            if (!string.IsNullOrEmpty(user))
            {
                cubeService.User = user;
            }
            if (!string.IsNullOrEmpty(password))
            {
                cubeService.Password = password;
            }
            return this;
        }

        /// <summary>
        /// Sets CubeService by builder.
        /// </summary>
        /// <param name="cubeServiceBuild">The build action for setting CubeService.</param>
        /// <returns>Current builder.</returns>
        public PivotEngineBuilder BindCubeService(Action<CubeServiceBuilder> cubeServiceBuild)
        {
            cubeServiceBuild(new CubeServiceBuilder(Object.CubeService));
            return this;
        }

        /// <summary>
        /// Sets the RowFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields shown as rows in the output table.
        /// </remarks>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder RowFields(Action<ViewFieldCollectionBuilder> build)
        {
            build(new ViewFieldCollectionBuilder(Object.RowFields));
            return this;
        }

        /// <summary>
        /// Sets the RowFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields shown as rows in the output table.
        /// </remarks>
        /// <param name="fieldKeys">The field keys.</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder RowFields(params string[] fieldKeys)
        {
            new ViewFieldCollectionBuilder(Object.RowFields).Items(fieldKeys);
            return this;
        }

        /// <summary>
        /// Sets the ColumnFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields shown as columns in the output table.
        /// </remarks>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder ColumnFields(Action<ViewFieldCollectionBuilder> build)
        {
            build(new ViewFieldCollectionBuilder(Object.ColumnFields));
            return this;
        }

        /// <summary>
        /// Sets the ColumnFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields shown as columns in the output table.
        /// </remarks>
        /// <param name="fieldKeys">The field keys.</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder ColumnFields(params string[] fieldKeys)
        {
            new ViewFieldCollectionBuilder(Object.ColumnFields).Items(fieldKeys);
            return this;
        }

        /// <summary>
        /// Sets the FilterFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields used as filters.
        /// </remarks>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder FilterFields(Action<ViewFieldCollectionBuilder> build)
        {
            build(new ViewFieldCollectionBuilder(Object.FilterFields));
            return this;
        }

        /// <summary>
        /// Sets the FilterFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields used as filters.
        /// </remarks>
        /// <param name="fieldKeys">The field keys.</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder FilterFields(params string[] fieldKeys)
        {
            new ViewFieldCollectionBuilder(Object.FilterFields).Items(fieldKeys);
            return this;
        }

        /// <summary>
        /// Sets the ValueFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields summarized in the output table.
        /// </remarks>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder ValueFields(Action<ViewFieldCollectionBuilder> build)
        {
            build(new ViewFieldCollectionBuilder(Object.ValueFields));
            return this;
        }

        /// <summary>
        /// Sets the ValueFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields summarized in the output table.
        /// </remarks>
        /// <param name="fieldKeys">The field keys.</param>
        /// <returns>Current builder</returns>
        public PivotEngineBuilder ValueFields(params string[] fieldKeys)
        {
            new ViewFieldCollectionBuilder(Object.ValueFields).Items(fieldKeys);
            return this;
        }

        /// <summary>
        /// Creates one <see cref="PivotEngineBuilder" /> instance to configurate <paramref name="component"/>.
        /// </summary>
        /// <param name="component">The <see cref="PivotEngine" /> object to be configurated.</param>
        public PivotEngineBuilder(PivotEngine component) : base(component)
        {
        }
    }
}
