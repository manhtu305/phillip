﻿using C1.Web.Mvc.Fluent;
using System;
using System.Collections.Generic;

namespace C1.Web.Mvc.Olap.Fluent
{
    partial class PivotPanelBuilder
    {
        /// <summary>
        /// Sets the engine being controlled by this <see cref="PivotPanel"/>.
        /// </summary>
        ///<param name="engineBuild">The build action to set the engine.</param>
        /// <returns>Current builder.</returns>
        public PivotPanelBuilder BindEngine(Action<PivotEngineBuilder> engineBuild)
        {
            engineBuild(new PivotEngineBuilder(Object.Engine));
            return this;
        }

        /// <summary>
        /// Binds the service.
        /// </summary>
        /// <param name="serviceUrl">The service url.</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder BindService(string serviceUrl)
        {
            return BindEngine(peb => peb.BindService(serviceUrl));
        }

        /// <summary>
        /// Bind to a collection for ItemsSource.
        /// </summary>
        /// <param name="sourceCollection">The source collection.</param>
        /// <returns>Current builder.</returns>
        public PivotPanelBuilder Bind(IEnumerable<object> sourceCollection)
        {
            return Bind(isb => isb.Bind(sourceCollection));
        }

        /// <summary>
        /// Sets ItemsSource by builder.
        /// </summary>
        /// <param name="itemsSourceBuild">The build action for setting ItemsSource.</param>
        /// <returns>Current builder.</returns>
        public PivotPanelBuilder Bind(Action<CollectionViewServiceBuilder<object>> itemsSourceBuild)
        {
            itemsSourceBuild(new CollectionViewServiceBuilder<object>(Object.ItemsSource as CollectionViewService<object>));
            return this;
        }

        /// <summary>
        /// Sets the read action url for ItemsSource.
        /// </summary>
        /// <param name="readActionUrl">The action url.</param>
        /// <returns>Current builder.</returns>
        public PivotPanelBuilder Bind(string readActionUrl)
        {
            return Bind(isb => isb.Bind(readActionUrl));
        }

        /// <summary>
        /// Sets the RowFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields shown as rows in the output table.
        /// </remarks>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder RowFields(Action<ViewFieldCollectionBuilder> build)
        {
            build(new ViewFieldCollectionBuilder(Object.RowFields));
            return this;
        }

        /// <summary>
        /// Sets the RowFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields shown as rows in the output table.
        /// </remarks>
        /// <param name="fieldKeys">The field keys.</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder RowFields(params string[] fieldKeys)
        {
            new ViewFieldCollectionBuilder(Object.RowFields).Items(fieldKeys);
            return this;
        }

        /// <summary>
        /// Sets the ColumnFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields shown as columns in the output table.
        /// </remarks>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder ColumnFields(Action<ViewFieldCollectionBuilder> build)
        {
            build(new ViewFieldCollectionBuilder(Object.ColumnFields));
            return this;
        }

        /// <summary>
        /// Sets the ColumnFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields shown as columns in the output table.
        /// </remarks>
        /// <param name="fieldKeys">The field keys.</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder ColumnFields(params string[] fieldKeys)
        {
            new ViewFieldCollectionBuilder(Object.ColumnFields).Items(fieldKeys);
            return this;
        }

        /// <summary>
        /// Sets the FilterFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields used as filters.
        /// </remarks>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder FilterFields(Action<ViewFieldCollectionBuilder> build)
        {
            build(new ViewFieldCollectionBuilder(Object.FilterFields));
            return this;
        }

        /// <summary>
        /// Sets the FilterFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields used as filters.
        /// </remarks>
        /// <param name="fieldKeys">The field keys.</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder FilterFields(params string[] fieldKeys)
        {
            new ViewFieldCollectionBuilder(Object.FilterFields).Items(fieldKeys);
            return this;
        }

        /// <summary>
        /// Sets the ValueFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields summarized in the output table.
        /// </remarks>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder ValueFields(Action<ViewFieldCollectionBuilder> build)
        {
            build(new ViewFieldCollectionBuilder(Object.ValueFields));
            return this;
        }

        /// <summary>
        /// Sets the ValueFields property.
        /// </summary>
        /// <remarks>
        /// Gets the list of <see cref="PivotField" /> objects that define the fields summarized in the output table.
        /// </remarks>
        /// <param name="fieldKeys">The field keys.</param>
        /// <returns>Current builder</returns>
        public PivotPanelBuilder ValueFields(params string[] fieldKeys)
        {
            new ViewFieldCollectionBuilder(Object.ValueFields).Items(fieldKeys);
            return this;
        }
    }
}
