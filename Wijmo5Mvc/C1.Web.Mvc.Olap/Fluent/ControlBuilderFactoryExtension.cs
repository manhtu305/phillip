﻿using C1.Web.Mvc.Fluent;
#if ASPNETCORE
using IHtmlString = Microsoft.AspNetCore.Html.IHtmlContent;
#endif

namespace C1.Web.Mvc.Olap.Fluent
{
    /// <summary>
    /// Extends ControlBuilderFactory for olap controls creation.
    /// </summary>
    public static class ControlBuilderFactoryExtension
    {
        /// <summary>
        /// Create a <see cref="PivotEngineBuilder"/>.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <returns>The <see cref="PivotEngineBuilder"/>.</returns>
        public static PivotEngineBuilder PivotEngine(this ControlBuilderFactory controlBuilderFactory)
        {
            return new PivotEngineBuilder(new PivotEngine(controlBuilderFactory._helper));
        }

        /// <summary>
        /// Create a <see cref="PivotPanelBuilder"/>.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <param name="selector">The selector</param>
        /// <returns>The <see cref="PivotPanelBuilder"/>.</returns>
        public static PivotPanelBuilder PivotPanel(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new PivotPanelBuilder(new PivotPanel(controlBuilderFactory._helper, selector));
        }

        /// <summary>
        /// Create a <see cref="PivotGridBuilder"/>.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <param name="selector">The selector</param>
        /// <returns>The <see cref="PivotGridBuilder"/>.</returns>
        public static PivotGridBuilder PivotGrid(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new PivotGridBuilder(new PivotGrid(controlBuilderFactory._helper, selector));
        }

        /// <summary>
        /// Create a <see cref="PivotChartBuilder"/>.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <param name="selector">The selector</param>
        /// <returns>The <see cref="PivotChartBuilder"/>.</returns>
        public static PivotChartBuilder PivotChart(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new PivotChartBuilder(new PivotChart(controlBuilderFactory._helper, selector));
        }

        /// <summary>
        /// Create a <see cref="SlicerBuilder"/>.
        /// </summary>
        /// <param name="controlBuilderFactory">The ControlBuilderFactory.</param>
        /// <param name="selector">The selector</param>
        /// <returns>The <see cref="SlicerBuilder"/>.</returns>
        public static SlicerBuilder Slicer(this ControlBuilderFactory controlBuilderFactory, string selector = null)
        {
            return new SlicerBuilder(new Slicer(controlBuilderFactory._helper, selector));
        }
    }
}
