﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using C1.Web.Mvc.Fluent;
using C1.Web.Mvc.Grid;

namespace C1.Web.Mvc.Olap.Fluent
{
    partial class PivotGridBuilder
    {
        /// <summary>
        /// Sets the source id.
        /// </summary>
        /// <param name="Id">The id of the source. The source could be a <see cref="PivotPanel"/> component or a <see cref="PivotEngine"/> component.</param>
        /// <returns>Current builder.</returns>
        public override PivotGridBuilder ItemsSourceId(string Id)
        {
            return base.ItemsSourceId(Id);
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder AllowAddNew(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder AllowDelete(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder AllowDragging(AllowDragging value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder AutoClipboard(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder AutoGenerateColumns(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="readActionUrl">The action url.</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder Bind(string readActionUrl)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="itemsSourceBuild">The build action for setting ItemsSource</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder Bind(Action<CollectionViewServiceBuilder<object>> itemsSourceBuild)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="sourceCollection">The source collection</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder Bind(IEnumerable<object> sourceCollection)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder BottomLeftCellsTemplate(Action<CellTemplateBuilder> build)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ChildItemsPath(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="build">The build function</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ColumnFootersTemplate(Action<CellTemplateBuilder> build)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ColumnLayout(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="builder">The builder action.</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder Columns(Action<ListItemFactory<Column, ColumnBuilder>> builder)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder DeferResizing(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="names">The property names used for grouping.</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder GroupBy(params string[] names)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder GroupHeaderFormat(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ImeEnabled(bool value)
        {
            return this;
        }

        /// <summary>
        /// Gets or sets a value that determines whether the user can modify cell values using the mouse and keyboard. Default value is true
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        public override PivotGridBuilder IsReadOnly(bool value)
        {
            Object.IsReadOnly = value;
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder NewRowAtTop(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientBeginningEdit(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientCellEditEnded(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientCellEditEnding(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientCopied(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientCopying(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientDeletedRow(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientDeletingRow(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientDraggedColumn(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientDraggedRow(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientDraggingColumn(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientDraggingColumnOver(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientDraggingRow(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientDraggingRowOver(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientGroupCollapsedChanged(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientGroupCollapsedChanging(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientPasted(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientPastedCell(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientPasting(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientPastingCell(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientPrepareCellForEdit(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientRowAdded(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientRowEditEnded(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientRowEditEnding(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientRowEditStarted(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OnClientRowEditStarting(string value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="names">Sort by these names.</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OrderBy(params string[] names)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="names">Sort by these names with descending.</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder OrderByDescending(params string[] names)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="pageSize">The page size.</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder PageSize(int pageSize)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder PreserveOutlineState(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder PreserveSelectedState(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ShowAlternatingRows(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">A bool value indicates whether to show a group row.</param>
        /// <param name="rowHeaderText">The row header text of the group row. If it is not set, it will use the default value: a sigma character.</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ShowColumnFooters(bool value = true, string rowHeaderText = null)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ShowErrors(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ShowGroups(bool value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder TreeIndent(int value)
        {
            return this;
        }

        /// <summary>
        /// Overrides to hide this method.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Current builder</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override PivotGridBuilder ValidateEdits(bool value)
        {
            return this;
        }
    }
}
