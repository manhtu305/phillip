﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Builder
{
    class Program
    {

        #region "** json struct"
        public struct FilePackage
        {
            public string source;
            public string package;
        }
        public struct ExternalPackage
        {
            public string source;
            public string package;
            public bool term;
            public string combine;
        }
        public struct SamplePackage
        {
            public string source;
            public string dest;
            public string package;
        }
        public struct jsver
        {
            public string wijmo;
            public string jquery;
            public string jqueryui;
        }
        public struct copyright
        {
            public string open;
            public string complete;
        }
        public struct wijmo
        {
            public FilePackage[] js;
            public SamplePackage[] sample;
            public ExternalPackage[] externals;
            public FilePackage[] themes;
            public jsver version;
            public copyright copyright;
            
        };
        #endregion

        static string _help = "builder <json> <root> <dest> [all, theme, control, extender]";
        static DirectoryInfo _rootDir = null;
        static DirectoryInfo _destDir = null;
        static string _json = "";
        static wijmo _buildInfo;
        static UTF8Encoding _utf8NoBOM = new UTF8Encoding(false);

        static void Main(string[] args)
        {
            _json = args[0];
            _rootDir = new DirectoryInfo(args[1]);
            _destDir = new DirectoryInfo(args[2]);

            // deserialize the json
            JsonSerializer se = new JsonSerializer();
            _buildInfo = (wijmo)se.Deserialize(new JsonTextReader(new StreamReader(_json)),typeof(wijmo));

            // update the version in the copyright notice
            _buildInfo.copyright.open = _buildInfo.copyright.open.Replace("@VERSION@", _buildInfo.version.wijmo);
            _buildInfo.copyright.complete = _buildInfo.copyright.complete.Replace("@VERSION@", _buildInfo.version.wijmo);

            // create the destination folder
            string destPath = _destDir.FullName + "\\wijmo." + _buildInfo.version.wijmo;
            if (Directory.Exists(destPath))
            {
                Directory.Delete(destPath,true);
            }
            Directory.CreateDirectory(destPath);

            _destDir = new DirectoryInfo(destPath);

            ProcessJS();
        }

        static void CreateFolders(DirectoryInfo root)
        {
            root.CreateSubdirectory("css");
            root.CreateSubdirectory("css/images");
            root.CreateSubdirectory("js");
            root.CreateSubdirectory("development-bundle");
            root.CreateSubdirectory("development-bundle/external");
            root.CreateSubdirectory("development-bundle/samples");
            root.CreateSubdirectory("development-bundle/themes");
            root.CreateSubdirectory("development-bundle/themes/wijmo");
            root.CreateSubdirectory("development-bundle/themes/wijmo/images");
            root.CreateSubdirectory("development-bundle/wijmo");
            root.CreateSubdirectory("development-bundle/wijmo/minified");
        }

        // copies the js files and updates the copyright
        static void CopyJS(string rootPath, string destPath, FilePackage[] files, string packageID, string copyright)
        {
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].package == packageID || files[i].package == "both")
                {
                    UpdateCopyright(copyright, rootPath + "\\" + files[i].source, destPath + "\\" + files[i].source);
                }
            }
        }

        // minifies the js
        static void MinifyJS(string path, string copyright)
        {
            string destPath = path + "\\minified";
            foreach (var file in Directory.EnumerateFiles(path, "*.js"))
            {
                string temppath = Path.GetTempPath() + @"\derf.min";
                if (File.Exists(temppath)) File.Delete(temppath);

                string minifiedTool = _rootDir + "\\tools\\AjaxMin.exe";
                ProcessStartInfo psi = new ProcessStartInfo(minifiedTool, "-JS -term -clobber:true " + file + " -out " + temppath);

                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;

                Process minify = Process.Start(psi);
                minify.WaitForExit();
                FileInfo f = new FileInfo(file);
                UpdateCopyright(copyright, temppath, destPath+"\\"+f.Name.Replace(".js",".min.js"));

            }
        }

        static void CombineJS(string path, string edition, string copyright, ExternalPackage[] externals)
        {
            string combineJsDir = path + "\\js";
            string jsDir = path + "\\development-bundle\\wijmo";
            string minJsDir = jsDir + "\\minified";

            StreamWriter js = new StreamWriter(combineJsDir+"\\jquery.wijmo-"+edition.ToLower()+".all."+_buildInfo.version.wijmo+".js", false, _utf8NoBOM);
            StreamWriter minJs = new StreamWriter(combineJsDir+"\\jquery.wijmo-"+edition.ToLower()+".all."+_buildInfo.version.wijmo+".min.js", false, _utf8NoBOM);
            minJs.WriteLine(copyright);
            js.WriteLine(copyright);

            // add the externals first
            for (int i = 0; i < externals.Length; i++)
            {
                if (externals[i].combine == edition)
                {
                    StreamReader reader = new StreamReader(path + "\\development-bundle\\external\\" + externals[i].source);
                    string s = reader.ReadToEnd();
                    js.Write(s);
                    minJs.Write(s);
                    if (externals[i].term)
                    {
                        js.WriteLine(";");
                        minJs.WriteLine(";");
                    }
                    else
                    {
                        js.WriteLine();
                        minJs.WriteLine();
                    }
                }
            }

            foreach (var file in Directory.EnumerateFiles(jsDir, "*.js"))
            {
                StreamReader reader = new StreamReader(file);
                js.WriteLine(reader.ReadToEnd());
            }
            js.Close();

            foreach (var file in Directory.EnumerateFiles(minJsDir, "*.js"))
            {
                var regex = new Regex(@"/\*((?!\*/).)*\*/", RegexOptions.Singleline);
                StreamReader reader = new StreamReader(file);
                string s = reader.ReadToEnd();
                minJs.Write(regex.Replace(s,""));
            }
            minJs.Close();

        }

        static string VersionRef(string s)
        {
            s = s.Replace("@@version@@", _buildInfo.version.wijmo);
            s = s.Replace("@@jquery@@", _buildInfo.version.jquery);
            s = s.Replace("@@jqueryui@@", _buildInfo.version.jqueryui);

            return s;
        }

        static void CopyExtern(string srcPath, string destPath, string edition, ExternalPackage []files)
        {
            for (int i = 0; i < files.Length; i++)
            {
                files[i].source = VersionRef(files[i].source);
                if (files[i].package == edition || files[i].package == "both")
                {
                    if (Directory.Exists(srcPath + "\\" + files[i].source))
                    {
                        CopyFolder(srcPath + "\\" + files[i].source, destPath + "\\" + files[i].source);
                    }
                    else
                    {
                        CopyFile(srcPath + "\\" + files[i].source, destPath + "\\" + files[i].source);
                    }
                }
            }
        }

        // copies the js files and updates the copyright
        static void CopyCSS(string rootPath, string destPath, FilePackage[] files, string packageID, string copyright)
        {
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].package == packageID || files[i].package == "both")
                {
                    if (Directory.Exists(rootPath + "\\" + files[i].source))
                    {
                        CopyFolder(rootPath + "\\" + files[i].source, destPath + "\\" + files[i].source);
                    }
                    else
                    {
                        CopyFile(rootPath + "\\" + files[i].source, destPath + "\\" + files[i].source);
                    }
                }
            }
        }

        static void CombineCSS(string srcPath, string destPath, FilePackage []files, string edition, string copyright)
        {
            StreamWriter combine = new StreamWriter(destPath+"\\jquery.wijmo-open."+_buildInfo.version.wijmo+".css",false, _utf8NoBOM);
            StreamWriter minCombine = new StreamWriter(destPath+"\\jquery.wijmo-open."+_buildInfo.version.wijmo+".min.css", false,_utf8NoBOM);
            minCombine.Write(copyright);
            
            for(int i=0; i < files.Length; i++)
            {
                // don't combine directories
                if(!Directory.Exists(srcPath+"\\"+files[i].source))
                {
                    if (files[i].package == edition || files[i].package == "both")
                    {
                        if (isBinaryFile(srcPath + "\\" + files[i].source))
                        {
                            FileInfo fi = new FileInfo(srcPath + "\\" + files[i].source);
                            CopyFile(srcPath + "\\" + files[i].source, destPath + "\\images\\" + fi.Name);
                        }
                        else
                        {
                            StreamReader reader = new StreamReader(srcPath + "\\" + files[i].source);
                            combine.WriteLine(reader.ReadToEnd());
                            reader.Close();

                            string temppath = Path.GetTempPath() + @"\derf.min";
                            if (File.Exists(temppath)) File.Delete(temppath);

                            string minifiedTool = _rootDir + "\\tools\\AjaxMin.exe";

                            ProcessStartInfo psi = new ProcessStartInfo(minifiedTool, "-CSS -term -clobber:true " + srcPath + "\\" + files[i].source + " -out " + temppath);

                            psi.WindowStyle = ProcessWindowStyle.Hidden;
                            psi.UseShellExecute = false;

                            Process minify = Process.Start(psi);

                            minify.WaitForExit();

                            minCombine.Write(File.ReadAllText(temppath));
                        }
                    }
                }

            }
            combine.Close();
            minCombine.Close();
        }

        static void CopySamples(string srcPath, string destPath, SamplePackage []files, string edition)
        {
            // first copy the files to the dest directory, rename if needed
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].package == edition || files[i].package == "both")
                {
                    string dest = files[i].dest.Length < 1 ? files[i].source : files[i].dest;
                    if (Directory.Exists(srcPath + "\\" + files[i].source))
                    {
                        CopyFolder(srcPath + "\\" + files[i].source, destPath + "\\" + dest);
                    }
                    else
                    {
                        CopyFile(srcPath + "\\" + files[i].source, destPath + "\\" + dest);
                    }
                }
            }
        }

        // Process all the js files for distribution
        static void ProcessJS()
        {
            // create the wijmo-open distribution
            DirectoryInfo openDir = _destDir.CreateSubdirectory("wijmo-open");
            CreateFolders(openDir);
            CopyJS(_rootDir.FullName + "\\wijmo-complete\\development-bundle\\wijmo", 
                    openDir.FullName + "\\development-bundle\\wijmo", _buildInfo.js, "open", _buildInfo.copyright.open);
            MinifyJS(openDir.FullName + "\\development-bundle\\wijmo", _buildInfo.copyright.open);
            CopyExtern(_rootDir.FullName + "\\wijmo-complete\\development-bundle\\external",  openDir.FullName + "\\development-bundle\\external", 
                "open", _buildInfo.externals);
            CombineJS(openDir.FullName, "open", _buildInfo.copyright.open, _buildInfo.externals);

            CopyCSS(_rootDir.FullName + "\\wijmo-complete\\development-bundle\\themes",
                        openDir.FullName + "\\development-bundle\\themes",
                        _buildInfo.themes, "open", _buildInfo.copyright.open);
            CombineCSS(openDir.FullName + "\\development-bundle\\themes",openDir.FullName + "\\css",
                           _buildInfo.themes, "open", _buildInfo.copyright.open);
            CopySamples(_rootDir.FullName + "\\wijmo-complete\\development-bundle\\samples",
                        openDir.FullName + "\\development-bundle\\samples",
                        _buildInfo.sample, "open");

            // create the wijmo-complete distribution
/*            openDir = _destDir.CreateSubdirectory("wijmo-complete");
            CreateFolders(openDir);
            CopyJS(_rootDir.FullName + "\\wijmo-complete\\development-bundle\\wijmo",
                    openDir.FullName + "\\development-bundle\\wijmo", _buildInfo.js, "complete", _buildInfo.copyright.complete);
            MinifyJS(openDir.FullName + "\\development-bundle\\wijmo", _buildInfo.copyright.complete);
            CopyExtern(_rootDir.FullName + "\\wijmo-complete\\development-bundle\\external", openDir.FullName + "\\development-bundle\\external",
                "complete", _buildInfo.externals);
            CombineJS(openDir.FullName, "complete", _buildInfo.copyright.complete, _buildInfo.externals);

            CopyCSS(_rootDir.FullName + "\\wijmo-complete\\development-bundle\\themes",
                        openDir.FullName + "\\development-bundle\\themes",
                        _buildInfo.themes, "complete", _buildInfo.copyright.complete);
            CombineCSS(openDir.FullName + "\\development-bundle\\themes", openDir.FullName + "\\css",
                           _buildInfo.themes, "complete", _buildInfo.copyright.complete);
 */
        }

        static void ProcessCSS()
        {
            // copy the css files

            // minify

            // combine
        }

        static void ProcessSamples()
        {
            // copy the samples, update the references
        }
        static bool isBinaryFile(string path)
        {
            FileInfo f = new FileInfo(path);
            string ext = f.Extension.ToLower();
            if (ext.Contains("gif") || ext.Contains("png") || ext.Contains("jpg") || ext.Contains("ico") || ext.Contains("bmp") ||
                ext.Contains("eot") || ext.Contains("svg") || ext.Contains("ttf") || ext.Contains("woff") || ext.Contains("tif"))
                return true;
            return false;
        }
        static void CopyFile(string src, string dst)
        {
            if (isBinaryFile(src))
            {
                File.Copy(src, dst);
                var f = new FileInfo(dst);
                f.Attributes = FileAttributes.Normal;
            }
            else
            {
                StreamReader input = new StreamReader(src);
                StreamWriter output = new StreamWriter(dst, false, _utf8NoBOM);
                output.Write(input.ReadToEnd());
                input.Close();
                output.Close();
            }
        }
        static void CopyFolder(string srcPath, string destPath)
        {
            Directory.CreateDirectory(destPath);

            var files = Directory.GetFiles(srcPath);
            for (int i = 0; i < files.Length; i++)
            {
                FileInfo f = new FileInfo(files[i]);
                CopyFile(files[i], destPath + "\\" + f.Name);
            }

            var dirs = Directory.GetDirectories(srcPath);

            for (int i = 0; i < dirs.Length; i++)
            {
                CopyFolder(dirs[i], dirs[i].ToLower().Replace(srcPath.ToLower(), destPath));
            }
        }
        static void UpdateCopyright(string copyright, string srcpath, string destpath)
        {
            if (File.Exists(srcpath))
            {
                StreamReader input = new StreamReader(srcpath);
                StreamWriter output = new StreamWriter(destpath, false, _utf8NoBOM);

                string line = input.ReadLine();
                string line2 = input.ReadToEnd();
                if (line2.Contains("Copyright(c)"))
                {
                    // update the version # before we write it
                    string pat = "Wijmo Library";
                    int begin = line2.IndexOf(pat);
                    pat = line2.Substring(begin, pat.Length + 6);
                    line2 = line2.Replace(pat, "Wijmo Library " + _buildInfo.version.wijmo);
                    output.WriteLine(line);
                    output.Write(line2);
                }
                else
                {
                    output.Write(copyright);
                    // minified files only have 1 line
                    if (line2.Length == 0)
                    {
                        output.WriteLine("*/");
                        output.Write(line);
                    }
                    else
                    {
                        output.Write(line2);
                    }
                }
                input.Close();
                output.Close();
            }
            else
            {
                Console.WriteLine("Can't copyright, input file not found: {0}", srcpath);
            }

        }

    }
}
