﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Text.RegularExpressions;
using C1.C1Zip;

namespace Builder
{
    class Program
    {
        static string _version = "";
        static string _jqVersion = "";
        static string _jqUiVersion = "";
        static string _rootPath = "";
        static string _destPath = "";
        static XmlDocument _xml = new XmlDocument();
        static UTF8Encoding _utf8NoBOM = new UTF8Encoding(false);

        static void Main(string[] args)
        {
            // main root path for the js widgets (i.e. $rhino/main/widgets)
            _rootPath = args[0];

            _xml.Load(_rootPath + @"\documentation\deployment\wijmo.xml");
            _version = GetNodeValue("wijmo/version");
            _jqUiVersion = GetNodeValue("wijmo/jq-ui-version");
            _jqVersion = GetNodeValue("wijmo/jq-version");

            _destPath = _rootPath + @"\wijmo." + _version;

            CleanDest();

            ProcessWijmo("wijmo/open");
            ProcessWijmo("wijmo/complete");

            CopyFile(_rootPath + @"\index.html", _destPath + @"\index.html");
            CopyFolder(_rootPath + @"\explore", _destPath + @"\explore");
            CopyFile(_rootPath + @"\explore.html", _destPath + @"\explore.html");
            UpdateSampleScriptReferences(_destPath + @"\explore");

            CreateZipFiles();
        }

        static void ProcessWijmo(string xpath)
        {
            // wijmo-open/wijmo-complete
            string edition = GetNodeValue(xpath + "/path");

            // get copyright and upate the version in it
            string copyright = GetNodeValue(xpath + "/copyright");
            copyright = copyright.Replace("@VERSION@", _version);
            File.WriteAllText(_destPath + @"\" + edition + @"\version.txt", _version);

            #region ***js
            // get the all the js files to process
            XmlNodeList nlist = _xml.SelectNodes(xpath+"/js/file");
            string inpath = _rootPath + @"\" + edition + @"\development-bundle\wijmo";
            string outpath = _destPath + @"\" + edition + @"\development-bundle\wijmo";

            // get the list of external js files
            // these get combined in .all.min.js to reduce the number of script references
            XmlNodeList externList = _xml.SelectNodes(xpath + "/external/file");
            StreamWriter streamCombinedMinJS = new StreamWriter(_destPath + @"\" + edition + @"\js\jquery." + edition.ToLower() + ".all." + _version + ".min.js", false, _utf8NoBOM);
            StreamWriter streamCombinedJS = new StreamWriter(_destPath + @"\" + edition + @"\js\jquery." + edition.ToLower() + ".all." + _version + ".js", false, _utf8NoBOM);
            streamCombinedMinJS.Write(copyright);
            streamCombinedMinJS.WriteLine("*/");
            foreach (XmlNode node in externList)
            {
                if (node.Attributes["directory"] == null)
                {
                    StreamReader reader = new StreamReader(VersionRef(_rootPath + @"\" + edition + @"\development-bundle\external\" + node.InnerText));
                    if (node.Attributes["term"] != null)
                    {
                        string s = reader.ReadToEnd();
                        streamCombinedMinJS.Write(s);
                        streamCombinedMinJS.WriteLine(";");
                        streamCombinedJS.Write(s);
                        streamCombinedJS.WriteLine(";");
                    }
                    else if (node.Attributes["combine"] != null)
                    {
                        string s = reader.ReadToEnd();
                        streamCombinedMinJS.WriteLine(s);
                        streamCombinedMinJS.WriteLine(s);
                    }
                    reader.Close();
                    CopyFile(VersionRef(_rootPath + @"\" + edition + @"\development-bundle\external\" + node.InnerText), 
                             VersionRef(_destPath + @"\" + edition + @"\development-bundle\external\" + node.InnerText));
                }
                else
                {
                    CopyFolder(_rootPath + @"\" + edition + @"\development-bundle\external\" + node.InnerText, _destPath + @"\" + edition + @"\development-bundle\external\" + node.InnerText);
                }
            }
            // copy the .js to the destination folder with the updated copyright
            foreach (XmlNode node in nlist)
            {
                UpdateCopyright(copyright, inpath + @"\" + node.InnerText, outpath + @"\" + node.InnerText);
                StreamWriter combiner = null;
                if (node.Attributes["combine"] != null)
                {
                    combiner = streamCombinedMinJS;
                    streamCombinedJS.Write(File.ReadAllText(outpath + @"\" + node.InnerText));

                }
//                StreamWriter combiner = node.Attributes["combine"] != null ? streamCombinedMinJS : null;
                MinifyJS(copyright, outpath + @"\" + node.InnerText, outpath + @"\minified\" + node.InnerText.Replace(".js",".min.js"), combiner);
            }
            streamCombinedMinJS.Close();
            streamCombinedJS.Close();
            #endregion

            #region ***css
            // copy the theme folder
            CopyFolder(_rootPath + @"\" + edition + @"\development-bundle\themes", _destPath + @"\" + edition + @"\development-bundle\themes");

            // now combine the css for easier script reference
            // this is the minified version that *includes* wijmo-open css
            StreamWriter combinedAllMinCSS = new StreamWriter(_destPath + @"\" + edition + @"\css\jquery." + edition.ToLower() + ".all." + _version + ".min.css", false, _utf8NoBOM);

            // this is the combined wijmo-complete
            StreamWriter combinedCSS = new StreamWriter(_destPath + @"\" + edition + @"\css\jquery." + edition.ToLower() + "." + _version + ".css", false, _utf8NoBOM);
            combinedCSS.Write(copyright); combinedCSS.WriteLine("*/");
            combinedAllMinCSS.Write(copyright); combinedAllMinCSS.WriteLine("*/");

            // now get the open css
            nlist = _xml.SelectNodes(xpath + "/css/combine");
            foreach (XmlNode node in nlist)
            {
//                string s = node.InnerText.Replace("@@version@@", _version);
                string s = VersionRef(node.InnerText);
                s = File.ReadAllText(_destPath + @"\" + s);

                s = Regex.Replace(s, @"/\*(.*?)\*/", me =>
                {
                    if (me.Value.StartsWith("/*")) return "";
                    return me.Value;
                }, RegexOptions.Singleline);
                combinedAllMinCSS.Write(s);
            }

            // process individual css files
            nlist = _xml.SelectNodes(xpath + "/css/file");
            foreach (XmlNode node in nlist)
            {
                StreamReader reader = new StreamReader(VersionRef(_rootPath + @"\" + edition + @"\development-bundle\themes\wijmo\" + node.InnerText));
                combinedCSS.WriteLine(reader.ReadToEnd());
                reader.Close();

                MinifyCSS(_rootPath + @"\" + edition + @"\development-bundle\themes\wijmo\" + node.InnerText, combinedAllMinCSS);
            }

            combinedCSS.Close();
            combinedAllMinCSS.Close();

            // 
            nlist = _xml.SelectNodes(xpath + "/css/images");
            foreach (XmlNode node in nlist)
            {
                CopyFolder(_rootPath + @"\" + edition + @"\development-bundle\" + node.InnerText, _destPath + @"\" + edition + @"\" + node.Attributes["destination"].Value);
            }
            #endregion

            #region *** samples
            nlist = _xml.SelectNodes(xpath + "/samples/file");
            foreach (XmlNode node in nlist)
            {
                CopyFolder(_rootPath + @"\" + edition + @"\development-bundle\" + node.InnerText, _destPath + @"\" + edition + @"\development-bundle\" + node.InnerText);
                UpdateSampleScriptReferences(_destPath + @"\" + edition + @"\development-bundle\" + node.InnerText);
            }
            #endregion
            #region *** documentation
            nlist = _xml.SelectNodes(xpath + "/documentation/file");
            foreach (XmlNode node in nlist)
            {
                CopyFile(_rootPath + @"\" + edition + @"\" + node.InnerText, _destPath + @"\" + edition + @"\" + node.InnerText);
            }
            #endregion

            #region *** additional
            // handle additional file operations here
            nlist = _xml.SelectNodes(xpath + "/additional/file");
            foreach (XmlNode node in nlist)
            {
                string dest = node.Attributes["destination"].Value;
                string src = VersionRef(node.InnerText); //.Replace("@@version@@", _version);
                FileInfo finfo = new FileInfo(_destPath + @"\" + src);
                CopyFile(_destPath + @"\" + src, _destPath + @"\" + edition + @"\" + dest + @"\" + finfo.Name);
            }
            #endregion
        }

        static void UpdateScriptReference(string file)
        {
            StreamReader input = new StreamReader(file);
            var content = input.ReadToEnd();
            input.Close();

            content = Regex.Replace(content, @"jquery.wijmo-open\.\d{1,3}\.\d{1,3}\.\d{1,3}\.css", "jquery.wijmo-open." + _version + ".css");
            content = Regex.Replace(content, @"jquery.wijmo-open\.\d{1,3}\.\d{1,3}\.\d{1,3}\.min.js", "jquery.wijmo-open.all." + _version + ".min.js");
            content = Regex.Replace(content, @"jquery.wijmo-complete\.\d{1,3}\.\d{1,3}\.\d{1,3}\.css", "jquery.wijmo-complete.all." + _version + ".min.css");
            content = Regex.Replace(content, @"jquery.wijmo-complete\.\d{1,3}\.\d{1,3}\.\d{1,3}\.min.js", "jquery.wijmo-complete.all." + _version + ".min.js");
            content = Regex.Replace(content, @"jquery.wijmo-complete\.\d{1,3}\.\d{1,3}\.\d{1,3}\.min.js", "jquery.wijmo-complete.all." + _version + ".min.js");
            content = Regex.Replace(content, @"jquery-\d{1,3}\.\d{1,3}\.\d{1,3}\.min.js", "jquery-" + _jqVersion + ".min.js");
            content = Regex.Replace(content, @"jquery-ui-\d{1,3}\.\d{1,3}\.\d{1,3}\.custom.min.js", "jquery-ui-" + _jqUiVersion + ".custom.min.js");

            StreamWriter writer = new StreamWriter(file, false, _utf8NoBOM);
            writer.Write(content);
            writer.Close();
        }

        static void UpdateSampleScriptReferences(string dir)
        {
            var files = Directory.GetFiles(dir, "*.html", SearchOption.AllDirectories);

            for (int i = 0; i < files.Length; i++)
            {
                UpdateScriptReference(files[i]);
            }
        }

        static string VersionRef(string s)
        {
            s = s.Replace("@@version@@", _version);
            s = s.Replace("@@jq-version@@", _jqVersion);
            s = s.Replace("@@jq-ui-version@@", _jqUiVersion);

            return s;
        }
        static void MinifyJS(string copyright, string srcpath, string destpath, StreamWriter combineMin)
        {
            if (!File.Exists(srcpath))
            {
                throw new Exception("Minify: File doesn't not exist: " + srcpath);
            }
            string temppath = Path.GetTempPath() + @"\derf.min";
            if (File.Exists(temppath)) File.Delete(temppath);

            string minifiedTool = _rootPath + "\\tools\\AjaxMin.exe";
            ProcessStartInfo psi = new ProcessStartInfo(minifiedTool, "-JS -term -clobber:true " + srcpath + " -out " + temppath);

            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;

            Process minify = Process.Start(psi);
            minify.WaitForExit();

            if (combineMin != null)
            {
                StreamReader reader = new StreamReader(temppath);
                combineMin.WriteLine(reader.ReadToEnd());
                reader.Close();
            }
            UpdateCopyright(copyright, temppath, destpath);
        }

        static void MinifyCSS(string srcpath, StreamWriter combine)
        {
            if (!File.Exists(srcpath))
            {
                throw new Exception("Minify: File doesn't not exist: " + srcpath);
            }

            string temppath = Path.GetTempPath() + @"\derf.min";
            if (File.Exists(temppath)) File.Delete(temppath);

            string minifiedTool = _rootPath + "\\tools\\AjaxMin.exe";

            ProcessStartInfo psi = new ProcessStartInfo(minifiedTool, "-CSS -term -clobber:true " + srcpath + " -out " + temppath);

            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;

            Process minify = Process.Start(psi);

            minify.WaitForExit();

            combine.Write(File.ReadAllText(temppath));
        }
        static void UpdateCopyright(string copyright, string srcpath, string destpath)
        {
            if (File.Exists(srcpath))
            {
                StreamReader input = new StreamReader(srcpath);
                StreamWriter output = new StreamWriter(destpath, false, _utf8NoBOM);

                string line = input.ReadLine();
                string line2 = input.ReadToEnd();
                if (line2.Contains("Copyright(c)"))
                {
                    // update the version # before we write it
                    string pat = "Wijmo Library";
                    int begin = line2.IndexOf(pat);
                    pat = line2.Substring(begin, pat.Length + 6);
                    line2 = line2.Replace(pat, "Wijmo Library " + _version);
                    output.WriteLine(line);
                    output.Write(line2);
                }
                else
                {
                    output.Write(copyright);
                    // minified files only have 1 line
                    if (line2.Length == 0)
                    {
                        output.WriteLine("*/");
                        output.Write(line);
                    }
                    else
                    {
                        output.Write(line2);
                    }
                }
                input.Close();
                output.Close();
            }
            else
            {
                Console.WriteLine("Can't copyright, input file not found: {0}", srcpath);
            }

        }

        static string GetNodeValue(string xpath)
        {
            XmlNode nlist = _xml.SelectSingleNode(xpath);

            return nlist.InnerText;
        }

        static bool isBinaryFile(string path)
        {
            FileInfo f = new FileInfo(path);
            string ext = f.Extension.ToLower();
            if( ext.Contains("gif") || ext.Contains("png") || ext.Contains("jpg") || ext.Contains("ico") || ext.Contains("bmp") ||
                ext.Contains("eot") || ext.Contains("svg") || ext.Contains("ttf") || ext.Contains("woff") || ext.Contains("tif") ) 
                return true;
            return false;
        }
        static void CopyFile(string src, string dst)
        {
            if (isBinaryFile(src))
            {
                File.Copy(src, dst);
                var f = new FileInfo(dst);
                f.Attributes = FileAttributes.Normal;
            }
            else
            {
                StreamReader input = new StreamReader(src);
                StreamWriter output = new StreamWriter(dst, false, _utf8NoBOM);
                output.Write(input.ReadToEnd());
                input.Close();
                output.Close();
            }
        }
        static void CopyFolder(string srcPath, string destPath)
        {
            Directory.CreateDirectory(destPath);

            var files = Directory.GetFiles(srcPath);
            for (int i = 0; i < files.Length; i++)
            {
                FileInfo f = new FileInfo(files[i]);
                CopyFile(files[i], destPath + "\\" + f.Name);
            }

            var dirs = Directory.GetDirectories(srcPath);

            for (int i = 0; i < dirs.Length; i++)
            {
                CopyFolder(dirs[i], dirs[i].ToLower().Replace(srcPath.ToLower(), destPath));
            }
        }

        // cleans the destination directory and creates the directory structure for distribution
        static void CleanDest()
        {
            if (Directory.Exists(_destPath))
            {
                Directory.Delete(_destPath, true);
            }
            Directory.CreateDirectory(_destPath);

            Directory.CreateDirectory(_destPath + @"\Wijmo-Open\css");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Open\js");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Open\development-bundle\themes");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Open\development-bundle\samples");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Open\development-bundle\external");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Open\development-bundle\wijmo\minified");

            Directory.CreateDirectory(_destPath + @"\Wijmo-Complete\css");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Complete\js");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Complete\development-bundle\themes");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Complete\development-bundle\samples");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Complete\development-bundle\external");
            Directory.CreateDirectory(_destPath + @"\Wijmo-Complete\development-bundle\wijmo\minified");
        }

        static void CreateZipFiles()
        {
            var zipOpen = new C1.C1Zip.C1ZipFile();
            zipOpen.Create(_rootPath + "\\Wijmo-Open." + _version + ".zip");

            var zipComplete = new C1.C1Zip.C1ZipFile();
            zipComplete.Create(_rootPath + "\\Wijmo-Complete." + _version + ".zip");

            AddFilesAndFolders(zipOpen, _destPath + "\\wijmo-open", 1);

            AddFilesAndFolders(zipComplete, _destPath, 1);
        }

        static void AddFilesAndFolders(C1ZipFile zip, string path, int lvl)
        {
            var files = Directory.GetFiles(path);
            for (int i = 0; i < files.Length; i++)
            {
                zip.Entries.Add(files[i], lvl);
            }

            var dirs = Directory.GetDirectories(path);
            for (int i = 0; i < dirs.Length; i++)
            {
                AddFilesAndFolders(zip, dirs[i], lvl + 1);
            }

        }
    }
}
