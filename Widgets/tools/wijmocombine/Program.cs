﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Text.RegularExpressions;
using C1.C1Zip;

namespace wijmoCombine
{
    class Program
    {
        static string _version = "0.0.0";
        static string _copyrightComplete = "";
        static string _copyrightOpen = "";
        static string _sourceRootPath = "";
        static string _destinationPath = "";
        static string _namespace = "Controls";  // used to identify the namespace for embedded images for resource url
        static string[] _wijmoComplete = null;
        static string[] _wijmoOpen = null;
        static string[] _wijmoCompleteExternal = null;
        static string[] _wijmoOpenExternal = null;
        static string[] _wijmoCompleteCSS = null;
        static string[] _wijmoOpenCSS = null;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args">args[1] is the path to the widgets folder.</param>
        static void Main(string[] args)
        {
            _namespace = args[0];
            _sourceRootPath = args[1];

            // get the version
            _version = GetVersion();

            _destinationPath = args[2];

            // get the copyright template
            GetCopyRight();

            // update the version in the copyright template
            _copyrightComplete = _copyrightComplete.Replace("@VERSION@", _version);
            _copyrightOpen = _copyrightOpen.Replace("@VERSION@", _version);

            CreateDeployment();

            Clean();
        }

        /// <summary>
        /// Returns the version number given a folder
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static string GetVersion()
        {
            string retval = _version;

            StreamReader reader = new StreamReader(_sourceRootPath + @"\documentation\deployment\version.txt");
            retval = reader.ReadLine().Trim();
            reader.Close();
            return retval;
        }

        /// <summary>
        /// Returns a copyright template used as a header for WijMo
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static void GetCopyRight()
        {
            StreamReader reader = new StreamReader(_sourceRootPath + @"\documentation\deployment\copyright_complete.txt");
            _copyrightComplete = reader.ReadToEnd();
            reader.Close();

            reader = new StreamReader(_sourceRootPath + @"\documentation\deployment\copyright_open.txt");
            _copyrightOpen = reader.ReadToEnd();
            reader.Close();
        }

        static string[] GetDeploymentList(string path)
        {
            StreamReader reader = new StreamReader(path);
            string s = reader.ReadToEnd();
            s = s.Replace("\r", "");
            reader.Close();

            return s.Split('\n');
        }
        /// <summary>
        /// Creates the distribution folder structure by copying relevant files, minifies, and
        /// combines the WijMo sources, ready for zipping and deploying.
        /// </summary>
        /// <param name="path">The root path when the WijMo sources live.  The deployment folder will be created as a child of this folder.</param>
        static void CreateDeployment()
        {
            // open package the text.  this file contains a list of files that goes into the
            // wijmo complete distribution.
            _wijmoComplete = GetDeploymentList(_sourceRootPath + @"\documentation\deployment\wijmocomplete.lst");
            _wijmoOpen = GetDeploymentList(_sourceRootPath + @"\documentation\deployment\wijmoopen.lst");
            _wijmoCompleteExternal = GetDeploymentList(_sourceRootPath + @"\documentation\deployment\wijmocompleteexternal.lst");
            _wijmoOpenExternal = GetDeploymentList(_sourceRootPath + @"\documentation\deployment\wijmoopenexternal.lst");
            _wijmoCompleteCSS = GetDeploymentList(_sourceRootPath + @"\documentation\deployment\wijmocompletecss.lst");
            _wijmoOpenCSS = GetDeploymentList(_sourceRootPath + @"\documentation\deployment\wijmoopencss.lst");

            string f = string.Empty;

            if (JSisNewer())
            {
                MinifyJS();
            }

            if (CssIsNewer())
            {
                CombineCSS();
            }

        }

        static bool CssIsNewer()
        {
            // check the css
            if (File.Exists(_destinationPath + @"\jquery.wijmo-complete." + _namespace + ".css"))
            {
                FileInfo complete = new FileInfo(_destinationPath + @"\jquery.wijmo-complete." + _namespace + ".css");

                for (int i = 0; i < _wijmoCompleteCSS.Length; i++)
                {
                    string f = _wijmoCompleteCSS[i];
                    if (f == string.Empty) continue;

                    FileInfo src = new FileInfo(_sourceRootPath + @"\Wijmo-Complete\development-bundle\themes\wijmo\" + f);
                    if (complete.LastWriteTime < src.LastWriteTime)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }

            if (File.Exists(_destinationPath + @"\jquery.wijmo-open." + _namespace + ".css"))
            {
                FileInfo complete = new FileInfo(_destinationPath + @"\jquery.wijmo-open." + _namespace + ".css");

                for (int i = 0; i < _wijmoCompleteCSS.Length; i++)
                {
                    string f = _wijmoCompleteCSS[i];
                    if (f == string.Empty) continue;

                    FileInfo src = new FileInfo(_sourceRootPath + @"\Wijmo-open\development-bundle\themes\wijmo\" + f);
                    if (complete.LastWriteTime < src.LastWriteTime)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        // returns true we need to create the joined .js files for wijmo
        static bool JSisNewer()
        {
            if(File.Exists(_destinationPath+@"\jquery.wijmo-complete.js"))
            {
                FileInfo complete = new FileInfo(_destinationPath + @"\jquery.wijmo-complete.js");

                for (int i = 0; i < _wijmoComplete.Length; i++)
                {
                    string f = _wijmoComplete[i];
                    if (f == string.Empty) continue;

                    FileInfo src = new FileInfo(_sourceRootPath + @"\Wijmo-Complete\development-bundle\wijmo\" + f);
                    if (complete.LastWriteTime < src.LastWriteTime)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }

            if (File.Exists(_destinationPath + @"\jquery.wijmo-open.js"))
            {
                FileInfo open = new FileInfo(_destinationPath + @"\jquery.wijmo-open.js");

                for (int i = 0; i < _wijmoOpen.Length; i++)
                {
                    string f = _wijmoOpen[i];
                    if (f == string.Empty) continue;

                    FileInfo src = new FileInfo(_sourceRootPath + @"\Wijmo-Open\development-bundle\wijmo\" + f);
                    if (open.LastWriteTime < src.LastWriteTime)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }


            return false;
        }


        static void Clean()
        {
            if (Directory.Exists(_destinationPath + "\\wijmo-complete"))
            {
                Directory.Delete(_destinationPath + "\\wijmo-complete", true);
            }
            if (Directory.Exists(_destinationPath + "\\wijmo-open"))
            {
                Directory.Delete(_destinationPath + "\\wijmo-open", true);
            }
        }

        static void MinifyJS()
        {

            string destPath = _destinationPath + @"\Wijmo-Complete";
            Directory.CreateDirectory(destPath);

            string f = string.Empty;

            string minifiedTool = _destinationPath + "\\AjaxMin.exe";

            // minify the complete
            for (int i = 0; i < _wijmoComplete.Length; i++)
            {
                f = _wijmoComplete[i];
                if (f == string.Empty) continue;

                string sourcefile = _sourceRootPath + @"\Wijmo-Complete\development-bundle\wijmo\" + f;
                string destFile = destPath + "\\" + f;
                destFile = destFile.Replace(".js", ".min.js");

                ProcessStartInfo psi = new ProcessStartInfo(minifiedTool, "-JS -term -clobber:true " + sourcefile + " -out " + destFile);

                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;

                Process minify = Process.Start(psi);
                minify.WaitForExit();
            }

            // minify the open

            destPath = _destinationPath + @"\Wijmo-Open";
            Directory.CreateDirectory(destPath);

            for (int i = 0; i < _wijmoOpen.Length; i++)
            {
                f = _wijmoOpen[i];
                if (f == string.Empty) continue;

                string sourcefile = _sourceRootPath + @"\Wijmo-Open\development-bundle\wijmo\" + f;
                string destFile = destPath + "\\" + f;
                destFile = destFile.Replace(".js", ".min.js");

                ProcessStartInfo psi = new ProcessStartInfo(minifiedTool, "-JS -term -clobber:true " + sourcefile + " -out " + destFile);

                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;

                Process minify = Process.Start(psi);
                minify.WaitForExit();
            }

            // combine the open mindified files as we need these for wijmo-open
            StreamWriter combined = new StreamWriter(_destinationPath + @"\jquery.wijmo-open.min.js");
            combined.Write(_copyrightOpen);
            combined.WriteLine("*/");
            // get the external .js
            for (int i = 0; i < _wijmoOpenExternal.Length; i++)
            {
                f = _wijmoOpenExternal[i];
                if (f == string.Empty) continue;
                var split = f.Split(' ');
                f = split[0];
                string inputfile = _sourceRootPath + @"\wijmo-open\development-bundle\external\" + f;

                StreamReader reader = new StreamReader(inputfile);
                if (split.Length > 1)
                {
                    combined.Write(reader.ReadToEnd());
                    combined.WriteLine(";");
                }
                else
                {
                    combined.WriteLine(reader.ReadToEnd());
                }
                reader.Close();
            }

            // now combine all the minified open files and put them in the complete/external
            for (int i = 0; i < _wijmoOpen.Length; i++)
            {
                f = _wijmoOpen[i];
                if (f == string.Empty) continue;

                string inputfile = _destinationPath + @"\wijmo-open\" + f;
                inputfile = inputfile.Replace(".js", ".min.js");

                StreamReader reader = new StreamReader(inputfile);
                combined.WriteLine(reader.ReadToEnd());
                reader.Close();
            }
            combined.Close();

            // combine the complete minified
            combined = new StreamWriter(_destinationPath + @"\jquery.wijmo-complete.min.js");
            combined.Write(_copyrightComplete);
            combined.WriteLine("*/");
            // get the external .js
            for (int i = 0; i < _wijmoCompleteExternal.Length; i++)
            {
                f = _wijmoCompleteExternal[i];
                if (f == string.Empty) continue;
                var split = f.Split(' ');
                f = split[0];
                string inputfile = _sourceRootPath + @"\wijmo-complete\development-bundle\external\" + f;

                StreamReader reader = new StreamReader(inputfile);
                if (split.Length > 1)
                {
                    combined.Write(reader.ReadToEnd());
                    combined.WriteLine(";");
                }
                else
                {
                    combined.WriteLine(reader.ReadToEnd());
                }
                reader.Close();
            }
            // now combine all the minified open files and put them in the complete/external
            for (int i = 0; i < _wijmoComplete.Length; i++)
            {
                f = _wijmoComplete[i];
                if (f == string.Empty) continue;

                string inputfile = _destinationPath + @"\wijmo-complete\" + f;
                inputfile = inputfile.Replace(".js", ".min.js");

                StreamReader reader = new StreamReader(inputfile);
                combined.WriteLine(reader.ReadToEnd());
                reader.Close();
            }
            combined.Close();


            // ******
            // for debugging - combine the open non-mindified files as we need these for wijmo-open
            combined = new StreamWriter(_destinationPath + @"\jquery.wijmo-open.js");
            for (int i = 0; i < _wijmoOpenExternal.Length; i++)
            {
                f = _wijmoOpenExternal[i];
                if (f == string.Empty) continue;
                var split = f.Split(' ');
                f = split[0];
                string inputfile = _sourceRootPath + @"\wijmo-open\development-bundle\external\" + f;

                StreamReader reader = new StreamReader(inputfile);
                if (split.Length > 1)
                {
                    combined.Write(reader.ReadToEnd());
                    combined.WriteLine(";");
                }
                else
                {
                    combined.WriteLine(reader.ReadToEnd());
                }
                reader.Close();
            }
            // now combine all the minified open files and put them in the complete/external
            for (int i = 0; i < _wijmoOpen.Length; i++)
            {
                f = _wijmoOpen[i];
                if (f == string.Empty) continue;

                string inputfile = _sourceRootPath + @"\Wijmo-Open\development-bundle\wijmo\" + f;

                StreamReader reader = new StreamReader(inputfile);
                combined.Write(reader.ReadToEnd());
                reader.Close();
            }
            combined.Close();

            // combine all the complete non-minified for debuggin purposes
            combined = new StreamWriter(_destinationPath + @"\jquery.wijmo-complete.js");
            for (int i = 0; i < _wijmoCompleteExternal.Length; i++)
            {
                f = _wijmoCompleteExternal[i];
                if (f == string.Empty) continue;
                var split = f.Split(' ');
                f = split[0];
                string inputfile = _sourceRootPath + @"\wijmo-complete\development-bundle\external\" + f;

                StreamReader reader = new StreamReader(inputfile);
                if (split.Length > 1)
                {
                    combined.Write(reader.ReadToEnd());
                    combined.WriteLine(";");
                }
                else
                {
                    combined.WriteLine(reader.ReadToEnd());
                }
                reader.Close();
            }
            // now combine all the minified open files and put them in the complete/external
            for (int i = 0; i < _wijmoComplete.Length; i++)
            {
                f = _wijmoComplete[i];
                if (f == string.Empty) continue;

                string inputfile = _sourceRootPath + @"\wijmo-complete\development-bundle\wijmo\" + f;

                StreamReader reader = new StreamReader(inputfile);
                combined.WriteLine(reader.ReadToEnd());
                reader.Close();
            }
            combined.Close();
        }

        static bool isInList(string name, string[] array)
        {
            var results = Array.FindAll(array, s => s.Equals(name));

            return results.Length > 0;
        }

        static void CleanOldWijmo()
        {
            CleanOldWijmo(_destinationPath + @"\Wijmo-Complete\development-bundle\external");
            CleanOldWijmo(_destinationPath + @"\Wijmo-Open\development-bundle\external");
        }
        static void CleanOldWijmo(string path)
        {
            var files = Directory.GetFiles(path, "*.js");

            for (int i = 0; i < files.Length; i++)
            {
                FileInfo f = new FileInfo(files[i]);
                if(f.Name.ToLower().StartsWith("jquery.wijmo-open."))
                {
                    f.Delete();
                }
            }

        }

        static string ReplaceImageUrl(string buf)
        {
            StringBuilder updatedCss = new StringBuilder();

            int idx = buf.IndexOf("url", 0, StringComparison.CurrentCultureIgnoreCase);

            if (idx >= 0)
            {
                int start = 0;
                while (idx >= 0)
                {
                    string s1 = buf.Substring(start, idx - start);
                    updatedCss.Append(buf.Substring(start, idx - start));
                    // get the image name
                    int nameStart = buf.IndexOf('(', idx);
                    int nameEnd = buf.IndexOf(')', idx);
                    string imageName = buf.Substring(nameStart + 1, nameEnd - nameStart - 1).Trim();
                    imageName = imageName.Replace("'", "");
                    imageName = imageName.Replace("\"", "");
                    imageName = imageName.Replace('/', '.');
                    imageName = imageName.Replace('\\', '.');

                    string res = string.Format("url('<%= WebResource(\"C1.Web.Wijmo." + _namespace + ".Resources.themes.wijmo.{0}\")%>')",
                                imageName);
                    updatedCss.Append(res);
                    start = nameEnd + 1;
                    string s2 = buf.Substring(start);
                    idx = buf.IndexOf("url", start);
                    if (idx < 0)
                        updatedCss.Append(buf.Substring(start));
                }
            }
            else
            {
                updatedCss.Append(buf);
            }

            return updatedCss.ToString();
        }

        static void CombineCSS()
        {
            // now copy the css files
            StreamWriter opencss = new StreamWriter(_destinationPath + @"\jquery.wijmo-open."+ _namespace +".css");
            string path = _sourceRootPath + @"\wijmo-open\development-bundle\themes\wijmo";

            foreach (string file in _wijmoOpenCSS)
            {
                if (file.Length > 0)
                {
                    StreamReader reader = new StreamReader(path + "\\" + file);
                    opencss.Write(ReplaceImageUrl(reader.ReadToEnd()));
                    reader.Close();
                }
            }

            opencss.Close();


            // now copy the css files
            opencss = new StreamWriter(_destinationPath + @"\jquery.wijmo-complete."+ _namespace +".css");
            path = _sourceRootPath + @"\wijmo-complete\development-bundle\themes\wijmo";

            foreach (string file in _wijmoCompleteCSS)
            {
                if (file.Length > 0)
                {
                    StreamReader reader = new StreamReader(path + "\\" + file);
                    opencss.Write(ReplaceImageUrl(reader.ReadToEnd()));
                    reader.Close();
                }
            }

            opencss.Close();
        }

        static void MinifyCSS()
        {
            string root = _sourceRootPath;
            string deploy = _destinationPath;

            Directory.CreateDirectory(_destinationPath + "\\css");
            // copy the css
            CopyFolder(_destinationPath + @"\development-bundle\themes\wijmo", _destinationPath + "\\css");

            var output = File.CreateText(_destinationPath + @"\css\jquery.ui.wijmo."+_version+".css");
            foreach (var f in _wijmoCompleteCSS)
            {
                string path = _destinationPath + @"\css\" + f;
                if (File.Exists(path))
                {
                    var input = File.OpenText(path);
                    output.Write(input.ReadToEnd());
                    input.Close();
                    File.Delete(path);
                }
            }
            output.Close();


            /*
            // now actually minify the files
            string minifiedTool = rootPath + "\\tools\\AjaxMin.exe";

            foreach (var f in _wijmoComplete)
            {
                string cssName = f.Replace(".js", ".css");

                minifyFilename = minifyFilename.Replace("e\\wijmo\\", "e\\wijmo\\themes\\wijmominified\\");

                ProcessStartInfo psi = new ProcessStartInfo(minifiedTool,
                    @"-CSS -clobber:true " + f + " -out " + minifyFilename);

                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;

                Process minify = Process.Start(psi);
                minify.WaitForExit();
            }
             * */

        }

        static void CopyFolder(string srcPath, string destPath)
        {
            Directory.CreateDirectory(destPath);

            var files = Directory.GetFiles(srcPath);
            for (int i = 0; i < files.Length; i++)
            {
                FileInfo f = new FileInfo(files[i]);
                File.Copy(files[i], destPath + "\\" + f.Name, true);
                f = new FileInfo(destPath + "\\" + f.Name);
                f.Attributes = FileAttributes.Normal;
            }

            var dirs = Directory.GetDirectories(srcPath);

            for (int i = 0; i < dirs.Length; i++)
            {
                CopyFolder(dirs[i], dirs[i].ToLower().Replace(srcPath.ToLower(), destPath));
            }

        }
    }
}
