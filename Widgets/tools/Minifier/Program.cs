﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Text.RegularExpressions;
using C1.C1Zip;

namespace C1Minifier
{
    class Program
    {
        static string _version = "0.0.0";
        static string _copyrightComplete = "";
        static string _copyrightOpen = "";
        static string _rootPath = "";
        static string _deploymentRootPath = "";
        static string[] _wijmoComplete = null;
        static string[] _wijmoOpen = null;
        static string[] _wijmoCompleteCSS = null;
        static string[] _wijmoOpenCSS = null;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args">args[1] is the path to the widgets folder.</param>
        static void Main(string[] args)
        {
            _rootPath = args[0];

            // get the version
            _version = GetVersion();

            _deploymentRootPath = _rootPath + "\\Wijmo." + _version;

            // get the copyright template
            GetCopyRight();

            // update the version in the copyright template
            _copyrightComplete = _copyrightComplete.Replace("@VERSION@", _version);
            _copyrightOpen = _copyrightOpen.Replace("@VERSION@", _version);

            if (Directory.Exists(_deploymentRootPath))
            {
                Directory.Delete(_deploymentRootPath, true);
            }

            CreateDeployment();

            Clean();

            CreateZipFiles();
        }

        /// <summary>
        /// Returns the version number given a folder
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static string GetVersion()
        {
            string retval = _version;

            StreamReader reader = new StreamReader(_rootPath + @"\documentation\deployment\version.txt");
            retval = reader.ReadLine().Trim();
            reader.Close();

            return retval;
        }

        /// <summary>
        /// Returns a copyright template used as a header for WijMo
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static void GetCopyRight()
        {
            StreamReader reader = new StreamReader(_rootPath + @"\documentation\deployment\copyright_complete.txt");
            _copyrightComplete = reader.ReadToEnd();
            reader.Close();

            reader = new StreamReader(_rootPath + @"\documentation\deployment\copyright_open.txt");
            _copyrightOpen = reader.ReadToEnd();
            reader.Close();
        }

        static string[] GetDeploymentList(string path)
        {
            StreamReader reader = new StreamReader(path);
            string s = reader.ReadToEnd();
            s = s.Replace("\r", "");
            reader.Close();

            return s.Split('\n');
        }
        /// <summary>
        /// Creates the distribution folder structure by copying relevant files, minifies, and
        /// combines the WijMo sources, ready for zipping and deploying.
        /// </summary>
        /// <param name="path">The root path when the WijMo sources live.  The deployment folder will be created as a child of this folder.</param>
        static void CreateDeployment()
        {
            // open package the text.  this file contains a list of files that goes into the
            // wijmo complete distribution.
            _wijmoComplete = GetDeploymentList(_rootPath + @"\documentation\deployment\wijmocomplete.lst");
            _wijmoOpen = GetDeploymentList(_rootPath + @"\documentation\deployment\wijmoopen.lst");
            _wijmoCompleteCSS = GetDeploymentList(_rootPath + @"\documentation\deployment\wijmocompletecss.lst");
            _wijmoOpenCSS = GetDeploymentList(_rootPath + @"\documentation\deployment\wijmoopencss.lst");

            string destPath = _deploymentRootPath + @"\Wijmo-Complete\development-bundle\wijmo";
            Directory.CreateDirectory(destPath);

            string f = string.Empty;

            // process the wijmo complete files - the copyright is more restricitve
            for (int i = 0; i < _wijmoComplete.Length; i++)
            {
                f = _wijmoComplete[i];
                if (f == string.Empty) continue;

                string sourcefile = _rootPath + @"\wijmo-complete\development-bundle\wijmo\" + f;
                string destFile = destPath + "\\" + f;
                StreamReader input = new StreamReader(sourcefile);
                StreamWriter output = new StreamWriter(destFile);

                string line = input.ReadLine();
                string line2 = input.ReadToEnd();
                if (line2.Contains("Copyright(c)"))
                {
                    // update the version # before we write it
                    string pat = "Wijmo Library";
                    int begin = line2.IndexOf(pat);
                    pat = line2.Substring(begin, pat.Length + 6);
                    line2 = line2.Replace(pat,"Wijmo Library "+ _version);
                    output.WriteLine(line);
                    output.Write(line2);
                }
                else
                {
                    output.Write(_copyrightComplete);
                    output.Write(line2);
                }
                input.Close();
                output.Close();
            }


            // now process the wijmo open widgets - this has the least restrictive license available
            string openPath = _deploymentRootPath + @"\Wijmo-Open\development-bundle\wijmo";
            Directory.CreateDirectory(openPath);

            // first process the wijmo open
            for (int i = 0; i < _wijmoOpen.Length; i++)
            {
                f = _wijmoOpen[i];
                if (f == string.Empty) continue;

                string sourcefile = _rootPath + @"\wijmo-open\development-bundle\wijmo\" + f;
                string destFile = openPath + "\\" + f;
                StreamReader input = new StreamReader(sourcefile);
                StreamWriter output = new StreamWriter(destFile);

                string line = input.ReadLine();
                string line2 = input.ReadToEnd();
                if (line2.Contains("Copyright(c)"))
                {
                    // update the version # before we write it
                    string pat = "Wijmo Library";
                    int begin = line2.IndexOf(pat);
                    pat = line2.Substring(begin, pat.Length + 6);
                    line2 = line2.Replace(pat, "Wijmo Library " + _version);
                    output.WriteLine(line);
                    output.Write(line2);
                }
                else
                {
                    output.Write(_copyrightOpen);
                    output.Write(line2);
                }
                input.Close();
                output.Close();
            }

            // copy the samples
            CopyFolder(_rootPath + "\\wijmo-complete\\development-bundle\\samples", _deploymentRootPath + @"\Wijmo-Complete\development-bundle\samples");
            CopyFolder(_rootPath + "\\wijmo-open\\development-bundle\\samples", _deploymentRootPath + @"\Wijmo-Open\development-bundle\samples");

            // copy the external .js files
            CopyFolder(_rootPath + "\\wijmo-complete\\development-bundle\\external", _deploymentRootPath + @"\Wijmo-Complete\development-bundle\external");
            CopyFolder(_rootPath + "\\wijmo-open\\development-bundle\\external", _deploymentRootPath + @"\Wijmo-Open\development-bundle\external");

            // copy the css
            CopyFolder(_rootPath + "\\wijmo-complete\\development-bundle\\themes", _deploymentRootPath + @"\Wijmo-Complete\development-bundle\themes");
            CopyFolder(_rootPath + "\\wijmo-open\\development-bundle\\themes", _deploymentRootPath + @"\Wijmo-Open\development-bundle\themes");

            // clean up old version of wijmo
            CleanOldWijmo();

            // minify our .js 
            MinifyJS();

            CleanCSSFiles();

            File.Copy(_rootPath + "\\index.html", _deploymentRootPath + @"\index.html");
            File.SetAttributes(_deploymentRootPath + @"\index.html", FileAttributes.Normal);
            CopyFolder(_rootPath + "\\explore", _deploymentRootPath + @"\explore");

            File.Copy(_rootPath + "\\wijmo-complete\\COMMERCIAL-LICENSE.txt", _deploymentRootPath + "\\wijmo-complete\\COMMERCIAL-LICENSE.txt");
            File.Copy(_rootPath + "\\wijmo-complete\\GPL-LICENSE.txt", _deploymentRootPath + "\\wijmo-complete\\GPL-LICENSE.txt");
            File.Copy(_rootPath + @"\documentation\deployment\version.txt", _deploymentRootPath + "\\wijmo-complete\\version.txt");

            File.Copy(_rootPath + "\\wijmo-open\\MIT-LICENSE.txt", _deploymentRootPath + "\\wijmo-open\\MIT-LICENSE.txt");
            File.Copy(_rootPath + "\\wijmo-open\\GPL-LICENSE.txt", _deploymentRootPath + "\\wijmo-open\\GPL-LICENSE.txt");
            File.Copy(_rootPath + @"\documentation\deployment\version.txt", _deploymentRootPath + "\\wijmo-open\\version.txt");

            File.Copy(_rootPath + "\\wijmo-complete\\change.log", _deploymentRootPath + "\\wijmo-complete\\change.log");
            File.Copy(_rootPath + "\\wijmo-open\\change.log", _deploymentRootPath + "\\wijmo-open\\change.log");

            UpdateDirectoryWijmoRef(_deploymentRootPath + @"\Wijmo-Complete\development-bundle\samples");
            UpdateDirectoryWijmoRef(_deploymentRootPath + @"\Wijmo-open\development-bundle\samples");
            UpdateWijmoRef(_deploymentRootPath + @"\index.html");
        }

        static void Clean()
        {
            if (File.Exists(_rootPath + "\\Wijmo-Open." + _version + ".zip"))
            {
                File.Delete(_rootPath + "\\Wijmo-Open." + _version + ".zip");
            }
            if (File.Exists(_rootPath + "\\Wijmo-Complete." + _version + ".zip"))
            {
                File.Delete(_rootPath + "\\Wijmo-Complete." + _version + ".zip");
            }

            // remove readonly attribute
            var files = Directory.GetFiles(_deploymentRootPath, "*.*", SearchOption.AllDirectories);

            for (int i = 0; i < files.Length; i++)
            {
                File.SetAttributes(files[i], FileAttributes.Normal);
            }

            // remove unneeded stuff
            if (Directory.Exists(@"\Wijmo-Complete\development-bundle\themes\wijmo\images\wijeditor\"))
            {
                Directory.Delete(_deploymentRootPath + @"\Wijmo-Complete\development-bundle\themes\wijmo\images\wijeditor\", true);
            }

        }

        static void CreateZipFiles()
        {
            var zipOpen = new C1.C1Zip.C1ZipFile();
            zipOpen.Create(_rootPath + "\\Wijmo-Open." + _version + ".zip");

            var zipComplete = new C1.C1Zip.C1ZipFile();
            zipComplete.Create(_rootPath + "\\Wijmo-Complete." + _version + ".zip");

            AddFilesAndFolders(zipOpen, _deploymentRootPath + "\\wijmo-open", 1);

            AddFilesAndFolders(zipComplete, _deploymentRootPath, 1);
        }

        static void AddFilesAndFolders(C1ZipFile zip, string path, int lvl)
        {
            var files = Directory.GetFiles(path);
            for (int i = 0; i < files.Length; i++)
            {
                zip.Entries.Add(files[i], lvl);
            }

            var dirs = Directory.GetDirectories(path);
            for (int i = 0; i < dirs.Length; i++)
            {
                AddFilesAndFolders(zip, dirs[i], lvl + 1);
            }

        }

        static void UpdateWijmoRef(string file)
        {
            StreamReader input = new StreamReader(file);
            var content = input.ReadToEnd();
            input.Close();

            content = Regex.Replace(content, @"jquery.wijmo-open\.\d{1,3}\.\d{1,3}\.\d{1,3}\.css", "jquery.wijmo-open." + _version + ".css");
            content = Regex.Replace(content, @"jquery.wijmo-open\.\d{1,3}\.\d{1,3}\.\d{1,3}\.min.js", "jquery.wijmo-open." + _version + ".min.js");
            content = Regex.Replace(content, @"jquery.wijmo-complete\.\d{1,3}\.\d{1,3}\.\d{1,3}\.css", "jquery.wijmo-complete." + _version + ".css");
            content = Regex.Replace(content, @"jquery.wijmo-complete\.\d{1,3}\.\d{1,3}\.\d{1,3}\.min.js", "jquery.wijmo-complete." + _version + ".min.js");

            StreamWriter writer = new StreamWriter(file, false);
            writer.Write(content);
            writer.Close();
        }

        static void UpdateDirectoryWijmoRef(string path)
        {
            var files = Directory.GetFiles(path, "*.html", SearchOption.AllDirectories);

            for (int i = 0; i < files.Length; i++)
            {
                UpdateWijmoRef(files[i]);
            }
        }
        static void MinifyJS()
        {

            string destPath = _deploymentRootPath + @"\Wijmo-Complete\development-bundle\wijmo\minified";
            Directory.CreateDirectory(destPath);

            string f = string.Empty;

            string minifiedTool = _rootPath + "\\tools\\AjaxMin.exe";

            // minify the complete
            for (int i = 0; i < _wijmoComplete.Length; i++)
            {
                f = _wijmoComplete[i];
                if (f == string.Empty) continue;

                string sourcefile = _deploymentRootPath + @"\Wijmo-Complete\development-bundle\wijmo\" + f;
                string destFile = destPath + "\\" + f;
                destFile = destFile.Replace(".js", ".min.js");

                ProcessStartInfo psi = new ProcessStartInfo(minifiedTool, "-JS -term -clobber:true " + sourcefile + " -out " + destFile);

                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;

                Process minify = Process.Start(psi);
                minify.WaitForExit();
            }

            // minify the open

            destPath = _deploymentRootPath + @"\Wijmo-Open\development-bundle\wijmo\minified";
            Directory.CreateDirectory(destPath);

            for (int i = 0; i < _wijmoOpen.Length; i++)
            {
                f = _wijmoOpen[i];
                if (f == string.Empty) continue;

                string sourcefile = _deploymentRootPath + @"\Wijmo-Open\development-bundle\wijmo\" + f;
                string destFile = destPath + "\\" + f;
                destFile = destFile.Replace(".js", ".min.js");

                ProcessStartInfo psi = new ProcessStartInfo(minifiedTool, "-JS -term -clobber:true " + sourcefile + " -out " + destFile);

                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;

                Process minify = Process.Start(psi);
                minify.WaitForExit();
            }

            // combine the open mindified files as we need these for wijmo-complete
            StreamWriter combined = new StreamWriter(_deploymentRootPath + @"\wijmo-complete\development-bundle\external\jquery.wijmo-open." + _version + ".min.js");
            combined.Write(_copyrightOpen);
            combined.WriteLine("*/");
            // now combine all the minified open files and put them in the complete/external
            for (int i = 0; i < _wijmoOpen.Length; i++)
            {
                f = _wijmoOpen[i];
                if (f == string.Empty) continue;

                string inputfile = _deploymentRootPath + @"\wijmo-open\development-bundle\wijmo\minified\" + f;
                inputfile = inputfile.Replace(".js", ".min.js");

                StreamReader reader = new StreamReader(inputfile);
                combined.WriteLine(reader.ReadToEnd());
                reader.Close();
            }
            combined.Close();

            // also copy the open mindified to the js folder
            Directory.CreateDirectory(_deploymentRootPath + @"\wijmo-open\js");
            File.Copy(_deploymentRootPath + @"\wijmo-complete\development-bundle\external\jquery.wijmo-open." + _version + ".min.js",
                _deploymentRootPath + @"\wijmo-open\js\jquery.wijmo-open." + _version + ".min.js", true);

            // combine the complete minified
            Directory.CreateDirectory(_deploymentRootPath + @"\wijmo-complete\js");
            combined = new StreamWriter(_deploymentRootPath + @"\wijmo-complete\js\jquery.wijmo-complete." + _version + ".min.js");
            combined.Write(_copyrightComplete);
            combined.WriteLine("*/");
            // now combine all the minified open files and put them in the complete/external
            for (int i = 0; i < _wijmoComplete.Length; i++)
            {
                f = _wijmoComplete[i];
                if (f == string.Empty) continue;

                string inputfile = _deploymentRootPath + @"\wijmo-complete\development-bundle\wijmo\minified\" + f;
                inputfile = inputfile.Replace(".js", ".min.js");

                StreamReader reader = new StreamReader(inputfile);
                combined.WriteLine(reader.ReadToEnd());
                reader.Close();
            }
            combined.Close();


            // ******
            // combine the open non-mindified files as we need these for wijmo-complete
            combined = new StreamWriter(_deploymentRootPath + @"\wijmo-complete\development-bundle\external\jquery.wijmo-open." + _version + ".js");
            // now combine all the minified open files and put them in the complete/external
            for (int i = 0; i < _wijmoOpen.Length; i++)
            {
                f = _wijmoOpen[i];
                if (f == string.Empty) continue;

                string inputfile = _deploymentRootPath + @"\wijmo-open\development-bundle\wijmo\" + f;

                StreamReader reader = new StreamReader(inputfile);
                combined.Write(reader.ReadToEnd());
                reader.Close();
            }
            combined.Close();

            // combine all the complete non-minified for debuggin purposes
            combined = new StreamWriter(_deploymentRootPath + @"\wijmo-complete\js\jquery.wijmo-complete." + _version + ".js");
            // now combine all the minified open files and put them in the complete/external
            for (int i = 0; i < _wijmoComplete.Length; i++)
            {
                f = _wijmoComplete[i];
                if (f == string.Empty) continue;

                string inputfile = _deploymentRootPath + @"\wijmo-complete\development-bundle\wijmo\" + f;

                StreamReader reader = new StreamReader(inputfile);
                combined.WriteLine(reader.ReadToEnd());
                reader.Close();
            }
            combined.Close();

            // now copyright all the minified files
            #region ** copyright minified files

            var files = Directory.GetFiles(_deploymentRootPath + "\\wijmo-open\\development-bundle\\wijmo\\minified", "*.js");

            // now add the copyright to the individual minified files
            foreach (var minfile in files)
            {
                FileInfo finfo = new FileInfo(minfile);
                StreamReader reader = new StreamReader(minfile);
                var line = reader.ReadToEnd();
                reader.Close();
                File.Delete(minfile);
                StreamWriter writer = new StreamWriter(minfile);
                writer.Write(_copyrightOpen);
                writer.WriteLine("*/");
                writer.Write(line);
                writer.Close();
            }

            files = Directory.GetFiles(_deploymentRootPath + "\\wijmo-complete\\development-bundle\\wijmo\\minified", "*.js");

            // now add the copyright to the individual minified files
            foreach (var minfile in files)
            {
                FileInfo finfo = new FileInfo(minfile);
                StreamReader reader = new StreamReader(minfile);
                var line = reader.ReadToEnd();
                reader.Close();
                File.Delete(minfile);
                StreamWriter writer = new StreamWriter(minfile);
                writer.Write(_copyrightComplete);
                writer.WriteLine("*/");
                writer.Write(line);
                writer.Close();
            }

            #endregion

        }

        static bool isInList(string name, string[] array)
        {
            var results = Array.FindAll(array, s => s.Equals(name));

            return results.Length > 0;
        }

        static void CleanOldWijmo()
        {
            CleanOldWijmo(_deploymentRootPath + @"\Wijmo-Complete\development-bundle\external");
            CleanOldWijmo(_deploymentRootPath + @"\Wijmo-Open\development-bundle\external");
        }
        static void CleanOldWijmo(string path)
        {
            var files = Directory.GetFiles(path, "*.js");

            for (int i = 0; i < files.Length; i++)
            {
                FileInfo f = new FileInfo(files[i]);
                if(f.Name.ToLower().StartsWith("jquery.wijmo-open."))
                {
                    f.Delete();
                }
            }

        }

        static void CleanCSSFiles()
        {
            // clean up open css
            string path = _deploymentRootPath + @"\wijmo-open\development-bundle\themes\wijmo";

            var files = Directory.GetFiles(path);

            foreach (string file in files)
            {
                FileInfo finfo = new FileInfo(file);

                if (!isInList(finfo.Name, _wijmoOpenCSS))
                {
                    File.Delete(file);
                }
            }

            // clean up complete css
            path = _deploymentRootPath + @"\wijmo-complete\development-bundle\themes\wijmo";

            files = Directory.GetFiles(path);

            foreach (string file in files)
            {
                FileInfo finfo = new FileInfo(file);

                if (!isInList(finfo.Name, _wijmoCompleteCSS))
                {
                    File.Delete(file);
                }
            }

            // now copy the css files
            StreamWriter opencss = new StreamWriter(_deploymentRootPath + @"\wijmo-complete\development-bundle\themes\wijmo\jquery.wijmo-open." + _version + ".css");
            path = _deploymentRootPath + @"\wijmo-open\development-bundle\themes\wijmo";

            foreach (string file in _wijmoOpenCSS)
            {
                if (file.Length > 0)
                {
                    StreamReader reader = new StreamReader(path + "\\" + file);
                    opencss.Write(reader.ReadToEnd());
                    reader.Close();
                }
            }

            opencss.Close();
            Directory.CreateDirectory(_deploymentRootPath + @"\wijmo-open\css");

            File.Copy(_deploymentRootPath + @"\wijmo-complete\development-bundle\themes\wijmo\jquery.wijmo-open." + _version + ".css",
                _deploymentRootPath + @"\wijmo-open\css\jquery.wijmo-open." + _version + ".css", true);


            // now copy the css files
            Directory.CreateDirectory(_deploymentRootPath + @"\wijmo-complete\css");
            opencss = new StreamWriter(_deploymentRootPath + @"\wijmo-complete\css\jquery.wijmo-complete." + _version + ".css");
            path = _deploymentRootPath + @"\wijmo-complete\development-bundle\themes\wijmo";

            foreach (string file in _wijmoCompleteCSS)
            {
                if (file.Length > 0)
                {
                    StreamReader reader = new StreamReader(path + "\\" + file);
                    opencss.Write(reader.ReadToEnd());
                    reader.Close();
                }
            }

            opencss.Close();
        }

        static void MinifyCSS()
        {
            string root = _rootPath;
            string deploy = _deploymentRootPath;

            Directory.CreateDirectory(_deploymentRootPath + "\\css");
            // copy the css
            CopyFolder(_deploymentRootPath + @"\development-bundle\themes\wijmo", _deploymentRootPath + "\\css");

            var output = File.CreateText(_deploymentRootPath + @"\css\jquery.ui.wijmo."+_version+".css");
            foreach (var f in _wijmoCompleteCSS)
            {
                string path = _deploymentRootPath + @"\css\" + f;
                if (File.Exists(path))
                {
                    var input = File.OpenText(path);
                    output.Write(input.ReadToEnd());
                    input.Close();
                    File.Delete(path);
                }
            }
            output.Close();


            /*
            // now actually minify the files
            string minifiedTool = rootPath + "\\tools\\AjaxMin.exe";

            foreach (var f in _wijmoComplete)
            {
                string cssName = f.Replace(".js", ".css");

                minifyFilename = minifyFilename.Replace("e\\wijmo\\", "e\\wijmo\\themes\\wijmominified\\");

                ProcessStartInfo psi = new ProcessStartInfo(minifiedTool,
                    @"-CSS -clobber:true " + f + " -out " + minifyFilename);

                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;

                Process minify = Process.Start(psi);
                minify.WaitForExit();
            }
             * */

        }

        static void CopyFolder(string srcPath, string destPath)
        {
            Directory.CreateDirectory(destPath);

            var files = Directory.GetFiles(srcPath);
            for (int i = 0; i < files.Length; i++)
            {
                FileInfo f = new FileInfo(files[i]);
                File.Copy(files[i], destPath + "\\" + f.Name, true);
                f = new FileInfo(destPath + "\\" + f.Name);
                f.Attributes = FileAttributes.Normal;
            }

            var dirs = Directory.GetDirectories(srcPath);

            for (int i = 0; i < dirs.Length; i++)
            {
                CopyFolder(dirs[i], dirs[i].ToLower().Replace(srcPath.ToLower(), destPath));
            }

        }
    }
}
