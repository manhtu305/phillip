﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace wijmoThemes
{
    class Program
    {
        static string _srcPath = string.Empty;
        static string _dstPath = string.Empty;
        static string _controlType = string.Empty;

        static void Main(string[] args)
        {
            _controlType = args[0];
            _srcPath = args[1];
            _dstPath = args[2];
            if (_dstPath.EndsWith("\\"))
            {
                _dstPath = _dstPath.TrimEnd('\\');
            }

            ProcessThemes();
        }

        static void ProcessThemes()
        {
            if (!Directory.Exists(_dstPath))
            {
                Directory.CreateDirectory(_dstPath);
            }

            if (!Directory.Exists(_dstPath + "\\Resources"))
            {
                Directory.CreateDirectory(_dstPath + "\\Resources");
            }

            if (!Directory.Exists(_dstPath + "\\Resources\\themes"))
            {
                Directory.CreateDirectory(_dstPath + "\\Resources\\themes");
            }

            var dirs = Directory.GetDirectories(_srcPath);

            foreach (var dir in dirs)
            {
                ProcessCss(dir);
            }
        }

		static void CopyDirectory(string src, string des)
		{
			if (Directory.Exists(src))
			{
				if (!Directory.Exists(des))
				{
					Directory.CreateDirectory(des);
				}

				foreach (string str in Directory.GetFiles(src))
				{
					FileInfo finfo = new FileInfo(str);
					string fname = string.Format(@"{0}\{1}", des, finfo.Name);
					File.Copy(str, fname, true);
					finfo = new FileInfo(fname);
					finfo.Attributes = FileAttributes.Normal;
				}

				foreach (string subDir in Directory.GetDirectories(src))
				{
					string subFileName = Path.GetFileName(subDir);
					string subDesFileName = string.Format(@"{0}\{1}", des, subFileName);

					Directory.CreateDirectory(subDesFileName);

					try
					{
						CopyDirectory(subDir, subDesFileName);
					}
					catch (Exception)
					{
					}
				}
			}
		}

        static void ProcessCss(string dir)
        {
            DirectoryInfo dinfo = new DirectoryInfo(dir);
            string themePath = _dstPath + "\\Resources\\themes\\" + dinfo.Name;
            if (!Directory.Exists(themePath))
            {
                Directory.CreateDirectory(themePath);
            }
            if (!Directory.Exists(themePath + "\\images"))
            {
                Directory.CreateDirectory(themePath + "\\images");
            }
			//Add comments by RyanWu@20111026.
			//In order to copy the sub folds under the images fold.
            // copy all the image files
			//var images = Directory.GetFiles(dir+"\\images");
			//foreach(var image in images)
			//{
			//    FileInfo finfo = new FileInfo(image);
			//    string fname = themePath + "\\images\\" + finfo.Name;
			//    File.Copy(image, fname, true);
			//    finfo = new FileInfo(fname);
			//    finfo.Attributes = FileAttributes.Normal;
			//}
			CopyDirectory(dir + "\\images", themePath + "\\images");
			//end by RyanWu@20111026.

            var files = Directory.GetFiles(dir, "*.css");

            foreach (var file in files)
            {
                FileInfo finfo = new FileInfo(file);
                StreamReader reader = new StreamReader(file);
                StreamWriter writer = new StreamWriter(themePath + "\\" + finfo.Name);
                string buf = reader.ReadToEnd();

                // remove comments
                string expr = @"/\*[\d\D]*?\*/";
                buf = Regex.Replace(buf, expr, "", RegexOptions.Multiline);

                reader.Close();


                writer.Write(ReplaceImageUrl(buf, dinfo.Name));
                writer.Close();

            }
        }

        static string ReplaceImageUrl(string buf, string theme)
        {
            StringBuilder updatedCss = new StringBuilder();

            int idx = buf.IndexOf("url", 0, StringComparison.CurrentCultureIgnoreCase);

            if (idx >= 0)
            {
                int start = 0;
                while (idx >= 0)
                {
                    string s1 = buf.Substring(start, idx - start);
                    updatedCss.Append(buf.Substring(start, idx - start));
                    // get the image name
                    int nameStart = buf.IndexOf('(', idx);
                    int nameEnd = buf.IndexOf(')', idx);
                    string imageName = buf.Substring(nameStart + 1, nameEnd - nameStart - 1).Trim();
                    imageName = imageName.Replace("'", "");
                    imageName = imageName.Replace("\"", "");
                    imageName = imageName.Replace('/', '.');
                    imageName = imageName.Replace('\\', '.');

                    string res = string.Format("url('<%= WebResource(\"C1.Web.Wijmo.{0}.Resources.themes.{1}.{2}\")%>')",
                                _controlType, theme.Replace("-","_"), imageName);
                    updatedCss.Append(res);
                    start = nameEnd + 1;
                    string s2 = buf.Substring(start);
                    idx = buf.IndexOf("url", start);
                    if (idx < 0)
                        updatedCss.Append(buf.Substring(start));
                }
            }
            else
            {
                updatedCss.Append(buf);
            }

            return updatedCss.ToString();
        }
    }
}
