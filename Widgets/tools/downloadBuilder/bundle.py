import os
from os import path
import re
from component import *

class Bundle:
    isWijmo = False

    def __init__(self, name):
        self.name = name
        self.components = {}

    def add(self, component):
        self.components[component.name] = component

    def _findAny(self, dirs, names, isJs, patterns = None):
        ext = '.js' if isJs else '.css'

        names = list(name + ext for name in names)
        patterns = list('^' + p + re.escape(ext) + '$' for p in patterns) if patterns else None

        for dir in dirs:
            for name in names:
                filename = path.join(dir, name)
                if path.exists(filename):
                    return filename

            if patterns:
                for name in os.listdir(dir):
                    filename = path.join(dir, name)
                    if path.isfile(filename) and any([re.match(p, name) for p in patterns]):
                        return filename

        return None

    def findFile(self, component, isJs, minified):
        raise NotImplementedError()


class WijmoBundle(Bundle):
    isWijmo = True

    def __init__(self, name, path):
        Bundle.__init__(self, name)
        self.path = path

    def findFile(self, name, isJs, minified):
        dir = self.jsFolder if isJs else self.cssFolder
        dirs = [dir]
        if minified:
            dirs.append(path.join(dir, 'minified'))

        if minified:
            name += '.min'
        names = ['jquery.wijmo.' + name, 'jquery.plugin.' + name]
        return self._findAny(dirs, names, isJs)

    @property
    def devBundleFolder(self):
        return os.path.join(self.path, 'development-bundle')

    @property
    def jsFolder(self):
        return os.path.join(self.devBundleFolder, 'wijmo')

    @property
    def themesFolder(self):
        return os.path.join(self.devBundleFolder, 'themes')

    @property
    def cssFolder(self):
        return os.path.join(self.themesFolder, 'wijmo')
    
    @property
    def externalFolder(self):
        return path.join(self.devBundleFolder, 'external')


class ExternalBundle(Bundle):
    def __init__(self, wijmoBundles):
        Bundle.__init__(self, 'external')
        self.wijmoBundles = wijmoBundles

    def findFile(self, name, isJs, minified):
        dirs = [b.externalFolder for b in self.wijmoBundles]
        names = [name, name + '.min', name + '-min']

        withoutMin = re.escape(name).replace('\.', '[\.\-]') + '([\.\-][\d\.]+)?'
        withMin = withoutMin + '([\.\-]min)'
        patterns = [withoutMin, withMin] if not minified else [withMin, withoutMin]
        return self._findAny(dirs, names, isJs, patterns)


class BundleSet:
    def __init__(self):
        self._bundles = []
        self.componentIndex = {}

    def add(self, bundle):
        self._bundles.append(bundle)
        for (name, w) in bundle.components.items():
            self.componentIndex[name] = w

    def findComponent(self, name):
        return self.componentIndex[name]