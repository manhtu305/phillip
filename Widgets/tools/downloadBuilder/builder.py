import os
import re
import shutil
import util
from os import path
import re

names = {
    'combinedJs': "jquery.wijmo.custom.{ver}{min}.js",
    'combinedCss': "jquery.wijmo.custom.{ver}{min}.css"
}


doNotCombine = ['jquery', 'jquery.ui']


class Builder:
    def __init__(self, version):
        self.version = version
        self._components = None
        self._themes = None

    def _subdir(self, parent, name):
        dir = path.join(parent, name)
        os.makedirs(dir)
        return dir

    def _getName(self, name, **kwargs):
        kwargs['ver'] = self.version
        template = names[name]
        return template.format(**kwargs)

    def _combine(self, dir, templateName, getFullPath, customMinify=None, raiseIfNotFound=True):
        def combineFile(minified):
            targetName = self._getName(templateName, min='.min' if minified else '')
            targetPath = path.join(dir, targetName)
            with open(targetPath, 'w', encoding='utf-8') as output:
                for c in self._components:
                    if c.name in doNotCombine:
                        continue
                    doMinify = False
                    srcFile = None
                    if minified:
                        srcFile = getFullPath(c, True)
                        doMinify = not srcFile
                    srcFile = srcFile or getFullPath(c, False)
                    if not srcFile:
                        if raiseIfNotFound:
                            raise util.DownloadBuilderError('Module %s not found' % c.name)
                        else:
                            continue

                    output.write('/******************** %s ********************/\n' % path.basename(srcFile))
                    with open(srcFile, encoding='utf-8') as widgetFile:
                        contents = widgetFile.read()
                        if doMinify and customMinify:
                            contents = customMinify(contents)
                        output.write(contents)
                    output.write(';\r\n')

        combineFile(False)
        combineFile(True)

    def _buildJsFolder(self, parent):
        self._combine(self._subdir(parent, 'js'), 'combinedJs', lambda w, minified: w.jsPath(minified))

    def _buildCssFolder(self, parent):
        self._combine(self._subdir(parent, 'css'), 'combinedCss', lambda w, minified: w.cssPath(minified),
            self._minifyCss, raiseIfNotFound=False)

    def _copyFiles(self, dir, paths):
        for srcPath in paths:
            if not srcPath or not path.exists(srcPath): continue
            targetPath = path.join(dir, path.basename(srcPath))
            shutil.copyfile(srcPath, targetPath)

    def _buildWidgetsFolder(self, parent):
        def copyFiles(dir, minified):
            self._copyFiles(dir, [c.jsPath(minified) for c in self._components if c.isWijmo])

        wijmo = self._subdir(parent, 'wijmo')
        copyFiles(wijmo, False)
        minified = self._subdir(wijmo, 'minified')
        copyFiles(minified, True)

    def _buildThemes(self, parent):
        themes = self._subdir(parent, 'themes')
        wijmo = self._subdir(themes, 'wijmo')
        self._copyFiles(wijmo, [c.cssPath() for c in self._components if c.isWijmo])
        for b in self._components.wijmoBundles():
            util.copySubfolders(b.cssFolder, wijmo)

        for themeName in self._themes:
            themeFound = False
            for b in self._components.wijmoBundles():
                themeFolder = path.join(b.themesFolder, themeName)
                if not path.exists(themeFolder): continue
                themeFound = True
                util.copyTree(themeFolder, path.join(themes, themeName))
            if not themeFound:
                raise util.DownloadBuilderError('Theme %s not found' % themeName)

    def _buildExternal(self, parent):
        dir = self._subdir(parent, 'external')
        self._copyFiles(dir, [c.jsPath() for c in self._components if not c.isWijmo])

    def _buildDevelopmentBundleFolder(self, parent):
        dir = self._subdir(parent, 'development-bundle')
        self._buildWidgetsFolder(dir)
        self._buildThemes(dir)
        self._buildExternal(dir)

    def _minifyCss(self, css):
        return re.subn('\s+', ' ', css)[0]

#    def _putRootFiles(self, dir):
#        for f in os.listdir(self.cfg.srcPath):
#            srcPath = path.join(self.cfg.srcPath, f)
#            if path.isfile(srcPath):
#                shutil.copyfile(srcPath, path.join(dir, f))

    def build(self, dir, components, themes):
        self._components = components
        self._themes = themes
        try:
            self._buildJsFolder(dir)
            self._buildCssFolder(dir)
            self._buildDevelopmentBundleFolder(dir)
#            self._putRootFiles(dir)
        finally:
            self._components = None
            self._themes = None
