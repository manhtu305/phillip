from queue import Queue
from os import path
import util

class Component:
    def __init__(self, name, bundle, dependencies, customCss):
        self.name = name
        self.bundle = bundle
        self.dependencies = dependencies
        self.customCss = customCss
        self.resolvedDependencies = None

    def jsPath(self, minified=False):
        return self.bundle.findFile(self.name, True, minified)

    def cssPath(self, minified=False):
        return self.bundle.findFile(self.customCss or self.name, False, minified)

    @property
    def isWijmo(self):
        return self.bundle.isWijmo

class ComponentSet:
    def __init__(self, bundleSet):
        self.bundleSet = bundleSet
        self._components = []
        self._index = {}

    def resolve(self, name):
        component = self._index.get(name)
        if component:
            return component

        component = self.bundleSet.findComponent(name)
        if not component:
            raise util.DownloadBuilderError('%s not found' % name)
        self._index[name] = component
        self._components.append(component)
        return component

    def resolveAll(self, names):
        for name in names:
            self.resolve(name)

    def resolveDependencies(self):
        queue = Queue()
        for w in self._components:
            queue.put(w)

        while not queue.empty():
            component = queue.get()
            if not component.dependencies:
                continue

            component.resolvedDependencies = []
            for depName in component.dependencies:
                dep = self.resolve(depName)
                component.resolvedDependencies.append(dep)
                if dep.dependencies and not dep.resolvedDependencies:
                    queue.put(dep)

    def sort(self):
        added = set()
        def add(component):
            self._components.append(component)
            added.add(component)

        def addWithDeps(component):
            if component in added: return
            if component.resolvedDependencies:
                for d in component.resolvedDependencies:
                    addWithDeps(d)
            add(component)

        unsorted = self._components.copy()
        self._components.clear()
        for d in unsorted:
            addWithDeps(d)

    def __iter__(self):
        return iter(self._components)

    def bundles(self):
        yielded = set()
        for w in self:
            if w.bundle in yielded: continue
            yielded.add(w.bundle)
            yield  w.bundle

    def wijmoBundles(self):
        return [b for b in self.bundles() if b.isWijmo]