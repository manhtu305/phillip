import argparse
import tempfile
import zipfile
import xml.etree.ElementTree as ET
import json
import shutil

import util
from bundle import *
import builder

mainDir = path.normpath(path.dirname(__file__))

def parseOptions():
    parser = argparse.ArgumentParser(description='Wijmo download builder')
    parser.add_argument('-w', '--widgets', action='store', default='',
        metavar='widgetList', required=True, help='List of widgets separated by comma (,)')
    parser.add_argument('-t', '--themes', action='store', default='',
        metavar='themeList', help='List of themes separated by comma (,)')
    parser.add_argument('path')
    return parser.parse_args()

def readFile(filename):
    with open(filename, encoding='utf-8') as file:
        return file.read()

def expandDropFile(targetDir):
    cfg = ET.parse(path.join(mainDir, 'wijDownloadBuilder.config'))
    filename = cfg.getroot().attrib['dropFile']
    if not path.isabs(filename):
        filename = path.join(mainDir, filename)
    filename = path.normpath(filename)

    if not path.exists(filename):
        raise util.DownloadBuilderError('Drop file not found: ' + filename)

    with zipfile.ZipFile(filename, 'r') as drop:
        verFileName = 'Wijmo-Open\\version.txt'
        verFilePath = path.join(targetDir, verFileName)
        if path.exists(verFilePath):
            curVersion = readFile(verFilePath)
            rootDir = next(iter(drop.namelist()))
            while path.dirname(rootDir):
                rootDir = path.dirname(rootDir)
            dropVersion = drop.read(path.join(rootDir, verFileName).replace('\\', '/')).decode('utf-8')
            if curVersion == dropVersion:
                return curVersion
        if path.exists(targetDir):
            shutil.rmtree(targetDir)
        print('Extracting Wijmo drop...')
        parentDir = path.dirname(targetDir)
        if not path.exists(parentDir):
            os.makedirs(parentDir)
        tmpDir = path.join(parentDir, 'tmp')
        if path.exists(tmpDir):
            shutil.rmtree(tmpDir)
        drop.extractall(tmpDir)
        wijmoDir = path.join(tmpDir, next(iter(os.listdir(tmpDir))))
        os.rename(wijmoDir, targetDir)
        shutil.rmtree(tmpDir)
        return readFile(verFilePath)


def loadBundles(srcPath):
    with open(path.join(mainDir, 'widgets.json')) as widgetsFile:
        widgetCfg = json.load(widgetsFile)
    bundles = BundleSet()

    def loadBundle(bundle):
        bundleCfg = widgetCfg[bundle.name]

        for name, component in bundleCfg.items():
            if type(component) == list:
                dependencies = component
                css = None
            else:
                dependencies = component.get('dependencies', [])
                css = component.get('css')

            bundle.add(Component(name, bundle, dependencies, css))
        bundles.add(bundle)

        return bundle

    wijmoOpen = WijmoBundle('open', path.join(srcPath, 'Wijmo-Open'))
    wijmoPro = WijmoBundle('pro', path.join(srcPath, 'Wijmo-Pro'))
    loadBundle(wijmoOpen)
    loadBundle(wijmoPro)
    loadBundle(ExternalBundle([wijmoOpen, wijmoPro]))
    return bundles


options = parseOptions()

try:
    srcPath = path.join(mainDir, 'cache/drop')
    if not path.exists(srcPath):
        os.makedirs(srcPath)
    version = expandDropFile(srcPath)
    bundles = loadBundles(srcPath)
    
    widgets = ComponentSet(bundles)
    widgets.resolveAll(options.widgets.split(','))
    widgets.resolveDependencies()
    widgets.sort()

    themes = options.themes.split(',')

    builder = builder.Builder(version)

    with tempfile.TemporaryDirectory() as tmpDir:
        with zipfile.ZipFile(options.path, 'w') as zip:
            builder.build(tmpDir, widgets, themes)
            util.zipDir(tmpDir, zip)
except util.DownloadBuilderError as err:
    print('Error: ' + err.message)
    exit(1)