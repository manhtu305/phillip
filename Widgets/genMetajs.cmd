@echo off

SET TF="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\CommonExtensions\Microsoft\TeamFoundation\Team Explorer\TF.exe"

echo Checking out .ts.meta.js files
%TF% checkout Documentation\dx\*.ts.meta.js

echo Checking out jquery.wijmo.widget.js
%TF% checkout Documentation\dx\jquery.wijmo.widget.js

echo Files are checked out

echo Building docs
call grunt docs

if %ERRORLEVEL% NEQ 0 (
	echo Could not build the docs!
	GOTO FIN
)
echo Success: .ts.meta.js files are updated

:FIN

echo Press Enter to continue
set /p x=