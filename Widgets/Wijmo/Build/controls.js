/// <reference path="../External/declarations/node.d.ts" />
var fs = require("fs");
var path = require("path");
var util = require("./util");
var child_process = require("child_process");

function convertUrlsToWebResources(css, namespace, theme) {
    return css.replace(/url\(['"]?([^'"\)]+)[\'"]?\)/g, function (m, url) {
        url = url.replace(/['"]/g, "").replace(/[\/\\]/g, ".");

        theme = theme.replace(/\-/g, "_");
        var fullUrl = "C1.Web.Wijmo." + namespace + ".Resources.themes." + theme + "." + url;
        return "url('<%= WebResource(\"" + fullUrl + "\")%>')";
    });
}

function prepareCssForControls(css, namespace, theme) {
    css = convertUrlsToWebResources(css, namespace, theme);
    return css;
}
exports.prepareCssForControls = prepareCssForControls;

function readListFile(file) {
    file = path.join("Wijmo/Build/controls", file);
    if (!fs.existsSync(file)) {
        return [];
    }
    return util.readFile(file).replace("\r", "").split("\n").map(function (f) {
        return f.trim();
    }).filter(function (f) {
        return f.length > 0;
    });
}

function getConfig() {
    return {
        concat: {
            "controls-open": {
                src: readListFile("extension-open.lst"),
                dest: "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Resources/scripts/extensions-open.js"
            },
            "controls-pro": {
                src: readListFile("extension-pro.lst"),
                dest: "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Resources/scripts/extensions-pro.js"
            },
            "controls-extension": {
                src: readListFile("extensions-control.lst"),
                dest: "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Resources/scripts/extensions-control.js"
            },
            "controls-widgets": {
                src: readListFile("widgets-control.lst"),
                dest: "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Resources/scripts/widgets-control.js"
            },
            "mobile-control": {
                src: readListFile("mobile-control.lst"),
                dest: "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Resources/scripts/mobile-control.js"
            }
        },
        updateMVCWijmoVer: {
            mvc: {
                src: ["../ASP.Net.MVC/Samples/**/*.*html", "../ASP.Net.MVC/VSProjectTemplates*/**/*.*html"]
            },
            webForm: {
                src: "../ASP.Net.Webforms/Samples/**/*.cshtml"
            }
        },
        updateSampleCtrlVer: {
            src: "../ASP.Net.Webforms/Samples/**/*.config"
        }
    };
}
exports.getConfig = getConfig;

function registerTasks(grunt, namespace, wijmoOpen, wijmoPro) {
    function copyCssFile(src, dest, theme) {
        var css = util.readFile(src);
        css = exports.prepareCssForControls(css, namespace, theme);
        grunt.file.write(dest, css);
    }

    function copyFolder(src, dest, theme) {
        var pattern = path.join(src, "**/*.*");
        grunt.file.expand(pattern).forEach(function (filename) {
            var targetFilename = path.join(dest, filename.substr(src.length));
            if (filename.match(/\.css$/i)) {
                copyCssFile(filename, targetFilename, theme);
            } else {
                grunt.file.copy(filename, targetFilename);
            }
        });
    }

    function checkoutModifiedFiles(files) {
        var tfs = require('../../node_modules/tfs-unlock/tfs-unlock.js');

        tfs.init({
            "visualStudioPath": tfs["vs2015"]["bit64"]
        });

        // Iterate over all specified file groups.
        files.forEach(function (file) {
            // Warn on and remove invalid source files (if nonull was set).
            if (!grunt.file.exists(file)) {
                console.log('Source file "' + file + '" not found.');
                return false;
            }
            console.log("Checkout the file:" + tfs["checkout"]([file]));
        });
    }

    var resourcesFolder = "../ASP.Net.Webforms/C1.Web.Wijmo." + namespace + "/Resources/";
    var targetThemesFolder = path.join(resourcesFolder, "themes");
    var nsLower = namespace.toLocaleLowerCase();
    var themesTask = nsLower + "-themes";

    grunt.registerTask(themesTask, function () {
        [wijmoOpen, wijmoPro].forEach(function (p) {
            var targetFile = path.join(targetThemesFolder, "/wijmo/jquery.wijmo-" + p.name.toLowerCase() + "." + namespace + ".css");
            copyCssFile(p.combinedCssFile(), targetFile, "wijmo");
        });
        copyFolder(path.join(wijmoPro.dist, "css/images"), path.join(targetThemesFolder, "wijmo/images"), "wijmo");

        //copy boot strap css.
        var src_bootstrap = path.join(wijmoPro.dist, "css/interop/bootstrap-wijmo.css"), dest_bootstrap = path.join(targetThemesFolder, "wijmo/jquery.wijmo-bootstrap.Extenders.css");
        if (nsLower === "controls") {
            dest_bootstrap = path.join(targetThemesFolder, "wijmo/jquery.wijmo-bootstrap.Controls.css");
        }
        util.makeWritableSync(dest_bootstrap);
        copyCssFile(src_bootstrap, dest_bootstrap, "wijmo");

        // Copy themes
        grunt.file.expand(path.join(wijmoPro.dist, "themes/*")).forEach(function (themeDir) {
            var themeName = path.basename(themeDir);
            copyFolder(themeDir, path.join(targetThemesFolder, themeName), themeName);
        });
    });

    var jsTask = nsLower + "-js";
    grunt.registerTask(jsTask, function () {
        [wijmoOpen, wijmoPro].forEach(function (p) {
            [true, false].forEach(function (min) {
                var targetFile = path.join(resourcesFolder, "scripts/jquery.wijmo-" + p.name.toLowerCase() + (min ? ".min" : "") + ".js");

                //copy the bootstrap js under interop folder.
                var targetbootstrap = path.join(resourcesFolder, "scripts/bootstrap.wijmo" + (min ? ".min" : "") + ".js");

                // for the first time build, if use debug mode, the min file will not generate.
                var combinedJsFile = p.combinedJsFile(min), isCombinedJsFileExist = grunt.file.exists(combinedJsFile);

                if (!isCombinedJsFileExist) {
                    // if the target file is exist and the min js file is not exist, Do not copy this file.
                    if (grunt.file.exists(targetFile)) {
                        console.log(p.combinedJsFile(min));
                        return true;
                    }

                    // if the target file is not exist, copy an empty file.
                    grunt.file.write(p.combinedJsFile(min), "");
                    grunt.file.write(p.bootstrapWijmoJSFile(min), "");
                }

                grunt.file.copy(p.combinedJsFile(min), targetFile);
            });
        });
    });

    grunt.registerTask("processBootstrap", function () {
        var wijmoBootStrap = grunt.file.read(wijmoPro.bootstrapWijmoJSFile(false)), resourceFolder = "../ASP.Net.Webforms/C1.Web.Wijmo.Extenders/Resources/scripts", bootstrapControls = grunt.file.read(path.join(resourceFolder, "bootstrap.controls.js")), reg = /([\s\S]*)(\$\(document\).ready\(function\s?\(\s?\)\s?{)([\s\S]*)(\}\);)([\r|\n]+\}\)\(typeof[\s\S]*)/ig, content = wijmoBootStrap.replace(reg, "$1$3" + bootstrapControls + "$5");

        grunt.file.write(path.join(resourceFolder, "bootstrap.wijmo.js"), content);
        grunt.file.write(path.join(resourceFolder, "bootstrap.wijmo.min.js"), grunt.helper('uglify', content, grunt.config('uglify')));
    });

    var tscTask = "tsc-" + nsLower;
    grunt.registerTask(tscTask, function () {
        // if extender, compile extender's ts files, if controls, compile extender and control's ts files.
        var pattern = "../ASP.Net.Webforms/C1.Web.Wijmo.Extenders/Resources/**/*.ts", files = grunt.file.expand(pattern), force = Array.prototype.indexOf.call(arguments, "force") >= 0, tsc_path = path.join(__dirname, "../../tools/tsc/tsc.exe"), jsFile;

        if (nsLower === "controls") {
            pattern = "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Resources/**/*.ts";
            files = files.concat(grunt.file.expand(pattern));
        }

        //return;
        if (!files.length) {
            console.log("No files found");
            return;
        }
        for (var i = files.length - 1; i >= 0; i--) {
            jsFile = files[i].replace(/\.ts$/, ".js");

            if (fs.existsSync(jsFile)) {
                util.makeWritableSync(jsFile);
                if (fs.statSync(files[i]).mtime <= fs.statSync(jsFile).mtime && !force) {
                    if (files.splice(i, 1))
                        ;
                    continue;
                }
            }
        }
        if (!files.length) {
            return;
        }
        var done = this.async();

        //var tscArgs = ["-c"].concat(files);
        //"-c" "--comments" is removed from TypeScript 0.9
        //var tscArgs = ["-c"].concat(files);
        var tscArgs = [].concat(files);

        var tsc = child_process.execFile(tsc_path, tscArgs, null, function (error, stdout, stdin) {
            if (tsc.exitCode !== 0) {
                grunt.log.error("TSC task failed with exit code " + tsc.exitCode);
                grunt.log.error(stdout.toString("utf8"));
                grunt.log.error(error.toString());
                done(false);
            } else {
                grunt.verbose.ok('Compiled ' + files.length + " file(s).");
                done();
            }
        });
    });

    var cetask = "updateCE-" + nsLower;

    grunt.registerTask(cetask, function () {
        var isControl = nsLower === "controls", versionFile = "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Properties/AssemblyVersion.cs", versionContent = grunt.file.read(versionFile), regReleaseNumber = /ReleaseNumber\s*=\s*"(\d+)/i, regBuildNumber = /BuildNumber\s*=\s*"(\d+)/i, releaseNumber = versionContent.match(regReleaseNumber)[1], buildNumber = versionContent.match(regBuildNumber)[1], versionStr = releaseNumber + "." + buildNumber, configFile = isControl ? "../ASP.Net.Webforms/Samples/CS/ControlExplorer/Web.config" : "../ASP.Net.Webforms/Samples/CS/ToolkitExplorer/Web.config", configStr = grunt.file.read(configFile);

        if (nsLower === "controls") {
            configStr = configStr.replace(/(C1\.Web\.Wijmo\.Controls\.4\,\s*Version=4\.0)(\.\d+\.\d+)/ig, "$1" + "." + versionStr);
        } else {
            configStr = configStr.replace(/(C1\.Web\.Wijmo\.Extenders\.3\,\s*Version=3\.5)(\.\d+\.\d+)/ig, "$1" + "." + versionStr);
        }
        util.makeWritableSync(configFile);
        grunt.file.write(configFile, configStr);
    });

    var mintask = "min-" + nsLower;

    function doMinFile(grunt, src, dest, force) {
        if (typeof force === "undefined") { force = false; }
        var max = util.readFile(src), min;

        if (fs.existsSync(dest)) {
            if (fs.statSync(src).mtime <= fs.statSync(dest).mtime || !force) {
                return;
            }
        }

        min = grunt.helper('uglify', max, grunt.config('uglify'));
        if (fs.existsSync(dest)) {
            util.makeWritableSync(dest);
        }
        grunt.file.write(dest, min);
    }

    grunt.registerTask(mintask, function () {
        // this is similar with the ts task.
        var pattern = "../ASP.Net.Webforms/C1.Web.Wijmo.Extenders/Resources/**/*.ts", files = grunt.file.expand(pattern), force = Array.prototype.indexOf.call(arguments, "force") >= 0, jsFile, minFile, max, min, otherFiles, cultureFiles;

        if (nsLower === "controls") {
            pattern = path.join("../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Resources/**/*.ts");
            files = files.concat(grunt.file.expand(pattern));

            // minify the combined files in controls.
            files = files.concat(["extensions-control", "extensions-open", "extensions-pro", "widgets-control", "mobile-control"].map(function (val) {
                return "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Resources/scripts/" + val + ".js";
            }));
        }

        for (var i = files.length - 1; i >= 0; i--) {
            jsFile = files[i].replace(/\.ts$/, ".js");
            minFile = jsFile.replace(/\.js$/, ".min.js");
            doMinFile(grunt, jsFile, minFile, force);
        }
        ;

        // minify the swfupload and swfobject files.
        otherFiles = [
            {
                src: "./Wijmo/External/swfobject.js",
                dest: "scripts/swfobject.min.js"
            }, {
                src: "./Wijmo/External/swfupload.js",
                dest: "scripts/swfupload.min.js"
            }];
        otherFiles.forEach(function (file) {
            var dest = path.join(resourcesFolder, file.dest);
            doMinFile(grunt, file.src, dest, force);
        });
    });

    grunt.registerMultiTask("updateMVCWijmoVer", "Update the wijmo version in mvc samples and vs templates.", function () {
        var files = grunt.file.expand(this.file.src), regex = /(wijmo\S*?)\.\d\.\d{5}\.\d+/ig, wijmoVersion = wijmoOpen.pkg.version;
        files.forEach(function (file) {
            if (fs.statSync(file).isFile()) {
                var content = grunt.file.read(file);
                if (regex.test(content)) {
                    content = content.replace(regex, "$1." + wijmoVersion);
                    util.makeWritableSync(file);
                    console.log("Version updated: " + file);
                    grunt.file.write(file, content);
                    checkoutModifiedFiles([file]);
                }
            }
        });
    });

    grunt.registerMultiTask("updateSampleCtrlVer", "Update the control/extender build version in asp.net samples.", function () {
        var files = grunt.file.expand(this.file.src), regex = /(C1\.Web\.Wijmo\.(?:Controls|Extenders)\.\d\,\s*Version=\d\.\d)\.\d+\.\d+/ig, versionFile = "../ASP.Net.Webforms/C1.Web.Wijmo.Controls/Properties/AssemblyVersion.cs", versionContent = grunt.file.read(versionFile), regReleaseNumber = /ReleaseNumber\s*=\s*"(\d+)/i, regBuildNumber = /BuildNumber\s*=\s*"(\d+)/i, releaseNumber = versionContent.match(regReleaseNumber)[1], buildNumber = versionContent.match(regBuildNumber)[1], versionStr = releaseNumber + "." + buildNumber;

        files.forEach(function (file) {
            var content = grunt.file.read(file);
            if (regex.test(content)) {
                content = content.replace(regex, "$1." + versionStr);
                console.log("Version updated: " + file);
                util.makeWritableSync(file);
                grunt.file.write(file, content);
                checkoutModifiedFiles([file]);
            }
        });
    });

    grunt.registerTask("concat-controls", ["concat:controls-open", "concat:controls-pro", "concat:controls-extension", "concat:controls-widgets", "concat:mobile-control"]);
    grunt.registerTask("concat-extenders", function () {
    });
    var concatTask = "concat-" + nsLower;
    grunt.registerTask(nsLower, ["wijmo", jsTask, "copy:themes", themesTask, tscTask, concatTask, mintask, cetask, "processBootstrap"]);
    grunt.registerTask(nsLower + "-release", ["wijmo-release", jsTask, "copy:themes", themesTask, tscTask + ":force", concatTask, mintask + ":force", cetask, "processBootstrap"]);
    grunt.registerTask("updateVersion", ["updateMVCWijmoVer", "updateSampleCtrlVer"]);
}
exports.registerTasks = registerTasks;
