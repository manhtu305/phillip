[Wijmo Pro](http://wijmo.com/) - Commercial jQuery UI Widgets
================================

Wijmo Pro a complete kit of jQuery UI widgets. Wijmo is an extension to jQuery UI and adds new widgets and features to your arsenal. Every widget was built to jQuery UI's standards and framework. Some widgets like slider and dialog literally extend their counterpart from jQuery UI. Each widget is ThemeRoller-ready and comes styled with our own version of Aristo.

If you want to use Wijmo, go to [wijmo.com](http://wijmo.com) to get started. Or visit the [Wijmo Forum](http://wijmo.com/groups/) for discussions and questions.

Wijmo Pro is designed for individuals or businesses that want to purchase a complete kit of jQuery UI Widgets. It includes everything in Wijmo Open in addition to more complex and powerful widgets. Wijmo Pro is licensed under the [Wijmo Commercial License](http://wijmo.com/license/wijmo-commercial-license)(fee required). Wijmo Pro will also have a more restrictive open source option for those who can comply with [GPLv3](http://wijmo.com/license/wijmo-gplv3-license).