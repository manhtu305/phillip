﻿/// <reference path="../../External/declarations/node.d.ts" />
/// <reference path="../../External/declarations/elementtree.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/** This file contains classes that represent structure of a script
* by a tree rooted with a Unit instance
*/
var fs = require("fs");
var et = require("elementtree");

;

(function (MetadataType) {
    MetadataType[MetadataType["None"] = 0] = "None";
    MetadataType[MetadataType["Module"] = 1] = "Module";
    MetadataType[MetadataType["Unit"] = 2] = "Unit";
    MetadataType[MetadataType["Class"] = 3] = "Class";
    MetadataType[MetadataType["Interface"] = 4] = "Interface";
    MetadataType[MetadataType["EnumMember"] = 5] = "EnumMember";
    MetadataType[MetadataType["Enum"] = 6] = "Enum";
    MetadataType[MetadataType["Func"] = 7] = "Func";
    MetadataType[MetadataType["Parameter"] = 8] = "Parameter";
    MetadataType[MetadataType["Property"] = 9] = "Property";
    MetadataType[MetadataType["Widget"] = 10] = "Widget";
    MetadataType[MetadataType["Option"] = 11] = "Option";
    MetadataType[MetadataType["Event"] = 12] = "Event";
    MetadataType[MetadataType["EventDataKey"] = 13] = "EventDataKey";
    MetadataType[MetadataType["Dependency"] = 14] = "Dependency";
    MetadataType[MetadataType["Dependencies"] = 15] = "Dependencies";
})(exports.MetadataType || (exports.MetadataType = {}));
var MetadataType = exports.MetadataType;

/** Base class for all metadata objects.
* @remarks
* In order to customize conversion to XML, derived classes should override _serializeAttributes and _serializeContents.
*/
var MetadataObject = (function () {
    function MetadataObject(name, summary) {
        this.name = name;
        this.summary = summary;
        this.examples = [];
        this.nodeType = 0 /* None */;
        this.elementName = "unexpected";
    }
    MetadataObject.prototype.fullName = function () {
        return this.name;
    };

    /** Add itself to the given parent element.
    * @remarks
    * Normally not overridden.
    */
    MetadataObject.prototype.serialize = function (parent) {
        if (typeof parent === "undefined") { parent = null; }
        var element = parent ? et.SubElement(parent, this.elementName) : et.Element(this.elementName);
        this._serialize(element);

        if (this.remarks) {
            et.SubElement(element, "remarks").text = this.remarks;
        }
        return element;
    };

    /** Serialize itself to the given target element.
    * @remarks
    * Normally not overridden.
    */
    MetadataObject.prototype._serialize = function (target) {
        this._serializeAttributes(target);
        this._serializeContent(target);
    };

    /** Add attributes to the target element.
    * @remarks
    * Normally overridden in derived classes.
    */
    MetadataObject.prototype._serializeAttributes = function (target) {
        if (this.name) {
            target.set("name", this.name);
        }
    };

    /** Add subelements and/or text to the target element.
    * @remarks
    * Normally overridden in derived classes.
    */
    MetadataObject.prototype._serializeContent = function (target) {
        if (this.summary) {
            et.SubElement(target, "summary").text = this.summary;
        }
        this.examples.forEach(function (ex) {
            return et.SubElement(target, "example").text = ex;
        });
    };

    MetadataObject.prototype.serializeToFile = function (filename) {
        var tree = new et.ElementTree(this.serialize());
        var xmlText = tree.write({ indent: 2, lexicalOrder: false });
        fs.writeFileSync(filename, xmlText, "utf-8");
    };

    MetadataObject.prototype.serializeChildren = function (children, parent) {
        children.forEach(function (c) {
            return c.serialize(parent);
        });
    };
    return MetadataObject;
})();
exports.MetadataObject = MetadataObject;

var Module = (function (_super) {
    __extends(Module, _super);
    function Module() {
        _super.apply(this, arguments);
        this.nodeType = 1 /* Module */;
        this.members = [];
        this.elementName = "module";
    }
    Module.prototype._serializeContent = function (target) {
        _super.prototype._serializeContent.call(this, target);
        this.serializeChildren(this.members, target);
    };
    return Module;
})(MetadataObject);
exports.Module = Module;

var Dependencies = (function (_super) {
    __extends(Dependencies, _super);
    function Dependencies() {
        _super.apply(this, arguments);
        this.nodeType = 15 /* Dependencies */;
        this.members = [];
        this.elementName = "dependencies";
    }
    Dependencies.prototype._serializeContent = function (target) {
        _super.prototype._serializeContent.call(this, target);
        this.serializeChildren(this.members, target);
    };
    return Dependencies;
})(MetadataObject);
exports.Dependencies = Dependencies;

var Dependency = (function (_super) {
    __extends(Dependency, _super);
    function Dependency() {
        _super.apply(this, arguments);
        this.nodeType = 14 /* Dependency */;
        this.elementName = "dependency";
    }
    return Dependency;
})(MetadataObject);
exports.Dependency = Dependency;

var Unit = (function (_super) {
    __extends(Unit, _super);
    function Unit() {
        _super.apply(this, arguments);
        this.nodeType = 2 /* Unit */;
        this.elementName = "unit";
    }
    return Unit;
})(Module);
exports.Unit = Unit;

/** Member of a module */
var Member = (function (_super) {
    __extends(Member, _super);
    function Member() {
        _super.apply(this, arguments);
        this.isStatic = false;
    }
    Member.prototype._serializeAttributes = function (target) {
        _super.prototype._serializeAttributes.call(this, target);
        if (this.isStatic) {
            target.set("static", true);
        }
    };
    return Member;
})(MetadataObject);
exports.Member = Member;
var Parameter = (function (_super) {
    __extends(Parameter, _super);
    function Parameter(name, type, summary) {
        _super.call(this, name, summary);
        this.type = type;
        this.nodeType = 8 /* Parameter */;
        this.elementName = "param";
        this.isOptional = false;
    }
    Parameter.prototype._serializeAttributes = function (target) {
        _super.prototype._serializeAttributes.call(this, target);
        if (this.type) {
            target.set("type", this.type);
        }
        if (this.isOptional || this.defaultValue) {
            target.set("optional", true);
        }
        if (this.defaultValue) {
            target.set("default", this.defaultValue.toString());
        }
    };
    Parameter.prototype._serializeContent = function (target) {
        target.text = this.summary;
        target.getchildren().length = 0;
    };
    return Parameter;
})(MetadataObject);
exports.Parameter = Parameter;

var EnumMember = (function (_super) {
    __extends(EnumMember, _super);
    function EnumMember() {
        _super.apply(this, arguments);
        this.nodeType = 5 /* EnumMember */;
        this.elementName = "member";
        this.value = null;
    }
    EnumMember.prototype._serializeAttributes = function (target) {
        _super.prototype._serializeAttributes.call(this, target);
        if (this.value != null) {
            target.set("value", this.value);
        }
    };
    return EnumMember;
})(MetadataObject);
exports.EnumMember = EnumMember;

var Enum = (function (_super) {
    __extends(Enum, _super);
    function Enum() {
        _super.apply(this, arguments);
        this.nodeType = 6 /* Enum */;
        this.elementName = "enum";
        this.enumMembers = [];
    }
    Enum.prototype._serializeContent = function (target) {
        _super.prototype._serializeContent.call(this, target);
        this.serializeChildren(this.enumMembers, target);
    };
    return Enum;
})(MetadataObject);
exports.Enum = Enum;

var Func = (function (_super) {
    __extends(Func, _super);
    function Func() {
        _super.apply(this, arguments);
        this.nodeType = 7 /* Func */;
        this.elementName = "function";
        this.parameters = [];
    }
    Func.prototype._serializeContent = function (target) {
        _super.prototype._serializeContent.call(this, target);
        this.serializeChildren(this.parameters, target);
        if (this.returns) {
            this.returns._serialize(et.SubElement(target, "returns"));
        }
    };
    return Func;
})(Member);
exports.Func = Func;

var DefaultValue = (function () {
    function DefaultValue(value) {
        this.value = value;
    }
    DefaultValue.prototype.toString = function () {
        var result = JSON.stringify(this.value);
        if (result.match(/^"[^']+"$/)) {
            result = "'" + result.substr(1, result.length - 2) + "'";
        }
        return result;
    };
    return DefaultValue;
})();
exports.DefaultValue = DefaultValue;

var Property = (function (_super) {
    __extends(Property, _super);
    function Property(name, isObservable) {
        if (typeof isObservable === "undefined") { isObservable = false; }
        _super.call(this, name);
        this.isObservable = isObservable;
        this.nodeType = 9 /* Property */;
        this.elementName = "property";
    }
    Property.prototype._serializeAttributes = function (target) {
        _super.prototype._serializeAttributes.call(this, target);
        if (this.isObservable) {
            target.set("observable", true);
        }
        if (this.type) {
            target.set("type", this.type);
        }
        if (this.defaultValue) {
            target.set("default", this.defaultValue.toString());
        }
    };
    return Property;
})(Member);
exports.Property = Property;

var ClassOrInterface = (function (_super) {
    __extends(ClassOrInterface, _super);
    function ClassOrInterface() {
        _super.apply(this, arguments);
        this.methods = [];
        this.properties = [];
        this.interfaces = [];
        this.interfaceElementName = "implements";
    }
    ClassOrInterface.prototype._serializeContent = function (target) {
        var _this = this;
        _super.prototype._serializeContent.call(this, target);
        this.interfaces.forEach(function (iface) {
            return et.SubElement(target, _this.interfaceElementName).text = iface;
        });
        this.serializeChildren(this.methods, target);
        this.serializeChildren(this.properties, target);
    };
    return ClassOrInterface;
})(MetadataObject);
exports.ClassOrInterface = ClassOrInterface;

var Class = (function (_super) {
    __extends(Class, _super);
    function Class() {
        _super.apply(this, arguments);
        this.nodeType = 3 /* Class */;
        this.elementName = "class";
    }
    Class.prototype._serializeAttributes = function (target) {
        _super.prototype._serializeAttributes.call(this, target);
        if (this.base) {
            target.set("base", this.base);
        }
    };
    return Class;
})(ClassOrInterface);
exports.Class = Class;

var Interface = (function (_super) {
    __extends(Interface, _super);
    function Interface() {
        _super.apply(this, arguments);
        this.nodeType = 4 /* Interface */;
        this.elementName = "interface";
        this.interfaceElementName = "extends";
    }
    return Interface;
})(ClassOrInterface);
exports.Interface = Interface;

var Option = (function (_super) {
    __extends(Option, _super);
    function Option() {
        _super.apply(this, arguments);
        this.nodeType = 11 /* Option */;
        this.elementName = "option";
        this.parameters = [];
    }
    Option.prototype._serializeContent = function (target) {
        _super.prototype._serializeContent.call(this, target);
        if (this.parameters && this.parameters.length) {
            this.serializeChildren(this.parameters, target);
        }
    };
    return Option;
})(Property);
exports.Option = Option;
var Event = (function (_super) {
    __extends(Event, _super);
    function Event() {
        _super.apply(this, arguments);
        this.nodeType = 12 /* Event */;
        this.elementName = "event";
        this.parameters = [];
    }
    Event.prototype._serializeContent = function (target) {
        _super.prototype._serializeContent.call(this, target);
        this.serializeChildren(this.parameters, target);
    };
    return Event;
})(Member);
exports.Event = Event;
var Widget = (function (_super) {
    __extends(Widget, _super);
    function Widget() {
        _super.apply(this, arguments);
        this.nodeType = 10 /* Widget */;
        this.options = [];
        this.events = [];
        this.elementName = "widget";
    }
    Widget.prototype._serializeContent = function (target) {
        _super.prototype._serializeContent.call(this, target);
        this.serializeChildren(this.options, target);
        this.serializeChildren(this.events, target);
    };
    return Widget;
})(Class);
exports.Widget = Widget;
