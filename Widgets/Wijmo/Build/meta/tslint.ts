/// <reference path="TypeScript/TypeScript.d.ts" />
/// <reference path="../../External/declarations/node.d.ts" />

import path = require("path");
import fs = require("fs");
import util = require("./../util");
import tsutil = require("./tsutil");
var tsc = require("./tsc.js"),
    TypeScript = tsc.TypeScript;

export class Linter {
    
    checkScript(script: TypeScript.Script) {
        var modules: TypeScript.ModuleDeclaration[] = [];
        var path = new TypeScript.AstPath();

        function reportError(node: TypeScript.AST, message: string) {
            var anyNode: any = node;
            var locationText = script.locationInfo.filename;
            var lineCol = TypeScript.getLineColumnFromPosition(script, node.minChar);
            locationText += " (" + lineCol.line + ", " + lineCol.col + ")";
            console.log(locationText + ":\t" + message);
        }

        function isTypeExpr() {
            for (var i = path.top; i > 0; i--) {
                var parent: any = path.get(i - 1);
                if (parent.typeExpr && parent.typeExpr === path.get(i)) {
                    return true;
                }
            }

            return false;
        }

        var vars = [{}];

        function pre(ast: TypeScript.AST, parent: TypeScript.AST) {
            path.push(ast);

            switch (ast.nodeType) {
                //case ts.NodeType.ModuleDeclaration:
                //    modules.push(ast);
                //    break;

                case TypeScript.NodeType.FuncDecl:
                case TypeScript.NodeType.ModuleDeclaration:
                    var varHash = Object.create(vars[vars.length - 1]);
                    var node: any = ast;
                    if (node.arguments) {
                        node.arguments.members.forEach((a: TypeScript.ArgDecl) => varHash[a.id.text] = a);
                    }
                    if (node.vars) {
                        node.vars.members.forEach((v: TypeScript.VarDecl) => varHash[v.id.text] = v);
                    }
                    vars.push(varHash);
                    break;

                case TypeScript.NodeType.Name:
                    var id = <TypeScript.Identifier> ast;
                    if (id.sym && id.sym.declModule && parent.nodeType != TypeScript.NodeType.Dot && parent.isExpression()) {
                        if (parent.isDeclaration() && ((<any>parent).name === id || (<any>parent).id === id)) {
                            return ast;
                        }
                        if ((id.sym.declAST.nodeType === TypeScript.NodeType.VarDecl || id.sym.declAST.nodeType === TypeScript.NodeType.FuncDecl)
                                && path.asts.indexOf(id.sym.declModule) >= 0) {
                            return ast;
                        }
                        var overridingVar = vars[vars.length - 1][id.sym.declModule.prettyName];
                        if (overridingVar) {
                            reportError(parent, "Containing module may be overriden by a variable: " + id.text);
                            reportError(overridingVar || ast, "Variable overriding a module: " + overridingVar.id.text);
                        }

                    }
                    break;

                //case ts.NodeType.VarDecl:
                //    if (!path.isChildOfClass() && !path.isChildOfInterface()) {
                //        checkVarModuleClash(ast);
                //    }
                //    break;

                //case ts.NodeType.ArgDecl: {
                //    if (parent && parent.nodeType == ts.NodeType.FuncDecl && !parent.isSignature()) {
                //        checkVarModuleClash(ast);
                //    }
                //    break;
                //}
            }

            return ast;
        }
        function post(node: TypeScript.AST, parent: TypeScript.AST, walker: TypeScript.IAstWalker) {
            if (node.nodeType === TypeScript.NodeType.FuncDecl || node.nodeType === TypeScript.NodeType.ModuleDeclaration) {
                vars.pop();
            }
            path.pop();
            return node;
        }

        TypeScript.getAstWalkerFactory().walk(script.bod, pre, post, null);
    }

    check(files: string[]) {
        var scripts = tsutil.StandaloneTypeCheck.parse(files);
        //var visitor = new LinterVisitor();
        scripts.forEach((s: TypeScript.Script) => {
            this.checkScript(s);
        });
    }
}

function main() {
    try {
        if (process.argv.length < 2) {
            throw new Error("File not specified.\nUsage: node metagen.js <filename>");
        }

        var filename = process.argv[2];
        var linter = new Linter();
        linter.check([filename]);
        return 0;
    } catch (err) {
        console.log(err);
        if (err.stack) {
            console.log(err.stack);
        }
        return 1;
    }
}

if (require.main === module) {
    process.exit(main());
}