/// <reference path="TypeScript/TypeScript.d.ts" />
/// <reference path="../../External/declarations/node.d.ts" />

import path = require("path");
import fs = require("fs");
import util = require("./../util");
import metagen = require("./metagen");
var tsc = require("./tsc.js"),
    TypeScript = tsc.TypeScript;

export class StandaloneTypeCheck {
    batchCompiler: TypeScript.BatchCompiler;
    compiledCount: number;

    constructor() {
        TypeScript.IO.arguments = [];
        this.batchCompiler = new TypeScript.BatchCompiler(TypeScript.IO);

        var stdlib = path.join(__dirname, "lib.d.ts");
        this.batchCompiler.inputFiles.push(stdlib);
    }
    addUnit(path: string) {
        this.batchCompiler.inputFiles.push(path);
    }
    typeCheck() {
        this.batchCompiler.batchCompile();
        var tsc = new TypeScript.TypeScriptCompiler(new TypeScript.NullLogger(), this.batchCompiler.compilationSettings);

        this.batchCompiler.resolvedFiles.map(c => {
            var sf = this.batchCompiler.getSourceFile(c.path);
            tsc.addFile(c.path, sf.scriptSnapshot, sf.byteOrderMark, 0, false, c.referencedFiles);
        });

        tsc.resolveAllFiles();
        var docs = tsc.semanticInfoChain.documents;

        if (this.batchCompiler.hasErrors)
            return null;
        while (docs.length > 0 && (docs[0].fileName === "" || docs[0].fileName.indexOf("lib.d.ts") > -1)) {
            docs.shift();
        }

        return tsc.semanticInfoChain;
    }

    static parse(files: string[]) {
        var typeChecker = new StandaloneTypeCheck();
        files.forEach(f => typeChecker.addUnit(f));
        return typeChecker.typeCheck();
    }
}