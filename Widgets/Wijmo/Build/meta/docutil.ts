/// <reference path="../../External/declarations/node.d.ts" />

/** JSDoc utilities */

import path = require("path");
import fs = require("fs");
import util = require("./../util")

/** XML tools */
export module xml {
    var escapes = {
        "&": "&amp;",
        ">": "&gt;",
        "<": "&lt;"
    };
    /** Returns XML-encoded string */
    export function escape(text: string) {
        for (var e in escapes) {
            text = text.replace(new RegExp(e, "g"), escapes[e]);
        }
        return text;
    }
}

/** A part of JSDOC string starting */
export interface JsDocEntry {
    /** A word after @ sign */
    tag: string;
    text: string;
}
/** A parsed jsdoc comment */
export class JsDoc {
    /** Untagged part of jsdoc that goes first */
    title: string;
    /** Tags and their contents */
    entries: JsDocEntry[] = [];

    /** Add a tag */
    push(tag: string, text: string = "") {
        this.entries.push({ tag: tag, text: text });
    }
    /** Add a @param tag */
    pushParam(name: string, type?: string, description?: string) {
        var text = "";
        if (type) {
            text += "{" + type + "} ";
        }
        text += name;
        if (description) {
            text += " " + description;
        }
        this.push("param", text);
    }
    /** Add a @return tag */
    pushReturns(type: string, description?: string) {
        var text = "";
        if (type) {
            text += "{" + type + "} ";
        }
        if (description) {
            text += description;
        }
        text = text.trim();
        if (text) {
            this.push("returns", text);
        }
    }
    /** Sort tags to match conventions */
    sort() {
        function priority(entry: JsDocEntry) {
            switch (entry.tag) {
                case "example":
                    return 2;
                case "remarks":
                    return 1;
                default:
                    return 0;
            }
        }
        this.entries.sort((a, b) => {
            return priority(a) - priority(b);
        });
    }

    /** Find a tag by name and returns its index */
    indexOf(tag: string) {
        for (var i = 0; i < this.entries.length; i++) {
            if (this.entries[i].tag === tag) {
                return i;
            }
        }
    }
    /** Find a tag by name */
    find(tag: string) {
        var index = this.indexOf(tag);
        return index >= 0 ? this.entries[index] : null;
    }
    /** Find all tags with a given name */
    findAll(tag: string) {
        return this.entries.filter(e => e.tag === tag);
    }
    /** Invoke iter function for all tags with a given name and text matching a given regex */
    match(tag: string, rgx: RegExp, iter: (m: RegExpExecArray, ...groups: string[]) => any) {
        this.findAll(tag).forEach(e => {
            var m = rgx.exec(e.text);
            if (!m) {
                console.error("Could not parse @" + tag + ": " + e.text);
                return;
            }

            var args = [m].concat(m);
            args.splice(1, 1);
            iter.apply(this, args);
        });
    }

    lineBreaks = {
        param: false,
        remarks: true,
        example: true
    };
    /** Returns JSDoc string */
    toString() {
        var text = "";
        if (this.title) {
            text = this.title + "\n";
        }
        this.entries.forEach(e => {
            text += "@" + e.tag;
            var entryText = trimLines(e.text, e.tag === "example");
            var breakLine = this.lineBreaks[e.tag];
            if (breakLine === undefined) {
                breakLine = entryText.match(/\n/);
            }
            text += breakLine ? "\n" : " ";
            text += entryText + "\n";
        });
        return text.trim();
    }
    /** Returns JSDoc string as a comment */
    toStringWithComment() {
        var text = this.toString();
        text = "/** " + text;
        if (text.match(/\n/)) {
            text = text.replace(/\n+/g, "\n  * ") + "\n ";
        }
        text += " */\n";
        return text;
    }

    /** Extract comment body */
    static removeComment(text: string) {
        return text
            .replace(/^\/\*{2}/, "")
            .replace(/\*\/\s*$/m, "")
            .replace(/^\s*\*\s?/gm, "");
    }
    /** Parse a jsdoc comment string */
    static parse(text: string) {
        text = this.removeComment(text).trim();

        var doc = new JsDoc(),
            rgxTag = /^@(\w+)\s?/mg,
            start = 0,
            prevTag = "";

        do {
            var m = rgxTag.exec(text);
            var end = m ? m.index : text.length;
            var value = text.substring(start, end).trim();
            if (prevTag) {
                doc.entries.push({ tag: prevTag, text: value });
            } else if (!m || m.index > 0) {
                doc.title = value;
            }

            if (m) {
                prevTag = m[1];
                start = m.index + m[0].length;
            }
        } while (m);

        return doc;
    }
}

/** Indent all lines in the text */
export function indent(text: string, level) {
    if (typeof level === "number") {
        level = new Array(level + 1).join(" ");
    }
    return level + text.replace(/(\n[\s\S])/mg, "$1" + level);
}

/** Remove indentation from all lines in the text
  * @param sameIndent {boolean} Remove equal amount of whitespace from each line 
  */
export function trimLines(text: string, sameIndent = false): string {
    if (!sameIndent) {
        return text
            .trim()
            .replace(/\n[ \t]+/g, "\n");
    }


    text = text.replace(/\t/g, "    ");
    var lines = text.split(/\n/);
    var minIndent = null;
    lines.forEach(l => {
        var m = /^ */.exec(l);
        if (m) {
            var indent = m[0].length;
            minIndent = minIndent === null ? indent : Math.min(minIndent, indent);
        }
    });
    if (minIndent === null) {
        return text;
    }

    for (var i = 0; i < lines.length; i++) {
        lines[i] = lines[i].substr(minIndent);
    }
    return lines.join("\n");
}

/** Remove trailing whitespace */
export function trimLineBreaks(text: string) {
    var prevText;
    do {
        prevText = text;
        text = text.replace(/^\s*\n+/, "");
    } while (prevText !== text);

    do {
        prevText = text;
        text = text.replace(/\n+\s*$/, "");
    } while (prevText !== text);
    return text;
}

/** Convert a string to camelCase */
export function camelCase(text: string) {
    text = text.replace(/\s\w/g, m => {
        return m[m.length - 1].toUpperCase();
    });
    text = text.charAt(0).toLowerCase() + text.substr(1);
    return text;
}

/** Insert \r before each \n */
export function crlf(text: string) {
    return text.replace(/\n/g, "\r\n");
}

/** Replace \n\r or \r\n with \n */
export function lf(text: string) {
    return text.replace(/(\r\n?|\n\r)/g, "\n");
}