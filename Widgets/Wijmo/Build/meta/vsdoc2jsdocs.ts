/// <reference path="../../External/declarations/node.d.ts" />

import path = require("path");
import fs = require("fs");
import util = require("./../util")
import docutil = require("./docutil")
var et = require("elementtree"),
    tabSubstitute = "    ";

module wijmo.docs {
    "use strict";

    function cleanTabs(text: string) {
        return text.replace(/\t/g, tabSubstitute);
    }

    var knownTags = ["Default", "Type", "Code Example"];

    function parseSummary(summary: string) {
        var splitRegex = /^\s*(Default|Type|Code Example):/gmi;

        var doc = new docutil.JsDoc();

        var prevMatch: RegExpExecArray = null;
        do {
            var match = splitRegex.exec(summary);
            var start = prevMatch ? prevMatch.index + prevMatch[0].length : 0;
            var end = match ? match.index : summary.length;
            var value = docutil.trimLineBreaks(summary.substring(start, end));
            var isMultiline = value.indexOf("\n") >= 0;
            var tag = prevMatch && docutil.camelCase(prevMatch[1]);
            if (tag !== "codeExample") {
                value = docutil.trimLines(value);
            } else {
                tag = "example";
                value = docutil.trimLines(value, true);
            }

            if (tag) {
                doc.entries.push({ tag: tag, text: value });
            } else {
                doc.title = value;
            }

            prevMatch = match;
        } while (prevMatch);

        return doc;
    }

    function vsdoc2jsdoc(code: string) {
        var result = {
            errors: [],
            code: ""
        };
        result.code = code.replace(/^(([ \t]*\/{3}\s*<.+\r?\n)(\s*\/{3}.*\r?\n)*)/mg, function (xmlComment) {
            var indentation = xmlComment.match(/^[ \t]*/)[0],
                xml = xmlComment.replace(/^\s*\/{2,}/mg, "").trim();

            if (xml.match(/^<reference\s+path/)) {
                return xmlComment;
            }

            var tree;
            try {
                tree = et.parse("<root>" + xml + "</root>");
            } catch(err) {
                result.errors.push({
                    message: "Could not parse as XML",
                    comment: xmlComment,
                    innerError: err
                });
                return xmlComment;
            }

            var root = tree.getroot();
            var summary = root.find("summary");
            var jsdoc = summary ? parseSummary(summary.text) : new docutil.JsDoc();

            root.getchildren().forEach(node => {
                if (node.tag === "summary") return;
                var text = docutil.trimLines(node.text);

                if (node.tag === "param") {
                    text = node.attrib.name + " " + text;
                    if (node.attrib.type) {
                        text = "{" + node.attrib.type + "} " + text;
                    }
                }

                jsdoc.entries.push({ tag: node.tag, text: text });
            });

            return docutil.indent(jsdoc.toStringWithComment(), cleanTabs(indentation));
        });
        return result;
    }
    function moveJsDocUp(code: string) {
        return code.replace(/^([\t ]*)(\w+\([^\)]*\)\s*{\s*)\n(\s*\/\*{2}.[\s\S]+?\*\/)/gm, (m, indentation, signature, jsdoc) => {
            indentation = cleanTabs(indentation);
            jsdoc = cleanTabs(jsdoc);
            jsdoc = docutil.trimLines(jsdoc, true);
            jsdoc = docutil.indent(jsdoc, indentation.replace(/\n/g, ""));
            return jsdoc + "\n" + indentation + signature;
        });
    }
    export function convertCode(code: string) {
        code = docutil.lf(code);
        var result = vsdoc2jsdoc(code);
        if (result.errors.length) {
            return result;
        }
        result.code = moveJsDocUp(result.code);
        result.code = docutil.crlf(result.code);
        return result;
    }
}

exports.convertCode = wijmo.docs.convertCode;

if (process.argv.length < 2) {
    console.log("File not specified. Usage:");
    console.log("node vsdoc2jsdocs.js <filename>");
    process.exit(1);
}

var filename = process.argv[2];
if (!util.isWritableSync(filename)) {
    console.log("File is read-only");
    process.exit(2);
}
var code = util.readFile(filename);
var result = wijmo.docs.convertCode(code);
if (result.errors.length) {
    result.errors.forEach(err => {
        console.log(err.message);
        console.log(err.comment);
        console.log(err.innerError);
        console.log("------");
    });
} else {
    fs.writeFileSync(filename, result.code, "utf-8");
}