/// <reference path="../../External/declarations/node.d.ts" />
/// <reference path="../../External/declarations/elementtree.d.ts" />

/** This script contains classes for generating TypeScript code out of .d.ts definition files.
  */

import path = require("path");
import fs = require("fs");
import util = require("./../util")
import et = require("elementtree");
import docutil = require("./../meta/docutil");

var emptyFn = "function () {}";
var xmlEscape = docutil.xml.escape;

/** Metadata-to-TypeScript definition converter */
export class Generator {
    private _out: util.CodeWriter;
    interfaceExtensions: {
        name: string;
        extendList: string[];
    }[] = [];

    //#region export path

    /** A stack of variable/field names representing a property path which is used to export names */
    exportNames: string[] = [];
    /** Concatenated exportNames */
    exportPath: string;

    /** Run a function with a name exported
      * @param append if True, the name is appended to the existing names (pushed onto the exportNames stack). Defaults to True.
      */
    exported(name: string, fn: () => any, append = true) {
        if (append && this.exportPath) {
            name = this.exportPath + "." + name;
        }
        this.exportNames.push(name);
        var oldPath = this.exportPath;
        this.exportPath = name;
        try {
            fn.call(this);
        } finally {
            this.exportPath = oldPath;
            this.exportNames.pop();
        }
    }

    /** Export a name and its value */
    writeExport(name, value) {
        if (this.exportPath) {
            this._out.write(this.exportPath + ".");
        }
        this._out.writeLine("{0} = {1};", name, value);
    }

    /** Write jsdoc string */
    writeDoc(doc: docutil.JsDoc) {
        doc.sort();
        var text = doc.toStringWithComment()
            .replace(/(^|[^\r])\n/g, "$1\r\n");
        this._out.write(text);
    }
    //#endregion

    /** Convert an XML meta node to a JSDoc object */
    readDoc(elem: et.IElement) {
        var jsdoc = new docutil.JsDoc();

        var summary = elem.find("summary");
        if (summary) {
            jsdoc.title = xmlEscape(summary.text);
        }

        var remarks = elem.find("remarks");
        if (remarks) {
            jsdoc.push("remarks", xmlEscape(remarks.text));
        }

        elem.findall("example").forEach(ex => jsdoc.push("example", xmlEscape(ex.text)));
        
        return jsdoc;
    }

    defaultTypes = {
        "bool": "boolean",
        "boolean": "boolean",
        "array": "any[]",
        "[]": "any[]",
        "object[]": "any[]",
        "date": "Date",
        "datetime": "Date",
        "function": "Function",
        "object": "any",
        "obj": "any",
        "raphael element": "RaphaelElement",
        "raphaelelement": "RaphaelElement",
        "raphael": "RaphaelPaper",
        "eventobj": "Event",
        "event": "Event",
        "e": "Event",
        "any": "any",
        "eventtarget": "EventTarget",
        "jquery": "JQuery",
        "domelement": "HTMLElement",
        "jquerypromise": "JQueryPromise<any>"
    };

    readType(elem: et.IElement) {
        var type: string = elem.get("type") || "";
        //Convert type to TypeScript type
        type = type.replace(/\?|\{|\}/, "").toLowerCase();
        if (type in this.defaultTypes) {
            type = this.defaultTypes[type];
        } else {
            type = "any";
        }
        return type;
    }
    readReturns(elem: et.IElement, key: string) {
        var returns = elem.find(key),
            type = "any";
        if (returns) {
            type = this.readType(returns);
        }
        if (type === "any") {
            return " ";
        }
        else {
            return ": " + type;
        }
    }

    visitModule(mod: et.IElement) {
        var name = mod.get("name");
        //this._out.writeLine("declare module wijmo.{0} {", name);
        //this._out.writeLine("}");
        this.exported(name, () => this.visitChildren(mod));
        //this.interfaceExtensions.forEach(e => e.extendList.forEach(base => {
        //    var exists = "typeof " + base + " != 'undefined'";
        //    var doExtend = "$.extend(" + e.name + ", " + base + ".prototype)";
        //    this._out.writeLine(exists + " && " + doExtend + ";");
        //}));
        this.interfaceExtensions = [];
        //var name = mod.get("name");
        //if (this.exportPath) {
        //    this._out.writeLine("{0} = {0} || {};", this.exportPath + "." + name);
        //} else {
        //    this._out.writeLine("var {0} = {0} || {};", name);
        //}
        //this._out.writeLine();
        //this._out.writeLine("(function () {");
        //this.exported(name, () => this.visitChildren(mod));
        //this.interfaceExtensions.forEach(e => e.extendList.forEach(base => {
        //    var exists = "typeof " + base + " != 'undefined'";
        //    var doExtend = "$.extend(" + e.name + ", " + base + ".prototype)";
        //    this._out.writeLine(exists + " && " + doExtend + ";");
        //}));
        //this.interfaceExtensions = [];
        //this._out.writeLine("})()");
    }

    insideWidget = false;
    widgetName = "";

    writejQueryInterfaceStart() {
        this._out.writeLine("/** Definitions of {0} widget */", this.widgetName);
        this._out.writeLine("interface JQuery {");
        this._out._indentString = this._out._indentStringFor(1);
        this._out.writeLine();
        //this._out._indentLevel++;
    }
    writejQueryInterfaceEnd() {
        //this._out._indentLevel--;
        this._out._indentString = "";
        this._out.writeLine();
        this._out.writeLine("}");
        this._out.writeLine();
    }
    writeGenericWidgetFunction(name) {
        //Initialize
        this._out.writeLine("/** Initialize a {0} widget */", name);
        this._out.writeLine('{0}(): JQuery;', name);
        this._out.writeLine("/** Initialize a {0} widget with the given options */", name);
        this._out.writeLine('{0}(options: Object): JQuery;', name);
        this._out.writeLine('{0}(methodName: string, ...otherParams: any[]): any;', name);
        //Generic get option
        this._out.writeLine("/** Get specified option of {0} widget */", name);
        this._out.writeLine('{0}(methodName: "option", optionName: string): any;', name);
        //Generic set option
        this._out.writeLine("/** Set options to {0} widget */", name);
        this._out.writeLine('{0}(methodName: "option", optionValues: Object): any;', name);
        this._out.writeLine("/** Set specified option to {0} widget */", name);
        this._out.writeLine('{0}(methodName: "option", optionName: string, optionValue: any): JQuery;', name);
        this._out.writeLine();
    }

    visitWidgetDefinition(widget: et.IElement) {
        var name = widget.get("name");
        this.widgetName = name;
        try {
        this.writejQueryInterfaceStart();

        //var doc = this.readDoc(widget);
        //this.writeDoc(doc);

        this.writeGenericWidgetFunction(name);
        this.visitWidgetChildren(widget);

        } finally {
        this.writejQueryInterfaceEnd();
        }
    }
    visitWidgetParamsTo(target: docutil.JsDoc, container: et.IElement) {
        container.findall("param").forEach(p => target.pushParam(p.get("name"), p.get("type"), xmlEscape(p.text)));
        var returns = container.find("returns");
        if (returns) {
            target.pushReturns(returns.get("type"), xmlEscape(returns.text));
        }
    }
    visitWidgetFunction(func: et.IElement) {
        var name = func.get("name"),
            doc = this.readDoc(func);
        this.visitWidgetParamsTo(doc, func);
        this.writeDoc(doc);
        var params = func.findall("param").map(param => {
            var str, n = param.get("name"),
                t = this.readType(param),
                optional = !!param.get("optional");
            str = n;
            if (optional) {
                str += "?";
            }
            if (t) {
                str += ": " + t;
            }
            return str;
        }).join(", ");
        if (params && params.length) {
            this._out.writeLine('{0}(methodName: "{1}", {2}){3};', this.widgetName, name, params, this.readReturns(func, "returns"));
        } else {
            this._out.writeLine('{0}(methodName: "{1}"){2};', this.widgetName, name, this.readReturns(func, "returns"));
        }
    }
    visitWidgetEvent(event: et.IElement) {
        var name = event.get("name");
        //get event
        this._out.writeLine("/** Get {0} option(event) of {1} widget */", name, this.widgetName);
        this._out.writeLine('{0}(methodName: "option", eventName: "{1}"): Function;', this.widgetName, name);
        //set event

        var params = event.findall("param").map(param => {
            var str, n = param.get("name"),
                t = this.readType(param);
            str = n;
            str += "?";
            //if (t) {
            //    str += ": " + t;
            //}
            return str;
        }).join(", ");
        this._out.writeLine("/** Set {0} option(event) to {1} widget */", name, this.widgetName);
        this._out.writeLine('{0}(methodName: "option", eventName: "{1}", {1}EventValue: ({2}) => void): JQuery;', this.widgetName, name, params);
    }
    visitWidgetOption(option: et.IElement) {
        var name: string = option.get("name"),
            type: string = this.readReturns(option, "type");

        //get option
        this._out.writeLine("/** Get {0} option of {1} widget */", name, this.widgetName);
        this._out.writeLine('{0}(methodName: "option", optionName: "{1}"){2};', this.widgetName, name, type);
        //set option
        this._out.writeLine("/** Set {0} option to {1} widget */", name, this.widgetName);
        this._out.writeLine('{0}(methodName: "option", optionName: "{1}", {1}Value{2}): JQuery;', this.widgetName, name, type);
    }

    visitWidgetAll(elements: et.IElement[]) {
        elements.forEach(e => {
            this.visitWidgetChild(e);
        });
    }
    visitWidgetChildren(parent: et.IElement) {
        this.visitWidgetAll(parent.getchildren());
    }
    visitWidgetChild(xElement: et.IElement) {
        var method = "visitWidget" + xElement.tag.charAt(0).toUpperCase() + xElement.tag.substr(1);
        if (this[method]) {
            this[method](xElement);
            this._out.writeLine();
        } else {
            //throw new Error("Cannot process " + xElement.tag + " element");
        }
    }

    visitWidget(widget: et.IElement) {
        //var name = widget.get("name"),
        //    baseClass: string = widget.get("base") || "",
        //    baseWidget = !baseClass.match(/^wijmo\./)
        //    ? baseClass
        //    : "$.wijmo." + baseClass.substr(baseClass.lastIndexOf(".") + 1)
        //        .replace(/(wijmoWidget|JQueryUIWidget)/, "widget");

        //this.insideWidget = true;
        //try {
        //    var doc = this.readDoc(widget);
        //    doc.push("class", name);
        //    doc.push("widget");
        //    if (this.exportPath) {
        //        doc.push("namespace", "jQuery." + this.exportPath);
        //    }
        //    if (baseClass) {
        //        doc.push("extends", baseClass);
        //    }
        //    this.writeDoc(doc);
        //    this.writeExport(name, emptyFn);
        //    if (baseClass) {
        //        this.writeExport(name + ".prototype", "new " + baseClass + "()");
        //    }
        //    this.exported(name + ".prototype", () => this.visitAll(widget.findall("function")));

        //    // options
        //    this._out.writeLine();
        //    var doc = new docutil.JsDoc();
        //    doc.push("class");
        //    this.writeDoc(doc);
        //    var options = name + "_options";
        //    this._out.writeLine("var " + options + " = " + emptyFn + ";");
        //    this.exported(options + ".prototype", () => {
        //        this.visitAll(widget.findall("option"));
        //        this.visitAll(widget.findall("event"));
        //    }, false);
        //    var optionsObject = "$.extend({}, true, ";
        //    if (baseClass) {
        //        optionsObject += baseClass + ".prototype.options, ";
        //    }
        //    optionsObject += "new " + options + "())";
        //    this.writeExport(name + ".prototype.options", optionsObject);

        //    // register widget
        //    var widgetDef = "$.widget(\"wijmo." + name + "\"";
        //    if (baseWidget) {
        //        widgetDef += ", " + baseWidget;
        //    }
        //    widgetDef += ", " + this.exportPath + "." + name + ".prototype);";
        //    this._out.writeLine(widgetDef)

        //} finally {
        //    this.insideWidget = false;
        //}
    }

    visitClassOrInterface(type: et.IElement, isInterface) {
        var name = type.get("name"),
            base = type.get("base"),
            extendList: string[];

        if (isInterface) {
            extendList = type.findall("extends").map(e => e.text);
        } else if (base) {
            extendList = [type.get("base")];
        }

        var doc = this.readDoc(type);
        doc.push(isInterface ? "interface" : "class", name);
        if (this.exportPath) {
            doc.push("namespace", this.exportPath);
        }

        if (extendList) {
            extendList.forEach(base => doc.push("extends", base));
        }

        this.writeDoc(doc);
        this.writeExport(name, emptyFn);

        if (!isInterface && base) {
            this.writeExport(name + ".prototype", "new " + base + "()");
        }

        this.exported(name + ".prototype", () => {
            if (isInterface && extendList.length > 0) {
                this.interfaceExtensions.push({
                    name: this.exportPath,
                    extendList: extendList
                });
            }
            this.visitChildren(type);
        });
    }
    visitInterface(type: et.IElement) {
        this.visitClassOrInterface(type, true);
    }
    visitClass(type: et.IElement) {
        this.visitClassOrInterface(type, false);
    }

    visitParamsTo(target: docutil.JsDoc, container: et.IElement) {
        container.findall("param").forEach(p => target.pushParam(p.get("name"), p.get("type"), xmlEscape(p.text)));
        var returns = container.find("returns");
        if (returns) {
            target.pushReturns(returns.get("type"), xmlEscape(returns.text));
        }
    }
    visitFunction(func: et.IElement) {
        var name = func.get("name"),
            doc = this.readDoc(func);
        // If name is not defined, the function is the constructor.
        if (!name) {
            name = "constructor";
        }
        this.visitParamsTo(doc, func);
        this.writeDoc(doc);
        var paramNames = func.findall("param").map(p => p.get("name")).join(", ");
        var fnDef = "function (" + paramNames + ") {}";
        this.writeExport(name, fnDef);
    }
    visitOption(option: et.IElement) {
        this.visitProperty(option, d => d.push("option"));
    }
    resolveTypeUrl(type: string) {
        var trailingBrackets = type.match(/(\[\])+$/);
        if (trailingBrackets) {
            type = type.substr(0, type.length - trailingBrackets.length);
        }
        return "Wijmo~" + type + ".html";
    }
    visitProperty(prop: et.IElement, extendDoc?: (jsdoc: docutil.JsDoc) => void) {
        var name: string = prop.get("name"),
            type: string = prop.get("type"),
            defaultValue = prop.get("default"),
            doc = this.readDoc(prop),
            observable = (prop.get ("observable") || "false").match(/true/i),
            prefixHtml = "";

        if (/^wijmo\./.exec(type)) {
            // A workaround for DX bug: 
            // temporarily insert link to to type
            prefixHtml += "<p class='widgetType'>Type: <a href='" + this.resolveTypeUrl(type) + "'>" + type + "</a></p>\n";
        }

        if (defaultValue) {
            prefixHtml += "<p class='defaultValue'>Default value: " + docutil.xml.escape(defaultValue) + "</p>\n";
        }

        doc.title = prefixHtml + "<p>" + doc.title + "</p>";
        doc.push("field");
        if (type) {
            doc.push("type", "{" + type + "}");
        }
        //add params to option
        this.visitParamsTo(doc, prop);
        if (observable) {
            doc.push("accessor");
            doc.push("observable");
        }
        if (extendDoc) {
            extendDoc(doc);
        }
        this.writeDoc(doc);
        this.writeExport(name, defaultValue || "null");
    }

    visitEvent(event: et.IElement) {
        var name = event.get("name"),
            doc = this.readDoc(event);
        doc.push("event");
        this.visitParamsTo(doc, event);

        this.writeDoc(doc);

        this.writeExport(name, "null");
    }

    visit(xElement: et.IElement) {
        var method = "visit" + xElement.tag.charAt(0).toUpperCase() + xElement.tag.substr(1);
        if (this[method] && method === "visitWidget") {
            //this[method](xElement);
            this.visitWidgetDefinition(xElement);
        } else {
            //throw new Error("Cannot process " + xElement.tag + " element");
        }
        //var method = "visit" + xElement.tag.charAt(0).toUpperCase() + xElement.tag.substr(1);
        //if (this[method]) {
        //    this[method](xElement);
        //} else {
        //    //throw new Error("Cannot process " + xElement.tag + " element");
        //}
    }
    visitAll(elements: et.IElement[]) {
        elements.forEach(e => {
            this.visit(e);
        });
    }
    visitChildren(parent: et.IElement) {
        this.visitAll(parent.getchildren());
    }

    /** Read a .ts.meta file and generate JavaScript */
    generate(srcFile: string, out: util.CodeWriter, addRefDesc: boolean) {
        this._out = out;
        try {
            var xUnit = et.parse(fs.readFileSync(srcFile, "utf-8"));
            
            if (addRefDesc) {
                this._out.writeLine("/**");
                this._out.writeLine("* ");
                this._out.writeLine("* Please add references of jQuery.d.ts and Raphael.d.ts manually");
                this._out.writeLine("* ");
                this._out.writeLine("*/");
            }
            xUnit.getroot().findall("module").forEach(xModule => {
                var name = xModule.get("name");
                if (name === "wijmo") {
                    xModule.findall("module").forEach(mod => this.visitModule(mod));
                }
            });
        } finally {
            this._out = null;
        }
    }
}

function main() {
    try {
        if (process.argv.length < 2) {
            throw new Error("File not specified.\nUsage: node defgen.js <filename>");
        }
        var filename = process.argv[2];
        // create output as a file with postfix .d.ts
        var output = new util.FileCodeWriter(filename.replace(/.ts.meta/, ".d.ts"));
        try {
            // convert a .ts.meta file to .ts.meta.js
            new Generator().generate(filename, output, true);
        } finally {
            output.close();
        }
        return 0;
    } catch (err) {
        console.log(err);
        return 1;
    }
}

if (require.main === module ) {
    process.exit(main());
}
