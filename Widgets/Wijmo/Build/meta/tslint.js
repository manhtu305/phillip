/// <reference path="TypeScript/TypeScript.d.ts" />
/// <reference path="../../External/declarations/node.d.ts" />
var tsutil = require("./tsutil");
var tsc = require("./tsc.js"), TypeScript = tsc.TypeScript;

var Linter = (function () {
    function Linter() {
    }
    Linter.prototype.checkScript = function (script) {
        var modules = [];
        var path = new TypeScript.AstPath();

        function reportError(node, message) {
            var anyNode = node;
            var locationText = script.locationInfo.filename;
            var lineCol = TypeScript.getLineColumnFromPosition(script, node.minChar);
            locationText += " (" + lineCol.line + ", " + lineCol.col + ")";
            console.log(locationText + ":\t" + message);
        }

        function isTypeExpr() {
            for (var i = path.top; i > 0; i--) {
                var parent = path.get(i - 1);
                if (parent.typeExpr && parent.typeExpr === path.get(i)) {
                    return true;
                }
            }

            return false;
        }

        var vars = [{}];

        function pre(ast, parent) {
            path.push(ast);

            switch (ast.nodeType) {
                case TypeScript.NodeType.FuncDecl:
                case TypeScript.NodeType.ModuleDeclaration:
                    var varHash = Object.create(vars[vars.length - 1]);
                    var node = ast;
                    if (node.arguments) {
                        node.arguments.members.forEach(function (a) {
                            return varHash[a.id.text] = a;
                        });
                    }
                    if (node.vars) {
                        node.vars.members.forEach(function (v) {
                            return varHash[v.id.text] = v;
                        });
                    }
                    vars.push(varHash);
                    break;

                case TypeScript.NodeType.Name:
                    var id = ast;
                    if (id.sym && id.sym.declModule && parent.nodeType != TypeScript.NodeType.Dot && parent.isExpression()) {
                        if (parent.isDeclaration() && (parent.name === id || parent.id === id)) {
                            return ast;
                        }
                        if ((id.sym.declAST.nodeType === TypeScript.NodeType.VarDecl || id.sym.declAST.nodeType === TypeScript.NodeType.FuncDecl) && path.asts.indexOf(id.sym.declModule) >= 0) {
                            return ast;
                        }
                        var overridingVar = vars[vars.length - 1][id.sym.declModule.prettyName];
                        if (overridingVar) {
                            reportError(parent, "Containing module may be overriden by a variable: " + id.text);
                            reportError(overridingVar || ast, "Variable overriding a module: " + overridingVar.id.text);
                        }
                    }
                    break;
            }

            return ast;
        }
        function post(node, parent, walker) {
            if (node.nodeType === TypeScript.NodeType.FuncDecl || node.nodeType === TypeScript.NodeType.ModuleDeclaration) {
                vars.pop();
            }
            path.pop();
            return node;
        }

        TypeScript.getAstWalkerFactory().walk(script.bod, pre, post, null);
    };

    Linter.prototype.check = function (files) {
        var _this = this;
        var scripts = tsutil.StandaloneTypeCheck.parse(files);

        //var visitor = new LinterVisitor();
        scripts.forEach(function (s) {
            _this.checkScript(s);
        });
    };
    return Linter;
})();
exports.Linter = Linter;

function main() {
    try  {
        if (process.argv.length < 2) {
            throw new Error("File not specified.\nUsage: node metagen.js <filename>");
        }

        var filename = process.argv[2];
        var linter = new Linter();
        linter.check([filename]);
        return 0;
    } catch (err) {
        console.log(err);
        if (err.stack) {
            console.log(err.stack);
        }
        return 1;
    }
}

if (require.main === module) {
    process.exit(main());
}
