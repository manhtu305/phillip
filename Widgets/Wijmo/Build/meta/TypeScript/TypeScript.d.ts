// Type definitions for Typescript
// There's conficts between NodeJS definitions and TypeScript, so add this file to make TypeScript happy.

declare module TypeScript {
    interface SyntaxKind {
    }
    interface MemberFunctionDeclaration {
    }
    interface MemberVariableDeclaration {
        variableDeclarator?: any;
        equalsValueClause?: any;
    }
    interface PropertySignature extends AST {
        init?: any;
        typeExpr?: any;
        id?: any;
        isStatic? (): boolean;
        propertyName?: any;
        identifier?: any;
        typeAnnotation?: any;
        variableDeclarator?: any;
    }
    interface TypeAnnotation {
        type?: any;
    }
    interface MethodSignature extends AST {
        init?: any;
        typeExpr?: any;
        id?: any;
        isStatic? (): boolean;
        propertyName?: any;
    }
    interface AST {
        preComments?: any;
        nodeType?: any;
        minChar?: any;
        isDeclaration? (): boolean;
        isExpression? (): boolean;
        kind?();
        typeNames?: any;
    }
    interface FunctionDeclaration {
        arguments?: any;
        returnTypeAnnotation?: any;
        returnStatementsWithExpressions?: any;
        name?: any;
        isStatic? (): boolean;
        callSignature?: any;
        propertyName?: any;
        modifiers?: any;
        kind? ();
        block?: any;
        identifier?: any;
    }
    interface ArgDecl {
        id?: any;
        typeExpr?: any;
        isOptionalArg? (): boolean;
        init?: any;
        identifier?: any;
        typeAnnotation?: any;
        questionToken?: any;
        equalsValueClause?: any;
        dotDotDotToken?: any;
    }
    interface NodeType { }
    interface TypeReference extends AST {
        arrayCount?: any;
    }
    interface VariableDeclaration {
        init?: any;
        typeExpr?: any;
        id?: any;
        isStatic? (): boolean;
    }
    interface Identifier extends AST {
        sym?: any;
        text?: any;
    }
    interface BinaryExpression extends AST {
        type?: any;
        operand1?: any;
        operand2?: any;
    }
    interface ClassDeclaration {
        extendsList?: any;
        name?: any;
        implementsList?: any;
        body?: any;
        kind();
        identifier?: any;
        heritageClauses?: any;
    }
    interface TypeDeclaration {
        nodeType?: any;
        members?: any;
        body?: any;
        kind();
        classElements?: any;
    }
    interface InterfaceDeclaration {
        name?: any;
        extendsList?: any;
        identifier: any;
        kind();
        heritageClauses?: any;
    }
    interface EnumDeclaration {
        identifier: any;
        enumElements: any;
    }
    interface ModuleDeclaration {
        prettyName?: any;
        stringLiteral?: any;
        members?: any;
        moduleElements?: any;
        name?: any;
    }
    interface ASTList {
        members?: any;
        moduleElements?: any;
    }
    interface ISourceText { }
    interface IAstWalker { }
    interface Script {
        locationInfo?: any;
        bod?: any;
        fileName?: string;
        sourceUnit();
    }
    interface BatchCompiler {
        compilationEnvironment?: any;
        errorReporter?: any;
        compilationSettings?: any;
        resolve? (): any;
        getSourceFile(): any;
        inputFiles: any[];
        resolvedFiles: any[];
        batchCompile();
        getSourceFile(filePath: string);
        hasErrors: boolean;
    }
}
