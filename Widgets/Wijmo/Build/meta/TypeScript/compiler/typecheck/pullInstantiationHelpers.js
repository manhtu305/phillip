// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    var WrapsTypeParameterCache = (function () {
        function WrapsTypeParameterCache() {
            this._wrapsTypeParameterCache = TypeScript.BitVector.getBitVector(true);
        }
        // 0 indicates that it does not wrap type parameter
        // undefined indicates that the info wasnt available from cache
        // rest indicates the valid type parameter id
        WrapsTypeParameterCache.prototype.getWrapsTypeParameter = function (typeParameterArgumentMap) {
            // Find result from cache
            var mapHasTypeParameterNotCached = false;
            for (var typeParameterID in typeParameterArgumentMap) {
                if (typeParameterArgumentMap.hasOwnProperty(typeParameterID)) {
                    var cachedValue = this._wrapsTypeParameterCache.valueAt(typeParameterID);
                    if (cachedValue) {
                        // Cached value indicates that the type parameter is wrapped
                        return typeParameterID;
                    }
                    mapHasTypeParameterNotCached = mapHasTypeParameterNotCached || cachedValue === undefined;
                }
            }

            // If everything was cached, then this type doesnt wrap the type parameter
            if (!mapHasTypeParameterNotCached) {
                return 0;
            }

            return undefined;
        };

        WrapsTypeParameterCache.prototype.setWrapsTypeParameter = function (typeParameterArgumentMap, wrappingTypeParameterID) {
            if (wrappingTypeParameterID) {
                // wrappingTypeParameterID is the known type parameter that is wrapped
                // We dont know about other type parameters present in the map and hence we cant set their values
                this._wrapsTypeParameterCache.setValueAt(wrappingTypeParameterID, true);
            } else {
                for (var typeParameterID in typeParameterArgumentMap) {
                    if (typeParameterArgumentMap.hasOwnProperty(typeParameterID)) {
                        this._wrapsTypeParameterCache.setValueAt(typeParameterID, false);
                    }
                }
            }
        };
        return WrapsTypeParameterCache;
    })();
    TypeScript.WrapsTypeParameterCache = WrapsTypeParameterCache;

    (function (PullInstantiationHelpers) {
        // This class helps in creating the type argument map
        // But it creates another copy only if the type argument map is changing
        // helping in not modifying entried in the existing map
        var MutableTypeArgumentMap = (function () {
            function MutableTypeArgumentMap(typeParameterArgumentMap) {
                this.typeParameterArgumentMap = typeParameterArgumentMap;
                this.createdDuplicateTypeArgumentMap = false;
            }
            MutableTypeArgumentMap.prototype.ensureTypeArgumentCopy = function () {
                if (!this.createdDuplicateTypeArgumentMap) {
                    var passedInTypeArgumentMap = this.typeParameterArgumentMap;
                    this.typeParameterArgumentMap = [];
                    for (var typeParameterID in passedInTypeArgumentMap) {
                        if (passedInTypeArgumentMap.hasOwnProperty(typeParameterID)) {
                            this.typeParameterArgumentMap[typeParameterID] = passedInTypeArgumentMap[typeParameterID];
                        }
                    }
                    this.createdDuplicateTypeArgumentMap = true;
                }
            };
            return MutableTypeArgumentMap;
        })();
        PullInstantiationHelpers.MutableTypeArgumentMap = MutableTypeArgumentMap;

        // Instantiate the type arguments
        // This instantiates all the type parameters the symbol can reference and the type argument in the end
        // will contain the final instantiated version for each typeparameter
        // eg. if the type is already specialized, we need to create a new type argument map that represents
        // the mapping of type arguments we've just received to type arguments as previously passed through
        // If we have below sample
        //interface IList<T> {
        //    owner: IList<IList<T>>;
        //}
        //class List<U> implements IList<U> {
        //    owner: IList<IList<U>>;
        //}
        //class List2<V> extends List<V> {
        //    owner: List2<List2<V>>;
        //}
        // When instantiating List<V> with U = V and trying to get owner property we would have the map that
        // says U = V, but when creating the IList<V> we want to updates its type argument maps to say T = V because
        // IList<T>  would now be instantiated with V
        function instantiateTypeArgument(resolver, symbol, mutableTypeParameterMap) {
            if (symbol.getIsSpecialized()) {
                // Get the type argument map from the signature and update our type argument map
                var rootTypeArgumentMap = symbol.getTypeParameterArgumentMap();
                var newTypeArgumentMap = [];
                var allowedTypeParameters = symbol.getAllowedToReferenceTypeParameters();
                for (var i = 0; i < allowedTypeParameters.length; i++) {
                    var typeParameterID = allowedTypeParameters[i].pullSymbolID;
                    var typeArg = rootTypeArgumentMap[typeParameterID];
                    if (typeArg) {
                        newTypeArgumentMap[typeParameterID] = resolver.instantiateType(typeArg, mutableTypeParameterMap.typeParameterArgumentMap);
                    }
                }

                for (var i = 0; i < allowedTypeParameters.length; i++) {
                    var typeParameterID = allowedTypeParameters[i].pullSymbolID;
                    if (newTypeArgumentMap[typeParameterID] && mutableTypeParameterMap.typeParameterArgumentMap[typeParameterID] != newTypeArgumentMap[typeParameterID]) {
                        mutableTypeParameterMap.ensureTypeArgumentCopy();
                        mutableTypeParameterMap.typeParameterArgumentMap[typeParameterID] = newTypeArgumentMap[typeParameterID];
                    }
                }
            }
        }
        PullInstantiationHelpers.instantiateTypeArgument = instantiateTypeArgument;

        // Removes any entries that this instantiable symbol cannot reference
        // eg. In any type, typeparameter map should only contain information about the allowed to reference type parameters
        // so remove unnecessary entries that are outside these scope, eg. from above sample we need to remove entry U = V
        // and keep only T = V
        function cleanUpTypeArgumentMap(symbol, mutableTypeArgumentMap) {
            var allowedToReferenceTypeParameters = symbol.getAllowedToReferenceTypeParameters();
            for (var typeParameterID in mutableTypeArgumentMap.typeParameterArgumentMap) {
                if (mutableTypeArgumentMap.typeParameterArgumentMap.hasOwnProperty(typeParameterID)) {
                    if (!TypeScript.ArrayUtilities.any(allowedToReferenceTypeParameters, function (typeParameter) {
                        return typeParameter.pullSymbolID == typeParameterID;
                    })) {
                        mutableTypeArgumentMap.ensureTypeArgumentCopy();
                        delete mutableTypeArgumentMap.typeParameterArgumentMap[typeParameterID];
                    }
                }
            }
        }
        PullInstantiationHelpers.cleanUpTypeArgumentMap = cleanUpTypeArgumentMap;

        // This method get the allowed to reference type parameter in any decl
        // eg. in below code
        // interface IList<T> {
        //     owner: /*Any type here can only refere to type parameter T*/;
        //     map<U>(a: /*any type parameter here can only refere to U and T*/
        // }
        function getAllowedToReferenceTypeParametersFromDecl(decl) {
            var allowedToReferenceTypeParameters = [];

            var allowedToUseDeclTypeParameters = false;
            var getTypeParametersFromParentDecl = false;

            switch (decl.kind) {
                case TypeScript.PullElementKind.Method:
                    if (TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.Static)) {
                        // Static method/property cannot use type parameters from parent
                        allowedToUseDeclTypeParameters = true;
                        break;
                    }

                case TypeScript.PullElementKind.FunctionType:
                case TypeScript.PullElementKind.ConstructorType:
                case TypeScript.PullElementKind.ConstructSignature:
                case TypeScript.PullElementKind.CallSignature:
                case TypeScript.PullElementKind.FunctionExpression:
                case TypeScript.PullElementKind.Function:
                    allowedToUseDeclTypeParameters = true;
                    getTypeParametersFromParentDecl = true;
                    break;

                case TypeScript.PullElementKind.Property:
                    if (TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.Static)) {
                        break;
                    }

                case TypeScript.PullElementKind.Parameter:
                case TypeScript.PullElementKind.GetAccessor:
                case TypeScript.PullElementKind.SetAccessor:
                case TypeScript.PullElementKind.ConstructorMethod:
                case TypeScript.PullElementKind.IndexSignature:
                case TypeScript.PullElementKind.ObjectType:
                case TypeScript.PullElementKind.ObjectLiteral:
                case TypeScript.PullElementKind.TypeParameter:
                    getTypeParametersFromParentDecl = true;
                    break;

                case TypeScript.PullElementKind.Class:
                case TypeScript.PullElementKind.Interface:
                    allowedToUseDeclTypeParameters = true;
                    break;
            }

            if (getTypeParametersFromParentDecl) {
                allowedToReferenceTypeParameters = allowedToReferenceTypeParameters.concat(getAllowedToReferenceTypeParametersFromDecl(decl.getParentDecl()));
            }

            if (allowedToUseDeclTypeParameters) {
                var typeParameterDecls = decl.getTypeParameters();
                for (var i = 0; i < typeParameterDecls.length; i++) {
                    allowedToReferenceTypeParameters.push(typeParameterDecls[i].getSymbol());
                }
            }

            return allowedToReferenceTypeParameters;
        }
        PullInstantiationHelpers.getAllowedToReferenceTypeParametersFromDecl = getAllowedToReferenceTypeParametersFromDecl;

        function createTypeParameterArgumentMap(typeParameters, typeArguments) {
            return updateTypeParameterArgumentMap(typeParameters, typeArguments, {});
        }
        PullInstantiationHelpers.createTypeParameterArgumentMap = createTypeParameterArgumentMap;

        function updateTypeParameterArgumentMap(typeParameters, typeArguments, typeParameterArgumentMap) {
            for (var i = 0; i < typeParameters.length; i++) {
                // The reason we call getRootSymbol below is to handle the case where a signature
                // has a type parameter constrained to an outer type parameter. Because signatures
                // are instantiated from the root signature, the map needs to be in terms of the root
                // type parameter. For example,
                // interface I<T> {
                //     foo<U extends T>(u: U): U;
                // }
                // var i: I<string>;
                // var f = i.foo(""); // f should be string
                //
                // When we instantiate T to string, we create a new U, but instantiations of the
                // signature must be against the root U.
                // Note that when this type of situation does not apply, getRootSymbol is the
                // identity function.
                typeParameterArgumentMap[typeParameters[i].getRootSymbol().pullSymbolID] = typeArguments[i];
            }
            return typeParameterArgumentMap;
        }
        PullInstantiationHelpers.updateTypeParameterArgumentMap = updateTypeParameterArgumentMap;

        function updateMutableTypeParameterArgumentMap(typeParameters, typeArguments, mutableMap) {
            for (var i = 0; i < typeParameters.length; i++) {
                // See comment in updateTypeParameterArgumentMap for why we use getRootSymbol
                var typeParameterId = typeParameters[i].getRootSymbol().pullSymbolID;
                if (mutableMap.typeParameterArgumentMap[typeParameterId] !== typeArguments[i]) {
                    mutableMap.ensureTypeArgumentCopy();
                    mutableMap.typeParameterArgumentMap[typeParameterId] = typeArguments[i];
                }
            }
        }
        PullInstantiationHelpers.updateMutableTypeParameterArgumentMap = updateMutableTypeParameterArgumentMap;

        function twoTypesAreInstantiationsOfSameNamedGenericType(type1, type2) {
            var type1IsGeneric = type1.isGeneric() && type1.getTypeArguments() !== null;
            var type2IsGeneric = type2.isGeneric() && type2.getTypeArguments() !== null;

            if (type1IsGeneric && type2IsGeneric) {
                var type1Root = TypeScript.PullHelpers.getRootType(type1);
                var type2Root = TypeScript.PullHelpers.getRootType(type2);
                return type1Root === type2Root;
            }

            return false;
        }
        PullInstantiationHelpers.twoTypesAreInstantiationsOfSameNamedGenericType = twoTypesAreInstantiationsOfSameNamedGenericType;
    })(TypeScript.PullInstantiationHelpers || (TypeScript.PullInstantiationHelpers = {}));
    var PullInstantiationHelpers = TypeScript.PullInstantiationHelpers;
})(TypeScript || (TypeScript = {}));
