// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
///<reference path="..\typescript.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var TypeScript;
(function (TypeScript) {
    (function (GenerativeTypeClassification) {
        GenerativeTypeClassification[GenerativeTypeClassification["Unknown"] = 0] = "Unknown";
        GenerativeTypeClassification[GenerativeTypeClassification["Open"] = 1] = "Open";
        GenerativeTypeClassification[GenerativeTypeClassification["Closed"] = 2] = "Closed";
        GenerativeTypeClassification[GenerativeTypeClassification["InfinitelyExpanding"] = 3] = "InfinitelyExpanding";
    })(TypeScript.GenerativeTypeClassification || (TypeScript.GenerativeTypeClassification = {}));
    var GenerativeTypeClassification = TypeScript.GenerativeTypeClassification;

    // Type references and instantiated type references
    var PullTypeReferenceSymbol = (function (_super) {
        __extends(PullTypeReferenceSymbol, _super);
        // use the root symbol to model the actual type
        // do not call this directly!
        function PullTypeReferenceSymbol(referencedTypeSymbol) {
            _super.call(this, referencedTypeSymbol.name, referencedTypeSymbol.kind);
            this.referencedTypeSymbol = referencedTypeSymbol;
            this.isResolved = true;

            TypeScript.Debug.assert(referencedTypeSymbol !== null, "Type root symbol may not be null");

            this.setRootSymbol(referencedTypeSymbol);

            this.typeReference = this;
        }
        PullTypeReferenceSymbol.createTypeReference = function (type) {
            if (type.isTypeReference()) {
                return type;
            }

            var typeReference = type.typeReference;

            if (!typeReference) {
                typeReference = new PullTypeReferenceSymbol(type);
                type.typeReference = typeReference;
            }

            return typeReference;
        };

        PullTypeReferenceSymbol.prototype.isTypeReference = function () {
            return true;
        };

        PullTypeReferenceSymbol.prototype.setResolved = function () {
        };

        // do nothing on invalidate
        PullTypeReferenceSymbol.prototype.setUnresolved = function () {
        };
        PullTypeReferenceSymbol.prototype.invalidate = function () {
        };

        PullTypeReferenceSymbol.prototype.ensureReferencedTypeIsResolved = function () {
            this._getResolver().resolveDeclaredSymbol(this.referencedTypeSymbol);
        };

        PullTypeReferenceSymbol.prototype.getReferencedTypeSymbol = function () {
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol;
        };

        PullTypeReferenceSymbol.prototype._getResolver = function () {
            return this.referencedTypeSymbol._getResolver();
        };

        // type symbol shims
        PullTypeReferenceSymbol.prototype.hasMembers = function () {
            // no need to resolve first - members are collected during binding
            return this.referencedTypeSymbol.hasMembers();
        };

        PullTypeReferenceSymbol.prototype.setAssociatedContainerType = function (type) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": setAssociatedContainerType");
        };

        PullTypeReferenceSymbol.prototype.getAssociatedContainerType = function () {
            return this.referencedTypeSymbol.getAssociatedContainerType();
        };

        PullTypeReferenceSymbol.prototype.getFunctionSymbol = function () {
            // necessary because the function symbol may be set during type resolution to
            // facilitate doc comments
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.getFunctionSymbol();
        };
        PullTypeReferenceSymbol.prototype.setFunctionSymbol = function (symbol) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": setFunctionSymbol");
        };

        PullTypeReferenceSymbol.prototype.addContainedNonMember = function (nonMember) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addContainedNonMember");
        };
        PullTypeReferenceSymbol.prototype.findContainedNonMemberContainer = function (containerName, kind) {
            if (typeof kind === "undefined") { kind = 0 /* None */; }
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.findContainedNonMemberContainer(containerName, kind);
        };

        PullTypeReferenceSymbol.prototype.addMember = function (memberSymbol) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addMember");
        };
        PullTypeReferenceSymbol.prototype.addEnclosedMemberType = function (enclosedType) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addEnclosedMemberType");
        };
        PullTypeReferenceSymbol.prototype.addEnclosedMemberContainer = function (enclosedContainer) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addEnclosedMemberContainer");
        };
        PullTypeReferenceSymbol.prototype.addEnclosedNonMember = function (enclosedNonMember) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addEnclosedNonMember");
        };
        PullTypeReferenceSymbol.prototype.addEnclosedNonMemberType = function (enclosedNonMemberType) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addEnclosedNonMemberType");
        };
        PullTypeReferenceSymbol.prototype.addEnclosedNonMemberContainer = function (enclosedNonMemberContainer) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addEnclosedNonMemberContainer");
        };
        PullTypeReferenceSymbol.prototype.addTypeParameter = function (typeParameter) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addTypeParameter");
        };
        PullTypeReferenceSymbol.prototype.addConstructorTypeParameter = function (typeParameter) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addConstructorTypeParameter");
        };

        PullTypeReferenceSymbol.prototype.findContainedNonMember = function (name) {
            // need to ensure the referenced type is resolved so we can find the non-member
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.findContainedNonMember(name);
        };

        PullTypeReferenceSymbol.prototype.findContainedNonMemberType = function (typeName, kind) {
            if (typeof kind === "undefined") { kind = 0 /* None */; }
            // similar to the above, need to ensure that the type is resolved so we can introspect any
            // contained types
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.findContainedNonMemberType(typeName, kind);
        };

        PullTypeReferenceSymbol.prototype.getMembers = function () {
            // need to resolve the referenced types to get the members
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.getMembers();
        };

        PullTypeReferenceSymbol.prototype.setHasDefaultConstructor = function (hasOne) {
            if (typeof hasOne === "undefined") { hasOne = true; }
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ":setHasDefaultConstructor");
        };
        PullTypeReferenceSymbol.prototype.getHasDefaultConstructor = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getHasDefaultConstructor();
        };
        PullTypeReferenceSymbol.prototype.getConstructorMethod = function () {
            // need to resolve so we don't accidentally substitute in a default constructor
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getConstructorMethod();
        };
        PullTypeReferenceSymbol.prototype.setConstructorMethod = function (constructorMethod) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": setConstructorMethod");
        };
        PullTypeReferenceSymbol.prototype.getTypeParameters = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getTypeParameters();
        };

        PullTypeReferenceSymbol.prototype.isGeneric = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.isGeneric();
        };

        PullTypeReferenceSymbol.prototype.addSpecialization = function (specializedVersionOfThisType, substitutingTypes) {
            //Debug.fail("Reference symbol " + this.pullSymbolIDString + ": addSpecialization");
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.addSpecialization(specializedVersionOfThisType, substitutingTypes);
        };
        PullTypeReferenceSymbol.prototype.getSpecialization = function (substitutingTypes) {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getSpecialization(substitutingTypes);
        };
        PullTypeReferenceSymbol.prototype.getKnownSpecializations = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getKnownSpecializations();
        };
        PullTypeReferenceSymbol.prototype.getTypeArguments = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getTypeArguments();
        };
        PullTypeReferenceSymbol.prototype.getTypeArgumentsOrTypeParameters = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getTypeArgumentsOrTypeParameters();
        };

        PullTypeReferenceSymbol.prototype.appendCallSignature = function (callSignature) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": appendCallSignature");
        };
        PullTypeReferenceSymbol.prototype.insertCallSignatureAtIndex = function (callSignature, index) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": insertCallSignatureAtIndex");
        };
        PullTypeReferenceSymbol.prototype.appendConstructSignature = function (callSignature) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": appendConstructSignature");
        };
        PullTypeReferenceSymbol.prototype.insertConstructSignatureAtIndex = function (callSignature, index) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": insertConstructSignatureAtIndex");
        };
        PullTypeReferenceSymbol.prototype.addIndexSignature = function (indexSignature) {
            TypeScript.Debug.fail("Reference symbol " + this.pullSymbolID + ": addIndexSignature");
        };

        PullTypeReferenceSymbol.prototype.hasOwnCallSignatures = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.hasOwnCallSignatures();
        };
        PullTypeReferenceSymbol.prototype.getCallSignatures = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getCallSignatures();
        };
        PullTypeReferenceSymbol.prototype.hasOwnConstructSignatures = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.hasOwnConstructSignatures();
        };
        PullTypeReferenceSymbol.prototype.getConstructSignatures = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getConstructSignatures();
        };
        PullTypeReferenceSymbol.prototype.hasOwnIndexSignatures = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.hasOwnIndexSignatures();
        };
        PullTypeReferenceSymbol.prototype.getIndexSignatures = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getIndexSignatures();
        };

        PullTypeReferenceSymbol.prototype.addImplementedType = function (implementedType) {
            this.referencedTypeSymbol.addImplementedType(implementedType);
        };
        PullTypeReferenceSymbol.prototype.getImplementedTypes = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getImplementedTypes();
        };
        PullTypeReferenceSymbol.prototype.addExtendedType = function (extendedType) {
            this.referencedTypeSymbol.addExtendedType(extendedType);
        };
        PullTypeReferenceSymbol.prototype.getExtendedTypes = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getExtendedTypes();
        };
        PullTypeReferenceSymbol.prototype.addTypeThatExtendsThisType = function (type) {
            this.referencedTypeSymbol.addTypeThatExtendsThisType(type);
        };
        PullTypeReferenceSymbol.prototype.getTypesThatExtendThisType = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getTypesThatExtendThisType();
        };
        PullTypeReferenceSymbol.prototype.addTypeThatExplicitlyImplementsThisType = function (type) {
            this.referencedTypeSymbol.addTypeThatExplicitlyImplementsThisType(type);
        };
        PullTypeReferenceSymbol.prototype.getTypesThatExplicitlyImplementThisType = function () {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.getTypesThatExplicitlyImplementThisType();
        };

        PullTypeReferenceSymbol.prototype.isValidBaseKind = function (baseType, isExtendedType) {
            this.ensureReferencedTypeIsResolved();
            return this.referencedTypeSymbol.isValidBaseKind(baseType, isExtendedType);
        };

        PullTypeReferenceSymbol.prototype.findMember = function (name, lookInParent) {
            if (typeof lookInParent === "undefined") { lookInParent = true; }
            // ensure that the type is resolved before looking for members
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.findMember(name, lookInParent);
        };
        PullTypeReferenceSymbol.prototype.findNestedType = function (name, kind) {
            if (typeof kind === "undefined") { kind = 0 /* None */; }
            // ensure that the type is resolved before looking for nested types
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.findNestedType(name, kind);
        };
        PullTypeReferenceSymbol.prototype.findNestedContainer = function (name, kind) {
            if (typeof kind === "undefined") { kind = 0 /* None */; }
            // ensure that the type is resolved before looking for nested containers
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.findNestedContainer(name, kind);
        };
        PullTypeReferenceSymbol.prototype.getAllMembers = function (searchDeclKind, memberVisiblity) {
            // ensure that the type is resolved before trying to collect all members
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.getAllMembers(searchDeclKind, memberVisiblity);
        };

        PullTypeReferenceSymbol.prototype.findTypeParameter = function (name) {
            // ensure that the type is resolved before trying to look up a type parameter
            this.ensureReferencedTypeIsResolved();

            return this.referencedTypeSymbol.findTypeParameter(name);
        };

        /*
        public getNamePartForFullName(): string {
        return this.referencedTypeSymbol.getNamePartForFullName();
        }
        public getScopedName(scopeSymbol?: PullSymbol, useConstraintInName?: boolean): string {
        return this.referencedTypeSymbol.getScopedName(scopeSymbol, useConstraintInName);
        }
        public isNamedTypeSymbol(): boolean {
        return this.referencedTypeSymbol.isNamedTypeSymbol();
        }
        
        public toString(scopeSymbol?: PullSymbol, useConstraintInName?: boolean): string {
        return this.referencedTypeSymbol.toString(scopeSymbol, useConstraintInName);
        }
        
        public getScopedNameEx(scopeSymbol?: PullSymbol, useConstraintInName?: boolean, getPrettyTypeName?: boolean, getTypeParamMarkerInfo?: boolean): MemberName {
        return this.referencedTypeSymbol.getScopedNameEx(scopeSymbol, useConstraintInName, getPrettyTypeName, getTypeParamMarkerInfo);
        }
        */
        PullTypeReferenceSymbol.prototype.hasOnlyOverloadCallSignatures = function () {
            // no need to resolve the referenced type - only computed during printing
            return this.referencedTypeSymbol.hasOnlyOverloadCallSignatures();
        };
        return PullTypeReferenceSymbol;
    })(TypeScript.PullTypeSymbol);
    TypeScript.PullTypeReferenceSymbol = PullTypeReferenceSymbol;

    TypeScript.nSpecializationsCreated = 0;
    TypeScript.nSpecializedSignaturesCreated = 0;
    TypeScript.nSpecializedTypeParameterCreated = 0;

    var PullInstantiatedTypeReferenceSymbol = (function (_super) {
        __extends(PullInstantiatedTypeReferenceSymbol, _super);
        function PullInstantiatedTypeReferenceSymbol(referencedTypeSymbol, _typeParameterArgumentMap, isInstanceReferenceType) {
            _super.call(this, referencedTypeSymbol);
            this.referencedTypeSymbol = referencedTypeSymbol;
            this._typeParameterArgumentMap = _typeParameterArgumentMap;
            this.isInstanceReferenceType = isInstanceReferenceType;
            this._instantiatedMembers = null;
            this._allInstantiatedMemberNameCache = null;
            this._instantiatedMemberNameCache = TypeScript.createIntrinsicsObject();
            this._instantiatedCallSignatures = null;
            this._instantiatedConstructSignatures = null;
            this._instantiatedIndexSignatures = null;
            this._typeArgumentReferences = undefined;
            this._instantiatedConstructorMethod = null;
            this._instantiatedAssociatedContainerType = null;
            this._isArray = undefined;
            this._generativeTypeClassification = [];

            TypeScript.nSpecializationsCreated++;
        }
        PullInstantiatedTypeReferenceSymbol.prototype.getIsSpecialized = function () {
            return !this.isInstanceReferenceType;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getGenerativeTypeClassification = function (enclosingType) {
            // Generative type classification is available only on named type symbols
            if (!this.isNamedTypeSymbol()) {
                return 0 /* Unknown */;
            }

            var generativeTypeClassification = this._generativeTypeClassification[enclosingType.pullSymbolID] || 0 /* Unknown */;
            if (generativeTypeClassification === 0 /* Unknown */) {
                // With respect to the enclosing type, is this type reference open, closed or
                // infinitely expanding?
                // Create a type parameter map for figuring out if the typeParameter wraps
                var typeParameters = enclosingType.getTypeParameters();
                var enclosingTypeParameterMap = [];
                for (var i = 0; i < typeParameters.length; i++) {
                    enclosingTypeParameterMap[typeParameters[i].pullSymbolID] = typeParameters[i];
                }

                var typeArguments = this.getTypeArguments();
                for (var i = 0; i < typeArguments.length; i++) {
                    // Spec section 3.8.7 Recursive Types:
                    // - A type reference without type arguments or with type arguments that do not reference
                    //      any of G's type parameters is classified as a closed type reference.
                    // - A type reference that references any of G's type parameters in a type argument is
                    //      classified as an open type reference.
                    if (typeArguments[i].wrapsSomeTypeParameter(enclosingTypeParameterMap, true)) {
                        // This type wraps type parameter of the enclosing type so it is at least open
                        generativeTypeClassification = 1 /* Open */;
                        break;
                    }
                }

                // If the type reference is determined to be atleast open, determine if it is infinitely expanding
                if (generativeTypeClassification === 1 /* Open */) {
                    if (this.wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReference(enclosingType)) {
                        generativeTypeClassification = 3 /* InfinitelyExpanding */;
                    }
                } else {
                    // This type doesnot wrap any type parameter from the enclosing type so it is closed
                    generativeTypeClassification = 2 /* Closed */;
                }

                this._generativeTypeClassification[enclosingType.pullSymbolID] = generativeTypeClassification;
            }

            return generativeTypeClassification;
        };

        // shims
        PullInstantiatedTypeReferenceSymbol.prototype.isArrayNamedTypeReference = function () {
            if (this._isArray === undefined) {
                this._isArray = this.getRootSymbol().isArrayNamedTypeReference() || (this.getRootSymbol() === this._getResolver().getArrayNamedType());
            }
            return this._isArray;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getElementType = function () {
            if (!this.isArrayNamedTypeReference()) {
                return null;
            }

            var typeArguments = this.getTypeArguments();
            return typeArguments ? typeArguments[0] : null;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getReferencedTypeSymbol = function () {
            this.ensureReferencedTypeIsResolved();

            if (this.getIsSpecialized()) {
                return this;
            }

            return this.referencedTypeSymbol;
        };

        // The typeParameterArgumentMap parameter represents a mapping of PUllSymbolID strings of type parameters to type argument symbols
        // The instantiateFunctionTypeParameters parameter is set to true when a signature is being specialized at a call site, or if its
        // type parameters need to otherwise be specialized (say, during a type relationship check)
        PullInstantiatedTypeReferenceSymbol.create = function (resolver, type, typeParameterArgumentMap) {
            TypeScript.Debug.assert(resolver);

            // check for an existing instantiation
            // If the typeArgument map is going to change, we need to create copy so that
            // we dont polute the map entry passed in by the caller.
            // (eg. when getting the member type using enclosing types type argument map)
            var mutableTypeParameterMap = new TypeScript.PullInstantiationHelpers.MutableTypeArgumentMap(typeParameterArgumentMap);

            // if the type is already specialized, we need to create a new type argument map that represents
            // the mapping of type arguments we've just received to type arguments as previously passed through
            // If we have below sample
            //interface IList<T> {
            //    owner: IList<IList<T>>;
            //}
            //class List<U> implements IList<U> {
            //    owner: IList<IList<U>>;
            //}
            //class List2<V> extends List<V> {
            //    owner: List2<List2<V>>;
            //}
            // When instantiating List<V> with U = V and trying to get owner property we would have the map that
            // says U = V, but when creating the IList<V> we want to updates its type argument maps to say T = V because
            // IList<T>  would now be instantiated with V
            TypeScript.PullInstantiationHelpers.instantiateTypeArgument(resolver, type, mutableTypeParameterMap);

            // Lookup in cache if this specialization already exists
            var rootType = type.getRootSymbol();
            var instantiation = rootType.getSpecialization(mutableTypeParameterMap.typeParameterArgumentMap);
            if (instantiation) {
                return instantiation;
            }

            // In any type, typeparameter map should only contain information about the allowed to reference type parameters
            // so remove unnecessary entries that are outside these scope, eg. from above sample we need to remove entry U = V
            // and keep only T = V
            TypeScript.PullInstantiationHelpers.cleanUpTypeArgumentMap(type, mutableTypeParameterMap);
            typeParameterArgumentMap = mutableTypeParameterMap.typeParameterArgumentMap;

            // If the reference is made to itself (e.g., referring to Array<T> within the declaration of Array<T>,
            // We want to special-case the reference so later calls to getMember, etc., will delegate directly
            // to the referenced declaration type, and not force any additional instantiation
            var isInstanceReferenceType = (type.kind & TypeScript.PullElementKind.SomeInstantiatableType) != 0;
            var resolvedTypeParameterArgumentMap = typeParameterArgumentMap;
            if (isInstanceReferenceType) {
                var typeParameters = rootType.getTypeParameters();
                for (var i = 0; i < typeParameters.length; i++) {
                    if (!TypeScript.PullHelpers.typeSymbolsAreIdentical(typeParameters[i], typeParameterArgumentMap[typeParameters[i].pullSymbolID])) {
                        isInstanceReferenceType = false;
                        break;
                    }
                }

                if (isInstanceReferenceType) {
                    typeParameterArgumentMap = [];
                }
            }

            // Create the type using typeArgument map
            instantiation = new PullInstantiatedTypeReferenceSymbol(rootType, typeParameterArgumentMap, isInstanceReferenceType);

            // Store in the cache
            rootType.addSpecialization(instantiation, resolvedTypeParameterArgumentMap);

            return instantiation;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.isGeneric = function () {
            return this.getRootSymbol().isGeneric();
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getTypeParameterArgumentMap = function () {
            return this._typeParameterArgumentMap;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getTypeArguments = function () {
            if (this.isInstanceReferenceType) {
                return this.getTypeParameters();
            }

            if (this._typeArgumentReferences === undefined) {
                var typeParameters = this.referencedTypeSymbol.getTypeParameters();

                if (typeParameters.length) {
                    var typeArgument = null;
                    var typeArguments = [];

                    for (var i = 0; i < typeParameters.length; i++) {
                        typeArgument = this._typeParameterArgumentMap[typeParameters[i].pullSymbolID];

                        if (!typeArgument) {
                            TypeScript.Debug.fail("type argument count mismatch");
                        }

                        if (typeArgument) {
                            typeArguments[typeArguments.length] = typeArgument;
                        }
                    }

                    this._typeArgumentReferences = typeArguments;
                } else {
                    this._typeArgumentReferences = null;
                }
            }

            return this._typeArgumentReferences;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getTypeArgumentsOrTypeParameters = function () {
            return this.getTypeArguments();
        };

        PullInstantiatedTypeReferenceSymbol.prototype.populateInstantiatedMemberFromReferencedMember = function (referencedMember) {
            var instantiatedMember;
            TypeScript.PullHelpers.resolveDeclaredSymbolToUseType(referencedMember);

            // if the member does not require further specialization, re-use the referenced symbol
            if (!referencedMember.type.wrapsSomeTypeParameter(this._typeParameterArgumentMap)) {
                instantiatedMember = referencedMember;
            } else {
                instantiatedMember = new TypeScript.PullSymbol(referencedMember.name, referencedMember.kind);
                instantiatedMember.setRootSymbol(referencedMember);
                instantiatedMember.type = this._getResolver().instantiateType(referencedMember.type, this._typeParameterArgumentMap);
                instantiatedMember.isOptional = referencedMember.isOptional;
            }
            this._instantiatedMemberNameCache[instantiatedMember.name] = instantiatedMember;
        };

        //
        // lazily evaluated members
        //
        PullInstantiatedTypeReferenceSymbol.prototype.getMembers = function () {
            // need to resolve the referenced types to get the members
            this.ensureReferencedTypeIsResolved();

            if (this.isInstanceReferenceType) {
                return this.referencedTypeSymbol.getMembers();
            }

            // for each of the referenced type's members, need to properly instantiate their
            // type references
            if (!this._instantiatedMembers) {
                var referencedMembers = this.referencedTypeSymbol.getMembers();
                var referencedMember = null;
                var instantiatedMember = null;

                this._instantiatedMembers = [];

                for (var i = 0; i < referencedMembers.length; i++) {
                    referencedMember = referencedMembers[i];

                    this._getResolver().resolveDeclaredSymbol(referencedMember);

                    if (!this._instantiatedMemberNameCache[referencedMember.name]) {
                        this.populateInstantiatedMemberFromReferencedMember(referencedMember);
                    }

                    this._instantiatedMembers[this._instantiatedMembers.length] = this._instantiatedMemberNameCache[referencedMember.name];
                }
            }

            return this._instantiatedMembers;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.findMember = function (name, lookInParent) {
            if (typeof lookInParent === "undefined") { lookInParent = true; }
            // ensure that the type is resolved before looking for members
            this.ensureReferencedTypeIsResolved();

            if (this.isInstanceReferenceType) {
                return this.referencedTypeSymbol.findMember(name, lookInParent);
            }

            // if the member exists on the referenced type, need to ensure that it's
            // instantiated
            var memberSymbol = this._instantiatedMemberNameCache[name];

            if (!memberSymbol) {
                var referencedMemberSymbol = this.referencedTypeSymbol.findMember(name, lookInParent);

                if (referencedMemberSymbol) {
                    this.populateInstantiatedMemberFromReferencedMember(referencedMemberSymbol);
                    memberSymbol = this._instantiatedMemberNameCache[name];
                } else {
                    memberSymbol = null;
                }
            }

            return memberSymbol;
        };

        // May need to cache based on search kind / visibility combinations
        PullInstantiatedTypeReferenceSymbol.prototype.getAllMembers = function (searchDeclKind, memberVisiblity) {
            // ensure that the type is resolved before trying to collect all members
            this.ensureReferencedTypeIsResolved();

            if (this.isInstanceReferenceType) {
                return this.referencedTypeSymbol.getAllMembers(searchDeclKind, memberVisiblity);
            }

            var requestedMembers = [];
            var allReferencedMembers = this.referencedTypeSymbol.getAllMembers(searchDeclKind, memberVisiblity);

            if (!this._allInstantiatedMemberNameCache) {
                this._allInstantiatedMemberNameCache = TypeScript.createIntrinsicsObject();

                // first, seed with this type's members
                var members = this.getMembers();

                for (var i = 0; i < members.length; i++) {
                    this._allInstantiatedMemberNameCache[members[i].name] = members[i];
                }
            }

            // next, for add any symbols belonging to the parent type, if necessary
            var referencedMember = null;
            var requestedMember = null;

            for (var i = 0; i < allReferencedMembers.length; i++) {
                referencedMember = allReferencedMembers[i];

                this._getResolver().resolveDeclaredSymbol(referencedMember);

                if (this._allInstantiatedMemberNameCache[referencedMember.name]) {
                    requestedMembers[requestedMembers.length] = this._allInstantiatedMemberNameCache[referencedMember.name];
                } else {
                    if (!referencedMember.type.wrapsSomeTypeParameter(this._typeParameterArgumentMap)) {
                        this._allInstantiatedMemberNameCache[referencedMember.name] = referencedMember;
                        requestedMembers[requestedMembers.length] = referencedMember;
                    } else {
                        requestedMember = new TypeScript.PullSymbol(referencedMember.name, referencedMember.kind);
                        requestedMember.setRootSymbol(referencedMember);

                        requestedMember.type = this._getResolver().instantiateType(referencedMember.type, this._typeParameterArgumentMap);
                        requestedMember.isOptional = referencedMember.isOptional;

                        this._allInstantiatedMemberNameCache[requestedMember.name] = requestedMember;
                        requestedMembers[requestedMembers.length] = requestedMember;
                    }
                }
            }

            return requestedMembers;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getConstructorMethod = function () {
            if (this.isInstanceReferenceType) {
                return this.referencedTypeSymbol.getConstructorMethod();
            }

            if (!this._instantiatedConstructorMethod) {
                var referencedConstructorMethod = this.referencedTypeSymbol.getConstructorMethod();
                this._instantiatedConstructorMethod = new TypeScript.PullSymbol(referencedConstructorMethod.name, referencedConstructorMethod.kind);
                this._instantiatedConstructorMethod.setRootSymbol(referencedConstructorMethod);
                this._instantiatedConstructorMethod.setResolved();

                this._instantiatedConstructorMethod.type = PullInstantiatedTypeReferenceSymbol.create(this._getResolver(), referencedConstructorMethod.type, this._typeParameterArgumentMap);
            }

            return this._instantiatedConstructorMethod;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getAssociatedContainerType = function () {
            if (!this.isInstanceReferenceType) {
                return this.referencedTypeSymbol.getAssociatedContainerType();
            }

            if (!this._instantiatedAssociatedContainerType) {
                var referencedAssociatedContainerType = this.referencedTypeSymbol.getAssociatedContainerType();

                if (referencedAssociatedContainerType) {
                    this._instantiatedAssociatedContainerType = PullInstantiatedTypeReferenceSymbol.create(this._getResolver(), referencedAssociatedContainerType, this._typeParameterArgumentMap);
                }
            }

            return this._instantiatedAssociatedContainerType;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getCallSignatures = function () {
            this.ensureReferencedTypeIsResolved();

            if (this.isInstanceReferenceType) {
                return this.referencedTypeSymbol.getCallSignatures();
            }

            if (this._instantiatedCallSignatures) {
                return this._instantiatedCallSignatures;
            }

            var referencedCallSignatures = this.referencedTypeSymbol.getCallSignatures();
            this._instantiatedCallSignatures = [];

            for (var i = 0; i < referencedCallSignatures.length; i++) {
                this._getResolver().resolveDeclaredSymbol(referencedCallSignatures[i]);

                if (!referencedCallSignatures[i].wrapsSomeTypeParameter(this._typeParameterArgumentMap)) {
                    this._instantiatedCallSignatures[this._instantiatedCallSignatures.length] = referencedCallSignatures[i];
                } else {
                    this._instantiatedCallSignatures[this._instantiatedCallSignatures.length] = this._getResolver().instantiateSignature(referencedCallSignatures[i], this._typeParameterArgumentMap);
                    this._instantiatedCallSignatures[this._instantiatedCallSignatures.length - 1].functionType = this;
                }
            }

            return this._instantiatedCallSignatures;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getConstructSignatures = function () {
            this.ensureReferencedTypeIsResolved();

            if (this.isInstanceReferenceType) {
                return this.referencedTypeSymbol.getConstructSignatures();
            }

            if (this._instantiatedConstructSignatures) {
                return this._instantiatedConstructSignatures;
            }

            var referencedConstructSignatures = this.referencedTypeSymbol.getConstructSignatures();
            this._instantiatedConstructSignatures = [];

            for (var i = 0; i < referencedConstructSignatures.length; i++) {
                this._getResolver().resolveDeclaredSymbol(referencedConstructSignatures[i]);

                if (!referencedConstructSignatures[i].wrapsSomeTypeParameter(this._typeParameterArgumentMap)) {
                    this._instantiatedConstructSignatures[this._instantiatedConstructSignatures.length] = referencedConstructSignatures[i];
                } else {
                    this._instantiatedConstructSignatures[this._instantiatedConstructSignatures.length] = this._getResolver().instantiateSignature(referencedConstructSignatures[i], this._typeParameterArgumentMap);
                    this._instantiatedConstructSignatures[this._instantiatedConstructSignatures.length - 1].functionType = this;
                }
            }

            return this._instantiatedConstructSignatures;
        };

        PullInstantiatedTypeReferenceSymbol.prototype.getIndexSignatures = function () {
            this.ensureReferencedTypeIsResolved();

            if (this.isInstanceReferenceType) {
                return this.referencedTypeSymbol.getIndexSignatures();
            }

            if (this._instantiatedIndexSignatures) {
                return this._instantiatedIndexSignatures;
            }

            var referencedIndexSignatures = this.referencedTypeSymbol.getIndexSignatures();
            this._instantiatedIndexSignatures = [];

            for (var i = 0; i < referencedIndexSignatures.length; i++) {
                this._getResolver().resolveDeclaredSymbol(referencedIndexSignatures[i]);

                if (!referencedIndexSignatures[i].wrapsSomeTypeParameter(this._typeParameterArgumentMap)) {
                    this._instantiatedIndexSignatures[this._instantiatedIndexSignatures.length] = referencedIndexSignatures[i];
                } else {
                    this._instantiatedIndexSignatures[this._instantiatedIndexSignatures.length] = this._getResolver().instantiateSignature(referencedIndexSignatures[i], this._typeParameterArgumentMap);
                    this._instantiatedIndexSignatures[this._instantiatedIndexSignatures.length - 1].functionType = this;
                }
            }

            return this._instantiatedIndexSignatures;
        };
        return PullInstantiatedTypeReferenceSymbol;
    })(PullTypeReferenceSymbol);
    TypeScript.PullInstantiatedTypeReferenceSymbol = PullInstantiatedTypeReferenceSymbol;

    var PullInstantiatedSignatureSymbol = (function (_super) {
        __extends(PullInstantiatedSignatureSymbol, _super);
        function PullInstantiatedSignatureSymbol(rootSignature, _typeParameterArgumentMap) {
            _super.call(this, rootSignature.kind, rootSignature.isDefinition());
            this._typeParameterArgumentMap = _typeParameterArgumentMap;
            this.setRootSymbol(rootSignature);
            TypeScript.nSpecializedSignaturesCreated++;

            // Store in the cache
            rootSignature.addSpecialization(this, _typeParameterArgumentMap);
        }
        PullInstantiatedSignatureSymbol.prototype.getTypeParameterArgumentMap = function () {
            return this._typeParameterArgumentMap;
        };

        PullInstantiatedSignatureSymbol.prototype.getIsSpecialized = function () {
            return true;
        };

        PullInstantiatedSignatureSymbol.prototype._getResolver = function () {
            return this.getRootSymbol()._getResolver();
        };

        PullInstantiatedSignatureSymbol.prototype.getTypeParameters = function () {
            var _this = this;
            if (!this._typeParameters) {
                var rootSymbol = this.getRootSymbol();
                var typeParameters = rootSymbol.getTypeParameters();
                var hasInstantiatedTypeParametersOfThisSignature = TypeScript.ArrayUtilities.all(typeParameters, function (typeParameter) {
                    return _this._typeParameterArgumentMap[typeParameter.pullSymbolID] !== undefined;
                });

                if (!hasInstantiatedTypeParametersOfThisSignature && typeParameters.length) {
                    // Type parameteres are the instantiated version of the rootTypeparmeters with our own type parameter argument map
                    this._typeParameters = [];
                    for (var i = 0; i < typeParameters.length; i++) {
                        this._typeParameters[this._typeParameters.length] = this._getResolver().instantiateTypeParameter(typeParameters[i], this._typeParameterArgumentMap);
                    }
                } else {
                    this._typeParameters = TypeScript.sentinelEmptyArray;
                }
            }

            return this._typeParameters;
        };

        PullInstantiatedSignatureSymbol.prototype.getAllowedToReferenceTypeParameters = function () {
            var rootSymbol = this.getRootSymbol();
            return rootSymbol.getAllowedToReferenceTypeParameters();
        };
        return PullInstantiatedSignatureSymbol;
    })(TypeScript.PullSignatureSymbol);
    TypeScript.PullInstantiatedSignatureSymbol = PullInstantiatedSignatureSymbol;

    // Instantiated type parameter symbol is the type parameter with the instantiated version of the constraint
    var PullInstantiatedTypeParameterSymbol = (function (_super) {
        __extends(PullInstantiatedTypeParameterSymbol, _super);
        function PullInstantiatedTypeParameterSymbol(rootTypeParameter, constraintType) {
            _super.call(this, rootTypeParameter.name);
            TypeScript.nSpecializedTypeParameterCreated++;

            this.setRootSymbol(rootTypeParameter);
            this.setConstraint(constraintType);

            // Store in the cache
            rootTypeParameter.addSpecialization(this, [constraintType]);
        }
        PullInstantiatedTypeParameterSymbol.prototype._getResolver = function () {
            return this.getRootSymbol()._getResolver();
        };
        return PullInstantiatedTypeParameterSymbol;
    })(TypeScript.PullTypeParameterSymbol);
    TypeScript.PullInstantiatedTypeParameterSymbol = PullInstantiatedTypeParameterSymbol;
})(TypeScript || (TypeScript = {}));
