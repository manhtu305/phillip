// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    TypeScript.pullSymbolID = 0;
    TypeScript.sentinelEmptyArray = [];

    var PullSymbol = (function () {
        function PullSymbol(name, declKind) {
            // private state
            // The symbol ids start with 1 and not 0 as 0 is reserved to detect false conditions eg. in determining
            // wrappingTypeParameterID
            this.pullSymbolID = ++TypeScript.pullSymbolID;
            this._container = null;
            this.type = null;
            // We cache the declarations to improve look-up speed
            // (but we re-create on edits because deletion from the linked list is
            // much faster)
            this._declarations = null;
            this.isResolved = false;
            this.isOptional = false;
            this.inResolution = false;
            this.isSynthesized = false;
            this.isVarArg = false;
            this.rootSymbol = null;
            this._enclosingSignature = null;
            this._docComments = null;
            this.isPrinting = false;
            this.name = name;
            this.kind = declKind;
        }
        PullSymbol.prototype.isAny = function () {
            return false;
        };

        PullSymbol.prototype.isType = function () {
            return (this.kind & TypeScript.PullElementKind.SomeType) !== 0;
        };

        PullSymbol.prototype.isTypeReference = function () {
            return false;
        };

        PullSymbol.prototype.isSignature = function () {
            return (this.kind & TypeScript.PullElementKind.SomeSignature) !== 0;
        };

        PullSymbol.prototype.isArrayNamedTypeReference = function () {
            return false;
        };

        PullSymbol.prototype.isPrimitive = function () {
            return this.kind === TypeScript.PullElementKind.Primitive;
        };

        PullSymbol.prototype.isAccessor = function () {
            return false;
        };

        PullSymbol.prototype.isError = function () {
            return false;
        };

        PullSymbol.prototype.isInterface = function () {
            return this.kind === TypeScript.PullElementKind.Interface;
        };

        PullSymbol.prototype.isMethod = function () {
            return this.kind === TypeScript.PullElementKind.Method;
        };

        PullSymbol.prototype.isProperty = function () {
            return this.kind === TypeScript.PullElementKind.Property;
        };

        PullSymbol.prototype.isAlias = function () {
            return false;
        };

        PullSymbol.prototype.isContainer = function () {
            return false;
        };

        // Finds alias if present representing this symbol
        PullSymbol.prototype.findAliasedType = function (scopeSymbol, skipScopeSymbolAliasesLookIn, lookIntoOnlyExportedAlias, aliasSymbols, visitedScopeDeclarations) {
            if (typeof aliasSymbols === "undefined") { aliasSymbols = []; }
            if (typeof visitedScopeDeclarations === "undefined") { visitedScopeDeclarations = []; }
            var scopeDeclarations = scopeSymbol.getDeclarations();
            var scopeSymbolAliasesToLookIn = [];

            for (var i = 0; i < scopeDeclarations.length; i++) {
                var scopeDecl = scopeDeclarations[i];
                if (!TypeScript.ArrayUtilities.contains(visitedScopeDeclarations, scopeDecl)) {
                    visitedScopeDeclarations.push(scopeDecl);

                    var childDecls = scopeDecl.getChildDecls();
                    for (var j = 0; j < childDecls.length; j++) {
                        var childDecl = childDecls[j];
                        if (childDecl.kind === TypeScript.PullElementKind.TypeAlias && (!lookIntoOnlyExportedAlias || (childDecl.flags & 1 /* Exported */))) {
                            var symbol = childDecl.getSymbol();

                            if (PullContainerSymbol.usedAsSymbol(symbol, this) || (this.rootSymbol && PullContainerSymbol.usedAsSymbol(symbol, this.rootSymbol))) {
                                aliasSymbols.push(symbol);
                                return aliasSymbols;
                            }

                            if (!skipScopeSymbolAliasesLookIn && PullSymbol._isExternalModuleReferenceAlias(symbol) && (!symbol.assignedContainer().hasExportAssignment() || (symbol.assignedContainer().getExportAssignedContainerSymbol() && symbol.assignedContainer().getExportAssignedContainerSymbol().kind === TypeScript.PullElementKind.DynamicModule))) {
                                scopeSymbolAliasesToLookIn.push(symbol);
                            }
                        }
                    }
                }
            }

            for (var i = 0; i < scopeSymbolAliasesToLookIn.length; i++) {
                var scopeSymbolAlias = scopeSymbolAliasesToLookIn[i];

                aliasSymbols.push(scopeSymbolAlias);
                var result = this.findAliasedType(scopeSymbolAlias.assignedContainer().hasExportAssignment() ? scopeSymbolAlias.assignedContainer().getExportAssignedContainerSymbol() : scopeSymbolAlias.assignedContainer(), false, true, aliasSymbols, visitedScopeDeclarations);
                if (result) {
                    return result;
                }

                aliasSymbols.pop();
            }

            return null;
        };

        // Gets alias with external module reference if present representing this symbol
        PullSymbol.prototype.getExternalAliasedSymbols = function (scopeSymbol) {
            if (!scopeSymbol) {
                return null;
            }

            var scopePath = scopeSymbol.pathToRoot();
            if (scopePath.length && scopePath[scopePath.length - 1].kind === TypeScript.PullElementKind.DynamicModule) {
                var symbols = this.findAliasedType(scopePath[scopePath.length - 1]);
                return symbols;
            }

            return null;
        };

        PullSymbol._isExternalModuleReferenceAlias = function (aliasSymbol) {
            if (aliasSymbol) {
                // Has value symbol
                if (aliasSymbol.assignedValue()) {
                    return false;
                }

                // Has type that is not same as container
                if (aliasSymbol.assignedType() && aliasSymbol.assignedType() !== aliasSymbol.assignedContainer()) {
                    return false;
                }

                // Its internal module
                if (aliasSymbol.assignedContainer() && aliasSymbol.assignedContainer().kind !== TypeScript.PullElementKind.DynamicModule) {
                    return false;
                }

                return true;
            }

            return false;
        };

        // Gets exported alias with internal module reference if present representing this symbol
        PullSymbol.prototype.getExportedInternalAliasSymbol = function (scopeSymbol) {
            if (scopeSymbol) {
                if (this.kind !== TypeScript.PullElementKind.TypeAlias) {
                    var scopePath = scopeSymbol.pathToRoot();
                    for (var i = 0; i < scopePath.length; i++) {
                        var internalAliases = this.findAliasedType(scopeSymbol, true, true);
                        if (internalAliases) {
                            TypeScript.Debug.assert(internalAliases.length === 1);
                            return internalAliases[0];
                        }
                    }
                }
            }

            return null;
        };

        // Get alias Name using the name getter methods
        PullSymbol.prototype.getAliasSymbolName = function (scopeSymbol, aliasNameGetter, aliasPartsNameGetter, skipInternalAlias) {
            if (!skipInternalAlias) {
                var internalAlias = this.getExportedInternalAliasSymbol(scopeSymbol);
                if (internalAlias) {
                    return aliasNameGetter(internalAlias);
                }
            }

            var externalAliases = this.getExternalAliasedSymbols(scopeSymbol);

            // Use only alias symbols to the dynamic module
            if (externalAliases && PullSymbol._isExternalModuleReferenceAlias(externalAliases[externalAliases.length - 1])) {
                var aliasFullName = aliasNameGetter(externalAliases[0]);
                if (!aliasFullName) {
                    return null;
                }
                for (var i = 1, symbolsLen = externalAliases.length; i < symbolsLen; i++) {
                    aliasFullName = aliasFullName + "." + aliasPartsNameGetter(externalAliases[i]);
                }
                return aliasFullName;
            }

            return null;
        };

        PullSymbol.prototype._getResolver = function () {
            TypeScript.Debug.assert(this._declarations && this._declarations.length > 0);
            return this._declarations[0].semanticInfoChain.getResolver();
        };

        PullSymbol.prototype._resolveDeclaredSymbol = function () {
            return this._getResolver().resolveDeclaredSymbol(this);
        };

        /** Use getName for type checking purposes, and getDisplayName to report an error or display info to the user.
        * They will differ when the identifier is an escaped unicode character or the identifier "__proto__".
        */
        PullSymbol.prototype.getName = function (scopeSymbol, useConstraintInName) {
            var aliasName = this.getAliasSymbolName(scopeSymbol, function (symbol) {
                return symbol.getName(scopeSymbol, useConstraintInName);
            }, function (symbol) {
                return symbol.getName();
            });
            return aliasName || this.name;
        };

        PullSymbol.prototype.getDisplayName = function (scopeSymbol, useConstraintInName, skipInternalAliasName) {
            var aliasDisplayName = this.getAliasSymbolName(scopeSymbol, function (symbol) {
                return symbol.getDisplayName(scopeSymbol, useConstraintInName);
            }, function (symbol) {
                return symbol.getDisplayName();
            }, skipInternalAliasName);
            if (aliasDisplayName) {
                return aliasDisplayName;
            }

            // Get the actual name associated with a declaration for this symbol
            var decls = this.getDeclarations();
            var name = decls.length && decls[0].getDisplayName();

            // In case the decl does not have a name like in the case of named function expression
            return (name && name.length) ? name : this.name;
        };

        PullSymbol.prototype.getIsSpecialized = function () {
            return false;
        };

        PullSymbol.prototype.getRootSymbol = function () {
            if (!this.rootSymbol) {
                return this;
            }
            return this.rootSymbol;
        };
        PullSymbol.prototype.setRootSymbol = function (symbol) {
            this.rootSymbol = symbol;
        };

        PullSymbol.prototype.setIsSynthesized = function (value) {
            if (typeof value === "undefined") { value = true; }
            TypeScript.Debug.assert(this.rootSymbol == null);
            this.isSynthesized = value;
        };

        PullSymbol.prototype.getIsSynthesized = function () {
            if (this.rootSymbol) {
                return this.rootSymbol.getIsSynthesized();
            }
            return this.isSynthesized;
        };

        PullSymbol.prototype.setEnclosingSignature = function (signature) {
            this._enclosingSignature = signature;
        };

        PullSymbol.prototype.getEnclosingSignature = function () {
            return this._enclosingSignature;
        };

        // declaration methods
        PullSymbol.prototype.addDeclaration = function (decl) {
            TypeScript.Debug.assert(!!decl);

            if (this.rootSymbol) {
                return;
            }

            if (!this._declarations) {
                this._declarations = [decl];
            } else {
                this._declarations[this._declarations.length] = decl;
            }
        };

        PullSymbol.prototype.getDeclarations = function () {
            if (this.rootSymbol) {
                return this.rootSymbol.getDeclarations();
            }

            if (!this._declarations) {
                this._declarations = [];
            }

            return this._declarations;
        };

        PullSymbol.prototype.hasDeclaration = function (decl) {
            if (!this._declarations) {
                return false;
            }

            return TypeScript.ArrayUtilities.any(this._declarations, function (eachDecl) {
                return eachDecl === decl;
            });
        };

        // link methods
        PullSymbol.prototype.setContainer = function (containerSymbol) {
            if (this.rootSymbol) {
                return;
            }

            this._container = containerSymbol;
        };

        PullSymbol.prototype.getContainer = function () {
            if (this.rootSymbol) {
                return this.rootSymbol.getContainer();
            }

            return this._container;
        };

        PullSymbol.prototype.setResolved = function () {
            this.isResolved = true;
            this.inResolution = false;
        };

        PullSymbol.prototype.startResolving = function () {
            this.inResolution = true;
        };

        PullSymbol.prototype.setUnresolved = function () {
            this.isResolved = false;
            this.inResolution = false;
        };

        PullSymbol.prototype.anyDeclHasFlag = function (flag) {
            var declarations = this.getDeclarations();
            for (var i = 0, n = declarations.length; i < n; i++) {
                if (TypeScript.hasFlag(declarations[i].flags, flag)) {
                    return true;
                }
            }
            return false;
        };

        PullSymbol.prototype.allDeclsHaveFlag = function (flag) {
            var declarations = this.getDeclarations();
            for (var i = 0, n = declarations.length; i < n; i++) {
                if (!TypeScript.hasFlag(declarations[i].flags, flag)) {
                    return false;
                }
            }
            return true;
        };

        PullSymbol.prototype.pathToRoot = function () {
            var path = [];
            var node = this;
            while (node) {
                if (node.isType()) {
                    var associatedContainerSymbol = node.getAssociatedContainerType();
                    if (associatedContainerSymbol) {
                        node = associatedContainerSymbol;
                    }
                }
                path[path.length] = node;
                var nodeKind = node.kind;
                if (nodeKind === TypeScript.PullElementKind.Parameter) {
                    break;
                } else {
                    node = node.getContainer();
                }
            }
            return path;
        };

        PullSymbol.unqualifiedNameReferencesDifferentSymbolInScope = function (symbol, scopePath, endScopePathIndex) {
            // Declaration path is reverse of symbol path
            // That is for symbol A.B.C.W the symbolPath = W C B A
            // while declPath = A B C W
            var declPath = scopePath[0].getDeclarations()[0].getParentPath();
            for (var i = 0, declIndex = declPath.length - 1; i <= endScopePathIndex; i++, declIndex--) {
                // We should be doing this for all that is type/namespace/value kinds but for now we do this only for
                // containers and types in another container
                if (scopePath[i].isContainer()) {
                    var scopeContainer = scopePath[i];
                    if (symbol.isContainer()) {
                        // Non exported container
                        var memberSymbol = scopeContainer.findContainedNonMemberContainer(symbol.name, TypeScript.PullElementKind.SomeContainer);
                        if (memberSymbol && memberSymbol != symbol && memberSymbol.getDeclarations()[0].getParentDecl() == declPath[declIndex]) {
                            // If we found different non exported symbol that is originating in same parent
                            // So symbol with this name would refer to the memberSymbol instead of symbol
                            return true;
                        }

                        // Exported container
                        var memberSymbol = scopeContainer.findNestedContainer(symbol.name, TypeScript.PullElementKind.SomeContainer);
                        if (memberSymbol && memberSymbol != symbol) {
                            return true;
                        }
                    } else if (symbol.isType()) {
                        // Non exported type
                        var memberSymbol = scopeContainer.findContainedNonMemberType(symbol.name, TypeScript.PullElementKind.SomeType);
                        var symbolRootType = TypeScript.PullHelpers.getRootType(symbol);
                        if (memberSymbol && TypeScript.PullHelpers.getRootType(memberSymbol) != symbolRootType && memberSymbol.getDeclarations()[0].getParentDecl() == declPath[declIndex]) {
                            // If we found different non exported symbol that is originating in same parent
                            // So symbol with this name would refer to the memberSymbol instead of symbol
                            return true;
                        }

                        // Exported type
                        var memberSymbol = scopeContainer.findNestedType(symbol.name, TypeScript.PullElementKind.SomeType);
                        if (memberSymbol && TypeScript.PullHelpers.getRootType(memberSymbol) != symbolRootType) {
                            return true;
                        }
                    }
                }
            }

            return false;
        };

        // Find the path of this symbol in common ancestor of this symbol and b Symbol
        PullSymbol.prototype.findQualifyingSymbolPathInScopeSymbol = function (scopeSymbol) {
            var thisPath = this.pathToRoot();
            if (thisPath.length === 1) {
                // Global symbol
                return thisPath;
            }

            var scopeSymbolPath;
            if (scopeSymbol) {
                scopeSymbolPath = scopeSymbol.pathToRoot();
            } else {
                // If scopeSymbol wasnt provided, then the path is full path
                return thisPath;
            }

            var thisCommonAncestorIndex = TypeScript.ArrayUtilities.indexOf(thisPath, function (thisNode) {
                return TypeScript.ArrayUtilities.contains(scopeSymbolPath, thisNode);
            });
            if (thisCommonAncestorIndex > 0) {
                // If the symbols matched that does not mean we can use the symbol before the common ancestor directly.
                // e.g
                //module A.C {
                //    export interface Z {
                //    }
                //}
                //module A.B.C {
                //    export class W implements A.C.Z /*This*/ {
                //    }
                //}
                // When trying to get Name of A.C.Z in the context A.B.C.W
                // We find that thisPath = [Z C A] and scopeSymbolPath = [W C B A]
                // thisCommonAncestorIndex = 2
                // But we cant use (thisCommonAncestorIndex - 1)C of C.Z as the reference path because
                // C in A.B.C.W references A.B.C rather than A.C
                var thisCommonAncestor = thisPath[thisCommonAncestorIndex];
                var scopeCommonAncestorIndex = TypeScript.ArrayUtilities.indexOf(scopeSymbolPath, function (scopeNode) {
                    return scopeNode === thisCommonAncestor;
                });
                TypeScript.Debug.assert(thisPath.length - thisCommonAncestorIndex === scopeSymbolPath.length - scopeCommonAncestorIndex);

                for (; thisCommonAncestorIndex < thisPath.length; thisCommonAncestorIndex++, scopeCommonAncestorIndex++) {
                    if (!PullSymbol.unqualifiedNameReferencesDifferentSymbolInScope(thisPath[thisCommonAncestorIndex - 1], scopeSymbolPath, scopeCommonAncestorIndex)) {
                        break;
                    }
                }
            }

            if (thisCommonAncestorIndex >= 0 && thisCommonAncestorIndex < thisPath.length) {
                return thisPath.slice(0, thisCommonAncestorIndex);
            } else {
                return thisPath;
            }
        };

        PullSymbol.prototype.toString = function (scopeSymbol, useConstraintInName) {
            var str = this.getNameAndTypeName(scopeSymbol);
            return str;
        };

        PullSymbol.prototype.getNamePartForFullName = function () {
            return this.getDisplayName(null, true);
        };

        PullSymbol.prototype.fullName = function (scopeSymbol) {
            var _this = this;
            var path = this.pathToRoot();
            var fullName = "";

            var aliasFullName = this.getAliasSymbolName(scopeSymbol, function (symbol) {
                return symbol.fullName(scopeSymbol);
            }, function (symbol) {
                return symbol.getNamePartForFullName();
            });
            if (aliasFullName) {
                return aliasFullName;
            }

            for (var i = 1; i < path.length; i++) {
                var aliasFullName = path[i].getAliasSymbolName(scopeSymbol, function (symbol) {
                    return symbol === _this ? null : symbol.fullName(scopeSymbol);
                }, function (symbol) {
                    return symbol.getNamePartForFullName();
                });
                if (aliasFullName) {
                    fullName = aliasFullName + "." + fullName;
                    break;
                }

                var scopedName = path[i].getNamePartForFullName();
                if (path[i].kind === TypeScript.PullElementKind.DynamicModule && !TypeScript.isQuoted(scopedName)) {
                    break;
                }

                if (scopedName === "") {
                    break;
                }

                fullName = scopedName + "." + fullName;
            }

            fullName = fullName + this.getNamePartForFullName();
            return fullName;
        };

        PullSymbol.prototype.getScopedName = function (scopeSymbol, skipTypeParametersInName, useConstraintInName, skipInternalAliasName) {
            var path = this.findQualifyingSymbolPathInScopeSymbol(scopeSymbol);
            var fullName = "";

            var aliasFullName = this.getAliasSymbolName(scopeSymbol, function (symbol) {
                return symbol.getScopedName(scopeSymbol, skipTypeParametersInName, useConstraintInName, skipInternalAliasName);
            }, function (symbol) {
                return symbol.getNamePartForFullName();
            }, skipInternalAliasName);
            if (aliasFullName) {
                return aliasFullName;
            }

            for (var i = 1; i < path.length; i++) {
                var kind = path[i].kind;
                if (kind === TypeScript.PullElementKind.Container || kind === TypeScript.PullElementKind.DynamicModule) {
                    var aliasFullName = path[i].getAliasSymbolName(scopeSymbol, function (symbol) {
                        return symbol.getScopedName(scopeSymbol, skipTypeParametersInName, false, skipInternalAliasName);
                    }, function (symbol) {
                        return symbol.getNamePartForFullName();
                    }, skipInternalAliasName);
                    if (aliasFullName) {
                        fullName = aliasFullName + "." + fullName;
                        break;
                    }

                    if (kind === TypeScript.PullElementKind.Container) {
                        fullName = path[i].getDisplayName() + "." + fullName;
                    } else {
                        // Dynamic module
                        var displayName = path[i].getDisplayName();
                        if (TypeScript.isQuoted(displayName)) {
                            fullName = displayName + "." + fullName;
                        }
                        break;
                    }
                } else {
                    break;
                }
            }
            fullName = fullName + this.getDisplayName(scopeSymbol, useConstraintInName, skipInternalAliasName);
            return fullName;
        };

        PullSymbol.prototype.getScopedNameEx = function (scopeSymbol, skipTypeParametersInName, useConstraintInName, getPrettyTypeName, getTypeParamMarkerInfo, skipInternalAliasName) {
            var name = this.getScopedName(scopeSymbol, skipTypeParametersInName, useConstraintInName, skipInternalAliasName);
            return TypeScript.MemberName.create(name);
        };

        PullSymbol.prototype.getTypeName = function (scopeSymbol, getPrettyTypeName) {
            var memberName = this.getTypeNameEx(scopeSymbol, getPrettyTypeName);
            return memberName.toString();
        };

        PullSymbol.prototype.getTypeNameEx = function (scopeSymbol, getPrettyTypeName) {
            var type = this.type;
            if (type) {
                var memberName = getPrettyTypeName ? this.getTypeNameForFunctionSignature("", scopeSymbol, getPrettyTypeName) : null;
                if (!memberName) {
                    memberName = type.getScopedNameEx(scopeSymbol, false, true, getPrettyTypeName);
                }

                return memberName;
            }
            return TypeScript.MemberName.create("");
        };

        PullSymbol.prototype.getTypeNameForFunctionSignature = function (prefix, scopeSymbol, getPrettyTypeName) {
            var type = this.type;
            if (type && !type.isNamedTypeSymbol() && this.kind !== TypeScript.PullElementKind.Property && this.kind !== TypeScript.PullElementKind.Variable && this.kind !== TypeScript.PullElementKind.Parameter) {
                var signatures = type.getCallSignatures();
                if (signatures.length === 1 || (getPrettyTypeName && signatures.length)) {
                    var typeName = new TypeScript.MemberNameArray();
                    var signatureName = PullSignatureSymbol.getSignaturesTypeNameEx(signatures, prefix, false, false, scopeSymbol, getPrettyTypeName);
                    typeName.addAll(signatureName);
                    return typeName;
                }
            }

            return null;
        };

        PullSymbol.prototype.getNameAndTypeName = function (scopeSymbol) {
            var nameAndTypeName = this.getNameAndTypeNameEx(scopeSymbol);
            return nameAndTypeName.toString();
        };

        PullSymbol.prototype.getNameAndTypeNameEx = function (scopeSymbol) {
            var type = this.type;
            var nameStr = this.getDisplayName(scopeSymbol);
            if (type) {
                nameStr = nameStr + (this.isOptional ? "?" : "");
                var memberName = this.getTypeNameForFunctionSignature(nameStr, scopeSymbol);
                if (!memberName) {
                    var typeNameEx = type.getScopedNameEx(scopeSymbol);
                    memberName = TypeScript.MemberName.create(typeNameEx, nameStr + ": ", "");
                }
                return memberName;
            }
            return TypeScript.MemberName.create(nameStr);
        };

        PullSymbol.getTypeParameterString = function (typars, scopeSymbol, useContraintInName) {
            return PullSymbol.getTypeParameterStringEx(typars, scopeSymbol, undefined, useContraintInName).toString();
        };

        PullSymbol.getTypeParameterStringEx = function (typeParameters, scopeSymbol, getTypeParamMarkerInfo, useContraintInName) {
            var builder = new TypeScript.MemberNameArray();
            builder.prefix = "";

            if (typeParameters && typeParameters.length) {
                builder.add(TypeScript.MemberName.create("<"));

                for (var i = 0; i < typeParameters.length; i++) {
                    if (i) {
                        builder.add(TypeScript.MemberName.create(", "));
                    }

                    if (getTypeParamMarkerInfo) {
                        builder.add(new TypeScript.MemberName());
                    }

                    builder.add(typeParameters[i].getScopedNameEx(scopeSymbol, false, useContraintInName));

                    if (getTypeParamMarkerInfo) {
                        builder.add(new TypeScript.MemberName());
                    }
                }

                builder.add(TypeScript.MemberName.create(">"));
            }

            return builder;
        };

        PullSymbol.getIsExternallyVisible = function (symbol, fromIsExternallyVisibleSymbol, inIsExternallyVisibleSymbols) {
            if (inIsExternallyVisibleSymbols) {
                for (var i = 0; i < inIsExternallyVisibleSymbols.length; i++) {
                    if (inIsExternallyVisibleSymbols[i] === symbol) {
                        return true;
                    }
                }
            } else {
                inIsExternallyVisibleSymbols = [];
            }

            if (fromIsExternallyVisibleSymbol === symbol) {
                return true;
            }

            inIsExternallyVisibleSymbols.push(fromIsExternallyVisibleSymbol);

            var result = symbol.isExternallyVisible(inIsExternallyVisibleSymbols);

            TypeScript.Debug.assert(TypeScript.ArrayUtilities.last(inIsExternallyVisibleSymbols) === fromIsExternallyVisibleSymbol);
            inIsExternallyVisibleSymbols.pop();

            return result;
        };

        PullSymbol.prototype.isExternallyVisible = function (inIsExternallyVisibleSymbols) {
            // Primitive
            var kind = this.kind;
            if (kind === TypeScript.PullElementKind.Primitive) {
                return true;
            }

            if (this.rootSymbol) {
                return PullSymbol.getIsExternallyVisible(this.rootSymbol, this, inIsExternallyVisibleSymbols);
            }

            // Type - use container to determine privacy info
            if (this.isType()) {
                var associatedContainerSymbol = this.getAssociatedContainerType();
                if (associatedContainerSymbol) {
                    return PullSymbol.getIsExternallyVisible(associatedContainerSymbol, this, inIsExternallyVisibleSymbols);
                }
            }

            // Private member
            if (this.anyDeclHasFlag(TypeScript.PullElementFlags.Private)) {
                return false;
            }

            // If the container for this symbol is null, then this symbol is visible
            var container = this.getContainer();
            if (container === null) {
                var decls = this.getDeclarations();
                if (decls.length) {
                    var parentDecl = decls[0].getParentDecl();
                    if (parentDecl) {
                        var parentSymbol = parentDecl.getSymbol();
                        if (!parentSymbol || parentDecl.kind === TypeScript.PullElementKind.Script) {
                            return true;
                        }

                        return PullSymbol.getIsExternallyVisible(parentSymbol, this, inIsExternallyVisibleSymbols);
                    }
                }

                return true;
            }

            // If export assignment check if this is the symbol that is exported
            if (container.kind === TypeScript.PullElementKind.DynamicModule || (container.getAssociatedContainerType() && container.getAssociatedContainerType().kind === TypeScript.PullElementKind.DynamicModule)) {
                var containerSymbol = container.kind === TypeScript.PullElementKind.DynamicModule ? container : container.getAssociatedContainerType();
                if (PullContainerSymbol.usedAsSymbol(containerSymbol, this)) {
                    return true;
                }
            }

            // If non exported member and is not class properties and method, it is not visible
            if (!this.anyDeclHasFlag(1 /* Exported */) && kind !== TypeScript.PullElementKind.Property && kind !== TypeScript.PullElementKind.Method) {
                return false;
            }

            // Visible if parent is visible
            return PullSymbol.getIsExternallyVisible(container, this, inIsExternallyVisibleSymbols);
        };

        PullSymbol.prototype.getDocCommentsOfDecl = function (decl) {
            var ast = decl.ast();

            if (ast) {
                var enclosingModuleDeclaration = TypeScript.ASTHelpers.getEnclosingModuleDeclaration(ast);
                if (TypeScript.ASTHelpers.isLastNameOfModule(enclosingModuleDeclaration, ast)) {
                    return TypeScript.ASTHelpers.docComments(enclosingModuleDeclaration);
                }

                if (ast.kind() !== 130 /* ModuleDeclaration */ || decl.kind !== TypeScript.PullElementKind.Variable) {
                    return TypeScript.ASTHelpers.docComments(ast);
                }
            }

            return [];
        };

        PullSymbol.prototype.getDocCommentArray = function (symbol) {
            var docComments = [];
            if (!symbol) {
                return docComments;
            }

            var isParameter = symbol.kind === TypeScript.PullElementKind.Parameter;
            var decls = symbol.getDeclarations();
            for (var i = 0; i < decls.length; i++) {
                if (isParameter && decls[i].kind === TypeScript.PullElementKind.Property) {
                    continue;
                }
                docComments = docComments.concat(this.getDocCommentsOfDecl(decls[i]));
            }
            return docComments;
        };

        PullSymbol.getDefaultConstructorSymbolForDocComments = function (classSymbol) {
            if (classSymbol.getHasDefaultConstructor()) {
                // get from parent if possible
                var extendedTypes = classSymbol.getExtendedTypes();
                if (extendedTypes.length) {
                    return PullSymbol.getDefaultConstructorSymbolForDocComments(extendedTypes[0]);
                }
            }

            return classSymbol.type.getConstructSignatures()[0];
        };

        PullSymbol.prototype.getDocCommentText = function (comments) {
            var docCommentText = new Array();
            for (var c = 0; c < comments.length; c++) {
                var commentText = this.getDocCommentTextValue(comments[c]);
                if (commentText !== "") {
                    docCommentText.push(commentText);
                }
            }
            return docCommentText.join("\n");
        };

        PullSymbol.prototype.getDocCommentTextValue = function (comment) {
            return this.cleanJSDocComment(comment.fullText());
        };

        PullSymbol.prototype.docComments = function (useConstructorAsClass) {
            var decls = this.getDeclarations();
            if (useConstructorAsClass && decls.length && decls[0].kind === TypeScript.PullElementKind.ConstructorMethod) {
                var classDecl = decls[0].getParentDecl();
                return this.getDocCommentText(this.getDocCommentsOfDecl(classDecl));
            }

            if (this._docComments === null) {
                var docComments = "";
                if (!useConstructorAsClass && this.kind === TypeScript.PullElementKind.ConstructSignature && decls.length && decls[0].kind === TypeScript.PullElementKind.Class) {
                    var classSymbol = this.returnType;
                    var extendedTypes = classSymbol.getExtendedTypes();
                    if (extendedTypes.length) {
                        docComments = extendedTypes[0].getConstructorMethod().docComments();
                    } else {
                        docComments = "";
                    }
                } else if (this.kind === TypeScript.PullElementKind.Parameter) {
                    var parameterComments = [];

                    var funcContainer = this.getEnclosingSignature();
                    var funcDocComments = this.getDocCommentArray(funcContainer);
                    var paramComment = this.getParameterDocCommentText(this.getDisplayName(), funcDocComments);
                    if (paramComment != "") {
                        parameterComments.push(paramComment);
                    }

                    var paramSelfComment = this.getDocCommentText(this.getDocCommentArray(this));
                    if (paramSelfComment != "") {
                        parameterComments.push(paramSelfComment);
                    }
                    docComments = parameterComments.join("\n");
                } else {
                    var getSymbolComments = true;
                    if (this.kind === TypeScript.PullElementKind.FunctionType) {
                        var functionSymbol = this.getFunctionSymbol();

                        if (functionSymbol) {
                            docComments = functionSymbol._docComments || "";
                            getSymbolComments = false;
                        } else {
                            var declarationList = this.getDeclarations();
                            if (declarationList.length > 0) {
                                docComments = declarationList[0].getSymbol()._docComments || "";
                                getSymbolComments = false;
                            }
                        }
                    }
                    if (getSymbolComments) {
                        docComments = this.getDocCommentText(this.getDocCommentArray(this));
                        if (docComments === "") {
                            if (this.kind === TypeScript.PullElementKind.CallSignature) {
                                var callTypeSymbol = this.functionType;
                                if (callTypeSymbol && callTypeSymbol.getCallSignatures().length === 1) {
                                    docComments = callTypeSymbol.docComments();
                                }
                            }
                        }
                    }
                }

                this._docComments = docComments;
            }

            return this._docComments;
        };

        PullSymbol.prototype.getParameterDocCommentText = function (param, fncDocComments) {
            if (fncDocComments.length === 0 || fncDocComments[0].kind() !== 6 /* MultiLineCommentTrivia */) {
                // there were no fnc doc comments and the comment is not block comment then it cannot have
                // @param comment that can be parsed
                return "";
            }

            for (var i = 0; i < fncDocComments.length; i++) {
                var commentContents = fncDocComments[i].fullText();
                for (var j = commentContents.indexOf("@param", 0); 0 <= j; j = commentContents.indexOf("@param", j)) {
                    j += 6;
                    if (!this.isSpaceChar(commentContents, j)) {
                        continue;
                    }

                    // This is param tag. Check if it is what we are looking for
                    j = this.consumeLeadingSpace(commentContents, j);
                    if (j === -1) {
                        break;
                    }

                    // Ignore the type expression
                    if (commentContents.charCodeAt(j) === 123 /* openBrace */) {
                        j++;

                        // Consume the type
                        var charCode = 0;
                        for (var curlies = 1; j < commentContents.length; j++) {
                            charCode = commentContents.charCodeAt(j);

                            // { character means we need to find another } to match the found one
                            if (charCode === 123 /* openBrace */) {
                                curlies++;
                                continue;
                            }

                            // } char
                            if (charCode === 125 /* closeBrace */) {
                                curlies--;
                                if (curlies === 0) {
                                    break;
                                } else {
                                    continue;
                                }
                            }

                            // Found start of another tag
                            if (charCode === 64 /* at */) {
                                break;
                            }
                        }

                        // End of the comment
                        if (j === commentContents.length) {
                            break;
                        }

                        // End of the tag, go onto looking for next tag
                        if (charCode === 64 /* at */) {
                            continue;
                        }

                        j = this.consumeLeadingSpace(commentContents, j + 1);
                        if (j === -1) {
                            break;
                        }
                    }

                    // Parameter name
                    if (param !== commentContents.substr(j, param.length) || !this.isSpaceChar(commentContents, j + param.length)) {
                        continue;
                    }

                    // Found the parameter we were looking for
                    j = this.consumeLeadingSpace(commentContents, j + param.length);
                    if (j === -1) {
                        return "";
                    }

                    var endOfParam = commentContents.indexOf("@", j);
                    var paramHelpString = commentContents.substring(j, endOfParam < 0 ? commentContents.length : endOfParam);

                    // Find alignement spaces to remove
                    var paramSpacesToRemove = undefined;
                    var paramLineIndex = commentContents.substring(0, j).lastIndexOf("\n") + 1;
                    if (paramLineIndex !== 0) {
                        if (paramLineIndex < j && commentContents.charAt(paramLineIndex + 1) === "\r") {
                            paramLineIndex++;
                        }
                    }
                    var startSpaceRemovalIndex = this.consumeLeadingSpace(commentContents, paramLineIndex);
                    if (startSpaceRemovalIndex !== j && commentContents.charAt(startSpaceRemovalIndex) === "*") {
                        paramSpacesToRemove = j - startSpaceRemovalIndex - 1;
                    }

                    // Clean jsDocComment and return
                    return this.cleanJSDocComment(paramHelpString, paramSpacesToRemove);
                }
            }

            return "";
        };

        PullSymbol.prototype.cleanJSDocComment = function (content, spacesToRemove) {
            var docCommentLines = new Array();
            content = content.replace("/**", ""); // remove /**
            if (content.length >= 2 && content.charAt(content.length - 1) === "/" && content.charAt(content.length - 2) === "*") {
                content = content.substring(0, content.length - 2); // remove last */
            }
            var lines = content.split("\n");
            var inParamTag = false;
            for (var l = 0; l < lines.length; l++) {
                var line = lines[l];
                var cleanLinePos = this.cleanDocCommentLine(line, true, spacesToRemove);
                if (!cleanLinePos) {
                    continue;
                }

                var docCommentText = "";
                var prevPos = cleanLinePos.start;
                for (var i = line.indexOf("@", cleanLinePos.start); 0 <= i && i < cleanLinePos.end; i = line.indexOf("@", i + 1)) {
                    // We have encoutered @.
                    // If we were omitting param comment, we dont have to do anything
                    // other wise the content of the text till @ tag goes as doc comment
                    var wasInParamtag = inParamTag;

                    // Parse contents next to @
                    if (line.indexOf("param", i + 1) === i + 1 && this.isSpaceChar(line, i + 6)) {
                        // It is param tag.
                        // If we were not in param tag earlier, push the contents from prev pos of the tag this tag start as docComment
                        if (!wasInParamtag) {
                            docCommentText += line.substring(prevPos, i);
                        }

                        // New start of contents
                        prevPos = i;
                        inParamTag = true;
                    } else if (wasInParamtag) {
                        // Non param tag start
                        prevPos = i;
                        inParamTag = false;
                    }
                }

                if (!inParamTag) {
                    docCommentText += line.substring(prevPos, cleanLinePos.end);
                }

                // Add line to comment text if it is not only white space line
                var newCleanPos = this.cleanDocCommentLine(docCommentText, false);
                if (newCleanPos) {
                    if (spacesToRemove === undefined) {
                        spacesToRemove = cleanLinePos.jsDocSpacesRemoved;
                    }
                    docCommentLines.push(docCommentText);
                }
            }

            return docCommentLines.join("\n");
        };

        PullSymbol.prototype.consumeLeadingSpace = function (line, startIndex, maxSpacesToRemove) {
            var endIndex = line.length;
            if (maxSpacesToRemove !== undefined) {
                endIndex = TypeScript.MathPrototype.min(startIndex + maxSpacesToRemove, endIndex);
            }

            for (; startIndex < endIndex; startIndex++) {
                var charCode = line.charCodeAt(startIndex);
                if (charCode !== 32 /* space */ && charCode !== 9 /* tab */) {
                    return startIndex;
                }
            }

            if (endIndex !== line.length) {
                return endIndex;
            }

            return -1;
        };

        PullSymbol.prototype.isSpaceChar = function (line, index) {
            var length = line.length;
            if (index < length) {
                var charCode = line.charCodeAt(index);

                // If the character is space
                return charCode === 32 /* space */ || charCode === 9 /* tab */;
            }

            // If the index is end of the line it is space
            return index === length;
        };

        PullSymbol.prototype.cleanDocCommentLine = function (line, jsDocStyleComment, jsDocLineSpaceToRemove) {
            var nonSpaceIndex = this.consumeLeadingSpace(line, 0);
            if (nonSpaceIndex !== -1) {
                var jsDocSpacesRemoved = nonSpaceIndex;
                if (jsDocStyleComment && line.charAt(nonSpaceIndex) === '*') {
                    var startIndex = nonSpaceIndex + 1;
                    nonSpaceIndex = this.consumeLeadingSpace(line, startIndex, jsDocLineSpaceToRemove);

                    if (nonSpaceIndex !== -1) {
                        jsDocSpacesRemoved = nonSpaceIndex - startIndex;
                    } else {
                        return null;
                    }
                }

                return {
                    start: nonSpaceIndex,
                    end: line.charAt(line.length - 1) === "\r" ? line.length - 1 : line.length,
                    jsDocSpacesRemoved: jsDocSpacesRemoved
                };
            }

            return null;
        };
        return PullSymbol;
    })();
    TypeScript.PullSymbol = PullSymbol;

    

    var PullSignatureSymbol = (function (_super) {
        __extends(PullSignatureSymbol, _super);
        function PullSignatureSymbol(kind, _isDefinition) {
            if (typeof _isDefinition === "undefined") { _isDefinition = false; }
            _super.call(this, "", kind);
            this._isDefinition = _isDefinition;
            this._memberTypeParameterNameCache = null;
            this._stringConstantOverload = undefined;
            this.parameters = TypeScript.sentinelEmptyArray;
            this._typeParameters = null;
            this.returnType = null;
            this.functionType = null;
            this.hasOptionalParam = false;
            this.nonOptionalParamCount = 0;
            this.hasVarArgs = false;
            this._allowedToReferenceTypeParameters = null;
            this._instantiationCache = null;
            this.hasBeenChecked = false;
            this.inWrapCheck = false;
            this.inWrapInfiniteExpandingReferenceCheck = false;
        }
        PullSignatureSymbol.prototype.isDefinition = function () {
            return this._isDefinition;
        };

        // GTODO
        PullSignatureSymbol.prototype.isGeneric = function () {
            var typeParameters = this.getTypeParameters();
            return !!typeParameters && typeParameters.length !== 0;
        };

        PullSignatureSymbol.prototype.addParameter = function (parameter, isOptional) {
            if (typeof isOptional === "undefined") { isOptional = false; }
            if (this.parameters === TypeScript.sentinelEmptyArray) {
                this.parameters = [];
            }

            this.parameters[this.parameters.length] = parameter;
            this.hasOptionalParam = isOptional;

            if (!parameter.getEnclosingSignature()) {
                parameter.setEnclosingSignature(this);
            }

            if (!isOptional) {
                this.nonOptionalParamCount++;
            }
        };

        PullSignatureSymbol.prototype.addTypeParameter = function (typeParameter) {
            if (!this._typeParameters) {
                this._typeParameters = [];
            }

            if (!this._memberTypeParameterNameCache) {
                this._memberTypeParameterNameCache = TypeScript.createIntrinsicsObject();
            }

            this._typeParameters[this._typeParameters.length] = typeParameter;

            this._memberTypeParameterNameCache[typeParameter.getName()] = typeParameter;
        };

        PullSignatureSymbol.prototype.addTypeParametersFromReturnType = function () {
            var typeParameters = this.returnType.getTypeParameters();
            for (var i = 0; i < typeParameters.length; i++) {
                this.addTypeParameter(typeParameters[i]);
            }
        };

        PullSignatureSymbol.prototype.getTypeParameters = function () {
            if (!this._typeParameters) {
                this._typeParameters = [];
            }

            return this._typeParameters;
        };

        PullSignatureSymbol.prototype.findTypeParameter = function (name) {
            var memberSymbol;

            if (!this._memberTypeParameterNameCache) {
                this._memberTypeParameterNameCache = TypeScript.createIntrinsicsObject();

                for (var i = 0; i < this.getTypeParameters().length; i++) {
                    this._memberTypeParameterNameCache[this._typeParameters[i].getName()] = this._typeParameters[i];
                }
            }

            memberSymbol = this._memberTypeParameterNameCache[name];

            return memberSymbol;
        };

        PullSignatureSymbol.prototype.getTypeParameterArgumentMap = function () {
            return null;
        };

        PullSignatureSymbol.prototype.getAllowedToReferenceTypeParameters = function () {
            TypeScript.Debug.assert(this.getRootSymbol() == this);
            if (!this._allowedToReferenceTypeParameters) {
                // If the type is not named, it cannot have its own type parameters
                // But it can refer to typeParameters from enclosing type
                this._allowedToReferenceTypeParameters = TypeScript.PullInstantiationHelpers.getAllowedToReferenceTypeParametersFromDecl(this.getDeclarations()[0]);
            }

            return this._allowedToReferenceTypeParameters;
        };

        PullSignatureSymbol.prototype.addSpecialization = function (specializedVersionOfThisSignature, typeArgumentMap) {
            TypeScript.Debug.assert(this.getRootSymbol() == this);
            if (!this._instantiationCache) {
                this._instantiationCache = TypeScript.createIntrinsicsObject();
            }

            this._instantiationCache[getIDForTypeSubstitutions(this, typeArgumentMap)] = specializedVersionOfThisSignature;
        };

        PullSignatureSymbol.prototype.getSpecialization = function (typeArgumentMap) {
            TypeScript.Debug.assert(this.getRootSymbol() == this);
            if (!this._instantiationCache) {
                return null;
            }

            var result = this._instantiationCache[getIDForTypeSubstitutions(this, typeArgumentMap)];
            return result || null;
        };

        PullSignatureSymbol.prototype.isStringConstantOverloadSignature = function () {
            if (this._stringConstantOverload === undefined) {
                var params = this.parameters;
                this._stringConstantOverload = false;
                for (var i = 0; i < params.length; i++) {
                    var paramType = params[i].type;
                    if (paramType && paramType.isPrimitive() && paramType.isStringConstant()) {
                        this._stringConstantOverload = true;
                    }
                }
            }

            return this._stringConstantOverload;
        };

        // Gets the type of parameter at given index
        PullSignatureSymbol.prototype.getParameterTypeAtIndex = function (iParam) {
            if (iParam < this.parameters.length - 1 || (iParam < this.parameters.length && !this.hasVarArgs)) {
                return this.parameters[iParam].type;
            } else if (this.hasVarArgs) {
                var paramType = this.parameters[this.parameters.length - 1].type;
                if (paramType.isArrayNamedTypeReference()) {
                    paramType = paramType.getElementType();
                }
                return paramType;
            }

            return null;
        };

        PullSignatureSymbol.getSignatureTypeMemberName = function (candidateSignature, signatures, scopeSymbol) {
            var allMemberNames = new TypeScript.MemberNameArray();
            var signatureMemberName = PullSignatureSymbol.getSignaturesTypeNameEx(signatures, "", false, false, scopeSymbol, true, candidateSignature);
            allMemberNames.addAll(signatureMemberName);
            return allMemberNames;
        };

        PullSignatureSymbol.getSignaturesTypeNameEx = function (signatures, prefix, shortform, brackets, scopeSymbol, getPrettyTypeName, candidateSignature) {
            var result = [];
            if (!signatures) {
                return result;
            }

            var len = signatures.length;
            if (!getPrettyTypeName && len > 1) {
                shortform = false;
            }

            var foundDefinition = false;
            if (candidateSignature && candidateSignature.isDefinition() && len > 1) {
                // Overloaded signature with candidateSignature = definition - cannot be used.
                candidateSignature = null;
            }

            for (var i = 0; i < len; i++) {
                // the definition signature shouldn't be printed if there are overloads
                if (len > 1 && signatures[i].isDefinition()) {
                    foundDefinition = true;
                    continue;
                }

                var signature = signatures[i];
                if (getPrettyTypeName && candidateSignature) {
                    signature = candidateSignature;
                }

                result.push(signature.getSignatureTypeNameEx(prefix, shortform, brackets, scopeSymbol));
                if (getPrettyTypeName) {
                    break;
                }
            }

            if (getPrettyTypeName && result.length && len > 1) {
                var lastMemberName = result[result.length - 1];
                for (var i = i + 1; i < len; i++) {
                    if (signatures[i].isDefinition()) {
                        foundDefinition = true;
                        break;
                    }
                }
                var overloadString = TypeScript.getLocalizedText(TypeScript.DiagnosticCode._0_overload_s, [foundDefinition ? len - 2 : len - 1]);
                lastMemberName.add(TypeScript.MemberName.create(overloadString));
            }

            return result;
        };

        PullSignatureSymbol.prototype.toString = function (scopeSymbol, useConstraintInName) {
            var s = this.getSignatureTypeNameEx(this.getScopedNameEx().toString(), false, false, scopeSymbol, undefined, useConstraintInName).toString();
            return s;
        };

        PullSignatureSymbol.prototype.getSignatureTypeNameEx = function (prefix, shortform, brackets, scopeSymbol, getParamMarkerInfo, getTypeParamMarkerInfo) {
            var typeParamterBuilder = new TypeScript.MemberNameArray();

            typeParamterBuilder.add(PullSymbol.getTypeParameterStringEx(this.getTypeParameters(), scopeSymbol, getTypeParamMarkerInfo, true));

            if (brackets) {
                typeParamterBuilder.add(TypeScript.MemberName.create("["));
            } else {
                typeParamterBuilder.add(TypeScript.MemberName.create("("));
            }

            var builder = new TypeScript.MemberNameArray();
            builder.prefix = prefix;

            if (getTypeParamMarkerInfo) {
                builder.prefix = prefix;
                builder.addAll(typeParamterBuilder.entries);
            } else {
                builder.prefix = prefix + typeParamterBuilder.toString();
            }

            var params = this.parameters;
            var paramLen = params.length;
            for (var i = 0; i < paramLen; i++) {
                var paramType = params[i].type;
                var typeString = paramType ? ": " : "";
                var paramIsVarArg = params[i].isVarArg;
                var varArgPrefix = paramIsVarArg ? "..." : "";
                var optionalString = (!paramIsVarArg && params[i].isOptional) ? "?" : "";
                if (getParamMarkerInfo) {
                    builder.add(new TypeScript.MemberName());
                }
                builder.add(TypeScript.MemberName.create(varArgPrefix + params[i].getScopedNameEx(scopeSymbol).toString() + optionalString + typeString));
                if (paramType) {
                    builder.add(paramType.getScopedNameEx(scopeSymbol));
                }
                if (getParamMarkerInfo) {
                    builder.add(new TypeScript.MemberName());
                }
                if (i < paramLen - 1) {
                    builder.add(TypeScript.MemberName.create(", "));
                }
            }

            if (shortform) {
                if (brackets) {
                    builder.add(TypeScript.MemberName.create("] => "));
                } else {
                    builder.add(TypeScript.MemberName.create(") => "));
                }
            } else {
                if (brackets) {
                    builder.add(TypeScript.MemberName.create("]: "));
                } else {
                    builder.add(TypeScript.MemberName.create("): "));
                }
            }

            if (this.returnType) {
                builder.add(this.returnType.getScopedNameEx(scopeSymbol));
            } else {
                builder.add(TypeScript.MemberName.create("any"));
            }

            return builder;
        };

        PullSignatureSymbol.prototype.forAllParameterTypes = function (length, predicate) {
            if (this.parameters.length < length && !this.hasVarArgs) {
                length = this.parameters.length;
            }

            for (var i = 0; i < length; i++) {
                var paramType = this.getParameterTypeAtIndex(i);
                if (!predicate(paramType, i)) {
                    return false;
                }
            }

            return true;
        };

        PullSignatureSymbol.prototype.forAllCorrespondingParameterTypesInThisAndOtherSignature = function (otherSignature, predicate) {
            // First determine the length
            var length;
            if (this.hasVarArgs) {
                length = otherSignature.hasVarArgs ? Math.max(this.parameters.length, otherSignature.parameters.length) : otherSignature.parameters.length;
            } else {
                length = otherSignature.hasVarArgs ? this.parameters.length : Math.min(this.parameters.length, otherSignature.parameters.length);
            }

            for (var i = 0; i < length; i++) {
                // Finally, call the callback using the knowledge of whether each param is a rest param
                var thisParamType = this.getParameterTypeAtIndex(i);
                var otherParamType = otherSignature.getParameterTypeAtIndex(i);
                if (!predicate(thisParamType, otherParamType, i)) {
                    return false;
                }
            }

            return true;
        };

        PullSignatureSymbol.prototype.wrapsSomeTypeParameter = function (typeParameterArgumentMap) {
            return this.getWrappingTypeParameterID(typeParameterArgumentMap) !== 0;
        };

        PullSignatureSymbol.prototype.getWrappingTypeParameterID = function (typeParameterArgumentMap) {
            var signature = this;
            if (signature.inWrapCheck) {
                return 0;
            }

            // Find result from cache
            this._wrapsTypeParameterCache = this._wrapsTypeParameterCache || new TypeScript.WrapsTypeParameterCache();

            var wrappingTypeParameterID = this._wrapsTypeParameterCache.getWrapsTypeParameter(typeParameterArgumentMap);
            if (wrappingTypeParameterID === undefined) {
                wrappingTypeParameterID = this.getWrappingTypeParameterIDWorker(typeParameterArgumentMap);
                this._wrapsTypeParameterCache.setWrapsTypeParameter(typeParameterArgumentMap, wrappingTypeParameterID);
            }

            return wrappingTypeParameterID;
        };

        PullSignatureSymbol.prototype.getWrappingTypeParameterIDWorker = function (typeParameterArgumentMap) {
            var signature = this;
            signature.inWrapCheck = true;
            TypeScript.PullHelpers.resolveDeclaredSymbolToUseType(signature);
            var wrappingTypeParameterID = signature.returnType ? signature.returnType.getWrappingTypeParameterID(typeParameterArgumentMap) : 0;

            // Continue iterating over parameter types to find if they wrap type parameter,
            // only until we find the first wrapping Type parameter
            var parameters = signature.parameters;
            for (var i = 0; !wrappingTypeParameterID && i < parameters.length; i++) {
                TypeScript.PullHelpers.resolveDeclaredSymbolToUseType(parameters[i]);
                wrappingTypeParameterID = parameters[i].type.getWrappingTypeParameterID(typeParameterArgumentMap);
            }

            signature.inWrapCheck = false;

            return wrappingTypeParameterID;
        };

        PullSignatureSymbol.prototype._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReference = function (enclosingType, knownWrapMap) {
            var wrapsIntoInfinitelyExpandingTypeReference = knownWrapMap.valueAt(this.pullSymbolID, enclosingType.pullSymbolID);
            if (wrapsIntoInfinitelyExpandingTypeReference != undefined) {
                return wrapsIntoInfinitelyExpandingTypeReference;
            }

            if (this.inWrapInfiniteExpandingReferenceCheck) {
                return false;
            }

            this.inWrapInfiniteExpandingReferenceCheck = true;
            wrapsIntoInfinitelyExpandingTypeReference = this._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceWorker(enclosingType, knownWrapMap);
            knownWrapMap.setValueAt(this.pullSymbolID, enclosingType.pullSymbolID, wrapsIntoInfinitelyExpandingTypeReference);
            this.inWrapInfiniteExpandingReferenceCheck = false;

            return wrapsIntoInfinitelyExpandingTypeReference;
        };

        PullSignatureSymbol.prototype._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceWorker = function (enclosingType, knownWrapMap) {
            if (this.returnType && this.returnType._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceRecurse(enclosingType, knownWrapMap)) {
                return true;
            }

            var parameters = this.parameters;

            for (var i = 0; i < parameters.length; i++) {
                if (parameters[i].type && parameters[i].type._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceRecurse(enclosingType, knownWrapMap)) {
                    return true;
                }
            }

            return false;
        };
        return PullSignatureSymbol;
    })(PullSymbol);
    TypeScript.PullSignatureSymbol = PullSignatureSymbol;

    var PullTypeSymbol = (function (_super) {
        __extends(PullTypeSymbol, _super);
        function PullTypeSymbol(name, kind) {
            _super.call(this, name, kind);
            this._members = TypeScript.sentinelEmptyArray;
            this._enclosedMemberTypes = null;
            this._enclosedMemberContainers = null;
            this._typeParameters = null;
            this._allowedToReferenceTypeParameters = null;
            this._specializedVersionsOfThisType = null;
            this._arrayVersionOfThisType = null;
            this._implementedTypes = null;
            this._extendedTypes = null;
            this._typesThatExplicitlyImplementThisType = null;
            this._typesThatExtendThisType = null;
            this._callSignatures = null;
            this._allCallSignatures = null;
            this._constructSignatures = null;
            this._allConstructSignatures = null;
            this._indexSignatures = null;
            this._allIndexSignatures = null;
            this._allIndexSignaturesOfAugmentedType = null;
            this._memberNameCache = null;
            this._enclosedTypeNameCache = null;
            this._enclosedContainerCache = null;
            this._typeParameterNameCache = null;
            this._containedNonMemberNameCache = null;
            this._containedNonMemberTypeNameCache = null;
            this._containedNonMemberContainerCache = null;
            // The instanatiation cache we use when we are instantiating this type with a single
            // non-object type.
            this._simpleInstantiationCache = null;
            // The instantiation cache we use in all other circumstances.  i.e. instantiating with
            // multiple types, or instantiating with object types.
            this._complexInstantiationCache = null;
            // GTODO
            this._hasGenericSignature = false;
            this._hasGenericMember = false;
            this._hasBaseTypeConflict = false;
            this._knownBaseTypeCount = 0;
            this._associatedContainerTypeSymbol = null;
            this._constructorMethod = null;
            this._hasDefaultConstructor = false;
            // TODO: Really only used to track doc comments...
            this._functionSymbol = null;
            this._inMemberTypeNameEx = false;
            this.inSymbolPrivacyCheck = false;
            this.inWrapCheck = false;
            this.inWrapInfiniteExpandingReferenceCheck = false;
            this.typeReference = null;
            this._widenedType = null;
            // Returns true if this is type reference to Array<T>.  Note that because this is a type
            // reference, it will have type arguments, not type parameters.
            this._isArrayNamedTypeReference = undefined;
            this.type = this;
        }
        PullTypeSymbol.prototype.isArrayNamedTypeReference = function () {
            if (this._isArrayNamedTypeReference === undefined) {
                this._isArrayNamedTypeReference = this.computeIsArrayNamedTypeReference();
            }

            return this._isArrayNamedTypeReference;
        };

        PullTypeSymbol.prototype.computeIsArrayNamedTypeReference = function () {
            var typeArgs = this.getTypeArguments();
            if (typeArgs && this.getTypeArguments().length === 1 && this.name === "Array") {
                var declaration = this.getDeclarations()[0];

                // If we're a child of the global module (i.e. we have a parent decl, but our
                // parent has no parent), then we're the Array<T> type.
                if (declaration && declaration.getParentDecl() && declaration.getParentDecl().getParentDecl() === null) {
                    return true;
                }
            }

            return false;
        };

        PullTypeSymbol.prototype.isType = function () {
            return true;
        };
        PullTypeSymbol.prototype.isClass = function () {
            return this.kind === TypeScript.PullElementKind.Class || (this._constructorMethod !== null);
        };
        PullTypeSymbol.prototype.isFunction = function () {
            return (this.kind & (TypeScript.PullElementKind.ConstructorType | TypeScript.PullElementKind.FunctionType)) !== 0;
        };
        PullTypeSymbol.prototype.isConstructor = function () {
            return this.kind === TypeScript.PullElementKind.ConstructorType || (this._associatedContainerTypeSymbol && this._associatedContainerTypeSymbol.isClass());
        };
        PullTypeSymbol.prototype.isTypeParameter = function () {
            return false;
        };
        PullTypeSymbol.prototype.isTypeVariable = function () {
            return false;
        };
        PullTypeSymbol.prototype.isError = function () {
            return false;
        };
        PullTypeSymbol.prototype.isEnum = function () {
            return this.kind === TypeScript.PullElementKind.Enum;
        };

        PullTypeSymbol.prototype.getTypeParameterArgumentMap = function () {
            return null;
        };

        PullTypeSymbol.prototype.isObject = function () {
            return TypeScript.hasFlag(this.kind, TypeScript.PullElementKind.Class | TypeScript.PullElementKind.ConstructorType | TypeScript.PullElementKind.Enum | TypeScript.PullElementKind.FunctionType | TypeScript.PullElementKind.Interface | TypeScript.PullElementKind.ObjectType | TypeScript.PullElementKind.ObjectLiteral);
        };

        PullTypeSymbol.prototype.isFunctionType = function () {
            return this.getCallSignatures().length > 0 || this.getConstructSignatures().length > 0;
        };

        PullTypeSymbol.prototype.getKnownBaseTypeCount = function () {
            return this._knownBaseTypeCount;
        };
        PullTypeSymbol.prototype.resetKnownBaseTypeCount = function () {
            this._knownBaseTypeCount = 0;
        };
        PullTypeSymbol.prototype.incrementKnownBaseCount = function () {
            this._knownBaseTypeCount++;
        };

        PullTypeSymbol.prototype.setHasBaseTypeConflict = function () {
            this._hasBaseTypeConflict = true;
        };
        PullTypeSymbol.prototype.hasBaseTypeConflict = function () {
            return this._hasBaseTypeConflict;
        };

        PullTypeSymbol.prototype.hasMembers = function () {
            if (this._members !== TypeScript.sentinelEmptyArray) {
                return true;
            }

            var parents = this.getExtendedTypes();

            for (var i = 0; i < parents.length; i++) {
                if (parents[i].hasMembers()) {
                    return true;
                }
            }

            return false;
        };

        // GTODO
        PullTypeSymbol.prototype.setHasGenericSignature = function () {
            this._hasGenericSignature = true;
        };
        PullTypeSymbol.prototype.getHasGenericSignature = function () {
            return this._hasGenericSignature;
        };

        // GTODO
        PullTypeSymbol.prototype.setHasGenericMember = function () {
            this._hasGenericMember = true;
        };
        PullTypeSymbol.prototype.getHasGenericMember = function () {
            return this._hasGenericMember;
        };

        PullTypeSymbol.prototype.setAssociatedContainerType = function (type) {
            this._associatedContainerTypeSymbol = type;
        };

        PullTypeSymbol.prototype.getAssociatedContainerType = function () {
            return this._associatedContainerTypeSymbol;
        };

        // REVIEW
        PullTypeSymbol.prototype.getArrayType = function () {
            return this._arrayVersionOfThisType;
        };

        PullTypeSymbol.prototype.getElementType = function () {
            return null;
        };

        PullTypeSymbol.prototype.setArrayType = function (arrayType) {
            this._arrayVersionOfThisType = arrayType;
        };

        PullTypeSymbol.prototype.getFunctionSymbol = function () {
            return this._functionSymbol;
        };

        PullTypeSymbol.prototype.setFunctionSymbol = function (symbol) {
            if (symbol) {
                this._functionSymbol = symbol;
            }
        };

        // TODO: This seems to conflate exposed members with private non-Members
        PullTypeSymbol.prototype.findContainedNonMember = function (name) {
            if (!this._containedNonMemberNameCache) {
                return null;
            }

            return this._containedNonMemberNameCache[name];
        };

        PullTypeSymbol.prototype.findContainedNonMemberType = function (typeName, kind) {
            if (typeof kind === "undefined") { kind = 0 /* None */; }
            if (!this._containedNonMemberTypeNameCache) {
                return null;
            }

            var nonMemberSymbol = this._containedNonMemberTypeNameCache[typeName];

            if (nonMemberSymbol && kind !== 0 /* None */) {
                nonMemberSymbol = TypeScript.hasFlag(nonMemberSymbol.kind, kind) ? nonMemberSymbol : null;
            }

            return nonMemberSymbol;
        };

        PullTypeSymbol.prototype.findContainedNonMemberContainer = function (containerName, kind) {
            if (typeof kind === "undefined") { kind = 0 /* None */; }
            if (!this._containedNonMemberContainerCache) {
                return null;
            }

            var nonMemberSymbol = this._containedNonMemberContainerCache[containerName];

            if (nonMemberSymbol && kind !== 0 /* None */) {
                nonMemberSymbol = TypeScript.hasFlag(nonMemberSymbol.kind, kind) ? nonMemberSymbol : null;
            }

            return nonMemberSymbol;
        };

        PullTypeSymbol.prototype.addMember = function (memberSymbol) {
            if (!memberSymbol) {
                return;
            }

            memberSymbol.setContainer(this);

            if (!this._memberNameCache) {
                this._memberNameCache = TypeScript.createIntrinsicsObject();
            }

            if (this._members === TypeScript.sentinelEmptyArray) {
                this._members = [];
            }

            this._members.push(memberSymbol);
            this._memberNameCache[memberSymbol.name] = memberSymbol;
        };

        PullTypeSymbol.prototype.addEnclosedMemberType = function (enclosedType) {
            if (!enclosedType) {
                return;
            }

            enclosedType.setContainer(this);

            if (!this._enclosedTypeNameCache) {
                this._enclosedTypeNameCache = TypeScript.createIntrinsicsObject();
            }

            if (!this._enclosedMemberTypes) {
                this._enclosedMemberTypes = [];
            }

            this._enclosedMemberTypes[this._enclosedMemberTypes.length] = enclosedType;
            this._enclosedTypeNameCache[enclosedType.name] = enclosedType;
        };

        PullTypeSymbol.prototype.addEnclosedMemberContainer = function (enclosedContainer) {
            if (!enclosedContainer) {
                return;
            }

            enclosedContainer.setContainer(this);

            if (!this._enclosedContainerCache) {
                this._enclosedContainerCache = TypeScript.createIntrinsicsObject();
            }

            if (!this._enclosedMemberContainers) {
                this._enclosedMemberContainers = [];
            }

            this._enclosedMemberContainers[this._enclosedMemberContainers.length] = enclosedContainer;
            this._enclosedContainerCache[enclosedContainer.name] = enclosedContainer;
        };

        PullTypeSymbol.prototype.addEnclosedNonMember = function (enclosedNonMember) {
            if (!enclosedNonMember) {
                return;
            }

            enclosedNonMember.setContainer(this);

            if (!this._containedNonMemberNameCache) {
                this._containedNonMemberNameCache = TypeScript.createIntrinsicsObject();
            }

            this._containedNonMemberNameCache[enclosedNonMember.name] = enclosedNonMember;
        };

        PullTypeSymbol.prototype.addEnclosedNonMemberType = function (enclosedNonMemberType) {
            if (!enclosedNonMemberType) {
                return;
            }

            enclosedNonMemberType.setContainer(this);

            if (!this._containedNonMemberTypeNameCache) {
                this._containedNonMemberTypeNameCache = TypeScript.createIntrinsicsObject();
            }

            this._containedNonMemberTypeNameCache[enclosedNonMemberType.name] = enclosedNonMemberType;
        };

        PullTypeSymbol.prototype.addEnclosedNonMemberContainer = function (enclosedNonMemberContainer) {
            if (!enclosedNonMemberContainer) {
                return;
            }

            enclosedNonMemberContainer.setContainer(this);

            if (!this._containedNonMemberContainerCache) {
                this._containedNonMemberContainerCache = TypeScript.createIntrinsicsObject();
            }

            this._containedNonMemberContainerCache[enclosedNonMemberContainer.name] = enclosedNonMemberContainer;
        };

        PullTypeSymbol.prototype.addTypeParameter = function (typeParameter) {
            if (!typeParameter) {
                return;
            }

            if (!typeParameter.getContainer()) {
                typeParameter.setContainer(this);
            }

            if (!this._typeParameterNameCache) {
                this._typeParameterNameCache = TypeScript.createIntrinsicsObject();
            }

            if (!this._typeParameters) {
                this._typeParameters = [];
            }

            this._typeParameters[this._typeParameters.length] = typeParameter;
            this._typeParameterNameCache[typeParameter.getName()] = typeParameter;
        };

        PullTypeSymbol.prototype.getMembers = function () {
            return this._members;
        };

        PullTypeSymbol.prototype.setHasDefaultConstructor = function (hasOne) {
            if (typeof hasOne === "undefined") { hasOne = true; }
            this._hasDefaultConstructor = hasOne;
        };

        PullTypeSymbol.prototype.getHasDefaultConstructor = function () {
            return this._hasDefaultConstructor;
        };

        PullTypeSymbol.prototype.getConstructorMethod = function () {
            return this._constructorMethod;
        };

        PullTypeSymbol.prototype.setConstructorMethod = function (constructorMethod) {
            this._constructorMethod = constructorMethod;
        };

        PullTypeSymbol.prototype.getTypeParameters = function () {
            if (!this._typeParameters) {
                return TypeScript.sentinelEmptyArray;
            }

            return this._typeParameters;
        };

        PullTypeSymbol.prototype.getAllowedToReferenceTypeParameters = function () {
            if (!!(this.kind && TypeScript.PullElementKind.SomeInstantiatableType) && this.isNamedTypeSymbol() && !this.isTypeParameter()) {
                return this.getTypeParameters();
            }

            if (!this._allowedToReferenceTypeParameters) {
                // If the type is not named, it cannot have its own type parameters
                // But it can refer to typeParameters from enclosing type
                this._allowedToReferenceTypeParameters = TypeScript.PullInstantiationHelpers.getAllowedToReferenceTypeParametersFromDecl(this.getDeclarations()[0]);
            }

            return this._allowedToReferenceTypeParameters;
        };

        // GTODO
        PullTypeSymbol.prototype.isGeneric = function () {
            return (this._typeParameters && this._typeParameters.length > 0) || this._hasGenericSignature || this._hasGenericMember || this.isArrayNamedTypeReference();
        };

        PullTypeSymbol.prototype.canUseSimpleInstantiationCache = function (typeArgumentMap) {
            if (this.isTypeParameter()) {
                return true;
            }

            var typeParameters = this.getTypeParameters();
            return typeArgumentMap && this.isNamedTypeSymbol() && typeParameters.length === 1 && typeArgumentMap[typeParameters[0].pullSymbolID].kind !== TypeScript.PullElementKind.ObjectType;
        };

        PullTypeSymbol.prototype.getSimpleInstantiationCacheId = function (typeArgumentMap) {
            if (this.isTypeParameter()) {
                return typeArgumentMap[0].pullSymbolID;
            }

            return typeArgumentMap[this.getTypeParameters()[0].pullSymbolID].pullSymbolID;
        };

        PullTypeSymbol.prototype.addSpecialization = function (specializedVersionOfThisType, typeArgumentMap) {
            if (this.canUseSimpleInstantiationCache(typeArgumentMap)) {
                if (!this._simpleInstantiationCache) {
                    this._simpleInstantiationCache = [];
                }

                this._simpleInstantiationCache[this.getSimpleInstantiationCacheId(typeArgumentMap)] = specializedVersionOfThisType;
            } else {
                if (!this._complexInstantiationCache) {
                    this._complexInstantiationCache = TypeScript.createIntrinsicsObject();
                }

                this._complexInstantiationCache[getIDForTypeSubstitutions(this, typeArgumentMap)] = specializedVersionOfThisType;
            }

            if (!this._specializedVersionsOfThisType) {
                this._specializedVersionsOfThisType = [];
            }

            this._specializedVersionsOfThisType.push(specializedVersionOfThisType);
        };

        PullTypeSymbol.prototype.getSpecialization = function (typeArgumentMap) {
            if (this.canUseSimpleInstantiationCache(typeArgumentMap)) {
                if (!this._simpleInstantiationCache) {
                    return null;
                }

                var result = this._simpleInstantiationCache[this.getSimpleInstantiationCacheId(typeArgumentMap)];
                return result || null;
            } else {
                if (!this._complexInstantiationCache) {
                    return null;
                }

                if (this.getAllowedToReferenceTypeParameters().length == 0) {
                    return this;
                }

                var result = this._complexInstantiationCache[getIDForTypeSubstitutions(this, typeArgumentMap)];
                return result || null;
            }
        };

        PullTypeSymbol.prototype.getKnownSpecializations = function () {
            if (!this._specializedVersionsOfThisType) {
                return TypeScript.sentinelEmptyArray;
            }

            return this._specializedVersionsOfThisType;
        };

        // GTODO
        PullTypeSymbol.prototype.getTypeArguments = function () {
            return null;
        };

        PullTypeSymbol.prototype.getTypeArgumentsOrTypeParameters = function () {
            return this.getTypeParameters();
        };

        PullTypeSymbol.prototype.addCallOrConstructSignaturePrerequisiteBase = function (signature) {
            if (signature.isGeneric()) {
                this._hasGenericSignature = true;
            }

            signature.functionType = this;
        };

        PullTypeSymbol.prototype.addCallSignaturePrerequisite = function (callSignature) {
            if (!this._callSignatures) {
                this._callSignatures = [];
            }

            this.addCallOrConstructSignaturePrerequisiteBase(callSignature);
        };

        PullTypeSymbol.prototype.appendCallSignature = function (callSignature) {
            this.addCallSignaturePrerequisite(callSignature);
            this._callSignatures.push(callSignature);
        };

        PullTypeSymbol.prototype.insertCallSignatureAtIndex = function (callSignature, index) {
            this.addCallSignaturePrerequisite(callSignature);
            TypeScript.Debug.assert(index <= this._callSignatures.length);
            if (index === this._callSignatures.length) {
                this._callSignatures.push(callSignature);
            } else {
                this._callSignatures.splice(index, /*deleteCount*/ 0, callSignature);
            }
        };

        PullTypeSymbol.prototype.addConstructSignaturePrerequisite = function (constructSignature) {
            if (!this._constructSignatures) {
                this._constructSignatures = [];
            }

            this.addCallOrConstructSignaturePrerequisiteBase(constructSignature);
        };

        PullTypeSymbol.prototype.appendConstructSignature = function (constructSignature) {
            this.addConstructSignaturePrerequisite(constructSignature);
            this._constructSignatures.push(constructSignature);
        };

        PullTypeSymbol.prototype.insertConstructSignatureAtIndex = function (constructSignature, index) {
            this.addConstructSignaturePrerequisite(constructSignature);
            TypeScript.Debug.assert(index <= this._constructSignatures.length);
            if (index === this._constructSignatures.length) {
                this._constructSignatures.push(constructSignature);
            } else {
                this._constructSignatures.splice(index, /*deleteCount*/ 0, constructSignature);
            }
        };

        PullTypeSymbol.prototype.addIndexSignature = function (indexSignature) {
            if (!this._indexSignatures) {
                this._indexSignatures = [];
            }

            this._indexSignatures[this._indexSignatures.length] = indexSignature;

            if (indexSignature.isGeneric()) {
                this._hasGenericSignature = true;
            }

            indexSignature.functionType = this;
        };

        PullTypeSymbol.prototype.hasOwnCallSignatures = function () {
            return this._callSignatures !== null;
        };

        PullTypeSymbol.prototype.getOwnCallSignatures = function () {
            return this._callSignatures || TypeScript.sentinelEmptyArray;
        };

        PullTypeSymbol.prototype.getCallSignatures = function () {
            if (this._allCallSignatures) {
                return this._allCallSignatures;
            }

            var signatures = [];

            if (this._callSignatures) {
                signatures = signatures.concat(this._callSignatures);
            }

            // Check for inherited call signatures
            // Only interfaces can inherit call signatures
            if (this._extendedTypes && this.kind === TypeScript.PullElementKind.Interface) {
                for (var i = 0; i < this._extendedTypes.length; i++) {
                    if (this._extendedTypes[i].hasBase(this)) {
                        continue;
                    }

                    // October 16, 2013: Section 7.1:
                    // A call signature declaration hides a base type call signature that is
                    // identical when return types are ignored.
                    this._getResolver()._addUnhiddenSignaturesFromBaseType(this._callSignatures, this._extendedTypes[i].getCallSignatures(), signatures);
                }
            }

            this._allCallSignatures = signatures;

            return signatures;
        };

        PullTypeSymbol.prototype.hasOwnConstructSignatures = function () {
            return this._constructSignatures !== null;
        };

        PullTypeSymbol.prototype.getOwnDeclaredConstructSignatures = function () {
            return this._constructSignatures || TypeScript.sentinelEmptyArray;
        };

        PullTypeSymbol.prototype.getConstructSignatures = function () {
            if (this._allConstructSignatures) {
                return this._allConstructSignatures;
            }

            var signatures = [];

            if (this._constructSignatures) {
                signatures = signatures.concat(this._constructSignatures);
            } else if (this.isConstructor()) {
                if (this._extendedTypes && this._extendedTypes.length > 0) {
                    signatures = this.getBaseClassConstructSignatures(this._extendedTypes[0]);
                } else {
                    signatures = [this.getDefaultClassConstructSignature()];
                }
            }

            // If it's a constructor type, we don't inherit construct signatures
            // (E.g., we'd be looking at the statics on a class, where we want
            // to inherit members, but not construct signatures
            if (this._extendedTypes && this.kind === TypeScript.PullElementKind.Interface) {
                for (var i = 0; i < this._extendedTypes.length; i++) {
                    if (this._extendedTypes[i].hasBase(this)) {
                        continue;
                    }

                    // October 16, 2013: Section 7.1:
                    // A construct signature declaration hides a base type construct signature that is
                    // identical when return types are ignored.
                    this._getResolver()._addUnhiddenSignaturesFromBaseType(this._constructSignatures, this._extendedTypes[i].getConstructSignatures(), signatures);
                }
            }

            this._allConstructSignatures = signatures;

            return signatures;
        };

        PullTypeSymbol.prototype.hasOwnIndexSignatures = function () {
            return this._indexSignatures !== null;
        };

        PullTypeSymbol.prototype.getOwnIndexSignatures = function () {
            return this._indexSignatures || TypeScript.sentinelEmptyArray;
        };

        PullTypeSymbol.prototype.getIndexSignatures = function () {
            if (this._allIndexSignatures) {
                return this._allIndexSignatures;
            }

            var signatures = [];

            if (this._indexSignatures) {
                signatures = signatures.concat(this._indexSignatures);
            }

            if (this._extendedTypes) {
                for (var i = 0; i < this._extendedTypes.length; i++) {
                    if (this._extendedTypes[i].hasBase(this)) {
                        continue;
                    }

                    // October 16, 2013: Section 7.1:
                    // A string index signature declaration hides a base type string index signature.
                    // A numeric index signature declaration hides a base type numeric index signature.
                    this._getResolver()._addUnhiddenSignaturesFromBaseType(this._indexSignatures, this._extendedTypes[i].getIndexSignatures(), signatures);
                }
            }

            this._allIndexSignatures = signatures;

            return signatures;
        };

        // The augmented form of an object type T adds to T those members of the global interface
        // type 'Object' that aren't hidden by members in T. Furthermore, if T has one or more call
        // or construct signatures, the augmented form of T adds to T the members of the global
        // interface type 'Function' that aren't hidden by members in T. Members in T hide 'Object'
        // or 'Function' interface members in the following manner:
        // ...
        // An index signature hides an 'Object' or 'Function' index signature with the same parameter type.
        PullTypeSymbol.prototype.getIndexSignaturesOfAugmentedType = function (resolver, globalFunctionInterface, globalObjectInterface) {
            if (!this._allIndexSignaturesOfAugmentedType) {
                // Start with the types own and inherited index signatures
                var initialIndexSignatures = this.getIndexSignatures();
                var shouldAddFunctionSignatures = false;
                var shouldAddObjectSignatures = false;

                if (globalFunctionInterface && this.isFunctionType() && this !== globalFunctionInterface) {
                    var functionIndexSignatures = globalFunctionInterface.getIndexSignatures();
                    if (functionIndexSignatures.length) {
                        shouldAddFunctionSignatures = true;
                    }
                }

                if (globalObjectInterface && this !== globalObjectInterface) {
                    var objectIndexSignatures = globalObjectInterface.getIndexSignatures();
                    if (objectIndexSignatures.length) {
                        shouldAddObjectSignatures = true;
                    }
                }

                // The 'then' block of this conditional should almost never be entered, since
                // Object and Function in lib.d.ts have no index signatures. This could only
                // happen if the user has added index signatures to Object or Function.
                if (shouldAddFunctionSignatures || shouldAddObjectSignatures) {
                    // First, copy the existing signatures into a new array
                    // .slice(0) is an idiom for copying
                    this._allIndexSignaturesOfAugmentedType = initialIndexSignatures.slice(0);
                    if (shouldAddFunctionSignatures) {
                        // The following call mutates this._allIndexSignaturesOfAugmentedType
                        resolver._addUnhiddenSignaturesFromBaseType(initialIndexSignatures, functionIndexSignatures, this._allIndexSignaturesOfAugmentedType);
                    }
                    if (shouldAddObjectSignatures) {
                        if (shouldAddFunctionSignatures) {
                            // Concat function index signatures to make sure we don't add object signatures
                            // hidden by function signatures.
                            initialIndexSignatures = initialIndexSignatures.concat(functionIndexSignatures);
                        }
                        resolver._addUnhiddenSignaturesFromBaseType(initialIndexSignatures, objectIndexSignatures, this._allIndexSignaturesOfAugmentedType);
                    }
                } else {
                    this._allIndexSignaturesOfAugmentedType = initialIndexSignatures;
                }
            }

            return this._allIndexSignaturesOfAugmentedType;
        };

        PullTypeSymbol.prototype.getBaseClassConstructSignatures = function (baseType) {
            TypeScript.Debug.assert(this.isConstructor() && baseType.isConstructor());
            var instanceTypeSymbol = this.getAssociatedContainerType();
            TypeScript.Debug.assert(instanceTypeSymbol.getDeclarations().length === 1);
            if (baseType.hasBase(this)) {
                return null;
            }

            var baseConstructSignatures = baseType.getConstructSignatures();
            var signatures = [];
            for (var i = 0; i < baseConstructSignatures.length; i++) {
                var baseSignature = baseConstructSignatures[i];
                var currentSignature = new PullSignatureSymbol(TypeScript.PullElementKind.ConstructSignature, baseSignature.isDefinition());
                currentSignature.returnType = instanceTypeSymbol;
                currentSignature.addTypeParametersFromReturnType();
                for (var j = 0; j < baseSignature.parameters.length; j++) {
                    currentSignature.addParameter(baseSignature.parameters[j], baseSignature.parameters[j].isOptional);
                }
                if (baseSignature.parameters.length > 0) {
                    currentSignature.hasVarArgs = baseSignature.parameters[baseSignature.parameters.length - 1].isVarArg;
                }

                // Assume the class has only one decl, since it can't mix with anything
                currentSignature.addDeclaration(instanceTypeSymbol.getDeclarations()[0]);
                this.addCallOrConstructSignaturePrerequisiteBase(currentSignature);
                signatures.push(currentSignature);
            }

            return signatures;
        };

        PullTypeSymbol.prototype.getDefaultClassConstructSignature = function () {
            TypeScript.Debug.assert(this.isConstructor());
            var instanceTypeSymbol = this.getAssociatedContainerType();
            TypeScript.Debug.assert(instanceTypeSymbol.getDeclarations().length == 1);
            var signature = new PullSignatureSymbol(TypeScript.PullElementKind.ConstructSignature, true);
            signature.returnType = instanceTypeSymbol;
            signature.addTypeParametersFromReturnType();
            signature.addDeclaration(instanceTypeSymbol.getDeclarations()[0]);
            this.addCallOrConstructSignaturePrerequisiteBase(signature);

            return signature;
        };

        PullTypeSymbol.prototype.addImplementedType = function (implementedType) {
            if (!implementedType) {
                return;
            }

            if (!this._implementedTypes) {
                this._implementedTypes = [];
            }

            this._implementedTypes[this._implementedTypes.length] = implementedType;

            implementedType.addTypeThatExplicitlyImplementsThisType(this);
        };

        PullTypeSymbol.prototype.getImplementedTypes = function () {
            if (!this._implementedTypes) {
                return TypeScript.sentinelEmptyArray;
            }

            return this._implementedTypes;
        };

        PullTypeSymbol.prototype.addExtendedType = function (extendedType) {
            if (!extendedType) {
                return;
            }

            if (!this._extendedTypes) {
                this._extendedTypes = [];
            }

            this._extendedTypes[this._extendedTypes.length] = extendedType;

            extendedType.addTypeThatExtendsThisType(this);
        };

        PullTypeSymbol.prototype.getExtendedTypes = function () {
            if (!this._extendedTypes) {
                return TypeScript.sentinelEmptyArray;
            }

            return this._extendedTypes;
        };

        PullTypeSymbol.prototype.addTypeThatExtendsThisType = function (type) {
            if (!type) {
                return;
            }

            if (!this._typesThatExtendThisType) {
                this._typesThatExtendThisType = [];
            }

            this._typesThatExtendThisType[this._typesThatExtendThisType.length] = type;
        };

        PullTypeSymbol.prototype.getTypesThatExtendThisType = function () {
            if (!this._typesThatExtendThisType) {
                this._typesThatExtendThisType = [];
            }

            return this._typesThatExtendThisType;
        };

        PullTypeSymbol.prototype.addTypeThatExplicitlyImplementsThisType = function (type) {
            if (!type) {
                return;
            }

            if (!this._typesThatExplicitlyImplementThisType) {
                this._typesThatExplicitlyImplementThisType = [];
            }

            this._typesThatExplicitlyImplementThisType[this._typesThatExplicitlyImplementThisType.length] = type;
        };

        PullTypeSymbol.prototype.getTypesThatExplicitlyImplementThisType = function () {
            if (!this._typesThatExplicitlyImplementThisType) {
                this._typesThatExplicitlyImplementThisType = [];
            }

            return this._typesThatExplicitlyImplementThisType;
        };

        PullTypeSymbol.prototype.hasBase = function (potentialBase, visited) {
            if (typeof visited === "undefined") { visited = []; }
            // Check if this is the potential base:
            //      A extends A  => this === potentialBase
            //      A<T> extends A<T>  => this.getRootSymbol() === potentialBase
            //      A<T> extends A<string> => this === potentialBase.getRootSymbol()
            if (this === potentialBase || this.getRootSymbol() === potentialBase || this === potentialBase.getRootSymbol()) {
                return true;
            }

            if (TypeScript.ArrayUtilities.contains(visited, this)) {
                return true;
            }

            visited.push(this);

            var extendedTypes = this.getExtendedTypes();

            for (var i = 0; i < extendedTypes.length; i++) {
                if (extendedTypes[i].hasBase(potentialBase, visited)) {
                    return true;
                }
            }

            var implementedTypes = this.getImplementedTypes();

            for (var i = 0; i < implementedTypes.length; i++) {
                if (implementedTypes[i].hasBase(potentialBase, visited)) {
                    return true;
                }
            }

            // Clean the list if we are returning false to ensure we are not leaving symbols that
            // were not in the path. No need to do that if we return true, as that will short circuit
            // the search
            visited.pop();

            return false;
        };

        PullTypeSymbol.prototype.isValidBaseKind = function (baseType, isExtendedType) {
            // Error type symbol is invalid base kind
            if (baseType.isError()) {
                return false;
            }

            var thisIsClass = this.isClass();
            if (isExtendedType) {
                if (thisIsClass) {
                    // Class extending non class Type is invalid
                    return baseType.kind === TypeScript.PullElementKind.Class;
                }
            } else {
                if (!thisIsClass) {
                    // Interface implementing baseType is invalid
                    return false;
                }
            }

            // Interface extending non interface or class
            // or class implementing non interface or class - are invalid
            return !!(baseType.kind & (TypeScript.PullElementKind.Interface | TypeScript.PullElementKind.Class));
        };

        PullTypeSymbol.prototype.findMember = function (name, lookInParent) {
            var memberSymbol = null;

            if (this._memberNameCache) {
                memberSymbol = this._memberNameCache[name];
            }

            if (memberSymbol || !lookInParent) {
                return memberSymbol;
            }

            // check parents
            if (this._extendedTypes) {
                for (var i = 0; i < this._extendedTypes.length; i++) {
                    memberSymbol = this._extendedTypes[i].findMember(name, lookInParent);

                    if (memberSymbol) {
                        return memberSymbol;
                    }
                }
            }

            return null;
        };

        PullTypeSymbol.prototype.findNestedType = function (name, kind) {
            if (typeof kind === "undefined") { kind = 0 /* None */; }
            var memberSymbol;

            if (!this._enclosedTypeNameCache) {
                return null;
            }

            memberSymbol = this._enclosedTypeNameCache[name];

            if (memberSymbol && kind !== 0 /* None */) {
                memberSymbol = TypeScript.hasFlag(memberSymbol.kind, kind) ? memberSymbol : null;
            }

            return memberSymbol;
        };

        PullTypeSymbol.prototype.findNestedContainer = function (name, kind) {
            if (typeof kind === "undefined") { kind = 0 /* None */; }
            var memberSymbol;

            if (!this._enclosedContainerCache) {
                return null;
            }

            memberSymbol = this._enclosedContainerCache[name];

            if (memberSymbol && kind !== 0 /* None */) {
                memberSymbol = TypeScript.hasFlag(memberSymbol.kind, kind) ? memberSymbol : null;
            }

            return memberSymbol;
        };

        PullTypeSymbol.prototype.getAllMembers = function (searchDeclKind, memberVisiblity) {
            var allMembers = [];

            // Add members
            if (this._members !== TypeScript.sentinelEmptyArray) {
                for (var i = 0, n = this._members.length; i < n; i++) {
                    var member = this._members[i];
                    if ((member.kind & searchDeclKind) && (memberVisiblity !== 2 /* externallyVisible */ || !member.anyDeclHasFlag(TypeScript.PullElementFlags.Private))) {
                        allMembers[allMembers.length] = member;
                    }
                }
            }

            // Add parent members
            if (this._extendedTypes) {
                // Do not look for the parent's private members unless we need to enumerate all members
                var extenedMembersVisibility = memberVisiblity !== 0 /* all */ ? 2 /* externallyVisible */ : 0 /* all */;

                for (var i = 0, n = this._extendedTypes.length; i < n; i++) {
                    var extendedMembers = this._extendedTypes[i].getAllMembers(searchDeclKind, extenedMembersVisibility);

                    for (var j = 0, m = extendedMembers.length; j < m; j++) {
                        var extendedMember = extendedMembers[j];
                        if (!(this._memberNameCache && this._memberNameCache[extendedMember.name])) {
                            allMembers[allMembers.length] = extendedMember;
                        }
                    }
                }
            }

            if (this.isContainer()) {
                if (this._enclosedMemberTypes) {
                    for (var i = 0; i < this._enclosedMemberTypes.length; i++) {
                        allMembers[allMembers.length] = this._enclosedMemberTypes[i];
                    }
                }
                if (this._enclosedMemberContainers) {
                    for (var i = 0; i < this._enclosedMemberContainers.length; i++) {
                        allMembers[allMembers.length] = this._enclosedMemberContainers[i];
                    }
                }
            }

            return allMembers;
        };

        PullTypeSymbol.prototype.findTypeParameter = function (name) {
            if (!this._typeParameterNameCache) {
                return null;
            }

            return this._typeParameterNameCache[name];
        };

        PullTypeSymbol.prototype.setResolved = function () {
            _super.prototype.setResolved.call(this);
        };

        PullTypeSymbol.prototype.getNamePartForFullName = function () {
            var name = _super.prototype.getNamePartForFullName.call(this);

            var typars = this.getTypeArgumentsOrTypeParameters();
            var typarString = PullSymbol.getTypeParameterString(typars, this, true);
            return name + typarString;
        };

        PullTypeSymbol.prototype.getScopedName = function (scopeSymbol, skipTypeParametersInName, useConstraintInName, skipInternalAliasName) {
            return this.getScopedNameEx(scopeSymbol, skipTypeParametersInName, useConstraintInName, false, false, skipInternalAliasName).toString();
        };

        PullTypeSymbol.prototype.isNamedTypeSymbol = function () {
            var kind = this.kind;
            if (kind === TypeScript.PullElementKind.Primitive || kind === TypeScript.PullElementKind.Class || kind === TypeScript.PullElementKind.Container || kind === TypeScript.PullElementKind.DynamicModule || kind === TypeScript.PullElementKind.TypeAlias || kind === TypeScript.PullElementKind.Enum || kind === TypeScript.PullElementKind.TypeParameter || ((kind === TypeScript.PullElementKind.Interface || kind === TypeScript.PullElementKind.ObjectType) && this.name !== "")) {
                return true;
            }

            return false;
        };

        PullTypeSymbol.prototype.toString = function (scopeSymbol, useConstraintInName) {
            var s = this.getScopedNameEx(scopeSymbol, false, useConstraintInName).toString();
            return s;
        };

        PullTypeSymbol.prototype.getScopedNameEx = function (scopeSymbol, skipTypeParametersInName, useConstraintInName, getPrettyTypeName, getTypeParamMarkerInfo, skipInternalAliasName, shouldAllowArrayType) {
            if (typeof shouldAllowArrayType === "undefined") { shouldAllowArrayType = true; }
            if (this.isArrayNamedTypeReference() && shouldAllowArrayType) {
                var elementType = this.getElementType();
                var elementMemberName = elementType ? (elementType.isArrayNamedTypeReference() || elementType.isNamedTypeSymbol() ? elementType.getScopedNameEx(scopeSymbol, false, false, getPrettyTypeName, getTypeParamMarkerInfo, skipInternalAliasName) : elementType.getMemberTypeNameEx(false, scopeSymbol, getPrettyTypeName)) : TypeScript.MemberName.create("any");
                return TypeScript.MemberName.create(elementMemberName, "", "[]");
            }

            if (!this.isNamedTypeSymbol()) {
                return this.getMemberTypeNameEx(true, scopeSymbol, getPrettyTypeName);
            }

            if (skipTypeParametersInName) {
                return TypeScript.MemberName.create(_super.prototype.getScopedName.call(this, scopeSymbol, skipTypeParametersInName, useConstraintInName, skipInternalAliasName));
            } else {
                var builder = new TypeScript.MemberNameArray();
                builder.prefix = _super.prototype.getScopedName.call(this, scopeSymbol, skipTypeParametersInName, useConstraintInName, skipInternalAliasName);

                var typars = this.getTypeArgumentsOrTypeParameters();
                builder.add(PullSymbol.getTypeParameterStringEx(typars, scopeSymbol, getTypeParamMarkerInfo, useConstraintInName));

                return builder;
            }
        };

        PullTypeSymbol.prototype.hasOnlyOverloadCallSignatures = function () {
            var members = this.getMembers();
            var callSignatures = this.getCallSignatures();
            var constructSignatures = this.getConstructSignatures();
            return members.length === 0 && constructSignatures.length === 0 && callSignatures.length > 1;
        };

        PullTypeSymbol.prototype.getTypeOfSymbol = function () {
            // typeof Module/Class/Enum
            var associatedContainerType = this.getAssociatedContainerType();
            if (associatedContainerType && associatedContainerType.isNamedTypeSymbol()) {
                return associatedContainerType;
            }

            // typeof Function
            var functionSymbol = this.getFunctionSymbol();
            if (functionSymbol && functionSymbol.kind === TypeScript.PullElementKind.Function && !TypeScript.PullHelpers.isSymbolLocal(functionSymbol)) {
                // workaround for missing 'bindAllDeclsInOrder' for classes: do not return typeof symbol for functions defined in module part of clodules (they are considered non-local)
                return TypeScript.PullHelpers.isExportedSymbolInClodule(functionSymbol) ? null : functionSymbol;
            }

            return null;
        };

        PullTypeSymbol.prototype.getMemberTypeNameEx = function (topLevel, scopeSymbol, getPrettyTypeName) {
            var members = this.getMembers();
            var callSignatures = this.getCallSignatures();
            var constructSignatures = this.getConstructSignatures();
            var indexSignatures = this.getIndexSignatures();

            var typeOfSymbol = this.getTypeOfSymbol();
            if (typeOfSymbol) {
                var nameForTypeOf = typeOfSymbol.getScopedNameEx(scopeSymbol, true);
                return TypeScript.MemberName.create(nameForTypeOf, "typeof ", "");
            }

            if (members.length > 0 || callSignatures.length > 0 || constructSignatures.length > 0 || indexSignatures.length > 0) {
                if (this._inMemberTypeNameEx) {
                    // If recursive without type name possible if function expression type
                    return TypeScript.MemberName.create("any");
                }

                this._inMemberTypeNameEx = true;

                var allMemberNames = new TypeScript.MemberNameArray();
                var curlies = !topLevel || indexSignatures.length !== 0;
                var delim = "; ";
                for (var i = 0; i < members.length; i++) {
                    if (members[i].kind === TypeScript.PullElementKind.Method && members[i].type.hasOnlyOverloadCallSignatures()) {
                        // Add all Call signatures of the method
                        var methodCallSignatures = members[i].type.getCallSignatures();
                        var nameStr = members[i].getDisplayName(scopeSymbol) + (members[i].isOptional ? "?" : "");
                        ;
                        var methodMemberNames = PullSignatureSymbol.getSignaturesTypeNameEx(methodCallSignatures, nameStr, false, false, scopeSymbol);
                        allMemberNames.addAll(methodMemberNames);
                    } else {
                        var memberTypeName = members[i].getNameAndTypeNameEx(scopeSymbol);
                        if (memberTypeName.isArray() && memberTypeName.delim === delim) {
                            allMemberNames.addAll(memberTypeName.entries);
                        } else {
                            allMemberNames.add(memberTypeName);
                        }
                    }
                    curlies = true;
                }

                // Use pretty Function overload signature if this is just a call overload
                var getPrettyFunctionOverload = getPrettyTypeName && !curlies && this.hasOnlyOverloadCallSignatures();

                var signatureCount = callSignatures.length + constructSignatures.length + indexSignatures.length;
                var useShortFormSignature = !curlies && (signatureCount === 1);
                var signatureMemberName;

                if (callSignatures.length > 0) {
                    signatureMemberName = PullSignatureSymbol.getSignaturesTypeNameEx(callSignatures, "", useShortFormSignature, false, scopeSymbol, getPrettyFunctionOverload);
                    allMemberNames.addAll(signatureMemberName);
                }

                if (constructSignatures.length > 0) {
                    signatureMemberName = PullSignatureSymbol.getSignaturesTypeNameEx(constructSignatures, "new", useShortFormSignature, false, scopeSymbol);
                    allMemberNames.addAll(signatureMemberName);
                }

                if (indexSignatures.length > 0) {
                    signatureMemberName = PullSignatureSymbol.getSignaturesTypeNameEx(indexSignatures, "", useShortFormSignature, true, scopeSymbol);
                    allMemberNames.addAll(signatureMemberName);
                }

                if ((curlies) || (!getPrettyFunctionOverload && (signatureCount > 1) && topLevel)) {
                    allMemberNames.prefix = "{ ";
                    allMemberNames.suffix = "}";
                    allMemberNames.delim = delim;
                } else if (allMemberNames.entries.length > 1) {
                    allMemberNames.delim = delim;
                }

                this._inMemberTypeNameEx = false;

                return allMemberNames;
            }

            return TypeScript.MemberName.create("{}");
        };

        PullTypeSymbol.prototype.getGenerativeTypeClassification = function (enclosingType) {
            return 2 /* Closed */;
        };

        PullTypeSymbol.prototype.wrapsSomeTypeParameter = function (typeParameterArgumentMap, skipTypeArgumentCheck) {
            return this.getWrappingTypeParameterID(typeParameterArgumentMap, skipTypeArgumentCheck) != 0;
        };

        // 0 means there was no type parameter ID wrapping
        // otherwise typeParameterID that was wrapped
        PullTypeSymbol.prototype.getWrappingTypeParameterID = function (typeParameterArgumentMap, skipTypeArgumentCheck) {
            var type = this;

            // if we encounter a type paramter, we're obviously wrapping
            if (type.isTypeParameter()) {
                if (typeParameterArgumentMap[type.pullSymbolID] || typeParameterArgumentMap[type.getRootSymbol().pullSymbolID]) {
                    return type.pullSymbolID;
                }

                var constraint = type.getConstraint();
                var wrappingTypeParameterID = constraint ? constraint.getWrappingTypeParameterID(typeParameterArgumentMap) : 0;
                return wrappingTypeParameterID;
            }

            if (type.inWrapCheck) {
                return 0;
            }

            // Find result from cache
            this._wrapsTypeParameterCache = this._wrapsTypeParameterCache || new TypeScript.WrapsTypeParameterCache();
            var wrappingTypeParameterID = this._wrapsTypeParameterCache.getWrapsTypeParameter(typeParameterArgumentMap);
            if (wrappingTypeParameterID === undefined) {
                wrappingTypeParameterID = this.getWrappingTypeParameterIDWorker(typeParameterArgumentMap, skipTypeArgumentCheck);

                // Update the cache
                this._wrapsTypeParameterCache.setWrapsTypeParameter(typeParameterArgumentMap, wrappingTypeParameterID);
            }
            return wrappingTypeParameterID;
        };

        PullTypeSymbol.prototype.getWrappingTypeParameterIDFromSignatures = function (signatures, typeParameterArgumentMap) {
            for (var i = 0; i < signatures.length; i++) {
                var wrappingTypeParameterID = signatures[i].getWrappingTypeParameterID(typeParameterArgumentMap);
                if (wrappingTypeParameterID !== 0) {
                    return wrappingTypeParameterID;
                }
            }

            return 0;
        };

        PullTypeSymbol.prototype.getWrappingTypeParameterIDWorker = function (typeParameterArgumentMap, skipTypeArgumentCheck) {
            var type = this;
            var wrappingTypeParameterID = 0;

            if (!skipTypeArgumentCheck) {
                type.inWrapCheck = true;

                var typeArguments = type.getTypeArguments();

                // If there are no type arguments, we could be instantiating the 'root' type
                // declaration
                if (type.isGeneric() && !typeArguments) {
                    typeArguments = type.getTypeParameters();
                }

                // if it's a generic type, scan the type arguments to see which may wrap type parameters
                if (typeArguments) {
                    for (var i = 0; !wrappingTypeParameterID && i < typeArguments.length; i++) {
                        wrappingTypeParameterID = typeArguments[i].getWrappingTypeParameterID(typeParameterArgumentMap);
                    }
                }
            }

            // if it's not a named type, we'll need to introspect its member list
            if (skipTypeArgumentCheck || !(type.kind & TypeScript.PullElementKind.SomeInstantiatableType) || !type.name) {
                // otherwise, walk the member list and signatures, checking for wraps
                var members = type.getAllMembers(TypeScript.PullElementKind.SomeValue, 0 /* all */);
                for (var i = 0; !wrappingTypeParameterID && i < members.length; i++) {
                    TypeScript.PullHelpers.resolveDeclaredSymbolToUseType(members[i]);
                    wrappingTypeParameterID = members[i].type.getWrappingTypeParameterID(typeParameterArgumentMap);
                }

                wrappingTypeParameterID = wrappingTypeParameterID || this.getWrappingTypeParameterIDFromSignatures(type.getCallSignatures(), typeParameterArgumentMap) || this.getWrappingTypeParameterIDFromSignatures(type.getConstructSignatures(), typeParameterArgumentMap) || this.getWrappingTypeParameterIDFromSignatures(type.getIndexSignatures(), typeParameterArgumentMap);
            }

            if (!skipTypeArgumentCheck) {
                type.inWrapCheck = false;
            }

            return wrappingTypeParameterID;
        };

        // Detect if a type parameter is wrapped in a wrapped form that generates infinite expansion.  E.g., for 'T'
        //  class C<T> {
        //      p1: T; <- no
        //      p2: C<T>; <- no
        //      p3: C<C<T>> <- yes
        //  }
        PullTypeSymbol.prototype.wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReference = function (enclosingType) {
            // Only named types can form infinite expansion
            TypeScript.Debug.assert(this.isNamedTypeSymbol());
            TypeScript.Debug.assert(TypeScript.PullHelpers.getRootType(enclosingType) == enclosingType); // Enclosing type passed has to be the root symbol
            var knownWrapMap = TypeScript.BitMatrix.getBitMatrix(true);
            var wrapsIntoInfinitelyExpandingTypeReference = this._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceRecurse(enclosingType, knownWrapMap);
            knownWrapMap.release();
            return wrapsIntoInfinitelyExpandingTypeReference;
        };

        PullTypeSymbol.prototype._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceRecurse = function (enclosingType, knownWrapMap) {
            var wrapsIntoInfinitelyExpandingTypeReference = knownWrapMap.valueAt(this.pullSymbolID, enclosingType.pullSymbolID);
            if (wrapsIntoInfinitelyExpandingTypeReference != undefined) {
                return wrapsIntoInfinitelyExpandingTypeReference;
            }

            if (this.inWrapInfiniteExpandingReferenceCheck) {
                return false;
            }

            this.inWrapInfiniteExpandingReferenceCheck = true;
            wrapsIntoInfinitelyExpandingTypeReference = this._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceWorker(enclosingType, knownWrapMap);
            knownWrapMap.setValueAt(this.pullSymbolID, enclosingType.pullSymbolID, wrapsIntoInfinitelyExpandingTypeReference);
            this.inWrapInfiniteExpandingReferenceCheck = false;

            return wrapsIntoInfinitelyExpandingTypeReference;
        };

        PullTypeSymbol.prototype._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceWorker = function (enclosingType, knownWrapMap) {
            var thisRootType = TypeScript.PullHelpers.getRootType(this);

            if (thisRootType != enclosingType) {
                var thisIsNamedType = this.isNamedTypeSymbol();

                if (thisIsNamedType) {
                    if (thisRootType.inWrapInfiniteExpandingReferenceCheck) {
                        // No need to iterating members of the type again if we are checking members of some
                        // version of this type
                        return false;
                    }

                    thisRootType.inWrapInfiniteExpandingReferenceCheck = true;
                }

                var wrapsIntoInfinitelyExpandingTypeReference = this._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceStructure(enclosingType, knownWrapMap);

                if (thisIsNamedType) {
                    thisRootType.inWrapInfiniteExpandingReferenceCheck = false;
                }

                return wrapsIntoInfinitelyExpandingTypeReference;
            }

            // Spec section 3.8.7: Recursive Types
            // A type reference which, directly or indirectly, references G through open type references and
            // which contains a wrapped form of any of G’s type parameters in one or more type arguments is classified
            // as an infinitely expanding type reference.A type is said to wrap a type parameter if it references
            // the type parameter but isn’t simply the type parameter itself.
            var enclosingTypeParameters = enclosingType.getTypeParameters();
            var typeArguments = this.getTypeArguments();
            for (var i = 0; i < typeArguments.length; i++) {
                if (TypeScript.ArrayUtilities.contains(enclosingTypeParameters, typeArguments[i])) {
                    continue;
                }

                // Type arguments wraps type parameter
                if (typeArguments[i].wrapsSomeTypeParameter(this.getTypeParameterArgumentMap())) {
                    return true;
                }
            }

            return false;
        };

        PullTypeSymbol.prototype._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceStructure = function (enclosingType, knownWrapMap) {
            var members = this.getAllMembers(TypeScript.PullElementKind.SomeValue, 0 /* all */);
            for (var i = 0; i < members.length; i++) {
                if (members[i].type && members[i].type._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReferenceRecurse(enclosingType, knownWrapMap)) {
                    return true;
                }
            }

            var sigs = this.getCallSignatures();
            for (var i = 0; i < sigs.length; i++) {
                if (sigs[i]._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReference(enclosingType, knownWrapMap)) {
                    return true;
                }
            }

            sigs = this.getConstructSignatures();
            for (var i = 0; i < sigs.length; i++) {
                if (sigs[i]._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReference(enclosingType, knownWrapMap)) {
                    return true;
                }
            }

            sigs = this.getIndexSignatures();
            for (var i = 0; i < sigs.length; i++) {
                if (sigs[i]._wrapsSomeTypeParameterIntoInfinitelyExpandingTypeReference(enclosingType, knownWrapMap)) {
                    return true;
                }
            }

            return false;
        };

        // This is the same signature as PullTypeResolver.widenType (except that the positions
        // of the resolver are switched). This method just returns the cached value of the widened
        // type, otherwise, calls into the resolver.
        PullTypeSymbol.prototype.widenedType = function (resolver, ast, context) {
            if (!this._widenedType) {
                this._widenedType = resolver.widenType(this, ast, context);
            }
            return this._widenedType;
        };
        return PullTypeSymbol;
    })(PullSymbol);
    TypeScript.PullTypeSymbol = PullTypeSymbol;

    var PullPrimitiveTypeSymbol = (function (_super) {
        __extends(PullPrimitiveTypeSymbol, _super);
        function PullPrimitiveTypeSymbol(name) {
            _super.call(this, name, TypeScript.PullElementKind.Primitive);

            this.isResolved = true;
        }
        PullPrimitiveTypeSymbol.prototype.isAny = function () {
            return !this.isStringConstant() && this.name === "any";
        };

        PullPrimitiveTypeSymbol.prototype.isNull = function () {
            return !this.isStringConstant() && this.name === "null";
        };

        PullPrimitiveTypeSymbol.prototype.isUndefined = function () {
            return !this.isStringConstant() && this.name === "undefined";
        };

        PullPrimitiveTypeSymbol.prototype.isStringConstant = function () {
            return false;
        };

        PullPrimitiveTypeSymbol.prototype.setUnresolved = function () {
            // do nothing...
        };

        // Overrides the PullSymbol.getDisplayName to give the appearance of widening. The spec
        // doesn't say anything about displaying types, but we should leave no trace of undefined
        // or null.
        PullPrimitiveTypeSymbol.prototype.getDisplayName = function () {
            if (this.isNull() || this.isUndefined()) {
                return "any";
            } else {
                return _super.prototype.getDisplayName.call(this);
            }
        };
        return PullPrimitiveTypeSymbol;
    })(PullTypeSymbol);
    TypeScript.PullPrimitiveTypeSymbol = PullPrimitiveTypeSymbol;

    var PullStringConstantTypeSymbol = (function (_super) {
        __extends(PullStringConstantTypeSymbol, _super);
        function PullStringConstantTypeSymbol(name) {
            _super.call(this, name);
        }
        PullStringConstantTypeSymbol.prototype.isStringConstant = function () {
            return true;
        };
        return PullStringConstantTypeSymbol;
    })(PullPrimitiveTypeSymbol);
    TypeScript.PullStringConstantTypeSymbol = PullStringConstantTypeSymbol;

    var PullErrorTypeSymbol = (function (_super) {
        __extends(PullErrorTypeSymbol, _super);
        function PullErrorTypeSymbol(_anyType, name) {
            _super.call(this, name);
            this._anyType = _anyType;

            TypeScript.Debug.assert(this._anyType);
            this.isResolved = true;
        }
        PullErrorTypeSymbol.prototype.isError = function () {
            return true;
        };

        PullErrorTypeSymbol.prototype._getResolver = function () {
            return this._anyType._getResolver();
        };

        PullErrorTypeSymbol.prototype.getName = function (scopeSymbol, useConstraintInName) {
            return this._anyType.getName(scopeSymbol, useConstraintInName);
        };

        PullErrorTypeSymbol.prototype.getDisplayName = function (scopeSymbol, useConstraintInName, skipInternalAliasName) {
            return this._anyType.getName(scopeSymbol, useConstraintInName);
        };

        PullErrorTypeSymbol.prototype.toString = function (scopeSymbol, useConstraintInName) {
            return this._anyType.getName(scopeSymbol, useConstraintInName);
        };
        return PullErrorTypeSymbol;
    })(PullPrimitiveTypeSymbol);
    TypeScript.PullErrorTypeSymbol = PullErrorTypeSymbol;

    // represents the module "namespace" type
    var PullContainerSymbol = (function (_super) {
        __extends(PullContainerSymbol, _super);
        function PullContainerSymbol(name, kind) {
            _super.call(this, name, kind);
            this.instanceSymbol = null;
            this.assignedValue = null;
            this.assignedType = null;
            this.assignedContainer = null;
        }
        PullContainerSymbol.prototype.isContainer = function () {
            return true;
        };

        PullContainerSymbol.prototype.setInstanceSymbol = function (symbol) {
            this.instanceSymbol = symbol;
        };

        PullContainerSymbol.prototype.getInstanceSymbol = function () {
            return this.instanceSymbol;
        };

        PullContainerSymbol.prototype.setExportAssignedValueSymbol = function (symbol) {
            this.assignedValue = symbol;
        };

        PullContainerSymbol.prototype.getExportAssignedValueSymbol = function () {
            return this.assignedValue;
        };

        PullContainerSymbol.prototype.setExportAssignedTypeSymbol = function (type) {
            this.assignedType = type;
        };

        PullContainerSymbol.prototype.getExportAssignedTypeSymbol = function () {
            return this.assignedType;
        };

        PullContainerSymbol.prototype.setExportAssignedContainerSymbol = function (container) {
            this.assignedContainer = container;
        };

        PullContainerSymbol.prototype.getExportAssignedContainerSymbol = function () {
            return this.assignedContainer;
        };

        PullContainerSymbol.prototype.hasExportAssignment = function () {
            return !!this.assignedValue || !!this.assignedType || !!this.assignedContainer;
        };

        PullContainerSymbol.usedAsSymbol = function (containerSymbol, symbol) {
            if (!containerSymbol || !containerSymbol.isContainer()) {
                return false;
            }

            if (!containerSymbol.isAlias() && containerSymbol.type === symbol) {
                return true;
            }

            var moduleSymbol = containerSymbol;
            var valueExportSymbol = moduleSymbol.getExportAssignedValueSymbol();
            var typeExportSymbol = moduleSymbol.getExportAssignedTypeSymbol();
            var containerExportSymbol = moduleSymbol.getExportAssignedContainerSymbol();
            if (valueExportSymbol || typeExportSymbol || containerExportSymbol) {
                return valueExportSymbol === symbol || typeExportSymbol == symbol || containerExportSymbol == symbol || PullContainerSymbol.usedAsSymbol(containerExportSymbol, symbol);
            }

            return false;
        };

        PullContainerSymbol.prototype.getInstanceType = function () {
            return this.instanceSymbol ? this.instanceSymbol.type : null;
        };
        return PullContainerSymbol;
    })(PullTypeSymbol);
    TypeScript.PullContainerSymbol = PullContainerSymbol;

    var PullTypeAliasSymbol = (function (_super) {
        __extends(PullTypeAliasSymbol, _super);
        function PullTypeAliasSymbol(name) {
            _super.call(this, name, TypeScript.PullElementKind.TypeAlias);
            this._assignedValue = null;
            this._assignedType = null;
            this._assignedContainer = null;
            this._isUsedAsValue = false;
            this._typeUsedExternally = false;
            this._isUsedInExportAlias = false;
            this.retrievingExportAssignment = false;
            this.linkedAliasSymbols = null;
        }
        PullTypeAliasSymbol.prototype.isUsedInExportedAlias = function () {
            this._resolveDeclaredSymbol();
            return this._isUsedInExportAlias;
        };

        PullTypeAliasSymbol.prototype.typeUsedExternally = function () {
            this._resolveDeclaredSymbol();
            return this._typeUsedExternally;
        };

        PullTypeAliasSymbol.prototype.isUsedAsValue = function () {
            this._resolveDeclaredSymbol();
            return this._isUsedAsValue;
        };

        PullTypeAliasSymbol.prototype.setTypeUsedExternally = function () {
            this._typeUsedExternally = true;
        };

        PullTypeAliasSymbol.prototype.setIsUsedInExportedAlias = function () {
            this._isUsedInExportAlias = true;
            if (this.linkedAliasSymbols) {
                this.linkedAliasSymbols.forEach(function (s) {
                    return s.setIsUsedInExportedAlias();
                });
            }
        };

        PullTypeAliasSymbol.prototype.addLinkedAliasSymbol = function (contingentValueSymbol) {
            if (!this.linkedAliasSymbols) {
                this.linkedAliasSymbols = [contingentValueSymbol];
            } else {
                this.linkedAliasSymbols.push(contingentValueSymbol);
            }
        };

        PullTypeAliasSymbol.prototype.setIsUsedAsValue = function () {
            this._isUsedAsValue = true;
            if (this.linkedAliasSymbols) {
                this.linkedAliasSymbols.forEach(function (s) {
                    return s.setIsUsedAsValue();
                });
            }
        };

        PullTypeAliasSymbol.prototype.assignedValue = function () {
            this._resolveDeclaredSymbol();
            return this._assignedValue;
        };

        PullTypeAliasSymbol.prototype.assignedType = function () {
            this._resolveDeclaredSymbol();
            return this._assignedType;
        };

        PullTypeAliasSymbol.prototype.assignedContainer = function () {
            this._resolveDeclaredSymbol();
            return this._assignedContainer;
        };

        PullTypeAliasSymbol.prototype.isAlias = function () {
            return true;
        };
        PullTypeAliasSymbol.prototype.isContainer = function () {
            return true;
        };

        PullTypeAliasSymbol.prototype.setAssignedValueSymbol = function (symbol) {
            this._assignedValue = symbol;
        };

        PullTypeAliasSymbol.prototype.getExportAssignedValueSymbol = function () {
            if (this._assignedValue) {
                return this._assignedValue;
            }

            if (this.retrievingExportAssignment) {
                return null;
            }

            if (this._assignedContainer) {
                if (this._assignedContainer.hasExportAssignment()) {
                    this.retrievingExportAssignment = true;
                    var sym = this._assignedContainer.getExportAssignedValueSymbol();
                    this.retrievingExportAssignment = false;
                    return sym;
                }

                return this._assignedContainer.getInstanceSymbol();
            }

            return null;
        };

        PullTypeAliasSymbol.prototype.setAssignedTypeSymbol = function (type) {
            this._assignedType = type;
        };

        PullTypeAliasSymbol.prototype.getExportAssignedTypeSymbol = function () {
            if (this.retrievingExportAssignment) {
                return null;
            }

            if (this._assignedType) {
                if (this._assignedType.isAlias()) {
                    this.retrievingExportAssignment = true;
                    var sym = this._assignedType.getExportAssignedTypeSymbol();
                    this.retrievingExportAssignment = false;
                } else if (this._assignedType !== this._assignedContainer) {
                    return this._assignedType;
                }
            }

            if (this._assignedContainer) {
                this.retrievingExportAssignment = true;
                var sym = this._assignedContainer.getExportAssignedTypeSymbol();
                this.retrievingExportAssignment = false;
                if (sym) {
                    return sym;
                }
            }

            return this._assignedContainer;
        };

        PullTypeAliasSymbol.prototype.setAssignedContainerSymbol = function (container) {
            this._assignedContainer = container;
        };

        PullTypeAliasSymbol.prototype.getExportAssignedContainerSymbol = function () {
            if (this.retrievingExportAssignment) {
                return null;
            }

            if (this._assignedContainer) {
                this.retrievingExportAssignment = true;
                var sym = this._assignedContainer.getExportAssignedContainerSymbol();
                this.retrievingExportAssignment = false;
                if (sym) {
                    return sym;
                }
            }

            return this._assignedContainer;
        };

        PullTypeAliasSymbol.prototype.getMembers = function () {
            if (this._assignedType) {
                return this._assignedType.getMembers();
            }

            return TypeScript.sentinelEmptyArray;
        };

        PullTypeAliasSymbol.prototype.getCallSignatures = function () {
            if (this._assignedType) {
                return this._assignedType.getCallSignatures();
            }

            return TypeScript.sentinelEmptyArray;
        };

        PullTypeAliasSymbol.prototype.getConstructSignatures = function () {
            if (this._assignedType) {
                return this._assignedType.getConstructSignatures();
            }

            return TypeScript.sentinelEmptyArray;
        };

        PullTypeAliasSymbol.prototype.getIndexSignatures = function () {
            if (this._assignedType) {
                return this._assignedType.getIndexSignatures();
            }

            return TypeScript.sentinelEmptyArray;
        };

        PullTypeAliasSymbol.prototype.findMember = function (name) {
            if (this._assignedType) {
                return this._assignedType.findMember(name, true);
            }

            return null;
        };

        PullTypeAliasSymbol.prototype.findNestedType = function (name) {
            if (this._assignedType) {
                return this._assignedType.findNestedType(name);
            }

            return null;
        };

        PullTypeAliasSymbol.prototype.findNestedContainer = function (name) {
            if (this._assignedType) {
                return this._assignedType.findNestedContainer(name);
            }

            return null;
        };

        PullTypeAliasSymbol.prototype.getAllMembers = function (searchDeclKind, memberVisibility) {
            if (this._assignedType) {
                return this._assignedType.getAllMembers(searchDeclKind, memberVisibility);
            }

            return TypeScript.sentinelEmptyArray;
        };
        return PullTypeAliasSymbol;
    })(PullTypeSymbol);
    TypeScript.PullTypeAliasSymbol = PullTypeAliasSymbol;

    var PullTypeParameterSymbol = (function (_super) {
        __extends(PullTypeParameterSymbol, _super);
        function PullTypeParameterSymbol(name) {
            _super.call(this, name, TypeScript.PullElementKind.TypeParameter);
            this._constraint = null;
        }
        PullTypeParameterSymbol.prototype.isTypeParameter = function () {
            return true;
        };

        PullTypeParameterSymbol.prototype.setConstraint = function (constraintType) {
            this._constraint = constraintType;
        };

        PullTypeParameterSymbol.prototype.getConstraint = function () {
            return this._constraint;
        };

        // Section 3.4.1 (November 18, 2013):
        // The base constraint of a type parameter T is defined as follows:
        //  If T has no declared constraint, T's base constraint is the empty object type {}
        //  If T's declared constraint is a type parameter, T's base constraint is that of the type parameter.
        //  Otherwise, T's base constraint is T's declared constraint.
        PullTypeParameterSymbol.prototype.getBaseConstraint = function (semanticInfoChain) {
            var preBaseConstraint = this.getConstraintRecursively({});
            TypeScript.Debug.assert(preBaseConstraint === null || !preBaseConstraint.isTypeParameter());
            return preBaseConstraint || semanticInfoChain.emptyTypeSymbol;
        };

        // Returns null if you hit a cycle or no constraint
        // The visitedTypeParameters bag is for catching cycles
        PullTypeParameterSymbol.prototype.getConstraintRecursively = function (visitedTypeParameters) {
            var constraint = this.getConstraint();

            if (constraint) {
                if (constraint.isTypeParameter()) {
                    var constraintAsTypeParameter = constraint;
                    if (!visitedTypeParameters[constraintAsTypeParameter.pullSymbolID]) {
                        visitedTypeParameters[constraintAsTypeParameter.pullSymbolID] = constraintAsTypeParameter;
                        return constraintAsTypeParameter.getConstraintRecursively(visitedTypeParameters);
                    }
                } else {
                    return constraint;
                }
            }

            return null;
        };

        // The default constraint is just like the base constraint, but without recursively traversing
        // type parameters.
        PullTypeParameterSymbol.prototype.getDefaultConstraint = function (semanticInfoChain) {
            return this._constraint || semanticInfoChain.emptyTypeSymbol;
        };

        // Note: This is a deviation from the spec. Using the constraint to get signatures is only
        // warranted when we explicitly ask for an apparent type.
        PullTypeParameterSymbol.prototype.getCallSignatures = function () {
            if (this._constraint) {
                return this._constraint.getCallSignatures();
            }

            return _super.prototype.getCallSignatures.call(this);
        };

        PullTypeParameterSymbol.prototype.getConstructSignatures = function () {
            if (this._constraint) {
                return this._constraint.getConstructSignatures();
            }

            return _super.prototype.getConstructSignatures.call(this);
        };

        PullTypeParameterSymbol.prototype.getIndexSignatures = function () {
            if (this._constraint) {
                return this._constraint.getIndexSignatures();
            }

            return _super.prototype.getIndexSignatures.call(this);
        };

        PullTypeParameterSymbol.prototype.isGeneric = function () {
            return true;
        };

        PullTypeParameterSymbol.prototype.fullName = function (scopeSymbol) {
            var name = this.getDisplayName(scopeSymbol);
            var container = this.getContainer();
            if (container) {
                var containerName = container.fullName(scopeSymbol);
                name = name + " in " + containerName;
            }

            return name;
        };

        PullTypeParameterSymbol.prototype.getName = function (scopeSymbol, useConstraintInName) {
            var name = _super.prototype.getName.call(this, scopeSymbol);

            if (this.isPrinting) {
                return name;
            }

            this.isPrinting = true;

            if (useConstraintInName && this._constraint) {
                name += " extends " + this._constraint.toString(scopeSymbol);
            }

            this.isPrinting = false;

            return name;
        };

        PullTypeParameterSymbol.prototype.getDisplayName = function (scopeSymbol, useConstraintInName, skipInternalAliasName) {
            var name = _super.prototype.getDisplayName.call(this, scopeSymbol, useConstraintInName, skipInternalAliasName);

            if (this.isPrinting) {
                return name;
            }

            this.isPrinting = true;

            if (useConstraintInName && this._constraint) {
                name += " extends " + this._constraint.toString(scopeSymbol);
            }

            this.isPrinting = false;

            return name;
        };

        PullTypeParameterSymbol.prototype.isExternallyVisible = function (inIsExternallyVisibleSymbols) {
            return true;
        };
        return PullTypeParameterSymbol;
    })(PullTypeSymbol);
    TypeScript.PullTypeParameterSymbol = PullTypeParameterSymbol;

    var PullAccessorSymbol = (function (_super) {
        __extends(PullAccessorSymbol, _super);
        function PullAccessorSymbol(name) {
            _super.call(this, name, TypeScript.PullElementKind.Property);
            this._getterSymbol = null;
            this._setterSymbol = null;
        }
        PullAccessorSymbol.prototype.isAccessor = function () {
            return true;
        };

        PullAccessorSymbol.prototype.setSetter = function (setter) {
            if (!setter) {
                return;
            }

            this._setterSymbol = setter;
        };

        PullAccessorSymbol.prototype.getSetter = function () {
            return this._setterSymbol;
        };

        PullAccessorSymbol.prototype.setGetter = function (getter) {
            if (!getter) {
                return;
            }

            this._getterSymbol = getter;
        };

        PullAccessorSymbol.prototype.getGetter = function () {
            return this._getterSymbol;
        };
        return PullAccessorSymbol;
    })(PullSymbol);
    TypeScript.PullAccessorSymbol = PullAccessorSymbol;

    function getIDForTypeSubstitutions(instantiatingTypeOrSignature, typeArgumentMap) {
        var substitution = "";
        var members = null;

        var allowedToReferenceTypeParameters = instantiatingTypeOrSignature.getAllowedToReferenceTypeParameters();
        for (var i = 0; i < allowedToReferenceTypeParameters.length; i++) {
            var typeParameter = allowedToReferenceTypeParameters[i];
            var typeParameterID = typeParameter.pullSymbolID;
            var typeArg = typeArgumentMap[typeParameterID];
            if (!typeArg) {
                typeArg = typeParameter;
            }
            substitution += typeParameterID + ":" + getIDForTypeSubstitutionsOfType(typeArg);
        }

        return substitution;
    }
    TypeScript.getIDForTypeSubstitutions = getIDForTypeSubstitutions;

    function getIDForTypeSubstitutionsOfType(type) {
        var structure;
        if (type.isError()) {
            structure = "E" + getIDForTypeSubstitutionsOfType(type._anyType);
        } else if (!type.isNamedTypeSymbol()) {
            structure = getIDForTypeSubstitutionsFromObjectType(type);
        }

        if (!structure) {
            structure = type.pullSymbolID + "#";
        }

        return structure;
    }

    function getIDForTypeSubstitutionsFromObjectType(type) {
        if (type.isResolved) {
            var getIDForTypeSubStitutionWalker = new GetIDForTypeSubStitutionWalker();
            TypeScript.PullHelpers.walkPullTypeSymbolStructure(type, getIDForTypeSubStitutionWalker);
        }

        return null;
    }

    var GetIDForTypeSubStitutionWalker = (function () {
        function GetIDForTypeSubStitutionWalker() {
            this.structure = "";
        }
        GetIDForTypeSubStitutionWalker.prototype.memberSymbolWalk = function (memberSymbol) {
            this.structure += memberSymbol.name + "@" + getIDForTypeSubstitutionsOfType(memberSymbol.type);
            return true;
        };
        GetIDForTypeSubStitutionWalker.prototype.callSignatureWalk = function (signatureSymbol) {
            this.structure += "(";
            return true;
        };
        GetIDForTypeSubStitutionWalker.prototype.constructSignatureWalk = function (signatureSymbol) {
            this.structure += "new(";
            return true;
        };
        GetIDForTypeSubStitutionWalker.prototype.indexSignatureWalk = function (signatureSymbol) {
            this.structure += "[](";
            return true;
        };
        GetIDForTypeSubStitutionWalker.prototype.signatureParameterWalk = function (parameterSymbol) {
            this.structure += parameterSymbol.name + "@" + getIDForTypeSubstitutionsOfType(parameterSymbol.type);
            return true;
        };
        GetIDForTypeSubStitutionWalker.prototype.signatureReturnTypeWalk = function (returnType) {
            this.structure += ")" + getIDForTypeSubstitutionsOfType(returnType);
            return true;
        };
        return GetIDForTypeSubStitutionWalker;
    })();

    (function (GetAllMembersVisiblity) {
        // All properties of the type regardless of their accessibility level
        GetAllMembersVisiblity[GetAllMembersVisiblity["all"] = 0] = "all";

        // Only properties that are accessible on a class instance, i.e. public and private members of
        // the current class, and only public members of any bases it extends
        GetAllMembersVisiblity[GetAllMembersVisiblity["internallyVisible"] = 1] = "internallyVisible";

        // Only public members of classes
        GetAllMembersVisiblity[GetAllMembersVisiblity["externallyVisible"] = 2] = "externallyVisible";
    })(TypeScript.GetAllMembersVisiblity || (TypeScript.GetAllMembersVisiblity = {}));
    var GetAllMembersVisiblity = TypeScript.GetAllMembersVisiblity;
})(TypeScript || (TypeScript = {}));
