///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    var OverloadApplicabilityStatus;
    (function (OverloadApplicabilityStatus) {
        OverloadApplicabilityStatus[OverloadApplicabilityStatus["NotAssignable"] = 0] = "NotAssignable";
        OverloadApplicabilityStatus[OverloadApplicabilityStatus["AssignableButWithProvisionalErrors"] = 1] = "AssignableButWithProvisionalErrors";
        OverloadApplicabilityStatus[OverloadApplicabilityStatus["AssignableWithNoProvisionalErrors"] = 2] = "AssignableWithNoProvisionalErrors";
        OverloadApplicabilityStatus[OverloadApplicabilityStatus["Subtype"] = 3] = "Subtype";
    })(OverloadApplicabilityStatus || (OverloadApplicabilityStatus = {}));

    var PullAdditionalCallResolutionData = (function () {
        function PullAdditionalCallResolutionData() {
            this.targetSymbol = null;
            this.resolvedSignatures = null;
            this.candidateSignature = null;
            this.actualParametersContextTypeSymbols = null;
            this.diagnosticsFromOverloadResolution = [];
        }
        return PullAdditionalCallResolutionData;
    })();
    TypeScript.PullAdditionalCallResolutionData = PullAdditionalCallResolutionData;

    var PullAdditionalObjectLiteralResolutionData = (function () {
        function PullAdditionalObjectLiteralResolutionData() {
            this.membersContextTypeSymbols = null;
        }
        return PullAdditionalObjectLiteralResolutionData;
    })();
    TypeScript.PullAdditionalObjectLiteralResolutionData = PullAdditionalObjectLiteralResolutionData;

    // A base origin is for inherited members. It points to which base the member was inherited from.
    var MemberWithBaseOrigin = (function () {
        function MemberWithBaseOrigin(memberSymbol, baseOrigin) {
            this.memberSymbol = memberSymbol;
            this.baseOrigin = baseOrigin;
        }
        return MemberWithBaseOrigin;
    })();

    var SignatureWithBaseOrigin = (function () {
        function SignatureWithBaseOrigin(signature, baseOrigin) {
            this.signature = signature;
            this.baseOrigin = baseOrigin;
        }
        return SignatureWithBaseOrigin;
    })();

    var InheritedIndexSignatureInfo = (function () {
        function InheritedIndexSignatureInfo() {
        }
        return InheritedIndexSignatureInfo;
    })();

    var CompilerReservedName;
    (function (CompilerReservedName) {
        CompilerReservedName[CompilerReservedName["_this"] = 1] = "_this";
        CompilerReservedName[CompilerReservedName["_super"] = 2] = "_super";
        CompilerReservedName[CompilerReservedName["arguments"] = 3] = "arguments";
        CompilerReservedName[CompilerReservedName["_i"] = 4] = "_i";
        CompilerReservedName[CompilerReservedName["require"] = 5] = "require";
        CompilerReservedName[CompilerReservedName["exports"] = 6] = "exports";
    })(CompilerReservedName || (CompilerReservedName = {}));

    function getCompilerReservedName(name) {
        var nameText = name.valueText();
        return CompilerReservedName[nameText];
    }

    // The resolver associates types with a given AST
    var PullTypeResolver = (function () {
        function PullTypeResolver(compilationSettings, semanticInfoChain) {
            this.compilationSettings = compilationSettings;
            this.semanticInfoChain = semanticInfoChain;
            this._cachedArrayInterfaceType = null;
            this._cachedNumberInterfaceType = null;
            this._cachedStringInterfaceType = null;
            this._cachedBooleanInterfaceType = null;
            this._cachedObjectInterfaceType = null;
            this._cachedFunctionInterfaceType = null;
            this._cachedIArgumentsInterfaceType = null;
            this._cachedRegExpInterfaceType = null;
            this._cachedAnyTypeArgs = null;
            this.typeCheckCallBacks = [];
            this.postTypeCheckWorkitems = [];
            this._cachedFunctionArgumentsSymbol = null;
            this.assignableCache = TypeScript.BitMatrix.getBitMatrix(true);
            this.subtypeCache = TypeScript.BitMatrix.getBitMatrix(true);
            this.identicalCache = TypeScript.BitMatrix.getBitMatrix(true);
            this.inResolvingOtherDeclsWalker = new TypeScript.PullHelpers.OtherPullDeclsWalker();
        }
        PullTypeResolver.prototype.cachedArrayInterfaceType = function () {
            if (!this._cachedArrayInterfaceType) {
                this._cachedArrayInterfaceType = this.getSymbolFromDeclPath("Array", [], TypeScript.PullElementKind.Interface) || this.semanticInfoChain.emptyTypeSymbol;
            }

            if (!this._cachedArrayInterfaceType.isResolved) {
                this.resolveDeclaredSymbol(this._cachedArrayInterfaceType, new TypeScript.PullTypeResolutionContext(this));
            }

            return this._cachedArrayInterfaceType;
        };

        // Returns the named type for the global Array<T> symbol.  Note that this will be
        // uninstantiated (i.e. it will have type parameters, and not type arguments).
        PullTypeResolver.prototype.getArrayNamedType = function () {
            return this.cachedArrayInterfaceType();
        };

        PullTypeResolver.prototype.cachedNumberInterfaceType = function () {
            if (!this._cachedNumberInterfaceType) {
                this._cachedNumberInterfaceType = this.getSymbolFromDeclPath("Number", [], TypeScript.PullElementKind.Interface) || this.semanticInfoChain.emptyTypeSymbol;
            }

            if (this._cachedNumberInterfaceType && !this._cachedNumberInterfaceType.isResolved) {
                this.resolveDeclaredSymbol(this._cachedNumberInterfaceType, new TypeScript.PullTypeResolutionContext(this));
            }

            return this._cachedNumberInterfaceType;
        };

        PullTypeResolver.prototype.cachedStringInterfaceType = function () {
            if (!this._cachedStringInterfaceType) {
                this._cachedStringInterfaceType = this.getSymbolFromDeclPath("String", [], TypeScript.PullElementKind.Interface) || this.semanticInfoChain.emptyTypeSymbol;
            }

            if (this._cachedStringInterfaceType && !this._cachedStringInterfaceType.isResolved) {
                this.resolveDeclaredSymbol(this._cachedStringInterfaceType, new TypeScript.PullTypeResolutionContext(this));
            }

            return this._cachedStringInterfaceType;
        };

        PullTypeResolver.prototype.cachedBooleanInterfaceType = function () {
            if (!this._cachedBooleanInterfaceType) {
                this._cachedBooleanInterfaceType = this.getSymbolFromDeclPath("Boolean", [], TypeScript.PullElementKind.Interface) || this.semanticInfoChain.emptyTypeSymbol;
            }

            if (this._cachedBooleanInterfaceType && !this._cachedBooleanInterfaceType.isResolved) {
                this.resolveDeclaredSymbol(this._cachedBooleanInterfaceType, new TypeScript.PullTypeResolutionContext(this));
            }

            return this._cachedBooleanInterfaceType;
        };

        PullTypeResolver.prototype.cachedObjectInterfaceType = function () {
            if (!this._cachedObjectInterfaceType) {
                this._cachedObjectInterfaceType = this.getSymbolFromDeclPath("Object", [], TypeScript.PullElementKind.Interface) || this.semanticInfoChain.emptyTypeSymbol;
            }

            if (!this._cachedObjectInterfaceType) {
                this._cachedObjectInterfaceType = this.semanticInfoChain.anyTypeSymbol;
            }

            if (!this._cachedObjectInterfaceType.isResolved) {
                this.resolveDeclaredSymbol(this._cachedObjectInterfaceType, new TypeScript.PullTypeResolutionContext(this));
            }

            return this._cachedObjectInterfaceType;
        };

        PullTypeResolver.prototype.cachedFunctionInterfaceType = function () {
            if (!this._cachedFunctionInterfaceType) {
                this._cachedFunctionInterfaceType = this.getSymbolFromDeclPath("Function", [], TypeScript.PullElementKind.Interface) || this.semanticInfoChain.emptyTypeSymbol;
            }

            if (this._cachedFunctionInterfaceType && !this._cachedFunctionInterfaceType.isResolved) {
                this.resolveDeclaredSymbol(this._cachedFunctionInterfaceType, new TypeScript.PullTypeResolutionContext(this));
            }

            return this._cachedFunctionInterfaceType;
        };

        PullTypeResolver.prototype.cachedIArgumentsInterfaceType = function () {
            if (!this._cachedIArgumentsInterfaceType) {
                this._cachedIArgumentsInterfaceType = this.getSymbolFromDeclPath("IArguments", [], TypeScript.PullElementKind.Interface) || this.semanticInfoChain.emptyTypeSymbol;
            }

            if (this._cachedIArgumentsInterfaceType && !this._cachedIArgumentsInterfaceType.isResolved) {
                this.resolveDeclaredSymbol(this._cachedIArgumentsInterfaceType, new TypeScript.PullTypeResolutionContext(this));
            }

            return this._cachedIArgumentsInterfaceType;
        };

        PullTypeResolver.prototype.cachedRegExpInterfaceType = function () {
            if (!this._cachedRegExpInterfaceType) {
                this._cachedRegExpInterfaceType = this.getSymbolFromDeclPath("RegExp", [], TypeScript.PullElementKind.Interface) || this.semanticInfoChain.emptyTypeSymbol;
            }

            if (this._cachedRegExpInterfaceType && !this._cachedRegExpInterfaceType.isResolved) {
                this.resolveDeclaredSymbol(this._cachedRegExpInterfaceType, new TypeScript.PullTypeResolutionContext(this));
            }

            return this._cachedRegExpInterfaceType;
        };

        PullTypeResolver.prototype.cachedFunctionArgumentsSymbol = function () {
            if (!this._cachedFunctionArgumentsSymbol) {
                this._cachedFunctionArgumentsSymbol = new TypeScript.PullSymbol("arguments", TypeScript.PullElementKind.Variable);
                this._cachedFunctionArgumentsSymbol.type = this.cachedIArgumentsInterfaceType() || this.semanticInfoChain.anyTypeSymbol;
                this._cachedFunctionArgumentsSymbol.setResolved();

                var functionArgumentsDecl = new TypeScript.PullSynthesizedDecl("arguments", "arguments", TypeScript.PullElementKind.Parameter, 0 /* None */, null, this.semanticInfoChain);
                functionArgumentsDecl.setSymbol(this._cachedFunctionArgumentsSymbol);
                this._cachedFunctionArgumentsSymbol.addDeclaration(functionArgumentsDecl);
            }

            return this._cachedFunctionArgumentsSymbol;
        };

        // Section 3.8.1: November 18, 2013
        // If T is the primitive type Number, Boolean, or String, the apparent type
        // of T is the augmented form(as defined below) of the global interface type
        // 'Number', 'Boolean', or 'String'.
        // If T is an enum type, the apparent type of T is the augmented form of the
        // global interface type 'Number'.
        // If T is an object type, the apparent type of T is the augmented form of T.
        // If T is a type parameter, the apparent type of T is the augmented form
        // of the base constraint(section 3.4.1) of T.
        // Otherwise, the apparent type of T is T itself.
        PullTypeResolver.prototype.getApparentType = function (type) {
            if (type.isTypeParameter()) {
                var baseConstraint = type.getBaseConstraint(this.semanticInfoChain);
                if (baseConstraint === this.semanticInfoChain.anyTypeSymbol) {
                    // This is not yet in the spec. If a type parameter's base constraint is any
                    // (implicitly or explicitly), its apparent type is {}.
                    return this.semanticInfoChain.emptyTypeSymbol;
                } else {
                    // Don't return. We need to feed the resulting type thru the rest of the method
                    type = baseConstraint;
                }
            }
            if (type.isPrimitive()) {
                if (type === this.semanticInfoChain.numberTypeSymbol) {
                    return this.cachedNumberInterfaceType();
                }
                if (type === this.semanticInfoChain.booleanTypeSymbol) {
                    return this.cachedBooleanInterfaceType();
                }
                if (type === this.semanticInfoChain.stringTypeSymbol) {
                    return this.cachedStringInterfaceType();
                }
                return type;
            }
            if (type.isEnum()) {
                return this.cachedNumberInterfaceType();
            }
            return type;
        };

        PullTypeResolver.prototype.setTypeChecked = function (ast, context) {
            context.setTypeChecked(ast);
        };

        PullTypeResolver.prototype.canTypeCheckAST = function (ast, context) {
            return context.canTypeCheckAST(ast);
        };

        PullTypeResolver.prototype.setSymbolForAST = function (ast, symbol, context) {
            if (context && context.inProvisionalResolution()) {
                // Cache provisionally
                context.setSymbolForAST(ast, symbol);
            } else {
                // Cache globally
                this.semanticInfoChain.setSymbolForAST(ast, symbol);
            }
        };

        PullTypeResolver.prototype.getSymbolForAST = function (ast, context) {
            // Check global cache
            var symbol = this.semanticInfoChain.getSymbolForAST(ast);

            if (!symbol) {
                // Check provisional cache
                if (context && context.inProvisionalResolution()) {
                    symbol = context.getSymbolForAST(ast);
                }
            }

            return symbol;
        };

        PullTypeResolver.prototype.getASTForDecl = function (decl) {
            return this.semanticInfoChain.getASTForDecl(decl);
        };

        PullTypeResolver.prototype.getNewErrorTypeSymbol = function (name) {
            if (typeof name === "undefined") { name = null; }
            return new TypeScript.PullErrorTypeSymbol(this.semanticInfoChain.anyTypeSymbol, name);
        };

        PullTypeResolver.prototype.getEnclosingDecl = function (decl) {
            var declPath = decl.getParentPath();

            if (declPath.length > 1 && declPath[declPath.length - 1] === decl) {
                return declPath[declPath.length - 2];
            } else {
                return declPath[declPath.length - 1];
            }
        };

        PullTypeResolver.prototype.getExportedMemberSymbol = function (symbol, parent) {
            if (!(symbol.kind & (TypeScript.PullElementKind.Method | TypeScript.PullElementKind.Property))) {
                var isContainer = (parent.kind & (TypeScript.PullElementKind.Container | TypeScript.PullElementKind.DynamicModule)) !== 0;
                var containerType = !isContainer ? parent.getAssociatedContainerType() : parent;

                if (isContainer && containerType) {
                    if (symbol.anyDeclHasFlag(1 /* Exported */)) {
                        return symbol;
                    }

                    return null;
                }
            }

            return symbol;
        };

        // This is a modification of getMemberSymbol to fall back to Object or Function as necessary
        // November 18th, 2013, Section 3.8.1:
        // The augmented form of an object type T adds to T those members of the global interface type
        // 'Object' that aren’t hidden by members in T. Furthermore, if T has one or more call or
        // construct signatures, the augmented form of T adds to T the members of the global interface
        // type 'Function' that aren’t hidden by members in T.
        // Note: The most convenient way to do this is to have a method on PullTypeSymbol that
        // gives the apparent type. However, this would require synthesizing a new type because
        // almost every type in the system should inherit the properties from Object. It would essentially
        // double the number of type symbols, which would significantly increase memory usage.
        PullTypeResolver.prototype._getNamedPropertySymbolOfAugmentedType = function (symbolName, parent) {
            var memberSymbol = this.getNamedPropertySymbol(symbolName, TypeScript.PullElementKind.SomeValue, parent);
            if (memberSymbol) {
                return memberSymbol;
            }

            // Check if the parent has a call/construct signature, and if so, inherit from Function
            if (this.cachedFunctionInterfaceType() && parent.isFunctionType()) {
                memberSymbol = this.cachedFunctionInterfaceType().findMember(symbolName, true);
                if (memberSymbol) {
                    return memberSymbol;
                }
            }

            // Lastly, check the Object type
            if (this.cachedObjectInterfaceType()) {
                return this.cachedObjectInterfaceType().findMember(symbolName, true);
            }

            return null;
        };

        PullTypeResolver.prototype.getNamedPropertySymbol = function (symbolName, declSearchKind, parent) {
            var member = null;

            if (declSearchKind & TypeScript.PullElementKind.SomeValue) {
                member = parent.findMember(symbolName, true);
            } else if (declSearchKind & TypeScript.PullElementKind.SomeType) {
                member = parent.findNestedType(symbolName);
            } else if (declSearchKind & TypeScript.PullElementKind.SomeContainer) {
                member = parent.findNestedContainer(symbolName);
            }

            if (member) {
                return this.getExportedMemberSymbol(member, parent);
            }

            var containerType = parent.getAssociatedContainerType();

            if (containerType) {
                // If we were searching over the constructor type, we don't want to also search
                // over the class instance type (we only want to consider static fields)
                if (containerType.isClass()) {
                    return null;
                }

                parent = containerType;

                if (declSearchKind & TypeScript.PullElementKind.SomeValue) {
                    member = parent.findMember(symbolName, true);
                } else if (declSearchKind & TypeScript.PullElementKind.SomeType) {
                    member = parent.findNestedType(symbolName);
                } else if (declSearchKind & TypeScript.PullElementKind.SomeContainer) {
                    member = parent.findNestedContainer(symbolName);
                }

                if (member) {
                    return this.getExportedMemberSymbol(member, parent);
                }
            }

            if (parent.kind & TypeScript.PullElementKind.SomeContainer) {
                var typeDeclarations = parent.getDeclarations();
                var childDecls = null;

                for (var j = 0; j < typeDeclarations.length; j++) {
                    childDecls = typeDeclarations[j].searchChildDecls(symbolName, declSearchKind);

                    if (childDecls.length) {
                        member = childDecls[0].getSymbol();

                        if (!member) {
                            member = childDecls[0].getSignatureSymbol();
                        }
                        return this.getExportedMemberSymbol(member, parent);
                    }

                    // If we were looking  for some type or value, we need to look for alias so we can see if it has associated value or type symbol with it
                    if ((declSearchKind & TypeScript.PullElementKind.SomeType) !== 0 || (declSearchKind & TypeScript.PullElementKind.SomeValue) !== 0) {
                        childDecls = typeDeclarations[j].searchChildDecls(symbolName, TypeScript.PullElementKind.TypeAlias);
                        if (childDecls.length && childDecls[0].kind === TypeScript.PullElementKind.TypeAlias) {
                            var aliasSymbol = this.getExportedMemberSymbol(childDecls[0].getSymbol(), parent);
                            if (aliasSymbol) {
                                if ((declSearchKind & TypeScript.PullElementKind.SomeType) !== 0) {
                                    // Some type
                                    var typeSymbol = aliasSymbol.getExportAssignedTypeSymbol();
                                    if (typeSymbol) {
                                        return typeSymbol;
                                    }
                                } else {
                                    // Some value
                                    var valueSymbol = aliasSymbol.getExportAssignedValueSymbol();
                                    if (valueSymbol) {
                                        aliasSymbol.setIsUsedAsValue();
                                        return valueSymbol;
                                    }
                                }

                                return aliasSymbol;
                            }
                        }
                    }
                }
            }
        };

        // search for an unqualified symbol name within a given decl path
        PullTypeResolver.prototype.getSymbolFromDeclPath = function (symbolName, declPath, declSearchKind) {
            var _this = this;
            var symbol = null;

            // search backwards through the decl list
            //  - if the decl in question is a function, search its members
            //  - if the decl in question is a module, search the decl then the symbol
            //  - Otherwise, search globally
            var decl = null;
            var childDecls;
            var declSymbol = null;
            var declMembers;
            var pathDeclKind;
            var valDecl = null;
            var kind;
            var instanceSymbol = null;
            var instanceType = null;
            var childSymbol = null;

            // if search kind allows searching for enum members - expand search space to enums
            var allowedContainerDeclKind = TypeScript.PullElementKind.Container | TypeScript.PullElementKind.DynamicModule;
            if (TypeScript.hasFlag(declSearchKind, TypeScript.PullElementKind.EnumMember)) {
                allowedContainerDeclKind |= TypeScript.PullElementKind.Enum;
            }

            var isAcceptableAlias = function (symbol) {
                if (symbol.isAlias()) {
                    _this.resolveDeclaredSymbol(symbol);
                    if (TypeScript.hasFlag(declSearchKind, TypeScript.PullElementKind.SomeContainer)) {
                        if (symbol.assignedContainer() || symbol.getExportAssignedContainerSymbol()) {
                            return true;
                        }
                    } else if (TypeScript.hasFlag(declSearchKind, TypeScript.PullElementKind.SomeType)) {
                        // check if alias points to dynamic module - it should not be returned if type is requested
                        var type = symbol.getExportAssignedTypeSymbol();
                        if (type && type.kind !== TypeScript.PullElementKind.DynamicModule) {
                            return true;
                        }

                        // check if alias points to dynamic module - if should not be returned if type is requested
                        var type = symbol.assignedType();
                        if (type && type.kind !== TypeScript.PullElementKind.DynamicModule) {
                            return true;
                        }
                    } else if (TypeScript.hasFlag(declSearchKind, TypeScript.PullElementKind.SomeValue & ~TypeScript.PullElementKind.EnumMember)) {
                        // if alias is dereferenced to unresolved module - be conservative and treat it as value
                        if (symbol.assignedType() && symbol.assignedType().isError()) {
                            return true;
                        } else if (symbol.assignedValue() || symbol.getExportAssignedValueSymbol()) {
                            return true;
                        } else {
                            // alias is dereferenced to container with existing instance side - return alias
                            var assignedType = symbol.assignedType();
                            if (assignedType && assignedType.isContainer() && assignedType.getInstanceType()) {
                                return true;
                            }

                            // Spec Nov 18:
                            // 11.2.3 Unlike a non-instantiated internal module (section 10.1),
                            // an external module containing only interface types and non-instantiated internal modules still has a module instance associated with it, albeit one with no members.
                            var decls = symbol.getDeclarations();
                            var ast = decls[0].ast();
                            return ast.moduleReference.kind() === 245 /* ExternalModuleReference */;
                        }
                    }
                }

                return false;
            };

            var tryFindAlias = function (decl) {
                var childDecls = decl.searchChildDecls(symbolName, TypeScript.PullElementKind.TypeAlias);

                if (childDecls.length) {
                    var sym = childDecls[0].getSymbol();
                    if (isAcceptableAlias(sym)) {
                        return sym;
                    }
                }
                return null;
            };

            for (var i = declPath.length - 1; i >= 0; i--) {
                decl = declPath[i];
                pathDeclKind = decl.kind;

                if (decl.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock) {
                    return this.semanticInfoChain.anyTypeSymbol;
                }

                if (pathDeclKind & allowedContainerDeclKind) {
                    // first check locally
                    childDecls = decl.searchChildDecls(symbolName, declSearchKind);

                    if (childDecls.length) {
                        return childDecls[0].getSymbol();
                    }

                    var alias = tryFindAlias(decl);
                    if (alias) {
                        return alias;
                    }

                    if (declSearchKind & TypeScript.PullElementKind.SomeValue) {
                        // search "split" exported members
                        instanceSymbol = decl.getSymbol().getInstanceSymbol();

                        if (instanceSymbol) {
                            instanceType = instanceSymbol.type;

                            childSymbol = this.getNamedPropertySymbol(symbolName, declSearchKind, instanceType);

                            // Make sure we are not picking up a static from a class (it is never in scope)
                            if (childSymbol && (childSymbol.kind & declSearchKind) && !childSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.Static)) {
                                return childSymbol;
                            }
                        }

                        valDecl = decl.getValueDecl();

                        if (valDecl) {
                            decl = valDecl;
                        }
                    }

                    // otherwise, check the members
                    declSymbol = decl.getSymbol().type;

                    var childSymbol = this.getNamedPropertySymbol(symbolName, declSearchKind, declSymbol);

                    if (childSymbol && (childSymbol.kind & declSearchKind) && !childSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.Static)) {
                        return childSymbol;
                    }
                } else if ((declSearchKind & (TypeScript.PullElementKind.SomeType | TypeScript.PullElementKind.SomeContainer)) || !(pathDeclKind & TypeScript.PullElementKind.Class)) {
                    var candidateSymbol = null;

                    // If the decl is a function expression, we still want to check its children since it may be shadowed by one
                    // of its parameters
                    if (pathDeclKind === TypeScript.PullElementKind.FunctionExpression && symbolName === decl.getFunctionExpressionName()) {
                        candidateSymbol = decl.getSymbol();
                    }

                    childDecls = decl.searchChildDecls(symbolName, declSearchKind);

                    if (childDecls.length) {
                        // if the enclosing decl is a function of some sort, we need to ensure that it's bound
                        // otherwise, the child decl may not be properly bound if it's a parameter (since they're
                        // bound when binding the function symbol)
                        if (decl.kind & TypeScript.PullElementKind.SomeFunction) {
                            decl.ensureSymbolIsBound();
                        }
                        return childDecls[0].getSymbol();
                    }

                    if (candidateSymbol) {
                        return candidateSymbol;
                    }

                    var alias = tryFindAlias(decl);
                    if (alias) {
                        return alias;
                    }
                }
            }

            // otherwise, search globally
            symbol = this.semanticInfoChain.findSymbol([symbolName], declSearchKind);
            if (symbol) {
                return symbol;
            }

            // if element was not found and we were not searching for aliases- try to find globally defined type alias
            if (!TypeScript.hasFlag(declSearchKind, TypeScript.PullElementKind.TypeAlias)) {
                symbol = this.semanticInfoChain.findSymbol([symbolName], TypeScript.PullElementKind.TypeAlias);
                if (symbol && isAcceptableAlias(symbol)) {
                    return symbol;
                }
            }

            return null;
        };

        PullTypeResolver.prototype.getVisibleDeclsFromDeclPath = function (declPath, declSearchKind) {
            var result = [];
            var decl = null;
            var childDecls;
            var pathDeclKind;

            for (var i = declPath.length - 1; i >= 0; i--) {
                decl = declPath[i];
                pathDeclKind = decl.kind;

                var declKind = decl.kind;

                // First add locals
                // Child decls of classes and interfaces are members, and should only be visible as members of 'this'
                if (declKind !== TypeScript.PullElementKind.Class && declKind !== TypeScript.PullElementKind.Interface) {
                    this.addFilteredDecls(decl.getChildDecls(), declSearchKind, result);
                }

                switch (declKind) {
                    case TypeScript.PullElementKind.Container:
                    case TypeScript.PullElementKind.DynamicModule:
                        // Add members from other instances
                        var otherDecls = this.semanticInfoChain.findDeclsFromPath(declPath.slice(0, i + 1), TypeScript.PullElementKind.SomeContainer);
                        for (var j = 0, m = otherDecls.length; j < m; j++) {
                            var otherDecl = otherDecls[j];
                            if (otherDecl === decl) {
                                continue;
                            }

                            var otherDeclChildren = otherDecl.getChildDecls();
                            for (var k = 0, s = otherDeclChildren.length; k < s; k++) {
                                var otherDeclChild = otherDeclChildren[k];
                                if ((otherDeclChild.flags & 1 /* Exported */) && (otherDeclChild.kind & declSearchKind)) {
                                    result.push(otherDeclChild);
                                }
                            }
                        }

                        break;

                    case TypeScript.PullElementKind.Class:
                    case TypeScript.PullElementKind.Interface:
                        // Add generic types prameters
                        var parameters = decl.getTypeParameters();
                        if (parameters && parameters.length) {
                            this.addFilteredDecls(parameters, declSearchKind, result);
                        }

                        break;

                    case TypeScript.PullElementKind.FunctionExpression:
                        var functionExpressionName = decl.getFunctionExpressionName();
                        if (functionExpressionName) {
                            result.push(decl);
                        }

                    case TypeScript.PullElementKind.Function:
                    case TypeScript.PullElementKind.ConstructorMethod:
                    case TypeScript.PullElementKind.Method:
                        // Add generic types prameters
                        var parameters = decl.getTypeParameters();
                        if (parameters && parameters.length) {
                            this.addFilteredDecls(parameters, declSearchKind, result);
                        }

                        break;
                }
            }

            // Get the global decls
            var topLevelDecls = this.semanticInfoChain.topLevelDecls();
            for (var i = 0, n = topLevelDecls.length; i < n; i++) {
                var topLevelDecl = topLevelDecls[i];
                if (declPath.length > 0 && topLevelDecl.fileName() === declPath[0].fileName()) {
                    continue;
                }

                if (!topLevelDecl.isExternalModule()) {
                    this.addFilteredDecls(topLevelDecl.getChildDecls(), declSearchKind, result);
                }
            }

            return result;
        };

        PullTypeResolver.prototype.addFilteredDecls = function (decls, declSearchKind, result) {
            if (decls.length) {
                for (var i = 0, n = decls.length; i < n; i++) {
                    var decl = decls[i];
                    if (decl.kind & declSearchKind) {
                        result.push(decl);
                    }
                }
            }
        };

        PullTypeResolver.prototype.getVisibleDecls = function (enclosingDecl) {
            var declPath = enclosingDecl.getParentPath();

            var declSearchKind = TypeScript.PullElementKind.SomeType | TypeScript.PullElementKind.SomeContainer | TypeScript.PullElementKind.SomeValue;

            return this.getVisibleDeclsFromDeclPath(declPath, declSearchKind);
        };

        PullTypeResolver.prototype.getVisibleContextSymbols = function (enclosingDecl, context) {
            var contextualTypeSymbol = context.getContextualType();
            if (!contextualTypeSymbol || this.isAnyOrEquivalent(contextualTypeSymbol)) {
                return null;
            }

            var declSearchKind = TypeScript.PullElementKind.SomeType | TypeScript.PullElementKind.SomeContainer | TypeScript.PullElementKind.SomeValue;
            var members = contextualTypeSymbol.getAllMembers(declSearchKind, 2 /* externallyVisible */);

            for (var i = 0; i < members.length; i++) {
                members[i].setUnresolved();
            }

            return members;
        };

        PullTypeResolver.prototype.getVisibleMembersFromExpression = function (expression, enclosingDecl, context) {
            var lhs = this.resolveAST(expression, false, context);

            if (isTypesOnlyLocation(expression) && (lhs.kind === TypeScript.PullElementKind.Class || lhs.kind === TypeScript.PullElementKind.Interface || lhs.kind === TypeScript.PullElementKind.Enum)) {
                // No more sub types in these types
                return null;
            }

            var lhsType = lhs.type;
            if (!lhsType) {
                return null;
            }

            this.resolveDeclaredSymbol(lhsType, context);

            if (lhsType.isContainer() && lhsType.isAlias()) {
                lhsType = lhsType.getExportAssignedTypeSymbol();
            }

            if (this.isAnyOrEquivalent(lhsType)) {
                return null;
            }

            // Figure out if privates are available under the current scope
            var memberVisibilty = 2 /* externallyVisible */;
            var containerSymbol = lhsType;
            if (containerSymbol.kind === TypeScript.PullElementKind.ConstructorType) {
                containerSymbol = containerSymbol.getConstructSignatures()[0].returnType;
            }

            if (containerSymbol && containerSymbol.isClass()) {
                var declPath = enclosingDecl.getParentPath();
                if (declPath && declPath.length) {
                    var declarations = containerSymbol.getDeclarations();
                    for (var i = 0, n = declarations.length; i < n; i++) {
                        var declaration = declarations[i];
                        if (TypeScript.ArrayUtilities.contains(declPath, declaration)) {
                            memberVisibilty = 1 /* internallyVisible */;
                            break;
                        }
                    }
                }
            }

            var declSearchKind = TypeScript.PullElementKind.SomeType | TypeScript.PullElementKind.SomeContainer | TypeScript.PullElementKind.SomeValue;

            var members = [];

            if (lhsType.isContainer()) {
                var exportedAssignedContainerSymbol = lhsType.getExportAssignedContainerSymbol();
                if (exportedAssignedContainerSymbol) {
                    lhsType = exportedAssignedContainerSymbol;
                }
            }

            lhsType = this.getApparentType(lhsType);

            if (!lhsType.isResolved) {
                var potentiallySpecializedType = this.resolveDeclaredSymbol(lhsType, context);

                if (potentiallySpecializedType !== lhsType) {
                    if (!lhs.isType()) {
                        context.setTypeInContext(lhs, potentiallySpecializedType);
                    }

                    lhsType = potentiallySpecializedType;
                }
            }

            members = lhsType.getAllMembers(declSearchKind, memberVisibilty);

            // In order to show all members in type or value positions, query the associated symbol for members
            // on modules.
            if (lhsType.isContainer()) {
                var associatedInstance = lhsType.getInstanceSymbol();
                if (associatedInstance) {
                    var instanceType = associatedInstance.type;
                    this.resolveDeclaredSymbol(instanceType, context);
                    var instanceMembers = instanceType.getAllMembers(declSearchKind, memberVisibilty);
                    members = members.concat(instanceMembers);
                }

                var exportedContainer = lhsType.getExportAssignedContainerSymbol();
                if (exportedContainer) {
                    var exportedContainerMembers = exportedContainer.getAllMembers(declSearchKind, memberVisibilty);
                    members = members.concat(exportedContainerMembers);
                }
            } else if (!lhsType.isConstructor() && !lhsType.isEnum()) {
                var associatedContainerSymbol = lhsType.getAssociatedContainerType();
                if (associatedContainerSymbol) {
                    var containerType = associatedContainerSymbol.type;
                    this.resolveDeclaredSymbol(containerType, context);
                    var containerMembers = containerType.getAllMembers(declSearchKind, memberVisibilty);
                    members = members.concat(containerMembers);
                }
            }

            // could be a function symbol
            if (lhsType.isFunctionType() && this.cachedFunctionInterfaceType()) {
                members = members.concat(this.cachedFunctionInterfaceType().getAllMembers(declSearchKind, 2 /* externallyVisible */));
            }

            return members;
        };

        PullTypeResolver.prototype.isAnyOrEquivalent = function (type) {
            return (type === this.semanticInfoChain.anyTypeSymbol) || type.isError();
        };

        PullTypeResolver.prototype.resolveExternalModuleReference = function (idText, currentFileName) {
            var originalIdText = idText;
            var symbol = null;

            if (TypeScript.isRelative(originalIdText)) {
                // Find the module relative to current file
                var path = TypeScript.getRootFilePath(TypeScript.switchToForwardSlashes(currentFileName));
                symbol = this.semanticInfoChain.findExternalModule(path + idText);
            } else {
                idText = originalIdText;

                // Search in global context if there exists ambient module
                symbol = this.semanticInfoChain.findAmbientExternalModuleInGlobalContext(TypeScript.quoteStr(originalIdText));

                if (!symbol) {
                    // REVIEW: Technically, we shouldn't have to normalize here - we should normalize in addUnit.
                    // Still, normalizing here alows any language services to be free of assumptions
                    var path = TypeScript.getRootFilePath(TypeScript.switchToForwardSlashes(currentFileName));

                    while (symbol === null && path != "") {
                        symbol = this.semanticInfoChain.findExternalModule(path + idText);
                        if (symbol === null) {
                            if (path === '/') {
                                path = '';
                            } else {
                                path = TypeScript.normalizePath(path + "..");
                                path = path && path != '/' ? path + '/' : path;
                            }
                        }
                    }
                }
            }

            return symbol;
        };

        // PULLTODO: VERY IMPORTANT
        // Right now, the assumption is that the declaration's parse tree is still in memory
        // we need to add a cache-in/cache-out mechanism so that we can break the dependency on in-memory ASTs
        PullTypeResolver.prototype.resolveDeclaredSymbol = function (symbol, context) {
            if (!symbol || symbol.isResolved || symbol.isTypeReference()) {
                return symbol;
            }

            if (!context) {
                context = new TypeScript.PullTypeResolutionContext(this);
            }

            return this.resolveDeclaredSymbolWorker(symbol, context);
        };

        PullTypeResolver.prototype.resolveDeclaredSymbolWorker = function (symbol, context) {
            if (!symbol || symbol.isResolved) {
                return symbol;
            }

            if (symbol.inResolution) {
                if (!symbol.type && !symbol.isType()) {
                    symbol.type = this.semanticInfoChain.anyTypeSymbol;
                }

                return symbol;
            }

            var decls = symbol.getDeclarations();

            for (var i = 0; i < decls.length; i++) {
                var decl = decls[i];

                var ast = this.semanticInfoChain.getASTForDecl(decl);

                // if it's an object literal member, just return the symbol and wait for
                // the object lit to be resolved
                if (!ast || (ast.kind() === 139 /* GetAccessor */ && ast.parent.parent.kind() === 215 /* ObjectLiteralExpression */) || (ast.kind() === 140 /* SetAccessor */ && ast.parent.parent.kind() === 215 /* ObjectLiteralExpression */)) {
                    // We'll return the cached results, and let the decl be corrected on the next invalidation
                    return symbol;
                }

                // There are a few places where an identifier may represent the declaration of
                // a symbol.  They include:
                // A catch variable.  i.e.:  catch (e) { ...
                if (ast.parent && ast.parent.kind() === 236 /* CatchClause */ && ast.parent.identifier === ast) {
                    return symbol;
                }

                // A simple arrow parameter.  i.e.:   a => ...
                if (ast.parent && ast.parent.kind() === 219 /* SimpleArrowFunctionExpression */ && ast.parent.identifier === ast) {
                    return symbol;
                }

                // One of the names in a module declaration.  i.e.:  module A.B.C { ...
                // If our decl points at a single name of a module, then just resolve that individual module.
                var enclosingModule = TypeScript.ASTHelpers.getEnclosingModuleDeclaration(ast);
                var resolvedSymbol;
                if (TypeScript.ASTHelpers.isAnyNameOfModule(enclosingModule, ast)) {
                    resolvedSymbol = this.resolveSingleModuleDeclaration(enclosingModule, ast, context);
                } else if (ast.kind() === 120 /* SourceUnit */ && decl.kind === TypeScript.PullElementKind.DynamicModule) {
                    // Otherwise, if we have a decl for the top level external module, then just resolve that
                    // specific module.
                    resolvedSymbol = this.resolveModuleSymbol(decl.getSymbol(), context, null, null, ast);
                } else {
                    // This assert is here to catch potential stack overflows. There have been infinite recursions resulting
                    // from one of these decls pointing to a name expression.
                    TypeScript.Debug.assert(ast.kind() !== 11 /* IdentifierName */ && ast.kind() !== 212 /* MemberAccessExpression */);
                    resolvedSymbol = this.resolveAST(ast, false, context);
                }

                // if the symbol is a parameter property referenced in an out-of-order fashion, it may not have been resolved
                // along with the original property, so we need to "fix" its type here
                if (decl.kind === TypeScript.PullElementKind.Parameter && !symbol.isResolved && !symbol.type && resolvedSymbol && symbol.anyDeclHasFlag(TypeScript.PullElementFlags.PropertyParameter | TypeScript.PullElementFlags.ConstructorParameter)) {
                    symbol.type = resolvedSymbol.type;
                    symbol.setResolved();
                }
            }

            return symbol;
        };

        PullTypeResolver.prototype.resolveOtherDecl = function (otherDecl, context) {
            var astForOtherDecl = this.getASTForDecl(otherDecl);
            var moduleDecl = TypeScript.ASTHelpers.getEnclosingModuleDeclaration(astForOtherDecl);
            if (TypeScript.ASTHelpers.isAnyNameOfModule(moduleDecl, astForOtherDecl)) {
                this.resolveSingleModuleDeclaration(moduleDecl, astForOtherDecl, context);
            } else {
                this.resolveAST(astForOtherDecl, false, context);
            }
        };

        PullTypeResolver.prototype.resolveOtherDeclarations = function (astName, context) {
            var _this = this;
            var resolvedDecl = this.semanticInfoChain.getDeclForAST(astName);
            var symbol = resolvedDecl.getSymbol();

            var allDecls = symbol.getDeclarations();
            this.inResolvingOtherDeclsWalker.walkOtherPullDecls(resolvedDecl, symbol.getDeclarations(), function (otherDecl) {
                return _this.resolveOtherDecl(otherDecl, context);
            });
        };

        PullTypeResolver.prototype.resolveSourceUnit = function (sourceUnit, context) {
            // Ensure that any export assignments are resolved before we proceed.
            var enclosingDecl = this.getEnclosingDeclForAST(sourceUnit);
            var moduleSymbol = enclosingDecl.getSymbol();
            this.ensureAllSymbolsAreBound(moduleSymbol);

            this.resolveFirstExportAssignmentStatement(sourceUnit.moduleElements, context);
            this.resolveAST(sourceUnit.moduleElements, false, context);

            if (this.canTypeCheckAST(sourceUnit, context)) {
                this.typeCheckSourceUnit(sourceUnit, context);
            }

            return moduleSymbol;
        };

        PullTypeResolver.prototype.typeCheckSourceUnit = function (sourceUnit, context) {
            var _this = this;
            this.setTypeChecked(sourceUnit, context);

            this.resolveAST(sourceUnit.moduleElements, false, context);

            this.typeCheckCallBacks.push(function (context) {
                return _this.verifyUniquenessOfImportNamesInSourceUnit(sourceUnit);
            });
        };

        PullTypeResolver.prototype.verifyUniquenessOfImportNamesInSourceUnit = function (sourceUnit) {
            var _this = this;
            var enclosingDecl = this.semanticInfoChain.getDeclForAST(sourceUnit);

            var doesImportNameExistInOtherFiles = function (name) {
                var importSymbol = _this.semanticInfoChain.findTopLevelSymbol(name, TypeScript.PullElementKind.TypeAlias, null);
                return importSymbol && importSymbol.isAlias();
            };

            this.checkUniquenessOfImportNames([enclosingDecl], doesImportNameExistInOtherFiles);
        };

        PullTypeResolver.prototype.resolveEnumDeclaration = function (ast, context) {
            var containerDecl = this.semanticInfoChain.getDeclForAST(ast);
            var containerSymbol = containerDecl.getSymbol();

            if (containerSymbol.isResolved || containerSymbol.inResolution) {
                return containerSymbol;
            }

            containerSymbol.inResolution = true;

            var containerDecls = containerSymbol.getDeclarations();

            for (var i = 0; i < containerDecls.length; i++) {
                var childDecls = containerDecls[i].getChildDecls();

                for (var j = 0; j < childDecls.length; j++) {
                    childDecls[j].ensureSymbolIsBound();
                }
            }

            containerSymbol.setResolved();

            this.resolveOtherDeclarations(ast, context);

            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckEnumDeclaration(ast, context);
            }

            return containerSymbol;
        };

        PullTypeResolver.prototype.typeCheckEnumDeclaration = function (ast, context) {
            var _this = this;
            this.setTypeChecked(ast, context);

            this.resolveAST(ast.enumElements, false, context);
            var containerDecl = this.semanticInfoChain.getDeclForAST(ast);
            this.validateVariableDeclarationGroups(containerDecl, context);

            this.typeCheckCallBacks.push(function (context) {
                return _this.checkInitializersInEnumDeclarations(containerDecl, context);
            });

            if (!TypeScript.ASTHelpers.enumIsElided(ast)) {
                this.checkNameForCompilerGeneratedDeclarationCollision(ast, true, ast.identifier, context);
            }
        };

        PullTypeResolver.prototype.postTypeCheckEnumDeclaration = function (ast, context) {
            this.checkThisCaptureVariableCollides(ast, true, context);
        };

        PullTypeResolver.prototype.checkInitializersInEnumDeclarations = function (decl, context) {
            var symbol = decl.getSymbol();

            // check should be executed just once so run it only for one decl
            var declarations = symbol.getDeclarations();
            if (decl !== declarations[0]) {
                return;
            }

            // SPEC: NOV 18
            // It isn’t possible for one enum declaration to continue the automatic numbering sequence of another,
            // and when an enum type has multiple declarations, only one declaration is permitted to omit a value for the first member.
            var seenEnumDeclWithNoFirstMember = false;
            for (var i = 0; i < declarations.length; ++i) {
                var currentDecl = declarations[i];

                var ast = currentDecl.ast();
                if (ast.enumElements.nonSeparatorCount() === 0) {
                    continue;
                }

                var firstVariable = ast.enumElements.nonSeparatorAt(0);
                if (!firstVariable.equalsValueClause) {
                    if (!seenEnumDeclWithNoFirstMember) {
                        seenEnumDeclWithNoFirstMember = true;
                    } else {
                        this.semanticInfoChain.addDiagnosticFromAST(firstVariable, TypeScript.DiagnosticCode.In_enums_with_multiple_declarations_only_one_declaration_can_omit_an_initializer_for_the_first_enum_element);
                    }
                }
            }
        };

        //
        // Resolve a module declaration
        //
        PullTypeResolver.prototype.resolveModuleDeclaration = function (ast, context) {
            var result;

            if (ast.stringLiteral) {
                result = this.resolveSingleModuleDeclaration(ast, ast.stringLiteral, context);
            } else {
                var moduleNames = TypeScript.ASTHelpers.getModuleNames(ast.name);
                for (var i = 0, n = moduleNames.length; i < n; i++) {
                    result = this.resolveSingleModuleDeclaration(ast, moduleNames[i], context);
                }
            }

            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckModuleDeclaration(ast, context);
            }

            return result;
        };

        PullTypeResolver.prototype.ensureAllSymbolsAreBound = function (containerSymbol) {
            if (containerSymbol) {
                var containerDecls = containerSymbol.getDeclarations();

                for (var i = 0; i < containerDecls.length; i++) {
                    var childDecls = containerDecls[i].getChildDecls();

                    for (var j = 0; j < childDecls.length; j++) {
                        childDecls[j].ensureSymbolIsBound();
                    }
                }
            }
        };

        PullTypeResolver.prototype.resolveModuleSymbol = function (containerSymbol, context, moduleDeclAST, moduleDeclNameAST, sourceUnitAST) {
            if (containerSymbol.isResolved || containerSymbol.inResolution) {
                return containerSymbol;
            }

            containerSymbol.inResolution = true;
            this.ensureAllSymbolsAreBound(containerSymbol);

            var instanceSymbol = containerSymbol.getInstanceSymbol();

            // resolve the instance variable, if neccesary
            if (instanceSymbol) {
                this.resolveDeclaredSymbol(instanceSymbol, context);
            }

            var isLastName = TypeScript.ASTHelpers.isLastNameOfModule(moduleDeclAST, moduleDeclNameAST);
            if (isLastName) {
                this.resolveFirstExportAssignmentStatement(moduleDeclAST.moduleElements, context);
            } else if (sourceUnitAST) {
                this.resolveFirstExportAssignmentStatement(sourceUnitAST.moduleElements, context);
            }

            containerSymbol.setResolved();

            if (moduleDeclNameAST) {
                this.resolveOtherDeclarations(moduleDeclNameAST, context);
            }

            return containerSymbol;
        };

        PullTypeResolver.prototype.resolveFirstExportAssignmentStatement = function (moduleElements, context) {
            for (var i = 0, n = moduleElements.childCount(); i < n; i++) {
                var moduleElement = moduleElements.childAt(i);
                if (moduleElement.kind() === 134 /* ExportAssignment */) {
                    this.resolveExportAssignmentStatement(moduleElement, context);
                    return;
                }
            }
        };

        PullTypeResolver.prototype.resolveSingleModuleDeclaration = function (ast, astName, context) {
            var containerDecl = this.semanticInfoChain.getDeclForAST(astName);
            var containerSymbol = containerDecl.getSymbol();

            return this.resolveModuleSymbol(containerSymbol, context, ast, astName, null);
        };

        PullTypeResolver.prototype.typeCheckModuleDeclaration = function (ast, context) {
            if (ast.stringLiteral) {
                this.typeCheckSingleModuleDeclaration(ast, ast.stringLiteral, context);
            } else {
                var moduleNames = TypeScript.ASTHelpers.getModuleNames(ast.name);
                for (var i = 0, n = moduleNames.length; i < n; i++) {
                    this.typeCheckSingleModuleDeclaration(ast, moduleNames[i], context);
                }
            }
        };

        PullTypeResolver.prototype.typeCheckSingleModuleDeclaration = function (ast, astName, context) {
            var _this = this;
            this.setTypeChecked(ast, context);

            if (TypeScript.ASTHelpers.isLastNameOfModule(ast, astName)) {
                this.resolveAST(ast.moduleElements, false, context);
            }

            var containerDecl = this.semanticInfoChain.getDeclForAST(astName);
            this.validateVariableDeclarationGroups(containerDecl, context);

            if (ast.stringLiteral) {
                if (TypeScript.isRelative(ast.stringLiteral.valueText())) {
                    this.semanticInfoChain.addDiagnosticFromAST(ast.stringLiteral, TypeScript.DiagnosticCode.Ambient_external_module_declaration_cannot_specify_relative_module_name);
                }
            }

            if (!TypeScript.ASTHelpers.moduleIsElided(ast) && !ast.stringLiteral) {
                this.checkNameForCompilerGeneratedDeclarationCollision(astName, true, astName, context);
            }

            this.typeCheckCallBacks.push(function (context) {
                return _this.verifyUniquenessOfImportNamesInModule(containerDecl);
            });
        };

        PullTypeResolver.prototype.verifyUniquenessOfImportNamesInModule = function (decl) {
            var symbol = decl.getSymbol();
            if (!symbol) {
                return;
            }

            var decls = symbol.getDeclarations();

            // this check should be performed once per module.
            // to guarantee this we'll perform check only for one declaration that form the module
            if (decls[0] !== decl) {
                return;
            }

            this.checkUniquenessOfImportNames(decls);
        };

        PullTypeResolver.prototype.checkUniquenessOfImportNames = function (decls, doesNameExistOutside) {
            var _this = this;
            var importDeclarationNames;

            for (var i = 0; i < decls.length; ++i) {
                var childDecls = decls[i].getChildDecls();
                for (var j = 0; j < childDecls.length; ++j) {
                    var childDecl = childDecls[j];
                    if (childDecl.kind === TypeScript.PullElementKind.TypeAlias) {
                        importDeclarationNames = importDeclarationNames || TypeScript.createIntrinsicsObject();
                        importDeclarationNames[childDecl.name] = true;
                    }
                }
            }

            if (!importDeclarationNames && !doesNameExistOutside) {
                return;
            }

            for (var i = 0; i < decls.length; ++i) {
                // Walk over all variable declaration groups located in 'decls[i]',
                // pick the first item from the group and test if it name conflicts with the name of any of import declaration.
                // Taking just first item is enough since all variables in group have the same name
                this.scanVariableDeclarationGroups(decls[i], function (firstDeclInGroup) {
                    // Make sure the variable declaration doesn't conflict with an import declaration.
                    var nameConflict = importDeclarationNames && importDeclarationNames[firstDeclInGroup.name];
                    if (!nameConflict) {
                        nameConflict = doesNameExistOutside && doesNameExistOutside(firstDeclInGroup.name);
                        if (nameConflict) {
                            // save result for name == 'firstDeclInGroup.name' so if we'll see it again the result will be picked from the cache instead of invoking 'doesNameExistOutside' callback.
                            importDeclarationNames = importDeclarationNames || TypeScript.createIntrinsicsObject();
                            importDeclarationNames[firstDeclInGroup.name] = true;
                        }
                    }

                    if (nameConflict) {
                        _this.semanticInfoChain.addDiagnosticFromAST(firstDeclInGroup.ast(), TypeScript.DiagnosticCode.Variable_declaration_cannot_have_the_same_name_as_an_import_declaration);
                    }
                });
            }
        };

        PullTypeResolver.prototype.scanVariableDeclarationGroups = function (enclosingDecl, firstDeclHandler, subsequentDeclHandler) {
            var declGroups = enclosingDecl.getVariableDeclGroups();

            for (var i = 0; i < declGroups.length; i++) {
                var firstSymbol = null;
                var enclosingDeclForFirstSymbol = null;

                if (enclosingDecl.kind === TypeScript.PullElementKind.Script && declGroups[i].length) {
                    var name = declGroups[i][0].name;
                    var candidateSymbol = this.semanticInfoChain.findTopLevelSymbol(name, TypeScript.PullElementKind.Variable, enclosingDecl);
                    if (candidateSymbol && candidateSymbol.isResolved) {
                        if (!candidateSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.ImplicitVariable)) {
                            firstSymbol = candidateSymbol;
                        }
                    }
                }

                for (var j = 0; j < declGroups[i].length; j++) {
                    var decl = declGroups[i][j];

                    var name = decl.name;

                    var symbol = decl.getSymbol();

                    if (j === 0) {
                        firstDeclHandler(decl);
                        if (!subsequentDeclHandler) {
                            break;
                        }

                        if (!firstSymbol || !firstSymbol.type) {
                            firstSymbol = symbol;
                            continue;
                        }
                    }

                    subsequentDeclHandler(decl, firstSymbol);
                }
            }
        };

        PullTypeResolver.prototype.postTypeCheckModuleDeclaration = function (ast, context) {
            this.checkThisCaptureVariableCollides(ast, true, context);
        };

        PullTypeResolver.prototype.isTypeRefWithoutTypeArgs = function (term) {
            if (term.kind() === 11 /* IdentifierName */) {
                return true;
            } else if (term.kind() === 121 /* QualifiedName */) {
                var binex = term;

                if (binex.right.kind() === 11 /* IdentifierName */) {
                    return true;
                }
            }

            return false;
        };

        PullTypeResolver.prototype.createInstantiatedType = function (type, typeArguments) {
            if (!type.isGeneric()) {
                return type;
            }

            // if the type had previously been instantiated, we want to re-instantiate the type arguments.  Otherwise,
            // we just use the type parameters.  E.g., for
            //      class Foo<T> {
            //          public <U>(p: Foo<U>): void
            //      }
            // For parameter 'p', we'd need to specialize from 'T' to 'U' to 'any', so we'd need to request p's type arguments
            // and not its type parameters
            var typeParameters = type.getTypeArgumentsOrTypeParameters();

            var typeParameterArgumentMap = [];

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameterArgumentMap[typeParameters[i].pullSymbolID] = typeArguments[i] || new TypeScript.PullErrorTypeSymbol(this.semanticInfoChain.anyTypeSymbol, typeParameters[i].name);
            }

            return TypeScript.PullInstantiatedTypeReferenceSymbol.create(this, type, typeParameterArgumentMap);
        };

        //
        // Resolve a reference type (class or interface) type parameters, implements and extends clause, members, call, construct and index signatures
        //
        PullTypeResolver.prototype.resolveReferenceTypeDeclaration = function (classOrInterface, name, heritageClauses, context) {
            var _this = this;
            var typeDecl = this.semanticInfoChain.getDeclForAST(classOrInterface);
            var enclosingDecl = this.getEnclosingDecl(typeDecl);
            var typeDeclSymbol = typeDecl.getSymbol();
            var typeDeclIsClass = classOrInterface.kind() === 131 /* ClassDeclaration */;
            var hasVisited = this.getSymbolForAST(classOrInterface, context) !== null;

            if ((typeDeclSymbol.isResolved && hasVisited) || (typeDeclSymbol.inResolution && !context.isInBaseTypeResolution())) {
                return typeDeclSymbol;
            }

            var wasResolving = typeDeclSymbol.inResolution;
            typeDeclSymbol.startResolving();

            // ensure that all members are bound
            var typeRefDecls = typeDeclSymbol.getDeclarations();

            for (var i = 0; i < typeRefDecls.length; i++) {
                var childDecls = typeRefDecls[i].getChildDecls();

                for (var j = 0; j < childDecls.length; j++) {
                    childDecls[j].ensureSymbolIsBound();
                }
            }

            // Resolve Type Parameters
            if (!typeDeclSymbol.isResolved) {
                var typeDeclTypeParameters = typeDeclSymbol.getTypeParameters();
                for (var i = 0; i < typeDeclTypeParameters.length; i++) {
                    this.resolveDeclaredSymbol(typeDeclTypeParameters[i], context);
                }
            }

            var wasInBaseTypeResolution = context.startBaseTypeResolution();

            // if it's a "split" interface type, we'll need to consider constituent extends lists separately
            if (!typeDeclIsClass && !hasVisited) {
                typeDeclSymbol.resetKnownBaseTypeCount();
            }

            // Extends list
            var extendsClause = TypeScript.ASTHelpers.getExtendsHeritageClause(heritageClauses);
            if (extendsClause) {
                for (var i = typeDeclSymbol.getKnownBaseTypeCount(); i < extendsClause.typeNames.nonSeparatorCount(); i = typeDeclSymbol.getKnownBaseTypeCount()) {
                    typeDeclSymbol.incrementKnownBaseCount();
                    var parentType = this.resolveTypeReference(extendsClause.typeNames.nonSeparatorAt(i), context);

                    if (typeDeclSymbol.isValidBaseKind(parentType, true)) {
                        this.setSymbolForAST(extendsClause.typeNames.nonSeparatorAt(i), parentType, null);

                        // Do not add parentType as a base if it already added, or if it will cause a cycle as it already inherits from typeDeclSymbol
                        if (!typeDeclSymbol.hasBase(parentType) && !parentType.hasBase(typeDeclSymbol)) {
                            typeDeclSymbol.addExtendedType(parentType);

                            var specializations = typeDeclSymbol.getKnownSpecializations();

                            for (var j = 0; j < specializations.length; j++) {
                                specializations[j].addExtendedType(parentType);
                            }
                        }
                    } else if (parentType && !this.getSymbolForAST(extendsClause.typeNames.nonSeparatorAt(i), context)) {
                        this.setSymbolForAST(extendsClause.typeNames.nonSeparatorAt(i), parentType, null);
                    }
                }
            }

            var implementsClause = TypeScript.ASTHelpers.getImplementsHeritageClause(heritageClauses);
            if (implementsClause && typeDeclIsClass) {
                var extendsCount = extendsClause ? extendsClause.typeNames.nonSeparatorCount() : 0;
                for (var i = typeDeclSymbol.getKnownBaseTypeCount(); ((i - extendsCount) >= 0) && ((i - extendsCount) < implementsClause.typeNames.nonSeparatorCount()); i = typeDeclSymbol.getKnownBaseTypeCount()) {
                    typeDeclSymbol.incrementKnownBaseCount();
                    var implementedTypeAST = implementsClause.typeNames.nonSeparatorAt(i - extendsCount);
                    var implementedType = this.resolveTypeReference(implementedTypeAST, context);

                    if (typeDeclSymbol.isValidBaseKind(implementedType, false)) {
                        this.setSymbolForAST(implementsClause.typeNames.nonSeparatorAt(i - extendsCount), implementedType, null);

                        // Do not add parentType as a base if it already added, or if it will cause a cycle as it already inherits from typeDeclSymbol
                        if (!typeDeclSymbol.hasBase(implementedType) && !implementedType.hasBase(typeDeclSymbol)) {
                            typeDeclSymbol.addImplementedType(implementedType);
                        }
                    } else if (implementedType && !this.getSymbolForAST(implementsClause.typeNames.nonSeparatorAt(i - extendsCount), context)) {
                        this.setSymbolForAST(implementsClause.typeNames.nonSeparatorAt(i - extendsCount), implementedType, null);
                    }
                }
            }

            context.doneBaseTypeResolution(wasInBaseTypeResolution);

            if (wasInBaseTypeResolution) {
                // Do not resolve members as yet
                typeDeclSymbol.inResolution = false;

                // Store off and resolve the reference type after we've finished checking the file
                // (This way, we'll still properly resolve the type even if its parent was already resolved during
                // base type resolution, making the type otherwise inaccessible).
                this.typeCheckCallBacks.push(function (context) {
                    if (classOrInterface.kind() === 131 /* ClassDeclaration */) {
                        _this.resolveClassDeclaration(classOrInterface, context);
                    } else {
                        _this.resolveInterfaceDeclaration(classOrInterface, context);
                    }
                });

                return typeDeclSymbol;
            }

            this.setSymbolForAST(name, typeDeclSymbol, context);
            this.setSymbolForAST(classOrInterface, typeDeclSymbol, context);

            typeDeclSymbol.setResolved();

            return typeDeclSymbol;
        };

        //
        // Resolve a class declaration
        //
        // A class's implements and extends lists are not pre-bound, so they must be bound here
        // Once bound, we can add the parent type's members to the class
        //
        PullTypeResolver.prototype.resolveClassDeclaration = function (classDeclAST, context) {
            var classDecl = this.semanticInfoChain.getDeclForAST(classDeclAST);
            var classDeclSymbol = classDecl.getSymbol();

            if (!classDeclSymbol.isResolved) {
                this.resolveReferenceTypeDeclaration(classDeclAST, classDeclAST.identifier, classDeclAST.heritageClauses, context);

                var constructorMethod = classDeclSymbol.getConstructorMethod();
                var extendedTypes = classDeclSymbol.getExtendedTypes();
                var parentType = extendedTypes.length ? extendedTypes[0] : null;

                if (constructorMethod) {
                    // Need to ensure our constructor type can properly see our parent type's
                    // constructor type before going and resolving our members.
                    if (parentType) {
                        var parentConstructorSymbol = parentType.getConstructorMethod();

                        // this will only be null if we have upstream errors
                        if (parentConstructorSymbol) {
                            var parentConstructorTypeSymbol = parentConstructorSymbol.type;
                            var constructorTypeSymbol = constructorMethod.type;
                            if (!constructorTypeSymbol.hasBase(parentConstructorTypeSymbol)) {
                                constructorTypeSymbol.addExtendedType(parentConstructorTypeSymbol);
                            }
                        }
                    }

                    if (!classDeclSymbol.isResolved) {
                        return classDeclSymbol;
                    }
                }

                this.resolveOtherDeclarations(classDeclAST, context);
            }

            if (this.canTypeCheckAST(classDeclAST, context)) {
                this.typeCheckClassDeclaration(classDeclAST, context);
            }

            return classDeclSymbol;
        };

        PullTypeResolver.prototype.typeCheckTypeParametersOfTypeDeclaration = function (classOrInterface, context) {
            var _this = this;
            var typeParametersList = classOrInterface.kind() == 131 /* ClassDeclaration */ ? classOrInterface.typeParameterList : classOrInterface.typeParameterList;

            if (typeParametersList) {
                var typeDecl = this.semanticInfoChain.getDeclForAST(classOrInterface);
                var typeDeclSymbol = typeDecl.getSymbol();

                for (var i = 0; i < typeParametersList.typeParameters.nonSeparatorCount(); i++) {
                    // Resolve the type parameter
                    var typeParameterAST = typeParametersList.typeParameters.nonSeparatorAt(i);
                    this.resolveTypeParameterDeclaration(typeParameterAST, context);

                    var typeParameterDecl = this.semanticInfoChain.getDeclForAST(typeParameterAST);
                    var typeParameterSymbol = typeParameterDecl.getSymbol();

                    this.checkSymbolPrivacy(typeDeclSymbol, typeParameterSymbol, function (symbol) {
                        return _this.typeParameterOfTypeDeclarationPrivacyErrorReporter(classOrInterface, typeParameterAST, typeParameterSymbol, symbol, context);
                    });
                }
            }
        };

        PullTypeResolver.prototype.typeCheckClassDeclaration = function (classDeclAST, context) {
            this.setTypeChecked(classDeclAST, context);

            var classDecl = this.semanticInfoChain.getDeclForAST(classDeclAST);
            var classDeclSymbol = classDecl.getSymbol();

            // Add for post typeChecking if we want to verify name collision with _this
            this.checkNameForCompilerGeneratedDeclarationCollision(classDeclAST, true, classDeclAST.identifier, context);
            this.resolveAST(classDeclAST.classElements, false, context);

            this.typeCheckTypeParametersOfTypeDeclaration(classDeclAST, context);
            this.typeCheckBases(classDeclAST, classDeclAST.identifier, classDeclAST.heritageClauses, classDeclSymbol, this.getEnclosingDecl(classDecl), context);

            if (!classDeclSymbol.hasBaseTypeConflict()) {
                this.typeCheckMembersAgainstIndexer(classDeclSymbol, classDecl, context);
            }

            this.checkTypeForDuplicateIndexSignatures(classDeclSymbol);
        };

        PullTypeResolver.prototype.postTypeCheckClassDeclaration = function (classDeclAST, context) {
            this.checkThisCaptureVariableCollides(classDeclAST, true, context);
        };

        PullTypeResolver.prototype.resolveTypeSymbolSignatures = function (typeSymbol, context) {
            // Resolve call, construct and index signatures
            var callSignatures = typeSymbol.getCallSignatures();
            for (var i = 0; i < callSignatures.length; i++) {
                this.resolveDeclaredSymbol(callSignatures[i], context);
            }

            var constructSignatures = typeSymbol.getConstructSignatures();
            for (var i = 0; i < constructSignatures.length; i++) {
                this.resolveDeclaredSymbol(constructSignatures[i], context);
            }

            var indexSignatures = typeSymbol.getIndexSignatures();
            for (var i = 0; i < indexSignatures.length; i++) {
                this.resolveDeclaredSymbol(indexSignatures[i], context);
            }
        };

        PullTypeResolver.prototype.resolveInterfaceDeclaration = function (interfaceDeclAST, context) {
            this.resolveReferenceTypeDeclaration(interfaceDeclAST, interfaceDeclAST.identifier, interfaceDeclAST.heritageClauses, context);

            var interfaceDecl = this.semanticInfoChain.getDeclForAST(interfaceDeclAST);
            var interfaceDeclSymbol = interfaceDecl.getSymbol();

            this.resolveTypeSymbolSignatures(interfaceDeclSymbol, context);

            if (interfaceDeclSymbol.isResolved) {
                this.resolveOtherDeclarations(interfaceDeclAST, context);

                if (this.canTypeCheckAST(interfaceDeclAST, context)) {
                    this.typeCheckInterfaceDeclaration(interfaceDeclAST, context);
                }
            }

            return interfaceDeclSymbol;
        };

        PullTypeResolver.prototype.typeCheckInterfaceDeclaration = function (interfaceDeclAST, context) {
            this.setTypeChecked(interfaceDeclAST, context);

            var interfaceDecl = this.semanticInfoChain.getDeclForAST(interfaceDeclAST);
            var interfaceDeclSymbol = interfaceDecl.getSymbol();

            this.resolveAST(interfaceDeclAST.body.typeMembers, false, context);

            this.typeCheckTypeParametersOfTypeDeclaration(interfaceDeclAST, context);
            this.typeCheckBases(interfaceDeclAST, interfaceDeclAST.identifier, interfaceDeclAST.heritageClauses, interfaceDeclSymbol, this.getEnclosingDecl(interfaceDecl), context);

            if (!interfaceDeclSymbol.hasBaseTypeConflict()) {
                this.typeCheckMembersAgainstIndexer(interfaceDeclSymbol, interfaceDecl, context);
            }

            // Only check for duplicate index signatures once per symbol
            // We check that this interface decl is the last one for this symbol. The reason we don't
            // use the first decl is because in case one decl is in lib.d.ts, the experience is
            // slightly better in the language service because of the order we tend to type check
            // the files in. Lib.d.ts is usually last in the language service, but it contains the
            // first decl. Therefore, picking the last decl will report the error sooner.
            var allInterfaceDecls = interfaceDeclSymbol.getDeclarations();
            if (interfaceDecl === allInterfaceDecls[allInterfaceDecls.length - 1]) {
                this.checkTypeForDuplicateIndexSignatures(interfaceDeclSymbol);
            }

            if (!this.checkInterfaceDeclForIdenticalTypeParameters(interfaceDeclAST, context)) {
                this.semanticInfoChain.addDiagnosticFromAST(interfaceDeclAST.identifier, TypeScript.DiagnosticCode.All_declarations_of_an_interface_must_have_identical_type_parameters);
            }
        };

        // Spec section 7.2:
        // When a generic interface has multiple declarations, all declarations must have identical type parameter lists,
        // i.e.identical type parameter names with identical constraints in identical order.
        PullTypeResolver.prototype.checkInterfaceDeclForIdenticalTypeParameters = function (interfaceDeclAST, context) {
            var interfaceDecl = this.semanticInfoChain.getDeclForAST(interfaceDeclAST);
            var interfaceDeclSymbol = interfaceDecl.getSymbol();

            // Only generic symbols need the type parameter verification
            if (!interfaceDeclSymbol.isGeneric()) {
                return true;
            }

            // Verify against the first interface declaration only
            var firstInterfaceDecl = interfaceDeclSymbol.getDeclarations()[0];
            if (firstInterfaceDecl == interfaceDecl) {
                return true;
            }

            var typeParameters = interfaceDecl.getTypeParameters();
            var firstInterfaceDeclTypeParameters = firstInterfaceDecl.getTypeParameters();

            // Type parameter length should match
            if (typeParameters.length != firstInterfaceDeclTypeParameters.length) {
                return false;
            }

            for (var i = 0; i < typeParameters.length; i++) {
                var typeParameter = typeParameters[i];
                var firstInterfaceDeclTypeParameter = firstInterfaceDeclTypeParameters[i];

                // Type parameter names should be identical
                if (typeParameter.name != firstInterfaceDeclTypeParameter.name) {
                    return false;
                }

                var typeParameterSymbol = typeParameter.getSymbol();
                var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameter);
                var firstInterfaceDeclTypeParameterAST = this.semanticInfoChain.getASTForDecl(firstInterfaceDeclTypeParameter);

                // Constraint should be present or absent in both the decls
                if (!!typeParameterAST.constraint != !!firstInterfaceDeclTypeParameterAST.constraint) {
                    return false;
                }

                // If the constraint is present, it should be identical
                if (typeParameterAST.constraint) {
                    var typeParameterConstraint = this.resolveAST(typeParameterAST.constraint, false, context);
                    if (!this.typesAreIdenticalWithNewEnclosingTypes(typeParameterConstraint, typeParameterSymbol.getConstraint(), context)) {
                        return false;
                    }
                }
            }

            return true;
        };

        // November 18, 2013: Section 3.7.4:
        // An object type can contain at most one string index signature and one numeric index signature.
        PullTypeResolver.prototype.checkTypeForDuplicateIndexSignatures = function (enclosingTypeSymbol) {
            var indexSignatures = enclosingTypeSymbol.getOwnIndexSignatures();
            var firstStringIndexer = null;
            var firstNumberIndexer = null;
            for (var i = 0; i < indexSignatures.length; i++) {
                var currentIndexer = indexSignatures[i];
                var currentParameterType = currentIndexer.parameters[0].type;
                TypeScript.Debug.assert(currentParameterType);
                if (currentParameterType === this.semanticInfoChain.stringTypeSymbol) {
                    if (firstStringIndexer) {
                        this.semanticInfoChain.addDiagnosticFromAST(currentIndexer.getDeclarations()[0].ast(), TypeScript.DiagnosticCode.Duplicate_string_index_signature, null, [this.semanticInfoChain.locationFromAST(firstStringIndexer.getDeclarations()[0].ast())]);
                        return;
                    } else {
                        firstStringIndexer = currentIndexer;
                    }
                } else if (currentParameterType === this.semanticInfoChain.numberTypeSymbol) {
                    if (firstNumberIndexer) {
                        this.semanticInfoChain.addDiagnosticFromAST(currentIndexer.getDeclarations()[0].ast(), TypeScript.DiagnosticCode.Duplicate_number_index_signature, null, [this.semanticInfoChain.locationFromAST(firstNumberIndexer.getDeclarations()[0].ast())]);
                        return;
                    } else {
                        firstNumberIndexer = currentIndexer;
                    }
                }
            }
        };

        PullTypeResolver.prototype.filterSymbol = function (symbol, kind, enclosingDecl, context) {
            if (symbol) {
                if (symbol.kind & kind) {
                    return symbol;
                }

                if (symbol.isAlias()) {
                    this.resolveDeclaredSymbol(symbol, context);

                    var alias = symbol;
                    if (kind & TypeScript.PullElementKind.SomeContainer) {
                        return alias.getExportAssignedContainerSymbol();
                    } else if (kind & TypeScript.PullElementKind.SomeType) {
                        return alias.getExportAssignedTypeSymbol();
                    } else if (kind & TypeScript.PullElementKind.SomeValue) {
                        return alias.getExportAssignedValueSymbol();
                    }
                }
            }
            return null;
        };

        PullTypeResolver.prototype.getMemberSymbolOfKind = function (symbolName, kind, pullTypeSymbol, enclosingDecl, context) {
            var memberSymbol = this.getNamedPropertySymbol(symbolName, kind, pullTypeSymbol);

            // Verify that the symbol is actually of the given kind
            return {
                symbol: this.filterSymbol(memberSymbol, kind, enclosingDecl, context),
                aliasSymbol: memberSymbol && memberSymbol.isAlias() ? memberSymbol : null
            };
        };

        PullTypeResolver.prototype.resolveIdentifierOfInternalModuleReference = function (importDecl, identifier, moduleSymbol, enclosingDecl, context) {
            var rhsName = identifier.valueText();
            if (rhsName.length === 0) {
                return null;
            }

            var moduleTypeSymbol = moduleSymbol.type;
            var memberSymbol = this.getMemberSymbolOfKind(rhsName, TypeScript.PullElementKind.SomeContainer, moduleTypeSymbol, enclosingDecl, context);
            var containerSymbol = memberSymbol.symbol;
            var valueSymbol = null;
            var typeSymbol = null;
            var aliasSymbol = null;

            var acceptableAlias = true;

            if (containerSymbol) {
                acceptableAlias = (containerSymbol.kind & TypeScript.PullElementKind.AcceptableAlias) !== 0;
                aliasSymbol = memberSymbol.aliasSymbol;
            }

            if (!acceptableAlias && containerSymbol && containerSymbol.kind === TypeScript.PullElementKind.TypeAlias) {
                this.resolveDeclaredSymbol(containerSymbol, context);
                var aliasedAssignedValue = containerSymbol.getExportAssignedValueSymbol();
                var aliasedAssignedType = containerSymbol.getExportAssignedTypeSymbol();
                var aliasedAssignedContainer = containerSymbol.getExportAssignedContainerSymbol();

                if (aliasedAssignedValue || aliasedAssignedType || aliasedAssignedContainer) {
                    aliasSymbol = containerSymbol;
                    valueSymbol = aliasedAssignedValue;
                    typeSymbol = aliasedAssignedType;
                    containerSymbol = aliasedAssignedContainer;
                    acceptableAlias = true;
                }
            }

            // check for valid export assignment type (variable, function, class, interface, enum, internal module)
            if (!acceptableAlias) {
                this.semanticInfoChain.addDiagnosticFromAST(identifier, TypeScript.DiagnosticCode.Import_declaration_referencing_identifier_from_internal_module_can_only_be_made_with_variables_functions_classes_interfaces_enums_and_internal_modules);
                return null;
            }

            // if we haven't already gotten a value or type from the alias, look for them now
            if (!valueSymbol) {
                if (moduleTypeSymbol.getInstanceSymbol()) {
                    memberSymbol = this.getMemberSymbolOfKind(rhsName, TypeScript.PullElementKind.SomeValue, moduleTypeSymbol.getInstanceSymbol().type, enclosingDecl, context);
                    valueSymbol = memberSymbol.symbol;
                    if (valueSymbol && memberSymbol.aliasSymbol) {
                        aliasSymbol = memberSymbol.aliasSymbol;
                    }
                }
            }

            if (!typeSymbol) {
                memberSymbol = this.getMemberSymbolOfKind(rhsName, TypeScript.PullElementKind.SomeType, moduleTypeSymbol, enclosingDecl, context);
                typeSymbol = memberSymbol.symbol;
                if (typeSymbol && memberSymbol.aliasSymbol) {
                    aliasSymbol = memberSymbol.aliasSymbol;
                }
            }

            if (!valueSymbol && !typeSymbol && !containerSymbol) {
                this.semanticInfoChain.addDiagnosticFromAST(identifier, TypeScript.DiagnosticCode.Could_not_find_symbol_0_in_module_1, [rhsName, moduleSymbol.toString()]);
                return null;
            }

            if (!typeSymbol && containerSymbol) {
                typeSymbol = containerSymbol;
            }

            return {
                valueSymbol: valueSymbol,
                typeSymbol: typeSymbol,
                containerSymbol: containerSymbol,
                aliasSymbol: aliasSymbol
            };
        };

        PullTypeResolver.prototype.resolveModuleReference = function (importDecl, moduleNameExpr, enclosingDecl, context, declPath) {
            TypeScript.Debug.assert(moduleNameExpr.kind() === 121 /* QualifiedName */ || moduleNameExpr.kind() === 11 /* IdentifierName */ || moduleNameExpr.kind() === 14 /* StringLiteral */, "resolving module reference should always be either name or member reference");

            var moduleSymbol = null;
            var moduleName;

            if (moduleNameExpr.kind() === 121 /* QualifiedName */) {
                var dottedNameAST = moduleNameExpr;
                var moduleContainer = this.resolveModuleReference(importDecl, dottedNameAST.left, enclosingDecl, context, declPath);
                if (moduleContainer) {
                    moduleName = dottedNameAST.right.valueText();

                    // We dont care about setting alias symbol here, because it has to be exported member which would make the import statement to emit anyways
                    moduleSymbol = this.getMemberSymbolOfKind(moduleName, TypeScript.PullElementKind.Container, moduleContainer.type, enclosingDecl, context).symbol;
                    if (!moduleSymbol) {
                        this.semanticInfoChain.addDiagnosticFromAST(dottedNameAST.right, TypeScript.DiagnosticCode.Could_not_find_module_0_in_module_1, [moduleName, moduleContainer.toString()]);
                    }
                }
            } else {
                var valueText = moduleNameExpr.kind() === 11 /* IdentifierName */ ? moduleNameExpr.valueText() : moduleNameExpr.valueText();
                var text = moduleNameExpr.kind() === 11 /* IdentifierName */ ? moduleNameExpr.text() : moduleNameExpr.text();

                if (text.length > 0) {
                    var resolvedModuleNameSymbol = this.getSymbolFromDeclPath(valueText, declPath, TypeScript.PullElementKind.Container);
                    moduleSymbol = this.filterSymbol(resolvedModuleNameSymbol, TypeScript.PullElementKind.Container, enclosingDecl, context);
                    if (moduleSymbol) {
                        // Import declaration isn't contextual so set the symbol and diagnostic message irrespective of the context
                        this.semanticInfoChain.setSymbolForAST(moduleNameExpr, moduleSymbol);
                        if (resolvedModuleNameSymbol.isAlias()) {
                            this.semanticInfoChain.setAliasSymbolForAST(moduleNameExpr, resolvedModuleNameSymbol);
                            var importDeclSymbol = importDecl.getSymbol();
                            importDeclSymbol.addLinkedAliasSymbol(resolvedModuleNameSymbol);
                        }
                    } else {
                        this.semanticInfoChain.addDiagnosticFromAST(moduleNameExpr, TypeScript.DiagnosticCode.Unable_to_resolve_module_reference_0, [valueText]);
                    }
                }
            }

            return moduleSymbol;
        };

        PullTypeResolver.prototype.resolveInternalModuleReference = function (importStatementAST, context) {
            // ModuleName or ModuleName.Identifier or ModuleName.ModuleName....Identifier
            var importDecl = this.semanticInfoChain.getDeclForAST(importStatementAST);
            var enclosingDecl = this.getEnclosingDecl(importDecl);

            var moduleReference = importStatementAST.moduleReference;

            var aliasExpr = moduleReference.kind() === 245 /* ExternalModuleReference */ ? moduleReference.stringLiteral : moduleReference.moduleName;

            var declPath = enclosingDecl.getParentPath();
            var aliasedType = null;
            var importDeclSymbol = importDecl.getSymbol();

            if (aliasExpr.kind() === 11 /* IdentifierName */ || aliasExpr.kind() === 14 /* StringLiteral */) {
                var moduleSymbol = this.resolveModuleReference(importDecl, aliasExpr, enclosingDecl, context, declPath);
                if (moduleSymbol) {
                    aliasedType = moduleSymbol.type;
                    this.semanticInfoChain.setAliasSymbolForAST(moduleReference, this.semanticInfoChain.getAliasSymbolForAST(aliasExpr));
                    if (aliasedType.anyDeclHasFlag(TypeScript.PullElementFlags.InitializedModule)) {
                        var moduleName = aliasExpr.kind() === 11 /* IdentifierName */ ? aliasExpr.valueText() : aliasExpr.valueText();
                        var valueSymbol = this.getSymbolFromDeclPath(moduleName, declPath, TypeScript.PullElementKind.SomeValue);
                        var instanceSymbol = aliasedType.getInstanceSymbol();

                        // If there is module and it is instantiated, value symbol needs to refer to the module instance symbol
                        if (valueSymbol && (instanceSymbol != valueSymbol || valueSymbol.type === aliasedType)) {
                            var text = aliasExpr.kind() === 11 /* IdentifierName */ ? aliasExpr.text() : aliasExpr.text();
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(aliasExpr, TypeScript.DiagnosticCode.Internal_module_reference_0_in_import_declaration_does_not_reference_module_instance_for_1, [text, moduleSymbol.type.toString(enclosingDecl ? enclosingDecl.getSymbol() : null)]));
                        } else {
                            // Set value symbol, type and container setting will be taken care of later using aliasedType
                            importDeclSymbol.setAssignedValueSymbol(valueSymbol);
                        }
                    }
                } else {
                    aliasedType = this.getNewErrorTypeSymbol();
                }
            } else if (aliasExpr.kind() === 121 /* QualifiedName */) {
                var dottedNameAST = aliasExpr;
                var moduleSymbol = this.resolveModuleReference(importDecl, dottedNameAST.left, enclosingDecl, context, declPath);
                if (moduleSymbol) {
                    var identifierResolution = this.resolveIdentifierOfInternalModuleReference(importDecl, dottedNameAST.right, moduleSymbol, enclosingDecl, context);
                    if (identifierResolution) {
                        importDeclSymbol.setAssignedValueSymbol(identifierResolution.valueSymbol);
                        importDeclSymbol.setAssignedTypeSymbol(identifierResolution.typeSymbol);
                        importDeclSymbol.setAssignedContainerSymbol(identifierResolution.containerSymbol);
                        this.semanticInfoChain.setAliasSymbolForAST(moduleReference, identifierResolution.aliasSymbol);
                        return null;
                    }
                }
            }

            if (!aliasedType) {
                // unresolved alias - set error as assigned type
                importDeclSymbol.setAssignedTypeSymbol(this.getNewErrorTypeSymbol());
            }

            return aliasedType;
        };

        PullTypeResolver.prototype.resolveImportDeclaration = function (importStatementAST, context) {
            // internal or external? (Does it matter?)
            var importDecl = this.semanticInfoChain.getDeclForAST(importStatementAST);
            var enclosingDecl = this.getEnclosingDecl(importDecl);
            var importDeclSymbol = importDecl.getSymbol();

            var aliasedType = null;

            if (importDeclSymbol.isResolved) {
                return importDeclSymbol;
            }

            importDeclSymbol.startResolving();

            // the alias name may be a string literal, in which case we'll need to convert it to a type
            // reference
            if (importStatementAST.moduleReference.kind() === 245 /* ExternalModuleReference */) {
                // dynamic module name (string literal)
                var modPath = importStatementAST.moduleReference.stringLiteral.valueText();
                var declPath = enclosingDecl.getParentPath();

                aliasedType = this.resolveExternalModuleReference(modPath, importDecl.fileName());

                if (!aliasedType) {
                    var path = importStatementAST.moduleReference.stringLiteral.text();
                    this.semanticInfoChain.addDiagnosticFromAST(importStatementAST, TypeScript.DiagnosticCode.Unable_to_resolve_external_module_0, [path]);
                    aliasedType = this.getNewErrorTypeSymbol();
                }
            } else {
                aliasedType = this.resolveInternalModuleReference(importStatementAST, context);
            }

            if (aliasedType) {
                if (!aliasedType.isContainer()) {
                    this.semanticInfoChain.addDiagnosticFromAST(importStatementAST, TypeScript.DiagnosticCode.Module_cannot_be_aliased_to_a_non_module_type);
                    if (!aliasedType.isError()) {
                        aliasedType = this.getNewErrorTypeSymbol();
                    }
                }

                if (aliasedType.isContainer()) {
                    importDeclSymbol.setAssignedContainerSymbol(aliasedType);
                }
                importDeclSymbol.setAssignedTypeSymbol(aliasedType);

                // Import declaration isn't contextual so set the symbol and diagnostic message irrespective of the context
                this.setSymbolForAST(importStatementAST.moduleReference, aliasedType, null);
            }

            importDeclSymbol.setResolved();

            this.resolveDeclaredSymbol(importDeclSymbol.assignedValue(), context);
            this.resolveDeclaredSymbol(importDeclSymbol.assignedType(), context);
            this.resolveDeclaredSymbol(importDeclSymbol.assignedContainer(), context);

            // these checks should be made after 'resolveDeclaredSymbol' calls.since 'resolveDeclaredSymbol' can set 'assignedValue' to aliasedType
            if (aliasedType && importDeclSymbol.anyDeclHasFlag(1 /* Exported */)) {
                importDeclSymbol.setIsUsedInExportedAlias();

                if (aliasedType.isContainer() && aliasedType.getExportAssignedValueSymbol()) {
                    importDeclSymbol.setIsUsedAsValue();
                }
            }

            if (this.canTypeCheckAST(importStatementAST, context)) {
                this.typeCheckImportDeclaration(importStatementAST, context);
            }

            return importDeclSymbol;
        };

        PullTypeResolver.prototype.typeCheckImportDeclaration = function (importStatementAST, context) {
            var _this = this;
            this.setTypeChecked(importStatementAST, context);

            var importDecl = this.semanticInfoChain.getDeclForAST(importStatementAST);
            var enclosingDecl = this.getEnclosingDecl(importDecl);
            var importDeclSymbol = importDecl.getSymbol();

            if (importStatementAST.moduleReference.kind() === 245 /* ExternalModuleReference */) {
                if (this.compilationSettings.noResolve()) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(importStatementAST, TypeScript.DiagnosticCode.Import_declaration_cannot_refer_to_external_module_reference_when_noResolve_option_is_set, null));
                }

                var modPath = importStatementAST.moduleReference.stringLiteral.valueText();
                if (enclosingDecl.kind === TypeScript.PullElementKind.DynamicModule) {
                    var ast = TypeScript.ASTHelpers.getEnclosingModuleDeclaration(this.getASTForDecl(enclosingDecl));
                    if (ast && ast.kind() === 130 /* ModuleDeclaration */) {
                        if (TypeScript.isRelative(modPath)) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(importStatementAST, TypeScript.DiagnosticCode.Import_declaration_in_an_ambient_external_module_declaration_cannot_reference_external_module_through_relative_external_module_name));
                        }
                    }
                }
            }

            var checkPrivacy;
            if (importStatementAST.moduleReference.kind() === 245 /* ExternalModuleReference */) {
                var containerSymbol = importDeclSymbol.getExportAssignedContainerSymbol();
                var container = containerSymbol ? containerSymbol.getContainer() : null;
                if (container && container.kind === TypeScript.PullElementKind.DynamicModule) {
                    checkPrivacy = true;
                }
            } else {
                checkPrivacy = true;
            }

            if (checkPrivacy) {
                // Check if import satisfies type privacy
                var typeSymbol = importDeclSymbol.getExportAssignedTypeSymbol();
                var containerSymbol = importDeclSymbol.getExportAssignedContainerSymbol();
                var valueSymbol = importDeclSymbol.getExportAssignedValueSymbol();

                this.checkSymbolPrivacy(importDeclSymbol, containerSymbol, function (symbol) {
                    var messageCode = TypeScript.DiagnosticCode.Exported_import_declaration_0_is_assigned_container_that_is_or_is_using_inaccessible_module_1;
                    var messageArguments = [importDeclSymbol.getScopedName(enclosingDecl ? enclosingDecl.getSymbol() : null), symbol.getScopedName(enclosingDecl ? enclosingDecl.getSymbol() : null, false, false, true)];
                    context.postDiagnostic(_this.semanticInfoChain.diagnosticFromAST(importStatementAST, messageCode, messageArguments));
                });

                if (typeSymbol !== containerSymbol) {
                    this.checkSymbolPrivacy(importDeclSymbol, typeSymbol, function (symbol) {
                        var messageCode = symbol.isContainer() && !symbol.isEnum() ? TypeScript.DiagnosticCode.Exported_import_declaration_0_is_assigned_type_that_is_using_inaccessible_module_1 : TypeScript.DiagnosticCode.Exported_import_declaration_0_is_assigned_type_that_has_or_is_using_private_type_1;

                        var messageArguments = [importDeclSymbol.getScopedName(enclosingDecl ? enclosingDecl.getSymbol() : null), symbol.getScopedName(enclosingDecl ? enclosingDecl.getSymbol() : null, false, false, true)];
                        context.postDiagnostic(_this.semanticInfoChain.diagnosticFromAST(importStatementAST, messageCode, messageArguments));
                    });
                }

                if (valueSymbol) {
                    this.checkSymbolPrivacy(importDeclSymbol, valueSymbol.type, function (symbol) {
                        var messageCode = symbol.isContainer() && !symbol.isEnum() ? TypeScript.DiagnosticCode.Exported_import_declaration_0_is_assigned_value_with_type_that_is_using_inaccessible_module_1 : TypeScript.DiagnosticCode.Exported_import_declaration_0_is_assigned_value_with_type_that_has_or_is_using_private_type_1;
                        var messageArguments = [importDeclSymbol.getScopedName(enclosingDecl ? enclosingDecl.getSymbol() : null), symbol.getScopedName(enclosingDecl ? enclosingDecl.getSymbol() : null, false, false, true)];
                        context.postDiagnostic(_this.semanticInfoChain.diagnosticFromAST(importStatementAST, messageCode, messageArguments));
                    });
                }
            }

            // Check if there's a name collision with the import's name.  Note: checkNameFor...
            // will do the check in a post typeCheck callback.  This is critical for import
            // statements as we need to know how the import is actually used in order for us to
            // know if there will be a collision or not.  For example, if the import is never used
            // as a value, and is only used as a type, then no collision will occur.
            this.checkNameForCompilerGeneratedDeclarationCollision(importStatementAST, true, importStatementAST.identifier, context);
        };

        PullTypeResolver.prototype.postTypeCheckImportDeclaration = function (importStatementAST, context) {
            var importDecl = this.semanticInfoChain.getDeclForAST(importStatementAST);
            var importSymbol = importDecl.getSymbol();

            var isUsedAsValue = importSymbol.isUsedAsValue();
            var hasAssignedValue = importStatementAST.moduleReference.kind() !== 245 /* ExternalModuleReference */ && importSymbol.getExportAssignedValueSymbol() !== null;

            if (isUsedAsValue || hasAssignedValue) {
                this.checkThisCaptureVariableCollides(importStatementAST, true, context);
            }
        };

        PullTypeResolver.prototype.resolveExportAssignmentStatement = function (exportAssignmentAST, context) {
            var id = exportAssignmentAST.identifier.valueText();
            if (id.length === 0) {
                // No point trying to resolve an export assignment without an actual identifier.
                return this.semanticInfoChain.anyTypeSymbol;
            }

            // get the identifier text
            var valueSymbol = null;
            var typeSymbol = null;
            var containerSymbol = null;

            var enclosingDecl = this.getEnclosingDeclForAST(exportAssignmentAST);
            var parentSymbol = enclosingDecl.getSymbol();

            if (!parentSymbol.isType() && parentSymbol.isContainer()) {
                // Error
                // Export assignments may only be used at the top-level of external modules
                this.semanticInfoChain.addDiagnosticFromAST(exportAssignmentAST, TypeScript.DiagnosticCode.Export_assignments_may_only_be_used_at_the_top_level_of_external_modules);
                return this.semanticInfoChain.anyTypeSymbol;
            }

            // The Identifier of an export assignment must name a variable, function, class, interface,
            // enum, or internal module declared at the top level in the external module.
            // So look for the id only from this dynamic module
            var declPath = enclosingDecl !== null ? [enclosingDecl] : [];

            containerSymbol = this.getSymbolFromDeclPath(id, declPath, TypeScript.PullElementKind.SomeContainer);

            var acceptableAlias = true;

            if (containerSymbol) {
                acceptableAlias = (containerSymbol.kind & TypeScript.PullElementKind.AcceptableAlias) !== 0;
            }

            if (!acceptableAlias && containerSymbol && containerSymbol.kind === TypeScript.PullElementKind.TypeAlias) {
                this.resolveDeclaredSymbol(containerSymbol, context);

                var aliasSymbol = containerSymbol;
                var aliasedAssignedValue = aliasSymbol.getExportAssignedValueSymbol();
                var aliasedAssignedType = aliasSymbol.getExportAssignedTypeSymbol();
                var aliasedAssignedContainer = aliasSymbol.getExportAssignedContainerSymbol();

                if (aliasedAssignedValue || aliasedAssignedType || aliasedAssignedContainer) {
                    valueSymbol = aliasedAssignedValue;
                    typeSymbol = aliasedAssignedType;
                    containerSymbol = aliasedAssignedContainer;
                    aliasSymbol.setTypeUsedExternally();
                    if (valueSymbol) {
                        aliasSymbol.setIsUsedAsValue();
                    }
                    acceptableAlias = true;
                }
            }

            // check for valid export assignment type (variable, function, class, interface, enum, internal module)
            if (!acceptableAlias) {
                // Error
                // Export assignments may only be made with variables, functions, classes, interfaces, enums and internal modules
                this.semanticInfoChain.addDiagnosticFromAST(exportAssignmentAST, TypeScript.DiagnosticCode.Export_assignments_may_only_be_made_with_variables_functions_classes_interfaces_enums_and_internal_modules);
                return this.semanticInfoChain.voidTypeSymbol;
            }

            // if we haven't already gotten a value or type from the alias, look for them now
            if (!valueSymbol) {
                valueSymbol = this.getSymbolFromDeclPath(id, declPath, TypeScript.PullElementKind.SomeValue);
            }
            if (!typeSymbol) {
                typeSymbol = this.getSymbolFromDeclPath(id, declPath, TypeScript.PullElementKind.SomeType);
            }

            if (!valueSymbol && !typeSymbol && !containerSymbol) {
                // Error
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(exportAssignmentAST, TypeScript.DiagnosticCode.Could_not_find_symbol_0, [id]));
                return this.semanticInfoChain.voidTypeSymbol;
            }

            if (valueSymbol) {
                parentSymbol.setExportAssignedValueSymbol(valueSymbol);
            }
            if (typeSymbol) {
                parentSymbol.setExportAssignedTypeSymbol(typeSymbol);
            }
            if (containerSymbol) {
                parentSymbol.setExportAssignedContainerSymbol(containerSymbol);
            }

            this.resolveDeclaredSymbol(valueSymbol, context);
            this.resolveDeclaredSymbol(typeSymbol, context);
            this.resolveDeclaredSymbol(containerSymbol, context);

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveAnyFunctionTypeSignature = function (funcDeclAST, typeParameters, parameterList, returnTypeAnnotation, context) {
            var functionDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
            TypeScript.Debug.assert(functionDecl);

            var funcDeclSymbol = functionDecl.getSymbol();

            var signature = funcDeclSymbol.kind === TypeScript.PullElementKind.ConstructorType ? funcDeclSymbol.getConstructSignatures()[0] : funcDeclSymbol.getCallSignatures()[0];

            // resolve the return type annotation
            if (returnTypeAnnotation) {
                signature.returnType = this.resolveTypeReference(returnTypeAnnotation, context);
            }

            if (typeParameters) {
                for (var i = 0; i < typeParameters.typeParameters.nonSeparatorCount(); i++) {
                    this.resolveTypeParameterDeclaration(typeParameters.typeParameters.nonSeparatorAt(i), context);
                }
            }

            // link parameters and resolve their annotations
            if (parameterList) {
                for (var i = 0; i < parameterList.parameters.nonSeparatorCount(); i++) {
                    this.resolveFunctionTypeSignatureParameter(parameterList.parameters.nonSeparatorAt(i), signature, functionDecl, context);
                }
            }

            funcDeclSymbol.setResolved();

            if (this.canTypeCheckAST(funcDeclAST, context)) {
                this.setTypeChecked(funcDeclAST, context);
                this.typeCheckFunctionOverloads(funcDeclAST, context);
            }

            return funcDeclSymbol;
        };

        PullTypeResolver.prototype.resolveFunctionTypeSignatureParameter = function (argDeclAST, signature, enclosingDecl, context) {
            var paramDecl = this.semanticInfoChain.getDeclForAST(argDeclAST);
            var paramSymbol = paramDecl.getSymbol();

            if (argDeclAST.typeAnnotation) {
                var typeRef = this.resolveTypeReference(TypeScript.ASTHelpers.getType(argDeclAST), context);

                if (paramSymbol.isVarArg && !typeRef.isArrayNamedTypeReference()) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Rest_parameters_must_be_array_types));
                    typeRef = this.getNewErrorTypeSymbol();
                }

                context.setTypeInContext(paramSymbol, typeRef);
            } else {
                if (paramSymbol.isVarArg) {
                    if (this.cachedArrayInterfaceType()) {
                        context.setTypeInContext(paramSymbol, this.createInstantiatedType(this.cachedArrayInterfaceType(), [this.semanticInfoChain.anyTypeSymbol]));
                    } else {
                        context.setTypeInContext(paramSymbol, this.semanticInfoChain.anyTypeSymbol);
                    }
                } else {
                    context.setTypeInContext(paramSymbol, this.semanticInfoChain.anyTypeSymbol);
                }

                // if the noImplicitAny flag is set to be true, report an error
                if (this.compilationSettings.noImplicitAny()) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Parameter_0_of_function_type_implicitly_has_an_any_type, [argDeclAST.identifier.text()]));
                }
            }

            if (TypeScript.hasFlag(paramDecl.flags, TypeScript.PullElementFlags.Optional) && argDeclAST.equalsValueClause && isTypesOnlyLocation(argDeclAST)) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Default_arguments_are_only_allowed_in_implementation));
            }

            paramSymbol.setResolved();
        };

        PullTypeResolver.prototype.resolveFunctionExpressionParameter = function (argDeclAST, id, typeExpr, equalsValueClause, contextParam, enclosingDecl, context) {
            var paramDecl = this.semanticInfoChain.getDeclForAST(argDeclAST);
            var paramSymbol = paramDecl.getSymbol();
            var contextualType = contextParam && contextParam.type;
            var isImplicitAny = false;

            if (typeExpr) {
                var typeRef = this.resolveTypeReference(typeExpr, context);

                if (paramSymbol.isVarArg && !typeRef.isArrayNamedTypeReference()) {
                    var diagnostic = context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Rest_parameters_must_be_array_types));
                    typeRef = this.getNewErrorTypeSymbol();
                }

                // The contextual type now gets overriden by the type annotation
                contextualType = typeRef || contextualType;
            }
            if (contextualType) {
                // November 18, 2013, Section 4.12.2:
                // When a function expression is inferentially typed (section 4.9.3) and a type assigned
                // to a parameter in that expression references type parameters for which inferences are
                // being made, the corresponding inferred type arguments to become fixed and no further
                // candidate inferences are made for them.
                if (context.isInferentiallyTyping()) {
                    contextualType = context.fixAllTypeParametersReferencedByType(contextualType, this);
                }
                context.setTypeInContext(paramSymbol, contextualType);
            } else if (paramSymbol.isVarArg) {
                if (this.cachedArrayInterfaceType()) {
                    context.setTypeInContext(paramSymbol, this.createInstantiatedType(this.cachedArrayInterfaceType(), [this.semanticInfoChain.anyTypeSymbol]));
                } else {
                    context.setTypeInContext(paramSymbol, this.semanticInfoChain.anyTypeSymbol);
                }
                isImplicitAny = true;
            }

            // Resolve the function expression parameter init only if we have contexual type to evaluate the expression in or we are in typeCheck
            var canTypeCheckAST = this.canTypeCheckAST(argDeclAST, context);
            if (equalsValueClause && (canTypeCheckAST || !contextualType)) {
                if (contextualType) {
                    context.propagateContextualType(contextualType);
                }

                var initExprSymbol = this.resolveAST(equalsValueClause, contextualType !== null, context);

                if (contextualType) {
                    context.popAnyContextualType();
                }

                if (!initExprSymbol || !initExprSymbol.type) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Unable_to_resolve_type_of_0, [id.text()]));

                    if (!contextualType) {
                        context.setTypeInContext(paramSymbol, this.getNewErrorTypeSymbol(paramSymbol.name));
                    }
                } else {
                    var initTypeSymbol = this.getInstanceTypeForAssignment(argDeclAST, initExprSymbol.type, context);
                    if (!contextualType) {
                        // Set the type to the inferred initializer type
                        context.setTypeInContext(paramSymbol, initTypeSymbol.widenedType(this, equalsValueClause, context));
                        isImplicitAny = initTypeSymbol !== paramSymbol.type;
                    } else {
                        var comparisonInfo = new TypeComparisonInfo();

                        var isAssignable = this.sourceIsAssignableToTarget(initTypeSymbol, contextualType, argDeclAST, context, comparisonInfo);

                        if (!isAssignable) {
                            var enclosingSymbol = this.getEnclosingSymbolForAST(argDeclAST);
                            if (comparisonInfo.message) {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Cannot_convert_0_to_1_NL_2, [initTypeSymbol.toString(enclosingSymbol), contextualType.toString(enclosingSymbol), comparisonInfo.message]));
                            } else {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Cannot_convert_0_to_1, [initTypeSymbol.toString(enclosingSymbol), contextualType.toString(enclosingSymbol)]));
                            }
                        }
                    }
                }
            }

            // If we do not have any type for it, set it to any
            if (!contextualType && !paramSymbol.isVarArg && !initTypeSymbol) {
                context.setTypeInContext(paramSymbol, this.semanticInfoChain.anyTypeSymbol);
                isImplicitAny = true;
            }

            // if the noImplicitAny flag is set to be true, report an error
            if (isImplicitAny && this.compilationSettings.noImplicitAny()) {
                // there is a name for function expression then use the function expression name otherwise use "lambda"
                var functionExpressionName = paramDecl.getParentDecl().getFunctionExpressionName();
                if (functionExpressionName) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Parameter_0_of_1_implicitly_has_an_any_type, [id.text(), functionExpressionName]));
                } else {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(argDeclAST, TypeScript.DiagnosticCode.Parameter_0_of_lambda_function_implicitly_has_an_any_type, [id.text()]));
                }
            }

            if (canTypeCheckAST) {
                this.checkNameForCompilerGeneratedDeclarationCollision(argDeclAST, true, id, context);
            }

            paramSymbol.setResolved();
        };

        PullTypeResolver.prototype.checkNameForCompilerGeneratedDeclarationCollision = function (astWithName, isDeclaration, name, context) {
            var compilerReservedName = getCompilerReservedName(name);
            switch (compilerReservedName) {
                case 1 /* _this */:
                    this.postTypeCheckWorkitems.push(astWithName);
                    return;

                case 2 /* _super */:
                    this.checkSuperCaptureVariableCollides(astWithName, isDeclaration, context);
                    return;

                case 3 /* arguments */:
                    this.checkArgumentsCollides(astWithName, context);
                    return;

                case 4 /* _i */:
                    this.checkIndexOfRestArgumentInitializationCollides(astWithName, isDeclaration, context);
                    return;

                case 5 /* require */:
                case 6 /* exports */:
                    if (isDeclaration) {
                        this.checkExternalModuleRequireExportsCollides(astWithName, name, context);
                    }
                    return;
            }
        };

        PullTypeResolver.prototype.hasRestParameterCodeGen = function (someFunctionDecl) {
            var enclosingAST = this.getASTForDecl(someFunctionDecl);
            var nodeType = enclosingAST.kind();

            // If the method/function/constructor is non ambient, with code block and has rest parameter it would have the rest parameter code gen
            if (nodeType === 129 /* FunctionDeclaration */) {
                var functionDeclaration = enclosingAST;
                return !TypeScript.hasFlag(someFunctionDecl.kind === TypeScript.PullElementKind.Method ? someFunctionDecl.getParentDecl().flags : someFunctionDecl.flags, TypeScript.PullElementFlags.Ambient) && functionDeclaration.block && TypeScript.lastParameterIsRest(functionDeclaration.callSignature.parameterList);
            } else if (nodeType === 135 /* MemberFunctionDeclaration */) {
                var memberFunction = enclosingAST;
                return !TypeScript.hasFlag(someFunctionDecl.kind === TypeScript.PullElementKind.Method ? someFunctionDecl.getParentDecl().flags : someFunctionDecl.flags, TypeScript.PullElementFlags.Ambient) && memberFunction.block && TypeScript.lastParameterIsRest(memberFunction.callSignature.parameterList);
            } else if (nodeType === 137 /* ConstructorDeclaration */) {
                var constructorDeclaration = enclosingAST;
                return !TypeScript.hasFlag(someFunctionDecl.getParentDecl().flags, TypeScript.PullElementFlags.Ambient) && constructorDeclaration.block && TypeScript.lastParameterIsRest(constructorDeclaration.callSignature.parameterList);
            } else if (nodeType === 218 /* ParenthesizedArrowFunctionExpression */) {
                var arrowFunctionExpression = enclosingAST;
                return TypeScript.lastParameterIsRest(arrowFunctionExpression.callSignature.parameterList);
            } else if (nodeType === 222 /* FunctionExpression */) {
                var functionExpression = enclosingAST;
                return TypeScript.lastParameterIsRest(functionExpression.callSignature.parameterList);
            }

            return false;
        };

        PullTypeResolver.prototype.checkArgumentsCollides = function (ast, context) {
            if (ast.kind() === 242 /* Parameter */) {
                var enclosingDecl = this.getEnclosingDeclForAST(ast);
                if (TypeScript.hasFlag(enclosingDecl.kind, TypeScript.PullElementKind.SomeFunction)) {
                    if (this.hasRestParameterCodeGen(enclosingDecl)) {
                        // It is error to use the arguments as variable name or parameter name in function with rest parameters
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Duplicate_identifier_arguments_Compiler_uses_arguments_to_initialize_rest_parameters));
                    }
                }
            }
        };

        PullTypeResolver.prototype.checkIndexOfRestArgumentInitializationCollides = function (ast, isDeclaration, context) {
            if (!isDeclaration || ast.kind() === 242 /* Parameter */) {
                var enclosingDecl = this.getEnclosingDeclForAST(ast);
                var declPath = isDeclaration ? [enclosingDecl] : (enclosingDecl ? enclosingDecl.getParentPath() : []);
                var resolvedSymbol = null;
                var resolvedSymbolContainer;
                for (var i = declPath.length - 1; i >= 0; i--) {
                    var decl = declPath[i];
                    if (!isDeclaration) {
                        // Get the symbol this ast would be resolved to
                        if (!resolvedSymbol) {
                            resolvedSymbol = this.resolveNameExpression(ast, context);
                            if (resolvedSymbol.isError()) {
                                return;
                            }

                            resolvedSymbolContainer = resolvedSymbol.getContainer();
                        }

                        // If the resolved symbol was declared in this decl, then it would always resolve to this symbol
                        // so stop looking in the decls as it is valid usage of _i
                        if (resolvedSymbolContainer && TypeScript.ArrayUtilities.contains(resolvedSymbolContainer.getDeclarations(), decl)) {
                            break;
                        }
                    }

                    if (TypeScript.hasFlag(decl.kind, TypeScript.PullElementKind.SomeFunction)) {
                        if (this.hasRestParameterCodeGen(decl)) {
                            // It is error to use the _i varible name
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, isDeclaration ? TypeScript.DiagnosticCode.Duplicate_identifier_i_Compiler_uses_i_to_initialize_rest_parameter : TypeScript.DiagnosticCode.Expression_resolves_to_variable_declaration_i_that_compiler_uses_to_initialize_rest_parameter));
                        }
                    }
                }
            }
        };

        PullTypeResolver.prototype.checkExternalModuleRequireExportsCollides = function (ast, name, context) {
            var enclosingDecl = this.getEnclosingDeclForAST(ast);

            var enclosingModule = TypeScript.ASTHelpers.getEnclosingModuleDeclaration(name);
            if (TypeScript.ASTHelpers.isAnyNameOfModule(enclosingModule, name)) {
                // If we're actually the name of a module, then we want the enclosing decl for the
                // module that we're in.
                enclosingDecl = this.getEnclosingDeclForAST(enclosingModule);
            }

            // If the declaration is in external module
            if (enclosingDecl && enclosingDecl.kind === TypeScript.PullElementKind.DynamicModule) {
                var decl = this.semanticInfoChain.getDeclForAST(ast);

                // This is not ambient declaration, then there would be code gen
                if (!TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.Ambient)) {
                    // It is error to use 'require' or 'exports' as name for the declaration
                    var nameText = name.valueText();
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Duplicate_identifier_0_Compiler_reserves_name_1_in_top_level_scope_of_an_external_module, [nameText, nameText]));
                }
            }
        };

        PullTypeResolver.prototype.resolveObjectTypeTypeReference = function (objectType, context) {
            var interfaceDecl = this.semanticInfoChain.getDeclForAST(objectType);
            TypeScript.Debug.assert(interfaceDecl);

            var interfaceSymbol = interfaceDecl.getSymbol();
            TypeScript.Debug.assert(interfaceSymbol);

            if (objectType.typeMembers) {
                var memberDecl = null;
                var memberSymbol = null;
                var memberType = null;
                var typeMembers = objectType.typeMembers;

                for (var i = 0; i < typeMembers.nonSeparatorCount(); i++) {
                    memberDecl = this.semanticInfoChain.getDeclForAST(typeMembers.nonSeparatorAt(i));
                    memberSymbol = (memberDecl.kind & TypeScript.PullElementKind.SomeSignature) ? memberDecl.getSignatureSymbol() : memberDecl.getSymbol();

                    this.resolveAST(typeMembers.nonSeparatorAt(i), false, context);

                    memberType = memberSymbol.type;

                    if ((memberType && memberType.isGeneric()) || (memberSymbol.isSignature() && memberSymbol.isGeneric())) {
                        interfaceSymbol.setHasGenericMember();
                    }
                }
            }

            interfaceSymbol.setResolved();

            if (this.canTypeCheckAST(objectType, context)) {
                this.typeCheckObjectTypeTypeReference(objectType, context);
            }

            return interfaceSymbol;
        };

        PullTypeResolver.prototype.typeCheckObjectTypeTypeReference = function (objectType, context) {
            this.setTypeChecked(objectType, context);
            var objectTypeDecl = this.semanticInfoChain.getDeclForAST(objectType);
            var objectTypeSymbol = objectTypeDecl.getSymbol();

            this.typeCheckMembersAgainstIndexer(objectTypeSymbol, objectTypeDecl, context);
            this.checkTypeForDuplicateIndexSignatures(objectTypeSymbol);
        };

        PullTypeResolver.prototype.resolveTypeAnnotation = function (typeAnnotation, context) {
            return this.resolveTypeReference(typeAnnotation.type, context);
        };

        PullTypeResolver.prototype.resolveTypeReference = function (typeRef, context) {
            if (typeRef === null) {
                return null;
            }

            TypeScript.Debug.assert(typeRef.kind() !== 244 /* TypeAnnotation */);

            var aliasType = null;
            var type = this.computeTypeReferenceSymbol(typeRef, context);

            if (type.kind === TypeScript.PullElementKind.Container) {
                var container = type;
                var instanceSymbol = container.getInstanceSymbol();

                // check if it is actually merged with class
                if (instanceSymbol && (instanceSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.ClassConstructorVariable) || instanceSymbol.kind === TypeScript.PullElementKind.ConstructorMethod)) {
                    type = instanceSymbol.type.getAssociatedContainerType();
                }
            }

            // Unwrap an alias type to its true type.
            if (type && type.isAlias()) {
                aliasType = type;
                type = aliasType.getExportAssignedTypeSymbol();
            }

            if (type && !type.isGeneric()) {
                if (aliasType) {
                    this.semanticInfoChain.setAliasSymbolForAST(typeRef, aliasType);
                }
            }

            if (type && !type.isError()) {
                if ((type.kind & TypeScript.PullElementKind.SomeType) === 0) {
                    // Provide some helper messages for common cases.
                    if (type.kind & TypeScript.PullElementKind.SomeContainer) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(typeRef, TypeScript.DiagnosticCode.Type_reference_cannot_refer_to_container_0, [aliasType ? aliasType.toString() : type.toString()]));
                    } else {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(typeRef, TypeScript.DiagnosticCode.Type_reference_must_refer_to_type));
                    }
                }
            }

            if (this.canTypeCheckAST(typeRef, context)) {
                this.setTypeChecked(typeRef, context);
            }

            return type;
        };

        PullTypeResolver.prototype.getArrayType = function (elementType) {
            var arraySymbol = elementType.getArrayType();

            // ...But in case we haven't...
            if (!arraySymbol) {
                arraySymbol = this.createInstantiatedType(this.cachedArrayInterfaceType(), [elementType]);

                if (!arraySymbol) {
                    arraySymbol = this.semanticInfoChain.anyTypeSymbol;
                }

                elementType.setArrayType(arraySymbol);
            }

            return arraySymbol;
        };

        PullTypeResolver.prototype.computeTypeReferenceSymbol = function (term, context) {
            switch (term.kind()) {
                case 60 /* AnyKeyword */:
                    return this.semanticInfoChain.anyTypeSymbol;
                case 61 /* BooleanKeyword */:
                    return this.semanticInfoChain.booleanTypeSymbol;
                case 67 /* NumberKeyword */:
                    return this.semanticInfoChain.numberTypeSymbol;
                case 69 /* StringKeyword */:
                    return this.semanticInfoChain.stringTypeSymbol;
                case 41 /* VoidKeyword */:
                    return this.semanticInfoChain.voidTypeSymbol;
            }

            var typeDeclSymbol = null;

            // a name
            if (term.kind() === 11 /* IdentifierName */) {
                typeDeclSymbol = this.resolveTypeNameExpression(term, context);
            } else if (term.kind() === 123 /* FunctionType */) {
                var functionType = term;
                typeDeclSymbol = this.resolveAnyFunctionTypeSignature(functionType, functionType.typeParameterList, functionType.parameterList, functionType.type, context);
            } else if (term.kind() === 125 /* ConstructorType */) {
                var constructorType = term;
                typeDeclSymbol = this.resolveAnyFunctionTypeSignature(constructorType, constructorType.typeParameterList, constructorType.parameterList, constructorType.type, context);
            } else if (term.kind() === 122 /* ObjectType */) {
                typeDeclSymbol = this.resolveObjectTypeTypeReference(term, context);
            } else if (term.kind() === 126 /* GenericType */) {
                typeDeclSymbol = this.resolveGenericTypeReference(term, context);
            } else if (term.kind() === 121 /* QualifiedName */) {
                // find the decl
                typeDeclSymbol = this.resolveQualifiedName(term, context);
            } else if (term.kind() === 14 /* StringLiteral */) {
                var stringConstantAST = term;
                var enclosingDecl = this.getEnclosingDeclForAST(term);
                typeDeclSymbol = new TypeScript.PullStringConstantTypeSymbol(stringConstantAST.text());
                var decl = new TypeScript.PullSynthesizedDecl(stringConstantAST.text(), stringConstantAST.text(), typeDeclSymbol.kind, null, enclosingDecl, enclosingDecl.semanticInfoChain);
                typeDeclSymbol.addDeclaration(decl);
            } else if (term.kind() === 127 /* TypeQuery */) {
                var typeQuery = term;

                // TODO: This is a workaround if we encounter a TypeReference AST node. Remove it when we remove the AST.
                var typeQueryTerm = typeQuery.name;

                //if (typeQueryTerm.kind() === SyntaxKind.TypeRef) {
                //    typeQueryTerm = (<TypeReference>typeQueryTerm).term;
                //}
                var valueSymbol = this.resolveAST(typeQueryTerm, false, context);

                if (valueSymbol && valueSymbol.isAlias()) {
                    if (valueSymbol.assignedValue()) {
                        valueSymbol = valueSymbol.assignedValue();
                    } else {
                        var containerSymbol = valueSymbol.getExportAssignedContainerSymbol();
                        valueSymbol = (containerSymbol && containerSymbol.isContainer() && !containerSymbol.isEnum()) ? containerSymbol.getInstanceSymbol() : null;
                    }
                }

                // Get the type of the symbol
                if (valueSymbol) {
                    typeDeclSymbol = valueSymbol.type.widenedType(this, typeQueryTerm, context);
                } else {
                    typeDeclSymbol = this.getNewErrorTypeSymbol();
                }
            } else if (term.kind() === 124 /* ArrayType */) {
                var arrayType = term;
                var underlying = this.resolveTypeReference(arrayType.type, context);
                typeDeclSymbol = this.getArrayType(underlying);
            } else {
                throw TypeScript.Errors.invalidOperation("unknown type");
            }

            if (!typeDeclSymbol) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(term, TypeScript.DiagnosticCode.Unable_to_resolve_type));
                return this.getNewErrorTypeSymbol();
            }

            if (typeDeclSymbol.isError()) {
                return typeDeclSymbol;
            }

            if (this.genericTypeIsUsedWithoutRequiredTypeArguments(typeDeclSymbol, term, context)) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(term, TypeScript.DiagnosticCode.Generic_type_references_must_include_all_type_arguments));
                typeDeclSymbol = this.instantiateTypeToAny(typeDeclSymbol, context);
            }

            return typeDeclSymbol;
        };

        PullTypeResolver.prototype.genericTypeIsUsedWithoutRequiredTypeArguments = function (typeSymbol, term, context) {
            if (!typeSymbol) {
                return false;
            }

            if (typeSymbol.isAlias()) {
                return this.genericTypeIsUsedWithoutRequiredTypeArguments(typeSymbol.getExportAssignedTypeSymbol(), term, context);
            }

            return typeSymbol.isNamedTypeSymbol() && typeSymbol.isGeneric() && !typeSymbol.isTypeParameter() && (typeSymbol.isResolved || typeSymbol.inResolution) && !typeSymbol.getIsSpecialized() && typeSymbol.getTypeParameters().length && typeSymbol.getTypeArguments() === null && this.isTypeRefWithoutTypeArgs(term);
        };

        PullTypeResolver.prototype.resolveMemberVariableDeclaration = function (varDecl, context) {
            return this.resolveVariableDeclaratorOrParameterOrEnumElement(varDecl, varDecl.modifiers, varDecl.variableDeclarator.propertyName, TypeScript.ASTHelpers.getType(varDecl.variableDeclarator), varDecl.variableDeclarator.equalsValueClause, context);
        };

        PullTypeResolver.prototype.resolvePropertySignature = function (varDecl, context) {
            return this.resolveVariableDeclaratorOrParameterOrEnumElement(varDecl, TypeScript.sentinelEmptyArray, varDecl.propertyName, TypeScript.ASTHelpers.getType(varDecl), null, context);
        };

        PullTypeResolver.prototype.resolveVariableDeclarator = function (varDecl, context) {
            return this.resolveVariableDeclaratorOrParameterOrEnumElement(varDecl, TypeScript.ASTHelpers.getVariableDeclaratorModifiers(varDecl), varDecl.propertyName, TypeScript.ASTHelpers.getType(varDecl), varDecl.equalsValueClause, context);
        };

        PullTypeResolver.prototype.resolveParameterList = function (list, context) {
            return this.resolveSeparatedList(list.parameters, context);
        };

        PullTypeResolver.prototype.resolveParameter = function (parameter, context) {
            return this.resolveVariableDeclaratorOrParameterOrEnumElement(parameter, parameter.modifiers, parameter.identifier, TypeScript.ASTHelpers.getType(parameter), parameter.equalsValueClause, context);
        };

        PullTypeResolver.prototype.getEnumTypeSymbol = function (enumElement, context) {
            var enumDeclaration = enumElement.parent.parent;
            var decl = this.semanticInfoChain.getDeclForAST(enumDeclaration);
            var symbol = decl.getSymbol();
            this.resolveDeclaredSymbol(symbol, context);

            return symbol;
        };

        PullTypeResolver.prototype.resolveEnumElement = function (enumElement, context) {
            return this.resolveVariableDeclaratorOrParameterOrEnumElement(enumElement, TypeScript.sentinelEmptyArray, enumElement.propertyName, null, enumElement.equalsValueClause, context);
        };

        PullTypeResolver.prototype.typeCheckEnumElement = function (enumElement, context) {
            this.typeCheckVariableDeclaratorOrParameterOrEnumElement(enumElement, TypeScript.sentinelEmptyArray, enumElement.propertyName, null, enumElement.equalsValueClause, context);
        };

        PullTypeResolver.prototype.resolveEqualsValueClause = function (clause, isContextuallyTyped, context) {
            if (this.canTypeCheckAST(clause, context)) {
                this.setTypeChecked(clause, context);
            }

            return this.resolveAST(clause.value, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.resolveVariableDeclaratorOrParameterOrEnumElement = function (varDeclOrParameter, modifiers, name, typeExpr, init, context) {
            var hasTypeExpr = typeExpr !== null || varDeclOrParameter.kind() === 243 /* EnumElement */;
            var enclosingDecl = this.getEnclosingDeclForAST(varDeclOrParameter);
            var decl = this.semanticInfoChain.getDeclForAST(varDeclOrParameter);

            // if the enclosing decl is a lambda, we may not have bound the parent symbol
            if (enclosingDecl && decl.kind === TypeScript.PullElementKind.Parameter) {
                enclosingDecl.ensureSymbolIsBound();
            }

            var declSymbol = decl.getSymbol();
            var declParameterSymbol = decl.getValueDecl() ? decl.getValueDecl().getSymbol() : null;

            if (declSymbol.isResolved) {
                var declType = declSymbol.type;
                var valDecl = decl.getValueDecl();

                if (valDecl) {
                    var valSymbol = valDecl.getSymbol();

                    if (valSymbol && !valSymbol.isResolved) {
                        valSymbol.type = declType;
                        valSymbol.setResolved();
                    }
                }
            } else {
                if (declSymbol.inResolution) {
                    // PULLTODO: Error or warning?
                    declSymbol.type = this.semanticInfoChain.anyTypeSymbol;
                    declSymbol.setResolved();
                    return declSymbol;
                }

                if (!declSymbol.type || !declSymbol.type.isError()) {
                    declSymbol.startResolving();

                    // Does this have a type expression? If so, that's the type
                    var typeExprSymbol = this.resolveAndTypeCheckVariableDeclarationTypeExpr(varDeclOrParameter, name, typeExpr, context);

                    // If we're not type checking, and have a type expression, don't bother looking at the initializer expression
                    if (!hasTypeExpr) {
                        this.resolveAndTypeCheckVariableDeclaratorOrParameterInitExpr(varDeclOrParameter, name, typeExpr, init, context, typeExprSymbol);
                    }

                    // if we're lacking both a type annotation and an initialization expression, the type is 'any'
                    if (!(hasTypeExpr || init)) {
                        var defaultType = this.semanticInfoChain.anyTypeSymbol;

                        if (declSymbol.isVarArg && this.cachedArrayInterfaceType()) {
                            defaultType = this.createInstantiatedType(this.cachedArrayInterfaceType(), [defaultType]);
                        }

                        context.setTypeInContext(declSymbol, defaultType);

                        if (declParameterSymbol) {
                            declParameterSymbol.type = defaultType;
                        }
                    }
                    declSymbol.setResolved();

                    if (declParameterSymbol) {
                        declParameterSymbol.setResolved();
                    }
                }
            }

            if (this.canTypeCheckAST(varDeclOrParameter, context)) {
                this.typeCheckVariableDeclaratorOrParameterOrEnumElement(varDeclOrParameter, modifiers, name, typeExpr, init, context);
            }

            return declSymbol;
        };

        PullTypeResolver.prototype.resolveAndTypeCheckVariableDeclarationTypeExpr = function (varDeclOrParameter, name, typeExpr, context) {
            var enclosingDecl = this.getEnclosingDeclForAST(varDeclOrParameter);
            var decl = this.semanticInfoChain.getDeclForAST(varDeclOrParameter);
            var declSymbol = decl.getSymbol();
            var declParameterSymbol = decl.getValueDecl() ? decl.getValueDecl().getSymbol() : null;

            if (varDeclOrParameter.kind() === 243 /* EnumElement */) {
                var result = this.getEnumTypeSymbol(varDeclOrParameter, context);
                declSymbol.type = result;
                return result;
            }

            if (!typeExpr) {
                return null;
            }

            var wrapperDecl = this.getEnclosingDecl(decl);
            wrapperDecl = wrapperDecl || enclosingDecl;

            var typeExprSymbol = this.resolveTypeReference(typeExpr, context);

            if (!typeExprSymbol) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Unable_to_resolve_type_of_0, [name.text()]));
                declSymbol.type = this.getNewErrorTypeSymbol();

                if (declParameterSymbol) {
                    context.setTypeInContext(declParameterSymbol, this.semanticInfoChain.anyTypeSymbol);
                }
            } else if (typeExprSymbol.isError()) {
                context.setTypeInContext(declSymbol, typeExprSymbol);
                if (declParameterSymbol) {
                    context.setTypeInContext(declParameterSymbol, typeExprSymbol);
                }
            } else {
                if (typeExprSymbol === this.semanticInfoChain.anyTypeSymbol) {
                    decl.setFlag(TypeScript.PullElementFlags.IsAnnotatedWithAny);
                }

                // PULLREVIEW: If the type annotation is a container type, use the module instance type
                if (typeExprSymbol.isContainer()) {
                    var exportedTypeSymbol = typeExprSymbol.getExportAssignedTypeSymbol();

                    if (exportedTypeSymbol) {
                        typeExprSymbol = exportedTypeSymbol;
                    } else {
                        typeExprSymbol = typeExprSymbol.type;

                        if (typeExprSymbol.isAlias()) {
                            typeExprSymbol = typeExprSymbol.getExportAssignedTypeSymbol();
                        }

                        if (typeExprSymbol && typeExprSymbol.isContainer() && !typeExprSymbol.isEnum()) {
                            // aliased type could still be 'any' as the result of an error
                            var instanceSymbol = typeExprSymbol.getInstanceSymbol();

                            if (!instanceSymbol || !TypeScript.PullHelpers.symbolIsEnum(instanceSymbol)) {
                                typeExprSymbol = this.getNewErrorTypeSymbol();
                            } else {
                                typeExprSymbol = instanceSymbol.type;
                            }
                        }
                    }
                } else if (declSymbol.isVarArg && !(typeExprSymbol.isArrayNamedTypeReference() || typeExprSymbol === this.cachedArrayInterfaceType())) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Rest_parameters_must_be_array_types));
                    typeExprSymbol = this.getNewErrorTypeSymbol();
                }

                context.setTypeInContext(declSymbol, typeExprSymbol);

                if (declParameterSymbol) {
                    declParameterSymbol.type = typeExprSymbol;
                }

                // We associate the value with the function type here because we couldn't do so at biding
                // but need this information to get correct doc comments
                if (typeExprSymbol.kind === TypeScript.PullElementKind.FunctionType && !typeExprSymbol.getFunctionSymbol()) {
                    typeExprSymbol.setFunctionSymbol(declSymbol);
                }
            }

            return typeExprSymbol;
        };

        PullTypeResolver.prototype.resolveAndTypeCheckVariableDeclaratorOrParameterInitExpr = function (varDeclOrParameter, name, typeExpr, init, context, typeExprSymbol) {
            if (!init) {
                return null;
            }

            var hasTypeExpr = typeExpr !== null || varDeclOrParameter.kind() === 243 /* EnumElement */;
            if (typeExprSymbol) {
                context.pushNewContextualType(typeExprSymbol);
            }

            var enclosingDecl = this.getEnclosingDeclForAST(varDeclOrParameter);
            var decl = this.semanticInfoChain.getDeclForAST(varDeclOrParameter);
            var declSymbol = decl.getSymbol();
            var declParameterSymbol = decl.getValueDecl() ? decl.getValueDecl().getSymbol() : null;

            var wrapperDecl = this.getEnclosingDecl(decl);
            wrapperDecl = wrapperDecl || enclosingDecl;

            var initExprSymbol = this.resolveAST(init, typeExprSymbol !== null, context);

            if (typeExprSymbol) {
                context.popAnyContextualType();
            }

            if (!initExprSymbol) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Unable_to_resolve_type_of_0, [name.text()]));

                if (!hasTypeExpr) {
                    context.setTypeInContext(declSymbol, this.getNewErrorTypeSymbol());

                    if (declParameterSymbol) {
                        context.setTypeInContext(declParameterSymbol, this.semanticInfoChain.anyTypeSymbol);
                    }
                }
            } else {
                var initTypeSymbol = initExprSymbol.type;

                // Don't reset the type if we already have one from the type expression
                if (!hasTypeExpr) {
                    var widenedInitTypeSymbol = initTypeSymbol.widenedType(this, init.value, context);
                    context.setTypeInContext(declSymbol, widenedInitTypeSymbol);

                    if (declParameterSymbol) {
                        context.setTypeInContext(declParameterSymbol, widenedInitTypeSymbol);
                    }

                    // if the noImplicitAny flag is set to be true, report an error
                    if (this.compilationSettings.noImplicitAny()) {
                        // initializer is resolved to any type from widening variable declaration (i.e var x = null)
                        if ((widenedInitTypeSymbol !== initTypeSymbol) && (widenedInitTypeSymbol === this.semanticInfoChain.anyTypeSymbol)) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Variable_0_implicitly_has_an_any_type, [name.text()]));
                        }
                    }

                    return widenedInitTypeSymbol;
                }
            }

            return initTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckPropertySignature = function (varDecl, context) {
            this.typeCheckVariableDeclaratorOrParameterOrEnumElement(varDecl, TypeScript.sentinelEmptyArray, varDecl.propertyName, TypeScript.ASTHelpers.getType(varDecl), null, context);
        };

        PullTypeResolver.prototype.typeCheckMemberVariableDeclaration = function (varDecl, context) {
            this.typeCheckVariableDeclaratorOrParameterOrEnumElement(varDecl, varDecl.modifiers, varDecl.variableDeclarator.propertyName, TypeScript.ASTHelpers.getType(varDecl), varDecl.variableDeclarator.equalsValueClause, context);
        };

        PullTypeResolver.prototype.typeCheckVariableDeclarator = function (varDecl, context) {
            this.typeCheckVariableDeclaratorOrParameterOrEnumElement(varDecl, TypeScript.ASTHelpers.getVariableDeclaratorModifiers(varDecl), varDecl.propertyName, TypeScript.ASTHelpers.getType(varDecl), varDecl.equalsValueClause, context);
        };

        PullTypeResolver.prototype.typeCheckParameter = function (parameter, context) {
            this.typeCheckVariableDeclaratorOrParameterOrEnumElement(parameter, parameter.modifiers, parameter.identifier, TypeScript.ASTHelpers.getType(parameter), parameter.equalsValueClause, context);
        };

        PullTypeResolver.prototype.typeCheckVariableDeclaratorOrParameterOrEnumElement = function (varDeclOrParameter, modifiers, name, typeExpr, init, context) {
            var _this = this;
            this.setTypeChecked(varDeclOrParameter, context);

            var hasTypeExpr = typeExpr !== null || varDeclOrParameter.kind() === 243 /* EnumElement */;
            var enclosingDecl = this.getEnclosingDeclForAST(varDeclOrParameter);
            var decl = this.semanticInfoChain.getDeclForAST(varDeclOrParameter);
            var declSymbol = decl.getSymbol();

            var typeExprSymbol = this.resolveAndTypeCheckVariableDeclarationTypeExpr(varDeclOrParameter, name, typeExpr, context);

            // Report errors on init Expr only if typeExpr is present because we wouldnt have resolved the initExpr when just resolving
            var initTypeSymbol = this.resolveAndTypeCheckVariableDeclaratorOrParameterInitExpr(varDeclOrParameter, name, typeExpr, init, context, typeExprSymbol);

            // If we're type checking, test the initializer and type annotation for assignment compatibility
            if (hasTypeExpr || init) {
                if (typeExprSymbol && typeExprSymbol.isAlias()) {
                    typeExprSymbol = typeExprSymbol.getExportAssignedTypeSymbol();
                }

                if (typeExprSymbol && typeExprSymbol.kind === TypeScript.PullElementKind.DynamicModule) {
                    var exportedTypeSymbol = typeExprSymbol.getExportAssignedTypeSymbol();

                    if (exportedTypeSymbol) {
                        typeExprSymbol = exportedTypeSymbol;
                    } else {
                        var instanceTypeSymbol = typeExprSymbol.getInstanceType();

                        if (!instanceTypeSymbol) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Tried_to_set_variable_type_to_uninitialized_module_type_0, [typeExprSymbol.toString()]));
                            typeExprSymbol = null;
                        } else {
                            typeExprSymbol = instanceTypeSymbol;
                        }
                    }
                }

                initTypeSymbol = this.getInstanceTypeForAssignment(varDeclOrParameter, initTypeSymbol, context);

                if (initTypeSymbol && typeExprSymbol) {
                    var comparisonInfo = new TypeComparisonInfo();

                    var isAssignable = this.sourceIsAssignableToTarget(initTypeSymbol, typeExprSymbol, varDeclOrParameter, context, comparisonInfo);

                    if (!isAssignable) {
                        var enclosingSymbol = this.getEnclosingSymbolForAST(varDeclOrParameter);
                        if (comparisonInfo.message) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Cannot_convert_0_to_1_NL_2, [initTypeSymbol.toString(enclosingSymbol), typeExprSymbol.toString(enclosingSymbol), comparisonInfo.message]));
                        } else {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Cannot_convert_0_to_1, [initTypeSymbol.toString(enclosingSymbol), typeExprSymbol.toString(enclosingSymbol)]));
                        }
                    }
                }
            } else if (varDeclOrParameter.kind() !== 243 /* EnumElement */ && this.compilationSettings.noImplicitAny() && !this.isForInVariableDeclarator(varDeclOrParameter)) {
                // if we're lacking both a type annotation and an initialization expression, the type is 'any'
                // if the noImplicitAny flag is set to be true, report an error
                // Do not report an error if the variable declaration is declared in ForIn statement
                var wrapperDecl = this.getEnclosingDecl(decl);
                wrapperDecl = wrapperDecl || enclosingDecl;

                // error should be reported if
                // - container decl is not ambient
                // OR
                // - container decl is ambient and self decl is not private (is provided)
                var needReportError = function (containerDecl, selfDecl) {
                    if (!TypeScript.hasFlag(containerDecl.flags, TypeScript.PullElementFlags.Ambient)) {
                        return true;
                    }

                    return selfDecl && !TypeScript.hasFlag(selfDecl.flags, TypeScript.PullElementFlags.Private);
                };

                // check what enclosingDecl the varDecl is in and report an appropriate error message
                // varDecl is a function/constructor/constructor-signature parameter
                if ((wrapperDecl.kind === TypeScript.PullElementKind.Function || wrapperDecl.kind === TypeScript.PullElementKind.ConstructorMethod || wrapperDecl.kind === TypeScript.PullElementKind.ConstructSignature)) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Parameter_0_of_1_implicitly_has_an_any_type, [name.text(), enclosingDecl.name]));
                } else if (wrapperDecl.kind === TypeScript.PullElementKind.Method) {
                    // check if the parent of wrapperDecl is ambient class declaration
                    var parentDecl = wrapperDecl.getParentDecl();

                    // parentDecl is not an ambient declaration; so report an error
                    // OR
                    //parentDecl is an ambient declaration, but the wrapperDecl(method) is a not private; so report an error
                    if (needReportError(parentDecl, wrapperDecl)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Parameter_0_of_1_implicitly_has_an_any_type, [name.text(), enclosingDecl.name]));
                    }
                } else if (decl.kind === TypeScript.PullElementKind.Property && !declSymbol.getContainer().isNamedTypeSymbol()) {
                    if (needReportError(wrapperDecl, decl)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Member_0_of_object_type_implicitly_has_an_any_type, [name.text()]));
                    }
                } else if (wrapperDecl.kind !== TypeScript.PullElementKind.CatchBlock) {
                    // varDecl is not declared in ambient declaration; so report an error
                    // OR
                    // varDecl is declared in ambient declaration but it is not private; so report an error
                    if (needReportError(wrapperDecl) || !TypeScript.hasModifier(modifiers, TypeScript.PullElementFlags.Private)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Variable_0_implicitly_has_an_any_type, [name.text()]));
                    }
                }
            }

            if (init && varDeclOrParameter.kind() === 242 /* Parameter */) {
                var containerSignature = enclosingDecl.getSignatureSymbol();
                if (containerSignature && !containerSignature.isDefinition()) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(varDeclOrParameter, TypeScript.DiagnosticCode.Default_arguments_are_only_allowed_in_implementation));
                }
            }
            if (declSymbol.kind !== TypeScript.PullElementKind.Parameter && (declSymbol.kind !== TypeScript.PullElementKind.Property || declSymbol.getContainer().isNamedTypeSymbol())) {
                this.checkSymbolPrivacy(declSymbol, declSymbol.type, function (symbol) {
                    return _this.variablePrivacyErrorReporter(varDeclOrParameter, declSymbol, symbol, context);
                });
            }

            if ((declSymbol.kind !== TypeScript.PullElementKind.Property && declSymbol.kind !== TypeScript.PullElementKind.EnumMember) || declSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.PropertyParameter)) {
                // Non property variable with _this name, we need to verify if this would be ok
                this.checkNameForCompilerGeneratedDeclarationCollision(varDeclOrParameter, true, name, context);
            }
        };

        PullTypeResolver.prototype.isForInVariableDeclarator = function (ast) {
            return ast.kind() === 225 /* VariableDeclarator */ && ast.parent && ast.parent.parent && ast.parent.parent.parent && ast.parent.kind() === 2 /* SeparatedList */ && ast.parent.parent.kind() === 224 /* VariableDeclaration */ && ast.parent.parent.parent.kind() === 155 /* ForInStatement */ && ast.parent.parent.parent.variableDeclaration === ast.parent.parent;
        };

        PullTypeResolver.prototype.checkSuperCaptureVariableCollides = function (superAST, isDeclaration, context) {
            var enclosingDecl = this.getEnclosingDeclForAST(superAST);

            var classSymbol = this.getContextualClassSymbolForEnclosingDecl(superAST, enclosingDecl);

            if (classSymbol && !classSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.Ambient)) {
                if (superAST.kind() === 242 /* Parameter */) {
                    var enclosingAST = this.getASTForDecl(enclosingDecl);
                    if (enclosingAST.kind() !== 218 /* ParenthesizedArrowFunctionExpression */ && enclosingAST.kind() !== 219 /* SimpleArrowFunctionExpression */) {
                        var block = enclosingDecl.kind === TypeScript.PullElementKind.Method ? enclosingAST.block : enclosingAST.block;
                        if (!block) {
                            return;
                        }
                    }
                }

                this.resolveDeclaredSymbol(classSymbol, context);

                var parents = classSymbol.getExtendedTypes();
                if (parents.length) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(superAST, isDeclaration ? TypeScript.DiagnosticCode.Duplicate_identifier_super_Compiler_uses_super_to_capture_base_class_reference : TypeScript.DiagnosticCode.Expression_resolves_to_super_that_compiler_uses_to_capture_base_class_reference));
                }
            }
        };

        PullTypeResolver.prototype.checkThisCaptureVariableCollides = function (_thisAST, isDeclaration, context) {
            if (isDeclaration) {
                var decl = this.semanticInfoChain.getDeclForAST(_thisAST);
                if (TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.Ambient)) {
                    return;
                }
            }

            // Verify if this variable name conflicts with the _this that would be emitted to capture this in any of the enclosing context
            var enclosingDecl = this.getEnclosingDeclForAST(_thisAST);

            var enclosingModule = TypeScript.ASTHelpers.getEnclosingModuleDeclaration(_thisAST);
            if (TypeScript.ASTHelpers.isAnyNameOfModule(enclosingModule, _thisAST)) {
                // If we're actually the name of a module, then we want the enclosing decl for the
                // module that we're in.
                enclosingDecl = this.getEnclosingDeclForAST(enclosingModule);
            }

            var declPath = enclosingDecl.getParentPath();

            for (var i = declPath.length - 1; i >= 0; i--) {
                var decl = declPath[i];
                var declKind = decl.kind;
                if (declKind === TypeScript.PullElementKind.FunctionExpression && TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.ArrowFunction)) {
                    continue;
                }

                if (declKind === TypeScript.PullElementKind.Function || declKind === TypeScript.PullElementKind.Method || declKind === TypeScript.PullElementKind.ConstructorMethod || declKind === TypeScript.PullElementKind.GetAccessor || declKind === TypeScript.PullElementKind.SetAccessor || declKind === TypeScript.PullElementKind.FunctionExpression || declKind === TypeScript.PullElementKind.Class || declKind === TypeScript.PullElementKind.Container || declKind === TypeScript.PullElementKind.DynamicModule || declKind === TypeScript.PullElementKind.Script) {
                    if (TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.MustCaptureThis)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(_thisAST, isDeclaration ? TypeScript.DiagnosticCode.Duplicate_identifier_this_Compiler_uses_variable_declaration_this_to_capture_this_reference : TypeScript.DiagnosticCode.Expression_resolves_to_variable_declaration_this_that_compiler_uses_to_capture_this_reference));
                    }
                    break;
                }
            }
        };

        PullTypeResolver.prototype.postTypeCheckVariableDeclaratorOrParameter = function (varDeclOrParameter, context) {
            this.checkThisCaptureVariableCollides(varDeclOrParameter, true, context);
        };

        PullTypeResolver.prototype.resolveTypeParameterDeclaration = function (typeParameterAST, context) {
            var typeParameterDecl = this.semanticInfoChain.getDeclForAST(typeParameterAST);
            var typeParameterSymbol = typeParameterDecl.getSymbol();

            // Always resolve the first type parameter declaration to make sure we have the constraint set from the first decl
            this.resolveFirstTypeParameterDeclaration(typeParameterSymbol, context);

            if (typeParameterSymbol.isResolved && this.canTypeCheckAST(typeParameterAST, context)) {
                this.typeCheckTypeParameterDeclaration(typeParameterAST, context);
            }

            return typeParameterSymbol;
        };

        PullTypeResolver.prototype.resolveFirstTypeParameterDeclaration = function (typeParameterSymbol, context) {
            var typeParameterDecl = typeParameterSymbol.getDeclarations()[0];
            var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameterDecl);

            // REVIEW: We shouldn't bail if we're specializing
            if (typeParameterSymbol.isResolved || typeParameterSymbol.inResolution) {
                return;
            }

            typeParameterSymbol.startResolving();

            if (typeParameterAST.constraint) {
                var constraintTypeSymbol = this.resolveTypeReference(typeParameterAST.constraint.type, context);

                if (constraintTypeSymbol) {
                    typeParameterSymbol.setConstraint(constraintTypeSymbol);
                }
            }

            typeParameterSymbol.setResolved();
        };

        PullTypeResolver.prototype.typeCheckTypeParameterDeclaration = function (typeParameterAST, context) {
            this.setTypeChecked(typeParameterAST, context);

            var constraint = this.resolveAST(typeParameterAST.constraint, false, context);

            if (constraint) {
                // Get all type parameters and create a map that has entries at pullSymbolID for each of the type parameter
                // The value doesnt matter, the entry needs to be present for the wrapsSomeTypeParameter to be able to give the correct answer
                var typeParametersAST = typeParameterAST.parent;
                var typeParameters = [];
                for (var i = 0; i < typeParametersAST.nonSeparatorCount(); i++) {
                    var currentTypeParameterAST = typeParametersAST.nonSeparatorAt(i);
                    var currentTypeParameterDecl = this.semanticInfoChain.getDeclForAST(currentTypeParameterAST);
                    var currentTypeParameter = this.semanticInfoChain.getSymbolForDecl(currentTypeParameterDecl);
                    typeParameters[currentTypeParameter.pullSymbolID] = currentTypeParameter;
                }

                // If constraint wraps any of the type parameter from the type parameters list report error
                if (constraint.wrapsSomeTypeParameter(typeParameters)) {
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Constraint_of_a_type_parameter_cannot_reference_any_type_parameter_from_the_same_type_parameter_list);
                }
            }
        };

        PullTypeResolver.prototype.resolveConstraint = function (constraint, context) {
            if (this.canTypeCheckAST(constraint, context)) {
                this.setTypeChecked(constraint, context);
            }

            return this.resolveTypeReference(constraint.type, context);
        };

        PullTypeResolver.prototype.resolveFunctionBodyReturnTypes = function (funcDeclAST, block, bodyExpression, signature, useContextualType, enclosingDecl, context) {
            var _this = this;
            var returnStatementsExpressions = [];

            var enclosingDeclStack = [enclosingDecl];

            var preFindReturnExpressionTypes = function (ast, walker) {
                var go = true;

                switch (ast.kind()) {
                    case 129 /* FunctionDeclaration */:
                    case 219 /* SimpleArrowFunctionExpression */:
                    case 218 /* ParenthesizedArrowFunctionExpression */:
                    case 222 /* FunctionExpression */:
                    case 215 /* ObjectLiteralExpression */:
                        // don't recurse into a function\object literal decl - we don't want to confuse a nested
                        // return type with the top-level function's return type
                        go = false;
                        break;

                    case 150 /* ReturnStatement */:
                        var returnStatement = ast;
                        enclosingDecl.setFlag(TypeScript.PullElementFlags.HasReturnStatement);
                        returnStatementsExpressions.push({ expression: returnStatement.expression, enclosingDecl: enclosingDeclStack[enclosingDeclStack.length - 1] });
                        go = false;
                        break;

                    case 236 /* CatchClause */:
                    case 163 /* WithStatement */:
                        enclosingDeclStack[enclosingDeclStack.length] = _this.semanticInfoChain.getDeclForAST(ast);
                        break;

                    default:
                        break;
                }

                walker.options.goChildren = go;

                return ast;
            };

            var postFindReturnExpressionEnclosingDecls = function (ast, walker) {
                switch (ast.kind()) {
                    case 236 /* CatchClause */:
                    case 163 /* WithStatement */:
                        enclosingDeclStack.length--;
                        break;
                    default:
                        break;
                }

                walker.options.goChildren = true;

                return ast;
            };

            if (block) {
                TypeScript.getAstWalkerFactory().walk(block, preFindReturnExpressionTypes, postFindReturnExpressionEnclosingDecls);
            } else {
                returnStatementsExpressions.push({ expression: bodyExpression, enclosingDecl: enclosingDecl });
                enclosingDecl.setFlag(TypeScript.PullElementFlags.HasReturnStatement);
            }

            if (!returnStatementsExpressions.length) {
                signature.returnType = this.semanticInfoChain.voidTypeSymbol;
            } else {
                var returnExpressionSymbols = [];
                var returnExpressions = [];

                for (var i = 0; i < returnStatementsExpressions.length; i++) {
                    var returnExpression = returnStatementsExpressions[i].expression;
                    if (returnExpression) {
                        var returnType = this.resolveAST(returnExpression, useContextualType, context).type;

                        if (returnType.isError()) {
                            signature.returnType = returnType;
                            return;
                        } else {
                            if (returnExpression.parent.kind() === 150 /* ReturnStatement */) {
                                this.setSymbolForAST(returnExpression.parent, returnType, context);
                            }
                        }

                        returnExpressionSymbols.push(returnType);
                        returnExpressions.push(returnExpression);
                    }
                }

                if (!returnExpressionSymbols.length) {
                    signature.returnType = this.semanticInfoChain.voidTypeSymbol;
                } else {
                    // combine return expression types for best common type
                    var collection = {
                        getLength: function () {
                            return returnExpressionSymbols.length;
                        },
                        getTypeAtIndex: function (index) {
                            return returnExpressionSymbols[index].type;
                        }
                    };

                    var bestCommonReturnType = this.findBestCommonType(collection, context, new TypeComparisonInfo());
                    var returnType = bestCommonReturnType;
                    var returnExpression = returnExpressions[returnExpressionSymbols.indexOf(returnType)];

                    var functionDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
                    var functionSymbol = functionDecl.getSymbol();

                    if (returnType) {
                        var previousReturnType = returnType;
                        var newReturnType = returnType.widenedType(this, returnExpression, context);
                        signature.returnType = newReturnType;

                        if (!TypeScript.ArrayUtilities.contains(returnExpressionSymbols, bestCommonReturnType)) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Could_not_find_the_best_common_type_of_types_of_all_return_statement_expressions));
                        }

                        // if noImplicitAny flag is set to be true and return statements are not cast expressions, report an error
                        if (this.compilationSettings.noImplicitAny()) {
                            // if the returnType got widen to Any
                            if (previousReturnType !== newReturnType && newReturnType === this.semanticInfoChain.anyTypeSymbol) {
                                var functionName = enclosingDecl.name;
                                if (functionName === "") {
                                    functionName = enclosingDecl.getFunctionExpressionName();
                                }

                                if (functionName != "") {
                                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode._0_which_lacks_return_type_annotation_implicitly_has_an_any_return_type, [functionName]));
                                } else {
                                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Function_expression_which_lacks_return_type_annotation_implicitly_has_an_any_return_type));
                                }
                            }
                        }
                    }

                    // If the accessor is referenced via a recursive chain, we may not have set the accessor's type just yet and we'll
                    // need to do so before setting the 'isGeneric' flag
                    if (!functionSymbol.type && functionSymbol.isAccessor()) {
                        functionSymbol.type = signature.returnType;
                    }
                }
            }
        };

        PullTypeResolver.prototype.typeCheckConstructorDeclaration = function (funcDeclAST, context) {
            var _this = this;
            this.setTypeChecked(funcDeclAST, context);

            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

            for (var i = 0; i < funcDeclAST.callSignature.parameterList.parameters.nonSeparatorCount(); i++) {
                this.resolveAST(funcDeclAST.callSignature.parameterList.parameters.nonSeparatorAt(i), false, context);
            }

            this.resolveAST(funcDeclAST.block, false, context);

            if (funcDecl.getSignatureSymbol() && funcDecl.getSignatureSymbol().isDefinition() && this.enclosingClassIsDerived(funcDecl.getParentDecl())) {
                // Constructors for derived classes must contain a call to the class's 'super' constructor
                if (!this.constructorHasSuperCall(funcDeclAST)) {
                    context.postDiagnostic(new TypeScript.Diagnostic(funcDeclAST.fileName(), this.semanticInfoChain.lineMap(funcDeclAST.fileName()), funcDeclAST.start(), "constructor".length, TypeScript.DiagnosticCode.Constructors_for_derived_classes_must_contain_a_super_call));
                } else if (this.superCallMustBeFirstStatementInConstructor(funcDecl)) {
                    var firstStatement = this.getFirstStatementOfBlockOrNull(funcDeclAST.block);
                    if (!firstStatement || !this.isSuperInvocationExpressionStatement(firstStatement)) {
                        context.postDiagnostic(new TypeScript.Diagnostic(funcDeclAST.fileName(), this.semanticInfoChain.lineMap(funcDeclAST.fileName()), funcDeclAST.start(), "constructor".length, TypeScript.DiagnosticCode.A_super_call_must_be_the_first_statement_in_the_constructor_when_a_class_contains_initialized_properties_or_has_parameter_properties));
                    }
                }
            }

            this.validateVariableDeclarationGroups(funcDecl, context);

            this.checkFunctionTypePrivacy(funcDeclAST, false, null, TypeScript.ASTHelpers.parametersFromParameterList(funcDeclAST.callSignature.parameterList), null, funcDeclAST.block, context);

            this.typeCheckCallBacks.push(function (context) {
                // Function or constructor
                _this.typeCheckFunctionOverloads(funcDeclAST, context);
            });
        };

        PullTypeResolver.prototype.constructorHasSuperCall = function (constructorDecl) {
            var _this = this;
            // October 1, 2013
            // Constructors of classes with no extends clause may not contain super calls, whereas
            // constructors of derived classes must contain at least one super call somewhere in
            // their function body. Super calls are not permitted outside constructors or in local
            // functions inside constructors.
            if (constructorDecl.block) {
                var foundSuperCall = false;
                var pre = function (ast, walker) {
                    switch (ast.kind()) {
                        case 129 /* FunctionDeclaration */:
                        case 219 /* SimpleArrowFunctionExpression */:
                        case 218 /* ParenthesizedArrowFunctionExpression */:
                        case 222 /* FunctionExpression */:
                        case 215 /* ObjectLiteralExpression */:
                            walker.options.goChildren = false;
                        default:
                            // If we hit a super invocation, then we're done.  Stop everything we're doing.
                            // Note 1: there is no restriction on there being multiple super calls.
                            // Note 2: The restriction about super calls not being permitted in a local
                            // function is checked in typeCheckSuperExpression
                            if (_this.isSuperInvocationExpression(ast)) {
                                foundSuperCall = true;
                                walker.options.stopWalking = true;
                            }
                    }
                };

                TypeScript.getAstWalkerFactory().walk(constructorDecl.block, pre);
                return foundSuperCall;
            }

            return false;
        };

        PullTypeResolver.prototype.typeCheckFunctionExpression = function (funcDecl, isContextuallyTyped, context) {
            this.typeCheckAnyFunctionExpression(funcDecl, funcDecl.callSignature.typeParameterList, TypeScript.ASTHelpers.parametersFromParameterList(funcDecl.callSignature.parameterList), funcDecl.callSignature.typeAnnotation, funcDecl.block, null, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.typeCheckCallSignature = function (funcDecl, context) {
            this.typeCheckAnyFunctionDeclaration(funcDecl, false, null, funcDecl.typeParameterList, funcDecl.parameterList, TypeScript.ASTHelpers.getType(funcDecl), null, context);
        };

        PullTypeResolver.prototype.typeCheckConstructSignature = function (funcDecl, context) {
            this.typeCheckAnyFunctionDeclaration(funcDecl, false, null, funcDecl.callSignature.typeParameterList, funcDecl.callSignature.parameterList, TypeScript.ASTHelpers.getType(funcDecl), null, context);
        };

        PullTypeResolver.prototype.typeCheckMethodSignature = function (funcDecl, context) {
            this.typeCheckAnyFunctionDeclaration(funcDecl, false, funcDecl.propertyName, funcDecl.callSignature.typeParameterList, funcDecl.callSignature.parameterList, TypeScript.ASTHelpers.getType(funcDecl), null, context);
        };

        PullTypeResolver.prototype.typeCheckMemberFunctionDeclaration = function (funcDecl, context) {
            this.typeCheckAnyFunctionDeclaration(funcDecl, TypeScript.hasModifier(funcDecl.modifiers, TypeScript.PullElementFlags.Static), funcDecl.propertyName, funcDecl.callSignature.typeParameterList, funcDecl.callSignature.parameterList, TypeScript.ASTHelpers.getType(funcDecl), funcDecl.block, context);
        };

        PullTypeResolver.prototype.containsSingleThrowStatement = function (block) {
            return block !== null && block.statements.childCount() === 1 && block.statements.childAt(0).kind() === 157 /* ThrowStatement */;
        };

        PullTypeResolver.prototype.typeCheckAnyFunctionDeclaration = function (funcDeclAST, isStatic, name, typeParameters, parameters, returnTypeAnnotation, block, context) {
            var _this = this;
            this.setTypeChecked(funcDeclAST, context);

            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

            if (typeParameters) {
                for (var i = 0; i < typeParameters.typeParameters.nonSeparatorCount(); i++) {
                    this.resolveTypeParameterDeclaration(typeParameters.typeParameters.nonSeparatorAt(i), context);
                }
            }

            // resolve parameter type annotations as necessary
            this.resolveAST(parameters, false, context);

            this.resolveAST(block, false, context);
            var enclosingDecl = this.getEnclosingDecl(funcDecl);

            this.resolveReturnTypeAnnotationOfFunctionDeclaration(funcDeclAST, returnTypeAnnotation, context);
            this.validateVariableDeclarationGroups(funcDecl, context);

            this.checkFunctionTypePrivacy(funcDeclAST, isStatic, typeParameters, TypeScript.ASTHelpers.parametersFromParameterList(parameters), returnTypeAnnotation, block, context);

            this.checkThatNonVoidFunctionHasReturnExpressionOrThrowStatement(funcDecl, returnTypeAnnotation, funcDecl.getSignatureSymbol().returnType, block, context);

            if (funcDecl.kind === TypeScript.PullElementKind.Function) {
                this.checkNameForCompilerGeneratedDeclarationCollision(funcDeclAST, true, name, context);
            }

            this.typeCheckCallBacks.push(function (context) {
                // Function or constructor
                _this.typeCheckFunctionOverloads(funcDeclAST, context);
            });
        };

        PullTypeResolver.prototype.checkThatNonVoidFunctionHasReturnExpressionOrThrowStatement = function (functionDecl, returnTypeAnnotation, returnTypeSymbol, block, context) {
            var hasReturn = TypeScript.hasFlag(functionDecl.flags, TypeScript.PullElementFlags.HasReturnStatement);

            // November 18, 2013
            // An explicitly typed function returning a non-void type must have at least one return
            // statement somewhere in its body. An exception to this rule is if the function
            // implementation consists of a single 'throw' statement.
            if (block !== null && returnTypeAnnotation !== null && !hasReturn) {
                var isVoidOrAny = this.isAnyOrEquivalent(returnTypeSymbol) || returnTypeSymbol === this.semanticInfoChain.voidTypeSymbol;

                if (!isVoidOrAny && !this.containsSingleThrowStatement(block)) {
                    var funcName = functionDecl.getDisplayName() || TypeScript.getLocalizedText(TypeScript.DiagnosticCode.expression, null);

                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(returnTypeAnnotation, TypeScript.DiagnosticCode.Function_declared_a_non_void_return_type_but_has_no_return_expression));
                }
            }
        };

        PullTypeResolver.prototype.typeCheckIndexSignature = function (funcDeclAST, context) {
            var _this = this;
            this.setTypeChecked(funcDeclAST, context);

            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

            // resolve parameter type annotations as necessary
            this.resolveAST(funcDeclAST.parameter, false, context);

            var enclosingDecl = this.getEnclosingDecl(funcDecl);

            this.resolveReturnTypeAnnotationOfFunctionDeclaration(funcDeclAST, TypeScript.ASTHelpers.getType(funcDeclAST), context);
            this.validateVariableDeclarationGroups(funcDecl, context);

            this.checkFunctionTypePrivacy(funcDeclAST, false, null, TypeScript.ASTHelpers.parametersFromParameter(funcDeclAST.parameter), TypeScript.ASTHelpers.getType(funcDeclAST), null, context);

            var signature = funcDecl.getSignatureSymbol();

            this.typeCheckCallBacks.push(function (context) {
                var parentSymbol = funcDecl.getSignatureSymbol().getContainer();
                var allIndexSignatures = _this.getBothKindsOfIndexSignaturesExcludingAugmentedType(parentSymbol, context);
                var stringIndexSignature = allIndexSignatures.stringSignature;
                var numberIndexSignature = allIndexSignatures.numericSignature;
                var isNumericIndexer = numberIndexSignature === signature;

                // Section 3.7.4 (November 18, 2013):
                // Specifically, in a type with a string index signature of type T, all properties and numeric index signatures must
                // have types that are subtypes of T.
                // Check that the number signature is assignable to the string index signature. To ensure that we only check this once,
                // we make sure that if the two signatures share a container, we only check this when type checking the number signature.
                if (numberIndexSignature && stringIndexSignature && (isNumericIndexer || stringIndexSignature.getDeclarations()[0].getParentDecl() !== numberIndexSignature.getDeclarations()[0].getParentDecl())) {
                    var comparisonInfo = new TypeComparisonInfo();

                    if (!_this.sourceIsAssignableToTarget(numberIndexSignature.returnType, stringIndexSignature.returnType, funcDeclAST, context, comparisonInfo)) {
                        var enclosingSymbol = _this.getEnclosingSymbolForAST(funcDeclAST);
                        if (comparisonInfo.message) {
                            context.postDiagnostic(_this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Numeric_indexer_type_0_must_be_assignable_to_string_indexer_type_1_NL_2, [numberIndexSignature.returnType.toString(enclosingSymbol), stringIndexSignature.returnType.toString(enclosingSymbol), comparisonInfo.message]));
                        } else {
                            context.postDiagnostic(_this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Numeric_indexer_type_0_must_be_assignable_to_string_indexer_type_1, [numberIndexSignature.returnType.toString(enclosingSymbol), stringIndexSignature.returnType.toString(enclosingSymbol)]));
                        }
                    }
                }

                // Check that property names comply with indexer constraints (either string or numeric)
                var allMembers = parentSymbol.type.getAllMembers(TypeScript.PullElementKind.All, 0 /* all */);
                for (var i = 0; i < allMembers.length; i++) {
                    var member = allMembers[i];
                    var name = member.name;
                    if (name || (member.kind === TypeScript.PullElementKind.Property && name === "")) {
                        if (!allMembers[i].isResolved) {
                            _this.resolveDeclaredSymbol(allMembers[i], context);
                        }

                        // Skip members in the same container, they will be checked during their member type check
                        if (parentSymbol !== allMembers[i].getContainer()) {
                            // Check if the member name kind (number or string), matches the index signature kind. If it does give an error.
                            // If it doesn't we only want to give an error if this is a string signature, and we don't have a numeric signature
                            var isMemberNumeric = TypeScript.PullHelpers.isNameNumeric(name);
                            var indexerKindMatchesMemberNameKind = isNumericIndexer === isMemberNumeric;
                            var onlyStringIndexerIsPresent = !numberIndexSignature;

                            if (indexerKindMatchesMemberNameKind || onlyStringIndexerIsPresent) {
                                var comparisonInfo = new TypeComparisonInfo();
                                if (!_this.sourceIsAssignableToTarget(allMembers[i].type, signature.returnType, funcDeclAST, context, comparisonInfo, false)) {
                                    _this.reportErrorThatMemberIsNotSubtypeOfIndexer(allMembers[i], signature, funcDeclAST, context, comparisonInfo);
                                }
                            }
                        }
                    }
                }
            });
        };

        PullTypeResolver.prototype.postTypeCheckFunctionDeclaration = function (funcDeclAST, context) {
            this.checkThisCaptureVariableCollides(funcDeclAST, true, context);
        };

        PullTypeResolver.prototype.resolveReturnTypeAnnotationOfFunctionDeclaration = function (funcDeclAST, returnTypeAnnotation, context) {
            var returnTypeSymbol = null;

            // resolve the return type annotation
            if (returnTypeAnnotation) {
                var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

                // use the funcDecl for the enclosing decl, since we want to pick up any type parameters
                // on the function when resolving the return type
                returnTypeSymbol = this.resolveTypeReference(returnTypeAnnotation, context);

                if (!returnTypeSymbol) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(returnTypeAnnotation, TypeScript.DiagnosticCode.Cannot_resolve_return_type_reference));
                } else {
                    var isConstructor = funcDeclAST.kind() === 137 /* ConstructorDeclaration */ || funcDeclAST.kind() === 143 /* ConstructSignature */;
                    if (isConstructor && returnTypeSymbol === this.semanticInfoChain.voidTypeSymbol) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Constructors_cannot_have_a_return_type_of_void));
                    }
                }
            }

            return returnTypeSymbol;
        };

        PullTypeResolver.prototype.resolveMemberFunctionDeclaration = function (funcDecl, context) {
            return this.resolveFunctionDeclaration(funcDecl, TypeScript.hasModifier(funcDecl.modifiers, TypeScript.PullElementFlags.Static), funcDecl.propertyName, funcDecl.callSignature.typeParameterList, funcDecl.callSignature.parameterList, TypeScript.ASTHelpers.getType(funcDecl), funcDecl.block, context);
        };

        PullTypeResolver.prototype.resolveCallSignature = function (funcDecl, context) {
            return this.resolveFunctionDeclaration(funcDecl, false, null, funcDecl.typeParameterList, funcDecl.parameterList, TypeScript.ASTHelpers.getType(funcDecl), null, context);
        };

        PullTypeResolver.prototype.resolveConstructSignature = function (funcDecl, context) {
            return this.resolveFunctionDeclaration(funcDecl, false, null, funcDecl.callSignature.typeParameterList, funcDecl.callSignature.parameterList, TypeScript.ASTHelpers.getType(funcDecl), null, context);
        };

        PullTypeResolver.prototype.resolveMethodSignature = function (funcDecl, context) {
            return this.resolveFunctionDeclaration(funcDecl, false, funcDecl.propertyName, funcDecl.callSignature.typeParameterList, funcDecl.callSignature.parameterList, TypeScript.ASTHelpers.getType(funcDecl), null, context);
        };

        PullTypeResolver.prototype.resolveAnyFunctionDeclaration = function (funcDecl, context) {
            return this.resolveFunctionDeclaration(funcDecl, TypeScript.hasModifier(funcDecl.modifiers, TypeScript.PullElementFlags.Static), funcDecl.identifier, funcDecl.callSignature.typeParameterList, funcDecl.callSignature.parameterList, TypeScript.ASTHelpers.getType(funcDecl), funcDecl.block, context);
        };

        PullTypeResolver.prototype.resolveFunctionExpression = function (funcDecl, isContextuallyTyped, context) {
            return this.resolveAnyFunctionExpression(funcDecl, funcDecl.callSignature.typeParameterList, TypeScript.ASTHelpers.parametersFromParameterList(funcDecl.callSignature.parameterList), TypeScript.ASTHelpers.getType(funcDecl), funcDecl.block, null, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.resolveSimpleArrowFunctionExpression = function (funcDecl, isContextuallyTyped, context) {
            return this.resolveAnyFunctionExpression(funcDecl, null, TypeScript.ASTHelpers.parametersFromIdentifier(funcDecl.identifier), null, funcDecl.block, funcDecl.expression, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.resolveParenthesizedArrowFunctionExpression = function (funcDecl, isContextuallyTyped, context) {
            return this.resolveAnyFunctionExpression(funcDecl, funcDecl.callSignature.typeParameterList, TypeScript.ASTHelpers.parametersFromParameterList(funcDecl.callSignature.parameterList), TypeScript.ASTHelpers.getType(funcDecl), funcDecl.block, funcDecl.expression, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.getEnclosingClassDeclaration = function (ast) {
            while (ast) {
                if (ast.kind() === 131 /* ClassDeclaration */) {
                    return ast;
                }

                ast = ast.parent;
            }

            return null;
        };

        PullTypeResolver.prototype.resolveConstructorDeclaration = function (funcDeclAST, context) {
            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

            var funcSymbol = funcDecl.getSymbol();

            var signature = funcDecl.getSignatureSymbol();

            var hadError = false;

            if (signature) {
                if (signature.isResolved) {
                    if (this.canTypeCheckAST(funcDeclAST, context)) {
                        this.typeCheckConstructorDeclaration(funcDeclAST, context);
                    }
                    return funcSymbol;
                }

                if (!signature.inResolution) {
                    var classAST = this.getEnclosingClassDeclaration(funcDeclAST);

                    if (classAST) {
                        var classDecl = this.semanticInfoChain.getDeclForAST(classAST);
                        var classSymbol = classDecl.getSymbol();

                        if (!classSymbol.isResolved && !classSymbol.inResolution) {
                            this.resolveDeclaredSymbol(classSymbol, context);
                        }
                    }
                }

                // Save this in case we had set the function type to any because of a recursive reference.
                var functionTypeSymbol = funcSymbol && funcSymbol.type;

                if (signature.inResolution) {
                    signature.returnType = this.semanticInfoChain.anyTypeSymbol;

                    if (funcSymbol) {
                        funcSymbol.setUnresolved();
                        if (funcSymbol.type === this.semanticInfoChain.anyTypeSymbol) {
                            funcSymbol.type = functionTypeSymbol;
                        }
                    }
                    signature.setResolved();
                    return funcSymbol;
                }

                if (funcSymbol) {
                    funcSymbol.startResolving();
                }
                signature.startResolving();

                // resolve parameter type annotations as necessary
                var prevInTypeCheck = context.inTypeCheck;

                // TODO: why are we getting ourselves out of typecheck here?
                context.inTypeCheck = false;

                for (var i = 0; i < funcDeclAST.callSignature.parameterList.parameters.nonSeparatorCount(); i++) {
                    // TODO: why are we calling resolveParameter instead of resolveAST.
                    this.resolveParameter(funcDeclAST.callSignature.parameterList.parameters.nonSeparatorAt(i), context);
                }

                context.inTypeCheck = prevInTypeCheck;

                if (signature.isGeneric()) {
                    // PULLREVIEW: This is split into a spearate if statement to make debugging slightly easier...
                    if (funcSymbol) {
                        funcSymbol.type.setHasGenericSignature();
                    }
                }

                if (!hadError) {
                    if (funcSymbol) {
                        funcSymbol.setUnresolved();
                        if (funcSymbol.type === this.semanticInfoChain.anyTypeSymbol) {
                            funcSymbol.type = functionTypeSymbol;
                        }
                    }
                    signature.setResolved();
                }
            }

            if (funcSymbol) {
                this.resolveOtherDeclarations(funcDeclAST, context);
            }

            if (this.canTypeCheckAST(funcDeclAST, context)) {
                this.typeCheckConstructorDeclaration(funcDeclAST, context);
            }

            return funcSymbol;
        };

        PullTypeResolver.prototype.resolveIndexMemberDeclaration = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);
            }

            return this.resolveIndexSignature(ast.indexSignature, context);
        };

        PullTypeResolver.prototype.resolveIndexSignature = function (funcDeclAST, context) {
            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

            var funcSymbol = funcDecl.getSymbol();

            var signature = funcDecl.getSignatureSymbol();

            var hadError = false;

            if (signature) {
                if (signature.isResolved) {
                    if (this.canTypeCheckAST(funcDeclAST, context)) {
                        this.typeCheckIndexSignature(funcDeclAST, context);
                    }
                    return funcSymbol;
                }

                // Save this in case we had set the function type to any because of a recursive reference.
                var functionTypeSymbol = funcSymbol && funcSymbol.type;

                if (signature.inResolution) {
                    // try to set the return type, even though we may be lacking in some information
                    if (funcDeclAST.typeAnnotation) {
                        var returnTypeSymbol = this.resolveTypeReference(TypeScript.ASTHelpers.getType(funcDeclAST), context);
                        if (!returnTypeSymbol) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(TypeScript.ASTHelpers.getType(funcDeclAST), TypeScript.DiagnosticCode.Cannot_resolve_return_type_reference));
                            signature.returnType = this.getNewErrorTypeSymbol();
                            hadError = true;
                        } else {
                            signature.returnType = returnTypeSymbol;
                        }
                    } else {
                        signature.returnType = this.semanticInfoChain.anyTypeSymbol;
                    }

                    if (funcSymbol) {
                        funcSymbol.setUnresolved();
                        if (funcSymbol.type === this.semanticInfoChain.anyTypeSymbol) {
                            funcSymbol.type = functionTypeSymbol;
                        }
                    }
                    signature.setResolved();
                    return funcSymbol;
                }

                if (funcSymbol) {
                    funcSymbol.startResolving();
                }
                signature.startResolving();

                // resolve parameter type annotations as necessary
                if (funcDeclAST.parameter) {
                    var prevInTypeCheck = context.inTypeCheck;

                    // TODO: why are we setting inTypeCheck false here?
                    context.inTypeCheck = false;
                    this.resolveParameter(funcDeclAST.parameter, context);
                    context.inTypeCheck = prevInTypeCheck;
                }

                // resolve the return type annotation
                if (funcDeclAST.typeAnnotation) {
                    returnTypeSymbol = this.resolveReturnTypeAnnotationOfFunctionDeclaration(funcDeclAST, TypeScript.ASTHelpers.getType(funcDeclAST), context);

                    if (!returnTypeSymbol) {
                        signature.returnType = this.getNewErrorTypeSymbol();
                        hadError = true;
                    } else {
                        signature.returnType = returnTypeSymbol;
                    }
                } else {
                    signature.returnType = this.semanticInfoChain.anyTypeSymbol;
                }

                if (!hadError) {
                    if (funcSymbol) {
                        funcSymbol.setUnresolved();
                        if (funcSymbol.type === this.semanticInfoChain.anyTypeSymbol) {
                            funcSymbol.type = functionTypeSymbol;
                        }
                    }
                    signature.setResolved();
                }
            }

            if (funcSymbol) {
                this.resolveOtherDeclarations(funcDeclAST, context);
            }

            if (this.canTypeCheckAST(funcDeclAST, context)) {
                this.typeCheckIndexSignature(funcDeclAST, context);
            }

            return funcSymbol;
        };

        PullTypeResolver.prototype.resolveFunctionDeclaration = function (funcDeclAST, isStatic, name, typeParameters, parameterList, returnTypeAnnotation, block, context) {
            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

            var funcSymbol = funcDecl.getSymbol();

            var signature = funcDecl.getSignatureSymbol();

            var hadError = false;

            var isConstructor = funcDeclAST.kind() === 143 /* ConstructSignature */;

            if (signature) {
                if (signature.isResolved) {
                    if (this.canTypeCheckAST(funcDeclAST, context)) {
                        this.typeCheckAnyFunctionDeclaration(funcDeclAST, isStatic, name, typeParameters, parameterList, returnTypeAnnotation, block, context);
                    }
                    return funcSymbol;
                }

                if (isConstructor && !signature.inResolution) {
                    var classAST = this.getEnclosingClassDeclaration(funcDeclAST);

                    if (classAST) {
                        var classDecl = this.semanticInfoChain.getDeclForAST(classAST);
                        var classSymbol = classDecl.getSymbol();

                        if (!classSymbol.isResolved && !classSymbol.inResolution) {
                            this.resolveDeclaredSymbol(classSymbol, context);
                        }
                    }
                }

                // Save this in case we had set the function type to any because of a recursive reference.
                var functionTypeSymbol = funcSymbol && funcSymbol.type;

                if (signature.inResolution) {
                    // try to set the return type, even though we may be lacking in some information
                    if (returnTypeAnnotation) {
                        var returnTypeSymbol = this.resolveTypeReference(returnTypeAnnotation, context);
                        if (!returnTypeSymbol) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(returnTypeAnnotation, TypeScript.DiagnosticCode.Cannot_resolve_return_type_reference));
                            signature.returnType = this.getNewErrorTypeSymbol();
                            hadError = true;
                        } else {
                            signature.returnType = returnTypeSymbol;

                            if (isConstructor && returnTypeSymbol === this.semanticInfoChain.voidTypeSymbol) {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Constructors_cannot_have_a_return_type_of_void));
                            }
                        }
                    } else {
                        signature.returnType = this.semanticInfoChain.anyTypeSymbol;
                    }

                    if (funcSymbol) {
                        funcSymbol.setUnresolved();
                        if (funcSymbol.type === this.semanticInfoChain.anyTypeSymbol) {
                            funcSymbol.type = functionTypeSymbol;
                        }
                    }
                    signature.setResolved();
                    return funcSymbol;
                }

                if (funcSymbol) {
                    funcSymbol.startResolving();
                }
                signature.startResolving();

                if (typeParameters) {
                    for (var i = 0; i < typeParameters.typeParameters.nonSeparatorCount(); i++) {
                        this.resolveTypeParameterDeclaration(typeParameters.typeParameters.nonSeparatorAt(i), context);
                    }
                }

                // resolve parameter type annotations as necessary
                if (parameterList) {
                    var prevInTypeCheck = context.inTypeCheck;

                    // TODO: why are we setting inTypeCheck false here?
                    context.inTypeCheck = false;

                    for (var i = 0; i < parameterList.parameters.nonSeparatorCount(); i++) {
                        // TODO: why are are calling resolveParameter directly here?
                        this.resolveParameter(parameterList.parameters.nonSeparatorAt(i), context);
                    }

                    context.inTypeCheck = prevInTypeCheck;
                }

                // resolve the return type annotation
                if (returnTypeAnnotation) {
                    returnTypeSymbol = this.resolveReturnTypeAnnotationOfFunctionDeclaration(funcDeclAST, returnTypeAnnotation, context);

                    if (!returnTypeSymbol) {
                        signature.returnType = this.getNewErrorTypeSymbol();
                        hadError = true;
                    } else {
                        signature.returnType = returnTypeSymbol;
                    }
                } else if (funcDecl.kind !== TypeScript.PullElementKind.ConstructSignature) {
                    if (TypeScript.hasFlag(funcDecl.flags, TypeScript.PullElementFlags.Signature)) {
                        signature.returnType = this.semanticInfoChain.anyTypeSymbol;
                        var parentDeclFlags = 0 /* None */;
                        if (TypeScript.hasFlag(funcDecl.kind, TypeScript.PullElementKind.Method) || TypeScript.hasFlag(funcDecl.kind, TypeScript.PullElementKind.ConstructorMethod)) {
                            var parentDecl = funcDecl.getParentDecl();
                            parentDeclFlags = parentDecl.flags;
                        }

                        // if the noImplicitAny flag is set to be true, report an error
                        if (this.compilationSettings.noImplicitAny() && (!TypeScript.hasFlag(parentDeclFlags, TypeScript.PullElementFlags.Ambient) || (TypeScript.hasFlag(parentDeclFlags, TypeScript.PullElementFlags.Ambient) && !TypeScript.hasFlag(funcDecl.flags, TypeScript.PullElementFlags.Private)))) {
                            var funcDeclASTName = name;
                            if (funcDeclASTName) {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode._0_which_lacks_return_type_annotation_implicitly_has_an_any_return_type, [funcDeclASTName.text()]));
                            } else {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Lambda_Function_which_lacks_return_type_annotation_implicitly_has_an_any_return_type));
                            }
                        }
                    } else {
                        this.resolveFunctionBodyReturnTypes(funcDeclAST, block, null, signature, false, funcDecl, context);
                    }
                } else if (funcDecl.kind === TypeScript.PullElementKind.ConstructSignature) {
                    signature.returnType = this.semanticInfoChain.anyTypeSymbol;

                    // if the noImplicitAny flag is set to be true, report an error
                    if (this.compilationSettings.noImplicitAny()) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Constructor_signature_which_lacks_return_type_annotation_implicitly_has_an_any_return_type));
                    }
                }

                if (!hadError) {
                    if (funcSymbol) {
                        funcSymbol.setUnresolved();
                        if (funcSymbol.type === this.semanticInfoChain.anyTypeSymbol) {
                            funcSymbol.type = functionTypeSymbol;
                        }
                    }
                    signature.setResolved();
                }
            }

            if (funcSymbol) {
                this.resolveOtherDeclarations(funcDeclAST, context);
            }

            if (this.canTypeCheckAST(funcDeclAST, context)) {
                this.typeCheckAnyFunctionDeclaration(funcDeclAST, isStatic, name, typeParameters, parameterList, returnTypeAnnotation, block, context);
            }

            return funcSymbol;
        };

        PullTypeResolver.prototype.resolveGetterReturnTypeAnnotation = function (getterFunctionDeclarationAst, enclosingDecl, context) {
            if (getterFunctionDeclarationAst && getterFunctionDeclarationAst.typeAnnotation) {
                return this.resolveTypeReference(TypeScript.ASTHelpers.getType(getterFunctionDeclarationAst), context);
            }

            return null;
        };

        PullTypeResolver.prototype.resolveSetterArgumentTypeAnnotation = function (setterFunctionDeclarationAst, enclosingDecl, context) {
            if (setterFunctionDeclarationAst && setterFunctionDeclarationAst.parameterList && setterFunctionDeclarationAst.parameterList.parameters.nonSeparatorCount() > 0) {
                var parameter = setterFunctionDeclarationAst.parameterList.parameters.nonSeparatorAt(0);
                return this.resolveTypeReference(TypeScript.ASTHelpers.getType(parameter), context);
            }

            return null;
        };

        PullTypeResolver.prototype.resolveAccessorDeclaration = function (funcDeclAst, context) {
            var functionDeclaration = this.semanticInfoChain.getDeclForAST(funcDeclAst);
            var accessorSymbol = functionDeclaration.getSymbol();

            if (accessorSymbol.inResolution) {
                // TODO: Review, should an error be raised?
                accessorSymbol.type = this.semanticInfoChain.anyTypeSymbol;
                accessorSymbol.setResolved();

                return accessorSymbol;
            }

            if (accessorSymbol.isResolved) {
                if (!accessorSymbol.type) {
                    accessorSymbol.type = this.semanticInfoChain.anyTypeSymbol;
                }
            } else {
                var getterSymbol = accessorSymbol.getGetter();
                var getterFunctionDeclarationAst = getterSymbol ? getterSymbol.getDeclarations()[0].ast() : null;
                var hasGetter = getterSymbol !== null;

                var setterSymbol = accessorSymbol.getSetter();
                var setterFunctionDeclarationAst = setterSymbol ? setterSymbol.getDeclarations()[0].ast() : null;
                var hasSetter = setterSymbol !== null;

                var getterAnnotatedType = this.resolveGetterReturnTypeAnnotation(getterFunctionDeclarationAst, functionDeclaration, context);
                var getterHasTypeAnnotation = getterAnnotatedType !== null;

                var setterAnnotatedType = this.resolveSetterArgumentTypeAnnotation(setterFunctionDeclarationAst, functionDeclaration, context);
                var setterHasTypeAnnotation = setterAnnotatedType !== null;

                accessorSymbol.startResolving();

                // resolve accessors - resolution order doesn't matter
                if (hasGetter) {
                    getterSymbol = this.resolveGetAccessorDeclaration(getterFunctionDeclarationAst, getterFunctionDeclarationAst.parameterList, TypeScript.ASTHelpers.getType(getterFunctionDeclarationAst), getterFunctionDeclarationAst.block, setterAnnotatedType, context);
                }

                if (hasSetter) {
                    setterSymbol = this.resolveSetAccessorDeclaration(setterFunctionDeclarationAst, setterFunctionDeclarationAst.parameterList, context);
                }

                // enforce spec resolution rules
                if (hasGetter && hasSetter) {
                    var setterSig = setterSymbol.type.getCallSignatures()[0];
                    var setterParameters = setterSig.parameters;
                    var setterHasParameters = setterParameters.length > 0;
                    var getterSig = getterSymbol.type.getCallSignatures()[0];

                    var setterSuppliedTypeSymbol = setterHasParameters ? setterParameters[0].type : null;
                    var getterSuppliedTypeSymbol = getterSig.returnType;

                    // SPEC: October 1, 2013 section 4.5 -
                    // • If only one accessor includes a type annotation, the other behaves as if it had the same type annotation.
                    // -- In this case setter has annotation and getter does not.
                    if (setterHasTypeAnnotation && !getterHasTypeAnnotation) {
                        getterSuppliedTypeSymbol = setterSuppliedTypeSymbol;
                        getterSig.returnType = setterSuppliedTypeSymbol;
                    } else if ((getterHasTypeAnnotation && !setterHasTypeAnnotation) || (!getterHasTypeAnnotation && !setterHasTypeAnnotation)) {
                        setterSuppliedTypeSymbol = getterSuppliedTypeSymbol;

                        if (setterHasParameters) {
                            setterParameters[0].type = getterSuppliedTypeSymbol;
                        }
                    }

                    // SPEC: October 1, 2013 section 4.5 -
                    // • If both accessors include type annotations, the specified types must be identical.
                    if (!this.typesAreIdentical(setterSuppliedTypeSymbol, getterSuppliedTypeSymbol, context)) {
                        accessorSymbol.type = this.getNewErrorTypeSymbol();
                    } else {
                        accessorSymbol.type = getterSuppliedTypeSymbol;
                    }
                } else if (hasSetter) {
                    // only has setter
                    var setterSig = setterSymbol.type.getCallSignatures()[0];
                    var setterParameters = setterSig.parameters;
                    var setterHasParameters = setterParameters.length > 0;

                    accessorSymbol.type = setterHasParameters ? setterParameters[0].type : this.semanticInfoChain.anyTypeSymbol;
                } else {
                    // only has getter
                    var getterSig = getterSymbol.type.getCallSignatures()[0];
                    accessorSymbol.type = getterSig.returnType;
                }

                accessorSymbol.setResolved();
            }

            // type check if possible
            if (this.canTypeCheckAST(funcDeclAst, context)) {
                this.typeCheckAccessorDeclaration(funcDeclAst, context);
            }

            return accessorSymbol;
        };

        PullTypeResolver.prototype.typeCheckAccessorDeclaration = function (funcDeclAst, context) {
            this.setTypeChecked(funcDeclAst, context);
            var functionDeclaration = this.semanticInfoChain.getDeclForAST(funcDeclAst);
            var accessorSymbol = functionDeclaration.getSymbol();
            var getterSymbol = accessorSymbol.getGetter();
            var setterSymbol = accessorSymbol.getSetter();

            var isGetter = funcDeclAst.kind() === 139 /* GetAccessor */;
            if (isGetter) {
                var getterFunctionDeclarationAst = funcDeclAst;
                context.pushNewContextualType(getterSymbol.type);
                this.typeCheckGetAccessorDeclaration(getterFunctionDeclarationAst, context);
                context.popAnyContextualType();
            } else {
                var setterFunctionDeclarationAst = funcDeclAst;
                this.typeCheckSetAccessorDeclaration(setterFunctionDeclarationAst, context);
            }
        };

        PullTypeResolver.prototype.resolveGetAccessorDeclaration = function (funcDeclAST, parameters, returnTypeAnnotation, block, setterAnnotatedType, context) {
            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
            var accessorSymbol = funcDecl.getSymbol();

            var getterSymbol = accessorSymbol.getGetter();
            var getterTypeSymbol = getterSymbol.type;

            var signature = getterTypeSymbol.getCallSignatures()[0];

            var hadError = false;

            if (signature) {
                if (signature.isResolved) {
                    return getterSymbol;
                }

                if (signature.inResolution) {
                    // PULLTODO: Error or warning?
                    signature.returnType = this.semanticInfoChain.anyTypeSymbol;
                    signature.setResolved();

                    return getterSymbol;
                }

                signature.startResolving();

                // resolve the return type annotation
                if (returnTypeAnnotation) {
                    var returnTypeSymbol = this.resolveReturnTypeAnnotationOfFunctionDeclaration(funcDeclAST, returnTypeAnnotation, context);

                    if (!returnTypeSymbol) {
                        signature.returnType = this.getNewErrorTypeSymbol();

                        hadError = true;
                    } else {
                        signature.returnType = returnTypeSymbol;
                    }
                } else {
                    // SPEC: October 1, 2013 section 4.5 -
                    // • If only one accessor includes a type annotation, the other behaves as if it had the same type annotation.
                    // -- Use setterAnnotatedType if available
                    // • If neither accessor includes a type annotation, the inferred return type of the get accessor becomes the parameter type of the set accessor.
                    if (!setterAnnotatedType) {
                        this.resolveFunctionBodyReturnTypes(funcDeclAST, block, null, signature, false, funcDecl, context);
                    } else {
                        signature.returnType = setterAnnotatedType;
                    }
                }

                if (!hadError) {
                    signature.setResolved();
                }
            }

            return getterSymbol;
        };

        PullTypeResolver.prototype.checkIfGetterAndSetterTypeMatch = function (funcDeclAST, context) {
            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
            var accessorSymbol = funcDecl.getSymbol();
            var getter = accessorSymbol.getGetter();
            var setter = accessorSymbol.getSetter();

            if (getter && setter) {
                var getterAST = getter.getDeclarations()[0].ast();
                var setterAST = setter.getDeclarations()[0].ast();

                // There exists:
                //     return type annotaion for the getter &&
                //     parameter type annotation for the setter
                if (getterAST.typeAnnotation && PullTypeResolver.hasSetAccessorParameterTypeAnnotation(setterAST)) {
                    var setterSig = setter.type.getCallSignatures()[0];
                    var setterParameters = setterSig.parameters;

                    var getter = accessorSymbol.getGetter();
                    var getterSig = getter.type.getCallSignatures()[0];

                    var setterSuppliedTypeSymbol = setterParameters[0].type;
                    var getterSuppliedTypeSymbol = getterSig.returnType;

                    // Report errors if type do not match
                    if (!this.typesAreIdentical(setterSuppliedTypeSymbol, getterSuppliedTypeSymbol, context)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.get_and_set_accessor_must_have_the_same_type));
                    }
                }
            }
        };

        PullTypeResolver.prototype.typeCheckGetAccessorDeclaration = function (funcDeclAST, context) {
            // Accessors are handled only by resolve/typeCheckAccessorDeclaration,
            // hence the resolve/typeCheckGetAccessorDeclaration is helper and need not set setTypeChecked flag
            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
            var accessorSymbol = funcDecl.getSymbol();

            this.resolveReturnTypeAnnotationOfFunctionDeclaration(funcDeclAST, TypeScript.ASTHelpers.getType(funcDeclAST), context);

            this.resolveAST(funcDeclAST.block, false, context);

            this.validateVariableDeclarationGroups(funcDecl, context);

            var enclosingDecl = this.getEnclosingDecl(funcDecl);

            var hasReturn = (funcDecl.flags & (TypeScript.PullElementFlags.Signature | TypeScript.PullElementFlags.HasReturnStatement)) !== 0;
            var funcNameAST = funcDeclAST.propertyName;

            // If there is no return statement report error:
            //      signature is doesnt have the return statement flag &&
            //      accessor body has atleast one statement and it isnt throw statement
            if (!hasReturn && !this.containsSingleThrowStatement(funcDeclAST.block)) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcNameAST, TypeScript.DiagnosticCode.Getters_must_return_a_value));
            }

            // Setter with return value is checked in typeCheckReturnExpression
            var setter = accessorSymbol.getSetter();
            if (setter) {
                var setterDecl = setter.getDeclarations()[0];
                var setterIsPrivate = TypeScript.hasFlag(setterDecl.flags, TypeScript.PullElementFlags.Private);
                var getterIsPrivate = TypeScript.hasModifier(funcDeclAST.modifiers, TypeScript.PullElementFlags.Private);

                if (getterIsPrivate !== setterIsPrivate) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcNameAST, TypeScript.DiagnosticCode.Getter_and_setter_accessors_do_not_agree_in_visibility));
                }

                this.checkIfGetterAndSetterTypeMatch(funcDeclAST, context);
            }

            this.checkFunctionTypePrivacy(funcDeclAST, TypeScript.hasModifier(funcDeclAST.modifiers, TypeScript.PullElementFlags.Static), null, TypeScript.ASTHelpers.parametersFromParameterList(funcDeclAST.parameterList), TypeScript.ASTHelpers.getType(funcDeclAST), funcDeclAST.block, context);
        };

        PullTypeResolver.hasSetAccessorParameterTypeAnnotation = function (setAccessor) {
            return setAccessor.parameterList && setAccessor.parameterList.parameters.nonSeparatorCount() > 0 && setAccessor.parameterList.parameters.nonSeparatorAt(0).typeAnnotation !== null;
        };

        PullTypeResolver.prototype.resolveSetAccessorDeclaration = function (funcDeclAST, parameterList, context) {
            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
            var accessorSymbol = funcDecl.getSymbol();

            var setterSymbol = accessorSymbol.getSetter();
            var setterTypeSymbol = setterSymbol.type;

            var signature = funcDecl.getSignatureSymbol();

            var hadError = false;

            if (signature) {
                if (signature.isResolved) {
                    return setterSymbol;
                }

                if (signature.inResolution) {
                    // PULLTODO: Error or warning?
                    signature.returnType = this.semanticInfoChain.voidTypeSymbol;
                    signature.setResolved();
                    return setterSymbol;
                }

                signature.startResolving();

                // resolve parameter type annotations as necessary
                if (parameterList) {
                    for (var i = 0; i < parameterList.parameters.nonSeparatorCount(); i++) {
                        this.resolveParameter(parameterList.parameters.nonSeparatorAt(i), context);
                    }
                }

                // SPEC: October 1, 2013 section 4.5 -
                // • A set accessor declaration is processed in the same manner as an ordinary function declaration
                //      with a single parameter and a Void return type.
                signature.returnType = this.semanticInfoChain.voidTypeSymbol;

                if (!hadError) {
                    signature.setResolved();
                }
            }

            return setterSymbol;
        };

        PullTypeResolver.prototype.typeCheckSetAccessorDeclaration = function (funcDeclAST, context) {
            // Accessors are handled only by resolve/typeCheckAccessorDeclaration,
            // hence the resolve/typeCheckSetAccessorDeclaration is helper and need not set setTypeChecked flag
            var funcDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
            var accessorSymbol = funcDecl.getSymbol();

            if (funcDeclAST.parameterList) {
                for (var i = 0; i < funcDeclAST.parameterList.parameters.nonSeparatorCount(); i++) {
                    this.resolveParameter(funcDeclAST.parameterList.parameters.nonSeparatorAt(i), context);
                }
            }

            this.resolveAST(funcDeclAST.block, false, context);

            this.validateVariableDeclarationGroups(funcDecl, context);

            var hasReturn = (funcDecl.flags & (TypeScript.PullElementFlags.Signature | TypeScript.PullElementFlags.HasReturnStatement)) !== 0;

            var getter = accessorSymbol.getGetter();

            var funcNameAST = funcDeclAST.propertyName;

            // Setter with return value is checked in typeCheckReturnExpression
            if (getter) {
                var getterDecl = getter.getDeclarations()[0];
                var getterIsPrivate = TypeScript.hasFlag(getterDecl.flags, TypeScript.PullElementFlags.Private);
                var setterIsPrivate = TypeScript.hasModifier(funcDeclAST.modifiers, TypeScript.PullElementFlags.Private);

                if (getterIsPrivate !== setterIsPrivate) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcNameAST, TypeScript.DiagnosticCode.Getter_and_setter_accessors_do_not_agree_in_visibility));
                }

                this.checkIfGetterAndSetterTypeMatch(funcDeclAST, context);
            } else {
                // There is no getter specified
                // Only report noImplicitAny error message on setter if there is no getter
                // if the noImplicitAny flag is set to be true, report an error
                if (this.compilationSettings.noImplicitAny()) {
                    // if setter has an any type, it must be implicit any
                    var setterFunctionDeclarationAst = funcDeclAST;
                    if (!PullTypeResolver.hasSetAccessorParameterTypeAnnotation(setterFunctionDeclarationAst) && accessorSymbol.type === this.semanticInfoChain.anyTypeSymbol) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode._0_which_lacks_get_accessor_and_parameter_type_annotation_on_set_accessor_implicitly_has_an_any_type, [setterFunctionDeclarationAst.propertyName.text()]));
                    }
                }
            }

            this.checkFunctionTypePrivacy(funcDeclAST, TypeScript.hasModifier(funcDeclAST.modifiers, TypeScript.PullElementFlags.Static), null, TypeScript.ASTHelpers.parametersFromParameterList(funcDeclAST.parameterList), null, funcDeclAST.block, context);
        };

        PullTypeResolver.prototype.resolveList = function (list, context) {
            if (this.canTypeCheckAST(list, context)) {
                this.setTypeChecked(list, context);

                for (var i = 0, n = list.childCount(); i < n; i++) {
                    this.resolveAST(list.childAt(i), false, context);
                }
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveSeparatedList = function (list, context) {
            if (this.canTypeCheckAST(list, context)) {
                this.setTypeChecked(list, context);

                for (var i = 0, n = list.nonSeparatorCount(); i < n; i++) {
                    this.resolveAST(list.nonSeparatorAt(i), false, context);
                }
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveVoidExpression = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);

                this.resolveAST(ast.expression, false, context);
            }

            // September 17, 2013: The void operator takes an operand of any type and produces the
            // value undefined.The type of the result is the Undefined type(3.2.6).
            return this.semanticInfoChain.undefinedTypeSymbol;
        };

        PullTypeResolver.prototype.resolveLogicalOperation = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckLogicalOperation(ast, context);
            }

            // September 17, 2013: The result is always of the Boolean primitive type.
            return this.semanticInfoChain.booleanTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckLogicalOperation = function (binex, context) {
            this.setTypeChecked(binex, context);

            var leftType = this.resolveAST(binex.left, false, context).type;
            var rightType = this.resolveAST(binex.right, false, context).type;

            // September 17, 2013:
            // The <, >, <=, >=, ==, !=, ===, and !== operators
            // These operators require one operand type to be identical to or a subtype of the
            // other operand type.
            var comparisonInfo = new TypeComparisonInfo();
            if (!this.sourceIsAssignableToTarget(leftType, rightType, binex, context, comparisonInfo) && !this.sourceIsAssignableToTarget(rightType, leftType, binex, context, comparisonInfo)) {
                var enclosingSymbol = this.getEnclosingSymbolForAST(binex);
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binex, TypeScript.DiagnosticCode.Operator_0_cannot_be_applied_to_types_1_and_2, [
                    TypeScript.SyntaxFacts.getText(TypeScript.SyntaxFacts.getOperatorTokenFromBinaryExpression(binex.kind())),
                    leftType.toString(enclosingSymbol), rightType.toString(enclosingSymbol)]));
            }
        };

        PullTypeResolver.prototype.resolveLogicalNotExpression = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);

                this.resolveAST(ast.operand, false, context);
            }

            // September 17, 2013: The ! operator permits its operand to be of any type and
            // produces a result of the Boolean primitive type.
            return this.semanticInfoChain.booleanTypeSymbol;
        };

        PullTypeResolver.prototype.resolveUnaryArithmeticOperation = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckUnaryArithmeticOperation(ast, context);
            }

            // September 17, 2013:
            // The ++ and-- operators ... produce a result of the Number primitive type.
            // The +, –, and ~ operators ... produce a result of the Number primitive type.
            return this.semanticInfoChain.numberTypeSymbol;
        };

        PullTypeResolver.prototype.resolvePostfixUnaryExpression = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckPostfixUnaryExpression(ast, context);
            }

            // September 17, 2013:
            // The ++ and-- operators ... produce a result of the Number primitive type.
            // The +, –, and ~ operators ... produce a result of the Number primitive type.
            return this.semanticInfoChain.numberTypeSymbol;
        };

        PullTypeResolver.prototype.isAnyOrNumberOrEnum = function (type) {
            return this.isAnyOrEquivalent(type) || type === this.semanticInfoChain.numberTypeSymbol || TypeScript.PullHelpers.symbolIsEnum(type);
        };

        PullTypeResolver.prototype.typeCheckUnaryArithmeticOperation = function (unaryExpression, context) {
            this.setTypeChecked(unaryExpression, context);

            var nodeType = unaryExpression.kind();
            var expression = this.resolveAST(unaryExpression.operand, false, context);

            // September 17, 2013: The +, –, and ~ operators
            // These operators permit their operand to be of any type and produce a result of the
            // Number primitive type.
            if (nodeType === 164 /* PlusExpression */ || nodeType == 165 /* NegateExpression */ || nodeType == 166 /* BitwiseNotExpression */) {
                return;
            }

            TypeScript.Debug.assert(nodeType === 168 /* PreIncrementExpression */ || nodeType === 169 /* PreDecrementExpression */);

            // September 17, 2013: 4.14.1	The ++ and -- operators
            // These operators, in prefix or postfix form, require their operand to be of type Any,
            // the Number primitive type, or an enum type, and classified as a reference(section 4.1).
            var operandType = expression.type;
            if (!this.isAnyOrNumberOrEnum(operandType)) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(unaryExpression.operand, TypeScript.DiagnosticCode.The_type_of_a_unary_arithmetic_operation_operand_must_be_of_type_any_number_or_an_enum_type));
            }

            // September 17, ... and classified as a reference(section 4.1).
            if (!this.isReference(unaryExpression.operand, expression)) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(unaryExpression.operand, TypeScript.DiagnosticCode.The_operand_of_an_increment_or_decrement_operator_must_be_a_variable_property_or_indexer));
            }
        };

        PullTypeResolver.prototype.typeCheckPostfixUnaryExpression = function (unaryExpression, context) {
            this.setTypeChecked(unaryExpression, context);

            var nodeType = unaryExpression.kind();
            var expression = this.resolveAST(unaryExpression.operand, false, context);

            TypeScript.Debug.assert(nodeType === 210 /* PostIncrementExpression */ || nodeType === 211 /* PostDecrementExpression */);

            // September 17, 2013: 4.14.1	The ++ and -- operators
            // These operators, in prefix or postfix form, require their operand to be of type Any,
            // the Number primitive type, or an enum type, and classified as a reference(section 4.1).
            var operandType = expression.type;
            if (!this.isAnyOrNumberOrEnum(operandType)) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(unaryExpression.operand, TypeScript.DiagnosticCode.The_type_of_a_unary_arithmetic_operation_operand_must_be_of_type_any_number_or_an_enum_type));
            }

            // September 17, ... and classified as a reference(section 4.1).
            if (!this.isReference(unaryExpression.operand, expression)) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(unaryExpression.operand, TypeScript.DiagnosticCode.The_operand_of_an_increment_or_decrement_operator_must_be_a_variable_property_or_indexer));
            }
        };

        PullTypeResolver.prototype.resolveBinaryArithmeticExpression = function (binaryExpression, context) {
            if (this.canTypeCheckAST(binaryExpression, context)) {
                this.typeCheckBinaryArithmeticExpression(binaryExpression, context);
            }

            // September 17, 2013: The result is always of the Number primitive type.
            return this.semanticInfoChain.numberTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckBinaryArithmeticExpression = function (binaryExpression, context) {
            this.setTypeChecked(binaryExpression, context);

            var lhsSymbol = this.resolveAST(binaryExpression.left, false, context);

            var lhsType = lhsSymbol.type;
            var rhsType = this.resolveAST(binaryExpression.right, false, context).type;

            // September 17, 2013:
            // If one operand is the null or undefined value, it is treated as having the
            // type of the other operand.
            if (lhsType === this.semanticInfoChain.nullTypeSymbol || lhsType === this.semanticInfoChain.undefinedTypeSymbol) {
                lhsType = rhsType;
            }

            if (rhsType === this.semanticInfoChain.nullTypeSymbol || rhsType === this.semanticInfoChain.undefinedTypeSymbol) {
                rhsType = lhsType;
            }

            // September 17, 2013:
            // 4.15.1	The *, /, %, –, <<, >>, >>>, &, ^, and | operators
            // These operators require their operands to be of type Any, the Number primitive type,
            // or an enum type
            var lhsIsFit = this.isAnyOrNumberOrEnum(lhsType);
            var rhsIsFit = this.isAnyOrNumberOrEnum(rhsType);

            if (!rhsIsFit) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.right, TypeScript.DiagnosticCode.The_right_hand_side_of_an_arithmetic_operation_must_be_of_type_any_number_or_an_enum_type));
            }

            if (!lhsIsFit) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.left, TypeScript.DiagnosticCode.The_left_hand_side_of_an_arithmetic_operation_must_be_of_type_any_number_or_an_enum_type));
            }

            if (lhsIsFit && rhsIsFit) {
                switch (binaryExpression.kind()) {
                    case 183 /* LeftShiftAssignmentExpression */:
                    case 184 /* SignedRightShiftAssignmentExpression */:
                    case 185 /* UnsignedRightShiftAssignmentExpression */:
                    case 176 /* SubtractAssignmentExpression */:
                    case 177 /* MultiplyAssignmentExpression */:
                    case 178 /* DivideAssignmentExpression */:
                    case 179 /* ModuloAssignmentExpression */:
                    case 182 /* OrAssignmentExpression */:
                    case 180 /* AndAssignmentExpression */:
                    case 181 /* ExclusiveOrAssignmentExpression */:
                        // Check if LHS is a valid target
                        if (!this.isReference(binaryExpression.left, lhsSymbol)) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.left, TypeScript.DiagnosticCode.Invalid_left_hand_side_of_assignment_expression));
                        }

                        this.checkAssignability(binaryExpression.left, rhsType, lhsType, context);
                }
            }
        };

        PullTypeResolver.prototype.resolveTypeOfExpression = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);

                this.resolveAST(ast.expression, false, context);
            }

            // September 17, 2013: The typeof operator takes an operand of any type and produces
            // a value of the String primitive type
            return this.semanticInfoChain.stringTypeSymbol;
        };

        PullTypeResolver.prototype.resolveThrowStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);

                this.resolveAST(ast.expression, false, context);
            }

            // All statements have the 'void' type.
            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveDeleteExpression = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);
                this.resolveAST(ast.expression, false, context);
            }

            // September 17, 2013: The delete operator takes an operand of any type and produces a
            // result of the Boolean primitive type.
            return this.semanticInfoChain.booleanTypeSymbol;
        };

        PullTypeResolver.prototype.resolveInstanceOfExpression = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckInstanceOfExpression(ast, context);
            }

            // September 17, 2013: The result is always of the Boolean primitive type.
            return this.semanticInfoChain.booleanTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckInstanceOfExpression = function (binaryExpression, context) {
            this.setTypeChecked(binaryExpression, context);

            // Section 4.15.4:
            // September 17, 2013: The instanceof operator requires the left operand to be of type
            // Any, an object type, or a type parameter type, and the right operand to be of type
            // Any or a subtype of the ‘Function’ interface type.
            // We are using assignability instead of subtype here, which will be reflected in the
            // new spec.
            var lhsType = this.resolveAST(binaryExpression.left, false, context).type;
            var rhsType = this.resolveAST(binaryExpression.right, false, context).type;

            var enclosingSymbol = this.getEnclosingSymbolForAST(binaryExpression);
            var isValidLHS = this.isAnyOrEquivalent(lhsType) || lhsType.isObject() || lhsType.isTypeParameter();
            var isValidRHS = this.isAnyOrEquivalent(rhsType) || this.typeIsAssignableToFunction(rhsType, binaryExpression, context);

            if (!isValidLHS) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.left, TypeScript.DiagnosticCode.The_left_hand_side_of_an_instanceof_expression_must_be_of_type_any_an_object_type_or_a_type_parameter));
            }

            if (!isValidRHS) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.right, TypeScript.DiagnosticCode.The_right_hand_side_of_an_instanceof_expression_must_be_of_type_any_or_of_a_type_assignable_to_the_Function_interface_type));
            }
        };

        PullTypeResolver.prototype.resolveCommaExpression = function (commaExpression, context) {
            if (this.canTypeCheckAST(commaExpression, context)) {
                this.setTypeChecked(commaExpression, context);

                this.resolveAST(commaExpression.left, false, context);
            }

            // September 17, 2013: The comma operator permits the operands to be of any type and
            // produces a result that is of the same type as the second operand.
            return this.resolveAST(commaExpression.right, false, context).type;
        };

        PullTypeResolver.prototype.resolveInExpression = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckInExpression(ast, context);
            }

            // October 11, 2013:
            // The in operator requires the left operand to be of type Any, the String primitive
            // type, or the Number primitive type, and the right operand to be of type Any, an
            // object type, or a type parameter type.
            //
            // The result is always of the Boolean primitive type.
            return this.semanticInfoChain.booleanTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckInExpression = function (binaryExpression, context) {
            this.setTypeChecked(binaryExpression, context);

            // October 11, 2013:
            // The in operator requires the left operand to be of type Any, the String primitive
            // type, or the Number primitive type, and the right operand to be of type Any, an
            // object type, or a type parameter type.
            var lhsType = this.resolveAST(binaryExpression.left, false, context).type;
            var rhsType = this.resolveAST(binaryExpression.right, false, context).type;

            var isValidLHS = this.isAnyOrEquivalent(lhsType.type) || lhsType.type === this.semanticInfoChain.stringTypeSymbol || lhsType.type === this.semanticInfoChain.numberTypeSymbol;

            var isValidRHS = this.isAnyOrEquivalent(rhsType) || rhsType.isObject() || rhsType.isTypeParameter();

            if (!isValidLHS) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.left, TypeScript.DiagnosticCode.The_left_hand_side_of_an_in_expression_must_be_of_types_any_string_or_number));
            }

            if (!isValidRHS) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.right, TypeScript.DiagnosticCode.The_right_hand_side_of_an_in_expression_must_be_of_type_any_an_object_type_or_a_type_parameter));
            }
        };

        PullTypeResolver.prototype.resolveForStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);

                this.resolveAST(ast.variableDeclaration, false, context);
                this.resolveAST(ast.initializer, false, context);
                this.resolveAST(ast.condition, false, context);
                this.resolveAST(ast.incrementor, false, context);
                this.resolveAST(ast.statement, false, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveForInStatement = function (forInStatement, context) {
            if (this.canTypeCheckAST(forInStatement, context)) {
                this.typeCheckForInStatement(forInStatement, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckForInStatement = function (forInStatement, context) {
            this.setTypeChecked(forInStatement, context);

            if (forInStatement.variableDeclaration) {
                var declaration = forInStatement.variableDeclaration;

                // The parser will already have reported an error if 0 or more than 1 variable
                // declarators are provided.
                if (declaration.declarators.nonSeparatorCount() === 1) {
                    var varDecl = declaration.declarators.nonSeparatorAt(0);

                    if (varDecl.typeAnnotation) {
                        // November 18, 2013
                        // VarDecl must be a variable declaration without a type annotation.
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(declaration, TypeScript.DiagnosticCode.Variable_declarations_of_a_for_statement_cannot_use_a_type_annotation));
                    }
                }
            } else {
                // November 18, 2013
                // In a ‘for-in’ statement of the form
                // for (Var in Expr) Statement
                // Var must be an expression classified as a reference of type Any or the String primitive type
                var varSym = this.resolveAST(forInStatement.left, false, context);
                var isStringOrNumber = varSym.type === this.semanticInfoChain.stringTypeSymbol || this.isAnyOrEquivalent(varSym.type);

                if (!isStringOrNumber) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(forInStatement.left, TypeScript.DiagnosticCode.Variable_declarations_of_a_for_statement_must_be_of_types_string_or_any));
                }
            }

            // November 18, 2013
            // In a ‘for-in’ statement of the form
            // for (Var in Expr) Statement [or of the form]
            // for (var VarDecl in Expr) Statement
            // ... Expr must be an expression of type Any, an object type, or a type parameter type.
            var rhsType = this.resolveAST(forInStatement.expression, false, context).type;
            var isValidRHS = rhsType && (this.isAnyOrEquivalent(rhsType) || rhsType.isObject() || rhsType.isTypeParameter());

            if (!isValidRHS) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(forInStatement.expression, TypeScript.DiagnosticCode.The_right_hand_side_of_a_for_in_statement_must_be_of_type_any_an_object_type_or_a_type_parameter));
            }

            this.resolveAST(forInStatement.statement, false, context);
        };

        PullTypeResolver.prototype.resolveWhileStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckWhileStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckWhileStatement = function (ast, context) {
            this.setTypeChecked(ast, context);

            this.resolveAST(ast.condition, false, context);
            this.resolveAST(ast.statement, false, context);
        };

        PullTypeResolver.prototype.resolveDoStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckDoStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckDoStatement = function (ast, context) {
            this.setTypeChecked(ast, context);

            this.resolveAST(ast.condition, false, context);
            this.resolveAST(ast.statement, false, context);
        };

        PullTypeResolver.prototype.resolveIfStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckIfStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckIfStatement = function (ast, context) {
            this.setTypeChecked(ast, context);

            this.resolveAST(ast.condition, false, context);
            this.resolveAST(ast.statement, false, context);
            this.resolveAST(ast.elseClause, false, context);
        };

        PullTypeResolver.prototype.resolveElseClause = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckElseClause(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckElseClause = function (ast, context) {
            this.setTypeChecked(ast, context);

            this.resolveAST(ast.statement, false, context);
        };

        PullTypeResolver.prototype.resolveBlock = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);
                this.resolveAST(ast.statements, false, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveVariableStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);
                this.resolveAST(ast.declaration, false, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveVariableDeclarationList = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);
                this.resolveAST(ast.declarators, false, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveWithStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckWithStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckWithStatement = function (ast, context) {
            this.setTypeChecked(ast, context);
            var withStatement = ast;
            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(withStatement.condition, TypeScript.DiagnosticCode.All_symbols_within_a_with_block_will_be_resolved_to_any));
        };

        PullTypeResolver.prototype.resolveTryStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckTryStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckTryStatement = function (ast, context) {
            this.setTypeChecked(ast, context);
            var tryStatement = ast;

            this.resolveAST(tryStatement.block, false, context);
            this.resolveAST(tryStatement.catchClause, false, context);
            this.resolveAST(tryStatement.finallyClause, false, context);
        };

        PullTypeResolver.prototype.resolveCatchClause = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckCatchClause(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckCatchClause = function (ast, context) {
            this.setTypeChecked(ast, context);
            this.resolveAST(ast.block, false, context);

            var catchDecl = this.semanticInfoChain.getDeclForAST(ast);
            this.validateVariableDeclarationGroups(catchDecl, context);
        };

        PullTypeResolver.prototype.resolveFinallyClause = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckFinallyClause(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckFinallyClause = function (ast, context) {
            this.setTypeChecked(ast, context);
            this.resolveAST(ast.block, false, context);
        };

        PullTypeResolver.prototype.getEnclosingFunctionDeclaration = function (ast) {
            var enclosingDecl = this.getEnclosingDeclForAST(ast);

            while (enclosingDecl) {
                if (enclosingDecl.kind & TypeScript.PullElementKind.SomeFunction) {
                    return enclosingDecl;
                }

                enclosingDecl = enclosingDecl.getParentDecl();
            }

            return null;
        };

        PullTypeResolver.prototype.resolveReturnExpression = function (expression, enclosingFunction, context) {
            if (enclosingFunction) {
                enclosingFunction.setFlag(TypeScript.PullElementFlags.HasReturnStatement);
            }

            // push contextual type
            var isContextuallyTyped = false;

            if (enclosingFunction) {
                var enclosingDeclAST = this.getASTForDecl(enclosingFunction);
                var typeAnnotation = TypeScript.ASTHelpers.getType(enclosingDeclAST);
                if (typeAnnotation) {
                    // The containing function has a type annotation, propagate it as the contextual type
                    var returnTypeAnnotationSymbol = this.resolveTypeReference(typeAnnotation, context);
                    if (returnTypeAnnotationSymbol) {
                        isContextuallyTyped = true;
                        context.pushNewContextualType(returnTypeAnnotationSymbol);
                    }
                } else {
                    // No type annotation, check if there is a contextual type enforced on the function, and propagate that
                    var currentContextualType = context.getContextualType();
                    if (currentContextualType && currentContextualType.isFunction()) {
                        var contextualSignatures = currentContextualType.kind == TypeScript.PullElementKind.ConstructorType ? currentContextualType.getConstructSignatures() : currentContextualType.getCallSignatures();
                        var currentContextualTypeSignatureSymbol = contextualSignatures[0];

                        var currentContextualTypeReturnTypeSymbol = currentContextualTypeSignatureSymbol.returnType;
                        if (currentContextualTypeReturnTypeSymbol) {
                            isContextuallyTyped = true;
                            context.propagateContextualType(currentContextualTypeReturnTypeSymbol);
                        }
                    }
                }
            }

            var result = this.resolveAST(expression, isContextuallyTyped, context).type;
            if (isContextuallyTyped) {
                context.popAnyContextualType();
            }

            return result;
        };

        PullTypeResolver.prototype.typeCheckReturnExpression = function (expression, expressionType, enclosingFunction, context) {
            // Return type of constructor signature must be assignable to the instance type of the class.
            if (enclosingFunction && enclosingFunction.kind === TypeScript.PullElementKind.ConstructorMethod) {
                var classDecl = enclosingFunction.getParentDecl();
                if (classDecl) {
                    var classSymbol = classDecl.getSymbol();
                    this.resolveDeclaredSymbol(classSymbol, context);

                    var comparisonInfo = new TypeComparisonInfo();
                    var isAssignable = this.sourceIsAssignableToTarget(expressionType, classSymbol.type, expression, context, comparisonInfo);
                    if (!isAssignable) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(expression, TypeScript.DiagnosticCode.Return_type_of_constructor_signature_must_be_assignable_to_the_instance_type_of_the_class));
                    }
                }
            }

            if (enclosingFunction && enclosingFunction.kind === TypeScript.PullElementKind.SetAccessor) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(expression, TypeScript.DiagnosticCode.Setters_cannot_return_a_value));
            }

            if (enclosingFunction) {
                var enclosingDeclAST = this.getASTForDecl(enclosingFunction);
                var typeAnnotation = TypeScript.ASTHelpers.getType(enclosingDeclAST);
                if (typeAnnotation || enclosingFunction.kind === TypeScript.PullElementKind.GetAccessor) {
                    var signatureSymbol = enclosingFunction.getSignatureSymbol();
                    var sigReturnType = signatureSymbol.returnType;

                    if (expressionType && sigReturnType) {
                        var comparisonInfo = new TypeComparisonInfo();
                        var upperBound = null;

                        this.resolveDeclaredSymbol(expressionType, context);
                        this.resolveDeclaredSymbol(sigReturnType, context);

                        var isAssignable = this.sourceIsAssignableToTarget(expressionType, sigReturnType, expression, context, comparisonInfo);

                        if (!isAssignable) {
                            var enclosingSymbol = this.getEnclosingSymbolForAST(expression);
                            if (comparisonInfo.message) {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(expression, TypeScript.DiagnosticCode.Cannot_convert_0_to_1_NL_2, [expressionType.toString(enclosingSymbol), sigReturnType.toString(enclosingSymbol), comparisonInfo.message]));
                            } else {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(expression, TypeScript.DiagnosticCode.Cannot_convert_0_to_1, [expressionType.toString(enclosingSymbol), sigReturnType.toString(enclosingSymbol)]));
                            }
                        }
                    }
                }
            }
        };

        PullTypeResolver.prototype.resolveReturnStatement = function (returnAST, context) {
            var enclosingFunction = this.getEnclosingFunctionDeclaration(returnAST);
            if (enclosingFunction) {
                enclosingFunction.setFlag(TypeScript.PullElementFlags.HasReturnStatement);
            }

            var returnType = this.getSymbolForAST(returnAST, context);
            var canTypeCheckAST = this.canTypeCheckAST(returnAST, context);
            if (!returnType || canTypeCheckAST) {
                var returnExpr = returnAST.expression;

                var resolvedReturnType = returnExpr === null ? this.semanticInfoChain.voidTypeSymbol : this.resolveReturnExpression(returnExpr, enclosingFunction, context);

                if (!returnType) {
                    returnType = resolvedReturnType;
                    this.setSymbolForAST(returnAST, resolvedReturnType, context);
                }

                if (returnExpr && canTypeCheckAST) {
                    this.setTypeChecked(returnExpr, context);
                    this.typeCheckReturnExpression(returnExpr, resolvedReturnType, enclosingFunction, context);
                }
            }

            return returnType;
        };

        PullTypeResolver.prototype.resolveSwitchStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckSwitchStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckSwitchStatement = function (ast, context) {
            this.setTypeChecked(ast, context);

            var expressionType = this.resolveAST(ast.expression, false, context).type;

            for (var i = 0, n = ast.switchClauses.childCount(); i < n; i++) {
                var switchClause = ast.switchClauses.childAt(i);
                if (switchClause.kind() === 233 /* CaseSwitchClause */) {
                    var caseSwitchClause = switchClause;

                    var caseClauseExpressionType = this.resolveAST(caseSwitchClause.expression, false, context).type;
                    this.resolveAST(caseSwitchClause.statements, false, context);

                    var comparisonInfo = new TypeComparisonInfo();
                    if (!this.sourceIsAssignableToTarget(expressionType, caseClauseExpressionType, caseSwitchClause.expression, context, comparisonInfo) && !this.sourceIsAssignableToTarget(caseClauseExpressionType, expressionType, caseSwitchClause.expression, context, comparisonInfo)) {
                        var enclosingSymbol = this.getEnclosingSymbolForAST(caseSwitchClause.expression);
                        if (comparisonInfo.message) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(caseSwitchClause.expression, TypeScript.DiagnosticCode.Cannot_convert_0_to_1_NL_2, [caseClauseExpressionType.toString(enclosingSymbol), expressionType.toString(enclosingSymbol), comparisonInfo.message]));
                        } else {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(caseSwitchClause.expression, TypeScript.DiagnosticCode.Cannot_convert_0_to_1, [caseClauseExpressionType.toString(enclosingSymbol), expressionType.toString(enclosingSymbol)]));
                        }
                    }
                } else {
                    var defaultSwitchClause = switchClause;
                    this.resolveAST(defaultSwitchClause.statements, false, context);
                }
            }
        };

        PullTypeResolver.prototype.resolveLabeledStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckLabeledStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckLabeledStatement = function (ast, context) {
            this.setTypeChecked(ast, context);

            // Note that break/continue are treated differently.  ES5 says this about a break statement:
            // A program is considered syntactically incorrect if ...:
            //
            // The program contains a break statement with the optional Identifier, where Identifier
            // does not appear in the label set of an enclosing (but not crossing function boundaries)
            // **Statement.**
            //
            // However, it says this about continue statements:
            //
            // The program contains a continue statement with the optional Identifier, where Identifier
            // does not appear in the label set of an enclosing (but not crossing function boundaries)
            // **IterationStatement.**
            // In other words, you can 'break' to any enclosing statement.  But you can only 'continue'
            // to an enclosing *iteration* statement.
            var labelIdentifier = ast.identifier.valueText();

            var breakableLabels = this.getEnclosingLabels(ast, true, false);

            // It is invalid to have a label enclosed in a label of the same name.
            var matchingLabel = TypeScript.ArrayUtilities.firstOrDefault(breakableLabels, function (s) {
                return s.identifier.valueText() === labelIdentifier;
            });
            if (matchingLabel) {
                context.postDiagnostic(this.semanticInfoChain.duplicateIdentifierDiagnosticFromAST(ast.identifier, labelIdentifier, matchingLabel));
            }

            this.resolveAST(ast.statement, false, context);
        };

        PullTypeResolver.prototype.labelIsOnContinuableConstruct = function (statement) {
            switch (statement.kind()) {
                case 160 /* LabeledStatement */:
                    // Labels work transitively.  i.e. if you have:
                    //      foo:
                    //      bar:
                    //      while(...)
                    //
                    // Then both 'foo' and 'bar' are in the label set for 'while' and are thus
                    // continuable.
                    return this.labelIsOnContinuableConstruct(statement.statement);

                case 158 /* WhileStatement */:
                case 154 /* ForStatement */:
                case 155 /* ForInStatement */:
                case 161 /* DoStatement */:
                    return true;

                default:
                    return false;
            }
        };

        PullTypeResolver.prototype.resolveContinueStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckContinueStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.isIterationStatement = function (ast) {
            switch (ast.kind()) {
                case 154 /* ForStatement */:
                case 155 /* ForInStatement */:
                case 158 /* WhileStatement */:
                case 161 /* DoStatement */:
                    return true;
            }

            return false;
        };

        PullTypeResolver.prototype.isAnyFunctionExpressionOrDeclaration = function (ast) {
            switch (ast.kind()) {
                case 219 /* SimpleArrowFunctionExpression */:
                case 218 /* ParenthesizedArrowFunctionExpression */:
                case 222 /* FunctionExpression */:
                case 129 /* FunctionDeclaration */:
                case 135 /* MemberFunctionDeclaration */:
                case 241 /* FunctionPropertyAssignment */:
                case 137 /* ConstructorDeclaration */:
                case 139 /* GetAccessor */:
                case 140 /* SetAccessor */:
                    return true;
            }

            return false;
        };

        PullTypeResolver.prototype.inSwitchStatement = function (ast) {
            while (ast) {
                if (ast.kind() === 151 /* SwitchStatement */) {
                    return true;
                }

                if (this.isAnyFunctionExpressionOrDeclaration(ast)) {
                    return false;
                }

                ast = ast.parent;
            }

            return false;
        };

        PullTypeResolver.prototype.inIterationStatement = function (ast, crossFunctions) {
            while (ast) {
                if (this.isIterationStatement(ast)) {
                    return true;
                }

                if (!crossFunctions && this.isAnyFunctionExpressionOrDeclaration(ast)) {
                    return false;
                }

                ast = ast.parent;
            }

            return false;
        };

        PullTypeResolver.prototype.getEnclosingLabels = function (ast, breakable, crossFunctions) {
            var result = [];

            ast = ast.parent;
            while (ast) {
                if (ast.kind() === 160 /* LabeledStatement */) {
                    var labeledStatement = ast;
                    if (breakable) {
                        // Breakable labels can be placed on any construct
                        result.push(labeledStatement);
                    } else {
                        // They're asking for continuable labels.  Continuable labels must be on
                        // a loop construct.
                        if (this.labelIsOnContinuableConstruct(labeledStatement.statement)) {
                            result.push(labeledStatement);
                        }
                    }
                }

                if (!crossFunctions && this.isAnyFunctionExpressionOrDeclaration(ast)) {
                    break;
                }

                ast = ast.parent;
            }

            return result;
        };

        PullTypeResolver.prototype.typeCheckContinueStatement = function (ast, context) {
            this.setTypeChecked(ast, context);

            if (!this.inIterationStatement(ast, false)) {
                if (this.inIterationStatement(ast, true)) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Jump_target_cannot_cross_function_boundary));
                } else {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.continue_statement_can_only_be_used_within_an_enclosing_iteration_statement));
                }
            } else if (ast.identifier) {
                var continuableLabels = this.getEnclosingLabels(ast, false, false);

                if (!TypeScript.ArrayUtilities.any(continuableLabels, function (s) {
                    return s.identifier.valueText() === ast.identifier.valueText();
                })) {
                    // The target of the continue statement wasn't to a reachable label.
                    //
                    // Let hte user know, with a specialized message if the target was to an
                    // unreachable label (as opposed to a non-existed label)
                    var continuableLabels = this.getEnclosingLabels(ast, false, true);

                    if (TypeScript.ArrayUtilities.any(continuableLabels, function (s) {
                        return s.identifier.valueText() === ast.identifier.valueText();
                    })) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Jump_target_cannot_cross_function_boundary));
                    } else {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Jump_target_not_found));
                    }
                }
            }
        };

        PullTypeResolver.prototype.resolveBreakStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckBreakStatement(ast, context);
            }

            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.typeCheckBreakStatement = function (ast, context) {
            this.setTypeChecked(ast, context);

            // Note: the order here is important.  If the 'break' has a target, then it can jump to
            // any enclosing laballed statment.  If it has no target, it must be in an iteration or
            // swtich statement.
            if (ast.identifier) {
                var breakableLabels = this.getEnclosingLabels(ast, true, false);

                if (!TypeScript.ArrayUtilities.any(breakableLabels, function (s) {
                    return s.identifier.valueText() === ast.identifier.valueText();
                })) {
                    // The target of the continue statement wasn't to a reachable label.
                    //
                    // Let hte user know, with a specialized message if the target was to an
                    // unreachable label (as opposed to a non-existed label)
                    var breakableLabels = this.getEnclosingLabels(ast, true, true);
                    if (TypeScript.ArrayUtilities.any(breakableLabels, function (s) {
                        return s.identifier.valueText() === ast.identifier.valueText();
                    })) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Jump_target_cannot_cross_function_boundary));
                    } else {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Jump_target_not_found));
                    }
                }
            } else if (!this.inIterationStatement(ast, false) && !this.inSwitchStatement(ast)) {
                if (this.inIterationStatement(ast, true)) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Jump_target_cannot_cross_function_boundary));
                } else {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.break_statement_can_only_be_used_within_an_enclosing_iteration_or_switch_statement));
                }
            }
        };

        // Expression resolution
        PullTypeResolver.prototype.resolveAST = function (ast, isContextuallyTyped, context) {
            if (!ast) {
                return;
            }

            var symbol = this.getSymbolForAST(ast, context);
            if (symbol && symbol.isResolved) {
                this.typeCheckAST(ast, isContextuallyTyped, context);
                return symbol;
            }

            // We also have to check isTypesOnlyLocation because an identifier representing a type reference
            // is considered an expression at the AST level, yet we only want to resolve it as an expression
            // if it is in an expression position.
            if (ast.isExpression() && !isTypesOnlyLocation(ast)) {
                return this.resolveExpressionAST(ast, isContextuallyTyped, context);
            }

            var nodeType = ast.kind();

            switch (nodeType) {
                case 124 /* ArrayType */:
                case 126 /* GenericType */:
                case 122 /* ObjectType */:
                case 127 /* TypeQuery */:
                case 125 /* ConstructorType */:
                case 123 /* FunctionType */:
                    return this.resolveTypeReference(ast, context);

                case 1 /* List */:
                    return this.resolveList(ast, context);

                case 2 /* SeparatedList */:
                    return this.resolveSeparatedList(ast, context);

                case 120 /* SourceUnit */:
                    return this.resolveSourceUnit(ast, context);

                case 132 /* EnumDeclaration */:
                    return this.resolveEnumDeclaration(ast, context);

                case 130 /* ModuleDeclaration */:
                    return this.resolveModuleDeclaration(ast, context);

                case 128 /* InterfaceDeclaration */:
                    return this.resolveInterfaceDeclaration(ast, context);

                case 131 /* ClassDeclaration */:
                    return this.resolveClassDeclaration(ast, context);

                case 224 /* VariableDeclaration */:
                    return this.resolveVariableDeclarationList(ast, context);

                case 136 /* MemberVariableDeclaration */:
                    return this.resolveMemberVariableDeclaration(ast, context);

                case 225 /* VariableDeclarator */:
                    return this.resolveVariableDeclarator(ast, context);

                case 141 /* PropertySignature */:
                    return this.resolvePropertySignature(ast, context);

                case 227 /* ParameterList */:
                    return this.resolveParameterList(ast, context);

                case 242 /* Parameter */:
                    return this.resolveParameter(ast, context);

                case 243 /* EnumElement */:
                    return this.resolveEnumElement(ast, context);

                case 232 /* EqualsValueClause */:
                    return this.resolveEqualsValueClause(ast, isContextuallyTyped, context);

                case 238 /* TypeParameter */:
                    return this.resolveTypeParameterDeclaration(ast, context);

                case 239 /* Constraint */:
                    return this.resolveConstraint(ast, context);

                case 133 /* ImportDeclaration */:
                    return this.resolveImportDeclaration(ast, context);

                case 240 /* SimplePropertyAssignment */:
                    return this.resolveSimplePropertyAssignment(ast, isContextuallyTyped, context);

                case 241 /* FunctionPropertyAssignment */:
                    return this.resolveFunctionPropertyAssignment(ast, isContextuallyTyped, context);

                case 11 /* IdentifierName */:
                    TypeScript.Debug.assert(isTypesOnlyLocation(ast));
                    return this.resolveTypeNameExpression(ast, context);

                case 121 /* QualifiedName */:
                    return this.resolveQualifiedName(ast, context);

                case 137 /* ConstructorDeclaration */:
                    return this.resolveConstructorDeclaration(ast, context);

                case 139 /* GetAccessor */:
                case 140 /* SetAccessor */:
                    return this.resolveAccessorDeclaration(ast, context);

                case 138 /* IndexMemberDeclaration */:
                    return this.resolveIndexMemberDeclaration(ast, context);

                case 144 /* IndexSignature */:
                    return this.resolveIndexSignature(ast, context);

                case 135 /* MemberFunctionDeclaration */:
                    return this.resolveMemberFunctionDeclaration(ast, context);

                case 142 /* CallSignature */:
                    return this.resolveCallSignature(ast, context);

                case 143 /* ConstructSignature */:
                    return this.resolveConstructSignature(ast, context);

                case 145 /* MethodSignature */:
                    return this.resolveMethodSignature(ast, context);

                case 129 /* FunctionDeclaration */:
                    return this.resolveAnyFunctionDeclaration(ast, context);

                case 244 /* TypeAnnotation */:
                    return this.resolveTypeAnnotation(ast, context);

                case 134 /* ExportAssignment */:
                    return this.resolveExportAssignmentStatement(ast, context);

                case 157 /* ThrowStatement */:
                    return this.resolveThrowStatement(ast, context);

                case 149 /* ExpressionStatement */:
                    return this.resolveExpressionStatement(ast, context);

                case 154 /* ForStatement */:
                    return this.resolveForStatement(ast, context);

                case 155 /* ForInStatement */:
                    return this.resolveForInStatement(ast, context);

                case 158 /* WhileStatement */:
                    return this.resolveWhileStatement(ast, context);

                case 161 /* DoStatement */:
                    return this.resolveDoStatement(ast, context);

                case 147 /* IfStatement */:
                    return this.resolveIfStatement(ast, context);

                case 235 /* ElseClause */:
                    return this.resolveElseClause(ast, context);

                case 146 /* Block */:
                    return this.resolveBlock(ast, context);

                case 148 /* VariableStatement */:
                    return this.resolveVariableStatement(ast, context);

                case 163 /* WithStatement */:
                    return this.resolveWithStatement(ast, context);

                case 159 /* TryStatement */:
                    return this.resolveTryStatement(ast, context);

                case 236 /* CatchClause */:
                    return this.resolveCatchClause(ast, context);

                case 237 /* FinallyClause */:
                    return this.resolveFinallyClause(ast, context);

                case 150 /* ReturnStatement */:
                    return this.resolveReturnStatement(ast, context);

                case 151 /* SwitchStatement */:
                    return this.resolveSwitchStatement(ast, context);

                case 153 /* ContinueStatement */:
                    return this.resolveContinueStatement(ast, context);

                case 152 /* BreakStatement */:
                    return this.resolveBreakStatement(ast, context);

                case 160 /* LabeledStatement */:
                    return this.resolveLabeledStatement(ast, context);
            }

            return this.semanticInfoChain.anyTypeSymbol;
        };

        PullTypeResolver.prototype.resolveExpressionAST = function (ast, isContextuallyOrInferentiallyTyped, context) {
            var expressionSymbol = this.resolveExpressionWorker(ast, isContextuallyOrInferentiallyTyped, context);

            // November 18, 2013, Section 4.12.2:
            // If e is an expression of a function type that contains exactly one generic call signature
            // and no other members, and T is a function type with exactly one non-generic call signature
            // and no other members, then any inferences made for type parameters referenced by the
            // parameters of T's call signature are fixed and, if e's call signature can successfully be
            // instantiated in the context of T's call signature(section 3.8.5), e's type is changed to
            // a function type with that instantiated signature.
            if (isContextuallyOrInferentiallyTyped && context.isInferentiallyTyping()) {
                return this.alterPotentialGenericFunctionTypeToInstantiatedFunctionTypeForTypeArgumentInference(expressionSymbol, context);
            } else {
                return expressionSymbol;
            }
        };

        PullTypeResolver.prototype.resolveExpressionWorker = function (ast, isContextuallyTyped, context) {
            switch (ast.kind()) {
                case 215 /* ObjectLiteralExpression */:
                    return this.resolveObjectLiteralExpression(ast, isContextuallyTyped, context);

                case 11 /* IdentifierName */:
                    // We already know we are in an expression, so there is no need
                    // to decide between resolveNameExpression and resolveTypeNameExpression
                    return this.resolveNameExpression(ast, context);

                case 212 /* MemberAccessExpression */:
                    return this.resolveMemberAccessExpression(ast, context);

                case 222 /* FunctionExpression */:
                    return this.resolveFunctionExpression(ast, isContextuallyTyped, context);

                case 219 /* SimpleArrowFunctionExpression */:
                    return this.resolveSimpleArrowFunctionExpression(ast, isContextuallyTyped, context);

                case 218 /* ParenthesizedArrowFunctionExpression */:
                    return this.resolveParenthesizedArrowFunctionExpression(ast, isContextuallyTyped, context);

                case 214 /* ArrayLiteralExpression */:
                    return this.resolveArrayLiteralExpression(ast, isContextuallyTyped, context);

                case 35 /* ThisKeyword */:
                    return this.resolveThisExpression(ast, context);

                case 50 /* SuperKeyword */:
                    return this.resolveSuperExpression(ast, context);

                case 213 /* InvocationExpression */:
                    return this.resolveInvocationExpression(ast, context);

                case 216 /* ObjectCreationExpression */:
                    return this.resolveObjectCreationExpression(ast, context);

                case 220 /* CastExpression */:
                    return this.resolveCastExpression(ast, context);

                case 13 /* NumericLiteral */:
                    return this.semanticInfoChain.numberTypeSymbol;

                case 14 /* StringLiteral */:
                    return this.semanticInfoChain.stringTypeSymbol;

                case 32 /* NullKeyword */:
                    return this.semanticInfoChain.nullTypeSymbol;

                case 37 /* TrueKeyword */:
                case 24 /* FalseKeyword */:
                    return this.semanticInfoChain.booleanTypeSymbol;

                case 172 /* VoidExpression */:
                    return this.resolveVoidExpression(ast, context);

                case 174 /* AssignmentExpression */:
                    return this.resolveAssignmentExpression(ast, context);

                case 167 /* LogicalNotExpression */:
                    return this.resolveLogicalNotExpression(ast, context);

                case 193 /* NotEqualsWithTypeConversionExpression */:
                case 192 /* EqualsWithTypeConversionExpression */:
                case 194 /* EqualsExpression */:
                case 195 /* NotEqualsExpression */:
                case 196 /* LessThanExpression */:
                case 198 /* LessThanOrEqualExpression */:
                case 199 /* GreaterThanOrEqualExpression */:
                case 197 /* GreaterThanExpression */:
                    return this.resolveLogicalOperation(ast, context);

                case 208 /* AddExpression */:
                case 175 /* AddAssignmentExpression */:
                    return this.resolveBinaryAdditionOperation(ast, context);

                case 164 /* PlusExpression */:
                case 165 /* NegateExpression */:
                case 166 /* BitwiseNotExpression */:
                case 168 /* PreIncrementExpression */:
                case 169 /* PreDecrementExpression */:
                    return this.resolveUnaryArithmeticOperation(ast, context);

                case 210 /* PostIncrementExpression */:
                case 211 /* PostDecrementExpression */:
                    return this.resolvePostfixUnaryExpression(ast, context);

                case 209 /* SubtractExpression */:
                case 205 /* MultiplyExpression */:
                case 206 /* DivideExpression */:
                case 207 /* ModuloExpression */:
                case 189 /* BitwiseOrExpression */:
                case 191 /* BitwiseAndExpression */:
                case 202 /* LeftShiftExpression */:
                case 203 /* SignedRightShiftExpression */:
                case 204 /* UnsignedRightShiftExpression */:
                case 190 /* BitwiseExclusiveOrExpression */:
                case 181 /* ExclusiveOrAssignmentExpression */:
                case 183 /* LeftShiftAssignmentExpression */:
                case 184 /* SignedRightShiftAssignmentExpression */:
                case 185 /* UnsignedRightShiftAssignmentExpression */:
                case 176 /* SubtractAssignmentExpression */:
                case 177 /* MultiplyAssignmentExpression */:
                case 178 /* DivideAssignmentExpression */:
                case 179 /* ModuloAssignmentExpression */:
                case 182 /* OrAssignmentExpression */:
                case 180 /* AndAssignmentExpression */:
                    return this.resolveBinaryArithmeticExpression(ast, context);

                case 221 /* ElementAccessExpression */:
                    return this.resolveElementAccessExpression(ast, context);

                case 187 /* LogicalOrExpression */:
                    return this.resolveLogicalOrExpression(ast, isContextuallyTyped, context);

                case 188 /* LogicalAndExpression */:
                    return this.resolveLogicalAndExpression(ast, context);

                case 171 /* TypeOfExpression */:
                    return this.resolveTypeOfExpression(ast, context);

                case 170 /* DeleteExpression */:
                    return this.resolveDeleteExpression(ast, context);

                case 186 /* ConditionalExpression */:
                    return this.resolveConditionalExpression(ast, isContextuallyTyped, context);

                case 12 /* RegularExpressionLiteral */:
                    return this.resolveRegularExpressionLiteral();

                case 217 /* ParenthesizedExpression */:
                    return this.resolveParenthesizedExpression(ast, context);

                case 200 /* InstanceOfExpression */:
                    return this.resolveInstanceOfExpression(ast, context);

                case 173 /* CommaExpression */:
                    return this.resolveCommaExpression(ast, context);

                case 201 /* InExpression */:
                    return this.resolveInExpression(ast, context);

                case 223 /* OmittedExpression */:
                    return this.semanticInfoChain.undefinedTypeSymbol;
            }

            TypeScript.Debug.fail("resolveExpressionASTWorker: Missing expression kind: " + TypeScript.SyntaxKind[ast.kind()]);
        };

        PullTypeResolver.prototype.typeCheckAST = function (ast, isContextuallyTyped, context) {
            if (!this.canTypeCheckAST(ast, context)) {
                return;
            }

            var nodeType = ast.kind();
            switch (nodeType) {
                case 120 /* SourceUnit */:
                    this.typeCheckSourceUnit(ast, context);
                    return;

                case 132 /* EnumDeclaration */:
                    this.typeCheckEnumDeclaration(ast, context);
                    return;

                case 130 /* ModuleDeclaration */:
                    this.typeCheckModuleDeclaration(ast, context);
                    return;

                case 128 /* InterfaceDeclaration */:
                    this.typeCheckInterfaceDeclaration(ast, context);
                    return;

                case 131 /* ClassDeclaration */:
                    this.typeCheckClassDeclaration(ast, context);
                    return;

                case 243 /* EnumElement */:
                    this.typeCheckEnumElement(ast, context);
                    return;

                case 136 /* MemberVariableDeclaration */:
                    this.typeCheckMemberVariableDeclaration(ast, context);
                    return;

                case 225 /* VariableDeclarator */:
                    this.typeCheckVariableDeclarator(ast, context);
                    return;

                case 141 /* PropertySignature */:
                    this.typeCheckPropertySignature(ast, context);
                    return;

                case 242 /* Parameter */:
                    this.typeCheckParameter(ast, context);
                    return;

                case 133 /* ImportDeclaration */:
                    this.typeCheckImportDeclaration(ast, context);
                    return;

                case 215 /* ObjectLiteralExpression */:
                    this.resolveObjectLiteralExpression(ast, isContextuallyTyped, context);
                    return;

                case 241 /* FunctionPropertyAssignment */:
                    this.typeCheckFunctionPropertyAssignment(ast, isContextuallyTyped, context);
                    return;

                case 11 /* IdentifierName */:
                    if (isTypesOnlyLocation(ast)) {
                        this.resolveTypeNameExpression(ast, context);
                    } else {
                        this.resolveNameExpression(ast, context);
                    }
                    return;

                case 212 /* MemberAccessExpression */:
                    this.resolveMemberAccessExpression(ast, context);
                    return;

                case 121 /* QualifiedName */:
                    this.resolveQualifiedName(ast, context);
                    return;

                case 222 /* FunctionExpression */:
                    this.typeCheckFunctionExpression(ast, isContextuallyTyped, context);
                    return;

                case 137 /* ConstructorDeclaration */:
                    this.typeCheckConstructorDeclaration(ast, context);
                    return;

                case 139 /* GetAccessor */:
                case 140 /* SetAccessor */:
                    this.typeCheckAccessorDeclaration(ast, context);
                    return;

                case 135 /* MemberFunctionDeclaration */:
                    this.typeCheckMemberFunctionDeclaration(ast, context);
                    return;

                case 145 /* MethodSignature */:
                    this.typeCheckMethodSignature(ast, context);
                    return;

                case 144 /* IndexSignature */:
                    this.typeCheckIndexSignature(ast, context);
                    break;

                case 142 /* CallSignature */:
                    this.typeCheckCallSignature(ast, context);
                    return;

                case 143 /* ConstructSignature */:
                    this.typeCheckConstructSignature(ast, context);
                    return;

                case 129 /* FunctionDeclaration */: {
                    var funcDecl = ast;
                    this.typeCheckAnyFunctionDeclaration(funcDecl, TypeScript.hasModifier(funcDecl.modifiers, TypeScript.PullElementFlags.Static), funcDecl.identifier, funcDecl.callSignature.typeParameterList, funcDecl.callSignature.parameterList, TypeScript.ASTHelpers.getType(funcDecl), funcDecl.block, context);
                    return;
                }

                case 219 /* SimpleArrowFunctionExpression */:
                    this.typeCheckSimpleArrowFunctionExpression(ast, isContextuallyTyped, context);
                    return;

                case 218 /* ParenthesizedArrowFunctionExpression */:
                    this.typeCheckParenthesizedArrowFunctionExpression(ast, isContextuallyTyped, context);
                    return;

                case 214 /* ArrayLiteralExpression */:
                    this.resolveArrayLiteralExpression(ast, isContextuallyTyped, context);
                    return;

                case 213 /* InvocationExpression */:
                    this.typeCheckInvocationExpression(ast, context);
                    return;

                case 216 /* ObjectCreationExpression */:
                    this.typeCheckObjectCreationExpression(ast, context);
                    return;

                case 150 /* ReturnStatement */:
                    // Since we want to resolve the return expression to traverse parents, resolve will take care of typeChecking
                    this.resolveReturnStatement(ast, context);
                    return;

                default:
                    TypeScript.Debug.assert(false, "Failure nodeType: " + TypeScript.SyntaxKind[ast.kind()] + ". Implement typeCheck when symbol is set for the ast as part of resolution.");
            }
        };

        PullTypeResolver.prototype.processPostTypeCheckWorkItems = function (context) {
            while (this.postTypeCheckWorkitems.length) {
                var ast = this.postTypeCheckWorkitems.pop();
                this.postTypeCheck(ast, context);
            }
        };

        PullTypeResolver.prototype.postTypeCheck = function (ast, context) {
            var nodeType = ast.kind();

            switch (nodeType) {
                case 242 /* Parameter */:
                case 225 /* VariableDeclarator */:
                    this.postTypeCheckVariableDeclaratorOrParameter(ast, context);
                    return;

                case 131 /* ClassDeclaration */:
                    this.postTypeCheckClassDeclaration(ast, context);
                    return;

                case 129 /* FunctionDeclaration */:
                    this.postTypeCheckFunctionDeclaration(ast, context);
                    return;

                case 130 /* ModuleDeclaration */:
                    this.postTypeCheckModuleDeclaration(ast, context);
                    return;

                case 132 /* EnumDeclaration */:
                    this.postTypeCheckEnumDeclaration(ast, context);
                    return;

                case 133 /* ImportDeclaration */:
                    this.postTypeCheckImportDeclaration(ast, context);
                    return;

                case 11 /* IdentifierName */:
                    this.postTypeCheckNameExpression(ast, context);
                    return;

                default:
                    TypeScript.Debug.assert(false, "Implement postTypeCheck clause to handle the postTypeCheck work, nodeType: " + TypeScript.SyntaxKind[ast.kind()]);
            }
        };

        PullTypeResolver.prototype.resolveRegularExpressionLiteral = function () {
            if (this.cachedRegExpInterfaceType()) {
                return this.cachedRegExpInterfaceType();
            } else {
                return this.semanticInfoChain.anyTypeSymbol;
            }
        };

        PullTypeResolver.prototype.postTypeCheckNameExpression = function (nameAST, context) {
            this.checkThisCaptureVariableCollides(nameAST, false, context);
        };

        PullTypeResolver.prototype.typeCheckNameExpression = function (nameAST, context) {
            this.setTypeChecked(nameAST, context);
            this.checkNameForCompilerGeneratedDeclarationCollision(nameAST, false, nameAST, context);
        };

        PullTypeResolver.prototype.resolveNameExpression = function (nameAST, context) {
            var nameSymbol = this.getSymbolForAST(nameAST, context);
            var foundCached = nameSymbol !== null;

            if (!foundCached || this.canTypeCheckAST(nameAST, context)) {
                if (this.canTypeCheckAST(nameAST, context)) {
                    this.typeCheckNameExpression(nameAST, context);
                }
                nameSymbol = this.computeNameExpression(nameAST, context);
            }

            this.resolveDeclaredSymbol(nameSymbol, context);

            // We don't want to capture an intermediate 'any' from a recursive resolution
            if (nameSymbol && (nameSymbol.type !== this.semanticInfoChain.anyTypeSymbol || nameSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.IsAnnotatedWithAny | 1 /* Exported */))) {
                this.setSymbolForAST(nameAST, nameSymbol, context);
            }

            return nameSymbol;
        };

        PullTypeResolver.prototype.isInEnumDecl = function (decl) {
            if (decl.kind & TypeScript.PullElementKind.Enum) {
                return true;
            }

            var declPath = decl.getParentPath();

            // enum cannot contain these kinds of declarations, if one is met - exit with false
            var disallowedKinds = TypeScript.PullElementKind.SomeContainer | TypeScript.PullElementKind.SomeType;
            for (var i = declPath.length - 1; i >= 0; i--) {
                var decl = declPath[i];

                if (decl.kind & TypeScript.PullElementKind.Enum) {
                    return true;
                }

                if (decl.kind & disallowedKinds) {
                    return false;
                }
            }
            return false;
        };

        PullTypeResolver.prototype.getSomeInnermostFunctionScopeDecl = function (declPath) {
            for (var i = declPath.length - 1; i >= 0; i--) {
                var decl = declPath[i];
                if (decl.kind & TypeScript.PullElementKind.SomeFunction) {
                    return decl;
                }
            }

            return null;
        };

        PullTypeResolver.prototype.isFromFunctionScope = function (nameSymbol, functionScopeDecl) {
            var _this = this;
            // If there exists any declaration that is from same functionScopeDecl, nameSymbol is from functionScope
            return TypeScript.ArrayUtilities.any(nameSymbol.getDeclarations(), function (nameSymbolDecl) {
                return _this.getSomeInnermostFunctionScopeDecl(nameSymbolDecl.getParentPath()) === functionScopeDecl;
            });
        };

        PullTypeResolver.prototype.findConstructorDeclOfEnclosingType = function (decl) {
            var current = decl;
            while (current) {
                if (TypeScript.hasFlag(current.kind, TypeScript.PullElementKind.Property)) {
                    var parentDecl = current.getParentDecl();
                    if (TypeScript.hasFlag(parentDecl.kind, TypeScript.PullElementKind.Class)) {
                        return TypeScript.ArrayUtilities.lastOrDefault(parentDecl.getChildDecls(), function (decl) {
                            return TypeScript.hasFlag(decl.kind, TypeScript.PullElementKind.ConstructorMethod);
                        });
                    }
                }

                if (TypeScript.hasFlag(current.kind, TypeScript.PullElementKind.SomeContainer)) {
                    return null;
                }

                current = current.getParentDecl();
            }
            return null;
        };

        PullTypeResolver.prototype.checkNameAsPartOfInitializerExpressionForInstanceMemberVariable = function (nameAST, nameSymbol, context) {
            var id = nameAST.valueText();
            if (id.length === 0) {
                return false;
            }

            // SPEC: Nov 18, 2013
            // Initializer expressions for instance member variables are evaluated in the scope of the class constructor body
            // but are not permitted to reference parameters or local variables of the constructor.
            // This effectively means that entities from outer scopes by the same name as a constructor parameter or local variable are inaccessible
            // in initializer expressions for instance member variables.
            var memberVariableDeclarationAST = TypeScript.ASTHelpers.getEnclosingMemberVariableDeclaration(nameAST);
            if (memberVariableDeclarationAST) {
                var memberVariableDecl = this.semanticInfoChain.getDeclForAST(memberVariableDeclarationAST);
                if (!TypeScript.hasFlag(memberVariableDecl.flags, TypeScript.PullElementFlags.Static)) {
                    var constructorDecl = this.findConstructorDeclOfEnclosingType(memberVariableDecl);

                    if (constructorDecl) {
                        var childDecls = constructorDecl.searchChildDecls(id, TypeScript.PullElementKind.SomeValue);
                        if (childDecls.length) {
                            // Check if symbol is from scope that is outer to constructor's outer scope
                            if (TypeScript.PullHelpers.isSymbolDeclaredInScopeChain(nameSymbol, constructorDecl.getSymbol().getContainer())) {
                                var memberVariableSymbol = memberVariableDecl.getSymbol();

                                // Currently name was resolved to something from outerscope of the class
                                // But constructor contains same parameter name, and hence this member initializer
                                // when moved into constructor function in generated js would resolve to the constructor
                                // parameter but thats not what user intended. Report error
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(nameAST, TypeScript.DiagnosticCode.Initializer_of_instance_member_variable_0_cannot_reference_identifier_1_declared_in_the_constructor, [memberVariableSymbol.getScopedName(constructorDecl.getSymbol()), nameAST.text()]));
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        };

        PullTypeResolver.prototype.computeNameExpression = function (nameAST, context) {
            var id = nameAST.valueText();
            if (id.length === 0) {
                return this.semanticInfoChain.anyTypeSymbol;
            }

            var nameSymbol = null;
            var enclosingDecl = this.getEnclosingDeclForAST(nameAST);

            // if enclosing decl is property check if it has valueDecl that corresponds to parameter
            if (TypeScript.hasFlag(enclosingDecl.flags, TypeScript.PullElementFlags.PropertyParameter)) {
                var valueDecl = enclosingDecl.getValueDecl();
                if (valueDecl && TypeScript.hasFlag(valueDecl.kind, TypeScript.PullElementKind.Parameter)) {
                    enclosingDecl = valueDecl;
                }
            }

            // First check if this is the name child of a declaration. If it is, no need to search for a name in scope since this is not a reference.
            var isDeclarationASTOrDeclarationNameAST = TypeScript.ASTHelpers.isDeclarationASTOrDeclarationNameAST(nameAST);
            if (isDeclarationASTOrDeclarationNameAST) {
                nameSymbol = this.semanticInfoChain.getDeclForAST(nameAST.parent).getSymbol();
            }

            var declPath = enclosingDecl.getParentPath();

            if (!nameSymbol) {
                var searchKind = TypeScript.PullElementKind.SomeValue;

                // disallow accessing enum members with no qualification except the case when enclosing decl is enum
                if (!this.isInEnumDecl(enclosingDecl)) {
                    searchKind = searchKind & ~(TypeScript.PullElementKind.EnumMember);
                }

                var nameSymbol = this.getSymbolFromDeclPath(id, declPath, searchKind);
            }

            // arguments is resolved to local variable in the function or the arguments list that runtime passes and never to any other symbol
            if (id === "arguments") {
                var functionScopeDecl = this.getSomeInnermostFunctionScopeDecl(declPath);
                if (functionScopeDecl) {
                    // if there wasnt nameSymbol Or the nameSymbol isnt from the functionScope we are looking in
                    // resolve this to arguments of type IArguments
                    if (!nameSymbol || !this.isFromFunctionScope(nameSymbol, functionScopeDecl)) {
                        nameSymbol = this.cachedFunctionArgumentsSymbol();
                        this.resolveDeclaredSymbol(this.cachedIArgumentsInterfaceType(), context);
                    }
                }
            }

            var aliasSymbol = null;
            if (nameSymbol && nameSymbol.isAlias() && !isDeclarationASTOrDeclarationNameAST) {
                aliasSymbol = nameSymbol;
                if (!this.inTypeQuery(nameAST)) {
                    aliasSymbol.setIsUsedAsValue();
                }

                this.resolveDeclaredSymbol(nameSymbol, context);

                this.resolveDeclaredSymbol(aliasSymbol.assignedValue(), context);
                this.resolveDeclaredSymbol(aliasSymbol.assignedContainer(), context);

                nameSymbol = aliasSymbol.getExportAssignedValueSymbol();
                if (!nameSymbol) {
                    aliasSymbol = null;
                }
            }

            if (!nameSymbol) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(nameAST, TypeScript.DiagnosticCode.Could_not_find_symbol_0, [nameAST.text()]));
                return this.getNewErrorTypeSymbol(id);
            } else if (this.checkNameAsPartOfInitializerExpressionForInstanceMemberVariable(nameAST, nameSymbol, context)) {
                return this.getNewErrorTypeSymbol(id);
            }

            // October 11, 2013:
            // Initializer expressions are evaluated in the scope of the function body but are not
            // permitted to reference local variables and are only permitted to access parameters
            // that are declared to the left of the parameter they initialize.
            // If we've referenced a parameter of a function, make sure that we're either inside
            // the function, or if we're in a parameter initializer, that the parameter
            // initializer is to the left of this reference.
            var nameDeclaration = nameSymbol.getDeclarations()[0];
            var nameParentDecl = nameDeclaration.getParentDecl();
            if (nameParentDecl && (nameParentDecl.kind & TypeScript.PullElementKind.SomeFunction) && (nameParentDecl.flags & TypeScript.PullElementFlags.HasDefaultArgs)) {
                // Get the AST and look it up in the parameter index context to find which parameter we are in
                var enclosingFunctionAST = this.semanticInfoChain.getASTForDecl(nameParentDecl);
                var currentParameterIndex = this.getCurrentParameterIndexForFunction(nameAST, enclosingFunctionAST);

                var parameterList = TypeScript.ASTHelpers.getParameterList(enclosingFunctionAST);

                // Short circuit if we are located in the function body, since all child decls of the function are accessible there
                if (currentParameterIndex >= 0) {
                    // Search the enclosing function AST for a parameter with the right name, but stop once we hit our current context
                    var matchingParameter;
                    if (parameterList) {
                        for (var i = 0; i <= currentParameterIndex; i++) {
                            var candidateParameter = parameterList.parameters.nonSeparatorAt(i);
                            if (candidateParameter && candidateParameter.identifier.valueText() === id) {
                                matchingParameter = candidateParameter;
                                break;
                            }
                        }
                    }

                    if (!matchingParameter) {
                        // We didn't find a matching parameter to the left, so error
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(nameAST, TypeScript.DiagnosticCode.Initializer_of_parameter_0_cannot_reference_identifier_1_declared_after_it, [parameterList.parameters.nonSeparatorAt(currentParameterIndex).identifier.text(), nameAST.text()]));
                        return this.getNewErrorTypeSymbol(id);
                    } else if (matchingParameter === TypeScript.ASTHelpers.getEnclosingParameterForInitializer(nameAST)) {
                        // we've found matching parameter but it references itself from the initializer
                        // per spec Nov 18:
                        // Initializer expressions ..and are only permitted to access parameters that are declared to the left of the parameter they initialize
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(nameAST, TypeScript.DiagnosticCode.Parameter_0_cannot_be_referenced_in_its_initializer, [parameterList.parameters.nonSeparatorAt(currentParameterIndex).identifier.text()]));
                        return this.getNewErrorTypeSymbol(id);
                    }
                }
            }

            if (aliasSymbol) {
                this.semanticInfoChain.setAliasSymbolForAST(nameAST, aliasSymbol);
            }

            return nameSymbol;
        };

        // Returns the parameter index in the specified function declaration where ast is contained
        // within.  Returns -1 if ast is not contained within a parameter initializer in the
        // provided function declaration.
        PullTypeResolver.prototype.getCurrentParameterIndexForFunction = function (parameter, funcDecl) {
            var parameterList = TypeScript.ASTHelpers.getParameterList(funcDecl);
            if (parameterList) {
                while (parameter && parameter.parent) {
                    if (parameter.parent.parent === parameterList) {
                        // We were contained in the parameter list.  Return which parameter index
                        // we were at.
                        return parameterList.parameters.nonSeparatorIndexOf(parameter);
                    }

                    parameter = parameter.parent;
                }
            }

            // ast was not found within the parameter list of this function.
            return -1;
        };

        PullTypeResolver.prototype.resolveMemberAccessExpression = function (dottedNameAST, context) {
            return this.resolveDottedNameExpression(dottedNameAST, dottedNameAST.expression, dottedNameAST.name, context);
        };

        PullTypeResolver.prototype.resolveDottedNameExpression = function (dottedNameAST, expression, name, context) {
            var symbol = this.getSymbolForAST(dottedNameAST, context);
            var foundCached = symbol !== null;

            if (!foundCached || this.canTypeCheckAST(dottedNameAST, context)) {
                var canTypeCheckDottedNameAST = this.canTypeCheckAST(dottedNameAST, context);
                if (canTypeCheckDottedNameAST) {
                    this.setTypeChecked(dottedNameAST, context);
                }

                symbol = this.computeDottedNameExpression(expression, name, context, canTypeCheckDottedNameAST);
            }

            this.resolveDeclaredSymbol(symbol, context);

            if (symbol && (symbol.type !== this.semanticInfoChain.anyTypeSymbol || symbol.anyDeclHasFlag(TypeScript.PullElementFlags.IsAnnotatedWithAny | 1 /* Exported */))) {
                this.setSymbolForAST(dottedNameAST, symbol, context);
                this.setSymbolForAST(name, symbol, context);
            }

            return symbol;
        };

        PullTypeResolver.prototype.computeDottedNameExpression = function (expression, name, context, checkSuperPrivateAndStaticAccess) {
            var rhsName = name.valueText();
            if (rhsName.length === 0) {
                return this.semanticInfoChain.anyTypeSymbol;
            }

            // assemble the dotted name path
            var lhs = this.resolveAST(expression, false, context);
            return this.computeDottedNameExpressionFromLHS(lhs, expression, name, context, checkSuperPrivateAndStaticAccess);
        };

        PullTypeResolver.prototype.computeDottedNameExpressionFromLHS = function (lhs, expression, name, context, checkSuperPrivateAndStaticAccess) {
            // This can be case if incomplete qualified name
            var rhsName = name.valueText();
            if (rhsName.length === 0) {
                return this.semanticInfoChain.anyTypeSymbol;
            }

            var lhsType = lhs.type;

            if (lhs.isAlias()) {
                var lhsAlias = lhs;
                if (!this.inTypeQuery(expression)) {
                    lhsAlias.setIsUsedAsValue();
                }
                lhsType = lhsAlias.getExportAssignedTypeSymbol();
            }

            // this can happen if a var is set to a type alias and the var is used in a dotted name.
            // i.e.
            //    import myAlias = require('someModule');
            //    var myAliasVar = myAlias
            //    myAliasVar.someModulesMember();
            // without this we would not be able to resolve someModules Members
            if (lhsType.isAlias()) {
                lhsType = lhsType.getExportAssignedTypeSymbol();
            }

            if (!lhsType) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(name, TypeScript.DiagnosticCode.Could_not_find_enclosing_symbol_for_dotted_name_0, [name.text()]));
                return this.getNewErrorTypeSymbol();
            }

            if (!lhsType.isResolved) {
                var potentiallySpecializedType = this.resolveDeclaredSymbol(lhsType, context);

                if (potentiallySpecializedType !== lhsType) {
                    if (!lhs.isType()) {
                        context.setTypeInContext(lhs, potentiallySpecializedType);
                    }

                    lhsType = potentiallySpecializedType;
                }
            }

            if (lhsType.isContainer() && !lhsType.isAlias() && !lhsType.isEnum()) {
                // we're searching in the value space, so we should try to use the
                // instance value type
                var instanceSymbol = lhsType.getInstanceSymbol();

                if (instanceSymbol) {
                    lhsType = instanceSymbol.type;
                }
            }

            var originalLhsTypeForErrorReporting = lhsType;

            lhsType = this.getApparentType(lhsType).widenedType(this, expression, context);

            if (this.isAnyOrEquivalent(lhsType)) {
                return lhsType;
            }

            // November 18, 2013, Section 4.10:
            // If Name denotes a property member in the apparent type of ObjExpr, the property access
            // is of the type of that property.
            // Note that here we are assuming the apparent type is already accounted for up to
            // augmentation. So we just check the augmented type.
            var nameSymbol = this._getNamedPropertySymbolOfAugmentedType(rhsName, lhsType);

            if (!nameSymbol) {
                // could be a function symbol
                if (lhsType.kind === TypeScript.PullElementKind.DynamicModule) {
                    var container = lhsType;
                    var associatedInstance = container.getInstanceSymbol();

                    if (associatedInstance) {
                        var instanceType = associatedInstance.type;

                        nameSymbol = this.getNamedPropertySymbol(rhsName, TypeScript.PullElementKind.SomeValue, instanceType);
                    }
                } else {
                    var associatedType = lhsType.getAssociatedContainerType();

                    if (associatedType && !associatedType.isClass()) {
                        nameSymbol = this.getNamedPropertySymbol(rhsName, TypeScript.PullElementKind.SomeValue, associatedType);
                    }
                }

                if (!nameSymbol) {
                    var enclosingDecl = this.getEnclosingDeclForAST(expression);
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(name, TypeScript.DiagnosticCode.The_property_0_does_not_exist_on_value_of_type_1, [name.text(), originalLhsTypeForErrorReporting.toString(enclosingDecl ? enclosingDecl.getSymbol() : null)]));
                    return this.getNewErrorTypeSymbol(rhsName);
                }
            }

            if (checkSuperPrivateAndStaticAccess) {
                this.checkForSuperMemberAccess(expression, name, nameSymbol, context) || this.checkForPrivateMemberAccess(name, lhsType, nameSymbol, context);
            }

            return nameSymbol;
        };

        PullTypeResolver.prototype.resolveTypeNameExpression = function (nameAST, context) {
            var typeNameSymbol = this.getSymbolForAST(nameAST, context);

            // TODO(cyrusn): We really shouldn't be checking "isType" here.  However, we currently
            // have a bug where some part of the system calls resolveNameExpression on this node
            // and we cache the wrong thing.  We need to add appropriate checks to ensure that
            // resolveNameExpression is never called on a node that we should be calling
            // resolveTypeNameExpression (and vice versa).
            if (!typeNameSymbol || !typeNameSymbol.isType() || this.canTypeCheckAST(nameAST, context)) {
                if (this.canTypeCheckAST(nameAST, context)) {
                    this.setTypeChecked(nameAST, context);
                }
                typeNameSymbol = this.computeTypeNameExpression(nameAST, context);
                this.setSymbolForAST(nameAST, typeNameSymbol, context);
            }

            this.resolveDeclaredSymbol(typeNameSymbol, context);

            return typeNameSymbol;
        };

        PullTypeResolver.prototype.computeTypeNameExpression = function (nameAST, context) {
            var id = nameAST.valueText();
            if (id.length === 0) {
                return this.semanticInfoChain.anyTypeSymbol;
            }

            var enclosingDecl = this.getEnclosingDeclForAST(nameAST);

            var declPath = enclosingDecl.getParentPath();

            // If we're resolving a dotted type name, every dotted name but the last will be a container type, so we'll search those
            // first if need be, and then fall back to type names.  Otherwise, look for a type first, since we are probably looking for
            // a type reference (the exception being an alias or export assignment)
            var onLeftOfDot = this.isLeftSideOfQualifiedName(nameAST);

            var kindToCheckFirst = onLeftOfDot ? TypeScript.PullElementKind.SomeContainer : TypeScript.PullElementKind.SomeType;
            var kindToCheckSecond = onLeftOfDot ? TypeScript.PullElementKind.SomeType : TypeScript.PullElementKind.SomeContainer;

            var typeNameSymbol = this.getSymbolFromDeclPath(id, declPath, kindToCheckFirst);

            if (!typeNameSymbol) {
                typeNameSymbol = this.getSymbolFromDeclPath(id, declPath, kindToCheckSecond);
            }

            if (!typeNameSymbol) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(nameAST, TypeScript.DiagnosticCode.Could_not_find_symbol_0, [nameAST.text()]));
                return this.getNewErrorTypeSymbol(id);
            }

            var typeNameSymbolAlias = null;
            if (typeNameSymbol.isAlias()) {
                typeNameSymbolAlias = typeNameSymbol;
                this.resolveDeclaredSymbol(typeNameSymbol, context);

                var aliasedType = typeNameSymbolAlias.getExportAssignedTypeSymbol();

                this.resolveDeclaredSymbol(aliasedType, context);
            }

            if (typeNameSymbol.isTypeParameter()) {
                if (this.isInStaticMemberContext(enclosingDecl)) {
                    var parentDecl = typeNameSymbol.getDeclarations()[0].getParentDecl();

                    if (parentDecl.kind === TypeScript.PullElementKind.Class) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(nameAST, TypeScript.DiagnosticCode.Static_members_cannot_reference_class_type_parameters));
                        return this.getNewErrorTypeSymbol();
                    }
                }
            }

            if (!typeNameSymbol.isGeneric() && (typeNameSymbol.isClass() || typeNameSymbol.isInterface())) {
                typeNameSymbol = TypeScript.PullTypeReferenceSymbol.createTypeReference(typeNameSymbol);
            }

            return typeNameSymbol;
        };

        PullTypeResolver.prototype.isInStaticMemberContext = function (decl) {
            while (decl) {
                // check if decl correspond to static member of some sort
                if (TypeScript.hasFlag(decl.kind, TypeScript.PullElementKind.SomeFunction | TypeScript.PullElementKind.Property) && TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.Static)) {
                    return true;
                }

                // no container can exist in static context - exit early
                if (TypeScript.hasFlag(decl.kind, TypeScript.PullElementKind.SomeContainer)) {
                    return false;
                }

                decl = decl.getParentDecl();
            }

            return false;
        };

        PullTypeResolver.prototype.isLeftSideOfQualifiedName = function (ast) {
            return ast && ast.parent && ast.parent.kind() === 121 /* QualifiedName */ && ast.parent.left === ast;
        };

        PullTypeResolver.prototype.resolveGenericTypeReference = function (genericTypeAST, context) {
            var _this = this;
            var genericTypeSymbol = this.resolveAST(genericTypeAST.name, false, context).type;

            if (genericTypeSymbol.isError()) {
                return genericTypeSymbol;
            }

            if (!genericTypeSymbol.inResolution && !genericTypeSymbol.isResolved) {
                this.resolveDeclaredSymbol(genericTypeSymbol, context);
            }

            if (genericTypeSymbol.isAlias()) {
                if (this.inClassExtendsHeritageClause(genericTypeAST) && !this.inTypeArgumentList(genericTypeAST)) {
                    genericTypeSymbol.setIsUsedAsValue();
                }
                genericTypeSymbol = genericTypeSymbol.getExportAssignedTypeSymbol();
            }

            var typeParameters = genericTypeSymbol.getTypeParameters();
            if (typeParameters.length === 0) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(genericTypeAST, TypeScript.DiagnosticCode.Type_0_does_not_have_type_parameters, [genericTypeSymbol.toString()]));
                return this.getNewErrorTypeSymbol();
            }

            // specialize the type arguments
            var typeArgs = [];

            if (genericTypeAST.typeArgumentList && genericTypeAST.typeArgumentList.typeArguments.nonSeparatorCount()) {
                for (var i = 0; i < genericTypeAST.typeArgumentList.typeArguments.nonSeparatorCount(); i++) {
                    typeArgs[i] = this.resolveTypeReference(genericTypeAST.typeArgumentList.typeArguments.nonSeparatorAt(i), context);

                    if (typeArgs[i].isError()) {
                        typeArgs[i] = this.semanticInfoChain.anyTypeSymbol;
                    }
                }
            }

            if (typeArgs.length && typeArgs.length !== typeParameters.length) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(genericTypeAST, TypeScript.DiagnosticCode.Generic_type_0_requires_1_type_argument_s, [genericTypeSymbol.toString(), genericTypeSymbol.getTypeParameters().length]));
                return this.getNewErrorTypeSymbol();
            }

            // if the generic type symbol is not resolved, we need to ensure that all of its members are bound before specializing
            if (!genericTypeSymbol.isResolved) {
                var typeDecls = genericTypeSymbol.getDeclarations();
                var childDecls = null;

                for (var i = 0; i < typeDecls.length; i++) {
                    childDecls = typeDecls[i].getChildDecls();

                    for (var j = 0; j < childDecls.length; j++) {
                        childDecls[j].ensureSymbolIsBound();
                    }
                }
            }

            var specializedSymbol = this.createInstantiatedType(genericTypeSymbol, typeArgs);

            // check constraints, if appropriate
            var upperBound = null;

            // Get the instantiated versions of the type parameters (in case their constraints were generic)
            typeParameters = specializedSymbol.getTypeParameters();

            var typeConstraintSubstitutionMap = [];

            var instantiatedSubstitutionMap = specializedSymbol.getTypeParameterArgumentMap();

            for (var i = 0; i < typeParameters.length; i++) {
                typeConstraintSubstitutionMap[typeParameters[i].pullSymbolID] = typeParameters[i];
            }

            for (var id in instantiatedSubstitutionMap) {
                typeConstraintSubstitutionMap[id] = instantiatedSubstitutionMap[id];
            }

            for (var iArg = 0; (iArg < typeArgs.length) && (iArg < typeParameters.length); iArg++) {
                var typeArg = typeArgs[iArg];
                var typeParameter = typeParameters[iArg];
                var typeConstraint = typeParameter.getConstraint();

                typeConstraintSubstitutionMap[typeParameter.pullSymbolID] = typeArg;

                // test specialization type for assignment compatibility with the constraint
                if (typeConstraint) {
                    if (typeConstraint.isTypeParameter()) {
                        for (var j = 0; j < typeParameters.length && j < typeArgs.length; j++) {
                            if (typeParameters[j] === typeConstraint) {
                                typeConstraint = typeArgs[j];
                            }
                        }
                    } else if (typeConstraint.isGeneric()) {
                        typeConstraint = this.instantiateType(typeConstraint, typeConstraintSubstitutionMap);
                    }

                    if (typeArg.isTypeParameter()) {
                        upperBound = typeArg.getConstraint();

                        if (upperBound) {
                            typeArg = upperBound;
                        }
                    }

                    // handle cases where the type argument is a wrapped name type, that's being recursively resolved
                    if (typeArg.inResolution || (typeArg.isTypeReference() && typeArg.referencedTypeSymbol.inResolution)) {
                        return specializedSymbol;
                    }

                    if (context.canTypeCheckAST(genericTypeAST)) {
                        this.typeCheckCallBacks.push(function (context) {
                            // Section 3.4.2 (November 18, 2013):
                            // A type argument satisfies a type parameter constraint if the type argument is assignable
                            // to(section 3.8.4) the constraint type once type arguments are substituted for type parameters.
                            if (!_this.sourceIsAssignableToTarget(typeArg, typeConstraint, genericTypeAST, context)) {
                                var enclosingSymbol = _this.getEnclosingSymbolForAST(genericTypeAST);
                                context.postDiagnostic(_this.semanticInfoChain.diagnosticFromAST(genericTypeAST, TypeScript.DiagnosticCode.Type_0_does_not_satisfy_the_constraint_1_for_type_parameter_2, [typeArg.toString(enclosingSymbol, true), typeConstraint.toString(enclosingSymbol, true), typeParameter.toString(enclosingSymbol, true)]));
                            }
                        });
                    }
                }
            }

            return specializedSymbol;
        };

        PullTypeResolver.prototype.resolveQualifiedName = function (dottedNameAST, context) {
            if (this.inTypeQuery(dottedNameAST)) {
                // If we're in a type query, then treat the qualified name as a normal dotted
                // name expression.
                return this.resolveDottedNameExpression(dottedNameAST, dottedNameAST.left, dottedNameAST.right, context).type;
            }

            var symbol = this.getSymbolForAST(dottedNameAST, context);
            if (!symbol || this.canTypeCheckAST(dottedNameAST, context)) {
                var canTypeCheck = this.canTypeCheckAST(dottedNameAST, context);
                if (canTypeCheck) {
                    this.setTypeChecked(dottedNameAST, context);
                }

                symbol = this.computeQualifiedName(dottedNameAST, context);
                this.setSymbolForAST(dottedNameAST, symbol, context);
            }

            this.resolveDeclaredSymbol(symbol, context);

            return symbol;
        };

        PullTypeResolver.prototype.isLastNameOfModuleNameModuleReference = function (ast) {
            // SPEC: November 18, 2013 section 10.3 -
            // Don't walk up any other qualified names because the spec says only the last entity name can be a non-module
            // i.e. Only Module.Module.Module.var and not Module.Type.var or Module.var.member
            return ast.kind() === 11 /* IdentifierName */ && ast.parent && ast.parent.kind() === 121 /* QualifiedName */ && ast.parent.right === ast && ast.parent.parent && ast.parent.parent.kind() === 246 /* ModuleNameModuleReference */;
        };

        PullTypeResolver.prototype.computeQualifiedName = function (dottedNameAST, context) {
            var rhsName = dottedNameAST.right.valueText();
            if (rhsName.length === 0) {
                return this.semanticInfoChain.anyTypeSymbol;
            }

            // assemble the dotted name path
            var enclosingDecl = this.getEnclosingDeclForAST(dottedNameAST);
            var lhs = this.resolveAST(dottedNameAST.left, false, context);

            var lhsType = lhs.isAlias() ? lhs.getExportAssignedContainerSymbol() : lhs.type;

            if (this.inClassExtendsHeritageClause(dottedNameAST) && !this.inTypeArgumentList(dottedNameAST)) {
                if (lhs.isAlias()) {
                    lhs.setIsUsedAsValue();
                }
            }

            if (!lhsType) {
                return this.getNewErrorTypeSymbol();
            }

            if (this.isAnyOrEquivalent(lhsType)) {
                return lhsType;
            }

            // now for the name...
            var onLeftOfDot = this.isLeftSideOfQualifiedName(dottedNameAST);
            var isNameOfModule = dottedNameAST.parent.kind() === 130 /* ModuleDeclaration */ && dottedNameAST.parent.name === dottedNameAST;

            var memberKind = (onLeftOfDot || isNameOfModule) ? TypeScript.PullElementKind.SomeContainer : TypeScript.PullElementKind.SomeType;

            var childTypeSymbol = this.getNamedPropertySymbol(rhsName, memberKind, lhsType);

            // SPEC: November 18, 2013 section 10.3 -
            //  - An EntityName consisting of more than one identifier is resolved as
            //     a ModuleName followed by an identifier that names one or more exported entities in the given module.
            //     The resulting local alias has all the meanings and classifications of the referenced entity or entities.
            //     (As many as three distinct meanings are possible for an entity name — namespace, type, and member.)
            if (!childTypeSymbol && !isNameOfModule && this.isLastNameOfModuleNameModuleReference(dottedNameAST.right)) {
                childTypeSymbol = this.getNamedPropertySymbol(rhsName, TypeScript.PullElementKind.SomeValue, lhsType);
            }

            // if the lhs exports a container type, but not a type, we should check the container type
            if (!childTypeSymbol && lhsType.isContainer()) {
                var exportedContainer = lhsType.getExportAssignedContainerSymbol();

                if (exportedContainer) {
                    childTypeSymbol = this.getNamedPropertySymbol(rhsName, memberKind, exportedContainer);
                }
            }

            // If the name is expressed as a dotted name within the parent type,
            // then it will be considered a contained member, so back up to the nearest
            // enclosing symbol and look there
            if (!childTypeSymbol && enclosingDecl) {
                var parentDecl = enclosingDecl;

                while (parentDecl) {
                    if (parentDecl.kind & TypeScript.PullElementKind.SomeContainer) {
                        break;
                    }

                    parentDecl = parentDecl.getParentDecl();
                }

                if (parentDecl) {
                    var enclosingSymbolType = parentDecl.getSymbol().type;

                    if (enclosingSymbolType === lhsType) {
                        childTypeSymbol = this.getNamedPropertySymbol(rhsName, memberKind, lhsType); //lhsType.findContainedMember(rhsName);
                    }
                }
            }

            if (!childTypeSymbol) {
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(dottedNameAST.right, TypeScript.DiagnosticCode.The_property_0_does_not_exist_on_value_of_type_1, [dottedNameAST.right.text(), lhsType.toString(enclosingDecl ? enclosingDecl.getSymbol() : null)]));
                return this.getNewErrorTypeSymbol(rhsName);
            }

            return childTypeSymbol;
        };

        PullTypeResolver.prototype.shouldContextuallyTypeAnyFunctionExpression = function (functionExpressionAST, typeParameters, parameters, returnTypeAnnotation, context) {
            // September 21, 2013: If e is a FunctionExpression or ArrowFunctionExpression with no type parameters and no parameter
            // or return type annotations, and T is a function type with exactly one non - generic call signature, then any
            // inferences made for type parameters referenced by the parameters of T’s call signature are fixed(section 4.12.2)
            // and e is processed with the contextual type T, as described in section 4.9.3.
            // No type parameters
            if (typeParameters && typeParameters.typeParameters.nonSeparatorCount() > 0) {
                return false;
            }

            // No return type annotation
            if (returnTypeAnnotation) {
                return false;
            }

            // No parameter type annotations
            if (parameters) {
                for (var i = 0, n = parameters.length; i < n; i++) {
                    if (parameters.typeAt(i)) {
                        return false;
                    }
                }
            }

            var contextualFunctionTypeSymbol = context.getContextualType();

            // Exactly one non-generic call signature (note that this means it must have exactly one call signature,
            // AND that call signature must be non-generic)
            if (contextualFunctionTypeSymbol) {
                this.resolveDeclaredSymbol(contextualFunctionTypeSymbol, context);
                var callSignatures = contextualFunctionTypeSymbol.getCallSignatures();
                var exactlyOneCallSignature = callSignatures && callSignatures.length === 1;
                if (!exactlyOneCallSignature) {
                    return false;
                }

                var callSignatureIsGeneric = callSignatures[0].getTypeParameters().length > 0;
                return !callSignatureIsGeneric;
            }

            return false;
        };

        PullTypeResolver.prototype.resolveAnyFunctionExpression = function (funcDeclAST, typeParameters, parameters, returnTypeAnnotation, block, bodyExpression, isContextuallyTyped, context) {
            var funcDeclSymbol = null;
            var functionDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
            TypeScript.Debug.assert(functionDecl);

            if (functionDecl && functionDecl.hasSymbol()) {
                funcDeclSymbol = functionDecl.getSymbol();
                if (funcDeclSymbol.isResolved || funcDeclSymbol.inResolution) {
                    return funcDeclSymbol;
                }
            }

            funcDeclSymbol = functionDecl.getSymbol();
            TypeScript.Debug.assert(funcDeclSymbol);

            var funcDeclType = funcDeclSymbol.type;
            var signature = funcDeclType.getCallSignatures()[0];
            funcDeclSymbol.startResolving();

            if (typeParameters) {
                for (var i = 0; i < typeParameters.typeParameters.nonSeparatorCount(); i++) {
                    this.resolveTypeParameterDeclaration(typeParameters.typeParameters.nonSeparatorAt(i), context);
                }
            }

            var assigningFunctionSignature = null;
            if (isContextuallyTyped && this.shouldContextuallyTypeAnyFunctionExpression(funcDeclAST, typeParameters, parameters, returnTypeAnnotation, context)) {
                assigningFunctionSignature = context.getContextualType().getCallSignatures()[0];
            }

            // link parameters and resolve their annotations
            this.resolveAnyFunctionExpressionParameters(funcDeclAST, typeParameters, parameters, returnTypeAnnotation, isContextuallyTyped, context);

            // resolve the return type annotation
            if (returnTypeAnnotation) {
                signature.returnType = this.resolveTypeReference(returnTypeAnnotation, context);
            } else {
                if (assigningFunctionSignature) {
                    var returnType = assigningFunctionSignature.returnType;

                    if (returnType) {
                        context.propagateContextualType(returnType);
                        this.resolveFunctionBodyReturnTypes(funcDeclAST, block, bodyExpression, signature, true, functionDecl, context);
                        context.popAnyContextualType();
                    } else {
                        signature.returnType = this.semanticInfoChain.anyTypeSymbol;

                        // if noimplictiany flag is set to be true, report an error
                        if (this.compilationSettings.noImplicitAny()) {
                            var functionExpressionName = functionDecl.getFunctionExpressionName();

                            // If there is a function name for the funciton expression, report an error with that name
                            if (functionExpressionName != "") {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode._0_which_lacks_return_type_annotation_implicitly_has_an_any_return_type, [functionExpressionName]));
                            } else {
                                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Function_expression_which_lacks_return_type_annotation_implicitly_has_an_any_return_type));
                            }
                        }
                    }
                } else {
                    this.resolveFunctionBodyReturnTypes(funcDeclAST, block, bodyExpression, signature, false, functionDecl, context);
                }
            }

            // reset the type to the one we already had,
            // this makes sure if we had short - circuited the type of this symbol to any, we would get back to the function type
            context.setTypeInContext(funcDeclSymbol, funcDeclType);
            funcDeclSymbol.setResolved();

            if (this.canTypeCheckAST(funcDeclAST, context)) {
                this.typeCheckAnyFunctionExpression(funcDeclAST, typeParameters, parameters, returnTypeAnnotation, block, bodyExpression, isContextuallyTyped, context);
            }

            return funcDeclSymbol;
        };

        PullTypeResolver.prototype.resolveAnyFunctionExpressionParameters = function (funcDeclAST, typeParameters, parameters, returnTypeAnnotation, isContextuallyTyped, context) {
            if (!parameters) {
                return;
            }

            var functionDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

            var contextParams = [];

            var assigningFunctionSignature = null;
            if (isContextuallyTyped && this.shouldContextuallyTypeAnyFunctionExpression(funcDeclAST, typeParameters, parameters, returnTypeAnnotation, context)) {
                assigningFunctionSignature = context.getContextualType().getCallSignatures()[0];
            }

            if (assigningFunctionSignature) {
                contextParams = assigningFunctionSignature.parameters;
            }

            // Push the function onto the parameter index stack
            var contextualParametersCount = contextParams.length;
            for (var i = 0, n = parameters.length; i < n; i++) {
                // Function has a variable argument list, and this paramter is the last
                var actualParameterIsVarArgParameter = (i === (n - 1)) && parameters.lastParameterIsRest();
                var correspondingContextualParameter = null;
                var contextualParameterType = null;

                // Find the matching contextual paramter
                if (i < contextualParametersCount) {
                    correspondingContextualParameter = contextParams[i];
                } else if (contextualParametersCount && contextParams[contextualParametersCount - 1].isVarArg) {
                    correspondingContextualParameter = contextParams[contextualParametersCount - 1];
                }

                // Find the contextual type from the paramter
                if (correspondingContextualParameter) {
                    if (correspondingContextualParameter.isVarArg === actualParameterIsVarArgParameter) {
                        contextualParameterType = correspondingContextualParameter.type;
                    } else if (correspondingContextualParameter.isVarArg) {
                        contextualParameterType = correspondingContextualParameter.type.getElementType();
                    }
                }

                // use the function decl as the enclosing decl, so as to properly resolve type parameters
                this.resolveFunctionExpressionParameter(parameters.astAt(i), parameters.identifierAt(i), parameters.typeAt(i), parameters.initializerAt(i), contextualParameterType, functionDecl, context);
            }
        };

        PullTypeResolver.prototype.typeCheckSimpleArrowFunctionExpression = function (arrowFunction, isContextuallyTyped, context) {
            return this.typeCheckAnyFunctionExpression(arrowFunction, null, TypeScript.ASTHelpers.parametersFromIdentifier(arrowFunction.identifier), null, arrowFunction.block, arrowFunction.expression, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.typeCheckParenthesizedArrowFunctionExpression = function (arrowFunction, isContextuallyTyped, context) {
            return this.typeCheckAnyFunctionExpression(arrowFunction, arrowFunction.callSignature.typeParameterList, TypeScript.ASTHelpers.parametersFromParameterList(arrowFunction.callSignature.parameterList), TypeScript.ASTHelpers.getType(arrowFunction), arrowFunction.block, arrowFunction.expression, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.typeCheckAnyFunctionExpression = function (funcDeclAST, typeParameters, parameters, returnTypeAnnotation, block, bodyExpression, isContextuallyTyped, context) {
            var _this = this;
            this.setTypeChecked(funcDeclAST, context);

            var functionDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);

            var funcDeclSymbol = functionDecl.getSymbol();
            var funcDeclType = funcDeclSymbol.type;
            var signature = funcDeclType.getCallSignatures()[0];
            var returnTypeSymbol = signature.returnType;

            if (typeParameters) {
                for (var i = 0; i < typeParameters.typeParameters.nonSeparatorCount(); i++) {
                    this.resolveTypeParameterDeclaration(typeParameters.typeParameters.nonSeparatorAt(i), context);
                }
            }

            this.resolveAnyFunctionExpressionParameters(funcDeclAST, typeParameters, parameters, returnTypeAnnotation, isContextuallyTyped, context);

            // Make sure there is no contextual type on the stack when resolving the block
            context.pushNewContextualType(null);
            if (block) {
                this.resolveAST(block, false, context);
            } else {
                var bodyExpressionType = this.resolveReturnExpression(bodyExpression, functionDecl, context);
                this.typeCheckReturnExpression(bodyExpression, bodyExpressionType, functionDecl, context);
            }

            context.popAnyContextualType();

            this.checkThatNonVoidFunctionHasReturnExpressionOrThrowStatement(functionDecl, returnTypeAnnotation, returnTypeSymbol, block, context);

            this.validateVariableDeclarationGroups(functionDecl, context);

            this.typeCheckCallBacks.push(function (context) {
                _this.typeCheckFunctionOverloads(funcDeclAST, context);
            });
        };

        PullTypeResolver.prototype.resolveThisExpression = function (thisExpression, context) {
            var enclosingDecl = this.getEnclosingDeclForAST(thisExpression);
            var thisTypeSymbol = this.getContextualClassSymbolForEnclosingDecl(thisExpression, enclosingDecl) || this.semanticInfoChain.anyTypeSymbol;

            if (this.canTypeCheckAST(thisExpression, context)) {
                this.typeCheckThisExpression(thisExpression, context, enclosingDecl);
            }

            return thisTypeSymbol;
        };

        PullTypeResolver.prototype.inTypeArgumentList = function (ast) {
            var previous = null;
            var current = ast;

            while (current) {
                switch (current.kind()) {
                    case 126 /* GenericType */:
                        var genericType = current;
                        if (genericType.typeArgumentList === previous) {
                            return true;
                        }
                        break;

                    case 226 /* ArgumentList */:
                        var argumentList = current;
                        return argumentList.typeArgumentList === previous;
                }

                previous = current;
                current = current.parent;
            }

            return false;
        };

        PullTypeResolver.prototype.inClassExtendsHeritageClause = function (ast) {
            while (ast) {
                switch (ast.kind()) {
                    case 230 /* ExtendsHeritageClause */:
                        var heritageClause = ast;

                        // Heritage clause is parented by the heritage clause list.  Which is
                        // parented by either a class or an interface.  So check the grandparent.
                        return heritageClause.parent.parent.kind() === 131 /* ClassDeclaration */;

                    case 137 /* ConstructorDeclaration */:
                    case 131 /* ClassDeclaration */:
                    case 130 /* ModuleDeclaration */:
                        return false;
                }

                ast = ast.parent;
            }

            return false;
        };

        PullTypeResolver.prototype.inTypeQuery = function (ast) {
            while (ast) {
                switch (ast.kind()) {
                    case 127 /* TypeQuery */:
                        return true;
                    case 129 /* FunctionDeclaration */:
                    case 213 /* InvocationExpression */:
                    case 137 /* ConstructorDeclaration */:
                    case 131 /* ClassDeclaration */:
                    case 130 /* ModuleDeclaration */:
                        return false;
                }

                ast = ast.parent;
            }

            return false;
        };

        PullTypeResolver.prototype.inArgumentListOfSuperInvocation = function (ast) {
            var previous = null;
            var current = ast;
            while (current) {
                switch (current.kind()) {
                    case 213 /* InvocationExpression */:
                        var invocationExpression = current;
                        if (previous === invocationExpression.argumentList && invocationExpression.expression.kind() === 50 /* SuperKeyword */) {
                            return true;
                        }
                        break;

                    case 137 /* ConstructorDeclaration */:
                    case 131 /* ClassDeclaration */:
                    case 130 /* ModuleDeclaration */:
                        return false;
                }

                previous = current;
                current = current.parent;
            }

            return false;
        };

        PullTypeResolver.prototype.inConstructorParameterList = function (ast) {
            var previous = null;
            var current = ast;
            while (current) {
                switch (current.kind()) {
                    case 142 /* CallSignature */:
                        var callSignature = current;
                        if (previous === callSignature.parameterList && callSignature.parent.kind() === 137 /* ConstructorDeclaration */) {
                            return true;
                        }

                    case 131 /* ClassDeclaration */:
                    case 130 /* ModuleDeclaration */:
                        return false;
                }

                previous = current;
                current = current.parent;
            }

            return false;
        };
        PullTypeResolver.prototype.isFunctionAccessorOrNonArrowFunctionExpression = function (decl) {
            if (decl.kind === TypeScript.PullElementKind.GetAccessor || decl.kind === TypeScript.PullElementKind.SetAccessor) {
                return true;
            }

            return this.isFunctionOrNonArrowFunctionExpression(decl);
        };

        PullTypeResolver.prototype.isFunctionOrNonArrowFunctionExpression = function (decl) {
            if (decl.kind === TypeScript.PullElementKind.Function) {
                return true;
            } else if (decl.kind === TypeScript.PullElementKind.FunctionExpression && !TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.ArrowFunction)) {
                return true;
            }

            return false;
        };

        PullTypeResolver.prototype.typeCheckThisExpression = function (thisExpression, context, enclosingDecl) {
            this.checkForThisCaptureInArrowFunction(thisExpression);

            if (this.inConstructorParameterList(thisExpression)) {
                // October 11, 2013
                // Similar to functions, only the constructor implementation (and not
                // constructor overloads) can specify default value expressions for optional
                // parameters.It is a compile - time error for such default value expressions
                //  to reference this.
                //
                // Note: this applies for constructor parameters and constructor parameter
                // properties.
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(thisExpression, TypeScript.DiagnosticCode.this_cannot_be_referenced_in_constructor_arguments));
                return;
            }

            for (var currentDecl = enclosingDecl; currentDecl !== null; currentDecl = currentDecl.getParentDecl()) {
                if (this.isFunctionAccessorOrNonArrowFunctionExpression(currentDecl)) {
                    // 'this' is always ok in a function.  It just has the 'any' type.
                    return;
                } else if (currentDecl.kind === TypeScript.PullElementKind.Container || currentDecl.kind === TypeScript.PullElementKind.DynamicModule) {
                    if (currentDecl.getParentDecl() === null) {
                        // Legal in the global module.
                        return;
                    } else {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(thisExpression, TypeScript.DiagnosticCode.this_cannot_be_referenced_within_module_bodies));
                        return;
                    }
                } else if (currentDecl.kind === TypeScript.PullElementKind.Enum) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(thisExpression, TypeScript.DiagnosticCode.this_cannot_be_referenced_in_current_location));
                    return;
                } else if (currentDecl.kind === TypeScript.PullElementKind.ConstructorMethod) {
                    // October 11, 2013
                    // The first statement in the body of a constructor must be a super call if
                    // both of the following are true:
                    //      The containing class is a derived class.
                    //      The constructor declares parameter properties or the containing class
                    //      declares instance member variables with initializers.
                    // In such a required super call, it is a compile - time error for argument
                    // expressions to reference this.
                    if (this.inArgumentListOfSuperInvocation(thisExpression) && this.superCallMustBeFirstStatementInConstructor(currentDecl)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(thisExpression, TypeScript.DiagnosticCode.this_cannot_be_referenced_in_current_location));
                    }

                    // Otherwise, it's ok to access 'this' in a constructor.
                    return;
                } else if (currentDecl.kind === TypeScript.PullElementKind.Class) {
                    if (this.inStaticMemberVariableDeclaration(thisExpression)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(thisExpression, TypeScript.DiagnosticCode.this_cannot_be_referenced_in_static_initializers_in_a_class_body));
                    }

                    // Legal use of 'this'.
                    return;
                }
            }
        };

        PullTypeResolver.prototype.getContextualClassSymbolForEnclosingDecl = function (ast, enclosingDecl) {
            var declPath = enclosingDecl.getParentPath();

            // work back up the decl path, until you can find a class
            if (declPath.length) {
                var isStaticContext = false;

                for (var i = declPath.length - 1; i >= 0; i--) {
                    var decl = declPath[i];
                    var declKind = decl.kind;
                    var declFlags = decl.flags;

                    if (declFlags & TypeScript.PullElementFlags.Static) {
                        isStaticContext = true;
                    } else if (declKind === TypeScript.PullElementKind.FunctionExpression && !TypeScript.hasFlag(declFlags, TypeScript.PullElementFlags.ArrowFunction)) {
                        return null;
                    } else if (declKind === TypeScript.PullElementKind.Function) {
                        return null;
                    } else if (declKind === TypeScript.PullElementKind.Class) {
                        if (this.inStaticMemberVariableDeclaration(ast)) {
                            return this.getNewErrorTypeSymbol();
                        } else {
                            var classSymbol = decl.getSymbol();
                            if (isStaticContext) {
                                var constructorSymbol = classSymbol.getConstructorMethod();
                                return constructorSymbol.type;
                            } else {
                                return classSymbol;
                            }
                        }
                    }
                }
            }

            return null;
        };

        PullTypeResolver.prototype.inStaticMemberVariableDeclaration = function (ast) {
            while (ast) {
                if (ast.kind() === 136 /* MemberVariableDeclaration */ && TypeScript.hasModifier(ast.modifiers, TypeScript.PullElementFlags.Static)) {
                    return true;
                }

                ast = ast.parent;
            }

            return false;
        };

        PullTypeResolver.prototype.resolveSuperExpression = function (ast, context) {
            var enclosingDecl = this.getEnclosingDeclForAST(ast);
            var superType = this.semanticInfoChain.anyTypeSymbol;

            var classSymbol = this.getContextualClassSymbolForEnclosingDecl(ast, enclosingDecl);

            if (classSymbol) {
                this.resolveDeclaredSymbol(classSymbol, context);

                var parents = classSymbol.getExtendedTypes();

                if (parents.length) {
                    superType = parents[0];
                }
            }

            if (this.canTypeCheckAST(ast, context)) {
                this.typeCheckSuperExpression(ast, context, enclosingDecl);
            }

            return superType;
        };

        PullTypeResolver.prototype.typeCheckSuperExpression = function (ast, context, enclosingDecl) {
            this.setTypeChecked(ast, context);

            this.checkForThisCaptureInArrowFunction(ast);

            var isSuperCall = ast.parent.kind() === 213 /* InvocationExpression */;
            var isSuperPropertyAccess = ast.parent.kind() === 212 /* MemberAccessExpression */;
            TypeScript.Debug.assert(isSuperCall || isSuperPropertyAccess);

            if (isSuperPropertyAccess) {
                for (var currentDecl = enclosingDecl; currentDecl !== null; currentDecl = currentDecl.getParentDecl()) {
                    if (this.isFunctionOrNonArrowFunctionExpression(currentDecl)) {
                        break;
                    } else if (currentDecl.kind === TypeScript.PullElementKind.Class) {
                        // We're in some class member.  That's good.
                        if (!this.enclosingClassIsDerived(currentDecl)) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.super_cannot_be_referenced_in_non_derived_classes));
                            return;
                        } else if (this.inConstructorParameterList(ast)) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.super_cannot_be_referenced_in_constructor_arguments));
                            return;
                        } else if (this.inStaticMemberVariableDeclaration(ast)) {
                            break;
                        }

                        // We've checked the bad cases, at this point we're good.
                        return;
                    }
                }
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.super_property_access_is_permitted_only_in_a_constructor_member_function_or_member_accessor_of_a_derived_class));
                return;
            } else {
                // October 11, 2013
                // Constructors of classes with no extends clause may not contain super calls,
                // whereas constructors of derived classes must contain at least one super call
                // somewhere in their function body. Super calls are not permitted outside
                // constructors or in local functions inside constructors.
                //
                // The first statement in the body of a constructor must be a super call if both
                // of the following are true:
                //      The containing class is a derived class.
                //      The constructor declares parameter properties or the containing class
                //      declares instance member variables with initializers.
                if (enclosingDecl.kind === TypeScript.PullElementKind.ConstructorMethod) {
                    // We were in a constructor.  That's good.
                    var classDecl = enclosingDecl.getParentDecl();

                    if (!this.enclosingClassIsDerived(classDecl)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.super_cannot_be_referenced_in_non_derived_classes));
                        return;
                    } else if (this.inConstructorParameterList(ast)) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.super_cannot_be_referenced_in_constructor_arguments));
                        return;
                    }
                    // Nothing wrong with how we were referenced.  Note: the check if we're the
                    // first statement in the constructor happens in typeCheckConstructorDeclaration.
                } else {
                    // SPEC NOV 18: Super calls are not permitted outside constructors or in local functions inside constructors.
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Super_calls_are_not_permitted_outside_constructors_or_in_nested_functions_inside_constructors));
                }
            }
        };

        PullTypeResolver.prototype.resolveSimplePropertyAssignment = function (propertyAssignment, isContextuallyTyped, context) {
            return this.resolveAST(propertyAssignment.expression, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.resolveFunctionPropertyAssignment = function (funcProp, isContextuallyTyped, context) {
            return this.resolveAnyFunctionExpression(funcProp, funcProp.callSignature.typeParameterList, TypeScript.ASTHelpers.parametersFromParameterList(funcProp.callSignature.parameterList), TypeScript.ASTHelpers.getType(funcProp), funcProp.block, null, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.typeCheckFunctionPropertyAssignment = function (funcProp, isContextuallyTyped, context) {
            this.typeCheckAnyFunctionExpression(funcProp, funcProp.callSignature.typeParameterList, TypeScript.ASTHelpers.parametersFromParameterList(funcProp.callSignature.parameterList), TypeScript.ASTHelpers.getType(funcProp), funcProp.block, null, isContextuallyTyped, context);
        };

        PullTypeResolver.prototype.resolveObjectLiteralExpression = function (expressionAST, isContextuallyTyped, context, additionalResults) {
            var symbol = this.getSymbolForAST(expressionAST, context);
            var hasResolvedSymbol = symbol && symbol.isResolved;

            if (!hasResolvedSymbol || additionalResults || this.canTypeCheckAST(expressionAST, context)) {
                if (this.canTypeCheckAST(expressionAST, context)) {
                    this.setTypeChecked(expressionAST, context);
                }
                symbol = this.computeObjectLiteralExpression(expressionAST, isContextuallyTyped, context, additionalResults);
                this.setSymbolForAST(expressionAST, symbol, context);
            }

            return symbol;
        };

        PullTypeResolver.prototype.bindObjectLiteralMembers = function (objectLiteralDeclaration, objectLiteralTypeSymbol, objectLiteralMembers, isUsingExistingSymbol, pullTypeContext) {
            var boundMemberSymbols = [];
            var memberSymbol;
            for (var i = 0, len = objectLiteralMembers.nonSeparatorCount(); i < len; i++) {
                var propertyAssignment = objectLiteralMembers.nonSeparatorAt(i);

                var id = this.getPropertyAssignmentName(propertyAssignment);
                var assignmentText = getPropertyAssignmentNameTextFromIdentifier(id);

                var isAccessor = propertyAssignment.kind() === 139 /* GetAccessor */ || propertyAssignment.kind() === 140 /* SetAccessor */;
                var decl = this.semanticInfoChain.getDeclForAST(propertyAssignment);
                TypeScript.Debug.assert(decl);

                if (propertyAssignment.kind() === 240 /* SimplePropertyAssignment */) {
                    if (!isUsingExistingSymbol) {
                        memberSymbol = new TypeScript.PullSymbol(assignmentText.memberName, TypeScript.PullElementKind.Property);
                        memberSymbol.addDeclaration(decl);
                        decl.setSymbol(memberSymbol);
                    } else {
                        memberSymbol = decl.getSymbol();
                    }
                } else if (propertyAssignment.kind() === 241 /* FunctionPropertyAssignment */) {
                    memberSymbol = decl.getSymbol();
                } else {
                    TypeScript.Debug.assert(isAccessor);
                    memberSymbol = decl.getSymbol();
                }

                if (!isUsingExistingSymbol && !isAccessor) {
                    // Make sure this was not defined before
                    var existingMember = objectLiteralTypeSymbol.findMember(memberSymbol.name, true);
                    if (existingMember) {
                        pullTypeContext.postDiagnostic(this.semanticInfoChain.duplicateIdentifierDiagnosticFromAST(propertyAssignment, assignmentText.actualText, existingMember.getDeclarations()[0].ast()));
                    }

                    objectLiteralTypeSymbol.addMember(memberSymbol);
                }

                boundMemberSymbols.push(memberSymbol);
            }

            return boundMemberSymbols;
        };

        PullTypeResolver.prototype.resolveObjectLiteralMembers = function (objectLiteralDeclaration, objectLiteralTypeSymbol, objectLiteralContextualType, objectLiteralMembers, stringIndexerSignature, numericIndexerSignature, allMemberTypes, allNumericMemberTypes, boundMemberSymbols, isUsingExistingSymbol, pullTypeContext, additionalResults) {
            for (var i = 0, len = objectLiteralMembers.nonSeparatorCount(); i < len; i++) {
                var propertyAssignment = objectLiteralMembers.nonSeparatorAt(i);

                var acceptedContextualType = false;
                var assigningSymbol = null;

                var id = this.getPropertyAssignmentName(propertyAssignment);
                var memberSymbol = boundMemberSymbols[i];
                var contextualMemberType = null;

                if (objectLiteralContextualType) {
                    assigningSymbol = this.getNamedPropertySymbol(memberSymbol.name, TypeScript.PullElementKind.SomeValue, objectLiteralContextualType);

                    // Consider index signatures as potential contextual types
                    if (!assigningSymbol) {
                        if (numericIndexerSignature && TypeScript.PullHelpers.isNameNumeric(memberSymbol.name)) {
                            assigningSymbol = numericIndexerSignature;
                        } else if (stringIndexerSignature) {
                            assigningSymbol = stringIndexerSignature;
                        }
                    }

                    if (assigningSymbol) {
                        this.resolveDeclaredSymbol(assigningSymbol, pullTypeContext);

                        contextualMemberType = assigningSymbol.kind === TypeScript.PullElementKind.IndexSignature ? assigningSymbol.returnType : assigningSymbol.type;
                        pullTypeContext.propagateContextualType(contextualMemberType);

                        acceptedContextualType = true;

                        if (additionalResults) {
                            additionalResults.membersContextTypeSymbols[i] = contextualMemberType;
                        }
                    }
                }

                var memberSymbolType = this.resolveAST(propertyAssignment, contextualMemberType !== null, pullTypeContext).type;

                if (memberSymbolType) {
                    if (memberSymbolType.isGeneric()) {
                        objectLiteralTypeSymbol.setHasGenericMember();
                    }

                    // Add the member to the appropriate member type lists to compute the type of the synthesized index signatures
                    if (stringIndexerSignature) {
                        allMemberTypes.push(memberSymbolType);
                    }
                    if (numericIndexerSignature && TypeScript.PullHelpers.isNameNumeric(memberSymbol.name)) {
                        allNumericMemberTypes.push(memberSymbolType);
                    }
                }

                if (acceptedContextualType) {
                    pullTypeContext.popAnyContextualType();
                }

                var isAccessor = propertyAssignment.kind() === 140 /* SetAccessor */ || propertyAssignment.kind() === 139 /* GetAccessor */;
                if (!memberSymbol.isResolved) {
                    if (isAccessor) {
                        this.setSymbolForAST(id, memberSymbolType, pullTypeContext);
                    } else {
                        pullTypeContext.setTypeInContext(memberSymbol, memberSymbolType);
                        memberSymbol.setResolved();

                        this.setSymbolForAST(id, memberSymbol, pullTypeContext);
                    }
                }
            }
        };

        // if there's no type annotation on the assigning AST, we need to create a type from each binary expression
        // in the object literal
        PullTypeResolver.prototype.computeObjectLiteralExpression = function (objectLitAST, isContextuallyTyped, context, additionalResults) {
            // PULLTODO: Create a decl for the object literal
            // walk the members of the object literal,
            // create fields for each based on the value assigned in
            var objectLitDecl = this.semanticInfoChain.getDeclForAST(objectLitAST);
            TypeScript.Debug.assert(objectLitDecl);

            var typeSymbol = this.getSymbolForAST(objectLitAST, context);
            var isUsingExistingSymbol = !!typeSymbol;

            if (!typeSymbol) {
                // TODO: why don't se just use the normal symbol binder for this?
                typeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.ObjectLiteral);
                typeSymbol.addDeclaration(objectLitDecl);
                this.setSymbolForAST(objectLitAST, typeSymbol, context);
                objectLitDecl.setSymbol(typeSymbol);
            }

            var propertyAssignments = objectLitAST.propertyAssignments;
            var contextualType = null;

            if (isContextuallyTyped) {
                contextualType = context.getContextualType();
                this.resolveDeclaredSymbol(contextualType, context);
            }

            var stringIndexerSignature = null;
            var numericIndexerSignature = null;
            var allMemberTypes = null;
            var allNumericMemberTypes = null;

            // Get the index signatures for contextual typing
            if (contextualType) {
                var indexSignatures = this.getBothKindsOfIndexSignaturesExcludingAugmentedType(contextualType, context);

                stringIndexerSignature = indexSignatures.stringSignature;
                numericIndexerSignature = indexSignatures.numericSignature;

                // Start collecting the types of all the members so we can stamp the object
                // literal with the proper index signatures
                // October 16, 2013: Section 4.12.2: Where a contextual type would be included
                // in a candidate set for a best common type(such as when inferentially typing
                // an object or array literal), an inferential type is not.
                var inInferentialTyping = context.isInferentiallyTyping();
                if (stringIndexerSignature) {
                    allMemberTypes = inInferentialTyping ? [] : [stringIndexerSignature.returnType];
                }

                if (numericIndexerSignature) {
                    allNumericMemberTypes = inInferentialTyping ? [] : [numericIndexerSignature.returnType];
                }
            }

            if (propertyAssignments) {
                if (additionalResults) {
                    additionalResults.membersContextTypeSymbols = [];
                }

                // first bind decls and symbols
                var boundMemberSymbols = this.bindObjectLiteralMembers(objectLitDecl, typeSymbol, propertyAssignments, isUsingExistingSymbol, context);

                // now perform member symbol resolution
                this.resolveObjectLiteralMembers(objectLitDecl, typeSymbol, contextualType, propertyAssignments, stringIndexerSignature, numericIndexerSignature, allMemberTypes, allNumericMemberTypes, boundMemberSymbols, isUsingExistingSymbol, context, additionalResults);

                if (!isUsingExistingSymbol) {
                    this.stampObjectLiteralWithIndexSignature(typeSymbol, allMemberTypes, stringIndexerSignature, context);
                    this.stampObjectLiteralWithIndexSignature(typeSymbol, allNumericMemberTypes, numericIndexerSignature, context);
                }
            }

            typeSymbol.setResolved();
            return typeSymbol;
        };

        PullTypeResolver.prototype.getPropertyAssignmentName = function (propertyAssignment) {
            if (propertyAssignment.kind() === 240 /* SimplePropertyAssignment */) {
                return propertyAssignment.propertyName;
            } else if (propertyAssignment.kind() === 241 /* FunctionPropertyAssignment */) {
                return propertyAssignment.propertyName;
            } else if (propertyAssignment.kind() === 139 /* GetAccessor */) {
                return propertyAssignment.propertyName;
            } else if (propertyAssignment.kind() === 140 /* SetAccessor */) {
                return propertyAssignment.propertyName;
            } else {
                TypeScript.Debug.assert(false);
            }
        };

        PullTypeResolver.prototype.stampObjectLiteralWithIndexSignature = function (objectLiteralSymbol, indexerTypeCandidates, contextualIndexSignature, context) {
            if (contextualIndexSignature) {
                var typeCollection = {
                    getLength: function () {
                        return indexerTypeCandidates.length;
                    },
                    getTypeAtIndex: function (index) {
                        return indexerTypeCandidates[index];
                    }
                };
                var decl = objectLiteralSymbol.getDeclarations()[0];
                var indexerReturnType = this.findBestCommonType(typeCollection, context).widenedType(this, null, context);
                if (indexerReturnType === contextualIndexSignature.returnType) {
                    objectLiteralSymbol.addIndexSignature(contextualIndexSignature);
                } else {
                    // Create an index signature
                    this.semanticInfoChain.addSyntheticIndexSignature(decl, objectLiteralSymbol, this.getASTForDecl(decl), contextualIndexSignature.parameters[0].name, contextualIndexSignature.parameters[/*indexParamType*/ 0].type, indexerReturnType);
                }
            }
        };

        PullTypeResolver.prototype.resolveArrayLiteralExpression = function (arrayLit, isContextuallyTyped, context) {
            var symbol = this.getSymbolForAST(arrayLit, context);
            if (!symbol || this.canTypeCheckAST(arrayLit, context)) {
                if (this.canTypeCheckAST(arrayLit, context)) {
                    this.setTypeChecked(arrayLit, context);
                }
                symbol = this.computeArrayLiteralExpressionSymbol(arrayLit, isContextuallyTyped, context);
                this.setSymbolForAST(arrayLit, symbol, context);
            }

            return symbol;
        };

        PullTypeResolver.prototype.computeArrayLiteralExpressionSymbol = function (arrayLit, isContextuallyTyped, context) {
            var elements = arrayLit.expressions;
            var elementType = null;
            var elementTypes = [];
            var comparisonInfo = new TypeComparisonInfo();
            var contextualElementType = null;
            comparisonInfo.onlyCaptureFirstError = true;

            // if the target type is an array type, extract the element type
            if (isContextuallyTyped) {
                var contextualType = context.getContextualType();

                this.resolveDeclaredSymbol(contextualType, context);

                if (contextualType) {
                    // Get the number indexer if it exists
                    var indexSignatures = this.getBothKindsOfIndexSignaturesExcludingAugmentedType(contextualType, context);
                    if (indexSignatures.numericSignature) {
                        contextualElementType = indexSignatures.numericSignature.returnType;
                    }
                }
            }

            // Resolve element types
            if (elements) {
                if (contextualElementType) {
                    context.propagateContextualType(contextualElementType);
                }

                for (var i = 0, n = elements.nonSeparatorCount(); i < n; i++) {
                    elementTypes.push(this.resolveAST(elements.nonSeparatorAt(i), contextualElementType !== null, context).type);
                }

                if (contextualElementType) {
                    context.popAnyContextualType();
                }
            }

            // If there is no contextual type to apply attempt to find the best common type
            if (elementTypes.length) {
                elementType = elementTypes[0];
            }
            var collection;

            // October 16, 2013: Section 4.12.2: Where a contextual type would be included
            // in a candidate set for a best common type(such as when inferentially typing
            // an object or array literal), an inferential type is not.
            if (contextualElementType && !context.isInferentiallyTyping()) {
                if (!elementType) {
                    elementType = contextualElementType;
                }

                // Add the contextual type to the collection as one of the types to be considered for best common type
                collection = {
                    getLength: function () {
                        return elements.nonSeparatorCount() + 1;
                    },
                    getTypeAtIndex: function (index) {
                        return index === 0 ? contextualElementType : elementTypes[index - 1];
                    }
                };
            } else {
                collection = {
                    getLength: function () {
                        return elements.nonSeparatorCount();
                    },
                    getTypeAtIndex: function (index) {
                        return elementTypes[index];
                    }
                };
            }

            elementType = elementType ? this.findBestCommonType(collection, context, comparisonInfo) : elementType;

            if (!elementType) {
                elementType = this.semanticInfoChain.undefinedTypeSymbol;
            }

            return this.getArrayType(elementType);
        };

        PullTypeResolver.prototype.resolveElementAccessExpression = function (callEx, context) {
            var symbolAndDiagnostic = this.computeElementAccessExpressionSymbolAndDiagnostic(callEx, context);

            if (this.canTypeCheckAST(callEx, context)) {
                this.typeCheckElementAccessExpression(callEx, context, symbolAndDiagnostic);
            }

            return symbolAndDiagnostic.symbol;
        };

        PullTypeResolver.prototype.typeCheckElementAccessExpression = function (callEx, context, symbolAndDiagnostic) {
            this.setTypeChecked(callEx, context);
            context.postDiagnostic(symbolAndDiagnostic.diagnostic);
        };

        PullTypeResolver.prototype.computeElementAccessExpressionSymbolAndDiagnostic = function (callEx, context) {
            // resolve the target
            var targetSymbol = this.resolveAST(callEx.expression, false, context);
            var indexType = this.resolveAST(callEx.argumentExpression, false, context).type;

            var targetTypeSymbol = targetSymbol.type;

            targetTypeSymbol = this.getApparentType(targetTypeSymbol);

            if (this.isAnyOrEquivalent(targetTypeSymbol)) {
                return { symbol: targetTypeSymbol };
            }

            var elementType = targetTypeSymbol.getElementType();

            var isNumberIndex = indexType === this.semanticInfoChain.numberTypeSymbol || TypeScript.PullHelpers.symbolIsEnum(indexType);

            if (elementType && isNumberIndex) {
                return { symbol: elementType };
            }

            // if the index expression is a string literal or a numberic literal and the object expression has
            // a property with that name,  the property access is the type of that property
            if (callEx.argumentExpression.kind() === 14 /* StringLiteral */ || callEx.argumentExpression.kind() === 13 /* NumericLiteral */) {
                var memberName = callEx.argumentExpression.kind() === 14 /* StringLiteral */ ? TypeScript.stripStartAndEndQuotes(callEx.argumentExpression.text()) : callEx.argumentExpression.valueText();

                // November 18, 2013, Section 4.10:
                // If IndexExpr is a string literal or a numeric literal and ObjExpr's apparent type
                // has a property with the name given by that literal(converted to its string representation
                // in the case of a numeric literal), the property access is of the type of that property.
                // Note that here we are assuming the apparent type is already accounted for up to
                // augmentation. So we just check the augmented type.
                var member = this._getNamedPropertySymbolOfAugmentedType(memberName, targetTypeSymbol);

                if (member) {
                    this.resolveDeclaredSymbol(member, context);

                    return { symbol: member.type };
                }
            }

            var signatures = this.getBothKindsOfIndexSignaturesIncludingAugmentedType(targetTypeSymbol, context);

            var stringSignature = signatures.stringSignature;
            var numberSignature = signatures.numericSignature;

            // otherwise, if the object expression has a numeric index signature and the index expression is
            // of type Any, the Number primitive type or an enum type, the property access is of the type of that index
            // signature
            if (numberSignature && (isNumberIndex || indexType === this.semanticInfoChain.anyTypeSymbol)) {
                return { symbol: numberSignature.returnType || this.semanticInfoChain.anyTypeSymbol };
            } else if (stringSignature && (isNumberIndex || indexType === this.semanticInfoChain.anyTypeSymbol || indexType === this.semanticInfoChain.stringTypeSymbol)) {
                return { symbol: stringSignature.returnType || this.semanticInfoChain.anyTypeSymbol };
            } else if (isNumberIndex || indexType === this.semanticInfoChain.anyTypeSymbol || indexType === this.semanticInfoChain.stringTypeSymbol) {
                if (this.compilationSettings.noImplicitAny()) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(callEx.argumentExpression, TypeScript.DiagnosticCode.Index_signature_of_object_type_implicitly_has_an_any_type));
                }
                return { symbol: this.semanticInfoChain.anyTypeSymbol };
            } else {
                return {
                    symbol: this.getNewErrorTypeSymbol(),
                    diagnostic: this.semanticInfoChain.diagnosticFromAST(callEx, TypeScript.DiagnosticCode.Value_of_type_0_is_not_indexable_by_type_1, [targetTypeSymbol.toString(), indexType.toString()])
                };
            }
        };

        PullTypeResolver.prototype.getBothKindsOfIndexSignaturesIncludingAugmentedType = function (enclosingType, context) {
            return this._getBothKindsOfIndexSignatures(enclosingType, context, true);
        };

        PullTypeResolver.prototype.getBothKindsOfIndexSignaturesExcludingAugmentedType = function (enclosingType, context) {
            return this._getBothKindsOfIndexSignatures(enclosingType, context, false);
        };

        PullTypeResolver.prototype._getBothKindsOfIndexSignatures = function (enclosingType, context, includeAugmentedType) {
            var signatures = includeAugmentedType ? enclosingType.getIndexSignaturesOfAugmentedType(this, this.cachedFunctionInterfaceType(), this.cachedObjectInterfaceType()) : enclosingType.getIndexSignatures();

            var stringSignature = null;
            var numberSignature = null;
            var signature = null;
            var paramSymbols;
            var paramType;

            for (var i = 0; i < signatures.length; i++) {
                if (stringSignature && numberSignature) {
                    break;
                }

                signature = signatures[i];
                if (!signature.isResolved) {
                    this.resolveDeclaredSymbol(signature, context);
                }

                paramSymbols = signature.parameters;

                if (paramSymbols.length) {
                    paramType = paramSymbols[0].type;

                    if (!stringSignature && paramType === this.semanticInfoChain.stringTypeSymbol) {
                        stringSignature = signature;
                        continue;
                    } else if (!numberSignature && paramType === this.semanticInfoChain.numberTypeSymbol) {
                        numberSignature = signature;
                        continue;
                    }
                }
            }

            return {
                numericSignature: numberSignature,
                stringSignature: stringSignature
            };
        };

        // This method can be called to get unhidden (not shadowed by a signature in derived class)
        // signatures from a set of base class signatures. Can be used for signatures of any kind.
        // October 16, 2013: Section 7.1:
        // A call signature declaration hides a base type call signature that is identical when
        // return types are ignored.
        // A construct signature declaration hides a base type construct signature that is
        // identical when return types are ignored.
        // A string index signature declaration hides a base type string index signature.
        // A numeric index signature declaration hides a base type numeric index signature.
        PullTypeResolver.prototype._addUnhiddenSignaturesFromBaseType = function (derivedTypeSignatures, baseTypeSignatures, signaturesBeingAggregated) {
            var _this = this;
            // If there are no derived type signatures, none of the base signatures will be hidden.
            if (!derivedTypeSignatures) {
                signaturesBeingAggregated.push.apply(signaturesBeingAggregated, baseTypeSignatures);
                return;
            }

            var context = new TypeScript.PullTypeResolutionContext(this);
            for (var i = 0; i < baseTypeSignatures.length; i++) {
                var baseSignature = baseTypeSignatures[i];

                // If it is different from every signature in the derived type (modulo
                // return types, add it to the list)
                var signatureIsHidden = TypeScript.ArrayUtilities.any(derivedTypeSignatures, function (sig) {
                    return _this.signaturesAreIdenticalWithNewEnclosingTypes(baseSignature, sig, context, false);
                });

                if (!signatureIsHidden) {
                    signaturesBeingAggregated.push(baseSignature);
                }
            }
        };

        PullTypeResolver.prototype.resolveBinaryAdditionOperation = function (binaryExpression, context) {
            var lhsExpression = this.resolveAST(binaryExpression.left, false, context);
            var lhsType = lhsExpression.type;
            var rhsType = this.resolveAST(binaryExpression.right, false, context).type;

            // November 18, 2013
            // 4.15.2 The + operator
            // The binary + operator requires both operands to be of the Number primitive type or an enum type, or at least one of the operands to be of type Any or the String primitive type.
            // Operands of an enum type are treated as having the primitive type Number.
            // If one operand is the null or undefined value, it is treated as having the type of the other operand.
            if (TypeScript.PullHelpers.symbolIsEnum(lhsType)) {
                lhsType = this.semanticInfoChain.numberTypeSymbol;
            }

            if (TypeScript.PullHelpers.symbolIsEnum(rhsType)) {
                rhsType = this.semanticInfoChain.numberTypeSymbol;
            }

            var isLhsTypeNullOrUndefined = lhsType === this.semanticInfoChain.nullTypeSymbol || lhsType === this.semanticInfoChain.undefinedTypeSymbol;
            var isRhsTypeNullOrUndefined = rhsType === this.semanticInfoChain.nullTypeSymbol || rhsType === this.semanticInfoChain.undefinedTypeSymbol;

            if (isLhsTypeNullOrUndefined) {
                if (isRhsTypeNullOrUndefined) {
                    lhsType = rhsType = this.semanticInfoChain.anyTypeSymbol;
                } else {
                    lhsType = rhsType;
                }
            } else if (isRhsTypeNullOrUndefined) {
                rhsType = lhsType;
            }

            var exprType = null;

            if (lhsType === this.semanticInfoChain.stringTypeSymbol || rhsType === this.semanticInfoChain.stringTypeSymbol) {
                exprType = this.semanticInfoChain.stringTypeSymbol;
            } else if (this.isAnyOrEquivalent(lhsType) || this.isAnyOrEquivalent(rhsType)) {
                exprType = this.semanticInfoChain.anyTypeSymbol;
            } else if (rhsType === this.semanticInfoChain.numberTypeSymbol && lhsType === this.semanticInfoChain.numberTypeSymbol) {
                exprType = this.semanticInfoChain.numberTypeSymbol;
            }

            if (this.canTypeCheckAST(binaryExpression, context)) {
                this.setTypeChecked(binaryExpression, context);

                if (exprType) {
                    if (binaryExpression.kind() === 175 /* AddAssignmentExpression */) {
                        // Check if LHS is a valid target
                        if (!this.isReference(binaryExpression.left, lhsExpression)) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.left, TypeScript.DiagnosticCode.Invalid_left_hand_side_of_assignment_expression));
                        }

                        this.checkAssignability(binaryExpression.left, exprType, lhsType, context);
                    }
                } else {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.left, TypeScript.DiagnosticCode.Invalid_expression_types_not_known_to_support_the_addition_operator));
                }
            }

            if (!exprType) {
                exprType = this.semanticInfoChain.anyTypeSymbol;
            }

            return exprType;
        };

        PullTypeResolver.prototype.bestCommonTypeOfTwoTypes = function (type1, type2, context) {
            return this.findBestCommonType({
                getLength: function () {
                    return 2;
                },
                getTypeAtIndex: function (index) {
                    switch (index) {
                        case 0:
                            return type1;
                        case 1:
                            return type2;
                    }
                }
            }, context);
        };

        PullTypeResolver.prototype.bestCommonTypeOfThreeTypes = function (type1, type2, type3, context) {
            return this.findBestCommonType({
                getLength: function () {
                    return 3;
                },
                getTypeAtIndex: function (index) {
                    switch (index) {
                        case 0:
                            return type1;
                        case 1:
                            return type2;
                        case 2:
                            return type3;
                    }
                }
            }, context);
        };

        PullTypeResolver.prototype.resolveLogicalOrExpression = function (binex, isContextuallyTyped, context) {
            // October 11, 2013:
            // The || operator permits the operands to be of any type.
            if (this.canTypeCheckAST(binex, context)) {
                // So there's no type checking we actually have to do.
                this.setTypeChecked(binex, context);
            }

            if (isContextuallyTyped) {
                // If the || expression is contextually typed(section 4.19), the operands are
                // contextually typed by the same type and the result is of the best common type
                // (section 3.10) of the contextual type and the two operand types.
                var contextualType = context.getContextualType();
                var leftType = this.resolveAST(binex.left, isContextuallyTyped, context).type;
                var rightType = this.resolveAST(binex.right, isContextuallyTyped, context).type;

                return context.isInferentiallyTyping() ? this.bestCommonTypeOfTwoTypes(leftType, rightType, context) : this.bestCommonTypeOfThreeTypes(contextualType, leftType, rightType, context);
            } else {
                // If the || expression is not contextually typed, the right operand is contextually
                // typed by the type of the left operand and the result is of the best common type of
                // the two operand types.
                var leftType = this.resolveAST(binex.left, false, context).type;

                context.pushNewContextualType(leftType);
                var rightType = this.resolveAST(binex.right, true, context).type;
                context.popAnyContextualType();

                return this.bestCommonTypeOfTwoTypes(leftType, rightType, context);
            }
        };

        PullTypeResolver.prototype.resolveLogicalAndExpression = function (binex, context) {
            if (this.canTypeCheckAST(binex, context)) {
                this.setTypeChecked(binex, context);

                this.resolveAST(binex.left, false, context);
            }

            // September 17, 2013: The && operator permits the operands to be of any type and
            // produces a result of the same type as the second operand.
            return this.resolveAST(binex.right, false, context).type;
        };

        PullTypeResolver.prototype.computeTypeOfConditionalExpression = function (leftType, rightType, isContextuallyTyped, context) {
            if (isContextuallyTyped && !context.isInferentiallyTyping()) {
                // October 11, 2013
                // If the conditional expression is contextually typed (section 4.19), Expr1 and Expr2
                // are contextually typed by the same type and the result is of the best common type
                // (section 3.10) of the contextual type and the types of Expr1 and Expr2.
                var contextualType = context.getContextualType();
                return this.bestCommonTypeOfThreeTypes(contextualType, leftType, rightType, context);
            } else {
                // October 11, 2013
                // If the conditional expression is not contextually typed, the result is of the
                // best common type of the types of Expr1 and Expr2.
                return this.bestCommonTypeOfTwoTypes(leftType, rightType, context);
            }
        };

        PullTypeResolver.prototype.resolveConditionalExpression = function (trinex, isContextuallyTyped, context) {
            // October 11, 2013
            // If the conditional expression is contextually typed (section 4.19), Expr1 and Expr2
            // are contextually typed by the same type and the result is of the best common type
            // (section 3.10) of the contextual type and the types of Expr1 and Expr2.  An error
            // occurs if the best common type is not identical to at least one of the three
            // candidate types.
            var leftType = this.resolveAST(trinex.whenTrue, isContextuallyTyped, context).type;
            var rightType = this.resolveAST(trinex.whenFalse, isContextuallyTyped, context).type;

            var expressionType = this.computeTypeOfConditionalExpression(leftType, rightType, isContextuallyTyped, context);

            var conditionalTypesAreValid = this.conditionExpressionTypesAreValid(leftType, rightType, expressionType, isContextuallyTyped, context);

            if (this.canTypeCheckAST(trinex, context)) {
                this.setTypeChecked(trinex, context);
                this.resolveAST(trinex.condition, false, context);

                if (!this.conditionExpressionTypesAreValid(leftType, rightType, expressionType, isContextuallyTyped, context)) {
                    if (isContextuallyTyped) {
                        var contextualType = context.getContextualType();
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(trinex, TypeScript.DiagnosticCode.Type_of_conditional_0_must_be_identical_to_1_2_or_3, [expressionType.toString(), leftType.toString(), rightType.toString(), contextualType.toString()]));
                    } else {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(trinex, TypeScript.DiagnosticCode.Type_of_conditional_0_must_be_identical_to_1_or_2, [expressionType.toString(), leftType.toString(), rightType.toString()]));
                    }
                }
            }

            // If the conditional is not valid, then return an error symbol.  That way we won't
            // report further errors higher up the stack.
            if (!conditionalTypesAreValid) {
                return this.getNewErrorTypeSymbol();
            }

            return expressionType;
        };

        PullTypeResolver.prototype.conditionExpressionTypesAreValid = function (leftType, rightType, expressionType, isContextuallyTyped, context) {
            if (isContextuallyTyped) {
                var contextualType = context.getContextualType();
                if (this.typesAreIdentical(expressionType, leftType, context) || this.typesAreIdentical(expressionType, rightType, context) || this.typesAreIdentical(expressionType, contextualType, context)) {
                    return true;
                }
            } else {
                if (this.typesAreIdentical(expressionType, leftType, context) || this.typesAreIdentical(expressionType, rightType, context)) {
                    return true;
                }
            }

            return false;
        };

        PullTypeResolver.prototype.resolveParenthesizedExpression = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);
            }

            // September 17, 2013: A parenthesized expression (Expression) has the same type and
            // classification as the Expression itself
            return this.resolveAST(ast.expression, false, context);
        };

        PullTypeResolver.prototype.resolveExpressionStatement = function (ast, context) {
            if (this.canTypeCheckAST(ast, context)) {
                this.setTypeChecked(ast, context);

                this.resolveAST(ast.expression, false, context);
            }

            // All statements have the 'void' type.
            return this.semanticInfoChain.voidTypeSymbol;
        };

        PullTypeResolver.prototype.resolveInvocationExpression = function (callEx, context, additionalResults) {
            var symbol = this.getSymbolForAST(callEx, context);

            if (!symbol || !symbol.isResolved) {
                if (!additionalResults) {
                    additionalResults = new PullAdditionalCallResolutionData();
                }
                symbol = this.computeInvocationExpressionSymbol(callEx, context, additionalResults);
                if (this.canTypeCheckAST(callEx, context)) {
                    this.setTypeChecked(callEx, context);
                }
                if (symbol !== this.semanticInfoChain.anyTypeSymbol) {
                    this.setSymbolForAST(callEx, symbol, context);
                }
                this.semanticInfoChain.setCallResolutionDataForAST(callEx, additionalResults);
            } else {
                if (this.canTypeCheckAST(callEx, context)) {
                    this.typeCheckInvocationExpression(callEx, context);
                }

                var callResolutionData = this.semanticInfoChain.getCallResolutionDataForAST(callEx);
                if (additionalResults && (callResolutionData !== additionalResults)) {
                    additionalResults.actualParametersContextTypeSymbols = callResolutionData.actualParametersContextTypeSymbols;
                    additionalResults.candidateSignature = callResolutionData.candidateSignature;
                    additionalResults.resolvedSignatures = callResolutionData.resolvedSignatures;
                    additionalResults.targetSymbol = callResolutionData.targetSymbol;
                }
            }

            return symbol;
        };

        PullTypeResolver.prototype.typeCheckInvocationExpression = function (callEx, context) {
            this.setTypeChecked(callEx, context);
            var targetSymbol = this.resolveAST(callEx.expression, false, context);

            if (callEx.argumentList.arguments) {
                var callResolutionData = this.semanticInfoChain.getCallResolutionDataForAST(callEx);

                var len = callEx.argumentList.arguments.nonSeparatorCount();
                for (var i = 0; i < len; i++) {
                    // Ensure call resolution data contains additional information.
                    // Actual parameters context type symbols will be undefined if the call target resolves to any or error types.
                    var contextualType = callResolutionData.actualParametersContextTypeSymbols ? callResolutionData.actualParametersContextTypeSymbols[i] : null;
                    if (contextualType) {
                        context.pushNewContextualType(contextualType);
                    }

                    this.resolveAST(callEx.argumentList.arguments.nonSeparatorAt(i), contextualType !== null, context);

                    if (contextualType) {
                        context.popAnyContextualType();
                        contextualType = null;
                    }
                }
            }

            for (var i = 0; i < callResolutionData.diagnosticsFromOverloadResolution.length; i++) {
                context.postDiagnostic(callResolutionData.diagnosticsFromOverloadResolution[i]);
            }
        };

        PullTypeResolver.prototype.computeInvocationExpressionSymbol = function (callEx, context, additionalResults) {
            // resolve the target
            var targetSymbol = this.resolveAST(callEx.expression, false, context);
            var targetAST = this.getCallTargetErrorSpanAST(callEx);

            var targetTypeSymbol = targetSymbol.type;
            if (this.isAnyOrEquivalent(targetTypeSymbol)) {
                // Note: targetType is either any or an error.
                // resolve any arguments.
                this.resolveAST(callEx.argumentList.arguments, false, context);

                if (callEx.argumentList.typeArgumentList && callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount()) {
                    // Can't invoke 'any' generically.
                    if (targetTypeSymbol === this.semanticInfoChain.anyTypeSymbol) {
                        this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Untyped_function_calls_may_not_accept_type_arguments), additionalResults, context);
                        return this.getNewErrorTypeSymbol();
                    }
                    // Note: if we get here, targetType is an error type.  In that case we don't
                    // want to report *another* error since the user will have already gotten
                    // the first error about the target not resolving properly.
                }

                return this.semanticInfoChain.anyTypeSymbol;
            }

            var isSuperCall = false;

            if (callEx.expression.kind() === 50 /* SuperKeyword */) {
                isSuperCall = true;

                if (targetTypeSymbol.isClass()) {
                    targetSymbol = targetTypeSymbol.getConstructorMethod();
                    this.resolveDeclaredSymbol(targetSymbol, context);
                    targetTypeSymbol = targetSymbol.type;
                } else {
                    this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Calls_to_super_are_only_valid_inside_a_class), additionalResults, context);
                    this.resolveAST(callEx.argumentList.arguments, false, context);

                    // POST diagnostics
                    return this.getNewErrorTypeSymbol();
                }
            }

            var signatures = isSuperCall ? targetTypeSymbol.getConstructSignatures() : targetTypeSymbol.getCallSignatures();

            if (!signatures.length && (targetTypeSymbol.kind === TypeScript.PullElementKind.ConstructorType)) {
                this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Value_of_type_0_is_not_callable_Did_you_mean_to_include_new, [targetTypeSymbol.toString()]), additionalResults, context);
            }

            var explicitTypeArgs = null;
            var couldNotFindGenericOverload = false;
            var couldNotAssignToConstraint;
            var constraintDiagnostic = null;
            var typeArgumentCountDiagnostic = null;
            var diagnostics = [];

            // resolve the type arguments, specializing if necessary
            if (callEx.argumentList.typeArgumentList) {
                // specialize the type arguments
                explicitTypeArgs = [];

                if (callEx.argumentList.typeArgumentList && callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount()) {
                    for (var i = 0; i < callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount(); i++) {
                        explicitTypeArgs[i] = this.resolveTypeReference(callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorAt(i), context);
                    }
                }
            }

            var triedToInferTypeArgs = false;

            // next, walk the available signatures
            // if any are generic, and we don't have type arguments, try to infer
            // otherwise, try to specialize to the type arguments above
            var resolvedSignatures = [];
            var inferredOrExplicitTypeArgs;
            var specializedSignature;
            var typeParameters;
            var typeConstraint = null;
            var beforeResolutionSignatures = signatures;
            var targetTypeReplacementMap = targetTypeSymbol.getTypeParameterArgumentMap();

            for (var i = 0; i < signatures.length; i++) {
                typeParameters = signatures[i].getTypeParameters();
                couldNotAssignToConstraint = false;

                if (signatures[i].isGeneric() && typeParameters.length) {
                    // If it is a constructor call, the type arguments are that of the return Type that is the super type
                    if (isSuperCall && targetTypeSymbol.isGeneric() && !callEx.argumentList.typeArgumentList) {
                        explicitTypeArgs = signatures[i].returnType.getTypeArguments();
                    }

                    if (explicitTypeArgs) {
                        // October 16, 2013: Section 4.12.2
                        // A generic signature is a candidate in a function call with type arguments
                        // arguments when:
                        // The signature has the same number of type parameters as were supplied in
                        // the type argument list
                        // ...
                        if (explicitTypeArgs.length === typeParameters.length) {
                            inferredOrExplicitTypeArgs = explicitTypeArgs;
                        } else {
                            typeArgumentCountDiagnostic = typeArgumentCountDiagnostic || this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Signature_expected_0_type_arguments_got_1_instead, [typeParameters.length, explicitTypeArgs.length]);
                            continue;
                        }
                    } else {
                        TypeScript.Debug.assert(callEx.argumentList);
                        var typeArgumentInferenceContext = new TypeScript.InvocationTypeArgumentInferenceContext(this, context, signatures[i], callEx.argumentList.arguments);
                        inferredOrExplicitTypeArgs = this.inferArgumentTypesForSignature(typeArgumentInferenceContext, new TypeComparisonInfo(), context);
                        triedToInferTypeArgs = true;
                    }

                    // if we could infer Args, or we have type arguments, then attempt to specialize the signature
                    TypeScript.Debug.assert(inferredOrExplicitTypeArgs && inferredOrExplicitTypeArgs.length == typeParameters.length);

                    // When specializing the constraints, seed the replacement map with any substitutions already specified by
                    // the target function's type
                    var mutableTypeReplacementMap = new TypeScript.PullInstantiationHelpers.MutableTypeArgumentMap(targetTypeReplacementMap ? targetTypeReplacementMap : []);
                    TypeScript.PullInstantiationHelpers.updateMutableTypeParameterArgumentMap(typeParameters, inferredOrExplicitTypeArgs, mutableTypeReplacementMap);
                    var typeReplacementMap = mutableTypeReplacementMap.typeParameterArgumentMap;

                    if (explicitTypeArgs) {
                        for (var j = 0; j < typeParameters.length; j++) {
                            typeConstraint = typeParameters[j].getConstraint();

                            // test specialization type for assignment compatibility with the constraint
                            if (typeConstraint) {
                                if (typeConstraint.isGeneric()) {
                                    typeConstraint = this.instantiateType(typeConstraint, typeReplacementMap);
                                }

                                // Section 3.4.2 (November 18, 2013):
                                // A type argument satisfies a type parameter constraint if the type argument is assignable
                                // to(section 3.8.4) the constraint type once type arguments are substituted for type parameters.
                                if (!this.sourceIsAssignableToTarget(inferredOrExplicitTypeArgs[j], typeConstraint, targetAST, context, null, true)) {
                                    var enclosingSymbol = this.getEnclosingSymbolForAST(targetAST);
                                    constraintDiagnostic = this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Type_0_does_not_satisfy_the_constraint_1_for_type_parameter_2, [inferredOrExplicitTypeArgs[j].toString(enclosingSymbol, true), typeConstraint.toString(enclosingSymbol, true), typeParameters[j].toString(enclosingSymbol, true)]);
                                    couldNotAssignToConstraint = true;
                                }

                                if (couldNotAssignToConstraint) {
                                    break;
                                }
                            }
                        }
                    }

                    if (couldNotAssignToConstraint) {
                        continue;
                    }

                    specializedSignature = this.instantiateSignature(signatures[i], typeReplacementMap);

                    if (specializedSignature) {
                        resolvedSignatures[resolvedSignatures.length] = specializedSignature;
                    }
                } else {
                    if (!(callEx.argumentList.typeArgumentList && callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount())) {
                        resolvedSignatures[resolvedSignatures.length] = signatures[i];
                    }
                }
            }

            if (signatures.length && !resolvedSignatures.length) {
                couldNotFindGenericOverload = true;
            }

            signatures = resolvedSignatures;

            var errorCondition = null;
            if (!signatures.length) {
                additionalResults.targetSymbol = targetSymbol;
                additionalResults.resolvedSignatures = beforeResolutionSignatures;
                additionalResults.candidateSignature = beforeResolutionSignatures && beforeResolutionSignatures.length ? beforeResolutionSignatures[0] : null;

                additionalResults.actualParametersContextTypeSymbols = actualParametersContextTypeSymbols;

                this.resolveAST(callEx.argumentList.arguments, false, context);

                // if there are no call signatures, but the target is assignable to 'Function', return 'any'
                if (!couldNotFindGenericOverload) {
                    // if there are no call signatures, but the target is assignable to 'Function', return 'any'
                    if (this.cachedFunctionInterfaceType() && this.sourceIsAssignableToTarget(targetTypeSymbol, this.cachedFunctionInterfaceType(), targetAST, context)) {
                        if (callEx.argumentList.typeArgumentList) {
                            this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Non_generic_functions_may_not_accept_type_arguments), additionalResults, context);
                        }
                        return this.semanticInfoChain.anyTypeSymbol;
                    }

                    this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(callEx, TypeScript.DiagnosticCode.Cannot_invoke_an_expression_whose_type_lacks_a_call_signature), additionalResults, context);
                } else if (constraintDiagnostic) {
                    this.postOverloadResolutionDiagnostics(constraintDiagnostic, additionalResults, context);
                } else if (typeArgumentCountDiagnostic) {
                    this.postOverloadResolutionDiagnostics(typeArgumentCountDiagnostic, additionalResults, context);
                } else {
                    this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(callEx, TypeScript.DiagnosticCode.Could_not_select_overload_for_call_expression), additionalResults, context);
                }

                return this.getNewErrorTypeSymbol();
            }

            var signature = this.resolveOverloads(callEx, signatures, callEx.argumentList.typeArgumentList !== null, context, diagnostics);
            var useBeforeResolutionSignatures = signature == null;

            if (!signature) {
                for (var i = 0; i < diagnostics.length; i++) {
                    this.postOverloadResolutionDiagnostics(diagnostics[i], additionalResults, context);
                }

                this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Could_not_select_overload_for_call_expression), additionalResults, context);

                // Remember the error state
                // POST diagnostics
                errorCondition = this.getNewErrorTypeSymbol();

                if (!signatures.length) {
                    return errorCondition;
                }

                // Attempt to recover from the error condition
                // First, pick the first signature as the candidate signature
                signature = signatures[0];
            }

            var rootSignature = signature.getRootSymbol();
            if (!rootSignature.isGeneric() && callEx.argumentList.typeArgumentList) {
                this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Non_generic_functions_may_not_accept_type_arguments), additionalResults, context);
            } else if (rootSignature.isGeneric() && callEx.argumentList.typeArgumentList && rootSignature.getTypeParameters() && (callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount() !== rootSignature.getTypeParameters().length)) {
                this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Signature_expected_0_type_arguments_got_1_instead, [rootSignature.getTypeParameters().length, callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount()]), additionalResults, context);
            }

            var returnType = isSuperCall ? this.semanticInfoChain.voidTypeSymbol : signature.returnType;

            // contextually type arguments
            var actualParametersContextTypeSymbols = [];
            if (callEx.argumentList.arguments) {
                var len = callEx.argumentList.arguments.nonSeparatorCount();
                var params = signature.parameters;
                var contextualType = null;
                var signatureDecl = signature.getDeclarations()[0];

                for (var i = 0; i < len; i++) {
                    // account for varargs
                    if (params.length) {
                        if (i < params.length - 1 || (i < params.length && !signature.hasVarArgs)) {
                            this.resolveDeclaredSymbol(params[i], context);
                            contextualType = params[i].type;
                        } else if (signature.hasVarArgs) {
                            contextualType = params[params.length - 1].type;
                            if (contextualType.isArrayNamedTypeReference()) {
                                contextualType = contextualType.getElementType();
                            }
                        }
                    }

                    if (contextualType) {
                        context.pushNewContextualType(contextualType);
                        actualParametersContextTypeSymbols[i] = contextualType;
                    }

                    this.resolveAST(callEx.argumentList.arguments.nonSeparatorAt(i), contextualType !== null, context);

                    if (contextualType) {
                        context.popAnyContextualType();
                        contextualType = null;
                    }
                }
            }

            // Store any additional resolution results if needed before we return
            additionalResults.targetSymbol = targetSymbol;
            if (useBeforeResolutionSignatures && beforeResolutionSignatures) {
                additionalResults.resolvedSignatures = beforeResolutionSignatures;
                additionalResults.candidateSignature = beforeResolutionSignatures[0];
            } else {
                additionalResults.resolvedSignatures = signatures;
                additionalResults.candidateSignature = signature;
            }
            additionalResults.actualParametersContextTypeSymbols = actualParametersContextTypeSymbols;

            if (errorCondition) {
                return errorCondition;
            }

            if (!returnType) {
                returnType = this.semanticInfoChain.anyTypeSymbol;
            }

            return returnType;
        };

        PullTypeResolver.prototype.resolveObjectCreationExpression = function (callEx, context, additionalResults) {
            var symbol = this.getSymbolForAST(callEx, context);

            if (!symbol || !symbol.isResolved) {
                if (!additionalResults) {
                    additionalResults = new PullAdditionalCallResolutionData();
                }
                symbol = this.computeObjectCreationExpressionSymbol(callEx, context, additionalResults);
                if (this.canTypeCheckAST(callEx, context)) {
                    this.setTypeChecked(callEx, context);
                }
                this.setSymbolForAST(callEx, symbol, context);
                this.semanticInfoChain.setCallResolutionDataForAST(callEx, additionalResults);
            } else {
                if (this.canTypeCheckAST(callEx, context)) {
                    this.typeCheckObjectCreationExpression(callEx, context);
                }

                var callResolutionData = this.semanticInfoChain.getCallResolutionDataForAST(callEx);
                if (additionalResults && (callResolutionData !== additionalResults)) {
                    additionalResults.actualParametersContextTypeSymbols = callResolutionData.actualParametersContextTypeSymbols;
                    additionalResults.candidateSignature = callResolutionData.candidateSignature;
                    additionalResults.resolvedSignatures = callResolutionData.resolvedSignatures;
                    additionalResults.targetSymbol = callResolutionData.targetSymbol;
                }
            }

            return symbol;
        };

        PullTypeResolver.prototype.typeCheckObjectCreationExpression = function (callEx, context) {
            this.setTypeChecked(callEx, context);
            this.resolveAST(callEx.expression, false, context);
            var callResolutionData = this.semanticInfoChain.getCallResolutionDataForAST(callEx);
            if (callEx.argumentList) {
                var callResolutionData = this.semanticInfoChain.getCallResolutionDataForAST(callEx);
                var len = callEx.argumentList.arguments.nonSeparatorCount();

                for (var i = 0; i < len; i++) {
                    // Ensure call resolution data contains additional information.
                    // Actual parameters context type symbols will be undefined if the call target resolves to any or error types.
                    var contextualType = callResolutionData.actualParametersContextTypeSymbols ? callResolutionData.actualParametersContextTypeSymbols[i] : null;
                    if (contextualType) {
                        context.pushNewContextualType(contextualType);
                    }

                    this.resolveAST(callEx.argumentList.arguments.nonSeparatorAt(i), contextualType !== null, context);

                    if (contextualType) {
                        context.popAnyContextualType();
                        contextualType = null;
                    }
                }
            }

            for (var i = 0; i < callResolutionData.diagnosticsFromOverloadResolution.length; i++) {
                context.postDiagnostic(callResolutionData.diagnosticsFromOverloadResolution[i]);
            }
        };

        PullTypeResolver.prototype.postOverloadResolutionDiagnostics = function (diagnostic, additionalResults, context) {
            if (!context.inProvisionalResolution()) {
                additionalResults.diagnosticsFromOverloadResolution.push(diagnostic);
            }
            context.postDiagnostic(diagnostic);
        };

        PullTypeResolver.prototype.computeObjectCreationExpressionSymbol = function (callEx, context, additionalResults) {
            var _this = this;
            var returnType = null;

            // resolve the target
            var targetSymbol = this.resolveAST(callEx.expression, false, context);
            var targetTypeSymbol = targetSymbol.isType() ? targetSymbol : targetSymbol.type;

            var targetAST = this.getCallTargetErrorSpanAST(callEx);

            var constructSignatures = targetTypeSymbol.getConstructSignatures();

            var explicitTypeArgs = null;
            var usedCallSignaturesInstead = false;
            var couldNotAssignToConstraint;
            var constraintDiagnostic = null;
            var typeArgumentCountDiagnostic = null;
            var diagnostics = [];

            if (this.isAnyOrEquivalent(targetTypeSymbol)) {
                // resolve any arguments
                if (callEx.argumentList) {
                    this.resolveAST(callEx.argumentList.arguments, false, context);
                }

                return targetTypeSymbol;
            }

            if (!constructSignatures.length) {
                constructSignatures = targetTypeSymbol.getCallSignatures();
                usedCallSignaturesInstead = true;

                // if noImplicitAny flag is set to be true, report an error
                if (this.compilationSettings.noImplicitAny()) {
                    this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(callEx, TypeScript.DiagnosticCode.new_expression_which_lacks_a_constructor_signature_implicitly_has_an_any_type), additionalResults, context);
                }
            }

            if (constructSignatures.length) {
                // resolve the type arguments, specializing if necessary
                if (callEx.argumentList && callEx.argumentList.typeArgumentList) {
                    // specialize the type arguments
                    explicitTypeArgs = [];

                    if (callEx.argumentList.typeArgumentList && callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount()) {
                        for (var i = 0; i < callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount(); i++) {
                            explicitTypeArgs[i] = this.resolveTypeReference(callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorAt(i), context);
                        }
                    }
                }

                // next, walk the available signatures
                // if any are generic, and we don't have type arguments, try to infer
                // otherwise, try to specialize to the type arguments above
                if (targetTypeSymbol.isGeneric()) {
                    var resolvedSignatures = [];
                    var inferredOrExplicitTypeArgs;
                    var specializedSignature;
                    var typeParameters;
                    var typeConstraint = null;
                    var triedToInferTypeArgs;
                    var targetTypeReplacementMap = targetTypeSymbol.getTypeParameterArgumentMap();

                    for (var i = 0; i < constructSignatures.length; i++) {
                        couldNotAssignToConstraint = false;

                        if (constructSignatures[i].isGeneric()) {
                            typeParameters = constructSignatures[i].getTypeParameters();

                            if (explicitTypeArgs) {
                                // October 16, 2013: Section 4.12.2
                                // A generic signature is a candidate in a function call with type arguments
                                // arguments when:
                                // The signature has the same number of type parameters as were supplied in
                                // the type argument list
                                // ...
                                if (explicitTypeArgs.length === typeParameters.length) {
                                    inferredOrExplicitTypeArgs = explicitTypeArgs;
                                } else {
                                    typeArgumentCountDiagnostic = typeArgumentCountDiagnostic || this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Signature_expected_0_type_arguments_got_1_instead, [typeParameters.length, explicitTypeArgs.length]);
                                    continue;
                                }
                            } else if (callEx.argumentList) {
                                var typeArgumentInferenceContext = new TypeScript.InvocationTypeArgumentInferenceContext(this, context, constructSignatures[i], callEx.argumentList.arguments);
                                inferredOrExplicitTypeArgs = this.inferArgumentTypesForSignature(typeArgumentInferenceContext, new TypeComparisonInfo(), context);
                                triedToInferTypeArgs = true;
                            } else {
                                // Pretend to infer the constraints. This is useful for the following case:
                                // class C<T extends number, U> { }
                                // var c = new C; // c should have type C<number, {}>, just like calling it with new C()
                                inferredOrExplicitTypeArgs = TypeScript.ArrayUtilities.select(typeParameters, function (typeParameter) {
                                    return typeParameter.getDefaultConstraint(_this.semanticInfoChain);
                                });
                            }

                            // if we could infer Args, or we have type arguments, then attempt to specialize the signature
                            TypeScript.Debug.assert(inferredOrExplicitTypeArgs && inferredOrExplicitTypeArgs.length == typeParameters.length);

                            // When specializing the constraints, seed the replacement map with any substitutions already specified by
                            // the target function's type
                            var mutableTypeReplacementMap = new TypeScript.PullInstantiationHelpers.MutableTypeArgumentMap(targetTypeReplacementMap ? targetTypeReplacementMap : []);
                            TypeScript.PullInstantiationHelpers.updateMutableTypeParameterArgumentMap(typeParameters, inferredOrExplicitTypeArgs, mutableTypeReplacementMap);
                            var typeReplacementMap = mutableTypeReplacementMap.typeParameterArgumentMap;

                            if (explicitTypeArgs) {
                                for (var j = 0; j < typeParameters.length; j++) {
                                    typeConstraint = typeParameters[j].getConstraint();

                                    // test specialization type for assignment compatibility with the constraint
                                    if (typeConstraint) {
                                        if (typeConstraint.isGeneric()) {
                                            typeConstraint = this.instantiateType(typeConstraint, typeReplacementMap);
                                        }

                                        // Section 3.4.2 (November 18, 2013):
                                        // A type argument satisfies a type parameter constraint if the type argument is assignable
                                        // to(section 3.8.4) the constraint type once type arguments are substituted for type parameters.
                                        if (!this.sourceIsAssignableToTarget(inferredOrExplicitTypeArgs[j], typeConstraint, targetAST, context, null, true)) {
                                            var enclosingSymbol = this.getEnclosingSymbolForAST(targetAST);
                                            constraintDiagnostic = this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Type_0_does_not_satisfy_the_constraint_1_for_type_parameter_2, [inferredOrExplicitTypeArgs[j].toString(enclosingSymbol, true), typeConstraint.toString(enclosingSymbol, true), typeParameters[j].toString(enclosingSymbol, true)]);
                                            couldNotAssignToConstraint = true;
                                        }

                                        if (couldNotAssignToConstraint) {
                                            break;
                                        }
                                    }
                                }
                            }

                            if (couldNotAssignToConstraint) {
                                continue;
                            }

                            specializedSignature = this.instantiateSignature(constructSignatures[i], typeReplacementMap);

                            if (specializedSignature) {
                                resolvedSignatures[resolvedSignatures.length] = specializedSignature;
                            }
                        } else {
                            if (!(callEx.argumentList && callEx.argumentList.typeArgumentList && callEx.argumentList.typeArgumentList.typeArguments.nonSeparatorCount())) {
                                resolvedSignatures[resolvedSignatures.length] = constructSignatures[i];
                            }
                        }
                    }

                    // PULLTODO: Try to avoid copying here...
                    constructSignatures = resolvedSignatures;
                }

                var signature = this.resolveOverloads(callEx, constructSignatures, callEx.argumentList && callEx.argumentList.typeArgumentList !== null, context, diagnostics);

                // Store any additional resolution results if needed before we return
                additionalResults.targetSymbol = targetSymbol;
                additionalResults.resolvedSignatures = constructSignatures;
                additionalResults.candidateSignature = signature;
                additionalResults.actualParametersContextTypeSymbols = [];

                if (!constructSignatures.length) {
                    if (constraintDiagnostic) {
                        this.postOverloadResolutionDiagnostics(constraintDiagnostic, additionalResults, context);
                    } else if (typeArgumentCountDiagnostic) {
                        this.postOverloadResolutionDiagnostics(typeArgumentCountDiagnostic, additionalResults, context);
                    }

                    return this.getNewErrorTypeSymbol();
                }

                var errorCondition = null;

                // if we haven't been able to choose an overload, default to the first one
                if (!signature) {
                    for (var i = 0; i < diagnostics.length; i++) {
                        this.postOverloadResolutionDiagnostics(diagnostics[i], additionalResults, context);
                    }

                    this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Could_not_select_overload_for_new_expression), additionalResults, context);

                    // Remember the error
                    errorCondition = this.getNewErrorTypeSymbol();

                    if (!constructSignatures.length) {
                        // POST diagnostics
                        return errorCondition;
                    }

                    // First, pick the first signature as the candidate signature
                    signature = constructSignatures[0];
                }

                returnType = signature.returnType;

                // if it's a default constructor, and we have a type argument, we need to specialize
                if (returnType && !signature.isGeneric() && returnType.isGeneric() && !returnType.getIsSpecialized()) {
                    if (explicitTypeArgs && explicitTypeArgs.length) {
                        returnType = this.createInstantiatedType(returnType, explicitTypeArgs);
                    } else {
                        returnType = this.instantiateTypeToAny(returnType, context);
                    }
                }

                if (usedCallSignaturesInstead) {
                    if (returnType !== this.semanticInfoChain.voidTypeSymbol) {
                        this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Call_signatures_used_in_a_new_expression_must_have_a_void_return_type), additionalResults, context);

                        // POST diagnostics
                        return this.getNewErrorTypeSymbol();
                    } else {
                        returnType = this.semanticInfoChain.anyTypeSymbol;
                    }
                }

                if (!returnType) {
                    returnType = signature.returnType;

                    if (!returnType) {
                        returnType = targetTypeSymbol;
                    }
                }

                // contextually type arguments
                var actualParametersContextTypeSymbols = [];
                if (callEx.argumentList && callEx.argumentList.arguments) {
                    var len = callEx.argumentList.arguments.nonSeparatorCount();
                    var params = signature.parameters;
                    var contextualType = null;
                    var signatureDecl = signature.getDeclarations()[0];

                    for (var i = 0; i < len; i++) {
                        if (params.length) {
                            if (i < params.length - 1 || (i < params.length && !signature.hasVarArgs)) {
                                this.resolveDeclaredSymbol(params[i], context);
                                contextualType = params[i].type;
                            } else if (signature.hasVarArgs) {
                                contextualType = params[params.length - 1].type;
                                if (contextualType.isArrayNamedTypeReference()) {
                                    contextualType = contextualType.getElementType();
                                }
                            }
                        }

                        if (contextualType) {
                            context.pushNewContextualType(contextualType);
                            actualParametersContextTypeSymbols[i] = contextualType;
                        }

                        this.resolveAST(callEx.argumentList.arguments.nonSeparatorAt(i), contextualType !== null, context);

                        if (contextualType) {
                            context.popAnyContextualType();
                            contextualType = null;
                        }
                    }
                }

                // Store any additional resolution results if needed before we return
                additionalResults.targetSymbol = targetSymbol;
                additionalResults.resolvedSignatures = constructSignatures;
                additionalResults.candidateSignature = signature;
                additionalResults.actualParametersContextTypeSymbols = actualParametersContextTypeSymbols;

                if (errorCondition) {
                    // POST diagnostics
                    return errorCondition;
                }

                if (!returnType) {
                    returnType = this.semanticInfoChain.anyTypeSymbol;
                }

                return returnType;
            } else if (callEx.argumentList) {
                this.resolveAST(callEx.argumentList.arguments, false, context);
            }

            this.postOverloadResolutionDiagnostics(this.semanticInfoChain.diagnosticFromAST(targetAST, TypeScript.DiagnosticCode.Invalid_new_expression), additionalResults, context);

            // POST diagnostics
            return this.getNewErrorTypeSymbol();
        };

        // Here, we are instantiating signatureAToInstantiate in the context of contextSignatureB.
        // The A and B reference section 3.8.5 of the spec:
        // A is instantiated in the context of B. If A is a non-generic signature, the result of
        // this process is simply A. Otherwise, type arguments for A are inferred from B producing
        // an instantiation of A that can be related to B
        // The shouldFixContextualSignatureParameterTypes flag should be set to true when inferences
        // are being made for type parameters of the contextual signature (like for inferential
        // typing in section 4.12.2). It should be set to false for relation checking (sections
        // 3.8.3 & 3.8.4).
        PullTypeResolver.prototype.instantiateSignatureInContext = function (signatureAToInstantiate, contextualSignatureB, context, shouldFixContextualSignatureParameterTypes) {
            var typeReplacementMap = [];
            var inferredTypeArgs;
            var specializedSignature;
            var typeParameters = signatureAToInstantiate.getTypeParameters();
            var typeConstraint = null;

            var typeArgumentInferenceContext = new TypeScript.ContextualSignatureInstantiationTypeArgumentInferenceContext(this, context, signatureAToInstantiate, contextualSignatureB, shouldFixContextualSignatureParameterTypes);
            inferredTypeArgs = this.inferArgumentTypesForSignature(typeArgumentInferenceContext, new TypeComparisonInfo(), context);

            var functionTypeA = signatureAToInstantiate.functionType;
            var functionTypeB = contextualSignatureB.functionType;
            var enclosingTypeParameterMap;

            if (functionTypeA) {
                enclosingTypeParameterMap = functionTypeA.getTypeParameterArgumentMap();

                for (var id in enclosingTypeParameterMap) {
                    typeReplacementMap[id] = enclosingTypeParameterMap[id];
                }
            }

            if (functionTypeB) {
                enclosingTypeParameterMap = functionTypeB.getTypeParameterArgumentMap();

                for (var id in enclosingTypeParameterMap) {
                    typeReplacementMap[id] = enclosingTypeParameterMap[id];
                }
            }

            TypeScript.PullInstantiationHelpers.updateTypeParameterArgumentMap(typeParameters, inferredTypeArgs, typeReplacementMap);

            return this.instantiateSignature(signatureAToInstantiate, typeReplacementMap);
        };

        PullTypeResolver.prototype.resolveCastExpression = function (assertionExpression, context) {
            var typeAssertionType = this.resolveTypeReference(assertionExpression.type, context).type;

            if (this.canTypeCheckAST(assertionExpression, context)) {
                this.typeCheckCastExpression(assertionExpression, context, typeAssertionType);
            }

            // October 11, 2013:
            // In a type assertion expression of the form < T > e, e is contextually typed (section
            // 4.19) by T and the resulting type of e is required to be assignable to or from T, or
            // otherwise a compile - time error occurs.
            //
            // The type of the result is T.
            return typeAssertionType;
        };

        PullTypeResolver.prototype.typeCheckCastExpression = function (assertionExpression, context, typeAssertionType) {
            this.setTypeChecked(assertionExpression, context);

            // October 11, 2013:
            // In a type assertion expression of the form < T > e, e is contextually typed (section
            // 4.19) by T and the resulting type of e is required to be assignable to or from T, or
            // otherwise a compile - time error occurs.
            //
            // The type of the result is T.
            context.pushNewContextualType(typeAssertionType);
            var exprType = this.resolveAST(assertionExpression.expression, true, context).type;
            context.popAnyContextualType();

            // TODO: why are we resolving these symbols here?
            this.resolveDeclaredSymbol(typeAssertionType, context);
            this.resolveDeclaredSymbol(exprType, context);

            var comparisonInfo = new TypeComparisonInfo();

            // Be careful to check exprType assignable to typeAssertionType first. Only widen if
            // this direction is not assignable to avoid extra implicit any errors that widening
            // would cause.
            var isAssignable = this.sourceIsAssignableToTarget(exprType, typeAssertionType, assertionExpression, context, comparisonInfo);

            if (!isAssignable) {
                var widenedExprType = exprType.widenedType(this, assertionExpression.expression, context);
                isAssignable = this.sourceIsAssignableToTarget(typeAssertionType, widenedExprType, assertionExpression, context, comparisonInfo);
            }

            if (!isAssignable) {
                var enclosingSymbol = this.getEnclosingSymbolForAST(assertionExpression);
                if (comparisonInfo.message) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(assertionExpression, TypeScript.DiagnosticCode.Cannot_convert_0_to_1_NL_2, [exprType.toString(enclosingSymbol), typeAssertionType.toString(enclosingSymbol), comparisonInfo.message]));
                } else {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(assertionExpression, TypeScript.DiagnosticCode.Cannot_convert_0_to_1, [exprType.toString(enclosingSymbol), typeAssertionType.toString(enclosingSymbol)]));
                }
            }
        };

        PullTypeResolver.prototype.resolveAssignmentExpression = function (binaryExpression, context) {
            // September 17, 2013: An assignment of the form
            //
            //      VarExpr = ValueExpr
            //
            // requires VarExpr to be classified as a reference(section 4.1).ValueExpr is
            // contextually typed(section 4.19) by the type of VarExpr, and the type of ValueExpr
            // must be assignable to(section 3.8.4) the type of VarExpr, or otherwise a compile -
            // time error occurs.The result is a value with the type of ValueExpr.
            var leftExpr = this.resolveAST(binaryExpression.left, false, context);
            var leftType = leftExpr.type;

            context.pushNewContextualType(leftType);
            var rightType = this.resolveAST(binaryExpression.right, true, context).type;
            context.popAnyContextualType();

            rightType = this.getInstanceTypeForAssignment(binaryExpression.left, rightType, context);

            // Check if LHS is a valid target
            if (this.canTypeCheckAST(binaryExpression, context)) {
                this.setTypeChecked(binaryExpression, context);

                if (!this.isReference(binaryExpression.left, leftExpr)) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(binaryExpression.left, TypeScript.DiagnosticCode.Invalid_left_hand_side_of_assignment_expression));
                } else {
                    this.checkAssignability(binaryExpression.left, rightType, leftExpr.type, context);
                }
            }

            return rightType;
        };

        PullTypeResolver.prototype.getInstanceTypeForAssignment = function (lhs, type, context) {
            var typeToReturn = type;
            if (typeToReturn && typeToReturn.isAlias()) {
                typeToReturn = typeToReturn.getExportAssignedTypeSymbol();
            }

            if (typeToReturn && typeToReturn.isContainer() && !typeToReturn.isEnum()) {
                var instanceTypeSymbol = typeToReturn.getInstanceType();

                if (!instanceTypeSymbol) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(lhs, TypeScript.DiagnosticCode.Tried_to_set_variable_type_to_uninitialized_module_type_0, [type.toString()]));
                    typeToReturn = null;
                } else {
                    typeToReturn = instanceTypeSymbol;
                }
            }

            return typeToReturn;
        };

        PullTypeResolver.prototype.widenType = function (type, ast, context) {
            if (type === this.semanticInfoChain.undefinedTypeSymbol || type === this.semanticInfoChain.nullTypeSymbol || type.isError()) {
                return this.semanticInfoChain.anyTypeSymbol;
            }

            if (type.isArrayNamedTypeReference()) {
                return this.widenArrayType(type, ast, context);
            } else if (type.kind === TypeScript.PullElementKind.ObjectLiteral) {
                return this.widenObjectLiteralType(type, ast, context);
            }

            return type;
        };

        PullTypeResolver.prototype.widenArrayType = function (type, ast, context) {
            // Call into the symbol to get the widened type so we pick up the cached version if there is one
            var elementType = type.getElementType().widenedType(this, ast, context);

            if (this.compilationSettings.noImplicitAny() && ast) {
                // If we widened from non-'any' type to 'any', then report error.
                if (elementType === this.semanticInfoChain.anyTypeSymbol && type.getElementType() !== this.semanticInfoChain.anyTypeSymbol) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Array_Literal_implicitly_has_an_any_type_from_widening));
                }
            }

            return this.getArrayType(elementType);
        };

        PullTypeResolver.prototype.widenObjectLiteralType = function (type, ast, context) {
            if (!this.needsToWidenObjectLiteralType(type, ast, context)) {
                return type;
            }

            // The name here should be "", and the kind should be ObjectLiteral
            TypeScript.Debug.assert(type.name === "");
            var newObjectTypeSymbol = new TypeScript.PullTypeSymbol(type.name, type.kind);
            var declsOfObjectType = type.getDeclarations();
            TypeScript.Debug.assert(declsOfObjectType.length === 1);
            newObjectTypeSymbol.addDeclaration(declsOfObjectType[0]);
            var members = type.getMembers();

            for (var i = 0; i < members.length; i++) {
                var memberType = members[i].type;

                // Call into the symbol to get the widened type so we pick up the cached version if there is one
                var widenedMemberType = members[i].type.widenedType(this, ast, context);
                var newMember = new TypeScript.PullSymbol(members[i].name, members[i].kind);

                // Since this is an object literal, we only care about one decl for this member.
                // It has more than one decl in an error case where there was a duplicate member
                // declaration. For example:
                // var obj = { a: 3, a: 4 };
                // This will have two decls for "a", but an error is already given in computeObjectLiteralExpression.
                var declsOfMember = members[i].getDeclarations();

                //Debug.assert(declsOfMember.length === 1);
                //Debug.assert(members[i].isResolved);
                newMember.addDeclaration(declsOfMember[0]);
                newMember.type = widenedMemberType;
                newObjectTypeSymbol.addMember(newMember);
                newMember.setResolved();

                if (this.compilationSettings.noImplicitAny() && ast && widenedMemberType === this.semanticInfoChain.anyTypeSymbol && memberType !== this.semanticInfoChain.anyTypeSymbol) {
                    // Property was widened from non-any to any
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Object_literal_s_property_0_implicitly_has_an_any_type_from_widening, [members[i].name]));
                }
            }

            var indexers = type.getIndexSignatures();
            for (var i = 0; i < indexers.length; i++) {
                var newIndexer = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.IndexSignature);
                var parameter = indexers[i].parameters[0];
                var newParameter = new TypeScript.PullSymbol(parameter.name, parameter.kind);
                newParameter.type = parameter.type;
                newIndexer.addParameter(newParameter);
                newIndexer.returnType = indexers[i].returnType;
                newObjectTypeSymbol.addIndexSignature(newIndexer);
            }

            return newObjectTypeSymbol;
        };

        PullTypeResolver.prototype.needsToWidenObjectLiteralType = function (type, ast, context) {
            var members = type.getMembers();
            for (var i = 0; i < members.length; i++) {
                var memberType = members[i].type;
                if (memberType !== memberType.widenedType(this, ast, context)) {
                    return true;
                }
            }

            return false;
        };

        PullTypeResolver.prototype.findBestCommonType = function (collection, context, comparisonInfo) {
            var len = collection.getLength();

            for (var i = 0; i < len; i++) {
                var candidateType = collection.getTypeAtIndex(i);
                if (this.typeIsBestCommonTypeCandidate(candidateType, collection, context)) {
                    return candidateType;
                }
            }

            // October 16, 2013: It is possible that no such [common] type exists, in which case
            // the best common type is an empty object type.
            return this.semanticInfoChain.emptyTypeSymbol;
        };

        PullTypeResolver.prototype.typeIsBestCommonTypeCandidate = function (candidateType, collection, context) {
            for (var i = 0; i < collection.getLength(); i++) {
                var otherType = collection.getTypeAtIndex(i);
                if (candidateType === otherType) {
                    continue;
                }

                if (!this.sourceIsSubtypeOfTarget(otherType, candidateType, null, context)) {
                    return false;
                }
            }

            return true;
        };

        // Type Identity
        PullTypeResolver.prototype.typesAreIdenticalInEnclosingTypes = function (t1, t2, context) {
            t1 = this.getSymbolForRelationshipCheck(t1);
            t2 = this.getSymbolForRelationshipCheck(t2);

            if (t1 === t2) {
                return true;
            }

            if (t1 && t2) {
                // Section 3.8.7 - Recursive Types
                //  When comparing two types S and T for identity(section 3.8.2), subtype(section 3.8.3), and assignability(section 3.8.4) relationships,
                //  if either type originates in an infinitely expanding type reference, S and T are not compared by the rules in the preceding sections.Instead, for the relationship to be considered true,
                //  -	S and T must both be type references to the same named type, and
                //  -	the relationship in question must be true for each corresponding pair of type arguments in the type argument lists of S and T.
                if (context.oneOfClassificationsIsInfinitelyExpanding()) {
                    return this.infinitelyExpandingTypesAreIdentical(t1, t2, context);
                }
            }

            return this.typesAreIdentical(t1, t2, context);
        };

        PullTypeResolver.prototype.typesAreIdenticalWithNewEnclosingTypes = function (t1, t2, context) {
            var enclosingTypeWalkers = context.resetEnclosingTypeWalkers();
            var areTypesIdentical = this.typesAreIdentical(t1, t2, context);
            context.setEnclosingTypeWalkers(enclosingTypeWalkers);
            return areTypesIdentical;
        };

        PullTypeResolver.prototype.typesAreIdentical = function (t1, t2, context) {
            t1 = this.getSymbolForRelationshipCheck(t1);
            t2 = this.getSymbolForRelationshipCheck(t2);

            // This clause will cover both primitive types (since the type objects are shared),
            // as well as shared brands
            if (t1 === t2) {
                return true;
            }

            if (!t1 || !t2) {
                return false;
            }

            // identity check for enums is 't1 === t2'
            // if it returns false and one of elements is enum - they are not identical
            if (TypeScript.hasFlag(t1.kind, TypeScript.PullElementKind.Enum) || TypeScript.hasFlag(t2.kind, TypeScript.PullElementKind.Enum)) {
                return false;
            }

            if (t1.isPrimitive() && t1.isStringConstant() && t2.isPrimitive() && t2.isStringConstant()) {
                // Both are string constants
                return TypeScript.stripStartAndEndQuotes(t1.name) === TypeScript.stripStartAndEndQuotes(t2.name);
            }

            if (t1.isPrimitive() || t2.isPrimitive()) {
                return false;
            }

            if (t1.isError() && t2.isError()) {
                return true;
            }

            var isIdentical = this.identicalCache.valueAt(t1.pullSymbolID, t2.pullSymbolID);
            if (isIdentical != undefined) {
                return isIdentical;
            }

            if (t1.isTypeParameter() !== t2.isTypeParameter()) {
                return false;
            } else if (t1.isTypeParameter()) {
                // We compare parent declarations instead of container symbols because type parameter symbols are shared
                // accross overload groups
                var t1ParentDeclaration = t1.getDeclarations()[0].getParentDecl();
                var t2ParentDeclaration = t2.getDeclarations()[0].getParentDecl();

                if (t1ParentDeclaration === t2ParentDeclaration) {
                    return this.symbolsShareDeclaration(t1, t2);
                } else {
                    return false;
                }
            }

            if (t1.isPrimitive() !== t2.isPrimitive()) {
                return false;
            }

            this.identicalCache.setValueAt(t1.pullSymbolID, t2.pullSymbolID, true);
            var symbolsWhenStartedWalkingTypes = context.startWalkingTypes(t1, t2);
            isIdentical = this.typesAreIdenticalWorker(t1, t2, context);
            context.endWalkingTypes(symbolsWhenStartedWalkingTypes);
            this.identicalCache.setValueAt(t1.pullSymbolID, t2.pullSymbolID, isIdentical);

            return isIdentical;
        };

        PullTypeResolver.prototype.typesAreIdenticalWorker = function (t1, t2, context) {
            if (t1.getIsSpecialized() && t2.getIsSpecialized()) {
                // If types are specialized from same root symbol, comparing type arguments should be enough
                if (TypeScript.PullHelpers.getRootType(t1) === TypeScript.PullHelpers.getRootType(t2) && TypeScript.PullHelpers.getRootType(t1).isNamedTypeSymbol()) {
                    var t1TypeArguments = t1.getTypeArguments();
                    var t2TypeArguments = t2.getTypeArguments();

                    if (t1TypeArguments && t2TypeArguments) {
                        for (var i = 0; i < t1TypeArguments.length; i++) {
                            // The type arguments are not enclosed in current enclosing contexts
                            if (!this.typesAreIdenticalWithNewEnclosingTypes(t1TypeArguments[i], t2TypeArguments[i], context)) {
                                return false;
                            }
                        }
                    }

                    return true;
                }
            }

            // properties are identical in name, optionality, and type
            if (t1.hasMembers() && t2.hasMembers()) {
                var t1Members = t1.getAllMembers(TypeScript.PullElementKind.SomeValue, 0 /* all */);
                var t2Members = t2.getAllMembers(TypeScript.PullElementKind.SomeValue, 0 /* all */);

                if (t1Members.length !== t2Members.length) {
                    return false;
                }

                var t1MemberSymbol = null;
                var t2MemberSymbol = null;

                var t1MemberType = null;
                var t2MemberType = null;

                for (var iMember = 0; iMember < t1Members.length; iMember++) {
                    // Spec section 3.8.2:
                    // Two members are considered identical when
                    // they are public properties with identical names, optionality, and types,
                    // they are private properties originating in the same declaration and having identical types
                    t1MemberSymbol = t1Members[iMember];
                    t2MemberSymbol = this.getNamedPropertySymbol(t1MemberSymbol.name, TypeScript.PullElementKind.SomeValue, t2);

                    if (!this.propertiesAreIdentical(t1MemberSymbol, t2MemberSymbol, context)) {
                        return false;
                    }
                }
            } else if (t1.hasMembers() || t2.hasMembers()) {
                return false;
            }

            var t1CallSigs = t1.getCallSignatures();
            var t2CallSigs = t2.getCallSignatures();

            var t1ConstructSigs = t1.getConstructSignatures();
            var t2ConstructSigs = t2.getConstructSignatures();

            var t1IndexSigs = t1.getIndexSignatures();
            var t2IndexSigs = t2.getIndexSignatures();

            if (!this.signatureGroupsAreIdentical(t1CallSigs, t2CallSigs, context)) {
                return false;
            }

            if (!this.signatureGroupsAreIdentical(t1ConstructSigs, t2ConstructSigs, context)) {
                return false;
            }

            if (!this.signatureGroupsAreIdentical(t1IndexSigs, t2IndexSigs, context)) {
                return false;
            }

            return true;
        };

        PullTypeResolver.prototype.propertiesAreIdentical = function (propertySymbol1, propertySymbol2, context) {
            // Spec section 3.8.2:
            // Two members are considered identical when
            // they are public properties with identical names, optionality, and types,
            // they are private properties originating in the same declaration and having identical types
            if (!propertySymbol2 || (propertySymbol1.isOptional !== propertySymbol2.isOptional)) {
                return false;
            }

            var t1MemberSymbolIsPrivate = propertySymbol1.anyDeclHasFlag(TypeScript.PullElementFlags.Private);
            var t2MemberSymbolIsPrivate = propertySymbol2.anyDeclHasFlag(TypeScript.PullElementFlags.Private);

            // if visibility doesn't match, the types don't match
            if (t1MemberSymbolIsPrivate !== t2MemberSymbolIsPrivate) {
                return false;
            } else if (t2MemberSymbolIsPrivate && t1MemberSymbolIsPrivate) {
                var t1MemberSymbolDecl = propertySymbol1.getDeclarations()[0];
                var sourceDecl = propertySymbol2.getDeclarations()[0];
                if (t1MemberSymbolDecl !== sourceDecl) {
                    return false;
                }
            }

            var t1MemberType = propertySymbol1.type;
            var t2MemberType = propertySymbol2.type;

            context.walkMemberTypes(propertySymbol1.name);
            var areMemberTypesIdentical = this.typesAreIdenticalInEnclosingTypes(t1MemberType, t2MemberType, context);
            context.postWalkMemberTypes();
            return areMemberTypesIdentical;
        };

        PullTypeResolver.prototype.propertiesAreIdenticalWithNewEnclosingTypes = function (type1, type2, property1, property2, context) {
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(type1, type2);
            var arePropertiesIdentical = this.propertiesAreIdentical(property1, property2, context);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return arePropertiesIdentical;
        };

        PullTypeResolver.prototype.signatureGroupsAreIdentical = function (sg1, sg2, context) {
            // covers the null case
            if (sg1 === sg2) {
                return true;
            }

            // covers the mixed-null case
            if (!sg1 || !sg2) {
                return false;
            }

            if (sg1.length !== sg2.length) {
                return false;
            }

            for (var i = 0; i < sg1.length; i++) {
                context.walkSignatures(sg1[i].kind, i);
                var areSignaturesIdentical = this.signaturesAreIdentical(sg1[i], sg2[i], context, true);
                context.postWalkSignatures();
                if (!areSignaturesIdentical) {
                    return false;
                }
            }

            return true;
        };

        PullTypeResolver.prototype.typeParametersAreIdentical = function (tp1, tp2, context) {
            // Set the cache pairwise identity of type parameters so that
            // if the constraints refer to the type parameters, we would be able to say true
            var typeParamsAreIdentical = this.typeParametersAreIdenticalWorker(tp1, tp2, context);

            // Reset the cahce with pairwise identity of type parameters
            this.setTypeParameterIdentity(tp1, tp2, undefined);

            return typeParamsAreIdentical;
        };

        PullTypeResolver.prototype.typeParametersAreIdenticalWorker = function (tp1, tp2, context) {
            // Check if both the type parameter list present or both are absent
            if (!!(tp1 && tp1.length) !== !!(tp2 && tp2.length)) {
                return false;
            }

            // Verify the legth
            if (tp1 && tp2 && (tp1.length !== tp2.length)) {
                return false;
            }

            if (tp1 && tp2) {
                for (var i = 0; i < tp1.length; i++) {
                    // Verify the pairwise identity of the constraints
                    context.walkTypeParameterConstraints(i);
                    var areConstraintsIdentical = this.typesAreIdentical(tp1[i].getConstraint(), tp2[i].getConstraint(), context);
                    context.postWalkTypeParameterConstraints();
                    if (!areConstraintsIdentical) {
                        return false;
                    }
                }
            }

            return true;
        };

        PullTypeResolver.prototype.setTypeParameterIdentity = function (tp1, tp2, val) {
            if (tp1 && tp2 && tp1.length === tp2.length) {
                for (var i = 0; i < tp1.length; i++) {
                    this.identicalCache.setValueAt(tp1[i].pullSymbolID, tp2[i].pullSymbolID, val);
                }
            }
        };

        PullTypeResolver.prototype.signaturesAreIdenticalWithNewEnclosingTypes = function (s1, s2, context, includingReturnType) {
            if (typeof includingReturnType === "undefined") { includingReturnType = true; }
            // If signatures are identitical is called directally we need to get the enclosingType and
            // current symbol correctly
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(s1, s2);
            var areSignaturesIdentical = this.signaturesAreIdentical(s1, s2, context, includingReturnType);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return areSignaturesIdentical;
        };

        PullTypeResolver.prototype.signaturesAreIdentical = function (s1, s2, context, includingReturnType) {
            if (typeof includingReturnType === "undefined") { includingReturnType = true; }
            if (s1 === s2) {
                return true;
            }

            var signaturesIdentical = this.identicalCache.valueAt(s1.pullSymbolID, s2.pullSymbolID);
            if (signaturesIdentical || (signaturesIdentical != undefined && includingReturnType)) {
                return signaturesIdentical;
            }

            var oldValue = signaturesIdentical;
            this.identicalCache.setValueAt(s1.pullSymbolID, s2.pullSymbolID, true);

            signaturesIdentical = this.signaturesAreIdenticalWorker(s1, s2, context, includingReturnType);

            if (includingReturnType) {
                // Can cache the result
                this.identicalCache.setValueAt(s1.pullSymbolID, s2.pullSymbolID, signaturesIdentical);
            } else {
                // Not checking return type, revert the result
                this.identicalCache.setValueAt(s1.pullSymbolID, s2.pullSymbolID, oldValue);
            }

            return signaturesIdentical;
        };

        PullTypeResolver.prototype.signaturesAreIdenticalWorker = function (s1, s2, context, includingReturnType) {
            if (typeof includingReturnType === "undefined") { includingReturnType = true; }
            if (s1.hasVarArgs !== s2.hasVarArgs) {
                return false;
            }

            if (s1.nonOptionalParamCount !== s2.nonOptionalParamCount) {
                return false;
            }

            if (s1.parameters.length !== s2.parameters.length) {
                return false;
            }

            // Assume typeParameter pairwise identity before we check
            var s1TypeParameters = s1.getTypeParameters();
            var s2TypeParameters = s2.getTypeParameters();
            this.setTypeParameterIdentity(s1TypeParameters, s2TypeParameters, true);

            var typeParametersParametersAndReturnTypesAreIdentical = this.signatureTypeParametersParametersAndReturnTypesAreIdentical(s1, s2, context, includingReturnType);

            // Reset the cahce with pairwise identity of type parameters
            this.setTypeParameterIdentity(s1TypeParameters, s2TypeParameters, undefined);
            return typeParametersParametersAndReturnTypesAreIdentical;
        };

        PullTypeResolver.prototype.signatureTypeParametersParametersAndReturnTypesAreIdentical = function (s1, s2, context, includingReturnType) {
            if (!this.typeParametersAreIdenticalWorker(s1.getTypeParameters(), s2.getTypeParameters(), context)) {
                return false;
            }

            if (includingReturnType) {
                TypeScript.PullHelpers.resolveDeclaredSymbolToUseType(s1);
                TypeScript.PullHelpers.resolveDeclaredSymbolToUseType(s2);
                context.walkReturnTypes();
                var areReturnTypesIdentical = this.typesAreIdenticalInEnclosingTypes(s1.returnType, s2.returnType, context);
                context.postWalkReturnTypes();
                if (!areReturnTypesIdentical) {
                    return false;
                }
            }

            var s1Params = s1.parameters;
            var s2Params = s2.parameters;

            for (var iParam = 0; iParam < s1Params.length; iParam++) {
                TypeScript.PullHelpers.resolveDeclaredSymbolToUseType(s1Params[iParam]);
                TypeScript.PullHelpers.resolveDeclaredSymbolToUseType(s2Params[iParam]);
                context.walkParameterTypes(iParam);
                var areParameterTypesIdentical = this.typesAreIdenticalInEnclosingTypes(s1Params[iParam].type, s2Params[iParam].type, context);
                context.postWalkParameterTypes();

                if (!areParameterTypesIdentical) {
                    return false;
                }
            }

            return true;
        };

        PullTypeResolver.prototype.signatureReturnTypesAreIdentical = function (s1, s2, context) {
            // Set the cache pairwise identity of type parameters, so if parameters refer to them, they would be treated as identical
            var s1TypeParameters = s1.getTypeParameters();
            var s2TypeParameters = s2.getTypeParameters();
            this.setTypeParameterIdentity(s1TypeParameters, s2TypeParameters, true);

            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(s1, s2);
            context.walkReturnTypes();
            var returnTypeIsIdentical = this.typesAreIdenticalInEnclosingTypes(s1.returnType, s2.returnType, context);

            // context.postWalkReturnTypes(); - this is not needed because we are restoring the old walkers
            context.setEnclosingTypeWalkers(enclosingWalkers);

            // Reset the cahce with pairwise identity of type parameters
            this.setTypeParameterIdentity(s1TypeParameters, s2TypeParameters, undefined);

            return returnTypeIsIdentical;
        };

        // Assignment Compatibility and Subtyping
        PullTypeResolver.prototype.symbolsShareDeclaration = function (symbol1, symbol2) {
            var decls1 = symbol1.getDeclarations();
            var decls2 = symbol2.getDeclarations();

            if (decls1.length && decls2.length) {
                return decls1[0] === decls2[0];
            }

            return false;
        };

        PullTypeResolver.prototype.sourceIsSubtypeOfTarget = function (source, target, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            return this.sourceIsRelatableToTarget(source, target, false, this.subtypeCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
        };

        PullTypeResolver.prototype.sourceMembersAreAssignableToTargetMembers = function (source, target, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(source, target);
            var areSourceMembersAreAssignableToTargetMembers = this.sourceMembersAreRelatableToTargetMembers(source, target, true, this.assignableCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return areSourceMembersAreAssignableToTargetMembers;
        };

        PullTypeResolver.prototype.sourcePropertyIsAssignableToTargetProperty = function (source, target, sourceProp, targetProp, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(source, target);
            var isSourcePropertyIsAssignableToTargetProperty = this.sourcePropertyIsRelatableToTargetProperty(source, target, sourceProp, targetProp, true, this.assignableCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return isSourcePropertyIsAssignableToTargetProperty;
        };

        PullTypeResolver.prototype.sourceCallSignaturesAreAssignableToTargetCallSignatures = function (source, target, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(source, target);
            var areSourceCallSignaturesAssignableToTargetCallSignatures = this.sourceCallSignaturesAreRelatableToTargetCallSignatures(source, target, true, this.assignableCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return areSourceCallSignaturesAssignableToTargetCallSignatures;
        };

        PullTypeResolver.prototype.sourceConstructSignaturesAreAssignableToTargetConstructSignatures = function (source, target, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(source, target);
            var areSourceConstructSignaturesAssignableToTargetConstructSignatures = this.sourceConstructSignaturesAreRelatableToTargetConstructSignatures(source, target, true, this.assignableCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return areSourceConstructSignaturesAssignableToTargetConstructSignatures;
        };

        PullTypeResolver.prototype.sourceIndexSignaturesAreAssignableToTargetIndexSignatures = function (source, target, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(source, target);
            var areSourceIndexSignaturesAssignableToTargetIndexSignatures = this.sourceIndexSignaturesAreRelatableToTargetIndexSignatures(source, target, true, this.assignableCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return areSourceIndexSignaturesAssignableToTargetIndexSignatures;
        };

        PullTypeResolver.prototype.typeIsAssignableToFunction = function (source, ast, context) {
            // Note that object types containing one or more call or construct signatures are
            // automatically assignable to Function, provided they do not hide properties of
            // Function, giving them incompatible types. This is a result of the apparent type
            // rules in section 3.1.
            if (source.isFunctionType()) {
                return true;
            }

            return this.cachedFunctionInterfaceType() && this.sourceIsAssignableToTarget(source, this.cachedFunctionInterfaceType(), ast, context);
        };

        PullTypeResolver.prototype.signatureIsAssignableToTarget = function (s1, s2, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            context.setEnclosingTypes(s1, s2);
            var isSignatureIsAssignableToTarget = this.signatureIsRelatableToTarget(s1, s2, true, this.assignableCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return isSignatureIsAssignableToTarget;
        };

        PullTypeResolver.prototype.sourceIsAssignableToTarget = function (source, target, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            return this.sourceIsRelatableToTarget(source, target, true, this.assignableCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
        };

        PullTypeResolver.prototype.sourceIsAssignableToTargetWithNewEnclosingTypes = function (source, target, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            return this.sourceIsRelatableToTargetWithNewEnclosingTypes(source, target, true, this.assignableCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
        };

        PullTypeResolver.prototype.getSymbolForRelationshipCheck = function (symbol) {
            if (symbol && symbol.isTypeReference()) {
                return symbol.getReferencedTypeSymbol();
            }

            return symbol;
        };

        PullTypeResolver.prototype.sourceIsRelatableToTargetInEnclosingTypes = function (source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            source = this.getSymbolForRelationshipCheck(source);
            target = this.getSymbolForRelationshipCheck(target);

            if (source === target) {
                return true;
            }

            if (source && target) {
                // Section 3.8.7 - Recursive Types
                //  When comparing two types S and T for identity(section 3.8.2), subtype(section 3.8.3), and assignability(section 3.8.4) relationships,
                //  if either type originates in an infinitely expanding type reference, S and T are not compared by the rules in the preceding sections.Instead, for the relationship to be considered true,
                //  -	S and T must both be type references to the same named type, and
                //  -	the relationship in question must be true for each corresponding pair of type arguments in the type argument lists of S and T.
                if (context.oneOfClassificationsIsInfinitelyExpanding()) {
                    return this.infinitelyExpandingSourceTypeIsRelatableToTargetType(source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
                }
            }

            return this.sourceIsRelatableToTarget(source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
        };

        PullTypeResolver.prototype.sourceIsRelatableToTargetWithNewEnclosingTypes = function (source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var enclosingWalkers = context.resetEnclosingTypeWalkers();
            var isSourceRelatable = this.sourceIsRelatableToTarget(source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
            context.setEnclosingTypeWalkers(enclosingWalkers);
            return isSourceRelatable;
        };

        PullTypeResolver.prototype.sourceIsRelatableToTargetInCache = function (source, target, comparisonCache, comparisonInfo) {
            var isRelatable = comparisonCache.valueAt(source.pullSymbolID, target.pullSymbolID);

            // If the source is relatable, return immediately
            if (isRelatable) {
                return { isRelatable: isRelatable };
            }

            // if comparison info is not asked, we can return cached false value,
            // otherwise we need to redo the check to fill in the comparison info
            if (isRelatable != undefined && !comparisonInfo) {
                return { isRelatable: isRelatable };
            }

            return null;
        };

        PullTypeResolver.prototype.sourceIsRelatableToTarget = function (source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            source = this.getSymbolForRelationshipCheck(source);
            target = this.getSymbolForRelationshipCheck(target);

            if (source === target) {
                return true;
            }

            // An error has already been reported in this case
            if (!(source && target)) {
                return true;
            }

            // Note, for subtype the apparent type rules are different for type parameters. This
            // is not yet reflected in the spec.
            var sourceApparentType = this.getApparentType(source);

            // In the case of a 'false', we want to short-circuit a recursive typecheck
            var isRelatableInfo = this.sourceIsRelatableToTargetInCache(source, target, comparisonCache, comparisonInfo);
            if (isRelatableInfo) {
                return isRelatableInfo.isRelatable;
            }

            if (source === this.semanticInfoChain.stringTypeSymbol && target.isPrimitive() && target.isStringConstant()) {
                return comparisonInfo && comparisonInfo.stringConstantVal && (comparisonInfo.stringConstantVal.kind() === 14 /* StringLiteral */) && (TypeScript.stripStartAndEndQuotes(comparisonInfo.stringConstantVal.text()) === TypeScript.stripStartAndEndQuotes(target.name));
            }

            // this is one difference between subtyping and assignment compatibility
            if (assignableTo) {
                if (this.isAnyOrEquivalent(source) || this.isAnyOrEquivalent(target)) {
                    return true;
                }
            } else {
                // This is one difference between assignment compatibility and subtyping
                if (this.isAnyOrEquivalent(target)) {
                    return true;
                }
            }

            if (target === this.semanticInfoChain.stringTypeSymbol && source.isPrimitive() && source.isStringConstant()) {
                return true;
            }

            if (source.isPrimitive() && source.isStringConstant() && target.isPrimitive() && target.isStringConstant()) {
                // Both are string constants
                return TypeScript.stripStartAndEndQuotes(source.name) === TypeScript.stripStartAndEndQuotes(target.name);
            }

            if (source === this.semanticInfoChain.undefinedTypeSymbol) {
                return true;
            }

            if ((source === this.semanticInfoChain.nullTypeSymbol) && (target !== this.semanticInfoChain.undefinedTypeSymbol && target != this.semanticInfoChain.voidTypeSymbol)) {
                return true;
            }

            if (target === this.semanticInfoChain.voidTypeSymbol) {
                if (source === this.semanticInfoChain.undefinedTypeSymbol || source == this.semanticInfoChain.nullTypeSymbol) {
                    return true;
                }

                return false;
            } else if (source === this.semanticInfoChain.voidTypeSymbol) {
                if (target === this.semanticInfoChain.anyTypeSymbol) {
                    return true;
                }

                return false;
            }

            if (target === this.semanticInfoChain.numberTypeSymbol && TypeScript.PullHelpers.symbolIsEnum(source)) {
                return true;
            }

            if (source === this.semanticInfoChain.numberTypeSymbol && TypeScript.PullHelpers.symbolIsEnum(target)) {
                return assignableTo;
            }

            if (TypeScript.PullHelpers.symbolIsEnum(target) && TypeScript.PullHelpers.symbolIsEnum(source)) {
                return this.symbolsShareDeclaration(target, source);
            }

            if ((source.kind & TypeScript.PullElementKind.Enum) || (target.kind & TypeScript.PullElementKind.Enum)) {
                return false;
            }

            // Note: this code isn't necessary, but is helpful for error reporting purposes.
            // Instead of reporting something like:
            //
            // Cannot convert 'A[]' to 'B[]':
            //  Types of property 'pop' of types 'A[]' and 'B[]' are incompatible:
            //    Call signatures of types '() => A' and '() => B' are incompatible:
            //      Type 'A' is missing property 'C' from type 'B'.
            //
            // We instead report:
            // Cannot convert 'A[]' to 'B[]':
            //   Type 'A' is missing property 'C' from type 'B'.
            if (source.getIsSpecialized() && target.getIsSpecialized()) {
                if (TypeScript.PullHelpers.getRootType(source) === TypeScript.PullHelpers.getRootType(target) && TypeScript.PullHelpers.getRootType(source).isNamedTypeSymbol()) {
                    var sourceTypeArguments = source.getTypeArguments();
                    var targetTypeArguments = target.getTypeArguments();

                    if (sourceTypeArguments && targetTypeArguments) {
                        comparisonCache.setValueAt(source.pullSymbolID, target.pullSymbolID, true);

                        for (var i = 0; i < sourceTypeArguments.length; i++) {
                            if (!this.sourceIsRelatableToTargetWithNewEnclosingTypes(sourceTypeArguments[i], targetTypeArguments[i], assignableTo, comparisonCache, ast, context, null, isComparingInstantiatedSignatures)) {
                                break;
                            }
                        }

                        if (i === sourceTypeArguments.length) {
                            return true;
                        } else {
                            comparisonCache.setValueAt(source.pullSymbolID, target.pullSymbolID, undefined);
                            // don't return from here - if we've failed, keep checking (this will allow contravariant checks against generic methods to properly pass or fail)
                        }
                    }
                }
            }

            if (target.isTypeParameter()) {
                if (source.isTypeParameter()) {
                    // SPEC Nov 18
                    // S is assignable to a type T, and T is assignable from S, if ...
                    // S and T are type parameters, and S is directly or indirectly constrained to T.
                    if (!source.getConstraint()) {
                        // if the source is another type parameter (with no constraints), they can only be assignable if they share
                        // a declaration
                        return this.typesAreIdentical(target, source, context);
                    } else {
                        return this.isSourceTypeParameterConstrainedToTargetTypeParameter(source, target);
                    }
                } else {
                    // if the source is not another type parameter, and we're specializing at a constraint site, we consider the
                    // target to be a subtype of its constraint
                    if (isComparingInstantiatedSignatures) {
                        target = target.getBaseConstraint(this.semanticInfoChain);
                    } else {
                        return this.typesAreIdentical(target, sourceApparentType, context);
                    }
                }
            }

            // this check ensures that we only operate on object types from this point forward,
            // since the checks involving primitives occurred above
            if (sourceApparentType.isPrimitive() || target.isPrimitive()) {
                // we already know that they're not the same, and that neither is 'any'
                return false;
            }

            comparisonCache.setValueAt(source.pullSymbolID, target.pullSymbolID, true);

            var symbolsWhenStartedWalkingTypes = context.startWalkingTypes(sourceApparentType, target);

            // If we were walking source and sourceSubstitution do not match, replace them in the currentSymbols
            var needsSourceSubstitutionUpdate = source != sourceApparentType && context.enclosingTypeWalker1._canWalkStructure() && context.enclosingTypeWalker1._getCurrentSymbol() != sourceApparentType;
            if (needsSourceSubstitutionUpdate) {
                context.enclosingTypeWalker1.setCurrentSymbol(sourceApparentType);
            }

            var isRelatable = this.sourceIsRelatableToTargetWorker(source, target, sourceApparentType, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);

            if (needsSourceSubstitutionUpdate) {
                context.enclosingTypeWalker1.setCurrentSymbol(source);
            }
            context.endWalkingTypes(symbolsWhenStartedWalkingTypes);

            comparisonCache.setValueAt(source.pullSymbolID, target.pullSymbolID, isRelatable);
            return isRelatable;
        };

        PullTypeResolver.prototype.isSourceTypeParameterConstrainedToTargetTypeParameter = function (source, target) {
            var current = source;
            while (current && current.isTypeParameter()) {
                if (current === target) {
                    return true;
                }

                current = current.getConstraint();
            }
            return false;
        };

        PullTypeResolver.prototype.sourceIsRelatableToTargetWorker = function (source, target, sourceSubstitution, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            if (target.hasMembers() && !this.sourceMembersAreRelatableToTargetMembers(sourceSubstitution, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures)) {
                return false;
            }

            if (!this.sourceCallSignaturesAreRelatableToTargetCallSignatures(sourceSubstitution, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures)) {
                return false;
            }

            if (!this.sourceConstructSignaturesAreRelatableToTargetConstructSignatures(sourceSubstitution, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures)) {
                return false;
            }

            if (!this.sourceIndexSignaturesAreRelatableToTargetIndexSignatures(sourceSubstitution, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures)) {
                return false;
            }

            return true;
        };

        PullTypeResolver.prototype.sourceMembersAreRelatableToTargetMembers = function (source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var targetProps = target.getAllMembers(TypeScript.PullElementKind.SomeValue, 0 /* all */);

            for (var itargetProp = 0; itargetProp < targetProps.length; itargetProp++) {
                var targetProp = targetProps[itargetProp];

                // November 18, 2013, Sections 3.8.3 + 3.8.4
                // ..., where S' denotes the apparent type (section 3.8.1) of S
                // Note that by this point, we should already have the apparent type of 'source',
                // not including augmentation, so the only thing left to do is augment the type as
                // we look for the property.
                var sourceProp = this._getNamedPropertySymbolOfAugmentedType(targetProp.name, source);

                this.resolveDeclaredSymbol(targetProp, context);

                var targetPropType = targetProp.type;

                if (!sourceProp) {
                    if (!(targetProp.isOptional)) {
                        if (comparisonInfo) {
                            var enclosingSymbol = this.getEnclosingSymbolForAST(ast);
                            comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.RequiredPropertyIsMissing;
                            comparisonInfo.addMessage(TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Type_0_is_missing_property_1_from_type_2, [source.toString(enclosingSymbol), targetProp.getScopedNameEx().toString(), target.toString(enclosingSymbol)]));
                        }
                        return false;
                    }
                    continue;
                }

                if (!this.sourcePropertyIsRelatableToTargetProperty(source, target, sourceProp, targetProp, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures)) {
                    return false;
                }
            }

            return true;
        };

        PullTypeResolver.prototype.infinitelyExpandingSourceTypeIsRelatableToTargetType = function (sourceType, targetType, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            // Section 3.8.7 - Recursive Types
            //  When comparing two types S and T for identity(section 3.8.2), subtype(section 3.8.3), and
            //  assignability(section 3.8.4) relationships,
            //  if either type originates in an infinitely expanding type reference, S and T are not compared
            //  by the rules in the preceding sections.Instead, for the relationship to be considered true,
            //  -	S and T must both be type references to the same named type, and
            //  -	the relationship in question must be true for each corresponding pair of type arguments in
            //      the type argument lists of S and T.
            var widenedTargetType = targetType.widenedType(this, null, context);
            var widenedSourceType = sourceType.widenedType(this, null, context);

            // Check if the type is not any/null or undefined
            if ((widenedSourceType !== this.semanticInfoChain.anyTypeSymbol) && (widenedTargetType !== this.semanticInfoChain.anyTypeSymbol)) {
                var sourceTypeNamedTypeReference = TypeScript.PullHelpers.getRootType(sourceType);
                var targetTypeNamedTypeReference = TypeScript.PullHelpers.getRootType(targetType);

                //  -	S and T must both be type references to the same named type, and
                if (sourceTypeNamedTypeReference !== targetTypeNamedTypeReference) {
                    comparisonCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, false);
                    if (comparisonInfo) {
                        var enclosingSymbol = this.getEnclosingSymbolForAST(ast);
                        comparisonInfo.addMessage(TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Types_0_and_1_originating_in_infinitely_expanding_type_reference_do_not_refer_to_same_named_type, [sourceType.getScopedNameEx(enclosingSymbol).toString(), targetType.toString(enclosingSymbol)]));
                    }
                    return false;
                }

                var sourceTypeArguments = sourceType.getTypeArguments();
                var targetTypeArguments = targetType.getTypeArguments();

                // Verify if all type arguments can relate
                if (!sourceTypeArguments && !targetTypeArguments) {
                    // Both interface have 0 type arguments, so they relate
                    comparisonCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, true);
                    return true;
                }

                // If the number of type arguments mismatch (because of incomplete list - types are incompatible
                if (!(sourceTypeArguments && targetTypeArguments) || sourceTypeArguments.length !== targetTypeArguments.length) {
                    comparisonCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, false);
                    if (comparisonInfo) {
                        var enclosingSymbol = this.getEnclosingSymbolForAST(ast);
                        comparisonInfo.addMessage(TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Types_0_and_1_originating_in_infinitely_expanding_type_reference_have_incompatible_type_arguments, [sourceType.toString(enclosingSymbol), targetType.toString(enclosingSymbol)]));
                    }
                    return false;
                }

                var comparisonInfoTypeArgumentsCheck = null;
                if (comparisonInfo && !comparisonInfo.onlyCaptureFirstError) {
                    comparisonInfoTypeArgumentsCheck = new TypeComparisonInfo(comparisonInfo);
                }
                var isRelatable = true;
                for (var i = 0; i < sourceTypeArguments.length && isRelatable; i++) {
                    //  -	the relationship in question must be true for each corresponding pair of type arguments
                    //      in the type argument lists of S and T.
                    context.walkTypeArgument(i);

                    if (!this.sourceIsRelatableToTargetInEnclosingTypes(sourceTypeArguments[i], targetTypeArguments[i], assignableTo, comparisonCache, ast, context, comparisonInfoTypeArgumentsCheck, isComparingInstantiatedSignatures)) {
                        isRelatable = false;
                        if (comparisonInfo) {
                            var message;
                            var enclosingSymbol = this.getEnclosingSymbolForAST(ast);

                            if (comparisonInfoTypeArgumentsCheck && comparisonInfoTypeArgumentsCheck.message) {
                                message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Types_0_and_1_originating_in_infinitely_expanding_type_reference_have_incompatible_type_arguments_NL_2, [sourceType.toString(enclosingSymbol), targetType.toString(enclosingSymbol), comparisonInfoTypeArgumentsCheck.message]);
                            } else {
                                message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Types_0_and_1_originating_in_infinitely_expanding_type_reference_have_incompatible_type_arguments, [sourceType.toString(enclosingSymbol), targetType.toString(enclosingSymbol)]);
                            }
                            comparisonInfo.addMessage(message);
                        }
                    }

                    context.postWalkTypeArgument();
                }
            }

            comparisonCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, isRelatable);
            return isRelatable;
        };

        PullTypeResolver.prototype.infinitelyExpandingTypesAreIdentical = function (sourceType, targetType, context) {
            // Section 3.8.7 - Recursive Types
            //  When comparing two types S and T for identity(section 3.8.2), subtype(section 3.8.3), and
            //  assignability(section 3.8.4) relationships,
            //  if either type originates in an infinitely expanding type reference, S and T are not compared
            //  by the rules in the preceding sections.Instead, for the relationship to be considered true,
            //  -	S and T must both be type references to the same named type, and
            //  -	the relationship in question must be true for each corresponding pair of type arguments in
            //      the type argument lists of S and T.
            var widenedTargetType = targetType.widenedType(this, null, null);
            var widenedSourceType = sourceType.widenedType(this, null, null);

            // Check if the type is not any/null or undefined
            if ((widenedSourceType !== this.semanticInfoChain.anyTypeSymbol) && (widenedTargetType !== this.semanticInfoChain.anyTypeSymbol)) {
                //  -	S and T must both be type references to the same named type, and
                var sourceTypeNamedTypeReference = TypeScript.PullHelpers.getRootType(sourceType);
                var targetTypeNamedTypeReference = TypeScript.PullHelpers.getRootType(targetType);
                if (sourceTypeNamedTypeReference !== targetTypeNamedTypeReference) {
                    this.identicalCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, false);
                    return false;
                }

                //  -	the relationship in question must be true for each corresponding pair of type arguments in
                //      the type argument lists of S and T.
                var sourceTypeArguments = sourceType.getTypeArguments();
                var targetTypeArguments = targetType.getTypeArguments();

                if (!sourceTypeArguments && !targetTypeArguments) {
                    // Both types do not refere to any type arguments so they are identical
                    this.identicalCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, true);
                    return true;
                }

                if (!(sourceTypeArguments && targetTypeArguments) || sourceTypeArguments.length !== targetTypeArguments.length) {
                    // Mismatch in type arguments length - may be missing type arguments - it is error
                    this.identicalCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, false);
                    return false;
                }

                for (var i = 0; i < sourceTypeArguments.length; i++) {
                    // Each pair of type argument needs to be identical for the type to be identical
                    context.walkTypeArgument(i);
                    var areIdentical = this.typesAreIdenticalInEnclosingTypes(sourceTypeArguments[i], targetTypeArguments[i], context);
                    context.postWalkTypeArgument();

                    if (!areIdentical) {
                        this.identicalCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, false);
                        return false;
                    }
                }
            }

            this.identicalCache.setValueAt(sourceType.pullSymbolID, targetType.pullSymbolID, true);
            return true;
        };

        PullTypeResolver.prototype.sourcePropertyIsRelatableToTargetProperty = function (source, target, sourceProp, targetProp, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var _this = this;
            var sourceAndTargetAreConstructors = source.isConstructor() && target.isConstructor();

            // source\target are not always equivalent to getContainer().
            // i.e.in cases of inheritance chains source will be derived type and getContainer() will yield some type from the middle of hierarchy
            var getNames = function (takeTypesFromPropertyContainers) {
                var enclosingSymbol = _this.getEnclosingSymbolForAST(ast);
                var sourceType = takeTypesFromPropertyContainers ? sourceProp.getContainer() : source;
                var targetType = takeTypesFromPropertyContainers ? targetProp.getContainer() : target;
                if (sourceAndTargetAreConstructors) {
                    sourceType = sourceType.getAssociatedContainerType();
                    targetType = targetType.getAssociatedContainerType();
                }
                return {
                    propertyName: targetProp.getScopedNameEx().toString(),
                    sourceTypeName: sourceType.toString(enclosingSymbol),
                    targetTypeName: targetType.toString(enclosingSymbol)
                };
            };

            var targetPropIsPrivate = targetProp.anyDeclHasFlag(TypeScript.PullElementFlags.Private);
            var sourcePropIsPrivate = sourceProp.anyDeclHasFlag(TypeScript.PullElementFlags.Private);

            // if visibility doesn't match, the types don't match
            if (targetPropIsPrivate !== sourcePropIsPrivate) {
                if (comparisonInfo) {
                    var names = getNames(true);
                    var code;
                    if (targetPropIsPrivate) {
                        // Overshadowing property in source that is already defined as private in target
                        code = sourceAndTargetAreConstructors ? TypeScript.DiagnosticCode.Static_property_0_defined_as_public_in_type_1_is_defined_as_private_in_type_2 : TypeScript.DiagnosticCode.Property_0_defined_as_public_in_type_1_is_defined_as_private_in_type_2;
                    } else {
                        // Public property of target is private in source
                        code = sourceAndTargetAreConstructors ? TypeScript.DiagnosticCode.Static_property_0_defined_as_private_in_type_1_is_defined_as_public_in_type_2 : TypeScript.DiagnosticCode.Property_0_defined_as_private_in_type_1_is_defined_as_public_in_type_2;
                    }
                    comparisonInfo.addMessage(TypeScript.getDiagnosticMessage(code, [names.propertyName, names.sourceTypeName, names.targetTypeName]));
                    comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.InconsistantPropertyAccesibility;
                }
                return false;
            } else if (sourcePropIsPrivate && targetPropIsPrivate) {
                var targetDecl = targetProp.getDeclarations()[0];
                var sourceDecl = sourceProp.getDeclarations()[0];

                if (targetDecl !== sourceDecl) {
                    if (comparisonInfo) {
                        var names = getNames(true);

                        // Both types define property with same name as private
                        comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.InconsistantPropertyAccesibility;
                        var code = sourceAndTargetAreConstructors ? TypeScript.DiagnosticCode.Types_0_and_1_define_static_property_2_as_private : TypeScript.DiagnosticCode.Types_0_and_1_define_property_2_as_private;
                        comparisonInfo.addMessage(TypeScript.getDiagnosticMessage(code, [names.sourceTypeName, names.targetTypeName, names.propertyName]));
                    }

                    return false;
                }
            }

            // If the target property is required, and the source property is optional, they are not compatible
            if (sourceProp.isOptional && !targetProp.isOptional) {
                if (comparisonInfo) {
                    var names = getNames(true);
                    comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.RequiredPropertyIsMissing;
                    comparisonInfo.addMessage(TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Property_0_defined_as_optional_in_type_1_but_is_required_in_type_2, [names.propertyName, names.sourceTypeName, names.targetTypeName]));
                }
                return false;
            }

            this.resolveDeclaredSymbol(sourceProp, context);

            var sourcePropType = sourceProp.type;
            var targetPropType = targetProp.type;

            // In the case of a 'false', we want to short-circuit a recursive typecheck
            var isRelatableInfo = this.sourceIsRelatableToTargetInCache(sourcePropType, targetPropType, comparisonCache, comparisonInfo);
            if (isRelatableInfo) {
                return isRelatableInfo.isRelatable;
            }

            var comparisonInfoPropertyTypeCheck = null;
            if (comparisonInfo && !comparisonInfo.onlyCaptureFirstError) {
                comparisonInfoPropertyTypeCheck = new TypeComparisonInfo(comparisonInfo);
            }

            context.walkMemberTypes(targetProp.name);
            var isSourcePropertyRelatableToTargetProperty = this.sourceIsRelatableToTargetInEnclosingTypes(sourcePropType, targetPropType, assignableTo, comparisonCache, ast, context, comparisonInfoPropertyTypeCheck, isComparingInstantiatedSignatures);
            context.postWalkMemberTypes();

            // Update error message correctly
            if (!isSourcePropertyRelatableToTargetProperty && comparisonInfo) {
                var enclosingSymbol = this.getEnclosingSymbolForAST(ast);
                comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.IncompatiblePropertyTypes;
                var message;
                var names = getNames(false);
                if (comparisonInfoPropertyTypeCheck && comparisonInfoPropertyTypeCheck.message) {
                    var code = sourceAndTargetAreConstructors ? TypeScript.DiagnosticCode.Types_of_static_property_0_of_class_1_and_class_2_are_incompatible_NL_3 : TypeScript.DiagnosticCode.Types_of_property_0_of_types_1_and_2_are_incompatible_NL_3;
                    message = TypeScript.getDiagnosticMessage(code, [names.propertyName, names.sourceTypeName, names.targetTypeName, comparisonInfoPropertyTypeCheck.message]);
                } else {
                    var code = sourceAndTargetAreConstructors ? TypeScript.DiagnosticCode.Types_of_static_property_0_of_class_1_and_class_2_are_incompatible : TypeScript.DiagnosticCode.Types_of_property_0_of_types_1_and_2_are_incompatible;
                    message = TypeScript.getDiagnosticMessage(code, [names.propertyName, names.sourceTypeName, names.targetTypeName]);
                }
                comparisonInfo.addMessage(message);
            }

            return isSourcePropertyRelatableToTargetProperty;
        };

        PullTypeResolver.prototype.sourceCallSignaturesAreRelatableToTargetCallSignatures = function (source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var targetCallSigs = target.getCallSignatures();

            // check signature groups
            if (targetCallSigs.length) {
                var comparisonInfoSignatuesTypeCheck = null;
                if (comparisonInfo && !comparisonInfo.onlyCaptureFirstError) {
                    comparisonInfoSignatuesTypeCheck = new TypeComparisonInfo(comparisonInfo);
                }

                var sourceCallSigs = source.getCallSignatures();
                if (!this.signatureGroupIsRelatableToTarget(source, target, sourceCallSigs, targetCallSigs, assignableTo, comparisonCache, ast, context, comparisonInfoSignatuesTypeCheck, isComparingInstantiatedSignatures)) {
                    if (comparisonInfo) {
                        var message;
                        var enclosingSymbol = this.getEnclosingSymbolForAST(ast);
                        if (sourceCallSigs.length && targetCallSigs.length) {
                            if (comparisonInfoSignatuesTypeCheck && comparisonInfoSignatuesTypeCheck.message) {
                                message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Call_signatures_of_types_0_and_1_are_incompatible_NL_2, [source.toString(enclosingSymbol), target.toString(enclosingSymbol), comparisonInfoSignatuesTypeCheck.message]);
                            } else {
                                message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Call_signatures_of_types_0_and_1_are_incompatible, [source.toString(enclosingSymbol), target.toString(enclosingSymbol)]);
                            }
                        } else {
                            var hasSig = targetCallSigs.length ? target.toString(enclosingSymbol) : source.toString(enclosingSymbol);
                            var lacksSig = !targetCallSigs.length ? target.toString(enclosingSymbol) : source.toString(enclosingSymbol);
                            message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Type_0_requires_a_call_signature_but_type_1_lacks_one, [hasSig, lacksSig]);
                        }
                        comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.IncompatibleSignatures;
                        comparisonInfo.addMessage(message);
                    }
                    return false;
                }
            }

            return true;
        };

        PullTypeResolver.prototype.sourceConstructSignaturesAreRelatableToTargetConstructSignatures = function (source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            // check signature groups
            var targetConstructSigs = target.getConstructSignatures();
            if (targetConstructSigs.length) {
                var comparisonInfoSignatuesTypeCheck = null;
                if (comparisonInfo && !comparisonInfo.onlyCaptureFirstError) {
                    comparisonInfoSignatuesTypeCheck = new TypeComparisonInfo(comparisonInfo);
                }

                var sourceConstructSigs = source.getConstructSignatures();
                if (!this.signatureGroupIsRelatableToTarget(source, target, sourceConstructSigs, targetConstructSigs, assignableTo, comparisonCache, ast, context, comparisonInfoSignatuesTypeCheck, isComparingInstantiatedSignatures)) {
                    if (comparisonInfo) {
                        var enclosingSymbol = this.getEnclosingSymbolForAST(ast);
                        var message;
                        if (sourceConstructSigs.length && targetConstructSigs.length) {
                            if (comparisonInfoSignatuesTypeCheck && comparisonInfoSignatuesTypeCheck.message) {
                                message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Construct_signatures_of_types_0_and_1_are_incompatible_NL_2, [source.toString(enclosingSymbol), target.toString(enclosingSymbol), comparisonInfoSignatuesTypeCheck.message]);
                            } else {
                                message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Construct_signatures_of_types_0_and_1_are_incompatible, [source.toString(enclosingSymbol), target.toString(enclosingSymbol)]);
                            }
                        } else {
                            var hasSig = targetConstructSigs.length ? target.toString(enclosingSymbol) : source.toString(enclosingSymbol);
                            var lacksSig = !targetConstructSigs.length ? target.toString(enclosingSymbol) : source.toString(enclosingSymbol);
                            message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Type_0_requires_a_construct_signature_but_type_1_lacks_one, [hasSig, lacksSig]);
                        }
                        comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.IncompatibleSignatures;
                        comparisonInfo.addMessage(message);
                    }
                    return false;
                }
            }

            return true;
        };

        PullTypeResolver.prototype.sourceIndexSignaturesAreRelatableToTargetIndexSignatures = function (source, target, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var targetIndexSigs = this.getBothKindsOfIndexSignaturesExcludingAugmentedType(target, context);
            var targetStringSig = targetIndexSigs.stringSignature;
            var targetNumberSig = targetIndexSigs.numericSignature;

            if (targetStringSig || targetNumberSig) {
                var sourceIndexSigs = this.getBothKindsOfIndexSignaturesIncludingAugmentedType(source, context);
                var enclosingTypeIndexSigs = context.getBothKindOfIndexSignatures(true, false);
                var sourceStringSig = sourceIndexSigs.stringSignature;
                var sourceNumberSig = sourceIndexSigs.numericSignature;

                var comparable = true;
                var comparisonInfoSignatuesTypeCheck = null;
                if (comparisonInfo && !comparisonInfo.onlyCaptureFirstError) {
                    comparisonInfoSignatuesTypeCheck = new TypeComparisonInfo(comparisonInfo);
                }

                if (targetStringSig) {
                    // Spec section 3.8.3	Subtypes and Supertypes
                    // S is a subtype of a type T, and T is a supertype of S, if one of the following is true,
                    // where S’ denotes the apparent type(section 3.8.1) of S:
                    //      - M is a string index signature of type U and
                    //        S’ contains a string index signature of a type that is assignable to U.
                    // Spec section 3.8.4	Assignment Compatibility
                    // S is assignable to a type T, and T is assignable from S, if one of the following is true,
                    // where S’ denotes the apparent type(section 3.8.1) of S:
                    //      - M is a string index signature of type U and
                    //        S’ contains a string index signature of a type that is assignable to U.
                    if (sourceStringSig) {
                        context.walkIndexSignatureReturnTypes(enclosingTypeIndexSigs, true, true);
                        comparable = this.sourceIsRelatableToTargetInEnclosingTypes(sourceStringSig.returnType, targetStringSig.returnType, assignableTo, comparisonCache, ast, context, comparisonInfoSignatuesTypeCheck, isComparingInstantiatedSignatures);
                        context.postWalkIndexSignatureReturnTypes();
                    } else {
                        comparable = false;
                    }
                }

                if (comparable && targetNumberSig) {
                    // Spec section 3.8.3	Subtypes and Supertypes
                    // S is a subtype of a type T, and T is a supertype of S, if one of the following is true,
                    // where S’ denotes the apparent type(section 3.8.1) of S:
                    //      - M is a numeric index signature of type U and
                    //        S’ contains a string or numeric index signature of a type that is a subtype of U.
                    // Spec section 3.8.4	Assignment Compatibility
                    // S is assignable to a type T, and T is assignable from S, if one of the following is true,
                    // where S’ denotes the apparent type(section 3.8.1) of S:
                    //      - M is a numeric index signature of type U and
                    //        S’ contains a string or numeric index signature of a type that is assignable to U.
                    if (sourceNumberSig) {
                        context.walkIndexSignatureReturnTypes(enclosingTypeIndexSigs, false, false);
                        comparable = this.sourceIsRelatableToTargetInEnclosingTypes(sourceNumberSig.returnType, targetNumberSig.returnType, assignableTo, comparisonCache, ast, context, comparisonInfoSignatuesTypeCheck, isComparingInstantiatedSignatures);
                        context.postWalkIndexSignatureReturnTypes();
                    } else if (sourceStringSig) {
                        context.walkIndexSignatureReturnTypes(enclosingTypeIndexSigs, true, false);
                        comparable = this.sourceIsRelatableToTargetInEnclosingTypes(sourceStringSig.returnType, targetNumberSig.returnType, assignableTo, comparisonCache, ast, context, comparisonInfoSignatuesTypeCheck, isComparingInstantiatedSignatures);
                        context.postWalkIndexSignatureReturnTypes();
                    } else {
                        comparable = false;
                    }
                }

                if (!comparable) {
                    if (comparisonInfo) {
                        var message;
                        var enclosingSymbol = this.getEnclosingSymbolForAST(ast);
                        if (comparisonInfoSignatuesTypeCheck && comparisonInfoSignatuesTypeCheck.message) {
                            message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Index_signatures_of_types_0_and_1_are_incompatible_NL_2, [source.toString(enclosingSymbol), target.toString(enclosingSymbol), comparisonInfoSignatuesTypeCheck.message]);
                        } else {
                            message = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Index_signatures_of_types_0_and_1_are_incompatible, [source.toString(enclosingSymbol), target.toString(enclosingSymbol)]);
                        }
                        comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.IncompatibleSignatures;
                        comparisonInfo.addMessage(message);
                    }
                    return false;
                }
            }

            return true;
        };

        // REVIEW: TypeChanges: Return an error context object so the user can get better diagnostic info
        PullTypeResolver.prototype.signatureGroupIsRelatableToTarget = function (source, target, sourceSG, targetSG, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            if (sourceSG === targetSG) {
                return true;
            }

            if (!(sourceSG.length && targetSG.length)) {
                return false;
            }

            var foundMatch = false;

            var targetExcludeDefinition = targetSG.length > 1;
            var sourceExcludeDefinition = sourceSG.length > 1;
            var sigsCompared = 0;
            var comparisonInfoSignatuesTypeCheck = null;
            if (comparisonInfo) {
                comparisonInfoSignatuesTypeCheck = new TypeComparisonInfo(comparisonInfo, true);
                comparisonInfoSignatuesTypeCheck.message = comparisonInfo.message;
            }
            for (var iMSig = 0; iMSig < targetSG.length; iMSig++) {
                var mSig = targetSG[iMSig];

                if (mSig.isStringConstantOverloadSignature() || (targetExcludeDefinition && mSig.isDefinition())) {
                    continue;
                }

                for (var iNSig = 0; iNSig < sourceSG.length; iNSig++) {
                    var nSig = sourceSG[iNSig];

                    if (nSig.isStringConstantOverloadSignature() || (sourceExcludeDefinition && nSig.isDefinition())) {
                        continue;
                    }

                    context.walkSignatures(nSig.kind, iNSig, iMSig);
                    var isSignatureRelatableToTarget = this.signatureIsRelatableToTarget(nSig, mSig, assignableTo, comparisonCache, ast, context, sigsCompared == 0 ? comparisonInfoSignatuesTypeCheck : null, isComparingInstantiatedSignatures);
                    context.postWalkSignatures();

                    sigsCompared++;

                    if (isSignatureRelatableToTarget) {
                        foundMatch = true;
                        break;
                    }
                }

                if (foundMatch) {
                    foundMatch = false;
                    continue;
                }

                // Give information about check fail only if we are comparing one signature.
                // This helps in perf (without comparisonInfo we can even use checks that were determined to be false)
                // Yet we can give useful info if we are comparing two types with one signature
                if (comparisonInfo && sigsCompared == 1) {
                    comparisonInfo.message = comparisonInfoSignatuesTypeCheck.message;
                }

                return false;
            }

            return true;
        };

        PullTypeResolver.prototype.signatureIsRelatableToTarget = function (sourceSig, targetSig, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var isRelatableInfo = this.sourceIsRelatableToTargetInCache(sourceSig, targetSig, comparisonCache, comparisonInfo);
            if (isRelatableInfo) {
                return isRelatableInfo.isRelatable;
            }

            comparisonCache.setValueAt(sourceSig.pullSymbolID, targetSig.pullSymbolID, true);
            var isRelatable = this.signatureIsRelatableToTargetWorker(sourceSig, targetSig, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
            comparisonCache.setValueAt(sourceSig.pullSymbolID, targetSig.pullSymbolID, isRelatable);
            return isRelatable;
        };

        PullTypeResolver.prototype.signatureIsRelatableToTargetWorker = function (sourceSig, targetSig, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures) {
            var _this = this;
            var sourceParameters = sourceSig.parameters;
            var targetParameters = targetSig.parameters;

            if (!sourceParameters || !targetParameters) {
                return false;
            }

            var targetNonOptionalParamCount = targetSig.nonOptionalParamCount;
            var sourceNonOptionalParamCount = sourceSig.nonOptionalParamCount;

            if (!targetSig.hasVarArgs && sourceNonOptionalParamCount > targetParameters.length) {
                if (comparisonInfo) {
                    comparisonInfo.flags |= 3 /* SourceSignatureHasTooManyParameters */;
                    comparisonInfo.addMessage(TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Call_signature_expects_0_or_fewer_parameters, [targetParameters.length]));
                }
                return false;
            }

            // If signatures are relatable if sourceSig and targetSig are identical
            if (this.signaturesAreIdentical(sourceSig, targetSig, context)) {
                return true;
            }

            // From the January 23 version of the spec, following the design change:
            // Section 3.8.3 + 3.8.4: M is a non-specialized call or construct signature and S'
            // contains a call or construct signature N where, when M and N are instantiated using
            // type Any as the type argument for all type parameters declared by M and N (if any)
            if (targetSig.isGeneric()) {
                targetSig = this.instantiateSignatureToAny(targetSig);
            }

            if (sourceSig.isGeneric()) {
                sourceSig = this.instantiateSignatureToAny(sourceSig);
            }

            var sourceReturnType = sourceSig.returnType;
            var targetReturnType = targetSig.returnType;

            if (targetReturnType !== this.semanticInfoChain.voidTypeSymbol) {
                context.walkReturnTypes();
                var returnTypesAreRelatable = this.sourceIsRelatableToTargetInEnclosingTypes(sourceReturnType, targetReturnType, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
                context.postWalkReturnTypes();
                if (!returnTypesAreRelatable) {
                    if (comparisonInfo) {
                        comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.IncompatibleReturnTypes;
                        // No need to print this one here - it's printed as part of the signature error in sourceIsRelatableToTarget
                        //comparisonInfo.addMessage("Incompatible return types: '" + sourceReturnType.getTypeName() + "' and '" + targetReturnType.getTypeName() + "'");
                    }

                    return false;
                }
            }

            return targetSig.forAllCorrespondingParameterTypesInThisAndOtherSignature(sourceSig, function (targetParamType, sourceParamType, iParam) {
                context.walkParameterTypes(iParam);
                var areParametersRelatable = _this.sourceIsRelatableToTargetInEnclosingTypes(sourceParamType, targetParamType, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
                if (!areParametersRelatable) {
                    // Switch type1 and type2 enclosing types since we are doing reverse check
                    context.swapEnclosingTypeWalkers();
                    areParametersRelatable = _this.sourceIsRelatableToTargetInEnclosingTypes(targetParamType, sourceParamType, assignableTo, comparisonCache, ast, context, comparisonInfo, isComparingInstantiatedSignatures);
                    context.swapEnclosingTypeWalkers();
                }
                context.postWalkParameterTypes();

                if (!areParametersRelatable) {
                    if (comparisonInfo) {
                        comparisonInfo.flags |= TypeScript.TypeRelationshipFlags.IncompatibleParameterTypes;
                    }
                }

                return areParametersRelatable;
            });
        };

        // Overload resolution
        PullTypeResolver.prototype.resolveOverloads = function (application, group, haveTypeArgumentsAtCallSite, context, diagnostics) {
            var _this = this;
            var hasOverloads = group.length > 1;
            var comparisonInfo = new TypeComparisonInfo();
            var args = application.argumentList ? application.argumentList.arguments : null;

            var initialCandidates = TypeScript.ArrayUtilities.where(group, function (signature) {
                // Filter out definition if overloads are available
                if (hasOverloads && signature.isDefinition()) {
                    return false;
                }

                // Filter out nongeneric signatures if type arguments are supplied
                var rootSignature = signature.getRootSymbol();
                if (haveTypeArgumentsAtCallSite && !rootSignature.isGeneric()) {
                    return false;
                }

                // Filter out overloads with the wrong arity
                return _this.overloadHasCorrectArity(signature, args);
            });

            // Now that we have trimmed initial candidates, find which ones are applicable per spec
            //    section 4.12.1
            // October 11, 2013: If the list of candidate signatures is empty, the function call is
            //    an error.
            // Otherwise, if the candidate list contains one or more signatures for which the type
            //    of each argument expression is a subtype of each corresponding parameter type,
            //    the return type of the first of those signatures becomes the return type of the
            //    function call.
            // Otherwise, the return type of the first signature in the candidate list becomes
            //    the return type of the function call.
            var firstAssignableButNotSupertypeSignature = null;
            var firstAssignableWithProvisionalErrorsSignature = null;

            for (var i = 0; i < initialCandidates.length; i++) {
                var applicability = this.overloadIsApplicable(initialCandidates[i], args, context, comparisonInfo);
                if (applicability === 3 /* Subtype */) {
                    return initialCandidates[i];
                } else if (applicability === 2 /* AssignableWithNoProvisionalErrors */ && !firstAssignableButNotSupertypeSignature) {
                    firstAssignableButNotSupertypeSignature = initialCandidates[i];
                } else if (applicability === 1 /* AssignableButWithProvisionalErrors */ && !firstAssignableWithProvisionalErrorsSignature) {
                    firstAssignableWithProvisionalErrorsSignature = initialCandidates[i];
                }
            }

            // Choose the best signature we have (between assignable candidates and ones with provisional errors)
            // In particular, do not error when we have one that fits but with provisional errors
            if (firstAssignableButNotSupertypeSignature || firstAssignableWithProvisionalErrorsSignature) {
                return firstAssignableButNotSupertypeSignature || firstAssignableWithProvisionalErrorsSignature;
            } else {
                var target = this.getCallTargetErrorSpanAST(application);
                if (comparisonInfo.message) {
                    diagnostics.push(this.semanticInfoChain.diagnosticFromAST(target, TypeScript.DiagnosticCode.Supplied_parameters_do_not_match_any_signature_of_call_target_NL_0, [comparisonInfo.message]));
                } else {
                    diagnostics.push(this.semanticInfoChain.diagnosticFromAST(target, TypeScript.DiagnosticCode.Supplied_parameters_do_not_match_any_signature_of_call_target, null));
                }
            }

            return null;
        };

        PullTypeResolver.prototype.getCallTargetErrorSpanAST = function (callEx) {
            return (callEx.expression.kind() === 212 /* MemberAccessExpression */) ? callEx.expression.name : callEx.expression;
        };

        PullTypeResolver.prototype.overloadHasCorrectArity = function (signature, args) {
            if (args == null) {
                return signature.nonOptionalParamCount === 0;
            }

            // First, figure out how many arguments there are. This is usually args.nonSeparatorCount(), but if we have trailing separators,
            // we need to pretend we have one more "phantom" argument that the user is currently typing. This is useful for signature help.
            // Example: foo(1, 2,
            // should have 3 arguments
            var numberOfArgs = (args.nonSeparatorCount() && args.nonSeparatorCount() === args.separatorCount()) ? args.separatorCount() + 1 : args.nonSeparatorCount();
            if (numberOfArgs < signature.nonOptionalParamCount) {
                return false;
            }
            if (!signature.hasVarArgs && numberOfArgs > signature.parameters.length) {
                return false;
            }

            return true;
        };

        PullTypeResolver.prototype.overloadIsApplicable = function (signature, args, context, comparisonInfo) {
            // Already checked for arity, so it's automatically applicable if there are no args
            if (args === null) {
                return 3 /* Subtype */;
            }

            var isInVarArg = false;
            var parameters = signature.parameters;
            var paramType = null;

            // Start by assuming that the argument types are all subtypes of the corresponding parameter types
            // Indeed this is the case for a call with no arguments.
            var overloadApplicability = 3 /* Subtype */;

            for (var i = 0; i < args.nonSeparatorCount(); i++) {
                if (!isInVarArg) {
                    this.resolveDeclaredSymbol(parameters[i], context);

                    if (parameters[i].isVarArg) {
                        // If the vararg has no element type, it is malformed, so just use the any symbol (we will have errored when resolving the signature).
                        paramType = parameters[i].type.getElementType() || this.getNewErrorTypeSymbol(parameters[i].type.getName());
                        isInVarArg = true;
                    } else {
                        paramType = parameters[i].type;
                    }
                }

                // We aggregate the statuses across arguments by taking the less flattering of the two statuses.
                // In the case where we get a completely unassignable argument, we can short circuit and just throw out the signature.
                var statusOfCurrentArgument = this.overloadIsApplicableForArgument(paramType, args.nonSeparatorAt(i), i, context, comparisonInfo);

                if (statusOfCurrentArgument === 0 /* NotAssignable */) {
                    return 0 /* NotAssignable */;
                } else if (statusOfCurrentArgument === 1 /* AssignableButWithProvisionalErrors */) {
                    overloadApplicability = 1 /* AssignableButWithProvisionalErrors */;
                } else if (overloadApplicability !== 1 /* AssignableButWithProvisionalErrors */ && statusOfCurrentArgument === 2 /* AssignableWithNoProvisionalErrors */) {
                    overloadApplicability = 2 /* AssignableWithNoProvisionalErrors */;
                }
                // else we have nothing to downgrade - just stick with what we have
            }

            return overloadApplicability;
        };

        PullTypeResolver.prototype.overloadIsApplicableForArgument = function (paramType, arg, argIndex, context, comparisonInfo) {
            if (paramType.isAny()) {
                return 3 /* Subtype */;
            } else if (paramType.isError()) {
                // This is for the case where the parameter type itself was malformed. We will treat this like a provisional error because
                // we will get an error for this in the overload defition group, and we want to avoid choosing this overload if possible.
                return 1 /* AssignableButWithProvisionalErrors */;
            } else if (arg.kind() === 219 /* SimpleArrowFunctionExpression */) {
                var simpleArrowFunction = arg;
                return this.overloadIsApplicableForAnyFunctionExpressionArgument(paramType, arg, null, TypeScript.ASTHelpers.parametersFromIdentifier(simpleArrowFunction.identifier), null, simpleArrowFunction.block, simpleArrowFunction.expression, argIndex, context, comparisonInfo);
            } else if (arg.kind() === 218 /* ParenthesizedArrowFunctionExpression */) {
                var arrowFunction = arg;
                return this.overloadIsApplicableForAnyFunctionExpressionArgument(paramType, arg, arrowFunction.callSignature.typeParameterList, TypeScript.ASTHelpers.parametersFromParameterList(arrowFunction.callSignature.parameterList), TypeScript.ASTHelpers.getType(arrowFunction), arrowFunction.block, arrowFunction.expression, argIndex, context, comparisonInfo);
            } else if (arg.kind() === 222 /* FunctionExpression */) {
                var functionExpression = arg;
                return this.overloadIsApplicableForAnyFunctionExpressionArgument(paramType, arg, functionExpression.callSignature.typeParameterList, TypeScript.ASTHelpers.parametersFromParameterList(functionExpression.callSignature.parameterList), TypeScript.ASTHelpers.getType(functionExpression), functionExpression.block, null, argIndex, context, comparisonInfo);
            } else if (arg.kind() === 215 /* ObjectLiteralExpression */) {
                return this.overloadIsApplicableForObjectLiteralArgument(paramType, arg, argIndex, context, comparisonInfo);
            } else if (arg.kind() === 214 /* ArrayLiteralExpression */) {
                return this.overloadIsApplicableForArrayLiteralArgument(paramType, arg, argIndex, context, comparisonInfo);
            } else {
                return this.overloadIsApplicableForOtherArgument(paramType, arg, argIndex, context, comparisonInfo);
            }
        };

        PullTypeResolver.prototype.overloadIsApplicableForAnyFunctionExpressionArgument = function (paramType, arg, typeParameters, parameters, returnTypeAnnotation, block, bodyExpression, argIndex, context, comparisonInfo) {
            if (this.cachedFunctionInterfaceType() && paramType === this.cachedFunctionInterfaceType()) {
                return 2 /* AssignableWithNoProvisionalErrors */;
            }

            context.pushProvisionalType(paramType);

            var argSym = this.resolveAnyFunctionExpression(arg, typeParameters, parameters, returnTypeAnnotation, block, bodyExpression, true, context);

            var applicabilityStatus = this.overloadIsApplicableForArgumentHelper(paramType, argSym.type, argIndex, comparisonInfo, arg, context);

            context.popAnyContextualType();

            return applicabilityStatus;
        };

        PullTypeResolver.prototype.overloadIsApplicableForObjectLiteralArgument = function (paramType, arg, argIndex, context, comparisonInfo) {
            // attempt to contextually type the object literal
            if (this.cachedObjectInterfaceType() && paramType === this.cachedObjectInterfaceType()) {
                return 2 /* AssignableWithNoProvisionalErrors */;
            }

            context.pushProvisionalType(paramType);
            var argSym = this.resolveObjectLiteralExpression(arg, true, context);

            var applicabilityStatus = this.overloadIsApplicableForArgumentHelper(paramType, argSym.type, argIndex, comparisonInfo, arg, context);

            context.popAnyContextualType();

            return applicabilityStatus;
        };

        PullTypeResolver.prototype.overloadIsApplicableForArrayLiteralArgument = function (paramType, arg, argIndex, context, comparisonInfo) {
            // attempt to contextually type the array literal
            if (paramType === this.cachedArrayInterfaceType()) {
                return 2 /* AssignableWithNoProvisionalErrors */;
            }

            context.pushProvisionalType(paramType);
            var argSym = this.resolveArrayLiteralExpression(arg, true, context);

            var applicabilityStatus = this.overloadIsApplicableForArgumentHelper(paramType, argSym.type, argIndex, comparisonInfo, arg, context);

            context.popAnyContextualType();

            return applicabilityStatus;
        };

        PullTypeResolver.prototype.overloadIsApplicableForOtherArgument = function (paramType, arg, argIndex, context, comparisonInfo) {
            // No need to contextually type or mark as provisional
            var argSym = this.resolveAST(arg, false, context);

            // If it is an alias, get its type
            if (argSym.type.isAlias()) {
                var aliasSym = argSym.type;
                argSym = aliasSym.getExportAssignedTypeSymbol();
            }

            // Just in case the argument is a string literal, and are checking overload on const, we set this stringConstantVal
            // (sourceIsAssignableToTarget will internally check if the argument is actually a string)
            comparisonInfo.stringConstantVal = arg;
            return this.overloadIsApplicableForArgumentHelper(paramType, argSym.type, argIndex, comparisonInfo, arg, context);
        };

        // The inner method that decides if an overload is applicable. It can return any of 4 applicability statuses
        // Spec October 11, 2013: Section 4.12.1: for each argument expression e and its corresponding parameter P,
        // when e is contextually typed(section 4.19) by the type of P, no errors ensue and the type of e is assignable
        // to (section 3.8.4) the type of P.
        // Note this also tracks whether the argument type is a subtype of the parameter type
        PullTypeResolver.prototype.overloadIsApplicableForArgumentHelper = function (paramType, argSym, argumentIndex, comparisonInfo, arg, context) {
            // Make a temporary comparison info to catch an error in case the parameter is not a supertype (we don't want to report such errors)
            var tempComparisonInfo = new TypeComparisonInfo();
            tempComparisonInfo.stringConstantVal = comparisonInfo.stringConstantVal;
            if (!context.hasProvisionalErrors() && this.sourceIsSubtypeOfTarget(argSym.type, paramType, arg, context, tempComparisonInfo)) {
                return 3 /* Subtype */;
            }

            // Now check for normal assignability using the original comparison info (or use the temporary one if the original one already has an error).
            if (this.sourceIsAssignableToTarget(argSym.type, paramType, arg, context, comparisonInfo.message ? tempComparisonInfo : comparisonInfo)) {
                return context.hasProvisionalErrors() ? 1 /* AssignableButWithProvisionalErrors */ : 2 /* AssignableWithNoProvisionalErrors */;
            }

            if (!comparisonInfo.message) {
                var enclosingSymbol = this.getEnclosingSymbolForAST(arg);
                comparisonInfo.addMessage(TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Could_not_apply_type_0_to_argument_1_which_is_of_type_2, [paramType.toString(enclosingSymbol), (argumentIndex + 1), argSym.getTypeName(enclosingSymbol)]));
            }

            return 0 /* NotAssignable */;
        };

        PullTypeResolver.prototype.inferArgumentTypesForSignature = function (argContext, comparisonInfo, context) {
            var inferenceResultTypes = argContext.inferTypeArguments();
            var typeParameters = argContext.signatureBeingInferred.getTypeParameters();
            TypeScript.Debug.assert(typeParameters.length == inferenceResultTypes.length);

            // Fall back to constraints if constraints are not satisfied
            var typeReplacementMapForConstraints = null;
            for (var i = 0; i < inferenceResultTypes.length; i++) {
                if (typeParameters[i].getConstraint()) {
                    typeReplacementMapForConstraints = typeReplacementMapForConstraints || TypeScript.PullInstantiationHelpers.createTypeParameterArgumentMap(typeParameters, inferenceResultTypes);
                    var associatedConstraint = this.instantiateType(typeParameters[i].getConstraint(), typeReplacementMapForConstraints);
                    if (!this.sourceIsAssignableToTargetWithNewEnclosingTypes(inferenceResultTypes[i], associatedConstraint, null, context, null, false)) {
                        inferenceResultTypes[i] = associatedConstraint;
                    }
                }
            }

            //  Do not let local type parameters escape at call sites
            //  Because we're comparing for equality between two signatures (where one is instantiated
            //  against the type parameters of the other), we want to know * exactly * what the
            //  instantiation would be give the set of type parameters.In the case where the two
            //  signatures are otherwise equal, we don't want to sub in '{ }' for any of the type parameters,
            //  since that would short - circuit the equality check.The substitution is only desirable at call sites,
            //  where type parameters can leak out of scope, but during contextual instantiation
            //  the type parameters * should * be considered in scope
            // We know that if we are inferring at a call expression we are not doing
            // contextual signature instantiation
            if (argContext.isInvocationInferenceContext()) {
                // Need to know if the type parameters are in scope. If not, they are not legal inference
                // candidates unless we are in contextual signature instantiation
                var invocationContext = argContext;
                if (!this.typeParametersAreInScopeAtArgumentList(typeParameters, invocationContext.argumentASTs)) {
                    for (var i = 0; i < inferenceResultTypes.length; i++) {
                        // Check if the inferred type wraps any one of the type parameters
                        if (inferenceResultTypes[i].wrapsSomeTypeParameter(argContext.candidateCache)) {
                            inferenceResultTypes[i] = this.semanticInfoChain.anyTypeSymbol;
                        }
                    }
                }
            }

            return inferenceResultTypes;
        };

        PullTypeResolver.prototype.typeParametersAreInScopeAtArgumentList = function (typeParameters, args) {
            // If the parent path from the current enclosing decl contains the type parameters'
            // parent decl, then the type parameters must be in scope
            var enclosingDecl = this.getEnclosingDeclForAST(args);
            var typeParameterParentDecl = typeParameters[0].getDeclarations()[0].getParentDecl();
            return enclosingDecl.getParentPath().indexOf(typeParameterParentDecl) > -1;
        };

        PullTypeResolver.prototype.relateTypeToTypeParametersInEnclosingType = function (expressionType, parameterType, argContext, context) {
            if (expressionType && parameterType) {
                if (context.oneOfClassificationsIsInfinitelyExpanding()) {
                    this.relateInifinitelyExpandingTypeToTypeParameters(expressionType, parameterType, argContext, context);
                    return;
                }
            }
            this.relateTypeToTypeParameters(expressionType, parameterType, argContext, context);
        };

        PullTypeResolver.prototype.relateTypeToTypeParametersWithNewEnclosingTypes = function (expressionType, parameterType, argContext, context) {
            var enclosingTypeWalkers = context.resetEnclosingTypeWalkers();
            this.relateTypeToTypeParameters(expressionType, parameterType, argContext, context);
            context.setEnclosingTypeWalkers(enclosingTypeWalkers);
        };

        PullTypeResolver.prototype.relateTypeToTypeParameters = function (expressionType, parameterType, argContext, context) {
            if (!expressionType || !parameterType) {
                return;
            }

            if (expressionType.isError()) {
                expressionType = this.semanticInfoChain.anyTypeSymbol;
            }

            if (parameterType.isTypeParameter()) {
                var typeParameter = parameterType;
                argContext.addCandidateForInference(typeParameter, expressionType);
                return;
            }

            // This is an optimization. Essentially the only way we can make an inference by diving
            // into a type is if our type parameters T occur inside that type. Because we don't have
            // nested named types, this can only occur if the type is anonymous, or if it is generic
            // and T is a type argument to that type.
            if (parameterType.isNamedTypeSymbol() && !parameterType.isGeneric() && !parameterType.getTypeArguments()) {
                return;
            }

            // As an optimization, if both types are generic and the same type, relate their type arguments
            if (TypeScript.PullInstantiationHelpers.twoTypesAreInstantiationsOfSameNamedGenericType(expressionType, parameterType)) {
                this.relateTypeArgumentsOfTypeToTypeParameters(expressionType, parameterType, argContext, context);
            } else {
                // Relate structurally
                var symbolsWhenStartedWalkingTypes = context.startWalkingTypes(expressionType, parameterType);
                this.relateObjectTypeToTypeParameters(expressionType, parameterType, argContext, context);
                context.endWalkingTypes(symbolsWhenStartedWalkingTypes);
            }
        };

        PullTypeResolver.prototype.relateTypeArgumentsOfTypeToTypeParameters = function (expressionType, parameterType, argContext, context) {
            var parameterSideTypeArguments = parameterType.getTypeArguments();
            var argumentSideTypeArguments = expressionType.getTypeArguments();

            TypeScript.Debug.assert(parameterSideTypeArguments && argumentSideTypeArguments && parameterSideTypeArguments.length === argumentSideTypeArguments.length);
            for (var i = 0; i < parameterSideTypeArguments.length; i++) {
                this.relateTypeToTypeParametersWithNewEnclosingTypes(argumentSideTypeArguments[i], parameterSideTypeArguments[i], argContext, context);
            }
        };

        PullTypeResolver.prototype.relateInifinitelyExpandingTypeToTypeParameters = function (expressionType, parameterType, argContext, context) {
            if (!expressionType || !parameterType) {
                return;
            }

            // Section 3.8.7 - Recursive Types
            // Likewise, when making type inferences(section 3.8.6) from a type S to a type T,
            // if either type originates in an infinitely expanding type reference, then
            //     if S and T are type references to the same named type, inferences are made from each type argument in S to each type argument in T,
            //     otherwise, no inferences are made.
            var expressionTypeNamedTypeReference = TypeScript.PullHelpers.getRootType(expressionType);
            var parameterTypeNamedTypeReference = TypeScript.PullHelpers.getRootType(parameterType);
            if (expressionTypeNamedTypeReference !== parameterTypeNamedTypeReference) {
                return;
            }

            var expressionTypeTypeArguments = expressionType.getTypeArguments();
            var parameterTypeArguments = parameterType.getTypeArguments();

            if (expressionTypeTypeArguments && parameterTypeArguments && expressionTypeTypeArguments.length === parameterTypeArguments.length) {
                for (var i = 0; i < expressionTypeTypeArguments.length; i++) {
                    this.relateTypeArgumentsOfTypeToTypeParameters(expressionType, parameterType, argContext, context);
                }
            }
        };

        PullTypeResolver.prototype.relateFunctionSignatureToTypeParameters = function (expressionSignature, parameterSignature, argContext, context) {
            var _this = this;
            var expressionReturnType = expressionSignature.returnType;
            var parameterReturnType = parameterSignature.returnType;

            parameterSignature.forAllCorrespondingParameterTypesInThisAndOtherSignature(expressionSignature, function (parameterSignatureParameterType, expressionSignatureParameterType, i) {
                context.walkParameterTypes(i);
                _this.relateTypeToTypeParametersInEnclosingType(expressionSignatureParameterType, parameterSignatureParameterType, argContext, context);
                context.postWalkParameterTypes();
                return true;
            });

            context.walkReturnTypes();
            this.relateTypeToTypeParametersInEnclosingType(expressionReturnType, parameterReturnType, argContext, context);
            context.postWalkReturnTypes();
        };

        PullTypeResolver.prototype.relateObjectTypeToTypeParameters = function (objectType, parameterType, argContext, context) {
            var parameterTypeMembers = parameterType.getMembers();
            var parameterSignatures;

            var objectMember;
            var objectSignatures;

            if (argContext.alreadyRelatingTypes(objectType, parameterType)) {
                return;
            }

            for (var i = 0; i < parameterTypeMembers.length; i++) {
                objectMember = this.getNamedPropertySymbol(parameterTypeMembers[i].name, TypeScript.PullElementKind.SomeValue, objectType);
                if (objectMember) {
                    // TODO: resolveDeclaredSymbol shouldn't be necessary here, but full fix requires changing underlying
                    // symbols to resolve themselves, which should be done at a later time.
                    this.resolveDeclaredSymbol(objectMember);
                    this.resolveDeclaredSymbol(parameterTypeMembers[i]);
                    context.walkMemberTypes(parameterTypeMembers[i].name);
                    this.relateTypeToTypeParametersInEnclosingType(objectMember.type, parameterTypeMembers[i].type, argContext, context);
                    context.postWalkMemberTypes();
                }
            }

            // If M is a call signature then for each call signature N in S, if the number of non-optional
            // parameters in N is greater than or equal to that of M, N is instantiated with each of its
            // type parameter constraints as type arguments (if any) and inferences are made from parameter
            // types in N to parameter types in the same position in M, and from the return type of N to the
            // return type of M.
            this.relateSignatureGroupToTypeParameters(objectType.getCallSignatures(), parameterType.getCallSignatures(), TypeScript.PullElementKind.CallSignature, argContext, context);

            // If M is a construct signature then for each construct signature N in S, if the number of non-optional
            // parameters in N is greater than or equal to that of M, N is instantiated with each of its
            // type parameter constraints as type arguments (if any) and inferences are made from parameter
            // types in N to parameter types in the same position in M, and from the return type of N to the
            // return type of M.
            this.relateSignatureGroupToTypeParameters(objectType.getConstructSignatures(), parameterType.getConstructSignatures(), TypeScript.PullElementKind.ConstructSignature, argContext, context);

            var parameterIndexSignatures = this.getBothKindsOfIndexSignaturesExcludingAugmentedType(parameterType, context);
            var objectIndexSignatures = this.getBothKindsOfIndexSignaturesExcludingAugmentedType(objectType, context);
            var indexSigInfo = context.getBothKindOfIndexSignatures(false, false);

            // - If M is a string index signature and S contains a string index signature N, inferences are made from the type of N to the type of M.
            // - If M is a numeric index signature and S contains a numeric index signature N, inferences are made from the type of N to the type of M.
            if (parameterIndexSignatures.stringSignature && objectIndexSignatures.stringSignature) {
                context.walkIndexSignatureReturnTypes(indexSigInfo, true, true, true);
                this.relateFunctionSignatureToTypeParameters(objectIndexSignatures.stringSignature, parameterIndexSignatures.stringSignature, argContext, context);
                context.postWalkIndexSignatureReturnTypes(true);
            }
            if (parameterIndexSignatures.numericSignature && objectIndexSignatures.numericSignature) {
                context.walkIndexSignatureReturnTypes(indexSigInfo, false, false, true);
                this.relateFunctionSignatureToTypeParameters(objectIndexSignatures.numericSignature, parameterIndexSignatures.numericSignature, argContext, context);
                context.postWalkIndexSignatureReturnTypes(true);
            }
        };

        // If M is a call/construct signature then for each call/construct signature N in S, if the number of non-optional
        // parameters in N is greater than or equal to that of M, N is instantiated with each of its
        // type parameter constraints as type arguments (if any) and inferences are made from parameter
        // types in N to parameter types in the same position in M, and from the return type of N to the
        // return type of M.
        PullTypeResolver.prototype.relateSignatureGroupToTypeParameters = function (argumentSignatures, parameterSignatures, signatureKind, argContext, context) {
            for (var i = 0; i < parameterSignatures.length; i++) {
                var paramSignature = parameterSignatures[i];
                if (argumentSignatures.length > 0 && paramSignature.isGeneric()) {
                    paramSignature = this.instantiateSignatureToAny(paramSignature);
                }
                for (var j = 0; j < argumentSignatures.length; j++) {
                    var argumentSignature = argumentSignatures[j];
                    if (argumentSignature.nonOptionalParamCount > paramSignature.nonOptionalParamCount) {
                        continue;
                    }

                    if (argumentSignature.isGeneric()) {
                        argumentSignature = this.instantiateSignatureToAny(argumentSignature);
                    }

                    // In relateTypeToTypeParameters, the argument type (expressionType) is passed in first
                    // (before the parameter type), so the correct order here is j, i
                    context.walkSignatures(signatureKind, j, i);
                    this.relateFunctionSignatureToTypeParameters(argumentSignature, paramSignature, argContext, context);
                    context.postWalkSignatures();
                }
            }
        };

        // November 18, 2013, Section 4.12.2:
        // If e is an expression of a function type that contains exactly one generic call signature
        // and no other members, and T is a function type with exactly one non-generic call signature
        // and no other members, then any inferences made for type parameters referenced by the
        // parameters of T's call signature are fixed and, if e's call signature can successfully be
        // instantiated in the context of T's call signature(section 3.8.5), e's type is changed to
        // a function type with that instantiated signature.
        PullTypeResolver.prototype.alterPotentialGenericFunctionTypeToInstantiatedFunctionTypeForTypeArgumentInference = function (expressionSymbol, context) {
            // In this case getContextualType will give us the inferential type
            var inferentialType = context.getContextualType();
            TypeScript.Debug.assert(inferentialType);
            var expressionType = expressionSymbol.type;
            if (this.isFunctionTypeWithExactlyOneCallSignatureAndNoOtherMembers(expressionType, true) && this.isFunctionTypeWithExactlyOneCallSignatureAndNoOtherMembers(inferentialType, false)) {
                // Here we overwrite contextualType because we will want to use the version of the type
                // that was instantiated with the fixed type parameters from the argument inference
                // context
                var genericExpressionSignature = expressionType.getCallSignatures()[0];
                var contextualSignature = inferentialType.getCallSignatures()[0];

                // Contextual signature instantiation will return null if the instantiation is unsuccessful
                // We pass true so that the parameters of the contextual signature will be fixed per the spec.
                var instantiatedSignature = this.instantiateSignatureInContext(genericExpressionSignature, contextualSignature, context, true);
                if (instantiatedSignature === null) {
                    return expressionSymbol;
                }

                // Create new type with just the given call signature
                var newType = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.FunctionType);
                newType.appendCallSignature(instantiatedSignature);
                return newType;
            }

            return expressionSymbol;
        };

        PullTypeResolver.prototype.isFunctionTypeWithExactlyOneCallSignatureAndNoOtherMembers = function (type, callSignatureShouldBeGeneric) {
            TypeScript.Debug.assert(type);
            if (type.getCallSignatures().length !== 1) {
                return false;
            }

            var callSignatureIsGeneric = type.getCallSignatures()[0].isGeneric();
            if (callSignatureIsGeneric !== callSignatureShouldBeGeneric) {
                return false;
            }

            var typeHasOtherMembers = type.getConstructSignatures().length || type.getIndexSignatures().length || type.getAllMembers(TypeScript.PullElementKind.SomeValue, 0 /* all */).length;
            if (typeHasOtherMembers) {
                return false;
            }

            return true;
        };

        PullTypeResolver.prototype.instantiateTypeToAny = function (typeToSpecialize, context) {
            var typeParameters = typeToSpecialize.getTypeParameters();

            if (!typeParameters.length) {
                return typeToSpecialize;
            }

            var typeArguments = null;

            if (!this._cachedAnyTypeArgs) {
                this._cachedAnyTypeArgs = [
                    [this.semanticInfoChain.anyTypeSymbol],
                    [this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol],
                    [this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol],
                    [this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol],
                    [this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol]
                ];
            }

            if (typeParameters.length < this._cachedAnyTypeArgs.length) {
                typeArguments = this._cachedAnyTypeArgs[typeParameters.length - 1];
            } else {
                // REVIEW: might want to cache these arg lists
                typeArguments = [];

                for (var i = 0; i < typeParameters.length; i++) {
                    typeArguments[typeArguments.length] = this.semanticInfoChain.anyTypeSymbol;
                }
            }

            var type = this.createInstantiatedType(typeToSpecialize, typeArguments);

            return type;
        };

        PullTypeResolver.prototype.instantiateSignatureToAny = function (signature) {
            var typeParameters = signature.getTypeParameters();
            if (!this._cachedAnyTypeArgs) {
                this._cachedAnyTypeArgs = [
                    [this.semanticInfoChain.anyTypeSymbol],
                    [this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol],
                    [this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol],
                    [this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol],
                    [this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol, this.semanticInfoChain.anyTypeSymbol]
                ];
            }

            if (typeParameters.length < this._cachedAnyTypeArgs.length) {
                var typeArguments = this._cachedAnyTypeArgs[typeParameters.length - 1];
            } else {
                // REVIEW: might want to cache these arg lists
                var typeArguments = [];

                for (var i = 0; i < typeParameters.length; i++) {
                    typeArguments[typeArguments.length] = this.semanticInfoChain.anyTypeSymbol;
                }
            }

            var typeParameterArgumentMap = TypeScript.PullInstantiationHelpers.createTypeParameterArgumentMap(typeParameters, typeArguments);
            return this.instantiateSignature(signature, typeParameterArgumentMap);
        };

        // type check infrastructure
        PullTypeResolver.typeCheck = function (compilationSettings, semanticInfoChain, document) {
            var sourceUnit = document.sourceUnit();

            var resolver = semanticInfoChain.getResolver();
            var context = new TypeScript.PullTypeResolutionContext(resolver, true, sourceUnit.fileName());

            if (resolver.canTypeCheckAST(sourceUnit, context)) {
                resolver.resolveAST(sourceUnit, false, context);
                resolver.validateVariableDeclarationGroups(semanticInfoChain.getDeclForAST(sourceUnit), context);

                while (resolver.typeCheckCallBacks.length) {
                    var callBack = resolver.typeCheckCallBacks.pop();
                    callBack(context);
                }

                resolver.processPostTypeCheckWorkItems(context);
            }
        };

        PullTypeResolver.prototype.validateVariableDeclarationGroups = function (enclosingDecl, context) {
            var _this = this;
            this.scanVariableDeclarationGroups(enclosingDecl, function (_) {
            }, function (subsequentDecl, firstSymbol) {
                // do not report 'must have same type' error for parameters - it makes no sense for them
                // having 'duplicate name' error that can be raised during parameter binding is enough
                if (TypeScript.hasFlag(subsequentDecl.kind, TypeScript.PullElementKind.Parameter) || TypeScript.hasFlag(subsequentDecl.flags, TypeScript.PullElementFlags.PropertyParameter)) {
                    return;
                }

                var boundDeclAST = _this.semanticInfoChain.getASTForDecl(subsequentDecl);

                var symbol = subsequentDecl.getSymbol();
                var symbolType = symbol.type;
                var firstSymbolType = firstSymbol.type;

                if (symbolType && firstSymbolType && symbolType !== firstSymbolType && !_this.typesAreIdentical(symbolType, firstSymbolType, context)) {
                    context.postDiagnostic(_this.semanticInfoChain.diagnosticFromAST(boundDeclAST, TypeScript.DiagnosticCode.Subsequent_variable_declarations_must_have_the_same_type_Variable_0_must_be_of_type_1_but_here_has_type_2, [symbol.getScopedName(), firstSymbolType.toString(firstSymbol), symbolType.toString(symbol)]));
                }
            });
        };

        PullTypeResolver.prototype.typeCheckFunctionOverloads = function (funcDecl, context, signature, allSignatures) {
            if (!signature) {
                var functionSignatureInfo = TypeScript.PullHelpers.getSignatureForFuncDecl(this.semanticInfoChain.getDeclForAST(funcDecl));
                signature = functionSignatureInfo.signature;
                allSignatures = functionSignatureInfo.allSignatures;
            }
            var functionDeclaration = this.semanticInfoChain.getDeclForAST(funcDecl);
            var funcSymbol = functionDeclaration.getSymbol();

            // Find the definition signature for this signature group
            var definitionSignature = null;
            for (var i = allSignatures.length - 1; i >= 0; i--) {
                if (allSignatures[i].isDefinition()) {
                    definitionSignature = allSignatures[i];
                    break;
                }
            }

            if (!signature.isDefinition()) {
                // Check for if the signatures are identical, check with the signatures before the current current one
                var signatureParentDecl = signature.getDeclarations()[0].getParentDecl();
                for (var i = 0; i < allSignatures.length; i++) {
                    if (allSignatures[i] === signature) {
                        break;
                    }

                    // Make sure they are in the same declaration
                    var allSignaturesParentDecl = allSignatures[i].getDeclarations()[0].getParentDecl();
                    if (allSignaturesParentDecl !== signatureParentDecl) {
                        continue;
                    }

                    if (this.signaturesAreIdenticalWithNewEnclosingTypes(allSignatures[i], signature, context, false)) {
                        if (!this.signatureReturnTypesAreIdentical(allSignatures[i], signature, context)) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Overloads_cannot_differ_only_by_return_type));
                        } else if (funcDecl.kind() === 137 /* ConstructorDeclaration */) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Duplicate_constructor_overload_signature));
                        } else if (functionDeclaration.kind === TypeScript.PullElementKind.ConstructSignature) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Duplicate_overload_construct_signature));
                        } else if (functionDeclaration.kind === TypeScript.PullElementKind.CallSignature) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Duplicate_overload_call_signature));
                        } else {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Duplicate_overload_signature_for_0, [funcSymbol.getScopedNameEx().toString()]));
                        }

                        break;
                    }
                }
            }

            // Verify assignment compatibility or in case of constantOverload signature, if its subtype of atleast one signature
            var isConstantOverloadSignature = signature.isStringConstantOverloadSignature();
            if (isConstantOverloadSignature) {
                if (signature.isDefinition()) {
                    // Report error - definition signature cannot specify constant type
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Overload_signature_implementation_cannot_use_specialized_type));
                } else {
                    var foundSubtypeSignature = false;
                    for (var i = 0; i < allSignatures.length; i++) {
                        if (allSignatures[i].isDefinition() || allSignatures[i] === signature) {
                            continue;
                        }

                        if (!allSignatures[i].isResolved) {
                            this.resolveDeclaredSymbol(allSignatures[i], context);
                        }

                        if (allSignatures[i].isStringConstantOverloadSignature()) {
                            continue;
                        }

                        if (this.signatureIsAssignableToTarget(signature, allSignatures[i], null, context)) {
                            foundSubtypeSignature = true;
                            break;
                        }
                    }

                    if (!foundSubtypeSignature) {
                        // Could not find the overload signature subtype
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Specialized_overload_signature_is_not_assignable_to_any_non_specialized_signature));
                    }
                }
            } else if (definitionSignature && definitionSignature !== signature) {
                var comparisonInfo = new TypeComparisonInfo();

                if (!definitionSignature.isResolved) {
                    this.resolveDeclaredSymbol(definitionSignature, context);
                }

                if (!this.signatureIsAssignableToTarget(definitionSignature, signature, funcDecl, context, comparisonInfo)) {
                    // definition signature is not assignable to functionSignature then its incorrect overload signature
                    if (comparisonInfo.message) {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Overload_signature_is_not_compatible_with_function_definition_NL_0, [comparisonInfo.message]));
                    } else {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, TypeScript.DiagnosticCode.Overload_signature_is_not_compatible_with_function_definition));
                    }
                }
            }

            var signatureForVisibilityCheck = definitionSignature;
            if (!definitionSignature) {
                if (allSignatures[0] === signature) {
                    return;
                }
                signatureForVisibilityCheck = allSignatures[0];
            }

            if (funcDecl.kind() !== 137 /* ConstructorDeclaration */ && functionDeclaration.kind !== TypeScript.PullElementKind.ConstructSignature && signatureForVisibilityCheck && signature !== signatureForVisibilityCheck) {
                var errorCode;

                // verify it satisfies all the properties of first signature
                if (signatureForVisibilityCheck.anyDeclHasFlag(TypeScript.PullElementFlags.Private) !== signature.anyDeclHasFlag(TypeScript.PullElementFlags.Private)) {
                    errorCode = TypeScript.DiagnosticCode.Overload_signatures_must_all_be_public_or_private;
                } else if (signatureForVisibilityCheck.anyDeclHasFlag(1 /* Exported */) !== signature.anyDeclHasFlag(1 /* Exported */)) {
                    errorCode = TypeScript.DiagnosticCode.Overload_signatures_must_all_be_exported_or_not_exported;
                } else if (signatureForVisibilityCheck.anyDeclHasFlag(TypeScript.PullElementFlags.Ambient) !== signature.anyDeclHasFlag(TypeScript.PullElementFlags.Ambient)) {
                    errorCode = TypeScript.DiagnosticCode.Overload_signatures_must_all_be_ambient_or_non_ambient;
                } else if (signatureForVisibilityCheck.anyDeclHasFlag(TypeScript.PullElementFlags.Optional) !== signature.anyDeclHasFlag(TypeScript.PullElementFlags.Optional)) {
                    errorCode = TypeScript.DiagnosticCode.Overload_signatures_must_all_be_optional_or_required;
                }

                if (errorCode) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(funcDecl, errorCode));
                }
            }
        };

        // Privacy checking
        PullTypeResolver.prototype.checkSymbolPrivacy = function (declSymbol, symbol, privacyErrorReporter) {
            if (!symbol || symbol.kind === TypeScript.PullElementKind.Primitive) {
                return;
            }

            if (symbol.isType()) {
                var typeSymbol = symbol;
                var isNamedType = typeSymbol.isNamedTypeSymbol();

                if (typeSymbol.isArrayNamedTypeReference()) {
                    this.checkSymbolPrivacy(declSymbol, typeSymbol.getElementType(), privacyErrorReporter);
                    return;
                }

                if (!isNamedType) {
                    var typeOfSymbol = typeSymbol.getTypeOfSymbol();
                    if (typeOfSymbol) {
                        this.checkSymbolPrivacy(declSymbol, typeOfSymbol, privacyErrorReporter);
                        return;
                    }
                }

                if (typeSymbol.inSymbolPrivacyCheck) {
                    return;
                }

                typeSymbol.inSymbolPrivacyCheck = true;

                var typars = typeSymbol.getTypeArgumentsOrTypeParameters();
                if (typars) {
                    for (var i = 0; i < typars.length; i++) {
                        this.checkSymbolPrivacy(declSymbol, typars[i], privacyErrorReporter);
                    }
                }

                if (!isNamedType) {
                    var members = typeSymbol.getMembers();
                    for (var i = 0; i < members.length; i++) {
                        this.checkSymbolPrivacy(declSymbol, members[i].type, privacyErrorReporter);
                    }

                    this.checkTypePrivacyOfSignatures(declSymbol, typeSymbol.getCallSignatures(), privacyErrorReporter);
                    this.checkTypePrivacyOfSignatures(declSymbol, typeSymbol.getConstructSignatures(), privacyErrorReporter);
                    this.checkTypePrivacyOfSignatures(declSymbol, typeSymbol.getIndexSignatures(), privacyErrorReporter);
                } else if (typeSymbol.kind === TypeScript.PullElementKind.TypeParameter) {
                    this.checkSymbolPrivacy(declSymbol, typeSymbol.getConstraint(), privacyErrorReporter);
                }

                typeSymbol.inSymbolPrivacyCheck = false;

                if (!isNamedType) {
                    return;
                }
            }

            // Check flags for the symbol itself
            if (declSymbol.isExternallyVisible()) {
                // Check if type symbol is externally visible
                var symbolIsVisible = symbol.isExternallyVisible();

                // If Visible check if the type is part of dynamic module
                if (symbolIsVisible && symbol.kind !== TypeScript.PullElementKind.TypeParameter) {
                    var symbolPath = symbol.pathToRoot();
                    var declSymbolPath = declSymbol.pathToRoot();

                    // Symbols are from different dynamic modules
                    if (symbolPath[symbolPath.length - 1].kind === TypeScript.PullElementKind.DynamicModule && declSymbolPath[declSymbolPath.length - 1].kind === TypeScript.PullElementKind.DynamicModule && declSymbolPath[declSymbolPath.length - 1] !== symbolPath[symbolPath.length - 1]) {
                        // Declaration symbol is from different modules
                        // Type may not be visible without import statement
                        symbolIsVisible = false;
                        var declSymbolScope = declSymbolPath[declSymbolPath.length - 1];
                        for (var i = symbolPath.length - 1; i >= 0; i--) {
                            var aliasSymbols = symbolPath[i].getExternalAliasedSymbols(declSymbolScope);
                            if (aliasSymbols) {
                                // Visible type.
                                symbolIsVisible = true;
                                aliasSymbols[0].setTypeUsedExternally();
                                break;
                            }
                        }
                        symbol = symbolPath[symbolPath.length - 1];
                    }
                } else if (symbol.kind === TypeScript.PullElementKind.TypeAlias) {
                    var aliasSymbol = symbol;
                    symbolIsVisible = true;
                    aliasSymbol.setTypeUsedExternally();
                }

                if (!symbolIsVisible) {
                    // declaration is visible from outside but the type isnt - Report error
                    privacyErrorReporter(symbol);
                }
            }
        };

        PullTypeResolver.prototype.checkTypePrivacyOfSignatures = function (declSymbol, signatures, privacyErrorReporter) {
            for (var i = 0; i < signatures.length; i++) {
                var signature = signatures[i];
                if (signatures.length > 1 && signature.isDefinition()) {
                    continue;
                }

                var typeParams = signature.getTypeParameters();
                for (var j = 0; j < typeParams.length; j++) {
                    this.checkSymbolPrivacy(declSymbol, typeParams[j], privacyErrorReporter);
                }

                var params = signature.parameters;
                for (var j = 0; j < params.length; j++) {
                    var paramType = params[j].type;
                    this.checkSymbolPrivacy(declSymbol, paramType, privacyErrorReporter);
                }

                var returnType = signature.returnType;
                this.checkSymbolPrivacy(declSymbol, returnType, privacyErrorReporter);
            }
        };

        PullTypeResolver.prototype.typeParameterOfTypeDeclarationPrivacyErrorReporter = function (classOrInterface, typeParameterAST, typeParameter, symbol, context) {
            var decl = this.semanticInfoChain.getDeclForAST(classOrInterface);
            var enclosingDecl = this.getEnclosingDecl(decl);
            var enclosingSymbol = enclosingDecl ? enclosingDecl.getSymbol() : null;
            var messageCode;

            var typeParameters = classOrInterface.kind() === 131 /* ClassDeclaration */ ? classOrInterface.typeParameterList : classOrInterface.typeParameterList;

            var typeSymbol = symbol;
            var typeSymbolName = typeSymbol.getScopedName(enclosingSymbol);
            if (typeSymbol.isContainer() && !typeSymbol.isEnum()) {
                if (!TypeScript.isQuoted(typeSymbolName)) {
                    typeSymbolName = "'" + typeSymbolName + "'";
                }
                if (classOrInterface.kind() === 131 /* ClassDeclaration */) {
                    // Class
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_exported_class_is_using_inaccessible_module_1;
                } else {
                    // Interface
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_exported_interface_is_using_inaccessible_module_1;
                }
            } else {
                if (classOrInterface.kind() === 131 /* ClassDeclaration */) {
                    // Class
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_exported_class_has_or_is_using_private_type_1;
                } else {
                    // Interface
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_exported_interface_has_or_is_using_private_type_1;
                }
            }

            var messageArguments = [typeParameter.getScopedName(enclosingSymbol, false, true), typeSymbolName];
            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(typeParameterAST, messageCode, messageArguments));
        };

        PullTypeResolver.prototype.baseListPrivacyErrorReporter = function (classOrInterface, declSymbol, baseAst, isExtendedType, symbol, context) {
            var decl = this.semanticInfoChain.getDeclForAST(classOrInterface);
            var enclosingDecl = this.getEnclosingDecl(decl);
            var enclosingSymbol = enclosingDecl ? enclosingDecl.getSymbol() : null;
            var messageCode;

            var typeSymbol = symbol;
            var typeSymbolName = typeSymbol.getScopedName(enclosingSymbol);
            if (typeSymbol.isContainer() && !typeSymbol.isEnum()) {
                if (!TypeScript.isQuoted(typeSymbolName)) {
                    typeSymbolName = "'" + typeSymbolName + "'";
                }
                if (classOrInterface.kind() === 131 /* ClassDeclaration */) {
                    // Class
                    if (isExtendedType) {
                        messageCode = TypeScript.DiagnosticCode.Exported_class_0_extends_class_from_inaccessible_module_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Exported_class_0_implements_interface_from_inaccessible_module_1;
                    }
                } else {
                    // Interface
                    messageCode = TypeScript.DiagnosticCode.Exported_interface_0_extends_interface_from_inaccessible_module_1;
                }
            } else {
                if (classOrInterface.kind() === 131 /* ClassDeclaration */) {
                    // Class
                    if (isExtendedType) {
                        messageCode = TypeScript.DiagnosticCode.Exported_class_0_extends_private_class_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Exported_class_0_implements_private_interface_1;
                    }
                } else {
                    // Interface
                    messageCode = TypeScript.DiagnosticCode.Exported_interface_0_extends_private_interface_1;
                }
            }

            var messageArguments = [declSymbol.getScopedName(enclosingSymbol), typeSymbolName];
            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(baseAst, messageCode, messageArguments));
        };

        PullTypeResolver.prototype.variablePrivacyErrorReporter = function (declAST, declSymbol, symbol, context) {
            var typeSymbol = symbol;
            var enclosingDecl = this.getEnclosingDecl(declSymbol.getDeclarations()[0]);
            var enclosingSymbol = enclosingDecl ? enclosingDecl.getSymbol() : null;

            var isProperty = declSymbol.kind === TypeScript.PullElementKind.Property;
            var isPropertyOfClass = false;
            var declParent = declSymbol.getContainer();
            if (declParent && (declParent.kind === TypeScript.PullElementKind.Class || declParent.kind === TypeScript.PullElementKind.ConstructorMethod)) {
                isPropertyOfClass = true;
            }

            var messageCode;
            var typeSymbolName = typeSymbol.getScopedName(enclosingSymbol);
            if (typeSymbol.isContainer() && !typeSymbol.isEnum()) {
                if (!TypeScript.isQuoted(typeSymbolName)) {
                    typeSymbolName = "'" + typeSymbolName + "'";
                }

                if (declSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.Static)) {
                    messageCode = TypeScript.DiagnosticCode.Public_static_property_0_of_exported_class_is_using_inaccessible_module_1;
                } else if (isProperty) {
                    if (isPropertyOfClass) {
                        messageCode = TypeScript.DiagnosticCode.Public_property_0_of_exported_class_is_using_inaccessible_module_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Property_0_of_exported_interface_is_using_inaccessible_module_1;
                    }
                } else {
                    messageCode = TypeScript.DiagnosticCode.Exported_variable_0_is_using_inaccessible_module_1;
                }
            } else {
                if (declSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.Static)) {
                    messageCode = TypeScript.DiagnosticCode.Public_static_property_0_of_exported_class_has_or_is_using_private_type_1;
                } else if (isProperty) {
                    if (isPropertyOfClass) {
                        messageCode = TypeScript.DiagnosticCode.Public_property_0_of_exported_class_has_or_is_using_private_type_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Property_0_of_exported_interface_has_or_is_using_private_type_1;
                    }
                } else {
                    messageCode = TypeScript.DiagnosticCode.Exported_variable_0_has_or_is_using_private_type_1;
                }
            }

            var messageArguments = [declSymbol.getScopedName(enclosingSymbol), typeSymbolName];
            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(declAST, messageCode, messageArguments));
        };

        PullTypeResolver.prototype.checkFunctionTypePrivacy = function (funcDeclAST, isStatic, typeParameters, parameters, returnTypeAnnotation, block, context) {
            var _this = this;
            if (funcDeclAST.kind() === 222 /* FunctionExpression */ || funcDeclAST.kind() === 241 /* FunctionPropertyAssignment */ || (funcDeclAST.kind() === 139 /* GetAccessor */ && funcDeclAST.parent.parent.kind() === 215 /* ObjectLiteralExpression */) || (funcDeclAST.kind() === 140 /* SetAccessor */ && funcDeclAST.parent.parent.kind() === 215 /* ObjectLiteralExpression */)) {
                return;
            }

            var functionDecl = this.semanticInfoChain.getDeclForAST(funcDeclAST);
            var functionSymbol = functionDecl.getSymbol();
            ;
            var functionSignature;

            var isGetter = funcDeclAST.kind() === 139 /* GetAccessor */;
            var isSetter = funcDeclAST.kind() === 140 /* SetAccessor */;
            var isIndexSignature = functionDecl.kind === TypeScript.PullElementKind.IndexSignature;

            if (isGetter || isSetter) {
                var accessorSymbol = functionSymbol;
                functionSignature = (isGetter ? accessorSymbol.getGetter() : accessorSymbol.getSetter()).type.getCallSignatures()[0];
            } else {
                if (!functionSymbol) {
                    var parentDecl = functionDecl.getParentDecl();
                    functionSymbol = parentDecl.getSymbol();
                    if (functionSymbol && functionSymbol.isType() && !functionSymbol.isNamedTypeSymbol()) {
                        // Call Signature from the non named type
                        return;
                    }
                } else if (functionSymbol.kind === TypeScript.PullElementKind.Method && !isStatic && !functionSymbol.getContainer().isNamedTypeSymbol()) {
                    // method of the unnmaed type
                    return;
                }
                functionSignature = functionDecl.getSignatureSymbol();
            }

            // TypeParameters
            if (typeParameters && !isGetter && !isSetter && !isIndexSignature && funcDeclAST.kind() !== 137 /* ConstructorDeclaration */) {
                for (var i = 0; i < typeParameters.typeParameters.nonSeparatorCount(); i++) {
                    var typeParameterAST = typeParameters.typeParameters.nonSeparatorAt(i);
                    var typeParameter = this.resolveTypeParameterDeclaration(typeParameterAST, context);
                    this.checkSymbolPrivacy(functionSymbol, typeParameter, function (symbol) {
                        return _this.functionTypeArgumentArgumentTypePrivacyErrorReporter(funcDeclAST, isStatic, typeParameterAST, typeParameter, symbol, context);
                    });
                }
            }

            // Check function parameters
            if (!isGetter && !isIndexSignature) {
                var funcParams = functionSignature.parameters;
                for (var i = 0; i < funcParams.length; i++) {
                    this.checkSymbolPrivacy(functionSymbol, funcParams[i].type, function (symbol) {
                        return _this.functionArgumentTypePrivacyErrorReporter(funcDeclAST, isStatic, parameters, i, funcParams[i], symbol, context);
                    });
                }
            }

            // Check return type
            if (!isSetter) {
                this.checkSymbolPrivacy(functionSymbol, functionSignature.returnType, function (symbol) {
                    return _this.functionReturnTypePrivacyErrorReporter(funcDeclAST, isStatic, returnTypeAnnotation, block, functionSignature.returnType, symbol, context);
                });
            }
        };

        PullTypeResolver.prototype.functionTypeArgumentArgumentTypePrivacyErrorReporter = function (declAST, isStatic, typeParameterAST, typeParameter, symbol, context) {
            var decl = this.semanticInfoChain.getDeclForAST(declAST);
            var enclosingDecl = this.getEnclosingDecl(decl);
            var enclosingSymbol = enclosingDecl ? enclosingDecl.getSymbol() : null;

            var isMethod = decl.kind === TypeScript.PullElementKind.Method;
            var isMethodOfClass = false;
            var declParent = decl.getParentDecl();
            if (declParent && (declParent.kind === TypeScript.PullElementKind.Class || isStatic)) {
                isMethodOfClass = true;
            }

            var typeSymbol = symbol;
            var typeSymbolName = typeSymbol.getScopedName(enclosingSymbol);
            var messageCode;
            if (typeSymbol.isContainer() && !typeSymbol.isEnum()) {
                if (!TypeScript.isQuoted(typeSymbolName)) {
                    typeSymbolName = "'" + typeSymbolName + "'";
                }

                if (decl.kind === TypeScript.PullElementKind.ConstructSignature) {
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_constructor_signature_from_exported_interface_is_using_inaccessible_module_1;
                } else if (decl.kind === TypeScript.PullElementKind.CallSignature) {
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_call_signature_from_exported_interface_is_using_inaccessible_module_1;
                } else if (isMethod) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_public_static_method_from_exported_class_is_using_inaccessible_module_1;
                    } else if (isMethodOfClass) {
                        messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_public_method_from_exported_class_is_using_inaccessible_module_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_method_from_exported_interface_is_using_inaccessible_module_1;
                    }
                } else {
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_exported_function_is_using_inaccessible_module_1;
                }
            } else {
                if (decl.kind === TypeScript.PullElementKind.ConstructSignature) {
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_constructor_signature_from_exported_interface_has_or_is_using_private_type_1;
                } else if (decl.kind === TypeScript.PullElementKind.CallSignature) {
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_call_signature_from_exported_interface_has_or_is_using_private_type_1;
                } else if (isMethod) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_public_static_method_from_exported_class_has_or_is_using_private_type_1;
                    } else if (isMethodOfClass) {
                        messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_public_method_from_exported_class_has_or_is_using_private_type_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_method_from_exported_interface_has_or_is_using_private_type_1;
                    }
                } else {
                    messageCode = TypeScript.DiagnosticCode.TypeParameter_0_of_exported_function_has_or_is_using_private_type_1;
                }
            }

            if (messageCode) {
                var messageArgs = [typeParameter.getScopedName(enclosingSymbol, false, true), typeSymbolName];
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(typeParameterAST, messageCode, messageArgs));
            }
        };

        PullTypeResolver.prototype.functionArgumentTypePrivacyErrorReporter = function (declAST, isStatic, parameters, argIndex, paramSymbol, symbol, context) {
            var decl = this.semanticInfoChain.getDeclForAST(declAST);
            var enclosingDecl = this.getEnclosingDecl(decl);
            var enclosingSymbol = enclosingDecl ? enclosingDecl.getSymbol() : null;

            var isGetter = declAST.kind() === 139 /* GetAccessor */;
            var isSetter = declAST.kind() === 140 /* SetAccessor */;
            var isMethod = decl.kind === TypeScript.PullElementKind.Method;
            var isMethodOfClass = false;
            var declParent = decl.getParentDecl();
            if (declParent && (declParent.kind === TypeScript.PullElementKind.Class || isStatic)) {
                isMethodOfClass = true;
            }

            var typeSymbol = symbol;
            var typeSymbolName = typeSymbol.getScopedName(enclosingSymbol);
            var messageCode;
            if (typeSymbol.isContainer() && !typeSymbol.isEnum()) {
                if (!TypeScript.isQuoted(typeSymbolName)) {
                    typeSymbolName = "'" + typeSymbolName + "'";
                }

                if (declAST.kind() === 137 /* ConstructorDeclaration */) {
                    messageCode = TypeScript.DiagnosticCode.Parameter_0_of_constructor_from_exported_class_is_using_inaccessible_module_1;
                } else if (isSetter) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_public_static_property_setter_from_exported_class_is_using_inaccessible_module_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_public_property_setter_from_exported_class_is_using_inaccessible_module_1;
                    }
                } else if (decl.kind === TypeScript.PullElementKind.ConstructSignature) {
                    messageCode = TypeScript.DiagnosticCode.Parameter_0_of_constructor_signature_from_exported_interface_is_using_inaccessible_module_1;
                } else if (decl.kind === TypeScript.PullElementKind.CallSignature) {
                    messageCode = TypeScript.DiagnosticCode.Parameter_0_of_call_signature_from_exported_interface_is_using_inaccessible_module_1;
                } else if (isMethod) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_public_static_method_from_exported_class_is_using_inaccessible_module_1;
                    } else if (isMethodOfClass) {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_public_method_from_exported_class_is_using_inaccessible_module_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_method_from_exported_interface_is_using_inaccessible_module_1;
                    }
                } else if (!isGetter) {
                    messageCode = TypeScript.DiagnosticCode.Parameter_0_of_exported_function_is_using_inaccessible_module_1;
                }
            } else {
                if (declAST.kind() === 137 /* ConstructorDeclaration */) {
                    messageCode = TypeScript.DiagnosticCode.Parameter_0_of_constructor_from_exported_class_has_or_is_using_private_type_1;
                } else if (isSetter) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_public_static_property_setter_from_exported_class_has_or_is_using_private_type_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_public_property_setter_from_exported_class_has_or_is_using_private_type_1;
                    }
                } else if (decl.kind === TypeScript.PullElementKind.ConstructSignature) {
                    messageCode = TypeScript.DiagnosticCode.Parameter_0_of_constructor_signature_from_exported_interface_has_or_is_using_private_type_1;
                } else if (decl.kind === TypeScript.PullElementKind.CallSignature) {
                    messageCode = TypeScript.DiagnosticCode.Parameter_0_of_call_signature_from_exported_interface_has_or_is_using_private_type_1;
                } else if (isMethod) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_public_static_method_from_exported_class_has_or_is_using_private_type_1;
                    } else if (isMethodOfClass) {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_public_method_from_exported_class_has_or_is_using_private_type_1;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Parameter_0_of_method_from_exported_interface_has_or_is_using_private_type_1;
                    }
                } else if (!isGetter && decl.kind !== TypeScript.PullElementKind.IndexSignature) {
                    messageCode = TypeScript.DiagnosticCode.Parameter_0_of_exported_function_has_or_is_using_private_type_1;
                }
            }

            if (messageCode) {
                var parameter = parameters.astAt(argIndex);

                var messageArgs = [paramSymbol.getScopedName(enclosingSymbol), typeSymbolName];
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(parameter, messageCode, messageArgs));
            }
        };

        PullTypeResolver.prototype.functionReturnTypePrivacyErrorReporter = function (declAST, isStatic, returnTypeAnnotation, block, funcReturnType, symbol, context) {
            var _this = this;
            var decl = this.semanticInfoChain.getDeclForAST(declAST);
            var enclosingDecl = this.getEnclosingDecl(decl);

            var isGetter = declAST.kind() === 139 /* GetAccessor */;
            var isSetter = declAST.kind() === 140 /* SetAccessor */;
            var isMethod = decl.kind === TypeScript.PullElementKind.Method;
            var isMethodOfClass = false;
            var declParent = decl.getParentDecl();
            if (declParent && (declParent.kind === TypeScript.PullElementKind.Class || isStatic)) {
                isMethodOfClass = true;
            }

            var messageCode = null;
            var typeSymbol = symbol;
            var typeSymbolName = typeSymbol.getScopedName(enclosingDecl ? enclosingDecl.getSymbol() : null);
            if (typeSymbol.isContainer() && !typeSymbol.isEnum()) {
                if (!TypeScript.isQuoted(typeSymbolName)) {
                    typeSymbolName = "'" + typeSymbolName + "'";
                }

                if (isGetter) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_public_static_property_getter_from_exported_class_is_using_inaccessible_module_0;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_public_property_getter_from_exported_class_is_using_inaccessible_module_0;
                    }
                } else if (decl.kind === TypeScript.PullElementKind.ConstructSignature) {
                    messageCode = TypeScript.DiagnosticCode.Return_type_of_constructor_signature_from_exported_interface_is_using_inaccessible_module_0;
                } else if (decl.kind === TypeScript.PullElementKind.CallSignature) {
                    messageCode = TypeScript.DiagnosticCode.Return_type_of_call_signature_from_exported_interface_is_using_inaccessible_module_0;
                } else if (decl.kind === TypeScript.PullElementKind.IndexSignature) {
                    messageCode = TypeScript.DiagnosticCode.Return_type_of_index_signature_from_exported_interface_is_using_inaccessible_module_0;
                } else if (isMethod) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_public_static_method_from_exported_class_is_using_inaccessible_module_0;
                    } else if (isMethodOfClass) {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_public_method_from_exported_class_is_using_inaccessible_module_0;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_method_from_exported_interface_is_using_inaccessible_module_0;
                    }
                } else if (!isSetter && declAST.kind() !== 137 /* ConstructorDeclaration */) {
                    messageCode = TypeScript.DiagnosticCode.Return_type_of_exported_function_is_using_inaccessible_module_0;
                }
            } else {
                if (isGetter) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_public_static_property_getter_from_exported_class_has_or_is_using_private_type_0;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_public_property_getter_from_exported_class_has_or_is_using_private_type_0;
                    }
                } else if (decl.kind === TypeScript.PullElementKind.ConstructSignature) {
                    messageCode = TypeScript.DiagnosticCode.Return_type_of_constructor_signature_from_exported_interface_has_or_is_using_private_type_0;
                } else if (decl.kind === TypeScript.PullElementKind.CallSignature) {
                    messageCode = TypeScript.DiagnosticCode.Return_type_of_call_signature_from_exported_interface_has_or_is_using_private_type_0;
                } else if (decl.kind === TypeScript.PullElementKind.IndexSignature) {
                    messageCode = TypeScript.DiagnosticCode.Return_type_of_index_signature_from_exported_interface_has_or_is_using_private_type_0;
                } else if (isMethod) {
                    if (isStatic) {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_public_static_method_from_exported_class_has_or_is_using_private_type_0;
                    } else if (isMethodOfClass) {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_public_method_from_exported_class_has_or_is_using_private_type_0;
                    } else {
                        messageCode = TypeScript.DiagnosticCode.Return_type_of_method_from_exported_interface_has_or_is_using_private_type_0;
                    }
                } else if (!isSetter && declAST.kind() !== 137 /* ConstructorDeclaration */) {
                    messageCode = TypeScript.DiagnosticCode.Return_type_of_exported_function_has_or_is_using_private_type_0;
                }
            }

            if (messageCode) {
                var messageArguments = [typeSymbolName];
                var reportOnFuncDecl = false;

                if (returnTypeAnnotation) {
                    // NOTE: we don't want to report this diagnostics.  They'll already have been
                    // reported when we first hit the return statement.
                    var returnExpressionSymbol = this.resolveTypeReference(returnTypeAnnotation, context);

                    if (TypeScript.PullHelpers.typeSymbolsAreIdentical(returnExpressionSymbol, funcReturnType)) {
                        // Error coming from return annotation
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(returnTypeAnnotation, messageCode, messageArguments));
                    }
                }

                if (block) {
                    var reportErrorOnReturnExpressions = function (ast, walker) {
                        var go = true;
                        switch (ast.kind()) {
                            case 129 /* FunctionDeclaration */:
                            case 219 /* SimpleArrowFunctionExpression */:
                            case 218 /* ParenthesizedArrowFunctionExpression */:
                            case 222 /* FunctionExpression */:
                                // don't recurse into a function decl - we don't want to confuse a nested
                                // return type with the top-level function's return type
                                go = false;
                                break;

                            case 150 /* ReturnStatement */:
                                var returnStatement = ast;
                                var returnExpressionSymbol = _this.resolveAST(returnStatement.expression, false, context).type;

                                // Check if return statement's type matches the one that we concluded
                                if (TypeScript.PullHelpers.typeSymbolsAreIdentical(returnExpressionSymbol, funcReturnType)) {
                                    context.postDiagnostic(_this.semanticInfoChain.diagnosticFromAST(returnStatement, messageCode, messageArguments));
                                } else {
                                    reportOnFuncDecl = true;
                                }
                                go = false;
                                break;

                            default:
                                break;
                        }

                        walker.options.goChildren = go;
                        return ast;
                    };

                    TypeScript.getAstWalkerFactory().walk(block, reportErrorOnReturnExpressions);
                }

                if (reportOnFuncDecl) {
                    // Show on function decl
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(declAST, messageCode, messageArguments));
                }
            }
        };

        PullTypeResolver.prototype.enclosingClassIsDerived = function (classDecl) {
            TypeScript.Debug.assert(classDecl.kind === TypeScript.PullElementKind.Class);

            var classSymbol = classDecl.getSymbol();
            return classSymbol.getExtendedTypes().length > 0;
        };

        PullTypeResolver.prototype.isSuperInvocationExpression = function (ast) {
            if (ast.kind() === 213 /* InvocationExpression */) {
                var invocationExpression = ast;
                if (invocationExpression.expression.kind() === 50 /* SuperKeyword */) {
                    return true;
                }
            }

            return false;
        };

        PullTypeResolver.prototype.isSuperInvocationExpressionStatement = function (node) {
            if (node && node.kind() === 149 /* ExpressionStatement */) {
                var expressionStatement = node;
                if (this.isSuperInvocationExpression(expressionStatement.expression)) {
                    return true;
                }
            }
            return false;
        };

        PullTypeResolver.prototype.getFirstStatementOfBlockOrNull = function (block) {
            if (block && block.statements && block.statements.childCount() > 0) {
                return block.statements.childAt(0);
            }

            return null;
        };

        PullTypeResolver.prototype.superCallMustBeFirstStatementInConstructor = function (constructorDecl) {
            TypeScript.Debug.assert(constructorDecl.kind === TypeScript.PullElementKind.ConstructorMethod);

            /*
            The first statement in the body of a constructor must be a super call if both of the following are true:
            •   The containing class is a derived class.
            •   The constructor declares parameter properties or the containing class declares instance member variables with initializers.
            In such a required super call, it is a compile-time error for argument expressions to reference this.
            */
            if (constructorDecl) {
                var enclosingClass = constructorDecl.getParentDecl();

                var classSymbol = enclosingClass.getSymbol();
                if (classSymbol.getExtendedTypes().length === 0) {
                    return false;
                }

                var classMembers = classSymbol.getMembers();
                for (var i = 0, n1 = classMembers.length; i < n1; i++) {
                    var member = classMembers[i];

                    if (member.kind === TypeScript.PullElementKind.Property) {
                        var declarations = member.getDeclarations();
                        for (var j = 0, n2 = declarations.length; j < n2; j++) {
                            var declaration = declarations[j];
                            var ast = this.semanticInfoChain.getASTForDecl(declaration);
                            if (ast.kind() === 242 /* Parameter */) {
                                return true;
                            }

                            if (ast.kind() === 136 /* MemberVariableDeclaration */) {
                                var variableDeclarator = ast;
                                if (variableDeclarator.variableDeclarator.equalsValueClause) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            return false;
        };

        PullTypeResolver.prototype.checkForThisCaptureInArrowFunction = function (expression) {
            var enclosingDecl = this.getEnclosingDeclForAST(expression);

            var declPath = enclosingDecl.getParentPath();

            // work back up the decl path, until you can find a class
            // PULLTODO: Obviously not completely correct, but this sufficiently unblocks testing of the pull model
            if (declPath.length) {
                var inArrowFunction = false;
                for (var i = declPath.length - 1; i >= 0; i--) {
                    var decl = declPath[i];
                    var declKind = decl.kind;
                    var declFlags = decl.flags;

                    if (declKind === TypeScript.PullElementKind.FunctionExpression && TypeScript.hasFlag(declFlags, TypeScript.PullElementFlags.ArrowFunction)) {
                        inArrowFunction = true;
                        continue;
                    }

                    if (inArrowFunction) {
                        if (declKind === TypeScript.PullElementKind.Function || declKind === TypeScript.PullElementKind.Method || declKind === TypeScript.PullElementKind.ConstructorMethod || declKind === TypeScript.PullElementKind.GetAccessor || declKind === TypeScript.PullElementKind.SetAccessor || declKind === TypeScript.PullElementKind.FunctionExpression || declKind === TypeScript.PullElementKind.Class || declKind === TypeScript.PullElementKind.Container || declKind === TypeScript.PullElementKind.DynamicModule || declKind === TypeScript.PullElementKind.Script) {
                            decl.setFlags(decl.flags | TypeScript.PullElementFlags.MustCaptureThis);

                            // If we're accessing 'this' in a class, then the class constructor
                            // needs to be marked as capturing 'this'.
                            if (declKind === TypeScript.PullElementKind.Class) {
                                var constructorSymbol = decl.getSymbol().getConstructorMethod();
                                var constructorDecls = constructorSymbol.getDeclarations();
                                for (var i = 0; i < constructorDecls.length; i++) {
                                    constructorDecls[i].flags = constructorDecls[i].flags | TypeScript.PullElementFlags.MustCaptureThis;
                                }
                            }
                            break;
                        }
                    } else if (declKind === TypeScript.PullElementKind.Function || declKind === TypeScript.PullElementKind.FunctionExpression) {
                        break;
                    }
                }
            }
        };

        PullTypeResolver.prototype.typeCheckMembersAgainstIndexer = function (containerType, containerTypeDecl, context) {
            // Check all the members defined in this symbol's declarations (no base classes)
            var indexSignatures = this.getBothKindsOfIndexSignaturesExcludingAugmentedType(containerType, context);
            var stringSignature = indexSignatures.stringSignature;
            var numberSignature = indexSignatures.numericSignature;

            if (stringSignature || numberSignature) {
                var members = containerTypeDecl.getChildDecls();
                for (var i = 0; i < members.length; i++) {
                    // Make sure the member is actually contained by the containerType, and not its associated constructor type
                    var member = members[i];
                    if ((member.name || (member.kind === TypeScript.PullElementKind.Property && member.name === "")) && member.kind !== TypeScript.PullElementKind.ConstructorMethod && !TypeScript.hasFlag(member.flags, TypeScript.PullElementFlags.Static)) {
                        // Decide whether to check against the number or string signature
                        var memberSymbol = member.getSymbol();
                        var relevantSignature = this.determineRelevantIndexerForMember(memberSymbol, numberSignature, stringSignature);
                        if (relevantSignature) {
                            var comparisonInfo = new TypeComparisonInfo();
                            if (!this.sourceIsAssignableToTarget(memberSymbol.type, relevantSignature.returnType, member.ast(), context, comparisonInfo)) {
                                this.reportErrorThatMemberIsNotSubtypeOfIndexer(memberSymbol, relevantSignature, member.ast(), context, comparisonInfo);
                            }
                        }
                    }
                }
            }
        };

        PullTypeResolver.prototype.determineRelevantIndexerForMember = function (member, numberIndexSignature, stringIndexSignature) {
            if (numberIndexSignature && TypeScript.PullHelpers.isNameNumeric(member.name)) {
                return numberIndexSignature;
            } else if (stringIndexSignature) {
                return stringIndexSignature;
            }

            return null;
        };

        PullTypeResolver.prototype.reportErrorThatMemberIsNotSubtypeOfIndexer = function (member, indexSignature, astForError, context, comparisonInfo) {
            var enclosingSymbol = this.getEnclosingSymbolForAST(astForError);
            if (indexSignature.parameters[0].type === this.semanticInfoChain.numberTypeSymbol) {
                if (comparisonInfo.message) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(astForError, TypeScript.DiagnosticCode.All_numerically_named_properties_must_be_assignable_to_numeric_indexer_type_0_NL_1, [indexSignature.returnType.toString(enclosingSymbol), comparisonInfo.message]));
                } else {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(astForError, TypeScript.DiagnosticCode.All_numerically_named_properties_must_be_assignable_to_numeric_indexer_type_0, [indexSignature.returnType.toString(enclosingSymbol)]));
                }
            } else {
                if (comparisonInfo.message) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(astForError, TypeScript.DiagnosticCode.All_named_properties_must_be_assignable_to_string_indexer_type_0_NL_1, [indexSignature.returnType.toString(enclosingSymbol), comparisonInfo.message]));
                } else {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(astForError, TypeScript.DiagnosticCode.All_named_properties_must_be_assignable_to_string_indexer_type_0, [indexSignature.returnType.toString(enclosingSymbol)]));
                }
            }
        };

        PullTypeResolver.prototype.typeCheckIfTypeMemberPropertyOkToOverride = function (typeSymbol, extendedType, typeMember, extendedTypeMember, enclosingDecl, comparisonInfo) {
            if (!typeSymbol.isClass()) {
                return true;
            }

            var typeMemberKind = typeMember.kind;
            var extendedMemberKind = extendedTypeMember.kind;

            if (typeMemberKind === extendedMemberKind) {
                return true;
            }

            var errorCode;
            if (typeMemberKind === TypeScript.PullElementKind.Property) {
                if (typeMember.isAccessor()) {
                    errorCode = TypeScript.DiagnosticCode.Class_0_defines_instance_member_accessor_1_but_extended_class_2_defines_it_as_instance_member_function;
                } else {
                    errorCode = TypeScript.DiagnosticCode.Class_0_defines_instance_member_property_1_but_extended_class_2_defines_it_as_instance_member_function;
                }
            } else if (typeMemberKind === TypeScript.PullElementKind.Method) {
                if (extendedTypeMember.isAccessor()) {
                    errorCode = TypeScript.DiagnosticCode.Class_0_defines_instance_member_function_1_but_extended_class_2_defines_it_as_instance_member_accessor;
                } else {
                    errorCode = TypeScript.DiagnosticCode.Class_0_defines_instance_member_function_1_but_extended_class_2_defines_it_as_instance_member_property;
                }
            }

            var message = TypeScript.getDiagnosticMessage(errorCode, [typeSymbol.toString(), typeMember.getScopedNameEx().toString(), extendedType.toString()]);
            comparisonInfo.addMessage(message);
            return false;
        };

        PullTypeResolver.prototype.typeCheckIfTypeExtendsType = function (classOrInterface, name, typeSymbol, extendedType, enclosingDecl, context) {
            var typeMembers = typeSymbol.getMembers();

            var comparisonInfo = new TypeComparisonInfo();
            var foundError = false;
            var foundError1 = false;
            var foundError2 = false;

            for (var i = 0; i < typeMembers.length; i++) {
                var propName = typeMembers[i].name;
                var extendedTypeProp = extendedType.findMember(propName, true);
                if (extendedTypeProp) {
                    this.resolveDeclaredSymbol(extendedTypeProp, context);
                    foundError1 = !this.typeCheckIfTypeMemberPropertyOkToOverride(typeSymbol, extendedType, typeMembers[i], extendedTypeProp, enclosingDecl, comparisonInfo);

                    if (!foundError1) {
                        foundError2 = !this.sourcePropertyIsAssignableToTargetProperty(typeSymbol, extendedType, typeMembers[i], extendedTypeProp, classOrInterface, context, comparisonInfo);
                    }

                    if (foundError1 || foundError2) {
                        foundError = true;
                        break;
                    }
                }
            }

            // Check call signatures
            if (!foundError && typeSymbol.hasOwnCallSignatures()) {
                foundError = !this.sourceCallSignaturesAreAssignableToTargetCallSignatures(typeSymbol, extendedType, classOrInterface, context, comparisonInfo);
            }

            // Check construct signatures
            if (!foundError && typeSymbol.hasOwnConstructSignatures()) {
                foundError = !this.sourceConstructSignaturesAreAssignableToTargetConstructSignatures(typeSymbol, extendedType, classOrInterface, context, comparisonInfo);
            }

            // Check index signatures
            if (!foundError && typeSymbol.hasOwnIndexSignatures()) {
                foundError = !this.sourceIndexSignaturesAreAssignableToTargetIndexSignatures(typeSymbol, extendedType, classOrInterface, context, comparisonInfo);
            }

            if (!foundError && typeSymbol.isClass()) {
                // If there is base class verify the constructor type is subtype of base class
                var typeConstructorType = typeSymbol.getConstructorMethod().type;
                var typeConstructorTypeMembers = typeConstructorType.getMembers();
                if (typeConstructorTypeMembers.length) {
                    var extendedConstructorType = extendedType.getConstructorMethod().type;
                    var comparisonInfoForPropTypeCheck = new TypeComparisonInfo(comparisonInfo);

                    for (var i = 0; i < typeConstructorTypeMembers.length; i++) {
                        var propName = typeConstructorTypeMembers[i].name;
                        var extendedConstructorTypeProp = extendedConstructorType.findMember(propName, true);
                        if (extendedConstructorTypeProp) {
                            if (!extendedConstructorTypeProp.isResolved) {
                                this.resolveDeclaredSymbol(extendedConstructorTypeProp, context);
                            }

                            if (!this.sourcePropertyIsAssignableToTargetProperty(typeConstructorType, extendedConstructorType, typeConstructorTypeMembers[i], extendedConstructorTypeProp, classOrInterface, context, comparisonInfo)) {
                                foundError = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (foundError) {
                var errorCode;
                if (typeSymbol.isClass()) {
                    errorCode = TypeScript.DiagnosticCode.Class_0_cannot_extend_class_1_NL_2;
                } else {
                    if (extendedType.isClass()) {
                        errorCode = TypeScript.DiagnosticCode.Interface_0_cannot_extend_class_1_NL_2;
                    } else {
                        errorCode = TypeScript.DiagnosticCode.Interface_0_cannot_extend_interface_1_NL_2;
                    }
                }

                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(name, errorCode, [typeSymbol.getScopedName(), extendedType.getScopedName(), comparisonInfo.message]));
            }
        };

        PullTypeResolver.prototype.typeCheckIfClassImplementsType = function (classDecl, classSymbol, implementedType, enclosingDecl, context) {
            var comparisonInfo = new TypeComparisonInfo();
            var foundError = !this.sourceMembersAreAssignableToTargetMembers(classSymbol, implementedType, classDecl, context, comparisonInfo);
            if (!foundError) {
                foundError = !this.sourceCallSignaturesAreAssignableToTargetCallSignatures(classSymbol, implementedType, classDecl, context, comparisonInfo);
                if (!foundError) {
                    foundError = !this.sourceConstructSignaturesAreAssignableToTargetConstructSignatures(classSymbol, implementedType, classDecl, context, comparisonInfo);
                    if (!foundError) {
                        foundError = !this.sourceIndexSignaturesAreAssignableToTargetIndexSignatures(classSymbol, implementedType, classDecl, context, comparisonInfo);
                    }
                }
            }

            // Report error
            if (foundError) {
                var enclosingSymbol = this.getEnclosingSymbolForAST(classDecl);
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(classDecl.identifier, TypeScript.DiagnosticCode.Class_0_declares_interface_1_but_does_not_implement_it_NL_2, [classSymbol.getScopedName(enclosingSymbol), implementedType.getScopedName(enclosingSymbol), comparisonInfo.message]));
            }
        };

        PullTypeResolver.prototype.computeValueSymbolFromAST = function (valueDeclAST, context) {
            // Disable type check because we do not want to report errors about missing symbols when resolving the
            // expression as the value expression
            var prevInTypeCheck = context.inTypeCheck;
            context.inTypeCheck = false;

            var typeSymbolAlias = this.semanticInfoChain.getAliasSymbolForAST(valueDeclAST);

            if (valueDeclAST.kind() == 11 /* IdentifierName */) {
                var valueSymbol = this.computeNameExpression(valueDeclAST, context);
            } else {
                TypeScript.Debug.assert(valueDeclAST.kind() == 121 /* QualifiedName */);
                var qualifiedName = valueDeclAST;

                // Compute lhs so we can compute dotted expression of the lhs.
                // We cant directly resolve this expression because otherwise we would end up using cached value of lhs
                // and that might not be the right one when resolving this expression as value expression
                var lhs = this.computeValueSymbolFromAST(qualifiedName.left, context);
                var valueSymbol = this.computeDottedNameExpressionFromLHS(lhs.symbol, qualifiedName.left, qualifiedName.right, context, false);
            }
            var valueSymbolAlias = this.semanticInfoChain.getAliasSymbolForAST(valueDeclAST);

            // Reset the alias value
            this.semanticInfoChain.setAliasSymbolForAST(valueDeclAST, typeSymbolAlias);
            context.inTypeCheck = prevInTypeCheck;

            return { symbol: valueSymbol, alias: valueSymbolAlias };
        };

        PullTypeResolver.prototype.hasClassTypeSymbolConflictAsValue = function (baseDeclAST, typeSymbol, enclosingDecl, context) {
            var typeSymbolAlias = this.semanticInfoChain.getAliasSymbolForAST(baseDeclAST);

            var valueDeclAST = baseDeclAST.kind() == 126 /* GenericType */ ? baseDeclAST.name : baseDeclAST;
            var valueSymbolInfo = this.computeValueSymbolFromAST(valueDeclAST, context);
            var valueSymbol = valueSymbolInfo.symbol;
            var valueSymbolAlias = valueSymbolInfo.alias;

            // If aliases are same
            if (typeSymbolAlias && valueSymbolAlias) {
                return typeSymbolAlias !== valueSymbolAlias;
            }

            // Verify if value refers to same class;
            if (!valueSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.ClassConstructorVariable)) {
                return true;
            }

            var associatedContainerType = valueSymbol.type ? valueSymbol.type.getAssociatedContainerType() : null;

            if (associatedContainerType) {
                // We may have specialized the typeSymbol to any for error recovery, as in the following example:
                // class A<T> { }
                // class B extends A { }
                // Since A was not given type arguments (which is an error), we may have specialized it to any, in which case A<any> != A<T>.
                // So we need to compare associatedContainerType to the rootSymbol (the unspecialized version) of typeSymbol
                return associatedContainerType !== typeSymbol.getRootSymbol();
            }

            return true;
        };

        PullTypeResolver.prototype.typeCheckBase = function (classOrInterface, name, typeSymbol, baseDeclAST, isExtendedType, enclosingDecl, context) {
            var _this = this;
            var typeDecl = this.semanticInfoChain.getDeclForAST(classOrInterface);

            var baseType = this.resolveTypeReference(baseDeclAST, context).type;

            if (!baseType) {
                return;
            }

            var typeDeclIsClass = typeSymbol.isClass();

            if (!typeSymbol.isValidBaseKind(baseType, isExtendedType)) {
                // Report error about invalid base kind
                if (!baseType.isError()) {
                    if (isExtendedType) {
                        if (typeDeclIsClass) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(baseDeclAST, TypeScript.DiagnosticCode.A_class_may_only_extend_another_class));
                        } else {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(baseDeclAST, TypeScript.DiagnosticCode.An_interface_may_only_extend_another_class_or_interface));
                        }
                    } else {
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(baseDeclAST, TypeScript.DiagnosticCode.A_class_may_only_implement_another_class_or_interface));
                    }
                }
                return;
            } else if (typeDeclIsClass && isExtendedType) {
                // Verify if the class extends another class verify the value position resolves to the same type expression
                if (this.hasClassTypeSymbolConflictAsValue(baseDeclAST, baseType, enclosingDecl, context)) {
                    // Report error
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(baseDeclAST, TypeScript.DiagnosticCode.Type_name_0_in_extends_clause_does_not_reference_constructor_function_for_1, [TypeScript.ASTHelpers.getNameOfIdenfierOrQualifiedName(baseDeclAST.kind() == 126 /* GenericType */ ? baseDeclAST.name : baseDeclAST), baseType.toString(enclosingDecl ? enclosingDecl.getSymbol() : null)]));
                }
            }

            // Check if its a recursive extend/implement type
            if (baseType.hasBase(typeSymbol)) {
                typeSymbol.setHasBaseTypeConflict();
                baseType.setHasBaseTypeConflict();

                // Report error
                context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(name, typeDeclIsClass ? TypeScript.DiagnosticCode.Class_0_is_recursively_referenced_as_a_base_type_of_itself : TypeScript.DiagnosticCode.Interface_0_is_recursively_referenced_as_a_base_type_of_itself, [typeSymbol.getScopedName()]));
                return;
            }

            if (isExtendedType) {
                // Verify all own overriding members are subtype
                this.typeCheckCallBacks.push(function (context) {
                    return _this.typeCheckIfTypeExtendsType(classOrInterface, name, typeSymbol, baseType, enclosingDecl, context);
                });
            } else {
                TypeScript.Debug.assert(classOrInterface.kind() === 131 /* ClassDeclaration */);

                // If class implementes interface or class, verify all the public members are implemented
                this.typeCheckCallBacks.push(function (context) {
                    return _this.typeCheckIfClassImplementsType(classOrInterface, typeSymbol, baseType, enclosingDecl, context);
                });
            }

            // Privacy error:
            this.checkSymbolPrivacy(typeSymbol, baseType, function (errorSymbol) {
                return _this.baseListPrivacyErrorReporter(classOrInterface, typeSymbol, baseDeclAST, isExtendedType, errorSymbol, context);
            });
        };

        PullTypeResolver.prototype.typeCheckBases = function (classOrInterface, name, heritageClauses, typeSymbol, enclosingDecl, context) {
            var _this = this;
            var extendsClause = TypeScript.ASTHelpers.getExtendsHeritageClause(heritageClauses);
            var implementsClause = TypeScript.ASTHelpers.getImplementsHeritageClause(heritageClauses);
            if (!extendsClause && !implementsClause) {
                return;
            }

            if (extendsClause) {
                for (var i = 0; i < extendsClause.typeNames.nonSeparatorCount(); i++) {
                    this.typeCheckBase(classOrInterface, name, typeSymbol, extendsClause.typeNames.nonSeparatorAt(i), true, enclosingDecl, context);
                }
            }

            if (typeSymbol.isClass()) {
                if (implementsClause) {
                    for (var i = 0; i < implementsClause.typeNames.nonSeparatorCount(); i++) {
                        this.typeCheckBase(classOrInterface, name, typeSymbol, implementsClause.typeNames.nonSeparatorAt(i), false, enclosingDecl, context);
                    }
                }
            } else if (extendsClause && !typeSymbol.hasBaseTypeConflict() && typeSymbol.getExtendedTypes().length > 1) {
                // October 16, 2013: Section 7.1:
                // Inherited properties with the same name must be identical (section 3.8.2).
                // If it is an interface it can extend multiple base types. We need to check for clashes
                // between inherited properties with the same name, per the spec. Note that we only do this
                // once per symbol, not once per declaration. We use the first declaration that has an
                // extends clause. Here is an example:
                // interface A {
                //    m: string;
                // }
                // interface B {
                //    m: number;
                // }
                // interface C extends A {}
                // interface C extends B {}
                // Here, we only report the error on the first C, because it is the first declaration
                // that has an extends clause. Since an interface cannot have an implements clause
                // (by the grammar) we only have to check that it has a heritage clause.
                var firstInterfaceASTWithExtendsClause = TypeScript.ArrayUtilities.firstOrDefault(typeSymbol.getDeclarations(), function (decl) {
                    return decl.ast().heritageClauses !== null;
                }).ast();
                if (classOrInterface === firstInterfaceASTWithExtendsClause) {
                    // Checking type compatibility between bases requires knowing the types of all
                    // base type members. Therefore, we have to delay this operation until all
                    // resolution has taken place. Doing the check immediately may cause a recursive
                    // resolution that results in a rogue "any".
                    this.typeCheckCallBacks.push(function (context) {
                        _this.checkTypeCompatibilityBetweenBases(classOrInterface.identifier, typeSymbol, context);
                    });
                }
            }
        };

        PullTypeResolver.prototype.checkTypeCompatibilityBetweenBases = function (name, typeSymbol, context) {
            // The membersBag will map each member name to its type and which base type we got it from
            var derivedIndexSignatures = typeSymbol.getOwnIndexSignatures();

            var inheritedMembersMap = TypeScript.createIntrinsicsObject();
            var inheritedIndexSignatures = new InheritedIndexSignatureInfo();

            var typeHasOwnNumberIndexer = false;
            var typeHasOwnStringIndexer = false;

            // Determine if this type has any index signatures of its own
            if (typeSymbol.hasOwnIndexSignatures()) {
                var ownIndexSignatures = typeSymbol.getOwnIndexSignatures();
                for (var i = 0; i < ownIndexSignatures.length; i++) {
                    if (ownIndexSignatures[i].parameters[0].type === this.semanticInfoChain.numberTypeSymbol) {
                        typeHasOwnNumberIndexer = true;
                    } else {
                        typeHasOwnStringIndexer = true;
                    }
                }
            }
            var baseTypes = typeSymbol.getExtendedTypes();
            for (var i = 0; i < baseTypes.length; i++) {
                // Check the member identity and the index signature identity between this base
                // and bases checked so far. Only report the first error.
                if (this.checkNamedPropertyIdentityBetweenBases(name, typeSymbol, baseTypes[i], inheritedMembersMap, context) || this.checkIndexSignatureIdentityBetweenBases(name, typeSymbol, baseTypes[i], inheritedIndexSignatures, typeHasOwnNumberIndexer, typeHasOwnStringIndexer, context)) {
                    return;
                }
            }

            // Check that number indexer is a subtype of the string indexer. If that check succeeds,
            // check all the inherited members against the relevant signature
            if (this.checkThatInheritedNumberSignatureIsSubtypeOfInheritedStringSignature(name, typeSymbol, inheritedIndexSignatures, context)) {
                return;
            }

            this.checkInheritedMembersAgainstInheritedIndexSignatures(name, typeSymbol, inheritedIndexSignatures, inheritedMembersMap, context);
        };

        // This method returns true if there was an error given, and false if there was no error.
        // The boolean value is used by the caller for short circuiting.
        PullTypeResolver.prototype.checkNamedPropertyIdentityBetweenBases = function (interfaceName, interfaceSymbol, baseTypeSymbol, inheritedMembersMap, context) {
            // October 16, 2013: Section 7.1:
            // Inherited properties with the same name must be identical (section 3.8.2).
            var baseMembers = baseTypeSymbol.getAllMembers(TypeScript.PullElementKind.Property | TypeScript.PullElementKind.Method, 0 /* all */);
            for (var i = 0; i < baseMembers.length; i++) {
                var member = baseMembers[i];
                var memberName = member.name;

                // Skip the member if it is shadowed in the derived type
                if (interfaceSymbol.findMember(memberName, false)) {
                    continue;
                }

                this.resolveDeclaredSymbol(member, context);

                // Error if there is already a member in the bag with that name, and it is not identical
                if (inheritedMembersMap[memberName]) {
                    var prevMember = inheritedMembersMap[memberName];
                    if (prevMember.baseOrigin !== baseTypeSymbol && !this.propertiesAreIdenticalWithNewEnclosingTypes(baseTypeSymbol, prevMember.baseOrigin, member, prevMember.memberSymbol, context)) {
                        var innerDiagnostic = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Named_properties_0_of_types_1_and_2_are_not_identical, [memberName, prevMember.baseOrigin.getScopedName(), baseTypeSymbol.getScopedName()]);
                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(interfaceName, TypeScript.DiagnosticCode.Interface_0_cannot_simultaneously_extend_types_1_and_2_NL_3, [interfaceSymbol.getDisplayName(), prevMember.baseOrigin.getScopedName(), baseTypeSymbol.getScopedName(), innerDiagnostic]));
                        return true;
                    }
                } else {
                    inheritedMembersMap[memberName] = new MemberWithBaseOrigin(member, baseTypeSymbol);
                }
            }

            return false;
        };

        // This method returns true if there was an error given, and false if there was no error.
        // The boolean value is used by the caller for short circuiting.
        PullTypeResolver.prototype.checkIndexSignatureIdentityBetweenBases = function (interfaceName, interfaceSymbol, baseTypeSymbol, allInheritedSignatures, derivedTypeHasOwnNumberSignature, derivedTypeHasOwnStringSignature, context) {
            if (derivedTypeHasOwnNumberSignature && derivedTypeHasOwnStringSignature) {
                return false;
            }

            // October 16, 2013:
            // Section 3.7.4:
            // An object type can contain at most one string index signature and one numeric index signature.
            var indexSignaturesFromThisBaseType = baseTypeSymbol.getIndexSignatures();
            for (var i = 0; i < indexSignaturesFromThisBaseType.length; i++) {
                var currentInheritedSignature = indexSignaturesFromThisBaseType[i];

                var parameterTypeIsString = currentInheritedSignature.parameters[0].type === this.semanticInfoChain.stringTypeSymbol;
                var parameterTypeIsNumber = currentInheritedSignature.parameters[0].type === this.semanticInfoChain.numberTypeSymbol;

                // Skip the signature if it is shadowed in the derived type
                if (parameterTypeIsString && derivedTypeHasOwnStringSignature || parameterTypeIsNumber && derivedTypeHasOwnNumberSignature) {
                    continue;
                }

                // Now error if there is already an inherited signature of the same kind as this one
                if (parameterTypeIsString) {
                    if (allInheritedSignatures.stringSignatureWithBaseOrigin) {
                        if (allInheritedSignatures.stringSignatureWithBaseOrigin.baseOrigin !== baseTypeSymbol && !this.typesAreIdentical(allInheritedSignatures.stringSignatureWithBaseOrigin.signature.returnType, currentInheritedSignature.returnType, context)) {
                            var innerDiagnostic = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Types_of_string_indexer_of_types_0_and_1_are_not_identical, [allInheritedSignatures.stringSignatureWithBaseOrigin.baseOrigin.getScopedName(), baseTypeSymbol.getScopedName()]);
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(interfaceName, TypeScript.DiagnosticCode.Interface_0_cannot_simultaneously_extend_types_1_and_2_NL_3, [interfaceSymbol.getDisplayName(), allInheritedSignatures.stringSignatureWithBaseOrigin.baseOrigin.getScopedName(), baseTypeSymbol.getScopedName(), innerDiagnostic]));
                            return true;
                        }
                    } else {
                        allInheritedSignatures.stringSignatureWithBaseOrigin = new SignatureWithBaseOrigin(currentInheritedSignature, baseTypeSymbol);
                    }
                } else if (parameterTypeIsNumber) {
                    if (allInheritedSignatures.numberSignatureWithBaseOrigin) {
                        if (allInheritedSignatures.numberSignatureWithBaseOrigin.baseOrigin !== baseTypeSymbol && !this.typesAreIdentical(allInheritedSignatures.numberSignatureWithBaseOrigin.signature.returnType, currentInheritedSignature.returnType, context)) {
                            var innerDiagnostic = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Types_of_number_indexer_of_types_0_and_1_are_not_identical, [allInheritedSignatures.numberSignatureWithBaseOrigin.baseOrigin.getScopedName(), baseTypeSymbol.getScopedName()]);
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(interfaceName, TypeScript.DiagnosticCode.Interface_0_cannot_simultaneously_extend_types_1_and_2_NL_3, [interfaceSymbol.getDisplayName(), allInheritedSignatures.numberSignatureWithBaseOrigin.baseOrigin.getScopedName(), baseTypeSymbol.getScopedName(), innerDiagnostic]));
                            return true;
                        }
                    } else {
                        allInheritedSignatures.numberSignatureWithBaseOrigin = new SignatureWithBaseOrigin(currentInheritedSignature, baseTypeSymbol);
                    }
                }
            }

            return false;
        };

        // This method returns true if there was an error given, and false if there was no error.
        // The boolean value is used by the caller for short circuiting.
        PullTypeResolver.prototype.checkInheritedMembersAgainstInheritedIndexSignatures = function (interfaceName, interfaceSymbol, inheritedIndexSignatures, inheritedMembers, context) {
            if (!inheritedIndexSignatures.stringSignatureWithBaseOrigin && !inheritedIndexSignatures.numberSignatureWithBaseOrigin) {
                return false;
            }

            // Now check that each named member is a subtype of its corresponding index signature type
            // October 16, 2013: Section 7.1:
            // All properties of the interface must satisfy the constraints implied by the index
            // signatures of the interface as specified in section 3.7.4.
            var comparisonInfo = new TypeComparisonInfo();
            var stringSignature = inheritedIndexSignatures.stringSignatureWithBaseOrigin && inheritedIndexSignatures.stringSignatureWithBaseOrigin.signature;
            var numberSignature = inheritedIndexSignatures.numberSignatureWithBaseOrigin && inheritedIndexSignatures.numberSignatureWithBaseOrigin.signature;
            for (var memberName in inheritedMembers) {
                var memberWithBaseOrigin = inheritedMembers[memberName];
                if (!memberWithBaseOrigin) {
                    continue;
                }

                var relevantSignature = this.determineRelevantIndexerForMember(memberWithBaseOrigin.memberSymbol, numberSignature, stringSignature);
                if (!relevantSignature) {
                    continue;
                }

                var relevantSignatureIsNumberSignature = relevantSignature.parameters[0].type === this.semanticInfoChain.numberTypeSymbol;
                var signatureBaseOrigin = relevantSignatureIsNumberSignature ? inheritedIndexSignatures.numberSignatureWithBaseOrigin.baseOrigin : inheritedIndexSignatures.stringSignatureWithBaseOrigin.baseOrigin;

                if (signatureBaseOrigin === memberWithBaseOrigin.baseOrigin) {
                    continue;
                }

                var memberIsSubtype = this.sourceIsAssignableToTarget(memberWithBaseOrigin.memberSymbol.type, relevantSignature.returnType, interfaceName, context, comparisonInfo);

                if (!memberIsSubtype) {
                    var enclosingSymbol = this.getEnclosingSymbolForAST(interfaceName);
                    if (relevantSignatureIsNumberSignature) {
                        var innerDiagnostic = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Type_of_property_0_in_type_1_is_not_assignable_to_number_indexer_type_in_type_2_NL_3, [memberName, memberWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), inheritedIndexSignatures.numberSignatureWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), comparisonInfo.message]);

                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(interfaceName, TypeScript.DiagnosticCode.Interface_0_cannot_simultaneously_extend_types_1_and_2_NL_3, [interfaceSymbol.getDisplayName(enclosingSymbol), memberWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), inheritedIndexSignatures.numberSignatureWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), innerDiagnostic]));
                    } else {
                        var innerDiagnostic = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Type_of_property_0_in_type_1_is_not_assignable_to_string_indexer_type_in_type_2_NL_3, [memberName, memberWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), inheritedIndexSignatures.stringSignatureWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), comparisonInfo.message]);

                        context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(interfaceName, TypeScript.DiagnosticCode.Interface_0_cannot_simultaneously_extend_types_1_and_2_NL_3, [interfaceSymbol.getDisplayName(enclosingSymbol), memberWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), inheritedIndexSignatures.stringSignatureWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), innerDiagnostic]));
                    }
                    return true;
                }
            }

            return false;
        };

        // This function assumes that we have neither a derived string indexer nor a derived
        // number indexer
        PullTypeResolver.prototype.checkThatInheritedNumberSignatureIsSubtypeOfInheritedStringSignature = function (interfaceName, interfaceSymbol, inheritedIndexSignatures, context) {
            if (inheritedIndexSignatures.numberSignatureWithBaseOrigin && inheritedIndexSignatures.stringSignatureWithBaseOrigin) {
                // If they originate in the same base, a separate error would have been reported
                if (inheritedIndexSignatures.numberSignatureWithBaseOrigin.baseOrigin === inheritedIndexSignatures.stringSignatureWithBaseOrigin.baseOrigin) {
                    return false;
                }

                var comparisonInfo = new TypeComparisonInfo();
                var signatureIsSubtype = this.sourceIsAssignableToTarget(inheritedIndexSignatures.numberSignatureWithBaseOrigin.signature.returnType, inheritedIndexSignatures.stringSignatureWithBaseOrigin.signature.returnType, interfaceName, context, comparisonInfo);

                if (!signatureIsSubtype) {
                    var enclosingSymbol = this.getEnclosingSymbolForAST(interfaceName);
                    var innerDiagnostic = TypeScript.getDiagnosticMessage(TypeScript.DiagnosticCode.Type_of_number_indexer_in_type_0_is_not_assignable_to_string_indexer_type_in_type_1_NL_2, [
                        inheritedIndexSignatures.numberSignatureWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol),
                        inheritedIndexSignatures.stringSignatureWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), comparisonInfo.message]);
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(interfaceName, TypeScript.DiagnosticCode.Interface_0_cannot_simultaneously_extend_types_1_and_2_NL_3, [
                        interfaceSymbol.getDisplayName(enclosingSymbol), inheritedIndexSignatures.numberSignatureWithBaseOrigin.baseOrigin.getScopedName(),
                        inheritedIndexSignatures.stringSignatureWithBaseOrigin.baseOrigin.getScopedName(enclosingSymbol), innerDiagnostic]));
                    return true;
                }
            }

            return false;
        };

        PullTypeResolver.prototype.checkAssignability = function (ast, source, target, context) {
            var comparisonInfo = new TypeComparisonInfo();

            var isAssignable = this.sourceIsAssignableToTarget(source, target, ast, context, comparisonInfo);

            if (!isAssignable) {
                var enclosingSymbol = this.getEnclosingSymbolForAST(ast);
                if (comparisonInfo.message) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Cannot_convert_0_to_1_NL_2, [source.toString(enclosingSymbol), target.toString(enclosingSymbol), comparisonInfo.message]));
                } else {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Cannot_convert_0_to_1, [source.toString(enclosingSymbol), target.toString(enclosingSymbol)]));
                }
            }
        };

        PullTypeResolver.prototype.isReference = function (ast, astSymbol) {
            // November 18, 2013
            // References are the subset of expressions that are permitted as the target of an
            // assignment.Specifically, references are combinations of identifiers(section 4.3),
            // parentheses(section 4.7), and property accesses(section 4.10).All other expression
            //  constructs described in this chapter are classified as values.
            if (ast.kind() === 217 /* ParenthesizedExpression */) {
                // A parenthesized LHS is valid if the expression it wraps is valid.
                return this.isReference(ast.expression, astSymbol);
            }

            if (ast.kind() !== 11 /* IdentifierName */ && ast.kind() !== 212 /* MemberAccessExpression */ && ast.kind() !== 221 /* ElementAccessExpression */) {
                return false;
            }

            // November 18, 2013
            // An identifier expression that references a variable or parameter is classified as a reference.
            // An identifier expression that references any other kind of entity is classified as a value(and therefore cannot be the target of an assignment).
            if (ast.kind() === 11 /* IdentifierName */) {
                if (astSymbol.kind === TypeScript.PullElementKind.Variable && astSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.Enum)) {
                    return false;
                }

                if (astSymbol.kind === TypeScript.PullElementKind.Variable && astSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.SomeInitializedModule)) {
                    return false;
                }

                if (astSymbol.kind === TypeScript.PullElementKind.ConstructorMethod || astSymbol.kind === TypeScript.PullElementKind.Function) {
                    return false;
                }
            }

            // Disallow assignment to an enum member (NOTE: not reflected in spec).
            if (ast.kind() === 212 /* MemberAccessExpression */ && astSymbol.kind === TypeScript.PullElementKind.EnumMember) {
                return false;
            }

            return true;
        };

        PullTypeResolver.prototype.checkForSuperMemberAccess = function (expression, name, resolvedName, context) {
            if (resolvedName) {
                if (expression.kind() === 50 /* SuperKeyword */ && !resolvedName.isError() && resolvedName.kind !== TypeScript.PullElementKind.Method) {
                    context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(name, TypeScript.DiagnosticCode.Only_public_methods_of_the_base_class_are_accessible_via_the_super_keyword));
                    return true;
                }
            }

            return false;
        };

        PullTypeResolver.prototype.getEnclosingDeclForAST = function (ast) {
            return this.semanticInfoChain.getEnclosingDecl(ast);
        };

        PullTypeResolver.prototype.getEnclosingSymbolForAST = function (ast) {
            var enclosingDecl = this.getEnclosingDeclForAST(ast);
            return enclosingDecl ? enclosingDecl.getSymbol() : null;
        };

        PullTypeResolver.prototype.checkForPrivateMemberAccess = function (name, expressionType, resolvedName, context) {
            if (resolvedName) {
                if (resolvedName.anyDeclHasFlag(TypeScript.PullElementFlags.Private)) {
                    var memberContainer = resolvedName.getContainer();
                    if (memberContainer && memberContainer.kind === TypeScript.PullElementKind.ConstructorType) {
                        memberContainer = memberContainer.getAssociatedContainerType();
                    }

                    if (memberContainer && memberContainer.isClass()) {
                        // We're accessing a private member of a class.  We can only do that if we're
                        // actually contained within that class.
                        var memberClass = memberContainer.getDeclarations()[0].ast();
                        TypeScript.Debug.assert(memberClass);

                        var containingClass = this.getEnclosingClassDeclaration(name);

                        if (!containingClass || containingClass !== memberClass) {
                            context.postDiagnostic(this.semanticInfoChain.diagnosticFromAST(name, TypeScript.DiagnosticCode._0_1_is_inaccessible, [memberContainer.toString(null, false), name.text()]));
                            return true;
                        }
                    }
                }
            }

            return false;
        };

        PullTypeResolver.prototype.instantiateType = function (type, typeParameterArgumentMap) {
            // if the type is a primitive type, nothing to do here
            if (type.isPrimitive()) {
                return type;
            }

            // if the type is an error, nothing to do here
            if (type.isError()) {
                return type;
            }

            if (typeParameterArgumentMap[type.pullSymbolID]) {
                return typeParameterArgumentMap[type.pullSymbolID];
            }

            type._resolveDeclaredSymbol();
            if (type.isTypeParameter()) {
                return this.instantiateTypeParameter(type, typeParameterArgumentMap);
            }

            if (type.wrapsSomeTypeParameter(typeParameterArgumentMap)) {
                return TypeScript.PullInstantiatedTypeReferenceSymbol.create(this, type, typeParameterArgumentMap);
            }

            return type;
        };

        // Instantiates the type parameter
        PullTypeResolver.prototype.instantiateTypeParameter = function (typeParameter, typeParameterArgumentMap) {
            // if the type parameter doesnot contain constraint the instantiated version of it is identical to itself
            var constraint = typeParameter.getConstraint();
            if (!constraint) {
                return typeParameter;
            }

            // Instantiate the constraint
            var instantiatedConstraint = this.instantiateType(constraint, typeParameterArgumentMap);

            // If the instantiated constraint == constraint of the type parameter, the instantiated type parameter is type parameter itself
            if (instantiatedConstraint == constraint) {
                return typeParameter;
            }

            // Look in cache
            var rootTypeParameter = typeParameter.getRootSymbol();
            var instantiation = rootTypeParameter.getSpecialization([instantiatedConstraint]);
            if (instantiation) {
                return instantiation;
            }

            // Create new instantiation of the type paramter with the constraint
            instantiation = new TypeScript.PullInstantiatedTypeParameterSymbol(rootTypeParameter, instantiatedConstraint);
            return instantiation;
        };

        // Note that the code below does not cache initializations of signatures.  We do this because we were only utilizing the cache on 1 our of
        // every 6 instantiations, and we would run the risk of getting this wrong when type checking calls within generic type declarations:
        // For example, if the signature is the root signature, it may not be safe to cache.  For example:
        //
        //  class C<T> {
        //      public p: T;
        //      public m<U>(u: U, t: T): void {}
        //      public n<U>() { m(null, this.p); }
        //  }
        //
        // In the code above, we don't want to cache the invocation of 'm' in 'n' against 'any', since the
        // signature to 'm' is only partially specialized
        PullTypeResolver.prototype.instantiateSignature = function (signature, typeParameterArgumentMap) {
            if (!signature.wrapsSomeTypeParameter(typeParameterArgumentMap)) {
                return signature;
            }

            var rootSignature = signature.getRootSymbol();
            var mutableTypeParameterMap = new TypeScript.PullInstantiationHelpers.MutableTypeArgumentMap(typeParameterArgumentMap);
            TypeScript.PullInstantiationHelpers.instantiateTypeArgument(this, signature, mutableTypeParameterMap);

            var instantiatedSignature = rootSignature.getSpecialization(mutableTypeParameterMap.typeParameterArgumentMap);
            if (instantiatedSignature) {
                return instantiatedSignature;
            }

            // Cleanup the type parameter argument map
            TypeScript.PullInstantiationHelpers.cleanUpTypeArgumentMap(signature, mutableTypeParameterMap);
            typeParameterArgumentMap = mutableTypeParameterMap.typeParameterArgumentMap;

            // Create a new one
            instantiatedSignature = new TypeScript.PullInstantiatedSignatureSymbol(rootSignature, typeParameterArgumentMap);

            // if the instantiation occurred via a recursive funciton invocation, the return type may be null so we should set it to any
            instantiatedSignature.returnType = this.instantiateType((rootSignature.returnType || this.semanticInfoChain.anyTypeSymbol), typeParameterArgumentMap);
            instantiatedSignature.functionType = this.instantiateType(rootSignature.functionType, typeParameterArgumentMap);

            var parameters = rootSignature.parameters;
            var parameter = null;

            if (parameters) {
                for (var j = 0; j < parameters.length; j++) {
                    parameter = new TypeScript.PullSymbol(parameters[j].name, TypeScript.PullElementKind.Parameter);
                    parameter.setRootSymbol(parameters[j]);

                    if (parameters[j].isOptional) {
                        parameter.isOptional = true;
                    }
                    if (parameters[j].isVarArg) {
                        parameter.isVarArg = true;
                        instantiatedSignature.hasVarArgs = true;
                    }
                    instantiatedSignature.addParameter(parameter, parameter.isOptional);

                    parameter.type = this.instantiateType(parameters[j].type, typeParameterArgumentMap);
                }
            }

            return instantiatedSignature;
        };
        PullTypeResolver.globalTypeCheckPhase = 0;
        return PullTypeResolver;
    })();
    TypeScript.PullTypeResolver = PullTypeResolver;

    var TypeComparisonInfo = (function () {
        function TypeComparisonInfo(sourceComparisonInfo, useSameIndent) {
            this.onlyCaptureFirstError = false;
            this.flags = 0 /* SuccessfulComparison */;
            this.message = "";
            this.stringConstantVal = null;
            this.indent = 1;
            if (sourceComparisonInfo) {
                this.flags = sourceComparisonInfo.flags;
                this.onlyCaptureFirstError = sourceComparisonInfo.onlyCaptureFirstError;
                this.stringConstantVal = sourceComparisonInfo.stringConstantVal;
                this.indent = sourceComparisonInfo.indent;
                if (!useSameIndent) {
                    this.indent++;
                }
            }
        }
        TypeComparisonInfo.prototype.indentString = function () {
            var result = "";

            for (var i = 0; i < this.indent; i++) {
                result += "\t";
            }

            return result;
        };

        TypeComparisonInfo.prototype.addMessage = function (message) {
            if (!this.onlyCaptureFirstError && this.message) {
                this.message = this.message + TypeScript.newLine() + this.indentString() + message;
            } else {
                this.message = this.indentString() + message;
            }
        };
        return TypeComparisonInfo;
    })();
    TypeScript.TypeComparisonInfo = TypeComparisonInfo;

    function getPropertyAssignmentNameTextFromIdentifier(identifier) {
        if (identifier.kind() === 11 /* IdentifierName */) {
            return { actualText: identifier.text(), memberName: identifier.valueText() };
        } else if (identifier.kind() === 14 /* StringLiteral */) {
            return { actualText: identifier.text(), memberName: identifier.valueText() };
        } else if (identifier.kind() === 13 /* NumericLiteral */) {
            return { actualText: identifier.text(), memberName: identifier.valueText() };
        } else {
            throw TypeScript.Errors.invalidOperation();
        }
    }
    TypeScript.getPropertyAssignmentNameTextFromIdentifier = getPropertyAssignmentNameTextFromIdentifier;

    function isTypesOnlyLocation(ast) {
        while (ast && ast.parent) {
            switch (ast.parent.kind()) {
                case 244 /* TypeAnnotation */:
                    return true;
                case 127 /* TypeQuery */:
                    // Inside a type query is actually an expression.
                    return false;
                case 125 /* ConstructorType */:
                    var constructorType = ast.parent;
                    if (constructorType.type === ast) {
                        return true;
                    }
                    break;
                case 123 /* FunctionType */:
                    var functionType = ast.parent;
                    if (functionType.type === ast) {
                        return true;
                    }
                    break;
                case 239 /* Constraint */:
                    var constraint = ast.parent;
                    if (constraint.type === ast) {
                        return true;
                    }
                    break;
                case 220 /* CastExpression */:
                    var castExpression = ast.parent;
                    return castExpression.type === ast;
                case 230 /* ExtendsHeritageClause */:
                case 231 /* ImplementsHeritageClause */:
                    return true;
                case 228 /* TypeArgumentList */:
                    return true;
                case 131 /* ClassDeclaration */:
                case 128 /* InterfaceDeclaration */:
                case 130 /* ModuleDeclaration */:
                case 129 /* FunctionDeclaration */:
                case 145 /* MethodSignature */:
                case 212 /* MemberAccessExpression */:
                case 242 /* Parameter */:
                    return false;
            }

            ast = ast.parent;
        }

        return false;
    }
    TypeScript.isTypesOnlyLocation = isTypesOnlyLocation;
})(TypeScript || (TypeScript = {}));
