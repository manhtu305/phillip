// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    var PullSymbolBinder = (function () {
        function PullSymbolBinder(semanticInfoChain) {
            this.semanticInfoChain = semanticInfoChain;
            this.declsBeingBound = [];
            this.inBindingOtherDeclsWalker = new TypeScript.PullHelpers.OtherPullDeclsWalker();
        }
        PullSymbolBinder.prototype.getParent = function (decl, returnInstanceType) {
            if (typeof returnInstanceType === "undefined") { returnInstanceType = false; }
            var parentDecl = decl.getParentDecl();

            if (parentDecl.kind === TypeScript.PullElementKind.Script) {
                return null;
            }

            var parent = parentDecl.getSymbol();

            if (!parent && parentDecl && !parentDecl.hasBeenBound()) {
                this.bindDeclToPullSymbol(parentDecl);
            }

            parent = parentDecl.getSymbol();
            if (parent) {
                var parentDeclKind = parentDecl.kind;
                if (parentDeclKind === TypeScript.PullElementKind.GetAccessor) {
                    parent = parent.getGetter();
                } else if (parentDeclKind === TypeScript.PullElementKind.SetAccessor) {
                    parent = parent.getSetter();
                }
            }

            if (parent) {
                if (returnInstanceType && parent.isType() && parent.isContainer()) {
                    var instanceSymbol = parent.getInstanceSymbol();

                    if (instanceSymbol) {
                        return instanceSymbol.type;
                    }
                }

                return parent.type;
            }

            return null;
        };

        PullSymbolBinder.prototype.findDeclsInContext = function (startingDecl, declKind, searchGlobally) {
            if (!searchGlobally) {
                var parentDecl = startingDecl.getParentDecl();
                return parentDecl.searchChildDecls(startingDecl.name, declKind);
            }

            var contextSymbolPath = startingDecl.getParentPath();

            // next, link back up to the enclosing context
            if (contextSymbolPath.length) {
                var copyOfContextSymbolPath = [];

                for (var i = 0; i < contextSymbolPath.length; i++) {
                    if (contextSymbolPath[i].kind & TypeScript.PullElementKind.Script) {
                        continue;
                    }
                    copyOfContextSymbolPath[copyOfContextSymbolPath.length] = contextSymbolPath[i].name;
                }

                return this.semanticInfoChain.findDecls(copyOfContextSymbolPath, declKind);
            }
        };

        // Called by all the bind methods when searching for existing symbols to reuse. Returns the symbol, or null if it does not exist.
        PullSymbolBinder.prototype.getExistingSymbol = function (decl, searchKind, parent) {
            var lookingForValue = (searchKind & TypeScript.PullElementKind.SomeValue) !== 0;
            var lookingForType = (searchKind & TypeScript.PullElementKind.SomeType) !== 0;
            var lookingForContainer = (searchKind & TypeScript.PullElementKind.SomeContainer) !== 0;
            var name = decl.name;
            if (parent) {
                var isExported = (decl.flags & 1 /* Exported */) !== 0;

                // First search for a nonmember
                var prevSymbol = null;
                if (lookingForValue) {
                    prevSymbol = parent.findContainedNonMember(name);
                } else if (lookingForType) {
                    prevSymbol = parent.findContainedNonMemberType(name, searchKind);
                } else if (lookingForContainer) {
                    prevSymbol = parent.findContainedNonMemberContainer(name, searchKind);
                }
                var prevIsExported = !prevSymbol;
                if (!prevSymbol) {
                    if (lookingForValue) {
                        prevSymbol = parent.findMember(name, false);
                    } else if (lookingForType) {
                        prevSymbol = parent.findNestedType(name, searchKind);
                    } else if (lookingForContainer) {
                        prevSymbol = parent.findNestedContainer(name, searchKind);
                    }
                }

                // If they are both exported, then they should definitely merge
                if (isExported && prevIsExported) {
                    return prevSymbol;
                }
                if (prevSymbol) {
                    // Check if they have the same parent (we use the LAST declaration to get the most positive answer on this)
                    var prevDecls = prevSymbol.getDeclarations();
                    var lastPrevDecl = prevDecls[prevDecls.length - 1];
                    var parentDecl = decl.getParentDecl();
                    var prevParentDecl = lastPrevDecl && lastPrevDecl.getParentDecl();
                    if (parentDecl !== prevParentDecl) {
                        // no merge
                        return null;
                    }

                    // They share the same parent, so merge them
                    return prevSymbol;
                }
            } else {
                var parentDecl = decl.getParentDecl();
                if (parentDecl && parentDecl.kind === TypeScript.PullElementKind.Script) {
                    return this.semanticInfoChain.findTopLevelSymbol(name, searchKind, decl);
                } else {
                    // The decl is in a control block (catch/with) that has no parent symbol. Luckily this type of parent can only have one decl.
                    var prevDecls = parentDecl && parentDecl.searchChildDecls(name, searchKind);
                    return prevDecls[0] && prevDecls[0].getSymbol();
                }
            }

            // Did not find a symbol
            return null;
        };

        // Reports an error and returns false if exports do not match. Otherwise, returns true.
        PullSymbolBinder.prototype.checkThatExportsMatch = function (decl, prevSymbol, reportError) {
            if (typeof reportError === "undefined") { reportError = true; }
            // Get export status of each (check against the last decl of the previous symbol)
            var isExported = (decl.flags & 1 /* Exported */) !== 0;
            var prevDecls = prevSymbol.getDeclarations();
            var prevIsExported = (prevDecls[prevDecls.length - 1].flags & 1 /* Exported */) !== 0;
            if ((isExported !== prevIsExported) && !prevSymbol.isSignature() && (decl.kind & TypeScript.PullElementKind.SomeSignature) === 0) {
                if (reportError) {
                    var ast = this.semanticInfoChain.getASTForDecl(decl);
                    this.semanticInfoChain.addDiagnosticFromAST(ast, TypeScript.DiagnosticCode.All_declarations_of_merged_declaration_0_must_be_exported_or_not_exported, [decl.getDisplayName()]);
                }
                return false;
            }

            return true;
        };

        PullSymbolBinder.prototype.getIndexForInsertingSignatureAtEndOfEnclosingDeclInSignatureList = function (signature, currentSignatures) {
            var signatureDecl = signature.getDeclarations()[0];
            TypeScript.Debug.assert(signatureDecl);
            var enclosingDecl = signatureDecl.getParentDecl();
            var indexToInsert = TypeScript.ArrayUtilities.indexOf(currentSignatures, function (someSignature) {
                return someSignature.getDeclarations()[0].getParentDecl() !== enclosingDecl;
            });
            return indexToInsert < 0 ? currentSignatures.length : indexToInsert;
        };

        //
        // decl binding
        //
        PullSymbolBinder.prototype.bindEnumDeclarationToPullSymbol = function (enumContainerDecl) {
            // 1. Test for existing decl - if it exists, use its symbol
            // 2. If no other decl exists, create a new symbol and use that one
            var enumName = enumContainerDecl.name;

            var enumContainerSymbol = null;
            var enumInstanceSymbol = null;
            var moduleInstanceTypeSymbol = null;

            var enumInstanceDecl = enumContainerDecl.getValueDecl();

            var enumDeclKind = enumContainerDecl.kind;

            var parent = this.getParent(enumContainerDecl);
            var parentInstanceSymbol = this.getParent(enumContainerDecl, true);
            var parentDecl = enumContainerDecl.getParentDecl();
            var enumAST = this.semanticInfoChain.getASTForDecl(enumContainerDecl);

            var isExported = enumContainerDecl.flags & 1 /* Exported */;

            var createdNewSymbol = false;

            enumContainerSymbol = this.getExistingSymbol(enumContainerDecl, TypeScript.PullElementKind.Enum, parent);

            if (enumContainerSymbol) {
                if (enumContainerSymbol.kind !== enumDeclKind) {
                    // duplicate symbol error
                    this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(enumAST.identifier, enumContainerDecl.getDisplayName(), enumContainerSymbol.getDeclarations()[0].ast());
                    enumContainerSymbol = null;
                } else if (!this.checkThatExportsMatch(enumContainerDecl, enumContainerSymbol)) {
                    enumContainerSymbol = null;
                }
            }

            if (enumContainerSymbol) {
                enumInstanceSymbol = enumContainerSymbol.getInstanceSymbol();
            } else {
                enumContainerSymbol = new TypeScript.PullContainerSymbol(enumName, enumDeclKind);
                createdNewSymbol = true;

                if (!parent) {
                    this.semanticInfoChain.cacheGlobalSymbol(enumContainerSymbol, TypeScript.PullElementKind.Enum);
                }
            }

            // We add the declaration early so that during any recursive binding of other module decls with the same name, this declaration is present.
            enumContainerSymbol.addDeclaration(enumContainerDecl);
            enumContainerDecl.setSymbol(enumContainerSymbol);

            this.semanticInfoChain.setSymbolForAST(enumAST.identifier, enumContainerSymbol);
            this.semanticInfoChain.setSymbolForAST(enumAST, enumContainerSymbol);

            if (!enumInstanceSymbol) {
                // search for a complementary instance symbol first
                var variableSymbol = null;
                if (parentInstanceSymbol) {
                    if (isExported) {
                        // We search twice because export visibility does not need to agree
                        variableSymbol = parentInstanceSymbol.findMember(enumName, false);

                        if (!variableSymbol) {
                            variableSymbol = parentInstanceSymbol.findContainedNonMember(enumName);
                        }
                    } else {
                        variableSymbol = parentInstanceSymbol.findContainedNonMember(enumName);

                        if (!variableSymbol) {
                            variableSymbol = parentInstanceSymbol.findMember(enumName, false);
                        }
                    }

                    if (variableSymbol) {
                        var declarations = variableSymbol.getDeclarations();

                        if (declarations.length) {
                            var variableSymbolParentDecl = declarations[0].getParentDecl();

                            if (parentDecl !== variableSymbolParentDecl) {
                                variableSymbol = null;
                            }
                        }
                    }
                } else if (!(enumContainerDecl.flags & 1 /* Exported */)) {
                    // Search locally to this file for a previous declaration that's suitable for augmentation
                    var siblingDecls = parentDecl.getChildDecls();
                    var augmentedDecl = null;

                    for (var i = 0; i < siblingDecls.length; i++) {
                        if (siblingDecls[i] === enumContainerDecl) {
                            break;
                        }

                        if ((siblingDecls[i].name === enumName) && (siblingDecls[i].kind & TypeScript.PullElementKind.SomeValue)) {
                            augmentedDecl = siblingDecls[i];
                            break;
                        }
                    }

                    if (augmentedDecl) {
                        variableSymbol = augmentedDecl.getSymbol();

                        if (variableSymbol) {
                            if (variableSymbol.isContainer()) {
                                variableSymbol = variableSymbol.getInstanceSymbol();
                            } else if (variableSymbol && variableSymbol.isType()) {
                                variableSymbol = variableSymbol.getConstructorMethod();
                            }
                        }
                    }
                }

                // The instance symbol is further set up in bindVariableDeclaration
                if (variableSymbol) {
                    enumInstanceSymbol = variableSymbol;
                    moduleInstanceTypeSymbol = variableSymbol.type;
                } else {
                    enumInstanceSymbol = new TypeScript.PullSymbol(enumName, TypeScript.PullElementKind.Variable);
                }

                enumContainerSymbol.setInstanceSymbol(enumInstanceSymbol);

                if (!moduleInstanceTypeSymbol) {
                    moduleInstanceTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.ObjectType);
                    enumInstanceSymbol.type = moduleInstanceTypeSymbol;
                }

                moduleInstanceTypeSymbol.addDeclaration(enumContainerDecl);

                if (!moduleInstanceTypeSymbol.getAssociatedContainerType()) {
                    moduleInstanceTypeSymbol.setAssociatedContainerType(enumContainerSymbol);
                }
            }

            if (createdNewSymbol && parent) {
                if (enumContainerDecl.flags & 1 /* Exported */) {
                    parent.addEnclosedMemberType(enumContainerSymbol);
                } else {
                    parent.addEnclosedNonMemberType(enumContainerSymbol);
                }
            }

            if (createdNewSymbol) {
                // if the enum container was created, add in the index signature
                this.bindEnumIndexerDeclsToPullSymbols(enumContainerSymbol);
            }
            var valueDecl = enumContainerDecl.getValueDecl();

            if (valueDecl) {
                valueDecl.ensureSymbolIsBound();
            }
        };

        PullSymbolBinder.prototype.bindEnumIndexerDeclsToPullSymbols = function (enumContainerSymbol) {
            // Add synthetic index signature to the enum instance
            var enumContainerInstanceTypeSymbol = enumContainerSymbol.getInstanceSymbol().type;

            var syntheticIndexerParameterSymbol = new TypeScript.PullSymbol("x", TypeScript.PullElementKind.Parameter);
            syntheticIndexerParameterSymbol.type = this.semanticInfoChain.numberTypeSymbol;
            syntheticIndexerParameterSymbol.setResolved();
            syntheticIndexerParameterSymbol.setIsSynthesized();

            var syntheticIndexerSignatureSymbol = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.IndexSignature);
            syntheticIndexerSignatureSymbol.addParameter(syntheticIndexerParameterSymbol);
            syntheticIndexerSignatureSymbol.returnType = this.semanticInfoChain.stringTypeSymbol;
            syntheticIndexerSignatureSymbol.setResolved();
            syntheticIndexerSignatureSymbol.setIsSynthesized();

            enumContainerInstanceTypeSymbol.addIndexSignature(syntheticIndexerSignatureSymbol);
        };

        PullSymbolBinder.prototype.findExistingVariableSymbolForModuleValueDecl = function (decl) {
            var isExported = TypeScript.hasFlag(decl.flags, 1 /* Exported */);
            var modName = decl.name;
            var parentInstanceSymbol = this.getParent(decl, true);
            var parentDecl = decl.getParentDecl();

            var variableSymbol = null;

            // search for a complementary instance symbol first
            if (parentInstanceSymbol) {
                if (isExported) {
                    // We search twice because export visibility does not need to agree
                    variableSymbol = parentInstanceSymbol.findMember(modName, false);

                    if (!variableSymbol) {
                        variableSymbol = parentInstanceSymbol.findContainedNonMember(modName);
                    }
                } else {
                    variableSymbol = parentInstanceSymbol.findContainedNonMember(modName);

                    if (!variableSymbol) {
                        variableSymbol = parentInstanceSymbol.findMember(modName, false);
                    }
                }

                // use currentModuleValueDecl to emphasise that we are merging value side of the module
                if (variableSymbol) {
                    var declarations = variableSymbol.getDeclarations();

                    if (declarations.length) {
                        var variableSymbolParentDecl = declarations[0].getParentDecl();
                        var isExportedOrHasTheSameParent = isExported || (parentDecl === variableSymbolParentDecl);

                        // previously defined variable symbol can be reused if
                        // - current decl is either exported or has the same parent with the previosly defined symbol
                        // AND
                        // exports match for both current and previous decl
                        var canReuseVariableSymbol = isExportedOrHasTheSameParent && this.checkThatExportsMatch(decl, variableSymbol, false);

                        if (!canReuseVariableSymbol) {
                            variableSymbol = null;
                        }
                    }
                }
            } else if (!isExported) {
                // Search locally to this file for a declaration that's suitable for augmentation.
                // Note: we have to check all declarations because it may be hte case (due to
                // recursive binding), that a later module gets bound before us.
                var siblingDecls = parentDecl.getChildDecls();

                for (var i = 0; i < siblingDecls.length; i++) {
                    var sibling = siblingDecls[i];

                    var siblingIsSomeValue = TypeScript.hasFlag(sibling.kind, TypeScript.PullElementKind.SomeValue);
                    var siblingIsFunctionOrHasImplictVarFlag = TypeScript.hasFlag(sibling.kind, TypeScript.PullElementKind.SomeFunction) || TypeScript.hasFlag(sibling.flags, TypeScript.PullElementFlags.ImplicitVariable);

                    // We need to determine of this sibling is something this module definition can augment
                    // Augmentable items are: Function declarations, Classes (whos value decl is its constructor method), Enums
                    var isSiblingAnAugmentableVariable = sibling !== decl && sibling !== decl.getValueDecl() && sibling.name === modName && siblingIsSomeValue && siblingIsFunctionOrHasImplictVarFlag;

                    if (isSiblingAnAugmentableVariable) {
                        // IMPORTANT: We don't want to just call sibling.getSymbol() here.
                        // That would force the sibling to get bound.  Something we don't want
                        // to do while binding ourselves (to avoid recursion issues).
                        if (sibling.hasSymbol()) {
                            variableSymbol = sibling.getSymbol();
                            if (variableSymbol.isContainer()) {
                                variableSymbol = variableSymbol.getInstanceSymbol();
                            } else if (variableSymbol && variableSymbol.isType()) {
                                variableSymbol = variableSymbol.getConstructorMethod();
                            }

                            break;
                        }
                    }
                }
            }
            return variableSymbol;
        };

        PullSymbolBinder.prototype.bindModuleDeclarationToPullSymbol = function (moduleContainerDecl) {
            // 1. Test for existing symbol - if it exists, use its symbol
            // 2. If no other symbols exists, create a new symbol and use that one
            var modName = moduleContainerDecl.name;

            var moduleContainerTypeSymbol = null;
            var moduleKind = moduleContainerDecl.kind;

            var parent = this.getParent(moduleContainerDecl);
            var parentInstanceSymbol = this.getParent(moduleContainerDecl, true);
            var parentDecl = moduleContainerDecl.getParentDecl();
            var moduleNameAST = this.semanticInfoChain.getASTForDecl(moduleContainerDecl);
            var moduleDeclAST = TypeScript.ASTHelpers.getEnclosingModuleDeclaration(moduleNameAST);
            if (!moduleDeclAST) {
                TypeScript.Debug.assert(moduleKind === TypeScript.PullElementKind.DynamicModule);
                TypeScript.Debug.assert(moduleNameAST.kind() === 120 /* SourceUnit */);

                // This is the module decl for the top level synthesized external module.
                moduleDeclAST = moduleNameAST;
            }

            var isExported = TypeScript.hasFlag(moduleContainerDecl.flags, 1 /* Exported */);
            var searchKind = TypeScript.PullElementKind.SomeContainer;
            var isInitializedModule = (moduleContainerDecl.flags & TypeScript.PullElementFlags.SomeInitializedModule) !== 0;

            if (parent && moduleKind === TypeScript.PullElementKind.DynamicModule) {
                // Dynamic modules cannot be parented
                this.semanticInfoChain.addDiagnosticFromAST(moduleNameAST, TypeScript.DiagnosticCode.Ambient_external_module_declaration_must_be_defined_in_global_context, null);
            }

            var createdNewSymbol = false;

            moduleContainerTypeSymbol = this.getExistingSymbol(moduleContainerDecl, searchKind, parent);

            if (moduleContainerTypeSymbol) {
                if (moduleContainerTypeSymbol.kind !== moduleKind) {
                    // duplicate symbol error
                    if (isInitializedModule) {
                        this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(moduleNameAST, moduleContainerDecl.getDisplayName(), moduleContainerTypeSymbol.getDeclarations()[0].ast());
                    }

                    moduleContainerTypeSymbol = null;
                } else if (moduleKind === TypeScript.PullElementKind.DynamicModule) {
                    // Dynamic modules cannot be reopened.
                    this.semanticInfoChain.addDiagnosticFromAST(moduleNameAST, TypeScript.DiagnosticCode.Ambient_external_module_declaration_cannot_be_reopened);
                } else if (!this.checkThatExportsMatch(moduleContainerDecl, moduleContainerTypeSymbol)) {
                    moduleContainerTypeSymbol = null;
                }
            }

            if (!moduleContainerTypeSymbol) {
                moduleContainerTypeSymbol = new TypeScript.PullContainerSymbol(modName, moduleKind);
                createdNewSymbol = true;

                if (!parent) {
                    this.semanticInfoChain.cacheGlobalSymbol(moduleContainerTypeSymbol, searchKind);
                }
            }

            // We add the declaration early so that during any recursive binding of other module decls with the same name, this declaration is present.
            moduleContainerTypeSymbol.addDeclaration(moduleContainerDecl);
            moduleContainerDecl.setSymbol(moduleContainerTypeSymbol);

            this.semanticInfoChain.setSymbolForAST(moduleNameAST, moduleContainerTypeSymbol);
            this.semanticInfoChain.setSymbolForAST(moduleDeclAST, moduleContainerTypeSymbol);

            var currentModuleValueDecl = moduleContainerDecl.getValueDecl();

            // If we have an enum with more than one declaration, then this enum's first element
            // must have an initializer.
            var moduleDeclarations = moduleContainerTypeSymbol.getDeclarations();

            if (createdNewSymbol) {
                if (parent) {
                    if (moduleContainerDecl.flags & 1 /* Exported */) {
                        parent.addEnclosedMemberContainer(moduleContainerTypeSymbol);
                    } else {
                        parent.addEnclosedNonMemberContainer(moduleContainerTypeSymbol);
                    }
                }
            }

            // do not create symbol for the module instance upfront - in some cases it can be harmful.
            // var x: B.C; // 1
            // declare function B(): B.C;
            // declare module B {
            //   export class C {
            //   }
            //}
            // When binding B.C in line 1 we first will bind module B as container (*) ->then we'll bind value side of the module.
            // During binding of the value side will discover that there are 2 value declarations with name B in scope, one - function, another - value decl for the module.
            // Function will be bound first so when we start binding module value decl - we'll find existing symbol that correspond for function and reuse it.
            // If we create instance symbol at point (*) - it will never be used.
            // To avoid this problem we'll bind value decl first and then check if it has symbol - if yes - this symbol will be used as module instance symbol
            if (currentModuleValueDecl) {
                currentModuleValueDecl.ensureSymbolIsBound();

                var instanceSymbol = null;
                var instanceTypeSymbol = null;
                if (currentModuleValueDecl.hasSymbol()) {
                    instanceSymbol = currentModuleValueDecl.getSymbol();
                } else {
                    // We associate the value decl to the module instance symbol. This should have
                    // already been achieved by ensureSymbolIsBound, but if bindModuleDeclarationToPullSymbol
                    // was called recursively while in the middle of binding the value decl, the cycle
                    // will be short-circuited. With a more organized binding pattern, this situation
                    // shouldn't be possible.
                    instanceSymbol = new TypeScript.PullSymbol(modName, TypeScript.PullElementKind.Variable);
                    currentModuleValueDecl.setSymbol(instanceSymbol);
                    if (!instanceSymbol.hasDeclaration(currentModuleValueDecl)) {
                        instanceSymbol.addDeclaration(currentModuleValueDecl);
                    }
                }

                if (!instanceSymbol.type) {
                    instanceSymbol.type = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.ObjectType);
                }

                moduleContainerTypeSymbol.setInstanceSymbol(instanceSymbol);

                if (!instanceSymbol.type.getAssociatedContainerType()) {
                    instanceSymbol.type.setAssociatedContainerType(moduleContainerTypeSymbol);
                }
            }
        };

        // aliases
        PullSymbolBinder.prototype.bindImportDeclaration = function (importDeclaration) {
            var declFlags = importDeclaration.flags;
            var declKind = importDeclaration.kind;
            var importDeclAST = this.semanticInfoChain.getASTForDecl(importDeclaration);

            var isExported = false;
            var importSymbol = null;
            var declName = importDeclaration.name;
            var parentHadSymbol = false;
            var parent = this.getParent(importDeclaration);

            importSymbol = this.getExistingSymbol(importDeclaration, TypeScript.PullElementKind.SomeContainer, parent);

            if (importSymbol) {
                parentHadSymbol = true;
            }

            if (importSymbol) {
                this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(importDeclAST, importDeclaration.getDisplayName(), importSymbol.getDeclarations()[0].ast());
                importSymbol = null;
            }

            if (!importSymbol) {
                importSymbol = new TypeScript.PullTypeAliasSymbol(declName);

                if (!parent) {
                    this.semanticInfoChain.cacheGlobalSymbol(importSymbol, TypeScript.PullElementKind.SomeContainer);
                }
            }

            importSymbol.addDeclaration(importDeclaration);
            importDeclaration.setSymbol(importSymbol);

            this.semanticInfoChain.setSymbolForAST(importDeclAST, importSymbol);

            if (parent && !parentHadSymbol) {
                if (declFlags & 1 /* Exported */) {
                    parent.addEnclosedMemberContainer(importSymbol);
                } else {
                    parent.addEnclosedNonMemberContainer(importSymbol);
                }
            }
        };

        // Preserves required binding order for a declaration with given name to prevent cases like:
        // module A { export module B { var x = 1} }
        // module A { export class B { c } }
        // Here if class declaration is bound before module declaration (i.e. because of IDE activities)
        // we won't report expected 'duplicate identifier' error for the class.
        PullSymbolBinder.prototype.ensurePriorDeclarationsAreBound = function (container, currentDecl) {
            if (!container) {
                return;
            }

            var parentDecls = container.getDeclarations();
            for (var i = 0; i < parentDecls.length; ++i) {
                var parentDecl = parentDecls[i];
                var childDecls = parentDecl.getChildDecls();
                for (var j = 0; j < childDecls.length; ++j) {
                    var childDecl = childDecls[j];
                    if (childDecl === currentDecl) {
                        return;
                    }

                    if (childDecl.name === currentDecl.name) {
                        childDecl.ensureSymbolIsBound();
                    }
                }
            }
        };

        // classes
        PullSymbolBinder.prototype.bindClassDeclarationToPullSymbol = function (classDecl) {
            var className = classDecl.name;
            var classSymbol = null;

            var constructorSymbol = null;
            var constructorTypeSymbol = null;

            var classAST = this.semanticInfoChain.getASTForDecl(classDecl);

            var parent = this.getParent(classDecl);

            // TODO: we should in general get all the decls from the parent and bind all of them together,
            // but thats a major change and we should fix it without losing perf, so adding just a todo here.
            this.ensurePriorDeclarationsAreBound(parent, classDecl);

            var parentDecl = classDecl.getParentDecl();
            var isExported = classDecl.flags & 1 /* Exported */;
            var isGeneric = false;

            classSymbol = this.getExistingSymbol(classDecl, TypeScript.PullElementKind.SomeType, parent);

            // Only error if it is an interface (for classes and enums we will error when we bind the implicit variable)
            if (classSymbol && classSymbol.kind === TypeScript.PullElementKind.Interface) {
                this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(classAST.identifier, classDecl.getDisplayName(), classSymbol.getDeclarations()[0].ast());
                classSymbol = null;
            }

            classSymbol = new TypeScript.PullTypeSymbol(className, TypeScript.PullElementKind.Class);

            if (!parent) {
                this.semanticInfoChain.cacheGlobalSymbol(classSymbol, TypeScript.PullElementKind.Class);
            }

            classSymbol.addDeclaration(classDecl);

            classDecl.setSymbol(classSymbol);

            this.semanticInfoChain.setSymbolForAST(classAST.identifier, classSymbol);
            this.semanticInfoChain.setSymbolForAST(classAST, classSymbol);

            if (parent) {
                if (classDecl.flags & 1 /* Exported */) {
                    parent.addEnclosedMemberType(classSymbol);
                } else {
                    parent.addEnclosedNonMemberType(classSymbol);
                }
            }

            var typeParameterDecls = classDecl.getTypeParameters();

            for (var i = 0; i < typeParameterDecls.length; i++) {
                var typeParameterSymbol = classSymbol.findTypeParameter(typeParameterDecls[i].name);

                if (typeParameterSymbol) {
                    var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameterSymbol.getDeclarations()[0]);
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameterSymbol.getName()]);
                }

                typeParameterSymbol = new TypeScript.PullTypeParameterSymbol(typeParameterDecls[i].name);

                classSymbol.addTypeParameter(typeParameterSymbol);
                typeParameterSymbol.addDeclaration(typeParameterDecls[i]);
                typeParameterDecls[i].setSymbol(typeParameterSymbol);
            }

            constructorSymbol = classSymbol.getConstructorMethod();
            constructorTypeSymbol = constructorSymbol ? constructorSymbol.type : null;

            if (!constructorSymbol) {
                // First, try to find a sibling value decl that is already bound. If there is one, reuse it.
                var siblingValueDecls = null;
                if (parentDecl) {
                    siblingValueDecls = parentDecl.searchChildDecls(className, TypeScript.PullElementKind.SomeValue);

                    // The first decl should have the right symbol
                    if (siblingValueDecls && siblingValueDecls[0] && siblingValueDecls[0].hasSymbol()) {
                        constructorSymbol = siblingValueDecls[0].getSymbol();
                    }
                }

                if (constructorSymbol) {
                    constructorTypeSymbol = constructorSymbol.type;
                } else {
                    constructorSymbol = new TypeScript.PullSymbol(className, TypeScript.PullElementKind.ConstructorMethod);
                    constructorTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.ConstructorType);
                    constructorSymbol.setIsSynthesized();
                    constructorSymbol.type = constructorTypeSymbol;
                }

                classSymbol.setConstructorMethod(constructorSymbol);
                classSymbol.setHasDefaultConstructor();
            }

            if (constructorSymbol.getIsSynthesized()) {
                constructorSymbol.addDeclaration(classDecl.getValueDecl());
                constructorTypeSymbol.addDeclaration(classDecl);
            } else {
                classSymbol.setHasDefaultConstructor(false);
            }

            constructorTypeSymbol.setAssociatedContainerType(classSymbol);

            var valueDecl = classDecl.getValueDecl();

            if (valueDecl) {
                valueDecl.ensureSymbolIsBound();
            }

            // Create the constructorTypeSymbol
            this.bindStaticPrototypePropertyOfClass(classAST, classSymbol, constructorTypeSymbol);
        };

        // interfaces
        PullSymbolBinder.prototype.bindInterfaceDeclarationToPullSymbol = function (interfaceDecl) {
            // 1. Test for existing decl - if it exists, use its symbol
            // 2. If no other decl exists, create a new symbol and use that one
            var interfaceName = interfaceDecl.name;
            var interfaceSymbol = null;

            var interfaceAST = this.semanticInfoChain.getASTForDecl(interfaceDecl);
            var createdNewSymbol = false;
            var parent = this.getParent(interfaceDecl);

            // We're not yet ready to support interfaces augmenting classes (or vice versa)
            var acceptableSharedKind = TypeScript.PullElementKind.Interface;

            interfaceSymbol = this.getExistingSymbol(interfaceDecl, TypeScript.PullElementKind.SomeType, parent);

            if (interfaceSymbol) {
                if (!(interfaceSymbol.kind & acceptableSharedKind)) {
                    this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(interfaceAST.identifier, interfaceDecl.getDisplayName(), interfaceSymbol.getDeclarations()[0].ast());
                    interfaceSymbol = null;
                } else if (!this.checkThatExportsMatch(interfaceDecl, interfaceSymbol)) {
                    interfaceSymbol = null;
                }
            }

            if (!interfaceSymbol) {
                interfaceSymbol = new TypeScript.PullTypeSymbol(interfaceName, TypeScript.PullElementKind.Interface);
                createdNewSymbol = true;

                if (!parent) {
                    this.semanticInfoChain.cacheGlobalSymbol(interfaceSymbol, acceptableSharedKind);
                }
            }

            interfaceSymbol.addDeclaration(interfaceDecl);
            interfaceDecl.setSymbol(interfaceSymbol);

            if (createdNewSymbol) {
                if (parent) {
                    if (interfaceDecl.flags & 1 /* Exported */) {
                        parent.addEnclosedMemberType(interfaceSymbol);
                    } else {
                        parent.addEnclosedNonMemberType(interfaceSymbol);
                    }
                }
            }

            var typeParameters = interfaceDecl.getTypeParameters();
            var typeParameter;
            var typeParameterDecls = null;

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameter = interfaceSymbol.findTypeParameter(typeParameters[i].name);

                if (!typeParameter) {
                    typeParameter = new TypeScript.PullTypeParameterSymbol(typeParameters[i].name);

                    interfaceSymbol.addTypeParameter(typeParameter);
                } else {
                    typeParameterDecls = typeParameter.getDeclarations();

                    for (var j = 0; j < typeParameterDecls.length; j++) {
                        var typeParameterDeclParent = typeParameterDecls[j].getParentDecl();

                        if (typeParameterDeclParent && typeParameterDeclParent === interfaceDecl) {
                            var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameterDecls[0]);
                            this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameter.getName()]);

                            break;
                        }
                    }
                }

                typeParameter.addDeclaration(typeParameters[i]);
                typeParameters[i].setSymbol(typeParameter);
            }
        };

        PullSymbolBinder.prototype.bindObjectTypeDeclarationToPullSymbol = function (objectDecl) {
            var objectSymbolAST = this.semanticInfoChain.getASTForDecl(objectDecl);

            var objectSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.ObjectType);

            objectSymbol.addDeclaration(objectDecl);
            objectDecl.setSymbol(objectSymbol);

            this.semanticInfoChain.setSymbolForAST(objectSymbolAST, objectSymbol);

            var childDecls = objectDecl.getChildDecls();

            for (var i = 0; i < childDecls.length; i++) {
                this.bindDeclToPullSymbol(childDecls[i]);
            }
        };

        PullSymbolBinder.prototype.bindConstructorTypeDeclarationToPullSymbol = function (constructorTypeDeclaration) {
            var declKind = constructorTypeDeclaration.kind;
            var declFlags = constructorTypeDeclaration.flags;
            var constructorTypeAST = this.semanticInfoChain.getASTForDecl(constructorTypeDeclaration);

            var constructorTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.ConstructorType);

            constructorTypeDeclaration.setSymbol(constructorTypeSymbol);
            constructorTypeSymbol.addDeclaration(constructorTypeDeclaration);
            this.semanticInfoChain.setSymbolForAST(constructorTypeAST, constructorTypeSymbol);

            var signature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.ConstructSignature);

            var funcDecl = this.semanticInfoChain.getASTForDecl(constructorTypeDeclaration);
            if (TypeScript.lastParameterIsRest(funcDecl.parameterList)) {
                signature.hasVarArgs = true;
            }

            signature.addDeclaration(constructorTypeDeclaration);
            constructorTypeDeclaration.setSignatureSymbol(signature);

            this.bindParameterSymbols(funcDecl, TypeScript.ASTHelpers.parametersFromParameterList(funcDecl.parameterList), constructorTypeSymbol, signature);

            var typeParameters = constructorTypeDeclaration.getTypeParameters();
            var typeParameter;

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameter = constructorTypeSymbol.findTypeParameter(typeParameters[i].name);

                if (!typeParameter) {
                    typeParameter = new TypeScript.PullTypeParameterSymbol(typeParameters[i].name);

                    signature.addTypeParameter(typeParameter);
                } else {
                    var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameter.getDeclarations()[0]);
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameter.name]);
                }

                typeParameter.addDeclaration(typeParameters[i]);
                typeParameters[i].setSymbol(typeParameter);
            }

            // add the implicit construct member for this function type
            constructorTypeSymbol.appendConstructSignature(signature);
        };

        // variables
        PullSymbolBinder.prototype.bindVariableDeclarationToPullSymbol = function (variableDeclaration) {
            var declFlags = variableDeclaration.flags;
            var declKind = variableDeclaration.kind;
            var varDeclAST = this.semanticInfoChain.getASTForDecl(variableDeclaration);
            var nameAST = varDeclAST.kind() === 131 /* ClassDeclaration */ ? varDeclAST.identifier : varDeclAST.kind() === 225 /* VariableDeclarator */ ? varDeclAST.propertyName : varDeclAST.kind() === 132 /* EnumDeclaration */ ? varDeclAST.identifier : varDeclAST;

            var isExported = (declFlags & 1 /* Exported */) !== 0;

            var variableSymbol = null;

            var declName = variableDeclaration.name;

            var parentHadSymbol = false;

            var parent = this.getParent(variableDeclaration, true);

            var parentDecl = variableDeclaration.getParentDecl();

            var isImplicit = (declFlags & TypeScript.PullElementFlags.ImplicitVariable) !== 0;
            var isModuleValue = (declFlags & (TypeScript.PullElementFlags.InitializedModule)) !== 0;
            var isEnumValue = (declFlags & TypeScript.PullElementFlags.Enum) !== 0;
            var isClassConstructorVariable = (declFlags & TypeScript.PullElementFlags.ClassConstructorVariable) !== 0;
            variableSymbol = this.getExistingSymbol(variableDeclaration, TypeScript.PullElementKind.SomeValue, parent);

            if (!variableSymbol && isModuleValue) {
                variableSymbol = this.findExistingVariableSymbolForModuleValueDecl(variableDeclaration.getContainerDecl());
            }

            if (variableSymbol && !variableSymbol.isType()) {
                parentHadSymbol = true;
            }

            var decl;
            var decls;
            var ast;
            var members;

            if (variableSymbol) {
                var prevKind = variableSymbol.kind;
                var prevIsEnum = variableSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.Enum);
                var prevIsClassConstructorVariable = variableSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.ClassConstructorVariable);
                var prevIsModuleValue = variableSymbol.allDeclsHaveFlag(TypeScript.PullElementFlags.InitializedModule);
                var prevIsImplicit = variableSymbol.anyDeclHasFlag(TypeScript.PullElementFlags.ImplicitVariable);
                var prevIsFunction = TypeScript.ArrayUtilities.any(variableSymbol.getDeclarations(), function (decl) {
                    return decl.kind === TypeScript.PullElementKind.Function;
                });
                var prevIsAmbient = variableSymbol.allDeclsHaveFlag(TypeScript.PullElementFlags.Ambient);
                var isAmbientOrPrevIsAmbient = prevIsAmbient || (variableDeclaration.flags & TypeScript.PullElementFlags.Ambient) !== 0;
                var prevDecl = variableSymbol.getDeclarations()[0];
                var prevParentDecl = prevDecl.getParentDecl();
                var bothAreGlobal = parentDecl && (parentDecl.kind === TypeScript.PullElementKind.Script) && (prevParentDecl.kind === TypeScript.PullElementKind.Script);
                var shareParent = bothAreGlobal || prevDecl.getParentDecl() === variableDeclaration.getParentDecl();
                var prevIsParam = shareParent && prevKind === TypeScript.PullElementKind.Parameter && declKind == TypeScript.PullElementKind.Variable;

                var acceptableRedeclaration = prevIsParam || (isImplicit && ((!isEnumValue && !isClassConstructorVariable && prevIsFunction) || ((isModuleValue || isEnumValue) && (prevIsModuleValue || prevIsEnum)) || (isClassConstructorVariable && prevIsModuleValue && isAmbientOrPrevIsAmbient) || (isModuleValue && prevIsClassConstructorVariable)));

                // if the previous declaration is a non-ambient class, it must be located in the same file as this declaration
                if (acceptableRedeclaration && (prevIsClassConstructorVariable || prevIsFunction) && !isAmbientOrPrevIsAmbient) {
                    if (prevDecl.fileName() !== variableDeclaration.fileName()) {
                        this.semanticInfoChain.addDiagnostic(TypeScript.PullHelpers.diagnosticFromDecl(variableDeclaration, TypeScript.DiagnosticCode.Module_0_cannot_merge_with_previous_declaration_of_1_in_a_different_file_2, [declName, declName, prevDecl.fileName()]));
                        variableSymbol.type = this.semanticInfoChain.getResolver().getNewErrorTypeSymbol(declName);
                    }
                }

                if (!acceptableRedeclaration || prevIsParam) {
                    // If neither of them are implicit (both explicitly declared as vars), we won't error now. We'll check that the types match during type check.
                    // However, we will error when a variable clobbers a function, or when the two explicit var declarations are not in the same parent declaration
                    if (!prevIsParam && (isImplicit || prevIsImplicit || TypeScript.hasFlag(prevKind, TypeScript.PullElementKind.SomeFunction)) || !shareParent) {
                        this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(nameAST, variableDeclaration.getDisplayName(), variableSymbol.getDeclarations()[0].ast());
                        variableSymbol.type = this.semanticInfoChain.getResolver().getNewErrorTypeSymbol(declName);
                    } else {
                        this.checkThatExportsMatch(variableDeclaration, variableSymbol);
                        variableSymbol = null;
                        parentHadSymbol = false;
                    }
                }

                // If we haven't given an error so far and we merged two decls, check that the exports match
                // Only report the error if they are not both initialized modules (if they are, the bind module code would report the error)
                if (variableSymbol && !(variableSymbol.type && variableSymbol.type.isError()) && !this.checkThatExportsMatch(variableDeclaration, variableSymbol, !(isModuleValue && prevIsModuleValue))) {
                    variableSymbol.type = this.semanticInfoChain.getResolver().getNewErrorTypeSymbol(declName);
                }
            }

            if ((declFlags & TypeScript.PullElementFlags.ImplicitVariable) === 0) {
                if (!variableSymbol) {
                    variableSymbol = new TypeScript.PullSymbol(declName, declKind);
                    if (!parent && parentDecl.kind === TypeScript.PullElementKind.Script) {
                        this.semanticInfoChain.cacheGlobalSymbol(variableSymbol, declKind);
                    }
                }

                variableSymbol.addDeclaration(variableDeclaration);
                variableDeclaration.setSymbol(variableSymbol);

                this.semanticInfoChain.setSymbolForAST(nameAST, variableSymbol);
                this.semanticInfoChain.setSymbolForAST(varDeclAST, variableSymbol);
            } else if (!parentHadSymbol) {
                if (isClassConstructorVariable) {
                    // it's really an implicit class decl, so we need to set the type of the symbol to
                    // the constructor type
                    // Note that we would have already found the class symbol in the search above
                    var classTypeSymbol = variableSymbol;

                    // PULLTODO: In both this case and the case below, we should have already received the
                    // class or module symbol as the variableSymbol found above
                    if (parent) {
                        members = parent.getMembers();

                        for (var i = 0; i < members.length; i++) {
                            if ((members[i].name === declName) && (members[i].kind === TypeScript.PullElementKind.Class)) {
                                classTypeSymbol = members[i];
                                break;
                            }
                        }
                    }

                    if (!classTypeSymbol) {
                        var containerDecl = variableDeclaration.getContainerDecl();
                        classTypeSymbol = containerDecl.getSymbol();
                        if (!classTypeSymbol) {
                            classTypeSymbol = this.semanticInfoChain.findTopLevelSymbol(declName, TypeScript.PullElementKind.SomeType, variableDeclaration);
                        }
                    }

                    if (classTypeSymbol && (classTypeSymbol.kind !== TypeScript.PullElementKind.Class)) {
                        classTypeSymbol = null;
                    }

                    if (classTypeSymbol && classTypeSymbol.isClass()) {
                        //replaceProperty = variableSymbol && variableSymbol.getIsSynthesized();
                        //if (replaceProperty) {
                        //    previousProperty = variableSymbol;
                        //}
                        variableSymbol = classTypeSymbol.getConstructorMethod();
                        variableDeclaration.setSymbol(variableSymbol);

                        // set the AST to the constructor method's if possible
                        decls = classTypeSymbol.getDeclarations();

                        if (decls.length) {
                            decl = decls[decls.length - 1];
                            ast = this.semanticInfoChain.getASTForDecl(decl);
                        }
                    } else {
                        // PULLTODO: Clodules/Interfaces on classes
                        if (!variableSymbol) {
                            variableSymbol = new TypeScript.PullSymbol(declName, declKind);
                        }

                        variableSymbol.addDeclaration(variableDeclaration);
                        variableDeclaration.setSymbol(variableSymbol);

                        variableSymbol.type = this.semanticInfoChain.anyTypeSymbol;
                    }
                } else if (declFlags & TypeScript.PullElementFlags.SomeInitializedModule) {
                    var moduleContainerTypeSymbol = null;
                    var moduleParent = this.getParent(variableDeclaration);

                    if (moduleParent) {
                        members = moduleParent.getMembers();

                        for (var i = 0; i < members.length; i++) {
                            if ((members[i].name === declName) && (members[i].isContainer())) {
                                moduleContainerTypeSymbol = members[i];
                                break;
                            }
                        }
                    }

                    if (!moduleContainerTypeSymbol) {
                        var containerDecl = variableDeclaration.getContainerDecl();
                        moduleContainerTypeSymbol = containerDecl.getSymbol();
                        if (!moduleContainerTypeSymbol) {
                            moduleContainerTypeSymbol = this.semanticInfoChain.findTopLevelSymbol(declName, TypeScript.PullElementKind.SomeContainer, variableDeclaration);

                            if (!moduleContainerTypeSymbol) {
                                moduleContainerTypeSymbol = this.semanticInfoChain.findTopLevelSymbol(declName, TypeScript.PullElementKind.Enum, variableDeclaration);
                            }
                        }
                    }

                    if (moduleContainerTypeSymbol && (!moduleContainerTypeSymbol.isContainer())) {
                        moduleContainerTypeSymbol = null;
                    }

                    if (moduleContainerTypeSymbol) {
                        variableSymbol = moduleContainerTypeSymbol.getInstanceSymbol();
                        if (!variableSymbol) {
                            variableSymbol = new TypeScript.PullSymbol(declName, declKind);
                            variableSymbol.type = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.ObjectType);
                        }

                        // If this method calls bindModuleDeclarationToPullSymbol recursively,
                        // we may associate the variable decl with its symbol in that recursive
                        // call before we do it here. Therefore, make sure the symbol doesn't already
                        // have the decl before adding it. Just like in bindModuleDeclarationToPullSymbol,
                        // we shouldn't need this maneuver with a more iterative binding pattern.
                        if (!variableSymbol.hasDeclaration(variableDeclaration)) {
                            variableSymbol.addDeclaration(variableDeclaration);
                        }
                        variableDeclaration.setSymbol(variableSymbol);
                    } else {
                        TypeScript.Debug.assert(false, "Attempted to bind invalid implicit variable symbol");
                    }
                }
            } else {
                if (!variableSymbol.hasDeclaration(variableDeclaration)) {
                    variableSymbol.addDeclaration(variableDeclaration);
                }
                variableDeclaration.setSymbol(variableSymbol);
            }

            var containerDecl = variableDeclaration.getContainerDecl();
            if (variableSymbol && variableSymbol.type && containerDecl && !variableSymbol.type.hasDeclaration(containerDecl)) {
                variableSymbol.type.addDeclaration(containerDecl);
            }

            if (parent && !parentHadSymbol) {
                if (declFlags & 1 /* Exported */) {
                    parent.addMember(variableSymbol);
                } else {
                    parent.addEnclosedNonMember(variableSymbol);
                }
            }
        };

        PullSymbolBinder.prototype.bindCatchVariableToPullSymbol = function (variableDeclaration) {
            var declFlags = variableDeclaration.flags;
            var declKind = variableDeclaration.kind;
            var identifier = this.semanticInfoChain.getASTForDecl(variableDeclaration);

            var declName = variableDeclaration.name;

            var variableSymbol = new TypeScript.PullSymbol(declName, declKind);

            variableSymbol.addDeclaration(variableDeclaration);
            variableDeclaration.setSymbol(variableSymbol);

            // Catch variable are of type 'any'.  So we don't need to actually resolve anything later.
            variableSymbol.type = this.semanticInfoChain.anyTypeSymbol;

            this.semanticInfoChain.setSymbolForAST(identifier, variableSymbol);
        };

        // properties
        PullSymbolBinder.prototype.bindEnumMemberDeclarationToPullSymbol = function (propertyDeclaration) {
            var declFlags = propertyDeclaration.flags;
            var declKind = propertyDeclaration.kind;
            var propDeclAST = this.semanticInfoChain.getASTForDecl(propertyDeclaration);

            var declName = propertyDeclaration.name;

            var parentHadSymbol = false;

            var parent = this.getParent(propertyDeclaration, true);

            var propertySymbol = parent.findMember(declName, false);

            if (propertySymbol) {
                this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(propDeclAST.propertyName, propertyDeclaration.getDisplayName(), propertySymbol.getDeclarations()[0].ast());
            }

            if (propertySymbol) {
                parentHadSymbol = true;
            }

            if (!parentHadSymbol) {
                propertySymbol = new TypeScript.PullSymbol(declName, declKind);
            }

            propertySymbol.addDeclaration(propertyDeclaration);
            propertyDeclaration.setSymbol(propertySymbol);

            this.semanticInfoChain.setSymbolForAST(propDeclAST.propertyName, propertySymbol);
            this.semanticInfoChain.setSymbolForAST(propDeclAST, propertySymbol);

            if (parent && !parentHadSymbol) {
                parent.addMember(propertySymbol);
            }
        };

        PullSymbolBinder.prototype.bindPropertyDeclarationToPullSymbol = function (propertyDeclaration) {
            var declFlags = propertyDeclaration.flags;
            var declKind = propertyDeclaration.kind;

            var ast = this.semanticInfoChain.getASTForDecl(propertyDeclaration);
            var astName = ast.kind() === 136 /* MemberVariableDeclaration */ ? ast.variableDeclarator.propertyName : ast.kind() === 141 /* PropertySignature */ ? ast.propertyName : ast.kind() === 242 /* Parameter */ ? ast.identifier : ast.propertyName;

            var isStatic = false;
            var isOptional = false;

            var propertySymbol = null;

            if (TypeScript.hasFlag(declFlags, TypeScript.PullElementFlags.Static)) {
                isStatic = true;
            }

            if (TypeScript.hasFlag(declFlags, TypeScript.PullElementFlags.Optional)) {
                isOptional = true;
            }

            var declName = propertyDeclaration.name;

            var parentHadSymbol = false;

            var parent = this.getParent(propertyDeclaration, true);

            if (parent.isClass() && isStatic) {
                parent = parent.getConstructorMethod().type;
            }

            propertySymbol = parent.findMember(declName, false);

            if (propertySymbol) {
                this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(astName, propertyDeclaration.getDisplayName(), propertySymbol.getDeclarations()[0].ast());
            }

            if (propertySymbol) {
                parentHadSymbol = true;
            }

            var classTypeSymbol;

            if (!parentHadSymbol) {
                propertySymbol = new TypeScript.PullSymbol(declName, declKind);
            }

            propertySymbol.addDeclaration(propertyDeclaration);
            propertyDeclaration.setSymbol(propertySymbol);

            this.semanticInfoChain.setSymbolForAST(astName, propertySymbol);
            this.semanticInfoChain.setSymbolForAST(ast, propertySymbol);

            if (isOptional) {
                propertySymbol.isOptional = true;
            }

            if (parent && !parentHadSymbol) {
                parent.addMember(propertySymbol);
            }
        };

        // parameters
        PullSymbolBinder.prototype.bindParameterSymbols = function (functionDeclaration, parameterList, funcType, signatureSymbol) {
            // create a symbol for each ast
            // if it's a property, add the symbol to the enclosing type's member list
            var parameters = [];
            var params = TypeScript.createIntrinsicsObject();
            var funcDecl = this.semanticInfoChain.getDeclForAST(functionDeclaration);

            if (parameterList) {
                for (var i = 0, n = parameterList.length; i < n; i++) {
                    var argDecl = parameterList.astAt(i);
                    var id = parameterList.identifierAt(i);
                    var decl = this.semanticInfoChain.getDeclForAST(argDecl);
                    var isProperty = TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.PropertyParameter);
                    var parameterSymbol = new TypeScript.PullSymbol(id.valueText(), TypeScript.PullElementKind.Parameter);

                    if ((i === (n - 1)) && parameterList.lastParameterIsRest()) {
                        parameterSymbol.isVarArg = true;
                    }

                    if (params[id.valueText()]) {
                        this.semanticInfoChain.addDiagnosticFromAST(argDecl, TypeScript.DiagnosticCode.Duplicate_identifier_0, [id.text()]);
                    } else {
                        params[id.valueText()] = true;
                    }

                    if (decl) {
                        var isParameterOptional = false;

                        if (isProperty) {
                            decl.ensureSymbolIsBound();
                            var valDecl = decl.getValueDecl();

                            // if this is a parameter property, we still need to set the value decl
                            // for the function parameter
                            if (valDecl) {
                                isParameterOptional = TypeScript.hasFlag(valDecl.flags, TypeScript.PullElementFlags.Optional);

                                valDecl.setSymbol(parameterSymbol);
                                parameterSymbol.addDeclaration(valDecl);
                            }
                        } else {
                            isParameterOptional = TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.Optional);

                            parameterSymbol.addDeclaration(decl);
                            decl.setSymbol(parameterSymbol);
                        }

                        parameterSymbol.isOptional = isParameterOptional;
                    }

                    signatureSymbol.addParameter(parameterSymbol, parameterSymbol.isOptional);

                    if (signatureSymbol.isDefinition()) {
                        funcType.addEnclosedNonMember(parameterSymbol);
                    }
                }
            }
        };

        // function declarations
        PullSymbolBinder.prototype.bindFunctionDeclarationToPullSymbol = function (functionDeclaration) {
            var declKind = functionDeclaration.kind;
            var declFlags = functionDeclaration.flags;
            var funcDeclAST = this.semanticInfoChain.getASTForDecl(functionDeclaration);

            var isExported = (declFlags & 1 /* Exported */) !== 0;

            var funcName = functionDeclaration.name;

            // 1. Test for existing decl - if it exists, use its symbol
            // 2. If no other decl exists, create a new symbol and use that one
            var isSignature = (declFlags & TypeScript.PullElementFlags.Signature) !== 0;

            var parent = this.getParent(functionDeclaration, true);

            var parentDecl = functionDeclaration.getParentDecl();
            var parentHadSymbol = false;

            // PULLREVIEW: On a re-bind, there's no need to search far-and-wide: just look in the parent's member list
            var functionSymbol = null;
            var functionTypeSymbol = null;

            functionSymbol = this.getExistingSymbol(functionDeclaration, TypeScript.PullElementKind.SomeValue, parent);

            if (functionSymbol) {
                // SPEC: Nov 18
                // When merging a non-ambient function or class declaration and a non-ambient internal module declaration,
                // the function or class declaration must be located prior to the internal module declaration in the same source file.
                // => when any of components is ambient - order doesn't matter
                var acceptableRedeclaration;

                // Duplicate is acceptable if it is another signature (not a duplicate implementation), or an ambient fundule
                if (functionSymbol.kind === TypeScript.PullElementKind.Function) {
                    // normal fundule - we are allowed to add overloads
                    acceptableRedeclaration = isSignature || functionSymbol.allDeclsHaveFlag(TypeScript.PullElementFlags.Signature);
                } else {
                    // check if this is ambient fundule?
                    var isCurrentDeclAmbient = TypeScript.hasFlag(functionDeclaration.flags, TypeScript.PullElementFlags.Ambient);
                    acceptableRedeclaration = TypeScript.ArrayUtilities.all(functionSymbol.getDeclarations(), function (decl) {
                        // allowed elements for ambient fundules
                        // - signatures
                        // - initialized modules that can be ambient or not depending on whether current decl is ambient
                        var isInitializedModuleOrAmbientDecl = TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.InitializedModule) && (isCurrentDeclAmbient || TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.Ambient));
                        var isSignature = TypeScript.hasFlag(decl.flags, TypeScript.PullElementFlags.Signature);
                        return isInitializedModuleOrAmbientDecl || isSignature;
                    });
                }

                if (!acceptableRedeclaration) {
                    this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(funcDeclAST.identifier, functionDeclaration.getDisplayName(), functionSymbol.getDeclarations()[0].ast());
                    functionSymbol.type = this.semanticInfoChain.getResolver().getNewErrorTypeSymbol(funcName);
                }
            }

            if (functionSymbol) {
                functionTypeSymbol = functionSymbol.type;
                parentHadSymbol = true;
            }

            if (!functionSymbol) {
                // PULLTODO: Make sure that we properly flag signature decl types when collecting decls
                functionSymbol = new TypeScript.PullSymbol(funcName, TypeScript.PullElementKind.Function);
            }

            if (!functionTypeSymbol) {
                functionTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.FunctionType);
                functionSymbol.type = functionTypeSymbol;
                functionTypeSymbol.setFunctionSymbol(functionSymbol);
            }

            functionDeclaration.setSymbol(functionSymbol);
            functionSymbol.addDeclaration(functionDeclaration);
            functionTypeSymbol.addDeclaration(functionDeclaration);

            this.semanticInfoChain.setSymbolForAST(funcDeclAST.identifier, functionSymbol);
            this.semanticInfoChain.setSymbolForAST(funcDeclAST, functionSymbol);

            if (parent && !parentHadSymbol) {
                if (isExported) {
                    parent.addMember(functionSymbol);
                } else {
                    parent.addEnclosedNonMember(functionSymbol);
                }
            }

            var signature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.CallSignature, !isSignature);

            signature.addDeclaration(functionDeclaration);
            functionDeclaration.setSignatureSymbol(signature);

            if (TypeScript.lastParameterIsRest(funcDeclAST.callSignature.parameterList)) {
                signature.hasVarArgs = true;
            }

            var funcDecl = this.semanticInfoChain.getASTForDecl(functionDeclaration);
            this.bindParameterSymbols(funcDecl, TypeScript.ASTHelpers.parametersFromParameterList(funcDecl.callSignature.parameterList), functionTypeSymbol, signature);

            var typeParameters = functionDeclaration.getTypeParameters();
            var typeParameter;

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameter = signature.findTypeParameter(typeParameters[i].name);

                if (!typeParameter) {
                    typeParameter = new TypeScript.PullTypeParameterSymbol(typeParameters[i].name);

                    signature.addTypeParameter(typeParameter);
                } else {
                    var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameter.getDeclarations()[0]);
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameter.name]);
                }

                typeParameter.addDeclaration(typeParameters[i]);
                typeParameters[i].setSymbol(typeParameter);
            }

            // add the implicit call member for this function type
            functionTypeSymbol.appendCallSignature(signature);
        };

        PullSymbolBinder.prototype.bindFunctionExpressionToPullSymbol = function (functionExpressionDeclaration) {
            var declKind = functionExpressionDeclaration.kind;
            var declFlags = functionExpressionDeclaration.flags;
            var ast = this.semanticInfoChain.getASTForDecl(functionExpressionDeclaration);

            var parameters = ast.kind() === 219 /* SimpleArrowFunctionExpression */ ? TypeScript.ASTHelpers.parametersFromIdentifier(ast.identifier) : TypeScript.ASTHelpers.parametersFromParameterList(TypeScript.ASTHelpers.getParameterList(ast));
            var funcExpAST = ast;

            // 1. Test for existing decl - if it exists, use its symbol
            // 2. If no other decl exists, create a new symbol and use that one
            var functionName = declKind === TypeScript.PullElementKind.FunctionExpression ? functionExpressionDeclaration.getFunctionExpressionName() : functionExpressionDeclaration.name;
            var functionSymbol = new TypeScript.PullSymbol(functionName, declKind);
            var functionTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.FunctionType);
            functionTypeSymbol.setFunctionSymbol(functionSymbol);

            functionSymbol.type = functionTypeSymbol;

            functionExpressionDeclaration.setSymbol(functionSymbol);
            functionSymbol.addDeclaration(functionExpressionDeclaration);
            functionTypeSymbol.addDeclaration(functionExpressionDeclaration);

            var name = funcExpAST.kind() === 222 /* FunctionExpression */ ? funcExpAST.identifier : funcExpAST.kind() === 241 /* FunctionPropertyAssignment */ ? funcExpAST.propertyName : null;
            if (name) {
                this.semanticInfoChain.setSymbolForAST(name, functionSymbol);
            }

            this.semanticInfoChain.setSymbolForAST(funcExpAST, functionSymbol);

            var signature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.CallSignature, true);

            if (parameters.lastParameterIsRest()) {
                signature.hasVarArgs = true;
            }

            var typeParameters = functionExpressionDeclaration.getTypeParameters();
            var typeParameter;

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameter = signature.findTypeParameter(typeParameters[i].name);

                if (!typeParameter) {
                    typeParameter = new TypeScript.PullTypeParameterSymbol(typeParameters[i].name);

                    signature.addTypeParameter(typeParameter);
                } else {
                    var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameter.getDeclarations()[0]);
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameter.getName()]);
                }

                typeParameter.addDeclaration(typeParameters[i]);
                typeParameters[i].setSymbol(typeParameter);
            }

            signature.addDeclaration(functionExpressionDeclaration);
            functionExpressionDeclaration.setSignatureSymbol(signature);

            this.bindParameterSymbols(funcExpAST, parameters, functionTypeSymbol, signature);

            // add the implicit call member for this function type
            functionTypeSymbol.appendCallSignature(signature);
        };

        PullSymbolBinder.prototype.bindFunctionTypeDeclarationToPullSymbol = function (functionTypeDeclaration) {
            var declKind = functionTypeDeclaration.kind;
            var declFlags = functionTypeDeclaration.flags;
            var funcTypeAST = this.semanticInfoChain.getASTForDecl(functionTypeDeclaration);

            // 1. Test for existing decl - if it exists, use its symbol
            // 2. If no other decl exists, create a new symbol and use that one
            var functionTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.FunctionType);

            functionTypeDeclaration.setSymbol(functionTypeSymbol);
            functionTypeSymbol.addDeclaration(functionTypeDeclaration);
            this.semanticInfoChain.setSymbolForAST(funcTypeAST, functionTypeSymbol);

            var isSignature = (declFlags & TypeScript.PullElementFlags.Signature) !== 0;
            var signature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.CallSignature, !isSignature);

            if (TypeScript.lastParameterIsRest(funcTypeAST.parameterList)) {
                signature.hasVarArgs = true;
            }

            var typeParameters = functionTypeDeclaration.getTypeParameters();
            var typeParameter;

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameter = signature.findTypeParameter(typeParameters[i].name);

                if (!typeParameter) {
                    typeParameter = new TypeScript.PullTypeParameterSymbol(typeParameters[i].name);

                    signature.addTypeParameter(typeParameter);
                } else {
                    var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameter.getDeclarations()[0]);
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameter.name]);
                }

                typeParameter.addDeclaration(typeParameters[i]);
                typeParameters[i].setSymbol(typeParameter);
            }

            signature.addDeclaration(functionTypeDeclaration);
            functionTypeDeclaration.setSignatureSymbol(signature);

            this.bindParameterSymbols(funcTypeAST, TypeScript.ASTHelpers.parametersFromParameterList(funcTypeAST.parameterList), functionTypeSymbol, signature);

            // add the implicit call member for this function type
            functionTypeSymbol.appendCallSignature(signature);
        };

        // method declarations
        PullSymbolBinder.prototype.bindMethodDeclarationToPullSymbol = function (methodDeclaration) {
            var declKind = methodDeclaration.kind;
            var declFlags = methodDeclaration.flags;
            var methodAST = this.semanticInfoChain.getASTForDecl(methodDeclaration);

            var isPrivate = (declFlags & TypeScript.PullElementFlags.Private) !== 0;
            var isStatic = (declFlags & TypeScript.PullElementFlags.Static) !== 0;
            var isOptional = (declFlags & TypeScript.PullElementFlags.Optional) !== 0;

            var methodName = methodDeclaration.name;

            var isSignature = (declFlags & TypeScript.PullElementFlags.Signature) !== 0;

            var parent = this.getParent(methodDeclaration, true);
            var parentHadSymbol = false;

            var methodSymbol = null;
            var methodTypeSymbol = null;

            if (parent.isClass() && isStatic) {
                parent = parent.getConstructorMethod().type;
            }

            methodSymbol = parent.findMember(methodName, false);

            if (methodSymbol && (methodSymbol.kind !== TypeScript.PullElementKind.Method || (!isSignature && !methodSymbol.allDeclsHaveFlag(TypeScript.PullElementFlags.Signature)))) {
                this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(methodAST, methodDeclaration.getDisplayName(), methodSymbol.getDeclarations()[0].ast());
                methodSymbol = null;
            }

            if (methodSymbol) {
                methodTypeSymbol = methodSymbol.type;
                parentHadSymbol = true;
            }

            if (!methodSymbol) {
                // PULLTODO: Make sure that we properly flag signature decl types when collecting decls
                methodSymbol = new TypeScript.PullSymbol(methodName, TypeScript.PullElementKind.Method);
            }

            if (!methodTypeSymbol) {
                methodTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.FunctionType);
                methodSymbol.type = methodTypeSymbol;
                methodTypeSymbol.setFunctionSymbol(methodSymbol);
            }

            methodDeclaration.setSymbol(methodSymbol);
            methodSymbol.addDeclaration(methodDeclaration);
            methodTypeSymbol.addDeclaration(methodDeclaration);

            var nameAST = methodAST.kind() === 135 /* MemberFunctionDeclaration */ ? methodAST.propertyName : methodAST.propertyName;

            TypeScript.Debug.assert(nameAST);

            this.semanticInfoChain.setSymbolForAST(nameAST, methodSymbol);
            this.semanticInfoChain.setSymbolForAST(methodAST, methodSymbol);

            if (isOptional) {
                methodSymbol.isOptional = true;
            }

            if (!parentHadSymbol) {
                parent.addMember(methodSymbol);
            }

            var sigKind = TypeScript.PullElementKind.CallSignature;

            var signature = new TypeScript.PullSignatureSymbol(sigKind, !isSignature);

            var parameterList = TypeScript.ASTHelpers.getParameterList(methodAST);
            if (TypeScript.lastParameterIsRest(parameterList)) {
                signature.hasVarArgs = true;
            }

            var typeParameters = methodDeclaration.getTypeParameters();
            var typeParameter;
            var typeParameterName;
            var typeParameterAST;

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameterName = typeParameters[i].name;
                typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameters[i]);

                typeParameter = signature.findTypeParameter(typeParameterName);

                if (!typeParameter) {
                    typeParameter = new TypeScript.PullTypeParameterSymbol(typeParameterName);
                    signature.addTypeParameter(typeParameter);
                } else {
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameter.getName()]);
                }

                typeParameter.addDeclaration(typeParameters[i]);
                typeParameters[i].setSymbol(typeParameter);
            }

            signature.addDeclaration(methodDeclaration);
            methodDeclaration.setSignatureSymbol(signature);

            var funcDecl = this.semanticInfoChain.getASTForDecl(methodDeclaration);
            this.bindParameterSymbols(funcDecl, TypeScript.ASTHelpers.parametersFromParameterList(TypeScript.ASTHelpers.getParameterList(funcDecl)), methodTypeSymbol, signature);

            // add the implicit call member for this function type
            var signatureIndex = this.getIndexForInsertingSignatureAtEndOfEnclosingDeclInSignatureList(signature, methodTypeSymbol.getOwnCallSignatures());
            methodTypeSymbol.insertCallSignatureAtIndex(signature, signatureIndex);
        };

        PullSymbolBinder.prototype.bindStaticPrototypePropertyOfClass = function (classAST, classTypeSymbol, constructorTypeSymbol) {
            var prototypeStr = "prototype";

            var prototypeSymbol = constructorTypeSymbol.findMember(prototypeStr, false);
            if (prototypeSymbol && !prototypeSymbol.getIsSynthesized()) {
                // Report duplicate symbol error on existing prototype symbol since class has explicit prototype symbol
                // This kind of scenario can happen with augmented module and class with module member named prototype
                this.semanticInfoChain.addDiagnostic(TypeScript.PullHelpers.diagnosticFromDecl(prototypeSymbol.getDeclarations()[0], TypeScript.DiagnosticCode.Duplicate_identifier_0, [prototypeSymbol.getDisplayName()]));
            }

            // Add synthetic prototype decl and symbol
            if (!prototypeSymbol || !prototypeSymbol.getIsSynthesized()) {
                var prototypeDecl = new TypeScript.PullSynthesizedDecl(prototypeStr, prototypeStr, TypeScript.PullElementKind.Property, TypeScript.PullElementFlags.Public | TypeScript.PullElementFlags.Static, constructorTypeSymbol.getDeclarations()[0], this.semanticInfoChain);

                prototypeSymbol = new TypeScript.PullSymbol(prototypeStr, TypeScript.PullElementKind.Property);
                prototypeSymbol.setIsSynthesized();
                prototypeSymbol.addDeclaration(prototypeDecl);
                prototypeSymbol.type = classTypeSymbol;
                constructorTypeSymbol.addMember(prototypeSymbol);

                if (prototypeSymbol.type && prototypeSymbol.type.isGeneric()) {
                    var resolver = this.semanticInfoChain.getResolver();
                    prototypeSymbol.type = resolver.instantiateTypeToAny(prototypeSymbol.type, new TypeScript.PullTypeResolutionContext(resolver));
                }
                prototypeSymbol.setResolved();
            }
        };

        // class constructor declarations
        PullSymbolBinder.prototype.bindConstructorDeclarationToPullSymbol = function (constructorDeclaration) {
            var declKind = constructorDeclaration.kind;
            var declFlags = constructorDeclaration.flags;
            var constructorAST = this.semanticInfoChain.getASTForDecl(constructorDeclaration);

            var constructorName = constructorDeclaration.name;

            var isSignature = (declFlags & TypeScript.PullElementFlags.Signature) !== 0;

            var parent = this.getParent(constructorDeclaration, true);

            var parentHadSymbol = false;

            var constructorSymbol = parent.getConstructorMethod();
            var constructorTypeSymbol = null;

            if (constructorSymbol && (constructorSymbol.kind !== TypeScript.PullElementKind.ConstructorMethod || (!isSignature && constructorSymbol.type && constructorSymbol.type.hasOwnConstructSignatures()))) {
                var hasDefinitionSignature = false;
                var constructorSigs = constructorSymbol.type.getOwnDeclaredConstructSignatures();

                for (var i = 0; i < constructorSigs.length; i++) {
                    if (!constructorSigs[i].anyDeclHasFlag(TypeScript.PullElementFlags.Signature)) {
                        hasDefinitionSignature = true;
                        break;
                    }
                }

                if (hasDefinitionSignature) {
                    this.semanticInfoChain.addDiagnosticFromAST(constructorAST, TypeScript.DiagnosticCode.Multiple_constructor_implementations_are_not_allowed);

                    constructorSymbol = null;
                }
            }

            if (constructorSymbol) {
                constructorTypeSymbol = constructorSymbol.type;
            } else {
                constructorSymbol = new TypeScript.PullSymbol(constructorName, TypeScript.PullElementKind.ConstructorMethod);
                constructorTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.ConstructorType);
            }

            // Even if we're reusing the symbol, it would have been cleared by the call to invalidate above
            parent.setConstructorMethod(constructorSymbol);
            constructorSymbol.type = constructorTypeSymbol;

            constructorDeclaration.setSymbol(constructorSymbol);
            constructorSymbol.addDeclaration(constructorDeclaration);
            constructorTypeSymbol.addDeclaration(constructorDeclaration);
            constructorSymbol.setIsSynthesized(false);
            this.semanticInfoChain.setSymbolForAST(constructorAST, constructorSymbol);

            // add a call signature to the constructor method, and a construct signature to the parent class type
            var constructSignature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.ConstructSignature, !isSignature);
            constructSignature.returnType = parent;
            constructSignature.addTypeParametersFromReturnType();

            constructSignature.addDeclaration(constructorDeclaration);
            constructorDeclaration.setSignatureSymbol(constructSignature);

            this.bindParameterSymbols(constructorAST, TypeScript.ASTHelpers.parametersFromParameterList(constructorAST.callSignature.parameterList), constructorTypeSymbol, constructSignature);

            if (TypeScript.lastParameterIsRest(constructorAST.callSignature.parameterList)) {
                constructSignature.hasVarArgs = true;
            }

            constructorTypeSymbol.appendConstructSignature(constructSignature);
        };

        PullSymbolBinder.prototype.bindConstructSignatureDeclarationToPullSymbol = function (constructSignatureDeclaration) {
            var parent = this.getParent(constructSignatureDeclaration, true);
            var constructorAST = this.semanticInfoChain.getASTForDecl(constructSignatureDeclaration);

            var constructSignature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.ConstructSignature);

            if (TypeScript.lastParameterIsRest(constructorAST.callSignature.parameterList)) {
                constructSignature.hasVarArgs = true;
            }

            var typeParameters = constructSignatureDeclaration.getTypeParameters();
            var typeParameter;

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameter = constructSignature.findTypeParameter(typeParameters[i].name);

                if (!typeParameter) {
                    typeParameter = new TypeScript.PullTypeParameterSymbol(typeParameters[i].name);

                    constructSignature.addTypeParameter(typeParameter);
                } else {
                    var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameter.getDeclarations()[0]);
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameter.getName()]);
                }

                typeParameter.addDeclaration(typeParameters[i]);
                typeParameters[i].setSymbol(typeParameter);
            }

            constructSignature.addDeclaration(constructSignatureDeclaration);
            constructSignatureDeclaration.setSignatureSymbol(constructSignature);

            var funcDecl = this.semanticInfoChain.getASTForDecl(constructSignatureDeclaration);
            this.bindParameterSymbols(funcDecl, TypeScript.ASTHelpers.parametersFromParameterList(TypeScript.ASTHelpers.getParameterList(funcDecl)), null, constructSignature);

            this.semanticInfoChain.setSymbolForAST(this.semanticInfoChain.getASTForDecl(constructSignatureDeclaration), constructSignature);

            var signatureIndex = this.getIndexForInsertingSignatureAtEndOfEnclosingDeclInSignatureList(constructSignature, parent.getOwnDeclaredConstructSignatures());
            parent.insertConstructSignatureAtIndex(constructSignature, signatureIndex);
        };

        PullSymbolBinder.prototype.bindCallSignatureDeclarationToPullSymbol = function (callSignatureDeclaration) {
            var parent = this.getParent(callSignatureDeclaration, true);
            var callSignatureAST = this.semanticInfoChain.getASTForDecl(callSignatureDeclaration);

            var callSignature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.CallSignature);

            if (TypeScript.lastParameterIsRest(callSignatureAST.parameterList)) {
                callSignature.hasVarArgs = true;
            }

            var typeParameters = callSignatureDeclaration.getTypeParameters();
            var typeParameter;

            for (var i = 0; i < typeParameters.length; i++) {
                typeParameter = callSignature.findTypeParameter(typeParameters[i].name);

                if (!typeParameter) {
                    typeParameter = new TypeScript.PullTypeParameterSymbol(typeParameters[i].name);

                    callSignature.addTypeParameter(typeParameter);
                } else {
                    var typeParameterAST = this.semanticInfoChain.getASTForDecl(typeParameter.getDeclarations()[0]);
                    this.semanticInfoChain.addDiagnosticFromAST(typeParameterAST, TypeScript.DiagnosticCode.Duplicate_identifier_0, [typeParameter.getName()]);
                }

                typeParameter.addDeclaration(typeParameters[i]);
                typeParameters[i].setSymbol(typeParameter);
            }

            callSignature.addDeclaration(callSignatureDeclaration);
            callSignatureDeclaration.setSignatureSymbol(callSignature);

            var funcDecl = this.semanticInfoChain.getASTForDecl(callSignatureDeclaration);
            this.bindParameterSymbols(funcDecl, TypeScript.ASTHelpers.parametersFromParameterList(funcDecl.parameterList), null, callSignature);

            this.semanticInfoChain.setSymbolForAST(this.semanticInfoChain.getASTForDecl(callSignatureDeclaration), callSignature);

            var signatureIndex = this.getIndexForInsertingSignatureAtEndOfEnclosingDeclInSignatureList(callSignature, parent.getOwnCallSignatures());
            parent.insertCallSignatureAtIndex(callSignature, signatureIndex);
        };

        PullSymbolBinder.prototype.bindIndexSignatureDeclarationToPullSymbol = function (indexSignatureDeclaration) {
            var indexSignature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.IndexSignature);

            indexSignature.addDeclaration(indexSignatureDeclaration);
            indexSignatureDeclaration.setSignatureSymbol(indexSignature);

            var funcDecl = this.semanticInfoChain.getASTForDecl(indexSignatureDeclaration);
            this.bindParameterSymbols(funcDecl, TypeScript.ASTHelpers.parametersFromParameter(funcDecl.parameter), null, indexSignature);

            this.semanticInfoChain.setSymbolForAST(this.semanticInfoChain.getASTForDecl(indexSignatureDeclaration), indexSignature);

            var parent = this.getParent(indexSignatureDeclaration);
            parent.addIndexSignature(indexSignature);
            indexSignature.setContainer(parent);
        };

        // getters and setters
        PullSymbolBinder.prototype.bindGetAccessorDeclarationToPullSymbol = function (getAccessorDeclaration) {
            var declKind = getAccessorDeclaration.kind;
            var declFlags = getAccessorDeclaration.flags;
            var funcDeclAST = this.semanticInfoChain.getASTForDecl(getAccessorDeclaration);

            var isExported = (declFlags & 1 /* Exported */) !== 0;

            var funcName = getAccessorDeclaration.name;

            var isSignature = (declFlags & TypeScript.PullElementFlags.Signature) !== 0;
            var isStatic = false;

            if (TypeScript.hasFlag(declFlags, TypeScript.PullElementFlags.Static)) {
                isStatic = true;
            }

            var parent = this.getParent(getAccessorDeclaration, true);
            var parentHadSymbol = false;

            var accessorSymbol = null;
            var getterSymbol = null;
            var getterTypeSymbol = null;

            if (isStatic) {
                parent = parent.getConstructorMethod().type;
            }

            accessorSymbol = parent.findMember(funcName, false);

            if (accessorSymbol) {
                if (!accessorSymbol.isAccessor()) {
                    this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(funcDeclAST.propertyName, getAccessorDeclaration.getDisplayName(), accessorSymbol.getDeclarations()[0].ast());
                    accessorSymbol = null;
                } else {
                    getterSymbol = accessorSymbol.getGetter();

                    if (getterSymbol) {
                        this.semanticInfoChain.addDiagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Getter_0_already_declared, [getAccessorDeclaration.getDisplayName()]);
                        accessorSymbol = null;
                        getterSymbol = null;
                    }
                }
            }

            if (accessorSymbol) {
                parentHadSymbol = true;
            }

            // we have an accessor we can use...
            if (accessorSymbol && getterSymbol) {
                getterTypeSymbol = getterSymbol.type;
            }

            if (!accessorSymbol) {
                accessorSymbol = new TypeScript.PullAccessorSymbol(funcName);
            }

            if (!getterSymbol) {
                getterSymbol = new TypeScript.PullSymbol(funcName, TypeScript.PullElementKind.Function);
                getterTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.FunctionType);
                getterTypeSymbol.setFunctionSymbol(getterSymbol);

                getterSymbol.type = getterTypeSymbol;

                accessorSymbol.setGetter(getterSymbol);
            }

            getAccessorDeclaration.setSymbol(accessorSymbol);
            accessorSymbol.addDeclaration(getAccessorDeclaration);
            getterSymbol.addDeclaration(getAccessorDeclaration);

            // Note that the name AST binds to the full accessor symbol, whereas the declaration AST
            // binds to just the getter symbol. This is because when the resolver resolves an
            // accessor declaration AST, it just expects the getter/setter symbol. But when
            // the language service looks up the name of an accessor, it should treat it as a
            // property and display it to the user as such.
            var nameAST = funcDeclAST.propertyName;
            this.semanticInfoChain.setSymbolForAST(nameAST, accessorSymbol);
            this.semanticInfoChain.setSymbolForAST(funcDeclAST, getterSymbol);

            if (!parentHadSymbol) {
                parent.addMember(accessorSymbol);
            }

            var signature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.CallSignature, !isSignature);

            signature.addDeclaration(getAccessorDeclaration);
            getAccessorDeclaration.setSignatureSymbol(signature);

            this.bindParameterSymbols(funcDeclAST, TypeScript.ASTHelpers.parametersFromParameterList(funcDeclAST.parameterList), getterTypeSymbol, signature);

            // add the implicit call member for this function type
            getterTypeSymbol.appendCallSignature(signature);
        };

        PullSymbolBinder.prototype.bindSetAccessorDeclarationToPullSymbol = function (setAccessorDeclaration) {
            var declKind = setAccessorDeclaration.kind;
            var declFlags = setAccessorDeclaration.flags;
            var funcDeclAST = this.semanticInfoChain.getASTForDecl(setAccessorDeclaration);

            var isExported = (declFlags & 1 /* Exported */) !== 0;

            var funcName = setAccessorDeclaration.name;

            var isSignature = (declFlags & TypeScript.PullElementFlags.Signature) !== 0;
            var isStatic = false;

            if (TypeScript.hasFlag(declFlags, TypeScript.PullElementFlags.Static)) {
                isStatic = true;
            }

            var parent = this.getParent(setAccessorDeclaration, true);
            var parentHadSymbol = false;

            var accessorSymbol = null;
            var setterSymbol = null;
            var setterTypeSymbol = null;

            if (isStatic) {
                parent = parent.getConstructorMethod().type;
            }

            accessorSymbol = parent.findMember(funcName, false);

            if (accessorSymbol) {
                if (!accessorSymbol.isAccessor()) {
                    this.semanticInfoChain.addDuplicateIdentifierDiagnosticFromAST(funcDeclAST.propertyName, setAccessorDeclaration.getDisplayName(), accessorSymbol.getDeclarations()[0].ast());
                    accessorSymbol = null;
                } else {
                    setterSymbol = accessorSymbol.getSetter();

                    if (setterSymbol) {
                        this.semanticInfoChain.addDiagnosticFromAST(funcDeclAST, TypeScript.DiagnosticCode.Setter_0_already_declared, [setAccessorDeclaration.getDisplayName()]);
                        accessorSymbol = null;
                        setterSymbol = null;
                    }
                }
            }

            if (accessorSymbol) {
                parentHadSymbol = true;

                // we have an accessor we can use...
                if (setterSymbol) {
                    setterTypeSymbol = setterSymbol.type;
                }
            }

            if (!accessorSymbol) {
                // PULLTODO: Make sure that we properly flag signature decl types when collecting decls
                accessorSymbol = new TypeScript.PullAccessorSymbol(funcName);
            }

            if (!setterSymbol) {
                setterSymbol = new TypeScript.PullSymbol(funcName, TypeScript.PullElementKind.Function);
                setterTypeSymbol = new TypeScript.PullTypeSymbol("", TypeScript.PullElementKind.FunctionType);
                setterTypeSymbol.setFunctionSymbol(setterSymbol);

                setterSymbol.type = setterTypeSymbol;

                accessorSymbol.setSetter(setterSymbol);
            }

            setAccessorDeclaration.setSymbol(accessorSymbol);
            accessorSymbol.addDeclaration(setAccessorDeclaration);
            setterSymbol.addDeclaration(setAccessorDeclaration);

            // Note that the name AST binds to the full accessor symbol, whereas the declaration AST
            // binds to just the setter symbol. This is because when the resolver resolves an
            // accessor declaration AST, it just expects the getter/setter symbol. But when
            // the language service looks up the name of an accessor, it should treat it as a
            // property and display it to the user as such.
            var nameAST = funcDeclAST.propertyName;
            this.semanticInfoChain.setSymbolForAST(nameAST, accessorSymbol);
            this.semanticInfoChain.setSymbolForAST(funcDeclAST, setterSymbol);

            if (!parentHadSymbol) {
                parent.addMember(accessorSymbol);
            }

            var signature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.CallSignature, !isSignature);

            signature.addDeclaration(setAccessorDeclaration);
            setAccessorDeclaration.setSignatureSymbol(signature);

            // PULLTODO: setter should not have a parameters
            this.bindParameterSymbols(funcDeclAST, TypeScript.ASTHelpers.parametersFromParameterList(funcDeclAST.parameterList), setterTypeSymbol, signature);

            // add the implicit call member for this function type
            setterTypeSymbol.appendCallSignature(signature);
        };

        PullSymbolBinder.prototype.getDeclsToBind = function (decl) {
            var decls;
            switch (decl.kind) {
                case TypeScript.PullElementKind.Enum:
                case TypeScript.PullElementKind.DynamicModule:
                case TypeScript.PullElementKind.Container:
                case TypeScript.PullElementKind.Interface:
                    decls = this.findDeclsInContext(decl, decl.kind, true);
                    break;

                case TypeScript.PullElementKind.Variable:
                case TypeScript.PullElementKind.Function:
                case TypeScript.PullElementKind.Method:
                case TypeScript.PullElementKind.ConstructorMethod:
                    decls = this.findDeclsInContext(decl, decl.kind, false);
                    break;

                default:
                    decls = [decl];
            }
            TypeScript.Debug.assert(decls && decls.length > 0);
            TypeScript.Debug.assert(TypeScript.ArrayUtilities.contains(decls, decl));
            return decls;
        };

        PullSymbolBinder.prototype.shouldBindDeclaration = function (decl) {
            return !decl.hasBeenBound() && this.declsBeingBound.indexOf(decl.declID) < 0;
        };

        // binding
        PullSymbolBinder.prototype.bindDeclToPullSymbol = function (decl) {
            if (this.shouldBindDeclaration(decl)) {
                // The decl does not have a symbol attached to it and
                // its not already being bound
                this.bindAllDeclsToPullSymbol(decl);
            }
        };

        PullSymbolBinder.prototype.bindAllDeclsToPullSymbol = function (askedDecl) {
            var allDecls = this.getDeclsToBind(askedDecl);
            for (var i = 0; i < allDecls.length; i++) {
                var decl = allDecls[i];

                // This check is necessary for two reasons
                // 1. This decl could be actually something we dont care to bind other decls corresponding to it
                //    and so it was already bound without binding other decls with same name corresponding to it (e.g parameter)
                // 2. This was bound as part of some recursion
                //    eg:
                //      module A {
                //          var o;
                //      }
                //      enum A {
                //          /*quickInfoHere*/c
                //      }
                //      module A {
                //          var p;
                //      }
                //     As part of binding the member declaration, we would bind enum and hence end up binding all value decls
                //     which are instance vars from module declaration. As part of it, we would want to bind corresponding
                //     module declaration which in turn would try to bind all the var declarations
                if (this.shouldBindDeclaration(decl)) {
                    this.bindSingleDeclToPullSymbol(decl);
                }
            }
        };

        PullSymbolBinder.prototype.bindSingleDeclToPullSymbol = function (decl) {
            // Add it to the list in case we revisit it during binding
            this.declsBeingBound.push(decl.declID);

            switch (decl.kind) {
                case TypeScript.PullElementKind.Script:
                    var childDecls = decl.getChildDecls();
                    for (var i = 0; i < childDecls.length; i++) {
                        this.bindDeclToPullSymbol(childDecls[i]);
                    }
                    break;

                case TypeScript.PullElementKind.Enum:
                    this.bindEnumDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.DynamicModule:
                case TypeScript.PullElementKind.Container:
                    this.bindModuleDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.Interface:
                    this.bindInterfaceDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.Class:
                    this.bindClassDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.Function:
                    this.bindFunctionDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.Variable:
                    this.bindVariableDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.CatchVariable:
                    this.bindCatchVariableToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.EnumMember:
                    this.bindEnumMemberDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.Property:
                    this.bindPropertyDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.Method:
                    this.bindMethodDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.ConstructorMethod:
                    this.bindConstructorDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.CallSignature:
                    this.bindCallSignatureDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.ConstructSignature:
                    this.bindConstructSignatureDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.IndexSignature:
                    this.bindIndexSignatureDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.GetAccessor:
                    this.bindGetAccessorDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.SetAccessor:
                    this.bindSetAccessorDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.ObjectType:
                    this.bindObjectTypeDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.FunctionType:
                    this.bindFunctionTypeDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.ConstructorType:
                    this.bindConstructorTypeDeclarationToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.FunctionExpression:
                    this.bindFunctionExpressionToPullSymbol(decl);
                    break;

                case TypeScript.PullElementKind.TypeAlias:
                    this.bindImportDeclaration(decl);
                    break;

                case TypeScript.PullElementKind.Parameter:
                case TypeScript.PullElementKind.TypeParameter:
                    // parameters are bound by their enclosing function or type.  Ensure that that
                    // decl is bound.
                    decl.getParentDecl().getSymbol();
                    break;

                case TypeScript.PullElementKind.CatchBlock:
                case TypeScript.PullElementKind.WithBlock:
                    break;

                default:
                    TypeScript.CompilerDiagnostics.assert(false, "Unrecognized type declaration");
            }

            // Rremove the decl from the list
            TypeScript.Debug.assert(TypeScript.ArrayUtilities.last(this.declsBeingBound) === decl.declID);
            this.declsBeingBound.pop();
        };
        return PullSymbolBinder;
    })();
    TypeScript.PullSymbolBinder = PullSymbolBinder;
})(TypeScript || (TypeScript = {}));
