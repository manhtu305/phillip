// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    var CandidateInferenceInfo = (function () {
        function CandidateInferenceInfo() {
            this.typeParameter = null;
            this._inferredTypeAfterFixing = null;
            this.inferenceCandidates = [];
        }
        CandidateInferenceInfo.prototype.addCandidate = function (candidate) {
            if (!this._inferredTypeAfterFixing) {
                this.inferenceCandidates[this.inferenceCandidates.length] = candidate;
            }
        };

        CandidateInferenceInfo.prototype.isFixed = function () {
            return !!this._inferredTypeAfterFixing;
        };

        CandidateInferenceInfo.prototype.fixTypeParameter = function (resolver, context) {
            var _this = this;
            if (!this._inferredTypeAfterFixing) {
                // November 18, 2013: Section 4.12.2:
                // The inferred type argument for a particular type parameter is the widened form
                // (section 3.9) of the best common type(section 3.10) of a set of candidate types.
                var collection = {
                    getLength: function () {
                        return _this.inferenceCandidates.length;
                    },
                    getTypeAtIndex: function (index) {
                        return _this.inferenceCandidates[index].type;
                    }
                };

                // Now widen (per the spec citation above)
                var bestCommonType = resolver.findBestCommonType(collection, context, new TypeScript.TypeComparisonInfo());
                this._inferredTypeAfterFixing = bestCommonType.widenedType(resolver, null, context);
            }
        };
        return CandidateInferenceInfo;
    })();
    TypeScript.CandidateInferenceInfo = CandidateInferenceInfo;

    var TypeArgumentInferenceContext = (function () {
        function TypeArgumentInferenceContext(resolver, context, signatureBeingInferred) {
            this.resolver = resolver;
            this.context = context;
            this.signatureBeingInferred = signatureBeingInferred;
            this.inferenceCache = TypeScript.BitMatrix.getBitMatrix(false);
            this.candidateCache = [];
            var typeParameters = signatureBeingInferred.getTypeParameters();
            for (var i = 0; i < typeParameters.length; i++) {
                this.addInferenceRoot(typeParameters[i]);
            }
        }
        TypeArgumentInferenceContext.prototype.alreadyRelatingTypes = function (objectType, parameterType) {
            if (this.inferenceCache.valueAt(objectType.pullSymbolID, parameterType.pullSymbolID)) {
                return true;
            } else {
                this.inferenceCache.setValueAt(objectType.pullSymbolID, parameterType.pullSymbolID, true);
                return false;
            }
        };

        TypeArgumentInferenceContext.prototype.resetRelationshipCache = function () {
            this.inferenceCache.release();
            this.inferenceCache = TypeScript.BitMatrix.getBitMatrix(false);
        };

        TypeArgumentInferenceContext.prototype.addInferenceRoot = function (param) {
            var info = this.candidateCache[param.pullSymbolID];

            if (!info) {
                info = new CandidateInferenceInfo();
                info.typeParameter = param;
                this.candidateCache[param.pullSymbolID] = info;
            }
        };

        TypeArgumentInferenceContext.prototype.getInferenceInfo = function (param) {
            return this.candidateCache[param.pullSymbolID];
        };

        TypeArgumentInferenceContext.prototype.addCandidateForInference = function (param, candidate) {
            var info = this.getInferenceInfo(param);

            // Add the candidate to the CandidateInferenceInfo for this type parameter
            // only if the candidate is not already present.
            if (info && candidate && info.inferenceCandidates.indexOf(candidate) < 0) {
                info.addCandidate(candidate);
            }
        };

        TypeArgumentInferenceContext.prototype.inferTypeArguments = function () {
            throw TypeScript.Errors.abstract();
        };

        TypeArgumentInferenceContext.prototype.fixTypeParameter = function (typeParameter) {
            var candidateInfo = this.candidateCache[typeParameter.pullSymbolID];
            if (candidateInfo) {
                candidateInfo.fixTypeParameter(this.resolver, this.context);
            }
        };

        TypeArgumentInferenceContext.prototype._finalizeInferredTypeArguments = function () {
            var results = [];
            var typeParameters = this.signatureBeingInferred.getTypeParameters();
            for (var i = 0; i < typeParameters.length; i++) {
                var info = this.candidateCache[typeParameters[i].pullSymbolID];

                info.fixTypeParameter(this.resolver, this.context);

                for (var i = 0; i < results.length; i++) {
                    if (results[i].type === info.typeParameter) {
                        results[i].type = info._inferredTypeAfterFixing;
                    }
                }

                results.push(info._inferredTypeAfterFixing);
            }

            return results;
        };

        TypeArgumentInferenceContext.prototype.isInvocationInferenceContext = function () {
            throw TypeScript.Errors.abstract();
        };
        return TypeArgumentInferenceContext;
    })();
    TypeScript.TypeArgumentInferenceContext = TypeArgumentInferenceContext;

    var InvocationTypeArgumentInferenceContext = (function (_super) {
        __extends(InvocationTypeArgumentInferenceContext, _super);
        function InvocationTypeArgumentInferenceContext(resolver, context, signatureBeingInferred, argumentASTs) {
            _super.call(this, resolver, context, signatureBeingInferred);
            this.argumentASTs = argumentASTs;
        }
        InvocationTypeArgumentInferenceContext.prototype.isInvocationInferenceContext = function () {
            return true;
        };

        InvocationTypeArgumentInferenceContext.prototype.inferTypeArguments = function () {
            var _this = this;
            // Resolve all of the argument ASTs in the callback
            this.signatureBeingInferred.forAllParameterTypes(this.argumentASTs.nonSeparatorCount(), function (parameterType, argumentIndex) {
                var argumentAST = _this.argumentASTs.nonSeparatorAt(argumentIndex);

                _this.context.pushInferentialType(parameterType, _this);
                var argumentType = _this.resolver.resolveAST(argumentAST, true, _this.context).type;
                _this.resolver.relateTypeToTypeParametersWithNewEnclosingTypes(argumentType, parameterType, _this, _this.context);
                _this.context.popAnyContextualType();

                return true;
            });

            return this._finalizeInferredTypeArguments();
        };
        return InvocationTypeArgumentInferenceContext;
    })(TypeArgumentInferenceContext);
    TypeScript.InvocationTypeArgumentInferenceContext = InvocationTypeArgumentInferenceContext;

    var ContextualSignatureInstantiationTypeArgumentInferenceContext = (function (_super) {
        __extends(ContextualSignatureInstantiationTypeArgumentInferenceContext, _super);
        // for the shouldFixContextualSignatureParameterTypes flag, pass true during inferential typing
        // and false during signature relation checking
        function ContextualSignatureInstantiationTypeArgumentInferenceContext(resolver, context, signatureBeingInferred, contextualSignature, shouldFixContextualSignatureParameterTypes) {
            _super.call(this, resolver, context, signatureBeingInferred);
            this.contextualSignature = contextualSignature;
            this.shouldFixContextualSignatureParameterTypes = shouldFixContextualSignatureParameterTypes;
        }
        ContextualSignatureInstantiationTypeArgumentInferenceContext.prototype.isInvocationInferenceContext = function () {
            return false;
        };

        ContextualSignatureInstantiationTypeArgumentInferenceContext.prototype.inferTypeArguments = function () {
            var _this = this;
            // We are in contextual signature instantiation. This callback will be executed
            // for each parameter we are trying to relate in order to infer type arguments.
            var relateTypesCallback = function (parameterTypeBeingInferred, contextualParameterType) {
                if (_this.shouldFixContextualSignatureParameterTypes) {
                    // Need to modify the callback to cause fixing. Per spec section 4.12.2
                    // 4th bullet of inferential typing:
                    // ... then any inferences made for type parameters referenced by the
                    // parameters of T's call signature are fixed
                    // (T here is the contextual signature)
                    contextualParameterType = _this.context.fixAllTypeParametersReferencedByType(contextualParameterType, _this.resolver, _this);
                }
                _this.resolver.relateTypeToTypeParametersWithNewEnclosingTypes(contextualParameterType, parameterTypeBeingInferred, _this, _this.context);

                return true;
            };

            this.signatureBeingInferred.forAllCorrespondingParameterTypesInThisAndOtherSignature(this.contextualSignature, relateTypesCallback);

            return this._finalizeInferredTypeArguments();
        };
        return ContextualSignatureInstantiationTypeArgumentInferenceContext;
    })(TypeArgumentInferenceContext);
    TypeScript.ContextualSignatureInstantiationTypeArgumentInferenceContext = ContextualSignatureInstantiationTypeArgumentInferenceContext;

    var PullContextualTypeContext = (function () {
        function PullContextualTypeContext(contextualType, provisional, isInferentiallyTyping, typeArgumentInferenceContext) {
            this.contextualType = contextualType;
            this.provisional = provisional;
            this.isInferentiallyTyping = isInferentiallyTyping;
            this.typeArgumentInferenceContext = typeArgumentInferenceContext;
            this.provisionallyTypedSymbols = [];
            this.hasProvisionalErrors = false;
            this.astSymbolMap = [];
        }
        PullContextualTypeContext.prototype.recordProvisionallyTypedSymbol = function (symbol) {
            this.provisionallyTypedSymbols[this.provisionallyTypedSymbols.length] = symbol;
        };

        PullContextualTypeContext.prototype.invalidateProvisionallyTypedSymbols = function () {
            for (var i = 0; i < this.provisionallyTypedSymbols.length; i++) {
                this.provisionallyTypedSymbols[i].setUnresolved();
            }
        };

        PullContextualTypeContext.prototype.setSymbolForAST = function (ast, symbol) {
            this.astSymbolMap[ast.syntaxID()] = symbol;
        };

        PullContextualTypeContext.prototype.getSymbolForAST = function (ast) {
            return this.astSymbolMap[ast.syntaxID()];
        };
        return PullContextualTypeContext;
    })();
    TypeScript.PullContextualTypeContext = PullContextualTypeContext;

    var PullTypeResolutionContext = (function () {
        function PullTypeResolutionContext(resolver, inTypeCheck, fileName) {
            if (typeof inTypeCheck === "undefined") { inTypeCheck = false; }
            if (typeof fileName === "undefined") { fileName = null; }
            this.resolver = resolver;
            this.inTypeCheck = inTypeCheck;
            this.fileName = fileName;
            this.contextStack = [];
            this.typeCheckedNodes = null;
            this.enclosingTypeWalker1 = null;
            this.enclosingTypeWalker2 = null;
            this.inBaseTypeResolution = false;
            if (inTypeCheck) {
                TypeScript.Debug.assert(fileName, "A file name must be provided if you are typechecking");
                this.typeCheckedNodes = TypeScript.BitVector.getBitVector(false);
            }
        }
        PullTypeResolutionContext.prototype.setTypeChecked = function (ast) {
            if (!this.inProvisionalResolution()) {
                this.typeCheckedNodes.setValueAt(ast.syntaxID(), true);
            }
        };

        PullTypeResolutionContext.prototype.canTypeCheckAST = function (ast) {
            // If we're in a context where we're type checking, and the ast we're typechecking
            // hasn't been typechecked in this phase yet, *and* the ast is from the file we're
            // currently typechecking, then we can typecheck.
            //
            // If the ast has been typechecked in this phase, then there's no need to typecheck
            // it again.  Also, if it's from another file, there's no need to typecheck it since
            // whatever host we're in will eventually get around to typechecking it.  This is
            // also important as it's very possible to stack overflow when typechecking if we
            // keep jumping around to AST nodes all around a large project.
            return this.typeCheck() && !this.typeCheckedNodes.valueAt(ast.syntaxID()) && this.fileName === ast.fileName();
        };

        PullTypeResolutionContext.prototype._pushAnyContextualType = function (type, provisional, isInferentiallyTyping, argContext) {
            this.contextStack.push(new PullContextualTypeContext(type, provisional, isInferentiallyTyping, argContext));
        };

        // Use this to push any kind of contextual type if it is NOT propagated inward from a parent
        // contextual type. This corresponds to the first series of bullets in Section 4.19 of the spec.
        PullTypeResolutionContext.prototype.pushNewContextualType = function (type) {
            this._pushAnyContextualType(type, this.inProvisionalResolution(), false, null);
        };

        // Use this when propagating a contextual type from a parent contextual type to a subexpression.
        // This corresponds to the second series of bullets in section 4.19 of the spec.
        PullTypeResolutionContext.prototype.propagateContextualType = function (type) {
            this._pushAnyContextualType(type, this.inProvisionalResolution(), this.isInferentiallyTyping(), this.getCurrentTypeArgumentInferenceContext());
        };

        // Use this if you are trying to infer type arguments.
        PullTypeResolutionContext.prototype.pushInferentialType = function (type, typeArgumentInferenceContext) {
            this._pushAnyContextualType(type, true, true, typeArgumentInferenceContext);
        };

        // Use this if you are trying to choose an overload and are trying a contextual type.
        PullTypeResolutionContext.prototype.pushProvisionalType = function (type) {
            this._pushAnyContextualType(type, true, false, null);
        };

        // Use this to pop any kind of contextual type
        PullTypeResolutionContext.prototype.popAnyContextualType = function () {
            var tc = this.contextStack.pop();

            tc.invalidateProvisionallyTypedSymbols();

            // If the context we just popped off had provisional errors, and we are *still* in a provisional context,
            // we need to not forget that we had provisional errors in a deeper context. We do this by setting the
            // hasProvisioanlErrors flag on the now top context on the stack.
            if (tc.hasProvisionalErrors && this.inProvisionalResolution()) {
                this.contextStack[this.contextStack.length - 1].hasProvisionalErrors = true;
            }

            return tc;
        };

        PullTypeResolutionContext.prototype.hasProvisionalErrors = function () {
            return this.contextStack.length ? this.contextStack[this.contextStack.length - 1].hasProvisionalErrors : false;
        };

        // Gets the current contextual or inferential type
        PullTypeResolutionContext.prototype.getContextualType = function () {
            var context = !this.contextStack.length ? null : this.contextStack[this.contextStack.length - 1];

            if (context) {
                var type = context.contextualType;

                if (!type) {
                    return null;
                }

                return type;
            }

            return null;
        };

        PullTypeResolutionContext.prototype.fixAllTypeParametersReferencedByType = function (type, resolver, argContext) {
            var argContext = this.getCurrentTypeArgumentInferenceContext();
            if (type.wrapsSomeTypeParameter(argContext.candidateCache)) {
                // Build up a type parameter argument map with which we will instantiate this type
                // after fixing type parameters
                var typeParameterArgumentMap = [];

                for (var n in argContext.candidateCache) {
                    var typeParameter = argContext.candidateCache[n] && argContext.candidateCache[n].typeParameter;
                    if (typeParameter) {
                        var dummyMap = [];
                        dummyMap[typeParameter.pullSymbolID] = typeParameter;
                        if (type.wrapsSomeTypeParameter(dummyMap)) {
                            argContext.fixTypeParameter(typeParameter);
                            TypeScript.Debug.assert(argContext.candidateCache[n]._inferredTypeAfterFixing);
                            typeParameterArgumentMap[typeParameter.pullSymbolID] = argContext.candidateCache[n]._inferredTypeAfterFixing;
                        }
                    }
                }

                return resolver.instantiateType(type, typeParameterArgumentMap);
            }

            return type;
        };

        // If we are not in inferential typing, this will return null
        PullTypeResolutionContext.prototype.getCurrentTypeArgumentInferenceContext = function () {
            return this.contextStack.length ? this.contextStack[this.contextStack.length - 1].typeArgumentInferenceContext : null;
        };

        PullTypeResolutionContext.prototype.isInferentiallyTyping = function () {
            return this.contextStack.length > 0 && this.contextStack[this.contextStack.length - 1].isInferentiallyTyping;
        };

        PullTypeResolutionContext.prototype.inProvisionalResolution = function () {
            return (!this.contextStack.length ? false : this.contextStack[this.contextStack.length - 1].provisional);
        };

        PullTypeResolutionContext.prototype.isInBaseTypeResolution = function () {
            return this.inBaseTypeResolution;
        };

        PullTypeResolutionContext.prototype.startBaseTypeResolution = function () {
            var wasInBaseTypeResoltion = this.inBaseTypeResolution;
            this.inBaseTypeResolution = true;
            return wasInBaseTypeResoltion;
        };

        PullTypeResolutionContext.prototype.doneBaseTypeResolution = function (wasInBaseTypeResolution) {
            this.inBaseTypeResolution = wasInBaseTypeResolution;
        };

        PullTypeResolutionContext.prototype.setTypeInContext = function (symbol, type) {
            // if type of symbol was already determined and it is error - do not replace it with something else
            // otherwise it might cause problems in cases like:
            // var bar: foo;
            // class bar {}
            // class foo {}
            // Symbol for variable bar will get created first, then after binding class constructor of class bar conflict will be detected and symbol.type will be set to error.
            // Later after binding type reference 'foo', symbol.type (that is now 'error') will be incorrectly replace with 'foo'.
            // Since this symbol shared between explicit variable declaration and implicit variable for class constructor
            // we now ended up in situation where class constructor for 'baz' is type reference to 'foo' which is wrong.
            if (symbol.type && symbol.type.isError() && !type.isError()) {
                return;
            }
            symbol.type = type;

            if (this.contextStack.length && this.inProvisionalResolution()) {
                this.contextStack[this.contextStack.length - 1].recordProvisionallyTypedSymbol(symbol);
            }
        };

        PullTypeResolutionContext.prototype.postDiagnostic = function (diagnostic) {
            if (diagnostic) {
                if (this.inProvisionalResolution()) {
                    (this.contextStack[this.contextStack.length - 1]).hasProvisionalErrors = true;
                } else if (this.inTypeCheck && this.resolver) {
                    this.resolver.semanticInfoChain.addDiagnostic(diagnostic);
                }
            }
        };

        PullTypeResolutionContext.prototype.typeCheck = function () {
            return this.inTypeCheck && !this.inProvisionalResolution();
        };

        PullTypeResolutionContext.prototype.setSymbolForAST = function (ast, symbol) {
            this.contextStack[this.contextStack.length - 1].setSymbolForAST(ast, symbol);
        };

        PullTypeResolutionContext.prototype.getSymbolForAST = function (ast) {
            for (var i = this.contextStack.length - 1; i >= 0; i--) {
                var typeContext = this.contextStack[i];
                if (!typeContext.provisional) {
                    break;
                }

                var symbol = typeContext.getSymbolForAST(ast);
                if (symbol) {
                    return symbol;
                }
            }

            return null;
        };

        PullTypeResolutionContext.prototype.startWalkingTypes = function (symbol1, symbol2) {
            if (!this.enclosingTypeWalker1) {
                this.enclosingTypeWalker1 = new TypeScript.PullTypeEnclosingTypeWalker();
            }
            var symbolsWhenStartedWalkingTypes1 = this.enclosingTypeWalker1.startWalkingType(symbol1);
            if (!this.enclosingTypeWalker2) {
                this.enclosingTypeWalker2 = new TypeScript.PullTypeEnclosingTypeWalker();
            }
            var symbolsWhenStartedWalkingTypes2 = this.enclosingTypeWalker2.startWalkingType(symbol2);
            return { symbolsWhenStartedWalkingTypes1: symbolsWhenStartedWalkingTypes1, symbolsWhenStartedWalkingTypes2: symbolsWhenStartedWalkingTypes2 };
        };

        PullTypeResolutionContext.prototype.endWalkingTypes = function (symbolsWhenStartedWalkingTypes) {
            this.enclosingTypeWalker1.endWalkingType(symbolsWhenStartedWalkingTypes.symbolsWhenStartedWalkingTypes1);
            this.enclosingTypeWalker2.endWalkingType(symbolsWhenStartedWalkingTypes.symbolsWhenStartedWalkingTypes2);
        };

        PullTypeResolutionContext.prototype.setEnclosingTypes = function (symbol1, symbol2) {
            if (!this.enclosingTypeWalker1) {
                this.enclosingTypeWalker1 = new TypeScript.PullTypeEnclosingTypeWalker();
            }
            this.enclosingTypeWalker1.setEnclosingType(symbol1);
            if (!this.enclosingTypeWalker2) {
                this.enclosingTypeWalker2 = new TypeScript.PullTypeEnclosingTypeWalker();
            }
            this.enclosingTypeWalker2.setEnclosingType(symbol2);
        };

        PullTypeResolutionContext.prototype.walkMemberTypes = function (memberName) {
            this.enclosingTypeWalker1.walkMemberType(memberName, this.resolver);
            this.enclosingTypeWalker2.walkMemberType(memberName, this.resolver);
        };

        PullTypeResolutionContext.prototype.postWalkMemberTypes = function () {
            this.enclosingTypeWalker1.postWalkMemberType();
            this.enclosingTypeWalker2.postWalkMemberType();
        };

        PullTypeResolutionContext.prototype.walkSignatures = function (kind, index, index2) {
            this.enclosingTypeWalker1.walkSignature(kind, index);
            this.enclosingTypeWalker2.walkSignature(kind, index2 == undefined ? index : index2);
        };

        PullTypeResolutionContext.prototype.postWalkSignatures = function () {
            this.enclosingTypeWalker1.postWalkSignature();
            this.enclosingTypeWalker2.postWalkSignature();
        };

        PullTypeResolutionContext.prototype.walkTypeParameterConstraints = function (index) {
            this.enclosingTypeWalker1.walkTypeParameterConstraint(index);
            this.enclosingTypeWalker2.walkTypeParameterConstraint(index);
        };

        PullTypeResolutionContext.prototype.postWalkTypeParameterConstraints = function () {
            this.enclosingTypeWalker1.postWalkTypeParameterConstraint();
            this.enclosingTypeWalker2.postWalkTypeParameterConstraint();
        };

        PullTypeResolutionContext.prototype.walkTypeArgument = function (index) {
            this.enclosingTypeWalker1.walkTypeArgument(index);
            this.enclosingTypeWalker2.walkTypeArgument(index);
        };

        PullTypeResolutionContext.prototype.postWalkTypeArgument = function () {
            this.enclosingTypeWalker1.postWalkTypeArgument();
            this.enclosingTypeWalker2.postWalkTypeArgument();
        };

        PullTypeResolutionContext.prototype.walkReturnTypes = function () {
            this.enclosingTypeWalker1.walkReturnType();
            this.enclosingTypeWalker2.walkReturnType();
        };

        PullTypeResolutionContext.prototype.postWalkReturnTypes = function () {
            this.enclosingTypeWalker1.postWalkReturnType();
            this.enclosingTypeWalker2.postWalkReturnType();
        };

        PullTypeResolutionContext.prototype.walkParameterTypes = function (iParam) {
            this.enclosingTypeWalker1.walkParameterType(iParam);
            this.enclosingTypeWalker2.walkParameterType(iParam);
        };

        PullTypeResolutionContext.prototype.postWalkParameterTypes = function () {
            this.enclosingTypeWalker1.postWalkParameterType();
            this.enclosingTypeWalker2.postWalkParameterType();
        };

        PullTypeResolutionContext.prototype.getBothKindOfIndexSignatures = function (includeAugmentedType1, includeAugmentedType2) {
            var indexSigs1 = this.enclosingTypeWalker1.getBothKindOfIndexSignatures(this.resolver, this, includeAugmentedType1);
            var indexSigs2 = this.enclosingTypeWalker2.getBothKindOfIndexSignatures(this.resolver, this, includeAugmentedType2);
            return { indexSigs1: indexSigs1, indexSigs2: indexSigs2 };
        };

        PullTypeResolutionContext.prototype.walkIndexSignatureReturnTypes = function (indexSigs, useStringIndexSignature1, useStringIndexSignature2, onlySignature) {
            this.enclosingTypeWalker1.walkIndexSignatureReturnType(indexSigs.indexSigs1, useStringIndexSignature1, onlySignature);
            this.enclosingTypeWalker2.walkIndexSignatureReturnType(indexSigs.indexSigs2, useStringIndexSignature2, onlySignature);
        };

        PullTypeResolutionContext.prototype.postWalkIndexSignatureReturnTypes = function (onlySignature) {
            this.enclosingTypeWalker1.postWalkIndexSignatureReturnType(onlySignature);
            this.enclosingTypeWalker2.postWalkIndexSignatureReturnType(onlySignature);
        };

        PullTypeResolutionContext.prototype.swapEnclosingTypeWalkers = function () {
            var tempEnclosingWalker1 = this.enclosingTypeWalker1;
            this.enclosingTypeWalker1 = this.enclosingTypeWalker2;
            this.enclosingTypeWalker2 = tempEnclosingWalker1;
        };

        PullTypeResolutionContext.prototype.oneOfClassificationsIsInfinitelyExpanding = function () {
            var generativeClassification1 = this.enclosingTypeWalker1.getGenerativeClassification();
            if (generativeClassification1 === 3 /* InfinitelyExpanding */) {
                return true;
            }
            var generativeClassification2 = this.enclosingTypeWalker2.getGenerativeClassification();
            if (generativeClassification2 === 3 /* InfinitelyExpanding */) {
                return true;
            }

            return false;
        };

        PullTypeResolutionContext.prototype.resetEnclosingTypeWalkers = function () {
            var enclosingTypeWalker1 = this.enclosingTypeWalker1;
            var enclosingTypeWalker2 = this.enclosingTypeWalker2;
            this.enclosingTypeWalker1 = null;
            this.enclosingTypeWalker2 = null;
            return {
                enclosingTypeWalker1: enclosingTypeWalker1,
                enclosingTypeWalker2: enclosingTypeWalker2
            };
        };

        PullTypeResolutionContext.prototype.setEnclosingTypeWalkers = function (enclosingTypeWalkers) {
            this.enclosingTypeWalker1 = enclosingTypeWalkers.enclosingTypeWalker1;
            this.enclosingTypeWalker2 = enclosingTypeWalkers.enclosingTypeWalker2;
        };
        return PullTypeResolutionContext;
    })();
    TypeScript.PullTypeResolutionContext = PullTypeResolutionContext;
})(TypeScript || (TypeScript = {}));
