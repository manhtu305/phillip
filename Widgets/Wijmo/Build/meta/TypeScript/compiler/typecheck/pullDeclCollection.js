// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    var DeclCollectionContext = (function () {
        function DeclCollectionContext(document, semanticInfoChain, propagateEnumConstants) {
            this.document = document;
            this.semanticInfoChain = semanticInfoChain;
            this.propagateEnumConstants = propagateEnumConstants;
            this.isDeclareFile = false;
            this.parentChain = [];
        }
        DeclCollectionContext.prototype.getParent = function () {
            return this.parentChain ? this.parentChain[this.parentChain.length - 1] : null;
        };

        DeclCollectionContext.prototype.pushParent = function (parentDecl) {
            if (parentDecl) {
                this.parentChain[this.parentChain.length] = parentDecl;
            }
        };

        DeclCollectionContext.prototype.popParent = function () {
            this.parentChain.length--;
        };
        return DeclCollectionContext;
    })();

    function moduleElementsHasExportAssignment(moduleElements) {
        return moduleElements.any(function (m) {
            return m.kind() === 134 /* ExportAssignment */;
        });
    }

    function containingModuleHasExportAssignment(ast) {
        ast = ast.parent;
        while (ast) {
            if (ast.kind() === 130 /* ModuleDeclaration */) {
                var moduleDecl = ast;
                return moduleElementsHasExportAssignment(moduleDecl.moduleElements);
            } else if (ast.kind() === 120 /* SourceUnit */) {
                var sourceUnit = ast;
                return moduleElementsHasExportAssignment(sourceUnit.moduleElements);
            }

            ast = ast.parent;
        }

        return false;
    }

    function isParsingAmbientModule(ast, context) {
        ast = ast.parent;
        while (ast) {
            if (ast.kind() === 130 /* ModuleDeclaration */) {
                if (TypeScript.hasModifier(ast.modifiers, TypeScript.PullElementFlags.Ambient)) {
                    return true;
                }
            }

            ast = ast.parent;
        }

        return false;
    }

    function preCollectImportDecls(ast, context) {
        var importDecl = ast;
        var declFlags = 0 /* None */;

        var parent = context.getParent();

        if (TypeScript.hasModifier(importDecl.modifiers, 1 /* Exported */) && !containingModuleHasExportAssignment(ast)) {
            declFlags |= 1 /* Exported */;
        }

        var decl = new TypeScript.NormalPullDecl(importDecl.identifier.valueText(), importDecl.identifier.text(), TypeScript.PullElementKind.TypeAlias, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(ast, decl);
        context.semanticInfoChain.setASTForDecl(decl, ast);
        // Note: it is intentional that a import does not get added to hte context stack.  An
        // import does not introduce a new name scope, so it shouldn't be in the context decl stack.
        // context.pushParent(decl);
    }

    function preCollectScriptDecls(sourceUnit, context) {
        var fileName = sourceUnit.fileName();

        var isExternalModule = context.document.isExternalModule();

        var decl = new TypeScript.RootPullDecl(fileName, fileName, TypeScript.PullElementKind.Script, 0 /* None */, context.semanticInfoChain, isExternalModule);
        context.semanticInfoChain.setDeclForAST(sourceUnit, decl);
        context.semanticInfoChain.setASTForDecl(decl, sourceUnit);

        context.isDeclareFile = context.document.isDeclareFile();

        context.pushParent(decl);

        // if it's an external module, create another decl to represent that module inside the top
        // level script module.
        if (isExternalModule) {
            var declFlags = 1 /* Exported */;
            if (TypeScript.isDTSFile(fileName)) {
                declFlags |= TypeScript.PullElementFlags.Ambient;
            }

            var moduleContainsExecutableCode = containsExecutableCode(sourceUnit.moduleElements);
            var kind = TypeScript.PullElementKind.DynamicModule;
            var valueText = TypeScript.quoteStr(fileName);

            var decl = new TypeScript.NormalPullDecl(valueText, fileName, kind, declFlags, context.getParent());

            context.semanticInfoChain.setASTForDecl(decl, sourceUnit);

            // Note: we're overring what the script points to.  For files with an external module,
            // the script node will point at the external module declaration.
            context.semanticInfoChain.setDeclForAST(sourceUnit, decl);

            if (!moduleElementsHasExportAssignment(sourceUnit.moduleElements) || moduleContainsExecutableCode) {
                createModuleVariableDecl(decl, sourceUnit, context);
            }

            context.pushParent(decl);
        }
    }

    function preCollectEnumDecls(enumDecl, context) {
        var declFlags = 0 /* None */;
        var enumName = enumDecl.identifier.valueText();

        if ((TypeScript.hasModifier(enumDecl.modifiers, 1 /* Exported */) || isParsingAmbientModule(enumDecl, context)) && !containingModuleHasExportAssignment(enumDecl)) {
            declFlags |= 1 /* Exported */;
        }

        if (TypeScript.hasModifier(enumDecl.modifiers, TypeScript.PullElementFlags.Ambient) || isParsingAmbientModule(enumDecl, context) || context.isDeclareFile) {
            declFlags |= TypeScript.PullElementFlags.Ambient;
        }

        // Consider an enum 'always initialized'.
        declFlags |= TypeScript.PullElementFlags.Enum;
        var kind = TypeScript.PullElementKind.Enum;

        var enumDeclaration = new TypeScript.NormalPullDecl(enumName, enumDecl.identifier.text(), kind, declFlags, context.getParent());
        context.semanticInfoChain.setDeclForAST(enumDecl, enumDeclaration);
        context.semanticInfoChain.setASTForDecl(enumDeclaration, enumDecl);

        // create the value decl
        var valueDecl = new TypeScript.NormalPullDecl(enumDeclaration.name, enumDeclaration.getDisplayName(), TypeScript.PullElementKind.Variable, enumDeclaration.flags, context.getParent());
        enumDeclaration.setValueDecl(valueDecl);
        context.semanticInfoChain.setASTForDecl(valueDecl, enumDecl);

        context.pushParent(enumDeclaration);
    }

    function createEnumElementDecls(propertyDecl, context) {
        var parent = context.getParent();

        var decl = new TypeScript.PullEnumElementDecl(propertyDecl.propertyName.valueText(), propertyDecl.propertyName.text(), parent);
        context.semanticInfoChain.setDeclForAST(propertyDecl, decl);
        context.semanticInfoChain.setASTForDecl(decl, propertyDecl);
        // Note: it is intentional that a enum element does not get added to hte context stack.  An
        // enum element does not introduce a new name scope, so it shouldn't be in the context decl stack.
        // context.pushParent(decl);
    }

    function preCollectModuleDecls(moduleDecl, context) {
        var declFlags = 0 /* None */;

        var moduleContainsExecutableCode = containsExecutableCode(moduleDecl.moduleElements);

        var isDynamic = moduleDecl.stringLiteral !== null;

        if ((TypeScript.hasModifier(moduleDecl.modifiers, 1 /* Exported */) || isParsingAmbientModule(moduleDecl, context)) && !containingModuleHasExportAssignment(moduleDecl)) {
            declFlags |= 1 /* Exported */;
        }

        if (TypeScript.hasModifier(moduleDecl.modifiers, TypeScript.PullElementFlags.Ambient) || isParsingAmbientModule(moduleDecl, context) || context.isDeclareFile) {
            declFlags |= TypeScript.PullElementFlags.Ambient;
        }

        var kind = isDynamic ? TypeScript.PullElementKind.DynamicModule : TypeScript.PullElementKind.Container;

        if (moduleDecl.stringLiteral) {
            var valueText = TypeScript.quoteStr(moduleDecl.stringLiteral.valueText());
            var text = moduleDecl.stringLiteral.text();

            var decl = new TypeScript.NormalPullDecl(valueText, text, kind, declFlags, context.getParent());

            context.semanticInfoChain.setDeclForAST(moduleDecl, decl);
            context.semanticInfoChain.setDeclForAST(moduleDecl.stringLiteral, decl);
            context.semanticInfoChain.setASTForDecl(decl, moduleDecl.stringLiteral);

            if (!moduleElementsHasExportAssignment(moduleDecl.moduleElements) || moduleContainsExecutableCode) {
                createModuleVariableDecl(decl, moduleDecl.stringLiteral, context);
            }

            context.pushParent(decl);
        } else {
            // Module has a name or dotted name.
            var moduleNames = TypeScript.ASTHelpers.getModuleNames(moduleDecl.name);
            for (var i = 0, n = moduleNames.length; i < n; i++) {
                var moduleName = moduleNames[i];

                // All the inner module decls are exported.
                var specificFlags = declFlags;
                if (i > 0) {
                    specificFlags |= 1 /* Exported */;
                }

                var decl = new TypeScript.NormalPullDecl(moduleName.valueText(), moduleName.text(), kind, specificFlags, context.getParent());

                //// The innermost moduleDecl maps to the entire ModuleDeclaration node.
                //// All the other ones map to the name node.  i.e. module A.B.C { }
                ////
                //// The decl for C points to the entire module declaration.  The decls for A and B
                //// will point at the A and B nodes respectively.
                //var ast = (i === (moduleName.length - 1))
                //    ? moduleDecl
                //    : moduleName;
                context.semanticInfoChain.setDeclForAST(moduleDecl, decl);
                context.semanticInfoChain.setDeclForAST(moduleName, decl);
                context.semanticInfoChain.setASTForDecl(decl, moduleName);

                if (moduleContainsExecutableCode) {
                    createModuleVariableDecl(decl, moduleName, context);
                }

                context.pushParent(decl);
            }
        }
    }

    function createModuleVariableDecl(decl, moduleNameAST, context) {
        decl.setFlags(decl.flags | getInitializationFlag(decl));

        // create the value decl
        var valueDecl = new TypeScript.NormalPullDecl(decl.name, decl.getDisplayName(), TypeScript.PullElementKind.Variable, decl.flags, context.getParent());
        decl.setValueDecl(valueDecl);
        context.semanticInfoChain.setASTForDecl(valueDecl, moduleNameAST);
    }

    function containsExecutableCode(members) {
        for (var i = 0, n = members.childCount(); i < n; i++) {
            var member = members.childAt(i);

            // October 11, 2013
            // Internal modules are either instantiated or non-instantiated. A non-instantiated
            // module is an internal module containing only interface types and other non -
            // instantiated modules.
            //
            // Note: small spec deviation.  We don't consider an import statement sufficient to
            // consider a module instantiated (except the case of 'export import' handled below ).
            // After all, if there is an import, but no actual code that references the imported value,
            // then there's no need to emit the import or the module.
            if (member.kind() === 130 /* ModuleDeclaration */) {
                var moduleDecl = member;

                // If we have a module in us, and it contains executable code, then we
                // contain executable code.
                if (containsExecutableCode(moduleDecl.moduleElements)) {
                    return true;
                }
            } else if (member.kind() === 133 /* ImportDeclaration */) {
                // pessimistically assume 'export import' declaration will be the alias to something instantiated
                // we cannot figure out it exactly until the resolution time.
                if (TypeScript.hasModifier(member.modifiers, 1 /* Exported */)) {
                    return true;
                }
            } else if (member.kind() !== 128 /* InterfaceDeclaration */ && member.kind() !== 134 /* ExportAssignment */) {
                // In case of export assignment we should be really checking meaning of Export assignment identifier, but thats TODO for later
                // If we contain anything that's not an interface declaration, then we contain
                // executable code.
                return true;
            }
        }

        return false;
    }

    function preCollectClassDecls(classDecl, context) {
        var declFlags = 0 /* None */;

        if ((TypeScript.hasModifier(classDecl.modifiers, 1 /* Exported */) || isParsingAmbientModule(classDecl, context)) && !containingModuleHasExportAssignment(classDecl)) {
            declFlags |= 1 /* Exported */;
        }

        if (TypeScript.hasModifier(classDecl.modifiers, TypeScript.PullElementFlags.Ambient) || isParsingAmbientModule(classDecl, context) || context.isDeclareFile) {
            declFlags |= TypeScript.PullElementFlags.Ambient;
        }

        var parent = context.getParent();

        var decl = new TypeScript.NormalPullDecl(classDecl.identifier.valueText(), classDecl.identifier.text(), TypeScript.PullElementKind.Class, declFlags, parent);

        var constructorDecl = new TypeScript.NormalPullDecl(classDecl.identifier.valueText(), classDecl.identifier.text(), TypeScript.PullElementKind.Variable, declFlags | TypeScript.PullElementFlags.ClassConstructorVariable, parent);

        decl.setValueDecl(constructorDecl);

        context.semanticInfoChain.setDeclForAST(classDecl, decl);
        context.semanticInfoChain.setASTForDecl(decl, classDecl);
        context.semanticInfoChain.setASTForDecl(constructorDecl, classDecl);

        context.pushParent(decl);
    }

    function preCollectObjectTypeDecls(objectType, context) {
        // if this is the 'body' of an interface declaration, then we don't want to create a decl
        // here.  We want the interface decl to be the parent decl of all the members we visit.
        if (objectType.parent.kind() === 128 /* InterfaceDeclaration */) {
            return;
        }

        var declFlags = 0 /* None */;

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl("", "", TypeScript.PullElementKind.ObjectType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(objectType, decl);
        context.semanticInfoChain.setASTForDecl(decl, objectType);

        context.pushParent(decl);
    }

    function preCollectInterfaceDecls(interfaceDecl, context) {
        var declFlags = 0 /* None */;

        if ((TypeScript.hasModifier(interfaceDecl.modifiers, 1 /* Exported */) || isParsingAmbientModule(interfaceDecl, context)) && !containingModuleHasExportAssignment(interfaceDecl)) {
            declFlags |= 1 /* Exported */;
        }

        var parent = context.getParent();

        var decl = new TypeScript.NormalPullDecl(interfaceDecl.identifier.valueText(), interfaceDecl.identifier.text(), TypeScript.PullElementKind.Interface, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(interfaceDecl, decl);
        context.semanticInfoChain.setASTForDecl(decl, interfaceDecl);

        context.pushParent(decl);
    }

    function preCollectParameterDecl(argDecl, context) {
        var declFlags = 0 /* None */;

        if (TypeScript.hasModifier(argDecl.modifiers, TypeScript.PullElementFlags.Private)) {
            declFlags |= TypeScript.PullElementFlags.Private;
        } else {
            declFlags |= TypeScript.PullElementFlags.Public;
        }

        if (argDecl.questionToken !== null || argDecl.equalsValueClause !== null || argDecl.dotDotDotToken !== null) {
            declFlags |= TypeScript.PullElementFlags.Optional;
        }

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl(argDecl.identifier.valueText(), argDecl.identifier.text(), TypeScript.PullElementKind.Parameter, declFlags, parent);

        // If it has a default arg, record the fact that the parent has default args (we will need this during resolution)
        if (argDecl.equalsValueClause) {
            parent.flags |= TypeScript.PullElementFlags.HasDefaultArgs;
        }

        if (parent.kind === TypeScript.PullElementKind.ConstructorMethod) {
            decl.setFlag(TypeScript.PullElementFlags.ConstructorParameter);
        }

        // if it's a property type, we'll need to add it to the parent's parent as well
        var isPublicOrPrivate = TypeScript.hasModifier(argDecl.modifiers, TypeScript.PullElementFlags.Public | TypeScript.PullElementFlags.Private);
        var isInConstructor = parent.kind === TypeScript.PullElementKind.ConstructorMethod;
        if (isPublicOrPrivate && isInConstructor) {
            var parentsParent = context.parentChain[context.parentChain.length - 2];

            // optional parameters don't introduce optional properties - always drop isOptional flag on the property declaration
            var propDeclFlags = declFlags & ~TypeScript.PullElementFlags.Optional;
            var propDecl = new TypeScript.NormalPullDecl(argDecl.identifier.valueText(), argDecl.identifier.text(), TypeScript.PullElementKind.Property, propDeclFlags, parentsParent);
            propDecl.setValueDecl(decl);
            decl.setFlag(TypeScript.PullElementFlags.PropertyParameter);
            propDecl.setFlag(TypeScript.PullElementFlags.PropertyParameter);

            if (parent.kind === TypeScript.PullElementKind.ConstructorMethod) {
                propDecl.setFlag(TypeScript.PullElementFlags.ConstructorParameter);
            }

            context.semanticInfoChain.setASTForDecl(decl, argDecl);
            context.semanticInfoChain.setASTForDecl(propDecl, argDecl);
            context.semanticInfoChain.setDeclForAST(argDecl, propDecl);
        } else {
            context.semanticInfoChain.setASTForDecl(decl, argDecl);
            context.semanticInfoChain.setDeclForAST(argDecl, decl);
        }

        // Record this decl in its parent in the declGroup with the corresponding name
        parent.addVariableDeclToGroup(decl);
        // Note: it is intentional that a parameter does not get added to hte context stack.  A
        // parameter does not introduce a new name scope, so it shouldn't be in the context decl stack.
        // context.pushParent(decl);
    }

    function preCollectTypeParameterDecl(typeParameterDecl, context) {
        var declFlags = 0 /* None */;

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl(typeParameterDecl.identifier.valueText(), typeParameterDecl.identifier.text(), TypeScript.PullElementKind.TypeParameter, declFlags, parent);
        context.semanticInfoChain.setASTForDecl(decl, typeParameterDecl);
        context.semanticInfoChain.setDeclForAST(typeParameterDecl, decl);
        // Note: it is intentional that a type parameter does not get added to hte context stack.
        // A type parameter does not introduce a new name scope, so it shouldn't be in the
        // context decl stack.
        // context.pushParent(decl);
    }

    // interface properties
    function createPropertySignature(propertyDecl, context) {
        var declFlags = TypeScript.PullElementFlags.Public;
        var parent = context.getParent();
        var declType = TypeScript.PullElementKind.Property;

        if (propertyDecl.questionToken !== null) {
            declFlags |= TypeScript.PullElementFlags.Optional;
        }

        var decl = new TypeScript.NormalPullDecl(propertyDecl.propertyName.valueText(), propertyDecl.propertyName.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(propertyDecl, decl);
        context.semanticInfoChain.setASTForDecl(decl, propertyDecl);
        // Note: it is intentional that a var decl does not get added to hte context stack.  A var
        // decl does not introduce a new name scope, so it shouldn't be in the context decl stack.
        // context.pushParent(decl);
    }

    // class member variables
    function createMemberVariableDeclaration(memberDecl, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.Property;

        if (TypeScript.hasModifier(memberDecl.modifiers, TypeScript.PullElementFlags.Private)) {
            declFlags |= TypeScript.PullElementFlags.Private;
        } else {
            declFlags |= TypeScript.PullElementFlags.Public;
        }

        if (TypeScript.hasModifier(memberDecl.modifiers, TypeScript.PullElementFlags.Static)) {
            declFlags |= TypeScript.PullElementFlags.Static;
        }

        var parent = context.getParent();

        var decl = new TypeScript.NormalPullDecl(memberDecl.variableDeclarator.propertyName.valueText(), memberDecl.variableDeclarator.propertyName.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(memberDecl, decl);
        context.semanticInfoChain.setDeclForAST(memberDecl.variableDeclarator, decl);
        context.semanticInfoChain.setASTForDecl(decl, memberDecl);
        // Note: it is intentional that a var decl does not get added to hte context stack.  A var
        // decl does not introduce a new name scope, so it shouldn't be in the context decl stack.
        // context.pushParent(decl);
    }

    function createVariableDeclaration(varDecl, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.Variable;

        var modifiers = TypeScript.ASTHelpers.getVariableDeclaratorModifiers(varDecl);
        if ((TypeScript.hasModifier(modifiers, 1 /* Exported */) || isParsingAmbientModule(varDecl, context)) && !containingModuleHasExportAssignment(varDecl)) {
            declFlags |= 1 /* Exported */;
        }

        if (TypeScript.hasModifier(modifiers, TypeScript.PullElementFlags.Ambient) || isParsingAmbientModule(varDecl, context) || context.isDeclareFile) {
            declFlags |= TypeScript.PullElementFlags.Ambient;
        }

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl(varDecl.propertyName.valueText(), varDecl.propertyName.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(varDecl, decl);
        context.semanticInfoChain.setASTForDecl(decl, varDecl);

        if (parent) {
            // Record this decl in its parent in the declGroup with the corresponding name
            parent.addVariableDeclToGroup(decl);
        }
        // Note: it is intentional that a var decl does not get added to hte context stack.  A var
        // decl does not introduce a new name scope, so it shouldn't be in the context decl stack.
        // context.pushParent(decl);
    }

    function preCollectVarDecls(ast, context) {
        if (ast.parent.kind() === 136 /* MemberVariableDeclaration */) {
            // Already handled this node.
            return;
        }

        var varDecl = ast;
        createVariableDeclaration(varDecl, context);
    }

    // function type expressions
    function createFunctionTypeDeclaration(functionTypeDeclAST, context) {
        var declFlags = TypeScript.PullElementFlags.Signature;
        var declType = TypeScript.PullElementKind.FunctionType;

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl("", "", declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(functionTypeDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, functionTypeDeclAST);

        context.pushParent(decl);
    }

    // constructor types
    function createConstructorTypeDeclaration(constructorTypeDeclAST, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.ConstructorType;

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl("", "", declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(constructorTypeDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, constructorTypeDeclAST);

        context.pushParent(decl);
    }

    // function declaration
    function createFunctionDeclaration(funcDeclAST, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.Function;

        if ((TypeScript.hasModifier(funcDeclAST.modifiers, 1 /* Exported */) || isParsingAmbientModule(funcDeclAST, context)) && !containingModuleHasExportAssignment(funcDeclAST)) {
            declFlags |= 1 /* Exported */;
        }

        if (TypeScript.hasModifier(funcDeclAST.modifiers, TypeScript.PullElementFlags.Ambient) || isParsingAmbientModule(funcDeclAST, context) || context.isDeclareFile) {
            declFlags |= TypeScript.PullElementFlags.Ambient;
        }

        if (!funcDeclAST.block) {
            declFlags |= TypeScript.PullElementFlags.Signature;
        }

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl(funcDeclAST.identifier.valueText(), funcDeclAST.identifier.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(funcDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, funcDeclAST);

        context.pushParent(decl);
    }

    // function expression
    function createAnyFunctionExpressionDeclaration(functionExpressionDeclAST, id, context, displayName) {
        if (typeof displayName === "undefined") { displayName = null; }
        var declFlags = 0 /* None */;

        if (functionExpressionDeclAST.kind() === 219 /* SimpleArrowFunctionExpression */ || functionExpressionDeclAST.kind() === 218 /* ParenthesizedArrowFunctionExpression */) {
            declFlags |= TypeScript.PullElementFlags.ArrowFunction;
        }

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var name = id ? id.text() : "";
        var displayNameText = displayName ? displayName.text() : "";
        var decl = new TypeScript.PullFunctionExpressionDecl(name, declFlags, parent, displayNameText);
        context.semanticInfoChain.setDeclForAST(functionExpressionDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, functionExpressionDeclAST);

        context.pushParent(decl);

        if (functionExpressionDeclAST.kind() === 219 /* SimpleArrowFunctionExpression */) {
            var simpleArrow = functionExpressionDeclAST;
            var declFlags = TypeScript.PullElementFlags.Public;

            var parent = context.getParent();

            if (TypeScript.hasFlag(parent.flags, TypeScript.PullElementFlags.DeclaredInAWithBlock)) {
                declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
            }

            var decl = new TypeScript.NormalPullDecl(simpleArrow.identifier.valueText(), simpleArrow.identifier.text(), TypeScript.PullElementKind.Parameter, declFlags, parent);

            context.semanticInfoChain.setASTForDecl(decl, simpleArrow.identifier);
            context.semanticInfoChain.setDeclForAST(simpleArrow.identifier, decl);

            // Record this decl in its parent in the declGroup with the corresponding name
            parent.addVariableDeclToGroup(decl);
        }
    }

    function createMemberFunctionDeclaration(funcDecl, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.Method;

        if (TypeScript.hasModifier(funcDecl.modifiers, TypeScript.PullElementFlags.Static)) {
            declFlags |= TypeScript.PullElementFlags.Static;
        }

        if (TypeScript.hasModifier(funcDecl.modifiers, TypeScript.PullElementFlags.Private)) {
            declFlags |= TypeScript.PullElementFlags.Private;
        } else {
            declFlags |= TypeScript.PullElementFlags.Public;
        }

        if (!funcDecl.block) {
            declFlags |= TypeScript.PullElementFlags.Signature;
        }

        var parent = context.getParent();

        var decl = new TypeScript.NormalPullDecl(funcDecl.propertyName.valueText(), funcDecl.propertyName.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(funcDecl, decl);
        context.semanticInfoChain.setASTForDecl(decl, funcDecl);

        context.pushParent(decl);
    }

    // index signatures
    function createIndexSignatureDeclaration(indexSignatureDeclAST, context) {
        var declFlags = TypeScript.PullElementFlags.Signature;
        var declType = TypeScript.PullElementKind.IndexSignature;

        var parent = context.getParent();

        var decl = new TypeScript.NormalPullDecl("", "", declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(indexSignatureDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, indexSignatureDeclAST);

        context.pushParent(decl);
    }

    // call signatures
    function createCallSignatureDeclaration(callSignature, context) {
        var isChildOfObjectType = callSignature.parent && callSignature.parent.parent && callSignature.parent.kind() === 2 /* SeparatedList */ && callSignature.parent.parent.kind() === 122 /* ObjectType */;

        if (!isChildOfObjectType) {
            // This was a call signature that was part of some other entity (like a function
            // declaration or construct signature).  Those are already handled specially and
            // we don't want to end up making another call signature for them.  We only want
            // to make an actual call signature if we're a standalone call signature in an
            // object/interface type.
            return;
        }

        var declFlags = TypeScript.PullElementFlags.Signature;
        var declType = TypeScript.PullElementKind.CallSignature;

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl("", "", declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(callSignature, decl);
        context.semanticInfoChain.setASTForDecl(decl, callSignature);

        context.pushParent(decl);
    }

    function createMethodSignatureDeclaration(method, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.Method;

        declFlags |= TypeScript.PullElementFlags.Public;
        declFlags |= TypeScript.PullElementFlags.Signature;

        if (method.questionToken !== null) {
            declFlags |= TypeScript.PullElementFlags.Optional;
        }

        var parent = context.getParent();

        var decl = new TypeScript.NormalPullDecl(method.propertyName.valueText(), method.propertyName.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(method, decl);
        context.semanticInfoChain.setASTForDecl(decl, method);

        context.pushParent(decl);
    }

    // construct signatures
    function createConstructSignatureDeclaration(constructSignatureDeclAST, context) {
        var declFlags = TypeScript.PullElementFlags.Signature;
        var declType = TypeScript.PullElementKind.ConstructSignature;

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl("", "", declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(constructSignatureDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, constructSignatureDeclAST);

        context.pushParent(decl);
    }

    // class constructors
    function createClassConstructorDeclaration(constructorDeclAST, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.ConstructorMethod;

        if (!constructorDeclAST.block) {
            declFlags |= TypeScript.PullElementFlags.Signature;
        }

        var parent = context.getParent();

        if (parent) {
            // if the parent is exported, the constructor decl must be as well
            var parentFlags = parent.flags;

            if (parentFlags & 1 /* Exported */) {
                declFlags |= 1 /* Exported */;
            }
        }

        var decl = new TypeScript.NormalPullDecl(parent.name, parent.getDisplayName(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(constructorDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, constructorDeclAST);

        context.pushParent(decl);
    }

    function createGetAccessorDeclaration(getAccessorDeclAST, context) {
        var declFlags = TypeScript.PullElementFlags.Public;
        var declType = TypeScript.PullElementKind.GetAccessor;

        if (TypeScript.hasModifier(getAccessorDeclAST.modifiers, TypeScript.PullElementFlags.Static)) {
            declFlags |= TypeScript.PullElementFlags.Static;
        }

        if (TypeScript.hasModifier(getAccessorDeclAST.modifiers, TypeScript.PullElementFlags.Private)) {
            declFlags |= TypeScript.PullElementFlags.Private;
        } else {
            declFlags |= TypeScript.PullElementFlags.Public;
        }

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl(getAccessorDeclAST.propertyName.valueText(), getAccessorDeclAST.propertyName.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(getAccessorDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, getAccessorDeclAST);

        context.pushParent(decl);
    }

    function createFunctionExpressionDeclaration(expression, context) {
        createAnyFunctionExpressionDeclaration(expression, expression.identifier, context);
    }

    function createSetAccessorDeclaration(setAccessorDeclAST, context) {
        var declFlags = TypeScript.PullElementFlags.Public;
        var declType = TypeScript.PullElementKind.SetAccessor;

        if (TypeScript.hasModifier(setAccessorDeclAST.modifiers, TypeScript.PullElementFlags.Static)) {
            declFlags |= TypeScript.PullElementFlags.Static;
        }

        if (TypeScript.hasModifier(setAccessorDeclAST.modifiers, TypeScript.PullElementFlags.Private)) {
            declFlags |= TypeScript.PullElementFlags.Private;
        } else {
            declFlags |= TypeScript.PullElementFlags.Public;
        }

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl(setAccessorDeclAST.propertyName.valueText(), setAccessorDeclAST.propertyName.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(setAccessorDeclAST, decl);
        context.semanticInfoChain.setASTForDecl(decl, setAccessorDeclAST);

        context.pushParent(decl);
    }

    function preCollectCatchDecls(ast, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.CatchBlock;

        var parent = context.getParent();

        if (parent && (parent.kind === TypeScript.PullElementKind.WithBlock || (parent.flags & TypeScript.PullElementFlags.DeclaredInAWithBlock))) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl("", "", declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(ast, decl);
        context.semanticInfoChain.setASTForDecl(decl, ast);

        context.pushParent(decl);

        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.CatchVariable;

        // Create a decl for the catch clause variable.
        var parent = context.getParent();

        if (TypeScript.hasFlag(parent.flags, TypeScript.PullElementFlags.DeclaredInAWithBlock)) {
            declFlags |= TypeScript.PullElementFlags.DeclaredInAWithBlock;
        }

        var decl = new TypeScript.NormalPullDecl(ast.identifier.valueText(), ast.identifier.text(), declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(ast.identifier, decl);
        context.semanticInfoChain.setASTForDecl(decl, ast.identifier);

        if (parent) {
            // Record this decl in its parent in the declGroup with the corresponding name
            parent.addVariableDeclToGroup(decl);
        }
    }

    function preCollectWithDecls(ast, context) {
        var declFlags = 0 /* None */;
        var declType = TypeScript.PullElementKind.WithBlock;

        var parent = context.getParent();

        var decl = new TypeScript.NormalPullDecl("", "", declType, declFlags, parent);
        context.semanticInfoChain.setDeclForAST(ast, decl);
        context.semanticInfoChain.setASTForDecl(decl, ast);

        context.pushParent(decl);
    }

    function preCollectObjectLiteralDecls(ast, context) {
        var decl = new TypeScript.NormalPullDecl("", "", TypeScript.PullElementKind.ObjectLiteral, 0 /* None */, context.getParent());

        context.semanticInfoChain.setDeclForAST(ast, decl);
        context.semanticInfoChain.setASTForDecl(decl, ast);

        context.pushParent(decl);
    }

    function preCollectSimplePropertyAssignmentDecls(propertyAssignment, context) {
        var assignmentText = TypeScript.getPropertyAssignmentNameTextFromIdentifier(propertyAssignment.propertyName);
        var span = TypeScript.TextSpan.fromBounds(propertyAssignment.start(), propertyAssignment.end());

        var decl = new TypeScript.NormalPullDecl(assignmentText.memberName, assignmentText.actualText, TypeScript.PullElementKind.Property, TypeScript.PullElementFlags.Public, context.getParent());

        context.semanticInfoChain.setDeclForAST(propertyAssignment, decl);
        context.semanticInfoChain.setASTForDecl(decl, propertyAssignment);
        // Note: it is intentional that a property assignment does not get added to hte context
        // stack.  A prop assignment does not introduce a new name scope, so it shouldn't be in
        // the context decl stack.
        // context.pushParent(decl);
    }

    function preCollectFunctionPropertyAssignmentDecls(propertyAssignment, context) {
        var assignmentText = TypeScript.getPropertyAssignmentNameTextFromIdentifier(propertyAssignment.propertyName);

        var decl = new TypeScript.NormalPullDecl(assignmentText.memberName, assignmentText.actualText, TypeScript.PullElementKind.Property, TypeScript.PullElementFlags.Public, context.getParent());

        context.semanticInfoChain.setDeclForAST(propertyAssignment, decl);
        context.semanticInfoChain.setASTForDecl(decl, propertyAssignment);

        createAnyFunctionExpressionDeclaration(propertyAssignment, propertyAssignment.propertyName, context, propertyAssignment.propertyName);
    }

    function preCollectDecls(ast, context) {
        switch (ast.kind()) {
            case 120 /* SourceUnit */:
                preCollectScriptDecls(ast, context);
                break;
            case 132 /* EnumDeclaration */:
                preCollectEnumDecls(ast, context);
                break;
            case 243 /* EnumElement */:
                createEnumElementDecls(ast, context);
                break;
            case 130 /* ModuleDeclaration */:
                preCollectModuleDecls(ast, context);
                break;
            case 131 /* ClassDeclaration */:
                preCollectClassDecls(ast, context);
                break;
            case 128 /* InterfaceDeclaration */:
                preCollectInterfaceDecls(ast, context);
                break;
            case 122 /* ObjectType */:
                preCollectObjectTypeDecls(ast, context);
                break;
            case 242 /* Parameter */:
                preCollectParameterDecl(ast, context);
                break;
            case 136 /* MemberVariableDeclaration */:
                createMemberVariableDeclaration(ast, context);
                break;
            case 141 /* PropertySignature */:
                createPropertySignature(ast, context);
                break;
            case 225 /* VariableDeclarator */:
                preCollectVarDecls(ast, context);
                break;
            case 137 /* ConstructorDeclaration */:
                createClassConstructorDeclaration(ast, context);
                break;
            case 139 /* GetAccessor */:
                createGetAccessorDeclaration(ast, context);
                break;
            case 140 /* SetAccessor */:
                createSetAccessorDeclaration(ast, context);
                break;
            case 222 /* FunctionExpression */:
                createFunctionExpressionDeclaration(ast, context);
                break;
            case 135 /* MemberFunctionDeclaration */:
                createMemberFunctionDeclaration(ast, context);
                break;
            case 144 /* IndexSignature */:
                createIndexSignatureDeclaration(ast, context);
                break;
            case 123 /* FunctionType */:
                createFunctionTypeDeclaration(ast, context);
                break;
            case 125 /* ConstructorType */:
                createConstructorTypeDeclaration(ast, context);
                break;
            case 142 /* CallSignature */:
                createCallSignatureDeclaration(ast, context);
                break;
            case 143 /* ConstructSignature */:
                createConstructSignatureDeclaration(ast, context);
                break;
            case 145 /* MethodSignature */:
                createMethodSignatureDeclaration(ast, context);
                break;
            case 129 /* FunctionDeclaration */:
                createFunctionDeclaration(ast, context);
                break;
            case 219 /* SimpleArrowFunctionExpression */:
            case 218 /* ParenthesizedArrowFunctionExpression */:
                createAnyFunctionExpressionDeclaration(ast, null, context);
                break;
            case 133 /* ImportDeclaration */:
                preCollectImportDecls(ast, context);
                break;
            case 238 /* TypeParameter */:
                preCollectTypeParameterDecl(ast, context);
                break;
            case 236 /* CatchClause */:
                preCollectCatchDecls(ast, context);
                break;
            case 163 /* WithStatement */:
                preCollectWithDecls(ast, context);
                break;
            case 215 /* ObjectLiteralExpression */:
                preCollectObjectLiteralDecls(ast, context);
                break;
            case 240 /* SimplePropertyAssignment */:
                preCollectSimplePropertyAssignmentDecls(ast, context);
                break;
            case 241 /* FunctionPropertyAssignment */:
                preCollectFunctionPropertyAssignmentDecls(ast, context);
                break;
        }
    }

    function isContainer(decl) {
        return decl.kind === TypeScript.PullElementKind.Container || decl.kind === TypeScript.PullElementKind.DynamicModule || decl.kind === TypeScript.PullElementKind.Enum;
    }

    function getInitializationFlag(decl) {
        if (decl.kind & TypeScript.PullElementKind.Container) {
            return TypeScript.PullElementFlags.InitializedModule;
        } else if (decl.kind & TypeScript.PullElementKind.DynamicModule) {
            return TypeScript.PullElementFlags.InitializedDynamicModule;
        }

        return 0 /* None */;
    }

    function hasInitializationFlag(decl) {
        var kind = decl.kind;

        if (kind & TypeScript.PullElementKind.Container) {
            return (decl.flags & TypeScript.PullElementFlags.InitializedModule) !== 0;
        } else if (kind & TypeScript.PullElementKind.DynamicModule) {
            return (decl.flags & TypeScript.PullElementFlags.InitializedDynamicModule) !== 0;
        }

        return false;
    }

    function postCollectDecls(ast, context) {
        var currentDecl = context.getParent();

        // We only want to pop the module decls when we're done with the module itself, and not
        // when we are done with the module names.
        if (ast.kind() === 11 /* IdentifierName */ || ast.kind() === 14 /* StringLiteral */) {
            if (currentDecl.kind === TypeScript.PullElementKind.Container || currentDecl.kind === TypeScript.PullElementKind.DynamicModule) {
                return;
            }
        }

        if (ast.kind() === 130 /* ModuleDeclaration */) {
            var moduleDeclaration = ast;
            if (moduleDeclaration.stringLiteral) {
                TypeScript.Debug.assert(currentDecl.ast() === moduleDeclaration.stringLiteral);
                context.popParent();
            } else {
                var moduleNames = TypeScript.ASTHelpers.getModuleNames(moduleDeclaration.name);
                for (var i = moduleNames.length - 1; i >= 0; i--) {
                    var moduleName = moduleNames[i];
                    TypeScript.Debug.assert(currentDecl.ast() === moduleName);
                    context.popParent();
                    currentDecl = context.getParent();
                }
            }
        }

        if (ast.kind() === 132 /* EnumDeclaration */) {
            // Now that we've created all the child decls for the enum elements, determine what
            // (if any) their constant values should be.
            computeEnumElementConstantValues(ast, currentDecl, context);
        }

        while (currentDecl.getParentDecl() && currentDecl.ast() === ast) {
            context.popParent();
            currentDecl = context.getParent();
        }
    }

    function computeEnumElementConstantValues(ast, enumDecl, context) {
        TypeScript.Debug.assert(enumDecl.kind === TypeScript.PullElementKind.Enum);

        // If this is a non ambient enum, then it starts with a constant section of enum elements.
        // Thus, elements without an initializer in these sections will be assumed to have a
        // constant value.
        //
        // However, if this an enum in an ambient context, then non initialized elements are
        // thought to have a computed value and are not in a constant section.
        var isAmbientEnum = TypeScript.hasFlag(enumDecl.flags, TypeScript.PullElementFlags.Ambient);
        var inConstantSection = !isAmbientEnum;
        var currentConstantValue = 0;
        var enumMemberDecls = enumDecl.getChildDecls();

        for (var i = 0, n = ast.enumElements.nonSeparatorCount(); i < n; i++) {
            var enumElement = ast.enumElements.nonSeparatorAt(i);
            var enumElementDecl = TypeScript.ArrayUtilities.first(enumMemberDecls, function (d) {
                return context.semanticInfoChain.getASTForDecl(d) === enumElement;
            });

            TypeScript.Debug.assert(enumElementDecl.kind === TypeScript.PullElementKind.EnumMember);

            if (enumElement.equalsValueClause === null) {
                // Didn't have an initializer.  If we're in a constant section, then this appears
                // to have the value of the current constant.  If we're in a non-constant section
                // then this gets no value.
                if (inConstantSection) {
                    enumElementDecl.constantValue = currentConstantValue;
                    currentConstantValue++;
                }
            } else {
                // Enum element had an initializer.  If it's a constant, then then enum gets that
                // value, and we transition to (or stay in) a constant section (as long as we're
                // not in an ambient context.
                //
                // If it's not a constant, then we transition to a non-constant section.
                enumElementDecl.constantValue = computeEnumElementConstantValue(enumElement.equalsValueClause.value, enumMemberDecls, context);
                if (enumElementDecl.constantValue !== null && !isAmbientEnum) {
                    // This enum element had a constant value.  We're now in a constant section.
                    // Any successive enum elements should get their values from this constant
                    // value onwards.
                    inConstantSection = true;
                    currentConstantValue = enumElementDecl.constantValue + 1;
                } else {
                    // Didn't get a constant value.  We're not in a constant section.
                    inConstantSection = false;
                }
            }

            TypeScript.Debug.assert(enumElementDecl.constantValue !== undefined);
        }
    }

    function computeEnumElementConstantValue(expression, enumMemberDecls, context) {
        TypeScript.Debug.assert(expression);

        if (TypeScript.ASTHelpers.isIntegerLiteralAST(expression)) {
            // Always produce a value for an integer literal.
            var token;
            switch (expression.kind()) {
                case 164 /* PlusExpression */:
                case 165 /* NegateExpression */:
                    token = expression.operand;
                    break;
                default:
                    token = expression;
            }

            var value = token.value();
            return value && expression.kind() === 165 /* NegateExpression */ ? -value : value;
        } else if (context.propagateEnumConstants) {
            switch (expression.kind()) {
                case 11 /* IdentifierName */:
                    // If it's a name, see if we already had an enum value named this.  If so,
                    // return that value.  Note, only search backward in the enum for a match.
                    var name = expression;
                    var matchingEnumElement = TypeScript.ArrayUtilities.firstOrDefault(enumMemberDecls, function (d) {
                        return d.name === name.valueText();
                    });

                    return matchingEnumElement ? matchingEnumElement.constantValue : null;

                case 202 /* LeftShiftExpression */:
                    // Handle the common case of a left shifted value.
                    var binaryExpression = expression;
                    var left = computeEnumElementConstantValue(binaryExpression.left, enumMemberDecls, context);
                    var right = computeEnumElementConstantValue(binaryExpression.right, enumMemberDecls, context);
                    if (left === null || right === null) {
                        return null;
                    }

                    return left << right;

                case 189 /* BitwiseOrExpression */:
                    // Handle the common case of an or'ed value.
                    var binaryExpression = expression;
                    var left = computeEnumElementConstantValue(binaryExpression.left, enumMemberDecls, context);
                    var right = computeEnumElementConstantValue(binaryExpression.right, enumMemberDecls, context);
                    if (left === null || right === null) {
                        return null;
                    }

                    return left | right;
            }

            // TODO: add more cases.
            return null;
        } else {
            // Wasn't an integer literal, and we're not aggressively propagating constants.
            // There is no constant value for this expression.
            return null;
        }
    }

    (function (DeclarationCreator) {
        function create(document, semanticInfoChain, compilationSettings) {
            var declCollectionContext = new DeclCollectionContext(document, semanticInfoChain, compilationSettings.propagateEnumConstants());

            // create decls
            TypeScript.getAstWalkerFactory().simpleWalk(document.sourceUnit(), preCollectDecls, postCollectDecls, declCollectionContext);

            return declCollectionContext.getParent();
        }
        DeclarationCreator.create = create;
    })(TypeScript.DeclarationCreator || (TypeScript.DeclarationCreator = {}));
    var DeclarationCreator = TypeScript.DeclarationCreator;
})(TypeScript || (TypeScript = {}));
