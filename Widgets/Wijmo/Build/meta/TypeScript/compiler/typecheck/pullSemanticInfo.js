// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    // PULLTODO: Get rid of these
    TypeScript.declCacheHit = 0;
    TypeScript.declCacheMiss = 0;
    TypeScript.symbolCacheHit = 0;
    TypeScript.symbolCacheMiss = 0;

    var sentinalEmptyArray = [];

    var SemanticInfoChain = (function () {
        function SemanticInfoChain(compiler, logger) {
            this.compiler = compiler;
            this.logger = logger;
            this.documents = [];
            this.fileNameToDocument = TypeScript.createIntrinsicsObject();
            this.anyTypeDecl = null;
            this.booleanTypeDecl = null;
            this.numberTypeDecl = null;
            this.stringTypeDecl = null;
            this.nullTypeDecl = null;
            this.undefinedTypeDecl = null;
            this.voidTypeDecl = null;
            this.undefinedValueDecl = null;
            this.anyTypeSymbol = null;
            this.booleanTypeSymbol = null;
            this.numberTypeSymbol = null;
            this.stringTypeSymbol = null;
            this.nullTypeSymbol = null;
            this.undefinedTypeSymbol = null;
            this.voidTypeSymbol = null;
            this.undefinedValueSymbol = null;
            this.emptyTypeSymbol = null;
            // <-- Data to clear when we get invalidated
            this.astSymbolMap = [];
            this.astAliasSymbolMap = [];
            this.astCallResolutionDataMap = [];
            this.declSymbolMap = [];
            this.declSignatureSymbolMap = [];
            this.declCache = null;
            this.symbolCache = null;
            this.fileNameToDiagnostics = null;
            this._binder = null;
            this._resolver = null;
            this._topLevelDecls = null;
            this._fileNames = null;
            var globalDecl = new TypeScript.RootPullDecl("", "", 0 /* Global */, 0 /* None */, this, false);
            this.documents[0] = new TypeScript.Document(this.compiler, this, "", [], null, 0 /* None */, /*version:*/ 0, false, null, globalDecl);

            this.anyTypeDecl = new TypeScript.NormalPullDecl("any", "any", TypeScript.PullElementKind.Primitive, 0 /* None */, globalDecl);
            this.booleanTypeDecl = new TypeScript.NormalPullDecl("boolean", "boolean", TypeScript.PullElementKind.Primitive, 0 /* None */, globalDecl);
            this.numberTypeDecl = new TypeScript.NormalPullDecl("number", "number", TypeScript.PullElementKind.Primitive, 0 /* None */, globalDecl);
            this.stringTypeDecl = new TypeScript.NormalPullDecl("string", "string", TypeScript.PullElementKind.Primitive, 0 /* None */, globalDecl);
            this.voidTypeDecl = new TypeScript.NormalPullDecl("void", "void", TypeScript.PullElementKind.Primitive, 0 /* None */, globalDecl);

            // add the global primitive values for "null" and "undefined"
            // Because you cannot reference them by name, they're not parented by any actual decl.
            this.nullTypeDecl = new TypeScript.RootPullDecl("null", "", TypeScript.PullElementKind.Primitive, 0 /* None */, this, false);
            this.undefinedTypeDecl = new TypeScript.RootPullDecl("undefined", "", TypeScript.PullElementKind.Primitive, 0 /* None */, this, false);
            this.undefinedValueDecl = new TypeScript.NormalPullDecl("undefined", "undefined", TypeScript.PullElementKind.Variable, TypeScript.PullElementFlags.Ambient, globalDecl);

            this.invalidate();
        }
        SemanticInfoChain.prototype.getDocument = function (fileName) {
            var document = this.fileNameToDocument[fileName];
            return document || null;
        };

        SemanticInfoChain.prototype.lineMap = function (fileName) {
            return this.getDocument(fileName).lineMap();
        };

        // Returns the names of the files we own, in the same order that they were added to us.
        SemanticInfoChain.prototype.fileNames = function () {
            if (this._fileNames === null) {
                // Skip the first semantic info (the synthesized one for the global decls).
                this._fileNames = this.documents.slice(1).map(function (s) {
                    return s.fileName;
                });
            }

            return this._fileNames;
        };

        // Must pass in a new decl, or an old symbol that has a decl available for ownership transfer
        SemanticInfoChain.prototype.bindPrimitiveSymbol = function (decl, newSymbol) {
            newSymbol.addDeclaration(decl);
            decl.setSymbol(newSymbol);
            newSymbol.setResolved();

            return newSymbol;
        };

        // Creates a new type symbol to be bound to this decl. Must be called during
        // invalidation after every edit.
        SemanticInfoChain.prototype.addPrimitiveTypeSymbol = function (decl) {
            var newSymbol = new TypeScript.PullPrimitiveTypeSymbol(decl.name);
            return this.bindPrimitiveSymbol(decl, newSymbol);
        };

        // Creates a new value symbol to be bound to this decl, and has the specified type.
        // Must be called during invalidation after every edit.
        SemanticInfoChain.prototype.addPrimitiveValueSymbol = function (decl, type) {
            var newSymbol = new TypeScript.PullSymbol(decl.name, TypeScript.PullElementKind.Variable);
            newSymbol.type = type;
            return this.bindPrimitiveSymbol(decl, newSymbol);
        };

        SemanticInfoChain.prototype.resetGlobalSymbols = function () {
            // add primitive types
            this.anyTypeSymbol = this.addPrimitiveTypeSymbol(this.anyTypeDecl);
            this.booleanTypeSymbol = this.addPrimitiveTypeSymbol(this.booleanTypeDecl);
            this.numberTypeSymbol = this.addPrimitiveTypeSymbol(this.numberTypeDecl);
            this.stringTypeSymbol = this.addPrimitiveTypeSymbol(this.stringTypeDecl);
            this.voidTypeSymbol = this.addPrimitiveTypeSymbol(this.voidTypeDecl);
            this.nullTypeSymbol = this.addPrimitiveTypeSymbol(this.nullTypeDecl);
            this.undefinedTypeSymbol = this.addPrimitiveTypeSymbol(this.undefinedTypeDecl);
            this.undefinedValueSymbol = this.addPrimitiveValueSymbol(this.undefinedValueDecl, this.undefinedTypeSymbol);

            // other decls not reachable from the globalDecl
            var emptyTypeDecl = new TypeScript.PullSynthesizedDecl("{}", "{}", TypeScript.PullElementKind.ObjectType, 0 /* None */, null, this);
            var emptyTypeSymbol = new TypeScript.PullTypeSymbol("{}", TypeScript.PullElementKind.ObjectType);
            emptyTypeDecl.setSymbol(emptyTypeSymbol);
            emptyTypeSymbol.addDeclaration(emptyTypeDecl);
            emptyTypeSymbol.setResolved();
            this.emptyTypeSymbol = emptyTypeSymbol;
        };

        SemanticInfoChain.prototype.addDocument = function (document) {
            var fileName = document.fileName;

            var existingIndex = TypeScript.ArrayUtilities.indexOf(this.documents, function (u) {
                return u.fileName === fileName;
            });
            if (existingIndex < 0) {
                // Adding the script for the first time.
                this.documents.push(document);
            } else {
                this.documents[existingIndex] = document;
            }

            this.fileNameToDocument[fileName] = document;

            // We changed the scripts we're responsible for.  Invalidate all existing cached
            // semantic information.
            this.invalidate();
        };

        SemanticInfoChain.prototype.removeDocument = function (fileName) {
            TypeScript.Debug.assert(fileName !== "", "Can't remove the semantic info for the global decl.");
            var index = TypeScript.ArrayUtilities.indexOf(this.documents, function (u) {
                return u.fileName === fileName;
            });
            if (index > 0) {
                this.fileNameToDocument[fileName] = undefined;
                this.documents.splice(index, 1);
                this.invalidate();
            }
        };

        SemanticInfoChain.prototype.getDeclPathCacheID = function (declPath, declKind) {
            var cacheID = "";

            for (var i = 0; i < declPath.length; i++) {
                cacheID += "#" + declPath[i];
            }

            return cacheID + "#" + declKind.toString();
        };

        // Looks for a top level decl matching the name/kind pair passed in.  This should be used
        // by methods in the binder to see if there is an existing symbol that a declaration should
        // merge into, or if the declaration should create a new symbol.
        //
        // The doNotGoPastThisDecl argument is important.  it should be the declaration that the
        // binder is currently creating a symbol for.  The chain will search itself from first to
        // last semantic info, and will not go past the file that that decl is declared in.
        // Furthermore, while searching hte file that that decl is declared in, it will also not
        // consider any decls at a later position in the file.
        //
        // In this manner, it will only find symbols declared 'before' the decl currently being
        // bound.  This gives us a nice ordering guarantee for open ended symbols.  Specifically
        // we'll create a symbol for the first file (in compiler order) that it was found it,
        // and we'll merge all later declarations into that symbol.  This means, for example, that
        // if a consumer tries to augment a lib.d.ts type, that the symbol will be created for
        // lib.d.ts (as that is in the chain prior to all user files).
        SemanticInfoChain.prototype.findTopLevelSymbol = function (name, kind, doNotGoPastThisDecl) {
            var cacheID = this.getDeclPathCacheID([name], kind);

            var symbol = this.symbolCache[cacheID];

            if (!symbol) {
                for (var i = 0, n = this.documents.length; i < n; i++) {
                    var topLevelDecl = this.documents[i].topLevelDecl();

                    var symbol = this.findTopLevelSymbolInDecl(topLevelDecl, name, kind, doNotGoPastThisDecl);
                    if (symbol) {
                        break;
                    }

                    // We finished searching up to the file that included the stopping point decl.
                    // no need to continue.
                    if (doNotGoPastThisDecl && topLevelDecl.name === doNotGoPastThisDecl.fileName()) {
                        return null;
                    }
                }

                if (symbol) {
                    this.symbolCache[cacheID] = symbol;
                }
            }

            return symbol;
        };

        SemanticInfoChain.prototype.findTopLevelSymbolInDecl = function (topLevelDecl, name, kind, doNotGoPastThisDecl) {
            // If we're currently searching the file that includes the decl we don't want to go
            // past, then we have to stop searching at the position of that decl.  Otherwise, we
            // search the entire file.
            var doNotGoPastThisPosition = doNotGoPastThisDecl && doNotGoPastThisDecl.fileName() === topLevelDecl.fileName() ? doNotGoPastThisDecl.ast().start() : -1;

            var foundDecls = topLevelDecl.searchChildDecls(name, kind);

            for (var j = 0; j < foundDecls.length; j++) {
                var foundDecl = foundDecls[j];

                // This decl was at or past the stopping point.  Don't search any further.
                if (doNotGoPastThisPosition !== -1 && foundDecl.ast() && foundDecl.ast().start() > doNotGoPastThisPosition) {
                    break;
                }

                var symbol = foundDecls[j].getSymbol();
                if (symbol) {
                    return symbol;
                }
            }

            return null;
        };

        SemanticInfoChain.prototype.findExternalModule = function (id) {
            id = TypeScript.normalizePath(id);

            // SPEC: Nov 18
            // An external import declaration that specifies a relative external module name (section 11.2.1) resolves the name
            // relative to the directory of the containing source file.
            // If a source file with the resulting path and file extension '.ts' exists, that file is added as a dependency.
            // Otherwise, if a source file with the resulting path and file extension '.d.ts' exists, that file is added as a dependency.
            var tsFile = id + ".ts";
            var tsCacheID = this.getDeclPathCacheID([tsFile], TypeScript.PullElementKind.DynamicModule);
            symbol = this.symbolCache[tsCacheID];
            if (symbol != undefined) {
                return symbol;
            }

            var dtsFile = id + ".d.ts";
            var dtsCacheID = this.getDeclPathCacheID([dtsFile], TypeScript.PullElementKind.DynamicModule);
            var symbol = this.symbolCache[dtsCacheID];
            if (symbol) {
                return symbol;
            }

            var dtsSymbol;
            for (var i = 0; i < this.documents.length; i++) {
                var document = this.documents[i];
                var topLevelDecl = document.topLevelDecl();

                if (topLevelDecl.isExternalModule()) {
                    var isTsFile = document.fileName === tsFile;
                    if (isTsFile || document.fileName === dtsFile) {
                        var dynamicModuleDecl = topLevelDecl.getChildDecls()[0];
                        symbol = dynamicModuleDecl.getSymbol();

                        if (isTsFile) {
                            this.symbolCache[tsCacheID] = symbol;

                            // .ts file found - can return immediately
                            return symbol;
                        } else {
                            // .d.ts file found - save the symbol and continue looking for .ts file
                            dtsSymbol = symbol;
                        }
                    }
                }
            }

            if (dtsSymbol) {
                this.symbolCache[dtsCacheID] = symbol;
                return dtsSymbol;
            }

            this.symbolCache[dtsCacheID] = null;
            this.symbolCache[tsCacheID] = null;

            return null;
        };

        SemanticInfoChain.prototype.findAmbientExternalModuleInGlobalContext = function (id) {
            var cacheID = this.getDeclPathCacheID([id], TypeScript.PullElementKind.DynamicModule);

            var symbol = this.symbolCache[cacheID];
            if (symbol == undefined) {
                symbol = null;
                for (var i = 0; i < this.documents.length; i++) {
                    var document = this.documents[i];
                    var topLevelDecl = document.topLevelDecl();

                    if (!topLevelDecl.isExternalModule()) {
                        var dynamicModules = topLevelDecl.searchChildDecls(id, TypeScript.PullElementKind.DynamicModule);
                        if (dynamicModules.length) {
                            symbol = dynamicModules[0].getSymbol();
                            break;
                        }
                    }
                }

                this.symbolCache[cacheID] = symbol;
            }

            return symbol;
        };

        // a decl path is a list of decls that reference the components of a declaration from the global scope down
        // E.g., string would be "['string']" and "A.B.C" would be "['A','B','C']"
        SemanticInfoChain.prototype.findDecls = function (declPath, declKind) {
            var cacheID = this.getDeclPathCacheID(declPath, declKind);

            if (declPath.length) {
                var cachedDecls = this.declCache[cacheID];

                if (cachedDecls && cachedDecls.length) {
                    TypeScript.declCacheHit++;
                    return cachedDecls;
                }
            }

            TypeScript.declCacheMiss++;

            var declsToSearch = this.topLevelDecls();

            var decls = TypeScript.sentinelEmptyArray;
            var path;
            var foundDecls = TypeScript.sentinelEmptyArray;

            for (var i = 0; i < declPath.length; i++) {
                path = declPath[i];
                decls = TypeScript.sentinelEmptyArray;

                var kind = (i === declPath.length - 1) ? declKind : TypeScript.PullElementKind.SomeContainer;
                for (var j = 0; j < declsToSearch.length; j++) {
                    foundDecls = declsToSearch[j].searchChildDecls(path, kind);

                    for (var k = 0; k < foundDecls.length; k++) {
                        if (decls === TypeScript.sentinelEmptyArray) {
                            decls = [];
                        }
                        decls[decls.length] = foundDecls[k];
                    }
                }

                declsToSearch = decls;

                if (!declsToSearch) {
                    break;
                }
            }

            if (decls.length) {
                this.declCache[cacheID] = decls;
            }

            return decls;
        };

        SemanticInfoChain.prototype.findDeclsFromPath = function (declPath, declKind) {
            var declString = [];

            for (var i = 0, n = declPath.length; i < n; i++) {
                if (declPath[i].kind & TypeScript.PullElementKind.Script) {
                    continue;
                }

                declString.push(declPath[i].name);
            }

            return this.findDecls(declString, declKind);
        };

        SemanticInfoChain.prototype.findSymbol = function (declPath, declType) {
            var cacheID = this.getDeclPathCacheID(declPath, declType);

            if (declPath.length) {
                var cachedSymbol = this.symbolCache[cacheID];

                if (cachedSymbol) {
                    TypeScript.symbolCacheHit++;
                    return cachedSymbol;
                }
            }

            TypeScript.symbolCacheMiss++;

            // symbol wasn't cached, so get the decl
            var decls = this.findDecls(declPath, declType);
            var symbol = null;

            if (decls.length) {
                // it might happen that container also has a value side: i.e fundule\clodule.
                // if value side is not bound yet then binding of the module (caused by getSymbol) will create a fresh variable symbol for the value side
                // instead of sharing one with function\enum\class => big problems with name resolution in future.
                // To avoid we make sure that symbol for value decl is already bound prior to getSymbol call for the container decl(if value decl is present)
                var decl = decls[0];
                if (TypeScript.hasFlag(decl.kind, TypeScript.PullElementKind.SomeContainer)) {
                    var valueDecl = decl.getValueDecl();
                    if (valueDecl) {
                        valueDecl.ensureSymbolIsBound();
                    }
                }
                symbol = decl.getSymbol();

                if (symbol) {
                    for (var i = 1; i < decls.length; i++) {
                        decls[i].ensureSymbolIsBound();
                    }

                    this.symbolCache[cacheID] = symbol;
                }
            }

            return symbol;
        };

        SemanticInfoChain.prototype.cacheGlobalSymbol = function (symbol, kind) {
            var cacheID1 = this.getDeclPathCacheID([symbol.name], kind);
            var cacheID2 = this.getDeclPathCacheID([symbol.name], symbol.kind);

            if (!this.symbolCache[cacheID1]) {
                this.symbolCache[cacheID1] = symbol;
            }

            if (!this.symbolCache[cacheID2]) {
                this.symbolCache[cacheID2] = symbol;
            }
        };

        SemanticInfoChain.prototype.invalidate = function (oldSettings, newSettings) {
            if (typeof oldSettings === "undefined") { oldSettings = null; }
            if (typeof newSettings === "undefined") { newSettings = null; }
            // A file has changed, increment the type check phase so that future type chech
            // operations will proceed.
            TypeScript.PullTypeResolver.globalTypeCheckPhase++;

            // this.logger.log("Invalidating SemanticInfoChain...");
            var cleanStart = new Date().getTime();

            this.astSymbolMap.length = 0;
            this.astAliasSymbolMap.length = 0;
            this.astCallResolutionDataMap.length = 0;

            this.declCache = TypeScript.createIntrinsicsObject();
            this.symbolCache = TypeScript.createIntrinsicsObject();
            this.fileNameToDiagnostics = TypeScript.createIntrinsicsObject();
            this._binder = null;
            this._resolver = null;
            this._topLevelDecls = null;
            this._fileNames = null;

            this.declSymbolMap.length = 0;
            this.declSignatureSymbolMap.length = 0;

            if (oldSettings && newSettings) {
                // Depending on which options changed, our cached syntactic data may not be valid
                // anymore.
                // Note: It is important to start at 1 in this loop because documents[0] is the
                // global decl with the primitive decls in it. Since documents[0] is the only
                // document that does not represent an editable file, there is no reason to ever
                // invalidate its decls. Doing this would break the invariant that all decls of
                // unedited files should persist across edits.
                if (this.settingsChangeAffectsSyntax(oldSettings, newSettings)) {
                    for (var i = 1, n = this.documents.length; i < n; i++) {
                        this.documents[i].invalidate();
                    }
                }
            }

            // Reset global counters
            TypeScript.pullSymbolID = 0;

            this.resetGlobalSymbols();

            var cleanEnd = new Date().getTime();
            // this.logger.log("   time to invalidate: " + (cleanEnd - cleanStart));
        };

        SemanticInfoChain.prototype.settingsChangeAffectsSyntax = function (before, after) {
            // If the automatic semicolon insertion option has changed, then we have to dump all
            // syntax trees in order to reparse them with the new option.
            //
            // If the language version changed, then that affects what types of things we parse. So
            // we have to dump all syntax trees.
            //
            // If propagateEnumConstants changes, then that affects the constant value data we've
            // stored in the AST.
            return before.allowAutomaticSemicolonInsertion() !== after.allowAutomaticSemicolonInsertion() || before.codeGenTarget() !== after.codeGenTarget() || before.propagateEnumConstants() !== after.propagateEnumConstants();
        };

        SemanticInfoChain.prototype.setSymbolForAST = function (ast, symbol) {
            this.astSymbolMap[ast.syntaxID()] = symbol;
        };

        SemanticInfoChain.prototype.getSymbolForAST = function (ast) {
            return this.astSymbolMap[ast.syntaxID()] || null;
        };

        SemanticInfoChain.prototype.setAliasSymbolForAST = function (ast, symbol) {
            this.astAliasSymbolMap[ast.syntaxID()] = symbol;
        };

        SemanticInfoChain.prototype.getAliasSymbolForAST = function (ast) {
            return this.astAliasSymbolMap[ast.syntaxID()];
        };

        SemanticInfoChain.prototype.getCallResolutionDataForAST = function (ast) {
            return this.astCallResolutionDataMap[ast.syntaxID()];
        };

        SemanticInfoChain.prototype.setCallResolutionDataForAST = function (ast, callResolutionData) {
            if (callResolutionData) {
                this.astCallResolutionDataMap[ast.syntaxID()] = callResolutionData;
            }
        };

        SemanticInfoChain.prototype.setSymbolForDecl = function (decl, symbol) {
            this.declSymbolMap[decl.declID] = symbol;
        };

        SemanticInfoChain.prototype.getSymbolForDecl = function (decl) {
            return this.declSymbolMap[decl.declID];
        };

        SemanticInfoChain.prototype.setSignatureSymbolForDecl = function (decl, signatureSymbol) {
            this.declSignatureSymbolMap[decl.declID] = signatureSymbol;
        };

        SemanticInfoChain.prototype.getSignatureSymbolForDecl = function (decl) {
            return this.declSignatureSymbolMap[decl.declID];
        };

        SemanticInfoChain.prototype.addDiagnostic = function (diagnostic) {
            var fileName = diagnostic.fileName();
            var diagnostics = this.fileNameToDiagnostics[fileName];
            if (!diagnostics) {
                diagnostics = [];
                this.fileNameToDiagnostics[fileName] = diagnostics;
            }

            diagnostics.push(diagnostic);
        };

        SemanticInfoChain.prototype.getDiagnostics = function (fileName) {
            var diagnostics = this.fileNameToDiagnostics[fileName];
            return diagnostics || [];
        };

        SemanticInfoChain.prototype.getBinder = function () {
            if (!this._binder) {
                this._binder = new TypeScript.PullSymbolBinder(this);
            }

            return this._binder;
        };

        SemanticInfoChain.prototype.getResolver = function () {
            if (!this._resolver) {
                this._resolver = new TypeScript.PullTypeResolver(this.compiler.compilationSettings(), this);
            }

            return this._resolver;
        };

        SemanticInfoChain.prototype.addSyntheticIndexSignature = function (containingDecl, containingSymbol, ast, indexParamName, indexParamType, returnType) {
            var indexSignature = new TypeScript.PullSignatureSymbol(TypeScript.PullElementKind.IndexSignature);
            var indexParameterSymbol = new TypeScript.PullSymbol(indexParamName, TypeScript.PullElementKind.Parameter);
            indexParameterSymbol.type = indexParamType;
            indexSignature.addParameter(indexParameterSymbol);
            indexSignature.returnType = returnType;
            indexSignature.setResolved();
            indexParameterSymbol.setResolved();

            containingSymbol.addIndexSignature(indexSignature);

            var indexSigDecl = new TypeScript.PullSynthesizedDecl("", "", TypeScript.PullElementKind.IndexSignature, TypeScript.PullElementFlags.Signature, containingDecl, containingDecl.semanticInfoChain);
            var indexParamDecl = new TypeScript.PullSynthesizedDecl(indexParamName, indexParamName, TypeScript.PullElementKind.Parameter, 0 /* None */, indexSigDecl, containingDecl.semanticInfoChain);
            indexSigDecl.setSignatureSymbol(indexSignature);
            indexParamDecl.setSymbol(indexParameterSymbol);
            indexSignature.addDeclaration(indexSigDecl);
            indexParameterSymbol.addDeclaration(indexParamDecl);
        };

        SemanticInfoChain.prototype.getDeclForAST = function (ast) {
            var document = this.getDocument(ast.fileName());

            if (document) {
                return document._getDeclForAST(ast);
            }

            return null;
        };

        SemanticInfoChain.prototype.getEnclosingDecl = function (ast) {
            return this.getDocument(ast.fileName()).getEnclosingDecl(ast);
        };

        SemanticInfoChain.prototype.setDeclForAST = function (ast, decl) {
            this.getDocument(decl.fileName())._setDeclForAST(ast, decl);
        };

        SemanticInfoChain.prototype.getASTForDecl = function (decl) {
            var document = this.getDocument(decl.fileName());
            if (document) {
                return document._getASTForDecl(decl);
            }

            return null;
        };

        SemanticInfoChain.prototype.setASTForDecl = function (decl, ast) {
            this.getDocument(decl.fileName())._setASTForDecl(decl, ast);
        };

        SemanticInfoChain.prototype.topLevelDecl = function (fileName) {
            var document = this.getDocument(fileName);
            if (document) {
                return document.topLevelDecl();
            }

            return null;
        };

        SemanticInfoChain.prototype.topLevelDecls = function () {
            if (!this._topLevelDecls) {
                this._topLevelDecls = TypeScript.ArrayUtilities.select(this.documents, function (u) {
                    return u.topLevelDecl();
                });
            }

            return this._topLevelDecls;
        };

        SemanticInfoChain.prototype.addDiagnosticFromAST = function (ast, diagnosticKey, _arguments, additionalLocations) {
            if (typeof _arguments === "undefined") { _arguments = null; }
            if (typeof additionalLocations === "undefined") { additionalLocations = null; }
            this.addDiagnostic(this.diagnosticFromAST(ast, diagnosticKey, _arguments, additionalLocations));
        };

        SemanticInfoChain.prototype.diagnosticFromAST = function (ast, diagnosticKey, _arguments, additionalLocations) {
            if (typeof _arguments === "undefined") { _arguments = null; }
            if (typeof additionalLocations === "undefined") { additionalLocations = null; }
            return new TypeScript.Diagnostic(ast.fileName(), this.lineMap(ast.fileName()), ast.start(), ast.width(), diagnosticKey, _arguments, additionalLocations);
        };

        SemanticInfoChain.prototype.locationFromAST = function (ast) {
            return new TypeScript.Location(ast.fileName(), this.lineMap(ast.fileName()), ast.start(), ast.width());
        };

        SemanticInfoChain.prototype.duplicateIdentifierDiagnosticFromAST = function (ast, identifier, additionalLocationAST) {
            return this.diagnosticFromAST(ast, TypeScript.DiagnosticCode.Duplicate_identifier_0, [identifier], additionalLocationAST ? [this.locationFromAST(additionalLocationAST)] : null);
        };

        SemanticInfoChain.prototype.addDuplicateIdentifierDiagnosticFromAST = function (ast, identifier, additionalLocationAST) {
            this.addDiagnostic(this.duplicateIdentifierDiagnosticFromAST(ast, identifier, additionalLocationAST));
        };
        return SemanticInfoChain;
    })();
    TypeScript.SemanticInfoChain = SemanticInfoChain;
})(TypeScript || (TypeScript = {}));
