// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    (function (PullElementFlags) {
        PullElementFlags[PullElementFlags["None"] = 0] = "None";
        PullElementFlags[PullElementFlags["Exported"] = 1] = "Exported";
        PullElementFlags[PullElementFlags["Private"] = 1 << 1] = "Private";
        PullElementFlags[PullElementFlags["Public"] = 1 << 2] = "Public";
        PullElementFlags[PullElementFlags["Ambient"] = 1 << 3] = "Ambient";
        PullElementFlags[PullElementFlags["Static"] = 1 << 4] = "Static";
        PullElementFlags[PullElementFlags["Optional"] = 1 << 7] = "Optional";
        PullElementFlags[PullElementFlags["Signature"] = 1 << 11] = "Signature";
        PullElementFlags[PullElementFlags["Enum"] = 1 << 12] = "Enum";
        PullElementFlags[PullElementFlags["ArrowFunction"] = 1 << 13] = "ArrowFunction";

        PullElementFlags[PullElementFlags["ClassConstructorVariable"] = 1 << 14] = "ClassConstructorVariable";
        PullElementFlags[PullElementFlags["InitializedModule"] = 1 << 15] = "InitializedModule";
        PullElementFlags[PullElementFlags["InitializedDynamicModule"] = 1 << 16] = "InitializedDynamicModule";

        PullElementFlags[PullElementFlags["MustCaptureThis"] = 1 << 18] = "MustCaptureThis";

        PullElementFlags[PullElementFlags["DeclaredInAWithBlock"] = 1 << 21] = "DeclaredInAWithBlock";

        PullElementFlags[PullElementFlags["HasReturnStatement"] = 1 << 22] = "HasReturnStatement";

        PullElementFlags[PullElementFlags["PropertyParameter"] = 1 << 23] = "PropertyParameter";

        PullElementFlags[PullElementFlags["IsAnnotatedWithAny"] = 1 << 24] = "IsAnnotatedWithAny";

        PullElementFlags[PullElementFlags["HasDefaultArgs"] = 1 << 25] = "HasDefaultArgs";

        PullElementFlags[PullElementFlags["ConstructorParameter"] = 1 << 26] = "ConstructorParameter";

        PullElementFlags[PullElementFlags["ImplicitVariable"] = PullElementFlags.ClassConstructorVariable | PullElementFlags.InitializedModule | PullElementFlags.InitializedDynamicModule | PullElementFlags.Enum] = "ImplicitVariable";
        PullElementFlags[PullElementFlags["SomeInitializedModule"] = PullElementFlags.InitializedModule | PullElementFlags.InitializedDynamicModule | PullElementFlags.Enum] = "SomeInitializedModule";
    })(TypeScript.PullElementFlags || (TypeScript.PullElementFlags = {}));
    var PullElementFlags = TypeScript.PullElementFlags;

    function hasModifier(modifiers, flag) {
        for (var i = 0, n = modifiers.length; i < n; i++) {
            if (TypeScript.hasFlag(modifiers[i], flag)) {
                return true;
            }
        }

        return false;
    }
    TypeScript.hasModifier = hasModifier;

    (function (PullElementKind) {
        PullElementKind[PullElementKind["None"] = 0] = "None";
        PullElementKind[PullElementKind["Global"] = 0] = "Global";

        PullElementKind[PullElementKind["Script"] = 1 << 0] = "Script";
        PullElementKind[PullElementKind["Primitive"] = 1 << 1] = "Primitive";

        PullElementKind[PullElementKind["Container"] = 1 << 2] = "Container";
        PullElementKind[PullElementKind["Class"] = 1 << 3] = "Class";
        PullElementKind[PullElementKind["Interface"] = 1 << 4] = "Interface";
        PullElementKind[PullElementKind["DynamicModule"] = 1 << 5] = "DynamicModule";
        PullElementKind[PullElementKind["Enum"] = 1 << 6] = "Enum";
        PullElementKind[PullElementKind["TypeAlias"] = 1 << 7] = "TypeAlias";
        PullElementKind[PullElementKind["ObjectLiteral"] = 1 << 8] = "ObjectLiteral";

        PullElementKind[PullElementKind["Variable"] = 1 << 9] = "Variable";
        PullElementKind[PullElementKind["CatchVariable"] = 1 << 10] = "CatchVariable";
        PullElementKind[PullElementKind["Parameter"] = 1 << 11] = "Parameter";
        PullElementKind[PullElementKind["Property"] = 1 << 12] = "Property";
        PullElementKind[PullElementKind["TypeParameter"] = 1 << 13] = "TypeParameter";

        PullElementKind[PullElementKind["Function"] = 1 << 14] = "Function";
        PullElementKind[PullElementKind["ConstructorMethod"] = 1 << 15] = "ConstructorMethod";
        PullElementKind[PullElementKind["Method"] = 1 << 16] = "Method";
        PullElementKind[PullElementKind["FunctionExpression"] = 1 << 17] = "FunctionExpression";

        PullElementKind[PullElementKind["GetAccessor"] = 1 << 18] = "GetAccessor";
        PullElementKind[PullElementKind["SetAccessor"] = 1 << 19] = "SetAccessor";

        PullElementKind[PullElementKind["CallSignature"] = 1 << 20] = "CallSignature";
        PullElementKind[PullElementKind["ConstructSignature"] = 1 << 21] = "ConstructSignature";
        PullElementKind[PullElementKind["IndexSignature"] = 1 << 22] = "IndexSignature";

        PullElementKind[PullElementKind["ObjectType"] = 1 << 23] = "ObjectType";
        PullElementKind[PullElementKind["FunctionType"] = 1 << 24] = "FunctionType";
        PullElementKind[PullElementKind["ConstructorType"] = 1 << 25] = "ConstructorType";

        PullElementKind[PullElementKind["EnumMember"] = 1 << 26] = "EnumMember";

        PullElementKind[PullElementKind["WithBlock"] = 1 << 27] = "WithBlock";
        PullElementKind[PullElementKind["CatchBlock"] = 1 << 28] = "CatchBlock";

        // WARNING: To prevent JS VMs from wrapping these values as floats, we don't want to utilize more than the 31 bits above.  (Doing so would
        // seriously slow down bitwise operations
        PullElementKind[PullElementKind["All"] = PullElementKind.Script | PullElementKind.Global | PullElementKind.Primitive | PullElementKind.Container | PullElementKind.Class | PullElementKind.Interface | PullElementKind.DynamicModule | PullElementKind.Enum | PullElementKind.TypeAlias | PullElementKind.ObjectLiteral | PullElementKind.Variable | PullElementKind.Parameter | PullElementKind.Property | PullElementKind.TypeParameter | PullElementKind.Function | PullElementKind.ConstructorMethod | PullElementKind.Method | PullElementKind.FunctionExpression | PullElementKind.GetAccessor | PullElementKind.SetAccessor | PullElementKind.CallSignature | PullElementKind.ConstructSignature | PullElementKind.IndexSignature | PullElementKind.ObjectType | PullElementKind.FunctionType | PullElementKind.ConstructorType | PullElementKind.EnumMember | PullElementKind.WithBlock | PullElementKind.CatchBlock] = "All";

        PullElementKind[PullElementKind["SomeFunction"] = PullElementKind.Function | PullElementKind.ConstructorMethod | PullElementKind.Method | PullElementKind.FunctionExpression | PullElementKind.GetAccessor | PullElementKind.SetAccessor] = "SomeFunction";

        // Warning: SomeValue and SomeType (along with their constituents) must be disjoint
        PullElementKind[PullElementKind["SomeValue"] = PullElementKind.Variable | PullElementKind.Parameter | PullElementKind.Property | PullElementKind.EnumMember | PullElementKind.SomeFunction] = "SomeValue";

        PullElementKind[PullElementKind["SomeType"] = PullElementKind.Script | PullElementKind.Global | PullElementKind.Primitive | PullElementKind.Class | PullElementKind.Interface | PullElementKind.Enum | PullElementKind.ObjectLiteral | PullElementKind.ObjectType | PullElementKind.FunctionType | PullElementKind.ConstructorType | PullElementKind.TypeParameter] = "SomeType";

        PullElementKind[PullElementKind["AcceptableAlias"] = PullElementKind.Variable | PullElementKind.SomeFunction | PullElementKind.Class | PullElementKind.Interface | PullElementKind.Enum | PullElementKind.Container | PullElementKind.ObjectType | PullElementKind.FunctionType | PullElementKind.ConstructorType] = "AcceptableAlias";

        PullElementKind[PullElementKind["SomeContainer"] = PullElementKind.Container | PullElementKind.DynamicModule | PullElementKind.TypeAlias] = "SomeContainer";

        PullElementKind[PullElementKind["SomeSignature"] = PullElementKind.CallSignature | PullElementKind.ConstructSignature | PullElementKind.IndexSignature] = "SomeSignature";

        PullElementKind[PullElementKind["SomeTypeReference"] = PullElementKind.Interface | PullElementKind.ObjectType | PullElementKind.FunctionType | PullElementKind.ConstructorType] = "SomeTypeReference";

        PullElementKind[PullElementKind["SomeInstantiatableType"] = PullElementKind.Class | PullElementKind.Interface | PullElementKind.TypeParameter] = "SomeInstantiatableType";
    })(TypeScript.PullElementKind || (TypeScript.PullElementKind = {}));
    var PullElementKind = TypeScript.PullElementKind;
})(TypeScript || (TypeScript = {}));
