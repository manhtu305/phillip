// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    // This is the walker that walks the type and type reference associated with the declaration.
    // This will make sure that any time, generative classification is asked, we have the right type of the declaration
    // and we can evaluate it in the correct context
    // interface IList<T> {
    //     owner: IList<IList<T>>;
    //     owner2: IList<IList<string>>;
    // }
    // class List<U> implements IList<U> {
    //     owner: List<List<U>>;
    // }
    // In the above example, when checking if owner of List<U> is subtype of owner of IList<U>
    // we want to traverse IList<T> to make sure when generative classification is asked we know exactly
    // which type parameters and which type need to be checked for infinite wrapping
    // This also is essential so that we dont incorrectly think owner2's type reference as infinitely expanding when
    // checking members of IList<string>
    var PullTypeEnclosingTypeWalker = (function () {
        function PullTypeEnclosingTypeWalker() {
            // Symbols walked
            this.currentSymbols = null;
        }
        // Enclosing type is the first symbol in the symbols visited
        PullTypeEnclosingTypeWalker.prototype.getEnclosingType = function () {
            if (this.currentSymbols && this.currentSymbols.length > 0) {
                return this.currentSymbols[0];
            }

            return null;
        };

        // We can/should walk the structure only if the enclosing type is generic
        PullTypeEnclosingTypeWalker.prototype._canWalkStructure = function () {
            var enclosingType = this.getEnclosingType();
            return !!enclosingType && enclosingType.isGeneric();
        };

        // Current symbol is the last symbol in the current symbols list
        PullTypeEnclosingTypeWalker.prototype._getCurrentSymbol = function () {
            if (this.currentSymbols && this.currentSymbols.length) {
                return this.currentSymbols[this.currentSymbols.length - 1];
            }

            return null;
        };

        // Gets the generative classification of the current symbol in the enclosing type
        PullTypeEnclosingTypeWalker.prototype.getGenerativeClassification = function () {
            if (this._canWalkStructure()) {
                var currentType = this.currentSymbols[this.currentSymbols.length - 1];
                if (!currentType) {
                    // This may occur if we are trying to walk type parameter in the original declaration
                    return 0 /* Unknown */;
                }

                var variableNeededToFixNodeJitterBug = this.getEnclosingType();

                return currentType.getGenerativeTypeClassification(variableNeededToFixNodeJitterBug);
            }

            return 2 /* Closed */;
        };

        PullTypeEnclosingTypeWalker.prototype._pushSymbol = function (symbol) {
            return this.currentSymbols.push(symbol);
        };

        PullTypeEnclosingTypeWalker.prototype._popSymbol = function () {
            return this.currentSymbols.pop();
        };

        // Sets the enclosing type along with parent declaration symbols
        PullTypeEnclosingTypeWalker.prototype._setEnclosingTypeOfParentDecl = function (decl, setSignature) {
            var parentDecl = decl.getParentDecl();
            if (parentDecl) {
                // Always set signatures in parents
                if (parentDecl.kind & TypeScript.PullElementKind.SomeInstantiatableType) {
                    this._setEnclosingTypeWorker(parentDecl.getSymbol(), true);
                } else {
                    this._setEnclosingTypeOfParentDecl(parentDecl, true);
                }

                if (this._canWalkStructure()) {
                    // Update the current decl in the
                    var symbol = decl.getSymbol();
                    if (symbol) {
                        // If symbol is raw PullSymbol (not a type or a signature, but
                        // rather a variable, function, etc), use its type instead
                        if (!symbol.isType() && !symbol.isSignature()) {
                            symbol = symbol.type;
                        }

                        this._pushSymbol(symbol);
                    }

                    // Set signature symbol if asked
                    if (setSignature) {
                        var signature = decl.getSignatureSymbol();
                        if (signature) {
                            this._pushSymbol(signature);
                        }
                    }
                }
            }
        };

        // Set the enclosing type of the symbol
        PullTypeEnclosingTypeWalker.prototype._setEnclosingTypeWorker = function (symbol, setSignature) {
            if (symbol.isType() && symbol.isNamedTypeSymbol()) {
                this.currentSymbols = [TypeScript.PullHelpers.getRootType(symbol)];
                return;
            }

            var decls = symbol.getDeclarations();
            for (var i = 0; i < decls.length; i++) {
                var decl = decls[i];
                this._setEnclosingTypeOfParentDecl(decl, setSignature);
                if (this._canWalkStructure()) {
                    return;
                }
            }
        };

        // Sets the current symbol with the symbol
        PullTypeEnclosingTypeWalker.prototype.setCurrentSymbol = function (symbol) {
            TypeScript.Debug.assert(this._canWalkStructure());
            this.currentSymbols[this.currentSymbols.length - 1] = symbol;
        };

        // Start walking type
        PullTypeEnclosingTypeWalker.prototype.startWalkingType = function (symbol) {
            var currentSymbols = this.currentSymbols;

            // If we dont have enclosing type or the symbol is named type, we need to set the new enclosing type
            var setEnclosingType = !this.getEnclosingType() || symbol.isNamedTypeSymbol();
            if (setEnclosingType) {
                this.currentSymbols = null;
                this.setEnclosingType(symbol);
            }
            return currentSymbols;
        };

        // Finish walking type
        PullTypeEnclosingTypeWalker.prototype.endWalkingType = function (currentSymbolsWhenStartedWalkingTypes) {
            this.currentSymbols = currentSymbolsWhenStartedWalkingTypes;
        };

        PullTypeEnclosingTypeWalker.prototype.setEnclosingType = function (symbol) {
            TypeScript.Debug.assert(!this.getEnclosingType());
            this._setEnclosingTypeWorker(symbol, symbol.isSignature());
        };

        // Walk members
        PullTypeEnclosingTypeWalker.prototype.walkMemberType = function (memberName, resolver) {
            if (this._canWalkStructure()) {
                var currentType = this._getCurrentSymbol();
                var memberSymbol = currentType ? resolver._getNamedPropertySymbolOfAugmentedType(memberName, currentType) : null;
                this._pushSymbol(memberSymbol ? memberSymbol.type : null);
            }
        };

        PullTypeEnclosingTypeWalker.prototype.postWalkMemberType = function () {
            if (this._canWalkStructure()) {
                this._popSymbol();
            }
        };

        // Walk signature
        PullTypeEnclosingTypeWalker.prototype.walkSignature = function (kind, index) {
            if (this._canWalkStructure()) {
                var currentType = this._getCurrentSymbol();
                var signatures;
                if (currentType) {
                    if (kind == TypeScript.PullElementKind.CallSignature) {
                        signatures = currentType.getCallSignatures();
                    } else if (kind == TypeScript.PullElementKind.ConstructSignature) {
                        signatures = currentType.getConstructSignatures();
                    } else {
                        signatures = currentType.getIndexSignatures();
                    }
                }

                this._pushSymbol(signatures ? signatures[index] : null);
            }
        };

        PullTypeEnclosingTypeWalker.prototype.postWalkSignature = function () {
            if (this._canWalkStructure()) {
                this._popSymbol();
            }
        };

        PullTypeEnclosingTypeWalker.prototype.walkTypeArgument = function (index) {
            if (this._canWalkStructure()) {
                var typeArgument = null;
                var currentType = this._getCurrentSymbol();
                if (currentType) {
                    var typeArguments = currentType.getTypeArguments();
                    typeArgument = typeArguments ? typeArguments[index] : null;
                }
                this._pushSymbol(typeArgument);
            }
        };

        PullTypeEnclosingTypeWalker.prototype.postWalkTypeArgument = function () {
            if (this._canWalkStructure()) {
                this._popSymbol();
            }
        };

        // Walk type parameter constraint
        PullTypeEnclosingTypeWalker.prototype.walkTypeParameterConstraint = function (index) {
            if (this._canWalkStructure()) {
                var typeParameters;
                var currentSymbol = this._getCurrentSymbol();
                if (currentSymbol) {
                    if (currentSymbol.isSignature()) {
                        typeParameters = currentSymbol.getTypeParameters();
                    } else {
                        TypeScript.Debug.assert(currentSymbol.isType());
                        typeParameters = currentSymbol.getTypeParameters();
                    }
                }
                this._pushSymbol(typeParameters ? typeParameters[index].getConstraint() : null);
            }
        };

        PullTypeEnclosingTypeWalker.prototype.postWalkTypeParameterConstraint = function () {
            if (this._canWalkStructure()) {
                this._popSymbol();
            }
        };

        // Walk return type
        PullTypeEnclosingTypeWalker.prototype.walkReturnType = function () {
            if (this._canWalkStructure()) {
                var currentSignature = this._getCurrentSymbol();
                this._pushSymbol(currentSignature ? currentSignature.returnType : null);
            }
        };

        PullTypeEnclosingTypeWalker.prototype.postWalkReturnType = function () {
            if (this._canWalkStructure()) {
                this._popSymbol();
            }
        };

        // Walk parameter type
        PullTypeEnclosingTypeWalker.prototype.walkParameterType = function (iParam) {
            if (this._canWalkStructure()) {
                var currentSignature = this._getCurrentSymbol();
                this._pushSymbol(currentSignature ? currentSignature.getParameterTypeAtIndex(iParam) : null);
            }
        };
        PullTypeEnclosingTypeWalker.prototype.postWalkParameterType = function () {
            if (this._canWalkStructure()) {
                this._popSymbol();
            }
        };

        // Get both kind of index signatures
        PullTypeEnclosingTypeWalker.prototype.getBothKindOfIndexSignatures = function (resolver, context, includeAugmentedType) {
            if (this._canWalkStructure()) {
                var currentType = this._getCurrentSymbol();
                if (currentType) {
                    return resolver._getBothKindsOfIndexSignatures(currentType, context, includeAugmentedType);
                }
            }
            return null;
        };

        // Walk index signature return type
        PullTypeEnclosingTypeWalker.prototype.walkIndexSignatureReturnType = function (indexSigInfo, useStringIndexSignature, onlySignature) {
            if (this._canWalkStructure()) {
                var indexSig = indexSigInfo ? (useStringIndexSignature ? indexSigInfo.stringSignature : indexSigInfo.numericSignature) : null;
                this._pushSymbol(indexSig);
                if (!onlySignature) {
                    this._pushSymbol(indexSig ? indexSig.returnType : null);
                }
            }
        };

        PullTypeEnclosingTypeWalker.prototype.postWalkIndexSignatureReturnType = function (onlySignature) {
            if (this._canWalkStructure()) {
                if (!onlySignature) {
                    this._popSymbol(); // return type
                }
                this._popSymbol(); // index signature type
            }
        };
        return PullTypeEnclosingTypeWalker;
    })();
    TypeScript.PullTypeEnclosingTypeWalker = PullTypeEnclosingTypeWalker;
})(TypeScript || (TypeScript = {}));
