// Copyright (c) Microsoft. All rights reserved. Licensed under the Apache License, Version 2.0.
// See LICENSE.txt in the project root for complete license information.
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path='..\references.ts' />
var TypeScript;
(function (TypeScript) {
    var pullDeclID = 0;
    var sentinelEmptyPullDeclArray = [];

    var PullDecl = (function () {
        function PullDecl(declName, displayName, kind, declFlags, semanticInfoChain) {
            this.declID = pullDeclID++;
            this.flags = 0 /* None */;
            this.declGroups = null;
            // Child decls
            this.childDecls = null;
            this.typeParameters = null;
            // In the case of classes, initialized modules and enums, we need to track the implicit
            // value set to the constructor or instance type.  We can use this field to make sure that on
            // edits and updates we don't leak the val decl or symbol
            this.synthesizedValDecl = null;
            // backreference from the value decl back to non-value part
            this.containerDecl = null;
            // Caches
            // Mappings from names to decls.  Public only for diffing purposes.
            this.childDeclTypeCache = null;
            this.childDeclValueCache = null;
            this.childDeclNamespaceCache = null;
            this.childDeclTypeParameterCache = null;
            this.name = declName;
            this.kind = kind;
            this.flags = declFlags;
            this.semanticInfoChain = semanticInfoChain;

            if (displayName !== this.name) {
                this.declDisplayName = displayName;
            }
        }
        PullDecl.prototype.fileName = function () {
            throw TypeScript.Errors.abstract();
        };

        PullDecl.prototype.getParentPath = function () {
            throw TypeScript.Errors.abstract();
        };

        PullDecl.prototype.getParentDecl = function () {
            throw TypeScript.Errors.abstract();
        };

        PullDecl.prototype.isExternalModule = function () {
            throw TypeScript.Errors.abstract();
        };

        // The enclosing decl is different from the parent decl. It is a syntactic ancestor decl
        // that introduces the scope in which this decl is defined.
        // Call this to get the enclosing decl of *this* decl
        PullDecl.prototype.getEnclosingDecl = function () {
            throw TypeScript.Errors.abstract();
        };

        // Note: Call this on the *parent* decl of the decl whose enclosing decl you want.
        // This will assume you have already skipped the current decl on your way to the
        // enclosing decl.
        PullDecl.prototype._getEnclosingDeclFromParentDecl = function () {
            // Skip over the decls that cannot introduce a scope
            var decl = this;
            while (decl) {
                switch (decl.kind) {
                    default:
                        return decl;
                    case TypeScript.PullElementKind.Variable:
                    case TypeScript.PullElementKind.TypeParameter:
                    case TypeScript.PullElementKind.Parameter:
                    case TypeScript.PullElementKind.TypeAlias:
                    case TypeScript.PullElementKind.EnumMember:
                }

                decl = decl.getParentDecl();
            }

            TypeScript.Debug.fail();
        };

        /** Use getName for type checking purposes, and getDisplayName to report an error or display info to the user.
        * They will differ when the identifier is an escaped unicode character or the identifier "__proto__".
        */
        PullDecl.prototype.getDisplayName = function () {
            return this.declDisplayName === undefined ? this.name : this.declDisplayName;
        };

        PullDecl.prototype.setSymbol = function (symbol) {
            this.semanticInfoChain.setSymbolForDecl(this, symbol);
        };

        PullDecl.prototype.ensureSymbolIsBound = function () {
            if (!this.hasBeenBound() && this.kind !== TypeScript.PullElementKind.Script) {
                var binder = this.semanticInfoChain.getBinder();
                binder.bindDeclToPullSymbol(this);
            }
        };

        PullDecl.prototype.getSymbol = function () {
            if (this.kind === TypeScript.PullElementKind.Script) {
                return null;
            }

            this.ensureSymbolIsBound();

            return this.semanticInfoChain.getSymbolForDecl(this);
        };

        PullDecl.prototype.hasSymbol = function () {
            var symbol = this.semanticInfoChain.getSymbolForDecl(this);
            return !!symbol;
        };

        PullDecl.prototype.setSignatureSymbol = function (signatureSymbol) {
            this.semanticInfoChain.setSignatureSymbolForDecl(this, signatureSymbol);
        };

        PullDecl.prototype.getSignatureSymbol = function () {
            this.ensureSymbolIsBound();
            return this.semanticInfoChain.getSignatureSymbolForDecl(this);
        };

        PullDecl.prototype.hasSignatureSymbol = function () {
            var signatureSymbol = this.semanticInfoChain.getSignatureSymbolForDecl(this);
            return !!signatureSymbol;
        };

        PullDecl.prototype.setFlags = function (flags) {
            this.flags = flags;
        };

        PullDecl.prototype.setFlag = function (flags) {
            this.flags |= flags;
        };

        PullDecl.prototype.setValueDecl = function (valDecl) {
            this.synthesizedValDecl = valDecl;
            valDecl.containerDecl = this;
        };

        PullDecl.prototype.getValueDecl = function () {
            return this.synthesizedValDecl;
        };

        PullDecl.prototype.getContainerDecl = function () {
            return this.containerDecl;
        };

        PullDecl.prototype.getChildDeclCache = function (declKind) {
            if (declKind === TypeScript.PullElementKind.TypeParameter) {
                if (!this.childDeclTypeParameterCache) {
                    this.childDeclTypeParameterCache = TypeScript.createIntrinsicsObject();
                }

                return this.childDeclTypeParameterCache;
            } else if (TypeScript.hasFlag(declKind, TypeScript.PullElementKind.SomeContainer)) {
                if (!this.childDeclNamespaceCache) {
                    this.childDeclNamespaceCache = TypeScript.createIntrinsicsObject();
                }

                return this.childDeclNamespaceCache;
            } else if (TypeScript.hasFlag(declKind, TypeScript.PullElementKind.SomeType)) {
                if (!this.childDeclTypeCache) {
                    this.childDeclTypeCache = TypeScript.createIntrinsicsObject();
                }

                return this.childDeclTypeCache;
            } else {
                if (!this.childDeclValueCache) {
                    this.childDeclValueCache = TypeScript.createIntrinsicsObject();
                }

                return this.childDeclValueCache;
            }
        };

        // Should only be called by subclasses.
        PullDecl.prototype.addChildDecl = function (childDecl) {
            if (childDecl.kind === TypeScript.PullElementKind.TypeParameter) {
                if (!this.typeParameters) {
                    this.typeParameters = [];
                }
                this.typeParameters[this.typeParameters.length] = childDecl;
            } else {
                if (!this.childDecls) {
                    this.childDecls = [];
                }
                this.childDecls[this.childDecls.length] = childDecl;
            }

            // add to the appropriate cache
            var declName = childDecl.name;

            if (!(childDecl.kind & TypeScript.PullElementKind.SomeSignature)) {
                var cache = this.getChildDeclCache(childDecl.kind);
                var childrenOfName = cache[declName];
                if (!childrenOfName) {
                    childrenOfName = [];
                }

                childrenOfName.push(childDecl);
                cache[declName] = childrenOfName;
            }
        };

        // Search for a child decl with the given name.  'isType' is used to specify whether or
        // not child types or child values are returned.
        PullDecl.prototype.searchChildDecls = function (declName, searchKind) {
            // find the decl with the optional type
            // if necessary, cache the decl
            // may be wise to return a chain of decls, or take a parent decl as a parameter
            var cacheVal = null;

            if (searchKind & TypeScript.PullElementKind.SomeType) {
                cacheVal = this.childDeclTypeCache ? this.childDeclTypeCache[declName] : null;
            } else if (searchKind & TypeScript.PullElementKind.SomeContainer) {
                cacheVal = this.childDeclNamespaceCache ? this.childDeclNamespaceCache[declName] : null;
            } else {
                cacheVal = this.childDeclValueCache ? this.childDeclValueCache[declName] : null;
            }

            if (cacheVal) {
                return cacheVal;
            } else {
                // If we didn't find it, and they were searching for types, then also check the
                // type parameter cache.
                if (searchKind & TypeScript.PullElementKind.SomeType) {
                    cacheVal = this.childDeclTypeParameterCache ? this.childDeclTypeParameterCache[declName] : null;

                    if (cacheVal) {
                        return cacheVal;
                    }
                }

                return sentinelEmptyPullDeclArray;
            }
        };

        PullDecl.prototype.getChildDecls = function () {
            return this.childDecls || sentinelEmptyPullDeclArray;
        };

        PullDecl.prototype.getTypeParameters = function () {
            return this.typeParameters || sentinelEmptyPullDeclArray;
        };

        PullDecl.prototype.addVariableDeclToGroup = function (decl) {
            if (!this.declGroups) {
                this.declGroups = TypeScript.createIntrinsicsObject();
            }

            var declGroup = this.declGroups[decl.name];
            if (declGroup) {
                declGroup.addDecl(decl);
            } else {
                declGroup = new PullDeclGroup(decl.name);
                declGroup.addDecl(decl);
                this.declGroups[decl.name] = declGroup;
            }
        };

        PullDecl.prototype.getVariableDeclGroups = function () {
            var declGroups = null;

            if (this.declGroups) {
                for (var declName in this.declGroups) {
                    if (this.declGroups[declName]) {
                        if (declGroups === null) {
                            declGroups = [];
                        }

                        declGroups.push(this.declGroups[declName].getDecls());
                    }
                }
            }

            return declGroups || sentinelEmptyPullDeclArray;
        };

        PullDecl.prototype.hasBeenBound = function () {
            return this.hasSymbol() || this.hasSignatureSymbol();
        };

        PullDecl.prototype.isSynthesized = function () {
            return false;
        };

        PullDecl.prototype.ast = function () {
            return this.semanticInfoChain.getASTForDecl(this);
        };

        PullDecl.prototype.isRootDecl = function () {
            throw TypeScript.Errors.abstract();
        };
        return PullDecl;
    })();
    TypeScript.PullDecl = PullDecl;

    // A root decl represents the top level decl for a file.  By specializing this decl, we
    // provide a location, per file, to store data that all decls in the file would otherwise
    // have to duplicate.  For example, there is no need to store the 'fileName' in each decl.
    // Instead, only the root decl needs to store this data.  Decls underneath it can determine
    // the file name by queryign their parent.  In other words, a Root Decl allows us to trade
    // space for logarithmic speed.
    var RootPullDecl = (function (_super) {
        __extends(RootPullDecl, _super);
        function RootPullDecl(name, fileName, kind, declFlags, semanticInfoChain, isExternalModule) {
            _super.call(this, name, name, kind, declFlags, semanticInfoChain);
            this.semanticInfoChain = semanticInfoChain;
            this._isExternalModule = isExternalModule;
            this._fileName = fileName;
        }
        RootPullDecl.prototype.fileName = function () {
            return this._fileName;
        };

        RootPullDecl.prototype.getParentPath = function () {
            return [this];
        };

        RootPullDecl.prototype.getParentDecl = function () {
            return null;
        };

        RootPullDecl.prototype.isExternalModule = function () {
            return this._isExternalModule;
        };

        // We never want the enclosing decl to be null, so if we are at a top level decl, return it
        RootPullDecl.prototype.getEnclosingDecl = function () {
            return this;
        };

        RootPullDecl.prototype.isRootDecl = function () {
            return true;
        };
        return RootPullDecl;
    })(PullDecl);
    TypeScript.RootPullDecl = RootPullDecl;

    var NormalPullDecl = (function (_super) {
        __extends(NormalPullDecl, _super);
        function NormalPullDecl(declName, displayName, kind, declFlags, parentDecl, addToParent) {
            if (typeof addToParent === "undefined") { addToParent = true; }
            _super.call(this, declName, displayName, kind, declFlags, parentDecl ? parentDecl.semanticInfoChain : null);
            this.parentDecl = null;
            this.parentPath = null;

            // Link to parent
            this.parentDecl = parentDecl;
            if (addToParent) {
                parentDecl.addChildDecl(this);
            }

            if (this.parentDecl) {
                if (this.parentDecl.isRootDecl()) {
                    this._rootDecl = this.parentDecl;
                } else {
                    this._rootDecl = this.parentDecl._rootDecl;
                }
            } else {
                // Synthetic
                TypeScript.Debug.assert(this.isSynthesized());
                this._rootDecl = null;
            }
            //if (!parentDecl && !this.isSynthesized() && kind !== PullElementKind.Primitive) {
            //    throw Errors.invalidOperation("Orphaned decl " + PullElementKind[kind]);
            //}
        }
        NormalPullDecl.prototype.fileName = function () {
            return this._rootDecl.fileName();
        };

        NormalPullDecl.prototype.getParentDecl = function () {
            return this.parentDecl;
        };

        NormalPullDecl.prototype.getParentPath = function () {
            if (!this.parentPath) {
                var path = [this];
                var parentDecl = this.parentDecl;

                while (parentDecl) {
                    if (parentDecl && path[path.length - 1] !== parentDecl && !(parentDecl.kind & (TypeScript.PullElementKind.ObjectLiteral | TypeScript.PullElementKind.ObjectType))) {
                        path.unshift(parentDecl);
                    }

                    parentDecl = parentDecl.getParentDecl();
                }

                this.parentPath = path;
            }

            return this.parentPath;
        };

        NormalPullDecl.prototype.isExternalModule = function () {
            return false;
        };

        NormalPullDecl.prototype.getEnclosingDecl = function () {
            return this.parentDecl && this.parentDecl._getEnclosingDeclFromParentDecl();
        };

        NormalPullDecl.prototype.isRootDecl = function () {
            return false;
        };
        return NormalPullDecl;
    })(PullDecl);
    TypeScript.NormalPullDecl = NormalPullDecl;

    var PullEnumElementDecl = (function (_super) {
        __extends(PullEnumElementDecl, _super);
        function PullEnumElementDecl(declName, displayName, parentDecl) {
            _super.call(this, declName, displayName, TypeScript.PullElementKind.EnumMember, TypeScript.PullElementFlags.Public, parentDecl);
            this.constantValue = null;
        }
        return PullEnumElementDecl;
    })(NormalPullDecl);
    TypeScript.PullEnumElementDecl = PullEnumElementDecl;

    var PullFunctionExpressionDecl = (function (_super) {
        __extends(PullFunctionExpressionDecl, _super);
        function PullFunctionExpressionDecl(expressionName, declFlags, parentDecl, displayName) {
            if (typeof displayName === "undefined") { displayName = ""; }
            _super.call(this, "", displayName, TypeScript.PullElementKind.FunctionExpression, declFlags, parentDecl);
            this.functionExpressionName = expressionName;
        }
        PullFunctionExpressionDecl.prototype.getFunctionExpressionName = function () {
            return this.functionExpressionName;
        };
        return PullFunctionExpressionDecl;
    })(NormalPullDecl);
    TypeScript.PullFunctionExpressionDecl = PullFunctionExpressionDecl;

    var PullSynthesizedDecl = (function (_super) {
        __extends(PullSynthesizedDecl, _super);
        // This is a synthesized decl; its life time should match that of the symbol using it, and
        // not that of its parent decl. To enforce this we are not making it reachable from its
        // parent, but will set the parent link.
        function PullSynthesizedDecl(declName, displayName, kind, declFlags, parentDecl, semanticInfoChain) {
            _super.call(this, declName, displayName, kind, declFlags, parentDecl, false);
            this.semanticInfoChain = semanticInfoChain;
        }
        PullSynthesizedDecl.prototype.isSynthesized = function () {
            return true;
        };

        PullSynthesizedDecl.prototype.fileName = function () {
            return this._rootDecl ? this._rootDecl.fileName() : "";
        };
        return PullSynthesizedDecl;
    })(NormalPullDecl);
    TypeScript.PullSynthesizedDecl = PullSynthesizedDecl;

    var PullDeclGroup = (function () {
        function PullDeclGroup(name) {
            this.name = name;
            this._decls = [];
        }
        PullDeclGroup.prototype.addDecl = function (decl) {
            if (decl.name === this.name) {
                this._decls[this._decls.length] = decl;
            }
        };

        PullDeclGroup.prototype.getDecls = function () {
            return this._decls;
        };
        return PullDeclGroup;
    })();
    TypeScript.PullDeclGroup = PullDeclGroup;
})(TypeScript || (TypeScript = {}));
