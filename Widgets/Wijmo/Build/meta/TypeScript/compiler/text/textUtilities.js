///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    (function (TextUtilities) {
        function parseLineStarts(text) {
            var length = text.length;

            // Corner case check
            if (0 === length) {
                var result = new Array();
                result.push(0);
                return result;
            }

            var position = 0;
            var index = 0;
            var arrayBuilder = new Array();
            var lineNumber = 0;

            while (index < length) {
                var c = text.charCodeAt(index);
                var lineBreakLength;

                // common case - ASCII & not a line break
                if (c > 13 /* carriageReturn */ && c <= 127) {
                    index++;
                    continue;
                } else if (c === 13 /* carriageReturn */ && index + 1 < length && text.charCodeAt(index + 1) === 10 /* lineFeed */) {
                    lineBreakLength = 2;
                } else if (c === 10 /* lineFeed */) {
                    lineBreakLength = 1;
                } else {
                    lineBreakLength = TextUtilities.getLengthOfLineBreak(text, index);
                }

                if (0 === lineBreakLength) {
                    index++;
                } else {
                    arrayBuilder.push(position);
                    index += lineBreakLength;
                    position = index;
                    lineNumber++;
                }
            }

            // Create a start for the final line.
            arrayBuilder.push(position);

            return arrayBuilder;
        }
        TextUtilities.parseLineStarts = parseLineStarts;

        function getLengthOfLineBreakSlow(text, index, c) {
            if (c === 13 /* carriageReturn */) {
                var next = index + 1;
                return (next < text.length) && 10 /* lineFeed */ === text.charCodeAt(next) ? 2 : 1;
            } else if (isAnyLineBreakCharacter(c)) {
                return 1;
            } else {
                return 0;
            }
        }
        TextUtilities.getLengthOfLineBreakSlow = getLengthOfLineBreakSlow;

        function getLengthOfLineBreak(text, index) {
            var c = text.charCodeAt(index);

            // common case - ASCII & not a line break
            if (c > 13 /* carriageReturn */ && c <= 127) {
                return 0;
            }

            return getLengthOfLineBreakSlow(text, index, c);
        }
        TextUtilities.getLengthOfLineBreak = getLengthOfLineBreak;

        function isAnyLineBreakCharacter(c) {
            return c === 10 /* lineFeed */ || c === 13 /* carriageReturn */ || c === 133 /* nextLine */ || c === 8232 /* lineSeparator */ || c === 8233 /* paragraphSeparator */;
        }
        TextUtilities.isAnyLineBreakCharacter = isAnyLineBreakCharacter;
    })(TypeScript.TextUtilities || (TypeScript.TextUtilities = {}));
    var TextUtilities = TypeScript.TextUtilities;
})(TypeScript || (TypeScript = {}));
