///<reference path='references.ts' />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var TypeScript;
(function (TypeScript) {
    (function (TextFactory) {
        /**
        * Return startLineBreak = index-1, lengthLineBreak = 2   if there is a \r\n at index-1
        * Return startLineBreak = index,   lengthLineBreak = 1   if there is a 1-char newline at index
        * Return startLineBreak = index+1, lengthLineBreak = 0   if there is no newline at index.
        */
        function getStartAndLengthOfLineBreakEndingAt(text, index, info) {
            var c = text.charCodeAt(index);
            if (c === 10 /* lineFeed */) {
                if (index > 0 && text.charCodeAt(index - 1) === 13 /* carriageReturn */) {
                    // "\r\n" is the only 2-character line break.
                    info.startPosition = index - 1;
                    info.length = 2;
                } else {
                    info.startPosition = index;
                    info.length = 1;
                }
            } else if (TypeScript.TextUtilities.isAnyLineBreakCharacter(c)) {
                info.startPosition = index;
                info.length = 1;
            } else {
                info.startPosition = index + 1;
                info.length = 0;
            }
        }

        var LinebreakInfo = (function () {
            function LinebreakInfo(startPosition, length) {
                this.startPosition = startPosition;
                this.length = length;
            }
            return LinebreakInfo;
        })();

        var TextLine = (function () {
            function TextLine(text, body, lineBreakLength, lineNumber) {
                this._text = null;
                this._textSpan = null;
                if (text === null) {
                    throw TypeScript.Errors.argumentNull('text');
                }
                TypeScript.Debug.assert(lineBreakLength >= 0);
                TypeScript.Debug.assert(lineNumber >= 0);
                this._text = text;
                this._textSpan = body;
                this._lineBreakLength = lineBreakLength;
                this._lineNumber = lineNumber;
            }
            TextLine.prototype.start = function () {
                return this._textSpan.start();
            };

            TextLine.prototype.end = function () {
                return this._textSpan.end();
            };

            TextLine.prototype.endIncludingLineBreak = function () {
                return this.end() + this._lineBreakLength;
            };

            TextLine.prototype.extent = function () {
                return this._textSpan;
            };

            TextLine.prototype.extentIncludingLineBreak = function () {
                return TypeScript.TextSpan.fromBounds(this.start(), this.endIncludingLineBreak());
            };

            TextLine.prototype.toString = function () {
                return this._text.toString(this._textSpan);
            };

            TextLine.prototype.lineNumber = function () {
                return this._lineNumber;
            };
            return TextLine;
        })();

        var TextBase = (function () {
            function TextBase() {
                this.linebreakInfo = new LinebreakInfo(0, 0);
                this.lastLineFoundForPosition = null;
            }
            /**
            * The length of the text represented by StringText.
            */
            TextBase.prototype.length = function () {
                throw TypeScript.Errors.abstract();
            };

            /**
            * Returns a character at given position. Throws an ArgumentOutOfRangeException when position is negative or
            * greater than length.
            * @param position The position to get the character from.
            */
            TextBase.prototype.charCodeAt = function (position) {
                throw TypeScript.Errors.abstract();
            };

            TextBase.prototype.checkSubSpan = function (span) {
                if (span.start() < 0 || span.start() > this.length() || span.end() > this.length()) {
                    throw TypeScript.Errors.argumentOutOfRange("span");
                }
            };

            /**
            * Provides a string representation of the StringText located within given span. Throws an ArgumentOutOfRangeException when given span is outside of the text range.
            */
            TextBase.prototype.toString = function (span) {
                if (typeof span === "undefined") { span = null; }
                throw TypeScript.Errors.abstract();
            };

            TextBase.prototype.subText = function (span) {
                this.checkSubSpan(span);

                return new SubText(this, span);
            };

            TextBase.prototype.substr = function (start, length, intern) {
                throw TypeScript.Errors.abstract();
            };

            /**
            * Copy a range of characters from this IText to a destination array.
            */
            TextBase.prototype.copyTo = function (sourceIndex, destination, destinationIndex, count) {
                throw TypeScript.Errors.abstract();
            };

            /**
            * The length of the text represented by StringText.
            */
            TextBase.prototype.lineCount = function () {
                return this._lineStarts().length;
            };

            /**
            * The sequence of lines represented by StringText.
            */
            TextBase.prototype.lines = function () {
                var lines = [];

                var length = this.lineCount();
                for (var i = 0; i < length; ++i) {
                    lines[i] = this.getLineFromLineNumber(i);
                }

                return lines;
            };

            TextBase.prototype.lineMap = function () {
                var _this = this;
                return new TypeScript.LineMap(function () {
                    return _this._lineStarts();
                }, this.length());
            };

            TextBase.prototype._lineStarts = function () {
                throw TypeScript.Errors.abstract();
            };

            TextBase.prototype.getLineFromLineNumber = function (lineNumber) {
                var lineStarts = this._lineStarts();

                if (lineNumber < 0 || lineNumber >= lineStarts.length) {
                    throw TypeScript.Errors.argumentOutOfRange("lineNumber");
                }

                var first = lineStarts[lineNumber];
                if (lineNumber === lineStarts.length - 1) {
                    return new TextLine(this, new TypeScript.TextSpan(first, this.length() - first), 0, lineNumber);
                } else {
                    getStartAndLengthOfLineBreakEndingAt(this, lineStarts[lineNumber + 1] - 1, this.linebreakInfo);
                    return new TextLine(this, new TypeScript.TextSpan(first, this.linebreakInfo.startPosition - first), this.linebreakInfo.length, lineNumber);
                }
            };

            TextBase.prototype.getLineFromPosition = function (position) {
                // After asking about a location on a particular line
                // it is common to ask about other position in the same line again.
                // try to see if this is the case.
                var lastFound = this.lastLineFoundForPosition;
                if (lastFound !== null && lastFound.start() <= position && lastFound.endIncludingLineBreak() > position) {
                    return lastFound;
                }

                var lineNumber = this.getLineNumberFromPosition(position);

                var result = this.getLineFromLineNumber(lineNumber);
                this.lastLineFoundForPosition = result;
                return result;
            };

            TextBase.prototype.getLineNumberFromPosition = function (position) {
                if (position < 0 || position > this.length()) {
                    throw TypeScript.Errors.argumentOutOfRange("position");
                }

                if (position === this.length()) {
                    // this can happen when the user tried to get the line of items
                    // that are at the absolute end of this text (i.e. the EndOfLine
                    // token, or missing tokens that are at the end of the text).
                    // In this case, we want the last line in the text.
                    return this.lineCount() - 1;
                }

                // Binary search to find the right line
                var lineNumber = TypeScript.ArrayUtilities.binarySearch(this._lineStarts(), position);
                if (lineNumber < 0) {
                    lineNumber = (~lineNumber) - 1;
                }

                return lineNumber;
            };

            TextBase.prototype.getLinePosition = function (position) {
                if (position < 0 || position > this.length()) {
                    throw TypeScript.Errors.argumentOutOfRange("position");
                }

                var lineNumber = this.getLineNumberFromPosition(position);

                return new TypeScript.LineAndCharacter(lineNumber, position - this._lineStarts()[lineNumber]);
            };
            return TextBase;
        })();

        /**
        * An IText that represents a subrange of another IText.
        */
        var SubText = (function (_super) {
            __extends(SubText, _super);
            function SubText(text, span) {
                _super.call(this);
                /**
                * The line start position of each line.
                */
                this._lazyLineStarts = null;

                if (text === null) {
                    throw TypeScript.Errors.argumentNull("text");
                }

                if (span.start() < 0 || span.start() >= text.length() || span.end() < 0 || span.end() > text.length()) {
                    throw TypeScript.Errors.argument("span");
                }

                this.text = text;
                this.span = span;
            }
            SubText.prototype.length = function () {
                return this.span.length();
            };

            SubText.prototype.charCodeAt = function (position) {
                if (position < 0 || position > this.length()) {
                    throw TypeScript.Errors.argumentOutOfRange("position");
                }

                return this.text.charCodeAt(this.span.start() + position);
            };

            SubText.prototype.subText = function (span) {
                this.checkSubSpan(span);

                return new SubText(this.text, this.getCompositeSpan(span.start(), span.length()));
            };

            SubText.prototype.copyTo = function (sourceIndex, destination, destinationIndex, count) {
                var span = this.getCompositeSpan(sourceIndex, count);
                this.text.copyTo(span.start(), destination, destinationIndex, span.length());
            };

            SubText.prototype.substr = function (start, length, intern) {
                var startInOriginalText = this.span.start() + start;
                return this.text.substr(startInOriginalText, length, intern);
            };

            SubText.prototype.getCompositeSpan = function (start, length) {
                var compositeStart = TypeScript.MathPrototype.min(this.text.length(), this.span.start() + start);
                var compositeEnd = TypeScript.MathPrototype.min(this.text.length(), compositeStart + length);
                return new TypeScript.TextSpan(compositeStart, compositeEnd - compositeStart);
            };

            SubText.prototype._lineStarts = function () {
                var _this = this;
                if (!this._lazyLineStarts) {
                    this._lazyLineStarts = TypeScript.TextUtilities.parseLineStarts({ charCodeAt: function (index) {
                            return _this.charCodeAt(index);
                        }, length: this.length() });
                }

                return this._lazyLineStarts;
            };
            return SubText;
        })(TextBase);

        /**
        * Implementation of IText based on a System.String input
        */
        var StringText = (function (_super) {
            __extends(StringText, _super);
            /**
            * Initializes an instance of StringText with provided data.
            */
            function StringText(data) {
                _super.call(this);
                /**
                * Underlying string on which this IText instance is based
                */
                this.source = null;
                /**
                * The line start position of each line.
                */
                this._lazyLineStarts = null;

                if (data === null) {
                    throw TypeScript.Errors.argumentNull("data");
                }

                this.source = data;
            }
            /**
            * The length of the text represented by StringText.
            */
            StringText.prototype.length = function () {
                return this.source.length;
            };

            /**
            * Returns a character at given position. Throws an ArgumentOutOfRangeException when position is negative or
            * greater than length.
            * @param position The position to get the character from.
            */
            StringText.prototype.charCodeAt = function (position) {
                if (position < 0 || position >= this.source.length) {
                    throw TypeScript.Errors.argumentOutOfRange("position");
                }

                return this.source.charCodeAt(position);
            };

            StringText.prototype.substr = function (start, length, intern) {
                return this.source.substr(start, length);
            };

            /**
            * Provides a string representation of the StringText located within given span. Throws an ArgumentOutOfRangeException when given span is outside of the text range.
            */
            StringText.prototype.toString = function (span) {
                if (typeof span === "undefined") { span = null; }
                if (span === null) {
                    span = new TypeScript.TextSpan(0, this.length());
                }

                this.checkSubSpan(span);

                if (span.start() === 0 && span.length() === this.length()) {
                    return this.source;
                }

                return this.source.substr(span.start(), span.length());
            };

            StringText.prototype.copyTo = function (sourceIndex, destination, destinationIndex, count) {
                TypeScript.StringUtilities.copyTo(this.source, sourceIndex, destination, destinationIndex, count);
            };

            StringText.prototype._lineStarts = function () {
                if (this._lazyLineStarts === null) {
                    this._lazyLineStarts = TypeScript.TextUtilities.parseLineStarts(this.source);
                }

                return this._lazyLineStarts;
            };
            return StringText;
        })(TextBase);

        function createText(value) {
            return new StringText(value);
        }
        TextFactory.createText = createText;
    })(TypeScript.TextFactory || (TypeScript.TextFactory = {}));
    var TextFactory = TypeScript.TextFactory;
})(TypeScript || (TypeScript = {}));

var TypeScript;
(function (TypeScript) {
    (function (SimpleText) {
        /**
        * An IText that represents a subrange of another IText.
        */
        var SimpleSubText = (function () {
            function SimpleSubText(text, span) {
                this.text = null;
                this.span = null;
                if (text === null) {
                    throw TypeScript.Errors.argumentNull("text");
                }

                if (span.start() < 0 || span.start() >= text.length() || span.end() < 0 || span.end() > text.length()) {
                    throw TypeScript.Errors.argument("span");
                }

                this.text = text;
                this.span = span;
            }
            SimpleSubText.prototype.checkSubSpan = function (span) {
                if (span.start() < 0 || span.start() > this.length() || span.end() > this.length()) {
                    throw TypeScript.Errors.argumentOutOfRange("span");
                }
            };

            SimpleSubText.prototype.checkSubPosition = function (position) {
                if (position < 0 || position >= this.length()) {
                    throw TypeScript.Errors.argumentOutOfRange("position");
                }
            };

            SimpleSubText.prototype.length = function () {
                return this.span.length();
            };

            SimpleSubText.prototype.subText = function (span) {
                this.checkSubSpan(span);

                return new SimpleSubText(this.text, this.getCompositeSpan(span.start(), span.length()));
            };

            SimpleSubText.prototype.copyTo = function (sourceIndex, destination, destinationIndex, count) {
                var span = this.getCompositeSpan(sourceIndex, count);
                this.text.copyTo(span.start(), destination, destinationIndex, span.length());
            };

            SimpleSubText.prototype.substr = function (start, length, intern) {
                var span = this.getCompositeSpan(start, length);
                return this.text.substr(span.start(), span.length(), intern);
            };

            SimpleSubText.prototype.getCompositeSpan = function (start, length) {
                var compositeStart = TypeScript.MathPrototype.min(this.text.length(), this.span.start() + start);
                var compositeEnd = TypeScript.MathPrototype.min(this.text.length(), compositeStart + length);
                return new TypeScript.TextSpan(compositeStart, compositeEnd - compositeStart);
            };

            SimpleSubText.prototype.charCodeAt = function (index) {
                this.checkSubPosition(index);
                return this.text.charCodeAt(this.span.start() + index);
            };

            SimpleSubText.prototype.lineMap = function () {
                return TypeScript.LineMap1.fromSimpleText(this);
            };
            return SimpleSubText;
        })();

        var SimpleStringText = (function () {
            function SimpleStringText(value) {
                this.value = value;
                this._lineMap = null;
            }
            SimpleStringText.prototype.length = function () {
                return this.value.length;
            };

            SimpleStringText.prototype.copyTo = function (sourceIndex, destination, destinationIndex, count) {
                TypeScript.StringUtilities.copyTo(this.value, sourceIndex, destination, destinationIndex, count);
            };

            SimpleStringText.prototype.substr = function (start, length, intern) {
                if (intern) {
                    // use a shared array instance of the length of this substring isn't too large.
                    var array = length <= SimpleStringText.charArray.length ? SimpleStringText.charArray : TypeScript.ArrayUtilities.createArray(length, /*defaultValue:*/ 0);
                    this.copyTo(start, array, 0, length);
                    return TypeScript.Collections.DefaultStringTable.addCharArray(array, 0, length);
                }

                return this.value.substr(start, length);
            };

            SimpleStringText.prototype.subText = function (span) {
                return new SimpleSubText(this, span);
            };

            SimpleStringText.prototype.charCodeAt = function (index) {
                return this.value.charCodeAt(index);
            };

            SimpleStringText.prototype.lineMap = function () {
                if (!this._lineMap) {
                    this._lineMap = TypeScript.LineMap1.fromString(this.value);
                }

                return this._lineMap;
            };
            SimpleStringText.charArray = TypeScript.ArrayUtilities.createArray(1024, 0);
            return SimpleStringText;
        })();

        // Class which wraps a host IScriptSnapshot and exposes an ISimpleText for newer compiler code.
        var SimpleScriptSnapshotText = (function () {
            function SimpleScriptSnapshotText(scriptSnapshot) {
                this.scriptSnapshot = scriptSnapshot;
            }
            SimpleScriptSnapshotText.prototype.charCodeAt = function (index) {
                return this.scriptSnapshot.getText(index, index + 1).charCodeAt(0);
            };

            SimpleScriptSnapshotText.prototype.length = function () {
                return this.scriptSnapshot.getLength();
            };

            SimpleScriptSnapshotText.prototype.copyTo = function (sourceIndex, destination, destinationIndex, count) {
                var text = this.scriptSnapshot.getText(sourceIndex, sourceIndex + count);
                TypeScript.StringUtilities.copyTo(text, 0, destination, destinationIndex, count);
            };

            SimpleScriptSnapshotText.prototype.substr = function (start, length, intern) {
                return this.scriptSnapshot.getText(start, start + length);
            };

            SimpleScriptSnapshotText.prototype.subText = function (span) {
                return new SimpleSubText(this, span);
            };

            SimpleScriptSnapshotText.prototype.lineMap = function () {
                var _this = this;
                return new TypeScript.LineMap(function () {
                    return _this.scriptSnapshot.getLineStartPositions();
                }, this.length());
            };
            return SimpleScriptSnapshotText;
        })();

        function fromString(value) {
            return new SimpleStringText(value);
        }
        SimpleText.fromString = fromString;

        function fromScriptSnapshot(scriptSnapshot) {
            return new SimpleScriptSnapshotText(scriptSnapshot);
        }
        SimpleText.fromScriptSnapshot = fromScriptSnapshot;
    })(TypeScript.SimpleText || (TypeScript.SimpleText = {}));
    var SimpleText = TypeScript.SimpleText;
})(TypeScript || (TypeScript = {}));
