///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    

    (function (ScriptSnapshot) {
        var StringScriptSnapshot = (function () {
            function StringScriptSnapshot(text) {
                this.text = text;
                this._lineStartPositions = null;
            }
            StringScriptSnapshot.prototype.getText = function (start, end) {
                return this.text.substring(start, end);
            };

            StringScriptSnapshot.prototype.getLength = function () {
                return this.text.length;
            };

            StringScriptSnapshot.prototype.getLineStartPositions = function () {
                if (!this._lineStartPositions) {
                    this._lineStartPositions = TypeScript.TextUtilities.parseLineStarts(this.text);
                }

                return this._lineStartPositions;
            };

            StringScriptSnapshot.prototype.getTextChangeRangeSinceVersion = function (scriptVersion) {
                throw TypeScript.Errors.notYetImplemented();
            };
            return StringScriptSnapshot;
        })();

        function fromString(text) {
            return new StringScriptSnapshot(text);
        }
        ScriptSnapshot.fromString = fromString;
    })(TypeScript.ScriptSnapshot || (TypeScript.ScriptSnapshot = {}));
    var ScriptSnapshot = TypeScript.ScriptSnapshot;
})(TypeScript || (TypeScript = {}));
