///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    (function (LineMap1) {
        function fromSimpleText(text) {
            return new TypeScript.LineMap(function () {
                return TypeScript.TextUtilities.parseLineStarts({ charCodeAt: function (index) {
                        return text.charCodeAt(index);
                    }, length: text.length() });
            }, text.length());
        }
        LineMap1.fromSimpleText = fromSimpleText;

        function fromScriptSnapshot(scriptSnapshot) {
            return new TypeScript.LineMap(function () {
                return scriptSnapshot.getLineStartPositions();
            }, scriptSnapshot.getLength());
        }
        LineMap1.fromScriptSnapshot = fromScriptSnapshot;

        function fromString(text) {
            return new TypeScript.LineMap(function () {
                return TypeScript.TextUtilities.parseLineStarts(text);
            }, text.length);
        }
        LineMap1.fromString = fromString;
    })(TypeScript.LineMap1 || (TypeScript.LineMap1 = {}));
    var LineMap1 = TypeScript.LineMap1;
})(TypeScript || (TypeScript = {}));
