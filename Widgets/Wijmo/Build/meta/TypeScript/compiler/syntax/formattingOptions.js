///<reference path='references.ts' />
var FormattingOptions = (function () {
    function FormattingOptions(useTabs, spacesPerTab, indentSpaces, newLineCharacter) {
        this.useTabs = useTabs;
        this.spacesPerTab = spacesPerTab;
        this.indentSpaces = indentSpaces;
        this.newLineCharacter = newLineCharacter;
    }
    FormattingOptions.defaultOptions = new FormattingOptions(false, /*spacesPerTab:*/ 4, /*indentSpaces:*/ 4, "\r\n");
    return FormattingOptions;
})();
