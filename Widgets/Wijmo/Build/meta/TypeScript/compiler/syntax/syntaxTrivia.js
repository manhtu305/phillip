///<reference path='references.ts' />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};

var TypeScript;
(function (TypeScript) {
    (function (Syntax) {
        var AbstractTrivia = (function () {
            function AbstractTrivia(_kind) {
                this._kind = _kind;
            }
            AbstractTrivia.prototype.fullWidth = function () {
                throw TypeScript.Errors.abstract();
            };

            AbstractTrivia.prototype.fullText = function () {
                throw TypeScript.Errors.abstract();
            };

            AbstractTrivia.prototype.skippedToken = function () {
                throw TypeScript.Errors.abstract();
            };

            AbstractTrivia.prototype.toJSON = function (key) {
                var result = {};

                for (var name in TypeScript.SyntaxKind) {
                    if (TypeScript.SyntaxKind[name] === this._kind) {
                        result.kind = name;
                        break;
                    }
                }

                if (this.isSkippedToken()) {
                    result.skippedToken = this.skippedToken();
                } else {
                    result.text = this.fullText();
                }
                return result;
            };

            AbstractTrivia.prototype.kind = function () {
                return this._kind;
            };

            AbstractTrivia.prototype.isWhitespace = function () {
                return this.kind() === 4 /* WhitespaceTrivia */;
            };

            AbstractTrivia.prototype.isComment = function () {
                return this.kind() === 7 /* SingleLineCommentTrivia */ || this.kind() === 6 /* MultiLineCommentTrivia */;
            };

            AbstractTrivia.prototype.isNewLine = function () {
                return this.kind() === 5 /* NewLineTrivia */;
            };

            AbstractTrivia.prototype.isSkippedToken = function () {
                return this.kind() === 8 /* SkippedTokenTrivia */;
            };

            AbstractTrivia.prototype.collectTextElements = function (elements) {
                elements.push(this.fullText());
            };
            return AbstractTrivia;
        })();

        var NormalTrivia = (function (_super) {
            __extends(NormalTrivia, _super);
            function NormalTrivia(kind, _text) {
                _super.call(this, kind);
                this._text = _text;
            }
            NormalTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };

            NormalTrivia.prototype.fullText = function () {
                return this._text;
            };

            NormalTrivia.prototype.skippedToken = function () {
                throw TypeScript.Errors.invalidOperation();
            };
            return NormalTrivia;
        })(AbstractTrivia);

        var SkippedTokenTrivia = (function (_super) {
            __extends(SkippedTokenTrivia, _super);
            function SkippedTokenTrivia(_skippedToken) {
                _super.call(this, 8 /* SkippedTokenTrivia */);
                this._skippedToken = _skippedToken;
            }
            SkippedTokenTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };

            SkippedTokenTrivia.prototype.fullText = function () {
                return this.skippedToken().fullText();
            };

            SkippedTokenTrivia.prototype.skippedToken = function () {
                return this._skippedToken;
            };
            return SkippedTokenTrivia;
        })(AbstractTrivia);

        var DeferredTrivia = (function (_super) {
            __extends(DeferredTrivia, _super);
            function DeferredTrivia(kind, _text, _fullStart, _fullWidth) {
                _super.call(this, kind);
                this._text = _text;
                this._fullStart = _fullStart;
                this._fullWidth = _fullWidth;
                this._fullText = null;
            }
            DeferredTrivia.prototype.fullWidth = function () {
                return this._fullWidth;
            };

            DeferredTrivia.prototype.fullText = function () {
                if (!this._fullText) {
                    this._fullText = this._text.substr(this._fullStart, this._fullWidth, false);
                    this._text = null;
                }

                return this._fullText;
            };

            DeferredTrivia.prototype.skippedToken = function () {
                throw TypeScript.Errors.invalidOperation();
            };
            return DeferredTrivia;
        })(AbstractTrivia);

        function deferredTrivia(kind, text, fullStart, fullWidth) {
            return new DeferredTrivia(kind, text, fullStart, fullWidth);
        }
        Syntax.deferredTrivia = deferredTrivia;

        function trivia(kind, text) {
            // Debug.assert(kind === SyntaxKind.MultiLineCommentTrivia || kind === SyntaxKind.NewLineTrivia || kind === SyntaxKind.SingleLineCommentTrivia || kind === SyntaxKind.WhitespaceTrivia || kind === SyntaxKind.SkippedTextTrivia);
            // Debug.assert(text.length > 0);
            return new NormalTrivia(kind, text);
        }
        Syntax.trivia = trivia;

        function skippedTokenTrivia(token) {
            TypeScript.Debug.assert(!token.hasLeadingTrivia());
            TypeScript.Debug.assert(!token.hasTrailingTrivia());
            TypeScript.Debug.assert(token.fullWidth() > 0);
            return new SkippedTokenTrivia(token);
        }
        Syntax.skippedTokenTrivia = skippedTokenTrivia;

        function spaces(count) {
            return trivia(4 /* WhitespaceTrivia */, TypeScript.StringUtilities.repeat(" ", count));
        }
        Syntax.spaces = spaces;

        function whitespace(text) {
            return trivia(4 /* WhitespaceTrivia */, text);
        }
        Syntax.whitespace = whitespace;

        function multiLineComment(text) {
            return trivia(6 /* MultiLineCommentTrivia */, text);
        }
        Syntax.multiLineComment = multiLineComment;

        function singleLineComment(text) {
            return trivia(7 /* SingleLineCommentTrivia */, text);
        }
        Syntax.singleLineComment = singleLineComment;

        Syntax.spaceTrivia = spaces(1);
        Syntax.lineFeedTrivia = trivia(5 /* NewLineTrivia */, "\n");
        Syntax.carriageReturnTrivia = trivia(5 /* NewLineTrivia */, "\r");
        Syntax.carriageReturnLineFeedTrivia = trivia(5 /* NewLineTrivia */, "\r\n");

        // Breaks a multiline trivia up into individual line components.  If the trivia doesn't span
        // any lines, then the result will be a single string with the entire text of the trivia.
        // Otherwise, there will be one entry in the array for each line spanned by the trivia.  Each
        // entry will contain the line separator at the end of the string.
        function splitMultiLineCommentTriviaIntoMultipleLines(trivia) {
            // Debug.assert(trivia.kind() === SyntaxKind.MultiLineCommentTrivia);
            var result = [];

            var triviaText = trivia.fullText();
            var currentIndex = 0;

            for (var i = 0; i < triviaText.length; i++) {
                var ch = triviaText.charCodeAt(i);

                // When we run into a newline for the first time, create the string builder and copy
                // all the values up to this newline into it.
                var isCarriageReturnLineFeed = false;
                switch (ch) {
                    case 13 /* carriageReturn */:
                        if (i < triviaText.length - 1 && triviaText.charCodeAt(i + 1) === 10 /* lineFeed */) {
                            // Consume the \r
                            i++;
                        }

                    case 10 /* lineFeed */:
                    case 8233 /* paragraphSeparator */:
                    case 8232 /* lineSeparator */:
                        // Eat from the last starting position through to the end of the newline.
                        result.push(triviaText.substring(currentIndex, i + 1));

                        // Set the current index to *after* the newline.
                        currentIndex = i + 1;
                        continue;
                }
            }

            result.push(triviaText.substring(currentIndex));
            return result;
        }
        Syntax.splitMultiLineCommentTriviaIntoMultipleLines = splitMultiLineCommentTriviaIntoMultipleLines;
    })(TypeScript.Syntax || (TypeScript.Syntax = {}));
    var Syntax = TypeScript.Syntax;
})(TypeScript || (TypeScript = {}));
