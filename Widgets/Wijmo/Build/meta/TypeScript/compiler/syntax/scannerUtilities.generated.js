///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    var ScannerUtilities = (function () {
        function ScannerUtilities() {
        }
        ScannerUtilities.identifierKind = function (array, startIndex, length) {
            switch (length) {
                case 2:
                    switch (array[startIndex]) {
                        case 100 /* d */:
                            // do
                            return (array[startIndex + 1] === 111 /* o */) ? 22 /* DoKeyword */ : 11 /* IdentifierName */;
                        case 105 /* i */:
                            switch (array[startIndex + 1]) {
                                case 102 /* f */:
                                    // if
                                    return 28 /* IfKeyword */;
                                case 110 /* n */:
                                    // in
                                    return 29 /* InKeyword */;
                                default:
                                    return 11 /* IdentifierName */;
                            }

                        default:
                            return 11 /* IdentifierName */;
                    }

                case 3:
                    switch (array[startIndex]) {
                        case 102 /* f */:
                            // for
                            return (array[startIndex + 1] === 111 /* o */ && array[startIndex + 2] === 114 /* r */) ? 26 /* ForKeyword */ : 11 /* IdentifierName */;
                        case 110 /* n */:
                            // new
                            return (array[startIndex + 1] === 101 /* e */ && array[startIndex + 2] === 119 /* w */) ? 31 /* NewKeyword */ : 11 /* IdentifierName */;
                        case 116 /* t */:
                            // try
                            return (array[startIndex + 1] === 114 /* r */ && array[startIndex + 2] === 121 /* y */) ? 38 /* TryKeyword */ : 11 /* IdentifierName */;
                        case 118 /* v */:
                            // var
                            return (array[startIndex + 1] === 97 /* a */ && array[startIndex + 2] === 114 /* r */) ? 40 /* VarKeyword */ : 11 /* IdentifierName */;
                        case 108 /* l */:
                            // let
                            return (array[startIndex + 1] === 101 /* e */ && array[startIndex + 2] === 116 /* t */) ? 53 /* LetKeyword */ : 11 /* IdentifierName */;
                        case 97 /* a */:
                            // any
                            return (array[startIndex + 1] === 110 /* n */ && array[startIndex + 2] === 121 /* y */) ? 60 /* AnyKeyword */ : 11 /* IdentifierName */;
                        case 103 /* g */:
                            // get
                            return (array[startIndex + 1] === 101 /* e */ && array[startIndex + 2] === 116 /* t */) ? 64 /* GetKeyword */ : 11 /* IdentifierName */;
                        case 115 /* s */:
                            // set
                            return (array[startIndex + 1] === 101 /* e */ && array[startIndex + 2] === 116 /* t */) ? 68 /* SetKeyword */ : 11 /* IdentifierName */;
                        default:
                            return 11 /* IdentifierName */;
                    }

                case 4:
                    switch (array[startIndex]) {
                        case 99 /* c */:
                            // case
                            return (array[startIndex + 1] === 97 /* a */ && array[startIndex + 2] === 115 /* s */ && array[startIndex + 3] === 101 /* e */) ? 16 /* CaseKeyword */ : 11 /* IdentifierName */;
                        case 101 /* e */:
                            switch (array[startIndex + 1]) {
                                case 108 /* l */:
                                    // else
                                    return (array[startIndex + 2] === 115 /* s */ && array[startIndex + 3] === 101 /* e */) ? 23 /* ElseKeyword */ : 11 /* IdentifierName */;
                                case 110 /* n */:
                                    // enum
                                    return (array[startIndex + 2] === 117 /* u */ && array[startIndex + 3] === 109 /* m */) ? 46 /* EnumKeyword */ : 11 /* IdentifierName */;
                                default:
                                    return 11 /* IdentifierName */;
                            }

                        case 110 /* n */:
                            // null
                            return (array[startIndex + 1] === 117 /* u */ && array[startIndex + 2] === 108 /* l */ && array[startIndex + 3] === 108 /* l */) ? 32 /* NullKeyword */ : 11 /* IdentifierName */;
                        case 116 /* t */:
                            switch (array[startIndex + 1]) {
                                case 104 /* h */:
                                    // this
                                    return (array[startIndex + 2] === 105 /* i */ && array[startIndex + 3] === 115 /* s */) ? 35 /* ThisKeyword */ : 11 /* IdentifierName */;
                                case 114 /* r */:
                                    // true
                                    return (array[startIndex + 2] === 117 /* u */ && array[startIndex + 3] === 101 /* e */) ? 37 /* TrueKeyword */ : 11 /* IdentifierName */;
                                default:
                                    return 11 /* IdentifierName */;
                            }

                        case 118 /* v */:
                            // void
                            return (array[startIndex + 1] === 111 /* o */ && array[startIndex + 2] === 105 /* i */ && array[startIndex + 3] === 100 /* d */) ? 41 /* VoidKeyword */ : 11 /* IdentifierName */;
                        case 119 /* w */:
                            // with
                            return (array[startIndex + 1] === 105 /* i */ && array[startIndex + 2] === 116 /* t */ && array[startIndex + 3] === 104 /* h */) ? 43 /* WithKeyword */ : 11 /* IdentifierName */;
                        default:
                            return 11 /* IdentifierName */;
                    }

                case 5:
                    switch (array[startIndex]) {
                        case 98 /* b */:
                            // break
                            return (array[startIndex + 1] === 114 /* r */ && array[startIndex + 2] === 101 /* e */ && array[startIndex + 3] === 97 /* a */ && array[startIndex + 4] === 107 /* k */) ? 15 /* BreakKeyword */ : 11 /* IdentifierName */;
                        case 99 /* c */:
                            switch (array[startIndex + 1]) {
                                case 97 /* a */:
                                    // catch
                                    return (array[startIndex + 2] === 116 /* t */ && array[startIndex + 3] === 99 /* c */ && array[startIndex + 4] === 104 /* h */) ? 17 /* CatchKeyword */ : 11 /* IdentifierName */;
                                case 108 /* l */:
                                    // class
                                    return (array[startIndex + 2] === 97 /* a */ && array[startIndex + 3] === 115 /* s */ && array[startIndex + 4] === 115 /* s */) ? 44 /* ClassKeyword */ : 11 /* IdentifierName */;
                                case 111 /* o */:
                                    // const
                                    return (array[startIndex + 2] === 110 /* n */ && array[startIndex + 3] === 115 /* s */ && array[startIndex + 4] === 116 /* t */) ? 45 /* ConstKeyword */ : 11 /* IdentifierName */;
                                default:
                                    return 11 /* IdentifierName */;
                            }

                        case 102 /* f */:
                            // false
                            return (array[startIndex + 1] === 97 /* a */ && array[startIndex + 2] === 108 /* l */ && array[startIndex + 3] === 115 /* s */ && array[startIndex + 4] === 101 /* e */) ? 24 /* FalseKeyword */ : 11 /* IdentifierName */;
                        case 116 /* t */:
                            // throw
                            return (array[startIndex + 1] === 104 /* h */ && array[startIndex + 2] === 114 /* r */ && array[startIndex + 3] === 111 /* o */ && array[startIndex + 4] === 119 /* w */) ? 36 /* ThrowKeyword */ : 11 /* IdentifierName */;
                        case 119 /* w */:
                            // while
                            return (array[startIndex + 1] === 104 /* h */ && array[startIndex + 2] === 105 /* i */ && array[startIndex + 3] === 108 /* l */ && array[startIndex + 4] === 101 /* e */) ? 42 /* WhileKeyword */ : 11 /* IdentifierName */;
                        case 115 /* s */:
                            // super
                            return (array[startIndex + 1] === 117 /* u */ && array[startIndex + 2] === 112 /* p */ && array[startIndex + 3] === 101 /* e */ && array[startIndex + 4] === 114 /* r */) ? 50 /* SuperKeyword */ : 11 /* IdentifierName */;
                        case 121 /* y */:
                            // yield
                            return (array[startIndex + 1] === 105 /* i */ && array[startIndex + 2] === 101 /* e */ && array[startIndex + 3] === 108 /* l */ && array[startIndex + 4] === 100 /* d */) ? 59 /* YieldKeyword */ : 11 /* IdentifierName */;
                        default:
                            return 11 /* IdentifierName */;
                    }

                case 6:
                    switch (array[startIndex]) {
                        case 100 /* d */:
                            // delete
                            return (array[startIndex + 1] === 101 /* e */ && array[startIndex + 2] === 108 /* l */ && array[startIndex + 3] === 101 /* e */ && array[startIndex + 4] === 116 /* t */ && array[startIndex + 5] === 101 /* e */) ? 21 /* DeleteKeyword */ : 11 /* IdentifierName */;
                        case 114 /* r */:
                            // return
                            return (array[startIndex + 1] === 101 /* e */ && array[startIndex + 2] === 116 /* t */ && array[startIndex + 3] === 117 /* u */ && array[startIndex + 4] === 114 /* r */ && array[startIndex + 5] === 110 /* n */) ? 33 /* ReturnKeyword */ : 11 /* IdentifierName */;
                        case 115 /* s */:
                            switch (array[startIndex + 1]) {
                                case 119 /* w */:
                                    // switch
                                    return (array[startIndex + 2] === 105 /* i */ && array[startIndex + 3] === 116 /* t */ && array[startIndex + 4] === 99 /* c */ && array[startIndex + 5] === 104 /* h */) ? 34 /* SwitchKeyword */ : 11 /* IdentifierName */;
                                case 116 /* t */:
                                    switch (array[startIndex + 2]) {
                                        case 97 /* a */:
                                            // static
                                            return (array[startIndex + 3] === 116 /* t */ && array[startIndex + 4] === 105 /* i */ && array[startIndex + 5] === 99 /* c */) ? 58 /* StaticKeyword */ : 11 /* IdentifierName */;
                                        case 114 /* r */:
                                            // string
                                            return (array[startIndex + 3] === 105 /* i */ && array[startIndex + 4] === 110 /* n */ && array[startIndex + 5] === 103 /* g */) ? 69 /* StringKeyword */ : 11 /* IdentifierName */;
                                        default:
                                            return 11 /* IdentifierName */;
                                    }

                                default:
                                    return 11 /* IdentifierName */;
                            }

                        case 116 /* t */:
                            // typeof
                            return (array[startIndex + 1] === 121 /* y */ && array[startIndex + 2] === 112 /* p */ && array[startIndex + 3] === 101 /* e */ && array[startIndex + 4] === 111 /* o */ && array[startIndex + 5] === 102 /* f */) ? 39 /* TypeOfKeyword */ : 11 /* IdentifierName */;
                        case 101 /* e */:
                            // export
                            return (array[startIndex + 1] === 120 /* x */ && array[startIndex + 2] === 112 /* p */ && array[startIndex + 3] === 111 /* o */ && array[startIndex + 4] === 114 /* r */ && array[startIndex + 5] === 116 /* t */) ? 47 /* ExportKeyword */ : 11 /* IdentifierName */;
                        case 105 /* i */:
                            // import
                            return (array[startIndex + 1] === 109 /* m */ && array[startIndex + 2] === 112 /* p */ && array[startIndex + 3] === 111 /* o */ && array[startIndex + 4] === 114 /* r */ && array[startIndex + 5] === 116 /* t */) ? 49 /* ImportKeyword */ : 11 /* IdentifierName */;
                        case 112 /* p */:
                            // public
                            return (array[startIndex + 1] === 117 /* u */ && array[startIndex + 2] === 98 /* b */ && array[startIndex + 3] === 108 /* l */ && array[startIndex + 4] === 105 /* i */ && array[startIndex + 5] === 99 /* c */) ? 57 /* PublicKeyword */ : 11 /* IdentifierName */;
                        case 109 /* m */:
                            // module
                            return (array[startIndex + 1] === 111 /* o */ && array[startIndex + 2] === 100 /* d */ && array[startIndex + 3] === 117 /* u */ && array[startIndex + 4] === 108 /* l */ && array[startIndex + 5] === 101 /* e */) ? 65 /* ModuleKeyword */ : 11 /* IdentifierName */;
                        case 110 /* n */:
                            // number
                            return (array[startIndex + 1] === 117 /* u */ && array[startIndex + 2] === 109 /* m */ && array[startIndex + 3] === 98 /* b */ && array[startIndex + 4] === 101 /* e */ && array[startIndex + 5] === 114 /* r */) ? 67 /* NumberKeyword */ : 11 /* IdentifierName */;
                        default:
                            return 11 /* IdentifierName */;
                    }

                case 7:
                    switch (array[startIndex]) {
                        case 100 /* d */:
                            switch (array[startIndex + 1]) {
                                case 101 /* e */:
                                    switch (array[startIndex + 2]) {
                                        case 102 /* f */:
                                            // default
                                            return (array[startIndex + 3] === 97 /* a */ && array[startIndex + 4] === 117 /* u */ && array[startIndex + 5] === 108 /* l */ && array[startIndex + 6] === 116 /* t */) ? 20 /* DefaultKeyword */ : 11 /* IdentifierName */;
                                        case 99 /* c */:
                                            // declare
                                            return (array[startIndex + 3] === 108 /* l */ && array[startIndex + 4] === 97 /* a */ && array[startIndex + 5] === 114 /* r */ && array[startIndex + 6] === 101 /* e */) ? 63 /* DeclareKeyword */ : 11 /* IdentifierName */;
                                        default:
                                            return 11 /* IdentifierName */;
                                    }

                                default:
                                    return 11 /* IdentifierName */;
                            }

                        case 102 /* f */:
                            // finally
                            return (array[startIndex + 1] === 105 /* i */ && array[startIndex + 2] === 110 /* n */ && array[startIndex + 3] === 97 /* a */ && array[startIndex + 4] === 108 /* l */ && array[startIndex + 5] === 108 /* l */ && array[startIndex + 6] === 121 /* y */) ? 25 /* FinallyKeyword */ : 11 /* IdentifierName */;
                        case 101 /* e */:
                            // extends
                            return (array[startIndex + 1] === 120 /* x */ && array[startIndex + 2] === 116 /* t */ && array[startIndex + 3] === 101 /* e */ && array[startIndex + 4] === 110 /* n */ && array[startIndex + 5] === 100 /* d */ && array[startIndex + 6] === 115 /* s */) ? 48 /* ExtendsKeyword */ : 11 /* IdentifierName */;
                        case 112 /* p */:
                            switch (array[startIndex + 1]) {
                                case 97 /* a */:
                                    // package
                                    return (array[startIndex + 2] === 99 /* c */ && array[startIndex + 3] === 107 /* k */ && array[startIndex + 4] === 97 /* a */ && array[startIndex + 5] === 103 /* g */ && array[startIndex + 6] === 101 /* e */) ? 54 /* PackageKeyword */ : 11 /* IdentifierName */;
                                case 114 /* r */:
                                    // private
                                    return (array[startIndex + 2] === 105 /* i */ && array[startIndex + 3] === 118 /* v */ && array[startIndex + 4] === 97 /* a */ && array[startIndex + 5] === 116 /* t */ && array[startIndex + 6] === 101 /* e */) ? 55 /* PrivateKeyword */ : 11 /* IdentifierName */;
                                default:
                                    return 11 /* IdentifierName */;
                            }

                        case 98 /* b */:
                            // boolean
                            return (array[startIndex + 1] === 111 /* o */ && array[startIndex + 2] === 111 /* o */ && array[startIndex + 3] === 108 /* l */ && array[startIndex + 4] === 101 /* e */ && array[startIndex + 5] === 97 /* a */ && array[startIndex + 6] === 110 /* n */) ? 61 /* BooleanKeyword */ : 11 /* IdentifierName */;
                        case 114 /* r */:
                            // require
                            return (array[startIndex + 1] === 101 /* e */ && array[startIndex + 2] === 113 /* q */ && array[startIndex + 3] === 117 /* u */ && array[startIndex + 4] === 105 /* i */ && array[startIndex + 5] === 114 /* r */ && array[startIndex + 6] === 101 /* e */) ? 66 /* RequireKeyword */ : 11 /* IdentifierName */;
                        default:
                            return 11 /* IdentifierName */;
                    }

                case 8:
                    switch (array[startIndex]) {
                        case 99 /* c */:
                            // continue
                            return (array[startIndex + 1] === 111 /* o */ && array[startIndex + 2] === 110 /* n */ && array[startIndex + 3] === 116 /* t */ && array[startIndex + 4] === 105 /* i */ && array[startIndex + 5] === 110 /* n */ && array[startIndex + 6] === 117 /* u */ && array[startIndex + 7] === 101 /* e */) ? 18 /* ContinueKeyword */ : 11 /* IdentifierName */;
                        case 100 /* d */:
                            // debugger
                            return (array[startIndex + 1] === 101 /* e */ && array[startIndex + 2] === 98 /* b */ && array[startIndex + 3] === 117 /* u */ && array[startIndex + 4] === 103 /* g */ && array[startIndex + 5] === 103 /* g */ && array[startIndex + 6] === 101 /* e */ && array[startIndex + 7] === 114 /* r */) ? 19 /* DebuggerKeyword */ : 11 /* IdentifierName */;
                        case 102 /* f */:
                            // function
                            return (array[startIndex + 1] === 117 /* u */ && array[startIndex + 2] === 110 /* n */ && array[startIndex + 3] === 99 /* c */ && array[startIndex + 4] === 116 /* t */ && array[startIndex + 5] === 105 /* i */ && array[startIndex + 6] === 111 /* o */ && array[startIndex + 7] === 110 /* n */) ? 27 /* FunctionKeyword */ : 11 /* IdentifierName */;
                        default:
                            return 11 /* IdentifierName */;
                    }

                case 9:
                    switch (array[startIndex]) {
                        case 105 /* i */:
                            // interface
                            return (array[startIndex + 1] === 110 /* n */ && array[startIndex + 2] === 116 /* t */ && array[startIndex + 3] === 101 /* e */ && array[startIndex + 4] === 114 /* r */ && array[startIndex + 5] === 102 /* f */ && array[startIndex + 6] === 97 /* a */ && array[startIndex + 7] === 99 /* c */ && array[startIndex + 8] === 101 /* e */) ? 52 /* InterfaceKeyword */ : 11 /* IdentifierName */;
                        case 112 /* p */:
                            // protected
                            return (array[startIndex + 1] === 114 /* r */ && array[startIndex + 2] === 111 /* o */ && array[startIndex + 3] === 116 /* t */ && array[startIndex + 4] === 101 /* e */ && array[startIndex + 5] === 99 /* c */ && array[startIndex + 6] === 116 /* t */ && array[startIndex + 7] === 101 /* e */ && array[startIndex + 8] === 100 /* d */) ? 56 /* ProtectedKeyword */ : 11 /* IdentifierName */;
                        default:
                            return 11 /* IdentifierName */;
                    }

                case 10:
                    switch (array[startIndex]) {
                        case 105 /* i */:
                            switch (array[startIndex + 1]) {
                                case 110 /* n */:
                                    // instanceof
                                    return (array[startIndex + 2] === 115 /* s */ && array[startIndex + 3] === 116 /* t */ && array[startIndex + 4] === 97 /* a */ && array[startIndex + 5] === 110 /* n */ && array[startIndex + 6] === 99 /* c */ && array[startIndex + 7] === 101 /* e */ && array[startIndex + 8] === 111 /* o */ && array[startIndex + 9] === 102 /* f */) ? 30 /* InstanceOfKeyword */ : 11 /* IdentifierName */;
                                case 109 /* m */:
                                    // implements
                                    return (array[startIndex + 2] === 112 /* p */ && array[startIndex + 3] === 108 /* l */ && array[startIndex + 4] === 101 /* e */ && array[startIndex + 5] === 109 /* m */ && array[startIndex + 6] === 101 /* e */ && array[startIndex + 7] === 110 /* n */ && array[startIndex + 8] === 116 /* t */ && array[startIndex + 9] === 115 /* s */) ? 51 /* ImplementsKeyword */ : 11 /* IdentifierName */;
                                default:
                                    return 11 /* IdentifierName */;
                            }

                        default:
                            return 11 /* IdentifierName */;
                    }

                case 11:
                    // constructor
                    return (array[startIndex] === 99 /* c */ && array[startIndex + 1] === 111 /* o */ && array[startIndex + 2] === 110 /* n */ && array[startIndex + 3] === 115 /* s */ && array[startIndex + 4] === 116 /* t */ && array[startIndex + 5] === 114 /* r */ && array[startIndex + 6] === 117 /* u */ && array[startIndex + 7] === 99 /* c */ && array[startIndex + 8] === 116 /* t */ && array[startIndex + 9] === 111 /* o */ && array[startIndex + 10] === 114 /* r */) ? 62 /* ConstructorKeyword */ : 11 /* IdentifierName */;
                default:
                    return 11 /* IdentifierName */;
            }
        };
        return ScannerUtilities;
    })();
    TypeScript.ScannerUtilities = ScannerUtilities;
})(TypeScript || (TypeScript = {}));
