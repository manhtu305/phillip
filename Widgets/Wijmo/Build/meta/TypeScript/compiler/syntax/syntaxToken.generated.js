///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    (function (Syntax) {
        var VariableWidthTokenWithNoTrivia = (function () {
            function VariableWidthTokenWithNoTrivia(fullText, kind) {
                this._fullText = fullText;
                this.tokenKind = kind;
            }
            VariableWidthTokenWithNoTrivia.prototype.clone = function () {
                return new VariableWidthTokenWithNoTrivia(this._fullText, this.tokenKind);
            };

            VariableWidthTokenWithNoTrivia.prototype.isNode = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.isToken = function () {
                return true;
            };
            VariableWidthTokenWithNoTrivia.prototype.isList = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.isSeparatedList = function () {
                return false;
            };

            VariableWidthTokenWithNoTrivia.prototype.kind = function () {
                return this.tokenKind;
            };

            VariableWidthTokenWithNoTrivia.prototype.childCount = function () {
                return 0;
            };
            VariableWidthTokenWithNoTrivia.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange('index');
            };

            VariableWidthTokenWithNoTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };
            VariableWidthTokenWithNoTrivia.prototype.width = function () {
                return this.fullWidth() - this.leadingTriviaWidth() - this.trailingTriviaWidth();
            };

            VariableWidthTokenWithNoTrivia.prototype.text = function () {
                return this.fullText().substr(this.leadingTriviaWidth(), this.width());
            };
            VariableWidthTokenWithNoTrivia.prototype.fullText = function () {
                return this._fullText;
            };

            VariableWidthTokenWithNoTrivia.prototype.value = function () {
                if (this._value === undefined) {
                    this._value = Syntax.value(this);
                }

                return this._value;
            };

            VariableWidthTokenWithNoTrivia.prototype.valueText = function () {
                if (this._valueText === undefined) {
                    this._valueText = Syntax.valueText(this);
                }

                return this._valueText;
            };

            VariableWidthTokenWithNoTrivia.prototype.hasLeadingTrivia = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.hasLeadingComment = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.hasLeadingNewLine = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.leadingTriviaWidth = function () {
                return 0;
            };
            VariableWidthTokenWithNoTrivia.prototype.leadingTrivia = function () {
                return Syntax.emptyTriviaList;
            };

            VariableWidthTokenWithNoTrivia.prototype.hasTrailingTrivia = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.hasTrailingComment = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.hasTrailingNewLine = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.trailingTriviaWidth = function () {
                return 0;
            };
            VariableWidthTokenWithNoTrivia.prototype.trailingTrivia = function () {
                return Syntax.emptyTriviaList;
            };

            VariableWidthTokenWithNoTrivia.prototype.hasSkippedToken = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.toJSON = function (key) {
                return Syntax.tokenToJSON(this);
            };
            VariableWidthTokenWithNoTrivia.prototype.firstToken = function () {
                return this;
            };
            VariableWidthTokenWithNoTrivia.prototype.lastToken = function () {
                return this;
            };
            VariableWidthTokenWithNoTrivia.prototype.isTypeScriptSpecific = function () {
                return false;
            };
            VariableWidthTokenWithNoTrivia.prototype.isIncrementallyUnusable = function () {
                return this.fullWidth() === 0 || TypeScript.SyntaxFacts.isAnyDivideOrRegularExpressionToken(this.tokenKind);
            };
            VariableWidthTokenWithNoTrivia.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };
            VariableWidthTokenWithNoTrivia.prototype.realize = function () {
                return Syntax.realizeToken(this);
            };
            VariableWidthTokenWithNoTrivia.prototype.collectTextElements = function (elements) {
                collectTokenTextElements(this, elements);
            };

            VariableWidthTokenWithNoTrivia.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            VariableWidthTokenWithNoTrivia.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            VariableWidthTokenWithNoTrivia.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            VariableWidthTokenWithNoTrivia.prototype.isExpression = function () {
                return Syntax.isExpression(this);
            };

            VariableWidthTokenWithNoTrivia.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithNoTrivia.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithNoTrivia.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithNoTrivia.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return VariableWidthTokenWithNoTrivia;
        })();
        Syntax.VariableWidthTokenWithNoTrivia = VariableWidthTokenWithNoTrivia;

        var VariableWidthTokenWithLeadingTrivia = (function () {
            function VariableWidthTokenWithLeadingTrivia(fullText, kind, leadingTriviaInfo) {
                this._fullText = fullText;
                this.tokenKind = kind;
                this._leadingTriviaInfo = leadingTriviaInfo;
            }
            VariableWidthTokenWithLeadingTrivia.prototype.clone = function () {
                return new VariableWidthTokenWithLeadingTrivia(this._fullText, this.tokenKind, this._leadingTriviaInfo);
            };

            VariableWidthTokenWithLeadingTrivia.prototype.isNode = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.isToken = function () {
                return true;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.isList = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.isSeparatedList = function () {
                return false;
            };

            VariableWidthTokenWithLeadingTrivia.prototype.kind = function () {
                return this.tokenKind;
            };

            VariableWidthTokenWithLeadingTrivia.prototype.childCount = function () {
                return 0;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange('index');
            };

            VariableWidthTokenWithLeadingTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.width = function () {
                return this.fullWidth() - this.leadingTriviaWidth() - this.trailingTriviaWidth();
            };

            VariableWidthTokenWithLeadingTrivia.prototype.text = function () {
                return this.fullText().substr(this.leadingTriviaWidth(), this.width());
            };
            VariableWidthTokenWithLeadingTrivia.prototype.fullText = function () {
                return this._fullText;
            };

            VariableWidthTokenWithLeadingTrivia.prototype.value = function () {
                if (this._value === undefined) {
                    this._value = Syntax.value(this);
                }

                return this._value;
            };

            VariableWidthTokenWithLeadingTrivia.prototype.valueText = function () {
                if (this._valueText === undefined) {
                    this._valueText = Syntax.valueText(this);
                }

                return this._valueText;
            };

            VariableWidthTokenWithLeadingTrivia.prototype.hasLeadingTrivia = function () {
                return true;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.hasLeadingComment = function () {
                return hasTriviaComment(this._leadingTriviaInfo);
            };
            VariableWidthTokenWithLeadingTrivia.prototype.hasLeadingNewLine = function () {
                return hasTriviaNewLine(this._leadingTriviaInfo);
            };
            VariableWidthTokenWithLeadingTrivia.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.leadingTriviaWidth = function () {
                return getTriviaWidth(this._leadingTriviaInfo);
            };
            VariableWidthTokenWithLeadingTrivia.prototype.leadingTrivia = function () {
                return TypeScript.Scanner.scanTrivia(TypeScript.SimpleText.fromString(this._fullText), 0, getTriviaWidth(this._leadingTriviaInfo), false);
            };

            VariableWidthTokenWithLeadingTrivia.prototype.hasTrailingTrivia = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.hasTrailingComment = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.hasTrailingNewLine = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.trailingTriviaWidth = function () {
                return 0;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.trailingTrivia = function () {
                return Syntax.emptyTriviaList;
            };

            VariableWidthTokenWithLeadingTrivia.prototype.hasSkippedToken = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.toJSON = function (key) {
                return Syntax.tokenToJSON(this);
            };
            VariableWidthTokenWithLeadingTrivia.prototype.firstToken = function () {
                return this;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.lastToken = function () {
                return this;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.isTypeScriptSpecific = function () {
                return false;
            };
            VariableWidthTokenWithLeadingTrivia.prototype.isIncrementallyUnusable = function () {
                return this.fullWidth() === 0 || TypeScript.SyntaxFacts.isAnyDivideOrRegularExpressionToken(this.tokenKind);
            };
            VariableWidthTokenWithLeadingTrivia.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };
            VariableWidthTokenWithLeadingTrivia.prototype.realize = function () {
                return Syntax.realizeToken(this);
            };
            VariableWidthTokenWithLeadingTrivia.prototype.collectTextElements = function (elements) {
                collectTokenTextElements(this, elements);
            };

            VariableWidthTokenWithLeadingTrivia.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            VariableWidthTokenWithLeadingTrivia.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            VariableWidthTokenWithLeadingTrivia.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            VariableWidthTokenWithLeadingTrivia.prototype.isExpression = function () {
                return Syntax.isExpression(this);
            };

            VariableWidthTokenWithLeadingTrivia.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithLeadingTrivia.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithLeadingTrivia.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithLeadingTrivia.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return VariableWidthTokenWithLeadingTrivia;
        })();
        Syntax.VariableWidthTokenWithLeadingTrivia = VariableWidthTokenWithLeadingTrivia;

        var VariableWidthTokenWithTrailingTrivia = (function () {
            function VariableWidthTokenWithTrailingTrivia(fullText, kind, trailingTriviaInfo) {
                this._fullText = fullText;
                this.tokenKind = kind;
                this._trailingTriviaInfo = trailingTriviaInfo;
            }
            VariableWidthTokenWithTrailingTrivia.prototype.clone = function () {
                return new VariableWidthTokenWithTrailingTrivia(this._fullText, this.tokenKind, this._trailingTriviaInfo);
            };

            VariableWidthTokenWithTrailingTrivia.prototype.isNode = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.isToken = function () {
                return true;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.isList = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.isSeparatedList = function () {
                return false;
            };

            VariableWidthTokenWithTrailingTrivia.prototype.kind = function () {
                return this.tokenKind;
            };

            VariableWidthTokenWithTrailingTrivia.prototype.childCount = function () {
                return 0;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange('index');
            };

            VariableWidthTokenWithTrailingTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.width = function () {
                return this.fullWidth() - this.leadingTriviaWidth() - this.trailingTriviaWidth();
            };

            VariableWidthTokenWithTrailingTrivia.prototype.text = function () {
                return this.fullText().substr(this.leadingTriviaWidth(), this.width());
            };
            VariableWidthTokenWithTrailingTrivia.prototype.fullText = function () {
                return this._fullText;
            };

            VariableWidthTokenWithTrailingTrivia.prototype.value = function () {
                if (this._value === undefined) {
                    this._value = Syntax.value(this);
                }

                return this._value;
            };

            VariableWidthTokenWithTrailingTrivia.prototype.valueText = function () {
                if (this._valueText === undefined) {
                    this._valueText = Syntax.valueText(this);
                }

                return this._valueText;
            };

            VariableWidthTokenWithTrailingTrivia.prototype.hasLeadingTrivia = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.hasLeadingComment = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.hasLeadingNewLine = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.leadingTriviaWidth = function () {
                return 0;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.leadingTrivia = function () {
                return Syntax.emptyTriviaList;
            };

            VariableWidthTokenWithTrailingTrivia.prototype.hasTrailingTrivia = function () {
                return true;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.hasTrailingComment = function () {
                return hasTriviaComment(this._trailingTriviaInfo);
            };
            VariableWidthTokenWithTrailingTrivia.prototype.hasTrailingNewLine = function () {
                return hasTriviaNewLine(this._trailingTriviaInfo);
            };
            VariableWidthTokenWithTrailingTrivia.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.trailingTriviaWidth = function () {
                return getTriviaWidth(this._trailingTriviaInfo);
            };
            VariableWidthTokenWithTrailingTrivia.prototype.trailingTrivia = function () {
                return TypeScript.Scanner.scanTrivia(TypeScript.SimpleText.fromString(this._fullText), this.leadingTriviaWidth() + this.width(), getTriviaWidth(this._trailingTriviaInfo), true);
            };

            VariableWidthTokenWithTrailingTrivia.prototype.hasSkippedToken = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.toJSON = function (key) {
                return Syntax.tokenToJSON(this);
            };
            VariableWidthTokenWithTrailingTrivia.prototype.firstToken = function () {
                return this;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.lastToken = function () {
                return this;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.isTypeScriptSpecific = function () {
                return false;
            };
            VariableWidthTokenWithTrailingTrivia.prototype.isIncrementallyUnusable = function () {
                return this.fullWidth() === 0 || TypeScript.SyntaxFacts.isAnyDivideOrRegularExpressionToken(this.tokenKind);
            };
            VariableWidthTokenWithTrailingTrivia.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };
            VariableWidthTokenWithTrailingTrivia.prototype.realize = function () {
                return Syntax.realizeToken(this);
            };
            VariableWidthTokenWithTrailingTrivia.prototype.collectTextElements = function (elements) {
                collectTokenTextElements(this, elements);
            };

            VariableWidthTokenWithTrailingTrivia.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            VariableWidthTokenWithTrailingTrivia.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            VariableWidthTokenWithTrailingTrivia.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            VariableWidthTokenWithTrailingTrivia.prototype.isExpression = function () {
                return Syntax.isExpression(this);
            };

            VariableWidthTokenWithTrailingTrivia.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithTrailingTrivia.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithTrailingTrivia.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithTrailingTrivia.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return VariableWidthTokenWithTrailingTrivia;
        })();
        Syntax.VariableWidthTokenWithTrailingTrivia = VariableWidthTokenWithTrailingTrivia;

        var VariableWidthTokenWithLeadingAndTrailingTrivia = (function () {
            function VariableWidthTokenWithLeadingAndTrailingTrivia(fullText, kind, leadingTriviaInfo, trailingTriviaInfo) {
                this._fullText = fullText;
                this.tokenKind = kind;
                this._leadingTriviaInfo = leadingTriviaInfo;
                this._trailingTriviaInfo = trailingTriviaInfo;
            }
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.clone = function () {
                return new VariableWidthTokenWithLeadingAndTrailingTrivia(this._fullText, this.tokenKind, this._leadingTriviaInfo, this._trailingTriviaInfo);
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isNode = function () {
                return false;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isToken = function () {
                return true;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isList = function () {
                return false;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isSeparatedList = function () {
                return false;
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.kind = function () {
                return this.tokenKind;
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.childCount = function () {
                return 0;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange('index');
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.width = function () {
                return this.fullWidth() - this.leadingTriviaWidth() - this.trailingTriviaWidth();
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.text = function () {
                return this.fullText().substr(this.leadingTriviaWidth(), this.width());
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.fullText = function () {
                return this._fullText;
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.value = function () {
                if (this._value === undefined) {
                    this._value = Syntax.value(this);
                }

                return this._value;
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.valueText = function () {
                if (this._valueText === undefined) {
                    this._valueText = Syntax.valueText(this);
                }

                return this._valueText;
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasLeadingTrivia = function () {
                return true;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasLeadingComment = function () {
                return hasTriviaComment(this._leadingTriviaInfo);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasLeadingNewLine = function () {
                return hasTriviaNewLine(this._leadingTriviaInfo);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.leadingTriviaWidth = function () {
                return getTriviaWidth(this._leadingTriviaInfo);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.leadingTrivia = function () {
                return TypeScript.Scanner.scanTrivia(TypeScript.SimpleText.fromString(this._fullText), 0, getTriviaWidth(this._leadingTriviaInfo), false);
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasTrailingTrivia = function () {
                return true;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasTrailingComment = function () {
                return hasTriviaComment(this._trailingTriviaInfo);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasTrailingNewLine = function () {
                return hasTriviaNewLine(this._trailingTriviaInfo);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.trailingTriviaWidth = function () {
                return getTriviaWidth(this._trailingTriviaInfo);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.trailingTrivia = function () {
                return TypeScript.Scanner.scanTrivia(TypeScript.SimpleText.fromString(this._fullText), this.leadingTriviaWidth() + this.width(), getTriviaWidth(this._trailingTriviaInfo), true);
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.hasSkippedToken = function () {
                return false;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.toJSON = function (key) {
                return Syntax.tokenToJSON(this);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.firstToken = function () {
                return this;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.lastToken = function () {
                return this;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isTypeScriptSpecific = function () {
                return false;
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isIncrementallyUnusable = function () {
                return this.fullWidth() === 0 || TypeScript.SyntaxFacts.isAnyDivideOrRegularExpressionToken(this.tokenKind);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.realize = function () {
                return Syntax.realizeToken(this);
            };
            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.collectTextElements = function (elements) {
                collectTokenTextElements(this, elements);
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isExpression = function () {
                return Syntax.isExpression(this);
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            VariableWidthTokenWithLeadingAndTrailingTrivia.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return VariableWidthTokenWithLeadingAndTrailingTrivia;
        })();
        Syntax.VariableWidthTokenWithLeadingAndTrailingTrivia = VariableWidthTokenWithLeadingAndTrailingTrivia;

        var FixedWidthTokenWithNoTrivia = (function () {
            function FixedWidthTokenWithNoTrivia(kind) {
                this.tokenKind = kind;
            }
            FixedWidthTokenWithNoTrivia.prototype.clone = function () {
                return new FixedWidthTokenWithNoTrivia(this.tokenKind);
            };

            FixedWidthTokenWithNoTrivia.prototype.isNode = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.isToken = function () {
                return true;
            };
            FixedWidthTokenWithNoTrivia.prototype.isList = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.isSeparatedList = function () {
                return false;
            };

            FixedWidthTokenWithNoTrivia.prototype.kind = function () {
                return this.tokenKind;
            };

            FixedWidthTokenWithNoTrivia.prototype.childCount = function () {
                return 0;
            };
            FixedWidthTokenWithNoTrivia.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange('index');
            };

            FixedWidthTokenWithNoTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };
            FixedWidthTokenWithNoTrivia.prototype.width = function () {
                return this.text().length;
            };

            FixedWidthTokenWithNoTrivia.prototype.text = function () {
                return TypeScript.SyntaxFacts.getText(this.tokenKind);
            };
            FixedWidthTokenWithNoTrivia.prototype.fullText = function () {
                return this.text();
            };

            FixedWidthTokenWithNoTrivia.prototype.value = function () {
                return Syntax.value(this);
            };
            FixedWidthTokenWithNoTrivia.prototype.valueText = function () {
                return Syntax.valueText(this);
            };
            FixedWidthTokenWithNoTrivia.prototype.hasLeadingTrivia = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.hasLeadingComment = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.hasLeadingNewLine = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.leadingTriviaWidth = function () {
                return 0;
            };
            FixedWidthTokenWithNoTrivia.prototype.leadingTrivia = function () {
                return Syntax.emptyTriviaList;
            };

            FixedWidthTokenWithNoTrivia.prototype.hasTrailingTrivia = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.hasTrailingComment = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.hasTrailingNewLine = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.trailingTriviaWidth = function () {
                return 0;
            };
            FixedWidthTokenWithNoTrivia.prototype.trailingTrivia = function () {
                return Syntax.emptyTriviaList;
            };

            FixedWidthTokenWithNoTrivia.prototype.hasSkippedToken = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.toJSON = function (key) {
                return Syntax.tokenToJSON(this);
            };
            FixedWidthTokenWithNoTrivia.prototype.firstToken = function () {
                return this;
            };
            FixedWidthTokenWithNoTrivia.prototype.lastToken = function () {
                return this;
            };
            FixedWidthTokenWithNoTrivia.prototype.isTypeScriptSpecific = function () {
                return false;
            };
            FixedWidthTokenWithNoTrivia.prototype.isIncrementallyUnusable = function () {
                return this.fullWidth() === 0 || TypeScript.SyntaxFacts.isAnyDivideOrRegularExpressionToken(this.tokenKind);
            };
            FixedWidthTokenWithNoTrivia.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };
            FixedWidthTokenWithNoTrivia.prototype.realize = function () {
                return Syntax.realizeToken(this);
            };
            FixedWidthTokenWithNoTrivia.prototype.collectTextElements = function (elements) {
                collectTokenTextElements(this, elements);
            };

            FixedWidthTokenWithNoTrivia.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            FixedWidthTokenWithNoTrivia.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            FixedWidthTokenWithNoTrivia.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            FixedWidthTokenWithNoTrivia.prototype.isExpression = function () {
                return Syntax.isExpression(this);
            };

            FixedWidthTokenWithNoTrivia.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithNoTrivia.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithNoTrivia.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithNoTrivia.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return FixedWidthTokenWithNoTrivia;
        })();
        Syntax.FixedWidthTokenWithNoTrivia = FixedWidthTokenWithNoTrivia;

        var FixedWidthTokenWithLeadingTrivia = (function () {
            function FixedWidthTokenWithLeadingTrivia(fullText, kind, leadingTriviaInfo) {
                this._fullText = fullText;
                this.tokenKind = kind;
                this._leadingTriviaInfo = leadingTriviaInfo;
            }
            FixedWidthTokenWithLeadingTrivia.prototype.clone = function () {
                return new FixedWidthTokenWithLeadingTrivia(this._fullText, this.tokenKind, this._leadingTriviaInfo);
            };

            FixedWidthTokenWithLeadingTrivia.prototype.isNode = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.isToken = function () {
                return true;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.isList = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.isSeparatedList = function () {
                return false;
            };

            FixedWidthTokenWithLeadingTrivia.prototype.kind = function () {
                return this.tokenKind;
            };

            FixedWidthTokenWithLeadingTrivia.prototype.childCount = function () {
                return 0;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange('index');
            };

            FixedWidthTokenWithLeadingTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.width = function () {
                return this.text().length;
            };

            FixedWidthTokenWithLeadingTrivia.prototype.text = function () {
                return TypeScript.SyntaxFacts.getText(this.tokenKind);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.fullText = function () {
                return this._fullText;
            };

            FixedWidthTokenWithLeadingTrivia.prototype.value = function () {
                return Syntax.value(this);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.valueText = function () {
                return Syntax.valueText(this);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.hasLeadingTrivia = function () {
                return true;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.hasLeadingComment = function () {
                return hasTriviaComment(this._leadingTriviaInfo);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.hasLeadingNewLine = function () {
                return hasTriviaNewLine(this._leadingTriviaInfo);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.leadingTriviaWidth = function () {
                return getTriviaWidth(this._leadingTriviaInfo);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.leadingTrivia = function () {
                return TypeScript.Scanner.scanTrivia(TypeScript.SimpleText.fromString(this._fullText), 0, getTriviaWidth(this._leadingTriviaInfo), false);
            };

            FixedWidthTokenWithLeadingTrivia.prototype.hasTrailingTrivia = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.hasTrailingComment = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.hasTrailingNewLine = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.trailingTriviaWidth = function () {
                return 0;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.trailingTrivia = function () {
                return Syntax.emptyTriviaList;
            };

            FixedWidthTokenWithLeadingTrivia.prototype.hasSkippedToken = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.toJSON = function (key) {
                return Syntax.tokenToJSON(this);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.firstToken = function () {
                return this;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.lastToken = function () {
                return this;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.isTypeScriptSpecific = function () {
                return false;
            };
            FixedWidthTokenWithLeadingTrivia.prototype.isIncrementallyUnusable = function () {
                return this.fullWidth() === 0 || TypeScript.SyntaxFacts.isAnyDivideOrRegularExpressionToken(this.tokenKind);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.realize = function () {
                return Syntax.realizeToken(this);
            };
            FixedWidthTokenWithLeadingTrivia.prototype.collectTextElements = function (elements) {
                collectTokenTextElements(this, elements);
            };

            FixedWidthTokenWithLeadingTrivia.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            FixedWidthTokenWithLeadingTrivia.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            FixedWidthTokenWithLeadingTrivia.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            FixedWidthTokenWithLeadingTrivia.prototype.isExpression = function () {
                return Syntax.isExpression(this);
            };

            FixedWidthTokenWithLeadingTrivia.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithLeadingTrivia.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithLeadingTrivia.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithLeadingTrivia.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return FixedWidthTokenWithLeadingTrivia;
        })();
        Syntax.FixedWidthTokenWithLeadingTrivia = FixedWidthTokenWithLeadingTrivia;

        var FixedWidthTokenWithTrailingTrivia = (function () {
            function FixedWidthTokenWithTrailingTrivia(fullText, kind, trailingTriviaInfo) {
                this._fullText = fullText;
                this.tokenKind = kind;
                this._trailingTriviaInfo = trailingTriviaInfo;
            }
            FixedWidthTokenWithTrailingTrivia.prototype.clone = function () {
                return new FixedWidthTokenWithTrailingTrivia(this._fullText, this.tokenKind, this._trailingTriviaInfo);
            };

            FixedWidthTokenWithTrailingTrivia.prototype.isNode = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.isToken = function () {
                return true;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.isList = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.isSeparatedList = function () {
                return false;
            };

            FixedWidthTokenWithTrailingTrivia.prototype.kind = function () {
                return this.tokenKind;
            };

            FixedWidthTokenWithTrailingTrivia.prototype.childCount = function () {
                return 0;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange('index');
            };

            FixedWidthTokenWithTrailingTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.width = function () {
                return this.text().length;
            };

            FixedWidthTokenWithTrailingTrivia.prototype.text = function () {
                return TypeScript.SyntaxFacts.getText(this.tokenKind);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.fullText = function () {
                return this._fullText;
            };

            FixedWidthTokenWithTrailingTrivia.prototype.value = function () {
                return Syntax.value(this);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.valueText = function () {
                return Syntax.valueText(this);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.hasLeadingTrivia = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.hasLeadingComment = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.hasLeadingNewLine = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.leadingTriviaWidth = function () {
                return 0;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.leadingTrivia = function () {
                return Syntax.emptyTriviaList;
            };

            FixedWidthTokenWithTrailingTrivia.prototype.hasTrailingTrivia = function () {
                return true;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.hasTrailingComment = function () {
                return hasTriviaComment(this._trailingTriviaInfo);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.hasTrailingNewLine = function () {
                return hasTriviaNewLine(this._trailingTriviaInfo);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.trailingTriviaWidth = function () {
                return getTriviaWidth(this._trailingTriviaInfo);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.trailingTrivia = function () {
                return TypeScript.Scanner.scanTrivia(TypeScript.SimpleText.fromString(this._fullText), this.leadingTriviaWidth() + this.width(), getTriviaWidth(this._trailingTriviaInfo), true);
            };

            FixedWidthTokenWithTrailingTrivia.prototype.hasSkippedToken = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.toJSON = function (key) {
                return Syntax.tokenToJSON(this);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.firstToken = function () {
                return this;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.lastToken = function () {
                return this;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.isTypeScriptSpecific = function () {
                return false;
            };
            FixedWidthTokenWithTrailingTrivia.prototype.isIncrementallyUnusable = function () {
                return this.fullWidth() === 0 || TypeScript.SyntaxFacts.isAnyDivideOrRegularExpressionToken(this.tokenKind);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.realize = function () {
                return Syntax.realizeToken(this);
            };
            FixedWidthTokenWithTrailingTrivia.prototype.collectTextElements = function (elements) {
                collectTokenTextElements(this, elements);
            };

            FixedWidthTokenWithTrailingTrivia.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            FixedWidthTokenWithTrailingTrivia.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            FixedWidthTokenWithTrailingTrivia.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            FixedWidthTokenWithTrailingTrivia.prototype.isExpression = function () {
                return Syntax.isExpression(this);
            };

            FixedWidthTokenWithTrailingTrivia.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithTrailingTrivia.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithTrailingTrivia.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithTrailingTrivia.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return FixedWidthTokenWithTrailingTrivia;
        })();
        Syntax.FixedWidthTokenWithTrailingTrivia = FixedWidthTokenWithTrailingTrivia;

        var FixedWidthTokenWithLeadingAndTrailingTrivia = (function () {
            function FixedWidthTokenWithLeadingAndTrailingTrivia(fullText, kind, leadingTriviaInfo, trailingTriviaInfo) {
                this._fullText = fullText;
                this.tokenKind = kind;
                this._leadingTriviaInfo = leadingTriviaInfo;
                this._trailingTriviaInfo = trailingTriviaInfo;
            }
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.clone = function () {
                return new FixedWidthTokenWithLeadingAndTrailingTrivia(this._fullText, this.tokenKind, this._leadingTriviaInfo, this._trailingTriviaInfo);
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isNode = function () {
                return false;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isToken = function () {
                return true;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isList = function () {
                return false;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isSeparatedList = function () {
                return false;
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.kind = function () {
                return this.tokenKind;
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.childCount = function () {
                return 0;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange('index');
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.fullWidth = function () {
                return this.fullText().length;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.width = function () {
                return this.text().length;
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.text = function () {
                return TypeScript.SyntaxFacts.getText(this.tokenKind);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.fullText = function () {
                return this._fullText;
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.value = function () {
                return Syntax.value(this);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.valueText = function () {
                return Syntax.valueText(this);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasLeadingTrivia = function () {
                return true;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasLeadingComment = function () {
                return hasTriviaComment(this._leadingTriviaInfo);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasLeadingNewLine = function () {
                return hasTriviaNewLine(this._leadingTriviaInfo);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.leadingTriviaWidth = function () {
                return getTriviaWidth(this._leadingTriviaInfo);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.leadingTrivia = function () {
                return TypeScript.Scanner.scanTrivia(TypeScript.SimpleText.fromString(this._fullText), 0, getTriviaWidth(this._leadingTriviaInfo), false);
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasTrailingTrivia = function () {
                return true;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasTrailingComment = function () {
                return hasTriviaComment(this._trailingTriviaInfo);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasTrailingNewLine = function () {
                return hasTriviaNewLine(this._trailingTriviaInfo);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.trailingTriviaWidth = function () {
                return getTriviaWidth(this._trailingTriviaInfo);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.trailingTrivia = function () {
                return TypeScript.Scanner.scanTrivia(TypeScript.SimpleText.fromString(this._fullText), this.leadingTriviaWidth() + this.width(), getTriviaWidth(this._trailingTriviaInfo), true);
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.hasSkippedToken = function () {
                return false;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.toJSON = function (key) {
                return Syntax.tokenToJSON(this);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.firstToken = function () {
                return this;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.lastToken = function () {
                return this;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isTypeScriptSpecific = function () {
                return false;
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isIncrementallyUnusable = function () {
                return this.fullWidth() === 0 || TypeScript.SyntaxFacts.isAnyDivideOrRegularExpressionToken(this.tokenKind);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.realize = function () {
                return Syntax.realizeToken(this);
            };
            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.collectTextElements = function (elements) {
                collectTokenTextElements(this, elements);
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isExpression = function () {
                return Syntax.isExpression(this);
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            FixedWidthTokenWithLeadingAndTrailingTrivia.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return FixedWidthTokenWithLeadingAndTrailingTrivia;
        })();
        Syntax.FixedWidthTokenWithLeadingAndTrailingTrivia = FixedWidthTokenWithLeadingAndTrailingTrivia;

        function collectTokenTextElements(token, elements) {
            token.leadingTrivia().collectTextElements(elements);
            elements.push(token.text());
            token.trailingTrivia().collectTextElements(elements);
        }

        function getTriviaWidth(value) {
            return value >>> 2 /* TriviaFullWidthShift */;
        }

        function hasTriviaComment(value) {
            return (value & 2 /* TriviaCommentMask */) !== 0;
        }

        function hasTriviaNewLine(value) {
            return (value & 1 /* TriviaNewLineMask */) !== 0;
        }
    })(TypeScript.Syntax || (TypeScript.Syntax = {}));
    var Syntax = TypeScript.Syntax;
})(TypeScript || (TypeScript = {}));
