///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    (function (Parser) {
        

        // The precedence of expressions in typescript.  While we're parsing an expression, we will
        // continue to consume and form new trees if the precedence is greater than our current
        // precedence.  For example, if we have: a + b * c, we will first parse 'a' with precedence 0.
        // We will then see the + with precedence 13.  13 is greater than 0 so we will decide to create
        // a binary expression with the result of parsing the sub expression "b * c".  We'll then parse
        // the term 'b' (passing in precedence 13).  We will then see the * with precedence 14.  14 is
        // greater than 13, so we will create a binary expression from "b" and "c", return that, and
        // join it with "a" producing:
        //
        //      +
        //     / \
        //    a   *
        //       / \
        //      b   c
        //
        // If we instead had: "a * b + c", we would first parser 'a' with precedence 0.  We would then see
        // the * with precedence 14.  14 is greater than 0 so we will decide to create a binary expression
        // with the result of parsing the sub expression "b + c".  We'll then parse the term 'b' (passing in
        // precedence 14).  We will then see the + with precedence 13.  13 is less than 14, so we won't
        // continue parsing subexpressions and will just return the expression 'b'.  The caller will join
        // that into "a * b" (and will be back at precedence 0). It will then see the + with precedence 11.
        // 11 is greater than 0 so it will parse the sub expression and make a binary expression out of it
        // producing:
        //
        //        +
        //       / \
        //      *   c
        //     / \
        //    a   b
        var ExpressionPrecedence;
        (function (ExpressionPrecedence) {
            // Intuitively, commas have the lowest precedence.  "a || b, c" is "(a || b), c", not
            // "a || (b, c)"
            ExpressionPrecedence[ExpressionPrecedence["CommaExpressionPrecedence"] = 1] = "CommaExpressionPrecedence";

            ExpressionPrecedence[ExpressionPrecedence["AssignmentExpressionPrecedence"] = 2] = "AssignmentExpressionPrecedence";

            ExpressionPrecedence[ExpressionPrecedence["ConditionalExpressionPrecedence"] = 3] = "ConditionalExpressionPrecedence";

            // REVIEW: Should ArrowFunctions have higher, lower, or the same precedence as ternary?
            ExpressionPrecedence[ExpressionPrecedence["ArrowFunctionPrecedence"] = 4] = "ArrowFunctionPrecedence";

            ExpressionPrecedence[ExpressionPrecedence["LogicalOrExpressionPrecedence"] = 5] = "LogicalOrExpressionPrecedence";
            ExpressionPrecedence[ExpressionPrecedence["LogicalAndExpressionPrecedence"] = 6] = "LogicalAndExpressionPrecedence";
            ExpressionPrecedence[ExpressionPrecedence["BitwiseOrExpressionPrecedence"] = 7] = "BitwiseOrExpressionPrecedence";
            ExpressionPrecedence[ExpressionPrecedence["BitwiseExclusiveOrExpressionPrecedence"] = 8] = "BitwiseExclusiveOrExpressionPrecedence";
            ExpressionPrecedence[ExpressionPrecedence["BitwiseAndExpressionPrecedence"] = 9] = "BitwiseAndExpressionPrecedence";
            ExpressionPrecedence[ExpressionPrecedence["EqualityExpressionPrecedence"] = 10] = "EqualityExpressionPrecedence";
            ExpressionPrecedence[ExpressionPrecedence["RelationalExpressionPrecedence"] = 11] = "RelationalExpressionPrecedence";
            ExpressionPrecedence[ExpressionPrecedence["ShiftExpressionPrecdence"] = 12] = "ShiftExpressionPrecdence";
            ExpressionPrecedence[ExpressionPrecedence["AdditiveExpressionPrecedence"] = 13] = "AdditiveExpressionPrecedence";
            ExpressionPrecedence[ExpressionPrecedence["MultiplicativeExpressionPrecedence"] = 14] = "MultiplicativeExpressionPrecedence";

            // Intuitively, unary expressions have the highest precedence.  After all, if you have:
            //   !foo || bar
            //
            // Then you have "(!foo) || bar", not "!(foo || bar)"
            ExpressionPrecedence[ExpressionPrecedence["UnaryExpressionPrecedence"] = 15] = "UnaryExpressionPrecedence";
        })(ExpressionPrecedence || (ExpressionPrecedence = {}));

        // The current state of the parser wrt to list parsing.  The way to read these is as:
        // CurrentProduction_SubList.  i.e. "Block_Statements" means "we're parsing a Block, and we're
        // currently parsing list of statements within it".  This is used by the list parsing mechanism
        // to parse the elements of the lists, and recover from errors we encounter when we run into
        // unexpected code.
        //
        // For example, when we are in ArgumentList_Arguments, we will continue trying to consume code
        // as long as "isArgument" is true.  If we run into a token for which "isArgument" is not true
        // we will do the following:
        //
        // If the token is a StopToken for ArgumentList_Arguments (like ")" ) then we will stop parsing
        // the list of arguments with no error.
        //
        // Otherwise, we *do* report an error for this unexpected token, and then enter error recovery
        // mode to decide how to try to recover from this unexpected token.
        //
        // Error recovery will walk up the list of states we're in seeing if the token is a stop token
        // for that construct *or* could start another element within what construct.  For example, if
        // the unexpected token was '}' then that would be a stop token for Block_Statements.
        // Alternatively, if the unexpected token was 'return', then that would be a start token for
        // the next statment in Block_Statements.
        //
        // If either of those cases are true, We will then return *without* consuming  that token.
        // (Remember, we've already reported an error).  Now we're just letting the higher up parse
        // constructs eventually try to consume that token.
        //
        // If none of the higher up states consider this a stop or start token, then we will simply
        // consume the token and add it to our list of 'skipped tokens'.  We will then repeat the
        // above algorithm until we resynchronize at some point.
        var ListParsingState;
        (function (ListParsingState) {
            ListParsingState[ListParsingState["SourceUnit_ModuleElements"] = 1 << 0] = "SourceUnit_ModuleElements";
            ListParsingState[ListParsingState["ClassDeclaration_ClassElements"] = 1 << 1] = "ClassDeclaration_ClassElements";
            ListParsingState[ListParsingState["ModuleDeclaration_ModuleElements"] = 1 << 2] = "ModuleDeclaration_ModuleElements";
            ListParsingState[ListParsingState["SwitchStatement_SwitchClauses"] = 1 << 3] = "SwitchStatement_SwitchClauses";
            ListParsingState[ListParsingState["SwitchClause_Statements"] = 1 << 4] = "SwitchClause_Statements";
            ListParsingState[ListParsingState["Block_Statements"] = 1 << 5] = "Block_Statements";
            ListParsingState[ListParsingState["TryBlock_Statements"] = 1 << 6] = "TryBlock_Statements";
            ListParsingState[ListParsingState["CatchBlock_Statements"] = 1 << 7] = "CatchBlock_Statements";
            ListParsingState[ListParsingState["EnumDeclaration_EnumElements"] = 1 << 8] = "EnumDeclaration_EnumElements";
            ListParsingState[ListParsingState["ObjectType_TypeMembers"] = 1 << 9] = "ObjectType_TypeMembers";
            ListParsingState[ListParsingState["ClassOrInterfaceDeclaration_HeritageClauses"] = 1 << 10] = "ClassOrInterfaceDeclaration_HeritageClauses";
            ListParsingState[ListParsingState["HeritageClause_TypeNameList"] = 1 << 11] = "HeritageClause_TypeNameList";
            ListParsingState[ListParsingState["VariableDeclaration_VariableDeclarators_AllowIn"] = 1 << 12] = "VariableDeclaration_VariableDeclarators_AllowIn";
            ListParsingState[ListParsingState["VariableDeclaration_VariableDeclarators_DisallowIn"] = 1 << 13] = "VariableDeclaration_VariableDeclarators_DisallowIn";
            ListParsingState[ListParsingState["ArgumentList_AssignmentExpressions"] = 1 << 14] = "ArgumentList_AssignmentExpressions";
            ListParsingState[ListParsingState["ObjectLiteralExpression_PropertyAssignments"] = 1 << 15] = "ObjectLiteralExpression_PropertyAssignments";
            ListParsingState[ListParsingState["ArrayLiteralExpression_AssignmentExpressions"] = 1 << 16] = "ArrayLiteralExpression_AssignmentExpressions";
            ListParsingState[ListParsingState["ParameterList_Parameters"] = 1 << 17] = "ParameterList_Parameters";
            ListParsingState[ListParsingState["TypeArgumentList_Types"] = 1 << 18] = "TypeArgumentList_Types";
            ListParsingState[ListParsingState["TypeParameterList_TypeParameters"] = 1 << 19] = "TypeParameterList_TypeParameters";

            ListParsingState[ListParsingState["FirstListParsingState"] = ListParsingState.SourceUnit_ModuleElements] = "FirstListParsingState";
            ListParsingState[ListParsingState["LastListParsingState"] = ListParsingState.TypeParameterList_TypeParameters] = "LastListParsingState";
        })(ListParsingState || (ListParsingState = {}));

        // Allows one to easily move over a syntax tree.  Used during incremental parsing to move over
        // the previously parsed tree to provide nodes and tokens that can be reused when parsing the
        // updated text.
        var SyntaxCursor = (function () {
            function SyntaxCursor(sourceUnit) {
                this._elements = [];
                this._index = 0;
                this._pinCount = 0;
                sourceUnit.insertChildrenInto(this._elements, 0);
            }
            SyntaxCursor.prototype.isFinished = function () {
                return this._index === this._elements.length;
            };

            SyntaxCursor.prototype.currentElement = function () {
                if (this.isFinished()) {
                    return null;
                }

                return this._elements[this._index];
            };

            SyntaxCursor.prototype.currentNode = function () {
                var element = this.currentElement();
                return element !== null && element.isNode() ? element : null;
            };

            SyntaxCursor.prototype.moveToFirstChild = function () {
                if (this.isFinished()) {
                    return;
                }

                var element = this._elements[this._index];
                if (element.isToken()) {
                    // If we're already on a token, there's nothing to do.
                    return;
                }

                // Otherwise, break the node we're pointing at into its children.  We'll then be
                // pointing at the first child
                var node = element;

                // Remove the item that we're pointing at.
                this._elements.splice(this._index, 1);

                // And add its children into the position it was at.
                node.insertChildrenInto(this._elements, this._index);
            };

            SyntaxCursor.prototype.moveToNextSibling = function () {
                if (this.isFinished()) {
                    return;
                }

                if (this._pinCount > 0) {
                    // If we're currently pinned, then just move our index forward.  We'll then be
                    // pointing at the next sibling.
                    this._index++;
                    return;
                }

                // if we're not pinned, we better be pointed at the first item in the list.
                // Debug.assert(this._index === 0);
                // Just shift ourselves over so we forget the current element we're pointing at and
                // we're pointing at the next slibing.
                this._elements.shift();
            };

            SyntaxCursor.prototype.getAndPinCursorIndex = function () {
                this._pinCount++;
                return this._index;
            };

            SyntaxCursor.prototype.releaseAndUnpinCursorIndex = function (index) {
                // this._index = index;
                // Debug.assert(this._pinCount > 0);
                this._pinCount--;
                if (this._pinCount === 0) {
                    // The first pin was given out at index 0.  So we better be back at index 0.
                    // Debug.assert(this._index === 0);
                }
            };

            SyntaxCursor.prototype.rewindToPinnedCursorIndex = function (index) {
                // Debug.assert(index >= 0 && index <= this._elements.length);
                // Debug.assert(this._pinCount > 0);
                this._index = index;
            };

            SyntaxCursor.prototype.pinCount = function () {
                return this._pinCount;
            };

            SyntaxCursor.prototype.moveToFirstToken = function () {
                var element;

                while (!this.isFinished()) {
                    element = this.currentElement();
                    if (element.isNode()) {
                        this.moveToFirstChild();
                        continue;
                    }

                    // Debug.assert(element.isToken());
                    return;
                }
            };

            SyntaxCursor.prototype.currentToken = function () {
                this.moveToFirstToken();
                if (this.isFinished()) {
                    return null;
                }

                var element = this.currentElement();

                // Debug.assert(element.isToken());
                return element;
            };

            SyntaxCursor.prototype.peekToken = function (n) {
                this.moveToFirstToken();
                var pin = this.getAndPinCursorIndex();

                for (var i = 0; i < n; i++) {
                    this.moveToNextSibling();
                    this.moveToFirstToken();
                }

                var result = this.currentToken();
                this.rewindToPinnedCursorIndex(pin);
                this.releaseAndUnpinCursorIndex(pin);

                return result;
            };
            return SyntaxCursor;
        })();

        

        // Parser source used in batch scenarios.  Directly calls into an underlying text scanner and
        // supports none of the functionality to reuse nodes.  Good for when you just want want to do
        // a single parse of a file.
        var NormalParserSource = (function () {
            function NormalParserSource(fileName, text, languageVersion) {
                // The previous token to the current token.  Set when we advance to the next token.
                this._previousToken = null;
                // The absolute position we're at in the text we're reading from.
                this._absolutePosition = 0;
                // The diagnostics we get while scanning.  Note: this never gets rewound when we do a normal
                // rewind.  That's because rewinding doesn't affect the tokens created.  It only affects where
                // in the token stream we're pointing at.  However, it will get modified if we we decide to
                // reparse a / or /= as a regular expression.
                this._tokenDiagnostics = [];
                // Pool of rewind points we give out if the parser needs one.
                this.rewindPointPool = [];
                this.rewindPointPoolCount = 0;
                this.slidingWindow = new TypeScript.SlidingWindow(this, TypeScript.ArrayUtilities.createArray(32, null), null);
                this.scanner = new TypeScript.Scanner(fileName, text, languageVersion);
            }
            NormalParserSource.prototype.currentNode = function () {
                // The normal parser source never returns nodes.  They're only returned by the
                // incremental parser source.
                return null;
            };

            NormalParserSource.prototype.moveToNextNode = function () {
                throw TypeScript.Errors.invalidOperation();
            };

            NormalParserSource.prototype.absolutePosition = function () {
                return this._absolutePosition;
            };

            NormalParserSource.prototype.previousToken = function () {
                return this._previousToken;
            };

            NormalParserSource.prototype.tokenDiagnostics = function () {
                return this._tokenDiagnostics;
            };

            NormalParserSource.prototype.getOrCreateRewindPoint = function () {
                if (this.rewindPointPoolCount === 0) {
                    return {};
                }

                this.rewindPointPoolCount--;
                var result = this.rewindPointPool[this.rewindPointPoolCount];
                this.rewindPointPool[this.rewindPointPoolCount] = null;
                return result;
            };

            NormalParserSource.prototype.getRewindPoint = function () {
                var slidingWindowIndex = this.slidingWindow.getAndPinAbsoluteIndex();

                var rewindPoint = this.getOrCreateRewindPoint();

                rewindPoint.slidingWindowIndex = slidingWindowIndex;
                rewindPoint.previousToken = this._previousToken;
                rewindPoint.absolutePosition = this._absolutePosition;

                rewindPoint.pinCount = this.slidingWindow.pinCount();

                return rewindPoint;
            };

            NormalParserSource.prototype.isPinned = function () {
                return this.slidingWindow.pinCount() > 0;
            };

            NormalParserSource.prototype.rewind = function (rewindPoint) {
                this.slidingWindow.rewindToPinnedIndex(rewindPoint.slidingWindowIndex);

                this._previousToken = rewindPoint.previousToken;
                this._absolutePosition = rewindPoint.absolutePosition;
            };

            NormalParserSource.prototype.releaseRewindPoint = function (rewindPoint) {
                // Debug.assert(this.slidingWindow.pinCount() === rewindPoint.pinCount);
                this.slidingWindow.releaseAndUnpinAbsoluteIndex(rewindPoint.absoluteIndex);

                this.rewindPointPool[this.rewindPointPoolCount] = rewindPoint;
                this.rewindPointPoolCount++;
            };

            NormalParserSource.prototype.fetchMoreItems = function (allowRegularExpression, sourceIndex, window, destinationIndex, spaceAvailable) {
                // Assert disabled because it is actually expensive enugh to affect perf.
                // Debug.assert(spaceAvailable > 0);
                window[destinationIndex] = this.scanner.scan(this._tokenDiagnostics, allowRegularExpression);
                return 1;
            };

            NormalParserSource.prototype.peekToken = function (n) {
                return this.slidingWindow.peekItemN(n);
            };

            NormalParserSource.prototype.moveToNextToken = function () {
                var currentToken = this.currentToken();
                this._absolutePosition += currentToken.fullWidth();
                this._previousToken = currentToken;

                this.slidingWindow.moveToNextItem();
            };

            NormalParserSource.prototype.currentToken = function () {
                return this.slidingWindow.currentItem(false);
            };

            NormalParserSource.prototype.removeDiagnosticsOnOrAfterPosition = function (position) {
                // walk backwards, removing any diagnostics that came after the the current token's
                // full start position.
                var tokenDiagnosticsLength = this._tokenDiagnostics.length;
                while (tokenDiagnosticsLength > 0) {
                    var diagnostic = this._tokenDiagnostics[tokenDiagnosticsLength - 1];
                    if (diagnostic.start() >= position) {
                        tokenDiagnosticsLength--;
                    } else {
                        break;
                    }
                }

                this._tokenDiagnostics.length = tokenDiagnosticsLength;
            };

            NormalParserSource.prototype.resetToPosition = function (absolutePosition, previousToken) {
                this._absolutePosition = absolutePosition;
                this._previousToken = previousToken;

                // First, remove any diagnostics that came after this position.
                this.removeDiagnosticsOnOrAfterPosition(absolutePosition);

                // Now, tell our sliding window to throw away all tokens after this position as well.
                this.slidingWindow.disgardAllItemsFromCurrentIndexOnwards();

                // Now tell the scanner to reset its position to this position as well.  That way
                // when we try to scan the next item, we'll be at the right location.
                this.scanner.setAbsoluteIndex(absolutePosition);
            };

            NormalParserSource.prototype.currentTokenAllowingRegularExpression = function () {
                // We better be on a divide token right now.
                // Debug.assert(SyntaxFacts.isAnyDivideToken(this.currentToken().tokenKind));
                // First, we're going to rewind all our data to the point where this / or /= token started.
                // That's because if it does turn out to be a regular expression, then any tokens or token
                // diagnostics we produced after the original / may no longer be valid.  This would actually
                // be a  fairly expected case.  For example, if you had:  / ... gibberish ... /, we may have
                // produced several diagnostics in the process of scanning the tokens after the first / as
                // they may not have been legal javascript okens.
                //
                // We also need to remove all the tokens we've gotten from the slash and onwards.  They may
                // not have been what the scanner would have produced if it decides that this is actually
                // a regular expresion.
                this.resetToPosition(this._absolutePosition, this._previousToken);

                // Now actually fetch the token again from the scanner. This time let it know that it
                // can scan it as a regex token if it wants to.
                var token = this.slidingWindow.currentItem(true);

                // We have better gotten some sort of regex token.  Otherwise, something *very* wrong has
                // occurred.
                // Debug.assert(SyntaxFacts.isAnyDivideOrRegularExpressionToken(token.tokenKind));
                return token;
            };
            return NormalParserSource;
        })();

        // Parser source used in incremental scenarios. This parser source wraps an old tree, text
        // change and new text, and uses all three to provide nodes and tokens to the parser.  In
        // general, nodes from the old tree are returned as long as they do not intersect with the text
        // change.  Then, once the text change is reached, tokens from the old tree are returned as
        // long as they do not intersect with the text change.  Then, the text that is actually changed
        // will be scanned using a normal scanner.  Then, once the new text is scanned, the source will
        // attempt to sync back up with nodes or tokens that started where the new tokens end. Once it
        // can do that, then all subsequent data will come from the original tree.
        //
        // This allows for an enormous amount of tree reuse in common scenarios.  Situations that
        // prevent this level of reuse include substantially destructive operations like introducing
        // "/*" without a "*/" nearby to terminate the comment.
        var IncrementalParserSource = (function () {
            function IncrementalParserSource(oldSyntaxTree, textChangeRange, newText) {
                // This number represents how our position in the old tree relates to the position we're
                // pointing at in the new text.  If it is 0 then our positions are in sync and we can read
                // nodes or tokens from the old tree.  If it is non-zero, then our positions are not in
                // sync and we cannot use nodes or tokens from the old tree.
                //
                // Now, changeDelta could be negative or positive.  Negative means 'the position we're at
                // in the original tree is behind the position we're at in the text'.  In this case we
                // keep throwing out old nodes or tokens (and thus move forward in the original tree) until
                // changeDelta becomes 0 again or positive.  If it becomes 0 then we are resynched and can
                // read nodes or tokesn from the tree.
                //
                // If changeDelta is positive, that means the current node or token we're pointing at in
                // the old tree is at a further ahead position than the position we're pointing at in the
                // new text.  In this case we have no choice but to scan tokens from teh new text.  We will
                // continue to do so until, again, changeDelta becomes 0 and we've resynced, or change delta
                // becomes negative and we need to skip nodes or tokes in the original tree.
                this._changeDelta = 0;
                var oldSourceUnit = oldSyntaxTree.sourceUnit();
                this._oldSourceUnitCursor = new SyntaxCursor(oldSourceUnit);

                // In general supporting multiple individual edits is just not that important.  So we
                // just collapse this all down to a single range to make the code here easier.  The only
                // time this could be problematic would be if the user made a ton of discontinuous edits.
                // For example, doing a column select on a *large* section of a code.  If this is a
                // problem, we can always update this code to handle multiple changes.
                this._changeRange = IncrementalParserSource.extendToAffectedRange(textChangeRange, oldSourceUnit);

                // The old tree's length, plus whatever length change was caused by the edit
                // Had better equal the new text's length!
                if (TypeScript.Debug.shouldAssert(2 /* Aggressive */)) {
                    TypeScript.Debug.assert((oldSourceUnit.fullWidth() - this._changeRange.span().length() + this._changeRange.newLength()) === newText.length());
                }

                // Set up a scanner so that we can scan tokens out of the new text.
                this._normalParserSource = new NormalParserSource(oldSyntaxTree.fileName(), newText, oldSyntaxTree.parseOptions().languageVersion());
            }
            IncrementalParserSource.extendToAffectedRange = function (changeRange, sourceUnit) {
                // Consider the following code:
                //      void foo() { /; }
                //
                // If the text changes with an insertion of / just before the semicolon then we end up with:
                //      void foo() { //; }
                //
                // If we were to just use the changeRange a is, then we would not rescan the { token
                // (as it does not intersect hte actual original change range).  Because an edit may
                // change the token touching it, we actually need to look back *at least* one token so
                // that the prior token sees that change.
                //
                // Note: i believe (outside of regex tokens) max lookahead is just one token for
                // TypeScript.  However, if this turns out to be wrong, we may have to increase how much
                // futher we look back.
                //
                // Note: lookahead handling for regex characters is handled specially in during
                // incremental parsing, and does not need to be handled here.
                var maxLookahead = 1;

                var start = changeRange.span().start();

                for (var i = 0; start > 0 && i <= maxLookahead; i++) {
                    var token = sourceUnit.findToken(start);

                    // Debug.assert(token.kind() !== SyntaxKind.None);
                    // Debug.assert(token.kind() === SyntaxKind.EndOfFileToken || token.fullWidth() > 0);
                    var position = token.fullStart();

                    start = TypeScript.MathPrototype.max(0, position - 1);
                }

                var finalSpan = TypeScript.TextSpan.fromBounds(start, changeRange.span().end());
                var finalLength = changeRange.newLength() + (changeRange.span().start() - start);

                return new TypeScript.TextChangeRange(finalSpan, finalLength);
            };

            IncrementalParserSource.prototype.absolutePosition = function () {
                return this._normalParserSource.absolutePosition();
            };

            IncrementalParserSource.prototype.previousToken = function () {
                return this._normalParserSource.previousToken();
            };

            IncrementalParserSource.prototype.tokenDiagnostics = function () {
                return this._normalParserSource.tokenDiagnostics();
            };

            IncrementalParserSource.prototype.getRewindPoint = function () {
                // Get a rewind point for our new text reader and for our old source unit cursor.
                var rewindPoint = this._normalParserSource.getRewindPoint();
                var oldSourceUnitCursorIndex = this._oldSourceUnitCursor.getAndPinCursorIndex();

                // Store where we were when the rewind point was created.
                rewindPoint.changeDelta = this._changeDelta;
                rewindPoint.changeRange = this._changeRange;
                rewindPoint.oldSourceUnitCursorIndex = oldSourceUnitCursorIndex;

                // Debug.assert(rewindPoint.pinCount === this._oldSourceUnitCursor.pinCount());
                return rewindPoint;
            };

            IncrementalParserSource.prototype.rewind = function (rewindPoint) {
                // Restore our state to the values when the rewind point was created.
                this._changeRange = rewindPoint.changeRange;
                this._changeDelta = rewindPoint.changeDelta;
                this._oldSourceUnitCursor.rewindToPinnedCursorIndex(rewindPoint.oldSourceUnitCursorIndex);

                this._normalParserSource.rewind(rewindPoint);
            };

            IncrementalParserSource.prototype.releaseRewindPoint = function (rewindPoint) {
                // Release both the new text reader and the old text cursor.
                this._oldSourceUnitCursor.releaseAndUnpinCursorIndex(rewindPoint.oldSourceUnitCursorIndex);
                this._normalParserSource.releaseRewindPoint(rewindPoint);
            };

            IncrementalParserSource.prototype.canReadFromOldSourceUnit = function () {
                // If we're currently pinned, then do not want to touch the cursor.  If we end up
                // reading from the old source unit, we'll try to then set the position of the normal
                // parser source to an absolute position (in moveToNextToken).  Doing is unsupported
                // while the underlying source is pinned.
                if (this._normalParserSource.isPinned()) {
                    return false;
                }

                // If our current absolute position is in the middle of the changed range in the new text
                // then we definitely can't read from the old source unit right now.
                if (this._changeRange !== null && this._changeRange.newSpan().intersectsWithPosition(this.absolutePosition())) {
                    return false;
                }

                // First, try to sync up with the new text if we're behind.
                this.syncCursorToNewTextIfBehind();

                // Now, if we're synced up *and* we're not currently pinned in the new text scanner,
                // then we can read a node from the cursor.  If we're pinned in the scanner then we
                // can't read a node from the cursor because we will mess up the pinned scanner when
                // we try to move it forward past this node.
                return this._changeDelta === 0 && !this._oldSourceUnitCursor.isFinished();
            };

            IncrementalParserSource.prototype.currentNode = function () {
                if (this.canReadFromOldSourceUnit()) {
                    // Try to read a node.  If we can't then our caller will call back in and just try
                    // to get a token.
                    return this.tryGetNodeFromOldSourceUnit();
                }

                // Either we were ahead of the old text, or we were pinned.  No node can be read here.
                return null;
            };

            IncrementalParserSource.prototype.currentToken = function () {
                if (this.canReadFromOldSourceUnit()) {
                    var token = this.tryGetTokenFromOldSourceUnit();
                    if (token !== null) {
                        return token;
                    }
                }

                // Either we couldn't read from the old source unit, or we weren't able to successfully
                // get a token from it.  In this case we need to read a token from the underlying text.
                return this._normalParserSource.currentToken();
            };

            IncrementalParserSource.prototype.currentTokenAllowingRegularExpression = function () {
                // Just delegate to the underlying source to handle this.
                return this._normalParserSource.currentTokenAllowingRegularExpression();
            };

            IncrementalParserSource.prototype.syncCursorToNewTextIfBehind = function () {
                while (true) {
                    if (this._oldSourceUnitCursor.isFinished()) {
                        break;
                    }

                    if (this._changeDelta >= 0) {
                        break;
                    }

                    // We're behind in the original tree.  Throw out a node or token in an attempt to
                    // catch up to the position we're at in the new text.
                    var currentElement = this._oldSourceUnitCursor.currentElement();

                    // If we're pointing at a node, and that node's width is less than our delta,
                    // then we can just skip that node.  Otherwise, if we're pointing at a node
                    // whose width is greater than the delta, then crumble it and try again.
                    // Otherwise, we must be pointing at a token.  Just skip it and try again.
                    if (currentElement.isNode() && (currentElement.fullWidth() > Math.abs(this._changeDelta))) {
                        // We were pointing at a node whose width was more than changeDelta.  Crumble the
                        // node and try again.  Note: we haven't changed changeDelta.  So the callers loop
                        // will just repeat this until we get to a node or token that we can skip over.
                        this._oldSourceUnitCursor.moveToFirstChild();
                    } else {
                        this._oldSourceUnitCursor.moveToNextSibling();

                        // Get our change delta closer to 0 as we skip past this item.
                        this._changeDelta += currentElement.fullWidth();
                        // If this was a node, then our changeDelta is 0 or negative.  If this was a
                        // token, then we could still be negative (and we have to read another token),
                        // we could be zero (we're done), or we could be positive (we've moved ahead
                        // of the new text).  Only if we're negative will we continue looping.
                    }
                }
                // At this point, we must be either:
                //   a) done with the cursor
                //   b) (ideally) caught up to the new text position.
                //   c) ahead of the new text position.
                // In case 'b' we can try to reuse a node from teh old tree.
                // Debug.assert(this._oldSourceUnitCursor.isFinished() || this._changeDelta >= 0);
            };

            IncrementalParserSource.prototype.intersectsWithChangeRangeSpanInOriginalText = function (start, length) {
                return this._changeRange !== null && this._changeRange.span().intersectsWith(start, length);
            };

            IncrementalParserSource.prototype.tryGetNodeFromOldSourceUnit = function () {
                while (true) {
                    var node = this._oldSourceUnitCursor.currentNode();
                    if (node === null) {
                        // Couldn't even read a node, nothing to return.
                        return null;
                    }

                    if (!this.intersectsWithChangeRangeSpanInOriginalText(this.absolutePosition(), node.fullWidth())) {
                        // Didn't intersect with the change range.
                        if (!node.isIncrementallyUnusable()) {
                            // Didn't contain anything that would make it unusable.  Awesome.  This is
                            // a node we can reuse.
                            return node;
                        }
                    }

                    // We couldn't use currentNode. Try to move to its first child (in case that's a
                    // node).  If it is we can try using that.  Otherwise we'll just bail out in the
                    // next iteration of the loop.
                    this._oldSourceUnitCursor.moveToFirstChild();
                }
            };

            IncrementalParserSource.prototype.canReuseTokenFromOldSourceUnit = function (position, token) {
                // A token is safe to return if:
                //  a) it does not intersect the changed text.
                //  b) it does not contain skipped text.
                //  c) it is not zero width.
                //  d) it is not a regex token.
                //  e) it is not a parser generated token.
                //
                // NOTE: It is safe to get a token regardless of what our strict context was/is.  That's
                // because the strict context doesn't change what tokens are scanned, only how the
                // parser reacts to them.
                if (token !== null) {
                    if (!this.intersectsWithChangeRangeSpanInOriginalText(position, token.fullWidth())) {
                        // Didn't intersect with the change range.
                        if (!token.isIncrementallyUnusable()) {
                            // Didn't contain anything that would make it unusable.  Awesome.  This is
                            // a token we can reuse.
                            return true;
                        }
                    }
                }

                return false;
            };

            IncrementalParserSource.prototype.tryGetTokenFromOldSourceUnit = function () {
                // Debug.assert(this.canReadFromOldSourceUnit());
                // get the current token that the cursor is pointing at.
                var token = this._oldSourceUnitCursor.currentToken();

                return this.canReuseTokenFromOldSourceUnit(this.absolutePosition(), token) ? token : null;
            };

            IncrementalParserSource.prototype.peekToken = function (n) {
                if (this.canReadFromOldSourceUnit()) {
                    var token = this.tryPeekTokenFromOldSourceUnit(n);
                    if (token !== null) {
                        return token;
                    }
                }

                // Couldn't peek this far in the old tree.  Get the token from the new text.
                return this._normalParserSource.peekToken(n);
            };

            IncrementalParserSource.prototype.tryPeekTokenFromOldSourceUnit = function (n) {
                // Debug.assert(this.canReadFromOldSourceUnit());
                // In order to peek the 'nth' token we need all the tokens up to that point.  That way
                // we know we know position that the nth token is at.  The position is necessary so
                // that we can test if this token (or any that precede it cross the change range).
                var currentPosition = this.absolutePosition();
                for (var i = 0; i < n; i++) {
                    var interimToken = this._oldSourceUnitCursor.peekToken(i);
                    if (!this.canReuseTokenFromOldSourceUnit(currentPosition, interimToken)) {
                        return null;
                    }

                    currentPosition += interimToken.fullWidth();
                }

                var token = this._oldSourceUnitCursor.peekToken(n);
                return this.canReuseTokenFromOldSourceUnit(currentPosition, token) ? token : null;
            };

            IncrementalParserSource.prototype.moveToNextNode = function () {
                // A node could have only come from the old source unit cursor.  Update it and our
                // current state.
                // Debug.assert(this._changeDelta === 0);
                // Get the current node we were pointing at, and move to the next element.
                var currentElement = this._oldSourceUnitCursor.currentElement();
                var currentNode = this._oldSourceUnitCursor.currentNode();

                // We better still be pointing at the node.
                // Debug.assert(currentElement === currentNode);
                this._oldSourceUnitCursor.moveToNextSibling();

                // Update the underlying source with where it should now be currently pointing, and
                // what the previous token is before that position.
                var absolutePosition = this.absolutePosition() + currentNode.fullWidth();
                var previousToken = currentNode.lastToken();
                this._normalParserSource.resetToPosition(absolutePosition, previousToken);

                // Debug.assert(previousToken !== null);
                // Debug.assert(previousToken.width() > 0);
                if (this._changeRange !== null) {
                    // If we still have a change range, then this node must have ended before the
                    // change range starts.  Thus, we don't need to call 'skipPastChanges'.
                    // Debug.assert(this.absolutePosition() < this._changeRange.span().start());
                }
            };

            IncrementalParserSource.prototype.moveToNextToken = function () {
                // This token may have come from the old source unit, or from the new text.  Handle
                // both accordingly.
                var currentToken = this.currentToken();

                if (this._oldSourceUnitCursor.currentToken() === currentToken) {
                    // The token came from the old source unit.  So our tree and text must be in sync.
                    // Debug.assert(this._changeDelta === 0);
                    // Move the cursor past this token.
                    this._oldSourceUnitCursor.moveToNextSibling();

                    // Debug.assert(!this._normalParserSource.isPinned());
                    // Update the underlying source with where it should now be currently pointing, and
                    // what the previous token is before that position.  We don't need to do this when
                    // the token came from the new text as the source will automatically be placed in
                    // the right position.
                    var absolutePosition = this.absolutePosition() + currentToken.fullWidth();
                    var previousToken = currentToken;
                    this._normalParserSource.resetToPosition(absolutePosition, previousToken);

                    // Debug.assert(previousToken !== null);
                    // Debug.assert(previousToken.width() > 0);
                    if (this._changeRange !== null) {
                        // If we still have a change range, then this token must have ended before the
                        // change range starts.  Thus, we don't need to call 'skipPastChanges'.
                        // Debug.assert(this.absolutePosition() < this._changeRange.span().start());
                    }
                } else {
                    // the token came from the new text.  We have to update our delta appropriately.
                    this._changeDelta -= currentToken.fullWidth();

                    // Move our underlying source forward.
                    this._normalParserSource.moveToNextToken();

                    // Because we read a token from the new text, we may have moved ourselves past the
                    // change range.  If we did, then we may also have to update our change delta to
                    // compensate for the length change between the old and new text.
                    if (this._changeRange !== null) {
                        // var changeEndInNewText = this._changeRange.span().start() + this._changeRange.newLength();
                        var changeRangeSpanInNewText = this._changeRange.newSpan();
                        if (this.absolutePosition() >= changeRangeSpanInNewText.end()) {
                            this._changeDelta += this._changeRange.newLength() - this._changeRange.span().length();
                            this._changeRange = null;
                        }
                    }
                }
            };
            return IncrementalParserSource;
        })();

        // Contains the actual logic to parse typescript/javascript.  This is the code that generally
        // represents the logic necessary to handle all the language grammar constructs.  When the
        // language changes, this should generally only be the place necessary to fix up.
        var ParserImpl = (function () {
            function ParserImpl(fileName, lineMap, source, parseOptions, newText_forDebuggingPurposesOnly) {
                this.newText_forDebuggingPurposesOnly = newText_forDebuggingPurposesOnly;
                // TODO: do we need to store/restore this when speculative parsing?  I don't think so.  The
                // parsing logic already handles storing/restoring this and should work properly even if we're
                // speculative parsing.
                this.listParsingState = 0;
                // Whether or not we are in strict parsing mode.  All that changes in strict parsing mode is
                // that some tokens that would be considered identifiers may be considered keywords.  When
                // rewinding, we need to store and restore this as the mode may have changed.
                //
                // TODO: do we need to store/restore this when speculative parsing?  I don't think so.  The
                // parsing logic already handles storing/restoring this and should work properly even if we're
                // speculative parsing.
                this.isInStrictMode = false;
                // Current state of the parser.  If we need to rewind we will store and reset these values as
                // appropriate.
                // Diagnostics created when parsing invalid code.  Any diagnosics created when speculative
                // parsing need to removed when rewinding.  To do this we store the count of diagnostics when
                // we start speculative parsing.  And if we rewind, we restore this to the same count that we
                // started at.
                this.diagnostics = [];
                this.factory = TypeScript.Syntax.normalModeFactory;
                this.mergeTokensStorage = [];
                this.arrayPool = [];
                this.fileName = fileName;
                this.lineMap = lineMap;
                this.source = source;
                this.parseOptions = parseOptions;
            }
            ParserImpl.prototype.getRewindPoint = function () {
                var rewindPoint = this.source.getRewindPoint();

                rewindPoint.diagnosticsCount = this.diagnostics.length;

                // Values we keep around for debug asserting purposes.
                rewindPoint.isInStrictMode = this.isInStrictMode;
                rewindPoint.listParsingState = this.listParsingState;

                return rewindPoint;
            };

            ParserImpl.prototype.rewind = function (rewindPoint) {
                this.source.rewind(rewindPoint);

                this.diagnostics.length = rewindPoint.diagnosticsCount;
            };

            ParserImpl.prototype.releaseRewindPoint = function (rewindPoint) {
                // Debug.assert(this.listParsingState === rewindPoint.listParsingState);
                // Debug.assert(this.isInStrictMode === rewindPoint.isInStrictMode);
                this.source.releaseRewindPoint(rewindPoint);
            };

            ParserImpl.prototype.currentTokenStart = function () {
                return this.source.absolutePosition() + this.currentToken().leadingTriviaWidth();
            };

            ParserImpl.prototype.previousTokenStart = function () {
                if (this.previousToken() === null) {
                    return 0;
                }

                return this.source.absolutePosition() - this.previousToken().fullWidth() + this.previousToken().leadingTriviaWidth();
            };

            ParserImpl.prototype.previousTokenEnd = function () {
                if (this.previousToken() === null) {
                    return 0;
                }

                return this.previousTokenStart() + this.previousToken().width();
            };

            ParserImpl.prototype.currentNode = function () {
                var node = this.source.currentNode();

                // We can only reuse a node if it was parsed under the same strict mode that we're
                // currently in.  i.e. if we originally parsed a node in non-strict mode, but then
                // the user added 'using strict' at hte top of the file, then we can't use that node
                // again as the presense of strict mode may cause us to parse the tokens in the file
                // differetly.
                //
                // Note: we *can* reuse tokens when the strict mode changes.  That's because tokens
                // are unaffected by strict mode.  It's just the parser will decide what to do with it
                // differently depending on what mode it is in.
                if (node === null || node.parsedInStrictMode() !== this.isInStrictMode) {
                    return null;
                }

                return node;
            };

            ParserImpl.prototype.currentToken = function () {
                return this.source.currentToken();
            };

            ParserImpl.prototype.currentTokenAllowingRegularExpression = function () {
                return this.source.currentTokenAllowingRegularExpression();
            };

            ParserImpl.prototype.peekToken = function (n) {
                return this.source.peekToken(n);
            };

            ParserImpl.prototype.eatAnyToken = function () {
                var token = this.currentToken();
                this.moveToNextToken();
                return token;
            };

            ParserImpl.prototype.moveToNextToken = function () {
                this.source.moveToNextToken();
            };

            ParserImpl.prototype.previousToken = function () {
                return this.source.previousToken();
            };

            ParserImpl.prototype.eatNode = function () {
                var node = this.source.currentNode();
                this.source.moveToNextNode();
                return node;
            };

            //this method is called very frequently
            //we should keep it simple so that it can be inlined.
            ParserImpl.prototype.eatToken = function (kind) {
                // Assert disabled because it is actually expensive enugh to affect perf.
                // Debug.assert(SyntaxFacts.isTokenKind(kind))
                var token = this.currentToken();
                if (token.tokenKind === kind) {
                    this.moveToNextToken();
                    return token;
                }

                //slow part of EatToken(SyntaxKind kind)
                return this.createMissingToken(kind, token);
            };

            // Eats the token if it is there.  Otherwise does nothing.  Will not report errors.
            ParserImpl.prototype.tryEatToken = function (kind) {
                if (this.currentToken().tokenKind === kind) {
                    return this.eatToken(kind);
                }

                return null;
            };

            ParserImpl.prototype.eatKeyword = function (kind) {
                // Debug.assert(SyntaxFacts.isTokenKind(kind))
                var token = this.currentToken();
                if (token.tokenKind === kind) {
                    this.moveToNextToken();
                    return token;
                }

                //slow part of EatToken(SyntaxKind kind)
                return this.createMissingToken(kind, token);
            };

            // An identifier is basically any word, unless it is a reserved keyword.  so 'foo' is an
            // identifier and 'return' is not.  Note: a word may or may not be an identifier depending
            // on the state of the parser.  For example, 'yield' is an identifier *unless* the parser
            // is in strict mode.
            ParserImpl.prototype.isIdentifier = function (token) {
                var tokenKind = token.tokenKind;

                if (tokenKind === 11 /* IdentifierName */) {
                    return true;
                }

                // Keywords are only identifiers if they're FutureReservedStrictWords and we're in
                // strict mode.  *Or* if it's a typescript 'keyword'.
                if (tokenKind >= TypeScript.SyntaxKind.FirstFutureReservedStrictKeyword) {
                    if (tokenKind <= TypeScript.SyntaxKind.LastFutureReservedStrictKeyword) {
                        // Could be a keyword or identifier.  It's an identifier if we're not in strict
                        // mode.
                        return !this.isInStrictMode;
                    }

                    // If it's typescript keyword, then it's actually a javascript identifier.
                    return tokenKind <= TypeScript.SyntaxKind.LastTypeScriptKeyword;
                }

                // Anything else is not an identifier.
                return false;
            };

            // This method should be called when the grammar calls for an *IdentifierName* and not an
            // *Identifier*.
            ParserImpl.prototype.eatIdentifierNameToken = function () {
                var token = this.currentToken();

                // If we have an identifier name, then consume and return it.
                if (token.tokenKind === 11 /* IdentifierName */) {
                    this.moveToNextToken();
                    return token;
                }

                // If we have a keyword, then it cna be used as an identifier name.  However, we need
                // to convert it to an identifier so that no later parts of the systems see it as a
                // keyword.
                if (TypeScript.SyntaxFacts.isAnyKeyword(token.tokenKind)) {
                    this.moveToNextToken();
                    return TypeScript.Syntax.convertToIdentifierName(token);
                }

                return this.createMissingToken(11 /* IdentifierName */, token);
            };

            // This method should be called when the grammar calls for an *Identifier* and not an
            // *IdentifierName*.
            ParserImpl.prototype.eatIdentifierToken = function () {
                var token = this.currentToken();
                if (this.isIdentifier(token)) {
                    this.moveToNextToken();

                    if (token.tokenKind === 11 /* IdentifierName */) {
                        return token;
                    }

                    return TypeScript.Syntax.convertToIdentifierName(token);
                }

                return this.createMissingToken(11 /* IdentifierName */, token);
            };

            ParserImpl.prototype.canEatAutomaticSemicolon = function (allowWithoutNewLine) {
                var token = this.currentToken();

                // An automatic semicolon is always allowed if we're at the end of the file.
                if (token.tokenKind === 10 /* EndOfFileToken */) {
                    return true;
                }

                // Or if the next token is a close brace (regardless of which line it is on).
                if (token.tokenKind === 71 /* CloseBraceToken */) {
                    return true;
                }

                if (allowWithoutNewLine) {
                    return true;
                }

                // It is also allowed if there is a newline between the last token seen and the next one.
                if (this.previousToken() !== null && this.previousToken().hasTrailingNewLine()) {
                    return true;
                }

                return false;
            };

            ParserImpl.prototype.canEatExplicitOrAutomaticSemicolon = function (allowWithoutNewline) {
                var token = this.currentToken();

                if (token.tokenKind === 78 /* SemicolonToken */) {
                    return true;
                }

                return this.canEatAutomaticSemicolon(allowWithoutNewline);
            };

            ParserImpl.prototype.eatExplicitOrAutomaticSemicolon = function (allowWithoutNewline) {
                var token = this.currentToken();

                // If we see a semicolon, then we can definitely eat it.
                if (token.tokenKind === 78 /* SemicolonToken */) {
                    return this.eatToken(78 /* SemicolonToken */);
                }

                // Check if an automatic semicolon could go here.  If so, synthesize one.  However, if the
                // user has the option set to error on automatic semicolons, then add an error to that
                // token as well.
                if (this.canEatAutomaticSemicolon(allowWithoutNewline)) {
                    // Note: the missing token needs to go between real tokens.  So we place it at the
                    // fullstart of the current token.
                    var semicolonToken = TypeScript.Syntax.emptyToken(78 /* SemicolonToken */);

                    if (!this.parseOptions.allowAutomaticSemicolonInsertion()) {
                        // Report the missing semicolon at the end of the *previous* token.
                        this.addDiagnostic(new TypeScript.Diagnostic(this.fileName, this.lineMap, this.previousTokenEnd(), 0, TypeScript.DiagnosticCode.Automatic_semicolon_insertion_not_allowed, null));
                    }

                    return semicolonToken;
                }

                // No semicolon could be consumed here at all.  Just call the standard eating function
                // so we get the token and the error for it.
                return this.eatToken(78 /* SemicolonToken */);
            };

            ParserImpl.prototype.isKeyword = function (kind) {
                if (kind >= TypeScript.SyntaxKind.FirstKeyword) {
                    if (kind <= TypeScript.SyntaxKind.LastFutureReservedKeyword) {
                        return true;
                    }

                    if (this.isInStrictMode) {
                        return kind <= TypeScript.SyntaxKind.LastFutureReservedStrictKeyword;
                    }
                }

                return false;
            };

            ParserImpl.prototype.createMissingToken = function (expectedKind, actual) {
                var diagnostic = this.getExpectedTokenDiagnostic(expectedKind, actual);
                this.addDiagnostic(diagnostic);

                // The missing token will be at the full start of the current token.  That way empty tokens
                // will always be between real tokens and not inside an actual token.
                return TypeScript.Syntax.emptyToken(expectedKind);
            };

            ParserImpl.prototype.getExpectedTokenDiagnostic = function (expectedKind, actual) {
                var token = this.currentToken();

                // They wanted something specific, just report that that token was missing.
                if (TypeScript.SyntaxFacts.isAnyKeyword(expectedKind) || TypeScript.SyntaxFacts.isAnyPunctuation(expectedKind)) {
                    return new TypeScript.Diagnostic(this.fileName, this.lineMap, this.currentTokenStart(), token.width(), TypeScript.DiagnosticCode._0_expected, [TypeScript.SyntaxFacts.getText(expectedKind)]);
                } else {
                    // They wanted an identifier.
                    // If the user supplied a keyword, give them a specialized message.
                    if (actual !== null && TypeScript.SyntaxFacts.isAnyKeyword(actual.tokenKind)) {
                        return new TypeScript.Diagnostic(this.fileName, this.lineMap, this.currentTokenStart(), token.width(), TypeScript.DiagnosticCode.Identifier_expected_0_is_a_keyword, [TypeScript.SyntaxFacts.getText(actual.tokenKind)]);
                    } else {
                        // Otherwise just report that an identifier was expected.
                        return new TypeScript.Diagnostic(this.fileName, this.lineMap, this.currentTokenStart(), token.width(), TypeScript.DiagnosticCode.Identifier_expected, null);
                    }
                }
                // throw Errors.notYetImplemented();
            };

            ParserImpl.getPrecedence = function (expressionKind) {
                switch (expressionKind) {
                    case 173 /* CommaExpression */:
                        return 1 /* CommaExpressionPrecedence */;

                    case 174 /* AssignmentExpression */:
                    case 175 /* AddAssignmentExpression */:
                    case 176 /* SubtractAssignmentExpression */:
                    case 177 /* MultiplyAssignmentExpression */:
                    case 178 /* DivideAssignmentExpression */:
                    case 179 /* ModuloAssignmentExpression */:
                    case 180 /* AndAssignmentExpression */:
                    case 181 /* ExclusiveOrAssignmentExpression */:
                    case 182 /* OrAssignmentExpression */:
                    case 183 /* LeftShiftAssignmentExpression */:
                    case 184 /* SignedRightShiftAssignmentExpression */:
                    case 185 /* UnsignedRightShiftAssignmentExpression */:
                        return 2 /* AssignmentExpressionPrecedence */;

                    case 186 /* ConditionalExpression */:
                        return 3 /* ConditionalExpressionPrecedence */;

                    case 187 /* LogicalOrExpression */:
                        return 5 /* LogicalOrExpressionPrecedence */;

                    case 188 /* LogicalAndExpression */:
                        return 6 /* LogicalAndExpressionPrecedence */;

                    case 189 /* BitwiseOrExpression */:
                        return 7 /* BitwiseOrExpressionPrecedence */;

                    case 190 /* BitwiseExclusiveOrExpression */:
                        return 8 /* BitwiseExclusiveOrExpressionPrecedence */;

                    case 191 /* BitwiseAndExpression */:
                        return 9 /* BitwiseAndExpressionPrecedence */;

                    case 192 /* EqualsWithTypeConversionExpression */:
                    case 193 /* NotEqualsWithTypeConversionExpression */:
                    case 194 /* EqualsExpression */:
                    case 195 /* NotEqualsExpression */:
                        return 10 /* EqualityExpressionPrecedence */;

                    case 196 /* LessThanExpression */:
                    case 197 /* GreaterThanExpression */:
                    case 198 /* LessThanOrEqualExpression */:
                    case 199 /* GreaterThanOrEqualExpression */:
                    case 200 /* InstanceOfExpression */:
                    case 201 /* InExpression */:
                        return 11 /* RelationalExpressionPrecedence */;

                    case 202 /* LeftShiftExpression */:
                    case 203 /* SignedRightShiftExpression */:
                    case 204 /* UnsignedRightShiftExpression */:
                        return 12 /* ShiftExpressionPrecdence */;

                    case 208 /* AddExpression */:
                    case 209 /* SubtractExpression */:
                        return 13 /* AdditiveExpressionPrecedence */;

                    case 205 /* MultiplyExpression */:
                    case 206 /* DivideExpression */:
                    case 207 /* ModuloExpression */:
                        return 14 /* MultiplicativeExpressionPrecedence */;

                    case 164 /* PlusExpression */:
                    case 165 /* NegateExpression */:
                    case 166 /* BitwiseNotExpression */:
                    case 167 /* LogicalNotExpression */:
                    case 170 /* DeleteExpression */:
                    case 171 /* TypeOfExpression */:
                    case 172 /* VoidExpression */:
                    case 168 /* PreIncrementExpression */:
                    case 169 /* PreDecrementExpression */:
                        return 15 /* UnaryExpressionPrecedence */;
                }

                throw TypeScript.Errors.invalidOperation();
            };

            ParserImpl.prototype.addSkippedTokenAfterNodeOrToken = function (nodeOrToken, skippedToken) {
                if (nodeOrToken.isToken()) {
                    return this.addSkippedTokenAfterToken(nodeOrToken, skippedToken);
                } else if (nodeOrToken.isNode()) {
                    return this.addSkippedTokenAfterNode(nodeOrToken, skippedToken);
                } else {
                    throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.addSkippedTokenAfterNode = function (node, skippedToken) {
                var oldToken = node.lastToken();
                var newToken = this.addSkippedTokenAfterToken(oldToken, skippedToken);

                return node.replaceToken(oldToken, newToken);
            };

            ParserImpl.prototype.addSkippedTokensBeforeNode = function (node, skippedTokens) {
                if (skippedTokens.length > 0) {
                    var oldToken = node.firstToken();
                    var newToken = this.addSkippedTokensBeforeToken(oldToken, skippedTokens);

                    return node.replaceToken(oldToken, newToken);
                }

                return node;
            };

            ParserImpl.prototype.addSkippedTokensBeforeToken = function (token, skippedTokens) {
                // Debug.assert(token.fullWidth() > 0 || token.tokenKind === SyntaxKind.EndOfFileToken);
                // Debug.assert(skippedTokens.length > 0);
                var leadingTrivia = [];
                for (var i = 0, n = skippedTokens.length; i < n; i++) {
                    this.addSkippedTokenToTriviaArray(leadingTrivia, skippedTokens[i]);
                }

                this.addTriviaTo(token.leadingTrivia(), leadingTrivia);

                // Don't need this array anymore.  Give it back so we can reuse it.
                this.returnArray(skippedTokens);
                return token.withLeadingTrivia(TypeScript.Syntax.triviaList(leadingTrivia));
            };

            ParserImpl.prototype.addSkippedTokensAfterToken = function (token, skippedTokens) {
                // Debug.assert(token.fullWidth() > 0);
                if (skippedTokens.length === 0) {
                    this.returnArray(skippedTokens);
                    return token;
                }

                var trailingTrivia = token.trailingTrivia().toArray();

                for (var i = 0, n = skippedTokens.length; i < n; i++) {
                    this.addSkippedTokenToTriviaArray(trailingTrivia, skippedTokens[i]);
                }

                // Don't need this array anymore.  Give it back so we can reuse it.
                this.returnArray(skippedTokens);
                return token.withTrailingTrivia(TypeScript.Syntax.triviaList(trailingTrivia));
            };

            ParserImpl.prototype.addSkippedTokenAfterToken = function (token, skippedToken) {
                // Debug.assert(token.fullWidth() > 0);
                var trailingTrivia = token.trailingTrivia().toArray();
                this.addSkippedTokenToTriviaArray(trailingTrivia, skippedToken);

                return token.withTrailingTrivia(TypeScript.Syntax.triviaList(trailingTrivia));
            };

            ParserImpl.prototype.addSkippedTokenToTriviaArray = function (array, skippedToken) {
                // Debug.assert(skippedToken.text().length > 0);
                // first, add the leading trivia of the skipped token to the array
                this.addTriviaTo(skippedToken.leadingTrivia(), array);

                // now, add the text of the token as skipped text to the trivia array.
                var trimmedToken = skippedToken.withLeadingTrivia(TypeScript.Syntax.emptyTriviaList).withTrailingTrivia(TypeScript.Syntax.emptyTriviaList);
                array.push(TypeScript.Syntax.skippedTokenTrivia(trimmedToken));

                // Finally, add the trailing trivia of the skipped token to the trivia array.
                this.addTriviaTo(skippedToken.trailingTrivia(), array);
            };

            ParserImpl.prototype.addTriviaTo = function (list, array) {
                for (var i = 0, n = list.count(); i < n; i++) {
                    array.push(list.syntaxTriviaAt(i));
                }
            };

            ParserImpl.prototype.parseSyntaxTree = function (isDeclaration) {
                var sourceUnit = this.parseSourceUnit();

                var allDiagnostics = this.source.tokenDiagnostics().concat(this.diagnostics);
                allDiagnostics.sort(function (a, b) {
                    return a.start() - b.start();
                });

                return new TypeScript.SyntaxTree(sourceUnit, isDeclaration, allDiagnostics, this.fileName, this.lineMap, this.parseOptions);
            };

            ParserImpl.prototype.setStrictMode = function (isInStrictMode) {
                this.isInStrictMode = isInStrictMode;
                this.factory = isInStrictMode ? TypeScript.Syntax.strictModeFactory : TypeScript.Syntax.normalModeFactory;
            };

            ParserImpl.prototype.parseSourceUnit = function () {
                // Note: technically we don't need to save and restore this here.  After all, this the top
                // level parsing entrypoint.  So it will always start as false and be reset to false when the
                // loop ends.  However, for sake of symmetry and consistancy we do this.
                var savedIsInStrictMode = this.isInStrictMode;

                var result = this.parseSyntaxList(ListParsingState.SourceUnit_ModuleElements, ParserImpl.updateStrictModeState);
                var moduleElements = result.list;

                this.setStrictMode(savedIsInStrictMode);

                var sourceUnit = this.factory.sourceUnit(moduleElements, this.currentToken());
                sourceUnit = this.addSkippedTokensBeforeNode(sourceUnit, result.skippedTokens);

                if (TypeScript.Debug.shouldAssert(2 /* Aggressive */)) {
                    TypeScript.Debug.assert(sourceUnit.fullWidth() === this.newText_forDebuggingPurposesOnly.length());

                    if (TypeScript.Debug.shouldAssert(3 /* VeryAggressive */)) {
                        TypeScript.Debug.assert(sourceUnit.fullText() === this.newText_forDebuggingPurposesOnly.substr(0, this.newText_forDebuggingPurposesOnly.length(), false));
                    }
                }

                return sourceUnit;
            };

            ParserImpl.updateStrictModeState = function (parser, items) {
                if (!parser.isInStrictMode) {
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        if (!TypeScript.SyntaxFacts.isDirectivePrologueElement(item)) {
                            return;
                        }
                    }

                    parser.setStrictMode(TypeScript.SyntaxFacts.isUseStrictDirective(items[items.length - 1]));
                }
            };

            ParserImpl.prototype.isModuleElement = function (inErrorRecovery) {
                if (this.currentNode() !== null && this.currentNode().isModuleElement()) {
                    return true;
                }

                var modifierCount = this.modifierCount();
                return this.isImportDeclaration(modifierCount) || this.isExportAssignment() || this.isModuleDeclaration(modifierCount) || this.isInterfaceDeclaration(modifierCount) || this.isClassDeclaration(modifierCount) || this.isEnumDeclaration(modifierCount) || this.isStatement(inErrorRecovery);
            };

            ParserImpl.prototype.parseModuleElement = function (inErrorRecovery) {
                if (this.currentNode() !== null && this.currentNode().isModuleElement()) {
                    return this.eatNode();
                }

                var modifierCount = this.modifierCount();
                if (this.isImportDeclaration(modifierCount)) {
                    return this.parseImportDeclaration();
                } else if (this.isExportAssignment()) {
                    return this.parseExportAssignment();
                } else if (this.isModuleDeclaration(modifierCount)) {
                    return this.parseModuleDeclaration();
                } else if (this.isInterfaceDeclaration(modifierCount)) {
                    return this.parseInterfaceDeclaration();
                } else if (this.isClassDeclaration(modifierCount)) {
                    return this.parseClassDeclaration();
                } else if (this.isEnumDeclaration(modifierCount)) {
                    return this.parseEnumDeclaration();
                } else if (this.isStatement(inErrorRecovery)) {
                    return this.parseStatement(inErrorRecovery);
                } else {
                    throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.isImportDeclaration = function (modifierCount) {
                // If we have at least one modifier, and we see 'import', then consider this an import
                // declaration.
                if (modifierCount > 0 && this.peekToken(modifierCount).tokenKind === 49 /* ImportKeyword */) {
                    return true;
                }

                // 'import' is not a javascript keyword.  So we need to use a bit of lookahead here to ensure
                // that we're actually looking at a import construct and not some javascript expression.
                return this.currentToken().tokenKind === 49 /* ImportKeyword */ && this.isIdentifier(this.peekToken(1));
            };

            ParserImpl.prototype.parseImportDeclaration = function () {
                // Debug.assert(this.isImportDeclaration());
                var modifiers = this.parseModifiers();
                var importKeyword = this.eatKeyword(49 /* ImportKeyword */);
                var identifier = this.eatIdentifierToken();
                var equalsToken = this.eatToken(107 /* EqualsToken */);
                var moduleReference = this.parseModuleReference();
                var semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.importDeclaration(modifiers, importKeyword, identifier, equalsToken, moduleReference, semicolonToken);
            };

            ParserImpl.prototype.isExportAssignment = function () {
                return this.currentToken().tokenKind === 47 /* ExportKeyword */ && this.peekToken(1).tokenKind === 107 /* EqualsToken */;
            };

            ParserImpl.prototype.parseExportAssignment = function () {
                // Debug.assert(this.isExportAssignment());
                var exportKeyword = this.eatKeyword(47 /* ExportKeyword */);
                var equalsToken = this.eatToken(107 /* EqualsToken */);
                var identifier = this.eatIdentifierToken();
                var semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.exportAssignment(exportKeyword, equalsToken, identifier, semicolonToken);
            };

            ParserImpl.prototype.parseModuleReference = function () {
                if (this.isExternalModuleReference()) {
                    return this.parseExternalModuleReference();
                } else {
                    return this.parseModuleNameModuleReference();
                }
            };

            ParserImpl.prototype.isExternalModuleReference = function () {
                var token0 = this.currentToken();
                if (token0.tokenKind === 66 /* RequireKeyword */) {
                    return this.peekToken(1).tokenKind === 72 /* OpenParenToken */;
                }

                return false;
            };

            ParserImpl.prototype.parseExternalModuleReference = function () {
                // Debug.assert(this.isExternalModuleReference());
                var requireKeyword = this.eatKeyword(66 /* RequireKeyword */);
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var stringLiteral = this.eatToken(14 /* StringLiteral */);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);

                return this.factory.externalModuleReference(requireKeyword, openParenToken, stringLiteral, closeParenToken);
            };

            ParserImpl.prototype.parseModuleNameModuleReference = function () {
                var name = this.parseName();
                return this.factory.moduleNameModuleReference(name);
            };

            // NOTE: This will allow all identifier names.  Even the ones that are keywords.
            ParserImpl.prototype.parseIdentifierName = function () {
                var identifierName = this.eatIdentifierNameToken();
                return identifierName;
            };

            ParserImpl.prototype.tryParseTypeArgumentList = function (inExpression) {
                if (this.currentToken().kind() !== 80 /* LessThanToken */) {
                    return null;
                }

                var lessThanToken;
                var greaterThanToken;
                var result;
                var typeArguments;

                if (!inExpression) {
                    // if we're not in an expression, this must be a type argument list.  Just parse
                    // it out as such.
                    lessThanToken = this.eatToken(80 /* LessThanToken */);

                    // Debug.assert(lessThanToken.fullWidth() > 0);
                    result = this.parseSeparatedSyntaxList(ListParsingState.TypeArgumentList_Types);
                    typeArguments = result.list;
                    lessThanToken = this.addSkippedTokensAfterToken(lessThanToken, result.skippedTokens);

                    greaterThanToken = this.eatToken(81 /* GreaterThanToken */);

                    return this.factory.typeArgumentList(lessThanToken, typeArguments, greaterThanToken);
                }

                // If we're in an expression, then we only want to consume this as a type argument list
                // if we're sure that it's a type arg list and not an arithmetic expression.
                var rewindPoint = this.getRewindPoint();

                // We've seen a '<'.  Try to parse it out as a type argument list.
                lessThanToken = this.eatToken(80 /* LessThanToken */);

                // Debug.assert(lessThanToken.fullWidth() > 0);
                result = this.parseSeparatedSyntaxList(ListParsingState.TypeArgumentList_Types);
                typeArguments = result.list;
                lessThanToken = this.addSkippedTokensAfterToken(lessThanToken, result.skippedTokens);

                greaterThanToken = this.eatToken(81 /* GreaterThanToken */);

                // We're in a context where '<' could be the start of a type argument list, or part
                // of an arithmetic expression.  We'll presume it's the latter unless we see the '>'
                // and a following token that guarantees that it's supposed to be a type argument list.
                if (greaterThanToken.fullWidth() === 0 || !this.canFollowTypeArgumentListInExpression(this.currentToken().kind())) {
                    this.rewind(rewindPoint);

                    this.releaseRewindPoint(rewindPoint);
                    return null;
                } else {
                    this.releaseRewindPoint(rewindPoint);
                    var typeArgumentList = this.factory.typeArgumentList(lessThanToken, typeArguments, greaterThanToken);

                    return typeArgumentList;
                }
            };

            ParserImpl.prototype.canFollowTypeArgumentListInExpression = function (kind) {
                switch (kind) {
                    case 72 /* OpenParenToken */:
                    case 76 /* DotToken */:

                    case 73 /* CloseParenToken */:
                    case 75 /* CloseBracketToken */:
                    case 106 /* ColonToken */:
                    case 78 /* SemicolonToken */:
                    case 79 /* CommaToken */:
                    case 105 /* QuestionToken */:
                    case 84 /* EqualsEqualsToken */:
                    case 87 /* EqualsEqualsEqualsToken */:
                    case 86 /* ExclamationEqualsToken */:
                    case 88 /* ExclamationEqualsEqualsToken */:
                    case 103 /* AmpersandAmpersandToken */:
                    case 104 /* BarBarToken */:
                    case 100 /* CaretToken */:
                    case 98 /* AmpersandToken */:
                    case 99 /* BarToken */:
                    case 71 /* CloseBraceToken */:
                    case 10 /* EndOfFileToken */:
                        // these cases can't legally follow a type arg list.  However, they're not legal
                        // expressions either.  The user is probably in the middle of a generic type. So
                        // treat it as such.
                        return true;

                    default:
                        // Anything else treat as an expression.
                        return false;
                }
            };

            ParserImpl.prototype.parseName = function () {
                var shouldContinue = this.isIdentifier(this.currentToken());
                var current = this.eatIdentifierToken();

                while (shouldContinue && this.currentToken().tokenKind === 76 /* DotToken */) {
                    var dotToken = this.eatToken(76 /* DotToken */);

                    var currentToken = this.currentToken();
                    var identifierName;

                    // Technically a keyword is valid here as all keywords are identifier names.
                    // However, often we'll encounter this in error situations when the keyword
                    // is actually starting another valid construct.
                    // So, we check for the following specific case:
                    //      name.
                    //      keyword identifierNameOrKeyword
                    // Note: the newlines are important here.  For example, if that above code
                    // were rewritten into:
                    //      name.keyword
                    //      identifierNameOrKeyword
                    // Then we would consider it valid.  That's because ASI would take effect and
                    // the code would be implicitly: "name.keyword; identifierNameOrKeyword".
                    // In the first case though, ASI will not take effect because there is not a
                    // line terminator after the dot.
                    if (TypeScript.SyntaxFacts.isAnyKeyword(currentToken.tokenKind) && this.previousToken().hasTrailingNewLine() && !currentToken.hasTrailingNewLine() && TypeScript.SyntaxFacts.isIdentifierNameOrAnyKeyword(this.peekToken(1))) {
                        identifierName = this.createMissingToken(11 /* IdentifierName */, currentToken);
                    } else {
                        identifierName = this.eatIdentifierNameToken();
                    }

                    current = this.factory.qualifiedName(current, dotToken, identifierName);

                    shouldContinue = identifierName.fullWidth() > 0;
                }

                return current;
            };

            ParserImpl.prototype.isEnumDeclaration = function (modifierCount) {
                // If we have at least one modifier, and we see 'enum', then consider this an enum
                // declaration.
                if (modifierCount > 0 && this.peekToken(modifierCount).tokenKind === 46 /* EnumKeyword */) {
                    return true;
                }

                // 'enum' is not a javascript keyword.  So we need to use a bit of lookahead here to ensure
                // that we're actually looking at a enum construct and not some javascript expression.
                return this.currentToken().tokenKind === 46 /* EnumKeyword */ && this.isIdentifier(this.peekToken(1));
            };

            ParserImpl.prototype.parseEnumDeclaration = function () {
                // Debug.assert(this.isEnumDeclaration());
                var modifiers = this.parseModifiers();
                var enumKeyword = this.eatKeyword(46 /* EnumKeyword */);
                var identifier = this.eatIdentifierToken();

                var openBraceToken = this.eatToken(70 /* OpenBraceToken */);
                var enumElements = TypeScript.Syntax.emptySeparatedList;

                if (openBraceToken.width() > 0) {
                    var result = this.parseSeparatedSyntaxList(ListParsingState.EnumDeclaration_EnumElements);
                    enumElements = result.list;
                    openBraceToken = this.addSkippedTokensAfterToken(openBraceToken, result.skippedTokens);
                }

                var closeBraceToken = this.eatToken(71 /* CloseBraceToken */);

                return this.factory.enumDeclaration(modifiers, enumKeyword, identifier, openBraceToken, enumElements, closeBraceToken);
            };

            ParserImpl.prototype.isEnumElement = function (inErrorRecovery) {
                if (this.currentNode() !== null && this.currentNode().kind() === 243 /* EnumElement */) {
                    return true;
                }

                return this.isPropertyName(this.currentToken(), inErrorRecovery);
            };

            ParserImpl.prototype.parseEnumElement = function () {
                // Debug.assert(this.isEnumElement());
                if (this.currentNode() !== null && this.currentNode().kind() === 243 /* EnumElement */) {
                    return this.eatNode();
                }

                var propertyName = this.eatPropertyName();
                var equalsValueClause = null;
                if (this.isEqualsValueClause(false)) {
                    equalsValueClause = this.parseEqualsValueClause(true);
                }

                return this.factory.enumElement(propertyName, equalsValueClause);
            };

            ParserImpl.isModifier = function (token) {
                switch (token.tokenKind) {
                    case 57 /* PublicKeyword */:
                    case 55 /* PrivateKeyword */:
                    case 58 /* StaticKeyword */:
                    case 47 /* ExportKeyword */:
                    case 63 /* DeclareKeyword */:
                        return true;

                    default:
                        return false;
                }
            };

            ParserImpl.prototype.modifierCount = function () {
                var modifierCount = 0;
                while (true) {
                    if (ParserImpl.isModifier(this.peekToken(modifierCount))) {
                        modifierCount++;
                        continue;
                    }

                    break;
                }

                return modifierCount;
            };

            ParserImpl.prototype.parseModifiers = function () {
                var tokens = this.getArray();

                while (true) {
                    if (ParserImpl.isModifier(this.currentToken())) {
                        tokens.push(this.eatAnyToken());
                        continue;
                    }

                    break;
                }

                var result = TypeScript.Syntax.list(tokens);

                // If the tokens array is greater than one, then we can't return it.  It will have been
                // copied directly into the syntax list.
                this.returnZeroOrOneLengthArray(tokens);

                return result;
            };

            ParserImpl.prototype.isClassDeclaration = function (modifierCount) {
                // If we have at least one modifier, and we see 'class', then consider this a class
                // declaration.
                if (modifierCount > 0 && this.peekToken(modifierCount).tokenKind === 44 /* ClassKeyword */) {
                    return true;
                }

                // 'class' is not a javascript keyword.  So we need to use a bit of lookahead here to ensure
                // that we're actually looking at a class construct and not some javascript expression.
                return this.currentToken().tokenKind === 44 /* ClassKeyword */ && this.isIdentifier(this.peekToken(1));
            };

            ParserImpl.prototype.parseHeritageClauses = function () {
                var heritageClauses = TypeScript.Syntax.emptyList;

                if (this.isHeritageClause()) {
                    var result = this.parseSyntaxList(ListParsingState.ClassOrInterfaceDeclaration_HeritageClauses);
                    heritageClauses = result.list;
                    TypeScript.Debug.assert(result.skippedTokens.length === 0);
                }

                return heritageClauses;
            };

            ParserImpl.prototype.parseClassDeclaration = function () {
                // Debug.assert(this.isClassDeclaration());
                var modifiers = this.parseModifiers();

                var classKeyword = this.eatKeyword(44 /* ClassKeyword */);
                var identifier = this.eatIdentifierToken();
                var typeParameterList = this.parseOptionalTypeParameterList(false);
                var heritageClauses = this.parseHeritageClauses();
                var openBraceToken = this.eatToken(70 /* OpenBraceToken */);
                var classElements = TypeScript.Syntax.emptyList;

                if (openBraceToken.width() > 0) {
                    var result = this.parseSyntaxList(ListParsingState.ClassDeclaration_ClassElements);

                    classElements = result.list;
                    openBraceToken = this.addSkippedTokensAfterToken(openBraceToken, result.skippedTokens);
                }

                var closeBraceToken = this.eatToken(71 /* CloseBraceToken */);
                return this.factory.classDeclaration(modifiers, classKeyword, identifier, typeParameterList, heritageClauses, openBraceToken, classElements, closeBraceToken);
            };

            ParserImpl.isPublicOrPrivateKeyword = function (token) {
                return token.tokenKind === 57 /* PublicKeyword */ || token.tokenKind === 55 /* PrivateKeyword */;
            };

            ParserImpl.prototype.isAccessor = function (inErrorRecovery) {
                var index = this.modifierCount();

                if (this.peekToken(index).tokenKind !== 64 /* GetKeyword */ && this.peekToken(index).tokenKind !== 68 /* SetKeyword */) {
                    return false;
                }

                index++;
                return this.isPropertyName(this.peekToken(index), inErrorRecovery);
            };

            ParserImpl.prototype.parseAccessor = function (checkForStrictMode) {
                // Debug.assert(this.isMemberAccessorDeclaration());
                var modifiers = this.parseModifiers();

                if (this.currentToken().tokenKind === 64 /* GetKeyword */) {
                    return this.parseGetMemberAccessorDeclaration(modifiers, checkForStrictMode);
                } else if (this.currentToken().tokenKind === 68 /* SetKeyword */) {
                    return this.parseSetMemberAccessorDeclaration(modifiers, checkForStrictMode);
                } else {
                    throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.parseGetMemberAccessorDeclaration = function (modifiers, checkForStrictMode) {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.GetKeyword);
                var getKeyword = this.eatKeyword(64 /* GetKeyword */);
                var propertyName = this.eatPropertyName();
                var parameterList = this.parseParameterList();
                var typeAnnotation = this.parseOptionalTypeAnnotation(false);
                var block = this.parseBlock(false, checkForStrictMode);

                return this.factory.getAccessor(modifiers, getKeyword, propertyName, parameterList, typeAnnotation, block);
            };

            ParserImpl.prototype.parseSetMemberAccessorDeclaration = function (modifiers, checkForStrictMode) {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.SetKeyword);
                var setKeyword = this.eatKeyword(68 /* SetKeyword */);
                var propertyName = this.eatPropertyName();
                var parameterList = this.parseParameterList();
                var block = this.parseBlock(false, checkForStrictMode);

                return this.factory.setAccessor(modifiers, setKeyword, propertyName, parameterList, block);
            };

            ParserImpl.prototype.isClassElement = function (inErrorRecovery) {
                if (this.currentNode() !== null && this.currentNode().isClassElement()) {
                    return true;
                }

                // Note: the order of these calls is important.  Specifically, isMemberVariableDeclaration
                // checks for a subset of the conditions of the previous two calls.
                return this.isConstructorDeclaration() || this.isMemberFunctionDeclaration(inErrorRecovery) || this.isAccessor(inErrorRecovery) || this.isMemberVariableDeclaration(inErrorRecovery) || this.isIndexMemberDeclaration();
            };

            ParserImpl.prototype.parseClassElement = function (inErrorRecovery) {
                // Debug.assert(this.isClassElement());
                if (this.currentNode() !== null && this.currentNode().isClassElement()) {
                    return this.eatNode();
                }

                if (this.isConstructorDeclaration()) {
                    return this.parseConstructorDeclaration();
                } else if (this.isMemberFunctionDeclaration(inErrorRecovery)) {
                    return this.parseMemberFunctionDeclaration();
                } else if (this.isAccessor(inErrorRecovery)) {
                    return this.parseAccessor(false);
                } else if (this.isMemberVariableDeclaration(inErrorRecovery)) {
                    return this.parseMemberVariableDeclaration();
                } else if (this.isIndexMemberDeclaration()) {
                    return this.parseIndexMemberDeclaration();
                } else {
                    throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.isConstructorDeclaration = function () {
                // Note: we deviate slightly from the spec here.  If we see 'constructor' then we
                // assume this is a constructor.  That means, if a user writes "public constructor;"
                // it won't be viewed as a member.  As a workaround, they can simply write:
                //      public 'constructor';
                var index = this.modifierCount();
                return this.peekToken(index).tokenKind === 62 /* ConstructorKeyword */;
            };

            ParserImpl.prototype.parseConstructorDeclaration = function () {
                // Debug.assert(this.isConstructorDeclaration());
                var modifiers = this.parseModifiers();
                var constructorKeyword = this.eatKeyword(62 /* ConstructorKeyword */);
                var callSignature = this.parseCallSignature(false);

                var semicolonToken = null;
                var block = null;

                if (this.isBlock()) {
                    block = this.parseBlock(false, true);
                } else {
                    semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);
                }

                return this.factory.constructorDeclaration(modifiers, constructorKeyword, callSignature, block, semicolonToken);
            };

            ParserImpl.prototype.isMemberFunctionDeclaration = function (inErrorRecovery) {
                var index = 0;

                while (true) {
                    var token = this.peekToken(index);
                    if (this.isPropertyName(token, inErrorRecovery) && this.isCallSignature(index + 1)) {
                        return true;
                    }

                    // We weren't at the name of the function.  If we have a modifier token, then
                    // consume it and try again.
                    if (ParserImpl.isModifier(token)) {
                        index++;
                        continue;
                    }

                    // Wasn't a member function.
                    return false;
                }
            };

            ParserImpl.prototype.parseMemberFunctionDeclaration = function () {
                // Debug.assert(this.isMemberFunctionDeclaration());
                var modifierArray = this.getArray();

                while (true) {
                    var currentToken = this.currentToken();
                    if (this.isPropertyName(currentToken, false) && this.isCallSignature(1)) {
                        break;
                    }

                    TypeScript.Debug.assert(ParserImpl.isModifier(currentToken));
                    modifierArray.push(this.eatAnyToken());
                }

                var modifiers = TypeScript.Syntax.list(modifierArray);
                this.returnZeroOrOneLengthArray(modifierArray);

                var propertyName = this.eatPropertyName();
                var callSignature = this.parseCallSignature(false);

                // If we got an errant => then we want to parse what's coming up without requiring an
                // open brace.
                var parseBlockEvenWithNoOpenBrace = false;
                var newCallSignature = this.tryAddUnexpectedEqualsGreaterThanToken(callSignature);
                if (newCallSignature !== callSignature) {
                    parseBlockEvenWithNoOpenBrace = true;
                    callSignature = newCallSignature;
                }

                var block = null;
                var semicolon = null;

                if (parseBlockEvenWithNoOpenBrace || this.isBlock()) {
                    block = this.parseBlock(parseBlockEvenWithNoOpenBrace, true);
                } else {
                    semicolon = this.eatExplicitOrAutomaticSemicolon(false);
                }

                return this.factory.memberFunctionDeclaration(modifiers, propertyName, callSignature, block, semicolon);
            };

            ParserImpl.prototype.isDefinitelyMemberVariablePropertyName = function (index) {
                // keywords are also property names.  Only accept a keyword as a property
                // name if is of the form:
                //      public;
                //      public=
                //      public:
                //      public }
                //      public <eof>
                if (TypeScript.SyntaxFacts.isAnyKeyword(this.peekToken(index).tokenKind)) {
                    switch (this.peekToken(index + 1).tokenKind) {
                        case 78 /* SemicolonToken */:
                        case 107 /* EqualsToken */:
                        case 106 /* ColonToken */:
                        case 71 /* CloseBraceToken */:
                        case 10 /* EndOfFileToken */:
                            return true;
                        default:
                            return false;
                    }
                } else {
                    // If was a property name and not a keyword, then we're good to go.
                    return true;
                }
            };

            ParserImpl.prototype.isMemberVariableDeclaration = function (inErrorRecovery) {
                var index = 0;

                while (true) {
                    var token = this.peekToken(index);
                    if (this.isPropertyName(token, inErrorRecovery) && this.isDefinitelyMemberVariablePropertyName(index)) {
                        return true;
                    }

                    // We weren't at the name of the variable.  If we have a modifier token, then
                    // consume it and try again.
                    if (ParserImpl.isModifier(this.peekToken(index))) {
                        index++;
                        continue;
                    }

                    // Wasn't a member variable.
                    return false;
                }
            };

            ParserImpl.prototype.parseMemberVariableDeclaration = function () {
                // Debug.assert(this.isMemberVariableDeclaration());
                var modifierArray = this.getArray();

                while (true) {
                    var currentToken = this.currentToken();
                    if (this.isPropertyName(currentToken, false) && this.isDefinitelyMemberVariablePropertyName(0)) {
                        break;
                    }

                    TypeScript.Debug.assert(ParserImpl.isModifier(currentToken));
                    modifierArray.push(this.eatAnyToken());
                }

                var modifiers = TypeScript.Syntax.list(modifierArray);
                this.returnZeroOrOneLengthArray(modifierArray);

                var variableDeclarator = this.parseVariableDeclarator(true, true);
                var semicolon = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.memberVariableDeclaration(modifiers, variableDeclarator, semicolon);
            };

            ParserImpl.prototype.isIndexMemberDeclaration = function () {
                var index = this.modifierCount();
                return this.isIndexSignature(index);
            };

            ParserImpl.prototype.parseIndexMemberDeclaration = function () {
                // Debug.assert(this.isIndexMemberDeclaration()
                var modifiers = this.parseModifiers();
                var indexSignature = this.parseIndexSignature();
                var semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.indexMemberDeclaration(modifiers, indexSignature, semicolonToken);
            };

            ParserImpl.prototype.tryAddUnexpectedEqualsGreaterThanToken = function (callSignature) {
                var token0 = this.currentToken();

                var hasEqualsGreaterThanToken = token0.tokenKind === 85 /* EqualsGreaterThanToken */;
                if (hasEqualsGreaterThanToken) {
                    // We can only do this if the call signature actually contains a final token that we
                    // could add the => to.
                    if (callSignature.lastToken()) {
                        // Previously the language allowed "function f() => expr;" as a shorthand for
                        // "function f() { return expr; }.
                        //
                        // Detect if the user is typing this and attempt recovery.
                        var diagnostic = new TypeScript.Diagnostic(this.fileName, this.lineMap, this.currentTokenStart(), token0.width(), TypeScript.DiagnosticCode.Unexpected_token_0_expected, [TypeScript.SyntaxFacts.getText(70 /* OpenBraceToken */)]);
                        this.addDiagnostic(diagnostic);

                        var token = this.eatAnyToken();
                        return this.addSkippedTokenAfterNode(callSignature, token0);
                    }
                }

                return callSignature;
            };

            ParserImpl.prototype.isFunctionDeclaration = function () {
                var index = this.modifierCount();
                return this.peekToken(index).tokenKind === 27 /* FunctionKeyword */;
            };

            ParserImpl.prototype.parseFunctionDeclaration = function () {
                // Debug.assert(this.isFunctionDeclaration());
                var modifiers = this.parseModifiers();
                var functionKeyword = this.eatKeyword(27 /* FunctionKeyword */);
                var identifier = this.eatIdentifierToken();
                var callSignature = this.parseCallSignature(false);

                // If we got an errant => then we want to parse what's coming up without requiring an
                // open brace.
                var parseBlockEvenWithNoOpenBrace = false;
                var newCallSignature = this.tryAddUnexpectedEqualsGreaterThanToken(callSignature);
                if (newCallSignature !== callSignature) {
                    parseBlockEvenWithNoOpenBrace = true;
                    callSignature = newCallSignature;
                }

                var semicolonToken = null;
                var block = null;

                // Parse a block if we're on a bock, or if we saw a '=>'
                if (parseBlockEvenWithNoOpenBrace || this.isBlock()) {
                    block = this.parseBlock(parseBlockEvenWithNoOpenBrace, true);
                } else {
                    semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);
                }

                return this.factory.functionDeclaration(modifiers, functionKeyword, identifier, callSignature, block, semicolonToken);
            };

            ParserImpl.prototype.isModuleDeclaration = function (modifierCount) {
                // If we have at least one modifier, and we see 'module', then consider this a module
                // declaration.
                if (modifierCount > 0 && this.peekToken(modifierCount).tokenKind === 65 /* ModuleKeyword */) {
                    return true;
                }

                // 'module' is not a javascript keyword.  So we need to use a bit of lookahead here to ensure
                // that we're actually looking at a module construct and not some javascript expression.
                if (this.currentToken().tokenKind === 65 /* ModuleKeyword */) {
                    var token1 = this.peekToken(1);
                    return this.isIdentifier(token1) || token1.tokenKind === 14 /* StringLiteral */;
                }

                return false;
            };

            ParserImpl.prototype.parseModuleDeclaration = function () {
                // Debug.assert(this.isModuleDeclaration());
                var modifiers = this.parseModifiers();
                var moduleKeyword = this.eatKeyword(65 /* ModuleKeyword */);

                var moduleName = null;
                var stringLiteral = null;

                if (this.currentToken().tokenKind === 14 /* StringLiteral */) {
                    stringLiteral = this.eatToken(14 /* StringLiteral */);
                } else {
                    moduleName = this.parseName();
                }

                var openBraceToken = this.eatToken(70 /* OpenBraceToken */);

                var moduleElements = TypeScript.Syntax.emptyList;
                if (openBraceToken.width() > 0) {
                    var result = this.parseSyntaxList(ListParsingState.ModuleDeclaration_ModuleElements);
                    moduleElements = result.list;
                    openBraceToken = this.addSkippedTokensAfterToken(openBraceToken, result.skippedTokens);
                }

                var closeBraceToken = this.eatToken(71 /* CloseBraceToken */);

                return this.factory.moduleDeclaration(modifiers, moduleKeyword, moduleName, stringLiteral, openBraceToken, moduleElements, closeBraceToken);
            };

            ParserImpl.prototype.isInterfaceDeclaration = function (modifierCount) {
                // If we have at least one modifier, and we see 'interface', then consider this an interface
                // declaration.
                if (modifierCount > 0 && this.peekToken(modifierCount).tokenKind === 52 /* InterfaceKeyword */) {
                    return true;
                }

                // 'interface' is not a javascript keyword.  So we need to use a bit of lookahead here to ensure
                // that we're actually looking at a interface construct and not some javascript expression.
                return this.currentToken().tokenKind === 52 /* InterfaceKeyword */ && this.isIdentifier(this.peekToken(1));
            };

            ParserImpl.prototype.parseInterfaceDeclaration = function () {
                // Debug.assert(this.isInterfaceDeclaration());
                var modifiers = this.parseModifiers();
                var interfaceKeyword = this.eatKeyword(52 /* InterfaceKeyword */);
                var identifier = this.eatIdentifierToken();
                var typeParameterList = this.parseOptionalTypeParameterList(false);
                var heritageClauses = this.parseHeritageClauses();

                var objectType = this.parseObjectType();
                return this.factory.interfaceDeclaration(modifiers, interfaceKeyword, identifier, typeParameterList, heritageClauses, objectType);
            };

            ParserImpl.prototype.parseObjectType = function () {
                var openBraceToken = this.eatToken(70 /* OpenBraceToken */);

                var typeMembers = TypeScript.Syntax.emptySeparatedList;
                if (openBraceToken.width() > 0) {
                    var result = this.parseSeparatedSyntaxList(ListParsingState.ObjectType_TypeMembers);
                    typeMembers = result.list;
                    openBraceToken = this.addSkippedTokensAfterToken(openBraceToken, result.skippedTokens);
                }

                var closeBraceToken = this.eatToken(71 /* CloseBraceToken */);
                return this.factory.objectType(openBraceToken, typeMembers, closeBraceToken);
            };

            ParserImpl.prototype.isTypeMember = function (inErrorRecovery) {
                if (this.currentNode() !== null && this.currentNode().isTypeMember()) {
                    return true;
                }

                return this.isCallSignature(0) || this.isConstructSignature() || this.isIndexSignature(0) || this.isMethodSignature(inErrorRecovery) || this.isPropertySignature(inErrorRecovery);
            };

            ParserImpl.prototype.parseTypeMember = function (inErrorRecovery) {
                if (this.currentNode() !== null && this.currentNode().isTypeMember()) {
                    return this.eatNode();
                }

                if (this.isCallSignature(0)) {
                    return this.parseCallSignature(false);
                } else if (this.isConstructSignature()) {
                    return this.parseConstructSignature();
                } else if (this.isIndexSignature(0)) {
                    return this.parseIndexSignature();
                } else if (this.isMethodSignature(inErrorRecovery)) {
                    // Note: it is important that isFunctionSignature is called before isPropertySignature.
                    // isPropertySignature checks for a subset of isFunctionSignature.
                    return this.parseMethodSignature();
                } else if (this.isPropertySignature(inErrorRecovery)) {
                    return this.parsePropertySignature();
                } else {
                    throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.parseConstructSignature = function () {
                // Debug.assert(this.isConstructSignature());
                var newKeyword = this.eatKeyword(31 /* NewKeyword */);
                var callSignature = this.parseCallSignature(false);

                return this.factory.constructSignature(newKeyword, callSignature);
            };

            ParserImpl.prototype.parseIndexSignature = function () {
                // Debug.assert(this.isIndexSignature());
                var openBracketToken = this.eatToken(74 /* OpenBracketToken */);
                var parameter = this.parseParameter();
                var closeBracketToken = this.eatToken(75 /* CloseBracketToken */);
                var typeAnnotation = this.parseOptionalTypeAnnotation(false);

                return this.factory.indexSignature(openBracketToken, parameter, closeBracketToken, typeAnnotation);
            };

            ParserImpl.prototype.parseMethodSignature = function () {
                // Debug.assert(this.isMethodSignature());
                var propertyName = this.eatPropertyName();
                var questionToken = this.tryEatToken(105 /* QuestionToken */);
                var callSignature = this.parseCallSignature(false);

                return this.factory.methodSignature(propertyName, questionToken, callSignature);
            };

            ParserImpl.prototype.parsePropertySignature = function () {
                // Debug.assert(this.isPropertySignature());
                var propertyName = this.eatPropertyName();
                var questionToken = this.tryEatToken(105 /* QuestionToken */);
                var typeAnnotation = this.parseOptionalTypeAnnotation(false);

                return this.factory.propertySignature(propertyName, questionToken, typeAnnotation);
            };

            ParserImpl.prototype.isCallSignature = function (tokenIndex) {
                var tokenKind = this.peekToken(tokenIndex).tokenKind;
                return tokenKind === 72 /* OpenParenToken */ || tokenKind === 80 /* LessThanToken */;
            };

            ParserImpl.prototype.isConstructSignature = function () {
                if (this.currentToken().tokenKind !== 31 /* NewKeyword */) {
                    return false;
                }

                var token1 = this.peekToken(1);
                return token1.tokenKind === 80 /* LessThanToken */ || token1.tokenKind === 72 /* OpenParenToken */;
            };

            ParserImpl.prototype.isIndexSignature = function (tokenIndex) {
                return this.peekToken(tokenIndex).tokenKind === 74 /* OpenBracketToken */;
            };

            ParserImpl.prototype.isMethodSignature = function (inErrorRecovery) {
                if (this.isPropertyName(this.currentToken(), inErrorRecovery)) {
                    // id(
                    if (this.isCallSignature(1)) {
                        return true;
                    }

                    // id?(
                    if (this.peekToken(1).tokenKind === 105 /* QuestionToken */ && this.isCallSignature(2)) {
                        return true;
                    }
                }

                return false;
            };

            ParserImpl.prototype.isPropertySignature = function (inErrorRecovery) {
                var currentToken = this.currentToken();

                // Keywords can start properties.  However, they're often intended to start something
                // else.  If we see a modifier before something that can be a property, then don't
                // try parse it out as a property.  For example, if we have:
                //
                //      public foo
                //
                // Then don't parse 'public' as a property name.  Note: if you have:
                //
                //      public
                //      foo
                //
                // Then we *should* parse it as a property name, as ASI takes effect here.
                if (ParserImpl.isModifier(currentToken) && !currentToken.hasTrailingNewLine() && this.isPropertyName(this.peekToken(1), inErrorRecovery)) {
                    return false;
                }

                // Note: property names also start function signatures.  So it's important that we call this
                // after we calll isFunctionSignature.
                return this.isPropertyName(currentToken, inErrorRecovery);
            };

            ParserImpl.prototype.isHeritageClause = function () {
                var token0 = this.currentToken();
                return token0.tokenKind === 48 /* ExtendsKeyword */ || token0.tokenKind === 51 /* ImplementsKeyword */;
            };

            ParserImpl.prototype.isNotHeritageClauseTypeName = function () {
                if (this.currentToken().tokenKind === 51 /* ImplementsKeyword */ || this.currentToken().tokenKind === 48 /* ExtendsKeyword */) {
                    return this.isIdentifier(this.peekToken(1));
                }

                return false;
            };

            ParserImpl.prototype.isHeritageClauseTypeName = function () {
                if (this.isIdentifier(this.currentToken())) {
                    // We want to make sure that the "extends" in "extends foo" or the "implements" in
                    // "implements foo" is not considered a type name.
                    return !this.isNotHeritageClauseTypeName();
                }

                return false;
            };

            ParserImpl.prototype.parseHeritageClause = function () {
                // Debug.assert(this.isHeritageClause());
                var extendsOrImplementsKeyword = this.eatAnyToken();
                TypeScript.Debug.assert(extendsOrImplementsKeyword.tokenKind === 48 /* ExtendsKeyword */ || extendsOrImplementsKeyword.tokenKind === 51 /* ImplementsKeyword */);

                var result = this.parseSeparatedSyntaxList(ListParsingState.HeritageClause_TypeNameList);
                var typeNames = result.list;
                extendsOrImplementsKeyword = this.addSkippedTokensAfterToken(extendsOrImplementsKeyword, result.skippedTokens);

                return this.factory.heritageClause(extendsOrImplementsKeyword.tokenKind === 48 /* ExtendsKeyword */ ? 230 /* ExtendsHeritageClause */ : 231 /* ImplementsHeritageClause */, extendsOrImplementsKeyword, typeNames);
            };

            ParserImpl.prototype.isStatement = function (inErrorRecovery) {
                if (this.currentNode() !== null && this.currentNode().isStatement()) {
                    return true;
                }

                var currentToken = this.currentToken();
                var currentTokenKind = currentToken.tokenKind;
                switch (currentTokenKind) {
                    case 57 /* PublicKeyword */:
                    case 55 /* PrivateKeyword */:
                    case 58 /* StaticKeyword */:
                        // None of hte above are actually keywords.  And they might show up in a real
                        // statement (i.e. "public();").  However, if we see 'public <identifier>' then
                        // that can't possibly be a statement (and instead will be a class element),
                        // and we should not parse it out here.
                        var token1 = this.peekToken(1);
                        if (TypeScript.SyntaxFacts.isIdentifierNameOrAnyKeyword(token1)) {
                            // Definitely not a statement.
                            return false;
                        }

                        break;

                    case 28 /* IfKeyword */:
                    case 70 /* OpenBraceToken */:
                    case 33 /* ReturnKeyword */:
                    case 34 /* SwitchKeyword */:
                    case 36 /* ThrowKeyword */:
                    case 15 /* BreakKeyword */:
                    case 18 /* ContinueKeyword */:
                    case 26 /* ForKeyword */:
                    case 42 /* WhileKeyword */:
                    case 43 /* WithKeyword */:
                    case 22 /* DoKeyword */:
                    case 38 /* TryKeyword */:
                    case 19 /* DebuggerKeyword */:
                        return true;
                }

                // Check for common things that might appear where we expect a statement, but which we
                // do not want to consume.  This can happen when the user does not terminate their
                // existing block properly.  We don't want to accidently consume these as expression
                // below.
                if (this.isInterfaceDeclaration(0) || this.isClassDeclaration(0) || this.isEnumDeclaration(0) || this.isModuleDeclaration(0)) {
                    return false;
                }

                // More complicated cases.
                return this.isLabeledStatement(currentToken) || this.isVariableStatement() || this.isFunctionDeclaration() || this.isEmptyStatement(currentToken, inErrorRecovery) || this.isExpressionStatement(currentToken);
            };

            ParserImpl.prototype.parseStatement = function (inErrorRecovery) {
                if (this.currentNode() !== null && this.currentNode().isStatement()) {
                    return this.eatNode();
                }

                var currentToken = this.currentToken();
                var currentTokenKind = currentToken.tokenKind;

                switch (currentTokenKind) {
                    case 28 /* IfKeyword */:
                        return this.parseIfStatement();
                    case 70 /* OpenBraceToken */:
                        return this.parseBlock(false, false);
                    case 33 /* ReturnKeyword */:
                        return this.parseReturnStatement();
                    case 34 /* SwitchKeyword */:
                        return this.parseSwitchStatement();
                    case 36 /* ThrowKeyword */:
                        return this.parseThrowStatement();
                    case 15 /* BreakKeyword */:
                        return this.parseBreakStatement();
                    case 18 /* ContinueKeyword */:
                        return this.parseContinueStatement();
                    case 26 /* ForKeyword */:
                        return this.parseForOrForInStatement();
                    case 42 /* WhileKeyword */:
                        return this.parseWhileStatement();
                    case 43 /* WithKeyword */:
                        return this.parseWithStatement();
                    case 22 /* DoKeyword */:
                        return this.parseDoStatement();
                    case 38 /* TryKeyword */:
                        return this.parseTryStatement();
                    case 19 /* DebuggerKeyword */:
                        return this.parseDebuggerStatement();
                }

                if (this.isVariableStatement()) {
                    return this.parseVariableStatement();
                } else if (this.isLabeledStatement(currentToken)) {
                    return this.parseLabeledStatement();
                } else if (this.isFunctionDeclaration()) {
                    return this.parseFunctionDeclaration();
                } else if (this.isEmptyStatement(currentToken, inErrorRecovery)) {
                    return this.parseEmptyStatement();
                } else {
                    // Fall back to parsing this as expression statement.
                    return this.parseExpressionStatement();
                }
            };

            ParserImpl.prototype.parseDebuggerStatement = function () {
                // Debug.assert(this.isDebuggerStatement());
                var debuggerKeyword = this.eatKeyword(19 /* DebuggerKeyword */);
                var semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.debuggerStatement(debuggerKeyword, semicolonToken);
            };

            ParserImpl.prototype.parseDoStatement = function () {
                // Debug.assert(this.isDoStatement());
                var doKeyword = this.eatKeyword(22 /* DoKeyword */);
                var statement = this.parseStatement(false);
                var whileKeyword = this.eatKeyword(42 /* WhileKeyword */);
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var condition = this.parseExpression(true);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);

                // From: https://mail.mozilla.org/pipermail/es-discuss/2011-August/016188.html
                // 157 min --- All allen at wirfs-brock.com CONF --- "do{;}while(false)false" prohibited in
                // spec but allowed in consensus reality. Approved -- this is the de-facto standard whereby
                //  do;while(0)x will have a semicolon inserted before x.
                var semicolonToken = this.eatExplicitOrAutomaticSemicolon(true);

                return this.factory.doStatement(doKeyword, statement, whileKeyword, openParenToken, condition, closeParenToken, semicolonToken);
            };

            ParserImpl.prototype.isLabeledStatement = function (currentToken) {
                return this.isIdentifier(currentToken) && this.peekToken(1).tokenKind === 106 /* ColonToken */;
            };

            ParserImpl.prototype.parseLabeledStatement = function () {
                // Debug.assert(this.isLabeledStatement());
                var identifier = this.eatIdentifierToken();
                var colonToken = this.eatToken(106 /* ColonToken */);
                var statement = this.parseStatement(false);

                return this.factory.labeledStatement(identifier, colonToken, statement);
            };

            ParserImpl.prototype.parseTryStatement = function () {
                // Debug.assert(this.isTryStatement());
                var tryKeyword = this.eatKeyword(38 /* TryKeyword */);

                var savedListParsingState = this.listParsingState;
                this.listParsingState |= ListParsingState.TryBlock_Statements;
                var block = this.parseBlock(false, false);
                this.listParsingState = savedListParsingState;

                var catchClause = null;
                if (this.isCatchClause()) {
                    catchClause = this.parseCatchClause();
                }

                // If we don't have a catch clause, then we must have a finally clause.  Try to parse
                // one out no matter what.
                var finallyClause = null;
                if (catchClause === null || this.isFinallyClause()) {
                    finallyClause = this.parseFinallyClause();
                }

                return this.factory.tryStatement(tryKeyword, block, catchClause, finallyClause);
            };

            ParserImpl.prototype.isCatchClause = function () {
                return this.currentToken().tokenKind === 17 /* CatchKeyword */;
            };

            ParserImpl.prototype.parseCatchClause = function () {
                // Debug.assert(this.isCatchClause());
                var catchKeyword = this.eatKeyword(17 /* CatchKeyword */);
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var identifier = this.eatIdentifierToken();
                var typeAnnotation = this.parseOptionalTypeAnnotation(false);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);

                var savedListParsingState = this.listParsingState;
                this.listParsingState |= ListParsingState.CatchBlock_Statements;
                var block = this.parseBlock(false, false);
                this.listParsingState = savedListParsingState;

                return this.factory.catchClause(catchKeyword, openParenToken, identifier, typeAnnotation, closeParenToken, block);
            };

            ParserImpl.prototype.isFinallyClause = function () {
                return this.currentToken().tokenKind === 25 /* FinallyKeyword */;
            };

            ParserImpl.prototype.parseFinallyClause = function () {
                var finallyKeyword = this.eatKeyword(25 /* FinallyKeyword */);
                var block = this.parseBlock(false, false);

                return this.factory.finallyClause(finallyKeyword, block);
            };

            ParserImpl.prototype.parseWithStatement = function () {
                // Debug.assert(this.isWithStatement());
                var withKeyword = this.eatKeyword(43 /* WithKeyword */);
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var condition = this.parseExpression(true);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);
                var statement = this.parseStatement(false);

                return this.factory.withStatement(withKeyword, openParenToken, condition, closeParenToken, statement);
            };

            ParserImpl.prototype.parseWhileStatement = function () {
                // Debug.assert(this.isWhileStatement());
                var whileKeyword = this.eatKeyword(42 /* WhileKeyword */);
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var condition = this.parseExpression(true);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);
                var statement = this.parseStatement(false);

                return this.factory.whileStatement(whileKeyword, openParenToken, condition, closeParenToken, statement);
            };

            ParserImpl.prototype.isEmptyStatement = function (currentToken, inErrorRecovery) {
                // If we're in error recovery, then we don't want to treat ';' as an empty statement.
                // The problem is that ';' can show up in far too many contexts, and if we see one
                // and assume it's a statement, then we may bail out innapropriately from whatever
                // we're parsing.  For example, if we have a semicolon in the middle of a class, then
                // we really don't want to assume the class is over and we're on a statement in the
                // outer module.  We just want to consume and move on.
                if (inErrorRecovery) {
                    return false;
                }

                return currentToken.tokenKind === 78 /* SemicolonToken */;
            };

            ParserImpl.prototype.parseEmptyStatement = function () {
                // Debug.assert(this.isEmptyStatement());
                var semicolonToken = this.eatToken(78 /* SemicolonToken */);
                return this.factory.emptyStatement(semicolonToken);
            };

            ParserImpl.prototype.parseForOrForInStatement = function () {
                // Debug.assert(this.isForOrForInStatement());
                var forKeyword = this.eatKeyword(26 /* ForKeyword */);
                var openParenToken = this.eatToken(72 /* OpenParenToken */);

                var currentToken = this.currentToken();
                if (currentToken.tokenKind === 40 /* VarKeyword */) {
                    // for ( var VariableDeclarationListNoIn; Expressionopt ; Expressionopt ) Statement
                    // for ( var VariableDeclarationNoIn in Expression ) Statement
                    return this.parseForOrForInStatementWithVariableDeclaration(forKeyword, openParenToken);
                } else if (currentToken.tokenKind === 78 /* SemicolonToken */) {
                    // for ( ; Expressionopt ; Expressionopt ) Statement
                    return this.parseForStatementWithNoVariableDeclarationOrInitializer(forKeyword, openParenToken);
                } else {
                    // for ( ExpressionNoInopt; Expressionopt ; Expressionopt ) Statement
                    // for ( LeftHandSideExpression in Expression ) Statement
                    return this.parseForOrForInStatementWithInitializer(forKeyword, openParenToken);
                }
            };

            ParserImpl.prototype.parseForOrForInStatementWithVariableDeclaration = function (forKeyword, openParenToken) {
                // Debug.assert(forKeyword.tokenKind === SyntaxKind.ForKeyword && openParenToken.tokenKind === SyntaxKind.OpenParenToken);
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.VarKeyword);
                // for ( var VariableDeclarationListNoIn; Expressionopt ; Expressionopt ) Statement
                // for ( var VariableDeclarationNoIn in Expression ) Statement
                var variableDeclaration = this.parseVariableDeclaration(false);

                if (this.currentToken().tokenKind === 29 /* InKeyword */) {
                    return this.parseForInStatementWithVariableDeclarationOrInitializer(forKeyword, openParenToken, variableDeclaration, null);
                }

                return this.parseForStatementWithVariableDeclarationOrInitializer(forKeyword, openParenToken, variableDeclaration, null);
            };

            ParserImpl.prototype.parseForInStatementWithVariableDeclarationOrInitializer = function (forKeyword, openParenToken, variableDeclaration, initializer) {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.InKeyword);
                // for ( var VariableDeclarationNoIn in Expression ) Statement
                var inKeyword = this.eatKeyword(29 /* InKeyword */);
                var expression = this.parseExpression(true);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);
                var statement = this.parseStatement(false);

                return this.factory.forInStatement(forKeyword, openParenToken, variableDeclaration, initializer, inKeyword, expression, closeParenToken, statement);
            };

            ParserImpl.prototype.parseForOrForInStatementWithInitializer = function (forKeyword, openParenToken) {
                // Debug.assert(forKeyword.tokenKind === SyntaxKind.ForKeyword && openParenToken.tokenKind === SyntaxKind.OpenParenToken);
                // for ( ExpressionNoInopt; Expressionopt ; Expressionopt ) Statement
                // for ( LeftHandSideExpression in Expression ) Statement
                var initializer = this.parseExpression(false);
                if (this.currentToken().tokenKind === 29 /* InKeyword */) {
                    return this.parseForInStatementWithVariableDeclarationOrInitializer(forKeyword, openParenToken, null, initializer);
                } else {
                    return this.parseForStatementWithVariableDeclarationOrInitializer(forKeyword, openParenToken, null, initializer);
                }
            };

            ParserImpl.prototype.parseForStatementWithNoVariableDeclarationOrInitializer = function (forKeyword, openParenToken) {
                // Debug.assert(forKeyword.tokenKind === SyntaxKind.ForKeyword && openParenToken.tokenKind === SyntaxKind.OpenParenToken);
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.SemicolonToken);
                // for ( ; Expressionopt ; Expressionopt ) Statement
                return this.parseForStatementWithVariableDeclarationOrInitializer(forKeyword, openParenToken, null, null);
            };

            ParserImpl.prototype.parseForStatementWithVariableDeclarationOrInitializer = function (forKeyword, openParenToken, variableDeclaration, initializer) {
                // NOTE: From the es5 section on Automatic Semicolon Insertion.
                // a semicolon is never inserted automatically if the semicolon would then ... become
                // one of the two semicolons in the header of a for statement
                var firstSemicolonToken = this.eatToken(78 /* SemicolonToken */);

                var condition = null;
                if (this.currentToken().tokenKind !== 78 /* SemicolonToken */ && this.currentToken().tokenKind !== 73 /* CloseParenToken */ && this.currentToken().tokenKind !== 10 /* EndOfFileToken */) {
                    condition = this.parseExpression(true);
                }

                // NOTE: See above.  Semicolons in for statements don't participate in automatic
                // semicolon insertion.
                var secondSemicolonToken = this.eatToken(78 /* SemicolonToken */);

                var incrementor = null;
                if (this.currentToken().tokenKind !== 73 /* CloseParenToken */ && this.currentToken().tokenKind !== 10 /* EndOfFileToken */) {
                    incrementor = this.parseExpression(true);
                }

                var closeParenToken = this.eatToken(73 /* CloseParenToken */);
                var statement = this.parseStatement(false);

                return this.factory.forStatement(forKeyword, openParenToken, variableDeclaration, initializer, firstSemicolonToken, condition, secondSemicolonToken, incrementor, closeParenToken, statement);
            };

            ParserImpl.prototype.parseBreakStatement = function () {
                // Debug.assert(this.isBreakStatement());
                var breakKeyword = this.eatKeyword(15 /* BreakKeyword */);

                // If there is no newline after the break keyword, then we can consume an optional
                // identifier.
                var identifier = null;
                if (!this.canEatExplicitOrAutomaticSemicolon(false)) {
                    if (this.isIdentifier(this.currentToken())) {
                        identifier = this.eatIdentifierToken();
                    }
                }

                var semicolon = this.eatExplicitOrAutomaticSemicolon(false);
                return this.factory.breakStatement(breakKeyword, identifier, semicolon);
            };

            ParserImpl.prototype.parseContinueStatement = function () {
                // Debug.assert(this.isContinueStatement());
                var continueKeyword = this.eatKeyword(18 /* ContinueKeyword */);

                // If there is no newline after the break keyword, then we can consume an optional
                // identifier.
                var identifier = null;
                if (!this.canEatExplicitOrAutomaticSemicolon(false)) {
                    if (this.isIdentifier(this.currentToken())) {
                        identifier = this.eatIdentifierToken();
                    }
                }

                var semicolon = this.eatExplicitOrAutomaticSemicolon(false);
                return this.factory.continueStatement(continueKeyword, identifier, semicolon);
            };

            ParserImpl.prototype.parseSwitchStatement = function () {
                // Debug.assert(this.isSwitchStatement());
                var switchKeyword = this.eatKeyword(34 /* SwitchKeyword */);
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var expression = this.parseExpression(true);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);

                var openBraceToken = this.eatToken(70 /* OpenBraceToken */);

                var switchClauses = TypeScript.Syntax.emptyList;
                if (openBraceToken.width() > 0) {
                    var result = this.parseSyntaxList(ListParsingState.SwitchStatement_SwitchClauses);
                    switchClauses = result.list;
                    openBraceToken = this.addSkippedTokensAfterToken(openBraceToken, result.skippedTokens);
                }

                var closeBraceToken = this.eatToken(71 /* CloseBraceToken */);
                return this.factory.switchStatement(switchKeyword, openParenToken, expression, closeParenToken, openBraceToken, switchClauses, closeBraceToken);
            };

            ParserImpl.prototype.isCaseSwitchClause = function () {
                return this.currentToken().tokenKind === 16 /* CaseKeyword */;
            };

            ParserImpl.prototype.isDefaultSwitchClause = function () {
                return this.currentToken().tokenKind === 20 /* DefaultKeyword */;
            };

            ParserImpl.prototype.isSwitchClause = function () {
                if (this.currentNode() !== null && this.currentNode().isSwitchClause()) {
                    return true;
                }

                return this.isCaseSwitchClause() || this.isDefaultSwitchClause();
            };

            ParserImpl.prototype.parseSwitchClause = function () {
                // Debug.assert(this.isSwitchClause());
                if (this.currentNode() !== null && this.currentNode().isSwitchClause()) {
                    return this.eatNode();
                }

                if (this.isCaseSwitchClause()) {
                    return this.parseCaseSwitchClause();
                } else if (this.isDefaultSwitchClause()) {
                    return this.parseDefaultSwitchClause();
                } else {
                    throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.parseCaseSwitchClause = function () {
                // Debug.assert(this.isCaseSwitchClause());
                var caseKeyword = this.eatKeyword(16 /* CaseKeyword */);
                var expression = this.parseExpression(true);
                var colonToken = this.eatToken(106 /* ColonToken */);
                var statements = TypeScript.Syntax.emptyList;

                // TODO: allow parsing of the list evne if there's no colon.  However, we have to make
                // sure we add any skipped tokens to the right previous node or token.
                if (colonToken.fullWidth() > 0) {
                    var result = this.parseSyntaxList(ListParsingState.SwitchClause_Statements);
                    statements = result.list;
                    colonToken = this.addSkippedTokensAfterToken(colonToken, result.skippedTokens);
                }

                return this.factory.caseSwitchClause(caseKeyword, expression, colonToken, statements);
            };

            ParserImpl.prototype.parseDefaultSwitchClause = function () {
                // Debug.assert(this.isDefaultSwitchClause());
                var defaultKeyword = this.eatKeyword(20 /* DefaultKeyword */);
                var colonToken = this.eatToken(106 /* ColonToken */);
                var statements = TypeScript.Syntax.emptyList;

                // TODO: Allow parsing witha colon here.  However, ensure that we attach any skipped
                // tokens to the defaultKeyword.
                if (colonToken.fullWidth() > 0) {
                    var result = this.parseSyntaxList(ListParsingState.SwitchClause_Statements);
                    statements = result.list;
                    colonToken = this.addSkippedTokensAfterToken(colonToken, result.skippedTokens);
                }

                return this.factory.defaultSwitchClause(defaultKeyword, colonToken, statements);
            };

            ParserImpl.prototype.parseThrowStatement = function () {
                // Debug.assert(this.isThrowStatement());
                var throwKeyword = this.eatKeyword(36 /* ThrowKeyword */);

                var expression = null;
                if (this.canEatExplicitOrAutomaticSemicolon(false)) {
                    // Because of automatic semicolon insertion, we need to report error if this
                    // throw could be terminated with a semicolon.  Note: we can't call 'parseExpression'
                    // directly as that might consume an expression on the following line.
                    var token = this.createMissingToken(11 /* IdentifierName */, null);
                    expression = token;
                } else {
                    expression = this.parseExpression(true);
                }

                var semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.throwStatement(throwKeyword, expression, semicolonToken);
            };

            ParserImpl.prototype.parseReturnStatement = function () {
                // Debug.assert(this.isReturnStatement());
                var returnKeyword = this.eatKeyword(33 /* ReturnKeyword */);

                var expression = null;
                if (!this.canEatExplicitOrAutomaticSemicolon(false)) {
                    expression = this.parseExpression(true);
                }

                var semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.returnStatement(returnKeyword, expression, semicolonToken);
            };

            ParserImpl.prototype.isExpressionStatement = function (currentToken) {
                // As per the gramar, neither { nor 'function' can start an expression statement.
                var kind = currentToken.tokenKind;
                if (kind === 70 /* OpenBraceToken */ || kind === 27 /* FunctionKeyword */) {
                    return false;
                }

                return this.isExpression(currentToken);
            };

            ParserImpl.prototype.isAssignmentOrOmittedExpression = function () {
                var currentToken = this.currentToken();
                if (currentToken.tokenKind === 79 /* CommaToken */) {
                    return true;
                }

                return this.isExpression(currentToken);
            };

            ParserImpl.prototype.parseAssignmentOrOmittedExpression = function () {
                // Debug.assert(this.isAssignmentOrOmittedExpression());
                if (this.currentToken().tokenKind === 79 /* CommaToken */) {
                    return this.factory.omittedExpression();
                }

                return this.parseAssignmentExpression(true);
            };

            ParserImpl.prototype.isExpression = function (currentToken) {
                switch (currentToken.tokenKind) {
                    case 13 /* NumericLiteral */:
                    case 14 /* StringLiteral */:
                    case 12 /* RegularExpressionLiteral */:

                    case 74 /* OpenBracketToken */:

                    case 72 /* OpenParenToken */:

                    case 80 /* LessThanToken */:

                    case 93 /* PlusPlusToken */:
                    case 94 /* MinusMinusToken */:
                    case 89 /* PlusToken */:
                    case 90 /* MinusToken */:
                    case 102 /* TildeToken */:
                    case 101 /* ExclamationToken */:

                    case 70 /* OpenBraceToken */:

                    case 85 /* EqualsGreaterThanToken */:

                    case 118 /* SlashToken */:
                    case 119 /* SlashEqualsToken */:

                    case 50 /* SuperKeyword */:
                    case 35 /* ThisKeyword */:
                    case 37 /* TrueKeyword */:
                    case 24 /* FalseKeyword */:
                    case 32 /* NullKeyword */:

                    case 31 /* NewKeyword */:

                    case 21 /* DeleteKeyword */:
                    case 41 /* VoidKeyword */:
                    case 39 /* TypeOfKeyword */:

                    case 27 /* FunctionKeyword */:
                        return true;
                }

                return this.isIdentifier(currentToken);
            };

            ParserImpl.prototype.parseExpressionStatement = function () {
                var expression = this.parseExpression(true);

                var semicolon = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.expressionStatement(expression, semicolon);
            };

            ParserImpl.prototype.parseIfStatement = function () {
                // Debug.assert(this.isIfStatement());
                var ifKeyword = this.eatKeyword(28 /* IfKeyword */);
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var condition = this.parseExpression(true);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);
                var statement = this.parseStatement(false);

                var elseClause = null;
                if (this.isElseClause()) {
                    elseClause = this.parseElseClause();
                }

                return this.factory.ifStatement(ifKeyword, openParenToken, condition, closeParenToken, statement, elseClause);
            };

            ParserImpl.prototype.isElseClause = function () {
                return this.currentToken().tokenKind === 23 /* ElseKeyword */;
            };

            ParserImpl.prototype.parseElseClause = function () {
                // Debug.assert(this.isElseClause());
                var elseKeyword = this.eatKeyword(23 /* ElseKeyword */);
                var statement = this.parseStatement(false);

                return this.factory.elseClause(elseKeyword, statement);
            };

            ParserImpl.prototype.isVariableStatement = function () {
                var index = this.modifierCount();
                return this.peekToken(index).tokenKind === 40 /* VarKeyword */;
            };

            ParserImpl.prototype.parseVariableStatement = function () {
                // Debug.assert(this.isVariableStatement());
                var modifiers = this.parseModifiers();
                var variableDeclaration = this.parseVariableDeclaration(true);
                var semicolonToken = this.eatExplicitOrAutomaticSemicolon(false);

                return this.factory.variableStatement(modifiers, variableDeclaration, semicolonToken);
            };

            ParserImpl.prototype.parseVariableDeclaration = function (allowIn) {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.VarKeyword);
                var varKeyword = this.eatKeyword(40 /* VarKeyword */);

                // Debug.assert(varKeyword.fullWidth() > 0);
                var listParsingState = allowIn ? ListParsingState.VariableDeclaration_VariableDeclarators_AllowIn : ListParsingState.VariableDeclaration_VariableDeclarators_DisallowIn;

                var result = this.parseSeparatedSyntaxList(listParsingState);
                var variableDeclarators = result.list;
                varKeyword = this.addSkippedTokensAfterToken(varKeyword, result.skippedTokens);

                return this.factory.variableDeclaration(varKeyword, variableDeclarators);
            };

            ParserImpl.prototype.isVariableDeclarator = function () {
                if (this.currentNode() !== null && this.currentNode().kind() === 225 /* VariableDeclarator */) {
                    return true;
                }

                return this.isIdentifier(this.currentToken());
            };

            ParserImpl.prototype.canReuseVariableDeclaratorNode = function (node) {
                if (node === null || node.kind() !== 225 /* VariableDeclarator */) {
                    return false;
                }

                // Very subtle incremental parsing bug.  Consider the following code:
                //
                //      var v = new List < A, B
                //
                // This is actually legal code.  It's a list of variable declarators "v = new List<A"
                // on one side and "B" on the other. If you then change that to:
                //
                //      var v = new List < A, B >()
                //
                // then we have a problem.  "v = new List<A" doesn't intersect the change range, so we
                // start reparsing at "B" and we completely fail to handle this properly.
                //
                // In order to prevent this, we do not allow a variable declarator to be reused if it
                // has an initializer.
                var variableDeclarator = node;
                return variableDeclarator.equalsValueClause === null;
            };

            ParserImpl.prototype.parseVariableDeclarator = function (allowIn, allowPropertyName) {
                // TODO(cyrusn): What if the 'allowIn' context has changed between when we last parsed
                // and now?  We could end up with an incorrect tree.  For example, say we had in the old
                // tree "var i = a in b".  Then, in the new tree the declarator portion moved into:
                // "for (var i = a in b".  We would not want to reuse the declarator as the "in b" portion
                // would need to be consumed by the for declaration instead.  Need to see if it is possible
                // to hit this case.
                if (this.canReuseVariableDeclaratorNode(this.currentNode())) {
                    return this.eatNode();
                }

                var propertyName = allowPropertyName ? this.eatPropertyName() : this.eatIdentifierToken();
                var equalsValueClause = null;
                var typeAnnotation = null;

                if (propertyName.width() > 0) {
                    typeAnnotation = this.parseOptionalTypeAnnotation(false);

                    if (this.isEqualsValueClause(false)) {
                        equalsValueClause = this.parseEqualsValueClause(allowIn);
                    }
                }

                return this.factory.variableDeclarator(propertyName, typeAnnotation, equalsValueClause);
            };

            ParserImpl.prototype.isColonValueClause = function () {
                return this.currentToken().tokenKind === 106 /* ColonToken */;
            };

            ParserImpl.prototype.isEqualsValueClause = function (inParameter) {
                var token0 = this.currentToken();
                if (token0.tokenKind === 107 /* EqualsToken */) {
                    return true;
                }

                // It's not uncommon during typing for the user to miss writing the '=' token.  Check if
                // there is no newline after the last token and if we're on an expression.  If so, parse
                // this as an equals-value clause with a missing equals.
                if (!this.previousToken().hasTrailingNewLine()) {
                    // The 'isExpression' call below returns true for "=>".  That's because it smartly
                    // assumes that there is just a missing identifier and the user wanted a lambda.
                    // While this is sensible, we don't want to allow that here as that would mean we're
                    // glossing over multiple erorrs and we're probably making things worse.  So don't
                    // treat this as an equals value clause and let higher up code handle things.
                    if (token0.tokenKind === 85 /* EqualsGreaterThanToken */) {
                        return false;
                    }

                    // There are two places where we allow equals-value clauses.  The first is in a
                    // variable declarator.  The second is with a parameter.  For variable declarators
                    // it's more likely that a { would be a allowed (as an object literal).  While this
                    // is also allowed for parameters, the risk is that we consume the { as an object
                    // literal when it really will be for the block following the parameter.
                    if (token0.tokenKind === 70 /* OpenBraceToken */ && inParameter) {
                        return false;
                    }

                    return this.isExpression(token0);
                }

                return false;
            };

            ParserImpl.prototype.parseEqualsValueClause = function (allowIn) {
                // Debug.assert(this.isEqualsValueClause());
                var equalsToken = this.eatToken(107 /* EqualsToken */);
                var value = this.parseAssignmentExpression(allowIn);

                return this.factory.equalsValueClause(equalsToken, value);
            };

            ParserImpl.prototype.parseExpression = function (allowIn) {
                return this.parseSubExpression(0, allowIn);
            };

            // Called when you need to parse an expression, but you do not want to allow 'CommaExpressions'.
            // i.e. if you have "var a = 1, b = 2" then when we parse '1' we want to parse with higher
            // precedence than 'comma'.  Otherwise we'll get: "var a = (1, (b = 2))", instead of
            // "var a = (1), b = (2)");
            ParserImpl.prototype.parseAssignmentExpression = function (allowIn) {
                return this.parseSubExpression(2 /* AssignmentExpressionPrecedence */, allowIn);
            };

            ParserImpl.prototype.parseUnaryExpressionOrLower = function () {
                var currentTokenKind = this.currentToken().tokenKind;
                if (TypeScript.SyntaxFacts.isPrefixUnaryExpressionOperatorToken(currentTokenKind)) {
                    var operatorKind = TypeScript.SyntaxFacts.getPrefixUnaryExpressionFromOperatorToken(currentTokenKind);

                    var operatorToken = this.eatAnyToken();

                    var operand = this.parseUnaryExpressionOrLower();
                    return this.factory.prefixUnaryExpression(operatorKind, operatorToken, operand);
                } else if (currentTokenKind === 39 /* TypeOfKeyword */) {
                    return this.parseTypeOfExpression();
                } else if (currentTokenKind === 41 /* VoidKeyword */) {
                    return this.parseVoidExpression();
                } else if (currentTokenKind === 21 /* DeleteKeyword */) {
                    return this.parseDeleteExpression();
                } else if (currentTokenKind === 80 /* LessThanToken */) {
                    return this.parseCastExpression();
                } else {
                    return this.parsePostfixExpressionOrLower();
                }
            };

            ParserImpl.prototype.parseSubExpression = function (precedence, allowIn) {
                if (precedence <= 2 /* AssignmentExpressionPrecedence */) {
                    if (this.isSimpleArrowFunctionExpression()) {
                        return this.parseSimpleArrowFunctionExpression();
                    }

                    var parethesizedArrowFunction = this.tryParseParenthesizedArrowFunctionExpression();
                    if (parethesizedArrowFunction !== null) {
                        return parethesizedArrowFunction;
                    }
                }

                // Parse out an expression that has higher precedence than all binary and ternary operators.
                var leftOperand = this.parseUnaryExpressionOrLower();
                return this.parseBinaryOrConditionalExpressions(precedence, allowIn, leftOperand);
            };

            ParserImpl.prototype.parseBinaryOrConditionalExpressions = function (precedence, allowIn, leftOperand) {
                while (true) {
                    // We either have a binary operator here, or we're finished.
                    var token0 = this.currentToken();
                    var token0Kind = token0.tokenKind;

                    // Check for binary expressions.
                    if (TypeScript.SyntaxFacts.isBinaryExpressionOperatorToken(token0Kind)) {
                        // also, if it's the 'in' operator, only allow if our caller allows it.
                        if (token0Kind === 29 /* InKeyword */ && !allowIn) {
                            break;
                        }

                        // check for >= or >> or >>= or >>> or >>>=.
                        //
                        // These are not created by the scanner since we want the individual > tokens for
                        // generics.
                        var mergedToken = this.tryMergeBinaryExpressionTokens();
                        var tokenKind = mergedToken === null ? token0Kind : mergedToken.syntaxKind;

                        var binaryExpressionKind = TypeScript.SyntaxFacts.getBinaryExpressionFromOperatorToken(tokenKind);
                        var newPrecedence = ParserImpl.getPrecedence(binaryExpressionKind);

                        // All binary operators must have precedence > 0!
                        // Debug.assert(newPrecedence > 0);
                        // Check the precedence to see if we should "take" this operator
                        if (newPrecedence < precedence) {
                            break;
                        }

                        // Same precedence, but not right-associative -- deal with this higher up in our stack "later"
                        if (newPrecedence === precedence && !this.isRightAssociative(binaryExpressionKind)) {
                            break;
                        }

                        // Precedence is okay, so we'll "take" this operator.  If we have a merged token,
                        // then create a new synthesized token with all the operators combined.  In that
                        // case make sure it has the right trivia associated with it.
                        var operatorToken = mergedToken === null ? token0 : TypeScript.Syntax.token(mergedToken.syntaxKind).withLeadingTrivia(token0.leadingTrivia()).withTrailingTrivia(this.peekToken(mergedToken.tokenCount - 1).trailingTrivia());

                        // Now skip the operator token we're on, or the tokens we merged.
                        var skipCount = mergedToken === null ? 1 : mergedToken.tokenCount;
                        for (var i = 0; i < skipCount; i++) {
                            this.eatAnyToken();
                        }

                        leftOperand = this.factory.binaryExpression(binaryExpressionKind, leftOperand, operatorToken, this.parseSubExpression(newPrecedence, allowIn));
                        continue;
                    }

                    // Now check for conditional expression.
                    // Only consume this as a ternary expression if our precedence is higher than the ternary
                    // level.  i.e. if we have "!f ? a : b" then we would not want to
                    // consume the "?" as part of "f" because the precedence of "!" is far too high.  However,
                    // if we have: "x = f ? a : b", then we would want to consume the "?" as part of "f".
                    //
                    // Note: if we have "m = f ? x ? y : z : b, then we do want the second "?" to go with 'x'.
                    if (token0Kind === 105 /* QuestionToken */ && precedence <= 3 /* ConditionalExpressionPrecedence */) {
                        var questionToken = this.eatToken(105 /* QuestionToken */);

                        var whenTrueExpression = this.parseAssignmentExpression(allowIn);
                        var colon = this.eatToken(106 /* ColonToken */);

                        var whenFalseExpression = this.parseAssignmentExpression(allowIn);
                        leftOperand = this.factory.conditionalExpression(leftOperand, questionToken, whenTrueExpression, colon, whenFalseExpression);
                        continue;
                    }

                    break;
                }

                return leftOperand;
            };

            ParserImpl.prototype.tryMergeBinaryExpressionTokens = function () {
                var token0 = this.currentToken();

                // Only merge if we have a '>' token with no trailing trivia.
                if (token0.tokenKind === 81 /* GreaterThanToken */ && !token0.hasTrailingTrivia()) {
                    var storage = this.mergeTokensStorage;
                    storage[0] = 0 /* None */;
                    storage[1] = 0 /* None */;
                    storage[2] = 0 /* None */;

                    for (var i = 0; i < storage.length; i++) {
                        var nextToken = this.peekToken(i + 1);

                        // We can merge with the next token if it doesn't have any leading trivia.
                        if (!nextToken.hasLeadingTrivia()) {
                            storage[i] = nextToken.tokenKind;
                        }

                        // Stop merging additional tokens if this token has any trailing trivia.
                        if (nextToken.hasTrailingTrivia()) {
                            break;
                        }
                    }

                    if (storage[0] === 81 /* GreaterThanToken */) {
                        if (storage[1] === 81 /* GreaterThanToken */) {
                            if (storage[2] === 107 /* EqualsToken */) {
                                // >>>=
                                return { tokenCount: 4, syntaxKind: 114 /* GreaterThanGreaterThanGreaterThanEqualsToken */ };
                            } else {
                                // >>>
                                return { tokenCount: 3, syntaxKind: 97 /* GreaterThanGreaterThanGreaterThanToken */ };
                            }
                        } else if (storage[1] === 107 /* EqualsToken */) {
                            // >>=
                            return { tokenCount: 3, syntaxKind: 113 /* GreaterThanGreaterThanEqualsToken */ };
                        } else {
                            // >>
                            return { tokenCount: 2, syntaxKind: 96 /* GreaterThanGreaterThanToken */ };
                        }
                    } else if (storage[0] === 107 /* EqualsToken */) {
                        // >=
                        return { tokenCount: 2, syntaxKind: 83 /* GreaterThanEqualsToken */ };
                    }
                }

                // Just use the normal logic as we're not merging the '>' with anything.
                return null;
            };

            ParserImpl.prototype.isRightAssociative = function (expressionKind) {
                switch (expressionKind) {
                    case 174 /* AssignmentExpression */:
                    case 175 /* AddAssignmentExpression */:
                    case 176 /* SubtractAssignmentExpression */:
                    case 177 /* MultiplyAssignmentExpression */:
                    case 178 /* DivideAssignmentExpression */:
                    case 179 /* ModuloAssignmentExpression */:
                    case 180 /* AndAssignmentExpression */:
                    case 181 /* ExclusiveOrAssignmentExpression */:
                    case 182 /* OrAssignmentExpression */:
                    case 183 /* LeftShiftAssignmentExpression */:
                    case 184 /* SignedRightShiftAssignmentExpression */:
                    case 185 /* UnsignedRightShiftAssignmentExpression */:
                        return true;
                    default:
                        return false;
                }
            };

            ParserImpl.prototype.parseMemberExpressionOrLower = function (inObjectCreation) {
                if (this.currentToken().tokenKind === 31 /* NewKeyword */) {
                    return this.parseObjectCreationExpression();
                }

                var expression = this.parsePrimaryExpression();
                if (expression === null) {
                    // Nothing else worked, just try to consume an identifier so we report an error.
                    return this.eatIdentifierToken();
                }

                return this.parseMemberExpressionRest(expression, false, inObjectCreation);
            };

            ParserImpl.prototype.parseCallExpressionOrLower = function () {
                var expression;
                if (this.currentToken().tokenKind === 50 /* SuperKeyword */) {
                    expression = this.eatKeyword(50 /* SuperKeyword */);

                    // If we have seen "super" it must be followed by '(' or '.'.
                    // If it wasn't then just try to parse out a '.' and report an error.
                    var currentTokenKind = this.currentToken().tokenKind;
                    if (currentTokenKind !== 72 /* OpenParenToken */ && currentTokenKind !== 76 /* DotToken */) {
                        expression = this.factory.memberAccessExpression(expression, this.eatToken(76 /* DotToken */), this.eatIdentifierNameToken());
                    }
                } else {
                    expression = this.parseMemberExpressionOrLower(false);
                }

                return this.parseMemberExpressionRest(expression, true, false);
            };

            ParserImpl.prototype.parseMemberExpressionRest = function (expression, allowArguments, inObjectCreation) {
                while (true) {
                    var currentTokenKind = this.currentToken().tokenKind;

                    switch (currentTokenKind) {
                        case 72 /* OpenParenToken */:
                            if (!allowArguments) {
                                return expression;
                            }

                            expression = this.factory.invocationExpression(expression, this.parseArgumentList(null));
                            continue;

                        case 80 /* LessThanToken */:
                            if (!allowArguments) {
                                return expression;
                            }

                            // See if this is the start of a generic invocation.  If so, consume it and
                            // keep checking for postfix expressions.  Otherwise, it's just a '<' that's
                            // part of an arithmetic expression.  Break out so we consume it higher in the
                            // stack.
                            var argumentList = this.tryParseArgumentList();
                            if (argumentList !== null) {
                                expression = this.factory.invocationExpression(expression, argumentList);
                                continue;
                            }

                            break;

                        case 74 /* OpenBracketToken */:
                            expression = this.parseElementAccessExpression(expression, inObjectCreation);
                            continue;

                        case 76 /* DotToken */:
                            expression = this.factory.memberAccessExpression(expression, this.eatToken(76 /* DotToken */), this.eatIdentifierNameToken());
                            continue;
                    }

                    return expression;
                }
            };

            ParserImpl.prototype.parseLeftHandSideExpressionOrLower = function () {
                if (this.currentToken().tokenKind === 31 /* NewKeyword */) {
                    return this.parseObjectCreationExpression();
                } else {
                    return this.parseCallExpressionOrLower();
                }
            };

            ParserImpl.prototype.parsePostfixExpressionOrLower = function () {
                var expression = this.parseLeftHandSideExpressionOrLower();
                if (expression === null) {
                    // Nothing else worked, just try to consume an identifier so we report an error.
                    return this.eatIdentifierToken();
                }

                var currentTokenKind = this.currentToken().tokenKind;

                switch (currentTokenKind) {
                    case 93 /* PlusPlusToken */:
                    case 94 /* MinusMinusToken */:
                        // Because of automatic semicolon insertion, we should only consume the ++ or --
                        // if it is on the same line as the previous token.
                        if (this.previousToken() !== null && this.previousToken().hasTrailingNewLine()) {
                            break;
                        }

                        return this.factory.postfixUnaryExpression(TypeScript.SyntaxFacts.getPostfixUnaryExpressionFromOperatorToken(currentTokenKind), expression, this.eatAnyToken());
                }

                return expression;
            };

            ParserImpl.prototype.tryParseGenericArgumentList = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.LessThanToken);
                // If we have a '<', then only parse this as a arugment list if the type arguments
                // are complete and we have an open paren.  if we don't, rewind and return nothing.
                var rewindPoint = this.getRewindPoint();

                var typeArgumentList = this.tryParseTypeArgumentList(true);
                var token0 = this.currentToken();

                var isOpenParen = token0.tokenKind === 72 /* OpenParenToken */;
                var isDot = token0.tokenKind === 76 /* DotToken */;
                var isOpenParenOrDot = isOpenParen || isDot;

                var argumentList = null;
                if (typeArgumentList === null || !isOpenParenOrDot) {
                    // Wasn't generic.  Rewind to where we started so this can be parsed as an
                    // arithmetic expression.
                    this.rewind(rewindPoint);
                    this.releaseRewindPoint(rewindPoint);
                    return null;
                } else {
                    this.releaseRewindPoint(rewindPoint);

                    // It's not uncommon for a user to type: "Foo<T>."
                    //
                    // This is not legal in typescript (as an parameter list must follow the type
                    // arguments).  We want to give a good error message for this as otherwise
                    // we'll bail out here and give a poor error message when we try to parse this
                    // as an arithmetic expression.
                    if (isDot) {
                        // A parameter list must follow a generic type argument list.
                        var diagnostic = new TypeScript.Diagnostic(this.fileName, this.lineMap, this.currentTokenStart(), token0.width(), TypeScript.DiagnosticCode.A_parameter_list_must_follow_a_generic_type_argument_list_expected, null);
                        this.addDiagnostic(diagnostic);

                        return this.factory.argumentList(typeArgumentList, TypeScript.Syntax.emptyToken(72 /* OpenParenToken */), TypeScript.Syntax.emptySeparatedList, TypeScript.Syntax.emptyToken(73 /* CloseParenToken */));
                    } else {
                        return this.parseArgumentList(typeArgumentList);
                    }
                }
            };

            ParserImpl.prototype.tryParseArgumentList = function () {
                if (this.currentToken().tokenKind === 80 /* LessThanToken */) {
                    return this.tryParseGenericArgumentList();
                }

                if (this.currentToken().tokenKind === 72 /* OpenParenToken */) {
                    return this.parseArgumentList(null);
                }

                return null;
            };

            ParserImpl.prototype.parseArgumentList = function (typeArgumentList) {
                var openParenToken = this.eatToken(72 /* OpenParenToken */);

                // Don't use the name 'arguments' it prevents V8 from optimizing this method.
                var _arguments = TypeScript.Syntax.emptySeparatedList;

                if (openParenToken.fullWidth() > 0) {
                    var result = this.parseSeparatedSyntaxList(ListParsingState.ArgumentList_AssignmentExpressions);
                    _arguments = result.list;
                    openParenToken = this.addSkippedTokensAfterToken(openParenToken, result.skippedTokens);
                }

                var closeParenToken = this.eatToken(73 /* CloseParenToken */);

                return this.factory.argumentList(typeArgumentList, openParenToken, _arguments, closeParenToken);
            };

            ParserImpl.prototype.parseElementAccessExpression = function (expression, inObjectCreation) {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.OpenBracketToken);
                var start = this.currentTokenStart();
                var openBracketToken = this.eatToken(74 /* OpenBracketToken */);
                var argumentExpression;

                // It's not uncommon for a user to write: "new Type[]".  Check for that common pattern
                // and report a better error message.
                if (this.currentToken().tokenKind === 75 /* CloseBracketToken */ && inObjectCreation) {
                    var end = this.currentTokenStart() + this.currentToken().width();
                    var diagnostic = new TypeScript.Diagnostic(this.fileName, this.lineMap, start, end - start, TypeScript.DiagnosticCode.new_T_cannot_be_used_to_create_an_array_Use_new_Array_T_instead, null);
                    this.addDiagnostic(diagnostic);

                    argumentExpression = TypeScript.Syntax.emptyToken(11 /* IdentifierName */);
                } else {
                    argumentExpression = this.parseExpression(true);
                }

                var closeBracketToken = this.eatToken(75 /* CloseBracketToken */);

                return this.factory.elementAccessExpression(expression, openBracketToken, argumentExpression, closeBracketToken);
            };

            ParserImpl.prototype.parsePrimaryExpression = function () {
                var currentToken = this.currentToken();

                if (this.isIdentifier(currentToken)) {
                    return this.eatIdentifierToken();
                }

                var currentTokenKind = currentToken.tokenKind;
                switch (currentTokenKind) {
                    case 35 /* ThisKeyword */:
                        return this.parseThisExpression();

                    case 37 /* TrueKeyword */:
                    case 24 /* FalseKeyword */:
                        return this.parseLiteralExpression();

                    case 32 /* NullKeyword */:
                        return this.parseLiteralExpression();

                    case 27 /* FunctionKeyword */:
                        return this.parseFunctionExpression();

                    case 13 /* NumericLiteral */:
                        return this.parseLiteralExpression();

                    case 12 /* RegularExpressionLiteral */:
                        return this.parseLiteralExpression();

                    case 14 /* StringLiteral */:
                        return this.parseLiteralExpression();

                    case 74 /* OpenBracketToken */:
                        return this.parseArrayLiteralExpression();

                    case 70 /* OpenBraceToken */:
                        return this.parseObjectLiteralExpression();

                    case 72 /* OpenParenToken */:
                        return this.parseParenthesizedExpression();

                    case 118 /* SlashToken */:
                    case 119 /* SlashEqualsToken */:
                        // If we see a standalone / or /= and we're expecting a term, then try to reparse
                        // it as a regular expression.  If we succeed, then return that.  Otherwise, fall
                        // back and just return a missing identifier as usual.  We'll then form a binary
                        // expression out of of the / as usual.
                        var result = this.tryReparseDivideAsRegularExpression();
                        if (result !== null) {
                            return result;
                        }
                        break;
                }

                // Wasn't able to parse this as a term.
                return null;
            };

            ParserImpl.prototype.tryReparseDivideAsRegularExpression = function () {
                // If we see a / or /= token, then that may actually be the start of a regex in certain
                // contexts.
                var currentToken = this.currentToken();

                // Debug.assert(SyntaxFacts.isAnyDivideToken(currentToken.tokenKind));
                // There are several contexts where we could never see a regex.  Don't even bother
                // reinterpretting the / in these contexts.
                if (this.previousToken() !== null) {
                    var previousTokenKind = this.previousToken().tokenKind;
                    switch (previousTokenKind) {
                        case 11 /* IdentifierName */:
                            // Regular expressions can't follow identifiers.
                            return null;

                        case 35 /* ThisKeyword */:
                        case 37 /* TrueKeyword */:
                        case 24 /* FalseKeyword */:
                            return null;

                        case 14 /* StringLiteral */:
                        case 13 /* NumericLiteral */:
                        case 12 /* RegularExpressionLiteral */:
                        case 93 /* PlusPlusToken */:
                        case 94 /* MinusMinusToken */:
                        case 75 /* CloseBracketToken */:
                            // A regular expression can't follow any of these.  It must be a divide. Note: this
                            // list *may* be incorrect (especially in the context of typescript).  We need to
                            // carefully review it.
                            return null;
                    }
                }

                // Ok, from our quick lexical check, this could be a place where a regular expression could
                // go.  Now we have to do a bunch of work.  Ask the source to retrive the token at the
                // current position again.  But this time allow it to retrieve it as a regular expression.
                currentToken = this.currentTokenAllowingRegularExpression();

                // Note: we *must* have gotten a /, /= or regular expression.  Or else something went *very*
                // wrong with our logic above.
                // Debug.assert(SyntaxFacts.isAnyDivideOrRegularExpressionToken(currentToken.tokenKind));
                if (currentToken.tokenKind === 118 /* SlashToken */ || currentToken.tokenKind === 119 /* SlashEqualsToken */) {
                    // Still came back as a / or /=.   This is not a regular expression literal.
                    return null;
                } else if (currentToken.tokenKind === 12 /* RegularExpressionLiteral */) {
                    return this.parseLiteralExpression();
                } else {
                    throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.parseTypeOfExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.TypeOfKeyword);
                var typeOfKeyword = this.eatKeyword(39 /* TypeOfKeyword */);
                var expression = this.parseUnaryExpressionOrLower();

                return this.factory.typeOfExpression(typeOfKeyword, expression);
            };

            ParserImpl.prototype.parseDeleteExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.DeleteKeyword);
                var deleteKeyword = this.eatKeyword(21 /* DeleteKeyword */);
                var expression = this.parseUnaryExpressionOrLower();

                return this.factory.deleteExpression(deleteKeyword, expression);
            };

            ParserImpl.prototype.parseVoidExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.VoidKeyword);
                var voidKeyword = this.eatKeyword(41 /* VoidKeyword */);
                var expression = this.parseUnaryExpressionOrLower();

                return this.factory.voidExpression(voidKeyword, expression);
            };

            ParserImpl.prototype.parseFunctionExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.FunctionKeyword);
                var functionKeyword = this.eatKeyword(27 /* FunctionKeyword */);
                var identifier = null;

                if (this.isIdentifier(this.currentToken())) {
                    identifier = this.eatIdentifierToken();
                }

                var callSignature = this.parseCallSignature(false);
                var block = this.parseBlock(false, true);

                return this.factory.functionExpression(functionKeyword, identifier, callSignature, block);
            };

            ParserImpl.prototype.parseObjectCreationExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.NewKeyword);
                var newKeyword = this.eatKeyword(31 /* NewKeyword */);

                // While parsing the sub term we don't want to allow invocations to be parsed.  that's because
                // we want "new Foo()" to parse as "new Foo()" (one node), not "new (Foo())".
                var expression = this.parseObjectCreationExpressionOrLower(true);
                var argumentList = this.tryParseArgumentList();

                var result = this.factory.objectCreationExpression(newKeyword, expression, argumentList);
                return this.parseMemberExpressionRest(result, true, false);
            };

            ParserImpl.prototype.parseObjectCreationExpressionOrLower = function (inObjectCreation) {
                if (this.currentToken().tokenKind === 31 /* NewKeyword */) {
                    return this.parseObjectCreationExpression();
                } else {
                    return this.parseMemberExpressionOrLower(inObjectCreation);
                }
            };

            ParserImpl.prototype.parseCastExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.LessThanToken);
                var lessThanToken = this.eatToken(80 /* LessThanToken */);
                var type = this.parseType();
                var greaterThanToken = this.eatToken(81 /* GreaterThanToken */);
                var expression = this.parseUnaryExpressionOrLower();

                return this.factory.castExpression(lessThanToken, type, greaterThanToken, expression);
            };

            ParserImpl.prototype.parseParenthesizedExpression = function () {
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var expression = this.parseExpression(true);
                var closeParenToken = this.eatToken(73 /* CloseParenToken */);

                return this.factory.parenthesizedExpression(openParenToken, expression, closeParenToken);
            };

            ParserImpl.prototype.tryParseParenthesizedArrowFunctionExpression = function () {
                var tokenKind = this.currentToken().tokenKind;
                if (tokenKind !== 72 /* OpenParenToken */ && tokenKind !== 80 /* LessThanToken */) {
                    return null;
                }

                // Because arrow functions and parenthesized expressions look similar, we have to check far
                // enough ahead to be sure we've actually got an arrow function. For example, both nodes can
                // start with:
                //    (a = b, c = d, ..., e = f).
                //So we effectively need infinite lookahead to decide which node we're in.
                //
                // First, check for things that definitely have enough information to let us know it's an
                // arrow function.
                if (this.isDefinitelyArrowFunctionExpression()) {
                    // We have something like "() =>" or "(a) =>".  Definitely a lambda, so parse it
                    // unilaterally as such.
                    return this.parseParenthesizedArrowFunctionExpression(false);
                }

                // Now, look for cases where we're sure it's not an arrow function.  This will help save us
                // a costly parse.
                if (!this.isPossiblyArrowFunctionExpression()) {
                    return null;
                }

                // Then, try to actually parse it as a arrow function, and only return if we see an =>
                var rewindPoint = this.getRewindPoint();

                var arrowFunction = this.parseParenthesizedArrowFunctionExpression(true);
                if (arrowFunction === null) {
                    this.rewind(rewindPoint);
                }

                this.releaseRewindPoint(rewindPoint);
                return arrowFunction;
            };

            ParserImpl.prototype.parseParenthesizedArrowFunctionExpression = function (requireArrow) {
                var currentToken = this.currentToken();

                // Debug.assert(currentToken.tokenKind === SyntaxKind.OpenParenToken || currentToken.tokenKind === SyntaxKind.LessThanToken);
                var callSignature = this.parseCallSignature(true);

                if (requireArrow && this.currentToken().tokenKind !== 85 /* EqualsGreaterThanToken */) {
                    return null;
                }

                var equalsGreaterThanToken = this.eatToken(85 /* EqualsGreaterThanToken */);

                var block = this.tryParseArrowFunctionBlock();
                var expression = null;
                if (block === null) {
                    expression = this.parseAssignmentExpression(true);
                }

                return this.factory.parenthesizedArrowFunctionExpression(callSignature, equalsGreaterThanToken, block, expression);
            };

            ParserImpl.prototype.tryParseArrowFunctionBlock = function () {
                if (this.isBlock()) {
                    return this.parseBlock(false, false);
                } else {
                    // We didn't have a block.  However, we may be in an error situation.  For example,
                    // if the user wrote:
                    //
                    //  a =>
                    //      var v = 0;
                    //  }
                    //
                    // (i.e. they're missing the open brace).  See if that's the case so we can try to
                    // recover better.  If we don't do this, then the next close curly we see may end
                    // up preemptively closing the containing construct.
                    if (this.isStatement(false) && !this.isExpressionStatement(this.currentToken()) && !this.isFunctionDeclaration()) {
                        // We've seen a statement (and it isn't an expressionStatement like 'foo()'),
                        // so treat this like a block with a missing open brace.
                        return this.parseBlock(true, false);
                    } else {
                        return null;
                    }
                }
            };

            ParserImpl.prototype.isSimpleArrowFunctionExpression = function () {
                // ERROR RECOVERY TWEAK:
                // If we see a standalone => try to parse it as an arrow function as that's likely what
                // the user intended to write.
                if (this.currentToken().tokenKind === 85 /* EqualsGreaterThanToken */) {
                    return true;
                }

                return this.isIdentifier(this.currentToken()) && this.peekToken(1).tokenKind === 85 /* EqualsGreaterThanToken */;
            };

            ParserImpl.prototype.parseSimpleArrowFunctionExpression = function () {
                // Debug.assert(this.isSimpleArrowFunctionExpression());
                var identifier = this.eatIdentifierToken();
                var equalsGreaterThanToken = this.eatToken(85 /* EqualsGreaterThanToken */);

                var block = this.tryParseArrowFunctionBlock();
                var expression = null;
                if (block === null) {
                    expression = this.parseAssignmentExpression(true);
                }

                return this.factory.simpleArrowFunctionExpression(identifier, equalsGreaterThanToken, block, expression);
            };

            ParserImpl.prototype.isBlock = function () {
                return this.currentToken().tokenKind === 70 /* OpenBraceToken */;
            };

            ParserImpl.prototype.isDefinitelyArrowFunctionExpression = function () {
                var token0 = this.currentToken();
                if (token0.tokenKind !== 72 /* OpenParenToken */) {
                    // If it didn't start with an (, then it could be generic.  That's too complicated
                    // and we can't say it's 'definitely' an arrow function.
                    return false;
                }

                var token1 = this.peekToken(1);
                var token2;

                if (token1.tokenKind === 73 /* CloseParenToken */) {
                    // ()
                    // Definitely an arrow function.  Could never be a parenthesized expression.
                    // *However*, because of error situations, we could end up with things like "().foo".
                    // In this case, we don't want to think of this as the start of an arrow function.
                    // To prevent this, we are a little stricter, and we require that we at least see:
                    //      "():"  or  "() =>"  or "() {}".  Note: the last one is illegal.  However it
                    // most likely is a missing => and not a parenthesized expression.
                    token2 = this.peekToken(2);
                    return token2.tokenKind === 106 /* ColonToken */ || token2.tokenKind === 85 /* EqualsGreaterThanToken */ || token2.tokenKind === 70 /* OpenBraceToken */;
                }

                if (token1.tokenKind === 77 /* DotDotDotToken */) {
                    // (...
                    // Definitely an arrow function.  Could never be a parenthesized expression.
                    return true;
                }

                token2 = this.peekToken(2);
                if (token1.tokenKind === 57 /* PublicKeyword */ || token1.tokenKind === 55 /* PrivateKeyword */) {
                    if (this.isIdentifier(token2)) {
                        // "(public id" or "(private id".  Definitely an arrow function.  Could never
                        // be a parenthesized expression.  Note: this will be an *illegal* arrow
                        // function (as accessibility modifiers are not allowed in it).  However, that
                        // will be reported by the grammar checker walker.
                        return true;
                    }
                }

                if (!this.isIdentifier(token1)) {
                    // All other arrow functions must start with (id
                    // so this is definitely not an arrow function.
                    return false;
                }

                // (id
                //
                // Lots of options here.  Check for things that make us certain it's an
                // arrow function.
                if (token2.tokenKind === 106 /* ColonToken */) {
                    // (id:
                    // Definitely an arrow function.  Could never be a parenthesized expression.
                    return true;
                }

                var token3 = this.peekToken(3);
                if (token2.tokenKind === 105 /* QuestionToken */) {
                    // (id?
                    // Could be an arrow function, or a parenthesized conditional expression.
                    // Check for the things that could only be arrow functions.
                    if (token3.tokenKind === 106 /* ColonToken */ || token3.tokenKind === 73 /* CloseParenToken */ || token3.tokenKind === 79 /* CommaToken */) {
                        // (id?:
                        // (id?)
                        // (id?,
                        // These are the only cases where this could be an arrow function.
                        // And none of them can be parenthesized expression.
                        return true;
                    }
                }

                if (token2.tokenKind === 73 /* CloseParenToken */) {
                    // (id)
                    // Could be an arrow function, or a parenthesized conditional expression.
                    if (token3.tokenKind === 85 /* EqualsGreaterThanToken */) {
                        // (id) =>
                        // Definitely an arrow function.  Could not be a parenthesized expression.
                        return true;
                    }
                    // Note: "(id):" *looks* like it could be an arrow function.  However, it could
                    // show up in:  "foo ? (id):
                    // So we can't return true here for that case.
                }

                // TODO: Add more cases if you're sure that there is enough information to know to
                // parse this as an arrow function.  Note: be very careful here.
                // Anything else wasn't clear enough.  Try to parse the expression as an arrow function and bail out
                // if we fail.
                return false;
            };

            ParserImpl.prototype.isPossiblyArrowFunctionExpression = function () {
                var token0 = this.currentToken();
                if (token0.tokenKind !== 72 /* OpenParenToken */) {
                    // If it didn't start with an (, then it could be generic.  That's too complicated
                    // and we have to say it's possibly an arrow function.
                    return true;
                }

                var token1 = this.peekToken(1);

                if (!this.isIdentifier(token1)) {
                    // All other arrow functions must start with (id
                    // so this is definitely not an arrow function.
                    return false;
                }

                var token2 = this.peekToken(2);
                if (token2.tokenKind === 107 /* EqualsToken */) {
                    // (id =
                    //
                    // This *could* be an arrow function.  i.e. (id = 0) => { }
                    // Or it could be a parenthesized expression.  So we'll have to actually
                    // try to parse it.
                    return true;
                }

                if (token2.tokenKind === 79 /* CommaToken */) {
                    // (id,
                    // This *could* be an arrow function.  i.e. (id, id2) => { }
                    // Or it could be a parenthesized expression (as javascript supports
                    // the comma operator).  So we'll have to actually try to parse it.
                    return true;
                }

                if (token2.tokenKind === 73 /* CloseParenToken */) {
                    // (id)
                    var token3 = this.peekToken(3);
                    if (token3.tokenKind === 106 /* ColonToken */) {
                        // (id):
                        //
                        // This could be an arrow function. i.e. (id): number => { }
                        // Or it could be parenthesized exprssion: foo ? (id) :
                        // So we'll have to actually try to parse it.
                        return true;
                    }
                }

                // Nothing else could be an arrow function.
                return false;
            };

            ParserImpl.prototype.parseObjectLiteralExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.OpenBraceToken);
                var openBraceToken = this.eatToken(70 /* OpenBraceToken */);

                // Debug.assert(openBraceToken.fullWidth() > 0);
                var result = this.parseSeparatedSyntaxList(ListParsingState.ObjectLiteralExpression_PropertyAssignments);
                var propertyAssignments = result.list;
                openBraceToken = this.addSkippedTokensAfterToken(openBraceToken, result.skippedTokens);

                var closeBraceToken = this.eatToken(71 /* CloseBraceToken */);

                return this.factory.objectLiteralExpression(openBraceToken, propertyAssignments, closeBraceToken);
            };

            ParserImpl.prototype.parsePropertyAssignment = function (inErrorRecovery) {
                // Debug.assert(this.isPropertyAssignment(/*inErrorRecovery:*/ false));
                if (this.isAccessor(inErrorRecovery)) {
                    return this.parseAccessor(true);
                } else if (this.isFunctionPropertyAssignment(inErrorRecovery)) {
                    return this.parseFunctionPropertyAssignment();
                } else if (this.isSimplePropertyAssignment(inErrorRecovery)) {
                    return this.parseSimplePropertyAssignment();
                } else {
                    throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.isPropertyAssignment = function (inErrorRecovery) {
                return this.isAccessor(inErrorRecovery) || this.isFunctionPropertyAssignment(inErrorRecovery) || this.isSimplePropertyAssignment(inErrorRecovery);
            };

            ParserImpl.prototype.eatPropertyName = function () {
                return TypeScript.SyntaxFacts.isIdentifierNameOrAnyKeyword(this.currentToken()) ? this.eatIdentifierNameToken() : this.eatAnyToken();
            };

            ParserImpl.prototype.isFunctionPropertyAssignment = function (inErrorRecovery) {
                return this.isPropertyName(this.currentToken(), inErrorRecovery) && this.isCallSignature(1);
            };

            ParserImpl.prototype.parseFunctionPropertyAssignment = function () {
                // Debug.assert(this.isFunctionPropertyAssignment(/*inErrorRecovery:*/ false));
                var propertyName = this.eatPropertyName();
                var callSignature = this.parseCallSignature(false);
                var block = this.parseBlock(false, true);

                return this.factory.functionPropertyAssignment(propertyName, callSignature, block);
            };

            ParserImpl.prototype.isSimplePropertyAssignment = function (inErrorRecovery) {
                return this.isPropertyName(this.currentToken(), inErrorRecovery);
            };

            ParserImpl.prototype.parseSimplePropertyAssignment = function () {
                // Debug.assert(this.isSimplePropertyAssignment(/*inErrorRecovery:*/ false));
                var propertyName = this.eatPropertyName();
                var colonToken = this.eatToken(106 /* ColonToken */);
                var expression = this.parseAssignmentExpression(true);

                return this.factory.simplePropertyAssignment(propertyName, colonToken, expression);
            };

            ParserImpl.prototype.isPropertyName = function (token, inErrorRecovery) {
                // NOTE: we do *not* want to check "this.isIdentifier" here.  Any IdentifierName is
                // allowed here, even reserved words like keywords.
                if (TypeScript.SyntaxFacts.isIdentifierNameOrAnyKeyword(token)) {
                    // Except: if we're in error recovery, then we don't want to consider keywords.
                    // After all, if we have:
                    //
                    //      { a: 1
                    //      return
                    //
                    // we don't want consider 'return' to be the next property in the object literal.
                    if (inErrorRecovery) {
                        return this.isIdentifier(token);
                    } else {
                        return true;
                    }
                }

                switch (token.tokenKind) {
                    case 14 /* StringLiteral */:
                    case 13 /* NumericLiteral */:
                        return true;

                    default:
                        return false;
                }
            };

            ParserImpl.prototype.parseArrayLiteralExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.OpenBracketToken);
                var openBracketToken = this.eatToken(74 /* OpenBracketToken */);

                // Debug.assert(openBracketToken.fullWidth() > 0);
                var result = this.parseSeparatedSyntaxList(ListParsingState.ArrayLiteralExpression_AssignmentExpressions);
                var expressions = result.list;
                openBracketToken = this.addSkippedTokensAfterToken(openBracketToken, result.skippedTokens);

                var closeBracketToken = this.eatToken(75 /* CloseBracketToken */);

                return this.factory.arrayLiteralExpression(openBracketToken, expressions, closeBracketToken);
            };

            ParserImpl.prototype.parseLiteralExpression = function () {
                // TODO: add appropriate asserts here.
                return this.eatAnyToken();
            };

            ParserImpl.prototype.parseThisExpression = function () {
                // Debug.assert(this.currentToken().tokenKind === SyntaxKind.ThisKeyword);
                return this.eatKeyword(35 /* ThisKeyword */);
            };

            ParserImpl.prototype.parseBlock = function (parseBlockEvenWithNoOpenBrace, checkForStrictMode) {
                var openBraceToken = this.eatToken(70 /* OpenBraceToken */);

                var statements = TypeScript.Syntax.emptyList;

                if (parseBlockEvenWithNoOpenBrace || openBraceToken.width() > 0) {
                    var savedIsInStrictMode = this.isInStrictMode;

                    var processItems = checkForStrictMode ? ParserImpl.updateStrictModeState : null;
                    var result = this.parseSyntaxList(ListParsingState.Block_Statements, processItems);
                    statements = result.list;
                    openBraceToken = this.addSkippedTokensAfterToken(openBraceToken, result.skippedTokens);

                    this.setStrictMode(savedIsInStrictMode);
                }

                var closeBraceToken = this.eatToken(71 /* CloseBraceToken */);

                return this.factory.block(openBraceToken, statements, closeBraceToken);
            };

            ParserImpl.prototype.parseCallSignature = function (requireCompleteTypeParameterList) {
                var typeParameterList = this.parseOptionalTypeParameterList(requireCompleteTypeParameterList);
                var parameterList = this.parseParameterList();
                var typeAnnotation = this.parseOptionalTypeAnnotation(false);

                return this.factory.callSignature(typeParameterList, parameterList, typeAnnotation);
            };

            ParserImpl.prototype.parseOptionalTypeParameterList = function (requireCompleteTypeParameterList) {
                if (this.currentToken().tokenKind !== 80 /* LessThanToken */) {
                    return null;
                }

                var rewindPoint = this.getRewindPoint();

                var lessThanToken = this.eatToken(80 /* LessThanToken */);

                // Debug.assert(lessThanToken.fullWidth() > 0);
                var result = this.parseSeparatedSyntaxList(ListParsingState.TypeParameterList_TypeParameters);
                var typeParameters = result.list;
                lessThanToken = this.addSkippedTokensAfterToken(lessThanToken, result.skippedTokens);

                var greaterThanToken = this.eatToken(81 /* GreaterThanToken */);

                // return null if we were required to have a '>' token and we did not  have one.
                if (requireCompleteTypeParameterList && greaterThanToken.fullWidth() === 0) {
                    this.rewind(rewindPoint);
                    this.releaseRewindPoint(rewindPoint);
                    return null;
                } else {
                    this.releaseRewindPoint(rewindPoint);
                    var typeParameterList = this.factory.typeParameterList(lessThanToken, typeParameters, greaterThanToken);

                    return typeParameterList;
                }
            };

            ParserImpl.prototype.isTypeParameter = function () {
                return this.isIdentifier(this.currentToken());
            };

            ParserImpl.prototype.parseTypeParameter = function () {
                // Debug.assert(this.isTypeParameter());
                var identifier = this.eatIdentifierToken();
                var constraint = this.parseOptionalConstraint();

                return this.factory.typeParameter(identifier, constraint);
            };

            ParserImpl.prototype.parseOptionalConstraint = function () {
                if (this.currentToken().kind() !== 48 /* ExtendsKeyword */) {
                    return null;
                }

                var extendsKeyword = this.eatKeyword(48 /* ExtendsKeyword */);
                var type = this.parseType();

                return this.factory.constraint(extendsKeyword, type);
            };

            ParserImpl.prototype.parseParameterList = function () {
                var openParenToken = this.eatToken(72 /* OpenParenToken */);
                var parameters = TypeScript.Syntax.emptySeparatedList;

                if (openParenToken.width() > 0) {
                    var result = this.parseSeparatedSyntaxList(ListParsingState.ParameterList_Parameters);
                    parameters = result.list;
                    openParenToken = this.addSkippedTokensAfterToken(openParenToken, result.skippedTokens);
                }

                var closeParenToken = this.eatToken(73 /* CloseParenToken */);
                return this.factory.parameterList(openParenToken, parameters, closeParenToken);
            };

            ParserImpl.prototype.isTypeAnnotation = function () {
                return this.currentToken().tokenKind === 106 /* ColonToken */;
            };

            ParserImpl.prototype.parseOptionalTypeAnnotation = function (allowStringLiteral) {
                return this.isTypeAnnotation() ? this.parseTypeAnnotation(allowStringLiteral) : null;
            };

            ParserImpl.prototype.parseTypeAnnotation = function (allowStringLiteral) {
                // Debug.assert(this.isTypeAnnotation());
                var colonToken = this.eatToken(106 /* ColonToken */);
                var type = allowStringLiteral && this.currentToken().tokenKind === 14 /* StringLiteral */ ? this.eatToken(14 /* StringLiteral */) : this.parseType();

                return this.factory.typeAnnotation(colonToken, type);
            };

            ParserImpl.prototype.isType = function () {
                var currentToken = this.currentToken();
                var currentTokenKind = currentToken.tokenKind;

                switch (currentTokenKind) {
                    case 39 /* TypeOfKeyword */:

                    case 60 /* AnyKeyword */:
                    case 67 /* NumberKeyword */:
                    case 61 /* BooleanKeyword */:
                    case 69 /* StringKeyword */:
                    case 41 /* VoidKeyword */:

                    case 70 /* OpenBraceToken */:

                    case 72 /* OpenParenToken */:
                    case 80 /* LessThanToken */:

                    case 31 /* NewKeyword */:
                        return true;
                }

                // Name
                return this.isIdentifier(currentToken);
            };

            ParserImpl.prototype.parseType = function () {
                var currentToken = this.currentToken();
                var currentTokenKind = currentToken.tokenKind;

                var type = this.parseNonArrayType(currentToken);

                while (this.currentToken().tokenKind === 74 /* OpenBracketToken */) {
                    var openBracketToken = this.eatToken(74 /* OpenBracketToken */);
                    var closeBracketToken = this.eatToken(75 /* CloseBracketToken */);

                    type = this.factory.arrayType(type, openBracketToken, closeBracketToken);
                }

                return type;
            };

            ParserImpl.prototype.isTypeQuery = function () {
                return this.currentToken().tokenKind === 39 /* TypeOfKeyword */;
            };

            ParserImpl.prototype.parseTypeQuery = function () {
                // Debug.assert(this.isTypeQuery());
                var typeOfKeyword = this.eatToken(39 /* TypeOfKeyword */);
                var name = this.parseName();

                return this.factory.typeQuery(typeOfKeyword, name);
            };

            ParserImpl.prototype.parseNonArrayType = function (currentToken) {
                var currentTokenKind = currentToken.tokenKind;
                switch (currentTokenKind) {
                    case 60 /* AnyKeyword */:
                    case 67 /* NumberKeyword */:
                    case 61 /* BooleanKeyword */:
                    case 69 /* StringKeyword */:
                    case 41 /* VoidKeyword */:
                        return this.eatAnyToken();

                    case 70 /* OpenBraceToken */:
                        return this.parseObjectType();

                    case 72 /* OpenParenToken */:
                    case 80 /* LessThanToken */:
                        return this.parseFunctionType();

                    case 31 /* NewKeyword */:
                        return this.parseConstructorType();

                    case 39 /* TypeOfKeyword */:
                        return this.parseTypeQuery();
                }

                return this.parseNameOrGenericType();
            };

            ParserImpl.prototype.parseNameOrGenericType = function () {
                var name = this.parseName();
                var typeArgumentList = this.tryParseTypeArgumentList(false);

                return typeArgumentList === null ? name : this.factory.genericType(name, typeArgumentList);
            };

            ParserImpl.prototype.parseFunctionType = function () {
                // Debug.assert(this.isFunctionType());
                var typeParameterList = this.parseOptionalTypeParameterList(false);
                var parameterList = this.parseParameterList();
                var equalsGreaterThanToken = this.eatToken(85 /* EqualsGreaterThanToken */);
                var returnType = this.parseType();

                return this.factory.functionType(typeParameterList, parameterList, equalsGreaterThanToken, returnType);
            };

            ParserImpl.prototype.parseConstructorType = function () {
                // Debug.assert(this.isConstructorType());
                var newKeyword = this.eatKeyword(31 /* NewKeyword */);
                var typeParameterList = this.parseOptionalTypeParameterList(false);
                var parameterList = this.parseParameterList();
                var equalsGreaterThanToken = this.eatToken(85 /* EqualsGreaterThanToken */);
                var type = this.parseType();

                return this.factory.constructorType(newKeyword, typeParameterList, parameterList, equalsGreaterThanToken, type);
            };

            ParserImpl.prototype.isParameter = function () {
                if (this.currentNode() !== null && this.currentNode().kind() === 242 /* Parameter */) {
                    return true;
                }

                var token = this.currentToken();
                var tokenKind = token.tokenKind;
                if (tokenKind === 77 /* DotDotDotToken */) {
                    return true;
                }

                if (ParserImpl.isModifier(token) && !this.isModifierUsedAsParameterIdentifier(token)) {
                    return true;
                }

                return this.isIdentifier(token);
            };

            // Modifiers are perfectly legal names for parameters.  i.e.  you can have: foo(public: number) { }
            // Most of the time we want to treat the modifier as a modifier.  However, if we're certain
            // it's a parameter identifier, then don't consider it as a modifier.
            ParserImpl.prototype.isModifierUsedAsParameterIdentifier = function (token) {
                if (this.isIdentifier(token)) {
                    // Check for:
                    // foo(public)
                    // foo(public: ...
                    // foo(public= ...
                    // foo(public, ...
                    // foo(public? ...
                    //
                    // In all these cases, it's not actually a modifier, but is instead the identifier.
                    // In any other case treat it as the modifier.
                    var nextTokenKind = this.peekToken(1).tokenKind;
                    switch (nextTokenKind) {
                        case 73 /* CloseParenToken */:
                        case 106 /* ColonToken */:
                        case 107 /* EqualsToken */:
                        case 79 /* CommaToken */:
                        case 105 /* QuestionToken */:
                            return true;
                    }
                }

                return false;
            };

            ParserImpl.prototype.parseParameter = function () {
                if (this.currentNode() !== null && this.currentNode().kind() === 242 /* Parameter */) {
                    return this.eatNode();
                }

                var dotDotDotToken = this.tryEatToken(77 /* DotDotDotToken */);

                var modifierArray = this.getArray();

                while (true) {
                    var currentToken = this.currentToken();
                    if (ParserImpl.isModifier(currentToken) && !this.isModifierUsedAsParameterIdentifier(currentToken)) {
                        modifierArray.push(this.eatAnyToken());
                        continue;
                    }

                    break;
                }

                var modifiers = TypeScript.Syntax.list(modifierArray);
                this.returnZeroOrOneLengthArray(modifierArray);

                var identifier = this.eatIdentifierToken();
                var questionToken = this.tryEatToken(105 /* QuestionToken */);
                var typeAnnotation = this.parseOptionalTypeAnnotation(true);

                var equalsValueClause = null;
                if (this.isEqualsValueClause(true)) {
                    equalsValueClause = this.parseEqualsValueClause(true);
                }

                return this.factory.parameter(dotDotDotToken, modifiers, identifier, questionToken, typeAnnotation, equalsValueClause);
            };

            ParserImpl.prototype.parseSyntaxList = function (currentListType, processItems) {
                if (typeof processItems === "undefined") { processItems = null; }
                var savedListParsingState = this.listParsingState;
                this.listParsingState |= currentListType;

                var result = this.parseSyntaxListWorker(currentListType, processItems);

                this.listParsingState = savedListParsingState;

                return result;
            };

            ParserImpl.prototype.parseSeparatedSyntaxList = function (currentListType) {
                var savedListParsingState = this.listParsingState;
                this.listParsingState |= currentListType;

                var result = this.parseSeparatedSyntaxListWorker(currentListType);

                this.listParsingState = savedListParsingState;

                return result;
            };

            // Returns true if we should abort parsing.
            ParserImpl.prototype.abortParsingListOrMoveToNextToken = function (currentListType, items, skippedTokens) {
                // Ok.  We're at a token that is not a terminator for the list and wasn't the start of
                // an item in the list. Definitely report an error for this token.
                this.reportUnexpectedTokenDiagnostic(currentListType);

                for (var state = ListParsingState.LastListParsingState; state >= ListParsingState.FirstListParsingState; state >>= 1) {
                    if ((this.listParsingState & state) !== 0) {
                        if (this.isExpectedListTerminator(state) || this.isExpectedListItem(state, true)) {
                            // Abort parsing this list.
                            return true;
                        }
                    }
                }

                // Otherwise, if none of the lists we're in can capture this token, then we need to
                // unilaterally skip it.  Note: we've already reported an error above.
                var skippedToken = this.currentToken();

                // Consume this token and move onto the next item in the list.
                this.moveToNextToken();

                this.addSkippedTokenToList(items, skippedTokens, skippedToken);

                // Continue parsing this list.  Attach this token to whatever we've seen already.
                return false;
            };

            ParserImpl.prototype.addSkippedTokenToList = function (items, skippedTokens, skippedToken) {
                for (var i = items.length - 1; i >= 0; i--) {
                    var item = items[i];
                    var lastToken = item.lastToken();
                    if (lastToken.fullWidth() > 0) {
                        items[i] = this.addSkippedTokenAfterNodeOrToken(item, skippedToken);
                        return;
                    }
                }

                // Didn't have anything in the list we could add to.  Add to the skipped items array
                // for our caller to handle.
                skippedTokens.push(skippedToken);
            };

            ParserImpl.prototype.tryParseExpectedListItem = function (currentListType, inErrorRecovery, items, processItems) {
                if (this.isExpectedListItem(currentListType, inErrorRecovery)) {
                    var item = this.parseExpectedListItem(currentListType, inErrorRecovery);

                    // Debug.assert(item !== null);
                    items.push(item);

                    if (processItems !== null) {
                        processItems(this, items);
                    }
                }
            };

            ParserImpl.prototype.listIsTerminated = function (currentListType) {
                return this.isExpectedListTerminator(currentListType) || this.currentToken().tokenKind === 10 /* EndOfFileToken */;
            };

            ParserImpl.prototype.getArray = function () {
                if (this.arrayPool.length > 0) {
                    return this.arrayPool.pop();
                }

                return [];
            };

            ParserImpl.prototype.returnZeroOrOneLengthArray = function (array) {
                if (array.length <= 1) {
                    this.returnArray(array);
                }
            };

            ParserImpl.prototype.returnArray = function (array) {
                array.length = 0;
                this.arrayPool.push(array);
            };

            ParserImpl.prototype.parseSyntaxListWorker = function (currentListType, processItems) {
                var items = this.getArray();
                var skippedTokens = this.getArray();

                while (true) {
                    // Try to parse an item of the list.  If we fail then decide if we need to abort or
                    // continue parsing.
                    var oldItemsCount = items.length;
                    this.tryParseExpectedListItem(currentListType, false, items, processItems);

                    var newItemsCount = items.length;
                    if (newItemsCount === oldItemsCount) {
                        // We weren't able to parse out a list element.
                        // That may have been because the list is complete.  In that case, break out
                        // and return the items we were able parse.
                        if (this.listIsTerminated(currentListType)) {
                            break;
                        }

                        // List wasn't complete and we didn't get an item.  Figure out if we should bail out
                        // or skip a token and continue.
                        var abort = this.abortParsingListOrMoveToNextToken(currentListType, items, skippedTokens);
                        if (abort) {
                            break;
                        }
                    }
                    // We either parsed an element.  Or we failed to, but weren't at the end of the list
                    // and didn't want to abort. Continue parsing elements.
                }

                var result = TypeScript.Syntax.list(items);

                // Can't return if it has more then 1 element.  In that case, the list will have been
                // copied into the SyntaxList.
                this.returnZeroOrOneLengthArray(items);

                return { skippedTokens: skippedTokens, list: result };
            };

            ParserImpl.prototype.parseSeparatedSyntaxListWorker = function (currentListType) {
                var items = this.getArray();
                var skippedTokens = this.getArray();
                TypeScript.Debug.assert(items.length === 0);
                TypeScript.Debug.assert(skippedTokens.length === 0);
                TypeScript.Debug.assert(skippedTokens !== items);

                var separatorKind = this.separatorKind(currentListType);
                var allowAutomaticSemicolonInsertion = separatorKind === 78 /* SemicolonToken */;

                var inErrorRecovery = false;
                var listWasTerminated = false;
                while (true) {
                    // Try to parse an item of the list.  If we fail then decide if we need to abort or
                    // continue parsing.
                    var oldItemsCount = items.length;

                    // Debug.assert(oldItemsCount % 2 === 0);
                    this.tryParseExpectedListItem(currentListType, inErrorRecovery, items, null);

                    var newItemsCount = items.length;
                    if (newItemsCount === oldItemsCount) {
                        // We weren't able to parse out a list element.
                        // Debug.assert(items === null || items.length % 2 === 0);
                        // That may have been because the list is complete.  In that case, break out
                        // and return the items we were able parse.
                        if (this.listIsTerminated(currentListType)) {
                            listWasTerminated = true;
                            break;
                        }

                        // List wasn't complete and we didn't get an item.  Figure out if we should bail out
                        // or skip a token and continue.
                        var abort = this.abortParsingListOrMoveToNextToken(currentListType, items, skippedTokens);
                        if (abort) {
                            break;
                        } else {
                            // We just skipped a token.  We're now in error recovery mode.
                            inErrorRecovery = true;
                            continue;
                        }
                    }

                    // Debug.assert(newItemsCount % 2 === 1);
                    // We were able to successfully parse out a list item.  So we're no longer in error
                    // recovery.
                    inErrorRecovery = false;

                    // Now, we have to see if we have a separator or not.  If we do have a separator
                    // we've got to consume it and continue trying to parse list items.  Note: we always
                    // allow 'comma' as a separator (for error tolerance).  We will later do a post pass
                    // to report when a comma was used improperly in a list that needed semicolons.
                    var currentToken = this.currentToken();
                    if (currentToken.tokenKind === separatorKind || currentToken.tokenKind === 79 /* CommaToken */) {
                        // Consume the last separator and continue parsing list elements.
                        items.push(this.eatAnyToken());
                        continue;
                    }

                    // We didn't see the expected separator.  There are two reasons this might happen.
                    // First, we may actually be at the end of the list.  If we are, then we're done
                    // parsing list elements.
                    if (this.listIsTerminated(currentListType)) {
                        listWasTerminated = true;
                        break;
                    }

                    // Otherwise, it might be a case where we can parse out an implicit semicolon.
                    // Note: it's important that we check this *after* the check above for
                    // 'listIsTerminated'.  Consider the following case:
                    //
                    //      {
                    //          a       // <-- just finished parsing 'a'
                    //      }
                    //
                    // Automatic semicolon insertion rules state: "When, as the program is parsed from
                    // left to right, a token (called the offending token) is encountered that is not
                    // allowed by any production of the grammar".  So we should only ever insert a
                    // semicolon if we couldn't consume something normally.  in the above case, we can
                    // consume the '}' just fine.  So ASI doesn't apply.
                    if (allowAutomaticSemicolonInsertion && this.canEatAutomaticSemicolon(false)) {
                        items.push(this.eatExplicitOrAutomaticSemicolon(false));

                        continue;
                    }

                    // We weren't at the end of the list.  And thre was no separator we could parse out.
                    // Try parse the separator we expected, and continue parsing more list elements.
                    // This time mark that we're in error recovery mode though.
                    //
                    // Note: trying to eat this token will emit the appropriate diagnostic.
                    items.push(this.eatToken(separatorKind));

                    // Now that we're in 'error recovery' mode we cantweak some parsing rules as
                    // appropriate.  For example, if we have:
                    //
                    //      var v = { a
                    //      return
                    //
                    // Then we'll be missing the comma.  As such, we want to parse 'return' in a less
                    // tolerant manner.  Normally 'return' could be a property in an object literal.
                    // However, in error recovery mode, we do *not* want it to be.
                    //
                    // Continue trying to parse out list elements.
                    inErrorRecovery = true;
                }

                var result = TypeScript.Syntax.separatedList(items);

                // Can't return if it has more then 1 element.  In that case, the list will have been
                // copied into the SyntaxList.
                this.returnZeroOrOneLengthArray(items);

                return { skippedTokens: skippedTokens, list: result };
            };

            ParserImpl.prototype.separatorKind = function (currentListType) {
                switch (currentListType) {
                    case ListParsingState.HeritageClause_TypeNameList:
                    case ListParsingState.ArgumentList_AssignmentExpressions:
                    case ListParsingState.EnumDeclaration_EnumElements:
                    case ListParsingState.VariableDeclaration_VariableDeclarators_AllowIn:
                    case ListParsingState.VariableDeclaration_VariableDeclarators_DisallowIn:
                    case ListParsingState.ObjectLiteralExpression_PropertyAssignments:
                    case ListParsingState.ParameterList_Parameters:
                    case ListParsingState.ArrayLiteralExpression_AssignmentExpressions:
                    case ListParsingState.TypeArgumentList_Types:
                    case ListParsingState.TypeParameterList_TypeParameters:
                        return 79 /* CommaToken */;

                    case ListParsingState.ObjectType_TypeMembers:
                        return 78 /* SemicolonToken */;

                    case ListParsingState.SourceUnit_ModuleElements:
                    case ListParsingState.ClassOrInterfaceDeclaration_HeritageClauses:
                    case ListParsingState.ClassDeclaration_ClassElements:
                    case ListParsingState.ModuleDeclaration_ModuleElements:
                    case ListParsingState.SwitchStatement_SwitchClauses:
                    case ListParsingState.SwitchClause_Statements:
                    case ListParsingState.Block_Statements:
                    default:
                        throw TypeScript.Errors.notYetImplemented();
                }
            };

            ParserImpl.prototype.reportUnexpectedTokenDiagnostic = function (listType) {
                var token = this.currentToken();

                var diagnostic = new TypeScript.Diagnostic(this.fileName, this.lineMap, this.currentTokenStart(), token.width(), TypeScript.DiagnosticCode.Unexpected_token_0_expected, [this.getExpectedListElementType(listType)]);
                this.addDiagnostic(diagnostic);
            };

            ParserImpl.prototype.addDiagnostic = function (diagnostic) {
                // Except: if we already have a diagnostic for this position, don't report another one.
                if (this.diagnostics.length > 0 && this.diagnostics[this.diagnostics.length - 1].start() === diagnostic.start()) {
                    return;
                }

                this.diagnostics.push(diagnostic);
            };

            ParserImpl.prototype.isExpectedListTerminator = function (currentListType) {
                switch (currentListType) {
                    case ListParsingState.SourceUnit_ModuleElements:
                        return this.isExpectedSourceUnit_ModuleElementsTerminator();

                    case ListParsingState.ClassOrInterfaceDeclaration_HeritageClauses:
                        return this.isExpectedClassOrInterfaceDeclaration_HeritageClausesTerminator();

                    case ListParsingState.ClassDeclaration_ClassElements:
                        return this.isExpectedClassDeclaration_ClassElementsTerminator();

                    case ListParsingState.ModuleDeclaration_ModuleElements:
                        return this.isExpectedModuleDeclaration_ModuleElementsTerminator();

                    case ListParsingState.SwitchStatement_SwitchClauses:
                        return this.isExpectedSwitchStatement_SwitchClausesTerminator();

                    case ListParsingState.SwitchClause_Statements:
                        return this.isExpectedSwitchClause_StatementsTerminator();

                    case ListParsingState.Block_Statements:
                        return this.isExpectedBlock_StatementsTerminator();

                    case ListParsingState.TryBlock_Statements:
                        return this.isExpectedTryBlock_StatementsTerminator();

                    case ListParsingState.CatchBlock_Statements:
                        return this.isExpectedCatchBlock_StatementsTerminator();

                    case ListParsingState.EnumDeclaration_EnumElements:
                        return this.isExpectedEnumDeclaration_EnumElementsTerminator();

                    case ListParsingState.ObjectType_TypeMembers:
                        return this.isExpectedObjectType_TypeMembersTerminator();

                    case ListParsingState.ArgumentList_AssignmentExpressions:
                        return this.isExpectedArgumentList_AssignmentExpressionsTerminator();

                    case ListParsingState.HeritageClause_TypeNameList:
                        return this.isExpectedHeritageClause_TypeNameListTerminator();

                    case ListParsingState.VariableDeclaration_VariableDeclarators_AllowIn:
                        return this.isExpectedVariableDeclaration_VariableDeclarators_AllowInTerminator();

                    case ListParsingState.VariableDeclaration_VariableDeclarators_DisallowIn:
                        return this.isExpectedVariableDeclaration_VariableDeclarators_DisallowInTerminator();

                    case ListParsingState.ObjectLiteralExpression_PropertyAssignments:
                        return this.isExpectedObjectLiteralExpression_PropertyAssignmentsTerminator();

                    case ListParsingState.ParameterList_Parameters:
                        return this.isExpectedParameterList_ParametersTerminator();

                    case ListParsingState.TypeArgumentList_Types:
                        return this.isExpectedTypeArgumentList_TypesTerminator();

                    case ListParsingState.TypeParameterList_TypeParameters:
                        return this.isExpectedTypeParameterList_TypeParametersTerminator();

                    case ListParsingState.ArrayLiteralExpression_AssignmentExpressions:
                        return this.isExpectedLiteralExpression_AssignmentExpressionsTerminator();

                    default:
                        throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.isExpectedSourceUnit_ModuleElementsTerminator = function () {
                return this.currentToken().tokenKind === 10 /* EndOfFileToken */;
            };

            ParserImpl.prototype.isExpectedEnumDeclaration_EnumElementsTerminator = function () {
                return this.currentToken().tokenKind === 71 /* CloseBraceToken */;
            };

            ParserImpl.prototype.isExpectedModuleDeclaration_ModuleElementsTerminator = function () {
                return this.currentToken().tokenKind === 71 /* CloseBraceToken */;
            };

            ParserImpl.prototype.isExpectedObjectType_TypeMembersTerminator = function () {
                return this.currentToken().tokenKind === 71 /* CloseBraceToken */;
            };

            ParserImpl.prototype.isExpectedObjectLiteralExpression_PropertyAssignmentsTerminator = function () {
                return this.currentToken().tokenKind === 71 /* CloseBraceToken */;
            };

            ParserImpl.prototype.isExpectedLiteralExpression_AssignmentExpressionsTerminator = function () {
                return this.currentToken().tokenKind === 75 /* CloseBracketToken */;
            };

            ParserImpl.prototype.isExpectedTypeArgumentList_TypesTerminator = function () {
                var token = this.currentToken();
                if (token.tokenKind === 81 /* GreaterThanToken */) {
                    return true;
                }

                // If we're at a token that can follow the type argument list, then we'll also consider
                // the list terminated.
                if (this.canFollowTypeArgumentListInExpression(token.tokenKind)) {
                    return true;
                }

                // TODO: add more cases as necessary for error tolerance.
                return false;
            };

            ParserImpl.prototype.isExpectedTypeParameterList_TypeParametersTerminator = function () {
                var token = this.currentToken();
                if (token.tokenKind === 81 /* GreaterThanToken */) {
                    return true;
                }

                // These commonly follow type parameter lists.
                if (token.tokenKind === 72 /* OpenParenToken */ || token.tokenKind === 70 /* OpenBraceToken */ || token.tokenKind === 48 /* ExtendsKeyword */ || token.tokenKind === 51 /* ImplementsKeyword */) {
                    return true;
                }

                // TODO: add more cases as necessary for error tolerance.
                return false;
            };

            ParserImpl.prototype.isExpectedParameterList_ParametersTerminator = function () {
                var token = this.currentToken();
                if (token.tokenKind === 73 /* CloseParenToken */) {
                    return true;
                }

                // We may also see a { in an error case.  i.e.:
                // function (a, b, c  {
                if (token.tokenKind === 70 /* OpenBraceToken */) {
                    return true;
                }

                // We may also see a => in an error case.  i.e.:
                // (f: number => { ... }
                if (token.tokenKind === 85 /* EqualsGreaterThanToken */) {
                    return true;
                }

                return false;
            };

            ParserImpl.prototype.isExpectedVariableDeclaration_VariableDeclarators_DisallowInTerminator = function () {
                // This is the case when we're parsing variable declarations in a for/for-in statement.
                if (this.currentToken().tokenKind === 78 /* SemicolonToken */ || this.currentToken().tokenKind === 73 /* CloseParenToken */) {
                    return true;
                }

                if (this.currentToken().tokenKind === 29 /* InKeyword */) {
                    return true;
                }

                return false;
            };

            ParserImpl.prototype.isExpectedVariableDeclaration_VariableDeclarators_AllowInTerminator = function () {
                //// This is the case when we're parsing variable declarations in a variable statement.
                // If we just parsed a comma, then we can't terminate this list.  i.e.:
                //      var a = bar, // <-- just consumed the comma
                //          b = baz;
                if (this.previousToken().tokenKind === 79 /* CommaToken */) {
                    return false;
                }

                // ERROR RECOVERY TWEAK:
                // For better error recovery, if we see a => then we just stop immediately.  We've got an
                // arrow function here and it's going to be veyr unlikely that we'll resynchronize and get
                // another variable declaration.
                if (this.currentToken().tokenKind === 85 /* EqualsGreaterThanToken */) {
                    return true;
                }

                // We're done when we can eat a semicolon and we've parsed at least one item.
                return this.canEatExplicitOrAutomaticSemicolon(false);
            };

            ParserImpl.prototype.isExpectedClassOrInterfaceDeclaration_HeritageClausesTerminator = function () {
                var token0 = this.currentToken();
                if (token0.tokenKind === 70 /* OpenBraceToken */ || token0.tokenKind === 71 /* CloseBraceToken */) {
                    return true;
                }

                return false;
            };

            ParserImpl.prototype.isExpectedHeritageClause_TypeNameListTerminator = function () {
                var token0 = this.currentToken();
                if (token0.tokenKind === 48 /* ExtendsKeyword */ || token0.tokenKind === 51 /* ImplementsKeyword */) {
                    return true;
                }

                if (this.isExpectedClassOrInterfaceDeclaration_HeritageClausesTerminator()) {
                    return true;
                }

                return false;
            };

            ParserImpl.prototype.isExpectedArgumentList_AssignmentExpressionsTerminator = function () {
                var token0 = this.currentToken();
                return token0.tokenKind === 73 /* CloseParenToken */ || token0.tokenKind === 78 /* SemicolonToken */;
            };

            ParserImpl.prototype.isExpectedClassDeclaration_ClassElementsTerminator = function () {
                return this.currentToken().tokenKind === 71 /* CloseBraceToken */;
            };

            ParserImpl.prototype.isExpectedSwitchStatement_SwitchClausesTerminator = function () {
                return this.currentToken().tokenKind === 71 /* CloseBraceToken */;
            };

            ParserImpl.prototype.isExpectedSwitchClause_StatementsTerminator = function () {
                return this.currentToken().tokenKind === 71 /* CloseBraceToken */ || this.isSwitchClause();
            };

            ParserImpl.prototype.isExpectedBlock_StatementsTerminator = function () {
                return this.currentToken().tokenKind === 71 /* CloseBraceToken */;
            };

            ParserImpl.prototype.isExpectedTryBlock_StatementsTerminator = function () {
                return this.currentToken().tokenKind === 17 /* CatchKeyword */ || this.currentToken().tokenKind === 25 /* FinallyKeyword */;
            };

            ParserImpl.prototype.isExpectedCatchBlock_StatementsTerminator = function () {
                return this.currentToken().tokenKind === 25 /* FinallyKeyword */;
            };

            ParserImpl.prototype.isExpectedListItem = function (currentListType, inErrorRecovery) {
                switch (currentListType) {
                    case ListParsingState.SourceUnit_ModuleElements:
                        return this.isModuleElement(inErrorRecovery);

                    case ListParsingState.ClassOrInterfaceDeclaration_HeritageClauses:
                        return this.isHeritageClause();

                    case ListParsingState.ClassDeclaration_ClassElements:
                        return this.isClassElement(inErrorRecovery);

                    case ListParsingState.ModuleDeclaration_ModuleElements:
                        return this.isModuleElement(inErrorRecovery);

                    case ListParsingState.SwitchStatement_SwitchClauses:
                        return this.isSwitchClause();

                    case ListParsingState.SwitchClause_Statements:
                        return this.isStatement(inErrorRecovery);

                    case ListParsingState.Block_Statements:
                        return this.isStatement(inErrorRecovery);

                    case ListParsingState.TryBlock_Statements:
                    case ListParsingState.CatchBlock_Statements:
                        // These two are special.  They're just augmentations of "Block_Statements"
                        // used so we can abort out of the try block if we see a 'catch' or 'finally'
                        // keyword.  There are no additional list items that they add, so we just
                        // return 'false' here.
                        return false;

                    case ListParsingState.EnumDeclaration_EnumElements:
                        return this.isEnumElement(inErrorRecovery);

                    case ListParsingState.VariableDeclaration_VariableDeclarators_AllowIn:
                    case ListParsingState.VariableDeclaration_VariableDeclarators_DisallowIn:
                        return this.isVariableDeclarator();

                    case ListParsingState.ObjectType_TypeMembers:
                        return this.isTypeMember(inErrorRecovery);

                    case ListParsingState.ArgumentList_AssignmentExpressions:
                        return this.isExpectedArgumentList_AssignmentExpression();

                    case ListParsingState.HeritageClause_TypeNameList:
                        return this.isHeritageClauseTypeName();

                    case ListParsingState.ObjectLiteralExpression_PropertyAssignments:
                        return this.isPropertyAssignment(inErrorRecovery);

                    case ListParsingState.ParameterList_Parameters:
                        return this.isParameter();

                    case ListParsingState.TypeArgumentList_Types:
                        return this.isType();

                    case ListParsingState.TypeParameterList_TypeParameters:
                        return this.isTypeParameter();

                    case ListParsingState.ArrayLiteralExpression_AssignmentExpressions:
                        return this.isAssignmentOrOmittedExpression();

                    default:
                        throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.isExpectedArgumentList_AssignmentExpression = function () {
                var currentToken = this.currentToken();
                if (this.isExpression(currentToken)) {
                    return true;
                }

                // If we're on a comma then the user has written something like "Foo(a,," or "Foo(,".
                // Instead of skipping the comma, create an empty expression to go before the comma
                // so that the tree is more well formed and doesn't have skipped tokens.
                if (currentToken.tokenKind === 79 /* CommaToken */) {
                    return true;
                }

                return false;
            };

            ParserImpl.prototype.parseExpectedListItem = function (currentListType, inErrorRecovery) {
                switch (currentListType) {
                    case ListParsingState.SourceUnit_ModuleElements:
                        return this.parseModuleElement(inErrorRecovery);

                    case ListParsingState.ClassOrInterfaceDeclaration_HeritageClauses:
                        return this.parseHeritageClause();

                    case ListParsingState.ClassDeclaration_ClassElements:
                        return this.parseClassElement(inErrorRecovery);

                    case ListParsingState.ModuleDeclaration_ModuleElements:
                        return this.parseModuleElement(inErrorRecovery);

                    case ListParsingState.SwitchStatement_SwitchClauses:
                        return this.parseSwitchClause();

                    case ListParsingState.SwitchClause_Statements:
                        return this.parseStatement(inErrorRecovery);

                    case ListParsingState.Block_Statements:
                        return this.parseStatement(inErrorRecovery);

                    case ListParsingState.EnumDeclaration_EnumElements:
                        return this.parseEnumElement();

                    case ListParsingState.ObjectType_TypeMembers:
                        return this.parseTypeMember(inErrorRecovery);

                    case ListParsingState.ArgumentList_AssignmentExpressions:
                        return this.parseAssignmentExpression(true);

                    case ListParsingState.HeritageClause_TypeNameList:
                        return this.parseNameOrGenericType();

                    case ListParsingState.VariableDeclaration_VariableDeclarators_AllowIn:
                        return this.parseVariableDeclarator(true, false);

                    case ListParsingState.VariableDeclaration_VariableDeclarators_DisallowIn:
                        return this.parseVariableDeclarator(false, false);

                    case ListParsingState.ObjectLiteralExpression_PropertyAssignments:
                        return this.parsePropertyAssignment(inErrorRecovery);

                    case ListParsingState.ArrayLiteralExpression_AssignmentExpressions:
                        return this.parseAssignmentOrOmittedExpression();

                    case ListParsingState.ParameterList_Parameters:
                        return this.parseParameter();

                    case ListParsingState.TypeArgumentList_Types:
                        return this.parseType();

                    case ListParsingState.TypeParameterList_TypeParameters:
                        return this.parseTypeParameter();

                    default:
                        throw TypeScript.Errors.invalidOperation();
                }
            };

            ParserImpl.prototype.getExpectedListElementType = function (currentListType) {
                switch (currentListType) {
                    case ListParsingState.SourceUnit_ModuleElements:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.module_class_interface_enum_import_or_statement, null);

                    case ListParsingState.ClassOrInterfaceDeclaration_HeritageClauses:
                        return '{';

                    case ListParsingState.ClassDeclaration_ClassElements:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.constructor_function_accessor_or_variable, null);

                    case ListParsingState.ModuleDeclaration_ModuleElements:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.module_class_interface_enum_import_or_statement, null);

                    case ListParsingState.SwitchStatement_SwitchClauses:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.case_or_default_clause, null);

                    case ListParsingState.SwitchClause_Statements:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.statement, null);

                    case ListParsingState.Block_Statements:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.statement, null);

                    case ListParsingState.VariableDeclaration_VariableDeclarators_AllowIn:
                    case ListParsingState.VariableDeclaration_VariableDeclarators_DisallowIn:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.identifier, null);

                    case ListParsingState.EnumDeclaration_EnumElements:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.identifier, null);

                    case ListParsingState.ObjectType_TypeMembers:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.call_construct_index_property_or_function_signature, null);

                    case ListParsingState.ArgumentList_AssignmentExpressions:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.expression, null);

                    case ListParsingState.HeritageClause_TypeNameList:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.type_name, null);

                    case ListParsingState.ObjectLiteralExpression_PropertyAssignments:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.property_or_accessor, null);

                    case ListParsingState.ParameterList_Parameters:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.parameter, null);

                    case ListParsingState.TypeArgumentList_Types:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.type, null);

                    case ListParsingState.TypeParameterList_TypeParameters:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.type_parameter, null);

                    case ListParsingState.ArrayLiteralExpression_AssignmentExpressions:
                        return TypeScript.getLocalizedText(TypeScript.DiagnosticCode.expression, null);

                    default:
                        throw TypeScript.Errors.invalidOperation();
                }
            };
            return ParserImpl;
        })();

        function parse(fileName, text, isDeclaration, options) {
            var source = new NormalParserSource(fileName, text, options.languageVersion());

            return new ParserImpl(fileName, text.lineMap(), source, options, text).parseSyntaxTree(isDeclaration);
        }
        Parser.parse = parse;

        function incrementalParse(oldSyntaxTree, textChangeRange, newText) {
            if (textChangeRange.isUnchanged()) {
                return oldSyntaxTree;
            }

            var source = new IncrementalParserSource(oldSyntaxTree, textChangeRange, newText);

            return new ParserImpl(oldSyntaxTree.fileName(), newText.lineMap(), source, oldSyntaxTree.parseOptions(), newText).parseSyntaxTree(oldSyntaxTree.isDeclaration());
        }
        Parser.incrementalParse = incrementalParse;
    })(TypeScript.Parser || (TypeScript.Parser = {}));
    var Parser = TypeScript.Parser;
})(TypeScript || (TypeScript = {}));
