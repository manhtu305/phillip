///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    var CharacterInfo = (function () {
        function CharacterInfo() {
        }
        CharacterInfo.isDecimalDigit = function (c) {
            return c >= 48 /* _0 */ && c <= 57 /* _9 */;
        };
        CharacterInfo.isOctalDigit = function (c) {
            return c >= 48 /* _0 */ && c <= 55 /* _7 */;
        };

        CharacterInfo.isHexDigit = function (c) {
            return CharacterInfo.isDecimalDigit(c) || (c >= 65 /* A */ && c <= 70 /* F */) || (c >= 97 /* a */ && c <= 102 /* f */);
        };

        CharacterInfo.hexValue = function (c) {
            // Debug.assert(isHexDigit(c));
            return CharacterInfo.isDecimalDigit(c) ? (c - 48 /* _0 */) : (c >= 65 /* A */ && c <= 70 /* F */) ? c - 65 /* A */ + 10 : c - 97 /* a */ + 10;
        };

        CharacterInfo.isWhitespace = function (ch) {
            switch (ch) {
                case 32 /* space */:
                case 160 /* nonBreakingSpace */:
                case 8192 /* enQuad */:
                case 8193 /* emQuad */:
                case 8194 /* enSpace */:
                case 8195 /* emSpace */:
                case 8196 /* threePerEmSpace */:
                case 8197 /* fourPerEmSpace */:
                case 8198 /* sixPerEmSpace */:
                case 8199 /* figureSpace */:
                case 8200 /* punctuationSpace */:
                case 8201 /* thinSpace */:
                case 8202 /* hairSpace */:
                case 8203 /* zeroWidthSpace */:
                case 8239 /* narrowNoBreakSpace */:
                case 12288 /* ideographicSpace */:

                case 9 /* tab */:
                case 11 /* verticalTab */:
                case 12 /* formFeed */:
                case 65279 /* byteOrderMark */:
                    return true;
            }

            return false;
        };

        CharacterInfo.isLineTerminator = function (ch) {
            switch (ch) {
                case 13 /* carriageReturn */:
                case 10 /* lineFeed */:
                case 8233 /* paragraphSeparator */:
                case 8232 /* lineSeparator */:
                    return true;
            }

            return false;
        };
        return CharacterInfo;
    })();
    TypeScript.CharacterInfo = CharacterInfo;
})(TypeScript || (TypeScript = {}));
