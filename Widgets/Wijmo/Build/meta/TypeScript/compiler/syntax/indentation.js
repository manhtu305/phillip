///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    (function (Indentation) {
        function columnForEndOfToken(token, syntaxInformationMap, options) {
            return columnForStartOfToken(token, syntaxInformationMap, options) + token.width();
        }
        Indentation.columnForEndOfToken = columnForEndOfToken;

        function columnForStartOfToken(token, syntaxInformationMap, options) {
            // Walk backward from this token until we find the first token in the line.  For each token
            // we see (that is not the first tokem in line), push the entirety of the text into the text
            // array.  Then, for the first token, add its text (without its leading trivia) to the text
            // array.  i.e. if we have:
            //
            //      var foo = a => bar();
            //
            // And we want the column for the start of 'bar', then we'll add the underlinded portions to
            // the text array:
            //
            //      var foo = a => bar();
            //                  _
            //                __
            //              __
            //          ____
            //      ____
            var firstTokenInLine = syntaxInformationMap.firstTokenInLineContainingToken(token);
            var leadingTextInReverse = [];

            var current = token;
            while (current !== firstTokenInLine) {
                current = syntaxInformationMap.previousToken(current);

                if (current === firstTokenInLine) {
                    // We're at the first token in teh line.
                    // We don't want the leading trivia for this token.  That will be taken care of in
                    // columnForFirstNonWhitespaceCharacterInLine.  So just push the trailing trivia
                    // and then the token text.
                    leadingTextInReverse.push(current.trailingTrivia().fullText());
                    leadingTextInReverse.push(current.text());
                } else {
                    // We're at an intermediate token on the line.  Just push all its text into the array.
                    leadingTextInReverse.push(current.fullText());
                }
            }

            // Now, add all trivia to the start of the line on the first token in the list.
            collectLeadingTriviaTextToStartOfLine(firstTokenInLine, leadingTextInReverse);

            return columnForLeadingTextInReverse(leadingTextInReverse, options);
        }
        Indentation.columnForStartOfToken = columnForStartOfToken;

        function columnForStartOfFirstTokenInLineContainingToken(token, syntaxInformationMap, options) {
            // Walk backward through the tokens until we find the first one on the line.
            var firstTokenInLine = syntaxInformationMap.firstTokenInLineContainingToken(token);
            var leadingTextInReverse = [];

            // Now, add all trivia to the start of the line on the first token in the list.
            collectLeadingTriviaTextToStartOfLine(firstTokenInLine, leadingTextInReverse);

            return columnForLeadingTextInReverse(leadingTextInReverse, options);
        }
        Indentation.columnForStartOfFirstTokenInLineContainingToken = columnForStartOfFirstTokenInLineContainingToken;

        // Collect all the trivia that precedes this token.  Stopping when we hit a newline trivia
        // or a multiline comment that spans multiple lines.  This is meant to be called on the first
        // token in a line.
        function collectLeadingTriviaTextToStartOfLine(firstTokenInLine, leadingTextInReverse) {
            var leadingTrivia = firstTokenInLine.leadingTrivia();

            for (var i = leadingTrivia.count() - 1; i >= 0; i--) {
                var trivia = leadingTrivia.syntaxTriviaAt(i);
                if (trivia.kind() === 5 /* NewLineTrivia */) {
                    break;
                }

                if (trivia.kind() === 6 /* MultiLineCommentTrivia */) {
                    var lineSegments = TypeScript.Syntax.splitMultiLineCommentTriviaIntoMultipleLines(trivia);
                    leadingTextInReverse.push(TypeScript.ArrayUtilities.last(lineSegments));

                    if (lineSegments.length > 0) {
                        break;
                    }
                    // It was only on a single line, so keep on going.
                }

                leadingTextInReverse.push(trivia.fullText());
            }
        }

        function columnForLeadingTextInReverse(leadingTextInReverse, options) {
            var column = 0;

            for (var i = leadingTextInReverse.length - 1; i >= 0; i--) {
                var text = leadingTextInReverse[i];
                column = columnForPositionInStringWorker(text, text.length, column, options);
            }

            return column;
        }

        // Returns the column that this input string ends at (assuming it starts at column 0).
        function columnForPositionInString(input, position, options) {
            return columnForPositionInStringWorker(input, position, 0, options);
        }
        Indentation.columnForPositionInString = columnForPositionInString;

        function columnForPositionInStringWorker(input, position, startColumn, options) {
            var column = startColumn;
            var spacesPerTab = options.spacesPerTab;

            for (var j = 0; j < position; j++) {
                var ch = input.charCodeAt(j);

                if (ch === 9 /* tab */) {
                    column += spacesPerTab - column % spacesPerTab;
                } else {
                    column++;
                }
            }

            return column;
        }

        function indentationString(column, options) {
            var numberOfTabs = 0;
            var numberOfSpaces = TypeScript.MathPrototype.max(0, column);

            if (options.useTabs) {
                numberOfTabs = Math.floor(column / options.spacesPerTab);
                numberOfSpaces -= numberOfTabs * options.spacesPerTab;
            }

            return TypeScript.StringUtilities.repeat('\t', numberOfTabs) + TypeScript.StringUtilities.repeat(' ', numberOfSpaces);
        }
        Indentation.indentationString = indentationString;

        function indentationTrivia(column, options) {
            return TypeScript.Syntax.whitespace(this.indentationString(column, options));
        }
        Indentation.indentationTrivia = indentationTrivia;

        function firstNonWhitespacePosition(value) {
            for (var i = 0; i < value.length; i++) {
                var ch = value.charCodeAt(i);
                if (!TypeScript.CharacterInfo.isWhitespace(ch)) {
                    return i;
                }
            }

            return value.length;
        }
        Indentation.firstNonWhitespacePosition = firstNonWhitespacePosition;
    })(TypeScript.Indentation || (TypeScript.Indentation = {}));
    var Indentation = TypeScript.Indentation;
})(TypeScript || (TypeScript = {}));
