///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    var isKeywordStartCharacter = TypeScript.ArrayUtilities.createArray(127 /* maxAsciiCharacter */, false);
    var isIdentifierStartCharacter = TypeScript.ArrayUtilities.createArray(127 /* maxAsciiCharacter */, false);
    var isIdentifierPartCharacter = TypeScript.ArrayUtilities.createArray(127 /* maxAsciiCharacter */, false);
    var isNumericLiteralStart = TypeScript.ArrayUtilities.createArray(127 /* maxAsciiCharacter */, false);

    for (var character = 0; character < 127 /* maxAsciiCharacter */; character++) {
        if (character >= 97 /* a */ && character <= 122 /* z */) {
            isIdentifierStartCharacter[character] = true;
            isIdentifierPartCharacter[character] = true;
        } else if ((character >= 65 /* A */ && character <= 90 /* Z */) || character === 95 /* _ */ || character === 36 /* $ */) {
            isIdentifierStartCharacter[character] = true;
            isIdentifierPartCharacter[character] = true;
        } else if (character >= 48 /* _0 */ && character <= 57 /* _9 */) {
            isIdentifierPartCharacter[character] = true;
            isNumericLiteralStart[character] = true;
        }
    }

    isNumericLiteralStart[46 /* dot */] = true;

    for (var keywordKind = TypeScript.SyntaxKind.FirstKeyword; keywordKind <= TypeScript.SyntaxKind.LastKeyword; keywordKind++) {
        var keyword = TypeScript.SyntaxFacts.getText(keywordKind);
        isKeywordStartCharacter[keyword.charCodeAt(0)] = true;
    }

    var Scanner = (function () {
        function Scanner(fileName, text, languageVersion, window) {
            if (typeof window === "undefined") { window = TypeScript.ArrayUtilities.createArray(2048, 0); }
            this.slidingWindow = new TypeScript.SlidingWindow(this, window, 0, text.length());
            this.fileName = fileName;
            this.text = text;
            this._languageVersion = languageVersion;
        }
        Scanner.prototype.languageVersion = function () {
            return this._languageVersion;
        };

        Scanner.prototype.fetchMoreItems = function (argument, sourceIndex, window, destinationIndex, spaceAvailable) {
            var charactersRemaining = this.text.length() - sourceIndex;
            var amountToRead = TypeScript.MathPrototype.min(charactersRemaining, spaceAvailable);
            this.text.copyTo(sourceIndex, window, destinationIndex, amountToRead);
            return amountToRead;
        };

        Scanner.prototype.currentCharCode = function () {
            return this.slidingWindow.currentItem(null);
        };

        Scanner.prototype.absoluteIndex = function () {
            return this.slidingWindow.absoluteIndex();
        };

        // Set's the scanner to a specific position in the text.
        Scanner.prototype.setAbsoluteIndex = function (index) {
            this.slidingWindow.setAbsoluteIndex(index);
        };

        // Scans a token starting at the current position.  Any errors encountered will be added to
        // 'diagnostics'.
        Scanner.prototype.scan = function (diagnostics, allowRegularExpression) {
            var diagnosticsLength = diagnostics.length;
            var fullStart = this.slidingWindow.absoluteIndex();
            var leadingTriviaInfo = this.scanTriviaInfo(diagnostics, false);

            var start = this.slidingWindow.absoluteIndex();
            var kindAndFlags = this.scanSyntaxToken(diagnostics, allowRegularExpression);
            var end = this.slidingWindow.absoluteIndex();

            var trailingTriviaInfo = this.scanTriviaInfo(diagnostics, true);
            var fullEnd = this.slidingWindow.absoluteIndex();

            var isVariableWidthKeyword = (kindAndFlags & TypeScript.SyntaxConstants.IsVariableWidthKeyword) !== 0;
            var kind = kindAndFlags & ~TypeScript.SyntaxConstants.IsVariableWidthKeyword;

            var token = this.createToken(fullStart, leadingTriviaInfo, start, kind, end, fullEnd, trailingTriviaInfo, isVariableWidthKeyword);

            // If we produced any diagnostics while creating this token, then realize the token so
            // it won't be reused in incremental scenarios.
            return diagnosticsLength !== diagnostics.length ? TypeScript.Syntax.realizeToken(token) : token;
        };

        Scanner.prototype.createToken = function (fullStart, leadingTriviaInfo, start, kind, end, fullEnd, trailingTriviaInfo, isVariableWidthKeyword) {
            if (!isVariableWidthKeyword && kind >= TypeScript.SyntaxKind.FirstFixedWidth) {
                if (leadingTriviaInfo === 0) {
                    if (trailingTriviaInfo === 0) {
                        return new TypeScript.Syntax.FixedWidthTokenWithNoTrivia(kind);
                    } else {
                        var fullText = this.text.substr(fullStart, fullEnd - fullStart, false);
                        return new TypeScript.Syntax.FixedWidthTokenWithTrailingTrivia(fullText, kind, trailingTriviaInfo);
                    }
                } else if (trailingTriviaInfo === 0) {
                    var fullText = this.text.substr(fullStart, fullEnd - fullStart, false);
                    return new TypeScript.Syntax.FixedWidthTokenWithLeadingTrivia(fullText, kind, leadingTriviaInfo);
                } else {
                    var fullText = this.text.substr(fullStart, fullEnd - fullStart, false);
                    return new TypeScript.Syntax.FixedWidthTokenWithLeadingAndTrailingTrivia(fullText, kind, leadingTriviaInfo, trailingTriviaInfo);
                }
            } else {
                var width = end - start;

                var fullText = this.text.substr(fullStart, fullEnd - fullStart, false);

                if (leadingTriviaInfo === 0) {
                    if (trailingTriviaInfo === 0) {
                        return new TypeScript.Syntax.VariableWidthTokenWithNoTrivia(fullText, kind);
                    } else {
                        return new TypeScript.Syntax.VariableWidthTokenWithTrailingTrivia(fullText, kind, trailingTriviaInfo);
                    }
                } else if (trailingTriviaInfo === 0) {
                    return new TypeScript.Syntax.VariableWidthTokenWithLeadingTrivia(fullText, kind, leadingTriviaInfo);
                } else {
                    return new TypeScript.Syntax.VariableWidthTokenWithLeadingAndTrailingTrivia(fullText, kind, leadingTriviaInfo, trailingTriviaInfo);
                }
            }
        };

        // Scans a subsection of 'text' as trivia.
        Scanner.scanTrivia = function (text, start, length, isTrailing) {
            // Debug.assert(length > 0);
            // Note: the scanner operates upon a subrange of the text passed in. However, we also
            // pass hte originl text along to 'scanTrivia' so the trivia can point back at it
            // directly (and not at the subtext wrapper).  This allows the subtext to get GC'ed
            // and means trivia can be represented with only a single allocation.
            var scanner = new Scanner(null, text.subText(new TypeScript.TextSpan(start, length)), 1 /* EcmaScript5 */, Scanner.triviaWindow);
            return scanner.scanTrivia(text, start, isTrailing);
        };

        Scanner.prototype.scanTrivia = function (underlyingText, underlyingTextStart, isTrailing) {
            // Keep this exactly in sync with scanTriviaInfo
            var trivia = new Array();

            while (true) {
                if (!this.slidingWindow.isAtEndOfSource()) {
                    var ch = this.currentCharCode();

                    switch (ch) {
                        case 32 /* space */:
                        case 160 /* nonBreakingSpace */:
                        case 8192 /* enQuad */:
                        case 8193 /* emQuad */:
                        case 8194 /* enSpace */:
                        case 8195 /* emSpace */:
                        case 8196 /* threePerEmSpace */:
                        case 8197 /* fourPerEmSpace */:
                        case 8198 /* sixPerEmSpace */:
                        case 8199 /* figureSpace */:
                        case 8200 /* punctuationSpace */:
                        case 8201 /* thinSpace */:
                        case 8202 /* hairSpace */:
                        case 8203 /* zeroWidthSpace */:
                        case 8239 /* narrowNoBreakSpace */:
                        case 12288 /* ideographicSpace */:

                        case 9 /* tab */:
                        case 11 /* verticalTab */:
                        case 12 /* formFeed */:
                        case 65279 /* byteOrderMark */:
                            // Normal whitespace.  Consume and continue.
                            trivia.push(this.scanWhitespaceTrivia(underlyingText, underlyingTextStart));
                            continue;

                        case 47 /* slash */:
                            // Potential comment.  Consume if so.  Otherwise, break out and return.
                            var ch2 = this.slidingWindow.peekItemN(1);
                            if (ch2 === 47 /* slash */) {
                                trivia.push(this.scanSingleLineCommentTrivia(underlyingText, underlyingTextStart));
                                continue;
                            }

                            if (ch2 === 42 /* asterisk */) {
                                trivia.push(this.scanMultiLineCommentTrivia(underlyingText, underlyingTextStart));
                                continue;
                            }

                            throw TypeScript.Errors.invalidOperation();

                        case 13 /* carriageReturn */:
                        case 10 /* lineFeed */:
                        case 8233 /* paragraphSeparator */:
                        case 8232 /* lineSeparator */:
                            trivia.push(this.scanLineTerminatorSequenceTrivia(ch));

                            // If we're consuming leading trivia, then we will continue consuming more
                            // trivia (including newlines) up to the first token we see.  If we're
                            // consuming trailing trivia, then we break after the first newline we see.
                            if (!isTrailing) {
                                continue;
                            }

                            break;

                        default:
                            throw TypeScript.Errors.invalidOperation();
                    }
                }

                // Debug.assert(trivia.length > 0);
                return TypeScript.Syntax.triviaList(trivia);
            }
        };

        Scanner.prototype.scanTriviaInfo = function (diagnostics, isTrailing) {
            // Keep this exactly in sync with scanTrivia
            var width = 0;
            var hasCommentOrNewLine = 0;

            while (true) {
                var ch = this.currentCharCode();

                switch (ch) {
                    case 32 /* space */:
                    case 160 /* nonBreakingSpace */:
                    case 8192 /* enQuad */:
                    case 8193 /* emQuad */:
                    case 8194 /* enSpace */:
                    case 8195 /* emSpace */:
                    case 8196 /* threePerEmSpace */:
                    case 8197 /* fourPerEmSpace */:
                    case 8198 /* sixPerEmSpace */:
                    case 8199 /* figureSpace */:
                    case 8200 /* punctuationSpace */:
                    case 8201 /* thinSpace */:
                    case 8202 /* hairSpace */:
                    case 8203 /* zeroWidthSpace */:
                    case 8239 /* narrowNoBreakSpace */:
                    case 12288 /* ideographicSpace */:

                    case 9 /* tab */:
                    case 11 /* verticalTab */:
                    case 12 /* formFeed */:
                    case 65279 /* byteOrderMark */:
                        // Normal whitespace.  Consume and continue.
                        this.slidingWindow.moveToNextItem();
                        width++;
                        continue;

                    case 47 /* slash */:
                        // Potential comment.  Consume if so.  Otherwise, break out and return.
                        var ch2 = this.slidingWindow.peekItemN(1);
                        if (ch2 === 47 /* slash */) {
                            hasCommentOrNewLine |= 2 /* TriviaCommentMask */;
                            width += this.scanSingleLineCommentTriviaLength();
                            continue;
                        }

                        if (ch2 === 42 /* asterisk */) {
                            hasCommentOrNewLine |= 2 /* TriviaCommentMask */;
                            width += this.scanMultiLineCommentTriviaLength(diagnostics);
                            continue;
                        }

                        break;

                    case 13 /* carriageReturn */:
                    case 10 /* lineFeed */:
                    case 8233 /* paragraphSeparator */:
                    case 8232 /* lineSeparator */:
                        hasCommentOrNewLine |= 1 /* TriviaNewLineMask */;
                        width += this.scanLineTerminatorSequenceLength(ch);

                        // If we're consuming leading trivia, then we will continue consuming more
                        // trivia (including newlines) up to the first token we see.  If we're
                        // consuming trailing trivia, then we break after the first newline we see.
                        if (!isTrailing) {
                            continue;
                        }

                        break;
                }

                return (width << 2 /* TriviaFullWidthShift */) | hasCommentOrNewLine;
            }
        };

        Scanner.prototype.isNewLineCharacter = function (ch) {
            switch (ch) {
                case 13 /* carriageReturn */:
                case 10 /* lineFeed */:
                case 8233 /* paragraphSeparator */:
                case 8232 /* lineSeparator */:
                    return true;
                default:
                    return false;
            }
        };

        Scanner.prototype.scanWhitespaceTrivia = function (underlyingText, underlyingTextStart) {
            // We're going to be extracting text out of sliding window.  Make sure it can't move past
            // this point.
            var absoluteStartIndex = this.absoluteIndex();

            var width = 0;
            while (true) {
                var ch = this.currentCharCode();

                switch (ch) {
                    case 32 /* space */:
                    case 160 /* nonBreakingSpace */:
                    case 8192 /* enQuad */:
                    case 8193 /* emQuad */:
                    case 8194 /* enSpace */:
                    case 8195 /* emSpace */:
                    case 8196 /* threePerEmSpace */:
                    case 8197 /* fourPerEmSpace */:
                    case 8198 /* sixPerEmSpace */:
                    case 8199 /* figureSpace */:
                    case 8200 /* punctuationSpace */:
                    case 8201 /* thinSpace */:
                    case 8202 /* hairSpace */:
                    case 8203 /* zeroWidthSpace */:
                    case 8239 /* narrowNoBreakSpace */:
                    case 12288 /* ideographicSpace */:

                    case 9 /* tab */:
                    case 11 /* verticalTab */:
                    case 12 /* formFeed */:
                    case 65279 /* byteOrderMark */:
                        // Normal whitespace.  Consume and continue.
                        this.slidingWindow.moveToNextItem();
                        width++;
                        continue;
                }

                break;
            }

            return TypeScript.Syntax.deferredTrivia(4 /* WhitespaceTrivia */, underlyingText, underlyingTextStart + absoluteStartIndex, width);
        };

        Scanner.prototype.scanSingleLineCommentTrivia = function (underlyingText, underlyingTextStart) {
            var absoluteStartIndex = this.slidingWindow.absoluteIndex();
            var width = this.scanSingleLineCommentTriviaLength();

            return TypeScript.Syntax.deferredTrivia(7 /* SingleLineCommentTrivia */, underlyingText, underlyingTextStart + absoluteStartIndex, width);
        };

        Scanner.prototype.scanSingleLineCommentTriviaLength = function () {
            this.slidingWindow.moveToNextItem();
            this.slidingWindow.moveToNextItem();

            // The '2' is for the "//" we consumed.
            var width = 2;
            while (true) {
                if (this.slidingWindow.isAtEndOfSource() || this.isNewLineCharacter(this.currentCharCode())) {
                    return width;
                }

                this.slidingWindow.moveToNextItem();
                width++;
            }
        };

        Scanner.prototype.scanMultiLineCommentTrivia = function (underlyingText, underlyingTextStart) {
            var absoluteStartIndex = this.absoluteIndex();
            var width = this.scanMultiLineCommentTriviaLength(null);

            return TypeScript.Syntax.deferredTrivia(6 /* MultiLineCommentTrivia */, underlyingText, underlyingTextStart + absoluteStartIndex, width);
        };

        Scanner.prototype.scanMultiLineCommentTriviaLength = function (diagnostics) {
            this.slidingWindow.moveToNextItem();
            this.slidingWindow.moveToNextItem();

            // The '2' is for the "/*" we consumed.
            var width = 2;
            while (true) {
                if (this.slidingWindow.isAtEndOfSource()) {
                    if (diagnostics !== null) {
                        diagnostics.push(new TypeScript.Diagnostic(this.fileName, this.text.lineMap(), this.slidingWindow.absoluteIndex(), 0, TypeScript.DiagnosticCode.AsteriskSlash_expected, null));
                    }

                    return width;
                }

                var ch = this.currentCharCode();
                if (ch === 42 /* asterisk */ && this.slidingWindow.peekItemN(1) === 47 /* slash */) {
                    this.slidingWindow.moveToNextItem();
                    this.slidingWindow.moveToNextItem();
                    width += 2;
                    return width;
                }

                this.slidingWindow.moveToNextItem();
                width++;
            }
        };

        Scanner.prototype.scanLineTerminatorSequenceTrivia = function (ch) {
            var absoluteStartIndex = this.slidingWindow.getAndPinAbsoluteIndex();
            var width = this.scanLineTerminatorSequenceLength(ch);

            var text = this.substring(absoluteStartIndex, absoluteStartIndex + width, false);
            this.slidingWindow.releaseAndUnpinAbsoluteIndex(absoluteStartIndex);

            return TypeScript.Syntax.trivia(5 /* NewLineTrivia */, text);
        };

        Scanner.prototype.scanLineTerminatorSequenceLength = function (ch) {
            // Consume the first of the line terminator we saw.
            this.slidingWindow.moveToNextItem();

            // If it happened to be a \r and there's a following \n, then consume both.
            if (ch === 13 /* carriageReturn */ && this.currentCharCode() === 10 /* lineFeed */) {
                this.slidingWindow.moveToNextItem();
                return 2;
            } else {
                return 1;
            }
        };

        Scanner.prototype.scanSyntaxToken = function (diagnostics, allowRegularExpression) {
            if (this.slidingWindow.isAtEndOfSource()) {
                return 10 /* EndOfFileToken */;
            }

            var character = this.currentCharCode();

            switch (character) {
                case 34 /* doubleQuote */:
                case 39 /* singleQuote */:
                    return this.scanStringLiteral(diagnostics);

                case 47 /* slash */:
                    return this.scanSlashToken(allowRegularExpression);

                case 46 /* dot */:
                    return this.scanDotToken(diagnostics);

                case 45 /* minus */:
                    return this.scanMinusToken();

                case 33 /* exclamation */:
                    return this.scanExclamationToken();

                case 61 /* equals */:
                    return this.scanEqualsToken();

                case 124 /* bar */:
                    return this.scanBarToken();

                case 42 /* asterisk */:
                    return this.scanAsteriskToken();

                case 43 /* plus */:
                    return this.scanPlusToken();

                case 37 /* percent */:
                    return this.scanPercentToken();

                case 38 /* ampersand */:
                    return this.scanAmpersandToken();

                case 94 /* caret */:
                    return this.scanCaretToken();

                case 60 /* lessThan */:
                    return this.scanLessThanToken();

                case 62 /* greaterThan */:
                    return this.advanceAndSetTokenKind(81 /* GreaterThanToken */);

                case 44 /* comma */:
                    return this.advanceAndSetTokenKind(79 /* CommaToken */);

                case 58 /* colon */:
                    return this.advanceAndSetTokenKind(106 /* ColonToken */);

                case 59 /* semicolon */:
                    return this.advanceAndSetTokenKind(78 /* SemicolonToken */);

                case 126 /* tilde */:
                    return this.advanceAndSetTokenKind(102 /* TildeToken */);

                case 40 /* openParen */:
                    return this.advanceAndSetTokenKind(72 /* OpenParenToken */);

                case 41 /* closeParen */:
                    return this.advanceAndSetTokenKind(73 /* CloseParenToken */);

                case 123 /* openBrace */:
                    return this.advanceAndSetTokenKind(70 /* OpenBraceToken */);

                case 125 /* closeBrace */:
                    return this.advanceAndSetTokenKind(71 /* CloseBraceToken */);

                case 91 /* openBracket */:
                    return this.advanceAndSetTokenKind(74 /* OpenBracketToken */);

                case 93 /* closeBracket */:
                    return this.advanceAndSetTokenKind(75 /* CloseBracketToken */);

                case 63 /* question */:
                    return this.advanceAndSetTokenKind(105 /* QuestionToken */);
            }

            if (isNumericLiteralStart[character]) {
                return this.scanNumericLiteral(diagnostics);
            }

            // We run into so many identifiers (and keywords) when scanning, that we want the code to
            // be as fast as possible.  To that end, we have an extremely fast path for scanning that
            // handles the 99.9% case of no-unicode characters and no unicode escapes.
            if (isIdentifierStartCharacter[character]) {
                var result = this.tryFastScanIdentifierOrKeyword(character);
                if (result !== 0 /* None */) {
                    return result;
                }
            }

            if (this.isIdentifierStart(this.peekCharOrUnicodeEscape())) {
                return this.slowScanIdentifierOrKeyword(diagnostics);
            }

            return this.scanDefaultCharacter(character, diagnostics);
        };

        Scanner.prototype.isIdentifierStart = function (interpretedChar) {
            if (isIdentifierStartCharacter[interpretedChar]) {
                return true;
            }

            return interpretedChar > 127 /* maxAsciiCharacter */ && TypeScript.Unicode.isIdentifierStart(interpretedChar, this._languageVersion);
        };

        Scanner.prototype.isIdentifierPart = function (interpretedChar) {
            if (isIdentifierPartCharacter[interpretedChar]) {
                return true;
            }

            return interpretedChar > 127 /* maxAsciiCharacter */ && TypeScript.Unicode.isIdentifierPart(interpretedChar, this._languageVersion);
        };

        Scanner.prototype.tryFastScanIdentifierOrKeyword = function (firstCharacter) {
            var slidingWindow = this.slidingWindow;
            var window = slidingWindow.window;

            var startIndex = slidingWindow.currentRelativeItemIndex;
            var endIndex = slidingWindow.windowCount;
            var currentIndex = startIndex;
            var character = 0;

            while (currentIndex < endIndex) {
                character = window[currentIndex];
                if (!isIdentifierPartCharacter[character]) {
                    break;
                }

                currentIndex++;
            }

            if (currentIndex === endIndex) {
                // We reached the end of the characters in the sliding window.  They were all
                // identifier characters.  Since we don't know what the next character is, we can't
                // tell what we've got here.  So just bail out to the slow path.
                return 0 /* None */;
            } else if (character === 92 /* backslash */ || character > 127 /* maxAsciiCharacter */) {
                // We saw a \ (which could start a unicode escape), or we saw a unicode character.
                // This can't be scanned quickly.  Don't update the window position and just bail out
                // to the slow path.
                return 0 /* None */;
            } else {
                // Saw an ascii character that wasn't a backslash and wasn't an identifier
                // character.  This identifier is done.
                // Also check if it a keyword if it started with a lowercase letter.
                var kind;
                var identifierLength = currentIndex - startIndex;
                if (isKeywordStartCharacter[firstCharacter]) {
                    kind = TypeScript.ScannerUtilities.identifierKind(window, startIndex, identifierLength);
                } else {
                    kind = 11 /* IdentifierName */;
                }

                // Now, update the position of the sliding window so it's pointing at the right place.
                slidingWindow.setAbsoluteIndex(slidingWindow.absoluteIndex() + identifierLength);

                return kind;
            }
        };

        // A slow path for scanning identifiers.  Called when we run into a unicode character or
        // escape sequence while processing the fast path.
        Scanner.prototype.slowScanIdentifierOrKeyword = function (diagnostics) {
            var startIndex = this.slidingWindow.absoluteIndex();
            var sawUnicodeEscape = false;

            do {
                var unicodeEscape = this.scanCharOrUnicodeEscape(diagnostics);
                sawUnicodeEscape = sawUnicodeEscape || unicodeEscape;
            } while(this.isIdentifierPart(this.peekCharOrUnicodeEscape()));

            // From ES6 specification.
            // The ReservedWord definitions are specified as literal sequences of Unicode
            // characters.However, any Unicode character in a ReservedWord can also be
            // expressed by a \ UnicodeEscapeSequence that expresses that same Unicode
            // character's code point.Use of such escape sequences does not change the meaning
            // of the ReservedWord.
            //
            // i.e. "\u0076ar" is the keyword 'var'.  Check for that here.
            var length = this.slidingWindow.absoluteIndex() - startIndex;
            var text = this.text.substr(startIndex, length, false);
            var valueText = TypeScript.Syntax.massageEscapes(text);

            var keywordKind = TypeScript.SyntaxFacts.getTokenKind(valueText);
            if (keywordKind >= TypeScript.SyntaxKind.FirstKeyword && keywordKind <= TypeScript.SyntaxKind.LastKeyword) {
                if (sawUnicodeEscape) {
                    return keywordKind | TypeScript.SyntaxConstants.IsVariableWidthKeyword;
                } else {
                    return keywordKind;
                }
            }

            return 11 /* IdentifierName */;
        };

        Scanner.prototype.scanNumericLiteral = function (diagnostics) {
            if (this.isHexNumericLiteral()) {
                this.scanHexNumericLiteral();
            } else if (this.isOctalNumericLiteral()) {
                this.scanOctalNumericLiteral(diagnostics);
            } else {
                this.scanDecimalNumericLiteral();
            }

            return 13 /* NumericLiteral */;
        };

        Scanner.prototype.isOctalNumericLiteral = function () {
            return this.currentCharCode() === 48 /* _0 */ && TypeScript.CharacterInfo.isOctalDigit(this.slidingWindow.peekItemN(1));
        };

        Scanner.prototype.scanOctalNumericLiteral = function (diagnostics) {
            var position = this.absoluteIndex();

            while (TypeScript.CharacterInfo.isOctalDigit(this.currentCharCode())) {
                this.slidingWindow.moveToNextItem();
            }

            if (this.languageVersion() >= 1 /* EcmaScript5 */) {
                diagnostics.push(new TypeScript.Diagnostic(this.fileName, this.text.lineMap(), position, this.absoluteIndex() - position, TypeScript.DiagnosticCode.Octal_literals_are_not_available_when_targeting_ECMAScript_5_and_higher, null));
            }
        };

        Scanner.prototype.scanDecimalDigits = function () {
            while (TypeScript.CharacterInfo.isDecimalDigit(this.currentCharCode())) {
                this.slidingWindow.moveToNextItem();
            }
        };

        Scanner.prototype.scanDecimalNumericLiteral = function () {
            this.scanDecimalDigits();

            if (this.currentCharCode() === 46 /* dot */) {
                this.slidingWindow.moveToNextItem();
            }

            this.scanDecimalDigits();

            // If we see an 'e' or 'E' we should only consume it if its of the form:
            // e<number> or E<number>
            // e+<number>   E+<number>
            // e-<number>   E-<number>
            var ch = this.currentCharCode();
            if (ch === 101 /* e */ || ch === 69 /* E */) {
                // Ok, we've got 'e' or 'E'.  Make sure it's followed correctly.
                var nextChar1 = this.slidingWindow.peekItemN(1);

                if (TypeScript.CharacterInfo.isDecimalDigit(nextChar1)) {
                    // e<number> or E<number>
                    // Consume 'e' or 'E' and the number portion.
                    this.slidingWindow.moveToNextItem();
                    this.scanDecimalDigits();
                } else if (nextChar1 === 45 /* minus */ || nextChar1 === 43 /* plus */) {
                    // e+ or E+ or e- or E-
                    var nextChar2 = this.slidingWindow.peekItemN(2);
                    if (TypeScript.CharacterInfo.isDecimalDigit(nextChar2)) {
                        // e+<number> or E+<number> or e-<number> or E-<number>
                        // Consume first two characters and the number portion.
                        this.slidingWindow.moveToNextItem();
                        this.slidingWindow.moveToNextItem();
                        this.scanDecimalDigits();
                    }
                }
            }
        };

        Scanner.prototype.scanHexNumericLiteral = function () {
            // Debug.assert(this.isHexNumericLiteral());
            // Move past the 0x.
            this.slidingWindow.moveToNextItem();
            this.slidingWindow.moveToNextItem();

            while (TypeScript.CharacterInfo.isHexDigit(this.currentCharCode())) {
                this.slidingWindow.moveToNextItem();
            }
        };

        Scanner.prototype.isHexNumericLiteral = function () {
            if (this.currentCharCode() === 48 /* _0 */) {
                var ch = this.slidingWindow.peekItemN(1);

                if (ch === 120 /* x */ || ch === 88 /* X */) {
                    ch = this.slidingWindow.peekItemN(2);

                    return TypeScript.CharacterInfo.isHexDigit(ch);
                }
            }

            return false;
        };

        Scanner.prototype.advanceAndSetTokenKind = function (kind) {
            this.slidingWindow.moveToNextItem();
            return kind;
        };

        Scanner.prototype.scanLessThanToken = function () {
            this.slidingWindow.moveToNextItem();
            if (this.currentCharCode() === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 82 /* LessThanEqualsToken */;
            } else if (this.currentCharCode() === 60 /* lessThan */) {
                this.slidingWindow.moveToNextItem();
                if (this.currentCharCode() === 61 /* equals */) {
                    this.slidingWindow.moveToNextItem();
                    return 112 /* LessThanLessThanEqualsToken */;
                } else {
                    return 95 /* LessThanLessThanToken */;
                }
            } else {
                return 80 /* LessThanToken */;
            }
        };

        Scanner.prototype.scanBarToken = function () {
            this.slidingWindow.moveToNextItem();
            if (this.currentCharCode() === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 116 /* BarEqualsToken */;
            } else if (this.currentCharCode() === 124 /* bar */) {
                this.slidingWindow.moveToNextItem();
                return 104 /* BarBarToken */;
            } else {
                return 99 /* BarToken */;
            }
        };

        Scanner.prototype.scanCaretToken = function () {
            this.slidingWindow.moveToNextItem();
            if (this.currentCharCode() === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 117 /* CaretEqualsToken */;
            } else {
                return 100 /* CaretToken */;
            }
        };

        Scanner.prototype.scanAmpersandToken = function () {
            this.slidingWindow.moveToNextItem();
            var character = this.currentCharCode();
            if (character === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 115 /* AmpersandEqualsToken */;
            } else if (this.currentCharCode() === 38 /* ampersand */) {
                this.slidingWindow.moveToNextItem();
                return 103 /* AmpersandAmpersandToken */;
            } else {
                return 98 /* AmpersandToken */;
            }
        };

        Scanner.prototype.scanPercentToken = function () {
            this.slidingWindow.moveToNextItem();
            if (this.currentCharCode() === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 111 /* PercentEqualsToken */;
            } else {
                return 92 /* PercentToken */;
            }
        };

        Scanner.prototype.scanMinusToken = function () {
            this.slidingWindow.moveToNextItem();
            var character = this.currentCharCode();

            if (character === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 109 /* MinusEqualsToken */;
            } else if (character === 45 /* minus */) {
                this.slidingWindow.moveToNextItem();
                return 94 /* MinusMinusToken */;
            } else {
                return 90 /* MinusToken */;
            }
        };

        Scanner.prototype.scanPlusToken = function () {
            this.slidingWindow.moveToNextItem();
            var character = this.currentCharCode();
            if (character === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 108 /* PlusEqualsToken */;
            } else if (character === 43 /* plus */) {
                this.slidingWindow.moveToNextItem();
                return 93 /* PlusPlusToken */;
            } else {
                return 89 /* PlusToken */;
            }
        };

        Scanner.prototype.scanAsteriskToken = function () {
            this.slidingWindow.moveToNextItem();
            if (this.currentCharCode() === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 110 /* AsteriskEqualsToken */;
            } else {
                return 91 /* AsteriskToken */;
            }
        };

        Scanner.prototype.scanEqualsToken = function () {
            this.slidingWindow.moveToNextItem();
            var character = this.currentCharCode();
            if (character === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();

                if (this.currentCharCode() === 61 /* equals */) {
                    this.slidingWindow.moveToNextItem();

                    return 87 /* EqualsEqualsEqualsToken */;
                } else {
                    return 84 /* EqualsEqualsToken */;
                }
            } else if (character === 62 /* greaterThan */) {
                this.slidingWindow.moveToNextItem();
                return 85 /* EqualsGreaterThanToken */;
            } else {
                return 107 /* EqualsToken */;
            }
        };

        Scanner.prototype.isDotPrefixedNumericLiteral = function () {
            if (this.currentCharCode() === 46 /* dot */) {
                var ch = this.slidingWindow.peekItemN(1);
                return TypeScript.CharacterInfo.isDecimalDigit(ch);
            }

            return false;
        };

        Scanner.prototype.scanDotToken = function (diagnostics) {
            if (this.isDotPrefixedNumericLiteral()) {
                return this.scanNumericLiteral(diagnostics);
            }

            this.slidingWindow.moveToNextItem();
            if (this.currentCharCode() === 46 /* dot */ && this.slidingWindow.peekItemN(1) === 46 /* dot */) {
                this.slidingWindow.moveToNextItem();
                this.slidingWindow.moveToNextItem();
                return 77 /* DotDotDotToken */;
            } else {
                return 76 /* DotToken */;
            }
        };

        Scanner.prototype.scanSlashToken = function (allowRegularExpression) {
            // NOTE: By default, we do not try scanning a / as a regexp here.  We instead consider it a
            // div or div-assign.  Later on, if the parser runs into a situation where it would like a
            // term, and it sees one of these then it may restart us asking specifically if we could
            // scan out a regex.
            if (allowRegularExpression) {
                var result = this.tryScanRegularExpressionToken();
                if (result !== 0 /* None */) {
                    return result;
                }
            }

            this.slidingWindow.moveToNextItem();
            if (this.currentCharCode() === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();
                return 119 /* SlashEqualsToken */;
            } else {
                return 118 /* SlashToken */;
            }
        };

        Scanner.prototype.tryScanRegularExpressionToken = function () {
            // Debug.assert(this.currentCharCode() === CharacterCodes.slash);
            var startIndex = this.slidingWindow.getAndPinAbsoluteIndex();

            this.slidingWindow.moveToNextItem();

            var inEscape = false;
            var inCharacterClass = false;
            while (true) {
                var ch = this.currentCharCode();

                if (this.isNewLineCharacter(ch) || this.slidingWindow.isAtEndOfSource()) {
                    this.slidingWindow.rewindToPinnedIndex(startIndex);
                    this.slidingWindow.releaseAndUnpinAbsoluteIndex(startIndex);
                    return 0 /* None */;
                }

                this.slidingWindow.moveToNextItem();
                if (inEscape) {
                    inEscape = false;
                    continue;
                }

                switch (ch) {
                    case 92 /* backslash */:
                        // We're now in an escape.  Consume the next character we see (unless it's
                        // a newline or null.
                        inEscape = true;
                        continue;

                    case 91 /* openBracket */:
                        // If we see a [ then we're starting an character class.  Note: it's ok if
                        // we then hit another [ inside a character class.  We'll just set the value
                        // to true again and that's ok.
                        inCharacterClass = true;
                        continue;

                    case 93 /* closeBracket */:
                        // If we ever hit a cloe bracket then we're now no longer in a character
                        // class.  If we weren't in a character class to begin with, then this has
                        // no effect.
                        inCharacterClass = false;
                        continue;

                    case 47 /* slash */:
                        // If we see a slash, and we're in a character class, then ignore it.
                        if (inCharacterClass) {
                            continue;
                        }

                        break;

                    default:
                        continue;
                }

                break;
            }

            while (isIdentifierPartCharacter[this.currentCharCode()]) {
                this.slidingWindow.moveToNextItem();
            }

            this.slidingWindow.releaseAndUnpinAbsoluteIndex(startIndex);
            return 12 /* RegularExpressionLiteral */;
        };

        Scanner.prototype.scanExclamationToken = function () {
            this.slidingWindow.moveToNextItem();
            if (this.currentCharCode() === 61 /* equals */) {
                this.slidingWindow.moveToNextItem();

                if (this.currentCharCode() === 61 /* equals */) {
                    this.slidingWindow.moveToNextItem();

                    return 88 /* ExclamationEqualsEqualsToken */;
                } else {
                    return 86 /* ExclamationEqualsToken */;
                }
            } else {
                return 101 /* ExclamationToken */;
            }
        };

        Scanner.prototype.scanDefaultCharacter = function (character, diagnostics) {
            var position = this.slidingWindow.absoluteIndex();
            this.slidingWindow.moveToNextItem();

            var text = String.fromCharCode(character);
            var messageText = this.getErrorMessageText(text);
            diagnostics.push(new TypeScript.Diagnostic(this.fileName, this.text.lineMap(), position, 1, TypeScript.DiagnosticCode.Unexpected_character_0, [messageText]));

            return 9 /* ErrorToken */;
        };

        // Convert text into a printable form usable for an error message.  This will both quote the
        // string, and ensure all characters printable (i.e. by using unicode escapes when they're not).
        Scanner.prototype.getErrorMessageText = function (text) {
            // For just a simple backslash, we return it as is.  The default behavior of JSON.stringify
            // is not what we want here.
            if (text === "\\") {
                return '"\\"';
            }

            return JSON.stringify(text);
        };

        Scanner.prototype.skipEscapeSequence = function (diagnostics) {
            // Debug.assert(this.currentCharCode() === CharacterCodes.backslash);
            var rewindPoint = this.slidingWindow.getAndPinAbsoluteIndex();

            // Consume the backslash.
            this.slidingWindow.moveToNextItem();

            // Get the char after the backslash
            var ch = this.currentCharCode();
            this.slidingWindow.moveToNextItem();
            switch (ch) {
                case 120 /* x */:
                case 117 /* u */:
                    this.slidingWindow.rewindToPinnedIndex(rewindPoint);
                    var value = this.scanUnicodeOrHexEscape(diagnostics);
                    break;

                case 13 /* carriageReturn */:
                    // If it's \r\n then consume both characters.
                    if (this.currentCharCode() === 10 /* lineFeed */) {
                        this.slidingWindow.moveToNextItem();
                    }
                    break;

                default:
                    break;
            }

            this.slidingWindow.releaseAndUnpinAbsoluteIndex(rewindPoint);
        };

        Scanner.prototype.scanStringLiteral = function (diagnostics) {
            var quoteCharacter = this.currentCharCode();

            // Debug.assert(quoteCharacter === CharacterCodes.singleQuote || quoteCharacter === CharacterCodes.doubleQuote);
            this.slidingWindow.moveToNextItem();

            while (true) {
                var ch = this.currentCharCode();
                if (ch === 92 /* backslash */) {
                    this.skipEscapeSequence(diagnostics);
                } else if (ch === quoteCharacter) {
                    this.slidingWindow.moveToNextItem();
                    break;
                } else if (this.isNewLineCharacter(ch) || this.slidingWindow.isAtEndOfSource()) {
                    diagnostics.push(new TypeScript.Diagnostic(this.fileName, this.text.lineMap(), TypeScript.MathPrototype.min(this.slidingWindow.absoluteIndex(), this.text.length()), 1, TypeScript.DiagnosticCode.Missing_close_quote_character, null));
                    break;
                } else {
                    this.slidingWindow.moveToNextItem();
                }
            }

            return 14 /* StringLiteral */;
        };

        Scanner.prototype.isUnicodeEscape = function (character) {
            if (character === 92 /* backslash */) {
                var ch2 = this.slidingWindow.peekItemN(1);
                if (ch2 === 117 /* u */) {
                    return true;
                }
            }

            return false;
        };

        Scanner.prototype.peekCharOrUnicodeEscape = function () {
            var character = this.currentCharCode();
            if (this.isUnicodeEscape(character)) {
                return this.peekUnicodeOrHexEscape();
            } else {
                return character;
            }
        };

        Scanner.prototype.peekUnicodeOrHexEscape = function () {
            var startIndex = this.slidingWindow.getAndPinAbsoluteIndex();

            // if we're peeking, then we don't want to change the position
            var ch = this.scanUnicodeOrHexEscape(null);

            this.slidingWindow.rewindToPinnedIndex(startIndex);
            this.slidingWindow.releaseAndUnpinAbsoluteIndex(startIndex);

            return ch;
        };

        // Returns true if this was a unicode escape.
        Scanner.prototype.scanCharOrUnicodeEscape = function (errors) {
            var ch = this.currentCharCode();
            if (ch === 92 /* backslash */) {
                var ch2 = this.slidingWindow.peekItemN(1);
                if (ch2 === 117 /* u */) {
                    this.scanUnicodeOrHexEscape(errors);
                    return true;
                }
            }

            this.slidingWindow.moveToNextItem();
            return false;
        };

        Scanner.prototype.scanUnicodeOrHexEscape = function (errors) {
            var start = this.slidingWindow.absoluteIndex();
            var character = this.currentCharCode();

            // Debug.assert(character === CharacterCodes.backslash);
            this.slidingWindow.moveToNextItem();

            character = this.currentCharCode();

            // Debug.assert(character === CharacterCodes.u || character === CharacterCodes.x);
            var intChar = 0;
            this.slidingWindow.moveToNextItem();

            var count = character === 117 /* u */ ? 4 : 2;

            for (var i = 0; i < count; i++) {
                var ch2 = this.currentCharCode();
                if (!TypeScript.CharacterInfo.isHexDigit(ch2)) {
                    if (errors !== null) {
                        var end = this.slidingWindow.absoluteIndex();
                        var info = this.createIllegalEscapeDiagnostic(start, end);
                        errors.push(info);
                    }

                    break;
                }

                intChar = (intChar << 4) + TypeScript.CharacterInfo.hexValue(ch2);
                this.slidingWindow.moveToNextItem();
            }

            return intChar;
        };

        Scanner.prototype.substring = function (start, end, intern) {
            var length = end - start;
            var offset = start - this.slidingWindow.windowAbsoluteStartIndex;

            // Debug.assert(offset >= 0);
            if (intern) {
                return TypeScript.Collections.DefaultStringTable.addCharArray(this.slidingWindow.window, offset, length);
            } else {
                return TypeScript.StringUtilities.fromCharCodeArray(this.slidingWindow.window.slice(offset, offset + length));
            }
        };

        Scanner.prototype.createIllegalEscapeDiagnostic = function (start, end) {
            return new TypeScript.Diagnostic(this.fileName, this.text.lineMap(), start, end - start, TypeScript.DiagnosticCode.Unrecognized_escape_sequence, null);
        };

        Scanner.isValidIdentifier = function (text, languageVersion) {
            var scanner = new Scanner(null, text, languageVersion, Scanner.triviaWindow);
            var errors = new Array();
            var token = scanner.scan(errors, false);

            return errors.length === 0 && TypeScript.SyntaxFacts.isIdentifierNameOrAnyKeyword(token) && token.width() === text.length();
        };
        Scanner.triviaWindow = TypeScript.ArrayUtilities.createArray(2048, 0);
        return Scanner;
    })();
    TypeScript.Scanner = Scanner;
})(TypeScript || (TypeScript = {}));
