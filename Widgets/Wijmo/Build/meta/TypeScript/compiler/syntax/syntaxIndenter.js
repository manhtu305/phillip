///<reference path='references.ts' />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var TypeScript;
(function (TypeScript) {
    var SyntaxIndenter = (function (_super) {
        __extends(SyntaxIndenter, _super);
        function SyntaxIndenter(indentFirstToken, indentationAmount, options) {
            _super.call(this);
            this.indentationAmount = indentationAmount;
            this.options = options;
            this.lastTriviaWasNewLine = indentFirstToken;
            this.indentationTrivia = TypeScript.Indentation.indentationTrivia(this.indentationAmount, this.options);
        }
        SyntaxIndenter.prototype.visitToken = function (token) {
            if (token.width() === 0) {
                return token;
            }

            var result = token;
            if (this.lastTriviaWasNewLine) {
                // have to add our indentation to every line that this token hits.
                result = token.withLeadingTrivia(this.indentTriviaList(token.leadingTrivia()));
            }

            this.lastTriviaWasNewLine = token.hasTrailingNewLine();
            return result;
        };

        SyntaxIndenter.prototype.indentTriviaList = function (triviaList) {
            var result = [];

            // First, update any existing trivia with the indent amount.  For example, combine the
            // indent with any whitespace trivia, or prepend any comments with the trivia.
            var indentNextTrivia = true;
            for (var i = 0, n = triviaList.count(); i < n; i++) {
                var trivia = triviaList.syntaxTriviaAt(i);

                var indentThisTrivia = indentNextTrivia;
                indentNextTrivia = false;

                switch (trivia.kind()) {
                    case 6 /* MultiLineCommentTrivia */:
                        this.indentMultiLineComment(trivia, indentThisTrivia, result);
                        continue;

                    case 7 /* SingleLineCommentTrivia */:
                    case 8 /* SkippedTokenTrivia */:
                        this.indentSingleLineOrSkippedText(trivia, indentThisTrivia, result);
                        continue;

                    case 4 /* WhitespaceTrivia */:
                        this.indentWhitespace(trivia, indentThisTrivia, result);
                        continue;

                    case 5 /* NewLineTrivia */:
                        // We hit a newline processing the trivia.  We need to add the indentation to the
                        // next line as well.  Note: don't bother indenting the newline itself.  This will
                        // just insert ugly whitespace that most users probably will not want.
                        result.push(trivia);
                        indentNextTrivia = true;
                        continue;

                    default:
                        throw TypeScript.Errors.invalidOperation();
                }
            }

            // Then, if the last trivia was a newline (or there was no trivia at all), then just add the
            // indentation in right before the token.
            if (indentNextTrivia) {
                result.push(this.indentationTrivia);
            }

            return TypeScript.Syntax.triviaList(result);
        };

        SyntaxIndenter.prototype.indentSegment = function (segment) {
            // Find the position of the first non whitespace character in the segment.
            var firstNonWhitespacePosition = TypeScript.Indentation.firstNonWhitespacePosition(segment);

            if (firstNonWhitespacePosition < segment.length && TypeScript.CharacterInfo.isLineTerminator(segment.charCodeAt(firstNonWhitespacePosition))) {
                // If this segment was just a newline, then don't bother indenting it.  That will just
                // leave the user with an ugly indent in their output that they probably do not want.
                return segment;
            }

            // Convert that position to a column.
            var firstNonWhitespaceColumn = TypeScript.Indentation.columnForPositionInString(segment, firstNonWhitespacePosition, this.options);

            // Find the new column we want the nonwhitespace text to start at.
            var newFirstNonWhitespaceColumn = firstNonWhitespaceColumn + this.indentationAmount;

            // Compute an indentation string for that.
            var indentationString = TypeScript.Indentation.indentationString(newFirstNonWhitespaceColumn, this.options);

            // Join the new indentation and the original string without its indentation.
            return indentationString + segment.substring(firstNonWhitespacePosition);
        };

        SyntaxIndenter.prototype.indentWhitespace = function (trivia, indentThisTrivia, result) {
            if (!indentThisTrivia) {
                // Line didn't start with this trivia.  So no need to touch it.  Just add to the result
                // and continue on.
                result.push(trivia);
                return;
            }

            // Line started with this trivia.  We want to figure out what the final column this
            // whitespace goes to will be.  To do that we add the column it is at now to the column we
            // want to indent to.  We then compute the final tabs+whitespace string for that.
            var newIndentation = this.indentSegment(trivia.fullText());
            result.push(TypeScript.Syntax.whitespace(newIndentation));
        };

        SyntaxIndenter.prototype.indentSingleLineOrSkippedText = function (trivia, indentThisTrivia, result) {
            if (indentThisTrivia) {
                // The line started with a comment or skipped text.  Add an indentation based
                // on the desired settings, and then add the trivia itself.
                result.push(this.indentationTrivia);
            }

            result.push(trivia);
        };

        SyntaxIndenter.prototype.indentMultiLineComment = function (trivia, indentThisTrivia, result) {
            if (indentThisTrivia) {
                // The line started with a multiline comment.  Add an indentation based
                // on the desired settings, and then add the trivia itself.
                result.push(this.indentationTrivia);
            }

            // If the multiline comment spans multiple lines, we need to add the right indent amount to
            // each successive line segment as well.
            var segments = TypeScript.Syntax.splitMultiLineCommentTriviaIntoMultipleLines(trivia);

            for (var i = 1; i < segments.length; i++) {
                segments[i] = this.indentSegment(segments[i]);
            }

            var newText = segments.join("");
            result.push(TypeScript.Syntax.multiLineComment(newText));
        };

        SyntaxIndenter.indentNode = function (node, indentFirstToken, indentAmount, options) {
            var indenter = new SyntaxIndenter(indentFirstToken, indentAmount, options);
            return node.accept(indenter);
        };

        SyntaxIndenter.indentNodes = function (nodes, indentFirstToken, indentAmount, options) {
            // Note: it is necessary for correctness that we reuse the same SyntaxIndenter here.
            // That's because when working on nodes 1-N, we need to know if the previous node ended
            // with a newline.  The indenter will track that for us.
            var indenter = new SyntaxIndenter(indentFirstToken, indentAmount, options);
            var result = TypeScript.ArrayUtilities.select(nodes, function (n) {
                return n.accept(indenter);
            });

            return result;
        };
        return SyntaxIndenter;
    })(TypeScript.SyntaxRewriter);
    TypeScript.SyntaxIndenter = SyntaxIndenter;
})(TypeScript || (TypeScript = {}));
