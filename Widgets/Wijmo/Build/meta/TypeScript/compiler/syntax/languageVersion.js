var TypeScript;
(function (TypeScript) {
    (function (LanguageVersion) {
        LanguageVersion[LanguageVersion["EcmaScript3"] = 0] = "EcmaScript3";
        LanguageVersion[LanguageVersion["EcmaScript5"] = 1] = "EcmaScript5";
    })(TypeScript.LanguageVersion || (TypeScript.LanguageVersion = {}));
    var LanguageVersion = TypeScript.LanguageVersion;
})(TypeScript || (TypeScript = {}));
