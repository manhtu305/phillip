///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    var ParseOptions = (function () {
        function ParseOptions(languageVersion, allowAutomaticSemicolonInsertion) {
            this._languageVersion = languageVersion;
            this._allowAutomaticSemicolonInsertion = allowAutomaticSemicolonInsertion;
        }
        ParseOptions.prototype.toJSON = function (key) {
            return { allowAutomaticSemicolonInsertion: this._allowAutomaticSemicolonInsertion };
        };

        ParseOptions.prototype.languageVersion = function () {
            return this._languageVersion;
        };

        ParseOptions.prototype.allowAutomaticSemicolonInsertion = function () {
            return this._allowAutomaticSemicolonInsertion;
        };
        return ParseOptions;
    })();
    TypeScript.ParseOptions = ParseOptions;
})(TypeScript || (TypeScript = {}));
