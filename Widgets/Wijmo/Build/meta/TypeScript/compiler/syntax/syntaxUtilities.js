///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    var SyntaxUtilities = (function () {
        function SyntaxUtilities() {
        }
        SyntaxUtilities.isAngleBracket = function (positionedElement) {
            var element = positionedElement.element();
            var parent = positionedElement.parentElement();
            if (parent !== null && (element.kind() === 80 /* LessThanToken */ || element.kind() === 81 /* GreaterThanToken */)) {
                switch (parent.kind()) {
                    case 228 /* TypeArgumentList */:
                    case 229 /* TypeParameterList */:
                    case 220 /* CastExpression */:
                        return true;
                }
            }

            return false;
        };

        SyntaxUtilities.getToken = function (list, kind) {
            for (var i = 0, n = list.childCount(); i < n; i++) {
                var token = list.childAt(i);
                if (token.tokenKind === kind) {
                    return token;
                }
            }

            return null;
        };

        SyntaxUtilities.containsToken = function (list, kind) {
            return SyntaxUtilities.getToken(list, kind) !== null;
        };

        SyntaxUtilities.hasExportKeyword = function (moduleElement) {
            return SyntaxUtilities.getExportKeyword(moduleElement) !== null;
        };

        SyntaxUtilities.getExportKeyword = function (moduleElement) {
            switch (moduleElement.kind()) {
                case 130 /* ModuleDeclaration */:
                case 131 /* ClassDeclaration */:
                case 129 /* FunctionDeclaration */:
                case 148 /* VariableStatement */:
                case 132 /* EnumDeclaration */:
                case 128 /* InterfaceDeclaration */:
                case 133 /* ImportDeclaration */:
                    return SyntaxUtilities.getToken(moduleElement.modifiers, 47 /* ExportKeyword */);
                default:
                    return null;
            }
        };

        SyntaxUtilities.isAmbientDeclarationSyntax = function (positionNode) {
            if (!positionNode) {
                return false;
            }

            var node = positionNode.node();
            switch (node.kind()) {
                case 130 /* ModuleDeclaration */:
                case 131 /* ClassDeclaration */:
                case 129 /* FunctionDeclaration */:
                case 148 /* VariableStatement */:
                case 132 /* EnumDeclaration */:
                    if (SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */)) {
                        return true;
                    }

                case 133 /* ImportDeclaration */:
                case 137 /* ConstructorDeclaration */:
                case 135 /* MemberFunctionDeclaration */:
                case 139 /* GetAccessor */:
                case 140 /* SetAccessor */:
                case 136 /* MemberVariableDeclaration */:
                    if (node.isClassElement() || node.isModuleElement()) {
                        return SyntaxUtilities.isAmbientDeclarationSyntax(positionNode.containingNode());
                    }

                case 243 /* EnumElement */:
                    return SyntaxUtilities.isAmbientDeclarationSyntax(positionNode.containingNode().containingNode());

                default:
                    return SyntaxUtilities.isAmbientDeclarationSyntax(positionNode.containingNode());
            }
        };
        return SyntaxUtilities;
    })();
    TypeScript.SyntaxUtilities = SyntaxUtilities;
})(TypeScript || (TypeScript = {}));
