///<reference path='references.ts' />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
// A debug class that we use to make sure a syntax node is valid.  Currently, this simply verifies
// that the same token does not appear in the tree multiple times.  This is important for
// subsystems that want to map between tokens and positions.  If a token shows up multiple times in
// the node, then it will not have a unique position, previous token, etc. etc. and that can screw
// many algorithms.  For this reason, when generating trees, it is important that nodes that are
// reused are cloned before insertion.
var TypeScript;
(function (TypeScript) {
    var SyntaxNodeInvariantsChecker = (function (_super) {
        __extends(SyntaxNodeInvariantsChecker, _super);
        function SyntaxNodeInvariantsChecker() {
            _super.apply(this, arguments);
            this.tokenTable = TypeScript.Collections.createHashTable(TypeScript.Collections.DefaultHashTableCapacity, TypeScript.Collections.identityHashCode);
        }
        SyntaxNodeInvariantsChecker.checkInvariants = function (node) {
            node.accept(new SyntaxNodeInvariantsChecker());
        };

        SyntaxNodeInvariantsChecker.prototype.visitToken = function (token) {
            // We're calling 'add', so the table will throw if we try to put the same token in multiple
            // times.
            this.tokenTable.add(token, token);
        };
        return SyntaxNodeInvariantsChecker;
    })(TypeScript.SyntaxWalker);
    TypeScript.SyntaxNodeInvariantsChecker = SyntaxNodeInvariantsChecker;
})(TypeScript || (TypeScript = {}));
