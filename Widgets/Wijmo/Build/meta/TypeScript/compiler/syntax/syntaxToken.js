///<reference path='references.ts' />

var TypeScript;
(function (TypeScript) {
    (function (Syntax) {
        function isExpression(token) {
            switch (token.tokenKind) {
                case 11 /* IdentifierName */:
                case 12 /* RegularExpressionLiteral */:
                case 13 /* NumericLiteral */:
                case 14 /* StringLiteral */:
                case 24 /* FalseKeyword */:
                case 32 /* NullKeyword */:
                case 35 /* ThisKeyword */:
                case 37 /* TrueKeyword */:
                case 50 /* SuperKeyword */:
                    return true;
            }

            return false;
        }
        Syntax.isExpression = isExpression;

        function realizeToken(token) {
            return new RealizedToken(token.tokenKind, token.leadingTrivia(), token.text(), token.value(), token.valueText(), token.trailingTrivia());
        }
        Syntax.realizeToken = realizeToken;

        function convertToIdentifierName(token) {
            TypeScript.Debug.assert(TypeScript.SyntaxFacts.isAnyKeyword(token.tokenKind));
            return new RealizedToken(11 /* IdentifierName */, token.leadingTrivia(), token.text(), token.text(), token.text(), token.trailingTrivia());
        }
        Syntax.convertToIdentifierName = convertToIdentifierName;

        function tokenToJSON(token) {
            var result = {};

            for (var name in TypeScript.SyntaxKind) {
                if (TypeScript.SyntaxKind[name] === token.kind()) {
                    result.kind = name;
                    break;
                }
            }

            result.width = token.width();
            if (token.fullWidth() !== token.width()) {
                result.fullWidth = token.fullWidth();
            }

            result.text = token.text();

            var value = token.value();
            if (value !== null) {
                result.value = value;
                result.valueText = token.valueText();
            }

            if (token.hasLeadingTrivia()) {
                result.hasLeadingTrivia = true;
            }

            if (token.hasLeadingComment()) {
                result.hasLeadingComment = true;
            }

            if (token.hasLeadingNewLine()) {
                result.hasLeadingNewLine = true;
            }

            if (token.hasLeadingSkippedText()) {
                result.hasLeadingSkippedText = true;
            }

            if (token.hasTrailingTrivia()) {
                result.hasTrailingTrivia = true;
            }

            if (token.hasTrailingComment()) {
                result.hasTrailingComment = true;
            }

            if (token.hasTrailingNewLine()) {
                result.hasTrailingNewLine = true;
            }

            if (token.hasTrailingSkippedText()) {
                result.hasTrailingSkippedText = true;
            }

            var trivia = token.leadingTrivia();
            if (trivia.count() > 0) {
                result.leadingTrivia = trivia;
            }

            trivia = token.trailingTrivia();
            if (trivia.count() > 0) {
                result.trailingTrivia = trivia;
            }

            return result;
        }
        Syntax.tokenToJSON = tokenToJSON;

        function value(token) {
            return value1(token.tokenKind, token.text());
        }
        Syntax.value = value;

        function hexValue(text, start, length) {
            var intChar = 0;
            for (var i = 0; i < length; i++) {
                var ch2 = text.charCodeAt(start + i);
                if (!TypeScript.CharacterInfo.isHexDigit(ch2)) {
                    break;
                }

                intChar = (intChar << 4) + TypeScript.CharacterInfo.hexValue(ch2);
            }

            return intChar;
        }

        var characterArray = [];

        function convertEscapes(text) {
            characterArray.length = 0;
            var result = "";

            for (var i = 0, n = text.length; i < n; i++) {
                var ch = text.charCodeAt(i);

                if (ch === 92 /* backslash */) {
                    i++;
                    if (i < n) {
                        ch = text.charCodeAt(i);
                        switch (ch) {
                            case 48 /* _0 */:
                                characterArray.push(0 /* nullCharacter */);
                                continue;

                            case 98 /* b */:
                                characterArray.push(8 /* backspace */);
                                continue;

                            case 102 /* f */:
                                characterArray.push(12 /* formFeed */);
                                continue;

                            case 110 /* n */:
                                characterArray.push(10 /* lineFeed */);
                                continue;

                            case 114 /* r */:
                                characterArray.push(13 /* carriageReturn */);
                                continue;

                            case 116 /* t */:
                                characterArray.push(9 /* tab */);
                                continue;

                            case 118 /* v */:
                                characterArray.push(11 /* verticalTab */);
                                continue;

                            case 120 /* x */:
                                characterArray.push(hexValue(text, i + /*start:*/ 1, /*length:*/ 2));
                                i += 2;
                                continue;

                            case 117 /* u */:
                                characterArray.push(hexValue(text, i + /*start:*/ 1, /*length:*/ 4));
                                i += 4;
                                continue;

                            case 13 /* carriageReturn */:
                                var nextIndex = i + 1;
                                if (nextIndex < text.length && text.charCodeAt(nextIndex) === 10 /* lineFeed */) {
                                    // Skip the entire \r\n sequence.
                                    i++;
                                }
                                continue;

                            case 10 /* lineFeed */:
                            case 8233 /* paragraphSeparator */:
                            case 8232 /* lineSeparator */:
                                continue;

                            default:
                        }
                    }
                }

                characterArray.push(ch);

                if (i && !(i % 1024)) {
                    result = result.concat(String.fromCharCode.apply(null, characterArray));
                    characterArray.length = 0;
                }
            }

            if (characterArray.length) {
                result = result.concat(String.fromCharCode.apply(null, characterArray));
            }

            return result;
        }

        function massageEscapes(text) {
            return text.indexOf("\\") >= 0 ? convertEscapes(text) : text;
        }
        Syntax.massageEscapes = massageEscapes;

        function value1(kind, text) {
            if (kind === 11 /* IdentifierName */) {
                return massageEscapes(text);
            }

            switch (kind) {
                case 37 /* TrueKeyword */:
                    return true;
                case 24 /* FalseKeyword */:
                    return false;
                case 32 /* NullKeyword */:
                    return null;
            }

            if (TypeScript.SyntaxFacts.isAnyKeyword(kind) || TypeScript.SyntaxFacts.isAnyPunctuation(kind)) {
                return TypeScript.SyntaxFacts.getText(kind);
            }

            if (kind === 13 /* NumericLiteral */) {
                return TypeScript.IntegerUtilities.isHexInteger(text) ? parseInt(text, /*radix:*/ 16) : parseFloat(text);
            } else if (kind === 14 /* StringLiteral */) {
                if (text.length > 1 && text.charCodeAt(text.length - 1) === text.charCodeAt(0)) {
                    // Properly terminated.  Remove the quotes, and massage any escape characters we see.
                    return massageEscapes(text.substr(1, text.length - 2));
                } else {
                    // Not property terminated.  Remove the first quote and massage any escape characters we see.
                    return massageEscapes(text.substr(1));
                }
            } else if (kind === 12 /* RegularExpressionLiteral */) {
                return regularExpressionValue(text);
            } else if (kind === 10 /* EndOfFileToken */ || kind === 9 /* ErrorToken */) {
                return null;
            } else {
                throw TypeScript.Errors.invalidOperation();
            }
        }

        function regularExpressionValue(text) {
            try  {
                var lastSlash = text.lastIndexOf("/");
                var body = text.substring(1, lastSlash);
                var flags = text.substring(lastSlash + 1);
                return new RegExp(body, flags);
            } catch (e) {
                return null;
            }
        }

        function valueText1(kind, text) {
            var value = value1(kind, text);
            return value === null ? "" : value.toString();
        }

        function valueText(token) {
            var value = token.value();
            return value === null ? "" : value.toString();
        }
        Syntax.valueText = valueText;

        var EmptyToken = (function () {
            function EmptyToken(kind) {
                this.tokenKind = kind;
            }
            EmptyToken.prototype.clone = function () {
                return new EmptyToken(this.tokenKind);
            };

            EmptyToken.prototype.kind = function () {
                return this.tokenKind;
            };

            EmptyToken.prototype.isToken = function () {
                return true;
            };
            EmptyToken.prototype.isNode = function () {
                return false;
            };
            EmptyToken.prototype.isList = function () {
                return false;
            };
            EmptyToken.prototype.isSeparatedList = function () {
                return false;
            };

            EmptyToken.prototype.childCount = function () {
                return 0;
            };

            EmptyToken.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange("index");
            };

            EmptyToken.prototype.toJSON = function (key) {
                return tokenToJSON(this);
            };
            EmptyToken.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };

            EmptyToken.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            EmptyToken.prototype.firstToken = function () {
                return this;
            };
            EmptyToken.prototype.lastToken = function () {
                return this;
            };
            EmptyToken.prototype.isTypeScriptSpecific = function () {
                return false;
            };

            // Empty tokens are never incrementally reusable.
            EmptyToken.prototype.isIncrementallyUnusable = function () {
                return true;
            };

            EmptyToken.prototype.fullWidth = function () {
                return 0;
            };
            EmptyToken.prototype.width = function () {
                return 0;
            };
            EmptyToken.prototype.text = function () {
                return "";
            };
            EmptyToken.prototype.fullText = function () {
                return "";
            };
            EmptyToken.prototype.value = function () {
                return null;
            };
            EmptyToken.prototype.valueText = function () {
                return "";
            };

            EmptyToken.prototype.hasLeadingTrivia = function () {
                return false;
            };
            EmptyToken.prototype.hasLeadingComment = function () {
                return false;
            };
            EmptyToken.prototype.hasLeadingNewLine = function () {
                return false;
            };
            EmptyToken.prototype.hasLeadingSkippedText = function () {
                return false;
            };
            EmptyToken.prototype.leadingTriviaWidth = function () {
                return 0;
            };
            EmptyToken.prototype.hasTrailingTrivia = function () {
                return false;
            };
            EmptyToken.prototype.hasTrailingComment = function () {
                return false;
            };
            EmptyToken.prototype.hasTrailingNewLine = function () {
                return false;
            };
            EmptyToken.prototype.hasTrailingSkippedText = function () {
                return false;
            };
            EmptyToken.prototype.hasSkippedToken = function () {
                return false;
            };

            EmptyToken.prototype.trailingTriviaWidth = function () {
                return 0;
            };
            EmptyToken.prototype.leadingTrivia = function () {
                return Syntax.emptyTriviaList;
            };
            EmptyToken.prototype.trailingTrivia = function () {
                return Syntax.emptyTriviaList;
            };
            EmptyToken.prototype.realize = function () {
                return realizeToken(this);
            };
            EmptyToken.prototype.collectTextElements = function (elements) {
            };

            EmptyToken.prototype.withLeadingTrivia = function (leadingTrivia) {
                return this.realize().withLeadingTrivia(leadingTrivia);
            };

            EmptyToken.prototype.withTrailingTrivia = function (trailingTrivia) {
                return this.realize().withTrailingTrivia(trailingTrivia);
            };

            EmptyToken.prototype.isExpression = function () {
                return isExpression(this);
            };

            EmptyToken.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            EmptyToken.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            EmptyToken.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            EmptyToken.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return EmptyToken;
        })();

        function emptyToken(kind) {
            return new EmptyToken(kind);
        }
        Syntax.emptyToken = emptyToken;

        var RealizedToken = (function () {
            function RealizedToken(tokenKind, leadingTrivia, text, value, valueText, trailingTrivia) {
                this.tokenKind = tokenKind;
                this._leadingTrivia = leadingTrivia;
                this._text = text;
                this._value = value;
                this._valueText = valueText;
                this._trailingTrivia = trailingTrivia;
            }
            RealizedToken.prototype.clone = function () {
                return new RealizedToken(this.tokenKind, this._leadingTrivia, this._text, this._value, this._valueText, this._trailingTrivia);
            };

            RealizedToken.prototype.kind = function () {
                return this.tokenKind;
            };
            RealizedToken.prototype.toJSON = function (key) {
                return tokenToJSON(this);
            };
            RealizedToken.prototype.firstToken = function () {
                return this;
            };
            RealizedToken.prototype.lastToken = function () {
                return this;
            };
            RealizedToken.prototype.isTypeScriptSpecific = function () {
                return false;
            };

            // Realized tokens are created from the parser.  They are *never* incrementally reusable.
            RealizedToken.prototype.isIncrementallyUnusable = function () {
                return true;
            };

            RealizedToken.prototype.accept = function (visitor) {
                return visitor.visitToken(this);
            };

            RealizedToken.prototype.childCount = function () {
                return 0;
            };

            RealizedToken.prototype.childAt = function (index) {
                throw TypeScript.Errors.argumentOutOfRange("index");
            };

            RealizedToken.prototype.isToken = function () {
                return true;
            };
            RealizedToken.prototype.isNode = function () {
                return false;
            };
            RealizedToken.prototype.isList = function () {
                return false;
            };
            RealizedToken.prototype.isSeparatedList = function () {
                return false;
            };
            RealizedToken.prototype.isTrivia = function () {
                return false;
            };
            RealizedToken.prototype.isTriviaList = function () {
                return false;
            };

            RealizedToken.prototype.fullWidth = function () {
                return this._leadingTrivia.fullWidth() + this.width() + this._trailingTrivia.fullWidth();
            };
            RealizedToken.prototype.width = function () {
                return this.text().length;
            };

            RealizedToken.prototype.text = function () {
                return this._text;
            };
            RealizedToken.prototype.fullText = function () {
                return this._leadingTrivia.fullText() + this.text() + this._trailingTrivia.fullText();
            };

            RealizedToken.prototype.value = function () {
                return this._value;
            };
            RealizedToken.prototype.valueText = function () {
                return this._valueText;
            };

            RealizedToken.prototype.hasLeadingTrivia = function () {
                return this._leadingTrivia.count() > 0;
            };
            RealizedToken.prototype.hasLeadingComment = function () {
                return this._leadingTrivia.hasComment();
            };
            RealizedToken.prototype.hasLeadingNewLine = function () {
                return this._leadingTrivia.hasNewLine();
            };
            RealizedToken.prototype.hasLeadingSkippedText = function () {
                return this._leadingTrivia.hasSkippedToken();
            };
            RealizedToken.prototype.leadingTriviaWidth = function () {
                return this._leadingTrivia.fullWidth();
            };

            RealizedToken.prototype.hasTrailingTrivia = function () {
                return this._trailingTrivia.count() > 0;
            };
            RealizedToken.prototype.hasTrailingComment = function () {
                return this._trailingTrivia.hasComment();
            };
            RealizedToken.prototype.hasTrailingNewLine = function () {
                return this._trailingTrivia.hasNewLine();
            };
            RealizedToken.prototype.hasTrailingSkippedText = function () {
                return this._trailingTrivia.hasSkippedToken();
            };
            RealizedToken.prototype.trailingTriviaWidth = function () {
                return this._trailingTrivia.fullWidth();
            };

            RealizedToken.prototype.hasSkippedToken = function () {
                return this.hasLeadingSkippedText() || this.hasTrailingSkippedText();
            };

            RealizedToken.prototype.leadingTrivia = function () {
                return this._leadingTrivia;
            };
            RealizedToken.prototype.trailingTrivia = function () {
                return this._trailingTrivia;
            };

            RealizedToken.prototype.findTokenInternal = function (parent, position, fullStart) {
                return new TypeScript.PositionedToken(parent, this, fullStart);
            };

            RealizedToken.prototype.collectTextElements = function (elements) {
                this.leadingTrivia().collectTextElements(elements);
                elements.push(this.text());
                this.trailingTrivia().collectTextElements(elements);
            };

            RealizedToken.prototype.withLeadingTrivia = function (leadingTrivia) {
                return new RealizedToken(this.tokenKind, leadingTrivia, this._text, this._value, this._valueText, this._trailingTrivia);
            };

            RealizedToken.prototype.withTrailingTrivia = function (trailingTrivia) {
                return new RealizedToken(this.tokenKind, this._leadingTrivia, this._text, this._value, this._valueText, trailingTrivia);
            };

            RealizedToken.prototype.isExpression = function () {
                return isExpression(this);
            };

            RealizedToken.prototype.isPrimaryExpression = function () {
                return this.isExpression();
            };

            RealizedToken.prototype.isMemberExpression = function () {
                return this.isExpression();
            };

            RealizedToken.prototype.isPostfixExpression = function () {
                return this.isExpression();
            };

            RealizedToken.prototype.isUnaryExpression = function () {
                return this.isExpression();
            };
            return RealizedToken;
        })();

        function token(kind, info) {
            if (typeof info === "undefined") { info = null; }
            var text = (info !== null && info.text !== undefined) ? info.text : TypeScript.SyntaxFacts.getText(kind);

            return new RealizedToken(kind, Syntax.triviaList(info === null ? null : info.leadingTrivia), text, value1(kind, text), valueText1(kind, text), Syntax.triviaList(info === null ? null : info.trailingTrivia));
        }
        Syntax.token = token;

        function identifier(text, info) {
            if (typeof info === "undefined") { info = null; }
            info = info || {};
            info.text = text;
            return token(11 /* IdentifierName */, info);
        }
        Syntax.identifier = identifier;
    })(TypeScript.Syntax || (TypeScript.Syntax = {}));
    var Syntax = TypeScript.Syntax;
})(TypeScript || (TypeScript = {}));
