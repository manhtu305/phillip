///<reference path='references.ts' />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var TypeScript;
(function (TypeScript) {
    var SyntaxTree = (function () {
        function SyntaxTree(sourceUnit, isDeclaration, diagnostics, fileName, lineMap, parseOtions) {
            this._allDiagnostics = null;
            this._sourceUnit = sourceUnit;
            this._isDeclaration = isDeclaration;
            this._parserDiagnostics = diagnostics;
            this._fileName = fileName;
            this._lineMap = lineMap;
            this._parseOptions = parseOtions;
        }
        SyntaxTree.prototype.toJSON = function (key) {
            var result = {};

            result.isDeclaration = this._isDeclaration;
            result.languageVersion = TypeScript.LanguageVersion[this._parseOptions.languageVersion()];
            result.parseOptions = this._parseOptions;

            if (this.diagnostics().length > 0) {
                result.diagnostics = this.diagnostics();
            }

            result.sourceUnit = this._sourceUnit;
            result.lineMap = this._lineMap;

            return result;
        };

        SyntaxTree.prototype.sourceUnit = function () {
            return this._sourceUnit;
        };

        SyntaxTree.prototype.isDeclaration = function () {
            return this._isDeclaration;
        };

        SyntaxTree.prototype.computeDiagnostics = function () {
            if (this._parserDiagnostics.length > 0) {
                return this._parserDiagnostics;
            }

            // No parser reported diagnostics.  Check for any additional grammar diagnostics.
            var diagnostics = [];
            this.sourceUnit().accept(new GrammarCheckerWalker(this, diagnostics));

            return diagnostics;
        };

        SyntaxTree.prototype.diagnostics = function () {
            if (this._allDiagnostics === null) {
                this._allDiagnostics = this.computeDiagnostics();
            }

            return this._allDiagnostics;
        };

        SyntaxTree.prototype.fileName = function () {
            return this._fileName;
        };

        SyntaxTree.prototype.lineMap = function () {
            return this._lineMap;
        };

        SyntaxTree.prototype.parseOptions = function () {
            return this._parseOptions;
        };

        SyntaxTree.prototype.structuralEquals = function (tree) {
            return TypeScript.ArrayUtilities.sequenceEquals(this.diagnostics(), tree.diagnostics(), TypeScript.Diagnostic.equals) && this.sourceUnit().structuralEquals(tree.sourceUnit());
        };
        return SyntaxTree;
    })();
    TypeScript.SyntaxTree = SyntaxTree;

    var GrammarCheckerWalker = (function (_super) {
        __extends(GrammarCheckerWalker, _super);
        function GrammarCheckerWalker(syntaxTree, diagnostics) {
            _super.call(this);
            this.syntaxTree = syntaxTree;
            this.diagnostics = diagnostics;
            this.inAmbientDeclaration = false;
            this.inBlock = false;
            this.inObjectLiteralExpression = false;
            this.currentConstructor = null;
        }
        GrammarCheckerWalker.prototype.childFullStart = function (parent, child) {
            return this.position() + TypeScript.Syntax.childOffset(parent, child);
        };

        GrammarCheckerWalker.prototype.childStart = function (parent, child) {
            return this.childFullStart(parent, child) + child.leadingTriviaWidth();
        };

        GrammarCheckerWalker.prototype.pushDiagnostic = function (start, length, diagnosticKey, args) {
            if (typeof args === "undefined") { args = null; }
            this.diagnostics.push(new TypeScript.Diagnostic(this.syntaxTree.fileName(), this.syntaxTree.lineMap(), start, length, diagnosticKey, args));
        };

        GrammarCheckerWalker.prototype.pushDiagnostic1 = function (elementFullStart, element, diagnosticKey, args) {
            if (typeof args === "undefined") { args = null; }
            this.diagnostics.push(new TypeScript.Diagnostic(this.syntaxTree.fileName(), this.syntaxTree.lineMap(), elementFullStart + element.leadingTriviaWidth(), element.width(), diagnosticKey, args));
        };

        GrammarCheckerWalker.prototype.visitCatchClause = function (node) {
            if (node.typeAnnotation) {
                this.pushDiagnostic(this.childStart(node, node.typeAnnotation), node.typeAnnotation.width(), TypeScript.DiagnosticCode.Catch_clause_parameter_cannot_have_a_type_annotation);
            }

            _super.prototype.visitCatchClause.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkParameterListOrder = function (node) {
            var parameterFullStart = this.childFullStart(node, node.parameters);

            var seenOptionalParameter = false;
            var parameterCount = node.parameters.nonSeparatorCount();

            for (var i = 0, n = node.parameters.childCount(); i < n; i++) {
                var nodeOrToken = node.parameters.childAt(i);
                if (i % 2 === 0) {
                    var parameterIndex = i / 2;
                    var parameter = node.parameters.childAt(i);

                    if (parameter.dotDotDotToken) {
                        if (parameterIndex !== (parameterCount - 1)) {
                            this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Rest_parameter_must_be_last_in_list);
                            return true;
                        }

                        if (parameter.questionToken) {
                            this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Rest_parameter_cannot_be_optional);
                            return true;
                        }

                        if (parameter.equalsValueClause) {
                            this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Rest_parameter_cannot_have_an_initializer);
                            return true;
                        }
                    } else if (parameter.questionToken || parameter.equalsValueClause) {
                        seenOptionalParameter = true;

                        if (parameter.questionToken && parameter.equalsValueClause) {
                            this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Parameter_cannot_have_question_mark_and_initializer);
                            return true;
                        }
                    } else {
                        if (seenOptionalParameter) {
                            this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Required_parameter_cannot_follow_optional_parameter);
                            return true;
                        }
                    }
                }

                parameterFullStart += nodeOrToken.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkParameterListAcessibilityModifiers = function (node) {
            var parameterFullStart = this.childFullStart(node, node.parameters);

            for (var i = 0, n = node.parameters.childCount(); i < n; i++) {
                var nodeOrToken = node.parameters.childAt(i);
                if (i % 2 === 0) {
                    var parameter = node.parameters.childAt(i);

                    if (this.checkParameterAccessibilityModifiers(node, parameter, parameterFullStart)) {
                        return true;
                    }
                }

                parameterFullStart += nodeOrToken.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkParameterAccessibilityModifiers = function (parameterList, parameter, parameterFullStart) {
            if (parameter.modifiers.childCount() > 0) {
                var modifiers = parameter.modifiers;
                var modifierFullStart = parameterFullStart + TypeScript.Syntax.childOffset(parameter, modifiers);

                for (var i = 0, n = modifiers.childCount(); i < n; i++) {
                    var modifier = modifiers.childAt(i);

                    if (this.checkParameterAccessibilityModifier(parameterList, modifier, modifierFullStart, i)) {
                        return true;
                    }

                    modifierFullStart += modifier.fullWidth();
                }
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkParameterAccessibilityModifier = function (parameterList, modifier, modifierFullStart, modifierIndex) {
            if (modifier.tokenKind !== 57 /* PublicKeyword */ && modifier.tokenKind !== 55 /* PrivateKeyword */) {
                this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode._0_modifier_cannot_appear_on_a_parameter, [modifier.text()]);
                return true;
            } else {
                if (modifierIndex > 0) {
                    this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode.Accessibility_modifier_already_seen);
                    return true;
                }

                if (!this.inAmbientDeclaration && this.currentConstructor && !this.currentConstructor.block && this.currentConstructor.callSignature.parameterList === parameterList) {
                    this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode.Parameter_property_declarations_cannot_be_used_in_a_constructor_overload);
                    return true;
                } else if (this.inAmbientDeclaration || this.currentConstructor === null || this.currentConstructor.callSignature.parameterList !== parameterList) {
                    this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode.Parameter_property_declarations_can_only_be_used_in_a_non_ambient_constructor_declaration);
                    return true;
                }
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkForTrailingSeparator = function (parent, list) {
            // If we have at least one child, and we have an even number of children, then that
            // means we have an illegal trailing separator.
            if (list.childCount() === 0 || list.childCount() % 2 === 1) {
                return false;
            }

            var currentElementFullStart = this.childFullStart(parent, list);

            for (var i = 0, n = list.childCount(); i < n; i++) {
                var child = list.childAt(i);
                if (i === n - 1) {
                    this.pushDiagnostic1(currentElementFullStart, child, TypeScript.DiagnosticCode.Trailing_separator_not_allowed);
                }

                currentElementFullStart += child.fullWidth();
            }

            return true;
        };

        GrammarCheckerWalker.prototype.checkForAtLeastOneElement = function (parent, list, expected) {
            if (list.childCount() > 0) {
                return false;
            }

            var listFullStart = this.childFullStart(parent, list);
            var tokenAtStart = this.syntaxTree.sourceUnit().findToken(listFullStart);

            this.pushDiagnostic1(listFullStart, tokenAtStart.token(), TypeScript.DiagnosticCode.Unexpected_token_0_expected, [expected]);

            return true;
        };

        GrammarCheckerWalker.prototype.visitParameterList = function (node) {
            if (this.checkParameterListAcessibilityModifiers(node) || this.checkParameterListOrder(node) || this.checkForTrailingSeparator(node, node.parameters)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitParameterList.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitHeritageClause = function (node) {
            if (this.checkForTrailingSeparator(node, node.typeNames) || this.checkForAtLeastOneElement(node, node.typeNames, TypeScript.getLocalizedText(TypeScript.DiagnosticCode.type_name, null))) {
                this.skip(node);
                return;
            }

            _super.prototype.visitHeritageClause.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitArgumentList = function (node) {
            if (this.checkForTrailingSeparator(node, node.arguments)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitArgumentList.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitVariableDeclaration = function (node) {
            if (this.checkForTrailingSeparator(node, node.variableDeclarators) || this.checkForAtLeastOneElement(node, node.variableDeclarators, TypeScript.getLocalizedText(TypeScript.DiagnosticCode.identifier, null))) {
                this.skip(node);
                return;
            }

            _super.prototype.visitVariableDeclaration.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitTypeArgumentList = function (node) {
            if (this.checkForTrailingSeparator(node, node.typeArguments) || this.checkForAtLeastOneElement(node, node.typeArguments, TypeScript.getLocalizedText(TypeScript.DiagnosticCode.identifier, null))) {
                this.skip(node);
                return;
            }

            _super.prototype.visitTypeArgumentList.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitTypeParameterList = function (node) {
            if (this.checkForTrailingSeparator(node, node.typeParameters) || this.checkForAtLeastOneElement(node, node.typeParameters, TypeScript.getLocalizedText(TypeScript.DiagnosticCode.identifier, null))) {
                this.skip(node);
                return;
            }

            _super.prototype.visitTypeParameterList.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkIndexSignatureParameter = function (node) {
            var parameterFullStart = this.childFullStart(node, node.parameter);
            var parameter = node.parameter;

            if (parameter.dotDotDotToken) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Index_signatures_cannot_have_rest_parameters);
                return true;
            } else if (parameter.modifiers.childCount() > 0) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Index_signature_parameter_cannot_have_accessibility_modifiers);
                return true;
            } else if (parameter.questionToken) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Index_signature_parameter_cannot_have_a_question_mark);
                return true;
            } else if (parameter.equalsValueClause) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Index_signature_parameter_cannot_have_an_initializer);
                return true;
            } else if (!parameter.typeAnnotation) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Index_signature_parameter_must_have_a_type_annotation);
                return true;
            } else if (parameter.typeAnnotation.type.kind() !== 69 /* StringKeyword */ && parameter.typeAnnotation.type.kind() !== 67 /* NumberKeyword */) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.Index_signature_parameter_type_must_be_string_or_number);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitIndexSignature = function (node) {
            if (this.checkIndexSignatureParameter(node)) {
                this.skip(node);
                return;
            }

            if (!node.typeAnnotation) {
                this.pushDiagnostic1(this.position(), node, TypeScript.DiagnosticCode.Index_signature_must_have_a_type_annotation);
                this.skip(node);
                return;
            }

            _super.prototype.visitIndexSignature.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkClassDeclarationHeritageClauses = function (node) {
            var heritageClauseFullStart = this.childFullStart(node, node.heritageClauses);

            var seenExtendsClause = false;
            var seenImplementsClause = false;

            for (var i = 0, n = node.heritageClauses.childCount(); i < n; i++) {
                TypeScript.Debug.assert(i <= 2);
                var heritageClause = node.heritageClauses.childAt(i);

                if (heritageClause.extendsOrImplementsKeyword.tokenKind === 48 /* ExtendsKeyword */) {
                    if (seenExtendsClause) {
                        this.pushDiagnostic1(heritageClauseFullStart, heritageClause, TypeScript.DiagnosticCode.extends_clause_already_seen);
                        return true;
                    }

                    if (seenImplementsClause) {
                        this.pushDiagnostic1(heritageClauseFullStart, heritageClause, TypeScript.DiagnosticCode.extends_clause_must_precede_implements_clause);
                        return true;
                    }

                    if (heritageClause.typeNames.nonSeparatorCount() > 1) {
                        this.pushDiagnostic1(heritageClauseFullStart, heritageClause, TypeScript.DiagnosticCode.Classes_can_only_extend_a_single_class);
                        return true;
                    }

                    seenExtendsClause = true;
                } else {
                    TypeScript.Debug.assert(heritageClause.extendsOrImplementsKeyword.tokenKind === 51 /* ImplementsKeyword */);
                    if (seenImplementsClause) {
                        this.pushDiagnostic1(heritageClauseFullStart, heritageClause, TypeScript.DiagnosticCode.implements_clause_already_seen);
                        return true;
                    }

                    seenImplementsClause = true;
                }

                heritageClauseFullStart += heritageClause.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkForDisallowedDeclareModifier = function (modifiers) {
            if (this.inAmbientDeclaration) {
                // If we're already in an ambient declaration, then 'declare' is not allowed.
                var declareToken = TypeScript.SyntaxUtilities.getToken(modifiers, 63 /* DeclareKeyword */);

                if (declareToken) {
                    this.pushDiagnostic1(this.childFullStart(modifiers, declareToken), declareToken, TypeScript.DiagnosticCode.declare_modifier_not_allowed_for_code_already_in_an_ambient_context);
                    return true;
                }
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkForRequiredDeclareModifier = function (moduleElement, typeKeyword, modifiers) {
            if (!this.inAmbientDeclaration && this.syntaxTree.isDeclaration()) {
                // We're at the top level in a declaration file, a 'declare' modifiers is required
                // on most module elements.
                if (!TypeScript.SyntaxUtilities.containsToken(modifiers, 63 /* DeclareKeyword */)) {
                    this.pushDiagnostic1(this.childFullStart(moduleElement, typeKeyword), typeKeyword.firstToken(), TypeScript.DiagnosticCode.declare_modifier_required_for_top_level_element);
                    return true;
                }
            }
        };

        GrammarCheckerWalker.prototype.checkFunctionOverloads = function (node, moduleElements) {
            if (!this.inAmbientDeclaration && !this.syntaxTree.isDeclaration()) {
                var moduleElementFullStart = this.childFullStart(node, moduleElements);

                var inFunctionOverloadChain = false;
                var functionOverloadChainName = null;

                for (var i = 0, n = moduleElements.childCount(); i < n; i++) {
                    var moduleElement = moduleElements.childAt(i);
                    var lastElement = i === (n - 1);

                    if (inFunctionOverloadChain) {
                        if (moduleElement.kind() !== 129 /* FunctionDeclaration */) {
                            this.pushDiagnostic1(moduleElementFullStart, moduleElement.firstToken(), TypeScript.DiagnosticCode.Function_implementation_expected);
                            return true;
                        }

                        var functionDeclaration = moduleElement;
                        if (functionDeclaration.identifier.valueText() !== functionOverloadChainName) {
                            var identifierFullStart = moduleElementFullStart + TypeScript.Syntax.childOffset(moduleElement, functionDeclaration.identifier);
                            this.pushDiagnostic1(identifierFullStart, functionDeclaration.identifier, TypeScript.DiagnosticCode.Function_overload_name_must_be_0, [functionOverloadChainName]);
                            return true;
                        }
                    }

                    if (moduleElement.kind() === 129 /* FunctionDeclaration */) {
                        functionDeclaration = moduleElement;
                        if (!TypeScript.SyntaxUtilities.containsToken(functionDeclaration.modifiers, 63 /* DeclareKeyword */)) {
                            inFunctionOverloadChain = functionDeclaration.block === null;
                            functionOverloadChainName = functionDeclaration.identifier.valueText();

                            if (inFunctionOverloadChain) {
                                if (lastElement) {
                                    this.pushDiagnostic1(moduleElementFullStart, moduleElement.firstToken(), TypeScript.DiagnosticCode.Function_implementation_expected);
                                    return true;
                                } else {
                                    // We're a function without a body, and there's another element
                                    // after us.  If it's another overload that doesn't have a body,
                                    // then report an error that we're missing an implementation here.
                                    var nextElement = moduleElements.childAt(i + 1);
                                    if (nextElement.kind() === 129 /* FunctionDeclaration */) {
                                        var nextFunction = nextElement;

                                        if (nextFunction.identifier.valueText() !== functionOverloadChainName && nextFunction.block === null) {
                                            var identifierFullStart = moduleElementFullStart + TypeScript.Syntax.childOffset(moduleElement, functionDeclaration.identifier);
                                            this.pushDiagnostic1(identifierFullStart, functionDeclaration.identifier, TypeScript.DiagnosticCode.Function_implementation_expected);
                                            return true;
                                        }
                                    }
                                }
                            }
                        } else {
                            inFunctionOverloadChain = false;
                            functionOverloadChainName = "";
                        }
                    }

                    moduleElementFullStart += moduleElement.fullWidth();
                }
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkClassOverloads = function (node) {
            if (!this.inAmbientDeclaration && !TypeScript.SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */)) {
                var classElementFullStart = this.childFullStart(node, node.classElements);

                var inFunctionOverloadChain = false;
                var inConstructorOverloadChain = false;

                var functionOverloadChainName = null;
                var isInStaticOverloadChain = null;
                var memberFunctionDeclaration = null;

                for (var i = 0, n = node.classElements.childCount(); i < n; i++) {
                    var classElement = node.classElements.childAt(i);
                    var lastElement = i === (n - 1);
                    var isStaticOverload = null;

                    if (inFunctionOverloadChain) {
                        if (classElement.kind() !== 135 /* MemberFunctionDeclaration */) {
                            this.pushDiagnostic1(classElementFullStart, classElement.firstToken(), TypeScript.DiagnosticCode.Function_implementation_expected);
                            return true;
                        }

                        memberFunctionDeclaration = classElement;
                        if (memberFunctionDeclaration.propertyName.valueText() !== functionOverloadChainName) {
                            var propertyNameFullStart = classElementFullStart + TypeScript.Syntax.childOffset(classElement, memberFunctionDeclaration.propertyName);
                            this.pushDiagnostic1(propertyNameFullStart, memberFunctionDeclaration.propertyName, TypeScript.DiagnosticCode.Function_overload_name_must_be_0, [functionOverloadChainName]);
                            return true;
                        }

                        isStaticOverload = TypeScript.SyntaxUtilities.containsToken(memberFunctionDeclaration.modifiers, 58 /* StaticKeyword */);
                        if (isStaticOverload !== isInStaticOverloadChain) {
                            var propertyNameFullStart = classElementFullStart + TypeScript.Syntax.childOffset(classElement, memberFunctionDeclaration.propertyName);
                            var diagnostic = isInStaticOverloadChain ? TypeScript.DiagnosticCode.Function_overload_must_be_static : TypeScript.DiagnosticCode.Function_overload_must_not_be_static;
                            this.pushDiagnostic1(propertyNameFullStart, memberFunctionDeclaration.propertyName, diagnostic, null);
                            return true;
                        }
                    } else if (inConstructorOverloadChain) {
                        if (classElement.kind() !== 137 /* ConstructorDeclaration */) {
                            this.pushDiagnostic1(classElementFullStart, classElement.firstToken(), TypeScript.DiagnosticCode.Constructor_implementation_expected);
                            return true;
                        }
                    }

                    if (classElement.kind() === 135 /* MemberFunctionDeclaration */) {
                        memberFunctionDeclaration = classElement;

                        inFunctionOverloadChain = memberFunctionDeclaration.block === null;
                        functionOverloadChainName = memberFunctionDeclaration.propertyName.valueText();
                        isInStaticOverloadChain = TypeScript.SyntaxUtilities.containsToken(memberFunctionDeclaration.modifiers, 58 /* StaticKeyword */);

                        if (inFunctionOverloadChain) {
                            if (lastElement) {
                                this.pushDiagnostic1(classElementFullStart, classElement.firstToken(), TypeScript.DiagnosticCode.Function_implementation_expected);
                                return true;
                            } else {
                                // We're a function without a body, and there's another element
                                // after us.  If it's another overload that doesn't have a body,
                                // then report an error that we're missing an implementation here.
                                var nextElement = node.classElements.childAt(i + 1);
                                if (nextElement.kind() === 135 /* MemberFunctionDeclaration */) {
                                    var nextMemberFunction = nextElement;

                                    if (nextMemberFunction.propertyName.valueText() !== functionOverloadChainName && nextMemberFunction.block === null) {
                                        var propertyNameFullStart = classElementFullStart + TypeScript.Syntax.childOffset(classElement, memberFunctionDeclaration.propertyName);
                                        this.pushDiagnostic1(propertyNameFullStart, memberFunctionDeclaration.propertyName, TypeScript.DiagnosticCode.Function_implementation_expected);
                                        return true;
                                    }
                                }
                            }
                        }
                    } else if (classElement.kind() === 137 /* ConstructorDeclaration */) {
                        var constructorDeclaration = classElement;

                        inConstructorOverloadChain = constructorDeclaration.block === null;
                        if (lastElement && inConstructorOverloadChain) {
                            this.pushDiagnostic1(classElementFullStart, classElement.firstToken(), TypeScript.DiagnosticCode.Constructor_implementation_expected);
                            return true;
                        }
                    }

                    classElementFullStart += classElement.fullWidth();
                }
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkForReservedName = function (parent, name, diagnosticKey) {
            var nameFullStart = this.childFullStart(parent, name);
            var token;
            var tokenFullStart;

            var current = name;
            while (current !== null) {
                if (current.kind() === 121 /* QualifiedName */) {
                    var qualifiedName = current;
                    token = qualifiedName.right;
                    tokenFullStart = nameFullStart + this.childFullStart(qualifiedName, token);
                    current = qualifiedName.left;
                } else {
                    TypeScript.Debug.assert(current.kind() === 11 /* IdentifierName */);
                    token = current;
                    tokenFullStart = nameFullStart;
                    current = null;
                }

                switch (token.valueText()) {
                    case "any":
                    case "number":
                    case "boolean":
                    case "string":
                    case "void":
                        this.pushDiagnostic(tokenFullStart + token.leadingTriviaWidth(), token.width(), diagnosticKey, [token.valueText()]);
                        return true;
                }
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitClassDeclaration = function (node) {
            if (this.checkForReservedName(node, node.identifier, TypeScript.DiagnosticCode.Class_name_cannot_be_0) || this.checkForDisallowedDeclareModifier(node.modifiers) || this.checkForRequiredDeclareModifier(node, node.classKeyword, node.modifiers) || this.checkModuleElementModifiers(node.modifiers) || this.checkClassDeclarationHeritageClauses(node) || this.checkClassOverloads(node)) {
                this.skip(node);
                return;
            }

            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = this.inAmbientDeclaration || this.syntaxTree.isDeclaration() || TypeScript.SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */);
            _super.prototype.visitClassDeclaration.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.checkInterfaceDeclarationHeritageClauses = function (node) {
            var heritageClauseFullStart = this.childFullStart(node, node.heritageClauses);

            var seenExtendsClause = false;

            for (var i = 0, n = node.heritageClauses.childCount(); i < n; i++) {
                TypeScript.Debug.assert(i <= 1);
                var heritageClause = node.heritageClauses.childAt(i);

                if (heritageClause.extendsOrImplementsKeyword.tokenKind === 48 /* ExtendsKeyword */) {
                    if (seenExtendsClause) {
                        this.pushDiagnostic1(heritageClauseFullStart, heritageClause, TypeScript.DiagnosticCode.extends_clause_already_seen);
                        return true;
                    }

                    seenExtendsClause = true;
                } else {
                    TypeScript.Debug.assert(heritageClause.extendsOrImplementsKeyword.tokenKind === 51 /* ImplementsKeyword */);
                    this.pushDiagnostic1(heritageClauseFullStart, heritageClause, TypeScript.DiagnosticCode.Interface_declaration_cannot_have_implements_clause);
                    return true;
                }

                heritageClauseFullStart += heritageClause.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkInterfaceModifiers = function (modifiers) {
            var modifierFullStart = this.position();

            for (var i = 0, n = modifiers.childCount(); i < n; i++) {
                var modifier = modifiers.childAt(i);
                if (modifier.tokenKind === 63 /* DeclareKeyword */) {
                    this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode.declare_modifier_cannot_appear_on_an_interface_declaration);
                    return true;
                }

                modifierFullStart += modifier.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitInterfaceDeclaration = function (node) {
            if (this.checkForReservedName(node, node.identifier, TypeScript.DiagnosticCode.Interface_name_cannot_be_0) || this.checkInterfaceModifiers(node.modifiers) || this.checkModuleElementModifiers(node.modifiers) || this.checkInterfaceDeclarationHeritageClauses(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitInterfaceDeclaration.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkClassElementModifiers = function (list) {
            var modifierFullStart = this.position();

            var seenAccessibilityModifier = false;
            var seenStaticModifier = false;

            for (var i = 0, n = list.childCount(); i < n; i++) {
                var modifier = list.childAt(i);
                if (modifier.tokenKind === 57 /* PublicKeyword */ || modifier.tokenKind === 55 /* PrivateKeyword */) {
                    if (seenAccessibilityModifier) {
                        this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode.Accessibility_modifier_already_seen);
                        return true;
                    }

                    if (seenStaticModifier) {
                        var previousToken = list.childAt(i - 1);
                        this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode._0_modifier_must_precede_1_modifier, [modifier.text(), previousToken.text()]);
                        return true;
                    }

                    seenAccessibilityModifier = true;
                } else if (modifier.tokenKind === 58 /* StaticKeyword */) {
                    if (seenStaticModifier) {
                        this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode._0_modifier_already_seen, [modifier.text()]);
                        return true;
                    }

                    seenStaticModifier = true;
                } else {
                    this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode._0_modifier_cannot_appear_on_a_class_element, [modifier.text()]);
                    return true;
                }

                modifierFullStart += modifier.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitMemberVariableDeclaration = function (node) {
            if (this.checkClassElementModifiers(node.modifiers)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitMemberVariableDeclaration.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitMemberFunctionDeclaration = function (node) {
            if (this.checkClassElementModifiers(node.modifiers)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitMemberFunctionDeclaration.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkGetAccessorParameter = function (node, getKeyword, parameterList) {
            var getKeywordFullStart = this.childFullStart(node, getKeyword);
            if (parameterList.parameters.childCount() !== 0) {
                this.pushDiagnostic1(getKeywordFullStart, getKeyword, TypeScript.DiagnosticCode.get_accessor_cannot_have_parameters);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitIndexMemberDeclaration = function (node) {
            if (this.checkIndexMemberModifiers(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitIndexMemberDeclaration.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkIndexMemberModifiers = function (node) {
            if (node.modifiers.childCount() > 0) {
                var modifierFullStart = this.childFullStart(node, node.modifiers);
                this.pushDiagnostic1(modifierFullStart, node.modifiers.childAt(0), TypeScript.DiagnosticCode.Modifiers_cannot_appear_here);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkEcmaScriptVersionIsAtLeast = function (parent, node, languageVersion, diagnosticKey) {
            if (this.syntaxTree.parseOptions().languageVersion() < languageVersion) {
                var nodeFullStart = this.childFullStart(parent, node);
                this.pushDiagnostic1(nodeFullStart, node, diagnosticKey);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitObjectLiteralExpression = function (node) {
            var savedInObjectLiteralExpression = this.inObjectLiteralExpression;
            this.inObjectLiteralExpression = true;
            _super.prototype.visitObjectLiteralExpression.call(this, node);
            this.inObjectLiteralExpression = savedInObjectLiteralExpression;
        };

        GrammarCheckerWalker.prototype.visitGetAccessor = function (node) {
            if (this.checkForAccessorDeclarationInAmbientContext(node) || this.checkEcmaScriptVersionIsAtLeast(node, node.getKeyword, 1 /* EcmaScript5 */, TypeScript.DiagnosticCode.Accessors_are_only_available_when_targeting_ECMAScript_5_and_higher) || this.checkForDisallowedModifiers(node, node.modifiers) || this.checkClassElementModifiers(node.modifiers) || this.checkGetAccessorParameter(node, node.getKeyword, node.parameterList)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitGetAccessor.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkForAccessorDeclarationInAmbientContext = function (accessor) {
            if (this.inAmbientDeclaration) {
                this.pushDiagnostic1(this.position(), accessor, TypeScript.DiagnosticCode.Accessors_are_not_allowed_in_ambient_contexts, null);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkSetAccessorParameter = function (node, setKeyword, parameterList) {
            var setKeywordFullStart = this.childFullStart(node, setKeyword);
            if (parameterList.parameters.childCount() !== 1) {
                this.pushDiagnostic1(setKeywordFullStart, setKeyword, TypeScript.DiagnosticCode.set_accessor_must_have_one_and_only_one_parameter);
                return true;
            }

            var parameterListFullStart = this.childFullStart(node, parameterList);
            var parameterFullStart = parameterListFullStart + TypeScript.Syntax.childOffset(parameterList, parameterList.openParenToken);
            var parameter = parameterList.parameters.childAt(0);

            if (parameter.questionToken) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.set_accessor_parameter_cannot_be_optional);
                return true;
            }

            if (parameter.equalsValueClause) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.set_accessor_parameter_cannot_have_an_initializer);
                return true;
            }

            if (parameter.dotDotDotToken) {
                this.pushDiagnostic1(parameterFullStart, parameter, TypeScript.DiagnosticCode.set_accessor_cannot_have_rest_parameter);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitSetAccessor = function (node) {
            if (this.checkForAccessorDeclarationInAmbientContext(node) || this.checkEcmaScriptVersionIsAtLeast(node, node.setKeyword, 1 /* EcmaScript5 */, TypeScript.DiagnosticCode.Accessors_are_only_available_when_targeting_ECMAScript_5_and_higher) || this.checkForDisallowedModifiers(node, node.modifiers) || this.checkClassElementModifiers(node.modifiers) || this.checkSetAccessorParameter(node, node.setKeyword, node.parameterList)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitSetAccessor.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitEnumDeclaration = function (node) {
            if (this.checkForReservedName(node, node.identifier, TypeScript.DiagnosticCode.Enum_name_cannot_be_0) || this.checkForDisallowedDeclareModifier(node.modifiers) || this.checkForRequiredDeclareModifier(node, node.enumKeyword, node.modifiers) || this.checkModuleElementModifiers(node.modifiers), this.checkEnumElements(node)) {
                this.skip(node);
                return;
            }

            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = this.inAmbientDeclaration || this.syntaxTree.isDeclaration() || TypeScript.SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */);
            _super.prototype.visitEnumDeclaration.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.checkEnumElements = function (node) {
            var enumElementFullStart = this.childFullStart(node, node.enumElements);

            var previousValueWasComputed = false;
            for (var i = 0, n = node.enumElements.childCount(); i < n; i++) {
                var child = node.enumElements.childAt(i);

                if (i % 2 === 0) {
                    var enumElement = child;

                    if (!enumElement.equalsValueClause && previousValueWasComputed) {
                        this.pushDiagnostic1(enumElementFullStart, enumElement, TypeScript.DiagnosticCode.Enum_member_must_have_initializer, null);
                        return true;
                    }

                    if (enumElement.equalsValueClause) {
                        var value = enumElement.equalsValueClause.value;
                        previousValueWasComputed = !TypeScript.Syntax.isIntegerLiteral(value);
                    }
                }

                enumElementFullStart += child.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitEnumElement = function (node) {
            if (this.inAmbientDeclaration && node.equalsValueClause) {
                var expression = node.equalsValueClause.value;
                if (!TypeScript.Syntax.isIntegerLiteral(expression)) {
                    this.pushDiagnostic1(this.childFullStart(node, node.equalsValueClause), node.equalsValueClause.firstToken(), TypeScript.DiagnosticCode.Ambient_enum_elements_can_only_have_integer_literal_initializers);
                    this.skip(node);
                    return;
                }
            }

            _super.prototype.visitEnumElement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitInvocationExpression = function (node) {
            if (node.expression.kind() === 50 /* SuperKeyword */ && node.argumentList.typeArgumentList !== null) {
                this.pushDiagnostic1(this.position(), node, TypeScript.DiagnosticCode.super_invocation_cannot_have_type_arguments);
            }

            _super.prototype.visitInvocationExpression.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkModuleElementModifiers = function (modifiers) {
            var modifierFullStart = this.position();
            var seenExportModifier = false;
            var seenDeclareModifier = false;

            for (var i = 0, n = modifiers.childCount(); i < n; i++) {
                var modifier = modifiers.childAt(i);
                if (modifier.tokenKind === 57 /* PublicKeyword */ || modifier.tokenKind === 55 /* PrivateKeyword */ || modifier.tokenKind === 58 /* StaticKeyword */) {
                    this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode._0_modifier_cannot_appear_on_a_module_element, [modifier.text()]);
                    return true;
                }

                if (modifier.tokenKind === 63 /* DeclareKeyword */) {
                    if (seenDeclareModifier) {
                        this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode.Accessibility_modifier_already_seen);
                        return;
                    }

                    seenDeclareModifier = true;
                } else if (modifier.tokenKind === 47 /* ExportKeyword */) {
                    if (seenExportModifier) {
                        this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode._0_modifier_already_seen, [modifier.text()]);
                        return;
                    }

                    if (seenDeclareModifier) {
                        this.pushDiagnostic1(modifierFullStart, modifier, TypeScript.DiagnosticCode._0_modifier_must_precede_1_modifier, [TypeScript.SyntaxFacts.getText(47 /* ExportKeyword */), TypeScript.SyntaxFacts.getText(63 /* DeclareKeyword */)]);
                        return;
                    }

                    seenExportModifier = true;
                }

                modifierFullStart += modifier.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkForDisallowedImportDeclaration = function (node) {
            var currentElementFullStart = this.childFullStart(node, node.moduleElements);

            for (var i = 0, n = node.moduleElements.childCount(); i < n; i++) {
                var child = node.moduleElements.childAt(i);
                if (child.kind() === 133 /* ImportDeclaration */) {
                    var importDeclaration = child;
                    if (importDeclaration.moduleReference.kind() === 245 /* ExternalModuleReference */) {
                        if (node.stringLiteral === null) {
                            this.pushDiagnostic1(currentElementFullStart, importDeclaration, TypeScript.DiagnosticCode.Import_declarations_in_an_internal_module_cannot_reference_an_external_module, null);
                        }
                    }
                }

                currentElementFullStart += child.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkForDisallowedDeclareModifierOnImportDeclaration = function (modifiers) {
            var declareToken = TypeScript.SyntaxUtilities.getToken(modifiers, 63 /* DeclareKeyword */);

            if (declareToken) {
                this.pushDiagnostic1(this.childFullStart(modifiers, declareToken), declareToken, TypeScript.DiagnosticCode.declare_modifier_not_allowed_on_import_declaration);
                return true;
            }
        };

        GrammarCheckerWalker.prototype.visitImportDeclaration = function (node) {
            if (this.checkForDisallowedDeclareModifierOnImportDeclaration(node.modifiers) || this.checkModuleElementModifiers(node.modifiers)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitImportDeclaration.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitModuleDeclaration = function (node) {
            if (this.checkForReservedName(node, node.name, TypeScript.DiagnosticCode.Module_name_cannot_be_0) || this.checkForDisallowedDeclareModifier(node.modifiers) || this.checkForRequiredDeclareModifier(node, node.moduleKeyword, node.modifiers) || this.checkModuleElementModifiers(node.modifiers) || this.checkForDisallowedImportDeclaration(node) || this.checkForDisallowedExports(node, node.moduleElements) || this.checkForMultipleExportAssignments(node, node.moduleElements)) {
                this.skip(node);
                return;
            }

            if (!TypeScript.SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */) && this.checkFunctionOverloads(node, node.moduleElements)) {
                this.skip(node);
                return;
            }

            if (node.stringLiteral) {
                if (!this.inAmbientDeclaration && !TypeScript.SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */)) {
                    var stringLiteralFullStart = this.childFullStart(node, node.stringLiteral);
                    this.pushDiagnostic1(stringLiteralFullStart, node.stringLiteral, TypeScript.DiagnosticCode.Only_ambient_modules_can_use_quoted_names);
                    this.skip(node);
                    return;
                }
            }

            if (!node.stringLiteral && this.checkForDisallowedExportAssignment(node)) {
                this.skip(node);
                return;
            }

            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = this.inAmbientDeclaration || this.syntaxTree.isDeclaration() || TypeScript.SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */);
            _super.prototype.visitModuleDeclaration.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.checkForDisallowedExports = function (node, moduleElements) {
            var seenExportedElement = false;
            for (var i = 0, n = moduleElements.childCount(); i < n; i++) {
                var child = moduleElements.childAt(i);

                if (TypeScript.SyntaxUtilities.hasExportKeyword(child)) {
                    seenExportedElement = true;
                    break;
                }
            }

            var moduleElementFullStart = this.childFullStart(node, moduleElements);
            if (seenExportedElement) {
                for (var i = 0, n = moduleElements.childCount(); i < n; i++) {
                    var child = moduleElements.childAt(i);

                    if (child.kind() === 134 /* ExportAssignment */) {
                        this.pushDiagnostic1(moduleElementFullStart, child, TypeScript.DiagnosticCode.Export_assignment_not_allowed_in_module_with_exported_element);
                        return true;
                    }

                    moduleElementFullStart += child.fullWidth();
                }
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkForMultipleExportAssignments = function (node, moduleElements) {
            var moduleElementFullStart = this.childFullStart(node, moduleElements);
            var seenExportAssignment = false;
            var errorFound = false;
            for (var i = 0, n = moduleElements.childCount(); i < n; i++) {
                var child = moduleElements.childAt(i);
                if (child.kind() === 134 /* ExportAssignment */) {
                    if (seenExportAssignment) {
                        this.pushDiagnostic1(moduleElementFullStart, child, TypeScript.DiagnosticCode.Module_cannot_have_multiple_export_assignments);
                        errorFound = true;
                    }
                    seenExportAssignment = true;
                }

                moduleElementFullStart += child.fullWidth();
            }

            return errorFound;
        };

        GrammarCheckerWalker.prototype.checkForDisallowedExportAssignment = function (node) {
            var moduleElementFullStart = this.childFullStart(node, node.moduleElements);

            for (var i = 0, n = node.moduleElements.childCount(); i < n; i++) {
                var child = node.moduleElements.childAt(i);

                if (child.kind() === 134 /* ExportAssignment */) {
                    this.pushDiagnostic1(moduleElementFullStart, child, TypeScript.DiagnosticCode.Export_assignment_cannot_be_used_in_internal_modules);

                    return true;
                }

                moduleElementFullStart += child.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitBlock = function (node) {
            if (this.inAmbientDeclaration || this.syntaxTree.isDeclaration()) {
                this.pushDiagnostic1(this.position(), node.firstToken(), TypeScript.DiagnosticCode.Implementations_are_not_allowed_in_ambient_contexts);
                this.skip(node);
                return;
            }

            if (this.checkFunctionOverloads(node, node.statements)) {
                this.skip(node);
                return;
            }

            var savedInBlock = this.inBlock;
            this.inBlock = true;
            _super.prototype.visitBlock.call(this, node);
            this.inBlock = savedInBlock;
        };

        GrammarCheckerWalker.prototype.checkForStatementInAmbientContxt = function (node) {
            if (this.inAmbientDeclaration || this.syntaxTree.isDeclaration()) {
                this.pushDiagnostic1(this.position(), node.firstToken(), TypeScript.DiagnosticCode.Statements_are_not_allowed_in_ambient_contexts);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitBreakStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitBreakStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitContinueStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitContinueStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitDebuggerStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitDebuggerStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitDoStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitDoStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitEmptyStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitEmptyStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitExpressionStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitExpressionStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitForInStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node) || this.checkForInStatementVariableDeclaration(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitForInStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkForInStatementVariableDeclaration = function (node) {
            // The parser accepts a Variable Declaration in a ForInStatement, but the grammar only
            // allows a very restricted form.  Specifically, there must be only a single Variable
            // Declarator in the Declaration.
            if (node.variableDeclaration && node.variableDeclaration.variableDeclarators.nonSeparatorCount() > 1) {
                var variableDeclarationFullStart = this.childFullStart(node, node.variableDeclaration);

                this.pushDiagnostic1(variableDeclarationFullStart, node.variableDeclaration, TypeScript.DiagnosticCode.Only_a_single_variable_declaration_is_allowed_in_a_for_in_statement);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitForStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitForStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitIfStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitIfStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitLabeledStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitLabeledStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitReturnStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitReturnStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitSwitchStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitSwitchStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitThrowStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitThrowStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitTryStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitTryStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitWhileStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitWhileStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitWithStatement = function (node) {
            if (this.checkForStatementInAmbientContxt(node)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitWithStatement.call(this, node);
        };

        GrammarCheckerWalker.prototype.checkForDisallowedModifiers = function (parent, modifiers) {
            if (this.inBlock || this.inObjectLiteralExpression) {
                if (modifiers.childCount() > 0) {
                    var modifierFullStart = this.childFullStart(parent, modifiers);
                    this.pushDiagnostic1(modifierFullStart, modifiers.childAt(0), TypeScript.DiagnosticCode.Modifiers_cannot_appear_here);
                    return true;
                }
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitFunctionDeclaration = function (node) {
            if (this.checkForDisallowedDeclareModifier(node.modifiers) || this.checkForDisallowedModifiers(node, node.modifiers) || this.checkForRequiredDeclareModifier(node, node.functionKeyword, node.modifiers) || this.checkModuleElementModifiers(node.modifiers)) {
                this.skip(node);
                return;
            }

            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = this.inAmbientDeclaration || this.syntaxTree.isDeclaration() || TypeScript.SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */);
            _super.prototype.visitFunctionDeclaration.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.visitVariableStatement = function (node) {
            if (this.checkForDisallowedDeclareModifier(node.modifiers) || this.checkForDisallowedModifiers(node, node.modifiers) || this.checkForRequiredDeclareModifier(node, node.variableDeclaration, node.modifiers) || this.checkModuleElementModifiers(node.modifiers)) {
                this.skip(node);
                return;
            }

            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = this.inAmbientDeclaration || this.syntaxTree.isDeclaration() || TypeScript.SyntaxUtilities.containsToken(node.modifiers, 63 /* DeclareKeyword */);
            _super.prototype.visitVariableStatement.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.checkListSeparators = function (parent, list, kind) {
            var currentElementFullStart = this.childFullStart(parent, list);

            for (var i = 0, n = list.childCount(); i < n; i++) {
                var child = list.childAt(i);
                if (i % 2 === 1 && child.kind() !== kind) {
                    this.pushDiagnostic1(currentElementFullStart, child, TypeScript.DiagnosticCode._0_expected, [TypeScript.SyntaxFacts.getText(kind)]);
                }

                currentElementFullStart += child.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitObjectType = function (node) {
            if (this.checkListSeparators(node, node.typeMembers, 78 /* SemicolonToken */)) {
                this.skip(node);
                return;
            }

            // All code in an object type is implicitly ambient. (i.e. parameters can't have initializer, etc.)
            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = true;
            _super.prototype.visitObjectType.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.visitArrayType = function (node) {
            // All code in an object type is implicitly ambient. (i.e. parameters can't have initializer, etc.)
            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = true;
            _super.prototype.visitArrayType.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.visitFunctionType = function (node) {
            // All code in an object type is implicitly ambient. (i.e. parameters can't have initializer, etc.)
            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = true;
            _super.prototype.visitFunctionType.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.visitConstructorType = function (node) {
            // All code in an object type is implicitly ambient. (i.e. parameters can't have initializer, etc.)
            var savedInAmbientDeclaration = this.inAmbientDeclaration;
            this.inAmbientDeclaration = true;
            _super.prototype.visitConstructorType.call(this, node);
            this.inAmbientDeclaration = savedInAmbientDeclaration;
        };

        GrammarCheckerWalker.prototype.visitVariableDeclarator = function (node) {
            if (this.inAmbientDeclaration && node.equalsValueClause) {
                this.pushDiagnostic1(this.childFullStart(node, node.equalsValueClause), node.equalsValueClause.firstToken(), TypeScript.DiagnosticCode.Initializers_are_not_allowed_in_ambient_contexts);
                this.skip(node);
                return;
            }

            _super.prototype.visitVariableDeclarator.call(this, node);
        };

        GrammarCheckerWalker.prototype.visitConstructorDeclaration = function (node) {
            if (this.checkClassElementModifiers(node.modifiers) || this.checkConstructorModifiers(node.modifiers) || this.checkConstructorTypeParameterList(node) || this.checkConstructorTypeAnnotation(node)) {
                this.skip(node);
                return;
            }

            var savedCurrentConstructor = this.currentConstructor;
            this.currentConstructor = node;
            _super.prototype.visitConstructorDeclaration.call(this, node);
            this.currentConstructor = savedCurrentConstructor;
        };

        GrammarCheckerWalker.prototype.checkConstructorModifiers = function (modifiers) {
            var currentElementFullStart = this.position();

            for (var i = 0, n = modifiers.childCount(); i < n; i++) {
                var child = modifiers.childAt(i);
                if (child.kind() !== 57 /* PublicKeyword */) {
                    this.pushDiagnostic1(currentElementFullStart, child, TypeScript.DiagnosticCode._0_modifier_cannot_appear_on_a_constructor_declaration, [TypeScript.SyntaxFacts.getText(child.kind())]);
                    return true;
                }

                currentElementFullStart += child.fullWidth();
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkConstructorTypeParameterList = function (node) {
            var currentElementFullStart = this.position();

            if (node.callSignature.typeParameterList) {
                var callSignatureFullStart = this.childFullStart(node, node.callSignature);
                var typeParameterListFullStart = callSignatureFullStart + TypeScript.Syntax.childOffset(node.callSignature, node.callSignature.typeAnnotation);
                this.pushDiagnostic1(callSignatureFullStart, node.callSignature.typeParameterList, TypeScript.DiagnosticCode.Type_parameters_cannot_appear_on_a_constructor_declaration);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.checkConstructorTypeAnnotation = function (node) {
            var currentElementFullStart = this.position();

            if (node.callSignature.typeAnnotation) {
                var callSignatureFullStart = this.childFullStart(node, node.callSignature);
                var typeAnnotationFullStart = callSignatureFullStart + TypeScript.Syntax.childOffset(node.callSignature, node.callSignature.typeAnnotation);
                this.pushDiagnostic1(typeAnnotationFullStart, node.callSignature.typeAnnotation, TypeScript.DiagnosticCode.Type_annotation_cannot_appear_on_a_constructor_declaration);
                return true;
            }

            return false;
        };

        GrammarCheckerWalker.prototype.visitSourceUnit = function (node) {
            if (this.checkFunctionOverloads(node, node.moduleElements) || this.checkForDisallowedExports(node, node.moduleElements) || this.checkForMultipleExportAssignments(node, node.moduleElements)) {
                this.skip(node);
                return;
            }

            _super.prototype.visitSourceUnit.call(this, node);
        };
        return GrammarCheckerWalker;
    })(TypeScript.PositionTrackingWalker);
})(TypeScript || (TypeScript = {}));
