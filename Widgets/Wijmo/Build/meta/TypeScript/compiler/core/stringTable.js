///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    (function (Collections) {
        Collections.DefaultStringTableCapacity = 256;

        var StringTableEntry = (function () {
            function StringTableEntry(Text, HashCode, Next) {
                this.Text = Text;
                this.HashCode = HashCode;
                this.Next = Next;
            }
            return StringTableEntry;
        })();

        // A table of interned strings.  Faster and better than an arbitrary hashtable for the needs of the
        // scanner. Specifically, the scanner operates over a sliding window of characters, with a start
        // and end pointer for the current lexeme.  The scanner then wants to get the *interned* string
        // represented by that subsection.
        //
        // Importantly, if the string is already interned, then it wants ask "is the string represented by
        // this section of a char array contained within the table" in a non-allocating fashion.  i.e. if
        // you have "[' ', 'p', 'u', 'b', 'l', 'i', 'c', ' ']" and you ask to get the string represented by
        //  range [1, 7), then this table will return "public" without any allocations if that value was
        // already in the table.
        //
        // Of course, if the value is not in the table then there will be an initial cost to allocate the
        // string and the bucket for the table.  However, that is only incurred the first time each unique
        // string is added.
        var StringTable = (function () {
            function StringTable(capacity) {
                this.count = 0;
                var size = TypeScript.Hash.getPrime(capacity);
                this.entries = TypeScript.ArrayUtilities.createArray(size, null);
            }
            StringTable.prototype.addCharArray = function (key, start, len) {
                // Compute the hash for this key.  Also ensure that it fits within 31 bits  (so that it
                // stays a non-heap integer, and so we can index into the array safely).
                var hashCode = TypeScript.Hash.computeSimple31BitCharArrayHashCode(key, start, len) & 0x7FFFFFFF;

                // Debug.assert(hashCode > 0);
                // First see if we already have the string represented by "key[start, start + len)" already
                // present in this table.  If we do, just return that string.  Do this without any
                // allocations
                var entry = this.findCharArrayEntry(key, start, len, hashCode);
                if (entry !== null) {
                    return entry.Text;
                }

                // We don't have an entry for that string in our table.  Convert that
                var slice = key.slice(start, start + len);
                return this.addEntry(TypeScript.StringUtilities.fromCharCodeArray(slice), hashCode);
            };

            StringTable.prototype.findCharArrayEntry = function (key, start, len, hashCode) {
                for (var e = this.entries[hashCode % this.entries.length]; e !== null; e = e.Next) {
                    if (e.HashCode === hashCode && StringTable.textCharArrayEquals(e.Text, key, start, len)) {
                        return e;
                    }
                }

                return null;
            };

            StringTable.prototype.addEntry = function (text, hashCode) {
                var index = hashCode % this.entries.length;

                var e = new StringTableEntry(text, hashCode, this.entries[index]);

                this.entries[index] = e;

                // We grow when our load factor equals 1.  I tried different load factors (like .75 and
                // .5), however they seemed to have no effect on running time.  With a load factor of 1
                // we seem to get about 80% slot fill rate with an average of around 1.25 table entries
                // per slot.
                if (this.count === this.entries.length) {
                    this.grow();
                }

                this.count++;
                return e.Text;
            };

            //private dumpStats() {
            //    var standardOut = Environment.standardOut;
            //    standardOut.WriteLine("----------------------")
            //    standardOut.WriteLine("String table stats");
            //    standardOut.WriteLine("Count            : " + this.count);
            //    standardOut.WriteLine("Entries Length   : " + this.entries.length);
            //    var longestSlot = 0;
            //    var occupiedSlots = 0;
            //    for (var i = 0; i < this.entries.length; i++) {
            //        if (this.entries[i] !== null) {
            //            occupiedSlots++;
            //            var current = this.entries[i];
            //            var slotCount = 0;
            //            while (current !== null) {
            //                slotCount++;
            //                current = current.Next;
            //            }
            //            longestSlot = MathPrototype.max(longestSlot, slotCount);
            //        }
            //    }
            //    standardOut.WriteLine("Occupied slots   : " + occupiedSlots);
            //    standardOut.WriteLine("Longest  slot    : " + longestSlot);
            //    standardOut.WriteLine("Avg Length/Slot  : " + (this.count / occupiedSlots));
            //    standardOut.WriteLine("----------------------");
            //}
            StringTable.prototype.grow = function () {
                // this.dumpStats();
                var newSize = TypeScript.Hash.expandPrime(this.entries.length);

                var oldEntries = this.entries;
                var newEntries = TypeScript.ArrayUtilities.createArray(newSize, null);

                this.entries = newEntries;

                for (var i = 0; i < oldEntries.length; i++) {
                    var e = oldEntries[i];
                    while (e !== null) {
                        var newIndex = e.HashCode % newSize;
                        var tmp = e.Next;
                        e.Next = newEntries[newIndex];
                        newEntries[newIndex] = e;
                        e = tmp;
                    }
                }
                // this.dumpStats();
            };

            StringTable.textCharArrayEquals = function (text, array, start, length) {
                if (text.length !== length) {
                    return false;
                }

                var s = start;
                for (var i = 0; i < length; i++) {
                    if (text.charCodeAt(i) !== array[s]) {
                        return false;
                    }

                    s++;
                }

                return true;
            };
            return StringTable;
        })();
        Collections.StringTable = StringTable;

        Collections.DefaultStringTable = new StringTable(Collections.DefaultStringTableCapacity);
    })(TypeScript.Collections || (TypeScript.Collections = {}));
    var Collections = TypeScript.Collections;
})(TypeScript || (TypeScript = {}));
