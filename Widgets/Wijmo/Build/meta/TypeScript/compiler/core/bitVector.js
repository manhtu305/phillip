///<reference path='references.ts'/>
var TypeScript;
(function (TypeScript) {
    (function (BitVector) {
        var pool = [];
        var Constants;
        (function (Constants) {
            // We only use up to 30 bits in a number.  That way the encoded value can always fit
            // within an int so that the underlying engine doesn't use a 64bit float here.
            Constants[Constants["MaxBitsPerEncodedNumber"] = 30] = "MaxBitsPerEncodedNumber";
            Constants[Constants["BitsPerEncodedBiStateValue"] = 1] = "BitsPerEncodedBiStateValue";

            // For a tri state vector we need 2 bits per encoded value.  00 for 'undefined',
            // '01' for 'false' and '10' for true.
            Constants[Constants["BitsPerEncodedTriStateValue"] = 2] = "BitsPerEncodedTriStateValue";

            Constants[Constants["BiStateEncodedTrue"] = 1] = "BiStateEncodedTrue";
            Constants[Constants["BiStateClearBitsMask"] = 1] = "BiStateClearBitsMask";

            Constants[Constants["TriStateEncodedFalse"] = 1] = "TriStateEncodedFalse";
            Constants[Constants["TriStateEncodedTrue"] = 2] = "TriStateEncodedTrue";
            Constants[Constants["TriStateClearBitsMask"] = 3] = "TriStateClearBitsMask";
        })(Constants || (Constants = {}));

        var BitVectorImpl = (function () {
            function BitVectorImpl(allowUndefinedValues) {
                this.allowUndefinedValues = allowUndefinedValues;
                this.isReleased = false;
                this.bits = [];
            }
            BitVectorImpl.prototype.computeTriStateArrayIndex = function (index) {
                // The number of values that can be encoded in a single number.
                var encodedValuesPerNumber = 30 /* MaxBitsPerEncodedNumber */ / 2 /* BitsPerEncodedTriStateValue */;

                return (index / encodedValuesPerNumber) >>> 0;
            };

            BitVectorImpl.prototype.computeBiStateArrayIndex = function (index) {
                // The number of values that can be encoded in a single number.
                var encodedValuesPerNumber = 30 /* MaxBitsPerEncodedNumber */ / 1 /* BitsPerEncodedBiStateValue */;

                return (index / encodedValuesPerNumber) >>> 0;
            };

            BitVectorImpl.prototype.computeTriStateEncodedValueIndex = function (index) {
                // The number of values that can be encoded in a single number.
                var encodedValuesPerNumber = 30 /* MaxBitsPerEncodedNumber */ / 2 /* BitsPerEncodedTriStateValue */;

                return (index % encodedValuesPerNumber) * 2 /* BitsPerEncodedTriStateValue */;
            };

            BitVectorImpl.prototype.computeBiStateEncodedValueIndex = function (index) {
                // The number of values that can be encoded in a single number.
                var encodedValuesPerNumber = 30 /* MaxBitsPerEncodedNumber */ / 1 /* BitsPerEncodedBiStateValue */;

                return (index % encodedValuesPerNumber) * 1 /* BitsPerEncodedBiStateValue */;
            };

            BitVectorImpl.prototype.valueAt = function (index) {
                TypeScript.Debug.assert(!this.isReleased, "Should not use a released bitvector");
                if (this.allowUndefinedValues) {
                    // tri-state bit vector.  2 bits per value.
                    var arrayIndex = this.computeTriStateArrayIndex(index);
                    var encoded = this.bits[arrayIndex];
                    if (encoded === undefined) {
                        // We don't even have an encoded value at this array position.  That's
                        // equivalent to 'undefined' for a tri-state vector.
                        return undefined;
                    }

                    var bitIndex = this.computeTriStateEncodedValueIndex(index);
                    if (encoded & (2 /* TriStateEncodedTrue */ << bitIndex)) {
                        return true;
                    } else if (encoded & (1 /* TriStateEncodedFalse */ << bitIndex)) {
                        return false;
                    } else {
                        return undefined;
                    }
                } else {
                    // Normal bitvector.  One bit per value stored.
                    var arrayIndex = this.computeBiStateArrayIndex(index);
                    var encoded = this.bits[arrayIndex];
                    if (encoded === undefined) {
                        // We don't even have an encoded value at this array position.  That's
                        // equivalent to 'false' for a bi-state vector.
                        return false;
                    }

                    // If we don't support undefined values, then we use one bit per value. Just
                    // index to that bit and see if it's set or not.
                    var bitIndex = this.computeBiStateEncodedValueIndex(index);
                    if (encoded & (1 /* BiStateEncodedTrue */ << bitIndex)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };

            BitVectorImpl.prototype.setValueAt = function (index, value) {
                TypeScript.Debug.assert(!this.isReleased, "Should not use a released bitvector");
                if (this.allowUndefinedValues) {
                    TypeScript.Debug.assert(value === true || value === false || value === undefined, "value must only be true, false or undefined.");

                    var arrayIndex = this.computeTriStateArrayIndex(index);
                    var encoded = this.bits[arrayIndex];
                    if (encoded === undefined) {
                        if (value === undefined) {
                            // They're trying to set a bit to undefined that we don't even have an entry
                            // for.  We can bail out quickly here.
                            return;
                        }

                        encoded = 0;
                    }

                    // First, we clear out any bits set at the appropriate index.
                    var bitIndex = this.computeTriStateEncodedValueIndex(index);

                    // Create a mask similar to: 11111111100111111
                    // i.e. all 1's except for 2 zeroes in the appropriate place.
                    var clearMask = ~(3 /* TriStateClearBitsMask */ << bitIndex);
                    encoded = encoded & clearMask;

                    if (value === true) {
                        encoded = encoded | (2 /* TriStateEncodedTrue */ << bitIndex);
                    } else if (value === false) {
                        encoded = encoded | (1 /* TriStateEncodedFalse */ << bitIndex);
                    }

                    // else {
                    //   They're setting the value to 'undefined'.  We already cleared the value
                    //   so there's nothing we need to do here.
                    // }
                    this.bits[arrayIndex] = encoded;
                } else {
                    TypeScript.Debug.assert(value === true || value === false, "value must only be true or false.");

                    var arrayIndex = this.computeBiStateArrayIndex(index);
                    var encoded = this.bits[arrayIndex];
                    if (encoded === undefined) {
                        if (value === false) {
                            // They're trying to set a bit to false that we don't even have an entry
                            // for.  We can bail out quickly here.
                            return;
                        }

                        encoded = 0;
                    }

                    var bitIndex = this.computeBiStateEncodedValueIndex(index);

                    // First, clear out the bit at this location.
                    encoded = encoded & ~(1 /* BiStateClearBitsMask */ << bitIndex);

                    if (value) {
                        encoded = encoded | (1 /* BiStateEncodedTrue */ << bitIndex);
                    }

                    // else {
                    //   They're setting the value to 'false'.  We already cleared the value
                    //   so there's nothing we need to do here.
                    // }
                    this.bits[arrayIndex] = encoded;
                }
            };

            BitVectorImpl.prototype.release = function () {
                TypeScript.Debug.assert(!this.isReleased, "Should not use a released bitvector");
                this.isReleased = true;
                this.bits.length = 0;
                pool.push(this);
            };
            return BitVectorImpl;
        })();

        function getBitVector(allowUndefinedValues) {
            if (pool.length === 0) {
                return new BitVectorImpl(allowUndefinedValues);
            }

            var vector = pool.pop();
            vector.isReleased = false;
            vector.allowUndefinedValues = allowUndefinedValues;

            return vector;
        }
        BitVector.getBitVector = getBitVector;
    })(TypeScript.BitVector || (TypeScript.BitVector = {}));
    var BitVector = TypeScript.BitVector;
})(TypeScript || (TypeScript = {}));
