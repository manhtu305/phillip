///<reference path='references.ts' />
var TypeScript;
(function (TypeScript) {
    (function (BitMatrix) {
        var pool = [];

        var BitMatrixImpl = (function () {
            function BitMatrixImpl(allowUndefinedValues) {
                this.allowUndefinedValues = allowUndefinedValues;
                this.isReleased = false;
                this.vectors = [];
            }
            BitMatrixImpl.prototype.valueAt = function (x, y) {
                TypeScript.Debug.assert(!this.isReleased, "Should not use a released bitvector");
                var vector = this.vectors[x];
                if (!vector) {
                    return this.allowUndefinedValues ? undefined : false;
                }

                return vector.valueAt(y);
            };

            BitMatrixImpl.prototype.setValueAt = function (x, y, value) {
                TypeScript.Debug.assert(!this.isReleased, "Should not use a released bitvector");
                var vector = this.vectors[x];
                if (!vector) {
                    if (value === undefined) {
                        // If they're storing an undefined value, and we don't even have a vector,
                        // then we can short circuit early here.
                        return;
                    }

                    vector = TypeScript.BitVector.getBitVector(this.allowUndefinedValues);
                    this.vectors[x] = vector;
                }

                vector.setValueAt(y, value);
            };

            BitMatrixImpl.prototype.release = function () {
                TypeScript.Debug.assert(!this.isReleased, "Should not use a released bitvector");
                this.isReleased = true;

                for (var name in this.vectors) {
                    if (this.vectors.hasOwnProperty(name)) {
                        var vector = this.vectors[name];
                        vector.release();
                    }
                }

                this.vectors.length = 0;
                pool.push(this);
            };
            return BitMatrixImpl;
        })();

        function getBitMatrix(allowUndefinedValues) {
            if (pool.length === 0) {
                return new BitMatrixImpl(allowUndefinedValues);
            }

            var matrix = pool.pop();
            matrix.isReleased = false;
            matrix.allowUndefinedValues = allowUndefinedValues;

            return matrix;
        }
        BitMatrix.getBitMatrix = getBitMatrix;
    })(TypeScript.BitMatrix || (TypeScript.BitMatrix = {}));
    var BitMatrix = TypeScript.BitMatrix;
})(TypeScript || (TypeScript = {}));
