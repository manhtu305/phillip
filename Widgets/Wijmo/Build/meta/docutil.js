/// <reference path="../../External/declarations/node.d.ts" />
/** XML tools */
(function (xml) {
    var escapes = {
        "&": "&amp;",
        ">": "&gt;",
        "<": "&lt;"
    };

    /** Returns XML-encoded string */
    function escape(text) {
        for (var e in escapes) {
            text = text.replace(new RegExp(e, "g"), escapes[e]);
        }
        return text;
    }
    xml.escape = escape;
})(exports.xml || (exports.xml = {}));
var xml = exports.xml;


/** A parsed jsdoc comment */
var JsDoc = (function () {
    function JsDoc() {
        /** Tags and their contents */
        this.entries = [];
        this.lineBreaks = {
            param: false,
            remarks: true,
            example: true
        };
    }
    /** Add a tag */
    JsDoc.prototype.push = function (tag, text) {
        if (typeof text === "undefined") { text = ""; }
        this.entries.push({ tag: tag, text: text });
    };

    /** Add a @param tag */
    JsDoc.prototype.pushParam = function (name, type, description) {
        var text = "";
        if (type) {
            text += "{" + type + "} ";
        }
        text += name;
        if (description) {
            text += " " + description;
        }
        this.push("param", text);
    };

    /** Add a @return tag */
    JsDoc.prototype.pushReturns = function (type, description) {
        var text = "";
        if (type) {
            text += "{" + type + "} ";
        }
        if (description) {
            text += description;
        }
        text = text.trim();
        if (text) {
            this.push("returns", text);
        }
    };

    /** Sort tags to match conventions */
    JsDoc.prototype.sort = function () {
        function priority(entry) {
            switch (entry.tag) {
                case "example":
                    return 2;
                case "remarks":
                    return 1;
                default:
                    return 0;
            }
        }
        this.entries.sort(function (a, b) {
            return priority(a) - priority(b);
        });
    };

    /** Find a tag by name and returns its index */
    JsDoc.prototype.indexOf = function (tag) {
        for (var i = 0; i < this.entries.length; i++) {
            if (this.entries[i].tag === tag) {
                return i;
            }
        }
    };

    /** Find a tag by name */
    JsDoc.prototype.find = function (tag) {
        var index = this.indexOf(tag);
        return index >= 0 ? this.entries[index] : null;
    };

    /** Find all tags with a given name */
    JsDoc.prototype.findAll = function (tag) {
        return this.entries.filter(function (e) {
            return e.tag === tag;
        });
    };

    /** Invoke iter function for all tags with a given name and text matching a given regex */
    JsDoc.prototype.match = function (tag, rgx, iter) {
        var _this = this;
        this.findAll(tag).forEach(function (e) {
            var m = rgx.exec(e.text);
            if (!m) {
                console.error("Could not parse @" + tag + ": " + e.text);
                return;
            }

            var args = [m].concat(m);
            args.splice(1, 1);
            iter.apply(_this, args);
        });
    };

    /** Returns JSDoc string */
    JsDoc.prototype.toString = function () {
        var _this = this;
        var text = "";
        if (this.title) {
            text = this.title + "\n";
        }
        this.entries.forEach(function (e) {
            text += "@" + e.tag;
            var entryText = exports.trimLines(e.text, e.tag === "example");
            var breakLine = _this.lineBreaks[e.tag];
            if (breakLine === undefined) {
                breakLine = entryText.match(/\n/);
            }
            text += breakLine ? "\n" : " ";
            text += entryText + "\n";
        });
        return text.trim();
    };

    /** Returns JSDoc string as a comment */
    JsDoc.prototype.toStringWithComment = function () {
        var text = this.toString();
        text = "/** " + text;
        if (text.match(/\n/)) {
            text = text.replace(/\n+/g, "\n  * ") + "\n ";
        }
        text += " */\n";
        return text;
    };

    /** Extract comment body */
    JsDoc.removeComment = function (text) {
        return text.replace(/^\/\*{2}/, "").replace(/\*\/\s*$/m, "").replace(/^\s*\*\s?/gm, "");
    };

    /** Parse a jsdoc comment string */
    JsDoc.parse = function (text) {
        text = this.removeComment(text).trim();

        var doc = new JsDoc(), rgxTag = /^@(\w+)\s?/mg, start = 0, prevTag = "";

        do {
            var m = rgxTag.exec(text);
            var end = m ? m.index : text.length;
            var value = text.substring(start, end).trim();
            if (prevTag) {
                doc.entries.push({ tag: prevTag, text: value });
            } else if (!m || m.index > 0) {
                doc.title = value;
            }

            if (m) {
                prevTag = m[1];
                start = m.index + m[0].length;
            }
        } while(m);

        return doc;
    };
    return JsDoc;
})();
exports.JsDoc = JsDoc;

/** Indent all lines in the text */
function indent(text, level) {
    if (typeof level === "number") {
        level = new Array(level + 1).join(" ");
    }
    return level + text.replace(/(\n[\s\S])/mg, "$1" + level);
}
exports.indent = indent;

/** Remove indentation from all lines in the text
* @param sameIndent {boolean} Remove equal amount of whitespace from each line
*/
function trimLines(text, sameIndent) {
    if (typeof sameIndent === "undefined") { sameIndent = false; }
    if (!sameIndent) {
        return text.trim().replace(/\n[ \t]+/g, "\n");
    }

    text = text.replace(/\t/g, "    ");
    var lines = text.split(/\n/);
    var minIndent = null;
    lines.forEach(function (l) {
        var m = /^ */.exec(l);
        if (m) {
            var indent = m[0].length;
            minIndent = minIndent === null ? indent : Math.min(minIndent, indent);
        }
    });
    if (minIndent === null) {
        return text;
    }

    for (var i = 0; i < lines.length; i++) {
        lines[i] = lines[i].substr(minIndent);
    }
    return lines.join("\n");
}
exports.trimLines = trimLines;

/** Remove trailing whitespace */
function trimLineBreaks(text) {
    var prevText;
    do {
        prevText = text;
        text = text.replace(/^\s*\n+/, "");
    } while(prevText !== text);

    do {
        prevText = text;
        text = text.replace(/\n+\s*$/, "");
    } while(prevText !== text);
    return text;
}
exports.trimLineBreaks = trimLineBreaks;

/** Convert a string to camelCase */
function camelCase(text) {
    text = text.replace(/\s\w/g, function (m) {
        return m[m.length - 1].toUpperCase();
    });
    text = text.charAt(0).toLowerCase() + text.substr(1);
    return text;
}
exports.camelCase = camelCase;

/** Insert \r before each \n */
function crlf(text) {
    return text.replace(/\n/g, "\r\n");
}
exports.crlf = crlf;

/** Replace \n\r or \r\n with \n */
function lf(text) {
    return text.replace(/(\r\n?|\n\r)/g, "\n");
}
exports.lf = lf;
