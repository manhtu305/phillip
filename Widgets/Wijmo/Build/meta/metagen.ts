/// <reference path="TypeScript/TypeScript.d.ts" />
/// <reference path="../../External/declarations/node.d.ts" />

/** This script contains classes for reading a TypeScript file and generating .ts.meta metadata file.
  * It uses TypeScript parser available in the TypeScript compiler, written in TypeScript.
  */

import path = require("path");
import fs = require("fs");
import util = require("./../util");
import docutil = require("./docutil")
import meta = require("./meta");
import tsutil = require("./tsutil");
var et = require("elementtree"),
    tsc = require("./tsc.js"), // load TypeScript compiler
    TypeScript = tsc.TypeScript;

/** Extracts metadata from a TypeScript file */
export class Extracter {
    /** EventArg interfaces to generate */
    eventDataInterfaces = {};
    /** A stack of current modules */
    currentModule = [];

    semanticInfoChain;

    /** Parse JSDoc attached to the given AST node */
    readDoc(ast: TypeScript.AST) {
        if (ast && ast.preComments && ast.preComments() != null) {
            var preComments = ast.preComments(), text;

            for (var i = 0; i < preComments.length; i++) {
                text = preComments[i].fullText();
                if (text.match(/^\/\*{2}[\s\S]+\*\//)) {
                    return docutil.JsDoc.parse(docutil.lf(text));
                }
            }
        }
        
        return new docutil.JsDoc();
    }

    /** Copy jsdoc values to metadata */
    importDoc(doc: docutil.JsDoc, metadata: meta.MetadataObject) {
        if (doc.title) {
            metadata.summary = doc.title;
        }
        var remarks = doc.find("remarks");
        if (remarks) {
            metadata.remarks = remarks.text;
        }

        doc.findAll("example").forEach(ex => metadata.examples.push(ex.text));
    }

    /** Find TypeScript.ArgDecl by name in the given TypeScript.FunctionDeclaration */
    findArgument(func: TypeScript.FunctionDeclaration, name: string) {
        var args = func.callSignature.parameterList.parameters.members,
            i: number;
        for (i = 0; i < args.length; i++) {
            var arg = <TypeScript.ArgDecl> args[i];
            if (arg.identifier.text() === name) {
                return arg;
            }
        }

        return null;
    }

    _getTypeNameFromAST(t) {
        if (t instanceof TypeScript.Identifier) {
            return t.text();
        } else if (t instanceof TypeScript.BuiltInType) {
            return t.text();
        } else if (t instanceof TypeScript.GenericType) {
            return this._getTypeNameFromAST(t.name);
        } else if (t instanceof TypeScript.ArrayType) {
            return this._getTypeNameFromAST(t.type) + "[]";
        } else if (t instanceof TypeScript.FunctionType) {
            return this._getTypeNameFromAST(t.type);
        } else if (t instanceof TypeScript.ObjectType) {
            return null;
        } else {
            return null;
        }
    }

    isBuiltInType(type, typeName, moduleName): boolean {
        if (type.type instanceof TypeScript.BuiltInType) {
            return true;
        } else if (type.type instanceof TypeScript.ArrayType) {
            return true;
        } else if (typeName === "Object") {
            return true;
        } else if (typeName === "Date") {
            return true;
        } else if (typeName === "Function") {
            return true;
        }
        return false;
    }
    /** Get a string of a type name as it should be look in metadata */
    getTypeName(type: TypeScript.TypeAnnotation): string {
        if (!type) {
            return null;
        }
        var typeName, moduleNames,
            moduleName = TypeScript.ASTHelpers.getEnclosingModuleDeclaration(type);
        typeName = this.getNameFromAST(type.type);
        if (!typeName) {
            typeName = this._getTypeNameFromAST(type.type);
            if (moduleName !== null && typeName !== null && !this.isBuiltInType(type, typeName, moduleName)) {
                moduleNames = this.readModuleName(moduleName).name;
                typeName = moduleNames + "." + typeName;
            }
        }
        return typeName;
    }

    /** A special object that, if returned by evalConstant(), means that a constant couldn't be evaluated */
    static constantNotResolved = {};
    evalConstant(expr) {
        switch (expr && expr.nodeType) {
            case TypeScript.SyntaxKind.QString:
                return expr.text.substr(1, expr.text.length - 2);

            case TypeScript.SyntaxKind.NumberLit:
                return expr.value;

            case TypeScript.SyntaxKind.Neg:
                var operand = this.evalConstant(expr.operand);
                if (operand != Extracter.constantNotResolved) {
                    operand -= operand;
                }
                return operand;

            case TypeScript.SyntaxKind.True:
                return true;
            case TypeScript.SyntaxKind.False:
                return false;
            case TypeScript.SyntaxKind.Null:
                return null;
            case TypeScript.SyntaxKind.Empty:
            case TypeScript.SyntaxKind.EmptyExpr:
                return undefined;

            case TypeScript.SyntaxKind.ArrayLit:
                var result = expr.members ? <any[]> expr.members.map(this.evalConstant) : [];
                // if at least one constant wasn't evaluated, return constantNotResolved
                return result.indexOf(Extracter.constantNotResolved) < 0 ? <any> result : Extracter.constantNotResolved;

            default:
                return Extracter.constantNotResolved;
        }
    }

    /** Returns a string name of a value type */
    getValueType(value) {
        if (value && typeof value === "object" && "length" in value) {
            return "array";
        }
        if (value instanceof Date) {
            return "date";
        }

        return typeof value;
    }
    
    static docTypeMap = { number: "number", string: "string", boolean: "bool", bool: "bool", any: null };
    /** Normalizes casing and converts boolean to bool */
    normalizeTypeName(docTypeName: string) {
        if (!docTypeName) return docTypeName;
        var lowerCase = docTypeName && docTypeName.toLowerCase();
        return lowerCase in Extracter.docTypeMap ? Extracter.docTypeMap[lowerCase] : docTypeName;
    }

    /** Call iterator for all parameter tags
      * @param doc JSDoc to parse
      * @param tag Name of a tag. Often it is "param"
      * @param iter Callback function to call
      * @param paramName True if tag contents contain parameter name. Defaults to true
      */
    readParams(doc: docutil.JsDoc, tag, iter: (name: string, type: string, summary: string) => any, paramName = true) {
        var paramRegex = paramName
            ? /({[^}]+}\s+)?(\w+)([\s\S]+)?/
            : /({[^}]+}\s+)?()([\s\S]+)?/;;

        doc.match(tag, paramRegex, (m, type, name, summary) => {
            summary = (summary || "").trim();
            type = type && type.substring(1, type.length - 2);
            type = this.normalizeTypeName(type);
            iter(name, type, summary);
        });
    }

    /** Returns string name of function return type */
    getFunctionReturnType(func: TypeScript.FunctionDeclaration) {
        var returnType;
        if (func.callSignature.typeAnnotation) {
            returnType = this.getTypeName(func.callSignature.typeAnnotation);
        } else if (func.block && func.block.statements.members && func.block.statements.members.length) {
            var pullSymbol = this.semanticInfoChain.getSymbolForAST(func),
                callSignatures, returnType;
            if (pullSymbol && pullSymbol.type && pullSymbol.type !== this.semanticInfoChain.anyTypeSymbol) {
                callSignatures = pullSymbol.type.getCallSignatures();
                if (callSignatures && callSignatures.length && callSignatures.length > 0) {
                    returnType = callSignatures[0].returnType.getDisplayName();
                    if (returnType === "void") {
                        returnType = null;
                    }
                }
            }
        } else {
            returnType = null;
        }
        return returnType;
    }

    readDefaultMemberVariableValue(expr: TypeScript.MemberVariableDeclaration) {
        var v, value;
        if (expr === undefined) {
            return null;
        }
        if (expr.equalsValueClause && expr.equalsValueClause && expr.equalsValueClause.value) {
            v = expr.equalsValueClause.value;
            if (v instanceof TypeScript.LiteralExpression) {
                value = eval(v.text());
            } else if (v instanceof TypeScript.NumericLiteral) {
                value = v.value();
            } else if (v instanceof TypeScript.PrefixUnaryExpression) {
                value = 0 - v.operand.value();
            } else if (v instanceof TypeScript.StringLiteral) {
                value = v.text().replace(/\"/g, "").replace(/'/g, "");
            } else if (v instanceof TypeScript.ArrayLiteralExpression) {
                //value = v.expressions.members;
                //TODO: return value instead of [];
                value = [];
            } else if (v instanceof TypeScript.MemberAccessExpression) {
            } else if (v instanceof TypeScript.ObjectLiteralExpression) {
            }
            if (value !== undefined) {
                return new meta.DefaultValue(value);
            }
        }
        return null;
    }

    /** Parses ast as a constant, if possible, and wraps with meta.DefaultValue */
    readDefaultValue(expr: TypeScript.AST) {
        if (expr === undefined) {
            return null;
        }
        var constant = this.evalConstant(expr);
        if (constant == Extracter.constantNotResolved) {
            return null;
        } else {
            return new meta.DefaultValue(constant);
        }
    }

    /** Read param tags and convert them to meta.Parameter array */
    readParameters(doc: docutil.JsDoc, func: TypeScript.FunctionDeclaration = null) {
        var params: meta.Parameter[] = [];
        this.readParams(doc, "param", (name, type, summary) => {
            var arg: TypeScript.ArgDecl;

            if (func) {
                arg = this.findArgument(func, name);
            }
            if (arg && !type) {
                type = arg && this.getTypeName(arg.typeAnnotation);
            }
            type = this.normalizeTypeName(type);
            var param = new meta.Parameter(name, type, summary);
            if (arg) {
                param.isOptional = (arg.questionToken !== null || arg.equalsValueClause !== null || arg.dotDotDotToken !== null);
                if (arg.equalsValueClause) {
                    param.defaultValue = this.readDefaultMemberVariableValue(arg);
                }
            }
            params.push(param);
        });
        return params;
    }

    /** Read a function declaration with jsdoc and convert to meta.Func */
    readFunction(func: TypeScript.FunctionDeclaration, doc = this.readDoc(func)) {
        var text = "";
        if (func.propertyName) {
            text = func.propertyName.text();
        } else if (func.identifier) {
            text = func.identifier.text();
        }
        var result = new meta.Func(text);
        this.importDoc(doc, result);

        // parameters
        result.parameters = result.parameters.concat(this.readParameters(doc, func));
        if (result.parameters.length === 0) {
            var params = func.callSignature.parameterList.parameters
            params.members.forEach((a: TypeScript.ArgDecl) => {
                var type = this.normalizeTypeName(this.getTypeName(a.typeAnnotation));
                result.parameters.push(new meta.Parameter(a.identifier.text(), type));
            });
        }
        // returns
        var returnType = this.normalizeTypeName(this.getFunctionReturnType(func));
        var returnSummary = "";
        this.readParams(doc, "returns", (name, type, summary) => {
            returnType = type || returnType;
            returnSummary = summary;
        }, false);
        if (returnType || returnSummary) {
            result.returns = new meta.Parameter("", returnType, returnSummary);
        }
        if (func.kind() === TypeScript.SyntaxKind.MemberFunctionDeclaration) {
            if (TypeScript.hasModifier(func.modifiers, 16/* static */)) {
                result.isStatic = true;
            }
        }

        return result;
    }

    /** Read type information */
    readTypeTag(doc: docutil.JsDoc) {
        var typeTag = doc.find("type");
        if (!typeTag) return null;
        var type = typeTag.text;
        if (type.match(/^\{.+\}$/)) {
            type = type.substr(1, type.length - 2);
        }
        return type;
    }

    isTypeKnown(type: string) {
        return type && !type.match(/any/i);
    }

    /** Copy property info from field and doc to the target meta.Property object */
    readPropertyTo(target: meta.Property, field: TypeScript.PropertySignature, doc = this.readDoc(field)) {
        var type;
        this.importDoc(doc, target);
        if (field.kind() === TypeScript.SyntaxKind.MemberVariableDeclaration) {
            target.defaultValue = this.readDefaultMemberVariableValue(field.variableDeclarator);

            type = this.readTypeTag(doc) || this.getTypeName(field.variableDeclarator.typeAnnotation);
        } else {
            target.defaultValue = this.readDefaultValue(field.init);
            type = this.readTypeTag(doc) || this.getTypeName(field.typeAnnotation);
        }

        if (type === "IObservable") {
            type = null;
            target.isObservable = true;
        }
        if (type === "any[]") {
            type = "array";
        }
        
        if (this.isTypeKnown(type)) {
            target.type = type;
        } else if (target.defaultValue) {
            target.type = this.getValueType(target.defaultValue.value);
        }

        if (field.isStatic && field.isStatic()) {
            target.isStatic = true;
        }

        return target;
    }
    /** Convert a VarDecl to meta.Property */
    readProperty(field: TypeScript.PropertySignature, observableOnly: boolean) {
        if (observableOnly) {
            var type = this.getTypeName(field.typeAnnotation);
            if (!type || !type.match(/observable/i)) {
                return null;
            }
        }
        return this.readPropertyTo(new meta.Property(field.propertyName.valueText()), field);
    }

    getNameFromAST(ast) {
        var name, pullSymbol;
        if (ast instanceof TypeScript.Identifier || ast instanceof TypeScript.QualifiedName) {
            pullSymbol = this.semanticInfoChain.getSymbolForAST(ast);
            if (pullSymbol) {
                name = pullSymbol.fullName();
            }
        } else if (ast instanceof TypeScript.GenericType && ast.name) {
            pullSymbol = this.semanticInfoChain.getSymbolForAST(ast.name);
            if (pullSymbol) {
                name = pullSymbol.getName();
            }
        } else if (ast instanceof TypeScript.ArrayType) {
            name = this.getNameFromAST(ast.type) + "[]";
        } else if (ast instanceof TypeScript.BuiltInType) {
            name = ast.text();
        } else if (ast instanceof TypeScript.FunctionType) {
            name = "function";
        }
        return name;
    }
    
    /** Read a string name from an AST node */
    readName(ast: TypeScript.AST, fullName = true) {
        switch (ast.kind()) {
            case TypeScript.SyntaxKind.ExtendsHeritageClause:
            case TypeScript.SyntaxKind.ImplementsHeritageClause: {
                var typeNames = ast.typeNames.members,
                    name: any = [];
                typeNames.forEach(tn => {
                    var tnName = this.getNameFromAST(tn);
                    if (tnName) {
                        name.push(tnName);
                    }
                });
                if (name.length === 0) {
                    name = "";
                } else if (name.length === 1) {
                    name = [name[0]];
                }
                return name;
            }
            case TypeScript.SyntaxKind.Name: {
                var id = <TypeScript.Identifier> ast;
                if (fullName && id.sym) {
                    return id.sym.fullName();
                }
                return id.text;
            }
            
            case TypeScript.SyntaxKind.Dot: {
                var dot = <TypeScript.BinaryExpression> ast;
                if (fullName && dot.type && dot.type.symbol) {
                    return dot.type.symbol.fullName();
                }
                var left = this.readName(dot.operand1, false);
                var right = this.readName(dot.operand2, false);
                if (left && right) {
                    return left + "." + right;
                }
            }
        }

        return null;
    }

    /** Return the name of the base type of a class declaration*/
    readBaseClass(ast: TypeScript.ClassDeclaration, doc: docutil.JsDoc, result) {
        var extendTag = doc.find("extends");
        if (extendTag && extendTag.text) {
            result.base = extendTag.text;
        }
        extendTag = doc.find("implements");
        if (extendTag && extendTag.text) {
            result.interfaces.push(extendTag.text);
        }

        if (!ast || !ast.heritageClauses) return null;
        ast.heritageClauses.members.forEach(m => {
            var name = this.readName(m);
            if (name) {
                if (m.kind() === TypeScript.SyntaxKind.ExtendsHeritageClause) {
                    result.base = name;
                } else if (m.kind() === TypeScript.SyntaxKind.ImplementsHeritageClause) {
                    result.interfaces.push.apply(result.interfaces, name);
                }
            }
        });
    }

    /** Read a widget option from a PropertySignature object */
    readOption(field: TypeScript.MemberVariableDeclaration, doc: docutil.JsDoc) {
        var option = <meta.Option> this.readPropertyTo(new meta.Option(field.variableDeclarator.propertyName.text()), field, doc);
        var parameters = this.readParameters(doc);
        if (parameters) {
            option.parameters = parameters;
        }
        return option;
    }
    
    /** Read a widget event from PropertySignature object */
    readEvent(field: TypeScript.MemberVariableDeclaration, doc: docutil.JsDoc, ownerTypeName: string) {
        var event = new meta.Event(field.variableDeclarator.propertyName.text());
        this.importDoc(doc, event);

        event.parameters = this.readParameters(doc);

        if (event.parameters.length == 0) {
            var dataType = new meta.Interface("I" + capitalize(event.name) + "EventArgs");
            dataType.summary = "Contains information about " + ownerTypeName + "." + event.name + " event";
            this.readParams(doc, "dataKey", (name, type, summary) => {
                var prop = new meta.Property(name, false);
                prop.type = type;
                prop.summary = summary;
                dataType.properties.push(prop);
            });

            if (dataType.properties.length > 0) {
                var list: meta.Interface[] = (this.eventDataInterfaces[ownerTypeName] = this.eventDataInterfaces[ownerTypeName] || []);
                list.push(dataType);

                var fullDataTypeName = this.currentModule.length == 0 ? "" : this.currentModule[this.currentModule.length - 1] + ".";
                fullDataTypeName += dataType.name;

                event.parameters.push(new meta.Parameter("data", fullDataTypeName, "Information about an event"));
            }
        }

        if (event.parameters.length == 0 || event.parameters.length < 2 && !/e/i.exec(event.parameters[0].name)) {
            event.parameters.unshift(new meta.Parameter("e", "jQuery.Event", "Standard jQuery event object"));
        }

        return event;
    }

    /** Copy widget info from an ast node to a meta.Widget object */
    readWidget(widget: meta.Widget, ast: TypeScript.ClassDeclaration, siblings: TypeScript.AST[]) {
        // find options and events
        if (!siblings) return;

        var optionClassName = widget.name + "_options";
        var optionClass = <TypeScript.ClassDeclaration>siblings.filter(s => s.kind() === TypeScript.SyntaxKind.ClassDeclaration &&
            (<TypeScript.ClassDeclaration>s).identifier.text() === optionClassName)[0];
        if (!optionClass) return;

        this._forEachClassPublicMember(optionClass, TypeScript.SyntaxKind.MemberVariableDeclaration, (f: TypeScript.MemberVariableDeclaration, doc: docutil.JsDoc) => {
            var isEvent = doc.find("event");
            if (isEvent) {
                widget.events.push(this.readEvent(f, doc, widget.name));
            } else {
                widget.options.push(this.readOption(f, doc));
            }
        });
    }

    /** Read class info from an ast node and jsdoc to a meta.ClassOrInterface object */
    readClassOrInterface(target: meta.ClassOrInterface, ast: TypeScript.TypeDeclaration, doc: docutil.JsDoc = this.readDoc(ast)) {
        this.importDoc(doc, target);
        this._forEachPublicMember(ast, TypeScript.SyntaxKind.MethodSignature, (f: TypeScript.MethodSignature) => target.methods.push(this.readFunction(f)));

        this._forEachPublicMember(ast, TypeScript.SyntaxKind.PropertySignature, (v: TypeScript.PropertySignature) => {
            var property = this.readProperty(v, ast.kind() === TypeScript.SyntaxKind.ClassDeclaration);
            if (property != null) {
                target.properties.push(property);
            }
        });
    }
    /** Read class info from an ast node and jsdoc to a meta.ClassOrInterface object */
    readClassDeclaration(target: meta.ClassOrInterface, ast: TypeScript.TypeDeclaration, doc: docutil.JsDoc = this.readDoc(ast)) {
        this.importDoc(doc, target);
        this._forEachClassPublicMember(ast, TypeScript.SyntaxKind.MemberFunctionDeclaration, (f: TypeScript.MemberFunctionDeclaration) => target.methods.push(this.readFunction(f)));

        this._forEachClassPublicMember(ast, TypeScript.SyntaxKind.MemberVariableDeclaration, (v: TypeScript.MemberVariableDeclaration) => {
            var property = this.readProperty(v, ast.kind() === TypeScript.SyntaxKind.ClassDeclaration);
            if (property != null) {
                target.properties.push(property);
            }
        });
    }
    /** Read class info from an ast node and jsdoc to a meta.ClassOrInterface object */
    readInterfaceSignature(target: meta.ClassOrInterface, ast: TypeScript.TypeDeclaration, doc: docutil.JsDoc = this.readDoc(ast)) {
        this.importDoc(doc, target);
        this._forEachInterfacePublicMember(ast, TypeScript.SyntaxKind.MethodSignature, (f: TypeScript.MethodSignature) => target.methods.push(this.readFunction(f)));

        this._forEachInterfacePublicMember(ast, TypeScript.SyntaxKind.PropertySignature, (v: TypeScript.PropertySignature) => {
            var property = this.readProperty(v, ast.kind() === TypeScript.SyntaxKind.ClassDeclaration);
            if (property != null) {
                target.properties.push(property);
            }
        });
    }

    /** Read enum info from an ast node and jsdoc to a meta.Enum object */
    readEnumSignature(target: meta.Enum, ast: TypeScript.EnumDeclaration, doc: docutil.JsDoc = this.readDoc(ast)) {
        this.importDoc(doc, target);
        ast.enumElements.members.forEach((m) => {
            var d = this.readDoc(m),
                e = new meta.EnumMember(m.propertyName.text()),
                equalClause = m.equalsValueClause, v;
            this.importDoc(d, e);
            if (equalClause != null && equalClause.value != null) {
                v = equalClause.value;
                if (v instanceof TypeScript.PrefixUnaryExpression) {
                    if (v._nodeType === 165) {
                        /** - */
                        e.value = -(v.operand.value());
                    } else if (v._nodeType === 164) {
                        /** + */
                        e.value = +(v.operand.value());
                    }
                } else if (v.value) {
                    e.value = v.value();
                }
            }
            target.enumMembers.push(e);
        });
    }

    /** Convert a ClassDeclaration to a meta.Class object
      * @returns Return value may be an instance of meta.Widget
      */
    readClass(ast: TypeScript.ClassDeclaration, siblings: TypeScript.AST[], doc: docutil.JsDoc) {
        var isWidget = doc.find("widget");
        var text = ast.identifier.text();
        var result = isWidget ? new meta.Widget(text) : new meta.Class(text),
            widget = isWidget && <meta.Widget>result,
            classBase;
        classBase = this.readBaseClass(ast, doc, result);

        this.readClassDeclaration(result, ast, doc);

        if (widget) {
            this.readWidget(widget, ast, siblings);
        }

        return result;
    }

    /** Convert an InterfaceDeclaration object to meta.Interface */
    readInterface(ast: TypeScript.InterfaceDeclaration, doc: docutil.JsDoc) {
        var result = new meta.Interface(ast.identifier.text());
        if (ast.heritageClauses) {
            this._forEachName(ast.heritageClauses, name => result.interfaces.push.apply(result.interfaces, name));
        }
        this.readInterfaceSignature(result, ast, doc);
        return result;
    }

    readEnum(ast: TypeScript.EnumDeclaration, doc: docutil.JsDoc) {
        var result = new meta.Enum(ast.identifier.text());
        this.readEnumSignature(result, ast, doc);
        return result;
    }

    readDependenciesTo(target: meta.Unit, members: any[]) {
        var dependenciesNode: meta.Dependencies = new meta.Dependencies(null),
            member: meta.MetadataObject;

        target.members.push(dependenciesNode);
        members.forEach((fileName: string) => {
            if (fileName.match(/lib.d.ts$/)) {
                return;
            }
            member = new meta.Dependency(fileName);
            dependenciesNode.members.push(member);
        });
    }

    /** Copy module info from an AST list to a meta.Module object */
    readModuleTo(target: meta.Module, members: TypeScript.ASTList, exportedOnly = true) {
        members.moduleElements.members.forEach((node: any) => {
            var doc = this.readDoc(node);
            if (doc.find("ignore")) return;

            var member: meta.MetadataObject;

            switch (node.kind()) {
                case TypeScript.SyntaxKind.FunctionDeclaration:
                    if (!TypeScript.hasModifier(node.modifiers, 1/* exported*/)) {
                        return;
                    }
                    member = this.readFunction(node, doc);
                    break;

                case TypeScript.SyntaxKind.ClassDeclaration: 
                    if (!TypeScript.hasModifier(node.modifiers, 1/* exported*/)) {
                        return;
                    }
                    member = this.readClass(node, members.moduleElements.members, doc);
                    break;

                case TypeScript.SyntaxKind.InterfaceDeclaration:
                    member = this.readInterface(node, doc);
                    break;

                case TypeScript.SyntaxKind.ModuleDeclaration:
                    var mod = this.readModule(node);
                    if (mod.members.length > 0) {
                        member = mod;
                    }
                    break;
                case TypeScript.SyntaxKind.EnumDeclaration:
                    if (!TypeScript.hasModifier(node.modifiers, 1/* exported*/)) {
                        return;
                    }
                    member = this.readEnum(node, doc);
                    break;
                case TypeScript.SyntaxKind.ExportAssignment:
                    break;
            }

            if (member) {
                target.members.push(member);
            }

            return false;
        });

        for (var widgetName in this.eventDataInterfaces) {
            this.eventDataInterfaces[widgetName].forEach(iface => target.members.push(iface));
        }
        this.eventDataInterfaces = {};
    }

    readQualifiedName(qualifiedName, getmeta, name?, rootMeta?, currentMeta?) {
        var n = name,
            rm = rootMeta,
            cm = currentMeta,
            returnVal;
        if (qualifiedName.left instanceof TypeScript.QualifiedName) {
            returnVal = this.readQualifiedName(qualifiedName.left, getmeta, n, rm, cm);
            n = returnVal.name;
            rm = returnVal.rootMeta;
            cm = returnVal.currentMeta;

        } else if (qualifiedName.left instanceof TypeScript.Identifier) {
            n = qualifiedName.left.text();
            if (getmeta) {
                rm = cm = new meta.Module(n);
            }
        }

        if (qualifiedName.right instanceof TypeScript.Identifier) {
            var newN = qualifiedName.right.text();
            if (getmeta) {
                var newM = new meta.Module(newN);
                cm.members.push(newM);
                cm = newM;
            }
            n = n + "." + newN;
        }
        return {
            name: n,
            rootMeta: rm,
            currentMeta: cm
        }
    }

    readModuleName(ast, getmeta?: boolean) {
        var name, rootMeta, currentMeta;
        if (ast.name != null && ast.name instanceof TypeScript.QualifiedName) {
            var returnVal = this.readQualifiedName(ast.name, getmeta);
            name = returnVal.name;
            rootMeta = returnVal.rootMeta;
            currentMeta = returnVal.currentMeta;
        } else {
            if (ast.name != null && ast.name instanceof TypeScript.Identifier) {
                name = ast.name.text();
            } else if (ast.stringLiteral != null && ast.stringLiteral instanceof TypeScript.StringLiteral) {
                name = ast.stringLiteral.text();
            } else {
                name = "";
            }
            if (getmeta) {
                rootMeta = currentMeta = new meta.Module(name);
            }
        }
        return {
            name: name,
            rootMeta: rootMeta,
            currentMeta: currentMeta
        };
    }

    /** Convevrt a ModuleDeclaration object to a meta.Module object */
    readModule(ast: TypeScript.ModuleDeclaration) {
        var moduleName = this.readModuleName(ast, true),
            fullName = this.currentModule.length == 0 ? "" : this.currentModule[this.currentModule.length - 1] + ".";

        fullName += moduleName.name;

        this.currentModule.push(fullName);

        try {
            this.readModuleTo(moduleName.currentMeta, ast);
            return moduleName.rootMeta;
        } finally {
            this.currentModule.pop();
        }
    }

    /** Parse code and return a meta.Unit object with a full meta tree */
    readUnit(context: TypeScript.ISourceText, filename = "", unitIndex = 0) {
        var unit = new meta.Unit(filename);
        return unit;
    }

    // -- util --

    /** Walk ASTs with a given 'process' function */
    _walkMembers(members: TypeScript.AST[], process: (node: TypeScript.AST, siblings: TypeScript.AST[]) => void) {
        function walk(members: TypeScript.AST[]) {
            members.forEach((node) => {
                if (process(node, members)) return;

                var children: TypeScript.ASTList = (<any>node).members;
                if (children) {
                    walk(children.members);
                }
            });
        }

        walk(members);
    }
    /** Call a function for each public member in given a type declaration
      * @param ast Type declaration containing the members to iterate
      * @param nodeType If specified, the callback is invoked only for nodes of the specified type
      * @param iter The callback function to invoke
      */
    _forEachInterfacePublicMember(ast: TypeScript.TypeDeclaration, nodeType: TypeScript.SyntaxKind, iter: (member: TypeScript.AST, doc: docutil.JsDoc) => any) {
        // find methods
        ast.body.typeMembers.members.forEach((m: any) => {
            if (nodeType && m.kind() !== nodeType) {
                return;
            }
            if (isFunc(m.isPublic) && !m.isPublic()) {
                return;
            }

            var id = m.name || m.id || m.propertyName;
            if (id && id.text().charAt(0) === "_") {
                return;
            }

            var doc = this.readDoc(m);
            if (doc.find("ignore")) {
                return;
            }

            return iter(m, doc);
        });
    }

    /** Call a function for each public member in given a type declaration
      * @param ast Type declaration containing the members to iterate
      * @param nodeType If specified, the callback is invoked only for nodes of the specified type
      * @param iter The callback function to invoke
      */
    _forEachClassPublicMember(ast: TypeScript.TypeDeclaration, nodeType: TypeScript.SyntaxKind, iter: (member: TypeScript.AST, doc: docutil.JsDoc) => any) {
        // find methods
        ast.classElements.members.forEach((m: any) => {
            if (nodeType && m.kind() !== nodeType) {
                return;
            }
            if (isFunc(m.isPublic) && !m.isPublic()) {
                return;
            }

            var id = m.name || m.id || m.propertyName;
            if (id && id.text().charAt(0) === "_") {
                return;
            }

            var doc = this.readDoc(m);
            if (doc.find("ignore")) {
                return;
            }

            return iter(m, doc);
        });
    }

    /** Call a function for each public member in given a type declaration
      * @param ast Type declaration containing the members to iterate
      * @param nodeType If specified, the callback is invoked only for nodes of the specified type
      * @param iter The callback function to invoke
      */
    _forEachPublicMember(ast: TypeScript.TypeDeclaration, nodeType: TypeScript.SyntaxKind, iter: (member: TypeScript.AST, doc: docutil.JsDoc) => any) {
        // find methods
        ast.body.typeMembers.members.forEach((m: any) => {
            if (nodeType && m.kind() !== nodeType) {
                return;
            }
            if (isFunc(m.isPublic) && !m.isPublic()) {
                return;
            }

            var id = m.name || m.id || m.propertyName;
            if (id && id.text().charAt(0) === "_") {
                return;
            }

            var doc = this.readDoc(m);
            if (doc.find("ignore")) {
                return;
            }

            return iter(m, doc);
        });
    }
    /** Inboke the callback function for each successfully parsed name in an AST list */
    _forEachName(astList: TypeScript.ASTList, iter: (name: string) => any) {
        if (!astList) return null;
        astList.members.forEach(m => {
            var name = this.readName(m);
            if (name && iter(name) === false) return false;
        });
    }
}

/** Accepts a list of filenames and generates ts.meta files along the .ts files */
export class MetaCompiler {

    extracter = new Extracter();

    /** Generates .ts.meta files along the given file list
      * @param throwOnEmpty if true, throws an error if no metadata was found in a file
      */
    compile(files: string[], throwOnEmpty = true) {
        var semanticInfoChain = tsutil.StandaloneTypeCheck.parse(files);
        this.extracter.semanticInfoChain = semanticInfoChain;
        var scripts = semanticInfoChain.documents;
        if (!scripts)
            return 0;

        var compiledCount = 0;
        scripts.forEach((s: TypeScript.Script) => {
            var filename = s.fileName;
            var sourceUnit = s.sourceUnit();
            var unit = new meta.Unit(filename);
            if (filename.match(/wijmo.d.ts/)) {
                //create dependenciesOrder.meta file when generating wijmo.d.ts.meta file
                var depsOrderFileName = filename.replace("wijmo.d.ts", "dependenciesorder.ts"),
                    depsOrderUnit = new meta.Unit(depsOrderFileName);
                this.extracter.readDependenciesTo(depsOrderUnit, semanticInfoChain.fileNames());
                depsOrderUnit.serializeToFile(depsOrderFileName + ".meta");
            }
            this.extracter.readModuleTo(unit, sourceUnit, false);

            //if (!hasNonModules(unit)) {
            //    if (throwOnEmpty) {
            //        throw new Error("Metadata not found");
            //    } else {
            //        return;
            //    }
            //}

            compiledCount++;
            unit.serializeToFile(filename + ".meta");
        });

        return compiledCount;
    }
}

function isFunc(func: Function) {
    return typeof func === "function";
}

export function hasNonModules(m: meta.Module) {
    return m.members.some(member => {
        return member.nodeType != meta.MetadataType.Module || hasNonModules(<meta.Module>member);
    });
}
function capitalize(text: string) {
    return text.charAt(0).toUpperCase() + text.substr(1);
}

function main() {
    try {
        if (process.argv.length < 2) {
            throw new Error("File not specified.\nUsage: node metagen.js <filename>");
        }

        var filename = process.argv[2];
        // generate metadata for the specified file
        var compiler = new MetaCompiler();
        compiler.compile([filename]);
        return 0;
    } catch (err) {
        console.log(err);
        if (err.stack) {
            console.log(err.stack);
        }
        return 1;
    }
}

if (require.main === module ) {
    process.exit(main());
}