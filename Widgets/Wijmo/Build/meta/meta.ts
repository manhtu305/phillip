/// <reference path="../../External/declarations/node.d.ts" />
/// <reference path="../../External/declarations/elementtree.d.ts" />

/** This file contains classes that represent structure of a script
  * by a tree rooted with a Unit instance
  */

import fs = require("fs");
import et = require("elementtree"); // XML utility

; // TypeScript compiler bug fix

export enum MetadataType {
    None,
    Module,
    Unit,
    Class,
    Interface,
    EnumMember,
    Enum,
    Func,
    Parameter,
    Property,
    Widget,
    Option,
    Event,
    EventDataKey,
    Dependency,
    Dependencies
}

/** Base class for all metadata objects.
  * @remarks
  * In order to customize conversion to XML, derived classes should override _serializeAttributes and _serializeContents.
  */
export class MetadataObject {
    remarks: string;
    examples: string[] = [];
    nodeType = MetadataType.None;

    constructor(public name: string, public summary?: string) {
    }
    fullName() {
        return this.name;
    }

    elementName = "unexpected";
    
    /** Add itself to the given parent element.
      * @remarks
      * Normally not overridden. 
      */
    serialize(parent: et.IElement = null) {
        var element = parent ? et.SubElement(parent, this.elementName) : et.Element(this.elementName);
        this._serialize(element);

        if (this.remarks) {
            et.SubElement(element, "remarks").text = this.remarks;
        }
        return element;
    }
    /** Serialize itself to the given target element.
      * @remarks
      * Normally not overridden. 
      */
    _serialize(target: et.IElement) {
        this._serializeAttributes(target);
        this._serializeContent(target);
    }
    /** Add attributes to the target element.
      * @remarks
      * Normally overridden in derived classes.
      */
    _serializeAttributes(target: et.IElement) {
        if (this.name) {
            target.set("name", this.name);
        }
    }
    /** Add subelements and/or text to the target element.
      * @remarks
      * Normally overridden in derived classes.
      */
    _serializeContent(target: et.IElement) {
        if (this.summary) {
            et.SubElement(target, "summary").text = this.summary;
        }
        this.examples.forEach(ex => et.SubElement(target, "example").text = ex);
    }

    serializeToFile(filename) {
        var tree = new et.ElementTree(this.serialize());
        var xmlText = tree.write({ indent: 2, lexicalOrder: false });
        fs.writeFileSync(filename, xmlText, "utf-8");
    }
        
    serializeChildren(children: MetadataObject[], parent: et.IElement) {
        children.forEach(c => c.serialize(parent));
    }
}

export class Module extends MetadataObject {
    nodeType = MetadataType.Module;
    members: MetadataObject[] = [];

    elementName = "module";
    _serializeContent(target: et.IElement) {
        super._serializeContent(target);
        this.serializeChildren(this.members, target);
    }
}

export class Dependencies extends MetadataObject {
    nodeType = MetadataType.Dependencies;
    members: Dependency[] = [];

    elementName = "dependencies";
    _serializeContent(target: et.IElement) {
        super._serializeContent(target);
        this.serializeChildren(this.members, target);
    }
}

export class Dependency extends MetadataObject {
    nodeType = MetadataType.Dependency;
    elementName = "dependency";
}

export class Unit extends Module {
    nodeType = MetadataType.Unit;
    elementName = "unit";
}

/** Member of a module */
export class Member extends MetadataObject {
    isStatic = false;

    _serializeAttributes(target) {
        super._serializeAttributes(target);
        if (this.isStatic) {
            target.set("static", true);
        }
    }
}
export class Parameter extends MetadataObject {
    nodeType = MetadataType.Parameter;
    elementName = "param";
    isOptional = false;
    defaultValue: DefaultValue;

    constructor(name: string, public type?: string, summary?: string) {
        super(name, summary);
    }
    _serializeAttributes(target: et.IElement) {
        super._serializeAttributes(target);
        if (this.type) {
            target.set("type", this.type);
        }
        if (this.isOptional || this.defaultValue) {
            target.set("optional", true);
        }
        if (this.defaultValue) {
            target.set("default", this.defaultValue.toString());
        }

    }
    _serializeContent(target: et.IElement) {
        target.text = this.summary;
        target.getchildren().length = 0;
    }
}

export class EnumMember extends MetadataObject {
    nodeType = MetadataType.EnumMember;
    elementName = "member";
    value = null;

    _serializeAttributes(target: et.IElement) {
        super._serializeAttributes(target);
        if (this.value != null) {
            target.set("value", this.value);
        }
    }
}

export class Enum extends MetadataObject {
    nodeType = MetadataType.Enum;
    elementName = "enum";
    enumMembers: EnumMember[] = [];

    _serializeContent(target: et.IElement) {
        super._serializeContent(target);
        this.serializeChildren(this.enumMembers, target);
    }
}

export class Func extends Member {
    nodeType = MetadataType.Func;
    elementName = "function";
    parameters: Parameter[] = [];
    returns: Parameter;

    _serializeContent(target: et.IElement) {
        super._serializeContent(target);
        this.serializeChildren(this.parameters, target);
        if (this.returns) {
            this.returns._serialize(et.SubElement(target, "returns"));
        }
    }
}

export class DefaultValue {
    constructor(public value) { }

    toString() {
        var result = JSON.stringify(this.value);
        if (result.match(/^"[^']+"$/)) {
            result = "'" + result.substr(1, result.length - 2) + "'";
        }
        return result;
    }
}

export class Property extends Member {
    nodeType = MetadataType.Property;
    elementName = "property";
    type: string;
    defaultValue: DefaultValue;

    constructor(name: string, public isObservable = false) {
        super(name);
    }

    _serializeAttributes(target: et.IElement) {
        super._serializeAttributes(target);
        if (this.isObservable) {
            target.set("observable", true);
        }
        if (this.type) {
            target.set("type", this.type);
        }
        if (this.defaultValue) {
            target.set("default", this.defaultValue.toString());
        }
    }
}

export class ClassOrInterface extends MetadataObject {
    methods: Func[] = [];
    properties: Property[] = [];
    interfaces: string[] = [];
    interfaceElementName = "implements";
    
    _serializeContent(target: et.IElement) {
        super._serializeContent(target);
        this.interfaces.forEach(iface => et.SubElement(target, this.interfaceElementName).text = iface);
        this.serializeChildren(this.methods, target);
        this.serializeChildren(this.properties, target);
    }
}

export class Class extends ClassOrInterface {
    nodeType = MetadataType.Class;
    base: string;

    elementName = "class";
    _serializeAttributes(target: et.IElement) {
        super._serializeAttributes(target);
        if (this.base) {
            target.set("base", this.base);
        }
    }
}

export class Interface extends ClassOrInterface {
    nodeType = MetadataType.Interface;
    elementName = "interface";
    interfaceElementName = "extends";

    //_serializeContent(target: et.IElement) {
    //    super._serializeContent(target);
    //    this.serializeChildren(this.methods, target);
    //}
}

export class Option extends Property {
    nodeType = MetadataType.Option;
    elementName = "option";
    parameters: Parameter[] = [];

    _serializeContent(target: et.IElement) {
        super._serializeContent(target);
        if (this.parameters && this.parameters.length) {
            this.serializeChildren(this.parameters, target);
        }
    }
}
export class Event extends Member {
    nodeType = MetadataType.Event;
    elementName = "event";
    parameters: Parameter[] = [];

    _serializeContent(target: et.IElement) {
        super._serializeContent(target);
        this.serializeChildren(this.parameters, target);
    }
}
export class Widget extends Class {
    nodeType = MetadataType.Widget;
    options: Option[] = [];
    events: Event[] = [];

    elementName = "widget";
    _serializeContent(target: et.IElement) {
        super._serializeContent(target);
        this.serializeChildren(this.options, target);
        this.serializeChildren(this.events, target);
    }
}