﻿/// <reference path="TypeScript/TypeScript.d.ts" />
/// <reference path="../../External/declarations/node.d.ts" />
var path = require("path");

var tsc = require("./tsc.js"), TypeScript = tsc.TypeScript;

var StandaloneTypeCheck = (function () {
    function StandaloneTypeCheck() {
        TypeScript.IO.arguments = [];
        this.batchCompiler = new TypeScript.BatchCompiler(TypeScript.IO);

        var stdlib = path.join(__dirname, "lib.d.ts");
        this.batchCompiler.inputFiles.push(stdlib);
    }
    StandaloneTypeCheck.prototype.addUnit = function (path) {
        this.batchCompiler.inputFiles.push(path);
    };
    StandaloneTypeCheck.prototype.typeCheck = function () {
        var _this = this;
        this.batchCompiler.batchCompile();
        var tsc = new TypeScript.TypeScriptCompiler(new TypeScript.NullLogger(), this.batchCompiler.compilationSettings);

        this.batchCompiler.resolvedFiles.map(function (c) {
            var sf = _this.batchCompiler.getSourceFile(c.path);
            tsc.addFile(c.path, sf.scriptSnapshot, sf.byteOrderMark, 0, false, c.referencedFiles);
        });

        tsc.resolveAllFiles();
        var docs = tsc.semanticInfoChain.documents;

        if (this.batchCompiler.hasErrors)
            return null;
        while (docs.length > 0 && (docs[0].fileName === "" || docs[0].fileName.indexOf("lib.d.ts") > -1)) {
            docs.shift();
        }

        return tsc.semanticInfoChain;
    };

    StandaloneTypeCheck.parse = function (files) {
        var typeChecker = new StandaloneTypeCheck();
        files.forEach(function (f) {
            return typeChecker.addUnit(f);
        });
        return typeChecker.typeCheck();
    };
    return StandaloneTypeCheck;
})();
exports.StandaloneTypeCheck = StandaloneTypeCheck;
