using System;
using System.IO;
using System.Linq;
using System.Xml.Xsl;

static class Program
{
	public static void Run(string xsltFile, string extension, string[] metaFiles)
	{
		var transform = new XslCompiledTransform();
		transform.Load(xsltFile);

		foreach (var metaFile in metaFiles)
		{
			var targetFile = Path.ChangeExtension(metaFile, extension);
			transform.Transform(metaFile, targetFile);
		}
	}
	public static int Main(string[] args)
	{
		if (args.Length < 3)
		{
			Console.WriteLine("Usage: htmlgen.exe <template> <extension> <metaFiles>");
			return 1;
		}

		try
		{
			Run(args[0], args[1], args.Skip(2).ToArray());
			return 0;
		}
		catch (Exception ex)
		{
			Console.Error.WriteLine(ex);
			return 1;
		}
	}
}