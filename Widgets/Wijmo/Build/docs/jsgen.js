/// <reference path="../../External/declarations/node.d.ts" />
/// <reference path="../../External/declarations/elementtree.d.ts" />
var fs = require("fs");
var util = require("./../util");
var et = require("elementtree");
var docutil = require("./../meta/docutil");

var emptyFn = "function () {}";
var xmlEscape = docutil.xml.escape;

/** Metadata-to-JavaScript converter */
var Generator = (function () {
    function Generator() {
        this.interfaceExtensions = [];
        //#region export path
        /** A stack of variable/field names representing a property path which is used to export names */
        this.exportNames = [];
        this.insideWidget = false;
    }
    /** Run a function with a name exported
    * @param append if True, the name is appended to the existing names (pushed onto the exportNames stack). Defaults to True.
    */
    Generator.prototype.exported = function (name, fn, append) {
        if (typeof append === "undefined") { append = true; }
        if (append && this.exportPath) {
            name = this.exportPath + "." + name;
        }
        this.exportNames.push(name);
        var oldPath = this.exportPath;
        this.exportPath = name;
        try  {
            fn.call(this);
        } finally {
            this.exportPath = oldPath;
            this.exportNames.pop();
        }
    };

    /** Export a name and its value */
    Generator.prototype.writeExport = function (name, value) {
        var reservedWords = ["delete"], isReserved = false, exportPath = "";
        if (reservedWords.indexOf(name) > -1) {
            isReserved = true;
        }
        if (this.exportPath) {
            exportPath = this.exportPath;
            //this._out.write(this.exportPath + ".");
        }
        if (isReserved) {
            exportPath += '["{0}"] = {1};';
        } else {
            exportPath += ".{0} = {1};";
        }
        this._out.writeLine(exportPath, name, value);
    };

    /** Write jsdoc string */
    Generator.prototype.writeDoc = function (doc) {
        doc.sort();
        var text = doc.toStringWithComment().replace(/(^|[^\r])\n/g, "$1\r\n");
        this._out.write(text);
    };

    //#endregion
    /** Convert an XML meta node to a JSDoc object */
    Generator.prototype.readDoc = function (elem) {
        var jsdoc = new docutil.JsDoc();

        var summary = elem.find("summary");
        if (summary) {
            jsdoc.title = xmlEscape(summary.text);
        }

        var remarks = elem.find("remarks");
        if (remarks) {
            jsdoc.push("remarks", xmlEscape(remarks.text));
        }

        elem.findall("example").forEach(function (ex) {
            return jsdoc.push("example", xmlEscape(ex.text));
        });

        return jsdoc;
    };

    Generator.prototype.visitDependencies = function (deps) {
        var _this = this;
        this._out.writeLine("/** Dependencies");
        deps.findall("dependency").forEach(function (dep) {
            var name = dep.get("name");
            _this._out.writeLine("* {0}", name);
        });
        this._out.writeLine("*/");
    };

    Generator.prototype.visitModule = function (mod) {
        var _this = this;
        var name = mod.get("name");
        if (this.exportPath) {
            this._out.writeLine("{0} = {0} || {};", this.exportPath + "." + name);
        } else {
            this._out.writeLine("var {0} = {0} || {};", name);
        }
        this._out.writeLine();
        this._out.writeLine("(function () {");
        this.exported(name, function () {
            return _this.visitChildren(mod);
        });
        this.interfaceExtensions.forEach(function (e) {
            return e.extendList.forEach(function (base) {
                var exists = "typeof " + base + " != 'undefined'";
                var doExtend = "$.extend(" + e.name + ", " + base + ".prototype)";
                _this._out.writeLine(exists + " && " + doExtend + ";");
            });
        });
        this.interfaceExtensions = [];
        this._out.writeLine("})()");
    };

    Generator.prototype.visitWidget = function (widget) {
        var _this = this;
        var name = widget.get("name"), baseClass = widget.get("base") || "", baseWidget = !baseClass.match(/^wijmo\./) ? baseClass : "$.wijmo." + baseClass.substr(baseClass.lastIndexOf(".") + 1).replace(/(wijmoWidget|JQueryUIWidget)/, "widget");

        this.insideWidget = true;
        try  {
            var doc = this.readDoc(widget);
            doc.push("class", name);
            doc.push("widget");
            if (this.exportPath) {
                doc.push("namespace", "jQuery." + this.exportPath);
            }
            if (baseClass) {
                doc.push("extends", baseClass);
            }
            this.writeDoc(doc);
            this.writeExport(name, emptyFn);
            if (baseClass) {
                this.writeExport(name + ".prototype", "new " + baseClass + "()");
            }
            this.exported(name + ".prototype", function () {
                return _this.visitAll(widget.findall("function"));
            });

            // options
            this._out.writeLine();
            var doc = new docutil.JsDoc();
            doc.push("class");
            this.writeDoc(doc);
            var options = name + "_options";
            this._out.writeLine("var " + options + " = " + emptyFn + ";");
            this.exported(options + ".prototype", function () {
                _this.visitAll(widget.findall("option"));
                _this.visitAll(widget.findall("event"));
            }, false);
            var optionsObject = "$.extend({}, true, ";
            if (baseClass) {
                optionsObject += baseClass + ".prototype.options, ";
            }
            optionsObject += "new " + options + "())";
            this.writeExport(name + ".prototype.options", optionsObject);

            // register widget
            var widgetDef = "$.widget(\"wijmo." + name + "\"";
            if (baseWidget) {
                widgetDef += ", " + baseWidget;
            }
            widgetDef += ", " + this.exportPath + "." + name + ".prototype);";
            this._out.writeLine(widgetDef);
        } finally {
            this.insideWidget = false;
        }
    };

    Generator.prototype.visitEnum = function (type) {
        var _this = this;
        var name = type.get("name"), doc = this.readDoc(type), memberList, idx = 0, len;

        doc.push("enum", name);
        if (this.exportPath) {
            doc.push("namespace", this.exportPath);
        }
        this.writeDoc(doc);
        if (this.exportPath) {
            this._out.write(this.exportPath + ".");
        }
        this._out.writeLine("{0} = {", name);

        memberList = type.findall("member");
        if (memberList && memberList.length > 0) {
            len = memberList.length;
            memberList.forEach(function (m, i) {
                var value = Number(type.get("value")), isLastItem = !(i === len - 1);
                if (isNaN(value)) {
                    value = idx;
                } else {
                    idx = value;
                }
                _this.visitEnumMember(m, value, isLastItem);
                idx++;
            });
        }
        this._out.writeLine("};");
    };

    Generator.prototype.visitEnumMember = function (type, value, addComma) {
        var name = type.get("name"), doc = this.readDoc(type);

        this.writeDoc(doc);
        this._out.writeLine("{0}: {1}{2}", name, value + "", addComma ? "," : " ");
    };

    Generator.prototype.visitClassOrInterface = function (type, isInterface) {
        var _this = this;
        var name = type.get("name"), base = type.get("base"), extendList;

        if (isInterface) {
            extendList = type.findall("extends").map(function (e) {
                return e.text;
            });
        } else if (base) {
            extendList = [type.get("base")];
        }

        var doc = this.readDoc(type);
        doc.push(isInterface ? "interface" : "class", name);
        if (this.exportPath) {
            doc.push("namespace", this.exportPath);
        }

        if (extendList) {
            extendList.forEach(function (base) {
                return doc.push("extends", base);
            });
        }

        this.writeDoc(doc);
        this.writeExport(name, emptyFn);

        if (!isInterface && base) {
            this.writeExport(name + ".prototype", "new " + base + "()");
        }

        this.exported(name + ".prototype", function () {
            if (isInterface && extendList.length > 0) {
                _this.interfaceExtensions.push({
                    name: _this.exportPath,
                    extendList: extendList
                });
            }
            _this.visitChildren(type);
        });
    };
    Generator.prototype.visitInterface = function (type) {
        this.visitClassOrInterface(type, true);
    };
    Generator.prototype.visitClass = function (type) {
        this.visitClassOrInterface(type, false);
    };

    Generator.prototype.visitParamsTo = function (target, container) {
        container.findall("param").forEach(function (p) {
            return target.pushParam(p.get("name"), p.get("type"), xmlEscape(p.text));
        });
        var returns = container.find("returns");
        if (returns) {
            target.pushReturns(returns.get("type"), xmlEscape(returns.text));
        }
    };
    Generator.prototype.visitFunction = function (func) {
        var name = func.get("name"), doc = this.readDoc(func);

        // If name is not defined, the function is the constructor.
        if (!name) {
            name = "constructor";
        }
        this.visitParamsTo(doc, func);
        this.writeDoc(doc);
        var paramNames = func.findall("param").map(function (p) {
            return p.get("name");
        }).join(", ");
        var fnDef = "function (" + paramNames + ") {}";
        this.writeExport(name, fnDef);
    };
    Generator.prototype.visitOption = function (option) {
        this.visitProperty(option, function (d) {
            return d.push("option");
        });
    };
    Generator.prototype.resolveTypeUrl = function (type) {
        var trailingBrackets = type.match(/(\[\])+$/);
        if (trailingBrackets) {
            type = type.substr(0, type.length - trailingBrackets.length);
        }
        return "Wijmo~" + type + ".html";
    };
    Generator.prototype.visitProperty = function (prop, extendDoc) {
        var name = prop.get("name"), type = prop.get("type"), defaultValue = prop.get("default"), doc = this.readDoc(prop), observable = (prop.get("observable") || "false").match(/true/i), prefixHtml = "";

        if (/^wijmo\./.exec(type)) {
            // A workaround for DX bug:
            // temporarily insert link to to type
            prefixHtml += "<p class='widgetType'>Type: <a href='" + this.resolveTypeUrl(type) + "'>" + type + "</a></p>\n";
        }

        if (defaultValue) {
            prefixHtml += "<p class='defaultValue'>Default value: " + docutil.xml.escape(defaultValue) + "</p>\n";
        }

        doc.title = prefixHtml + "<p>" + doc.title + "</p>";
        doc.push("field");
        if (type) {
            doc.push("type", "{" + type + "}");
        }

        //add params to option
        this.visitParamsTo(doc, prop);
        if (observable) {
            doc.push("accessor");
            doc.push("observable");
        }
        if (extendDoc) {
            extendDoc(doc);
        }
        this.writeDoc(doc);
        this.writeExport(name, defaultValue || "null");
    };

    Generator.prototype.visitEvent = function (event) {
        var name = event.get("name"), doc = this.readDoc(event);
        doc.push("event");
        this.visitParamsTo(doc, event);

        this.writeDoc(doc);

        this.writeExport(name, "null");
    };

    Generator.prototype.visit = function (xElement) {
        var method = "visit" + xElement.tag.charAt(0).toUpperCase() + xElement.tag.substr(1);
        if (this[method]) {
            this[method](xElement);
        } else {
            //throw new Error("Cannot process " + xElement.tag + " element");
        }
    };
    Generator.prototype.visitAll = function (elements) {
        var _this = this;
        elements.forEach(function (e) {
            return _this.visit(e);
        });
    };
    Generator.prototype.visitChildren = function (parent) {
        this.visitAll(parent.getchildren());
    };

    /** Read a .ts.meta file and generate JavaScript */
    Generator.prototype.generate = function (srcFile, out) {
        var _this = this;
        this._out = out;
        try  {
            var xUnit = et.parse(fs.readFileSync(srcFile, "utf-8")), root = xUnit.getroot(), dependencies = root.find("dependencies");
            if (dependencies) {
                this.visitDependencies(root.find("dependencies"));
            }
            root.findall("module").forEach(function (xModule) {
                return _this.visitModule(xModule);
            });
        } finally {
            this._out = null;
        }
    };
    return Generator;
})();
exports.Generator = Generator;

function main() {
    try  {
        if (process.argv.length < 2) {
            throw new Error("File not specified.\nUsage: node jsgen.js <filename>");
        }

        var filename = process.argv[2];

        // create output as a file with postfix .ts.meta.js
        var output = new util.FileCodeWriter(filename + ".js");
        try  {
            // convert a .ts.meta file to .ts.meta.js
            new Generator().generate(filename, output);
        } finally {
            output.close();
        }
        return 0;
    } catch (err) {
        console.log(err);
        return 1;
    }
}

if (require.main === module) {
    process.exit(main());
}
