﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="summary">
    <div class="wijdoc-summary">
      <p>
        <xsl:value-of select="self::node()"/>
      </p>
    </div>
  </xsl:template>

  <xsl:template name="type">
    <xsl:choose>
      <xsl:when test="@type">
        <xsl:value-of select="@type"/>
      </xsl:when>
      <xsl:otherwise>Any</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="param">
    <li class="wijdoc-param">
      <h5><xsl:value-of select="@name"/></h5>

      <div class="wijdoc-type">Type: <xsl:call-template name="type"/></div>

      <div class="wijdoc-summary">
        <p>
          <xsl:value-of select="self::node()"/>
        </p>
      </div>
    </li>
  </xsl:template>

  <xsl:template match="method">
    <xsl:variable name="signature">
      <xsl:value-of select="@name"/>
      <xsl:text>(</xsl:text>
      <xsl:for-each select="param">
        <xsl:value-of select="@name"/>
        <xsl:if test="following-sibling::param">, </xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
    </xsl:variable>

    <li class="wijdoc-method">
      <h4><xsl:value-of select="$signature"/></h4>
      <xsl:apply-templates select="summary"/>

      <xsl:if test="param">
        <div class="wijdoc-param-list">
          <h5>Parameters:</h5>
          <ul>
            <xsl:apply-templates select="param"/>
          </ul>
        </div>
      </xsl:if>
    </li>
  </xsl:template>

  <xsl:template name="method-list">
    <xsl:if test="method[@name]">
      <div class="wijdoc-method-list">
        <h3>Methods:</h3>
        <ul>
          <xsl:apply-templates select="method[@name]"/>
        </ul>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="property">
    <li class="wijdoc-property">
      <h4><xsl:value-of select="@name"/></h4>
      <div class="wijdoc-type">
        <xsl:text>Type: </xsl:text>
        <xsl:call-template name="type"/>
      </div>

      <xsl:apply-templates select="summary"/>
    </li>
  </xsl:template>

  <xsl:template name="property-list">
    <xsl:if test="property">
      <div class="wijdoc-property-list">
        <h3>Properties:</h3>
        <ul>
          <xsl:apply-templates select="property"/>
        </ul>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="interface">
    <li class="wijdoc-interface">
      <h2>Interface <xsl:value-of select="@name"/></h2>
      <xsl:apply-templates select="summary"/>
      <xsl:call-template name="method-list"/>
      <xsl:call-template name="property-list"/>
    </li>
  </xsl:template>

  <xsl:template match="class">
    <li class="wijdoc-class">
      <h2>Class <xsl:value-of select="@name"/></h2>
      <xsl:apply-templates select="summary"/>
      <xsl:call-template name="method-list"/>
      <xsl:call-template name="property-list"/>
    </li>
  </xsl:template>

  <xsl:template match="option">
    <li class="wijdoc-widget-option">
      <h4><xsl:value-of select="@name"/></h4>
      <xsl:apply-templates select="summary"/>

      <xsl:if test="@type">
        <div class="wijdoc-type">Type: <xsl:call-template name="type"/></div>
      </xsl:if>

      <xsl:if test="@default">
        <div class="wijdoc-default">Default: <xsl:value-of select="@value"/></div>
      </xsl:if>
    </li>
  </xsl:template>

  <xsl:template name="option-list">
    <xsl:if test="option">
      <div class="wijdoc-widget-option-list">
        <h3>Options:</h3>
        <ul>
          <xsl:apply-templates select="option"/>
        </ul>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="event">
    <li class="wijdoc-widget-event">
      <h4><xsl:value-of select="@name"/></h4>
      <xsl:apply-templates select="summary"/>

      <div class="wijdoc-param-list">
        <h5>Parameters:</h5>
        <ul>
          <li class="wijdoc-param wijdoc-param-jQuery-event">
            <h5>e</h5>
            <div class="wijdoc-param-type">Type: jQuery.Event</div>

            <div class="wijdoc-summary">
              <p>jQuery Event object</p>
            </div>
          </li>
      
          <xsl:if test="dataKey">
            <li class="wijdoc-param wijdoc-param-jQuery-event">
              <h5>data</h5>
              <div class="wijdoc-param-type">Type: Object</div>
              <div class="wijdoc-summary">
                <p>Event data</p>
              </div>
              <ul class="wijdoc-param-data-key-list">
            
                <xsl:for-each select="dataKey">
                  <li class="wijdoc-event-data-key">
                    <h3>
                      <xsl:text>data.</xsl:text>
                      <xsl:value-of select="@name"/>
                    </h3>
                    <span class="wijdoc-type">
                      <xsl:text>: </xsl:text>
                      <xsl:call-template name="type"/>
                    </span>
                    <xsl:text>. </xsl:text>
                    <p>
                      <xsl:value-of select="self::*"/>                  
                    </p>
                  </li>
                </xsl:for-each>
            
              </ul>
            </li>
          </xsl:if>
        </ul>
      </div>
    </li>
  </xsl:template>

  <xsl:template name="event-list">
    <xsl:if test="event">
      <div class="wijdoc-widget-event-list">
        <h3>Events:</h3>
        <ul>
          <xsl:apply-templates select="event"/>
        </ul>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="widget">
    <li class="wijdoc-widget">
      <h2>Widget <xsl:value-of select="@name"/></h2>
      <xsl:apply-templates select="summary"/>
      <xsl:call-template name="method-list"/>
      <xsl:call-template name="option-list"/>
      <xsl:call-template name="event-list"/>
    </li>
  </xsl:template>

  <xsl:template match="module">
    <xsl:param name="prefix"/>
    
    <xsl:if test="*[not(self::module)]">
      <div class="wijdoc-module">
        <h2>
          <xsl:text>Module </xsl:text>
          <xsl:value-of select="$prefix"/>
          <xsl:value-of select="@name"/>
        </h2>
        <ul>
          <xsl:apply-templates select="*[not(self::module)]"/>
        </ul>
      </div>
    </xsl:if>

    <xsl:apply-templates select="module">
      <xsl:with-param name="prefix">
        <xsl:value-of select="$prefix"/>
        <xsl:value-of select="@name"/>
        <xsl:text>.</xsl:text>
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="/unit">
    <html>
      <head>
        <title>
          <xsl:value-of select="@name"/>
        </title>
      </head>
      <body>
        <ul>
          <xsl:for-each select="module">
            <li>
              <xsl:apply-templates select="self::*" />
            </li>
          </xsl:for-each>
        </ul>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
