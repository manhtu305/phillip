﻿var path = require("path");
var util = require("./util");

function registerTasks(grunt, wijmoOpen, wijmoPro) {
    var copyJs = "copyJs";
    grunt.registerTask(copyJs, function () {
        var desFolder = "../ExportService/ExportService/Scripts", config = grunt.file.readJSON("package.json"), dependencies = config.dependencies, jQueryVer = dependencies.jquery, jQueryUIVer = dependencies["jquery-ui"], externalFolder = "./Wijmo/External", jQueryPath = path.join(externalFolder, "jquery-" + jQueryVer + ".min.js"), jQueryUIPath = path.join(externalFolder, "jquery-ui-" + jQueryUIVer + ".custom.min.js"), globalizePath = path.join(externalFolder, "globalize.min.js"), culturePath = path.join(externalFolder, "cultures/globalize.cultures.js");

        grunt.file.copy(wijmoOpen.combinedJsFile(true), path.join(desFolder, "jquery.wijmo-open.all.min.js"));
        grunt.file.copy(wijmoPro.combinedJsFile(true), path.join(desFolder, "jquery.wijmo-pro.all.min.js"));
        grunt.file.copy(jQueryPath, path.join(desFolder, "jquery.min.js"));
        grunt.file.copy(jQueryUIPath, path.join(desFolder, "jquery-ui.min.js"));
        grunt.file.copy(globalizePath, path.join(desFolder, "globalize.min.js"));
        grunt.file.copy(culturePath, path.join(desFolder, "globalize.cultures.js"));
    });

    function copyCssFile(src, dest) {
        var css = util.readFile(src);
        grunt.file.write(dest, css);
    }

    function copyFolder(src, dest) {
        var pattern = path.join(src, "**/*.*");
        grunt.file.expand(pattern).forEach(function (filename) {
            var targetFilename = path.join(dest, filename.substr(src.length));
            if (filename.match(/\.css$/i)) {
                copyCssFile(filename, targetFilename);
            } else {
                grunt.file.copy(filename, targetFilename);
            }
        });
    }

    var resourcesFolder = "../ExportService/ExportService/Styles";
    var targetThemesFolder = path.join(resourcesFolder, "themes");
    var themesTask = "themes";

    grunt.registerTask(themesTask, function () {
        [wijmoOpen, wijmoPro].forEach(function (p) {
            var targetFile = path.join(targetThemesFolder, "/wijmo/jquery.wijmo-" + p.name.toLowerCase() + ".css");
            copyCssFile(p.combinedCssFile(), targetFile);
        });

        copyFolder(path.join(wijmoPro.dist, "css/images"), path.join(targetThemesFolder, "wijmo/images"));

        //copy boot strap css.
        var src_bootstrap = path.join(wijmoPro.dist, "css/interop/bootstrap-wijmo.css"), dest_bootstrap = path.join(targetThemesFolder, "wijmo/jquery.wijmo-bootstrap.Controls.css");
        copyCssFile(src_bootstrap, dest_bootstrap);

        // Copy themes
        grunt.file.expand(path.join(wijmoPro.dist, "themes/*")).forEach(function (themeDir) {
            var themeName = path.basename(themeDir);
            copyFolder(themeDir, path.join(targetThemesFolder, themeName));
        });
    });

    grunt.registerTask("exporter", ["wijmo-release", copyJs, "copy:themes", themesTask]);
}
exports.registerTasks = registerTasks;
