﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/// <reference path="../External/declarations/node.d.ts" />
var path = require("path");

var fs = require("fs");

/** Analog of .NET TextWriter but with support of indentation */
var CodeWriter = (function () {
    function CodeWriter() {
        //#region indentation
        this._indentLevel = 0;
        this._indentString = "";
    }
    CodeWriter.prototype._indentStringFor = function (indent) {
        var spaces = indent * 4;
        return new Array(spaces + 1).join(" ");
    };
    CodeWriter.prototype.indented = function (fn) {
        this._indentLevel++;
        this._indentString = this._indentStringFor(this._indentLevel);
        try  {
            this._write(this._indentStringFor(1));
            fn.call(this);
        } finally {
            this._indentLevel--;
            this._indentString = this._indentStringFor(this._indentLevel);
        }
    };

    //#endregion
    CodeWriter.prototype._write = function (text) {
    };
    CodeWriter.prototype.write = function (format) {
        var args = [];
        for (var _i = 0; _i < (arguments.length - 1); _i++) {
            args[_i] = arguments[_i + 1];
        }
        var text = format.replace(/{(\d+)}/g, function (m) {
            //If args are not exist, don't replace {0} to undefined.
            var val = args[parseInt(m[1])];
            return val ? val : m;
        });
        if (this._indentString) {
            text = text.replace(/(\r?\n)/mg, "$1" + this._indentString);
        }
        this._write(text);
    };
    CodeWriter.prototype.writeLine = function (format) {
        var args = [];
        for (var _i = 0; _i < (arguments.length - 1); _i++) {
            args[_i] = arguments[_i + 1];
        }
        if (format) {
            this.write.apply(this, arguments);
        }
        this.write("\r\n");
    };
    return CodeWriter;
})();
exports.CodeWriter = CodeWriter;

/** A file-based CodeWriter */
var FileCodeWriter = (function (_super) {
    __extends(FileCodeWriter, _super);
    function FileCodeWriter(filename) {
        _super.call(this);
        if (fs.existsSync(filename) && !exports.isWritableSync(filename)) {
            throw new Error("File is read-only");
        }
        this._fd = fs.openSync(filename, "w");
    }
    FileCodeWriter.prototype._write = function (text) {
        var buf = new Buffer(text);
        fs.writeSync(this._fd, buf, 0, buf.length, null);
    };
    FileCodeWriter.prototype.close = function () {
        fs.closeSync(this._fd);
    };
    return FileCodeWriter;
})(CodeWriter);
exports.FileCodeWriter = FileCodeWriter;

exports.stats = { writable: 0x80 };

function makeWritableSync(filename) {
    var stat = fs.statSync(filename);
    if ((stat.mode & exports.stats.writable) !== exports.stats.writable) {
        fs.chmodSync(filename, stat.mode | exports.stats.writable);
    }
}
exports.makeWritableSync = makeWritableSync;

function isWritableSync(filename) {
    var stat = fs.statSync(filename);
    return (stat.mode && exports.stats.writable) === exports.stats.writable;
}
exports.isWritableSync = isWritableSync;

function rm(name) {
    if (!fs.existsSync(name))
        return;
    if (fs.statSync(name).isFile()) {
        fs.unlinkSync(name);
    } else {
        fs.readdirSync(name).forEach(function (child) {
            return exports.rm(path.join(name, child));
        });
        fs.rmdirSync(name);
    }
}
exports.rm = rm;

function toRegex(text) {
    var escaped = text.replace(/[\.\(\)]/g, "\\$&");
    return new RegExp(escaped, "g");
}
exports.toRegex = toRegex;

function readFile(filename) {
    var contents = fs.readFileSync(filename, "utf8");
    contents = contents.replace(/\uFEFF/g, "");
    if (contents.length > 0 && contents.charCodeAt(0) == 65279) {
        contents = contents.substr(1);
    }
    return contents;
}
exports.readFile = readFile;
