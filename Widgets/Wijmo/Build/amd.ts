/// <reference path="../External/declarations/node.d.ts" />
import path = require("path");
import child_process = require("child_process");
import fs = require("fs");

export function registerTasks(grunt, root) {
    "use strict";

    var amdJsFolder = path.join(root, "amd-js");

    var moduleNames = {
        grid: "wijmo.wijgrid",
        data: "wijmo.data",
		maps:"wijmo.wijmaps"
    };

    /** Read an AMD module name from a filename */
    function getModuleName(filename: string) {
		if (filename.match(/data\/src\//i)) {
			return moduleNames.data;
		} else if (filename.match(/wijgrid\//)) {
			return moduleNames.grid;
		} else if (filename.match(/wijmaps\//)) { 
			return moduleNames.maps;
		}

        filename = path.basename(filename).replace(/\.js$/, "");
        filename = filename.replace(/^jquery\.wijmo/, "wijmo");
        filename = filename.replace(/^jquery\.plugin\.wij/, "wijmo.wij");

        if (filename == "jquery.ui") {
            filename = "jquery-ui";
        }

        return filename;
    }

    /** Find AMD module dependencies from references in code */
    function findDependencies(code: string, moduleName: string) {
        var refRgx = /\/{3}\s+<reference path="([^"]+)"\s?\/>/g,
            isGrid = moduleName == moduleNames.grid,
            isData = moduleName == moduleNames.data,
			isMaps = moduleName == moduleNames.maps,
            isDataPlugin = !isData && moduleName.indexOf(moduleNames.data) == 0,
            deps: string[] = [];

        for (var m = refRgx.exec(code); m; m = refRgx.exec(code)) {
            var dep = m[1].replace(/(\.d)?\.ts$/, "");
            var upLevelMatch = dep.match(/^(\.\.\/)+/);
            var upLevel = upLevelMatch ? (upLevelMatch[0].length / 3) : 0;

            if (((isGrid||isMaps) && upLevel < 2) || (isData && upLevel == 0)) continue;

			//skip geojson in maps
			if (isMaps && dep.match("geojson")) { continue; }

            if (isGrid && dep.match(/\/data\//i) || isDataPlugin && upLevel == 0) {
                dep = moduleNames.data;
            }

            dep = getModuleName(dep);
            if (deps.indexOf(dep) < 0 && dep != "wijmo" && dep != moduleName && dep != "jquerymobile") {
                if(dep.match(/wijmo/)) {
                    //Makes dependencies load in relative to module, regardless of baseUrl.
                    dep = "./" + dep;
                }
                deps.push(dep);
            }
        }

		// remove repeat items.
		return deps.filter((val, index) => { 
			return deps.indexOf(val) == index;
		});

        //return deps;
    }

    /** Wrap code with define() module definition */
    function transformCode(code: string, moduleName: string) {
        // remove global wijmo variable
        code = code.replace(/var wijmo;\r?\n/g, "");
        var codeStart = /^\/\*[\s\S]+?\*\//.exec(code);
        var codeStartPos = codeStart ? codeStart[0].length : 0;

        var preamble = "var wijmo;\r\n" +
                       "define([";
        preamble += findDependencies(code, moduleName).map(d => '"' + d + '"').join(", ");
        preamble += "], function () { \r\n";

        var epilogue = "});\r\n";

        code = code.substring(0, codeStartPos) + "\r\n" + preamble + code.substr(codeStartPos) + epilogue;
        return code;
    }

    /** Generate module files */
    function copyModules() {
        var jsFiles = grunt.file.expand(path.join(root, "Wijmo/**/*.js"));

        jsFiles.forEach(js => {
            if (!js.match(/external\//) && js.match(/wijmo.+\.min\.js/)) return;

            var moduleName = getModuleName(js);
            var code = grunt.file.read(js);

            if (!js.match(/(Wijmo\/external\/|knockout\.wijmo|bootstrap\.wijmo)/)) {
                code = transformCode(code, moduleName);
                code = grunt.helper('uglify', code, grunt.config('uglify'));
            }

            var targetName = path.join(amdJsFolder, moduleName + ".js");
            grunt.file.write(targetName, code);
        });
    }

    // register amd-js grunt task that generates module files
    grunt.registerTask("amd-js", function () {
        copyModules();
    });

    grunt.registerTask("amd", "wijmo copy:wijmo amd-js");
}