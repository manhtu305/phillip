/// <reference path="../External/declarations/node.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/** The main class in this file is WijmoPackage. There are only two instances of this class created,
* one for wijmo-open and one for wijmo-pro. Other classes are used by WijmoPackage
*/
var path = require("path");
var fs = require("fs");
var util = require("./util");

var extend = require("node.extend"), thisModule = eval("module");

/** A base class for project files, such as .ts and .css */
var ProjectFile = (function () {
    function ProjectFile(path) {
        this.path = path;
    }
    return ProjectFile;
})();
exports.ProjectFile = ProjectFile;

/** A project file with code */
var CodeFile = (function (_super) {
    __extends(CodeFile, _super);
    function CodeFile(path, combine) {
        _super.call(this, path);
        this.combine = combine;
    }
    return CodeFile;
})(ProjectFile);
exports.CodeFile = CodeFile;

/** A .ts file */
var ScriptFile = (function (_super) {
    __extends(ScriptFile, _super);
    /** @param path Path to .js file
    * @param combine True if the file must be combined
    */
    function ScriptFile(path, combine) {
        _super.call(this, path, combine);
        this.path = path;
        this.combine = combine;
        this.min = path.replace(/\.js$/, '.min.js');

        // check if there is a .ts file
        var ts = path.replace(/\.js$/, '.ts');
        if (fs.existsSync(ts)) {
            this.ts = ts;
        }
    }
    /** A file is considered external if it is in the external folder */
    ScriptFile.prototype.isExternal = function () {
        return !!this.path.match(/^wijmo[\/\\]external/i);
    };
    return ScriptFile;
})(CodeFile);
exports.ScriptFile = ScriptFile;

/** A css file */
var StyleFile = (function (_super) {
    __extends(StyleFile, _super);
    function StyleFile() {
        _super.apply(this, arguments);
    }
    return StyleFile;
})(CodeFile);
exports.StyleFile = StyleFile;

/** A class representing wijmo-open or wijmo-pro */
var WijmoPackage = (function () {
    /** @param pkg The contents of the package.json file
    * @param name Name of the package, such as Open or Pro
    * @param dist Path to the distribution folder
    */
    function WijmoPackage(grunt, pkg, name, dist) {
        this.grunt = grunt;
        this.pkg = pkg;
        this.name = name;
        this.dist = dist;
        var dir = "Wijmo/Build/Wijmo-" + name;

        /** Reads a list file containing filenames, one on each line
        * @param file Path to the list file
        * @param folder Folder to be prepended to the filenames
        */
        function readListFile(file, folder) {
            file = path.join(dir, file);
            if (!fs.existsSync(file)) {
                return [];
            }
            return util.readFile(file).replace("\r", "").split("\n").map(function (f) {
                return f.trim();
            }).filter(function (f) {
                return f.length > 0;
            }).map(function (f) {
                return path.join(folder, f);
            });
        }

        // read script file list
        this.scripts = readListFile("wijmo.js.csv", "Wijmo").map(function (line) {
            var entries = line.split(/,\s*/);
            return new ScriptFile(entries[0], entries[1] == null || entries[1].toLowerCase() != "false");
        });

        // filter external files out
        this.internalScripts = this.scripts.filter(function (s) {
            return !s.isExternal();
        });

        // read css file list
        this.styles = readListFile("wijmo.css.lst", "Wijmo").map(function (path) {
            var combine = !path.match(/interop\\/);
            return new StyleFile(path, combine);
        });

        // read other file list
        this.files = readListFile("wijmo.lst", "Wijmo").map(function (p) {
            return new ProjectFile(p);
        });

        // read copyright
        this.copyrightFile = path.join(dir, "copyright.txt");
        this.copyright = util.readFile(this.copyrightFile);
    }
    /** Add -name suffix to properties in a given object. name is the package name  */
    WijmoPackage.prototype.addSuffix = function (obj) {
        var result = {}, p;
        for (p in obj) {
            result[p + "-" + this.name] = obj[p];
        }
        return result;
    };

    /** Add -name suffix to all properties of properties in a given object */
    WijmoPackage.prototype.addChildSuffixes = function (obj) {
        var p;
        for (p in obj) {
            obj[p] = this.addSuffix(obj[p]);
        }
        return obj;
    };

    /** Returns a new object with a property named after the package, with a value of a given object */
    WijmoPackage.prototype.wrapWithName = function (obj) {
        var result = {};
        result[this.name] = obj;
        return result;
    };

    /** Returns full filename of the combined .css file */
    WijmoPackage.prototype.combinedCssFile = function () {
        return this.dist + "/css/jquery.wijmo-" + this.name + "." + this.pkg.version + ".css";
    };

    /** Returns full filename of the combined .js file */
    WijmoPackage.prototype.combinedJsFile = function (min) {
        if (typeof min === "undefined") { min = false; }
        var path = this.dist + "/js/jquery.wijmo-" + this.name + ".all." + this.pkg.version;
        if (min) {
            path += ".min";
        }
        path += ".js";
        return path;
    };

    // return the bootstrap file
    WijmoPackage.prototype.bootstrapWijmoJSFile = function (min) {
        if (typeof min === "undefined") { min = false; }
        var path = this.dist + "/js/interop/bootstrap.wijmo." + this.pkg.version;
        if (min) {
            path += ".min";
        }
        path += ".js";
        return path;
    };

    //amdify(code: string) {
    //    var varWijmo = code.replace(/)
    //}
    /** Adds file header */
    WijmoPackage.prototype.processWidgetScript = function (code, filename) {
        if (!filename.match(/\.min\.js$/) && filename.match(/\.js$/)) {
            code = this.copyright + "\r\n" + code;
        }
        return code;
    };

    /** Returns a grunt config for this package */
    WijmoPackage.prototype.getConfig = function () {
        var _this = this;
        return {
            meta: this.addSuffix({
                banner: this.copyright
            }),
            // combine css
            concat: this.addSuffix({
                css: {
                    src: this.styles.filter(function (s) {
                        return s.combine;
                    }).map(function (s) {
                        return s.path;
                    }),
                    dest: this.combinedCssFile()
                }
            }),
            // minify non-external scripts
            mineach: this.wrapWithName({
                src: this.internalScripts.map(function (s) {
                    return s.path;
                })
            }),
            // copy all package files
            copy: this.addSuffix({
                wijmo: {
                    src: this.internalScripts.map(function (s) {
                        return s.path;
                    }).concat(this.internalScripts.map(function (s) {
                        return s.min;
                    })).concat(this.styles.map(function (s) {
                        return s.path;
                    })).concat(this.files.map(function (s) {
                        return s.path;
                    })),
                    // add header
                    process: function (code, filename) {
                        return _this.processWidgetScript(code, filename);
                    },
                    dest: this.dist
                }
            })
        };
    };

    WijmoPackage.prototype._toRegex = function (text) {
        var escaped = text.replace(/[\.\(\)]/g, "\\$&");
        return new RegExp(escaped, "g");
    };

    /** Converts C1.Wijmo.Controls-specific css files to C1.Wijmo.Extenders-specific */
    WijmoPackage.prototype.convertCssToExtenders = function (controlCssFile, namespace) {
        if (typeof namespace === "undefined") { namespace = "Extenders"; }
        var content = util.readFile(controlCssFile);
        content = content.replace(util.toRegex("WebResource(\"C1.Web.Wijmo.Controls.Resources"), "WebResource(\"C1.Web.Wijmo." + namespace + ".Resources");

        var targetFileName = controlCssFile.replace("Controls", namespace);
        this.grunt.file.write(targetFileName, content);
    };

    /** Registers grunt tasks */
    WijmoPackage.prototype.init = function (grunt) {
        var _this = this;
        // Expand variables in copyright text
        this.copyright = grunt.template.process(this.copyright);

        // replaces % with package name
        var addName = function (s) {
            return s.replace(/%/g, _this.name);
        };

        // Register a grunt task. For name and tasks addName is used
        var registerTask = function (name, tasks) {
            grunt.registerTask(addName(name), addName(tasks));
        };

        // Registers a concatenation task
        var concatTask = function (name, min) {
            grunt.registerTask(name, function () {
                // start with copyright
                var contents = _this.copyright + "\r\n";

                // then add external files
                contents += _this.scripts.filter(function (s) {
                    return s.isExternal() && fs.existsSync(s.path);
                }).map(function (f) {
                    return util.readFile(f.path);
                }).join(";\r\n");

                // then non-external files
                contents += _this.internalScripts.filter(function (s) {
                    return s.combine;
                }).map(function (f) {
                    return util.readFile(min ? f.min : f.path);
                }).join(";\r\n");

                // write the result
                grunt.file.write(_this.combinedJsFile(min), contents);

                // copy interop files
                _this.internalScripts.forEach(function (s) {
                    if (s.combine)
                        return;
                    var contents = _this.copyright + "\r\n";
                    contents += util.readFile(min ? s.min : s.path);

                    // include version number
                    var dest = path.basename(s.path).replace(/\.js$/, "." + _this.pkg.version + (min ? ".min" : "") + ".js");

                    // copy to the interop folder
                    dest = path.join(_this.dist + "/js/interop", dest);
                    grunt.file.write(dest, contents);
                });
            });
        };

        grunt.registerTask("interop-css-" + this.name, function () {
            _this.styles.forEach(function (s) {
                if (!s.path.match(/interop\\/)) {
                    return;
                }

                var targetName = path.join(_this.dist, "css/interop/" + path.basename(s.path));
                grunt.file.copy(s.path, targetName);
            });
        });

        concatTask("js-" + this.name, false);
        concatTask("js-min-" + this.name, true);
    };
    return WijmoPackage;
})();
exports.WijmoPackage = WijmoPackage;
