/// <reference path="../External/declarations/node.d.ts" />

/** The main class in this file is WijmoPackage. There are only two instances of this class created,
  * one for wijmo-open and one for wijmo-pro. Other classes are used by WijmoPackage
 */

import path = require("path");
import fs = require("fs");
import util = require("./util");

var extend = require("node.extend"),
    thisModule = eval("module"); // module is a keyword in TypeScript


/** A base class for project files, such as .ts and .css */
export class ProjectFile {
    constructor(public path: string) { }
}
/** A project file with code */
export class CodeFile extends ProjectFile {
    constructor(path: string, public combine: boolean) {
        super(path);
    }
}
/** A .ts file */
export class ScriptFile extends CodeFile {
    /** .ts file path */
    ts: string;
    /** Minified .js file path */
    min: string;

    /** @param path Path to .js file
      * @param combine True if the file must be combined
      */
    constructor(public path: string, public combine: boolean) {
        super(path, combine);
        this.min = path.replace(/\.js$/, '.min.js');

        // check if there is a .ts file
        var ts = path.replace(/\.js$/, '.ts');
        if (fs.existsSync(ts)) {
            this.ts = ts;
        }
    }

    /** A file is considered external if it is in the external folder */
    isExternal() {
        return !!this.path.match(/^wijmo[\/\\]external/i);
    }
}

/** A css file */
export class StyleFile extends CodeFile { }

/** A class representing wijmo-open or wijmo-pro */
export class WijmoPackage {
    scripts: ScriptFile[];
    /** Same as scripts, but without external files */
    internalScripts: ScriptFile[];
    styles: StyleFile[];
    /** Other files (non-scripts and non-css) */
    files: ProjectFile[];
    /** Path to the copyright file used as a header in all distributed files */
    copyrightFile: string;
    /** The copyright file contents */
    copyright: string;

    /** @param pkg The contents of the package.json file
      * @param name Name of the package, such as Open or Pro
      * @param dist Path to the distribution folder
      */
    constructor(public grunt, public pkg, public name: string, public dist: string) {
        var dir = "Wijmo/Build/Wijmo-" + name;

        /** Reads a list file containing filenames, one on each line
          * @param file Path to the list file
          * @param folder Folder to be prepended to the filenames
          */
        function readListFile(file: string, folder: string) {
            file = path.join(dir, file);
            if (!fs.existsSync(file)) {
                return [];
            }
            return util.readFile(file)
                .replace("\r", "").split("\n")
                .map(f => f.trim()).filter(f => f.length > 0)
                .map(f => path.join(folder, f));
        }

        // read script file list
        this.scripts = readListFile("wijmo.js.csv", "Wijmo").map(line => {
            var entries = line.split(/,\s*/);
            return new ScriptFile(entries[0],
                entries[1] == null || entries[1].toLowerCase() != "false"
            );
        });
        // filter external files out
        this.internalScripts = this.scripts.filter(s => !s.isExternal());

        // read css file list
        this.styles = readListFile("wijmo.css.lst", "Wijmo").map(path => {
            var combine = !path.match(/interop\\/);
            return new StyleFile(path, combine);
        });

        // read other file list
        this.files = readListFile("wijmo.lst", "Wijmo").map(p => new ProjectFile(p));

        // read copyright
        this.copyrightFile = path.join(dir, "copyright.txt");
        this.copyright = util.readFile(this.copyrightFile);
    }

    /** Add -name suffix to properties in a given object. name is the package name  */
    addSuffix(obj) {
        var result = {},
            p;
        for (p in obj) {
            result[p + "-" + this.name] = obj[p];
        }
        return result;
    }
    /** Add -name suffix to all properties of properties in a given object */
    addChildSuffixes(obj) {
        var p;
        for (p in obj) {
            obj[p] = this.addSuffix(obj[p]);
        }
        return obj;
    }
    /** Returns a new object with a property named after the package, with a value of a given object */
    wrapWithName(obj) {
        var result = {};
        result[this.name] = obj;
        return result;
    }

    /** Returns full filename of the combined .css file */
    combinedCssFile() {
        return this.dist + "/css/jquery.wijmo-" + this.name + "." + this.pkg.version + ".css";
    }
    /** Returns full filename of the combined .js file */
    combinedJsFile(min = false) {
        var path = this.dist + "/js/jquery.wijmo-" + this.name + ".all." + this.pkg.version;
        if (min) {
            path += ".min";
        }
        path += ".js";
        return path;
    }

	// return the bootstrap file
    bootstrapWijmoJSFile(min = false) {
    	var path = this.dist + "/js/interop/bootstrap.wijmo." + this.pkg.version;
    	if (min) {
    		path += ".min";
    	}
    	path += ".js";
    	return path;
    }


    //amdify(code: string) {
    //    var varWijmo = code.replace(/)
    //}

    /** Adds file header */
    processWidgetScript(code: string, filename: string) {
        if (!filename.match(/\.min\.js$/) && filename.match(/\.js$/)) {
            code = this.copyright + "\r\n" + code;
        }
        return code;
    }

    /** Returns a grunt config for this package */
    getConfig() {
        return {
            meta: this.addSuffix({
                banner: this.copyright
            }),
            // combine css
            concat: this.addSuffix({
                css: {
                    src: this.styles.filter(s => s.combine).map(s => s.path),
                    dest: this.combinedCssFile()
                }
            }),

            // minify non-external scripts
            mineach: this.wrapWithName({
                src: this.internalScripts.map(s => s.path)
            }),

            // copy all package files
            copy: this.addSuffix({
                wijmo: {
                    src: this.internalScripts.map(s => s.path)
                        .concat(this.internalScripts.map(s => s.min))
                        .concat(this.styles.map(s => s.path))
                        .concat(this.files.map(s => s.path)),

                    // add header
                    process: (code, filename) => {
                        return this.processWidgetScript(code, filename);
                    },
                    dest: this.dist
                }
            })
        };
    }

    _toRegex(text: string) {
        var escaped = text.replace(/[\.\(\)]/g, "\\$&");
        return new RegExp(escaped, "g");
    }

    /** Converts C1.Wijmo.Controls-specific css files to C1.Wijmo.Extenders-specific */
    convertCssToExtenders(controlCssFile: string, namespace = "Extenders") {
        var content = util.readFile(controlCssFile);
        content = content.replace(
            util.toRegex("WebResource(\"C1.Web.Wijmo.Controls.Resources"),
            "WebResource(\"C1.Web.Wijmo." + namespace + ".Resources"
        );

        var targetFileName = controlCssFile.replace("Controls", namespace);
        this.grunt.file.write(targetFileName, content);
    }

    /** Registers grunt tasks */
    init(grunt) {
        // Expand variables in copyright text
        this.copyright = grunt.template.process(this.copyright);

        // replaces % with package name
        var addName = (s: string) => {
            return s.replace(/%/g, this.name);
        };
        // Register a grunt task. For name and tasks addName is used
        var registerTask = (name, tasks) => {
            grunt.registerTask(addName(name), addName(tasks));
        };

        // Registers a concatenation task
        var concatTask = (name: string, min: boolean) => {
            grunt.registerTask(name, () => {
                // start with copyright
                var contents = this.copyright + "\r\n";
                // then add external files
                contents += this.scripts.filter(s => s.isExternal() && fs.existsSync(s.path))
                    .map(f => util.readFile(f.path))
                    .join(";\r\n");
                // then non-external files
                contents += this.internalScripts.filter(s => s.combine)
                    .map(f => util.readFile(min ? f.min : f.path)) // prefer minified files
                    .join(";\r\n");
                // write the result
                grunt.file.write(this.combinedJsFile(min), contents);

                // copy interop files
                this.internalScripts.forEach(s => {
                    if (s.combine) return; // skip if a file was combined
                    var contents = this.copyright + "\r\n";
                    contents += util.readFile(min ? s.min : s.path);

                    // include version number
                    var dest = path.basename(s.path)
                        .replace(/\.js$/, "." + this.pkg.version + (min ? ".min" : "") + ".js");
                    // copy to the interop folder
                    dest = path.join(this.dist + "/js/interop", dest);
                    grunt.file.write(dest, contents);
                });
            });
        }

        grunt.registerTask("interop-css-" + this.name, () => {
            this.styles.forEach(s => {
                if (!s.path.match(/interop\\/)) {
                    return;
                }

                var targetName = path.join(this.dist, "css/interop/" + path.basename(s.path));
                grunt.file.copy(s.path, targetName);
            });
        });

        concatTask("js-" + this.name, false);
        concatTask("js-min-" + this.name, true);
    }
}