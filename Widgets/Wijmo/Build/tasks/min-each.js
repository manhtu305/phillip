
eval("module").exports = function (grunt) {
    "use strict";
    grunt.registerMultiTask('mineach', 'Minify each file with UglifyJS.', function () {
        var files = grunt.file.expandFiles(this.file.src);
        files.forEach(function (filename) {
            var dest = filename.replace(/\.js$/, ".min.js");
            var max = grunt.file.read(filename);
            var min = grunt.helper('uglify', max, grunt.config('uglify'));
            grunt.file.write(dest, min);
        });
        grunt.log.writeln(files.length + ' file(s) minified.');
    });
};
