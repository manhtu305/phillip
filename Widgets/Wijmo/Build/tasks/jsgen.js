/// <reference path="../../External/declarations/node.d.ts" />
var path = require("path");

var jsgen = require("./../docs/jsgen");

var util = require("./../util"), htmlgen_path = path.join(__dirname, "./../docs/htmlgen.bat");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask("jsgen", "Generate JavaScript for DocumentX from metadata files", function () {
        var files = grunt.file.expandFiles(this.file.src), dest = this.file.dest;

        if (!files.length) {
            console.log("No files found");
            return;
        }
        if (!dest) {
            throw new Error("Destination not specified");
        }

        grunt.verbose.writeln("Generating JavaScript for " + files.join(", "));

        var gen = new jsgen.Generator();
        files.forEach(function (filename) {
            var targetFilename = path.join(dest, path.basename(filename) + ".js");
            var output = new util.FileCodeWriter(targetFilename);
            gen.generate(filename, output);
        });
    });
};
