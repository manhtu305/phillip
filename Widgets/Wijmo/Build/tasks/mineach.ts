/// <reference path="../../External/declarations/node.d.ts" />
import path = require("path");
import util = require("./../util");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask('mineach', 'Minify each file with UglifyJS.', function () {
        var files: string[] = grunt.file.expandFiles(this.file.src),
            exclude = this.data.exclude, exfiles;

        if (exclude) {
            exfiles = grunt.file.expandFiles(exclude);

            if (exfiles && exfiles.length) {
                files = files.filter((file) => {
                    var idx;

                    if (!exfiles.length) {
                        return true;
                    }

                    idx = exfiles.indexOf(file);
                    idx !== -1 && exfiles.splice(idx, 1);

                    return idx === -1;
                });
            }
        }

        files.forEach(filename => {
            if (filename.match(/\.min\.js$/)) return;
            var dest = filename.replace(/\.js$/, ".min.js");
            var max = util.readFile(filename);
            var min = grunt.helper('uglify', max, grunt.config('uglify'));
            grunt.file.write(dest, min);
        });

        grunt.log.writeln(files.length + ' file(s) minified.');
    });
};