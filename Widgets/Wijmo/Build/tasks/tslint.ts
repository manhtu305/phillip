import tslint = require("./../meta/tslint");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask("tslint", "Lint TypeScript files", function () {
        var tsFiles = grunt.file.expandFiles(this.file.src),
            linter = new tslint.Linter();

        linter.check(tsFiles);
    });
};