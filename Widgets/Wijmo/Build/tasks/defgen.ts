/// <reference path="../../External/declarations/node.d.ts" />
import path = require("path");
import fs = require("fs");
import child_process = require("child_process");
import defgen = require("./../meta/defgen");

var util = require("./../util");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask("defgen", "Generate wijmo's TypeScript Definition file from metadata files", function () {
        var files: string[] = grunt.file.expandFiles(this.file.src),
            dest = this.file.dest;

        if (!files.length) {
            console.log("No files found");
            return;
        }
        if (!dest) {
            throw new Error("Destination not specified");
        }

        grunt.verbose.writeln("Generating wijmo's TypeScript Definition for " + files.join(", "));

        var gen = new defgen.Generator();
        var output = new util.FileCodeWriter(dest);
        files.forEach((filename, idx) => {
            gen.generate(filename, output, idx === 0);
        });
    });
};