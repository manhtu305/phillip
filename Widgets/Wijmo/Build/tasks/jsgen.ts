/// <reference path="../../External/declarations/node.d.ts" />
import path = require("path");
import fs = require("fs");
import child_process = require("child_process");
import jsgen = require("./../docs/jsgen")

var util = require("./../util"),
    htmlgen_path = path.join(__dirname, "./../docs/htmlgen.bat");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask("jsgen", "Generate JavaScript for DocumentX from metadata files", function () {
        var files: string[] = grunt.file.expandFiles(this.file.src),
            dest = this.file.dest;

        if (!files.length) {
            console.log("No files found");
            return;
        }
        if (!dest) {
            throw new Error("Destination not specified");
        }

        grunt.verbose.writeln("Generating JavaScript for " + files.join(", "));

        var gen = new jsgen.Generator();
        files.forEach(filename => {
            var targetFilename = path.join(dest, path.basename(filename) + ".js");
            var output = new util.FileCodeWriter(targetFilename);
            gen.generate(filename, output);
        });
    });
};