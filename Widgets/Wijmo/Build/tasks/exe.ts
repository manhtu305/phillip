/// <reference path="../../External/declarations/node.d.ts" />
import path = require("path");
import fs = require("fs");
import child_process = require("child_process");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask("exe", "Run executable", function () {
        var file: string = this.data.path,
            cwd: string = this.data.cwd,
            args = this.data.args || [];

        file = path.resolve(cwd, file);

        if (!fs.existsSync(file)) {
            throw "Executable not found: " + file;
        }

        var options: any = {};
        if (cwd) {
            options.cwd = cwd;
        }

        var done = this.async();
        var proc = child_process.execFile(file, args, options, function (error, stdout, stdin) {
            console.log(stdout.toString("utf-8", 0, stdout.length));
            if (proc.exitCode !== 0) {
                grunt.log.error("Exe task failed with exit code " + proc.exitCode);
                grunt.log.error("Command: " + file);
                grunt.log.error("Args: " + args.join(", "));
                grunt.log.error(error.toString());
                done(false);
            } else {
                done();
            }
        });
    });
};