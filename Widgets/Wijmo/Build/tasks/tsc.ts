/// <reference path="../../External/declarations/node.d.ts" />
import path = require("path");
import fs = require("fs");
import child_process = require("child_process");

var util = require("./../util"),
    tsc_path = path.join(__dirname, "./../../../tools/tsc/tsc.exe");

if (!fs.existsSync(tsc_path)) {
    throw "TSC not found: " + tsc_path;
}

eval("module").exports = function (grunt) {
    "use strict";

    function considerTsFile(tsFile: string, checkTime = true) {
        if (!fs.existsSync(tsFile)) {
            return false;
        }

        var jsFile = tsFile.replace(/\.ts$/, ".js");
        if (fs.existsSync(jsFile)) {
            if (checkTime && fs.statSync(tsFile).mtime <= fs.statSync(jsFile).mtime) {
                return false;
            }

            util.makeWritableSync(jsFile);
        }

        return true;
    }

    grunt.registerMultiTask("tsc", "Compile TypeScript files", function () {
        var files: string[] = grunt.file.expandFiles(this.file.src),
            outFile = this.data.out,
            force = Array.prototype.indexOf.call(arguments, "force") >= 0;

        if (force) {
            grunt.log.writeln("All .ts files compilation is forced");
        }

        if (!files.length) {
            console.log("No files found");
            return;
        }

        for (var i = files.length - 1; i >= 0; i--) {
            if (!considerTsFile(files[i], !force && !outFile)) {
                if (files.splice(i, 1));
                continue;
            }
        }

        if (files.length == 0) {
            console.log("All JavaScript files are up to date");
            return;
        }

        var done = this.async();

        var allFiles = files.join(", ");
        grunt.verbose.writeln('Compiling ' + allFiles);

        //"-c" "--comments" is removed from TypeScript 0.9
        //var tscArgs = ["-c"].concat(files);
        var tscArgs = [].concat(files);
        if (outFile) {
            tscArgs.push("-out", outFile);
            if (fs.existsSync(outFile)) {
                util.makeWritableSync(outFile);
            }
        }

        var tsc = child_process.execFile(tsc_path, tscArgs, null, function (error, stdout, stdin) {
            if (tsc.exitCode !== 0) {
            	grunt.log.error("TSC task failed with exit code " + tsc.exitCode);
            	grunt.log.error(stdout.toString("utf8"));
                grunt.log.error(error.toString());
                done(false);
            } else {
                grunt.verbose.ok('Compiled ' + files.length + " file(s).");
                done();
            }
        });
    });
};