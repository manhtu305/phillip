/// <reference path="../../External/declarations/node.d.ts" />
import path = require("path");
import fs = require("fs");
import child_process = require("child_process");

var util = require("./../util"),
    htmlgen_path = path.join(__dirname, "./../docs/htmlgen.bat");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask("htmlgen", "Generate HTML documentation from metadata files", function () {
        var files: string[] = typeof this.file.src === "string" ? grunt.file.expandFiles(this.file.src) : this.file.src;

        if (!files.length) {
            console.log("No files found");
            return;
        }

        var done = this.async();

        grunt.verbose.writeln("Generating HTML for " + files.join(", "));

        files.forEach(filename => {
            var expectedHtmlFile = filename.replace(/\.meta$/, ".html");
            if (fs.existsSync(expectedHtmlFile)) {
                util.makeWritableSync(expectedHtmlFile);
            }
        });

        var htmlGen = child_process.execFile(htmlgen_path, files, null, function (error, stdout, stdin) {
            if (htmlGen.exitCode !== 0) {
                grunt.log.error("htmlgen task failed with exit code " + htmlGen.exitCode);
                grunt.log.error(error.toString());
                done(false);
            } else {
                grunt.verbose.ok("Done");
                done();
            }
        });
    });
};