/// <reference path="../../External/declarations/node.d.ts" />
var path = require("path");
var util = require("./../util");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask("copy", "Copy files to destination folder", function () {
        var files = grunt.file.expandFiles(this.file.src), except = this.data.except, target = this.file.dest + "/", strip = this.data.strip, process = this.data.process, processFilter = this.data.processFilter, processTemplate = this.data.processTemplate;

        if (except) {
            if (typeof except === "string" || except instanceof RegExp) {
                except = [except];
            }
            if (typeof except !== "function") {
                var exceptions = except;
                except = function (filename) {
                    return exceptions.some(function (e) {
                        return e instanceof RegExp ? e.exec(filename) : grunt.file.isMatch(e, filename);
                    });
                };
            }
        }

        files.forEach(function (fileName) {
            if (except && except(fileName)) {
                return;
            }

            var targetFile = fileName;
            var shouldProcessTemplate = processTemplate && grunt.file.isMatch(processTemplate, fileName);
            var callProcess = process && (!processFilter || grunt.file.isMatch(processFilter, fileName));
            if (strip) {
                if (typeof strip === "function") {
                    targetFile = strip(targetFile);
                } else {
                    targetFile = targetFile.replace(strip, "");
                }
            }
            targetFile = path.join(target, targetFile);
            if (callProcess || shouldProcessTemplate) {
                var contents = util.readFile(fileName);
                if (shouldProcessTemplate) {
                    contents = grunt.template.process(contents);
                }
                if (callProcess) {
                    contents = process(contents, fileName);
                }
                grunt.file.write(targetFile, contents);
            } else {
                grunt.file.copy(fileName, targetFile);
            }
        });
        grunt.log.writeln("Copied " + files.length + " files.");
    });
};
