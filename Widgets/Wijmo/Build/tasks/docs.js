eval("module").exports = function (grunt) {
    "use strict";

    var dxDir = "Documentation/dx/";

    grunt.registerTask("docs-copy-files", function () {
        grunt.file.copy("Wijmo/Base/jquery.wijmo.widget.js", dxDir + "jquery.wijmo.widget.js");
    });
    grunt.registerTask("docs", "tsc metagen jsgen docs-copy-files");
};
