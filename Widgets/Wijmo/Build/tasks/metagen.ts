/** This script registers "metagen" grunt task that runs MetaCompiler against the specified files */

import mg = require("./../meta/metagen");

eval("module").exports = function (grunt) {
    "use strict";

    grunt.registerMultiTask("metagen", "Generated metadata files", function () {
        var tsFiles = grunt.file.expandFiles(this.file.src),
            compiler = new mg.MetaCompiler();

        var count = compiler.compile(tsFiles, false);
        if (count) {
            grunt.log.writeln("Generated metadata for " + count + " files.");
        }
    });
};