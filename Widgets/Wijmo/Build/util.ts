/// <reference path="../External/declarations/node.d.ts" />
import path = require("path");
import child_process = require("child_process");
import fs = require("fs")

/** Analog of .NET TextWriter but with support of indentation */
export class CodeWriter {

    //#region indentation
    _indentLevel = 0;
    _indentString = "";

    _indentStringFor(indent: number) {
        var spaces = indent * 4;
        return new Array(spaces + 1).join(" ");
    }
    indented(fn: () => any) {
        this._indentLevel++;
        this._indentString = this._indentStringFor(this._indentLevel);
        try {
            this._write(this._indentStringFor(1));
            fn.call(this);
        } finally {
            this._indentLevel--;
            this._indentString = this._indentStringFor(this._indentLevel);
        }
    }
    //#endregion

    _write(text: string) {
    }
    write(format: string, ...args: any[]) {
        var text = format.replace(/{(\d+)}/g, (m) => {
            //If args are not exist, don't replace {0} to undefined.
            var val = args[parseInt(m[1])];
            return val ? val : m;
        });
        if (this._indentString) {
            text = text.replace(/(\r?\n)/mg, "$1" + this._indentString);
        }
        this._write(text);
    }
    writeLine(format?: string, ...args: any[]) {
        if (format) {
            this.write.apply(this, arguments);
        }
        this.write("\r\n");
    }
}

/** A file-based CodeWriter */
export class FileCodeWriter extends CodeWriter {
    private _fd;
    constructor(filename: string) {
        super();
        if (fs.existsSync(filename) && !isWritableSync(filename)) {
            throw new Error("File is read-only");
        }
        this._fd = fs.openSync(filename, "w");
    }
    _write(text: string) {
        var buf = new Buffer(text);
        fs.writeSync(this._fd, buf, 0, buf.length, null)
    }
    close() {
        fs.closeSync(this._fd);
    }
}


export var stats = { writable: 0x80 };

export function makeWritableSync(filename: string) {
    var stat = fs.statSync(filename);
    if ((stat.mode & stats.writable) !== stats.writable) {
        fs.chmodSync(filename, stat.mode | stats.writable);
    }
}

export function isWritableSync(filename: string) {
    var stat = fs.statSync(filename);
    return (stat.mode && stats.writable) === stats.writable;
}

export function rm(name: string) {
    if (!fs.existsSync(name)) return;
    if (fs.statSync(name).isFile()) {
        fs.unlinkSync(name);
    } else {
        fs.readdirSync(name).forEach(child => rm(path.join(name, child)));
        fs.rmdirSync(name);
    }
}

export function toRegex(text: string) {
    var escaped = text.replace(/[\.\(\)]/g, "\\$&");
    return new RegExp(escaped, "g");
}

export function readFile(filename: string) {
    var contents = fs.readFileSync(filename, "utf8");
    contents = contents.replace(/\uFEFF/g, "");
    if (contents.length > 0 && contents.charCodeAt(0) == 65279) {
        contents = contents.substr(1);
    }
    return contents;
}