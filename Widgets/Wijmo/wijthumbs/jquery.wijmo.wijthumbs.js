﻿/*
 * Depends:
 *	jquery-1.4.2.js
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *	jquery.ui.position.js
 *	jquery.effects.core.js	
 *  jquery.wijmo.wijsuperpanel.js
 *	jquery.wijmo.wijutil.js
 */

(function ($) {

    $.widget("wijmo.wijthumbs", {
        options: {
			rootUrl: "",
			thumbSize: {width: 80, height: 80},
			thumbTemplate: '<img src="%path"/>',
			items: [],
			selectedIndex:0,
			grayMode: false,
			scrollMode: 'scrollbar'  // possible values are: buttons, edge, scrollbar
        },

        _create: function () {
			var o = this.options, self = this;
			this.element.addClass('ui-widget ui-corner-all wijmo-wijthumbs');
			var $scrollwrap = $('<div></div>').addClass('wijmo-wijthumbs-scrollwrap').appendTo(this.element);
			var $list = $('<ul></ul>').addClass('wijmo-wijthumbs-list').appendTo($scrollwrap);
			var h = 0, w = 0, $item, $itemcontent, $img;
			$.each(o.items, function(index, data){
				//self._addItem($list, data);
				$item = $('<li></li>').addClass('wijmo-wijthumbs-item').appendTo($list);
				$itemcontent = $('<div></div>').addClass('wijmo-wijthumbs-item-content ui-corner-all ui-state-default' + (o.grayMode ? ' ui-priority-secondary' : '')).appendTo($item);
				$img = $('<img></img>').addClass('wijmo-wijthumbs-item-image')
				.attr('src', self._getUrl(data))
				.width(o.width || 80)
				.height(o.height || 80)
				.appendTo($itemcontent);
				
				if (!h){
					h = $item.outerHeight();
				}
				
				w += $item.outerWidth(true);
			});

			if (h){
				if (o.scrollMode === 'scrollbar'){
					h += 18;
				}
				$scrollwrap.height(h);
				this.element.height($scrollwrap.outerHeight(true));
			}
			
			$list.width(w+2);
			
			if (o.scrollMode === 'buttons'){
				var addState = function(state, el) {
					if (el.is(':not(.ui-state-disabled)')) {
						el.addClass('ui-state-' + state);
					}
				};
				var removeState = function(state, el) {
					el.removeClass('ui-state-' + state);
				};
			
				var $decBtn = $('<div></div>').addClass('wijmo-wijthumbs-button-dec ui-state-default ui-corner-all')
					.append($('<span></span>').addClass('ui-icon ui-icon-grip-dotted-vertical'))
					.height(h-2)
					.css('left', '0px')
					.appendTo(this.element)
					.bind({
						'mouseover': function() { addState('hover', $(this)); },
						'mouseout': function() { removeState('hover', $(this)); },
						'mousedown': function(){ $scrollwrap.wijsuperpanel('doScrolling', 'left', true); }
					}),
				$incBtn = $('<div></div>').addClass('wijmo-wijthumbs-button-inc ui-state-default ui-corner-all')
					.append($('<span></span>').addClass('ui-icon ui-icon-grip-dotted-vertical'))
					.height(h-2)
					.css('right', '0px')
					.appendTo(this.element)
					.bind({
						'mouseover': function() { addState('hover', $(this)); },
						'mouseout': function() { removeState('hover', $(this)); },
						'mousedown': function(){ $scrollwrap.wijsuperpanel('doScrolling', 'right', true); }
					});
					
				$scrollwrap.width(this.element.width() - $decBtn.outerWidth(true) - $incBtn.outerWidth(true))
					.css('left', $decBtn.outerWidth(true))
					.wijsuperpanel({hScroller: {scrollBarVisibility:false}, vScroller:{scrollBarVisibility:false}})
					.wijsuperpanel('option', 'animationOptions', o.animationOptions);
			}else if (o.scrollMode === 'scrollbar'){
				$scrollwrap.width(this.element.width());
				$scrollwrap.wijsuperpanel({vScroller:{scrollBarVisibility:false}});
			}else{
				$scrollwrap.width(this.element.width());
				$scrollwrap.wijsuperpanel({hScroller: {scrollMode:'edge'}, vScroller:{scrollBarVisibility:false}});
			}
			$scrollwrap.find('.wijmo-wijthumbs-item-content')
			.bind({
				'mouseover': function(){
					$(this).addClass('ui-state-hover').removeClass(o.grayMode ? 'ui-priority-secondary' : '');
				},
				'mouseout': function(){
					$(this).removeClass('ui-state-hover');
					if (o.grayMode && !$(this).hasClass('ui-priority-primary')){
						$(this).addClass('ui-priority-secondary');
					}
				},
				'mousedown': function(){
					self.select($(this).closest('li').index());
				}
			});
			
			this.select();
        },
		
		_addItem: function($list, data){
			var o = this.options
			$item = $('<li></li>').addClass('wijmo-wijthumbs-item').appendTo($list),
			$img = $('<img></img>').addClass('wijmo-wijthumbs-image')
				.attr('src', this._getUrl(data))
				.width(o.width || 80)
				.height(o.height || 80)
				.appendTo($item);
		},
		
		select: function(index){
			var o = this.options;
			var $contents = $('.wijmo-wijthumbs-item-content', this.element);
			if (index === undefined){
				index = o.selectedIndex;
			}else{
				if (index < 0 || index >= $contents.length || index === o.selectedIndex){
					return;
				}
			}
			
			if (this._trigger('beforeselect', null, {index: index}) === false){ 
				return; 
			}
			
			$contents.filter('.ui-priority-primary')
			.removeClass('ui-priority-primary ui-state-active')
			.addClass(o.grayMode ? 'ui-priority-secondary' : '')
			.end()
			.eq(index)
			.removeClass(o.grayMode ? 'ui-priority-secondary' : '')
			.addClass('ui-priority-primary ui-state-active');
			
			o.selectedIndex = index;
			
			this._trigger('afterselect');
		},
		
		_getUrl: function(item){
			var o = this.options, url = item.url, rootUrl = o.rootUrl;
			if (rootUrl && rootUrl.length > 0){
				if (rootUrl.indexOf('//') ==  -1) { 
					rootUrl = location.protocol + '//' + location.host + (rootUrl.startsWith('/') ? '' : '/') + rootUrl;
				};
				
				if (rootUrl.substr(rootUrl.length - 1, 1) !== '/')
					rootUrl += '/';

				url = rootUrl + (url.substr(0, 1) === '/' ? url.substr(1) : url);
			}
			
			return url;
		},

        destroy: function () {
        },

    });

}(jQuery)
);

