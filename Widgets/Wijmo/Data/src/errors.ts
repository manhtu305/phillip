/// <reference path="util.ts"/>

/** @ignore */
module wijmo.data {
    export var errors: any = {};
    errors._register = function (messages) {
        util.each(messages, function (name: string, msg) {
            function create(...fmtArgs: any[]) {
                var lastChar: string;
                if ($.isFunction(msg)) {
                    msg = msg.apply(this, arguments);
                } else if (arguments.length > 0) {
                    fmtArgs.unshift(msg);
                    msg = util.format.apply(this, fmtArgs);
                }

                msg = $.trim(msg);
                lastChar = msg[msg.length - 1];
                if (lastChar !== '.' && lastChar !== '!' && lastChar !== '?') {
                    msg += '.';
                }

                return new WijmoError(msg);
            }

            errors[name] = function () {
                throw create.apply(this, arguments);
            }
            errors[name].create = create;
        });
    }

    errors._register({
        indexOutOfBounds: "Index is outside the bounds of the array.",
        notImplemented: "The operation is not implemented",
        unsupportedOperation: "Unsupported operation",
        unsupportedFilterOperator: "Unsupported filter operator: {0}",
        unsupportedDataSource: "Unsupported data source",
        argument: function (paramName) {
            var message = "Unexpected argument value.";
            if (paramName) {
                message += "\nParameter name: " + paramName;
            }
            return message;
        },
        argumentNull: "Argument '{0}' is null/undefined",

        noParser: "There is no parser for type '{0}'",
        noUrl: "Url is not specified",
        cantConvert: "Value can't be converted to type '{0}': '{1}'",
        noGlobalize: "Globalize is not defined. Make sure you include globalize.js",

        itemNotInView: "Item {0} is not in the data view",

        unsupportedFilterFormat: "The filter format is not supported",

        multiPropertyKeysNotSupported: "Entities with multiple properties in the primary key are not supported. Entity type: {0}",
        keyPropertyNotFound: "Key property not found in {0} entity type"
    });
}