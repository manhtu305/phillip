﻿/// <reference path="core.ts"/>
/// <reference path="errors.ts"/>
/// <reference path="dataView.ts"/>
/// <reference path="observable.ts"/>
/// <reference path="currency.ts"/>

declare var ko;

module wijmo.data {
    var $ = jQuery;

    /** @ignore */
    export class Shape {
        filter = observable();
        _compiledFilter = filtering.compile(null);

        sort = observable();
        _compiledSort = sorting.compile(null);

        pageIndex = observable(0);
        pageSize = observable(0);
        _skip = 0;
        _take = -1;

        constructor(private onChanged: (paging?: boolean) => void) {
            this.filter.subscribe(function (newValue) {
                this.onFilterChanged(newValue);
                onChanged();
            }, this);
            this.sort.subscribe(function () {
                this._compiledSort = sorting.compile(this.sort());
                onChanged();
            }, this);

            function updatePaging() {
                if (this.pageSize() > 0 && this.pageIndex() >= 0) {
                    this._skip = this.pageSize() * this.pageIndex();
                    this._take = this.pageSize();
                } else {
                    this._skip = 0;
                    this._take = -1;
                }
                onChanged(true);
            }
            this.pageIndex.subscribe(updatePaging, this);
            this.pageSize.subscribe(updatePaging, this);
        }

        onFilterChanged(newValue) {
            this._compiledFilter = filtering.compile(newValue);
        }
        setFilter(filter) {
            this.filter(filter);
        }

        update(shape: IShape) {
            if ("filter" in shape) {
                this.setFilter(shape.filter);
            }
            if ("sort" in shape) {
                this.sort(shape.sort);
            }
            if ("pageSize" in shape) {
                this.pageSize(shape.pageSize);
            }
            if ("pageIndex" in shape) {
                this.pageIndex(shape.pageIndex);
            }
        }

        apply(array: any[], applyPaging = true, destination: any[] = null): { results: any[]; totalCount: number; } {
        	var i: number;
            // filter
        	if (!this._compiledFilter.isEmpty) {
        		if (destination) {
        			destination.length = 0;
        		} else {
        			destination = [];
        		}
                for (i = 0; i < array.length; i++) {
                    var item = array[i];
                    if (this._compiledFilter.func(item)) {
                    	destination.push(item);
                    }
                }
            } else {
            	// just clone it
            	if (!destination) {
            		destination = array.slice(0);
            	} else {
            		destination.length = array.length;
            		for (i = 0; i < array.length; i++) {
            			destination[i] = array[i];
            		}
            	}
            }

            // sort
            if (!this._compiledSort.isEmpty) {
                this._stableSort(destination, this._compiledSort.compare);
            }

            // page
            var totalCount = destination.length;
            if (applyPaging && this._take > 0) {
                if (this._skip > 0) {
                    destination.splice(0, Math.min(this._skip, destination.length));
                }
                if (this._take < destination.length) {
                    destination.length = this._take;
                }
            }

            return { results: destination, totalCount: totalCount };
        }

        _stableSort(arr: any[], fn: (a: any, b: any) => number) {
            var isChrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()) && /Google Inc/.test(navigator.vendor);
            if (fn && isChrome) {
                arr.forEach(function (ele, index) {
                    arr[index] = { index: index, value: ele };
                });
                arr.sort(function (c, d) {
                    var result = fn(c.value, d.value);
                    if (result === 0) {
                        return c.index - d.index;
                    }
                    return result;
                });
                arr.forEach(function (ele, index) {
                    arr[index] = ele.value;
                });
            } else {
                arr.sort(fn);
            }
        }

        toObj(): IShape {
            return {
                filter: this._compiledFilter.normalized,
                sort: this._compiledSort.normalized,
                pageSize: this.pageSize(),
                pageIndex: this.pageIndex()
            };
        }
    }

    export class ArrayDataViewBase implements IDataView, IPagedDataView, IEditableDataView, ICloneable {
        sourceArray: any[];
        isRemote = false;
        localPaging = true;
        constructor(shape?: IShape) {
            this.isLoading = this._isLoading.read();
            this.isLoaded = this._isLoaded.read();
            this._changed = new Subscribable(this);

            this.currentEditItem = this._currentEditItem.read();
            this._initCurrency();
            this._initShape(shape);
        }
		/**
		* @ ignore
		*/
        dispose() {}

		//#region items

    	/**
		* The element array of current view after client-side filtering/sorting/paging have been applied.
		*/
        local = [];

    	/** 
		* Returns the number of items in the current view after client-side filtering/sorting/paging have been applied.
		*/
        count() {
            return this.local.length;
        }

		/** 
		* Returns an element in the view by index.
        * @ param {Number} index The zero-based index of the element to get
        * @ remarks Throws an exception if the index is out of range   
        */
        item(index: number) {
            if (index < 0 || index >= this.local.length) {
                errors.indexOutOfBounds();
            }
            return this.local[index];
        }

    	/**
		* Returns the index of the element in current view
		* @ param {object} item The element in current view.
		*/
        indexOf(item) {
            return $.inArray(item, this.local);
        }

		/** 
		* Returns the element array before applying client-side filtering/sorting/paging.
        * @remarks In the case of a remote data source, usually it is the array returned by a server.
        */
        getSource() {
            return this.sourceArray;
        }

        _koArray;
    	/**
		* Converts the element array of current view after filter, sort and paging have been applied to Obervable Array.
		*/
        toObservableArray() {
            if (!this._koArray) {
                this._koArray = ko.observableArray(this.local);
            }
            return this._koArray;
        }
        //#endregion items

        //#region properties
        static _getProps(item): IPropertyDescriptor[] {
            var cols: IPropertyDescriptor[] = [];
            util.each(item, (key: string, value) => {
                key = String(key);
                if (key.match(/^_/)) return;
                if ($.isFunction(value) && !value.subscribe) return;
                cols.push({ name: key });
            });
            return cols;
        }

		/** 
		* Returns properties for elements in the view.
		*/
        getProperties(): IPropertyDescriptor[] {
            return this.count() ? ArrayDataViewBase._getProps(this.item(0)) : [];
        }

        _readProperty(item, property: string) {
            return util.getProperty(item, property);
        }

    	/** 
		* Returns the current value of the property in the element at the specified index.
        * @ param {Object} itemOrIndex The element with the property value that is to be read 
		* or the zero-based index of the element with the property value that is to be read.
        * @ param {string} property The name of the property to read
        * @ returns The current value of the property in the element
        */
        getProperty(itemOrIndex, property: string) {
            var item = this._getItem(itemOrIndex);
            return this._readProperty(item, property);
        }
        _writeProperty(item, property: string, newValue) {
            util.setProperty(item, property, newValue);
        }

    	/** 
		* Sets the value of the property in the element.
        * @ param {Object} itemOrIndex The element with the property value that is to be read 
		* or the zero-based index of the element with the property value that is to be read.
        * @ param {string} property The name of the property to set
        * @ param newValue The new value
        * @ remarks 
		* If the class implementing IDataView also implements IEditableDataView and the element is being edited, 
        * then the property value change is done within the editing transaction.
        */
        setProperty(itemOrIndex, property: string, newValue) {
            var item = this._getItem(itemOrIndex);

            if (item === this.currentEditItem() && this._currentEditItemSnapshot && !(property in this._currentEditItemSnapshot)) {
                this._currentEditItemSnapshot[property] = this.getProperty(itemOrIndex, property);
            }

            this._writeProperty(item, property, newValue);

            return this;
        }

        //#endregion properties

        //#region notifications
        private _changed: Subscribable;
    	/**
		* Registers Subcription of changes for current view.
		* @ param {function} handler Function that will be called when current view is changed.
		* @ param {Object?} context Indicates which calls the handler function. 
		*/
        subscribe(handler: (newItems: any[]) => void, context?) {
            return this._changed.subscribe(handler, context);
        }
		/**
		* Triggers the changes for current view.
		*/
        trigger() {
            this._currencyManager.update();
            this._changed.trigger(this.local);
            if (this._koArray) {
                this._koArray.notifySubscribers(this.local);
            }
        }
        //#endregion notifications

        //#region shape
        _shape: Shape;

		/** 
		* Returns a value that indicates whether the data view supports filtering. 
		*/
        canFilter() {
            return true;
        }

		/** 
		* The filter to be applied to the data view. When assigned, the data view is refreshed.
        */
        filter: IMutableObservable;

		/** 
		* Returns a value that indicates whether the data view supports sorting. 
		*/
        canSort() {
            return true;
        }

		/** An observable property that gets or sets the sort to be applied to the data view.
        * When assigned, the data view is refreshed.
        */
        sort: IMutableObservable;

        _updatingShape = false;
        _updateShape(shape: IShape) {
            this._updatingShape = true;
            try {
                this._shape.update(shape);
            } finally {
                this._updatingShape = false;
            }
        }

        _pageCount = observable(1);
		/** 
		* The number of pages. 
		*/
        pageCount: IObservable;
        _totalItemCount = numericObservable(0);
		/** 
		* The number of elements before paging is applied. 
		*/
        totalItemCount: IObservable;
		/** 
		* The current page index. 
		*/
        pageIndex: IMutableObservable;
		/** 
		* The current page index. 
		*/
        pageSize: IMutableObservable;

		/*
		* Goes to previous page of current view if paging has been applied.
		* @ remarks
		* If current page is the first page, it will return false.
		* Otherwise it will return true.
		*/
        prevPage() {
            if (this.pageIndex() < 1) {
                return false;
            }
            this.pageIndex(this.pageIndex() - 1);
            return true;
        }

    	/*
		* Goes to next page of current view if paging has been applied.
		* @ remarks 
		* If current page is the last page, it will return false.
		* Otherwise it will return true.
		*/
        nextPage() {
            if (this.pageCount() > 0 && this.pageIndex() + 1 >= this.pageCount()) {
                return false;
            }
            this.pageIndex(this.pageIndex() + 1);
            return true;
        }

        _createShape(onChanged: (paging?: boolean) => void) {
            return new Shape(onChanged);
        }
        _initShape(shape: IShape) {
            var onChanged = (onlyPaging = false) => {
                if (!this._updatingShape) {
                    this.refresh(null, onlyPaging && this.localPaging);
                }
            };

            this._shape = this._createShape(onChanged);
            if (shape) {
                this._updateShape(shape);
            }
            this.filter = this._shape.filter;
            this.sort = this._shape.sort;
            this.pageIndex = this._shape.pageIndex;
            this.pageSize = this._shape.pageSize;
            this.pageCount = this._pageCount.read();
            this.totalItemCount = this._totalItemCount.read();
        }

        //#endregion shape

        //#region loading
        _isLoaded = observable(false);
        _isLoading = observable(false);
		/** 
		* A value indicating whether the data view is being loaded 
		*/
        isLoading: IObservable;
		/** 
		* A value indicating whether the data view is loaded 
		*/
        isLoaded: IObservable;

        _localRefresh(doPaging = this.localPaging) {
            var result = this._shape.apply(this.sourceArray, doPaging, this.local);

            if (doPaging) {
                this._totalItemCount(result.totalCount);
            }
            this._pageCount(util.pageCount(this.totalItemCount(), this.pageSize()));

            // notify
            this.trigger();
            return $.Deferred().resolve().promise();
        }
        _remoteRefresh() {
            return this._localRefresh();
        }

		/** Reloads the data view.
        *
        * @ param {IShape} shape
        * Settings for filtering, sorting and/or paging to be applied before loading.
        *
        * In contrast to filter/sort properties in IDataView, 
        * the format of properties in the shape parameter does not have to follow the specs defined in IDataView.
        * For instance, BreezeDataView accepts a Breeze.Predicate as shape.filter.
        * However the IDataView properties must not violate the IDataView specs. If a custom filter format cannot be aligned to the IDataView specs,
        * then the properties values must be functions or null.
        *
        * @ param {boolean} local Prevents server requests and applies filtering/sorting/paging of the source array on the client-side.
        *
        * @ returns {IRefreshResult} A promise object that can be used to receive notification of asynchronous refresh completion or failure.
        * 
        * @ remarks
        * Depending on the implementation, data can be loaded from a local or a remote data source. 
        * The implementation transaltes filtering/sorting/paging defined in the IDataView to the data provider specific format.
        * If it is possible, then the data is reshaped on the client-side.
        *
        * The optional shape parameter can be used to change multiple filtering/sorting/paging settings at a time to avoid unnecessary refreshes.
        *
        * During the refresh, the isLoading property value must be true. After a successful refresh, the isLoaded property value must be true.
        *
        * When the method is called, the pending refresh, if any, is canceled.
        */
        refresh(shape?: IShape, local = false) {
            this.cancelRefresh();
            if (shape) {
                this._updateShape(shape);
            }
            this._isLoading(true);
            var promise = local ? this._localRefresh() : this._remoteRefresh();
            return promise.then(() => {
                this._isLoaded(true);
                this._isLoading(false);
            });
        }

		/** 
		* Cancels the ongoing refresh operation 
		*/
        cancelRefresh() {
        }
        //#endregion loading

        //#region currency
        _currencyManager: CurrencyManager;
		/** 
		* The current element in the data view 
		*/
        currentItem: IMutableObservable;
		/** 
		* The current element index in the data view 
		*/
        currentPosition: IMutableObservable;

        _initCurrency() {
            this._currencyManager = new CurrencyManager(this.local);
            this.currentItem = this._currencyManager.currentItem;
            this.currentPosition = this._currencyManager.currentPosition;
        }
        //#endregionn

        //#region editing
        _currentEditItem = observable();
		/** 
		* The element currently being edited.
        */
        currentEditItem: IObservable;
        _isCurrentEditItemNew = false;
        _currentEditItemSnapshot = null;

		/** 
		* Returns a value indicating whether the element being edited is added by the add(item) methods. 
		*/
        isCurrentEditItemNew() {
            return this._isCurrentEditItemNew;
        }

        _beginEdit(item, isNew: boolean) {
            this.commitEdit();
            this._currentEditItemSnapshot = {};
            this._isCurrentEditItemNew = isNew;
            this._currentEditItem(item);
        }

		/** 
		* Returns a value that indicates whether the add(item) method is implemented. 
		*/
        canAdd() {
            return true;
        }

		/** 
		* Adds a new element and marks it as being edited.
        * @ param {Object} item The item to add.
        * @ remarks
        * Adds a new element to the data view, but not to the underlying data source.
        * Commits the element currently being edited, if any.
        */
        add(item: Object) {
            if (!item) errors.argument("item");
            this.commitEdit();
            this.local.push(item);
            this._beginEdit(item, true);
            this.trigger();
        }

		/** 
		* Returns a value that indicates whether the addNew() method is implemented. 
		*/
        canAddNew() {
            return false;
        }

		/*
		* @ignore
		*/
        addNew() {
            return errors.unsupportedOperation();
        }

    	/** 
		* Starts editing of an element at the index.
        * @ param {Object} element The element to edit. 
		* Defaults to the value of IDataView.currentItem property.
        * @ remarks 
        * Commits the element currently being edited, if any.
        */
        editItem(item = this.currentItem()) {
            this.commitEdit();

            item = this._getItem(item);
            if (item) {
                this._beginEdit(item, false);
            }
        }

		/** 
		* Returns a value that indicates whether the remove method overloads are implemented. 
		*/
        canRemove() {
            return true;
        }
        _remove(entry: IViewEntry) {
            this.local.splice(entry.index, 1);
            util.remove(this.sourceArray, entry.item);
            this._totalItemCount.dec();
            this.trigger();
        }
    	/** 
		* Removes an element at the specified index.
        * @ param {Object} item The element to remove. 
		* Defaults to the value of the IDataView.currentItem property.
		* @ remarks 
		* If the element success removing, it will return true.
		* Otherwise it will return false.
        */
        remove(item = this.currentItem()) {
            this.commitEdit();

            var entry = this._resolve(item);
            if (!entry) return false;
            this._remove(entry);
            return true;
        }

		/** 
		* Returns a value that indicates whether the current changes can be canceled. 
		*/
        canCancelEdit() {
            return true;
        }

		/** 
		* Cancels the changes made to the currently editing element since the editing was started.
        * @ remarks After a successful cancelation, the currentEditItem property value must be null.
        */
        cancelEdit() {
            var key: string;
            if (!this.currentEditItem()) return;
            var item = this.currentEditItem();
            this._currentEditItem(null);

            if (this._isCurrentEditItemNew) {
                util.remove(this.local, item);
            } else if (this._currentEditItemSnapshot) {
                $.each(this._currentEditItemSnapshot, (k, v) => this._writeProperty(item, k, v));
            }
            this.trigger();
        }

		/** 
		* Returns a value that indicates whether the current changes can be committed 
		*/
        canCommitEdit() {
            return true;
        }

		/** 
		* Commits the changes made to the currently editing element
        * @ remarks After a successful commit, the currentEditItem property value must be null.
        * If the item being edited was new, it is added to the underlying data source.
        * If the element does not satisfy the filter, then it is removed from the data view, but not from the data source.
        */
        commitEdit() {
            if (!this.currentEditItem()) return;
            var item = this.currentEditItem();
            this._currentEditItem(null);

            if (this._isCurrentEditItemNew) {
                this.sourceArray.push(item);
                this._totalItemCount.inc();
            }

            var filter = this._shape._compiledFilter;
            if (!filter.isEmpty && !filter.func(item)) {
                util.remove(this.local, item);
            }
            this.trigger();
        }

        //#endregion editing

        _getItem(itemOrIndex): any {
            var index;
            if (util.isNumeric(itemOrIndex)) {
                return this.item(itemOrIndex)
            } else {
                return itemOrIndex;
            }
        }

        _resolve(itemOrIndex, raiseIfNotContained = false): IViewEntry {
            var index;
            if (util.isNumeric(itemOrIndex)) {
                return {
                    index: itemOrIndex,
                    item: this.item(itemOrIndex)
                };
            } else {
                index = this.indexOf(itemOrIndex);
                if (index < 0) {
                    if (raiseIfNotContained) errors.itemNotInView(itemOrIndex);
                    return null;
                }
                return {
                    index: index,
                    item: itemOrIndex
                };
            }
		}

		/** @ignore */
		clone(): ArrayDataViewBase {
			throw "Not supported.";
		}
    }

    /** @ignore */
    export interface IViewEntry {
        item;
        index: number;
    }
    export class ArrayDataView extends ArrayDataViewBase {
        constructor(source: any[], shape?: IShape) {
            super(shape);
            this.sourceArray = source;
            this.refresh();
        }

        /** @ignore */
        clone(): ArrayDataView {
            var shape = this._shape ? this._shape.toObj() : undefined;
            return new ArrayDataView(this.getSource(), shape);
        }
    }
}

//interface NavigatorID {
//    vendor: string;
//}