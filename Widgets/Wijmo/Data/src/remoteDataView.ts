﻿/// <reference path="arrayDataView.ts"/>

module wijmo.data {
    var $ = jQuery;

    export interface IRemoteDataViewOptions extends IShape {
        /*
         * Indicates whether to allow paging to be done on the client or on the server.
         * If set to true, paging is done on the client.
         */
        localPaging?: boolean;
    }

    export class RemoteDataView extends ArrayDataViewBase {
        isRemote = true;
        options: IRemoteDataViewOptions;

        constructor(options?: IRemoteDataViewOptions) {
            super();
            this.sourceArray = [];
            if (options) {
                this._construct(options);
            }
        }
        _construct(options: IRemoteDataViewOptions) {
            this.options = options = $.extend({
                localPaging: false
            }, options);
            this.localPaging = options.localPaging;
            this._updateShape(options);
        }
    }

}