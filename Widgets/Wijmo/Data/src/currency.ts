﻿/// <reference path="core.ts"/>
/// <reference path="arrayDataView.ts"/>

/** @ignore */
module wijmo.data {
    var $ = jQuery;

    export class CurrencyManager {
        _recentlyRemovedItem;
        currentItem = observableWithNewValueCheck(null);
        currentPosition = observableWithNewValueCheck(-1);

        constructor(public array: any[]) {
            var syncing = false;
            function synced(fn) {
                return function () {
                    if (syncing) return;
                    syncing = true;
                    try {
                        fn.apply(this, arguments);
                    } finally {
                        syncing = false;
                    }
                }
            }

            this.currentItem.subscribe(synced(function (value) {
                this.currentPosition(value == null ? -1 : $.inArray(value, this.array));
            }), this);

            this.currentPosition.subscribe(synced(function (value) {
                if (!util.isNumeric(value)) errors.argument("value");
                if (value < -1 || value >= this.array.length) errors.indexOutOfBounds();
                this.currentItem(value < 0 ? null : this.array[value]);
            }), this);
        }
        update() {
            var item = this.currentItem(),
                pos = this.currentPosition(),
                newIndex = $.inArray(item, this.array);

            if (newIndex < 0 && item == null && this._recentlyRemovedItem != null) {
                newIndex = $.inArray(this._recentlyRemovedItem, this.array);
            }

            if (newIndex >= 0) {
                if (item) {
                    this._recentlyRemovedItem = item;
                }
                this.currentPosition(newIndex);
            } else if (pos >= 0 && pos < this.array.length) {
                this.currentItem(this.array[pos]);
            } else if (pos >= this.array.length && this.array.length > 0) {
                pos = this.array.length - 1;
                this.currentPosition(pos);
                this.currentItem(this.array[pos]);
            } else {
                this.currentPosition(-1);
                this.currentItem(null);
            }
        }
        updateDelayed() {
            util.executeDelayed(this.update, this);
        }
    }
}