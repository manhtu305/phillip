/// <reference path="core.ts"/>
/// <reference path="filtering.ts"/>
/// <reference path="sorting.ts"/>
/// <reference path="arrayDataView.ts"/>

module wijmo.data {
    var $ = jQuery;

    /** Contains properties to specify filter, sort and paging. */
    export interface IShape {
        /** The filter to apply to a data view. 
          * See wijmo.data.IDataView.filter for details.
          */
        filter?;
        /** The sort to apply to a data view. 
          * See wijmo.data.IDataView.sort for details.
          */
        sort?;
        /** The page index to set in a data view.
          * See wijmo.data.IPagedDataView.pageIndex for details.
          */
        pageIndex?: number;
        /** The page size to set in a data view.
          * See wijmo.data.IPagedDataView.pageSize for details.
          */
        pageSize?: number;
    }

    /** Defines sorting by one property. */
    export interface ISortDescriptor {
        /** Name of the property to sort by. */
        property: string;
        /** A value indicating whether sorting must be ascending or descending. */
        asc: boolean;
    }
    
    /** Defines a filter for one property. */
    export interface IFilterDescriptor {
        /** A name of a built-in operator or an instance of IFilterOperator interface.
          * @remarks
          * Names of built-in operators: 
          *   ==, equals
          *   !=, notequal
          *   doesnotcontain, notcontain
          *   >, greater
          *   <, less
          *   >=, greaterorequal
          *   >=, lessorequal
          *   isnotempty, notisempty
          *   isnotnull, notisnull
          */
        operator;
        /** A filter operand, not used if the operator has an arity of 1. */
        value?;
    }

    /** Defines a filter operator that can be used as a IFilterDescriptor.operator property value. */
    export interface IFilterOperator {
        /** The number of operands the operator accepts. Support values: 1 and 2. */
        arity: number;
        /** Apply the operator to value(s) */
        apply: (value, operand) => any;
        /** Dispaly name of the opreator */
        displayName: string;

    }

    /** Defines a property of an IDataView element. */
    export interface IPropertyDescriptor {
        /** Property name. */
        name: string;
        /** Property value type. */
        type?: string;
    }

    /** A result of IDataView refresh operation, implements a jQuery promise. */
    export interface IRefreshResult<T> extends JQueryPromise<T> {
    }

    /** Infrastructure. */
    export interface ICloneable {
        /** @ignore */
        clone(): ICloneable
    }

    /** Provides filtering, sorting and loading of a collection. */
    export interface IDataView extends IDisposable, ISubscribable {
        /** Returns the number of items in the current view. */
        count(): number;
        /** Returns an element in the view by index.
          * @param {Number} index The zero-based index of the element to get
          * @remarks Throws an exception if the index is out of range   
          */
        item(index: number): any;

        /** Returns properties for elements in the view */
        getProperties(): IPropertyDescriptor[];
        /** Returns the current value of the property in the element at the specified index.
          * @param {Number} index The zero-based index of the element with the property value that is to be read.
          * @param {string} property The name of the property to read
          * @returns The current value of the property in the element
          */
        getProperty(index: number, property: string): any;
        /** Returns the current value of the property in the element.
          * @param {Object} item The element with the property value that is to be read.
          * @param {string} property The name of the property to read
          * @returns The current value of the property in the element
          */
        getProperty(item: Object, property: string): any;
        /** Sets the value of the property in the element at the specified index.
          * @param {Number} index The zero-based index of the element with the property value that is to be set.
          * @param {string} property The name of the property to set
          * @param newValue The new value
          * @remarks
          * If the class implementing IDataView also implements IEditableDataView and the element is being edited, 
          * then the property value change is done within the editing transaction.
          */
        setProperty(index: number, property: string, newValue): void;
        /** Sets the value of the property in the element.
          * @param {Object} item The element with the property value that is to be set.
          * @param {string} property The name of the property to set
          * @param newValue The new value
          * @remarks
          * If the class implementing IDataView also implements IEditableDataView and the element is being edited, 
          * then the property value change is done within the editing transaction.
          */
        setProperty(item: Object, property: string, newValue): void;

        /** Returns a value that indicates whether the data view supports filtering */
        canFilter(): boolean;
        /** The filter to be applied to the data view. When assigned, the data view is refreshed.
          *
          * @remarks
          * The following formats of a filter are defined by the IDataView interface:
          * 1) A predicate function. In this case the filtering is always done on the client-side
          * 2) A decomposable property-based filter or its short form described below. Can be used for server-side filtering.
          * 
          * Property based filter is a hash where a key matches an element property and a value may be one of these:
          * 1) An instance of FilterDescriptor interface
          * 2) A value, so only elements with the same property values satisfy a predicate
          *
          * Examples:
          *   studentView.filter({
          *     name: "Alfred",
          *     age: { operator: ">", value: "10" }
          *   });
          */
        filter: IMutableObservable;

        /** Returns a value that indicates whether the data view supports sorting */
        canSort(): boolean;
        /** An observable property that gets or sets the sort to be applied to the data view.
          * When assigned, the data view is refreshed.
          *
          * @remarks
          * The following formats of a sort are defined by the IDataView interface:
          * 1) A string, a comma-separated list of property names with optional " asc" or " desc" suffixes.
          * 2) An array of ISortDescriptor instances
          * 3) A comparison function of type (a, b) => number. In this case sorting and paging are always done on the client-side
          * 
          * Examples:
          *   customerView.sort("firstName");
          *   productView.sort("unitPrice desc, productName");
          */
        sort: IMutableObservable;

        /** Reloads the data view.
          *
          * @param {IShape} shape
          * Settings for filtering, sorting and/or paging to be applied before loading.
          *
          * In contrast to filter/sort properties in IDataView, 
          * the format of properties in the shape parameter does not have to follow the specs defined in IDataView.
          * For instance, BreezeDataView accepts a Breeze.Predicate as shape.filter.
          * However the IDataView properties must not violate the IDataView specs. If a custom filter format cannot be aligned to the IDataView specs,
          * then the properties values must be functions or null.
          *
          * @param {boolean} local Prevents server requests and applies filtering/sorting/paging of the source array on the client-side.
          *
          * @returns {IRefreshResult} A promise object that can be used to receive notification of asynchronous refresh completion or failure.
          * 
          * @remarks
          * Depending on the implementation, data can be loaded from a local or a remote data source. 
          * The implementation transaltes filtering/sorting/paging defined in the IDataView to the data provider specific format.
          * If it is possible, then the data is reshaped on the client-side.
          *
          * The optional shape parameter can be used to change multiple filtering/sorting/paging settings at a time to avoid unnecessary refreshes.
          *
          * During the refresh, the isLoading property value must be true. After a successful refresh, the isLoaded property value must be true.
          *
          * When the method is called, the pending refresh, if any, is canceled.
          */
        refresh(shape?: IShape, local?: boolean): IRefreshResult<any>;
        /** Cancels the ongoing refresh operation */
        cancelRefresh();
        /** A value indicating whether the data view is being loaded */
        isLoading: IObservable;
        /** A value indicating whether the data view is loaded */
        isLoaded: IObservable;

        /** The current element index in the data view */
        currentPosition: IMutableObservable;
        /** The current element in the data view */
        currentItem: IMutableObservable;

        /** Returns the element array before applying client-side filtering/sorting/paging.
          * @remarks In the case of a remote data source, usually it is the array returned by a server.
          */
        getSource(): any[];
    }

    /** Provides paging of a collection. */
    export interface IPagedDataView extends IDataView {
        /** The number of pages. */
        pageCount: IObservable;
        /** The number of elements before paging is applied. */
        totalItemCount: IObservable;
        /** The current page index. */
        pageIndex: IMutableObservable;
        /** The current page index. */
        pageSize: IMutableObservable;
    }

    /** Provides adding, modifying and removing of elements in a collection. */
    export interface IEditableDataView extends IDataView {
        /** The element currently being edited. 
          * @remarks There may be only one element being edited at time.
          */
        currentEditItem: IObservable;
        /** Returns a value indicating whether the element being edited is added by the add(item) or addNew(initialValues) methods. */
        isCurrentEditItemNew(): boolean;

        /** Returns a value that indicates whether the current changes can be committed */
        canCommitEdit(): boolean;
        /** Commits the changes made to the currently editing element
          * @remarks After a successful commit, the currentEditItem property value must be null.
          * If the item being edited was new, it is added to the underlying data source.
          * If the element does not satisfy the filter, then it is removed from the data view, but not from the data source.
          */
        commitEdit(): void;

        /** Returns a value that indicates whether the current changes can be canceled. */
        canCancelEdit(): boolean;
        /** Cancels the changes made to the currently editing element since the editing was started.
          * @remarks After a successful cancelation, the currentEditItem property value must be null.
          */
        cancelEdit(): void;

        /** Starts editing of an element at the index.
          * @param {Number} index The zero-based index of the element to edit.
          * @remarks 
          * Commits the element currently being edited, if any.
          */
        editItem(index: number);
        /** Starts editing of the element.
          * @param {Object} element The element to edit. Defaults to the value of IDataView.currentItem property.
          * @remarks 
          * Commits the element currently being edited, if any.
          */
        editItem(item?: Object);

        /** Returns a value that indicates whether the add(item) method is implemented. */
        canAdd(): boolean;
        /** Adds a new element and marks it as being edited.
          * @param {Object} item The item to add.
          * @remarks
          * Adds a new element to the data view, but not to the underlying data source.
          * Commits the element currently being edited, if any.
          */
        add(item: Object);

        /** Returns a value that indicates whether the addNew() method is implemented. */
        canAddNew(): boolean;
        /** Creates, adds a new element and marks it as being edited.
          * @param {Object} initialValue Optional hash of property values for the a element.
          * @remarks
          * Works the same way as add(item), but the new item is created by the data view.
          */
        addNew(initialValues?);

        /** Returns a value that indicates whether the remove method overloads are implemented. */
        canRemove(): boolean;
        /** Removes an element at the specified index.
          * @param {Number} index The zero-based index of the element to remove
          */
        remove(index: number): void;
        /** Removes an element.
          * @param {Object} item The element to remove. Defaults to the value of the IDataView.currentItem property.
          * @returns True if an element was removed.
          */
        remove(item?: Object): boolean;
    }

    //#region Data view factories
    /** A function that creates an IDataView for a data source if possible.*/
    export interface IDataViewFactory {
        (data): IDataView;
    }

    var dataViewFactories = [];
    /** Registers a new IDataView provider.
      * @param {IDataViewFactory} factory A function that creates a IDataView for a data source if possible. Otherwise returns null.
      * @returns An IDisposable that can be used to remove the registration.
      * @remarks
      * Use this method to provide your own IDataView implementation for a specific data source. See wijmo.data.breeze.ts for an example.
      */
    export function registerDataViewFactory(factory: IDataViewFactory) {
        if (!$.isFunction(factory)) {
            errors.argument("factory");
        }
        dataViewFactories.push(factory);
        return {
            dispose: () => { util.remove(dataViewFactories, factory); }
        };
    }

    registerDataViewFactory(view => isDataView(view) && view);
    registerDataViewFactory(array => $.isArray(array) && new ArrayDataView(array));
    //#endregion Data view factories

    /** Creates an IDataView for a data source.
      * @param src A data source, can be anything that is supported by the registered IDataView providers
      * @returns An IDataView instance for the data source.
      */
    export function asDataView(src): IDataView {
        if (isDataView(src)) return src;
        var view: IDataView = util.some(dataViewFactories, p => p(src));
        return view || errors.unsupportedDataSource();
    }
    /** Returns true if the view parameter is a IDataView */
    export function isDataView(view: IDataView) {
        return view && $.isFunction(view.count) && $.isFunction(view.item) && $.isFunction(view.getProperty);
    }
}