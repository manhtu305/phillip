﻿/// <reference path="core.ts"/>
/// <reference path="util.ts"/>

module wijmo.data {
    var $ = jQuery;

    export interface ISubscription extends IDisposable {}
    export interface ISubscribable {
        subscribe(handler: (newValue) => any, context?): ISubscription;
    }
    /** @ignore */
    export class SubscriberEntry {
        constructor(public handler: Function, public context) { }
        trigger(args: any[]) {
            return this.handler.apply(this.context, args);
        }
    }
    /** @ignore */
    export class Subscribable implements ISubscribable {
        _entries: SubscriberEntry[] = [];
        constructor(public defaultContext) { }
        subscribe(handler, context = this.defaultContext) {
            var entry = new SubscriberEntry(handler, context);
            this._entries.push(entry);
            return {
                dispose: () => util.remove(this._entries, entry)
            };
        }
        trigger(...args: any[]) {
            util.each(this._entries, (_, e: SubscriberEntry) => e.trigger(args));
        }

        static makeSubscribable(obj) {
            var subscribable = new Subscribable(obj);
            obj.subscribe = $.proxy(subscribable.subscribe, subscribable);
            return subscribable;
        }
    }
    export function isSubscriptable(subscribable: ISubscribable) {
        return $.isFunction(subscribable.subscribe);
    }


    export interface IObservable extends ISubscribable {
        (): any;
    }

    export interface IMutableObservable extends IObservable {
        (value: any): void;
        read(): IObservable;
    }

    export interface INumericMutableObservable extends IMutableObservable {
        (value: number): void;
        (): number;
        inc(): number;
        dec(): number;
        read(): IObservable;
    }

    /** @ignore */
    export class BaseObservable {
        public _subscribable: Subscribable;
        subscribe(handler: (newvalue) => void , context?) {
            this._subscribable = this._subscribable || new Subscribable(this);
            return this._subscribable.subscribe(handler, context);
        }
        _trigger(...args: any[]) {
            if (this._subscribable) {
                this._subscribable.trigger.apply(this._subscribable, arguments);
            }
        }
    }
    /** @ignore */
    export class _ReadOnlyObservable extends BaseObservable {
        constructor(private mutable: _MutableObservable) {
            super();
        }

        _call() {
            return this.mutable.value;
        }
    }
    var ReadOnlyObservable = util.funcClass(_ReadOnlyObservable);

    /** @ignore */
    export class _MutableObservable extends BaseObservable {
        constructor(public value?, public checkNewValue = false) {
            super();
        }
        _call(newValue?) {
            if (arguments.length > 0 && (!this.checkNewValue || newValue !== this.value)) {
                this.value = newValue;
                this._trigger(newValue);
                if (this._readOnly) {
                    this._readOnly._trigger(newValue);
                }
            }
            return this.value;
        }

        _readOnly: _ReadOnlyObservable;
        read() {
            this._readOnly = this._readOnly || new ReadOnlyObservable(this);
            return this._readOnly;
        }
    }
    var MutableObservable = util.funcClass(_MutableObservable);

    /** @ignore */
    export class _NumericMutableObservable extends _MutableObservable {
        constructor(public value?: number) {
            super(value, false);
        }

        change(delta: number): number {
            return this._call(this._call() + delta);
        }
        inc() {
            return this.change(1);
        }
        dec() {
            return this.change(-1);
        }
    }
    var NumericMutableObservable = util.funcClass(_NumericMutableObservable);

    export function observable(value = null): IMutableObservable {
        return <IMutableObservable><any>new MutableObservable(value);
    }
    /** @ignore */
    export function numericObservable(value = 0): INumericMutableObservable {
        return new NumericMutableObservable(value);
    }
    /** @ignore */
    export function observableWithNewValueCheck(value = null): IMutableObservable {
        return <IMutableObservable><any>new MutableObservable(value, true);
    }
    export function isObservable(observable: IObservable) {
        return $.isFunction(observable) && isSubscriptable(observable);
    }
}