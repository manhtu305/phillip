﻿/// <reference path="dataView.ts"/>

/** @ignore */
module wijmo.data.sorting {
    export interface ICompiledSort {
        isEmpty: boolean;
        original;
        normalized: ISortDescriptor[];
        propertyCompareTo: (a, b) => number;
        compare: (a, b) => number;
    }

    function normalize(sort): ISortDescriptor[] {
        var result = [];
        sort = util.isString(sort)
            ? sort.split(/,\s*/)
            : !$.isArray(sort) ? [sort] : sort.slice(0);

        sort = $.isArray(sort) ? sort.slice(0) : [sort];
        util.each(sort, function (_, prop) {
            var asc = true,
                i: number;
            if (prop == null) return;
            if (!util.isString(prop)) {
                if (prop.property != null) {
                    result.push(prop);
                }
                return;
            }

            if (prop[0] === "-") {
                asc = false;
                prop = prop.substr(1);
            } else {
                var match = /\s(asc|desc)\s*$/.exec(prop);
                if (match) {
                    prop = prop.substr(0, match.index);
                    asc = !(match[1] === "desc");
                }
            }

            result.push({ property: prop, asc: asc });
        });

        return result.length > 0 ? result : null;
    }
    export function compile(sort, compareTo = util.compare) {
        var normalized = normalize(sort);
        var result: ICompiledSort = {
            isEmpty: true,
            original: sort,
            propertyCompareTo: compareTo,
            compare: null,
            normalized: normalized
        };

        if (normalized != null) {
        	result.isEmpty = false;
            result.compare = function (a, b) {
                var i = 0,
                    cmp: number,
                    descr: ISortDescriptor;
                for (i = 0; i < normalized.length; i++) {
                    descr = normalized[i];
                    cmp = compareTo(util.getProperty(a, descr.property), util.getProperty(b, descr.property));
                    if (cmp !== 0) {
                        if (!descr.asc) {
                            cmp = -cmp;
                        }
                        return cmp;
                    }
                }

                return 0;
            };
        }
        return result;
    }
}