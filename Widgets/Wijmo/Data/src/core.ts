﻿/// <reference path="../../External/declarations/jquery.d.ts"/>

module wijmo {
    export var expando = ".wijmo";

    /** @ignore */
    export class WijmoError implements Error {
        stack = "Wijmo" + (<any>new Error()).stack;
        name = "WijmoError";

        constructor(public message: string) {}
    }
    var wijerr: any = WijmoError;
    wijerr.prototype = new Error();
    wijerr.prototype["constructor"] = wijerr;

    export interface IDisposable {
        dispose(): void;
    }

}

module wijmo.data {
    /** @ignore */
    export class Expando {
        constructor(public object: Object) { }
        static getFrom(obj, create = true): any {
            var propertyName = wijmo.expando,
                ext: Expando;
            if (Object(obj) !== obj) return null;

            ext = obj[propertyName];
            if (ext && ext.object !== obj) {
                ext = null;
            }

            if (create && !(ext instanceof Expando && Object.prototype.hasOwnProperty.call(obj, propertyName))) {
                ext = new Expando(obj);
                try {
                    Object.defineProperty(obj, propertyName, {
                        value: ext,
                        configurable: false,
                        enumerable: false,
                        writable: false
                    });
                } catch (e) {
                    obj[propertyName] = ext;
                }
            }
            return ext;
        }
    }
}

/** @ignore */
module wijmo.data.util {
    export function funcClass(ctor): any {
        return function () {
            var result = function () {
                return ctor.prototype._call.apply(result, arguments);
            };
            $.extend(result, ctor.prototype);
            ctor.apply(result, arguments);
            return result;
        };
    }
}