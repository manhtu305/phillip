﻿/// <reference path="dataView.ts"/>

/** @ignore */
module wijmo.data.filtering {
    var $ = jQuery;

    export var opMap = {
        "==": "equals",
        doesnotcontain: "notcontain",
        "!=": "notequal",
        ">": "greater",
        "<": "less",
        ">=": "greaterorequal",
        "<=": "lessorequal",
        isnotempty: "notisempty",
        isnotnull: "notisnull"
    };

    export interface IBuiltInFilterOperator extends IFilterOperator {
        name: string;
        applicableTo: string[];
    }

    export interface ICompiledFilterDescriptor extends IFilterDescriptor {
        op: IFilterOperator;
    }
    export interface ICompiledFilter {
        isEmpty: boolean;
        original;
        normalized;
        func: (x) => boolean;
    }

    function findOperator(name: string, throwIfNotFound = false) {
        name = name.toLowerCase();
        var op = ops[name];
        if (!op) {
            var mappedName = opMap[name];
            if (mappedName) {
                op = ops[mappedName];
            }
        }
        if (!op && throwIfNotFound) errors.unsupportedFilterOperator(name);
        return op;
    }

    export function normalizeCondition(cond): IFilterDescriptor {
        var filter: ICompiledFilterDescriptor;
        if (!$.isPlainObject(cond)) {
            return { operator: "==", op: ops.equals, value: cond };
        }

        var op: IFilterOperator = cond.operator || ops.equals;
        if (util.isString(cond.operator)) {
            if (cond.operator.toLowerCase() === "nofilter") return null;
            op = findOperator(cond.operator, true);
        } else if (!$.isFunction(op.apply)) {
            errors.unsupportedFilterOperator(op);
        }
        return { operator: cond.operator, op: op, value: cond.value };
    }

    export function compile(filter): ICompiledFilter {
        var result: ICompiledFilter = {
            isEmpty: false,
            original: filter,
            func: null,
            normalized: null
        };
        if ($.isFunction(filter)) {
            result.func = filter;
        } else if ($.isArray(filter)) {
            errors.unsupportedFilterFormat(filter);
        } else if (filter) {
            result.normalized = {};
            var hasConditions = false;
            util.each(filter, function (prop, cond) {
                if ($.isArray(cond)) {
                    errors.unsupportedFilterFormat();
                }

                cond = normalizeCondition(cond);
                if (cond) {
                    result.normalized[prop] = cond;
                    hasConditions = true;
                }
            });

            if (!hasConditions) {
                result.normalized = null;
            } else {
                result.func = x => util.every(result.normalized, (cond, prop) => {
                    var propValue = util.getProperty(x, prop);
                    return cond.op.apply(propValue, cond.value);
                });
            }
        }

        if (!result.normalized && !result.func) {
            result.isEmpty = true;
            result.func = x => true;
        }

        return result;
    }

    //#region operators

    export var ops = (function () {
        var ops = {},
            types = {
                str: ["string"],
                prim: ["string", "number", "datetime", "currency", "boolean"],
                comparable: ["string", "number", "datetime", "currency"]
            };

        function op(name, displayName, arity, types, apply) {
            return ops[name.toLowerCase()] = {
                name: name,
                displayName: displayName,
                arity: arity,
                applicableTo: types,
                apply: apply
            };
        }        

        function preprocessOperand(value) {
            if (value instanceof Date) {
                value = value.getTime();
            }
            if (util.isString(value)) {
                value = value.toLowerCase();
            }
            return value;
        }
        function bin(name, displayName, types, apply) {
            op(name, displayName, 2, types, function (left, right) {
                return apply(preprocessOperand(left), preprocessOperand(right));
            });
        }
        function unary(name, displayName, types, apply) {
            op(name, displayName, 1, types, apply);
        }
        function binprim(name, displayName, apply) {
            bin(name, displayName, types.prim, apply);
        }
        function binstr(name, displayName, apply) {
            bin(name, displayName, types.str, apply);
        }
        function bincomparable(name, displayName, apply) {
            bin(name, displayName, types.comparable, apply);
        }

        // Primitive binary operators
        binprim("Equals", "Equals", (l, r) => l == r);
        binprim("NotEqual", "Not equal", (l, r) => l != r);
        bincomparable("Greater", "Greater than", (l, r) => l > r);
        bincomparable("Less", "Less than", (l, r) => l < r);
        bincomparable("GreaterOrEqual", "Greater or equal", (l, r) => l >= r);
        bincomparable("LessOrEqual", "Less or equal", (l, r) => l <= r);

        // String operators
        binstr("Contains", "Contains", (left, right) => left == right || left && left.indexOf && left.indexOf(right) >= 0);
        binstr("NotContain", "Does not contain", (left, right) => left != right && (!left || !left.indexOf || left.indexOf(right) < 0));
        binstr("BeginsWith", "Begins with", (left, right) => left == right || left && left.indexOf && left.indexOf(right) == 0);
        binstr("EndsWith", "Ends with", function (left, right) {
            var idx;
            if (!util.isString(left) || !util.isString(right)) return false;
            idx = left.lastIndexOf(right);
            return idx >= 0 && left.length - idx === right.length;
        });
     
        // Unary operators
        unary("IsEmpty", "Is empty", types.str, x => !x && x !== 0 && x !== false); // null, undefined, or empty string
        unary("NotIsEmpty", "Is not empty", types.str, x => !!x || x === 0 || x === false);
        unary("IsNull", "Is null", types.prim, x => x == null);
        unary("NotIsNull", "Is not null", types.prim, x => x != null);
        
        return <{ [name: string]: IBuiltInFilterOperator; equals: IBuiltInFilterOperator; }> ops;
    })();

    //#endregion operators
}