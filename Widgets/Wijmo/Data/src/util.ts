﻿/// <reference path="../../Base/wijmo.d.ts"/>
/// <reference path="core.ts"/>
/// <reference path="observable.ts"/>

/** @ignore */
module wijmo.data.util {
    var $ = jQuery;

    export function clone(obj, deep = false) {
        if ($.isArray(obj)) {
            obj = obj.slice(0);
        } else if ($.isPlainObject(obj)) {
            obj = $.extend(!!deep, {}, obj);
        }
        return obj;
    }

    export function isString(str) {
        return typeof str === "string" || str instanceof String;
    }
    export function isNumeric(value) {
        return typeof value === "number";
    }

    export function isInternalProperty(p: string) {
        return p === expando || p === $.expando;
    }

    export function each(obj, fn: (key, value) => any) {
        $.each(obj, (key, value) => {
            if (!isInternalProperty(key)) {
                return fn.call(this, key, value);
            }
        });
    }
    export function map(obj, fn: (elem, key?) => any) {
        var result = $.map(obj, fn);
        delete result[expando];
        return result;
    }

    export function toStr(obj) {
        var text: string;
        if (obj && $.isFunction(obj.toString) && obj.toString !== Object.prototype.toString) {
            text = obj.toString();
        } else {
            text = JSON.stringify(obj);
        }
        if (text != null && text.length > 2 && text[0] === '"' && text[text.length - 1] === '"') {
            text = text.substr(1, text.length - 2);
        }
        return text;
    }
    export function format(format: string, ...args: any[]) {
        return format.replace(/{(\d+)}/g, function (m, index) {
            return toStr(args[parseInt(index, 10)]);
        });
    }

    export function every(obj, predicate: (value: any, key: any) => boolean) {
        var res: any = true;
        each(obj, (key, value) => {
            res = value;
            if (predicate) {
                res = predicate.call(res, res, key)
            }
            if (!res) {
                return false;
            }
        });
        return res;
    }
    export function some(obj, predicate: (value: any, key: any) => boolean) {
        var res: any = false;
        each(obj, (key, value) => {
            res = value;
            if (predicate) {
                res = predicate.call(res, res, key)
            }
            if (res) {
                return false;
            }
        });
        return res;
    }

    export function compare(a, b): number {
        var i, len, cmp;

        if (a == null) {
            return b == null ? 0 : -1;
        } else if (b == null) {
            return 1;
        }

        if ($.isArray(a) && $.isArray(b)) {
            len = Math.min(a.length, b.length);
            for (i = 0; i < len; i++) {
                cmp = compare(a[i], b[i]);
                if (cmp !== 0) {
                    return cmp;
                }
            }
            return a.length - b.length;
        } else if (isString(a) && isString(b)) {
            a = a.toLowerCase();
            b = b.toLowerCase();
            return a < b ? -1 : a > b ? 1 : 0;
        }

        cmp = a - b;
        return isNaN(cmp) ? 0 : cmp;
    }

    export function contains(array, elem) {
        return $.inArray(elem, array) >= 0;
    }
    export function remove(array: any[], elem) {
        var removed = 0,
            i: number;

        for (i = 0; i < array.length;) {
            if (array[i] !== elem) {
                i++;
            } else {
                array.splice(i, 1);
                removed++;
            }
        }
        return removed;
    }

    export function pageCount(totalCount: number, pageSize: number) {
        if (totalCount == -1) {
            return -1;
        } else if (totalCount == 0) {
            return 0;
        } else if (!pageSize) {
            return 1;
        } else {
            return Math.ceil(totalCount / pageSize);
        }
    };

    export function executeDelayed(fn: Function, context?, ...args: any[]) {
        function callback() {
            return fn.apply(context, args);
        }

        if (typeof setTimeout === typeof undefined) {
            return callback();
        } else {
            setTimeout(callback, 10);
        }
    }

    export function logError(message: string) {
        if (typeof console == "undefined") return;
        if (console.error) {
            console.error(message);
        } else if (console.log) {
            console.log(message);
        }
    }

    export function getProperty(obj: Object, property: string) {
        var start = 0,
            value: any = obj,
            key: string;
        while (true) {
            var point = property.indexOf('.', start);
            if (point >= 0) {
                key = property.substring(start, point);
                start = point + 1;
            } else if (start > 0) {
                key = property.substring(start);
            } else {
                key = property;
            }

            value = value[key];
            if (isObservable(value)) {
                value = value();
            }

            if (point < 0) {
                break;
            }
        }
        return value;
    }
    export function setProperty(obj: Object, property: string, newValue) {
        var start = 0,
            key: string;
        while (true) {
            var point = property.indexOf('.', start);
            if (point >= 0) {
                key = property.substring(start, point);
                start = point + 1;
            } else if (start > 0) {
                key = property.substring(start);
            } else {
                key = property;
            }

            var value = obj[key];

            if (point >= 0) {
                if (isObservable(value)) {
                    value = value();
                }
                obj = value;
            } else {
                if (isObservable(value)) {
                    value(newValue);
                } else {
                    obj[key] = newValue;
                }
                break;
            }
        }
    }

    export function isClassInstance(instance) {
        return typeof instance === "object" && !$.isArray(instance) && !$.isPlainObject(instance)
            && instance.constructor !== (<any> Object.prototype).constructor;
    }

    export function convertDateProperties(entities) {
        util.each(entities, function (_, entity) {
            if (!entity || typeof entity !== "object") return;
            util.each(entity, function (key, value) {
                var match;
                if (!util.isString(value)) return;
                match = /\/Date\((-?\d+)\)\//.exec(value);
                if (!match) return;
                entity[key] = new Date(parseInt(match[1], 10));
            });
        });
    }

    export class HashMapEntry {
        key;
        value;

        constructor(key) {
            this.key = key;
        }
    }

    export class HashMap {
        hash;
        nullEntry: HashMapEntry;

        constructor() {
            this.hash = {};
        }

        getEntry(key, create = false) {
            if (key === null) {
                if (!this.nullEntry && create) {
                    this.nullEntry = new HashMapEntry(key);
                }
                return this.nullEntry;
            }

            var strKey = String(key);
            var list: HashMapEntry[] = this.hash[strKey];
            var entry: HashMapEntry;
            if (list == null) {
                if (!create) {
                    return null;
                }

                list = [];
                this.hash[strKey] = list;
            }

            for (var i = 0; i < list.length; i++) {
                if (list[i].key === key) {
                    return list[i];
                }
            }

            if (create) {
                entry = new HashMapEntry(key);
                list.push(entry);
            }

            return null;
        }

        containsKey(key) {
            return !!this.getEntry(key);
        }

        get (key, defaultValue = null) {
            var entry = this.getEntry(key);
            return entry ? entry.value : defaultValue;
        }

        put(key, value) {
            this.getEntry(key, true).value = value;
        }
    }
}