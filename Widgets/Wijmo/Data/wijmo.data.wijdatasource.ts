/// <reference path="src/arrayDataView.ts"/>

/*globals jQuery, Globalize, wijmo */
/*
* Depends:
*  wijmo.data.js
*  globalize.js
*  jquery.js
*
*/

module wijmo.data {
	var $ = jQuery;

	declare var wijdatasource;

    class wijdatasourceReader {
        private _originalReader;

        constructor(originalReader) {
            this._originalReader = originalReader;
        }

        read(wijDataSource) {
            wijDataSource.items = null;

            if (this._originalReader && $.isFunction(this._originalReader.read)) {
                this._originalReader.read(wijDataSource);
            }

            if (!$.isArray(wijDataSource.items)) {
                if ($.isArray(wijDataSource.data)) {
                    wijDataSource.items = wijDataSource.data;
                } else if (wijDataSource.data && $.isArray(wijDataSource.data.rows)) {
                    wijDataSource.items = wijDataSource.data.rows; // remoteDynamical
                } else {
                    wijDataSource.items = [];
                }
            }

            if (wijDataSource.dynamic) {
                if (!wijDataSource.data || isNaN(wijDataSource.data.totalRows)) {
                    throw "totalRows value is missing";
                }
            }
        }
    }

    class WijdatasourceView extends ArrayDataViewBase {
        isRemote = true;
        localPaging = true;
        private _origLoaded;
        private _origReader;

        constructor(public dataSource) {
            super();

            this._origLoaded = dataSource.loaded;
            this._origReader = dataSource.reader;

            dataSource.loaded = (e, data) => {
                this._loaded();
                if ($.isFunction(this._origLoaded)) {
                    this._origLoaded.apply(this, arguments);
                }
            };

            dataSource.reader = new wijdatasourceReader(dataSource.reader);

            var hasItems = dataSource.items && dataSource.items.length > 0;
            if ($.isArray(dataSource.data) && !hasItems) {
                dataSource.read();
            }

            if (dataSource.items) {
                this._loaded();
            }
        }

        dispose() {
            this.dataSource.loaded = this._origLoaded;
            this.dataSource.reader = this._origReader;
            super.dispose();
        }

        getProperties(): IPropertyDescriptor[] {
            return this.sourceArray && this.sourceArray.length ? ArrayDataViewBase._getProps(this.sourceArray[0]) : [];
        }
        _loaded() {
            this.sourceArray = this.dataSource.items;
            if (this.dataSource.data && util.isNumeric(this.dataSource.data.totalRows)) {
                this._totalItemCount(this.dataSource.data.totalRows);
            }
            var def = super._localRefresh(!this.dataSource.dynamic);
            if (this._currentDeferred) {
                def.then(this._currentDeferred.resolve);
            }
        }

        _currentDeferred: JQueryDeferred<any>;
        _origDataOption;
        _remoteRefresh() {
            if (this._currentDeferred && this._currentDeferred.state() === "pending") {
                this._currentDeferred.fail();
            }
            this._currentDeferred = $.Deferred();

            var userData: any = {},
                forceReload = false;

            if (this.dataSource.dynamic) {
                forceReload = true;
                userData.data = this._prepareRequest();
                if (this.dataSource.proxy) {
                    if (!this._origDataOption) {
                        this._origDataOption = $.extend({}, this.dataSource.proxy.options.data);
                    }
                    this.dataSource.proxy.options.data = $.extend({}, this._origDataOption, userData.data);
                }
            }

            this.dataSource.load(userData, forceReload);
            return this._currentDeferred;
        }

        _prepareRequest() {
            return {
                filtering: this._prepareFilterRequest(),
                paging: this._preparePageRequest(),
                sorting: this._prepareSortRequest()
            };
        }

        _prepareFilterRequest() {
            var result = [];

            if (!this._shape._compiledFilter.isEmpty && this._shape._compiledFilter.normalized) {
                $.each(this._shape._compiledFilter.normalized, (prop, cond) => {
                    result.push({
                        dataKey: prop,
                        filterOperator: cond.op.Name,
                        filterValue: cond.value
                    });
                });
            }

            return result;
        }

        _preparePageRequest() {
            return {
                pageIndex: this._shape.pageIndex(),
                pageSize: this._shape.pageSize()
            };
        }

        _prepareSortRequest() {
            if (this._shape._compiledSort.isEmpty || !this._shape._compiledSort.normalized || this._shape._compiledSort.normalized.length == 0) {
                return [];
            }

            return $.map(this._shape._compiledSort.normalized, (sd: ISortDescriptor) => {
                return {
                    dataKey: sd.property,
                    sortDirection: sd.asc ? "ascending" : "descending"
                };
            });
        }
    }

    registerDataViewFactory(wds => {
        if (typeof wijdatasource !== "function" || !(wds instanceof wijdatasource)) return;
        return new WijdatasourceView(wds);
    });
}