﻿/// <reference path="src/util.ts"/>
/// <reference path="src/remoteDataView.ts"/>

/*globals jQuery, Globalize, wijmo */
/*
* Depends:
*  wijmo.data.js
*  globalize.js
*  jquery.js
*
*/

module wijmo.data {

    /** 
    * The ajax result which contains the data and the total items count.
    */
    export interface IAjaxResult {
        /** 
        * Gets the data from the ajax result.
        */
        results: any[];

        /** 
        * Gets the total items count from the ajax result.
        */
        totalItemCount?: number;
    }

    export interface IAjaxDataViewOptions extends IRemoteDataViewOptions {
        ajax?: JQueryAjaxSettings;
        onRequest?: (settings: JQueryAjaxSettings, shape: IShape) => JQueryAjaxSettings;
        onResponse?: (data) => IAjaxResult;

        /** 
        * @ignore
        */
        serverSettings?: {
            skip?: string;
            take?: string;
            results?: string;
            totalItemCount?: string;
        };
    }

    export class AjaxDataView extends RemoteDataView {
        isRemote = true;
        
        constructor(public url: string, public options?: IAjaxDataViewOptions) {
            super();
            if (!url) {
                errors.noUrl();
            }
            this._construct(options);
        }

        _construct(options: IAjaxDataViewOptions) {
            options = $.extend(true, <IAjaxDataViewOptions> {
                ajax: {
                    data: {},
                    timeout: 60 * 1000,
                },
                serverSettings: {
                    // request
                    skip: "$skip",
                    take: "$top",
                    // response
                    results: "results",
                    totalItemCount: "totalItemCount"
                }
            }, options);
            super._construct(options);
        }

        _currentRequest: {
            xhr: JQueryXHR;
            canceled: boolean;
        };

        _remoteRefresh() {
            var deferred = $.Deferred(),
                options = this.options,
                settings = derive(this.options.ajax),
                format = options.serverSettings;
            settings.data = settings.data && derive(settings.data) || {};

            if (format && !this.options.localPaging) {
                if (format.skip && this._shape._skip > 0) {
                    util.setProperty(settings.data, format.skip, this._shape._skip);
                }
                if (format.take && this._shape._take > 0) {
                    util.setProperty(settings.data, format.take, this._shape._take);
                }
            }

            if (this.options.onRequest) {
                settings = this.options.onRequest(settings, this._shape.toObj()) || settings;
            }

            var xhr: JQueryXHR = $.ajax(this.url, $.extend({}, settings, {
                error: (xhr, textStatus, errorThrown) => {
                    if (request.canceled) return;
                    util.logError("Could not load " + this.url);
                    if (errorThrown) {
                        util.logError(errorThrown);
                    }
                    deferred.rejectWith(this, arguments);
                },
                success: (res) => {
                    if (request.canceled) return;
                    var result: IAjaxResult;

                    if (util.isString(res)) {
                        res = tryParseAsJson(res);
                    }

                    if (this.options.onResponse) {
                        result = this.options.onResponse(res);
                    } else if ($.isArray(res)) {
                        result = { results: res };
                    } else if (format) {
                        result = {
                            results: util.getProperty(res, format.results),
                            totalItemCount: util.getProperty(res, format.totalItemCount)
                        };
                        if (util.isString(result.totalItemCount)) {
                            result.totalItemCount = parseInt(<any>result.totalItemCount, 10);
                        }
                    }

                    if (!result.results) {
                        errors.ajax_arrayResponseExpected(res);
                    }
                    util.convertDateProperties(result.results);

                    this.sourceArray = result.results;
                    if (!options.localPaging && util.isNumeric(result.totalItemCount)) {
                        this._totalItemCount(result.totalItemCount);
                    }
                    this._localRefresh().then(deferred.resolve);
                },
                complete: () => {
                    this._currentRequest = null;
                }
            }));
            var request = this._currentRequest = {
                xhr: xhr,
                canceled: false
            };
            return deferred.promise();
        }
        cancelRefresh() {
            if (this._currentRequest) {
                this._currentRequest.canceled = true;
                if (this.options.ajax.dataType !== "jsonp") {
                    this._currentRequest.xhr.abort();
                }
                this._currentRequest = null;
            }
        }

        /** @ignore */
        clone(): AjaxDataView {
            return new AjaxDataView(this.url, $.extend(true, {}, this.options));
        }
    }

    // OData

    export class ODataView extends AjaxDataView {
        _construct(options: IAjaxDataViewOptions) {
            options = $.extend(true, <IAjaxDataViewOptions>{
                ajax: {
                    jsonp: "$callback",
                    data: {
                        $format: "json",
                        $inlinecount: "allpages"
                    }
                },
                onRequest: function (settings: JQueryAjaxSettings, shape) {
                    if (typeof shape.filter == "object" && shape.filter != null) {
                        settings.data.$filter =  ODataView._filterExpr(shape.filter);
                    }
                    if ($.isArray(shape.sort)) {
                        settings.data.$orderby = $.map(shape.sort, sd => sd.property + (sd.asc ? "" : " desc")).join(", ");
                    }
                    return settings;
                },
                onResponse: function (res): IAjaxResult {
                    var result: IAjaxResult;
                    if (res.d) {
                        res = res.d;
                        if ($.isArray(res)) {
                            result = { results: res };
                        } else {
                            result = { results: res.results };
                            var totalCount = parseInt(res.__count, 10);
                            if (util.isNumeric(totalCount) && !options.localPaging) {
                                result.totalItemCount = totalCount;
                            }
                        }
                    } else if (res.value) {
                        result = { results: res.value };
                        var totalCount = parseInt(res["odata.count"], 10);
                        if (util.isNumeric(totalCount) && !options.localPaging) {
                            result.totalItemCount = totalCount;
                        }
                    } 

                    return result;
                }
            }, options);
            super._construct(options);
        }

        /** @ignore */
        clone(): ODataView {
            return new ODataView(this.url, $.extend(true, {}, this.options));
        }

        static _filterOperators = {
            "<": "lt",
            "<=": "le",
            ">": "gt",
            ">=": "ge",
            "==": "eq",
            "!=": "ne",
            less: "lt",
            lessorequal: "le",
            greater: "gt",
            greaterorequal: "ge",
            equals: "eq",
            notequal: "ne",
            contains: true,
            notcontains: true,
            beginswith: true,
            endswith: true
        };

        static _filterExpr(filter) {
            if (!filter) return "";
            var expressions = [];

            $.each(filter, (property, cond) => {
                expressions.push(ODataView._conditionExpr(property, cond));
            });

            return expressions.join(" and ");
        }
        static _conditionExpr(property, cond) {
            if ($.isArray(cond)) {
                var oFilter = [];
                util.each(cond, (_, cond) => {
                    if (util.isString(cond)) return;
                    oFilter.push(ODataView._conditionExpr(property, cond));
                });
                return oFilter.join(cond[0]);
			} else {
				var value = cond.value,
					args = "(" + property + ", '" + value + "')",
					op = cond.op.name.toLowerCase(),
					quote = true;

				if (value instanceof Date) {
					quote = false;
					value = this._convertDate(value);
				}

                switch (op) {
                    case "contains":
                        return "indexof" + args + " ge 0";
                    case "notcontains":
                        return "indexof" + args + " lt 0";
                    case "beginswith":
                        return "startswith" + args + " eq true";
                    case "endswith":
                        return "endswith" + args + " eq true";
                    default:
                        op = ODataView._filterOperators[op] || op;
                        return property + " " + op + " " + ((isNaN(value) && quote) ? ("'" + value + "'") : value);
                }
            }
		}

		static _convertDate(date: Date, toUTC?: boolean): string {
			var pad = (val: number): string => {
				return (val < 10) ? "0" + val : val + "";
			};

			if (date) {
				var dateStr = toUTC
					? date.getUTCFullYear() + '-' + pad(date.getUTCMonth() + 1) + '-' + pad(date.getUTCDate()) + 'T' + pad(date.getUTCHours()) + ':' + pad(date.getUTCMinutes()) + ':' + pad(date.getUTCSeconds()) + 'Z'
					: date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate()) + 'T' + pad(date.getHours()) + ':' + pad(date.getMinutes()) + ':' + pad(date.getSeconds());

				return "datetime'" + dateStr + "'";
			}

			return "";
		}
    }

    // util
    function derive(baseObj, newMembers?) {
        var result;
        if (Object.create) {
            try {
                result = Object.create(baseObj);
            } catch (err) {
                result = null;
            }
        }

        if (!result) {
            function Clazz() { }
            Clazz.prototype = baseObj;
            result = new Clazz();
        }

        if (newMembers) {
            $.extend(result, newMembers);
        }

        return result;
    }
    function tryParseAsJson(json) {
        try {
            return JSON.parse(json);
        }
        catch (err) {
            return json;
        }
    }

    errors._register({
        ajax_arrayResponseExpected: "Could not parse the server response. Specify onResponse option to parse the server response. \nResponse: {0}"
    });
}