﻿/// <reference path="src/remoteDataView.ts"/>
/// <reference path="../External/declarations/jquery.d.ts"/>
/// <reference path="../External/declarations/breeze.d.ts"/>

/*globals jQuery, Globalize, wijmo */
/*
* Depends:
*  wijmo.data.js
*  globalize.js
*  jquery.js
*
*/

module wijmo.data {

    export interface IBreezeDataViewOptions extends IRemoteDataViewOptions {
        /*
         * Whether or not inlineCount capability should be enabled.
         */
        inlineCount?: boolean;
    }

    /** @ignore */
    export class BreezeShape extends Shape {
        predicate: breeze.Predicate;
        entityType: breeze.EntityType;

        static opNameMap = {
            Greater: "GreaterThan",
            Less: "LessThan",
            GreaterOrEqual: "GreaterOrEqualThan",
            LessOrEqual: "LessOrEqualThan",
            BeginsWith: "StartsWith"
        };

        toPredicate(normalizedFilter): breeze.Predicate {
            var result: breeze.Predicate = null;

            if (normalizedFilter && typeof normalizedFilter === "object") {
                util.each(normalizedFilter, (prop, cond) => {
                    var opName = BreezeShape.opNameMap[cond.op.name] || cond.op.name;
                    var predicate = new breeze.Predicate(prop, opName, cond.value);
                    result = !result ? predicate : result.and(predicate);
                });
            }

            return result;
        }

        _skipUpdatePredicate = false;
        onFilterChanged(newValue) {
            super.onFilterChanged(newValue);
            if (!this._skipUpdatePredicate) {
                this.predicate = this.toPredicate(this._compiledFilter.normalized);
            }
        }
        setFilter(filter) {
            //if (!breeze.Predicate.isPredicate(filter)) {
            if (!(filter instanceof breeze.Predicate)) {
                super.setFilter(filter);
                return;
            }

            this.predicate = filter;
            this._skipUpdatePredicate = true;
            try {
                super.setFilter(null);
            } finally {
                this._skipUpdatePredicate = false;
            }
        }

        applyToQuery(query: breeze.EntityQuery, applyPaging = true, inlineCount = true) {
            var sort = this._compiledSort;

            if (this.predicate) {
                query = query.where(this.predicate);
            }

            if (!sort.isEmpty) {
                var sortString = $.map(
                    sort.normalized,
                    (sd: ISortDescriptor) => sd.property + (sd.asc ? "" : " desc")
                ).join(", ");
                query = query.orderBy(sortString);
            }

            if (applyPaging) {
                if (this._skip > 0) {
                    query = query.skip(this._skip);
                }
                if (this._take > 0) {
                    query = query.take(this._take);
                }
                if (inlineCount) {
                    query = query.inlineCount(true);
                }
            }

            return query;
        }
    }


    export class BreezeDataView extends RemoteDataView {
        entityType: breeze.EntityType;
        shapedQuery: breeze.EntityQuery;

        constructor(public query: breeze.EntityQuery, public manager: breeze.EntityManager, public options?: IBreezeDataViewOptions) {
            super();
            if (!query) errors.argumentNull("query");
            if (!manager) errors.argumentNull("manager");
            this.entityType = <breeze.EntityType> manager.metadataStore.getEntityType(query.resourceName, true);
            this._shape.entityType = this.entityType;
            this._construct(options);
        }
        _construct(options: IBreezeDataViewOptions) {
            options = $.extend({ inlineCount: true }, options);
            super._construct(options);
        }

        static _breezeDataTypeToString(dataType: breeze.DataTypeSymbol) {
            if (!dataType) {
                return null;
            }

            if (dataType == breeze.DataType.Decimal) {
                return "number";
            } else if (dataType.isNumeric) {
                return "number";
            } else if (dataType == breeze.DataType.String) {
                return "string";
            } else if (dataType == breeze.DataType.DateTime || dataType == breeze.DataType.Time)  {
                return "datetime";
            } else if (dataType == breeze.DataType.Boolean) {
                return "boolean";
            }

            return null;
        }
        getProperties(): IPropertyDescriptor[]{
            if (this.entityType && this.entityType.dataProperties) {
                return $.map(this.entityType.dataProperties, prop => {
                    return {
                        name: prop.name,
                        type: BreezeDataView._breezeDataTypeToString(prop.dataType)
                    }
                });
            }
            return this.entityType ? this.entityType.dataProperties : [];
        }

        //#region loading

        _shape: BreezeShape;
        _createShape(onChanged: (paging?: boolean) => void ) {
            return new BreezeShape(onChanged);
        }

        _currentRequest: { canceled: boolean; };
        _remoteRefresh() {
            var result = $.Deferred();
            this.shapedQuery = this._shape.applyToQuery(this.query, !this.localPaging, this.options.inlineCount);
            var request = this._currentRequest = { canceled: false };
            this.manager.executeQuery(this.shapedQuery)
                .fail(error => {
                    if (error) {
                        util.logError(error);
                    }
                    result.reject(error);
                })
                .then((res: any) => {
                    if (request.canceled) return;
                    if (request === this._currentRequest) {
                        this._currentRequest = null;
                    }
                    this.sourceArray = res.results;
                    if (util.isNumeric(res.inlineCount)) {
                        this._totalItemCount(res.inlineCount);
                    }
                    this._localRefresh().then(result.resolve);
                });
            return result.promise();
        }

        cancelRefresh() {
            if (this._currentRequest) {
                this._currentRequest.canceled = true;
            }
        }

        //#endregion loading

        //#region editing
        _beginEdit(item, isNew: boolean) {
            if (!item || !item.entityAspect) {
                errors.breeze_notEntity(item);
            }
            super._beginEdit(item, isNew);
        }
        _currentItemHasChanges() {
            var entity: breeze.Entity = this.currentEditItem(),
                entityAspect = entity && entity.entityAspect,
                p;
            if (!entityAspect) {
                return false;
            }

            for (p in entityAspect.originalValues) {
                if (this.getProperty(this.currentEditItem, p) !== entityAspect.originalValues[p]) {
                    return true;
                }
            }

            return false;
        }

        _ensureEntity(entity) {
            if (!this.entityType) {
                errors.breeze_entityTypeNotResolved(this.query.resourceName);
            }
            if (!entity || !entity.entityAspect) {
                entity = this.entityType.createEntity(entity);
            }
            return entity;
        }
        canAddNew() {
            return !!this.entityType;
        }
        addNew(initialValues = {}) {
            return this.add(initialValues);
        }
        add(entity) {
            return super.add(this._ensureEntity(entity));
        }

        commitEdit() {
            if (!this.currentEditItem()) {
                return;
            }
            var entity: breeze.Entity = this.currentEditItem(),
                isNew = this.isCurrentEditItemNew,
                hasChanges = this._currentItemHasChanges();
            super.commitEdit();
            if (isNew) {
                this.manager.addEntity(entity);
            } else if (hasChanges && entity && entity.entityAspect) {
                entity.entityAspect.setModified();
            }
        }
        cancelEdit() {
            if (!this.currentEditItem()) {
                return;
            }
            var entity: breeze.Entity = this.currentEditItem();
            super.cancelEdit();
            if (entity && entity.entityAspect) {
                entity.entityAspect.setDeleted();
            }
        }
        _remove(entry: IViewEntry) {
            var entity: breeze.EntityAspect = entry.item.entityAspect;
            if (entity) {
                entity.setDeleted();
            }
            return super._remove(entry);
        }
        //#endregion editing

        /** @ignore */
        clone(): BreezeDataView {
            return new BreezeDataView(this.query, this.manager, $.extend(true, {}, this.options));
        }
    }

    errors._register({
        breeze_entityTypeNotResolved: "Entity type {0} not resolved",
        breeze_notEntity: "{0} is not a Breeze entity"
    });
}