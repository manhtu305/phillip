/// <reference path="../Base/jquery.wijmo.widget.ts" />
/// <reference path="../wijutil/jquery.wijmo.wijutil.ts" />

/*globals jQuery,$,window,alert,document,confirm,location,setTimeout,Globalize,amplify*/
/*jslint white: false */
/*jslint nomen: false*/
/*jslint browser: true*/
/*jslint continue: true*/
/*jslint devel: true*/
/*jslint forin: true*/
/*jslint maxlen: 110*/

/*
* Depends:
*  jquery.ui.core.js
*  jquery.ui.widget.js
*  jquery.wijmo.wijutil.js
*  jquery.wijmo.wijaccordion.js
*
*/
module wijmo.accordion {

	var $: JQueryStatic = jQuery,
		widgetName: string = "wijaccordion",
		commonClass: wijAccordionClass = {
			//Classes
			accordionClass: "wijmo-wijaccordion",
			accordionTopClass: "wijmo-wijaccordion-top",
			accordionBottomClass: "wijmo-wijaccordion-bottom",
			accordionLeftClass: "wijmo-wijaccordion-left",
			accordionRightClass: "wijmo-wijaccordion-right",
			headerClass: "wijmo-wijaccordion-header",
			contentClass: "wijmo-wijaccordion-content",
			contentActiveClass: "wijmo-wijaccordion-content-active",
			iconsClass: "wijmo-wijaccordion-icons",
			horizontalClass: "ui-helper-horizontal"
		};

	/** @ignore */
	interface wijAccordionClass {
		accordionClass: string;
		accordionTopClass: string;
		accordionBottomClass: string;
		accordionLeftClass: string;
		accordionRightClass: string;
		headerClass: string;
		contentClass: string;
		contentActiveClass: string;
		iconsClass: string;
		horizontalClass: string;
	}

	/** Define the layout setting properties. */
	interface layoutSetting {
		/** Gets or sets the top padding of layoutSetting.
		*/
		paddingTop?: string;
		/** Gets or sets the bottom padding of layoutSetting.
		*/
		paddingBottom?: string;
		/** Gets or sets the left padding of layoutSetting.
		*/
		paddingLeft?: string;
		/** Gets or sets the right padding of layoutSetting.
		*/
		paddingRight?: string;
		/** Gets or sets the width of layoutSetting.
		*/
		width?: number;
	}

	/** Define the animation setting properties. */
	interface animateOptions {
		/** Gets the wijaccordion pane to show.
		*/
		toShow?: JQuery;
		/** Gets the wijaccordion pane to hide.
		*/
		toHide?: JQuery;
		/** @ignore */
		complete?: (index?: number, elem?: Element) => any;
		/** Gets or sets whether animation is horizontal.
		*/
		horizontal?: boolean;
		/** Gets or sets the duration for animation.
		*/
		duration?: number;
		/** Gets or sets the easing for animation.
		* @remarks Valid values are all animations available in jQuery UI.
		*/
		easing?: string;
		/** Gets or sets whether accordion is animated from right to left.
		*/
		rightToLeft?: boolean;
		/** Gets whether animated downside.
		* @remarks It returns true if next index is greater than previous index
		*/
		down?: boolean;
		/** Gets or sets whether wijaccordion pane is autoHeight.
		* @remarks When set to true, width is autosized if horizontal and otherwise height is autosized.
		*/
		autoHeight?: boolean;
		/** Gets or sets the default layout setting.
		*/
		defaultLayoutSetting?: layoutSetting;
	}

	/** @widget */
	export class wijaccordion extends wijmoWidget {
		wijRightToLeft: string;
		_headerCornerOpened: string;
		_contentCornerOpened: string;
		_triangleIconOpened: string;
		_triangleIconClosed: string;
		_alignmentClass: string;
		private _defaultLayoutSetting: layoutSetting;

		_setOption(key: string, value: any): void {
			var o: WijAccordionOptions = this.options;
			if (o[key] !== value) {
				switch (key) {
					case "selectedIndex":
						this.activate(value);
						break;
					case "event":
						this._unbindLiveEvents();
						this.options.event = value;
						this._bindLiveEvents();
						break;
					case "header":
						this._handleHeaderChange(value, o.header);
						break;
					case "animated":
						break;
					case "expandDirection":
						this._onDirectionChange(value, true, o.expandDirection);
						break;
					default:
						break;
				}
			}
			super._setOption(key, value);
		}

		_handleHeaderChange(newHeaderSelector: string, prevHeaderSelector: string): void {
			var wijCSS: WijAccordionCSS = this.options.wijCSS,
				prevHeaders: JQuery = this.element.find(prevHeaderSelector),
				prevHeadersClass: string = [wijCSS.wijaccordionHeader, commonClass.headerClass, wijCSS.stateActive, this._triangleIconOpened].join(" "),
				prevHeadersContentClass: string = [wijCSS.wijaccordionContent, commonClass.contentClass, wijCSS.content, wijCSS.wijaccordionContentActive, commonClass.contentActiveClass].join(" ");

			prevHeaders.removeClass(prevHeadersClass);
			prevHeaders.siblings("." + commonClass.contentClass).removeClass(prevHeadersContentClass);

			this._initHeaders(newHeaderSelector);
		}

		_initHeaders(selector?: string): void {
			var o: WijAccordionOptions = this.options,
				headersSelector: string = selector ? selector : o.header,
				headers: JQuery = this.element.find(headersSelector);

			headers.each(jQuery.proxy(this._initHeader, this));
		}

		_initHeader(index: number, elem: Element): void {
			var o: WijAccordionOptions = this.options, wijCSS: WijAccordionCSS = o.wijCSS,
				rightToLeft: boolean = this.element.data(this.wijRightToLeft),
				header: JQuery = $(elem), content: JQuery = $(header.next()[0]),
				headerClass: string = [commonClass.headerClass, wijCSS.wijaccordionHeader].join(" "),
				selectedHeaderClass: string = [wijCSS.stateDefault, wijCSS.stateActive, this._headerCornerOpened].join(" "),
				selectedContentClass: string = [commonClass.contentActiveClass, wijCSS.wijaccordionContentActive, this._contentCornerOpened].join(" "),
				unselectedHeaderClass: string = [wijCSS.stateDefault, wijCSS.cornerAll].join(" "),
				contentClass: string = [commonClass.contentClass, wijCSS.wijaccordionContent, wijCSS.content].join(" "),
				origStyle: string;

			if (rightToLeft) {
				header.remove();
				header.insertAfter(content);
			}
			header.addClass(headerClass).attr("role", "tab");
			content.attr("role", "tabpanel");

			// During the animation, styles of the content will be changed.
			// Store the value for each content, it can be used for recover content style when destroy wijaccordion.
			origStyle = content.attr("style");
			if (origStyle) {
				content.data("origStyle", content.attr("style"));
			}

			if (header.find("> a").length === 0) {
				header.wrapInner('<a href="#"></a>');
			}

			if (header.find("> ." + wijmo.getCSSSelector(wijCSS.icon)).length === 0) {
				$('<span></span>').addClass(wijCSS.icon).insertBefore($("> a", header)[0]);
			}
			if (index === o.selectedIndex) {
				header.addClass(selectedHeaderClass).attribute({
					"aria-expanded": "true",
					tabIndex: 0
				});
				header.find("> ." + wijmo.getCSSSelector(wijCSS.icon)).addClass(this._triangleIconOpened);
				content.addClass(selectedContentClass).wijTriggerVisibility();
			} else {
				header.addClass(unselectedHeaderClass).attribute({
					"aria-expanded": "false",
					tabIndex: -1
				});
				header.find("> .ui-icon").addClass(this._triangleIconClosed);
				content.hide();
			}
			content.addClass(contentClass);
			//if use the header option and the header's parent wrapped an div element, when the 
			// accordion's expandDirection is left/top, the layout is wrong.
			while (true) {
				if (header.parent().hasClass(commonClass.accordionClass)) {
					break;
				}
				header.unwrap();
			}
		}

		_layout(): void {
			var self: wijaccordion = this, o: WijAccordionOptions = self.options,
				accordionClass: string = [commonClass.accordionClass, o.wijCSS.wijaccordion, o.wijCSS.widget, commonClass.iconsClass, o.wijCSS.wijaccordionIcons, o.wijCSS.helperClearFix].join(" ");
			self.element.addClass(accordionClass);

			if (self._isDisabled()) {
				self.element.addClass(o.wijCSS.stateDisabled);
			}
			self._onDirectionChange(o.expandDirection, false);
			self._initHeaders();
			if (self._getHeaders().length > 0) {
				self.element.attr("role", "tablist");
			}
			//super._create(this, arguments);
			$(window).on("resize." + self.widgetEventPrefix, function (e) {
				self._adjustAccordion();
			});
			if (o.expandDirection === 'left' || o.expandDirection === 'right') {
				self._adjustAccordion();
			}
			self._getDefaultLayoutSetting(o.expandDirection);
		}

		_initState(): void {
			this.wijRightToLeft = "rightToLeft";
		}

		_create(): void {
			// enable touch support:
			if (window.wijmoApplyWijTouchUtilEvents) {
				$ = window.wijmoApplyWijTouchUtilEvents($);
			}
			this._initState();
			this._layout();
			super._create();
		}

		_init(): void {
			this._bindLiveEvents();
		}

		_adjustAccordion(): void {
			var o: WijAccordionOptions = this.options, headers: JQuery, contentEle: JQuery,
				fWidth: number, paddingAndBorderWidth: number, headerWidth: number,
				adjustContentWidth: number;

			if (o.expandDirection === 'top' || o.expandDirection === 'bottom') {
				return;
			}
			headers = this._getHeaders();
			contentEle = $('.' + commonClass.contentClass, this.element);
			fWidth = contentEle.parent().width();
			paddingAndBorderWidth = parseInt(contentEle.css("paddingLeft"), 10) +
			parseInt(contentEle.css("paddingRight"), 10) +
			parseInt(contentEle.css("borderRightWidth"), 10) +
			parseInt(contentEle.css("borderLeftWidth"), 10),
			headerWidth = $('.' + commonClass.headerClass, this.element).outerWidth(true);
			adjustContentWidth = fWidth - headers.length * (headerWidth) - 2 - paddingAndBorderWidth;

			contentEle.width(adjustContentWidth);
		}

		_getDefaultLayoutSetting(expandDirection: string): void {
			var contentEle: JQuery = $('.' + commonClass.contentClass, this.element);

			if (expandDirection === 'top' || expandDirection === 'bottom') {
				this._defaultLayoutSetting = {
					paddingTop: contentEle.css("paddingTop"),
					paddingBottom: contentEle.css("paddingBottom")
				};
			} else {
				this._defaultLayoutSetting = {
					paddingLeft: contentEle.css("paddingLeft"),
					paddingRight: contentEle.css("paddingRight"),
					width: contentEle.width()
				};
			}
		}

		/**
		* Remove the functionality completely. This will return the element back to its pre-init state.
		*/
		destroy(): void {
			var o: WijAccordionOptions = this.options,
				rightToLeft: boolean = this.element.data("rightToLeft"),
				elementCSS: string,
				headerCSS: string,
				contentCSS: string,
				headers: JQuery,
				header: JQuery,
				content: JQuery,
				originalHeaderHTML: string,
				originalContentStyle: string = "";

			elementCSS = [o.wijCSS.wijaccordion, commonClass.accordionClass, o.wijCSS.widget, o.wijCSS.wijaccordionIcons, o.wijCSS.helperClearFix, commonClass.horizontalClass, this._alignmentClass, commonClass.iconsClass, o.wijCSS.stateDisabled].join(" ");

			headerCSS = [commonClass.headerClass, o.wijCSS.wijaccordionHeader, o.wijCSS.stateDefault, o.wijCSS.stateActive, o.wijCSS.cornerAll, this._headerCornerOpened].join(" ");

			contentCSS = [commonClass.contentActiveClass, o.wijCSS.wijaccordionContentActive, this._contentCornerOpened, commonClass.contentClass, o.wijCSS.wijaccordionContent, o.wijCSS.content].join(" ");

			this._unbindLiveEvents();

			headers = this._getHeaders();

			$.each(headers, function (index, ele) {
				header = $(ele);
				if (rightToLeft) {
					content = header.prev();
					content.insertAfter(header);
				} else {
					content = header.next();
				}

				header.removeClass(headerCSS).removeAttribute("aria-expanded").removeAttr("tabIndex").removeAttr("role");
				if (header.attr("class") === "") {
					header.removeAttr("class");
				}

				originalHeaderHTML = header.find("> a").html();
				header.html(originalHeaderHTML);

				content.show();

				originalContentStyle = content.data("origStyle");
				if (originalContentStyle) {
					content.attr("style", originalContentStyle);
				} else {
					content.removeAttr("style");
				}
				content.removeClass(contentCSS).removeAttr("role");
				if (content.attr("class") === "") {
					content.removeAttr("class");
				}
			});

			this.element.removeClass(elementCSS).removeAttr("role");
			if (this.element.attr("class") === "") {
				this.element.removeAttr("class");
			}

			$(window).off("resize.wijaccordion");

			super.destroy();
		}

		_getHeaders(): JQuery {
			var o: WijAccordionOptions = this.options,
				rightToLeft: boolean = this.element.data(this.wijRightToLeft),
				headersArr: Element[] = [], i: number,
				hdr: JQuery,
				headers: JQuery = this.element.find(o.header);

			if (headers.length > 0 && !$(headers[0]).hasClass(commonClass.headerClass) && $(headers[0]).hasClass(commonClass.contentClass)) {
				for (i = 0; i < headers.length; i += 1) {
					// fix for 29695:
					hdr = rightToLeft ? $(headers[i]).next("." + commonClass.headerClass) : $(headers[i]).prev("." + commonClass.headerClass);
					if (hdr.length > 0) {
						headersArr.push(hdr[0]);
					}
				}
			} else {
				return headers;
			}
			return $(headersArr);
		}

		/**
		* Refresh the accordion. 
		*/
		refresh(): void {
			this._adjustAccordion();
		}

		/**
		* Activates the accordion content pane at the specified index. 
		* @remarks
		* You can use code like in the example below inside your document ready function
		* to activate the specified pane using the click event of a button.
		* @param {number} index The zero-based index of the accordion pane to activate.
		*/
		activate(index: number): boolean;
		activate(index: string): boolean;
		activate(index: EventTarget): boolean;
		activate(index: any): boolean {
			var o: WijAccordionOptions = this.options,
				headers: JQuery = this._getHeaders(),
				nextHeader: JQuery,
				prevHeader: JQuery = $(jQuery.grep(headers.get(), function (a) {
					return $(a).hasAllClasses(o.wijCSS.stateActive);
				}));

			if (typeof index === "number") {
				nextHeader = $(headers[index]);
			} else if (typeof index === "string") {
				index = parseInt(index, 0);
				nextHeader = $(headers[index]);
			} else {
				nextHeader = $(index);
				index = headers.index(index);
			}
			if (nextHeader.hasAllClasses(o.wijCSS.stateDisabled)) {
				return false;
			}
			if (nextHeader.hasAllClasses(o.wijCSS.stateActive)) {
				if (o.requireOpenedPane) {
					// fix for
					// [17869] Unable to select the desire panel 
					// after all the panels are open in certain scenarios
					if (prevHeader.length === nextHeader.length && prevHeader.index() === nextHeader.index()) {
						return false;
					}
				} else {
					prevHeader = nextHeader;
					nextHeader = $(null);
				}
			} else if (!o.requireOpenedPane) {
				prevHeader = $(null);
			}
			if (prevHeader.length === 0 && nextHeader.length === 0) {
				return false;
			}

			return this._activateLayout(prevHeader, nextHeader);

		}
		_activateLayout(prevHeader: JQuery, nextHeader: JQuery): boolean {
			var o: WijAccordionOptions = this.options,
				rightToLeft: boolean = this.element.data(this.wijRightToLeft),
				nextContent: JQuery,
				prevContent: JQuery,
				activeHeaderClass: string = [o.wijCSS.stateActive, this._headerCornerOpened].join(" "),
				headerClass: string = [o.wijCSS.stateDefault, o.wijCSS.cornerAll].join(" ");

			nextContent = rightToLeft ? nextHeader.prev("." + commonClass.contentClass) :
			nextHeader.next("." + commonClass.contentClass);
			prevContent = rightToLeft ?
			prevHeader.prev("." + commonClass.contentClass) :
			prevHeader.next("." + commonClass.contentClass);
			prevHeader.removeClass(activeHeaderClass)
				.addClass(headerClass)
				.attribute({
					"aria-expanded": "false",
					tabIndex: -1
				})
				.find("> .ui-icon").removeClass(this._triangleIconOpened)
				.addClass(this._triangleIconClosed);
			nextHeader.removeClass("ui-corner-all")
				.addClass(activeHeaderClass)
				.attribute({
					"aria-expanded": "true",
					tabIndex: 0
				})
				.find("> .ui-icon").removeClass(this._triangleIconClosed)
				.addClass(this._triangleIconOpened);

			return this._activateAnimate(nextContent, prevContent, prevHeader, nextHeader);
		}
		_activateAnimate(nextContent: JQuery, prevContent: JQuery, prevHeader: JQuery, nextHeader: JQuery): boolean {
			var o: WijAccordionOptions = this.options,
				animOptions: animateOptions,
				headers: JQuery = this._getHeaders(),
				animations: animations = $.wijmo.wijaccordion.animations,
				activeContentClass: string = [o.wijCSS.wijaccordionContentActive, commonClass.contentActiveClass].join(" "),
				nextActiveContentClass: string = activeContentClass + " " + this._contentCornerOpened,
				adjustWidth: number,
				newIndex: number,
				prevIndex: number,
				proxied: any,
				proxiedDuration: any,
				duration: number,
				effect: string;

			newIndex = headers.index(nextHeader);
			prevIndex = headers.index(prevHeader);
			if (o.expandDirection === 'left' || o.expandDirection === 'right') {
				adjustWidth = this._defaultLayoutSetting.width;
			} else {
				adjustWidth = parseInt(nextContent.css("width"));
			}
			if (!this._trigger("beforeSelectedIndexChanged", null, { newIndex: newIndex, prevIndex: prevIndex })) {
				return false;
			}
			if (o.animated) {
				animOptions = {
					toShow: nextContent,
					toHide: prevContent,
					complete: jQuery.proxy(function () {
						prevContent.removeClass(activeContentClass);
						nextContent.addClass(activeContentClass).wijTriggerVisibility();

						prevContent.css('display', '');
						nextContent.css('display', '');
						//prevContent.wijTriggerVisibility();
						//nextContent.wijTriggerVisibility();
						this._adjustContentSize(nextContent, adjustWidth);
						this._trigger("selectedIndexChanged", null, { newIndex: newIndex, prevIndex: prevIndex });
					}, this),
					horizontal: this.element.hasClass(commonClass.horizontalClass),
					rightToLeft: this.element.data(this.wijRightToLeft),
					down: (newIndex > prevIndex),
					autoHeight: o.autoHeight || o.fillSpace,
					defaultLayoutSetting: this._defaultLayoutSetting
				};
				proxied = o.animated;
				proxiedDuration = o.duration;
				if ($.isFunction(proxied)) {
					o.animated = proxied(animOptions);
				}
				if ($.isFunction(proxiedDuration)) {
					o.duration = proxiedDuration(animOptions);
				}

				duration = o.duration;
				effect = o.animated;

				if (effect && !animations[effect] && !$.easing[effect]) {
					effect = 'slide';
				}

				if (!animations[effect]) {
					animations[effect] = function (options) {
						this.slide(options, {
							easing: effect,
							duration: duration || 700
						});
					};
				}
				animations[effect](animOptions);
			} else {
				if (prevHeader.length > 0) {
					prevContent.hide().removeClass(activeContentClass);
				}
				if (nextHeader.length > 0) {
					nextContent.show().addClass(nextActiveContentClass).wijTriggerVisibility();
				}
				//prevContent.wijTriggerVisibility();
				//nextContent.wijTriggerVisibility();
				this._adjustContentSize(nextContent, adjustWidth);
				this._trigger("selectedIndexChanged", null, { newIndex: newIndex, prevIndex: prevIndex });
			}
			this.options.selectedIndex = newIndex;
			return true;
		}

		_adjustContentSize(content: JQuery, adjustWidth: number) {
			var o = this.options;
			if (o.expandDirection === "left" || o.expandDirection === "right") {
				content.css("height", "");
				if (adjustWidth) {
					content.width(adjustWidth);
				}
			} else {
				content.css("width", "");
			}
		}

		/** Private methods */
		_bindLiveEvents(): void {
			var self: wijaccordion = this,
				o: WijAccordionOptions = this.options,
				headerSelector: string = "." + commonClass.headerClass,
				eventPrefix: string = self.widgetEventPrefix;

			this.element.on(o.event + "." + eventPrefix, headerSelector, jQuery.proxy(this._onHeaderClick, this))
				.on("keydown." + eventPrefix, headerSelector, jQuery.proxy(this._onHeaderKeyDown, this))
				.on("mouseenter." + eventPrefix, headerSelector, function () {
					$(this).addClass(o.wijCSS.stateHover);
				})
				.on("mouseleave." + eventPrefix, headerSelector, function () {
					$(this).removeClass(o.wijCSS.stateHover);
				})
				.on("focus." + eventPrefix, headerSelector, function () {
					$(this).addClass(o.wijCSS.stateFocus);
				})
				.on("blur." + eventPrefix, headerSelector, function () {
					$(this).removeClass(o.wijCSS.stateFocus);
				});
		}

		_unbindLiveEvents(): void {
			this.element.off("." + this.widgetEventPrefix, "." + commonClass.headerClass);
		}

		_onHeaderClick(e: JQueryEventObject): boolean {
			if (!this._isDisabled()) {
				this.activate(e.currentTarget);
			}
			return false;
		}

		_onHeaderKeyDown(e: JQueryEventObject): boolean {
			if (this._isDisabled()
				|| e.altKey || e.ctrlKey) {
				return false;
			}
			if (!$.ui) {
				return false;
			}
			var keyCode: JQueryUI.KeyCode,
				focusedHeader: JQuery = this.element.find("." + commonClass.headerClass + "." + this.options.wijCSS.stateFocus),
				focusedInd: number, headers: JQuery;
			if (focusedHeader.length < 0) {
				return false;
			}
			keyCode = wijmo.getKeyCodeEnum();
			headers = this._getHeaders();
			focusedInd = $("." + commonClass.headerClass, this.element).index(focusedHeader);
			switch (e.keyCode) {
				case keyCode.RIGHT:
				case keyCode.DOWN:
					if (headers[focusedInd + 1]) {
						headers[focusedInd + 1].focus();
						return false;
					}
					break;
				case keyCode.LEFT:
				case keyCode.UP:
					if (headers[focusedInd - 1]) {
						headers[focusedInd - 1].focus();
						return false;
					}
					break;
				case keyCode.SPACE:
				case keyCode.ENTER:
					this.activate(e.currentTarget);
					e.preventDefault();
					break;
			}
			return true;
		}

		_onDirectionChange(newDirection: string, allowDOMChange: boolean, prevDirection: string = null) {
			var rightToLeft: boolean, openedHeaders: JQuery, openedContents: JQuery, openedTriangles: JQuery,
				closedTriangles: JQuery, prevIsRightToLeft: boolean, o: WijAccordionOptions = this.options;

			if (allowDOMChange) {
				openedHeaders = this.element.find("." + commonClass.headerClass + "." + this._headerCornerOpened);
				openedHeaders.removeClass(this._headerCornerOpened);
				openedContents = this.element.find("." + commonClass.contentClass + "." + this._contentCornerOpened);
				openedContents.removeClass(this._contentCornerOpened);
				openedTriangles = this.element.find("." + this._triangleIconOpened);
				closedTriangles = this.element.find("." + this._triangleIconClosed);
				openedTriangles.removeClass(this._triangleIconOpened);
				closedTriangles.removeClass(this._triangleIconClosed);
			}
			if (prevDirection !== null) {
				this.element.removeClass(commonClass.accordionClass + "-" + prevDirection);
			}
			switch (newDirection) {
				case "top":
					this._headerCornerOpened = o.wijCSS.cornerBottom;
					this._contentCornerOpened = o.wijCSS.cornerTop;
					this._triangleIconOpened = o.wijCSS.iconArrowUp;
					this._triangleIconClosed = o.wijCSS.iconArrowRight;
					rightToLeft = true;
					this.element.removeClass(commonClass.horizontalClass);
					this._alignmentClass = [commonClass.accordionTopClass, o.wijCSS.wijaccordionTop].join(" ");
					break;
				case "right":
					this._headerCornerOpened = o.wijCSS.cornerLeft;
					this._contentCornerOpened = o.wijCSS.cornerRight;
					this._triangleIconOpened = o.wijCSS.iconArrowRight;
					this._triangleIconClosed = o.wijCSS.iconArrowDown;
					rightToLeft = false;
					this.element.addClass(commonClass.horizontalClass);
					this._alignmentClass = [commonClass.accordionRightClass, o.wijCSS.wijaccordionRight].join(" ");
					break;
				case "left":
					this._headerCornerOpened = o.wijCSS.cornerRight;
					this._contentCornerOpened = o.wijCSS.cornerLeft;
					this._triangleIconOpened = o.wijCSS.iconArrowLeft;
					this._triangleIconClosed = o.wijCSS.iconArrowDown;
					rightToLeft = true;
					this.element.addClass(commonClass.horizontalClass);
					this._alignmentClass = [commonClass.accordionLeftClass, o.wijCSS.wijaccordionLeft].join(" ");
					break;
				default: //bottom
					this._headerCornerOpened = o.wijCSS.cornerTop;
					this._contentCornerOpened = o.wijCSS.cornerBottom;
					this._triangleIconOpened = o.wijCSS.iconArrowDown;
					this._triangleIconClosed = o.wijCSS.iconArrowRight;
					rightToLeft = false;
					this.element.removeClass(commonClass.horizontalClass);
					this._alignmentClass = [commonClass.accordionBottomClass, o.wijCSS.wijaccordionBottom].join(" ");
					break;
			}
			this.element.addClass(this._alignmentClass);

			prevIsRightToLeft = this.element.data(this.wijRightToLeft);
			this.element.data(this.wijRightToLeft, rightToLeft);

			if (allowDOMChange) {
				openedTriangles.addClass(this._triangleIconOpened);
				closedTriangles.addClass(this._triangleIconClosed);
				openedHeaders.addClass(this._headerCornerOpened);
				openedContents.addClass(this._contentCornerOpened);
			}

			if (allowDOMChange && rightToLeft !== prevIsRightToLeft) {
				this.element.children("." + commonClass.headerClass).each(function () {
					var header: JQuery = $(this), content: JQuery;
					if (rightToLeft) {
						content = header.next("." + commonClass.contentClass);
						header.remove();
						header.insertAfter(content);
					} else {
						content = header.prev("." + commonClass.contentClass);
						header.remove();
						header.insertBefore(content);
					}
				});
			}
		}
	};

	/** wijaccordion options definition */
	interface WijAccordionOptions extends WidgetOptions {
		/**  @ignore */
		wijCSS?: WijAccordionCSS;
		/**
		* Sets the animation easing effect that users experience when they switch
		* between panes. 
		* @remarks
		* Set this option to false in order to disable easing. This results in a plain, abrupt shift 
		* from one pane to the next. You can also create custom easing animations using jQuery UI Easings
		* Options available for the animation function include:
		* down - If true, indicates that the index of the pane should be expanded higher than the index 
		*	of the pane that must be collapsed.
		* horizontal - If true, indicates that the accordion have a horizontal 
		*	orientation (when the expandDirection is left or right).
		* rightToLeft - If true, indicates that the content element is located 
		*	before the header element (top and left expand direction).
		* toShow - jQuery object that contains the content element(s) should be shown.
		* toHide - jQuery object that contains the content element(s) should be hidden.
		* @example
		* //Create your own animation:
		* jQuery.wijmo.wijaccordion.animations.custom1 = function (options) {
		*     this.slide(options, {
		*     easing: options.down ? "easeOutBounce" : "swing",
		*     duration: options.down ? 1000 : 200
		*   });
		* }
		*  $("#accordion3").wijaccordion({
		*      expandDirection: "right",
		*      animated: "custom1"
		*  });
		* @type {string|function}
		*/
		animated?: any;
		/**
		* The animation duration in milliseconds.
		* @remarks
		* @type {number|function}
		* By default, the animation duration value depends on an animation effect specified
		* by the animation option.
		*/
		duration?: any;
		/**
		* Determines the event that triggers the accordion to change panes.
		* @remarks
		* To select multiple events, separate them by a space. Supported events include:
		*	focus -- The pane opens when you click its header.
		*	click (default) -- The pane opens when you click its header.
		*	dblclick -- The pane opens when you double-click its header.
		*	mousedown -- The pane opens when you press the mouse button over its header.
		*	mouseup -- The pane opens when you release the mouse button over its header.
		*	mousemove -- The pane opens when you move the mouse pointer into its header.
		*	mouseover -- The pane opens when you hover the mouse pointer over its header.
		*	mouseout -- The pane opens when the mouse pointer leaves its header.
		*	mouseenter -- The pane opens when the mouse pointer enters its header.
		*	mouseleave -- The pane opens when the mouse pointer leaves its header.
		*	select -- The pane opens when you select its header by clicking and then pressing Enter
		*	submit -- The pane opens when you select its header by clicking and then pressing Enter.
		*	keydown -- The pane opens when you select its header by clicking and then pressing any key.
		*	keypress -- The pane opens when you select its header by clicking and then pressing any key.
		*	keyup -- The pane opens when you select its header by clicking and then pressing and releasing any key.
		*/
		event?: string;
		/**
		* Determines the direction in which the content area of the control expands.
		* @remarks
		* Available values include: top, right, bottom, and left.
		*/
		expandDirection?: string;
		/**
		* Determines the selector for the header element.
		* @remarks
		* Set this option to put header and content elements inside the HTML tags of your choice.
		* By default, the header is the first child after an <LI> element, and the content is 
		* the second child html markup.
		*/
		header?: string;
		/**
		* Determines whether clicking a header closes the current pane before opening the new one.
		* @remarks
		* Setting this value to false causes the headers to act as toggles for opening and
		* closing the panes, leaving all previously clicked panes open until you click them again.
		*/
		requireOpenedPane?: boolean;
		/**
		* Gets or sets the zero-based index of the accordion pane to show expanded initially.
		* @remarks
		* By default, the first pane is expanded. A setting of -1 specifies that no pane
		* is expanded initially, if you also set the requireOpenedPane option to false.
		*/
		selectedIndex?: number;
		/**
		* Occurs before an active accordion pane change.
		* @remarks
		* Return false or call event.preventDefault() in order to cancel event and
		* prevent the selectedIndex change.
		* @event
		* @dataKey {Number} newIndex Index of a pane that will be expanded.
		* @dataKey {Number} prevIndex Index of a pane that will be collapsed.
		*/
		beforeSelectedIndexChanged?: IWijAccordionEvent;
		/**
		* Occurs when an active accordion pane changed.
		* @event
		* @dataKey {Number} newIndex Index of the activated pane.
		* @dataKey {Number} prevIndex Index of the collapsed pane.
		*/
		selectedIndexChanged?: IWijAccordionEvent;
		/** Gets or sets whether wijaccordion pane is autoHeight.
		* @remarks When set to true, width is autosized if horizontal and otherwise height is autosized.
		*/
		autoHeight?: boolean;
		/** @ignore */
		fillSpace?: boolean;
	}

	/** @ignore */
	interface WijAccordionCSS extends WijmoCSS {
		wijaccordion?: string;
		wijaccordionTop?: string;
		wijaccordionBottom?: string;
		wijaccordionLeft?: string;
		wijaccordionRight?: string;
		wijaccordionHeader?: string;
		wijaccordionContent?: string;
		wijaccordionContentActive?: string;
		wijaccordionIcons?: string;
	}

	/** This event argument signature is used in beforeSelectedIndexChanged and selectedIndexChanged event.*/
	interface IWijAccordionEventArgs {
		/** Gets next wijaccordion pane index.
		*/
		newIndex: number;
		/** Gets previous wijaccordion pane index.
		*/
		prevIndex: number;
	}

	/** This event definition signature is used in beforeSelectedIndexChanged and selectedIndexChanged event.*/
	interface IWijAccordionEvent {
		(event: JQueryEventObject, ui: IWijAccordionEventArgs): void;
	}

	class wijaccordion_options implements WijAccordionOptions {
		/**  @ignore */
		wijCSS: WijAccordionCSS = {
			wijaccordion: "",
			wijaccordionTop: "",
			wijaccordionBottom: "",
			wijaccordionLeft: "",
			wijaccordionRight: "",
			wijaccordionHeader: "",
			wijaccordionContent: "",
			wijaccordionContentActive: "",
			wijaccordionIcons: ""
		};
		/**
		* All CSS classes used in widgets that use Mobile theme framework
		* @ignore
		*/
		wijMobileCSS = {
			header: "ui-header ui-bar-a",
			content: "ui-body ui-body-b"
		};
		/**
		* Selector option for auto self initialization. This option is internal.
		* @ignore
		*/
		initSelector: string = ":jqmData(role='wijaccordion')";
		/**
		* Sets the animation easing effect that users experience when they switch
		* between panes. 
		* @remarks
		* Set this option to false in order to disable easing. This results in a plain, abrupt shift 
		* from one pane to the next. You can also create custom easing animations using jQuery UI Easings
		* Options available for the animation function include:
		* down - If true, indicates that the index of the pane should be expanded higher than the index 
		*	of the pane that must be collapsed.
		* horizontal - If true, indicates that the accordion have a horizontal 
		*	orientation (when the expandDirection is left or right).
		* rightToLeft - If true, indicates that the content element is located 
		*	before the header element (top and left expand direction).
		* toShow - jQuery object that contains the content element(s) should be shown.
		* toHide - jQuery object that contains the content element(s) should be hidden.
		* @example
		* //Create your own animation:
		* jQuery.wijmo.wijaccordion.animations.custom1 = function (options) {
		*     this.slide(options, {
		*     easing: options.down ? "easeOutBounce" : "swing",
		*     duration: options.down ? 1000 : 200
		*   });
		* }
		*  $("#accordion3").wijaccordion({
		*      expandDirection: "right",
		*      animated: "custom1"
		*  });
		* @type {string|function}
		*/
		animated: any = 'slide';
		/**
		* The animation duration in milliseconds.
		* @remarks
		* @type {number|function}
		* By default, the animation duration value depends on an animation effect specified
		* by the animation option.
		*/
		duration: any = null;
		/**
		* Determines the event that triggers the accordion to change panes.
		* @remarks
		* To select multiple events, separate them by a space. Supported events include:
		*	focus -- The pane opens when you click its header.
		*	click (default) -- The pane opens when you click its header.
		*	dblclick -- The pane opens when you double-click its header.
		*	mousedown -- The pane opens when you press the mouse button over its header.
		*	mouseup -- The pane opens when you release the mouse button over its header.
		*	mousemove -- The pane opens when you move the mouse pointer into its header.
		*	mouseover -- The pane opens when you hover the mouse pointer over its header.
		*	mouseout -- The pane opens when the mouse pointer leaves its header.
		*	mouseenter -- The pane opens when the mouse pointer enters its header.
		*	mouseleave -- The pane opens when the mouse pointer leaves its header.
		*	select -- The pane opens when you select its header by clicking and then pressing Enter
		*	submit -- The pane opens when you select its header by clicking and then pressing Enter.
		*	keydown -- The pane opens when you select its header by clicking and then pressing any key.
		*	keypress -- The pane opens when you select its header by clicking and then pressing any key.
		*	keyup -- The pane opens when you select its header by clicking and then pressing and releasing any key.
		*/
		event: string = "click";
		/**
		* Determines the direction in which the content area of the control expands.
		* @remarks
		* Available values include: top, right, bottom, and left.
		*/
		expandDirection: string = "bottom";
		/**
		* Determines the selector for the header element.
		* @remarks
		* Set this option to put header and content elements inside the HTML tags of your choice.
		* By default, the header is the first child after an <LI> element, and the content is 
		* the second child html markup.
		*/
		header: string = "> li > :first-child,> :not(li):even";
		/**
		* Determines whether clicking a header closes the current pane before opening the new one.
		* @remarks
		* Setting this value to false causes the headers to act as toggles for opening and
		* closing the panes, leaving all previously clicked panes open until you click them again.
		*/
		requireOpenedPane: boolean = true;
		/**
		* Gets or sets the zero-based index of the accordion pane to show expanded initially.
		* @remarks
		* By default, the first pane is expanded. A setting of -1 specifies that no pane
		* is expanded initially, if you also set the requireOpenedPane option to false.
		*/
		selectedIndex: number = 0;
		/**
		* Occurs before an active accordion pane change.
		* @remarks
		* Return false or call event.preventDefault() in order to cancel event and
		* prevent the selectedIndex change.
		* @event
		* @dataKey {Number} newIndex Index of a pane that will be expanded.
		* @dataKey {Number} prevIndex Index of a pane that will be collapsed.
		*/
		beforeSelectedIndexChanged: IWijAccordionEvent = null;
		/**
		* Occurs when an active accordion pane changed.
		* @event
		* @dataKey {Number} newIndex Index of the activated pane.
		* @dataKey {Number} prevIndex Index of the collapsed pane.
		*/
		selectedIndexChanged: IWijAccordionEvent = null;
	}

	wijaccordion.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options, new wijaccordion_options());

	$.wijmo.registerWidget(widgetName, wijaccordion.prototype);

	/** @ignore */
	interface lengthUnit {
		value: number;
		unit: string;
	}

	/** @ignore */
	interface cssDescription {
		height?: number;
		width?: number;
		overflow?: string;
	}

	class animations {
		_parseWidth(width: any): lengthUnit {
			var parts: string[] = ('' + width).match(/^([\d+-.]+)(.*)$/);
			return {
				value: parts ? +parts[1] : 0,
				unit: parts ? (parts[2] || "px") : "px"
			};
		}

		slide(options: animateOptions, additions: any): void {
			var simpleShowOpts: any, simpleHideOpts: any,
				animations: animations = $.wijmo.wijaccordion.animations,
				overflow: string = options.toShow.css('overflow'),
				percentDone: number = 0,
				showProps: { [prop: string]: lengthUnit; } = {},
				hideProps: { [prop: string]: string; } = {},
				toShowCssProps: cssDescription,
				fxAttrs: string[] = options.horizontal ? ["width", "paddingLeft", "paddingRight"] : ["height", "paddingTop", "paddingBottom"],
				originalWidth: string,
				s: JQuery = options.toShow;

			options = $.extend({
				easing: "swing",
				duration: 300
			}, options, additions);
			if (options.horizontal) {
				simpleShowOpts = { width: "show" };
				simpleHideOpts = { width: "hide" };
			} else {
				simpleShowOpts = { height: "show" };
				simpleHideOpts = { height: "hide" };
			}
			if (!options.toHide.length) {
				options.toShow.stop(true, true).animate(simpleShowOpts, options);
				return;
			}
			if (!options.toShow.length) {
				options.toHide.stop(true, true).animate(simpleHideOpts, options);
				return;
			}
			// fix width/height before calculating height/width of hidden element
			if (options.horizontal) {
				originalWidth = s[0].style.height;
				s.height(parseInt(s.parent().height().toString(), 10) -
					parseInt(s.css("paddingTop"), 10) -
					parseInt(s.css("paddingBottom"), 10) -
					(parseInt(s.css("borderTopWidth"), 10) || 0) -
					(parseInt(s.css("borderBottomWidth"), 10) || 0));
			} else {
				originalWidth = s[0].style.width;
				s.width(parseInt(s.parent().width().toString(), 10) -
					parseInt(s.css("paddingLeft"), 10) -
					parseInt(s.css("paddingRight"), 10) -
					(parseInt(s.css("borderLeftWidth"), 10) || 0) -
					(parseInt(s.css("borderRightWidth"), 10) || 0));
			}

			$.each(fxAttrs, function (i: number, prop: string) {
				hideProps[prop] = "hide";

				if (!options.horizontal && prop === "height") {
					showProps[prop] = animations._parseWidth(options.toShow.css(prop));
				} else {
					showProps[prop] = animations._parseWidth(options.defaultLayoutSetting[prop]);
				}
			});
			if (options.horizontal) {
				toShowCssProps = { width: 0, overflow: "hidden" };
			} else {
				toShowCssProps = { height: 0, overflow: "hidden" };
			}
			options.toShow.css(toShowCssProps).stop(true, true).show();
			options.toHide.filter(":hidden").each(options.complete).end().filter(":visible").stop(true, true).animate(hideProps, {
				step: function (now: number, settings: any) {
					var val: number;
					if ($.inArray(settings.prop, fxAttrs)) {
						percentDone = (settings.end - settings.start === 0) ? 0 : (settings.now - settings.start) / (settings.end - settings.start);
					}

					val = (percentDone * showProps[settings.prop].value);
					if (val < 0) {
						//fix for 16943:
						val = 0;
					}
					options.toShow[0].style[settings.prop] = val + showProps[settings.prop].unit;
				},
				duration: options.duration,
				easing: options.easing,
				complete: function () {
					if (!options.autoHeight) {
						options.toShow.css(options.horizontal ? "width" : "height", "");
					}
					options.toShow.css(options.horizontal ? "height" : "width", originalWidth);
					$.each(["paddingLeft", "paddingRight", "paddingTop", "paddingBottom"], (i, key) => {
						options.toShow.css(key, "");
					});
					options.toShow.css({ overflow: overflow });
					options.complete();
				}
			});
		}

		bounceslide(options: animateOptions): void {
			this.slide(options, {
				easing: options.down ? "easeOutBounce" : "swing",
				duration: options.down ? 1000 : 200
			});
		}
	}

	$.extend($.wijmo.wijaccordion, {
		animations: animations.prototype
	});
}

/** @ignore */
interface JQuery {
	wijaccordion: JQueryWidgetFunction;
}