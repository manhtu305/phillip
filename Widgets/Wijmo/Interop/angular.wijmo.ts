/// <reference path="../External/declarations/jquery.d.ts"/>
/// <reference path="../External/declarations/jquery.ui.d.ts"/>
/// <reference path="../External/declarations/globalize.d.ts"/>
/// <reference path="../Base/wijmo.d.ts"/>
/// <reference path="../Data/src/dataView.ts"/>

/*
* Wijmo AngularJS Directive Factory.
*
* Depends:
*  angular.js
*
*/
declare var angular;

module wijmo.ng {
    // declare the Node variable explicitly because IE8 and older versions do not expose it.
    enum Node {
        ELEMENT_NODE = 1,
        ATTRIBUTE_NODE = 2,
        TEXT_NODE = 3,
        CDATA_SECTION_NODE = 4,
        ENTITY_REFERENCE_NODE = 5,
        ENTITY_NODE = 6,
        PROCESSING_INSTRUCTION_NODE = 7,
        COMMENT_NODE = 8,
        DOCUMENT_NODE = 9,
        DOCUMENT_TYPE_NODE = 10,
        DOCUMENT_FRAGMENT_NODE = 11,
        NOTATION_NODE = 12
    }

    /** Describes type structure. If an object, lists its properties. If an array, specifies its element type */
    interface TypeDef {
        /** One of the following: string, number, boolean, object, array, date */
        type?: string;
        /** If type is an array, describes the structure of its elements */
        elementType?: TypeDef;
        /** If type is an object, describes object properties in form of a hash */
        properties?: { [name: string]: PropertyDefinition; };
    }
    /** Describes type structure of a property. */
    interface PropertyDefinition extends TypeDef {
        /** Specifies singular name for the property that describes a collection of items. 
          * For example, singular for property "ranges" is "range".
          */
        singular?: string;
        /** Specifies one or more events that are triggered when the value of this property changes
          * @type string|Array[string]
          */
        changeEvent?: any;
        /** True if two-way binding is needed for the property. Otherwise, false.
          * @remarks
          * If binding is two-way, the property name must not be surrounded with curly brackets when specified in markup.
          * If binding is one-way, the property value is cloned on binding */
        twoWayBinding?: boolean;
    }

    /** Reads type structure from a value */
    function getTypeDefFromExample(value): TypeDef {
        var flagPropertyName = "__WIJMO_getTypeDefFromExample_EXECUTING__";

        /** Mark object is being processed */
        function markObject() {
            try {
                value[flagPropertyName] = true;
            } catch (ex) {
            }
        }

        /** Unmark object is being processed */
        function unmarkObject() {
            try {
                delete value[flagPropertyName]
            } catch (ex) {
            }
        }

        /** Read structure from the value */
        function getMetadata() {
            var meta: TypeDef = {
                type: angular.isArray(value) ? "array" : typeof value
            };
            switch (meta.type) {
                case "object":
                    meta.properties = {};

                    if (value) {
                        $.each(value, function (key, propValue) {
                            if (key !== flagPropertyName && key !== "constructor") {
                                meta.properties[key] = getTypeDefFromExample(propValue);
                            }
                        });
                    }
                    break;
                case "array":
                    meta.elementType = getTypeDefFromExample(value[0]);
                    break;
            }
            return meta;
        }

        // if null, marked, a window, a document or a function, skip it
        if (value == null || value[flagPropertyName] || value == window || value == document
            || $.isFunction(value)) {
            return {};
        }

        // mark before processing
        markObject();
        try {
            return getMetadata();
        } finally {
            // unmark after processing
            unmarkObject();
        }
    }

    /** Property paths utilities.
      * A property path is a sequence of property names separated by dots, 
      * for example, "firstName.length" is a length of firstName property value
      */
    module propPath {
        /** Split a property path to components */
        export function partition(path: string): string[] {
            if (typeof path !== "string") return <any>path;
            var parts = path.split(/[\.\[\]]/g);
            for (var i = parts.length - 1; i >= 0; i--) {
                if (!parts[i]) {
                    parts.splice(i, 1);
                }
            }
            return parts;
        }
        /** Read property path value.
          * @remarks 
          * Throws an exception if an intermediate value is null or undefined
          */
        export function get(obj, path) {
            var parts = partition(path);
            for (var i = 0; obj && i < parts.length; i++) {
                obj = obj[parts[i]];
            }
            return obj;
        }
        /** Set property path value 
          * @remarks 
          * Throws an exception if an intermediate value is null or undefined
          */
        export function set(obj, path, value) {
            var parts = partition(path);
            var last = parts.pop();
            obj = get(obj, parts);
            if (obj) {
                obj[last] = value;
            }
        }
    }

    /** AngularJs-specific utilities */
    module ngTools {
        /** Call a function on the next digest broadcast message of a scope. */
        export function onDigest(scope, fn) {
            var removeWatcher;
            var removeWatcher = scope.$watch("any", () => {
                if (removeWatcher) {
                    removeWatcher();
                    removeWatcher = null;
                }
                fn();
            });
        }
    }

    /** Describes a widget */
    interface WidgetMetadata {
        /** Name of a base widget */
        inherits?: string;
        /** A hash of widget events */
        events?: { [name: string]: any; };
        /** A hash of widget properties */
        properties?: { [name: string]: PropertyDefinition; };
    }

    /** A binding between a widget option and scope expression */
    interface Binding {
        /** Value type */
        typeDef?: TypeDef;
        /** Scope expression */
        expression?: string;
        /** Widget option path. The first component must be an option name */
        path: string;
        /** True if widget option is an event handler */
        isEvent?: boolean;
        /** Angular interpolation string, such as "Hello {{firstName}}!" */
        interpolationString?: string;
    }

    /** Do scope.$apply if it won't cause an exception */
    function safeApply(scope, data) {
        var phase = scope.$root.$$phase;
        if (phase !== '$apply' && phase !== '$digest') {
            scope.$apply(data);
        }
    }

    /** Do scope.$digest if it won't cause an exception */
    function safeDigest(scope) {
        var phase = scope.$root.$$phase;
        if (phase !== '$apply' && phase !== '$digest') {
            scope.$digest();
        }
    }

    /** Angular services used by Wijmo directives */
    interface Services {
        $compile: (element, transclude?, maxPriority?) => LinkFunction;
    }

    /** A pair of a scope expression and its latest value */
    class TextEvalEntry {
        value;
        constructor(public expr: string) { }
    }
    interface TextEvalEntryHash {
        [expression: string]: TextEvalEntry;
    }
    /** Evaluates an Angular interpolation string */
    class TextEval {
        /** Latest expression values */
        entries: TextEvalEntryHash = {};
        /** Rendered interpolation string */
        result: string;

        static exprRegex = /{{([^}]+)}}/g;

        /** @param text The interpolation string
          * @param scope The scope containing the source data
          * @param onChange The callback function to invoke when value changes
          */
        constructor(public text: string, public scope, public onChange: Function) {
            var match;
            // find all expressions
            while (match = TextEval.exprRegex.exec(text)) {
                var expr = match[1];

                // do not add multiple entries for the same expressions
                if (this.entries[expr]) {
                    continue;
                }

                // add an entry for the expression
                this.entries[expr] = new TextEvalEntry(expr);
            }

            // for each entry, set up a watch and call this.update on change
            angular.forEach(this.entries, e => {
                var assigned = false;
                scope.$watch(e.expr, value => {
                    assigned = true;
                    // skip if value didn't change
                    if (e.value == value) return;
                    e.value = value;
                    this.update();
                });

                // if the entry value wasn't assigned so far, do it now
                if (!assigned) {
                    e.value = scope.$eval(expr);
                }
            });

            // render the current value
            this.update();
        }

        update() {
            // for each Angular expression, replace it with its value
            var result = this.text.replace(TextEval.exprRegex, (m, name) => {
                var entry = this.entries[name];
                return entry && entry.value;
            });

            // skip if the result didn't change
            if (result == this.result) return;

            // update the result and invoke the callback
            this.result = result;
            if (this.onChange) {
                this.onChange(result);
            }
        }
    }

    /** Parse text to a specified type */
    function parsePrimitiveValue(text: string, type: string): any {
        // if the type is a number or boolean, then parse it.
        // if it is not an object or the node is not an element, return the text
        switch (type.toLowerCase()) {
            case "boolean":
                return text.toLowerCase() === "true";
            case "number":
                return parseFloat(text);
            case "date":
            case "datetime":
                return Globalize.parseDate(text);

            default:
                return null;
        }
    }

    /** A link function returned by ng.$compile service
      * http://docs.angularjs.org/api/ng.$compile
      */
    interface LinkFunction {
        (scope, cloneAttachFn?: (JQuery) => any): any;
    }
    /** Inner markup of widget declaration, not options, such as content of splitter panels */
    class Subelement {
        constructor(public element: JQuery, public link: LinkFunction) { }
    }

    /** Parsed widget declaration. Includes widget option values, data-binding and inner markup  */
    class Markup {
        id: string;
        style: string;
        clazz: string;

        /** A hash of options to be used during a widget construction */
        options: any = {};
        /** Parsed data-binding information */
        bindings: Binding[] = [];
        /** Inner markup, not part of options */
        subElements: Subelement[] = [];

        /** Parses options, data-binding and inner markup from a DOM node according to widget metadata
          * @param node The root of a DOM tree
          * @param meta Widget metadata, describing how to parse a DOM tree
          * @param innerMarkupSelector a CSS selector for non-option inner markup
          * @param services Angular services needed for parsing
          */
        constructor(node, meta: WidgetMetadata, public innerMarkupSelector: string, public services: Services) {
            var $node = $(node);

            // read id, class and style first
            this.id = $node.attr("id");
            this.clazz = $node.attr("class");
            this.style = $node.attr("style");

            // extract inner markup using innerMarkupSelector
            this.parseInnerMarkup($node);

            // parse the rest
            var typeDef: TypeDef = { type: "object", properties: meta.properties };
            this.parseOptionsAttribute($node, typeDef, meta.events);
            this.parseOptions($node, typeDef, meta.events);
        }

        /** move elements and id/class/style attributes from one element to another */
        moveContents(from: JQuery, to: JQuery) {
            /** Move an attribute by name */
            function moveAttr(name) {
                var value = from.attr(name);
                if (value) {
                    to.attr(name, value);
                    from.removeAttr(name);
                }
            }

            // move subelements
            from.children().each((_, child) => to.append(child));
            if (!to.children().length) {
                to.text(from.text());
            }

            // move id, style and class attributes
            moveAttr("id");
            moveAttr("style");
            moveAttr("class");
            return to;
        }

        /** If element matches the innerMarkupSelector, extract the contents and $compile */
        extractSubelements(element: JQuery) {
            if (!this.innerMarkupSelector) return;
            var e = $(element);
            if (e.is(this.innerMarkupSelector)) {
                var clone: JQuery = element.clone();
                var converted = this.moveContents(element, $("<div/>"));
                this.subElements.push(new Subelement(clone, this.services.$compile(converted)));
                e.empty();
            }
        }

        /** CSS selector to extract non-option inner markup */
        static allowedInnerHtmlSelector = "div, span, table, ul, ol, p, li, h1, h2, h3, h4, a, img, label, input";
        /** Extract non-option inner markup from the element, prepare and put to this.subelements */
        parseInnerMarkup(element: JQuery) {
            var addInnerNode = ($node) => {
                var clone = $node.clone()
                this.subElements.push(new Subelement(clone, this.services.$compile($node)));
                $node.remove();
            }
            // find html elements, compile them and put to this.subelements
            element.children(Markup.allowedInnerHtmlSelector).each((_, e) => addInnerNode($(e)));

            // do the same with contents of <inner-markup> subelement, if found.
            var container = element.children("inner-markup");
            if (container.length > 0) {
                container.children().each((_, e) => addInnerNode($(e)));
                container.remove();
            }
        }

        /** Render parsed subelements to the parentElement */
        apply(scope, parentElement) {
            angular.forEach(this.subElements, se => se.link(scope.$parent, (el) => parentElement.append(el)));
        }

        /** Create a (lowerCaseName => name) hash for the properties of an object */
        getNameMap(obj) {
            var map = {},
                key: string;
            for (key in obj) {
                map[key.toLowerCase()] = key;
            }
            return map;
        }
        isSpecialAttribute(name) {
            return name == "id" || name == "style" || name == "class";
        }

        /** Extract options and put to this.options */
        parseOptions($node, typeDef: TypeDef, events) {
            $.extend(true, this.options, this.parse($node, typeDef, events, ""));
        }
        /** Parse options from a DOM tree according to widget metadata */
        parse($node, typeDef: TypeDef, events, path: string) {

            // process a child node, that can be an attribute or an element
            var readNode = (node) => {
                var $node = $(node),
                    value: string; // node value

                // read the node value
                switch (node.nodeType) {
                    case Node.ATTRIBUTE_NODE:
                        value = node.value;
                        break;
                    case Node.ELEMENT_NODE:
                        value = $node.text();
                        break;
                    default:
                        return;
                }

                var name = node.nodeName || node.name;
                // HTML is case-insenstive, so node.name is often all capital case.
                name = name.toLowerCase();

                // Restore the original property name casing using name map
                if (map[name]) {
                    name = map[name];
                } else if (name.match(/-/)) {
                    // if name is written using dashes, such as border-width, then remove dashes and make next-to-dash characters capital,such as borderWidth
                    var parts = name.split("-");
                    name = parts.shift();
                    angular.forEach(parts, p => name += p.charAt(0).toUpperCase() + p.substring(1));
                }

                // skip if name is special
                if (this.isSpecialAttribute(name) && node.nodeType === Node.ATTRIBUTE_NODE) {
                    return;
                }

                var isArrayElement = node.nodeType === Node.ELEMENT_NODE && array;
                // path for the node
                var newPath = isArrayElement ? path + "[" + array.length + "]" : (path && path + ".") + name;

                // try to find metadata for the property
                var metadata = properties && properties[name];

                if (name == "ngDisabled" && !metadata) {
                    metadata = properties && properties["disabled"];
                    newPath = "disabled";
                }

                // if value contains an interpolation string and there are no subelements, process as a data-bound option
                if (!hasChildElements(node) && value && value.match(/{{/)) {
                    toRemove.push(node);
                    this.bindings.push({
                        typeDef: metadata,
                        path: newPath,
                        interpolationString: value
                    });

                    // if the interpolation string will become an array element, put a null in the meantime
                    if (isArrayElement) {
                        array.push("");
                    }
                    return;
                }

                // if node is an element and an array is expected, parse as an array element
                if (isArrayElement) {
                    // then push the sub-element
                    array.push(this.parse($node, typeDef && typeDef.elementType, events, newPath));
                    return;
                }

                // if node is an attribute, check if the value contains a scope expression
                // skip if starts with a digit
                if (value.match(/^[^\d]/) && node.nodeType === Node.ATTRIBUTE_NODE) {
                    // there are two cases where attribute value is an expression: a two-way bound option or an event handler option
                    var isTwoWayBindingOption = metadata && (metadata.changeEvent || metadata.twoWayBinding);
                    var isEvent = events && name in events;
                    if (isTwoWayBindingOption || isEvent) {
                        toRemove.push(node);
                        this.bindings.push({ path: newPath, expression: value, isEvent: isEvent });
                        return;
                    }
                }

                // in all other cases, parse the contents of a node as a composite option
                obj[name] = this.parse($node, metadata, events, newPath);
            }

            var node = $node[0],

                // read node text
                text = node.nodeType === Node.TEXT_NODE ? (<Text>node).wholeText : (<Attr>node).value,
                isArray = typeDef && typeDef.type === "array",
                properties = typeDef && typeDef.properties,

                // we need this lowercase name map because HTML IS NOT CASE-SENSITIVE! Chris said that.
                map = properties && this.getNameMap(properties) || {},

                // list of nodes to remove
                toRemove = [],

                // an object return value
                obj: Object,
                // an array return value
                array: any[];

            // extract non-option inner markup before processing
            if (node.nodeType === Node.ELEMENT_NODE) {
                this.extractSubelements($node);
            }

            // try parse the node value as primitive
            var primitiveValue = typeDef && typeDef.type ? parsePrimitiveValue(text, typeDef && typeDef.type) : null;
            if (primitiveValue !== null) {
                return primitiveValue;
            } else if (primitiveValue == null) {
                var primitiveTypeRequested = typeDef && typeDef.type && typeDef.type !== "object" && typeDef.type !== "array";
                if (primitiveTypeRequested || node.nodeType !== Node.ELEMENT_NODE) {
                    return text;
                }
            }

            // parse a DOM element to an object/array
            if (isArray) {
                array = [];
            } else {
                obj = {};
            }

            // read attributes
            angular.forEach(node.attributes, readNode);
            // read subelements
            angular.forEach(node.childNodes, readNode);

            // remove nodes marked for removal
            $.each(toRemove, function (_, node) {
                if (node.nodeType === Node.ATTRIBUTE_NODE) {
                    $(node.ownerElement).removeAttr(node.name);
                } else {
                    $(node).remove();
                }
            });

            return obj || array;
        }

        /** Given an options object and metadata, extract data-bindings and event handlers */
        extractBindingsAndEvents(root, typeDef: TypeDef, events, path: string) {
            if (!root) return;

            var properties = typeDef && typeDef.properties;

            $.each(root, (name, value) => {
                var newPath = (path && path + ".") + name;
                var metadata = properties && properties[name];

                if (typeof value === "object") {
                    // call recursively to process property values
                    this.extractBindingsAndEvents(value, metadata, events, newPath);
                } else if (typeof value == "string") {
                    var remove = false;
                    if (typeof value == "string" && value.match(/{{/)) {
                        this.bindings.push({
                            typeDef: metadata,
                            path: newPath,
                            interpolationString: value
                        });
                        remove = true;
                    } else if (value.match(/^[^\d]/)) {
                        var isTwoWayBindingOption = metadata && (metadata.changeEvent || metadata.twoWayBinding);
                        var isEvent = events && name in events;
                        if (isTwoWayBindingOption || isEvent) {
                            remove = true;
                            this.bindings.push({ path: newPath, expression: value, isEvent: isEvent });
                        }
                    }

                    if (remove) {
                        delete root[name];
                    }
                }
            });
        }

        /** Read the 'options' attribute and parse its contents */
        parseOptionsAttribute($node, typeDef, events) {
            var optionsString: string = $node.attr("options");
            $node.attr("options", "");
            if (!optionsString) return;

            var paddedExpression = "(function() { return " + optionsString + "})()";
            var options;
            try {
                options = eval(paddedExpression);
            } catch (ex) {
                throw new Error("JSON could not be parsed: " + optionsString + ".\nError: " + ex);
            }

            if (options) {
                this.extractBindingsAndEvents(options, typeDef, events, "");
            }

            $.extend(true, this.options, options);
        }
    }

    /** This module contains Angular directive classes for widgets. If a class for a widget is not found, the one for its base widget is used.
      * DirectiveBase is the base class for all widget directives. Only two classes derive from DirectiveBase: ElementDirective and AttributeDirective, 
      * depending on whether a directive is an element or attribute
      */
    module definitions {

        /** Base class for all Wijmo directives */
        export class DirectiveBase {
            wijMetadata: WidgetMetadata;
            fullWidgetName: string;
            eventPrefix: string;
            internalEventNamespace = "wijmo-angular";
            delayInstanceCreation = false;

            /** Merge metadata from different sources: the widget type, its base types and default values */
            static mergeMetadata(widgetName: string, options) {
                var fromOptions = { properties: getTypeDefFromExample(options).properties },
                    result = $.extend({}, fromOptions, widgetMetadata["base"]),
                    inheritanceStack = [],
                    parentName = widgetName;

                do {
                    inheritanceStack.unshift(parentName);
                    parentName = widgetMetadata[parentName] && widgetMetadata[parentName].inherits;
                } while (parentName);

                angular.forEach(inheritanceStack, (name) => $.extend(true, result, widgetMetadata[name]));
                return result;
            }

            constructor(public widgetName: string, public namespace: string, clazz: Function, public services: Services) {
                this.fullWidgetName = namespace + "-" + widgetName;
                this.wijMetadata = DirectiveBase.mergeMetadata(widgetName, clazz.prototype.options);
                this.eventPrefix = clazz.prototype.widgetEventPrefix || widgetName;
            }

            /** Creates a new object prototyped from the current one 
              * @remarks
              * This method is used for transition from one state to another. 
              * The possible states are: created -> compiled -> linked
              * More than one object may be prototyped from one object, for example, a directive may be linked many times
              * and it means that many linked directives must be created from a compiled one
              */
            transition(): DirectiveBase {
                if (Object.create) {
                    try {
                        return Object.create(this);
                    } catch (err) {
                    }
                }

                var Clazz = function () { }
                Clazz.prototype = this;
                return new Clazz();
            }

            // ----- compiled state -----

            /** Compiles the directive according to the tElem and tAttrs params
              * @returns a new object in the compiled state, prototyped from this one
              */
            compile(tElem, tAttrs, $compile) {
                return this.transition()._compile(tElem, tAttrs, $compile);
            }
            /** Perform compilation. Overridden in base classes */
            _compile(tElem, tAttrs, $compile) {
                return $.proxy(this.link, this);
            }

            // ----- linked state-----
            $scope: any;
            element: any;
            /** widget instance */
            widget: any;

            markup: Markup;

            /** Initialize the 'widget' field */
            _initWidget() {
                this.widget = this.element.data(this.widgetName) || this.element.data(this.fullWidgetName);
            }

            /** Transition to the linked state */
            link(scope, elem, attrs) {
                // create a new object
                var transitioned = this.transition();
                // init the new object
                transitioned.$scope = scope;
                transitioned.element = elem;
                // link the new object
                transitioned._link(attrs);
            }
            /** Perform linkage. Overridden in base classes */
            _link(attrs) {
                ngTools.onDigest(this.$scope.$parent, () => {
                    this._createInstance(attrs);
                    this._initWidget();
                });
            }

            /** Create a widget. To be overridden in derived classes */
            _createInstance(attrs) { }

            //#region Events
            getEventFullName(name: string) {
                return this.eventPrefix + name.toLowerCase() + "." + this.internalEventNamespace;
            }
            eventHandlerWrappers: any = {};
            unbindFromWidget(name: string, handler: Function) {
                name = this.getEventFullName(name);
                this.element.unbind(name, this.eventHandlerWrappers[name] || handler);
            }
            bindToWidget(name: string, handler: Function) {
                name = this.getEventFullName(name);
                var scope = this.$scope;
                this.eventHandlerWrappers[name] = function () {
                    // Must return the result, e.g. "beforeXxx" event handler will return false to stop action.
                    var result = handler.apply(this, arguments);
                    safeDigest(scope.$parent);
                    return result;
                };
                this.element.bind(name, this.eventHandlerWrappers[name]);
            }
            //#endregion

            /** Listen to changes of the "data" option. */
            watchData(binding: Binding, handler: (value: any) => void) {
                var scope = this.$scope.$parent;
                var lastData: any[];
                var lastLength;

                /** Returns true if data didn't change. Otherwise, false */
                function sameData(data) {
                    // if length changed, return false
                    if (lastData && lastData.length != lastLength) {
                        return false;
                    }

                    // if value is the same object, nothing changed
                    if (lastData === data) {
                        return true;
                    }

                    // if lengths of the new and old arrays are differnt, return false
                    if (lastData == null || data == null || data.length != lastData.length) {
                        return false;
                    }

                    // if arrays have different elements, return false
                    for (var i = 0; i < data.length; i++) {
                        if (data[i] !== lastData[i]) {
                            return false;
                        }
                    }

                    // in all other cases, nothing changed
                    return true;
                }

                // listen to data and invoke the callback when changed
                scope.$watch(function () {
                    var data = scope.$eval(binding.expression);
                    if (!sameData(data)) {
                        lastData = JSON.parse(JSON.stringify(data));
                        lastLength = data.length;
                        handler(JSON.parse(JSON.stringify(data)));
                    }
                });
            }
            /** Listen to changes of an option */
            watchBinding(binding: Binding, handler: (value: any) => void) {
                if (binding.path === "data") {
                    // listen to "data" in a special way
                    this.watchData(binding, handler);
                } else {
                    this.$scope.$parent.$watch(binding.expression, handler, true);
                }
            }

            /** Process data-bindings */
            wireData(creationOptions) {
                var dataScope = this.$scope.$parent,
                    /** A set of options being applied at the moment */
                    applyingOptions = {},
                    /** A collection of events and data-bindings to be updated when an event is fired */
                    changeEvents: { [index: string]: Binding[]; } = {},
                    bindEvents = {};

                // for each data-binding, listen to changes in the view model and update options
                $.each(this.markup.bindings, (_, binding: Binding) => {
                    if (binding.interpolationString) {
                        var textEval = new TextEval(binding.interpolationString, dataScope, text => {
                            var value = text;
                            if (binding.typeDef && binding.typeDef.type) {
                                value = parsePrimitiveValue(value, binding.typeDef.type);
                                if (value === null) {
                                    value = text;
                                }
                            }

                            // set the option value
                            if (this.widget) {
                                // if widget is created, set the new value to the instance
                                this.setOption(binding.path, value);
                            } else {
                                // otherwise, set it to the options object
                                propPath.set(creationOptions, binding.path, value);
                            }
                        });
                        return;
                    }
                    //collect the event bindings
                    if (this.delayInstanceCreation && binding.isEvent) {
                        bindEvents[binding.path] = dataScope.$eval(binding.expression);
                    }
                    // listen to changes in the view model
                    var checkPrev = false;
                    this.watchBinding(binding, value => {
                        if (binding.isEvent) {
                            // if option is an event, then unbind/bind the old/new handler
                            if (prevValue) {
                                // clear the option value before calling $.fn.bind. Otherwise, a handler is called two times
                                // [86672] clear that option anyway in case that we wrap user's event handler 
                                this.widget.option(binding.path, null);
                                this.unbindFromWidget(binding.path, prevValue);
                            }
                            if (value) {
                                this.bindToWidget(binding.path, value);
                            }
                        } else { // if not an event
                            // skip if the option is being applied at the moment or value didn't change
                            if (applyingOptions[binding.path] && this.widget.option(binding.path) === value) {
                                return;
                            }

                            if (!checkPrev || value !== prevValue) {
                                this.setOption(binding.path, value);
                            }
                        }
                        checkPrev = false;
                        prevValue = value;
                    });
                    var prevValue = dataScope.$eval(binding.expression);
                    if (prevValue !== undefined || !binding.isEvent) {
                        checkPrev = true;
                        propPath.set(creationOptions, binding.path, prevValue);
                    }

                    // listen to changes in the widget options
                    if (binding.isEvent) return;
                    var meta = propPath.get(this.wijMetadata.properties, binding.path);

                    // get the change event list
                    var changeEventList = meta && meta.changeEvent;
                    if (!changeEventList) {
                        changeEventList = binding.path + "Changed";
                    }
                    if (typeof changeEventList === "string") {
                        changeEventList = [changeEventList];
                    }

                    // for each event, associate the binding to the event
                    $.each(changeEventList, (_, changeEvent: string) => {
                        changeEvents[changeEvent] = changeEvents[changeEvent] || [];
                        changeEvents[changeEvent].push(binding);
                    });
                });

                // for all events, listen to them and update associated bindings
                $.each(changeEvents, (changeEvent: string, bindings: Binding[]) => {
                    // when the change event has binded in markup, bind the funtion to widget element
                    // before creating widget, for safe: can add condition "this.delayInstanceCreation"
                    if (this.delayInstanceCreation && bindEvents[changeEvent]) {
                        this.unbindFromWidget(changeEvent, bindEvents[changeEvent]);
                        this.bindToWidget(changeEvent, bindEvents[changeEvent]);
                    }

                    creationOptions[changeEvent] = () => {
                        // for each binding, read an option value and update the scope
                        $.each(bindings, (_, binding: Binding) => {
                            if (!this.widget) {
                                // the event is raised during widget creation
                                return;
                            }
                            applyingOptions[binding.path] = true;
                            try {
                                propPath.set(dataScope, binding.expression, this.widget.option(binding.path));
                                safeApply(dataScope, binding.expression);
                            } finally {
                                applyingOptions[binding.path] = false;
                            }
                        });
                    };
                });
            }

            /** Set option value
              * @param path an option name, or a path that starts with an option name
              */
            setOption(path: string, value) {
                var parts = propPath.partition(path);
                if (parts.length == 1) {
                    this.widget.option(path, value);
                } else {
                    var optionName = parts.shift();
                    var optionValue = $.extend(true, {}, this.widget.option(optionName));
                    propPath.set(optionValue, parts, value);
                    this.widget.option(optionName, optionValue);
                }
            }
        }

        /** A base class for attribute directives */
        export class AttributeDirective extends DirectiveBase {

            scope = {}; // create isolate scope for attribute directives

            constructor(widgetName: string, namespace: string, clazz: Function, services: Services) {
                super(widgetName, namespace, clazz, services);
            }

            // ----- linked state-----

            _createInstance(attrs) {
                var create = () => {
                    var options = $.extend(true, {}, this.markup.options);
                    this.wireData(options);
                    this.element[this.widgetName](options);
                    this._initWidget();
                };

                if (this.delayInstanceCreation) {
                    setTimeout(create, 0);
                } else {
                    create();
                }
            }

            /** Parse an element tree to options, data-binding entries and inner markup subelements */
            parseMarkup(elem) {
                return new Markup(elem[0], this.wijMetadata, null, this.services);
            }

            // ---- compiled state-----
            _compile(tElem, tAttrs, $compile) {
                // parse markup during compilation
                this.markup = this.parseMarkup(tElem);
                return super._compile(tElem, tAttrs, $compile)
            }
        }

        /** A base class for element directives */
        export class ElementDirective extends DirectiveBase {
            /** A template for an element that will replace a custom element. 
              * A widget instance is created on top of the templated element. 
              * The majority of widgets can be created on top of a div.
              */
            expectedTemplate = "<div/>";
            restrict = 'E'; // require mapping to a DOM element
            scope = {};

            constructor(widgetName: string, namespace: string, clazz: Function, services: Services) {
                super(widgetName, namespace, clazz, services);
            }

            innerMarkupSelector: string = null;
            /** Creates Markup object. The method may be overridden in a derived class.*/
            createMarkup(elem, meta: WidgetMetadata) {
                return new Markup(elem[0], meta, this.innerMarkupSelector, this.services);
            }
            /** Parse an element tree to options, data-binding entries and inner markup subelements */
            parseMarkup(elem) {
                var markup = this.createMarkup(elem, this.wijMetadata);
                markup.options.dataSource = [];
                return markup;
            }

            // ---- compiled state-----

            _compile(tElem, tAttrs, $compile) {
                // parse markup during compilation
                this.markup = this.parseMarkup(tElem);
                return super._compile(tElem, tAttrs, $compile)
            }

            // ----- linked state -----

            _link(attrs) {
                // replace the given element with a new templated element
                this.element = $(this.expectedTemplate).replaceAll(this.element);
                // insert subelements found in the directive declaration
                this.markup.apply(this.$scope, this.element);
                // process with linkage
                super._link(attrs);
            }
            createInstanceCore(options) {
                this.element[this.widgetName](options);
            }
            _createInstance(attrs) {
                // clone options before creating a widget
                var options = $.extend(true, {}, this.markup.options),
                    create = () => {
                        // process data-bindings: update options and set up watchers
                        this.wireData(options);
                        // create a widget instance
                        this.createInstanceCore(options);
                        // update the 'widget' field
                        this._initWidget();
                    };

                // assign id, style and class to the new element
                this.element.attr({
                    style: this.markup.style,
                    id: this.markup.id,
                    "class": this.markup.clazz
                });

                if (this.delayInstanceCreation) {
                    setTimeout(create, 0);
                } else {
                    create();
                }
            }
        }

        interface GridCellTemplate {
            view?: Subelement;
            edit?: Subelement;
            dataKey?: String;
        }
        /** Grid-specific Markup, contains cell templates */
        class GridMarkup extends Markup {
            cellTemplates: GridCellTemplate[];

            extractCellTemplate($col, name) {
                var templateContainer = $col.children(name);
                if (templateContainer.length === 0) return null;
                var template = templateContainer.children().clone();
                if (template.length === 0) return null;
                templateContainer.remove();
                return {
                    element: template,
                    link: this.services.$compile(template)
                };
            }

            extractCellTemplates(node) {
                this.cellTemplates = this.cellTemplates || [];


                $(node).children("columns").children().each((index: number, col) => {
                    var $col = $(col);
                    var cellTemplate = this.extractCellTemplate($col, "cell-template");
                    var editorTemplate = this.extractCellTemplate($col, "editor-template");
                    if (cellTemplate || editorTemplate) {
                        this.cellTemplates[index] = <GridCellTemplate> {
                            view: cellTemplate,
                            edit: editorTemplate,
                            dataKey: $col.data("key")
                        };
                    }
                });

            }

            // override parseOptions to extract cell templates
            parseOptions($node, typeDef, events) {
                this.extractCellTemplates($node);
                super.parseOptions($node, typeDef, events);
                this.options.data = [];
            }
        }

        /** wijgrid directive extends ElementDirective by adding cell templates */
        export class wijgrid extends ElementDirective {
            expectedTemplate = "<table/>";
            markup: GridMarkup;

            createMarkup(elem, typeDef) {
                return new GridMarkup(elem[0], typeDef, this.innerMarkupSelector, this.services);
            }

            dataOptionExression() {
                var expr = null;
                $.each(this.markup.bindings, (_, b: Binding) => {
                    if (b.path === "data") {
                        expr = b.expression;
                        return false;
                    }
                });
                return expr;
            }

            applyCellTemplates(scope, options) {
                function applyCellTemplate(index, container, template: Subelement) {
                    if (index < 0) return false;

                    var items = scope.$parent.$eval(dataExpr);
                    if (!items) return false;

                    var rowScope = scope.$new();

                    rowScope.rowData = (wijmo && wijmo.data && wijmo.data.isDataView(items))
                    ? (<wijmo.data.IDataView>items).item(index) // dataView
                    : items[index]; // array

                    template.link(rowScope, el => {
                        container.empty().append(el);
                    });
                    container.children().data(ngKey, true);

                    safeDigest(rowScope);

                    return true;
                }
                var columns = options.columns,
                    dataExpr = this.dataOptionExression(),
                    ngKey = "wijmoNg";
                if (!dataExpr) return;

                var hasEditTemplates = false;
                $.each(this.markup.cellTemplates, (index: number, template: GridCellTemplate) => {
                    if (!template) return;
                    var column = (columns[index] = columns[index] || {});

                    if (template.view) {
                        var origFormatter = column.cellFormatter;
                        column.cellFormatter = args => {
                            var items = scope.$parent.$eval(dataExpr),
                                originalIndex = args.row.dataItemIndex;

                            if (args.row.data && $.isArray(items)) {
                                originalIndex = items.indexOf(args.row.data);
                            }

                            return $.isFunction(origFormatter) && origFormatter(args) || applyCellTemplate(originalIndex, args.$container, template.view);
                        }
                    }

                    if (template.edit) {
                        hasEditTemplates = true;
                    }
                });

                if (hasEditTemplates) {
                    var origBeforeCellEdit = options.beforeCellEdit;
                    var origAfterCellEdit = options.afterCellEdit;
                    var origBeforeCellUpdate = options.beforeCellUpdate;

                    options.beforeCellEdit = (e, args) => {
                        if ($.isFunction(origBeforeCellEdit)) {
                            origBeforeCellEdit(e, args);
                            if (args.handled) return;
                        }

                        var col = args.cell.column();
                        if (!col || col.dataIndex < 0 || col.dataIndex >= this.markup.cellTemplates.length) return;

                        var row = args.cell.row();
                        if (!row || row.dataItemIndex < 0) return;

                        var container: JQuery = args.cell.container();
                        if (!container || container.length == 0) return;
                        var template;
                        $.each(this.markup.cellTemplates, (index: number, editorTemplate: GridCellTemplate) => {
                            if (editorTemplate && editorTemplate["dataKey"] === col["dataKey"]) {
                                template = editorTemplate;
                                return;
                            }
                        });
                        if (!template) return;

                        if (applyCellTemplate(row.dataItemIndex, container, template.edit)) {
                            args.handled = true;
                        }
                    };
                    options.afterCellEdit = (e, args) => {
                        if ($.isFunction(origAfterCellEdit)) {
                            origAfterCellEdit(e, args);
                            if (args.handled) return;
                        }

                        var container: JQuery = args.cell.container();
                        if (container && container.children().data(ngKey)) {
                            container.empty();
                        }
                    };
                    options.beforeCellUpdate = (e, args) => {
                        if ($.isFunction(origBeforeCellUpdate)) {
                            origBeforeCellUpdate(e, args);
                        } else {
                            var col = args.cell.column();

                            if (col && col.dataIndex && this.markup.cellTemplates[col.dataIndex]) {
                                // We assume that template control has already refreshed the model, and return current value.
                                // Otherwise, wijgrid will return null if the extraction of new DOM value was unsuccessful.
                                args.value = args.cell.value();
                            }
                        }
                    };
                }
            }

            createInstanceCore(options) {
                this.applyCellTemplates(this.$scope, options);
                super.createInstanceCore(options);
            }
        }

        export class wijgridfilter extends wijgrid { }

        export class wijcombobox extends ElementDirective {
            expectedTemplate = "<input/>";
        }
        export class wijupload extends ElementDirective {
            expectedTemplate = "<input type='file' />";
        }
        export class wijinputcore extends ElementDirective {
            expectedTemplate = "<input/>";
        }
        export class wijinputdate extends wijinputcore { }
        export class wijinputmask extends wijinputcore { }
        export class wijinputnumber extends wijinputcore { }
        export class wijinputtext extends wijinputcore {
            _link(attrs) {
                if (attrs.maxLineCount > 1) {
                    this.expectedTemplate = "<textarea></textarea>";
                }
                super._link(attrs);
            }
        }

        export class wijcheckbox extends AttributeDirective {
            expectedTemplate = "<input type='checkbox'/>";
            delayInstanceCreation = true;
        }
        export class wijradio extends wijcheckbox {
        }

        export class wijsplitter extends ElementDirective {
            innerMarkupSelector = "panel1, panel2";
        }

        export class wijexpander extends ElementDirective {
            innerMarkupSelector = "h1, div";
        }

        export class wijmenu extends ElementDirective {
            expectedTemplate = "<ul/>";
            delayInstanceCreation = true;
        }

        export class wijtree extends ElementDirective {
            expectedTemplate = "<ul/>";
            delayInstanceCreation = true;
        }

        export class wijchartnavigator extends ElementDirective {
            delayInstanceCreation = true;
        }

        export class wijeditor extends ElementDirective {
            expectedTemplate = "<textarea/>";
        }

        class TabsMarkup extends Markup {
            constructor(node, typeDef: TypeDef, public services: Services) {
                super(node, typeDef, "tab", services);
            }

            apply(scope, parentElement) {
                super.apply(scope, parentElement);

                var ul = parentElement.children("ul").first();
                if (ul.length == 0) {
                    ul = $("<ul/>");
                    ul.prependTo(parentElement);
                }

                angular.forEach(this.subElements, (se) => {
                    if (!se.element.is("tab")) return;
                    var id = se.element.attr("id"),
                        anchor = $("<a/>").text(se.element.attr("title"));
                    if (id) {
                        anchor.attr("href", "#" + id);
                    }
                    $("<li/>").append(anchor).appendTo(ul);
                });
            }
        }

        export class wijtabs extends ElementDirective {
            createMarkup(element, typeDef) {
                return new TabsMarkup(element, typeDef, this.services);
            }
        }

        export class wijspread extends ElementDirective {
            setOption(path: string, value) {
                var sheetMatch;
                if (path === "dataSource") {
                    this.widget.spread().sheets[0].setDataSource(value);
                    return;
                }

                sheetMatch = path.match(/sheets\[(\d+)\]\.data/);
                if (sheetMatch) {
                    var sheetIndex = parseInt(sheetMatch[1], 10);
                    this.widget.spread().sheets[sheetIndex].setDataSource(value);
                    return;
                }

                super.setOption(path, value);
            }
            watchBinding(binding: Binding, handler: (any) => any) {
                if (binding.path.match(/sheets\[\d+\]\.data/)) {
                    this.watchData(binding, handler);
                } else {
                    super.watchBinding(binding, handler);
                }
            }
        }

        interface DirectiveClass {
            new (widgetName: string, namespace: string, clazz: Function, services: Services): DirectiveBase;
        }

        /** Find a directive class in this module for a given widget name */
        export function getDirectiveClass(widgetName: string): DirectiveClass {
            var find = name => {
                var metadata: WidgetMetadata = widgetMetadata[name],
                    parentMetadata;

                // return a class with the same name as widget, or the one for the base widget
                return <DirectiveClass>definitions[name] ||
                    metadata && metadata.inherits && getDirectiveClass(metadata.inherits);
            };

            var result = find(widgetName);
            if (result) {
                return result;
            }

            var meta = widgetMetadata[widgetName],
                isDecorator = meta && meta.isDecorator;
            return isDecorator ? <DirectiveClass> AttributeDirective : ElementDirective;
        }
    }

    // define the wijmo module
    var wijModule = angular["module"]('wijmo', []);

    /** Registers a widget directive */
    function registerDirective(widgetName: string, namespace: string, clazz, directiveName?: string) {
        var meta = widgetMetadata[widgetName],
            directiveClass = definitions.getDirectiveClass(widgetName);

        directiveName = directiveName || widgetName.toLowerCase();

        // call document.createElement, so IE knows that the directive name with dashes is a valid name
        createElementsForSubelements(directiveName, meta);

        // register the directive
        wijModule.directive(directiveName, [<any> "$compile", function ($compile) {
            var services: Services = { $compile: $compile };
            return new directiveClass(widgetName, namespace, clazz, services);
        }]);
    }

    /** Call document.createElement for all possible custom elements/subelements for a directive.
      * @remarks
      * Angular custom elements need it to work in old IE
      */
    function createElementsForSubelements(name: string, meta: WidgetMetadata) {
        function registerName(name: string) {
            document.createElement(name);
            var propNameWithDashes = insertDashes(name);
            if (propNameWithDashes != name) {
                document.createElement(propNameWithDashes);
            }
        }

        // recursive into properties
        function create(properties) {
            if (!properties) return;
            for (var propName in properties) {
                registerName(propName);

                var prop = properties[propName] && properties[propName];
                if (prop) {
                    if (prop.singular) {
                        registerName(prop.singular);
                    }
                    create(prop.properties);

                    if (prop.elementType) {
                        create(prop.elementType.properties);
                    }
                }
            }
        }

        registerName(name);
        if (meta) {
            create(meta.properties);
        }
    }

    /** Converts camelCase into with-dashes */
    function insertDashes(camelCase: string) {
        var result = "",
            i: number,
            c: string,
            isCapital; // char

        for (var i = 0; i < camelCase.length; i++) {
            c = camelCase.charAt(i);
            isCapital = c.match(/[A-Z]/);
            if (isCapital) {
                result += "-" + c.toLowerCase();
            } else {
                result += c;
            }
        }

        return result;
    }

    var chartAxisProperties = {
        type: "object",
        properties: {
            visible: { type: "boolean" },
            textVisible: { type: "boolean" },
            textStyle: {},
            labels: {
                type: "object",
                properties: {
                    width: { type: "number" },
                    textAlign: { type: "string" },
                }
            },
            autoMin: { type: "boolean" },
            autoMax: { type: "boolean" },
            autoMajor: { type: "boolean" },
            autoMinor: { type: "boolean" },
            unitMajor: { type: "boolean" },
            unitMinor: { type: "boolean" },
            gridMajor: {
                type: "object",
                properties: {
                    visible: { type: "boolean" },
                    style: {}
                }
            },
            gridMinor: {
                type: "object",
                properties: {
                    visible: { type: "boolean" },
                    style: {}
                }
            },
            tickMajor: {},
            tickMinor: {},
            annoMethod: { type: "string" },
            annoFormatString: { type: "string" },
            valueLabels: { type: "array", twoWayBinding: true }
        }
    };

    var widgetMetadata = {
        "base": {
            events: {
                "create": {},
                "change": {}
            }
        },
        "wijtooltip": {
            isDecorator: true,
            "events": {
                "showing": {},
                "shown": {},
                "hiding": {},
                "hidden": {}
            },
            "properties": {
                "group": {},
                "ajaxCallback": {}
            }
        },
        "wijslider": {
            "events": {
                "buttonMouseOver": {},
                "buttonMouseOut": {},
                "buttonMouseDown": {},
                "buttonMouseUp": {},
                "buttonClick": {},
                "slide": {},
                "start": {},
                "stop": {}
            },
            "properties": {
                "value": { changeEvent: "change" },
                "values": { changeEvent: "change" }
            }
        },
        "wijsplitter": {
            "events": {
                "sized": {},
                "load": {},
                "sizing": {},
                "expand": {},
                "collapse": {},
                "expanded": {},
                "collapsed": {}
            },
            "properties": {
                splitterDistance: {
                    type: "number",
                    changeEvent: "sized"
                }
            }
        },
        "wijflipcard": {
            "events": {
                "flipping": {},
                "flipped": {}
            },
            "properties": {
                "disabled": {},
                "width": {},
                "height": {},
                "triggerEvent": {}
            }
        },
        "wijprogressbar": {
            "events": {
                "progressChanging": {},
                "beforeProgressChanging": {},
                "progressChanged": {},
            },
            "properties": {
                value: {
                    type: "number",
                    changeEvent: "change"
                },
            }
        },
        "wijdialog": {
            "events": {
                "blur": {},
                "buttonCreating": {},
                "resize": {},
                "stateChanged": {},
                "focus": {},
                "resizeStart": {},
                "resizeStop": {},
                "open": {},
                "close": {},
                "beforeClose": {},
                "drag": {},
                "dragStart": {},
                "dragStop": {}
            },
            "properties": {
                "hide": {},
                "show": {},
                buttons: {
                    type: "object",
                    twoWayBinding: true
                },
                "collapsingAnimation": {},
                "expandingAnimation": {},
                "captionButtons": {
                    properties: {
                        pin: { properties: { visible: { type: "boolean" } } },
                        refresh: { properties: { visible: { type: "boolean" } } },
                        toggle: { properties: { visible: { type: "boolean" } } },
                        minimize: { properties: { visible: { type: "boolean" } } },
                        maximize: { properties: { visible: { type: "boolean" } } },
                        close: { properties: { visible: { type: "boolean" } } }
                    }
                }
            }
        },
        "wijaccordion": {
            "events": {
                "beforeSelectedIndexChanged": {},
                "selectedIndexChanged": {}
            },
            "properties": {
                "duration": {},
                selectedIndex: {
                    type: "number",
                    changeEvent: "selectedindexchanged"
                }
            }
        },
        "wijpopup": {
            "events": {
                "showing": {},
                "shown": {},
                "hiding": {},
                "hidden": {},
                "posChanged": {}
            }
        },
        "wijsuperpanel": {
            "events": {
                "dragStop": {},
                "painted": {},
                "scroll": {},
                "scrolling": {},
                "scrolled": {},
                "resized": {}
            },
            "properties": {
                "hScrollerActivating": {},
                "vScrollerActivating": {}
            }
        },
        "wijcheckbox": {
            isDecorator: true,
            "properties": {
                "checked": {
                    type: "boolean",
                    changeEvent: "changed"
                }
            }
        },
        "wijradio": {
            isDecorator: true,
            "properties": {
                "checked": {
                    type: "boolean",
                    changeEvent: "changed"
                }
            }
        },
        "wijlist": {
            "events": {
                "focusing": {},
                "focus": {},
                "blur": {},
                "selected": {},
                "listRendered": {},
                "itemRendering": {},
                "itemRendered": {}
            },
            "properties": {
                "superPanelOptions": {},
                dataSource: {
                    type: "array",
                    twoWayBinding: true
                },
                listItems: {
                    type: "object",
                    twoWayBinding: true
                }
            }
        },
        "wijcalendar": {
            "events": {
                "beforeSlide": {},
                "beforeSelect": {},
                "selectedDatesChanged": {},
                "afterSelect": {},
                "afterSlide": {}
            },
            "properties": {
                "customizeDate": {},
                "title": {},
                selectedDates: {
                    type: "array",
                    singular: "date",
                    elementType: "date",
                    changeEvent: "selecteddateschanged"
                }
            }
        },
        wijdropdown: {
            isDecorator: true
        },
        "wijexpander": {
            "events": {
                "beforeCollapse": {},
                "afterCollapse": {},
                "beforeExpand": {},
                "afterExpand": {}
            },
            properties: {
                expanded: {
                    type: "boolean",
                    attachEvents: ["aftercollapse", "afterexpand"]
                }
            }
        },
        "wijmenu": {
            "events": {
                "focus": {},
                "blur": {},
                "select": {},
                "showing": {},
                "shown": {},
                "hidding": {},
                "hidden": {}
            },
            "properties": {
                "superPanelOptions": {}
            }
        },
        "wijmenuitem": {
            "events": {
                "hidding": {},
                "hidden": {},
                "showing": {},
                "shown": {}
            }
        },
        "wijtabs": {
            "events": {
                "add": {},
                "remove": {},
                "select": {},
                "beforeShow": {},
                "show": {},
                "load": {},
                "disable": {},
                "enable": {}
            },
            "properties": {
                "ajaxOptions": {},
                "cookie": {},
                "hideOption": {},
                "showOption": {}
            }
        },
        wijtextbox: {
            isDecorator: true
        },
        "wijpager": {
            "events": {
                "pageIndexChanging": {},
                "pageIndexChanged": {}
            },
            properties: {
                pageIndex: {
                    type: "numeric",
                    changeEvent: "pageindexchanged"
                }
            }
        },
        "wijcombobox": {
            "events": {
                "select": {},
                "search": {},
                "open": {},
                "close": {},
                "changed": {},
                "textChanged": {},
                "selectedIndexChanged": {},
                "selectedIndexChanging": {},
            },
            "properties": {
                dataSource: {
                    type: "array",
                    twoWayBinding: true
                },
                data: {
                    type: "object",
                    twoWayBinding: true
                },
                value: {
                    changeEvent: "change"
                },
                "labelText": {},
                "showingAnimation": {},
                "hidingAnimation": {},
                selectedIndex: {
                    type: "numeric",
                    changeEvent: "changed"
                },
                selectedValue: {
                    changeEvent: "changed"
                },
                text: {
                    changeEvent: "changed"
                },
                inputTextInDropDownList: {
                    changeEvent: "changed"
                },
                "listOptions": {}
            }
        },
        "wijinputcore": {
            "events": {
                "initializing": {},
                "initialized": {},
                "triggerMouseDown": {},
                "triggerMouseUp": {},
                "dropDownButtonMouseDown": {},
                "dropDownButtonMouseUp": {},
                "dropDownOpen": {},
                "dropDownClose": {},
                "textChanged": {},
                "invalidInput": {}
            },
            "properties": {
                pickers: {
                    "properties": {
                        list: {
                            type: "Array",
                            twoWayBinding: true,
                        }
                    }
                }
            }
        },
        "wijinputdate": {
            inherits: "wijinputcore",
            "events": {
                "dateChanged": {}
            },
            "properties": {
                date: {
                    type: "datetime",
                    changeEvent: ["dateChanged", "textChanged"]
                },
                "minDate": {},
                "maxDate": {}
            }
        },
        "wijinputmask": {
            inherits: "wijinputcore",
            "properties": {
                "text": {
                    type: "string",
                    changeEvent: "textChanged"
                },
                maskFormat: {
                    type: "object",
                    twoWayBinding: true
                }
            }
        },
        "wijinputnumber": {
            inherits: "wijinputcore",
            "events": {
                "valueChanged": {},
                "valueBoundsExceeded": {}
            },
            "properties": {
                value: {
                    type: "number",
                    changeEvent: ["valueChanged", "textChanged"]
                }
            }
        },
        "wijinputtext": {
            inherits: "wijinputcore",
            "properties": {
                "text": {
                    type: "string",
                    changeEvent: "textChanged"
                }
            }
        },
        "wijgrid": {
            "properties": {
                data: { changeEvent: "afterCellEdit" },
                dataSource: { twoWayBinding: true },
                cellStyleFormatter: {
                    twoWayBinding: true
                },
                rowStyleFormatter: {
                    twoWayBinding: true
                },
                "columns": {
                    twoWayBinding: true,
                    type: "array",
                    singular: "column",
                    elementType: {
                        type: "object",
                        properties: {
                            allowSizing: {
                                type: "boolean"
                            },
                            allowSort: {
                                type: "boolean"
                            },
                            visible: {
                                type: "boolean"
                            },
                            dataFormatString: { type: "string" },
                            readOnly: { type: "boolean" },
                            "dataKey": { type: "string" },
                            "dataType": { type: "string" },
                            "headerText": { type: "string" },
                            "cellFormatter": { twoWayBinding: true },
                            groupInfo: {
                                type: "object",
                                properties: {
                                    headerText: {},
                                    footerText: {},
                                    outlineMode: {},
                                }
                            },
                            "showFilter": {
                                type: "boolean"
                            },
                            command: {
                                type: "object",
                                properties: {
                                    text: { type: "string" },
                                    click: { twoWayBinding: true },
                                    iconClass: { type: "string" }
                                }
                            },
                            cancelCommand: {
                                type: "object",
                                properties: {
                                    text: { type: "string" },
                                    click: { twoWayBinding: true },
                                    iconClass: { type: "string" }
                                }
                            },
                            deleteCommand: {
                                type: "object",
                                properties: {
                                    text: { type: "string" },
                                    click: { twoWayBinding: true },
                                    iconClass: { type: "string" }
                                }
                            },
                            editCommand: {
                                type: "object",
                                properties: {
                                    text: { type: "string" },
                                    click: { twoWayBinding: true },
                                    iconClass: { type: "string" }
                                }
                            },
                            updateCommand: {
                                type: "object",
                                properties: {
                                    text: { type: "string" },
                                    click: { twoWayBinding: true },
                                    iconClass: { type: "string" }
                                }
                            }
                        }
                    }
                }
            },
            "events": {
                "ajaxError": {},
                "dataLoading": {},
                "dataLoaded": {},
                "loading": {},
                "loaded": {},
                "columnDropping": {},
                "columnDropped": {},
                "columnGrouping": {},
                "columnGrouped": {},
                "columnUngrouping": {},
                "columnUngrouped": {},
                "filtering": {},
                "filtered": {},
                "sorting": {},
                "sorted": {},
                "currentCellChanged": {},
                "pageIndexChanging": {},
                "pageIndexChanged": {},
                "rendering": {},
                "rendered": {},
                "columnResizing": {},
                "columnResized": {},
                "currentCellChanging": {},
                "afterCellEdit": {},
                "afterCellUpdate": {},
                "beforeCellEdit": {},
                "beforeCellUpdate": {},
                "columnDragging": {},
                "columnDragged": {},
                "filterOperatorsListShowing": {},
                "groupAggregate": {},
                "groupText": {},
                "invalidCellValue": {},
                "selectionChanged": {},
                "cellClicked": {}
            },
        },
        "wijgridfilter": {
            inherits: "wijgrid"
        },
        "wijchartcore": {
            "events": {
                "beforeSeriesChange": {},
                "afterSeriesChange": {},
                "seriesChanged": {},
                "beforePaint": {},
                "painted": {},
                "mouseDown": {},
                "mouseUp": {},
                "mouseOver": {},
                "mouseOut": {},
                "mouseMove": {},
                "click": {}
            },
            "properties": {
                axis: {
                    type: "object",
                    properties: {
                        x: chartAxisProperties,
                        y: chartAxisProperties,
                    }
                },
                data: { twoWayBinding: true },
                dataSource: { twoWayBinding: true },
                "width": { type: "number" },
                "height": { type: "number" },
                seriesList: {
                    changeEvent: "serieschanged",
                    singular: "series",
                    type: "array",
                    elementType: {
                        type: "object",
                        properties: {
                            dataSource: {
                                type: "array",
                                twoWayBinding: true
                            },
                            data: { twoWayBinding: true }
                        }
                    }
                }
            }
        },
        "wijcompositechart": {
            inherits: "wijchartcore",
            "properties": {
                yAxes: {
                    singular: "y",
                    type: "array",
                    elementType: chartAxisProperties
                },
                seriesList: {
                    changeEvent: "serieschanged",
                    singular: "series",
                    type: "array",
                    elementType: {
                        type: "object",
                        properties: {
                            dataSource: {
                                type: "array",
                                twoWayBinding: true
                            },
                            data: {
                            },
                            radius: {
                                type: "number"
                            },
                            yAxis: {
                                type: "number"
                            }
                        }
                    }
                }
            }
        },
        "wijbarchart": {
            inherits: "wijchartcore"
        },
        "wijlinechart": {
            inherits: "wijchartcore",
            "properties": {
                "hole": {},
                seriesList: {
                    changeEvent: "serieschanged",
                    singular: "series",
                    type: "array",
                    elementType: {
                        type: "object",
                        properties: {
                            dataSource: {
                                type: "array",
                                twoWayBinding: true
                            },
                            data: {},
                            markers: {
                                type: "object",
                                properties: {
                                    visible: { type: "boolean" },
                                    symbol: {
                                        type: "array",
                                        singular: "symbol",
                                        elementType: {
                                            type: "object",
                                            properties: {
                                                width: { type: "number" },
                                                height: { type: "number" },
                                                url: { type: "string" },
                                                index: { type: "number" }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "wijscatterchart": {
            inherits: "wijchartcore"
        },
        "wijbubblechart": {
            inherits: "wijchartcore",
            properties: {
                seriesList: {
                    changeEvent: "serieschanged",
                    singular: "series",
                    type: "array",
                    elementType: {
                        type: "object",
                        properties: {
                            dataSource: {
                                type: "array",
                                twoWayBinding: true
                            },
                            data: {},
                            markers: {
                                type: "object",
                                properties: {
                                    symbol: {
                                        type: "array",
                                        singular: "symbol",
                                        elementType: {
                                            type: "object",
                                            properties: {
                                                url: { type: "string" },
                                                index: { type: "number" }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "wijpiechart": {
            inherits: "wijchartcore",
            "properties": {
                "radius": { type: "number" }
            }
        },
        "wijcandlestickchart": {
            inherits: "wijchartcore"
        },
        "wijchartnavigator": {
            "events": {
                "updating": {},
                "updated": {}
            },
            "properties": {
                xAxis: chartAxisProperties,
                data: { twoWayBinding: true },
                dataSource: { twoWayBinding: true },
                seriesList: {
                    changeEvent: "serieschanged",
                    singular: "series",
                    type: "array",
                    elementType: {
                        type: "object",
                        properties: {
                            dataSource: {
                                type: "array",
                                twoWayBinding: true
                            },
                            data: { twoWayBinding: true }
                        }
                    }
                },
                seriesStyles: {
                    type: "array",
                    singular: "seriesStyle",
                    elementType: {
                        type: "object"
                    }
                },
                targetSelector: {},
                rangeMin: { type: "number" },
                rangeMax: { type: "number" },
                step: { type: "number" },
                indicator: {
                    type: "object",
                    properties: {
                        visible: { type: "boolean" },
                        format: {}
                    }
                }
            }
        },
        "wijtree": {
            "events": {
                "nodeBeforeDropped": {},
                "nodeDropped": {},
                "nodeBlur": {},
                "nodeFocus": {},
                "nodeClick": {},
                "nodeCheckChanging": {},
                "nodeCheckChanged": {},
                "nodeCollapsed": {},
                "nodeExpanded": {},
                "nodeDragging": {},
                "nodeDragStarted": {},
                "nodeMouseOver": {},
                "nodeMouseOut": {},
                "nodeTextChanged": {},
                "selectedNodeChanged": {},
                "nodeExpanding": {},
                "nodeCollapsing": {}
            },
            properties: {
                nodes: {
                    type: "array",
                    singular: "node",
                    changeEvent: ["nodeCheckChanged", "nodeCollapsed", "nodeExpanded",
                        "nodeTextChanged", "selectedNodeChanged"]

                }
            }
        },
        "wijtreenode": {
            "events": {
                "nodeTextChanged": {},
                "nodeDragStarted": {},
                "nodeDragging": {},
                "nodeCheckChanging": {},
                "nodeCheckChanged": {},
                "nodeFocus": {},
                "nodeBlur": {},
                "nodeClick": {},
                "selectedNodeChanged": {},
                "nodeMouseOver": {},
                "nodeMouseOut": {}
            },
        },
        "wijupload": {
            "events": {
                "cancel": {},
                "totalComplete": {},
                "progress": {},
                "complete": {},
                "totalProgress": {},
                "upload": {},
                "totalUpload": {}
            }
        },
        "wijwizard": {
            "events": {
                "show": {},
                "add": {},
                "remove": {},
                "activeIndexChanged": {},
                "validating": {},
                "load": {}
            },
            "properties": {
                "ajaxOptions": {},
                "cookie": {}
            }
        },
        "wijribbon": {
            "events": {
                "click": {}
            }
        },
        "wijeditor": {
            "events": {
                "commandButtonClick": {},
                "textChanged": {}
            },
            "properties": {
                "simpleModeCommands": {
                    type: "array",
                    twoWayBinding: true
                },
                "localization": {},
                text: {
                    type: "string",
                    twoWayBinding: true,
                    changeEvent: "textChanged"
                }
            }
        },
        "wijrating": {
            "events": {
                "hover": {},
                "rating": {},
                "rated": {},
                "reset": {}
            },
            "properties": {
                "min": {},
                "max": {},
                "animation": {},
                value: {
                    type: "number",
                    changeEvent: ["rated", "reset"]
                },
                hint: {
                    properties: {
                        disabled: { type: "boolean" },
                        content: {
                            type: "array",
                            twoWayBinding: true
                        }
                    }
                }
            }
        },
        "wijcarousel": {
            "events": {
                "loadCallback": {},
                "itemClick": {},
                "beforeScroll": {},
                "afterScroll": {},
                "create": {}
            }
        },
        "wijgallery": {
            "events": {
                "loadCallback": {},
                "beforeTransition": {},
                "afterTransition": {},
                "create": {}
            }
        },
        "wijgauge": {
            "properties": {
                "ranges": {
                    type: "array",
                    singular: "range",
                    elementType: {
                        type: "object",
                        properties: {
                            "startValue": { type: "number" },
                            "endValue": { type: "number" },
                            "startDistance": { type: "number" },
                            "endDistance": { type: "number" },
                            "startWidth": { type: "number" },
                            "endWidth": { type: "number" }
                        }
                    }
                },
                value: {
                    type: "number",
                    twoWayBinding: true,
                    changeEvent: "valueChanged"
                },
                face: {
                    properties: {
                        template: { twoWayBinding: true }
                    }
                }
            },
            "events": {
                "beforeValueChanged": {},
                "valueChanged": {},
                "painted": {},
                "click": {},
                "create": {}
            }
        },
        "wijlineargauge": {
            inherits: "wijgauge"
        },
        "wijradialgauge": {
            inherits: "wijgauge"
        },
        "wijlightbox": {
            "events": {
                "show": {},
                "beforeShow": {},
                "beforeClose": {},
                "close": {},
                "open": {}
            },
            "properties": {
                "cookie": {}
            }
        },
        "wijdatepager": {
            "events": {
                "selectedDateChanged": {}
            },
            "properties": {
                "localization": {},
                "selectedDate": {
                    type: "datetime",
                    changeEvent: "selectedDateChanged"
                }
            }
        },
        "wijevcal": {
            "events": {
                "viewTypeChanged": {},
                "selectedDatesChanged": {},
                "initialized": {},
                "beforeDeleteCalendar": {},
                "beforeAddCalendar": {},
                "beforeUpdateCalendar": {},
                "beforeAddEvent": {},
                "beforeUpdateEvent": {},
                "beforeDeleteEvent": {},
                "beforeEditEventDialogShow": {},
                "eventsDataChanged": {},
                "calendarsChanged": {}
            },
            "properties": {
                "localization": { twoWayBinding: true },
                "datePagerLocalization": {},
                "colors": {},
                "selectedDate": {
                    type: "date",
                    changeEvent: "selectedDatesChanged"
                },
                "selectedDates": {
                    type: "date",
                    changeEvent: "selectedDatesChanged"
                },
                eventsData: {
                    type: "array",
                    changeEvent: "eventsdatachanged"
                },
                appointments: {
                    type: "array",
                    singular: "appointment",
                    changeEvent: "eventsdatachanged"
                }
            }
        },

        "wijvideo": {
            isDecorator: true,
            "properties": {
                "fullScreenButtonVisible": {
                    type: "boolean"
                },
                "showControlsOnHover": {
                    type: "boolean"
                }
            }
        },

        "wijspread": {
            properties: {
                dataSource: {
                    type: "array",
                    twoWayBinding: true
                },
                sheetCount: {
                    type: "number"
                },
                isProtected: {
                    type: "boolean"
                },
                sheets: {
                    type: "array",
                    singular: "sheet",
                    elementType: {
                        type: "object",
                        properties: {
                            data: {
                                type: "array",
                                twoWayBinding: true
                            },
                            isProtected: {
                                type: "boolean"
                            },
                            rowCount: {
                                type: "number"
                            },
                            colCount: {
                                type: "number"
                            },
                            defaultRowCount: {
                                type: "number"
                            },
                            defaultColCount: {
                                type: "number"
                            },
                            autoGenerateColumns: {
                                type: "boolean"
                            },
                            columns: {
                                type: "array",
                                singular: "column",
                                elementType: {
                                    type: "object",
                                    properties: {
                                        displayName: {
                                            type: "string"
                                        },
                                        name: {
                                            type: "string"
                                        },
                                        width: {
                                            type: "number"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },

        "wijsparkline": {
            "events": {
                "mouseMove": {},
                "click": {}
            },
            "properties": {
                "disabled": {},
                "type": {},
                "width": {},
                "height": {},
                "valueAxis": {},
                "origin": {},
                "min": {},
                "max": {},
                "columnWidth": {},
                "tooltipFormat": {},
                "bind": {},
                "data": {
                    type: "array",
                    twoWayBinding: true
                },
                "seriesList": {
                    type: "array",
                    singular: "series",
                    elementType: {
                        type: "object",
                        properties: {
                            type: {
                                type: "string"
                            },
                            bind: {
                                type: "string"
                            },
                            data: {
                                type: "array"
                            },
                            seriesStyle: {
                                type: "object"
                            }
                        }
                    }
                },
                "seriesStyles": {
                    type: "array",
                    singular: "seriesStyle",
                    elementType: {
                        type: "object"
                    }
                }
            }
        },

        "wijtreemap": {
            "properties": {
                "disabled": {
                    type: "boolean"
                },
                "type": {},
                "width": {
                    type: "number"
                },
                "height": {
                    type: "number"
                },
                "data": {
                    type: "array",
                    twoWayBinding: true
                },
                "valueBinding": {},
                "labelBinding": {},
                "colorBinding": {},
                "showLabel": {
                    type: "boolean"
                },
                "labelFormatter": {
                    type: "function",
                    twoWayBinding: true
                },
                "showTooltip": {
                    type: "boolean"
                },
                "tooltipOptions": {},
                "showTitle": {
                    type: "boolean"
                },
                "titleHeight": {
                    type: "number"
                },
                "titleFormatter": {
                    type: "function",
                    twoWayBinding: true
                },
                "minColor": {},
                "minColorValue": {
                    type: "number"
                },
                "midColor": {},
                "midColorValue": {
                    type: "number"
                },
                "maxColor": {},
                "maxColorValue": {
                    type: "number"
                },
                "showBackButtons": {
                    type: "boolean"
                }
            },
            "events": {
                "itemPainting": {},
                "itemPainted": {},
                "painting": {},
                "painted": {},
                "drillingDown": {},
                "drilledDown": {},
                "rollingUp": {},
                "rolledUp": {}
            }
        },

        wijfileexplorer: {
            properties: {
                mode: {},
                searchPatterns: {},
                initPath: {},
                viewPaths: {
                    type: "array",
                    twoWayBinding: true
                },
                viewMode: {},
                allowFileExtensionRename: {
                    type: "boolean"
                },
                allowMultipleSelection: {
                    type: "boolean"
                },
                allowPaging: {
                    type: "boolean"
                },
                pageSize: {
                    type: "number"
                },
                shortcuts: {
                    type: "object"
                },
                enableOpenFile: {
                    type: "boolean"
                },
                enableCreateNewFolder: {
                    type: "boolean"
                },
                enableCopy: {
                    type: "boolean"
                },
                enableFilteringOnEnterPressed: {
                    type: "boolean"
                },
                currentFolder: {
                    changeEvent: "folderChanged"
                },
                visibleControls: {},
                treePanelWidth: {
                    type: "number"
                },
                hostUri: {},
                actionUri: {},
                disabled: {
                    type: "boolean"
                }
            },
            events: {
                itemSelected: {},
                fileOpened: {},
                fileOpening: {},
                itemCopying: {},
                itemCopied: {},
                itemRenaming: {},
                itemRenamed: {},
                itemDeleting: {},
                itemDeleted: {},
                errorOccurred: {},
                itemPasting: {},
                itemPasted: {},
                itemMoving: {},
                itemMoved: {},
                newFolderCreating: {},
                newFolderCreated: {},
                folderChanged: {},
                folderLoaded: {},
                filtering: {},
                filtered: {}
            }
        },

        wijmaps: {
            properties: {
                source: {
                    twoWayBinding: true
                },
                center: {
                    changeEvent: "centerChanged"
                },
                zoom: {
                    type: 'number',
                    changeEvent: "zoomChanged"
                },
                showTools: {
                    type: "boolean"
                },
                targetZoom: {
                    type: 'number',
                    changeEvent: "targetZoomChanged"
                },
                targetZoomSpeed: {
                    type: 'number'
                },
                targetCenter: {
                    changeEvent: "targetCenterChanged"
                },
                targetCenterSpeed: {
                    type: 'number'
                },
                layers: {
                    type: 'array',
                    twoWayBinding: true
                },
                disabled: {
                    type: "boolean"
                }
            },
            events: {
                zoomChanged: {},
                targetZoomChanged: {},
                centerChanged: {},
                targetCenterChanged: {}
            }
        }
    };

    // register directives for all widgets
    $.each($.wijmo, (name, clazz) => {
        if (!name.match(/^wij/)) return;
        var directiveName = "wij" + name.charAt(3).toUpperCase() + name.substring(4);
        registerDirective(name, "wijmo", clazz, directiveName);
    });
    $.each($.ui, (name, clazz) => registerDirective(name, "ui", clazz, "jqui" + name.charAt(0).toUpperCase() + name.substring(1)));

    function hasChildElements(node) {
        if (!node || !node.childNodes) return false;
        var len = node.childNodes.length;
        for (var i = 0; i < len; i++) {
            var child = node.childNodes[i];
            if (child.nodeType == Node.ELEMENT_NODE) {
                return true;
            }
        }

        return false;
    }
}
