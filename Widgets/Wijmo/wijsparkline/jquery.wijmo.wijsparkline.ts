﻿/// <reference path="../Base/jquery.wijmo.widget.ts" />
/// <reference path="../wijutil/jquery.wijmo.wijutil.ts" />
/// <reference path="../wijutil/jquery.wijmo.raphael.ts"/> 

/*globals $, Raphael, jQuery, document, window */

/*
* Depends:
*  raphael.js
*  globalize.js
*
*/

module wijmo.sparkline {

	var $: JQueryStatic = jQuery,
		widgetName: string = "wijsparkline",
		defaultSeriesStyle = {
			line: {
				stroke: "#5b8f00",
				"stroke-width": 1,
				opacity: 0.9
			},
			area: {
				stroke: "#009999",
				"stroke-width": 1,
				fill: "#0099ff",
				"fill-opacity": 0.5
			},
			column: {
				stroke: "none",
				fill: "#0099ff",
				"fill-opacity": 0.9,
				negStyle: {
					fill: "#ff0000"
				},
				zeroStyle: {
					fill: "#808080"
				}
			}
		};

	/** @ignore */
	export class SparklineTooltip {
		_style: any;
		_tooltipEle: JQuery;
		_hOffset: number;
		_vOffset: number;

		constructor(style) {
			this._hOffset = 15;
			this._vOffset = -15;
			this._tooltipEle = $("<div style='position:absolute' class='wijmo-wijsparkline-tooltip'></div>");
			this._tooltipEle.appendTo("body").hide();
		}

		show(content: string, position: MousePosition) {
			this._tooltipEle.css({
				"left": position.originalLeft + this._hOffset,
				"top": position.originalTop + this._vOffset
			})
				.empty()
				.append(content);

			if (this._tooltipEle.is(":hidden")) {
				this._tooltipEle.show();
			}
		}

		hide() {
			this._tooltipEle.hide();
		}

		destroy() {
			this._tooltipEle.remove();
			this._tooltipEle = null;
		}
	}

	/**
	* @widget
	*/
	export class wijsparkline extends wijmoWidget {
		canvas: RaphaelPaper;
		canvasBounds: CanvasBounds;
		seriesRegionInfos: SeriesRegionInfo[];
		tooltip: SparklineTooltip;
		indicatorLine: RaphaelPath;
		wrapper: JQuery;
		canvasWrapper: JQuery;
		data: any[];
		containsColumnType: boolean;
		colunmWidth: number;

		_create() {
			var self = this,
				o: WijSparklineOptions = self.options,
				width: number = o.width || self.element.width(),
				height: number = o.height || self.element.height(),
				originalStyle: string = self.element.attr("style"),
				canvas: RaphaelPaper, vals: string;

			self.wrapper = $("<div class='ui-widget " + o.wijCSS.sparkline + "'></div>")
				.attr("style", originalStyle || "");
			if (width) {
				self.wrapper.css("width", width);
			}

			if (height) {
				self.wrapper.css("height", height);
			}

			self.canvasWrapper = $("<div class='" + o.wijCSS.canvasWrapper + "'></div>");

			self.element.wrap(self.wrapper)
				.after(self.canvasWrapper)
				.hide();

			if (self.canvasWrapper.is(":hidden") && self.canvasWrapper.wijAddVisibilityObserver) {
				if (self.canvasWrapper.hasClass("wijmo-wijobserver-visibility")) {
					return;
				}

				self.canvasWrapper.wijAddVisibilityObserver(function () {
					self.redraw();
					if (self.canvasWrapper.wijRemoveVisibilityObserver) {
						self.canvasWrapper.wijRemoveVisibilityObserver();
					}
				}, "wijsparkline");
			}

			if (o.data) {
				self.data = o.data;
			} else {
				vals = self.element.html();
				self.data = vals.replace(/(^\s*<!--)|(-->\s*$)|\s+/g, '').split(',');
			}

			canvas = Raphael(self.canvasWrapper[0], width, height);

			self.canvas = canvas;
			if (self.canvasWrapper.is(":visible")) {
				self._render();
			}

			super._create();
		}

		_setOption(key: string, value: any) {
			var o: WijSparklineOptions = this.options;
			if (key === "height" || key === "width") {
				o[key] = value;
				this.element[key](value);
			} else if (key === "data") {
				this.data = o.data = value;
			} else {
				super._setOption(key, value);
			}

			this.redraw();
		}

		_innerDisable() {
			super._innerDisable();
			this._toggleDisableSparkline(true);
		}

		_innerEnable() {
			super._innerEnable();
			this._toggleDisableSparkline(false);
		}

		_toggleDisableSparkline(disabled: boolean) {
			this.element.parent().toggleClass(this.options.wijCSS.stateDisabled, disabled);
		}

		_render() {
			var self: wijsparkline = this,
				o: WijSparklineOptions = self.options,
				type: string = o.type,
				seriesList: WijSparklineSeries[] = o.seriesList,
				seriesStylesLength: number = o.seriesStyles ? o.seriesStyles.length : 0,
				seriesStyle: WijSparklineSeriesStyle;

			self.seriesRegionInfos = [];

			self.canvas.clear();

			self.canvasBounds = {
				startX: 0,
				width: o.width || this.element.width() || this.canvasWrapper.width(),
				startY: 0,
				height: o.height || this.element.height() || this.canvasWrapper.height()
			};

			type = type && type !== "" ? type.toLowerCase() : "line";
			self.containsColumnType = type === "column" || $.grep(seriesList, (s: any) => s.type && s.type.toLowerCase() === "column").length > 0;

			if (seriesList && seriesList.length > 0) {
				$.each(seriesList, (idx, series) => {
					var seriesType = series.type && series.type !== "" ? series.type.toLowerCase() : type;

					seriesStyle = seriesStylesLength > 0 ? o.seriesStyles[idx % seriesStylesLength] : null;

					self._internalRender(series, o, seriesType, seriesStyle);
				});
			} else {
				seriesStyle = seriesStylesLength > 0 ? o.seriesStyles[0] : null;
				self._internalRender(null, o, type, seriesStyle);
			}

			self._bindCanvasEvents();
		}

		private _internalRender(series: WijSparklineSeries, options: WijSparklineOptions, type: string, seriesStyle: WijSparklineSeriesStyle) {
			var render: sparklineRenderBase,
				seriesRegionInfo: SeriesRegionInfo,
				data: any[],
				columnWidth: number,
				defaultStyle: WijSparklineSeriesStyle = defaultSeriesStyle[type];

			seriesStyle = $.extend({}, defaultStyle, seriesStyle, series ? series.seriesStyle : null);
			data = series && series.data ? series.data : this.data;

			if (series && series.columnWidth !== undefined
				&& series.columnWidth !== null && typeof series.columnWidth === "number") {
				columnWidth = series.columnWidth;
			} else {
				columnWidth = options.columnWidth;
			}
			render = new wijmo.sparkline[type + "Render"](this.element, this.canvas, type, {
				data: data,
				min: options.min,
				max: options.max,
				valueAxis: options.valueAxis,
				origin: options.origin,
				animation: options.animation,
				bind: series ? series.bind || options.bind : options.bind,
				seriesStyle: seriesStyle,
				columnWidth: columnWidth,
				canvasBounds: this.canvasBounds
			});

			if (!render.isValid()) {
				return true;
			}

			render.render();

			seriesRegionInfo = <SeriesRegionInfo>{
				type: type,
				bind: series ? series.bind : null,
				regionInfos: render.getRegionInfos()
			};

			if (type === "column") {
				this.colunmWidth = render.getColumnWidth();
			}

			this.seriesRegionInfos.push(seriesRegionInfo);
		}

		/**
		* This method redraws the chart.
		*/
		redraw() {
			var o: WijSparklineOptions = this.options,
				width: number = 0,
				height: number = 0;

			width = o.width || this.element.width() || this.canvasWrapper.width();
			height = o.height || this.element.height() || this.canvasWrapper.height();

			if (width < 1 || height < 1) {
				return;
			}

			this.canvas.setSize(width, height);
			this._render();
		}

		/**
		* Remove the functionality completely. This will return the element back to its pre-init state. 
		*/
		destroy() {
			this._unbindCanvasEvents();

			this.element.unwrap()
				.show();

			this.canvas.clear();

			this.wrapper = null;
			this.canvasWrapper.remove();
			this.canvasWrapper = null;

			this.seriesRegionInfos = [];
			this.canvasBounds = null;

			if (this.tooltip) {
				this.tooltip.destroy();
				this.tooltip = null;
			}

			super.destroy();
		}

		_bindCanvasEvents() {
			var self: wijsparkline = this,
				o: WijSparklineOptions = self.options,
				$wrapper: JQuery = self.canvasWrapper,
				touchEventPre: string = "",
				namespace: string = "." + widgetName;

			if (!$wrapper || !self.canvas) {
				return;
			}

			if ($.support.isTouchEnabled && $.support.isTouchEnabled()) {
				if (window.navigator["pointerEnabled"]) {
					this.element.css("touch-action", "none");
				}
				else if (window.navigator.msPointerEnabled) {
					this.element.css("-ms-touch-action", "none");
				}
			}

			if ($.support.isTouchEnabled && $.support.isTouchEnabled()) {
				touchEventPre = "wij";
			}
			$wrapper.bind(touchEventPre + "mousemove" + namespace, function (e: JQueryEventObject) {
				var mousePos: MousePosition = self._getMousePosition($wrapper, e),
					disabled: boolean = self._isDisabled();

				if (disabled) {
					return;
				}

				self._mouseMoveInsideArea(mousePos);
			});

			$wrapper.bind(touchEventPre + "mouseout" + namespace, function (e: any) {
				var tg = e.currentTarget,
					reltg = (e.relatedTarget) ? e.relatedTarget : e.toElement,
					disabled: boolean = self._isDisabled();

				if (disabled) {
					return;
				}

				if (tg.nodeName.toLowerCase() !== 'div') {
					return;
				}

				while (reltg && reltg !== tg && reltg.nodeName.toLowerCase() !== 'body') {
					reltg = reltg.parentNode;
				}

				if (reltg === tg) {
					return;
				}

				self._mouseOutInsideArea();
			});

			$wrapper.bind(touchEventPre + "click" + namespace, function (e: JQueryEventObject) {
				var mousePos: MousePosition = self._getMousePosition($wrapper, e),
					disabled: boolean = self._isDisabled(),
					currentRegionInfos: SeriesRegionInfo[] = [];

				if (disabled) {
					return;
				}

				currentRegionInfos = self._getCurrentRegionInfos(mousePos);

				self._trigger("click", null, {
					currentRegion: currentRegionInfos
				});
			});
		}

		_getMousePosition(element: JQuery, e: JQueryEventObject): MousePosition {
			var elePos: JQueryCoordinates = element.offset(),
				isQuirksMode: boolean = document.compatMode === "CSS1Compat",
				originalLeft: number = isQuirksMode ? e.pageX : e.clientX,
				originalTop: number = isQuirksMode ? e.pageY : e.clientY,
				mousePos: MousePosition = {
					left: originalLeft - elePos.left,
					originalLeft: originalLeft,
					top: originalTop - elePos.top,
					originalTop: originalTop
				};

			return mousePos;
		}

		_mouseMoveInsideArea(mousePos: MousePosition) {
			var self: wijsparkline = this,
				o: WijSparklineOptions = self.options,
				format: string = o.tooltipFormat,
				content: () => string = o.tooltipContent,
				tooltipContent: string = "",
				currentRegionInfos: SeriesRegionInfo[] = [],
				currentLeftPosition: number = NaN;

			currentRegionInfos = self._getCurrentRegionInfos(mousePos);

			if (currentRegionInfos.length > 0) {
				$.each(currentRegionInfos, (idx, currentRegionInfo) => {
					var currentContent: string;
					if (idx === 0) {
						tooltipContent += "<span>"
					}
					if (content && $.isFunction(content)) {
						currentContent = $.proxy(content, self.data[currentRegionInfo.index])();
					} else {
						if (currentRegionInfo.bind) {
							tooltipContent += currentRegionInfo.bind + ": ";
						}
						if (format) {
							currentContent = format.replace(/\{(\d+)\}/g, currentRegionInfo.value.toString());
						} else {
							currentContent = currentRegionInfo.value.toString();
						}
					}
					tooltipContent += currentContent;
					if (idx < currentRegionInfos.length - 1) {
						tooltipContent += "<br />"
					} else {
						tooltipContent += "</span>"
					}

					if (isNaN(currentLeftPosition)) {
						if (self.containsColumnType) {
							if (currentRegionInfo.type === "column") {
								currentLeftPosition = currentRegionInfo.position.x + self.colunmWidth / 2;
							}
						} else {
							if (currentRegionInfo.type === "line" || currentRegionInfo.type === "area") {
								currentLeftPosition = currentRegionInfo.position.x;
							}
						}
					}
				});

				this._paintTooltip(tooltipContent, mousePos);

				this._paintIndicatorLine(currentLeftPosition);
			}

			this._trigger("mouseMove", null, {
				currentRegion: currentRegionInfos
			});
		}

		_mouseOutInsideArea() {
			if (this.tooltip) {
				this.tooltip.hide();
			}

			if (this.indicatorLine) {
				this.indicatorLine.wijRemove();
				this.indicatorLine = null;
			}
		}

		_getCurrentRegionInfos(mousePos: MousePosition): SeriesRegionInfo[] {
			var self: wijsparkline = this,
				currentRegionInfos: SeriesRegionInfo[] = [];

			$.each(this.seriesRegionInfos, (idx, regionInfo) => {
				var currentRegionInfo: SeriesRegionInfo,
					methodName: string = "_" + regionInfo.type + "GetCurrentRegionInfo",
					method: Function = self[methodName];

				if (method) {
					currentRegionInfo = method.call(self, regionInfo, mousePos);

					if (currentRegionInfo) {
						currentRegionInfo.type = regionInfo.type;
						currentRegionInfo.bind = regionInfo.bind;
						currentRegionInfos.push(currentRegionInfo);
					}
				}
			});

			return currentRegionInfos;
		}

		_lineGetCurrentRegionInfo(seriesInfo: SeriesRegionInfo, mousePos: MousePosition): RegionInfo {
			var regionInfos = seriesInfo.regionInfos,
				length: number,
				currentRegion: RegionInfo;

			if (!regionInfos) {
				return;
			}

			length = regionInfos.length;

			$.each(regionInfos, (idx, regionInfo) => {
				var currentPosition: Point = regionInfo.position,
					nextRegionInfo: RegionInfo, nextPostion: Point;
				if (idx < length - 1) {
					nextRegionInfo = regionInfos[idx + 1];
					nextPostion = nextRegionInfo.position;
					if (mousePos.left >= currentPosition.x && mousePos.left < nextPostion.x) {
						currentRegion = regionInfo;
						currentRegion.index = idx;
						return false;
					}
				} else {
					if (mousePos.left >= currentPosition.x) {
						currentRegion = regionInfo;
						currentRegion.index = idx;
						return false;
					}
				}
			});

			return currentRegion;
		}

		_areaGetCurrentRegionInfo(seriesInfo: SeriesRegionInfo, mousePos: MousePosition) {
			return this._lineGetCurrentRegionInfo(seriesInfo, mousePos);
		}

		_columnGetCurrentRegionInfo(seriesInfo: SeriesRegionInfo, mousePos: MousePosition) {
			return this._lineGetCurrentRegionInfo(seriesInfo, mousePos);
		}

		_pieGetCurrentRegionInfo(seriesInfo: SeriesRegionInfo, mousePos: MousePosition) {
			/* Todo */
			return null;
		}

		_bulletGetCurrentRegionInfo(seriesInfo: SeriesRegionInfo, mousePos: MousePosition) {
			/* Todo */
			return null;
		}

		_unbindCanvasEvents() {
			if ($.support.isTouchEnabled && $.support.isTouchEnabled()) {
				if (window.navigator["pointerEnabled"]) {
					this.element.css("touch-action", "");
				}
				else if (window.navigator.msPointerEnabled) {
					this.element.css("-ms-touch-action", "");
				}
			}
			this.element.unbind("." + widgetName);
		}

		_paintTooltip(tooltipContent: string, mousePos: MousePosition) {
			if (!this.tooltip) {
				this.tooltip = new SparklineTooltip(this.options.tooltipStyle);
			}

			this.tooltip.show(tooltipContent, mousePos);
		}

		_paintIndicatorLine(currentLeftPosition) {
			var height: number, left: number, top: number, pathArr: any[];

			if (!this.indicatorLine) {
				height = this.canvasBounds.height;
				left = this.canvasBounds.startX;
				top = this.canvasBounds.startY;
				pathArr = ["M", left, top, "V", height];
				this.indicatorLine = this.canvas.path(pathArr.join(" "));
				this.indicatorLine.transform("T" + currentLeftPosition + " 0");
			} else {
				this.indicatorLine.transform("T" + currentLeftPosition + " 0");
			}
		}
	}

	/** @ignore */
	export class sparklineRenderBase {
		type: string;
		options: WijSparklineRenderOptions;
		values: number[];
		minValue: number;
		maxValue: number;
		regionInfos: RegionInfo[];
		valRange: number;
		element: JQuery;
		canvas: RaphaelPaper;
		animationSet: RaphaelSet;
		isValidData: boolean = true;

		constructor(element: JQuery, canvas: RaphaelPaper, type: string, options: WijSparklineRenderOptions) {
			this.values = [];
			this.type = type;
			this.options = options;
			this.element = element;
			this.canvas = canvas;
			this.animationSet = canvas.set();

			this.scanValues();
			if (this.options.min === null || this.options.min === undefined || typeof this.options.min !== "number") {
				this.minValue = Math.min.apply(Math, this.values);
			} else {
				this.minValue = this.options.min;
			}
			if (this.options.max === null || this.options.max === undefined || typeof this.options.max !== "number") {
				this.maxValue = Math.max.apply(Math, this.values);
			} else {
				this.maxValue = this.options.max;
			}
		}

		render() {
			if (!this.element || !this.canvas) {
				return;
			}
		}

		playAnimation() {
			var o: WijSparklineRenderOptions = this.options,
				canvasBounds: CanvasBounds = o.canvasBounds,
				animation: WijSparklineAnimation = o.animation;

			if (this.animationSet.length > 0 && animation && animation.enabled) {
				this.animationSet.wijAttr("clip-rect", Raphael.format("{0} {1} 0 {2}", canvasBounds.startX, canvasBounds.startY, canvasBounds.height));

				this.animationSet.wijAnimate({ "clip-rect": Raphael.format("{0} {1} {2} {3}", canvasBounds.startX, canvasBounds.startY, canvasBounds.width, canvasBounds.height) }, animation.duration, animation.easing, null);
			}
		}

		scanValues() {
			var self: sparklineRenderBase = this,
				o: WijSparklineRenderOptions = self.options,
				bind: string = o.bind,
				dataSource: any[] = o.data;

			if ($.isArray(dataSource)) {
				$.each(dataSource, (idx, val) => {
					var value: any;
					if (bind && val && val[bind] !== undefined) {
						value = val[bind];
					} else {
						value = val;
					}

					if (typeof value === "string") {
						value = parseFloat(value);
					}

					if (isNaN(value)) {
						self.isValidData = false;
						return false;
					}

					self.values.push(value);
				});
			}
		}

		setRegionInfos() {
			var self: sparklineRenderBase = this,
				valRange: number = 0,
				canvasHeight: number = self.options.canvasBounds.height,
				canvasWidth: number = self.options.canvasBounds.width,
				dataCnt: number;

			if (!self.values || self.values.length === 0) {
				return;
			}

			if (!this.regionInfos) {
				this.regionInfos = [];
			}

			dataCnt = self.values.length;

			valRange = self.maxValue - self.minValue;
			self.valRange = valRange = valRange === 0 ? 1 : valRange;

			$.each(self.values, (idx, val) => {
				var xPosition, yPosition;

				xPosition = idx * (canvasWidth / dataCnt);
				yPosition = canvasHeight * (1 - (val - self.minValue) / valRange);
				self.regionInfos.push(<RegionInfo>{
					value: val,
					position: {
						x: xPosition,
						y: yPosition
					}
				});
			});
		}

		getRegionInfos(): RegionInfo[] {
			if (!this.regionInfos || this.regionInfos.length === 0) {
				this.setRegionInfos();
			}
			return this.regionInfos;
		}

		getColumnWidth(): number {
			return null;
		}

		isValid(): boolean {
			return this.isValidData;
		}
	}

	/** @ignore */
	export class lineRender extends sparklineRenderBase {
		line: RaphaelPath;

		constructor(element: JQuery, canvas: RaphaelPaper, type: string, options: WijSparklineRenderOptions) {
			super(element, canvas, type, options);
			this.regionInfos = [];
		}

		render() {
			var o: WijSparklineRenderOptions = this.options,
				pathArr: any[] = [];

			super.render();

			this.setRegionInfos();

			if (this.regionInfos && this.regionInfos.length > 0) {
				$.each(this.regionInfos, (idx, regionInfo) => {
					var position = regionInfo.position;
					if (idx === 0) {
						pathArr.push("M");
					} else {
						pathArr.push("L");
					}
					pathArr.push(position.x);
					pathArr.push(position.y);
				});

				if (pathArr.length > 0) {
					this.line = this.canvas.path(pathArr.join(" "));

					this.line.wijAttr(o.seriesStyle);
					this.line.wijAttr("fill", "none");

					this.animationSet.push(this.line);

					if (this.type === "line") {
						this.playAnimation();
					}
				}
			}
		}
	}

	/** @ignore */
	export class areaRender extends lineRender {
		area: RaphaelPath;

		constructor(element: JQuery, canvas: RaphaelPaper, type: string, options: WijSparklineRenderOptions) {
			super(element, canvas, type, options);
		}

		render() {
			var canvasHeight: number = this.options.canvasBounds.height,
				maxValue: number = this.maxValue,
				minValue: number = this.minValue,
				origin: number = this.options.origin,
				startX: string, endY: number, currentPathArr: string[], pathArr: any[];

			super.render();

			if (this.options.valueAxis) {
				if (origin === null || origin === undefined || typeof origin !== "number") {
					origin = (maxValue - minValue) / 2 + minValue;
				}
				endY = canvasHeight * (1 - (origin - this.minValue) / this.valRange);
			} else {
				endY = canvasHeight;
			}

			currentPathArr = Raphael.parsePathString(this.line.attr("path"));

			if (currentPathArr && currentPathArr.length > 0) {
				pathArr = [];
				$.each(currentPathArr, function (i, currentPath) {
					$.each(currentPath, function (j, val) {
						pathArr.push(val);
					});
					if (currentPath[0] === "M") {
						startX = currentPath[1];
					}
					if (i === currentPathArr.length - 1) {
						pathArr.push("V");
						pathArr.push(endY);
						pathArr.push("H");
						pathArr.push(startX);
						pathArr.push("Z");
					}
				});
			}

			if (pathArr.length > 0) {
				this.area = this.canvas.path(pathArr.join(" "));
				this.area.wijAttr(this.options.seriesStyle);
				this.area.wijAttr("stroke", "none");

				this.animationSet.push(this.area);

				this.playAnimation();
			}
		}
	}

	/** @ignore */
	export class columnRender extends sparklineRenderBase {
		columnWidth: number;
		animateBars: RaphaelElement[];

		constructor(element: JQuery, canvas: RaphaelPaper, type: string, options: WijSparklineRenderOptions) {
			super(element, canvas, type, options);
			this.animateBars = [];
		}

		render() {
			var self: columnRender = this,
				maxValue: number = self.maxValue,
				minValue: number = self.minValue,
				canvas: RaphaelPaper = self.canvas,
				o: WijSparklineRenderOptions = self.options,
				origin: number = o.origin,
				animation: WijSparklineAnimation = o.animation,
				canvasHeight: number = o.canvasBounds.height,
				normalStyle: WijSparklineSeriesStyle = o.seriesStyle,
				negStyle: RaphaelStyle = o.seriesStyle.negStyle,
				zeroStyle: RaphaelStyle = o.seriesStyle.zeroStyle,
				unitHeight: number, animationEnable: boolean, start: number;

			super.render();

			self.setRegionInfos();

			delete normalStyle["zeroStyle"];
			delete normalStyle["negStyle"];
			negStyle = $.extend({}, normalStyle, negStyle);
			zeroStyle = $.extend({}, normalStyle, zeroStyle);

			this.columnWidth = self.adjustColumnWidth();
			unitHeight = canvasHeight / self.valRange;
			animationEnable = animation && animation.enabled;

			if (o.valueAxis) {
				if (origin === null || origin === undefined || typeof origin !== "number") {
					origin = (maxValue - minValue) / 2 + minValue;
				}
				start = (maxValue - origin) * unitHeight;
			} else {
				start = canvasHeight;
			}

			if (self.regionInfos && self.regionInfos.length > 0) {
				$.each(self.regionInfos, (idx, regionInfo) => {
					var columnTop: number, columnHeight: number,
						seriesStyle: WijSparklineSeriesStyle,
						bar: RaphaelElement,
						actualTop: number, isZeroValue: boolean,
						columnLeft: number = regionInfo.position.x,
						val: number = regionInfo.value;

					if (o.valueAxis) {
						if (val >= origin) {
							isZeroValue = val === origin;
							actualTop = isZeroValue ? regionInfo.position.y - 2 * unitHeight : regionInfo.position.y;;
							columnTop = animationEnable ? start : regionInfo.position.y;
							columnHeight = isZeroValue ? 2 * unitHeight : (val - origin) * unitHeight;
							seriesStyle = isZeroValue ? <WijSparklineSeriesStyle>zeroStyle : normalStyle;
						} else {
							actualTop = columnTop = start;
							columnHeight = (origin - val) * unitHeight;
							seriesStyle = <WijSparklineSeriesStyle>negStyle;
						}
					} else {
						isZeroValue = val === minValue;
						actualTop = isZeroValue ? regionInfo.position.y - 2 * unitHeight : regionInfo.position.y;
						columnTop = start;
						columnHeight = isZeroValue ? 2 * unitHeight : canvasHeight - regionInfo.position.y;
						seriesStyle = isZeroValue ? <WijSparklineSeriesStyle>zeroStyle : normalStyle;
					}

					bar = canvas.rect(columnLeft, columnTop, this.columnWidth, columnHeight);
					bar.wijAttr(seriesStyle);

					bar["top"] = actualTop;
					bar["height"] = columnHeight;
					self.animateBars.push(bar);
				});

				self.playAnimation();
			}
		}

		playAnimation() {
			var o: WijSparklineRenderOptions = this.options,
				animation: WijSparklineAnimation = o.animation;

			if (animation && animation.enabled) {
				if (this.animateBars.length > 0) {
					$.each(this.animateBars, (idx, bar) => {
						var params = { height: bar["height"], y: bar["top"] };
						bar.wijAttr({ height: 0 });
						bar.stop().wijAnimate(params, animation.duration, animation.easing, null);
					});
				}
			} else {
				if (this.animateBars.length > 0) {
					$.each(this.animateBars, (idx, bar) => {
						var params = { height: bar["height"], y: bar["top"] };
						bar.wijAttr(params);
					});
				}
			}
		}

		adjustColumnWidth(): number {
			var o: WijSparklineRenderOptions = this.options,
				optionColumnWidth: number = o.columnWidth,
				canvasWidth: number = o.canvasBounds.width,
				columnSpacing: number = 2,
				calcColumnWidth: number;

			calcColumnWidth = canvasWidth / this.values.length - columnSpacing;

			return optionColumnWidth > calcColumnWidth ? calcColumnWidth : optionColumnWidth;
		}

		getColumnWidth(): number {
			return this.columnWidth;
		}
	}

	/** @ignore */
	export interface WijSparklineAnimation {
		enabled: boolean;
		duration: number;
		easing: string;
	}

	/** @ignore */
	export interface WijSparklineSeries {
		type: string;
		data: any[];
		bind: string;
		seriesStyle: WijSparklineSeriesStyle;
		columnWidth: number;
	}

	/** @ignore */
	export interface WijSparklineSeriesStyle extends RaphaelStyle {
		zeroStyle: RaphaelStyle;
		negStyle: RaphaelStyle;
	}

	/** 
	* The interface of WijSparklineOptions.
	*/
	interface WijSparklineOptions extends WidgetOptions {
		/**
		* All CSS classes used in widgets.
		* @ignore
		*/
		wijCSS?: WijSparklineCSS;
		/**
		* Specifies the type of the sparkline widget.
		* @remarks
		* The value of the type can be 'line', 'area' and 'column'.
		* And the default value is 'line'.
		*/
		type: string;
		/**
		* Creates an array of series objects that contain data values and labels to display in the chart.
		* @remarks
		* The series object contains following options
		* 1) type: Specifies the type of the sparkline widget.  The value for this option can be 'line', 'area' and 'column'.  And the default value is 'Line'.
		* 2) bind: Indicates that which property value is get from the object in the array if the data is set as a object array.
		* 3) seriesStyle: The specific style applies for current series of the sparkline widget for 
		*/
		seriesList: WijSparklineSeries[];
		/** 
		* Sets the width of the sparkline widget in pixels. 
		* @remarks
		* Note that this value overrides any value you may set in the <div> element that 
		* you use in the body of the HTML page
		* If you specify a width in the <div> element that is different from this value, 
		* the chart and its border go out of synch.
		* @type {?number}
		*/
		width?: number;
		/**
		* Sets the height of the sparkline widget in pixels. 
		* @remarks 
		* Note that this value overrides any value you may set in the <div> element that 
		* you use in the body of the HTML page. If you specify a height in the <div> element that 
		* is different from this value, the chart and its border go out of synch.
		* @type {?number}
		*/
		height?: number;
		/**
		* Sets the array to use as a source for data that you can bind to the sparkline widget.
		* @remarks
		* The array can be a simple number array.  The array can be a complex object array also.
		* If it is an object array, use the seriesList object's bind option to specify the data field to use.
		*/
		data: any[];
		/**
		* A value indicates that which property value is get from the object in the array if the data is set as a object array.
		*/
		bind: string;
		/**
		* Sets an array of style objects to use in rendering sparklines for each series in the sparkline widget.
		* @remarks
		* Each style object in the array applies to one series in your seriesList,
		* so you need specify only as many style objects as you have series objects in your seriesList. 
		*/
		seriesStyles: WijSparklineSeriesStyle[];
		/** 
		* The animation option defines the animation effect and controls other aspects of the widget's animation, 
		* such as duration and easing.
		*/
		animation: WijSparklineAnimation;
		/**
		* Axis line as option (off by default) used for identifying negative or positive values.
		* @remarks
		* This option just works with area type sparkline.
		* @type {bool}
		*/
		valueAxis: boolean;
		/**
		* Centers the value axis at the origin option setting value.
		* @remarks
		* This option just works when valueAxis is set to true.
		* @type {number}
		*/
		origin: number;
		/**
		* A value that indicates the minimum value of the sparkline.
		* @type {?number}
		*/
		min?: number;
		/**
		* A value that indicates the maximum value of the sparkline.
		* @type {?number}
		*/
		max?: number;
		/**
		* Set width for each column
		* @remarks
		* This option only works for column type sparkline.
		*/
		columnWidth: number;
		/**
		* A value which formats the value for tooltip shown.
		* @remarks
		* If the tooltipContent option is set, this option won't work.
		*/
		tooltipFormat: string;
		/**
		* A function which is used to get a value for the tooltip shown.
		*/
		tooltipContent: () => string;
		/** 
		* This event fires when the user moves the mouse pointer while it is over a sparkline.
		* @event
		* @param {Object} e The jQuery.Event object.
		* @param {IWijSparklineEventArgs} args The data with this event.
		*/
		mouseMove: (e: JQueryEventObject, args: IWijSparklineEventArgs) => void;
		/** 
		* This event fires when the user clicks the sparkline.
		* @event
		* @param {Object} e The jQuery.Event object.
		* @param {IWijSparklineEventArgs} args The data with this event.
		*/
		click: (e: JQueryEventObject, args: IWijSparklineEventArgs) => void;
		/**
		* Indicates whether the event handler of the sparkline widget is enable.
		*/
		disabled: boolean;
	}

	/** @ignore */
	interface WijSparklineCSS extends WijmoCSS {
		sparkline: string;
		canvasWrapper: string;
	}

	/** @ignore */
	export interface WijSparklineRenderOptions {
		data: any[];
		min: number;
		max: number;
		valueAxis: boolean;
		origin: number;
		animation: WijSparklineAnimation;
		bind: string;
		seriesStyle: WijSparklineSeriesStyle;
		columnWidth: number;
		canvasBounds: CanvasBounds;
	}

	class wijsparkline_options {
		/**
		* Selector option for auto self initialization. This option is internal.
		* @ignore
		*/
		initSelector = ":jqmData(role='wijsparkline')";
		/**
		* All CSS classes used in widgets.
		* @ignore
		*/
		wijCSS = {
			sparkline: "wijmo-wijsparkline",
			canvasWrapper: "wijmo-sparkline-canvas-wrapper"
		};
		/**
		* Specifies the type of the sparkline widget.
		* @remarks
		* The value of the type can be 'line', 'area' and 'column'.
		* And the default value is 'line'.
		*/
		type = "line";
		/**
		* Creates an array of series objects that contain data values and labels to display in the chart.
		* @remarks
		* The series object contains following options
		* 1) type: Specifies the type of the sparkline widget.  The value for this option can be 'line', 'area' and 'column'.  And the default value is 'Line'.
		* 2) bind: Indicates that which property value is get from the object in the array if the data is set as a object array.
		* 3) seriesStyle: The specific style applies for current series of the sparkline widget for 
		*/
		seriesList = [];
		/** 
		* Sets the width of the sparkline widget in pixels. 
		* @remarks
		* Note that this value overrides any value you may set in the <div> element that 
		* you use in the body of the HTML page
		* If you specify a width in the <div> element that is different from this value, 
		* the chart and its border go out of synch.
		* @type {?number}
		*/
		width = null;
		/**
		* Sets the height of the sparkline widget in pixels. 
		* @remarks 
		* Note that this value overrides any value you may set in the <div> element that 
		* you use in the body of the HTML page. If you specify a height in the <div> element that 
		* is different from this value, the chart and its border go out of synch.
		* @type {?number}
		*/
		height = null;
		/**
		* Sets the array to use as a source for data that you can bind to the sparkline widget.
		* @remarks
		* The array can be a simple number array.  The array can be a complex object array also.
		* If it is an object array, use the seriesList object's bind option to specify the data field to use.
		*/
		data = null;
		/**
		* A value indicates that which property value is get from the object in the array if the data is set as a object array.
		*/
		bind = null;
		/**
		* Sets an array of style objects to use in rendering sparklines for each series in the sparkline widget.
		* @remarks
		* Each style object in the array applies to one series in your seriesList,
		* so you need specify only as many style objects as you have series objects in your seriesList. 
		*/
		seriesStyles = [];
		/** 
		* The animation option defines the animation effect and controls other aspects of the widget's animation, 
		* such as duration and easing.
		*/
		animation = {
			/** 
			* A value that determines whether to show the animation.
			* Set this option to false in order to disable easing.
			*/
			enabled: true,
			/** 
			* A value that indicates the duration for the animation.
			*/
			duration: 2000,
			/** 
			* Sets the type of animation easing effect that users experience 
			* when the wijlinechart series is loaded to the page. 
			* For example, a user can have the wijlinechart series bounce several times as it loads.
			* @remarks Values available for the animation easing effect include the following:
			* easeInCubic ¨C Cubic easing in. Begins at zero velocity and then accelerates.
			* easeOutCubic ¨C Cubic easing in and out. Begins at full velocity and then decelerates to zero.
			* easeInOutCubic ¨C Begins at zero velocity, accelerates until halfway, and then decelerates to zero velocity again.
			* easeInBack ¨C Begins slowly and then accelerates.
			* easeOutBack ¨C Begins quickly and then decelerates.
			* easeOutElastic ¨C Begins at full velocity and then decelerates to zero.
			* easeOutBounce ¨C Begins quickly and then decelerates. The number of bounces is related to the duration, longer durations produce more bounces.
			*/
			easing: "easeInCubic"
		}
		/**
		* Axis line as option (off by default) used for identifying negative or positive values.
		* @remarks
		* This option just works with area type sparkline.
		* @type {bool}
		*/
		valueAxis: boolean = false;
		/**
		* Centers the value axis at the origin option setting value.
		* @remarks
		* This option just works when valueAxis is set to true.
		* @type {number}
		*/
		origin = null;
		/**
		* A value that indicates the minimum value of the sparkline.
		* @type {?number}
		*/
		min = null;
		/**
		* A value that indicates the maximum value of the sparkline.
		* @type {?number}
		*/
		max = null;
		/**
		* Set width for each column
		* @remarks
		* This option only works for column type sparkline.
		*/
		columnWidth = 10;
		/**
		* A value which formats the value for tooltip shown.
		* @remarks
		* If the tooltipContent option is set, this option won't work.
		*/
		tooltipFormat = null;
		/**
		* A function which is used to get a value for the tooltip shown.
		*/
		tooltipContent: () => string = null;
		/** 
		* This event fires when the user moves the mouse pointer while it is over a sparkline.
		* @event
		* @param {Object} e The jQuery.Event object.
		* @param {IWijSparklineEventArgs} args The data with this event.
		*/
		mouseMove: (e: JQueryEventObject, args: IWijSparklineEventArgs) => void = null;
		/** 
		* This event fires when the user clicks the sparkline.
		* @event
		* @param {Object} e The jQuery.Event object.
		* @param {IWijSparklineEventArgs} args The data with this event.
		*/
		click: (e: JQueryEventObject, args: IWijSparklineEventArgs) => void = null;
		/**
		* Indicates whether the event handler of the sparkline widget is enable.
		*/
		disabled: boolean = false;
	}

	wijsparkline.prototype.options = $.extend(true, {}, wijsparkline.prototype.options, new wijsparkline_options());

	wijsparkline.prototype.widgetEventPrefix = "wijsparkline";

	$.wijmo.registerWidget("wijsparkline", wijsparkline.prototype);

	/** @ignore */
	export interface IWijSparklineEventArgs {
		/** Current Region */
		currentRegions: RegionInfo[];
	}

	/** @ignore */
	export interface CanvasBounds {
		startX: number;
		width: number;
		startY: number;
		height: number;
	}

	/** @ignore */
	export interface RegionInfo {
		index: number;
		value: number;
		position: Point;
	}

	/** @ignore */
	export interface SeriesRegionInfo extends RegionInfo {
		type: string;
		bind: string;
		regionInfos: RegionInfo[];
	}

	/** @ignore */
	export interface MousePosition {
		left: number;
		originalLeft: number;
		top: number;
		originalTop: number;
	}
}

/** @ignore */
interface JQuery {
	wijsparkline: JQueryWidgetFunction;
}