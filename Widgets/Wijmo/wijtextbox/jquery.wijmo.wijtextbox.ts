/// <reference path="../Base/jquery.wijmo.widget.ts" />
/*globals jQuery */
/*
* Depends:
*  jquery.js
*  jquery.ui.js
*
*/
module wijmo.textbox {

	var $ = jQuery,
		widgetName = "wijtextbox";
	/** @widget */
	export class wijtextbox extends wijmoWidget {
		_create() {
			var self = this,
				e = self.element,
				wijCSS = self.options.wijCSS,
				allowedNodes = {
					'input': true,
					'textarea': true
				},
				allowedInputTypes = {
					'text': true,
					'password': true,
					'email': true,
					'url': true
				},
				nodeName = e.get(0).nodeName.toLowerCase(),
				type = e.attr("type");

			// enable touch support:
			if (window.wijmoApplyWijTouchUtilEvents) {
				$ = window.wijmoApplyWijTouchUtilEvents($);
			}

			if (!allowedNodes.hasOwnProperty(nodeName)) {
				return;
			}
			if ((nodeName === 'input') && type &&
				!allowedInputTypes.hasOwnProperty(type.toLowerCase())) {
				return;
			}

			e.addClass(wijCSS.wijtextbox)
				.addClass(wijCSS.widget)
				.addClass(wijCSS.stateDefault)
				.addClass(wijCSS.cornerAll);
			self.element.bind("mouseover." + self.widgetName, function () {
				e.addClass(wijCSS.stateHover);
			}).bind("mouseout." + self.widgetName, function () {
					e.removeClass(wijCSS.stateHover);
				}).bind("mousedown." + self.widgetName, function () {
					e.addClass(wijCSS.stateActive);
				}).bind("mouseup." + self.widgetName, function () {
					e.removeClass(wijCSS.stateActive);
				}).bind("focus." + self.widgetName, function () {
					e.addClass(wijCSS.stateFocus);
				}).bind("blur." + self.widgetName, function () {
					e.removeClass(wijCSS.stateFocus);
				});

			//for case 20899
			if (e.is(":disabled") || self._isDisabled()) {
				self._setOption("disabled", true);
			}

			super._create();
		}

		/**
		* Remove the functionality completely. This will return the element back to its pre-init state.
		*/
		destroy() {
			var self = this,
				wijCSS = self.options.wijCSS;
			self.element.removeClass(wijCSS.widget)
				.removeClass(wijCSS.stateDefault)
				.removeClass(wijCSS.cornerAll)
				.removeClass(wijCSS.stateHover)
				.removeClass(wijCSS.stateActive)
				.removeClass(wijCSS.stateFocus)
				.removeClass(wijCSS.wijtextbox)
				.unbind("." + self.widgetName);
			super.destroy();
		}

		_innerDisable() {
			super._innerDisable();
			this._toggleDisableInput(true);
		}

		_innerEnable() {
			super._innerEnable();
			this._toggleDisableInput(false);
		}

		_toggleDisableInput(disabled: boolean) {
			this.element.attr('disabled', disabled);
		}
	}

	class wijtextbox_options {
		/** Selector option for auto self initialization. This option is internal.
		* @ignore
		*/
		initSelector = ":jqmData(role='wijtextbox')";
		/** wijtextbox css, extend from $.wijmo.wijCSS
		* @ignore
		*/
		wijCSS = {
			wijtextbox: "wijmo-wijtextbox"
		};
		/** @ignore*/
		wijMobileCSS = {
			stateDefault: "ui-input-text"
		};
	};

	wijtextbox.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options, new wijtextbox_options());

	$.wijmo.registerWidget(widgetName, wijtextbox.prototype);
}
/** @ignore */
interface JQuery {
	wijtextbox: JQueryWidgetFunction;
}