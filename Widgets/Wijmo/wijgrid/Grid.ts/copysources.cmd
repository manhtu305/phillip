@echo off

if "%1"=="" (
   set ProjectDir=%cd%
) else (
   set ProjectDir=%1
)

pushd ..\..\..\tools\tsc
set wijtsc=%cd%\tsc.exe
popd

cd %ProjectDir%

rem --- build

pushd ..\..\
%wijtsc% Interop\angular.wijmo.ts
%wijtsc% Base\jquery.wijmo.widget.ts
%wijtsc% wijutil\jquery.wijmo.wijutil.ts
%wijtsc% wijpopup\jquery.wijmo.wijpopup.ts
%wijtsc% wijcalendar\jquery.wijmo.wijcalendar.ts
%wijtsc% wijsuperpanel\jquery.wijmo.wijsuperpanel.ts
%wijtsc% wijlist\jquery.wijmo.wijlist.ts
%wijtsc% wijtooltip\jquery.wijmo.wijtooltip.ts
%wijtsc% wijpopup\jquery.wijmo.wijpopup.ts
%wijtsc% wijinput\jquery.wijmo.wijcharex.ts
%wijtsc% wijinput\jquery.wijmo.wijstringinfo.ts
%wijtsc% wijinput\jquery.wijmo.wijinputcore.ts
%wijtsc% wijinput\jquery.wijmo.wijinpututility.ts
%wijtsc% wijinput\jquery.wijmo.wijinputdateformat.ts
%wijtsc% wijinput\jquery.wijmo.wijinputdateroller.ts
%wijtsc% wijinput\jquery.wijmo.wijinputdate.ts
%wijtsc% wijinput\jquery.wijmo.wijinputnumberformat.ts
%wijtsc% wijinput\jquery.wijmo.wijinputnumber.ts
%wijtsc% wijinput\jquery.wijmo.wijinputtextformat.ts
%wijtsc% wijinput\jquery.wijmo.wijinputtext.ts
%wijtsc% wijpager\jquery.wijmo.wijpager.ts
%wijtsc% ..\Samples\WidgetExplorer\samples\filter\js\jquery.wijmo.wijgrid-filter-ext.ts

pushd Data
call merge.bat
popd

%wijtsc% Data\wijmo.data.wijdatasource.ts

popd

rem --- recreate directories

set GridDir=%cd%

rd /S /Q themes
rd /S /Q js

md themes\aristo
md themes\wijmo
md js

rem --- copy

pushd ..\..\

xcopy /S /Y /Q ..\Themes\aristo %GridDir%\themes\aristo

for /f "tokens=*" %%a in ('dir /b /s /ad %cd%') do (
  xcopy /Y /Q %%a\*.js %GridDir%\js /EXCLUDE:%GridDir%\exclude.list
  xcopy /Y /Q %%a\*.css %GridDir%\themes\wijmo
)

xcopy /S /Y /Q ..\Samples\WidgetExplorer\samples\filter\js\jquery.wijmo.wijgrid-filter-ext.js %GridDir%\js

popd

@echo on