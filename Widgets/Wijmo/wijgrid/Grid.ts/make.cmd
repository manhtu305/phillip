@echo off

pushd ..\..\..\tools\tsc
set wijtsc=%cd%\tsc.exe
popd

del /F /Q ..\jquery.wijmo.wijgrid.js

pushd Grid
%wijtsc% wijgrid.ts sketchTable.ts c1basefield.ts c1field.ts c1band.ts c1groupedfield.ts c1buttonfield.ts c1commandbutton.ts bands_traversing.ts settingsManager.ts dataViewWrapper.ts grouper.ts groupHelper.ts merger.ts misc.ts filteroperators.ts htmlTableAccessor.ts cellInfo.ts baseView.ts flatView.ts fixedView.ts selection.ts uiSelection.ts rowAccessor.ts cellEditorHelper.ts cellFormatterHelper.ts uiResizer.ts uiDragndrop.ts cellStyleFormatterHelper.ts rowStyleFormatterHelper.ts tally.ts uiFrozener.ts columnsGenerator.ts uiVirtualScroller.ts renderBoundsCollection.ts hierarchyBuilder.ts
copy /Y ..\copyright.txt /B + wijgrid.js /B + sketchTable.js /B + c1basefield.js /B + c1field.js /B + c1band.js /B + c1groupedfield.js /B + c1buttonfield.js /B + c1commandbutton.js /B + bands_traversing.js /B + settingsManager.js /B + dataViewWrapper.js /B + grouper.js /B + groupHelper.js /B + merger.js /B + misc.js /B + filteroperators.js /B + htmlTableAccessor.js /B + cellInfo.js /B + baseView.js /B + flatView.js /B + fixedView.js /B + selection.js /B + uiSelection.js /B + rowAccessor.js /B + cellEditorHelper.js /B + cellFormatterHelper.js /B + uiResizer.js /B + uiDragndrop.js /B + cellStyleFormatterHelper.js /B + rowStyleFormatterHelper.js /B + tally.js /B + uiFrozener.js /B + columnsGenerator.js /B + uiVirtualScroller.js /B + renderBoundsCollection.js /B + hierarchyBuilder.js /B ..\..\jquery.wijmo.wijgrid.js
del *.js *.map
popd


pushd data
%wijtsc% converters.ts wijmo.data.filtering.extended.ts dataViewAdapter.ts koDataView.ts
copy /Y ..\..\jquery.wijmo.wijgrid.js/B + converters.js /B + wijmo.data.filtering.extended.js /B + dataViewAdapter.js /B + koDataView.js /B ..\..\jquery.wijmo.wijgrid.js
del *.js *.map
popd