/// <reference path="../../../data/src/dataView.ts"/>
/// <reference path="../../../data/src/arrayDataView.ts"/>

declare var ko;

module wijmo.grid {

	/** @ignore */
	export class koDataView extends wijmo.data.ArrayDataView {
		public static validSource(source: any): boolean {
			return !!(ko && ko.isObservable(source) && $.isFunction(source.slice)); // is there is a better way to detect?
		}

		private mObservableArray;
		private mPlainArray;
		private mRefreshPlainValue: boolean;

		constructor(observableArray: any) {
			this.mObservableArray = observableArray;

			var sourceArray = ko.utils.unwrapObservable(this.mObservableArray);

			super(sourceArray, null);
		}

		refresh(shape?: wijmo.data.IShape, local = false): JQueryDeferred<any> {
			this.sourceArray = ko.utils.unwrapObservable(this.mObservableArray);
			this.mRefreshPlainValue = true;

			// ** #53562: ensure that pageIndex is within [0; pageCount) range
			var pageSize = shape && shape.pageSize !== undefined
				? shape.pageSize
				: this.pageSize();

			var pageIndex = shape && shape.pageIndex !== undefined
				? shape.pageIndex
				: this.pageIndex();

			if (pageSize > 0 && pageIndex > 0) {
				var pageCount = data.util.pageCount(this.sourceArray.length, pageSize);

				if (pageIndex >= pageCount) {
					pageIndex = Math.max(0, pageCount - 1);

					if (!shape) {
						shape = {};
					}

					shape.pageIndex = pageIndex;
				}
			}
			// ** #53562: ensure that pageIndex is within[0; pageCount) range

			return super.refresh.apply(this, [shape, local]);
		}

		getPlainSource() {
			if (this.mRefreshPlainValue) {
				this.mRefreshPlainValue = false;

				this.mPlainArray = ko.toJS(this.sourceArray);
			}

			return this.mPlainArray;
		}

		dispose() {
			this.mObservableArray = null;
			this.mPlainArray = null;

			super.dispose.apply(this, arguments);
		}

		remove(item?: any): boolean {
			var entry = this._resolve(item, false);

			if (entry) {
				this.mObservableArray.remove(entry.item);
				return true;
			}

			return false;
		}
	}

	//wijmo.data.registerDataViewFactory(observableArray => {
	//	if (wijmo.grid.koDataView.validSource(observableArray)) {
	//		return new koDataView(observableArray);
	//	}
	//});
}