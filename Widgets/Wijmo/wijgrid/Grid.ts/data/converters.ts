/// <reference path="../../../Data/src/core.ts"/>
/// <reference path="../../../Data/src/util.ts"/>
/// <reference path="../../../Data/src/errors.ts"/>
/// <reference path="../../../external/declarations/globalize.d.ts"/> 

module wijmo.data {
	var $ = jQuery,
		glob = Globalize;

	/** @ignore */
	export interface IParser {
		parse(value, culture?, format?: string, nullString?: string, convertEmptyStringToNull?: boolean): any;
		toStr(value, culture?, format?: string, nullString?: string, convertEmptyStringToNull?: boolean): string;
	}

	/** @ignore */
	export interface IConvertOptions {
		ignoreError?: boolean;
		nullString?: string;
		parser?: IParser;
		format: string;
		culture? ;
	}

	/** @ignore */
	export function convert(val, fromType, toType, options?: wijmo.data.IConvertOptions) {
		var origValue = val;
		options = <IConvertOptions> $.extend({
			nullString: "",
			format: ""
		}, options);

		function getParser(type) {
			options.parser = options.parser || defaultParsers[type];
			if (!options.parser && val != null) errors.noParser(type);
			return options.parser;
		}

		fromType = fromType || val != null && typeof val;
		toType = toType || fromType;
		if (!toType) return val;

		if (toType == "string") {
			getParser(fromType);
			if (!options.parser) return val;
			return options.parser.toStr(val, options.culture, options.format, options.nullString, true);
		}
		getParser(toType);
		if (!options.parser) return val;

		val = options.parser.parse(val, options.culture, options.format, options.nullString, true);

		if (isNaN(val) && val != null && util.isNumeric(val)) {
			if (options.ignoreError) return origValue;
			errors.cantConvert(toType, origValue);
		}
		return val;
	}

	/** @ignore */
	export var defaultParsers = {
		string: <IParser>{
			// string -> string
			parse: function (value, culture, format, nullString, convertEmptyStringToNull) {
				switch (value) {
					case null:
						return null;

					case nullString:
						if (convertEmptyStringToNull) {
							return null;
						}

					case undefined:
					case "&nbsp":
						return "";

					default:
						return "" + value;
				}
			},

			// string -> string
			toStr: function (value, culture, format, nullString, convertEmptyStringToNull) {
				if (value === null && convertEmptyStringToNull) {
					return nullString;
				}
				return "" + value;
			}
		},

		number: {
			// string/number -> number
			parse: function (value, culture, format, nullString, convertEmptyStringToNull) {
				var type = typeof (value);

				if (type === "number") {
					return isNaN(value)
						? NaN
						: value;
				}

				if ((!value && value !== 0) || (value === "&nbsp;") || (value === nullString && convertEmptyStringToNull)) {
					return null;
				}

				return glob.parseFloat(value, 10, culture/*.name*/);
			},

			// number -> string
			toStr: function (value, culture, format, nullString, convertEmptyStringToNull) {
				if (value === null && convertEmptyStringToNull) {
					return nullString;
				}

				return glob.format(value, format || "n", culture/*.name*/);
			}
		},

		currency: {
			// string/number -> number
			parse: function (value, culture, format, nullString, convertEmptyStringToNull) {
				var type = typeof (value);

				if (type === "number") {
					return isNaN(value)
						? NaN
						: value;
				}

				if ((!value && value !== 0) || (value === "&nbsp;") || (value === nullString && convertEmptyStringToNull)) {
					return null;
				}

				if (type === "string") {
					value = value.replace(culture.numberFormat.currency.symbol, "");
				}

				return glob.parseFloat(value, 10, culture/*.name*/);
			},

			// number -> string (currency)
			toStr: function (value, culture, format, nullString, convertEmptyStringToNull) {
				if (value === null && convertEmptyStringToNull) {
					return nullString;
				}

				return glob.format(value, format || "c", culture/*.name*/);
			}
		},

		datetime: {
			// string/datetime -> datetime
			parse: function (value, culture, format, nullString, convertEmptyStringToNull) {
				var match;

				if (value instanceof Date) {
					return value;
				}

				if (!value || (value === "&nbsp;") || (value === nullString && convertEmptyStringToNull)) {
					return null;
				}

				match = /^\/Date\((\d+)\)\/$/.exec(value);
				if (match) {
					return new Date(parseInt(match[1], 10));
				}

				var date: any = glob.parseDate(value, format || "d", culture /*.name*/);

				if (date == null || isNaN(date)) {
					date = Date.parse(value);
					date = isNaN(date) ? <any>NaN : new Date(date);
				}

				return date;
			},

			// datetime -> string
			toStr: function (value, culture, format, nullString, convertEmptyStringToNull) {
				if (value === null && convertEmptyStringToNull) {
					return nullString;
				}

				return glob.format(value, format || "d", culture /*.name*/);
			}
		},

		boolean: {
			// string/boolean -> boolean
			parse: function (value, culture, format, nullString, convertEmptyStringToNull) {
				var valType = typeof (value);

				if (valType === "number") {
					return value != 0;
				}

				if (valType === "boolean") {
					return value;
				}

				if (valType === "string") {
					value = $.trim(value);
				}

				if (!value || (value === "&nbsp;") || (value === nullString && convertEmptyStringToNull)) {
					return null;
				}

				switch (value.toLowerCase()) {
					case "true":
						return true;

					case "false":
						return false;
				}

				return NaN;
			},

			// boolean -> string
			toStr: function (value, culture, format, nullString, convertEmptyStringToNull) {
				if (value === null && convertEmptyStringToNull) {
					return nullString;
				}

				return (value) ? "true" : "false";
			}
		}
	};

	function checkGlob(func) {
		return function () {
			if (!glob) {
				util.logError(errors.noGlobalize.create().message);
			}
			return func.apply(this, arguments);
		};
	}

	$.each(defaultParsers, (_, parser: IParser) => {
		parser.parse = parser.parse && checkGlob(parser.parse);
		parser.toStr = parser.toStr && checkGlob(parser.toStr);
	});
}
