﻿/// <reference path="../../../Data/src/arrayDataView.ts"/>
/// <reference path="wijmo.data.filtering.extended.ts"/>

module wijmo.grid {
	var $ = jQuery;
	import wijdata = wijmo.data;
	import filterExt = wijmo.data.filtering.extended;

	// in fact implements IDataView in run-time, but not at compile-time
	/** @ignore */
	export class GridDataView {
		filter: wijmo.data.IMutableObservable;

		constructor(public real: wijmo.data.IDataView) {
			this._overrideFilter();
			this._makeMemberProxies();
		}

		_lastSetFilter;
		_updatingFilter = false;
		_overrideFilter() {
			this.filter = wijdata.observable(this.real.filter());

			this.filter.subscribe(newValue => {
				if (this.real.filter() !== newValue && !this._updatingFilter) {
					this.refresh({ filter: newValue });
				}
			});

			this.real.filter.subscribe(newValue => {
				// Do not update this.filter if it is the same filter we've assigned to the underlying dataView.
				// Otherwise we may end up replacing a property filter with a function
				if (newValue !== this._lastSetFilter) {
					this.filter(newValue);
				}
			});
		}

		refresh(shape?: wijmo.data.IShape, local = false) {
			shape = $.extend({}, shape);
			var origFilter = shape.filter;
			shape.filter = this._coerceFilter(shape.filter);
			this._lastSetFilter = shape.filter;

			this._updatingFilter = true;
			try {
				if ($.isFunction(shape.filter) && !$.isFunction(origFilter)) {
					// it became a function. It means that the filter was complex
					this.filter(origFilter);
				} else {
					this.filter(shape.filter);
				}
			}
			finally {
				this._updatingFilter = false;
			}

			return this.real.refresh(shape, local);
		}

		_makeMemberProxies() {
			// make proxy methods for those that are not defined manually
			wijdata.util.each(this.real, (key: string, value) => {
				if (!$.isFunction(value) || this[key] || key.charAt(0) === "_") return;

				this[key] = $.isFunction(value.subscribe)
				? value
				: () => value.apply(this.real, arguments);
			});
		}

		static create(dataView: wijmo.data.IDataView) {
			return <wijdata.IDataView> <any> new GridDataView(dataView);
		}

		_convertComplexPropertyFilterToExtendedFilterFormat(filter) {
			var result = [];
			$.each(filter, function (prop, condList) {
				if (!$.isArray(condList)) {
					condList = [condList];
				} else {
					var connective = wijdata.util.isString(condList[0]) && condList[0].toLowerCase();
					if (connective === filterExt.Connective.AND || connective === filterExt.Connective.OR) {
						result.push(condList);
						return;
					}
				}

				var normCondList = [];
				$.each(condList, function (_, cond) {
					var normCond: any = wijdata.filtering.normalizeCondition(cond);
					if (normCond) {
						normCond.property = prop;
						normCondList.push(normCond);
					}
				});

				if (normCondList.length > 0) {
					result.push(normCondList);
				}
			});

			return result.length > 0 ? result : null;
		}
		_coerceFilter(filter) {
			if ($.isArray(filter)) {
				// assume extended
				return filterExt.compile(filter).func;
			} else if (!$.isPlainObject(filter)) {
				return filter;
			}

			filter = $.extend(true, {}, filter);

			var simpleFilter = {};
			$.each(filter, (prop, cond: any[]) => {
				if (!$.isArray(cond)) {
					cond = [cond];
				}

				if (simpleFilter) {
					var possibleConnective = wijdata.util.isString(cond[0]) && cond[0].toLowerCase();
					if (cond.length == 1 || cond.length == 2 && (possibleConnective === "and" || possibleConnective === "or")) {
						simpleFilter[prop] = cond[cond.length - 1];
						return;
					}
				}

				filter[prop] = cond;
				simpleFilter = null;
			});

			if (simpleFilter) {
				return simpleFilter;
			}

			var extendedFilter = this._convertComplexPropertyFilterToExtendedFilterFormat(filter);
			return filterExt.compile(extendedFilter).func;
		}
	}

	/** @ignore */
	export class GridLocalDataDataView extends GridDataView {
		constructor(public real: wijmo.data.IDataView) {
			super(real);
		}

		static create(dataView: wijmo.data.IDataView) {
			return <wijdata.IDataView> <any> new GridLocalDataDataView(dataView);
		}

		_unsafeReplace(index: number, newItem: any) {
			(<any>this.real).sourceArray[index] = newItem;
			(<any>this.real).local[index] = newItem;
		}

		_unsafeSplice(index: number, count: number, item?: any) {
			if (arguments.length === 2) {
				(<any>this.real).sourceArray.splice(index, count);
				(<any>this.real).local.splice(index, count);
			} else {
				(<any>this.real).sourceArray.splice(index, count, item);
				(<any>this.real).local.splice(index, count, item);
			}
		}

		_unsafePush(item: any) {
			(<any>this.real).sourceArray.push(item);
			(<any>this.real).local.push(item);
		}
	}
}