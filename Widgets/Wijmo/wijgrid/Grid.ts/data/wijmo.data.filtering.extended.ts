﻿/// <reference path="../../../Data/src/arrayDataView.ts"/>

/** Provides compilation of the Extended Filtering Format
  *
  * @remarks
  * Some examples of extended filter format:
  *   [{ property: "name", value: "John" }, { property: "age", operator: "<", value: 10 }]
  *   ["or", { property: "name", value: "John" }, { property: "age", operator: "<", value: 10 }]
  *   ["and", 
  *      ["or", { property: "name", value: "John" }, { property: "name", operator: "BeginsWith", value: "A" } ], 
  *      { property: "age", operator: "<", value: 10 }]
  *   ]
  */

/** @ignore */
module wijmo.data.filtering.extended {
	/** @ignore */
	export var Connective = {
		AND: "and",
		OR: "or"
	};

	interface IPropertyPredicate extends IFilterDescriptor {
		property: string;
	}

	function normalizeFilter(filter: any[]): any[] {
		function norm(filter: any[]) {
			var result = [];
			if (filter.length === 0) return result;

			var connective = Connective.AND;
			util.each(filter, (i, cond) => {
				if (i == 0 && util.isString(cond)) {
					var lowerConnective = cond.toLowerCase();
					if (lowerConnective == Connective.AND || lowerConnective == Connective.OR) {
						connective = lowerConnective;
						return;
					}
				}

				if ($.isArray(cond)) {
					cond = norm(cond);
					if (!cond) return;
					if (cond[0] === connective || cond.length == 2) {
						cond.shift();
						result = result.concat(cond);
						return;
					}
				} else {
					var predicate = <IPropertyPredicate> normalizeCondition(cond);
					if (!predicate) return;
					predicate.property = cond.property;
					cond = predicate;
				}

				result.push(cond);
			});

			if (result.length == 0) {
				return null;
			} else {
				result.unshift(connective);
				return result;
			}
		}
		return norm(filter);
	}
	function compilAsExtended(extendedFilter: any[]) {
		if (!$.isArray(extendedFilter)) {
			return null;
		}

		var result: ICompiledFilter = {
			isEmpty: true,
			original: extendedFilter,
			normalized: normalizeFilter(extendedFilter),
			func: null
		};
		if (result.normalized == null) {
			result.func = x => true;
		} else {
			result.isEmpty = false;
			result.func = x => {
				function check(filter) {
					var isAnd = filter[0] === Connective.AND,
						checker = isAnd ? util.every : util.some;
					return checker(filter, (cond, i) => {
						if (i === 0) return isAnd;
						if ($.isArray(cond)) {
							return check(cond);
						} else {
							var value = util.getProperty(x, cond.property);
							return cond.op.apply(value, cond.value);
						}
					});
				}
				return check(result.normalized);
			};
		}
		return result;
	}

	/** @ignore */
	export function compile(filter): ICompiledFilter {
		return compilAsExtended(filter) || filtering.compile(filter);
	}
}
