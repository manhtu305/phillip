/// <reference path="interfaces.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />
/// <reference path="rowAccessor.ts" />
/// <reference path="filterOperators.ts" />
/// <reference path="htmlTableAccessor.ts" />

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class baseView {
		public _wijgrid: wijgrid;
		public _bounds: IRenderBounds;

		private _bodyRowsAccessor: wijmo.grid.rowAccessor;
		private _headerRowsAccessor: wijmo.grid.rowAccessor;
		private _rowsAccessor: wijmo.grid.rowAccessor;
		private mIsRendered = false;
		private mIsLiveUpdate = false;
		private mCurrentRenderableColumns: renderableColumnsCollection;
		private mDetailsToInstantiate: Function[] = [];
		static ROWS_LOAD_INITIAL: number = 100; // load first n row for speed up : task 313255 Investigate the performance of C1GridView
        static INITIAL_ROWS_LOAD_DELAY: number = 50; 

		private _sizesAdjCache = {
			th: 0,
			col: 0,
			subTable: 0
		};

		constructor(wijgrid: wijgrid, renderBounds: IRenderBounds) {
			if (!wijgrid) {
				throw "'wijgrid' must be specified";
			}

			this._wijgrid = wijgrid;
			this._bounds = renderBounds;

			this._wijgrid.element.addClass(wijmo.grid.wijgrid.CSS.table).addClass(this._wijgrid.options.wijCSS.wijgridTable);
		}

		dispose() {
			this.toggleDOMSelection(true);
			this._wijgrid.element.removeClass(wijmo.grid.wijgrid.CSS.table).removeClass(this._wijgrid.options.wijCSS.wijgridTable);
		}

		currentRenderableColumns(value?: renderableColumnsCollection): renderableColumnsCollection {
			if (arguments.length) {
				this.mCurrentRenderableColumns = value;
			}

			return this.mCurrentRenderableColumns;
		}

		ensureDisabledState() {
			var disabledClass = "wijmo-wijgrid" + "-disabled " + this._wijgrid.options.wijCSS.stateDisabled,
				disabled = this._wijgrid.options.disabled,
				self = this;

			$.each(this.subTables(), function (key, table: htmlTableAccessor) {
				if (table) {
					var $table = $(table.element());

					if (disabled) {
						$table.addClass(disabledClass);
						self._wijgrid._setAttr($table, "aria-disabled", true);
					}
					else {
						$table.removeClass(disabledClass);
						self._wijgrid._setAttr($table, "aria-disabled", false);
					}
				}
			});
		}

		//ensureWidth(index: number, value, oldValue) {
		//	this._setColumnWidth(index, value);
		//}

		ensureHeight(rowIndex?: number) {
		}

		getScrollValue(): IScrollingState {
			return null;
		}

		getVisibleAreaBounds(client?: boolean): IElementBounds {
			throw "not implemented";
		}

		getVisibleContentAreaBounds(): IElementBounds {
			throw "not implemented";
		}

		getFixedAreaVisibleBounds() {
			throw "not implemented";
		}

		isLiveUpdate(): boolean {
			return this.mIsLiveUpdate;
		}

		isRendered(): boolean {
			return this.mIsRendered;
		}

		render(live: boolean) {
			this.mIsLiveUpdate = live;

			if (!live) {
				this._provideRealColumnsWidth();
			}

			this._ensureRenderBounds();
			this._ensureRenderHBounds();

			this._preRender();

			this._renderContent();

			this._postRender();

			this.mIsLiveUpdate = false;

			this._ensureRenderHBounds(); // reset after mIsLiveUpdate is changed
		}

		toggleDOMSelection(enable) {
			$.each(this.subTables(), function (index, table: htmlTableAccessor) {
				(new wijmo.grid.domSelection(<any>table.element())).toggleSelection(enable);
			});

			(new wijmo.grid.domSelection(<any>this._wijgrid.mOuterDiv)).toggleSelection(enable);
		}

		updateSplits(scrollValue: IScrollingState, rowsToAdjust?: cellRange) {
			throw "not implemented";
		}

		updateSplitsLive() {
			throw "not implemented";
		}

		getInlineTotalWidth(): string {
			throw "not implemented";
		}

		// public **

		// ** DOMTable abstraction

		// ** rows accessors
		bodyRows(): wijmo.grid.rowAccessor {
			if (!this._bodyRowsAccessor) {
				if (!this.isRendered()) {
					throw "not rendered yet";
				}

				this._bodyRowsAccessor = new wijmo.grid.rowAccessor(this, wijmo.grid.rowScope.body, 0, 0);
			}

			return this._bodyRowsAccessor;
		}

		filterRow(): IRowObj {
			if (this._wijgrid.options.showFilter) {
				if (!this.isRendered()) {
					throw "not rendered yet";
				}

				var accessor = new wijmo.grid.rowAccessor(this, wijmo.grid.rowScope.head, 0, 0);
				return accessor.item(accessor.length() - 1); // filter is the last row in the tHead section
			}

			return null;
		}

		footerRow(): IRowObj {
			if (this._wijgrid.options.showFooter) {
				if (!this.isRendered()) {
					throw "not rendered yet";
				}

				var accessor = new wijmo.grid.rowAccessor(this, wijmo.grid.rowScope.foot, 0, 0);
				return accessor.item(0);
			}
		}

		headerRows(): wijmo.grid.rowAccessor {
			var bottomOffset;

			if (!this._headerRowsAccessor) {
				if (!this.isRendered()) {
					throw "not rendered yet";
				}

				bottomOffset = this._wijgrid.options.showFilter ? 1 : 0;
				this._headerRowsAccessor = new wijmo.grid.rowAccessor(this, wijmo.grid.rowScope.head, 0, bottomOffset);
			}

			return this._headerRowsAccessor;
		}

		rows(): wijmo.grid.rowAccessor {
			if (!this._rowsAccessor) {
				if (!this.isRendered()) {
					throw "not rendered yet";
				}

				this._rowsAccessor = new wijmo.grid.rowAccessor(this, wijmo.grid.rowScope.table, 0, 0);
			}

			return this._rowsAccessor;
		}

		// rows accessors **

		focusableElement(): JQuery {
			return this._wijgrid.mOuterDiv;
		}

		forEachColumnCell(columnIndex: number, callback, param: any): boolean {
			throw "not implemented";
		}

		forEachRowCell(rowIndex: number, callback, param: any): boolean {
			throw "not implemented";
		}

		// important: only body cells can be virtualized
		getAbsoluteCellInfo(domCell: HTMLTableCellElement, virtualize: boolean): wijmo.grid.cellInfo {
			throw "not implemented";
		}

		getAbsoluteCellIndex(domCell: HTMLTableCellElement): number {
			throw "not implemented";
		}

		getAbsoluteRowIndex(domRow: HTMLTableRowElement): number {
			throw "not implemented";
		}

		getCell(absColIdx: number, absRowIdx: number): HTMLTableCellElement {
			throw "not implemented";
		}

		getColumnIndex(domCell: HTMLTableCellElement): number {
			throw "not implemented";
		}

		getHeaderCell(absColIdx: number): HTMLTableHeaderCellElement {
			throw "not implemented";
		}

		// [col, col]
		getJoinedCols(columnIndex: number): HTMLTableColElement[] {
			throw "not implemented";
		}

		// [row, row]
		getJoinedRows(rowIndex: number, rowScope: wijmo.grid.rowScope): IRowObj {
			throw "not implemented";
		}

		// [table, table, offset:number]
		getJoinedTables(byColumn: boolean, index: number): any[] {
			throw "not implemented";
		}

		subTables(): wijmo.grid.htmlTableAccessor[] {
			throw "not implemented";
		}

		// DOMTable abstraction **

		// ** private abstract

		_getMappedScrollMode() {
			var scrollMode = this._wijgrid._lgGetScrollMode(),
				vScrollBarVisibility = "auto",
				hScrollBarVisibility = "auto";

			switch (scrollMode) {
				case "horizontal":
					vScrollBarVisibility = "hidden";
					hScrollBarVisibility = "visible";
					break;

				case "vertical":
					vScrollBarVisibility = "visible";
					hScrollBarVisibility = "hidden";
					break;

				case "both":
					vScrollBarVisibility = "visible";
					hScrollBarVisibility = "visible";
					break;
			}

			return { vScrollBarVisibility: vScrollBarVisibility, hScrollBarVisibility: hScrollBarVisibility };
		}

		// ** rendering
		_postRender() {
			this.mIsRendered = true;

			if (this.isLiveUpdate()) {
				this.updateSplitsLive();
				return;
			}

			this.ensureDisabledState();

			// ** cache some values to speedup sizes manipulation (using IE especially) **

			// reset
			this._sizesAdjCache.col = 0;
			this._sizesAdjCache.th = 0;
			this._sizesAdjCache.subTable = 0;

			// set a new values
			var leaves = this._wijgrid._visibleLeaves();
			if (leaves.length > 0) {
				// note: we assume that the margins, paddings and borders are common to all of the th\ col elements.
				var th = this.getHeaderCell(0),
					cols = this.getJoinedCols(0);

				if (th) {
					this._sizesAdjCache.th = $(th).leftBorderWidth() + $(th).rightBorderWidth();
				}

				if (cols && cols.length) {
					this._sizesAdjCache.col = $(cols[0]).leftBorderWidth() + $(cols[0]).rightBorderWidth();
				}
			}

			var subTable = this.subTables()[0];
			this._sizesAdjCache.subTable = $(subTable.element()).leftBorderWidth() + $(subTable.element()).rightBorderWidth();
		}

		_preRender() {
			this.mIsRendered = false;

			var renderedIndex = 0,
				leaves = this._wijgrid._renderedLeaves();

			// re-calculate rendered indicies
			$.each(this._wijgrid._leaves(), (i, column) => {
				if (column._isRendered()) {
					column.options._renderedIndex = renderedIndex++;
					leaves.push(column);
				} else {
					column.options._renderedIndex = -1;
				}
			});
		}

		_addDetailInstantiator(value: Function) {
			this.mDetailsToInstantiate.push(value);
		}

		_ensureRenderBounds() {
			var dataRange = this._wijgrid._getDataCellsRange(dataRowsRangeMode.sketch);

			// render all items of the sketchTable
			this._bounds.start = 0;
			this._bounds.end = dataRange.bottomRight().rowIndex();
		}

		_ensureRenderHBounds() {
			// render all columns

			var renderableColumns = this._wijgrid._renderableColumnsRange();

			renderableColumns.clear();
			renderableColumns.add(0, this._wijgrid._leaves().length - 1);

			this.currentRenderableColumns(renderableColumns);
		}

		_provideRealColumnsWidth() {
			var leaves = this._wijgrid._visibleLeaves(),
				outerDiv = this._wijgrid.mOuterDiv,
				hvScrolling = this._wijgrid._allowHVirtualScrolling();

			$.each(leaves, (index, column: c1basefield) => {
				var width = column.options.width;

				if (!width && (width !== 0) && hvScrolling) {
					width = this._wijgrid.options.scrollingSettings.virtualizationSettings.columnWidth;
				}

				if (width || (width === 0)) {
					width = wijmo.grid.isPercentage(width)
					? outerDiv.width() * parseFloat(width) / 100 //convert percent to value
					: parseFloat(width);

					column.options._realWidth = width;
				}
			});
		}

		_renderContent() {
			this._renderCOLS();
			this._renderHeader();

			if (this._wijgrid.options.showFilter) {
				this._renderFilter();
			}

			this.mDetailsToInstantiate = [];

			this._renderBody();

			for (var i = 0; i < this.mDetailsToInstantiate.length; i++) {
				this.mDetailsToInstantiate[i]();
			}

			if (this._wijgrid.options.showFooter) {
				this._renderFooter();
			}
		}

		_renderCOLS() {
			var leaves = this._wijgrid._leaves();
			this.currentRenderableColumns().forEachIndex((i) => {
				var leaf = leaves[i], domCol, visLeavesIdx;

				if (leaf._visible()) {
					visLeavesIdx = leaf.options._visLeavesIdx;
					domCol = this._createCol(leaf, visLeavesIdx);
					this._appendCol(domCol, leaf, visLeavesIdx);
				}
			});
		}

		_renderHeader() {
			var self = this, cht = self._wijgrid._columnsHeadersTable(), thX, row, thead, meta, hdr, cell, attr, column;

			if (cht && cht.length) {
				thX = -self._wijgrid._virtualLeaves().length; // skip virtual leaves

				for (var i = 0, chtRowLength = cht.length; i < chtRowLength; i++) {
					row = self._insertEmptyRow(rowType.header, renderState.rendering, i, -1, -1, -1, -1, undefined, {});
					thead = self._wijgrid._tHead();
					meta = thead && wijmo.grid.dataViewWrapper.getMetadata(thead[row.sectionRowIndex]);// rowInfo.sectionRowIndex == column._thY
					hdr = (meta && meta.rowAttributes) ? new SketchHeaderRow(meta.rowAttributes) : new SketchHeaderRow(undefined);

					for (var j = 0, chtCellLength = cht[i].length; j < chtCellLength; j++) {
						cell = cht[i][j];
						column = cell.column || undefined;

						if (column) {

							// provide cell attributes, that were read from the original header cell at initialization stage
							attr = (meta && meta.cellsAttributes && meta.cellsAttributes[thX]) ? meta.cellsAttributes[thX] : {};
							$.extend(attr, { colSpan: cell.colSpan, rowSpan: cell.rowSpan });
							thX++;
						}

						hdr.add(new HeaderCell(column, attr));
					}

					self._renderRow(row, null, hdr);
				}
			}
		}

		_renderFilter() {
			var rowInfo = this._insertEmptyRow(wijmo.grid.rowType.filter, wijmo.grid.renderState.rendering, -1, -1, -1, -1, -1, undefined, {});
			this._renderRow(rowInfo, this._wijgrid._leaves(), null);
		}

        _renderBody() {
            // render rows 
            var count = Math.max(this._bounds.end - this._bounds.start + 1, baseView.ROWS_LOAD_INITIAL);
            if (this._bounds.start >= 0 && this._bounds.end >= 0) {

                this._renderBodyPartial(this._bounds.start, count);
                var remainingCount = this._bounds.end - this._bounds.start - count + 1; //Fixed TFS-443329-case2
                if (remainingCount > 0) {
                    setTimeout(
                        this._renderBodyPartial(count, remainingCount)
                        , baseView.INITIAL_ROWS_LOAD_DELAY);
                }
            }
        }

        // startIndex : where loop start from
        // count : number of row need to loop
        _renderBodyPartial(startIndex: number, count: number) {
            var self = this,
                virtualDataItemIndexBase = 0,
                sketch = this._wijgrid.mSketchTable,
                dataRowIndex = startIndex - 1,
                rs = wijmo.grid.renderState,
                leaves = this._wijgrid._leaves(),
                dataOffset = this._wijgrid.mDataOffset, // >= 0 when server-side virtual scrolling is used.
                cnt = startIndex;

            this._wijgrid._renderableRowsRange().forEachIndex(startIndex, count, function (idx) {
                var sketchRowIndex = idx - dataOffset,
                    sketchRow = sketch.row(sketchRowIndex);
                if (sketchRow) {
                    var isDataRow = sketchRow.isDataRow(),
                        groupKey = (<SketchGroupRow>sketchRow).groupByValue,
                        rowInfo = self._insertEmptyRow(sketchRow.rowType,
                            rs.rendering,
                            cnt++,
                            isDataRow ? ++dataRowIndex : -1,
                            sketchRow.dataItemIndex(),
                            sketchRow.dataItemIndex(virtualDataItemIndexBase),
                            sketchRowIndex,
                            groupKey,
                            sketchRow.extInfo);

                    self._renderRow(rowInfo, leaves, sketchRow);
                }
            });
        }

		_renderFooter() {
			var rowInfo = this._insertEmptyRow(wijmo.grid.rowType.footer, wijmo.grid.renderState.rendering, -1, -1, -1, -1, -1, undefined, {});
			this._renderRow(rowInfo, this._wijgrid._leaves(), null);
		}

		_insertEmptyRow(rowType: wijmo.grid.rowType, renderState: wijmo.grid.renderState, sectionRowIndex: number, dataRowIndex: number, dataItemIndex: number, virtualDataItemIndex: number, sketchRowIndex: number, groupByValue: any, extInfo: IRowInfoExt): IRowInfo {
			var domRow = this._wijgrid._onViewInsertEmptyRow.apply(this._wijgrid, arguments),
				domRowArr = this._insertRow(rowType, sectionRowIndex, domRow);

			if (renderState === undefined) {
				renderState = wijmo.grid.renderState.rendering;
			}

			return this._createRowInfo(domRowArr, rowType, renderState, sectionRowIndex, dataRowIndex, dataItemIndex, virtualDataItemIndex, sketchRowIndex, groupByValue, extInfo);
		}

		_createEmptyCell(rowInfo: IRowInfo, dataCellIndex: number, column: c1basefield): JQuery {
			var rt = wijmo.grid.rowType,
				domCell = this._wijgrid._onViewCreateEmptyCell.apply(this._wijgrid, arguments);

			return this._createCell(rowInfo.type, domCell);
		}

		// override

		_insertRow(rowType: wijmo.grid.rowType, sectionRowIndex: number, domRow?: HTMLTableRowElement /* optional, used by c1gridview to clone rows of the original table */)
			: HTMLTableRowElement[] {
			throw "not implemented";
		}

		_createCell(rowType: wijmo.grid.rowType, domCell?: HTMLTableCellElement /* optional, used by c1gridview to clone cells of the original table */)
			: JQuery {
			var rt = wijmo.grid.rowType,
				innerContainer: HTMLElement,
				wrap: string;

			if (!domCell) {
				if (rowType === rt.header) {
					domCell = <HTMLTableHeaderCellElement>document.createElement("th");
				} else {
					domCell = <HTMLTableCellElement>document.createElement("td");
				}
			} else {
				wrap = $(domCell).css("white-space");
			}

			if (rowType !== rt.filter) {
				// * analogue of domCell.wrapInner("<div class=\"wijmo-wijgrid-innercell\"></div>")

				innerContainer = document.createElement("div");
				innerContainer.className = wijmo.grid.wijgrid.CSS.cellContainer + " " + this._wijgrid.options.wijCSS.wijgridCellContainer;

				if (domCell.firstChild) { // move nodes from domCell to innerContainer
					while (domCell.firstChild) {
						innerContainer.appendChild(domCell.firstChild);
					}
				}

				if (wrap && wrap !== "normal") {
					$(innerContainer).css("white-space", wrap);
					$(domCell).css("white-space", "");
				}

				domCell.appendChild(innerContainer);
			}

			return $(domCell);
		}

		_appendCell(rowInfo: IRowInfo, cellIndex: number, $cell: JQuery) {
			throw "not implemented";
		}

		_createCol(column: c1basefield, visibleIdx: number): HTMLTableColElement[] {
			throw "not implemented";
		}

		_appendCol(domCol: HTMLTableColElement[], column: c1basefield, visibleIdx: number) {
			throw "not implemented";
		}

		_renderRow(rowInfo: IRowInfo, leaves: c1basefield[], item: SketchRow) {
			var $rt = wijmo.grid.rowType,
				rowAttr, rowStyle;

			switch (rowInfo.type & ~($rt.dataAlt | $rt.dataHeader)) {
				case $rt.filter:
					this._renderFilterRow(rowInfo, leaves);
					break;

				case $rt.footer:
					this._renderFooterRow(rowInfo, leaves);
					break;

				case $rt.header:
					this._renderHeaderRow(rowInfo, <SketchHeaderRow>item);
					rowAttr = item.attr();
					rowStyle = item.style();
					break;

				case $rt.data:
					this._renderDataRow(rowInfo, leaves, item);
					rowAttr = item.attr();
					rowStyle = item.style();
					break;

				case $rt.detail: // wijgrid
				case $rt.data | $rt.dataDetail: // c1gridview
				case $rt.emptyDataRow:
				case $rt.groupHeader:
				case $rt.groupFooter:
					this._renderSpannedRow(rowInfo, leaves, item);
					rowAttr = item.attr();
					rowStyle = item.style();
					break;

				default:
					throw "unknown rowType";
			}

			this._rowRendered(rowInfo, rowAttr, rowStyle);
		}

		_renderCell(rowInfo: IRowInfo, cellIndex: number, value, useHtml: boolean, leaf: c1basefield, state: wijmo.grid.renderState, attr?, style?) {
			var $cell = this._createEmptyCell(rowInfo, (<IColumn>leaf.options).dataIndex, leaf);
			var $container = (rowInfo.type === wijmo.grid.rowType.filter)
				? $cell
				: $($cell[0].firstChild); // $cell.children("div"); -- slow

			this._appendCell(rowInfo, cellIndex, $cell);

			if (useHtml) {
				$container.html(value !== undefined ? value : "&nbsp;");
			} else {
				this._wijgrid.mCellFormatter.format($cell, cellIndex, $container, leaf, value, rowInfo);
			}

			this._cellRendered(rowInfo, $cell, $container, cellIndex, leaf, state, attr, style);
		}

		_renderDataRow(rowInfo: IRowInfo, leaves: c1basefield[], sketchRow: wijmo.grid.SketchRow) {
			//for (var i = 0, len = leaves.length; i < len; i++) {
			this.currentRenderableColumns().forEachIndex((i) => {
				var leaf = leaves[i];

				if (leaf._visible()) {
					var cell = sketchRow.cell(i),
						attr = cell.attr(),
						style = cell.style(),
						value,
						state = rowInfo.state;

					if (leaf instanceof c1field) {
						if (!cell || !cell.visible()) {
							//continue;
							return;
						}

						value = this._wijgrid.toStr(<IColumn>leaf.options, (<ValueCell>cell).value);
						attr = cell.attr();
						style = cell.style();
					} else {
						// unbound column
						value = null;
					}

					if ((<IColumn>leaf.options).readOnly) {
						state &= ~wijmo.grid.renderState.editing;
					}

					this._renderCell(rowInfo, i, value, false, leaf, state, attr, style);
				}
			});
		}

		_renderFilterRow(rowInfo: IRowInfo, leaves: c1basefield[]) {
			//for (var i = 0, len = leaves.length; i < len; i++) {
			this.currentRenderableColumns().forEachIndex((i) => {
				var leaf = leaves[i];

				if (leaf._visible()) {
					this._renderCell(rowInfo, i, wijmo.grid.filterHelper.getSingleValue((<IColumn>leaf.options).filterValue), false, leaf, rowInfo.state);
				}
			});
		}

		_renderFooterRow(rowInfo: IRowInfo, leaves: c1basefield[]) {
			//for (var i = 0, len = leaves.length; i < len; i++) {
			this.currentRenderableColumns().forEachIndex((i) => {
				var leaf = leaves[i];

				if (leaf._visible()) {
					this._renderCell(rowInfo, i, "", false, leaves[i], rowInfo.state);
				}
			});
		}

		_renderHeaderRow(rowInfo: IRowInfo, item: SketchHeaderRow) {
            var self = this, thX = 0; // vislble cells only

			this.currentRenderableColumns().forEachIndex((i) => {
				var cell = <HeaderCell>item.cell(i), column = cell.column, instance;

				if (column) {
					column._thX2 = i;

					if (column._parentVis) {
						column._thX = thX++;
						column._thY = rowInfo.sectionRowIndex;

						instance = self._wijgrid._findInstance(column);

						self._renderCell(rowInfo, i, column.headerText, false, instance, rowInfo.state, cell.attr());
					}
				}

			});
		}

		_renderSpannedRow(rowInfo: IRowInfo, leaves: c1basefield[], sketchRow: SketchRow) {
			var len = Math.min(leaves.length, sketchRow.cellCount());

			for (var i = 0; i < len; i++) {
				var leaf = leaves[i];
				var useHTML = rowInfo.type === wijmo.grid.rowType.emptyDataRow; // ignore cellFormatter and set content directly
				var cell = <HtmlCell> sketchRow.cell(i);

				this._renderCell(rowInfo, i, cell.html, useHTML, leaf, rowInfo.state, cell.attr(), cell.style());
			}
		}

		_cellRendered(row: IRowInfo, cell: JQuery, container: JQuery, cellIndex: number, column: c1basefield, cellState: wijmo.grid.renderState, attr, style) {
			this._wijgrid.mCellStyleFormatter.format(cell, cellIndex, column, row, cellState, attr, style);

			this._changeCellRenderState(cell, cellState, false);

			this._wijgrid._onViewCellRendered(cell, container, row, cellIndex, column);

			column._cellRendered(cell, container, row);
		}

		_rowRendered(rowInfo: IRowInfo, rowAttr, rowStyle) {
			this._wijgrid.mRowStyleFormatter.format(rowInfo, rowAttr, rowStyle);

			// change renderState AND associate rowInfo object with DOMRow
			//this._changeRowRenderState(rowInfo, $.wijmo.wijgrid.renderState.rendering, false);
			rowInfo.state &= ~wijmo.grid.renderState.rendering;
			this._setRowInfo(rowInfo.$rows, rowInfo);

			this._wijgrid._onViewRowRendered(rowInfo, rowAttr, rowStyle);
		}

		_makeRowEditable(rowInfo: IRowInfo) {
			var leaves = this._wijgrid._visibleLeaves(),
				cellEditor = new wijmo.grid.cellEditorHelper();

			if (leaves) {
				for (var i = 0; i < leaves.length; i++) {
					var column = leaves[i];

					if ((column instanceof c1field) && !(<IColumn>column.options).readOnly) {
						var absRowIndex = this.getAbsoluteRowIndex(<HTMLTableRowElement>rowInfo.$rows[0]);
						var cell = this.getCell(i, absRowIndex);

						if (cell) { // can be null if row merging is used
							var $cell = $(cell);
							var cellInfo = this.getAbsoluteCellInfo(cell, true);
							this._changeCellRenderState($cell, wijmo.grid.renderState.editing, cellEditor.cellEditStart(this._wijgrid, cellInfo, null)); // add or remove editing state depends on cellEditStart result.
						}

						//var $cell = rowInfo.$rows.children("td, th").eq(i);
						//if ($cell.length) {
						//	var cellInfo = this.getAbsoluteCellInfo(<HTMLTableCellElement>$cell[0], true);
						//	this._changeCellRenderState($cell, wijmo.grid.renderState.editing, cellEditor.cellEditStart(this._wijgrid, cellInfo, null));
						//}
					}
				}
			}
		}

		_isBodyRow(rowInfo: IRowInfo): boolean {
			var $rt = wijmo.grid.rowType,
				type = rowInfo.type;

			return <any>((type & $rt.data) || (type === $rt.groupHeader) || (type === $rt.groupFooter) || (type === $rt.emptyDataRow) || (type === $rt.detail));
		}

		_changeRowRenderState(rowInfo: IRowInfo, state: wijmo.grid.renderState, combine: boolean): void {
			if (combine) { // combine
				rowInfo.state |= state;
			} else { // clear
				rowInfo.state &= ~state;
			}

			this._setRowInfo(rowInfo.$rows, rowInfo);
		}

		_changeCellRenderState($obj, state: wijmo.grid.renderState, combine: boolean): wijmo.grid.renderState {
			var $dp = wijmo.grid.dataPrefix,
				prefix = this._wijgrid.mDataPrefix,
				prevState = $dp($obj, prefix, "renderState");

			if (combine) { // combine
				state = prevState | state;
				$dp($obj, prefix, "renderState", state);
			} else { // clear
				state = prevState & ~state;
				$dp($obj, prefix, "renderState", state);
			}

			return state;
		}

		// rendering **

		// ** sizing

		_resetWidth() {
			var leaves = this._wijgrid._visibleLeaves();

			$.each(leaves, (index: number, column: c1basefield) => {
				this._setColumnWidth(column, null);
			});

			$.each(this.subTables(), (index, table: htmlTableAccessor) => {
				table.element().style.width = "100%";
			});
		}

		_adjustWidthArray(maxWidthArray: IColumnWidth[], minWidthArray: IColumnWidth[], expectedWidth: number, ensureColumnsPxWidth: boolean): IColumnWidth[] {
			var maxWidth = this._sumWidthArray(maxWidthArray),
				minWidth = this._sumWidthArray(minWidthArray),
				widthArray = [],
				adjustWidth,
				expandCount = 0,
				expandWidth, remainingWidth,
				bFirst = true;

			if (maxWidth <= expectedWidth) {
				$.extend(true, widthArray, maxWidthArray);
				if (maxWidth === expectedWidth || ensureColumnsPxWidth) {
					return widthArray;
				} else {
					adjustWidth = expectedWidth - maxWidth;
				}
			} else {
				$.extend(true, widthArray, minWidthArray);
				if (minWidth >= expectedWidth) {
					return widthArray;
				} else {
					adjustWidth = expectedWidth - minWidth;
				}
			}

			$.each(widthArray, function (index, colWidth) {
				if (!colWidth.real) {
					expandCount++;
				}
			});

			if (expandCount !== 0) {
				expandWidth = Math.floor(adjustWidth / expandCount);
				remainingWidth = adjustWidth - expandWidth * expandCount;
				$.each(widthArray, function (index, colWidth) {
					if (!colWidth.real) {
						colWidth.width += expandWidth;
						if (bFirst) {
							colWidth.width += remainingWidth;
							bFirst = false;
						}
					}
				});
			}

			return widthArray;
		}

		_getColumnWidth(index: number): IColumnWidth {
			var column = this._wijgrid._visibleLeaves()[index];

			if (column instanceof c1rowheaderfield) {
				return { width: c1rowheaderfield.COLUMN_WIDTH, real: true };
			} else if (column.options._realWidth !== undefined && column._strictWidthMode()) {
				return { width: column.options._realWidth, real: true, realPercent: wijmo.grid.isPercentage(column.options.width) };
			} else {
				var maxW = 0,
					joinedTables = this.getJoinedTables(true, index),
					relIdx = joinedTables[2];

				for (var i = 0; i < 2; i++) {
					var table: wijmo.grid.htmlTableAccessor = joinedTables[i];

					if (table !== null) {
						var rows = table.element().rows,
							row: HTMLTableRowElement = null,
							len = rows.length;

						if (len > 0) {
							// try to find a row which doesn't contains a spanned cells
							for (var j = len - 1; j >= 0; j--) {
								if ((<HTMLTableRowElement>rows[j]).cells.length === table.width()) {
									row = <HTMLTableRowElement>rows[j];

									if (row.style.display !== "none") {
										break; // break on first visible row, skip invisible rows
									}
								}
							}

							if (row) { // can be invisible
								var cell = row.cells[relIdx];
								maxW = Math.max(maxW, $(cell).outerWidth());
							}
						}
					}
				}

				return { width: maxW, real: false };
			}
		}

		// px: null to reset.
		_setColumnWidth(column: c1basefield, px: number) {
			if (!column || !column._isRendered()) {
				return;
			}

			var visibleIndex = column.options._visLeavesIdx,
				th = this.getHeaderCell(visibleIndex),
				cols = this.getJoinedCols(visibleIndex),
				value: number;

			if (px || px === 0) {
				if (th) {
					value = px - this._sizesAdjCache.th;
					if (value < 0) {
						value = 0;
					}

					th.style.width = value + "px";
				}

				$.each(cols, (i, col: HTMLTableColElement) => {
					if (col) {
						value = px - this._sizesAdjCache.col;
						if (value < 0) {
							value = 0;
						}

						col.style.width = value + "px";
					}
				});
			} else {
				if (th) {
					th.style.width = "";
				}

				$.each(cols, (index, col) => {
					if (col) {
						col.style.width = "";
					}
				});
			}
		}

		_setTableWidth(subTables: JQuery[], expectedWidth: number, expandColumnWidth: number, expandColumn: c1basefield): void {
			var after, diff,
				self = this;

			$.each(subTables, function (index, table: JQuery) {
				//table.css("table-layout", "fixed").setOutWidth(expectedWidth); // very slow in IE9

				table[0].style.tableLayout = "fixed";
				table[0].style.width = (expectedWidth - self._sizesAdjCache.subTable) + "px";

			});

			after = subTables[0].outerWidth();
			diff = after - expectedWidth;
			if (diff !== 0) {
				this._setColumnWidth(expandColumn, expandColumnWidth - diff);
			}
		}

		_sumWidthArray(widthArray: IColumnWidth[], startIndex?: number, endIndex?: number): number {
			var result = 0,
				leaves = this._wijgrid._visibleLeaves();

			$.each(widthArray, (index, colWidth) => {
				if (startIndex !== undefined && endIndex !== undefined
					&& (index < startIndex || index > endIndex
					|| (startIndex >= 0 && !leaves[index]._isRendered())) // getting width of the rendered columns only
					) {
					return true;
				}
				result += colWidth.width;
			});

			return result;
		}

		// sizing **

		// private abstract **

		_clearBody() {
			$.each(this.subTables(), function (key, table: htmlTableAccessor) {
				table.clearSection(2);
			});
		}

		_rebuildOffsets() {
			$.each(this.subTables(), function (key, table: htmlTableAccessor) {
				table.rebuildOffsets();
			});
		}

		_removeBodyRow(sectionRowIndex: number, changeOffsets: boolean = true) {
			var rows = this._wijgrid._rows(),
				len: number;

			if ((sectionRowIndex >= 0) && (sectionRowIndex < (len = rows.length()))) {
				// remove DOMRows
				var rowInfo = this._getRowInfo(rows.item(sectionRowIndex), false),
					absRowIdx = this.getAbsoluteRowIndex(<HTMLTableRowElement>rowInfo.$rows[0]);

				rowInfo.$rows.remove();

				// ** update offsets
				if (changeOffsets) {
					var joinedTables = this.getJoinedTables(false, absRowIdx),
						ta: htmlTableAccessor;

					if (ta = joinedTables[0]) {
						ta.removeOffset(joinedTables[2]);
					}

					if (ta = joinedTables[1]) {
						ta.removeOffset(joinedTables[2]);
					}
				}
				// update offsets **
			}
		}

		_insertBodyRow(sketchRow: SketchRow, sectionRowIndex: number, dataItemIndex: number, virtualDataItemIndex: number, sketchRowIndex: number): IRowInfo {
			var leaves = this._wijgrid._leaves(),
				$rt = wijmo.grid.rowType,
				view = this._wijgrid._view(),
				rows = this._wijgrid._rows(),
				len = rows.length(),
				isDataRow = sketchRow.isDataRow(),
				rowInfo: IRowInfo,
				absRowIdx: number;

			if (sectionRowIndex < 0 || sectionRowIndex >= len || (!sectionRowIndex && sectionRowIndex !== 0)) {
				sectionRowIndex = len; // append
			}

			rowInfo = this._insertEmptyRow(sketchRow.rowType,
				sketchRow.renderState,
				sectionRowIndex,
				-1, // TODO: dataRowIndex
				dataItemIndex,
				virtualDataItemIndex,
				sketchRowIndex,
				(<SketchGroupRow>sketchRow).groupByValue,
				sketchRow.extInfo);

			this._renderRow(rowInfo, leaves, sketchRow);

			// ** update offsets
			absRowIdx = this.getAbsoluteRowIndex(<HTMLTableRowElement>rowInfo.$rows[0]);

			var joinedTables = this.getJoinedTables(false, absRowIdx),
				ta: htmlTableAccessor;

			if (ta = joinedTables[0]) {
				ta.insertOffset(joinedTables[2]);
			}

			if (ta = joinedTables[1]) {
				ta.insertOffset(joinedTables[2]);
			}
			// update offsets **

			return rowInfo;
		}

		_setRowInfo(obj: JQuery, row: IRowInfo) {
			if (!obj) {
				return;
			}

			var tmpRows = row.$rows,
				tmpData = row.data;

			row.$rows = undefined;
			row.data = undefined;

			wijmo.grid.dataPrefix(obj, this._wijgrid.mDataPrefix, "rowInfo", row);

			row.$rows = tmpRows;
			row.data = tmpData;
		}

		_getRowInfo(rowObj: IRowObj, retrieveDataItem: boolean = true): IRowInfo {
			var wijgrid = this._wijgrid,
				$rows = rowObj[1] ? $(rowObj) : $(rowObj[0]),
				rowInfo: IRowInfo = wijmo.grid.dataPrefix($rows, wijgrid.mDataPrefix, "rowInfo");

			// add $rows property
			rowInfo.$rows = $rows;

			// set data property
			if (retrieveDataItem && (rowInfo.dataItemIndex >= 0) && (rowInfo.type & wijmo.grid.rowType.data)) {
				try {
					rowInfo.data = wijgrid._getDataItem(rowInfo.dataItemIndex);
				}
				catch (ex) {
					rowInfo.data = null; // underlying data item was removed?
				}
			}

			return rowInfo;
		}

		_getRowInfoBySketchRowIndex(sketchIndex: number, retrieveDataItem: boolean = true): IRowInfo {
			if (sketchIndex >= 0) {
				var renderedRowIndex = this._isRowRendered(sketchIndex);

				if (renderedRowIndex >= 0) {
					var rowObj = this.bodyRows().item(renderedRowIndex);

					if (rowObj) {
						return this._getRowInfo(rowObj, retrieveDataItem);
					}
				} else {
					// detached row
					var sketchRow = this._wijgrid.mSketchTable.row(sketchIndex),
						rowInfo: IRowInfo = null;

					if (sketchRow) {
						rowInfo = sketchRow.getRowInfo();

						// set data property
						if (retrieveDataItem && (rowInfo.dataItemIndex >= 0) && sketchRow.isDataRow()) {
							try {
								rowInfo.data = this._wijgrid._getDataItem(rowInfo.dataItemIndex);
							}
							catch (ex) {
								rowInfo.data = null; // underlying data item was removed?
							}
						}
					}

					return rowInfo;
				}
			}

			return null;
		}

		_createRowInfo(row: HTMLTableRowElement[], type: wijmo.grid.rowType, state: wijmo.grid.renderState, sectionRowIndex: number, dataRowIndex: number, dataItemIndex: number, virtualDataItemIndex: number, sketchRowIndex: number, groupByValue: any, extInfo: IRowInfoExt): IRowInfo {
			var rowInfo: IRowInfo = {
				type: type,
				state: state,
				sectionRowIndex: sectionRowIndex,
				dataRowIndex: dataRowIndex,
				dataItemIndex: dataItemIndex,
				virtualDataItemIndex: virtualDataItemIndex,
				sketchRowIndex: sketchRowIndex,
				$rows: row ? $(row) : null,
				_extInfo: extInfo
			};

			if (groupByValue !== undefined) {
				rowInfo.groupByValue = groupByValue;
			}

			// set data property
			if ((dataItemIndex >= 0) && (type & wijmo.grid.rowType.data)) {
				rowInfo.data = this._wijgrid._getDataItem(dataItemIndex);
			}

			return rowInfo;
		}

		// returns tbody row index or -1
		_isRowRendered(sketchRowIndex: number): number {
			var visibleIndex = this._wijgrid._renderableRowsRange().getRenderedIndex(sketchRowIndex);

			if (visibleIndex >= 0) {
				if (visibleIndex >= this._bounds.start && visibleIndex <= this._bounds.end) {
					return visibleIndex - this._bounds.start;
				}
			}

			return -1;
		}
	}
}