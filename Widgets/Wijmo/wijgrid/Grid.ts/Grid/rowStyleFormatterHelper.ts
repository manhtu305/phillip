/// <reference path="interfaces.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class rowStyleFormatterHelper {
		private _wijgrid: wijgrid;

		constructor(wijgrid: wijgrid) {
			if (!wijgrid) {
				throw "invalid arguments";
			}

			this._wijgrid = wijgrid;
		}

		format(rowInfo: IRowInfo, rowAttr?, rowStyle?) {
			var $rs = wijmo.grid.renderState,
				$rt = wijmo.grid.rowType,
				state = rowInfo.state,
				args = rowInfo;

			if (state & $rs.rendering) {
				this._renderingStateFormatter(args, rowAttr, rowStyle);
			} else {
				this._currentStateFormatter(args, (state & $rs.current) !== 0);
				this._hoveredStateFormatter(args, (state & $rs.hovered) !== 0);
				this._selectedStateFormatter(args, (state & $rs.selected) !== 0);
			}

			if ($.isFunction(this._wijgrid.options.rowStyleFormatter)) {
				this._wijgrid.options.rowStyleFormatter(args);
			}

			//Due to the row height may be changed via rowStyleFormatter, we should store the height after rowStyleFormatter applied.
			if ((state & $rs.rendering) && args.$rows[0].style.height) {
				args.$rows.each(function () {
					$.data(this, "customHeight", this.style.height);
				});
			}
		}

		_groupFormatter(rowInfo: IRowInfo) {
			var rse = wijmo.grid.renderStateEx,
				extInfo = rowInfo._extInfo,
				defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._wijgrid.options.wijCSS;

			if (extInfo.state & rse.hidden) {
				rowInfo.$rows.css("display", "none");
				//rowInfo.$rows.attr("aria-hidden", true);
			} else {
				rowInfo.$rows.css("display", "");
				//rowInfo.$rows.removeAttr("aria-hidden");
			}

			if (rowInfo.type & wijmo.grid.rowType.groupHeader) {
				var gi = this._wijgrid._groupedLeaves()[rowInfo._extInfo.groupLevel - 1].options.groupInfo, //1-based
					expandedIcon = gi.expandedImageClass || wijCSS.iconArrowRightDown,
					collapsedIcon = gi.collapsedImageClass || wijCSS.iconArrowRight,
					//toggleBtn = rowInfo.$rows.children("td, th").eq(this._wijgrid._showRowHeader() ? 1 : 0).find("." + defCSS.groupToggleVisibilityButton);
					toggleBtn = wijmo.grid.rowAccessor.getCell$(rowInfo.$rows, this._wijgrid._showRowHeader() ? 1 : 0)
						.find("." + defCSS.groupToggleVisibilityButton);

				if (extInfo.state & rse.collapsed) {
					rowInfo.$rows.attribute("aria-expanded", false);

					rowInfo.$rows
						.removeClass(defCSS.groupHeaderRowExpanded + " " + wijCSS.wijgridGroupHeaderRowExpanded)
						.addClass(defCSS.groupHeaderRowCollapsed + " " + wijCSS.wijgridGroupHeaderRowCollapsed);

					toggleBtn
						.removeClass(expandedIcon)
						.addClass(collapsedIcon);
				} else {
					rowInfo.$rows.attribute("aria-expanded", true);

					rowInfo.$rows
						.removeClass(defCSS.groupHeaderRowCollapsed + " " + wijCSS.wijgridGroupHeaderRowCollapsed)
						.addClass(defCSS.groupHeaderRowExpanded + " " + wijCSS.wijgridGroupHeaderRowExpanded);

					toggleBtn
						.removeClass(collapsedIcon)
						.addClass(expandedIcon);
				}
			}
		}

		_masterFormatter(rowInfo: IRowInfo) {
			var defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._wijgrid.options.wijCSS,
				toggleBtn = rowInfo.$rows.find("." + defCSS.groupToggleVisibilityButton),
				expandedIcon = wijCSS.iconArrowRightDown,
				collapsedIcon = wijCSS.iconArrowRight;

			if (rowInfo._extInfo.state & renderStateEx.collapsed) {
				rowInfo.$rows.attribute("aria-expanded", false);

				toggleBtn
					.removeClass(expandedIcon)
					.addClass(collapsedIcon);
			} else {
				rowInfo.$rows.attribute("aria-expanded", true);

				toggleBtn
					.removeClass(collapsedIcon)
					.addClass(expandedIcon);
			}
		}

		// * private
		private _renderingStateFormatter(args: IRowInfo, rowAttr, rowStyle) {
			var defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._wijgrid.options.wijCSS,
				className: string,
				contentClass = defCSS.row + " " + wijCSS.wijgridRow + " " + wijCSS.content,
				$rt = wijmo.grid.rowType,
				rse = wijmo.grid.renderStateEx,
				key;

			$.each(args.$rows, function (index, value) {
				if ($(value).children().length > 0)
				{
					$(value).attr("role", "row");
				}
			});

			// copy attributes
			if (rowAttr) {
				for (key in rowAttr) {
					if (rowAttr.hasOwnProperty(key)) {
						if (key === "class") {
							args.$rows.addClass(rowAttr[key]);
						} else {
							args.$rows.attr(key, rowAttr[key]);
						}
					}
				}
			}

			// copy inline css
			if (rowStyle) {
				for (key in rowStyle) {
					if (rowStyle.hasOwnProperty(key)) {
						args.$rows.css(key, rowStyle[key]);
					}
				}
			}

			if (args._extInfo.groupLevel) {
				args.$rows.attribute("aria-level", args._extInfo.groupLevel);
			}

			// hide collapsed row
			if (args._extInfo.state & rse.hidden) {
				args.$rows.css("display", "none");
				// args.$rows.attr("aria-hidden", true);
			}

			switch (args.type & ~($rt.dataAlt | $rt.dataDetail | $rt.dataHeader)) { // clear modifiers
				case ($rt.header):
					className = defCSS.headerRow + " " + wijCSS.wijgridHeaderRow;
					break;

				case ($rt.data):
					className = contentClass + " " + defCSS.dataRow + " " + wijCSS.wijgridDataRow;

					if (args.type & $rt.dataAlt) {
						className += " " + defCSS.altRow + " " + wijCSS.wijgridAltRow;
					}

					break;

				case ($rt.emptyDataRow):
					className = contentClass + " " + defCSS.emptyDataRow + " " + wijCSS.wijgridEmptyDataRow;
					break;

				case ($rt.filter):
					className = defCSS.filterRow + " " + wijCSS.wijgridFilterRow;
					break;

				case ($rt.groupHeader):
					$(args.$rows.last()).attribute({
						"id": "GH" + args._extInfo.groupIndex + "-" + args._extInfo.groupLevel,
						"aria-expanded": ((args._extInfo.state & rse.collapsed) === 0)
					});

					className = contentClass + " " + defCSS.groupHeaderRow + " " + wijCSS.wijgridGroupHeaderRow + " ";

					className += (args._extInfo.state & rse.collapsed)
					? defCSS.groupHeaderRowCollapsed + " " + wijCSS.wijgridGroupHeaderRowCollapsed
					: defCSS.groupHeaderRowExpanded + " " + wijCSS.wijgridGroupHeaderRowExpanded;

					break;

				case ($rt.groupFooter):
					$(args.$rows.last()).attr("id", "GF" + args._extInfo.groupIndex + "-" + args._extInfo.groupLevel);

					className = contentClass + " " + defCSS.groupFooterRow + " " + wijCSS.wijgridGroupFooterRow;
					break;

				case ($rt.detail):
					className = contentClass + " " + defCSS.detailRow + " " + wijCSS.wijgridDetailRow;
					break;

				case ($rt.footer):
					className = defCSS.footerRow + " " + wijCSS.wijgridFooterRow + " " + wijCSS.stateDefault + " " + wijCSS.stateHighlight;
					break;

				default:
					throw wijmo.grid.stringFormat("unknown rowType: {0}", args.type);
			}

			args.$rows.addClass(className);
		}

		private _currentStateFormatter(args: IRowInfo, flag: boolean) {
			if (this._wijgrid._showRowHeader()) {
				var wijCSS = this._wijgrid.options.wijCSS,
					defCSS = wijmo.grid.wijgrid.CSS;

				// make deal with the row header cell
				if (flag) { // add formatting
					$((<HTMLTableRowElement>args.$rows[0]).cells[0])
						.addClass(wijCSS.stateActive + " " + defCSS.currentRowHeaderCell + " " + wijCSS.wijgridCurrentRowHeaderCell);
				} else { // remove formatting
					$((<HTMLTableRowElement>args.$rows[0]).cells[0])
						.removeClass(wijCSS.stateActive + " " + defCSS.currentRowHeaderCell + " " + wijCSS.wijgridCurrentRowHeaderCell);
				}
			}
		}

		private _hoveredStateFormatter(args: IRowInfo, flag: boolean) {
			var wijCSS = this._wijgrid.options.wijCSS;

			if (flag) { // add formatting
				args.$rows.addClass(wijCSS.stateDefault + " " + wijCSS.stateHover);
			} else {  // remove formatting
				args.$rows.removeClass(wijCSS.stateDefault + " " + wijCSS.stateHover);

				if (args.type & wijmo.grid.rowType.data) {
					args.$rows.addClass(wijCSS.wijgridDataRow); // bootsrap issue, both stateDefault and wijgridDataRow contains the same class "btn", restore it.
				}
			}
		}

		private _selectedStateFormatter(args: IRowInfo, flag: boolean) {
			if (flag) { // add formatting
			} else { // remove formatting
			}
		}
		// private *
	}
}
