/// <reference path="interfaces.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />

module wijmo.grid {


	var $ = jQuery;

	/** An object that represents a single cell. */
	export class cellInfo {
		static outsideValue: cellInfo = new cellInfo(-1, -1, null);

		private mIsPreparingForEditing: boolean = false;
		private mIsEdit: boolean = false;
		private _wijgrid: wijgrid;
		private _ci: number;
		private _ri: number;

		private _virtualize: boolean;

		/** Creates an object that represents a single cell. Normally you do not need to use this method.
		  * @example
		  * var cell = new wijmo.grid.cellInfo(0, 0, $("#demo").data("wijmo-wijgrid"));
		  * @param {Number} cellIndex The zero-based index of the required cell inside the corresponding row.
		  * @param {Number} rowIndex The zero-based index of the row that contains required cell.
		  * @param {Object} wijgrid The wijgrid instance.
		  * @returns {wijmo.grid.cellInfo} Object that represents a single cell.
		  */
		constructor(cellIndex: number, rowIndex: number, wijgrid: wijmo.grid.wijgrid = null, absolute: boolean = false, virtualize: boolean = true) {
			this._wijgrid = wijgrid;
			this._virtualize = virtualize;

			if (absolute) {
				this._ci = cellIndex - this._wijgrid._getDataToAbsOffset().x;
				this._ri = rowIndex - this._wijgrid._getDataToAbsOffset().y;

				if (this._virtualize) {
					if (this._ci >= 0) {
						this._ci = this._wijgrid._renderedLeaves()[this._ci].options._visLeavesIdx;
					}

					if (this._ri >= 0) {
						this._ri = this._wijgrid._renderableRowsRange().getAbsIndex(this._ri + this._wijgrid._viewPortBounds().start);
					}
				}
			} else {
				this._ci = cellIndex;
				this._ri = rowIndex;
			}
		}

		// public
		/** @ignore */
		cellIndexAbs(): number {
			var dataOffset = this._wijgrid._getDataToAbsOffset().x;

			var value = this._virtualize
				? this._wijgrid._visibleLeaves()[this._ci + dataOffset].options._renderedIndex
				: this._ci + dataOffset;

			return value;
		}

		/** @ignore */
		rowIndexAbs(): number {
			var value = this._virtualize
				? this._wijgrid._renderableRowsRange().getRenderedIndex(this._ri) - this._wijgrid._viewPortBounds().start
				: this._ri;

			value += this._wijgrid._getDataToAbsOffset().y;

			return value;
		}

		/** Gets the zero-based index of the cell in the row which it corresponds to.
		  * @example
		  * var index = cellInfoObj.cellIndex();
		  * @returns {Number} The zero-based index of the cell in the row which it corresponds to.
		  */
		cellIndex(): number;
		/** @ignore */
		cellIndex(value: number): number;
		/** @ignore */
		cellIndex(value?: number): number {
			if (!arguments.length) {
				return this._ci;
			}

			this._ci = value;
		}

		/** Gets the associated column object.
		  * @example
		  * var column = cellInfoObj.column();
		  * @returns {wijmo.grid.IColumn} The associated column object.
		  */
		column(): IColumn {
			var instance = this.columnInst();

			return instance
				? <IColumn>instance.options
				: null;
		}

		/** @ignore */
		columnInst(): c1basefield {
			if (this._wijgrid && this._isValid()) {
				var dataOffset = this._wijgrid._getDataToAbsOffset()
				return this._wijgrid._visibleLeaves()[this.cellIndex() + dataOffset.x];
			}

			return null;
		}

		/** Returns the jQuery object containing a cell content.
		  * @example
		  * var $container = cellInfoObj.container();
		  * @returns {Object} The jQuery object containing a cell content.
		  */
		container(): JQuery {
			var tableCell = this.tableCell(),
				$innerDiv;

			if (tableCell) {
				$innerDiv = $(tableCell).children("div." + wijmo.grid.wijgrid.CSS.cellContainer);
				if ($innerDiv) {
					return $innerDiv;
				}
			}

			return null;
		}

		/** Compares the current object with an object you have specified and indicates whether they are identical
		  * @example
		  * var isEqual = cellInfoObj1.isEqual(cellInfoObj2);
		  * @param {wijmo.grid.cellInfo} value The object to compare
		  * @returns {Boolean} True if the objects are identical, otherwise false.
		  */
		isEqual(value: wijmo.grid.cellInfo): boolean {
			return (value && (value.rowIndex() === this.rowIndex()) && (value.cellIndex() === this.cellIndex()));
		}

		/** Gets the accociated row's information.
		  * @example
		  * var row = cellInfoObj.row();
		  * @returns {wijmo.grid.IRowInfo} Information about associated row.
		  */
		row(): IRowInfo {
			var rowObj = null,
				result: IRowInfo = null;

			if (this._wijgrid) {
				if (this._virtualize) {
					result = this._wijgrid._view()._getRowInfoBySketchRowIndex(this.rowIndex());
				} else {
					rowObj = this._wijgrid._view().rows().item(this.rowIndexAbs());

					if (rowObj && rowObj.length) {
						result = this._wijgrid._view()._getRowInfo(rowObj);
					}
				}
			}

			return result;
		}

		/** Gets the zero-based index of the row containing the cell.
		  * @example
		  * var index = cellInfoObj.rowIndex();
		  * @returns {Number} The zero-based index of the row containing the cell.
		  */
		rowIndex(): number;
		/** @ignore */
		rowIndex(value: number): number;
		/** @ignore */
		rowIndex(value?: number): number {
			if (!arguments.length) {
				return this._ri;
			}

			this._ri = value;
		}

		/** Returns the table cell element corresponding to this object.
		  * @example
		  * var domCell = cellInfoObj.tableCell();
		  * @returns {HTMLTableCellElement} The table cell element corresponding to this object.
		  */
		tableCell(): HTMLTableCellElement {
			if (this._wijgrid && this._isValid()) {
				if (!this._virtualize || this._isRendered()) {
					return this._wijgrid._view().getCell(this.cellIndexAbs(), this.rowIndexAbs());
				}
			}

			return null;
		}

		/** Gets the underlying data value.
		  * @example
		  * var value = cellInfoObj.value();
		  * @remarks
		  * @returns {Object} The underlying data value.
		  */
		value(): any;
		/** Sets the underlying data vakue.
		  * @example
		  * cellInfoObj.value("value");
		  * @remarks
		  * An "invalid value" exception will be thrown by the setter if the value does not correspond to associated column data type.
		  * @param {Object} value The value to set.
		  */
		value(value: any): void;
		/** @ignore */
		value(value?: any/*opt*/): any {
			var column: IColumn,
				rowInfo: IRowInfo,
				colVal;

			if (this._wijgrid && this._isValid()) {
				rowInfo = this.row();

				if (rowInfo.type & wijmo.grid.rowType.data) {
					column = this.column();

					if (arguments.length === 0) { // getter
						colVal = this._wijgrid.mDataViewWrapper.getValue(rowInfo.data, column.dataKey);
						return this._wijgrid.parse(column, colVal);
					} else { // setter
						// validation
						value = this._wijgrid.parse(column, value);

						if ((value === null && column.valueRequired) || ((wijmo.grid.getDataType(column) !== "string") && isNaN(value))) {
							throw "invalid value";
						}

						// update dataView
						this._wijgrid.mDataViewWrapper.setValue(rowInfo.dataItemIndex, column.dataKey, value);

						// keep sketchTable values in sync (to avoid issues during virtual scrolling)
						var sketchRow = this._sketchRow();
						if (sketchRow) {
							var sketchCell = <wijmo.grid.ValueCell>sketchRow.cell(column._leavesIdx);
							sketchCell.value = value;
						}
					}
				}
			}
		}

		/** @ignore */
		toString(): string {
			return this.cellIndex() + ":" + this.rowIndex();
		}

		// internal

		_clip(range: cellInfoRange, absolute: boolean = false): boolean {
			var flag = false,
				val;

			if (absolute) {
				if (this.cellIndexAbs() < (val = range.topLeft().cellIndexAbs())) {
					flag = true;
					this._ci = range.topLeft().cellIndex();
				}

				if (this.cellIndexAbs() > (val = range.bottomRight().cellIndexAbs())) {
					flag = true;
					this._ci = range.bottomRight().cellIndex();
				}

				if (this.rowIndexAbs() < (val = range.topLeft().rowIndexAbs())) {
					flag = true;
					this._ri = range.topLeft().rowIndex();
				}

				if (this.rowIndexAbs() > (val = range.bottomRight().rowIndexAbs())) {
					flag = true;
					this._ri = range.bottomRight().rowIndex();
				}
			} else {
				if (this.cellIndex() < (val = range.topLeft().cellIndex())) {
					flag = true;
					this._ci = val;
				}

				if (this.cellIndex() > (val = range.bottomRight().cellIndex())) {
					flag = true;
					this._ci = val;
				}

				if (this.rowIndex() < (val = range.topLeft().rowIndex())) {
					flag = true;
					this._ri = val;
				}

				if (this.rowIndex() > (val = range.bottomRight().rowIndex())) {
					flag = true;
					this._ri = val;
				}
			}

			return flag;
		}

		_clone(): wijmo.grid.cellInfo {
			return new wijmo.grid.cellInfo(this.cellIndex(), this.rowIndex(), this._wijgrid, false, this._virtualize);
		}

		_isValid(): boolean {
			return this.cellIndex() >= 0 && this.rowIndex() >= 0;
		}

		_isRowRendered(): boolean {
			var view: baseView;

			if (this._wijgrid && (view = this._wijgrid._view()) && this._isValid()) {
				var bodyIndex = view._isRowRendered(this.rowIndex());
				return (bodyIndex >= 0);
			}

			return false;
		}

		_isRendered(): boolean {
			var view: baseView;

			if (this._wijgrid && (view = this._wijgrid._view()) && this._isValid()) {
				if (this.columnInst()._isRendered()) {
					var bodyIndex = view._isRowRendered(this.rowIndex());
					return (bodyIndex >= 0);
				}
			}

			return false;
		}

		_isPreparingForEditing(value?: boolean): boolean {
			if (arguments.length) {
				this.mIsPreparingForEditing = value;
			}

			return this.mIsPreparingForEditing;
		}

		_isEdit(value?: boolean): boolean {
			var tableCell = null,
				marker = wijmo.grid.wijgrid.CSS.editedCellMarker;

			if (this._isValid()) {
				try {
					tableCell = this.tableCell();
				} catch (e) { }
			}

			if (!arguments.length) {
				if (tableCell) {
					return $(tableCell).hasClass(marker);
				}

				return this.mIsEdit;
			} else {
				if (tableCell) {
					$(tableCell)[value ? "addClass" : "removeClass"](marker);
				}

				var grid = this._wijgrid;

				// add or remove the row header's icon
				if (grid && this._wijgrid._showRowHeader() && this._isRowRendered()) {
					var row = this.row(),
						container = $((<HTMLTableRowElement>row.$rows[0]).cells[0]).children("div." + wijmo.grid.wijgrid.CSS.cellContainer);

					if (value) {
						container.empty().append($("<div>&nbsp;</div>").addClass(grid.options.wijCSS.icon + " " + grid.options.wijCSS.iconPencil));
					} else {
						container.html("&nbsp;"); // remove icon-pencil
					}
				}

				this.mIsEdit = value;
			}
		}

		_setGridView(value: wijgrid) {
			this._wijgrid = value;
		}


		// gets associated row from the sketchTable
		_sketchRow(): SketchRow {
			var grid = this._wijgrid,
				row = this.row();

			if (grid && row && (row.sketchRowIndex >= 0)) {
				return grid.mSketchTable.row(row.sketchRowIndex); // .row(this.rowIndex())
			}

			return null;
		}

		// internal *

		// * private
		// private *
	}

	/** An object that specifies a range of cells determined by two cells. */
	export class cellInfoRange {
		_topLeft: cellInfo;
		_bottomRight: cellInfo;

		/** Creates an object that specifies a range of cells determined by two cells. Normally you do not need to use this method.
		  * @example
		  * var range = wijmo.grid.cellInfoRange(new wijmo.grid.cellInfo(0, 0), new wijmo.grid.cellInfo(0, 0));
		  * @param {wijmo.grid.cellInfo} topLeft Object that represents the top left cell of the range.
		  * @param {wijmo.grid.cellInfo} bottomRight Object that represents the bottom right cell of the range.
		  * @returns {wijmo.grid.cellInfoRange} Object that specifies a range of cells determined by two cells.
		  */
		constructor(topLeft: cellInfo, bottomRight: cellInfo) {
			if (!topLeft || !bottomRight) {
				throw "invalid arguments";
			}

			this._topLeft = topLeft._clone();
			this._bottomRight = bottomRight._clone();
		}

		/** Gets the object that represents the bottom right cell of the range.
		  * @example
		  * var cellInfoObj = range.bottomRight();
		  * @returns {wijmo.grid.cellInfo} The object that represents the bottom right cell of the range.
		  */
		bottomRight(): cellInfo {
			return this._bottomRight;
		}

		/** Compares the current range with a specified range and indicates whether they are identical.
		  * @example
		  * var isEqual = range1.isEqual(range2);
		  * @param {wijmo.grid.cellInfoRange} range Range to compare.
		  * @returns True if the ranges are identical, otherwise false.
		  */
		isEqual(range: cellInfoRange): boolean {
			return (range && this._topLeft.isEqual(range.topLeft()) && this._bottomRight.isEqual(range.bottomRight()));
		}

		/** Gets the object that represents the top left cell of the range.
		  * @example
		  * var cellInfoObj = range.topLeft();
		  * @returns {wijmo.grid.cellInfo} The object that represents the top left cell of the range.
		  */
		topLeft(): cellInfo {
			return this._topLeft;
		}

		/** @ignore */
		toString(): string {
			return this._topLeft.toString() + " - " + this._bottomRight.toString();
		}

		// public *

		// internal
		_isIntersect(range: cellInfoRange): boolean {
			var rangeH, thisH, rangeW, thisW;

			if (range) {
				rangeH = range.bottomRight().rowIndex() - range.topLeft().rowIndex() + 1;
				thisH = this._bottomRight.rowIndex() - this._topLeft.rowIndex() + 1;

				if ((range.topLeft().rowIndex() + rangeH) - this._topLeft.rowIndex() < rangeH + thisH) {
					rangeW = range.bottomRight().cellIndex() - range.topLeft().cellIndex() + 1;
					thisW = this._bottomRight.cellIndex() - this._topLeft.cellIndex() + 1;

					return ((range.topLeft().cellIndex() + rangeW) - this._topLeft.cellIndex() < rangeW + thisW);
				}
			}

			return false;
		}

		_isValid(): boolean {
			return this._topLeft._isValid() && this._bottomRight._isValid();
		}

		_clip(clipBy: cellInfoRange, absolute: boolean = false): boolean {
			var a = this._topLeft._clip(clipBy, absolute);
			var b = this._bottomRight._clip(clipBy, absolute);

			return a || b;
		}

		_clone(): cellInfoRange {
			return new cellInfoRange(this._topLeft._clone(), this._bottomRight._clone());
		}

		_containsCellInfo(info: cellInfo): boolean {
			return (info && info.cellIndex() >= this._topLeft.cellIndex() && info.cellIndex() <= this._bottomRight.cellIndex() &&
				info.rowIndex() >= this._topLeft.rowIndex() && info.rowIndex() <= this._bottomRight.rowIndex());
		}

		_containsCellRange(range: cellInfoRange): boolean {
			return (range && this._containsCellInfo(range.topLeft()) && this._containsCellInfo(range.bottomRight()));
		}

		_extend(mode: wijmo.grid.cellRangeExtendMode, borders: cellInfoRange): cellInfoRange {
			if (mode === wijmo.grid.cellRangeExtendMode.column) {
				this._topLeft.rowIndex(borders.topLeft().rowIndex());
				this._bottomRight.rowIndex(borders.bottomRight().rowIndex());
			} else {
				if (mode === wijmo.grid.cellRangeExtendMode.row) {
					this._topLeft.cellIndex(borders.topLeft().cellIndex());
					this._bottomRight.cellIndex(borders.bottomRight().cellIndex());
				}
			}

			return this;
		}

		_normalize() {
			var x0 = this._topLeft.cellIndex(),
				y0 = this._topLeft.rowIndex(),
				x1 = this._bottomRight.cellIndex(),
				y1 = this._bottomRight.rowIndex();

			this._topLeft.cellIndex(Math.min(x0, x1));
			this._topLeft.rowIndex(Math.min(y0, y1));

			this._bottomRight.cellIndex(Math.max(x0, x1));
			this._bottomRight.rowIndex(Math.max(y0, y1));
		}

		_height(): number {
			return this._bottomRight.rowIndex() - this._topLeft.rowIndex();
		}

		_width() {
			return this._bottomRight.cellIndex() - this._topLeft.cellIndex();
		}

		// internal *
	}

	// * compatibility: export members to the $.wijmo.wijgrid "namespace" *
	$.extend($.wijmo.wijgrid, {
		cellInfo: wijmo.grid.cellInfo,
		cellInfoRange: wijmo.grid.cellInfoRange
	});
}


