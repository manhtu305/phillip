/// <reference path="interfaces.ts" />
/// <reference path="c1commandbutton.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />

module wijmo.grid {


	var $ = jQuery;


	/** @ignore */
	export class cellFormatterHelper {
		private _div: HTMLDivElement;

		constructor() {
			this._div = <HTMLDivElement>document.createElement("div");
			this._div.style.display = "none";

			if (document && document.body) {
				document.body.appendChild(this._div);
			}
		}

		dispose() {
			try {
				if (this._div && this._div.parentNode) {
					this._div.parentNode.removeChild(this._div);
				}
			} catch (ex) {

			} finally {
				this._div = null;
			}
		}

		format($cell: JQuery, cellIndex: number, $container: JQuery, column: c1basefield, formattedValue, rowInfo: IRowInfo): void {
			column._initializeCell($cell, cellIndex, $container, formattedValue, rowInfo);
		}

		updateHTML(container: JQuery, value, encodeHTML: boolean = false) {
			// container.html(value || "&nbsp;"); // -- very slow in IE when table content is recreated more than once (after paging, sorting etc, especially in flat mode).
			var domContainer = container[0];

			// reset content
			if (domContainer.firstChild) {
				while (domContainer.firstChild) {
					domContainer.removeChild(domContainer.firstChild);
				}
			}

			wijmo.grid.setContent(this._div, encodeHTML, value);

			while (this._div.firstChild) {
				domContainer.appendChild(this._div.firstChild);
			}
		}
	}
}