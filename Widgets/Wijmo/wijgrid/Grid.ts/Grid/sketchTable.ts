/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class SketchObject {
		private __attr: any;
		private __style: any;

		constructor(attr) {
			if (attr) {
				this.__attr = attr;
			}
		}

		ensureAttr(): any {
			this.__attr = this.__attr || {};
			return this.__attr;
		}

		attr(): any;
		attr(name, value): void;
		attr(name): any;
		attr(name?, value?) {
			if (arguments.length == 0) {
				return this.__attr;
			}

			if (arguments.length == 1) {
				return this.__attr ? this.__attr[name] : null;
			} else {
				this.ensureAttr()[name] = value;
			}
		}

		style(): any;
		style(name: string): any;
		style(name: string, newValue: any): void;
		style(name?, newValue?): any {
			if (arguments.length == 0) {
				return this.__style;
			} else if (arguments.length == 1) {
				return this.__style && this.__style[name];
			} else {
				this.ensureStyle()[name] = newValue;
			}
		}
		deleteAttr(name: string): void {
			if (this.__attr) {
				delete this.__attr[name];
			}
		}
		deleteStyle(name: string): void {
			if (this.__style) {
				delete this.__style[name];
			}
		}
		/** Creates style object if it does not exist. Then returns it */
		ensureStyle(): any {
			this.__style = this.__style || {};
			return this.__style;
		}
	}

	/** @ignore */
	export class SketchCell extends SketchObject {
		private _visible: boolean;

		constructor(attr) {
			super(attr);
		}

		visible(): boolean;
		visible(newValue: boolean): void;
		visible(newValue?) {
			if (arguments.length == 0) {
				return !(this._visible === false);
			} else {
				this._visible = newValue;
			}
		}
	}

	/** @ignore */
	/* A sketch cell with a value */
	export class ValueCell extends SketchCell {
		constructor(public value, attr) {
			super(attr);
		}
	}

	/** @ignore */
	/* A sketch cell with raw html */
	export class HtmlCell extends SketchCell {
		public html: string;

		constructor(html: string, attr) {
			super(attr);

			this.html = html || "";
		}

		static nbsp(): HtmlCell {
			return new HtmlCell("&nbsp;", null);
		}
	}

	/** @ignore */
	export class HeaderCell extends SketchCell {
		public column: IColumn;

		constructor(column: IColumn, attrs) {
			super(attrs);

			this.column = column;
		}
	}


	/** @ignore */
	export class SketchRow extends SketchObject {
		public rowType: wijmo.grid.rowType;
		public renderState: wijmo.grid.renderState;
		public extInfo: IRowInfoExt;

		private _cells: SketchCell[];

		constructor(rowType: wijmo.grid.rowType, renderState: wijmo.grid.renderState, attrs) {
			super(attrs);

			this.rowType = rowType;
			this.renderState = renderState;
			this.extInfo = { state: renderStateEx.none };
		}

		/** returns true if this is data\ dataAlt\ dataHierarchyHeader\ dataHierarchyDetail row */
		isDataRow() {
			return (this.rowType & wijmo.grid.rowType.data) === wijmo.grid.rowType.data;
		}

		/** returns true if this is data\ dataAlt row */
		isPureDataRow() {
			var rt = wijmo.grid.rowType;
			return (this.rowType & ~rt.dataAlt) === rt.data;
		}

		dataItemIndex(offset = 0) {
			return -1;
		}

		cellCount() {
			return this._cells ? this._cells.length : 0;
		}
		/** create the cell table if it does not exist yet */
		_ensureCells() {
			this._cells = this._cells || [];
		}
		/** add a cell to the end */
		add(elem: SketchCell) {
			this._ensureCells();
			this._cells.push(elem);
		}
		/** insert a cell */
		insert(index: number, elem: SketchCell) {
			this._ensureCells();
			this._cells.splice(index, 0, elem);
		}

		cell(index: number): SketchCell {
			return this._cells[index];
		}
		valueCell(index: number): ValueCell {
			return <ValueCell> this.cell(index);
		}

		/** remove a cell by index */
		removeAt(index: number) {
			if (!this._cells) {
				throw "Wrong index";
			}
			this._cells.splice(index, 1);
		}

		/** remove all cells */
		clear() {
		}

		getRowInfo(): IRowInfo {
			return {
				type: this.rowType,
				state: this.renderState,
				sectionRowIndex: null,
				dataRowIndex: null,
				virtualDataItemIndex: null,
				dataItemIndex: this.dataItemIndex(),
				sketchRowIndex: null,
				$rows: null,
				_extInfo: this.extInfo
			};
		}
	}

	/** @ignore */
	export class SketchDataRow extends SketchRow {
		public originalRowIndex: number;
		//private mDataAccessor: (index: number) => any;

		constructor(originalRowIndex: number/*, dataAccessor: (index: number) => any*/, renderState: wijmo.grid.renderState, attrs) {
			super(wijmo.grid.rowType.data | ((originalRowIndex % 2) == 1 ? wijmo.grid.rowType.dataAlt : 0), renderState, attrs);

			//this.mDataAccessor = dataAccessor;
			this.originalRowIndex = originalRowIndex;
		}

		//data(offset = 0): any {
		//	return this.mDataAccessor.call(this, this.dataItemIndex(offset));
		//}

		dataItemIndex(offset = 0) {
			return offset + this.originalRowIndex;
		}

		isDataRow() {
			return true;
		}
	}

	/** @ignore */
	export class SketchGroupRow extends SketchRow {
		public groupByValue: any;

		constructor(header: boolean, attrs) {
			var rowType = header
				? wijmo.grid.rowType.groupHeader //"groupHeader"
				: wijmo.grid.rowType.groupFooter; // "groupFooter"
			super(rowType, wijmo.grid.renderState.rendering, attrs);
		}

		getRowInfo(): IRowInfo {
			var info = super.getRowInfo();
			if (this.groupByValue !== undefined) {
				info.groupByValue = this.groupByValue;
			}
			return info;
		}
	}

	/** @ignore */
	export class SketchHeaderRow extends SketchRow {
		constructor(attrs) {
			super(rowType.header, renderState.rendering, attrs);
		}
	}

	/** @ignore */
	export class SketchDetailRow extends SketchRow {
		constructor(attrs) {
			super(rowType.detail, renderState.rendering, attrs);
		}
	}

	/** @ignore */
	export class SketchTable implements ISearchable<SketchRow> {
		_table: SketchRow[] = [];

		getRawTable(): SketchRow[] {
			return this._table;
		}

		row(index: number): SketchRow {
			return this._table[index];
		}

		valueAt(rowIndex: number, colIndex: number): any {
			return this.row(rowIndex).valueCell(colIndex).value;
		}

		wijgridDataAttrValueAt(rowIndex: number, colIndex: number): string {
			return this.row(rowIndex).valueCell(colIndex).attr("wijgrid-data");
		}

		count(newValue?: number): number {
			if (typeof newValue === "number") {
				this._table.length = newValue;
			}
			return this._table.length;
		}

		add(row: SketchRow): void {
			this._table.push(row);
		}
		insert(index: number, row: SketchRow): void {
			this._table.splice(index, 0, row);
		}
		clear(): void {
			this._table.length = 0;
		}
		indexOf(element: SketchRow): number {
			return this._table.indexOf(element);
		}
		remove(index: number, count = 1): void {
			this._table.splice(index, count);
		}
		removeFirst(count = 1): void {
			this._table.splice(0, count);
		}
		removeLast(count = 1): void {
			this._table.splice(this._table.length - count, count);
		}

		replace(index: number, row: SketchRow): void {
			var oldRows = this._table.splice(index, 1, row);

			if (oldRows && oldRows.length && row) {
				row.extInfo = oldRows[0].extInfo;
			}
		}

		updateIndexes(): void {
			for (var i = 0; i < this._table.length; i++) {
				var sketchRow = this._table[i];

				if (this._table[i].isDataRow()) {
					(<SketchDataRow>sketchRow).originalRowIndex = i;
				}
			}
		}

		isLazy(): boolean {
			return false;
		}
		ensureNotLazy() {
		}
		find(startFrom: number, callback: (row: SketchRow) => any): SketchRow {
			var f = this.findEx(startFrom, callback);
			return f ? f.val : null;
		}

		findIndex(startFrom: number, callback: (row: SketchRow) => any): number {
			var f = this.findEx(startFrom, callback);
			return f ? f.at : -1;
		}

		findEx(startFrom: number, callback: (row: SketchRow) => any): ISearchableResult<SketchRow> {
			var cnt = this.count();

			for (var i = startFrom; i < cnt; i++) {
				var row = this.row(i);

				if (callback(this.row(i)) === true) {
					return {
						at: i,
						val: row
					};
				}
			}

			return null;
		}
	}

	/** @ignore */
	export class LazySketchTable extends SketchTable {
		private mSource: ISketchRowSource;
		mIsLazy = true;
		pageSize = 30;

		constructor(source: ISketchRowSource) {
			super();
			if (source == null) {
				throw "Row source is null";
			}
			this.mSource = source;
			this._table.length = source.count();
		}

		isLazy(): boolean {
			return this.mIsLazy;
		}
		/** Convert to not lazy and create rows if they don't exist*/
		ensureNotLazy() {
			if (!this.isLazy()) return;
			this.ensureRange();
			this.mIsLazy = false;
		}

		invalidate(): void {
			this._table = new Array(this.mSource.count());
			if (!this.isLazy()) {
				this.updateRange();
			}
		}

		row(index: number) {
			if (!super.row(index)) {
				this.ensureRange(
					Math.max(0, index - this.pageSize / 2),
					this.pageSize
					);
			}

			return super.row(index);
		}
		/** Create sketch rows if the don't exist yet. 
		  * Cannot create anything other than data rows. 
		  * Requires source
		  */
		ensureRange(start = 0, length = this._table.length - start): void {
			if (!this.isLazy()) return;
			if (start < 0) {
				throw "Wrong range start";
			}

			this._table.length = this.mSource.count();
			if (this._table.length == 0) {
				if (length > 0) {
					throw "Wrong range length";
				}
				return;
			}

			// skip existing rows in the beginning
			while (start < this._table.length && this._table[start]) {
				start++;
			}

			// is there anything to create?
			if (start >= this._table.length) return;

			var last = Math.min(start + length - 1, this._table.length - 1);
			// skip existing rows in the end
			while (last >= start && this._table[last]) {
				last--;
			}

			length = last - start + 1;
			if (length > 0) {
				this.updateRange(start, length);
			}
		}
		updateRange(start = 0, length = this._table.length - start): void {
			if (this.mSource == null) {
				throw "Cannot create sketch row because source is null";
			}
			this.mSource.getRange(start, length, this._table, start);
		}

	}

	//export interface ISketchRowFactory {
	//	createDataRow(dataItem: any, index: number): SketchDataRow;
	//	createEmptyDataRow(html, isLastRow: boolean): SketchRow;
	//}
	/** @ignore */
	export interface ISketchRowSource {
		count(): number;
		getRange(start: number, count: number, dest: SketchRow[], offset: number): void;
	}
}