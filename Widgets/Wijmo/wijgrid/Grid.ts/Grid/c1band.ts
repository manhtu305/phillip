/// <reference path="c1basefield.ts"/>
/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	export class c1bandfield extends c1basefield {
		/** @ignore */
		public static WIDGET_NAME = "wijmo.c1bandfield";

		/** @ignore */
		public static test(column: IColumn): boolean {
			return !!(column && column.columns);
		}

		public options: IC1BandFieldOptions;

		constructor(wijgrid: wijgrid, options, emulatedWidgetName?: string) {
			super(wijgrid, options, emulatedWidgetName || c1bandfield.WIDGET_NAME);
		}

		_provideDefaults() {
			super._provideDefaults();

			wijmo.grid.shallowMerge(this.options, c1bandfield.prototype.options);

			(<IColumn>this.options).dataKey = null;
		}
	}

	export class c1bandfield_options extends c1basefield_options implements IC1BandFieldOptions {
		/**
		* Gets a array of objects representing the band columns.
		* @example
		* $("#element").wijgrid({
		*   columns: [{
		*      headerText: "Band",
		*      columns: [
		*         { headerText: "ID" },
		*         { headerText: "Name" }
		*      ]
		*   }]
		* });
		*/
		columns: IColumn[] = [];
	};

	c1bandfield.prototype.options = wijmo.grid.extendWidgetOptions(c1basefield.prototype.options, new c1bandfield_options());

	wijmo.grid.registerColumnFactory((grid, column) => {
		if (!!(column && column.columns)) {
			return new c1bandfield(grid, column);
		}

		return null;
	});
}