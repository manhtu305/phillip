/// <reference path="interfaces.ts"/>
/// <reference path="wijgrid.ts"/>
/// <reference path="cellInfo.ts"/>
/// <reference path="rowAccessor.ts"/>

module wijmo.grid {
	  

	var $ = jQuery;

	/** @ignore */
	export class uiSelection {
		private _wijgrid: wijgrid;
		private _gap_to_start = 10;
		private _evntFormat: string;
		private _addedCells: wijmo.grid.cellInfoOrderedCollection;
		private _startPos: { x: number; y: number; };
		private _startCellInfo: wijmo.grid.cellInfo;
		private _endCellInfo: wijmo.grid.cellInfo;
		private _prevMouseMoveRange: wijmo.grid.cellInfoRange;
		private _inProgress = false;
		private _additionalEventsAttached = false;
		private _view: wijmo.grid.baseView;
		private _rootElement: JQuery;

		constructor(wijgrid: wijgrid) {
			this._wijgrid = wijgrid;

			this._evntFormat = "{0}." + this._wijgrid.widgetName + ".selectionui";
			this._addedCells = new wijmo.grid.cellInfoOrderedCollection(this._wijgrid);
			this._view = this._wijgrid._view();
			this._rootElement = this._view.focusableElement();

			this._rootElement.bind(this._eventKey("mousedown"), $.proxy(this._onGridMouseDown, this));
		}

		dispose() {
			this._rootElement.unbind(this._eventKey("mousedown"), this._onGridMouseDown);
			this._detachAdditionalEvents();
		}

		private _onGridMouseDown(args: JQueryEventObject) {
			if (!this._wijgrid._canInteract() || this._wijgrid.options.selectionMode.toLowerCase() === "none") {
				return;
			}

			var visibleBounds = this._view.getVisibleContentAreaBounds(), //.getVisibleAreaBounds(),
				mouse = { x: args.pageX, y: args.pageY },
				tag = ((args.target && (<Element>args.target).tagName !== undefined)
				? (<Element>args.target).tagName.toLowerCase()
				: undefined),
				$target = $(args.target),
				defCSS = wijmo.grid.wijgrid.CSS;

			if ((!tag || $target.is("td." + defCSS.TD + ", th." + defCSS.TD + ", div." + defCSS.cellContainer)) &&
				(mouse.x > visibleBounds.left && mouse.x < visibleBounds.left + visibleBounds.width) &&
				(mouse.y > visibleBounds.top && mouse.y < visibleBounds.top + visibleBounds.height)) {

				this._attachAdditionalEvents();
				this._startPos = mouse;

				this._startCellInfo = this._coordToDataCellInfo(this._startPos);
			}
		}

		private _onDocumentMouseMove(args: JQueryEventObject) {
			if (!this._startCellInfo || !this._startCellInfo._isValid()) {
				return;
			}

			var mouse = { x: args.pageX, y: args.pageY },
				rowInfo: IRowInfo,
				view = this._wijgrid._view(),
				$rs = wijmo.grid.renderState;

			if (!this._inProgress) {
				this._inProgress = (Math.abs(this._startPos.x - mouse.x) > this._gap_to_start) ||
				(Math.abs(this._startPos.y - mouse.y) > this._gap_to_start);
			}

			if (this._inProgress) {
				var tmp = this._coordToDataCellInfo(mouse);
				if (!tmp._isValid()) {
					return;
				}

				this._endCellInfo = tmp;

				if (!this._wijgrid.selection()._multipleEntitiesAllowed()) {
					this._startCellInfo = this._endCellInfo;
				}

				var range = new wijmo.grid.cellInfoRange(this._startCellInfo, this._endCellInfo);
				range._normalize();
				range._clip(this._wijgrid._getDataCellsRange(dataRowsRangeMode.sketch));

				if (range._isValid() && !range.isEqual(this._prevMouseMoveRange)) {
					this._prevMouseMoveRange = range;

					var desiredCells = new wijmo.grid.cellInfoOrderedCollection(this._wijgrid);

					for (var i = range.topLeft().rowIndex(), len = range.bottomRight().rowIndex(); i <= len; i++) {
						rowInfo = view._getRowInfoBySketchRowIndex(i);

						if (rowInfo.type & wijmo.grid.rowType.data) {
							for (var j = range.topLeft().cellIndex(), len2 = range.bottomRight().cellIndex(); j <= len2; j++) {
								desiredCells._appendUnsafe(new wijmo.grid.cellInfo(j, i));
							}
						}
					}

					var prevRowIndex = -1;

					for (var i = 0, len = this._addedCells.length(); i < len; i++) {
						var cellInfo = this._addedCells.item(i);

						if (desiredCells.indexOf(cellInfo) < 0) // remove css
						{
							if (this._wijgrid.selection().selectedCells().indexOf(cellInfo) < 0) {
								var cell = this._view.getCell(cellInfo.cellIndexAbs(), cellInfo.rowIndexAbs());

								if (cell) {
									if (prevRowIndex !== cellInfo.rowIndex()) {
										rowInfo = cellInfo.row();
										prevRowIndex = cellInfo.rowIndex();
									}

									var $cell = $(cell);
									var state = view._changeCellRenderState($cell, $rs.selected, false);
									this._wijgrid.mCellStyleFormatter.format($cell, cellInfo.cellIndex(), cellInfo.columnInst(), rowInfo, state);
								}
							}

							this._addedCells._removeAt(i);
							i--;
							len--;
						}
					}

					prevRowIndex = -1;
					for (var i = 0, len = desiredCells.length(); i < len; i++) {
						var cellInfo = desiredCells.item(i);

						if (this._addedCells.indexOf(cellInfo) < 0 && this._wijgrid.selection().selectedCells().indexOf(cellInfo) < 0) {
							if (this._addedCells._add(cellInfo)) {
								var cell = this._view.getCell(cellInfo.cellIndexAbs(), cellInfo.rowIndexAbs());

								if (cell) {
									if (prevRowIndex !== cellInfo.rowIndex()) {
										rowInfo = cellInfo.row();
										prevRowIndex = cellInfo.rowIndex();
									}

									var $cell = $(cell);
									var state = view._changeCellRenderState($cell, $rs.selected, true);
									this._wijgrid.mCellStyleFormatter.format($cell, cellInfo.cellIndex(), cellInfo.columnInst(), rowInfo, state);
								}
							}
						}
					}
				} // end if
			}
		}

		private _onDocumentMouseUp(args: JQueryEventObject) {
			this._detachAdditionalEvents();

			if (this._inProgress) {
				this._inProgress = false;

				if (this._prevMouseMoveRange && this._prevMouseMoveRange._isValid()) {
					this._wijgrid._changeCurrentCell(args, this._endCellInfo, { changeSelection: false, setFocus: false });

					if (!args.shiftKey || !this._wijgrid.selection()._multipleEntitiesAllowed()) {
						this._wijgrid.selection()._startNewTransaction(this._startCellInfo);
					}

					this._wijgrid.selection().beginUpdate();
					this._wijgrid.selection()._selectRange(this._prevMouseMoveRange, args.shiftKey, args.ctrlKey, wijmo.grid.cellRangeExtendMode.none, this._endCellInfo);
					this._wijgrid.selection().endUpdate();

					var view = this._wijgrid._view(),
						prevRowIndex = -1,
						rowInfo: IRowInfo,
						$rs = wijmo.grid.renderState;

					// clear remained cells
					for (var i = 0, len = this._addedCells.length(); i < len; i++) {
						var cellInfo = this._addedCells.item(i);

						if (this._wijgrid.selection().selectedCells().indexOf(cellInfo) < 0) {
							var cell = view.getCell(cellInfo.cellIndexAbs(), cellInfo.rowIndexAbs());

							if (cell !== null) {
								if (prevRowIndex !== cellInfo.rowIndex()) {
									rowInfo = cellInfo.row();
									prevRowIndex = cellInfo.rowIndex();
								}

								var $cell = $(cell),
									state = view._changeCellRenderState($cell, $rs.selected, false);

								this._wijgrid.mCellStyleFormatter.format($cell, cellInfo.cellIndex(), cellInfo.columnInst(), rowInfo, state);
							}
						}
					}

					this._addedCells._clear();
					this._startCellInfo = this._endCellInfo = this._prevMouseMoveRange = null;

					return false; // cancel bubbling
				}
			}
		}

		private _attachAdditionalEvents() {
			if (!this._additionalEventsAttached) {
				try {
					this._view.toggleDOMSelection(false); // disable selection

					$(document)
						.bind(this._eventKey("mousemove"), $.proxy(this._onDocumentMouseMove, this))
						.bind(this._eventKey("mouseup"), $.proxy(this._onDocumentMouseUp, this));
				}
				finally {
					this._additionalEventsAttached = true;
				}
			}
		}

		private _detachAdditionalEvents() {
			if (this._additionalEventsAttached) {
				try {
					this._view.toggleDOMSelection(true); // enable selection

					$(document)
						.unbind(this._eventKey("mousemove"), this._onDocumentMouseMove)
						.unbind(this._eventKey("mouseup"), this._onDocumentMouseUp);
				} finally {
					this._additionalEventsAttached = false;
				}
			}
		}

		private _eventKey(eventType: string): string {
			return wijmo.grid.stringFormat(this._evntFormat, eventType);
		}

		private _coordToDataCellInfo(pnt: { x: number; y: number; }): wijmo.grid.cellInfo {
			var left = 0,
				right = this._wijgrid._renderedLeaves().length - 1,
				median = 0,
				cellIdx = -1,
				bounds: IElementBounds,
				gridRowsAccessor = new wijmo.grid.rowAccessor(this._view, 2 /* tbody */, 0, 0),
				row: IRowObj;

			// get cell index
			while (left <= right) {
				median = ((right - left) >> 1) + left;

				bounds = wijmo.grid.bounds(this._view.getHeaderCell(median)); // get header cell
				if (!bounds) { // no header?
					row = gridRowsAccessor.item(0);
					bounds = wijmo.grid.bounds(wijmo.grid.rowAccessor.getCell(row, median)); // get data cell
				}

				if (!bounds) {
					break;
				}

				if (pnt.x < bounds.left) { // -1 
					right = median - 1;
				}
				else
					if (pnt.x > bounds.left + bounds.width) { // 1
						left = median + 1;
					} else { // 0
						cellIdx = median;
						break;
					}
			}

			if (cellIdx === -1) {
				return wijmo.grid.cellInfo.outsideValue;
			}

			gridRowsAccessor = new wijmo.grid.rowAccessor(this._view, 0 /* all */, 0, 0);

			var rowIdx = -1;
			left = 0;
			right = gridRowsAccessor.length() - 1;
			median = 0;

			// get row index
			while (left <= right) {
				median = ((right - left) >> 1) + left;
				row = gridRowsAccessor.item(median);
				bounds = wijmo.grid.bounds(wijmo.grid.rowAccessor.getCell(row, 0));

				if (pnt.y < bounds.top) { // -1
					right = median - 1;
				}
				else
					if (pnt.y > bounds.top + bounds.height) { // 1
						left = median + 1;
					} else { // 0
						rowIdx = median;
						break;
					}
			} // end while { }


			if (rowIdx === -1) {
				return wijmo.grid.cellInfo.outsideValue;
			}


			var result = new wijmo.grid.cellInfo(cellIdx, rowIdx, this._wijgrid, true);

			return result;
		}
	}
}