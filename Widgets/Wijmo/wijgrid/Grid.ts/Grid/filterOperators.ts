/// <reference path="wijgrid.ts"/>
/// <reference path="interfaces.ts"/>

/// <reference path="../../../data/src/dataView.ts"/>
/// <reference path="../../../data/src/filtering.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	class builtInFilterOperators {
		public static NoFilterOp: IFilterOperator = {
			applicableTo: null, // any type
			name: "NoFilter",
			displayName: "No filter",
			arity: 1,
			operator: function () {
				return true;
			}
		}
	}

	/** @ignore */
	export class filterOperatorsCache {
		private _wijgrid: wijgrid;
		private _cache: { [index: string]: IInternalFilterOperator; } = {};

		constructor(wijgrid: wijgrid) {
			var self = this;

			this._wijgrid = wijgrid;

			this._addOperator(null, builtInFilterOperators.NoFilterOp);

			$.each(wijmo.data.filtering.ops, function (name: string, op: wijmo.data.filtering.IBuiltInFilterOperator) {
				self._addOperator(name, <wijmo.grid.IFilterOperator><any>op);
			});

			$.each(wijgrid.options.customFilterOperators, function (key, fop) {
				self._addOperator(null, fop, true);
			});
		}

		getByName(name: string): IFilterOperator {
			var fop = this.getByNameInt(name);

			return (fop)
				? fop.op
				: null;
		}

		getByNameInt(name: string): IInternalFilterOperator {
			return this._cache[(name || "").toLowerCase()];
		}

		getByDataType(dataType: string): IFilterOperator[] {
			var intResult: IInternalFilterOperator[] = [],
				result: IFilterOperator[];

			$.each(this._cache, function (key: string, val: IInternalFilterOperator) {
				var fop = val.op;

				if (!fop.applicableTo /* NoFilter*/ || $.inArray(dataType, fop.applicableTo) >= 0) {
					intResult.push(val);
				}
			});

			switch (this._wijgrid.options.filterOperatorsSortMode.toLowerCase()) {
				case "alphabetical":
					intResult.sort(this._sortAlpha);
					break;
				case "alphabeticalcustomfirst":
					intResult.sort(this._sortAlphaCustomFirst);
					break;

				case "alphabeticalembeddedFirst":
					intResult.sort(this._sortAlphaEmbeddedFirst);
					break;

				case "none": // do nothing 
					break;

				default:
					break;
			}

			result = <any>$.map(intResult, function (val: any, key?: any) {
				return val.op;
			});

			return result;
		}

		private _addOperator(name: string, fop: IFilterOperator, isCustom: boolean = false) {
			if (name && !fop.name) {
				fop.name = name;
			}

			name = (name || fop.name).toLowerCase();

			if (!this._cache[name]) {
				this._cache[name] = {
					op: fop,
					isCustom: (isCustom === true)
				};
			}
		}

		private _sortAlpha(a: IInternalFilterOperator, b: IInternalFilterOperator): number {
			var n1 = a.op.name.toLowerCase(),
				n2 = b.op.name.toLowerCase();

			if (n1 !== n2) {
				if (n1 === "nofilter") {
					return -1;
				}

				if (n2 === "nofilter") {
					return 1;
				}
			}

			if (n1 === n2) {
				return 0;
			}

			return (n1 < n2)
				? -1
				: 1;
		}

		private _sortAlphaEmbeddedFirst(a: IInternalFilterOperator, b: IInternalFilterOperator): number {
			var n1 = a.op.name.toLowerCase(),
				n2 = b.op.name.toLowerCase();

			if (n1 !== n2) {
				if (n1 === "nofilter") {
					return -1;
				}

				if (n2 === "nofilter") {
					return 1;
				}
			}

			if (a.isCustom !== b.isCustom) {
				if (a.isCustom) {
					return 1;
				}

				if (b.isCustom) {
					return -1;
				}
			}

			if (n1 === n2) {
				return 0;
			}

			return (n1 < n2)
				? -1
				: 1;
		}

		private _sortAlphaCustomFirst(a: IInternalFilterOperator, b: IInternalFilterOperator): number {
			var n1 = a.op.name.toLowerCase(),
				n2 = b.op.name.toLowerCase();

			if (n1 !== n2) {
				if (n1 === "nofilter") {
					return -1;
				}

				if (n2 === "nofilter") {
					return 1;
				}
			}

			if (a.isCustom !== b.isCustom) {
				if (a.isCustom) {
					return -1;
				}

				if (b.isCustom) {
					return 1;
				}
			}

			if (n1 === n2) {
				return 0;
			}

			return (n1 < n2)
				? -1
				: 1;
		}
	}

	/** @ignore */
	export interface IInternalFilterOperator {
		op: IFilterOperator;
		isCustom: boolean;
	}

	/** @ignore */
	export class filterHelper {
		private static marker = "_wijgrid";

		// filterValue
		// [filterValue, ..., filterValue]
		// [[filterValue, ..., filterValue], ..., [filterValue, ..., filterValue]]
		public static getSingleValue(filterValue): any {
			if ($.isArray(filterValue)) {
				filterValue = filterValue[0];

				if ($.isArray(filterValue)) {
					filterValue = filterValue[0];
				}
			}

			return filterValue;
		}

		// filterOperator -> name | { name, condition }
		// filterOperator -> filterOperator | [ filterOperator, ..., filterOperator]
		public static getSingleOperatorName(filterOperator): string {
			if ($.isArray(filterOperator)) {
				filterOperator = filterOperator[0];
			}

			return filterOperator.name || filterOperator || "";
		}

		// filterOperator: opName | [opName, ..., opName] | [ { name, condition }, ..., { name, condition } ]
		// filterValue: filterValue | [filterValue, ... , filterValue] | [[], ..., []]
		public static verify(filterOperator, filterValue, dataType: string, cache: wijmo.grid.filterOperatorsCache): wijmo.data.IFilterDescriptor {
			if (filterOperator) {
				if ($.isArray(filterOperator)) {
					var fop = [],
						fval = [];

					if (!$.isArray(filterValue)) {
						filterValue = [filterValue];
					}

					for (var i = 0, len = filterOperator.length; i < len; i++) {
						if (wijmo.grid.filterHelper._verifySingleOp(filterOperator[i], filterValue[i], dataType, cache)) {
							fop.push({
								name: filterOperator[i].name || filterOperator[i],
								condition: filterOperator[i].condition || "or"
							});

							fval.push(filterValue ? filterValue[i] : undefined);
						}
					}

					if (fop.length) {
						return {
							operator: fop,
							value: fval
						};
					}
				} else {
					if (wijmo.grid.filterHelper._verifySingleOp(filterOperator, filterValue, dataType, cache)) {
						return { // compatibility with old model
							operator: filterOperator,
							value: filterValue
						};
					}
				}
			}

			return null;
		}

		// filterOpeator: name | { name, condition }
		private static _verifySingleOp(filterOperator, filterValue, dataType: string, cache: wijmo.grid.filterOperatorsCache) {
			if (filterOperator && (filterOperator = (filterOperator.name || filterOperator))) {
				var fop;

				filterOperator = (filterOperator || "").toLowerCase();

				if ((filterOperator !== "nofilter" || filterValue !== undefined) && (fop = cache.getByName(filterOperator))) {

					if (fop.applicableTo === null /*NoFilter*/ || $.inArray(dataType || "string", fop.applicableTo) >= 0) {
						if (fop.arity === 1 || (fop.arity > 1 && wijmo.grid.filterHelper.getSingleValue(filterValue) !== undefined)) {
							return true;
						}
					}
				}
			}

			return false;
		}
	}
}