/// <reference path="interfaces.ts"/>
/// <reference path="bands_traversing.ts"/>
/// <reference path="misc.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class columnsGenerator {
		public static generate(mode: string, fieldsInfo: IFieldInfoCollection, columns: IColumn[]) {
			mode = (mode || "").toLowerCase();

			switch (mode) {
				case "append":
					columnsGenerator._processAppendMode(fieldsInfo, columns);
					break;

				case "merge":
					columnsGenerator._processMergeMode(fieldsInfo, columns);
					break;

				case "none":
					break;

				default:
					throw wijmo.grid.stringFormat("Unsupported value: \"{0}\"", mode);
			}
		}

		private static _processAppendMode(fieldsInfo: IFieldInfoCollection, columns: IColumn[]) {
			var autoColumns = {};

			wijmo.grid.traverse(columns, function (column) {
				if (column._dynamic && wijmo.grid.validDataKey(column.dataKey)) {
					autoColumns[column.dataKey] = true;
				}
			});

			$.each(fieldsInfo, function (key, fieldInfo: IFieldInfo) {
				if (("name" in fieldInfo) && !autoColumns[fieldInfo.name]) {
					columns.push(columnsGenerator._createAutoField(fieldInfo));
				}
			});
		}

		private static _processMergeMode(fieldsInfo: IFieldInfoCollection, columns: IColumn[]) {
			var columnsHasNoDataKey = [];

			wijmo.grid.traverse(columns, function (column) {
				if (column._isLeaf && columnsGenerator._isBindableColumn(column)) {
					var dataKey = column.dataKey;

					if (wijmo.grid.validDataKey(dataKey)) {
						if (fieldsInfo[dataKey] !== undefined) {
							delete fieldsInfo[dataKey];
						}
					} else {
						if (dataKey !== null) { // don't linkup with any data field if dataKey is null
							columnsHasNoDataKey.push(column);
						}
					}
				}
			});

			if (columnsHasNoDataKey.length) {
				var i = 0;

				$.each(fieldsInfo, function (key, info: IFieldInfo) {
					var leaf = columnsHasNoDataKey[i++];
					if (leaf) {
						leaf.dataKey = info.name;
						delete fieldsInfo[key];
					}
				});
			}

			$.each(fieldsInfo, function (key, info: IFieldInfo) {
				columns.push(columnsGenerator._createAutoField(info));
			});
		}

		private static _createAutoField(fieldInfo: IFieldInfo): IColumn {
			return <any>wijmo.grid.createDynamicField({ dataKey: fieldInfo.name });
		}

		private static _isBindableColumn(column: IColumn): boolean {
			return !c1bandfield.test(column)
				&& !c1rowheaderfield.test(column)
				&& !c1buttonbasefield.test(column)
				&& !c1buttonfield.test(column)
				&& !c1commandbuttonfield.test(column);
		}
	}
}



