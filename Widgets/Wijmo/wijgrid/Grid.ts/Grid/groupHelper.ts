/// <reference path="interfaces.ts"/>
/// <reference path="merger.ts"/>
/// <reference path="grouper.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class groupHelper {
		private static _getGroupInfoRegExp = new RegExp(".*G([HF]){1}(\\d+)-(\\d+)$");

		static getGroupInfo(row: HTMLTableRowElement)
			: { level: number; index: number; isHeader: boolean; toString(): string; } {
			if (row) {
				var info = wijmo.grid.groupHelper._getGroupInfoRegExp.exec(row.id),
					level, index, isHeader;

				if (info) {
					level = parseInt(info[3], 10);
					index = parseInt(info[2], 10);
					isHeader = (info[1] === "H");

					return {
						level: level,
						index: index,
						isHeader: isHeader,
						toString: function () {
							return (isHeader ? "GH" : "GF") + index + "-" + level;
						}
					};
				}
			}

			return null;
		}

		static getColumnByGroupLevel(leaves: c1basefield[], level: number): c1field {
			for (var i = 0, len = leaves.length; i < len; i++) {
				var leaf = leaves[i],
					opt = <IColumn>leaves[i].options;

				if (opt.groupInfo && (opt.groupInfo.level === level)) {
					return <c1field>leaf;
				}
			}

			return null;
		}

		static getGroupedColumnsCount(leaves: c1basefield[]): number {
			var result = 0;

			for (var i = 0, len = leaves.length; i < len; i++) {
				var groupInfo = (<IColumn>leaves[i].options).groupInfo;

				if (groupInfo && (groupInfo.position === "header" || groupInfo.position === "headerAndFooter" || groupInfo.position === "footer")) {
					result++;
				}
			}

			return result;
		}

		// cellRange cellRange
		// groupRange[] childExpandInfo
		static getChildGroupIndex(cellRange: wijmo.grid.cellRange, childExpandInfo: wijmo.grid.groupRange[]): number {
			var left = 0,
				right = childExpandInfo.length - 1,
				median, cmp;

			while (left <= right) {
				median = ((right - left) >> 1) + left;
				cmp = childExpandInfo[median].cr.r1 - cellRange.r1;

				if (cmp === 0) {
					return median;
				}

				if (cmp < 0) {
					left = median + 1;
				} else {
					right = median - 1;
				}
			}

			return left;
			//return ~left;
		}

		static getParentGroupIndex(cellRange: wijmo.grid.cellRange, parentExpandInfo: wijmo.grid.groupRange[]): number {
			var idx = wijmo.grid.groupHelper.getChildGroupIndex(cellRange, parentExpandInfo);

			if (idx > 0) {
				idx--;
			}

			//return (idx < parentExpandInfo.length)
			//	? idx
			//	: -1;

			if (idx < parentExpandInfo.length) { // #63623
				var groupRange = parentExpandInfo[idx];
				if ((cellRange.r1 >= groupRange.cr.r1) && (cellRange.r2 <= groupRange.cr.r2)) {
					return idx;
				}
			}

			return -1;
		}

		// level: 1-based level of the cellRange;
		static getChildGroupRanges(leaves: c1basefield[], cellRange: wijmo.grid.cellRange, level: number): { column: c1field; ranges: wijmo.grid.groupRange[]; } {
			var result: wijmo.grid.groupRange[] = [],
				childGroupedColumn = wijmo.grid.groupHelper.getColumnByGroupLevel(leaves, level + 1);

			if (childGroupedColumn) {
				var childRanges: wijmo.grid.groupRange[] = (<IColumn>childGroupedColumn.options).groupInfo.expandInfo,
					firstChildIdx = wijmo.grid.groupHelper.getChildGroupIndex(cellRange, childRanges);

				for (var i = firstChildIdx, len = childRanges.length; i < len; i++) {
					var childRange: wijmo.grid.groupRange = childRanges[i];

					if (childRange.cr.r2 <= cellRange.r2) {
						result.push(childRange);
					} else {
						break;
					}
				}

				return {
					column: childGroupedColumn,
					ranges: result
				};
			}

			return null;
		}

		// level: 1-based level of the cellRange; optional.
		static getParentGroupRange(leaves: c1basefield[], cellRange: wijmo.grid.cellRange, level?: number): wijmo.grid.groupRange {
			if (level === undefined) {
				level = 0xFFFF;
			}

			if (cellRange && (level - 2 >= 0)) {
				for (var i = leaves.length - 1; i >= 0; i--) {
					var groupInfo: IGroupInfo = (<IColumn>leaves[i].options).groupInfo;

					if (!groupInfo || !groupInfo.expandInfo || (groupInfo.level < 0) || (groupInfo.level !== level - 1)) {
						continue;
					}

					var idx = wijmo.grid.groupHelper.getParentGroupIndex(cellRange, groupInfo.expandInfo);
					if (idx >= 0) {
						return groupInfo.expandInfo[idx];
					}
				}
			}

			return null;
		}

		// level: 1-based level of the cellRange.
		static isParentCollapsed(leaves: c1basefield[], cellRange: wijmo.grid.cellRange, level: number): boolean {
			if (level === 1) {
				return false;
			}

			for (var i = level; i > 1; i--) {
				var parentGroupRange: wijmo.grid.groupRange = wijmo.grid.groupHelper.getParentGroupRange(leaves, cellRange, i);

				if (!parentGroupRange) {
					return false;
				}

				if (!parentGroupRange.isExpanded) {
					return true;
				}

				cellRange = parentGroupRange.cr;
			}

			return false;
		}

		// level: 1-based level of the cellRange.
		static isParentExpanded(leaves: c1basefield[], cellRange: wijmo.grid.cellRange, level: number): boolean {
			if (level === 1) {
				return true;
			}

			for (var i = level; i > 1; i--) {
				var parentGroupRange: wijmo.grid.groupRange = wijmo.grid.groupHelper.getParentGroupRange(leaves, cellRange, i);

				if (!parentGroupRange || (parentGroupRange && parentGroupRange.isExpanded)) {
					return true;
				}

				cellRange = parentGroupRange.cr;
			}

			return false;
		}
	}
}