/// <reference path="../../../data/src/dataView.ts"/>
/// <reference path="../data/dataViewAdapter.ts"/>
/// <reference path="../data/koDataView.ts"/>

/// <reference path="interfaces.ts"/>
/// <reference path="misc.ts"/>
/// <reference path="wijgrid.ts"/>
/// <reference path="settingsManager.ts"/>
/// <reference path="htmlTableAccessor.ts"/>
/// <reference path="tally.ts"/>

module wijmo.grid {


	var $ = jQuery;

	declare var wijdatasource;

	/** @ignore */
	export interface IParseHandler {
		parseCtx(column: IColumn, value: any, dataItem: any, cell: HTMLTableCellElement, forcedCulture?: GlobalizeCulture): any;
		parseCtxFailed(column: IColumn, value: any, dataItem: any, cell: HTMLTableCellElement, forcedCulture?: GlobalizeCulture): any;

		parse(column: IColumn, value: any, dataItem?: any, domCell?: HTMLTableCellElement, forcedCulture?: GlobalizeCulture): any; // dataItem and domCell arguments are provided at the DOM parsing stage only
		toStr(column: IColumn, value: any, forcedCulture?: GlobalizeCulture): string;
	}

	/** @ignore */
	export class dataViewWrapper {
		private _wijgrid: wijgrid;
		private _domSource: IDOMSource = null;
		private _ignoreAllEvents = false;
		private _ignoreChangeEvent: boolean = false;
		private _ignoreCurrentChangedEvent: boolean = false;
		private _userData: IUserDataArgs = null;
		private _totals = null;
		//private _propChangeListener: propChangeListener;
		private _changeTimer: number = 0;
		private _toDispose = [];

		private _isOwnDataView: boolean = false;
		private _isWijdatasource: boolean = false;
		private _isKODataView: boolean = false;
		private _isDynamicWijdatasource: boolean = false;

		static getMetadata(dataItem: any): IRowAttributes {
			var expando: IRowAttributes;

			if (dataItem && (expando = wijmo.data.Expando.getFrom(dataItem, false)) && (expando = expando[wijmo.grid.EXPANDO])) {
				return expando || null;
			}

			return null;
		}

		static testMetaForClass(meta: IRowAttributes, className: string): boolean {
			if (meta) {
				var ra = meta.rowAttributes;
				return !!(ra && ra["class"] && (ra["class"].indexOf(className) >= 0));
			}

			return false;
		}

		static isHierarchyDetailItem(dataItem: any, metaOut?: { value: IRowAttributes; }): boolean {
			var meta = wijmo.grid.dataViewWrapper.getMetadata(dataItem),
				res = false;

			if (meta) {
				res = wijmo.grid.dataViewWrapper.testMetaForClass(meta, wijgrid.CSS.detailRow);
			}

			if (metaOut) {
				metaOut.value = meta;
			}

			return res;
		}

		constructor(wijgrid: wijgrid) {
			this._wijgrid = wijgrid;
			this._createDataViewWrapper(); // set _dataView
		}

		data(): { totalRows: number; totals: any; emptyData: any; } {
			var dataView = this._getDataViewInst(),
				pagedDataView = wijmo.grid.asPagedDataView(dataView);

			return {
				totalRows: pagedDataView != null
				? pagedDataView.totalItemCount()
				: (dataView.getSource() || []).length,
				totals: this._getTotals(),
				emptyData: this.isBoundedToDOM()
				? this._domSource.emptyData
				: null
			};
		}

		dataView(): wijmo.data.IDataView {
			return this._getDataViewInst();
		}

		dispose() {
			var dataView = this._getDataViewInst();

			//this._propChangeListener.dispose();

			$.each(this._toDispose, function (_, disposable) {
				disposable.dispose();
			});

			if (dataView && this._isOwnDataView) {
				dataView.dispose();
			}

			this._wijgrid.mWijDataView = null;
		}

		ignoreChangeEvent(value?: boolean): boolean {
			if (arguments.length) {
				this._ignoreChangeEvent = (value === true);
			} else {
				return this._ignoreChangeEvent;
			}
		}

		ignoreCurrentChangedEvent(value?: boolean): boolean {
			if (arguments.length) {
				this._ignoreCurrentChangedEvent = (value === true);
			} else {
				return this._ignoreCurrentChangedEvent;
			}
		}

		isDataLoaded(): boolean {
			var dataView = this._getDataViewInst();
			return dataView.isLoaded();
		}

		isOwnDataView(): boolean {
			return this._isOwnDataView;
		}

		getFieldsInfo(): IFieldInfoCollection {
			var dataView = this._getDataViewInst();

			return this._propDescriptorsToFieldsInfo(dataView.getProperties());
		}

		private _propDescriptorsToFieldsInfo(propDescriptors: wijmo.data.IPropertyDescriptor[]): IFieldInfoCollection {
			var result: IFieldInfoCollection = {};

			if (propDescriptors) {
				$.each(propDescriptors, (_, prop: wijmo.data.IPropertyDescriptor) => {
					if (prop.name === "$$hash" || prop.name === "$$hashKey") { // exclude $$hash property (Angular).
						return;
					}

					result[prop.name] = {
						name: prop.name,
						type: prop.type || "string"
					};
				});
			}

			return result;
		}

		isBoundedToDOM(): boolean {
			return this._domSource !== null;
		}

		isKODataView(): boolean {
			return this._isKODataView;
		}

		load(userData: IUserDataArgs): void {
			this._userData = userData;
			var dataView = this._getDataViewInst();

			if (!dataView) {
				this._createDataViewWrapper();
				dataView = this._getDataViewInst();
			}

			this._onDataViewLoading();

			var sm = new wijmo.grid.settingsManager(this._wijgrid);

			if ((userData && userData.forceDataLoad) || this._needToLoad(sm)) {
				var loadParams = sm.MapWGToDV(),
					local = false;

				if (this._isWijdatasource && !this._isDynamicWijdatasource && dataView.isLoaded()) {
					local = true;
				}

				// ** ensure pageIndex
				var pagedDataView = wijmo.grid.asPagedDataView(dataView),
					totalItems = -1;

				// if paging is enabled and dataView provides totalItemCount then ensure that pageIndex is within[0; pageCount) range.
				if (pagedDataView && (loadParams.pageSize >= 0) && ((totalItems = pagedDataView.totalItemCount()) >= 0)) {

					// ** 47731: handle situation when underlying array was changed directly by user.
					if (this._isOwnDataView && !this._isKODataView && !this._isDynamicWijdatasource && (pagedDataView.pageSize() > 0)) { // test if dataView is paged already
						var source = pagedDataView.getSource();
						if (source && (source.length < /*!==*/ totalItems)) {
							totalItems = source.length;
						}
					}
					// 47731 **

					var pageCount = Math.ceil(totalItems / loadParams.pageSize) || 1,
						pageIndex = loadParams.pageIndex;

					if (pageIndex >= pageCount) {
						pageIndex = Math.max(0, pageCount - 1);
					}

					loadParams.pageIndex = pageIndex;
				}
				// ensure pageIndex **

				this.ignoreCurrentChangedEvent(true); // The currentPositionChanged event fires before the change event, stop listening. Listening will  be restored in the _onDataViewReset method.
				dataView.refresh(loadParams, local);
			} else {
				if (this.isDataLoaded()) {
					this._onDataViewLoaded();
					this._onDataViewReset(); // suppose that data is loaded, send notification to wijgrid.
				}
			}
		}

		currentPosition(rowIndex?: number): number {
			var dataView = this._getDataViewInst();

			if (!arguments.length) {
				return dataView.currentPosition();
			}

			this.ignoreCurrentChangedEvent(true);

			try {
				dataView.currentPosition(rowIndex);
			}
			finally {
				this.ignoreCurrentChangedEvent(false);
			}
		}

		getValue(indexOrItem: any, key): any {
			var dataView = this._getDataViewInst();

			if (typeof (key) === "number") {
				key = key + "";
			}

			return dataView.getProperty(indexOrItem, key);
		}

		setValue(indexOrItem: any, key, value): void {
			var dataView = this._getDataViewInst();

			this.ignoreChangeEvent(true);

			try {
				if (typeof (key) === "number") {
					key = key + "";
				}

				dataView.setProperty(indexOrItem, key, value);
			}
			finally {
				this.ignoreChangeEvent(false);
			}
		}

		makeDirty() {
			//this._propChangeListener.removeAll(); // remove old subscriptions
			this._totals = null;
		}

		private _createDataViewWrapper(): void {
			var dataItemToGetProperties,
				data = this._wijgrid.options.data,
				dataView = this._getDataViewInst(),
				isWijdatasource = false;

			if (dataView) {
				return;
			}

			if (!data) { // DOM
				this._domSource = this._processDOM(this._wijgrid.element, this._wijgrid.options.readAttributesFromData);
				data = this._domSource.items;
			}

			isWijdatasource = (typeof (wijdatasource) !== "undefined" && (data instanceof wijdatasource));
			this._isKODataView = data instanceof wijmo.grid.koDataView;
			this._isOwnDataView = ($.isArray(data) || isWijdatasource || this._isKODataView);
			this._isWijdatasource = !!(this._isOwnDataView && isWijdatasource);
			//this._isRemoteWijdatasource = !!(this._isOwnDataView && isWijdatasource && data.proxy);
			this._isDynamicWijdatasource = !!(this._isOwnDataView && isWijdatasource && data.dynamic);

			if (this._isOwnDataView && !this._isKODataView) {
				if (!this._domSource && this._wijgrid.options.readAttributesFromData) {
					this._moveAttributesToExpando(data);
				}

				var tBody = this.isBoundedToDOM() && wijmo.grid.getTableSection(this._wijgrid.element, wijmo.grid.rowScope.body);

				dataView = wijmo.grid.GridLocalDataDataView.create(wijmo.data.asDataView(this._parseOwnData(data, null /* autogenerate columns from data */, tBody)));
			} else {
				// dataView = this._isKODataView ? data : wijmo.grid.GridDataView.create(data); // test for GridDataView instance (wijgridfilter)!
				dataView = data;
			}

			this._wijgrid.mWijDataView = dataView;

			this._toDispose.push(dataView.isLoading.subscribe($.proxy(this._onDataViewLoadingInternal, this)));
			this._toDispose.push(dataView.isLoaded.subscribe($.proxy(this._onDataViewLoadedInternal, this)));
			this._toDispose.push(dataView.subscribe($.proxy(this._onDataViewChangeInternal, this)));
			this._toDispose.push(dataView.currentPosition.subscribe($.proxy(this._onDataViewCurrentChangedInternal, this)));
		}

		// if columns argument is null, then it will be autogenerated from the first data item
		_parseOwnData(data: any[], columns: IColumn[], tbody?: HTMLTableSectionElement) {
			if (data && data.length) {
				var grid = this._wijgrid,
					dataLeaves: IColumn[] = [];

				if (!columns) { // autogenerate
					columns = <IColumn[]> <any> $.extend(true, [], this._wijgrid.options.columns);

					var props = wijmo.data.ArrayDataViewBase._getProps(data[0]) || [],
						fieldsInfo = this._propDescriptorsToFieldsInfo(props);

					grid._prepareColumns(columns, "merge", fieldsInfo, true, true, false);
				}

				wijmo.grid.traverse(columns, (column) => {
					if (grid._needParseColumnDOM(column)) {
						dataLeaves.push(column);
					}
				});

				if (dataLeaves.length && tbody) {
					var readerCulture = grid._getDOMReaderCulture();

					$.each(data, (i, dataItem) => {
						if (!wijmo.grid.dataViewWrapper.isHierarchyDetailItem(dataItem)) {
							wijmo.grid.dataViewWrapper._parseDataItem(grid, dataItem, <HTMLTableRowElement><any>(tbody && tbody.rows[i]), dataLeaves, readerCulture);
						}
					});
				}
			}

			return data;
		}

		static _parseDataItem(parseHandler: IParseHandler, dataItem: any, domRow: HTMLTableRowElement, leaves: IColumn[], culture: GlobalizeCulture): any {
			$.each(leaves, (i, leaf: IColumn) => {
				var value = dataItem[leaf.dataKey],
					newValue = parseHandler.parseCtx(leaf, value, dataItem, <HTMLTableCellElement>domRow.cells[leaf.dataKey], culture);

				if (wijmo.grid.isNaN(newValue)) { // failed
					var domCell: HTMLTableCellElement = null;

					if (domRow) {
						domCell = <HTMLTableCellElement>domRow.cells[leaf.dataKey];
					}

					newValue = parseHandler.parseCtxFailed(leaf, value, dataItem, domCell, culture);
				}

				dataItem[leaf.dataKey] = newValue;
			});

			return dataItem;
		}

		private _getDataViewInst(): wijmo.data.IDataView {
			return this._wijgrid.mWijDataView;
		}

		private _needToLoad(settingsManager): boolean {
			var dataView = this._getDataViewInst();

			if (this._isDynamicWijdatasource || (this._isWijdatasource && !dataView.isLoaded())) {
				return true;
			}

			if (this._isOwnDataView && !this._isWijdatasource && this.isDataLoaded()) { // TFS Issue #36277
				return true;
			}

			if (this.isDataLoaded() || dataView.isLoading()) { // data is loaded already or loading, check reshaping settings
				return !settingsManager.compareSettings();
			}

			return true; // data is not loaded yet or user want to load them manually
		}

		private _validateSettings(settingsManager): void {
			if (!this._isOwnDataView && this._wijgrid.options.allowPaging && ((settingsManager.DVPagingSettings() || {}).pageSize !== (settingsManager.WGPagingSettings() || {}).pageSize)) {
				throw "The pageSize option of the external dataView can't be changed.";
			}
		}

		// ** dataView events handlers

		private _onDataViewLoadingInternal(isLoading: boolean) {
			if (this._ignoreAllEvents) {
				return;
			}

			if (isLoading) {
				if (!this._userData) { // triggered outside
					this._onDataViewLoading();
				}
			}
		}

		private _onDataViewLoadedInternal(isLoaded: boolean) {
			if (this._ignoreAllEvents) {
				return;
			}

			if (isLoaded) {
				this._onDataViewLoaded();
			}
		}

		private _onDataViewChangeInternal(args) {
			var self = this;

			if (this._ignoreAllEvents || this._ignoreChangeEvent) {
				return;
			}

			if (args.changes) {
				if (args.length && args[0].entityState() === "detached")  // check first item only, suppose that other items have the same entityState.
				{
					return; // do not handle the detached items
				}

				$.each(args.changes, function (_, change) {
					switch (change.changeType) {
						case "remove":
							//self._propChangeListener.remove(change.index);
							break;

						case "add":
							//self._propChangeListener.insert(change.index, change.element);
							break;
					}
				});
			}

			this._onDataViewChange.apply(this, arguments);
		}

		private _onDataViewCurrentChangedInternal(): void {
			if (this._ignoreAllEvents || this._ignoreCurrentChangedEvent) {
				return;
			}

			this._onDataViewCurrentChanged.apply(this, arguments);
		}

		// dataView events handlers **

		// ** event handlers

		private _onDataViewReset() {
			try {
				this.ignoreCurrentChangedEvent(false); // restore listening (see the load() method).
				this.makeDirty(); // force to recreate  the _totals and _sharedDataItems fields when the this.data() method will be called.
				this._wijgrid._onDataViewReset(this._userData,
					this._isKODataView); // 47851: recreate columns each time if koDataView is used to handle situation when observable array value was changed completely: viewModel.property([]) -> viewModel.property([a, b, c]).
			}
			finally {
				this._userData = null;
			}
		}

		private _onPropertyChanged(newValue) {
			var self = this;

			if (this._changeTimer > 0) {
				window.clearTimeout(this._changeTimer);
				this._changeTimer = 0;
			}

			if (this._changeTimer != -1) {
				this._changeTimer = window.setTimeout(function () {
					self._changeTimer = -1;

					if (!self._wijgrid.mDestroyed) {
						self._onDataViewChange();
					}

					self._changeTimer = 0;
				}, 100);
			}
		}

		// args can be empty
		private _onDataViewChange(args?: any) {
			this._onDataViewReset(); // force re-rendering. TODO: handle "add", "remove", "modify" etc.
		}

		private _onDataViewCurrentChanged(e, args) {
			this._wijgrid._onDataViewCurrentPositionChanged(e, args);
		}

		private _onDataViewLoading() {
			this._wijgrid._onDataViewLoading();
		}

		private _onDataViewLoaded() {
			this._wijgrid._onDataViewLoaded();
		}

		// event handlers **

		private _getTotals() {
			if (!this._totals) {
				var dataView = this._getDataViewInst();
				this._totals = this._prepareTotals(dataView, this._wijgrid._prepareTotalsRequest());
			}

			if (!this._totals) {
				this._totals = {};
			}

			return this._totals;
		}

		private _prepareTotals(dataView: wijmo.data.IDataView, request: c1field[]) {
			if (!request || request.length === 0) {
				return {};
			}

			var tallies: wijmo.grid.tally[] = [],
				result = {};

			for (var i = 0, len = request.length; i < len; i++) {
				tallies.push(new wijmo.grid.tally(this._wijgrid));
			}

			for (var i = 0, len = dataView.count(); i < len; i++) {
				for (var j = 0, len2 = tallies.length; j < len2; j++) {
					var opt = <IColumn>request[j].options;

					tallies[j].add(this._wijgrid.parse(opt, this.getValue(i, opt.dataKey)));
				}
			}

			for (var i = 0, len = tallies.length; i < len; i++) {
				var opt = <IColumn>request[i].options;
				result[opt.dataKey] = tallies[i].getValue(request[i]);
			}

			return result;
		}

		// ** DOM
		private _processDOM($obj: JQuery, readAttributes: boolean): IDOMSource {
			var result: IDOMSource = {
				items: [],
				emptyData: []
			};

			if (wijmo.grid.getTableSectionLength($obj, rowScope.body) === 1 && $(wijmo.grid.getTableSectionRow($obj, rowScope.body, 0)).hasClass(wijmo.grid.wijgrid.CSS.emptyDataRow)) { // special case - empty data row
				result.emptyData = this._wijgrid._readTableSection($obj, rowScope.body);
			} else { // read data rows
				result.items = this._wijgrid._readTableSection($obj, rowScope.body, readAttributes);
			}

			return result;
		}
		// DOM **


		_moveAttributesToExpando(rawData: any[]) {
			$.each(rawData, function (i, item) {
				var expando = (<any>wijmo).data.Expando.getFrom(item, true),
					rowMeta: IRowAttributes;

				rowMeta = expando[wijmo.grid.EXPANDO] = { cellsAttributes: {}, rowAttributes: {} }; // store attributes within the original item using Expando

				if (item.rowAttributes) {
					rowMeta.rowAttributes = item.rowAttributes;
					delete item.rowAttributes;
				};

				$.each(item, function (dataKey, dataValue) {
					if ($.isArray(dataValue)) {
						rowMeta.cellsAttributes[dataKey] = dataValue[1];
						item[dataKey] = dataValue[0];
					}
				});
			});
		}

		_wrapDataItem(dataItem, dataItemIndex: number): IWrappedDataItem {
			return {
				values: dataItem,
				originalRowIndex: dataItemIndex
			};
		}

		// ** used by c1gridview to update underlying data during callbacks.

		_refreshSilent() {
			// used by c1gridview to refresh underlying data during callbacks.
			var dataView = this._getDataViewInst();

			if (dataView) {
				try {
					this._ignoreAllEvents = true;
					dataView.refresh();
				}
				finally {
					this._ignoreAllEvents = false;
				}
			}
		}

		_unsafeReplace(index: number, newItem: any) {
			var dataView = <wijmo.grid.GridLocalDataDataView><any>this._getDataViewInst();

			if (!(dataView instanceof wijmo.grid.GridLocalDataDataView)) {
				"operation is not supported";
			}

			dataView._unsafeReplace(index, newItem);
		}

		_unsafeSplice(index: number, count: number, item?: any) {
			var dataView = <wijmo.grid.GridLocalDataDataView><any>this._getDataViewInst();

			if (!(dataView instanceof wijmo.grid.GridLocalDataDataView)) {
				"operation is not supported";
			}

			if (arguments.length === 2) {
				dataView._unsafeSplice(index, count);
			} else {
				dataView._unsafeSplice(index, count, item);
			}
		}

		_unsafePush(item: any) {
			var dataView = <wijmo.grid.GridLocalDataDataView><any>this._getDataViewInst();

			if (!(dataView instanceof wijmo.grid.GridLocalDataDataView)) {
				"operation is not supported";
			}

			dataView._unsafePush(item);
		}
		// used by c1gridview to refresh underlying data during callbacks **
	}

	/** @ignore */
	export function asPagedDataView(dataView: wijmo.data.IDataView): wijmo.data.IPagedDataView {
		return dataView && ("pageCount" in dataView)
			? <wijmo.data.IPagedDataView>dataView
			: null;
	}

	/** @ignore */
	export function asEditableDataView(dataView: wijmo.data.IDataView): wijmo.data.IEditableDataView {
		return dataView && ("commitEdit" in dataView)
			? <wijmo.data.IEditableDataView>dataView
			: null;
	}

	/** @ignore */
	export class propChangeListener {
		private _callback;
		private _subscriptions = [];

		constructor(callback) {
			this._callback = callback;
		}

		insert(index: number, dataViewItem): boolean {
			var itemSubscrArray = null,
				self = this;

			$.each(dataViewItem, function (key, value) {
				if (self._isValidPropName(key) && value && $.isFunction(value.subscribe)) {
					itemSubscrArray = itemSubscrArray || [];
					itemSubscrArray.push(value.subscribe(self._callback));
				}
			});

			if (!itemSubscrArray) {
				// we didn't subscribe in fact
				return false;
			}

			if (this._subscriptions.length < index) {
				// inflate the array before inserting
				this._subscriptions.length = index;
			}
			this._subscriptions.splice(index, 0, itemSubscrArray);

			return true;
		}

		remove(index: number): void {
			var subscrArray = this._subscriptions[index];

			if (subscrArray) {
				$.each(subscrArray, function (key, propSubscr) {
					propSubscr.dispose();
					subscrArray[key] = null;
				});
			}

			this._subscriptions[index] = null;
			this._subscriptions.splice(index, 1);
		}

		removeAll() {
			var len, subscr;

			while (len = this._subscriptions.length) {
				this.remove(len - 1);
			}

			this._subscriptions = [];
		}

		dispose() {
			this.removeAll();
		}

		private _isValidPropName(name): boolean {
			if (name && (typeof (name) === "string")) {
				return name.match(/^entityState|jQuery/) === null;
			}

			return true;
		}
	}
}
