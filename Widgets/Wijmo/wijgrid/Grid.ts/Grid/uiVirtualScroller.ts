/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class uiVirtualScroller {
		private _wijgrid: wijgrid;
		//private _fixedAreaHeight: number;
		private _timer = 0;
		private _timeout = 50; // msec

		private _ignoreScrollEvents = false;
		private _postScrolled;
		private _view: wijmo.grid.fixedView;
		private _$scroller: JQuery;
		private _$content: JQuery;
		private _panelInst: wijmo.JQueryUIWidget;
		private _completeListener: Function;
		private _rowOuterHeight: number;
		private _debounceScrolledEvent = true;

		private mScrollLeftPos: number = undefined;
		private mScrollTopPos: number = undefined;

		private _N: number;

		private mVertical: boolean = false;
		private mHorizontal: boolean = false;


		constructor(wijgrid: wijgrid, $content: JQuery, fixedAreaHeight: number, rowOuterHeight: number) {
			this._wijgrid = wijgrid;

			this.mVertical = wijgrid._allowVVirtualScrolling();
			this.mHorizontal = wijgrid._allowHVirtualScrolling();

			//this._fixedAreaHeight = fixedAreaHeight;
			this._$content = $content;

			this._view = <wijmo.grid.fixedView>this._wijgrid._view();

			if (this.mVertical) {
				this._N = this._wijgrid._renderableRowsCount();

				this._rowOuterHeight = rowOuterHeight;

				this._updateContentHeight();

				var height = this._wijgrid.mOuterDiv.height() + this._N * this._rowOuterHeight; // total height
				this._view._splitAreas.sw.height(height);
			}
		}

		scrollToColumn(column: c1basefield, left: number, callback?: Function): void {
			this._debounceScrolledEvent = false; // otherwise sequential method calls will be blocked by timer
			this._completeListener = callback;
			(<any>this._panelInst).hScrollTo(left, false);
		}

		scrollToRow(rowIndex: number, callback?: Function): void {
			this._debounceScrolledEvent = false; // otherwise sequential method calls will be blocked by timer
			this._completeListener = callback;
			(<any>this._panelInst).vScrollTo(rowIndex * (<any>this._panelInst).options.vScroller.scrollSmallChange, true);
		}

		/** calculate content height from row number, row height and fixed area */
		_updateContentHeight(): void {
			// subtract _fixedAreaHeight, otherwise dummy rows will be hanging "under" the grid
			this._$content.height(this._rowOuterHeight * this._N /*- this._fixedAreaHeight*/);
		}

		attach($scroller: JQuery): void {
			this._$scroller = $scroller;
			this._panelInst = $scroller.data("wijmo-wijsuperpanel");

			if (this.mVertical) {
				this._updateContentHeight();
				this._updateVerticalScroller();
			}

			$scroller.bind("wijsuperpanelscrolled.wijgrid", $.proxy(this._onSuperpanelScrolled, this));
			$scroller.bind("wijsuperpanelscrolling.wijgrid", $.proxy(this._onSuperpanelScrolling, this));
			$scroller.bind("wijsuperpanelscrolled.wijgrid", $.proxy(this._onSuperpanelPostScrolled, this)); // manipulate with the _ignoreScrollEvents property.
		}

		_updateVerticalScroller() {
			// if vScroll.scrollValue was an y-offset of the content, rather than a percentage of the offset,
			// smallChange and max would be in pixels
			var max = Math.max(0, this._N - this._view.getVirtualPageSize(false));
			// scrollValue is the index of the first row to show
			this._setVerticalScrollerOptions(1, max);
		}

		_setVerticalScrollerOptions(smallChange: number, max: number) {
			var vScroller = this._panelInst.options.vScroller;
			vScroller.scrollSmallChange = smallChange;
			vScroller.scrollLargeChange = smallChange * 4;

            if (max <= vScroller.scrollLargeChange) {
                vScroller.scrollLargeChange = max;
            }

            max += vScroller.scrollLargeChange - 1;
			vScroller.scrollMax = max;

			this._panelInst.option("vScroller", vScroller);

			// TODO: rewrite!!
			if (!this._view._isNativeSuperPanel() && ((<any>this._panelInst)._scrollDrag != undefined)) {
				var f = (<any>this._panelInst)._fields(),
					vbarContainer = f.vbarContainer,
					vbarDrag = f.vbarDrag;

				(<any>this._panelInst)._scrollDrag("v", vbarContainer, vbarDrag, false);
			}
		}

		_changeVisibleRowsCount(visibleRowsCount: number) {
			this._N = visibleRowsCount;
			this._updateContentHeight();
			this._updateVerticalScroller();
		}

		dispose() {
			this._$scroller.unbind(".wijgrid");
			this._clearTimer();
		}

		private _clearTimer() {
			window.clearTimeout(this._timer);
			this._timer = 0;
		}

		private _onSuperpanelScrolling(e, args) {
			if (this._ignoreScrollEvents || (args.dir === "v" && !this.mVertical) || (args.dir === "h" && !this.mHorizontal)) {
				//if (this._ignoreScrollEvents || (args.dir !== "v")) {
				return;
			}

			if (this._timer === -1) {
				return false;  // cancel while scrolling will not be handled.
			}
		}

		private _onSuperpanelScrolled(e, args) {
			var self = this;

			if (this._ignoreScrollEvents || (args.dir === "v" && !this.mVertical) || (args.dir === "h" && !this.mHorizontal)) {
				//if (this._ignoreScrollEvents || (args.dir !== "v")) {
				return;
			}

			if (this._timer > 0) {
				this._clearTimer();
			}

			if (this._timer !== -1) {
				this._timer = window.setTimeout(function () {
					self._timer = -1; // lock

					var doHScroll = false,
						leftPos = -args.afterPosition.left,
						newRange: renderableColumnsCollection;

					if (args.dir === "h") {
						var newRange = new renderableColumnsCollection();

						self._view._getRenderableColumnsBounds(newRange, leftPos);
						doHScroll = !self._wijgrid._renderableColumnsRange().equals(newRange);
					} else {
						var scrollToIndex = Math.floor(args.newValue / self._panelInst.options.vScroller.scrollSmallChange),
							oldScrollIndex = self._view._bounds.start;

						if (scrollToIndex < 0) {
							scrollToIndex = 0;
						}

						if (scrollToIndex >= self._N) {
							scrollToIndex = self._N - 1;
						}
					}

					if ((scrollToIndex !== oldScrollIndex) || doHScroll) {
						self._debounceScrolledEvent = true;

						if (args.dir === "v" && self.mVertical) {
							self._wijgrid._handleVerticalVirtualScrolling(scrollToIndex, null, $.proxy(self._verticalScrollingCompleted, self));
						} else {
							self._wijgrid._handleHorizontalVirtualScrolling(newRange, $.proxy(self._horizontalScrollingCompleted, self));
						}
					} else {
						self._debounceScrolledEvent = true;
						self._log();
						self._clearTimer(); // unlock
					}

				}, this._debounceScrolledEvent ? this._timeout : 0);
			}
		}

		private _verticalScrollingCompleted(scrollIndex: number): void {
			this._wijgrid._trackScrollingIndex(scrollIndex);

			this._log();

			if (this._completeListener) {
				this._completeListener();
				this._completeListener = null;
			}

			this._clearTimer(); // unlock
		}

		private _horizontalScrollingCompleted(): void {
			this._log();

			if (this._completeListener) {
				this._completeListener();
				this._completeListener = null;
			}

			this._clearTimer(); // unlock
		}

		private _onSuperpanelPostScrolled() {
			if ($.isFunction(this._postScrolled)) {
				this._postScrolled.apply(this, arguments);
			}
		}

		private _log() {
			//if (window.console) {
			//	var bounds = this._wijgrid._view()._bounds;
			//	window.console.log("bounds: [" + bounds.start + ", " + bounds.end + "], scrollTo: " + bounds.start);
			//}
		}
	}
}