/// <reference path="interfaces.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class cellStyleFormatterHelper {
		private _wijgrid: wijgrid;

		constructor(wijgrid: wijgrid) {
			if (!wijgrid) {
				throw "invalid arguments";
			}

			this._wijgrid = wijgrid;
		}

		format($cell: JQuery, cellIndex: number, column: c1basefield, rowInfo: IRowInfo, state: wijmo.grid.renderState, cellAttr?, cellStyle?) {
			var $rs = wijmo.grid.renderState,
				$rt = wijmo.grid.rowType,
				rowType = rowInfo.type,
				groupRowCellInfo = null;

			if (cellIndex < this._wijgrid._virtualLeaves().length) {
				column = null; //TODO: check
			}

			if (rowType === $rt.groupHeader || rowType === $rt.groupFooter) {
				column = null;

				if (cellAttr && (groupRowCellInfo = cellAttr.groupInfo)) {
					column = this._wijgrid._leaves()[groupRowCellInfo.leafIndex]; // replace "column" with the one associated with the $cell's content
					//delete cellAttr.groupInfo;
				}
			}

			var args: ICellStyleFormaterArgs = {
				$cell: $cell,
				state: state,
				row: rowInfo,
				column: column && <IColumn>column.options, //TODO: check
				_cellIndex: cellIndex,
				_purpose: groupRowCellInfo
				? groupRowCellInfo.purpose
				: undefined
			};

			if (state & $rs.rendering) {
				this._renderingStateFormatter(args, cellAttr, cellStyle);
			} else {
				this._currentStateFormatter(args, state & $rs.current);
				//hoveredStateFormatter(args, state & $rs.hovered);
				this._selectedStateFormatter(args, state & $rs.selected);

				if (rowType !== $rt.header) {
					if ((state & $rs.current) || (state & $rs.selected)) {
						args.$cell.addClass(this._wijgrid.options.wijCSS.stateDefault); // make bootstrap happy
					} else {
						args.$cell.removeClass(this._wijgrid.options.wijCSS.stateDefault);
					}
				}
			}

			if ($.isFunction(this._wijgrid.options.cellStyleFormatter)) {
				this._wijgrid.options.cellStyleFormatter(args);
			}
		}

		// private ---

		private _renderingStateFormatter(args: ICellStyleFormaterArgs, cellAttr, cellStyles) {
			var $rt = wijmo.grid.rowType,
				key, value,
				leaf = args.column,
				rowType: wijmo.grid.rowType = args.row.type,
				defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._wijgrid.options.wijCSS;

			switch (rowType) {
				case $rt.header:
					args.$cell.addClass(defCSS.TH + " " + wijCSS.wijgridTH);
					break;

				default:
					args.$cell.addClass(defCSS.TD + " " + wijCSS.wijgridTD);
			}

			if ((rowType & $rt.data) && leaf && leaf.textAlignment) {
				// set text alignment
				switch (leaf.textAlignment.toLowerCase()) {
					case "left":
						args.$cell.addClass(defCSS.cellAlignLeft + " " + wijCSS.wijgridCellAlignLeft);
						break;

					case "right":
						args.$cell.addClass(defCSS.cellAlignRight + " " + wijCSS.wijgridCellAlignRight);
						break;

					case "center":
						args.$cell.addClass(defCSS.cellAlignCenter + " " + wijCSS.wijgridCellAlignCenter);
						break;
				}
			}

			// copy attributes
			if (cellAttr) {
				for (key in cellAttr) {
					if (cellAttr.hasOwnProperty(key)) {
						value = cellAttr[key];

						if ((key === "groupInfo" || key === "colSpan" || key === "rowSpan") && !(value > 1)) {
							continue;
						}

						if (key === "class") {
							args.$cell.addClass(value);
						} else {
							args.$cell.attr(key, value);
						}
					}
				}
			}

			// copy inline css
			if (cellStyles) {
				for (key in cellStyles) {
					if (cellStyles.hasOwnProperty(key)) {
						if (key === "paddingLeft") { // groupIndent
							args.$cell.children("." + defCSS.cellContainer).css(key, cellStyles[key]);
							continue;
						}
						args.$cell.css(key, cellStyles[key]);
					}
				}
			}

			if (args._cellIndex < this._wijgrid._virtualLeaves().length) {
				args.$cell
					.attr({ "role": "rowheader", "scope": "row" })
					.addClass(wijCSS.stateDefault + " " + wijCSS.content + " " + defCSS.rowHeader + " " + wijCSS.wijgridRowHeader);
			} else {
				switch (rowType) {
					case ($rt.header):
						args.$cell.attr({ "role": "columnheader", "scope": "col" });
						break;
					case ($rt.footer):
						args.$cell.attr({ "role": "gridcell", "scope": "col" });
						break;
					default:
						args.$cell.attr("role", "gridcell");
				}
			}

			if (rowType & $rt.data) {
				if (args._cellIndex >= 0 && leaf) {
					args.$cell.attr("headers", (<any>window).escape(leaf.headerText));

					if (leaf.readOnly) {
						args.$cell.attribute("aria-readonly", true);
					}

					if (leaf.dataIndex >= 0) {
						args.$cell.addClass("wijdata-type-" + wijmo.grid.getDataType(leaf));
					}
				}
			}

			if (rowType === $rt.groupHeader || rowType === $rt.groupFooter) {
				// append wijdata-type class only to the aggregate cells of the group row, not grouped cells.
				if (leaf && args._purpose === wijmo.grid.groupRowCellPurpose.aggregateCell) {
					args.$cell.addClass("wijdata-type-" + wijmo.grid.getDataType(leaf));
				}
			}
		}

		private _currentStateFormatter(args, add) {
			var $rt = wijmo.grid.rowType,
				defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._wijgrid.options.wijCSS;

			if (add) {
				args.$cell.addClass(wijCSS.stateActive);

				if (args.row.type === $rt.header) {
					args.$cell.addClass(defCSS.currentHeaderCell + " " + wijCSS.wijgridCurrentHeaderCell);
				} else {
					args.$cell.addClass(defCSS.currentCell + " " + wijCSS.wijgridCurrentCell);
				}
			} else {
				args.$cell.removeClass(wijCSS.stateActive);

				if (args.row.type === $rt.header) {
					args.$cell.removeClass(defCSS.currentHeaderCell + " " + wijCSS.wijgridCurrentHeaderCell);
				} else {
					args.$cell.removeClass(defCSS.currentCell + " " + wijCSS.wijgridCurrentCell);
				}
			}
		}

		private _hoveredStateFormatter(args, add) {
			if (add) {
			} else {
			}
		}

		private _selectedStateFormatter(args, add) {
			var wijCSS = this._wijgrid.options.wijCSS;

			if (add) {
				args.$cell
					.addClass(wijCSS.stateHighlight)
					.attribute("aria-selected", "true");
			} else {
				args.$cell
					.removeClass(wijCSS.stateHighlight)
					.removeAttribute("aria-selected");
			}
		}

		// --- private
	}
}
