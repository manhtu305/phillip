/// <reference path="interfaces.ts"/>
/// <reference path="misc.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export function getTableSection(table: JQuery, scope: wijmo.grid.rowScope): HTMLTableSectionElement;
	/** @ignore */
	export function getTableSection(table: HTMLTableElement, scope: wijmo.grid.rowScope): HTMLTableSectionElement;
	/** @ignore */
	export function getTableSection(table: any, scope: wijmo.grid.rowScope): HTMLTableSectionElement {
		if (table && !table.nodeType) {
			table = table[0];
		}

		if (table) {
			switch (scope) {
				case wijmo.grid.rowScope.head:
					return table.tHead;

				case wijmo.grid.rowScope.body:
					if (table.tBodies) {
						return table.tBodies[0] || null;
					}
					break;

				case wijmo.grid.rowScope.foot:
					return table.tFoot;

				default:
					return table;
			}
		}

		return null;
	}

	/** @ignore */
	export function getTableSectionLength(table: JQuery, scope: wijmo.grid.rowScope): number;
	/** @ignore */
	export function getTableSectionLength(table: HTMLTableElement, scope: wijmo.grid.rowScope): number;
	/** @ignore */
	export function getTableSectionLength(table: any, scope: wijmo.grid.rowScope): number {
		var section: HTMLTableSectionElement;

		if (table && !table.nodeType) {
			table = table[0]; // jQuery
		}

		return (table && (section = this.getTableSection(table, scope)))
			? section.rows.length
			: 0;
	}

	/** @ignore */
	export function getTableSectionRow(table: JQuery, scope: wijmo.grid.rowScope, rowIndex: number): HTMLTableRowElement;
	/** @ignore */
	export function getTableSectionRow(table: HTMLTableElement, scope: wijmo.grid.rowScope, rowIndex: number): HTMLTableRowElement;
	/** @ignore */
	export function getTableSectionRow(table, scope: wijmo.grid.rowScope, rowIndex: number): HTMLTableRowElement {
		var section: HTMLTableSectionElement;

		if (table && !table.nodeType) {
			table = table[0]; // jQuery
		}

		return (table && (section = this.getTableSection(table, scope)))
			? <HTMLTableRowElement>(section.rows[rowIndex] || null)
			: null;
	}

	/** @ignore */
	export function readTableSection(table: JQuery, scope: wijmo.grid.rowScope, decodeHTML: boolean, trim: trimMethod, readAttributes?: boolean): any[];
	/** @ignore */
	export function readTableSection(table: HTMLTableElement, scope: wijmo.grid.rowScope, decodeHTML: boolean, trim: trimMethod, readAttributes?: boolean): any[];
	/** @ignore */
	export function readTableSection(table, scope: wijmo.grid.rowScope, decodeHTML: boolean, trim: trimMethod, readAttributes?: boolean): any[] {
		var result = [],
			prevent = function (attrName: string): boolean {
				attrName = attrName.toLowerCase();
				return attrName === "rowspan" || attrName === "colspan";
			},
			section: HTMLTableSectionElement;

		if (table && !table.nodeType) {
			table = table[0]; // jQuery
		}

		if (table && (section = this.getTableSection(table, scope))) {
			for (var i = 0, len = section.rows.length; i < len; i++) {
				var domRow = <HTMLTableRowElement>section.rows[i],
					row = [],
					rowAttributes: IRowAttributes;

				if (readAttributes) {
					var expando = (<any>wijmo).data.Expando.getFrom(row, true);

					rowAttributes = expando[wijmo.grid.EXPANDO] = {
						cellsAttributes: {},
						rowAttributes: wijmo.grid.getAttributes(domRow) || {}
					};
				}

				for (var j = 0, len2 = domRow.cells.length; j < len2; j++) {
					var value = wijmo.grid.getContent(<HTMLElement>domRow.cells[j], decodeHTML, trim);

					row[j] = value;

					if (readAttributes) {
						rowAttributes.cellsAttributes[j] = wijmo.grid.getAttributes(<HTMLElement>domRow.cells[j], prevent) || {};
					}
				}

				result[i] = row;
			}
		}

		return result;
	}

	/** @ignore */
	export function determineSection(cell: HTMLTableCellElement): wijmo.grid.rowScope {
		var element: HTMLTableSectionElement = <HTMLTableSectionElement>cell.parentNode.parentNode;

		switch (element.tagName.toLowerCase()) {
			case "thead":
				return wijmo.grid.rowScope.head;

			case "tbody":
				return wijmo.grid.rowScope.body;

			case "tfoot":
				return wijmo.grid.rowScope.foot;
		}

		return null;
	}

	/** @ignore */
	export class htmlTableAccessor {
		private _table: HTMLTableElement;
		private _width = 0;
		private _offsets: { cellIdx: number; colIdx: number; }[][];

		constructor(domTable: HTMLTableElement, skipOffsets?: boolean, ensureTBody?: boolean, ensureColgroup?: boolean) {
			this._table = domTable;
			this._offsets = [];

			if (ensureColgroup) { // important: colGroup must preceed tBody in a table
				this.ensureColGroup();
			}

			if (ensureTBody) {
				this.ensureTBody();
			}

			if (!skipOffsets) {
				this._buildOffsets();
			}
		}

		element(): HTMLTableElement {
			return this._table;
		}

		width() {
			return this._width;
		}

		getCellIdx(colIdx: number, rowIdx: number): number {
			return (colIdx < this._width && rowIdx >= 0 && rowIdx < this._offsets.length)
				? this._offsets[rowIdx][colIdx].cellIdx
				: -1;
		}

		getColumnIdx(cell: HTMLTableCellElement): number;
		getColumnIdx(cellIdx: number, rowIdx: number): number;
		getColumnIdx(cellIdx: any, rowIdx?: any): number {
			if (typeof (cellIdx) !== "number") { // domCell
				var domCell = cellIdx;

				cellIdx = domCell.cellIndex;
				rowIdx = domCell.parentNode.rowIndex;
			}

			return (cellIdx < this._width)
				? this._offsets[rowIdx][cellIdx].colIdx
				: -1;
		}

		clearContent() {
			if (this._table) {
				while (this._table.firstChild) {
					this._table.removeChild(this._table.firstChild);
				}

				this.ensureColGroup();
				this.ensureTBody();
			}
		}

		clearSection(scope: wijmo.grid.rowScope): void {
			var start, end,
				section = wijmo.grid.getTableSection(this._table, scope);

			switch (scope) {
				case wijmo.grid.rowScope.body:
					start = this.getSectionLength(wijmo.grid.rowScope.table);
					end = start + this.getSectionLength(scope) - 1;
					break;

				case wijmo.grid.rowScope.foot:
					start = this.getSectionLength(wijmo.grid.rowScope.table) + this.getSectionLength(wijmo.grid.rowScope.head);
					end = start + this.getSectionLength(scope) - 1;
					break;

				default: // header or whole table
					start = 0;
					end = this.getSectionLength(scope) - 1;
			}

			// update DOM
			while (section.rows.length) {
				$(section.rows[0]).remove();
			}

			// update offsets
			this._offsets.splice(start, end - start + 1);
		}

		getSectionLength(scope: wijmo.grid.rowScope): number {
			return wijmo.grid.getTableSectionLength(this._table, scope);
		}

		getSectionRow(rowIndex: number, scope: wijmo.grid.rowScope): HTMLTableRowElement {
			return wijmo.grid.getTableSectionRow(this._table, scope, rowIndex);
		}

		// iterates through the table rows using natural cells order
		forEachColumnCellNatural(columnIdx: number, callback: (cell: HTMLTableCellElement, rowIndex: number, param: any) => boolean, param: any): boolean {
			for (var i = 0, len = this._table.rows.length; i < len; i++) {
				var row = <HTMLTableRowElement>this._table.rows[i];

				if (columnIdx < row.cells.length) {
					var result = callback(<HTMLTableCellElement>row.cells[columnIdx], i, param);

					if (result !== true) {
						return result;
					}
				}
			}

			return true;
		}

		// iterates through the table rows using colSpan\rowSpan offsets
		forEachColumnCell(columnIdx: number, callback: (cell: HTMLTableCellElement, rowIndex: number, param: any) => boolean, param: any): boolean {
			for (var i = 0, len = this._offsets.length; i < len; i++) {
				var row = <HTMLTableRowElement>this._table.rows[i],
					offsetCellIdx = this.getCellIdx(columnIdx, i);

				if (offsetCellIdx >= 0) {
					var result = callback(<HTMLTableCellElement>row.cells[offsetCellIdx], i, param);

					if (result !== true) {
						return result;
					}
				}
			}

			return true;
		}

		// iterates throw the cells of a table row
		forEachRowCell(rowIndex: number, callback: (cell: HTMLTableCellElement, cellIndex: number, param: any) => boolean, param: any): boolean {
			var row: HTMLTableRowElement = <HTMLTableRowElement>this._table.rows[rowIndex];

			for (var i = 0, len = row.cells.length; i < len; i++) {
				var result = callback(<HTMLTableCellElement>row.cells[i], i, param);

				if (result !== true) {
					return result;
				}
			}

			return true;
		}

		colGroupTag(): HTMLElement {
			var cgs = this._table.getElementsByTagName("colgroup");
			return (<HTMLElement>(cgs && cgs[0])) || null;
		}

		colTags(): HTMLTableColElement[] {
			var colGroup = this.colGroupTag();
			return <HTMLTableColElement[]>((colGroup && colGroup.getElementsByTagName("col")) || []);
		}

		ensureTBody(): HTMLTableSectionElement {
			return <HTMLTableSectionElement>((this._table.tBodies && this._table.tBodies[0]) || this._table.appendChild(document.createElement("tbody")));
		}

		ensureTHead(): HTMLTableSectionElement {
			return (this._table.tHead && this._table.tHead[0]) || this._table.createTHead();
		}

		ensureTFoot(): HTMLTableSectionElement {
			return (this._table.tFoot && this._table.tFoot[0]) || this._table.createTFoot();
		}

		ensureColGroup(): HTMLElement {
			var colGroup = this._table.getElementsByTagName("colgroup");
			return <HTMLElement>((colGroup && colGroup[0]) || this._table.appendChild(document.createElement("colgroup")));
		}

		appendCol(domCol?: HTMLTableColElement /*opt*/): HTMLTableColElement {
			var colGroup = this.ensureColGroup();
			return <HTMLTableColElement>((domCol && colGroup.appendChild(domCol)) || colGroup.appendChild(document.createElement("col")));
		}

		removeOffset(idx: number): void {
			if (idx >= 0 && idx < this._offsets.length) {

				if (idx < 0 || (!idx && idx !== 0)) {
					idx = this._offsets.length - 1; // last row
				}

				this._offsets.splice(idx, 1);
			}
		}

		insertOffset(idx: number): void {
			var row, i;

			if (this._width > 0) {
				row = [];

				for (i = 0; i < this._width; i++) {
					row.push({ cellIdx: i, colIdx: i });
				}

				if (idx < 0 || (!idx && idx !== 0)) {
					idx = this._offsets.length; // append row
				}

				this._offsets.splice(idx, 0, row);
			}
		}

		rebuildOffsets() {
			this._offsets = [];
			this._width = 0;
			this._buildOffsets();
		}

		private _buildOffsets() {
			var rowSpan = [];

			for (var i = 0, len = this._table.rows.length; i < len; i++) {
				var rowOffsets: { cellIdx: number; colIdx: number; }[] = [],
					jOffset = 0,
					row = <HTMLTableRowElement>this._table.rows[i];

				this._offsets[i] = rowOffsets;

				for (var j = 0, len2 = row.cells.length; j < len2; j++, jOffset++) {
					var cell = <HTMLTableCellElement>row.cells[j];

					// process rowspan
					for (; rowSpan[jOffset] > 1; jOffset++) {
						rowSpan[jOffset]--;
						rowOffsets[jOffset] = { cellIdx: -1, colIdx: -1 };
					}

					if (!(rowSpan[jOffset] > 1)) {
						rowSpan[jOffset] = cell.rowSpan;
					}

					rowOffsets[jOffset] = { cellIdx: j, colIdx: -1 };
					rowOffsets[j].colIdx = jOffset;

					// process colspan
					var cs = cell.colSpan;
					for (; cs > 1; cs--) {
						rowOffsets[++jOffset] = { cellIdx: -1, colIdx: -1 };
					}
				}

				var rowSpanLen = rowSpan.length;
				for (; jOffset < rowSpanLen; jOffset++) {
					rowSpan[jOffset]--;
					rowOffsets[jOffset] = { cellIdx: -1, colIdx: -1 };
				}

				this._width = Math.max(this._width, rowSpanLen);
			}
		}

	}
}