/// <reference path="interfaces.ts"/>
/// <reference path="c1basefield.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export interface ICommandClickEventArgs {
		row: IRowInfo;
	}

	// An "abstract" class without a factory.
	export class c1buttonbasefield extends c1basefield {
		public options: IC1ButtonBaseFieldOptions;

		public static test(options: IColumn): boolean {
			return !!(options && options.buttonType);
		}

		_provideDefaults() {
			super._provideDefaults();

			wijmo.grid.shallowMerge(this.options, c1buttonbasefield.prototype.options);

			(<IColumn>this.options).dataKey = null;
		}

		_createCommand(container: JQuery, cmd: ICommandButton, row: IRowInfo, clickCallback: (e: JQueryEventObject, arg: ICommandClickEventArgs) => void): void {
			var text = cmd.text;

			if (wijmo.grid.validDataKey(cmd.textDataKey) && row.data) {
				var dataVal = row.data[cmd.textDataKey];
				if ($.isFunction(dataVal)) {
					dataVal = dataVal();
				}

				text = wijmo.grid.stringFormat(text, dataVal);
			}

			var btnOptions: ICommandButtonExt = <ICommandButtonExt><any>$.extend({}, {
				mobile: this._owner()._isMobileEnv(),
				//text: cmd.text,
				text: text,
				iconClass: cmd.iconClass,
				disabled: this._owner().options.disabled,
				click: function (e) {
					var processDefault = true;

					if ($.isFunction(cmd.click)) {
						var res = cmd.click.apply(this, [e, <IButtonCommandEventArgs>{
							row: row,
							column: this.options
						}]);

						processDefault = (res !== false) && !e.isDefaultPrevented(); // let user to cancel the action
					}

					if (processDefault && clickCallback) {
						clickCallback.apply(this, [e, <ICommandClickEventArgs>{ row: row }]);
					}

					e.preventDefault(); // prevent # beging added to url.
				}
			});

			var btnContainer = $("<div />");
			container.append(btnContainer);

			switch (this.options.buttonType) {
				case "link":
					btnContainer.wijgridcommandlink(btnOptions);
					break;

				case "imageButton":
					btnContainer.wijgridcommandimagebutton(btnOptions);
					break;

				case "button":
					btnContainer.wijgridcommandbutton(btnOptions);
					break;

				case "image":
					btnContainer.wijgridcommandimage(btnOptions);
					break;

				default:
					throw "Unknown buttonType";
			}
		}
	}

	export class c1buttonbasefield_options extends c1basefield_options implements IC1ButtonBaseFieldOptions {
		/** Gets or sets the type of the button in the column. Possible values are "link", "button", "imageButton", "image".
		  * @example
		  * $("#element").wijgrid({ columns: [{ buttonType: "button" }]});
		  */
		buttonType = "button";
	};

	c1buttonbasefield.prototype.options = wijmo.grid.extendWidgetOptions(c1basefield.prototype.options, new c1buttonbasefield_options());
}


module wijmo.grid {


	var $ = jQuery;

	export class c1buttonfield extends c1buttonbasefield {
		public static test(options: IColumn): boolean {
			return !!(c1buttonbasefield.test(options) || options.command);
		}

		/** @ignore */
		public static WIDGET_NAME = "wijmo.c1buttonfield";

		public options: IC1ButtonFieldOptions;

		constructor(wijgrid: wijgrid, options, widgetName?: string) {
			super(wijgrid, options, widgetName || c1buttonfield.WIDGET_NAME);
		}

		_provideDefaults() {
			super._provideDefaults();

			wijmo.grid.shallowMerge(this.options, c1basefield.prototype.options);

			this.options.command = this.options.command || <any>{};
			wijmo.grid.shallowMerge(this.options.command, c1buttonfield.prototype.options.command);
		}

		//#region rendering
		_setupDataCell(container: JQuery, formattedValue, row: IRowInfo) {
			this._createCommand(container, this.options.command, row, null);
		}
		//#endregion
	}

	export class c1buttonfield_options extends c1buttonbasefield_options implements IC1ButtonFieldOptions {
		/** Represents options of a command button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       buttonType: "link",
		  *       command: {
		  *          text: "myCommand",
		  *          click: function (e, args) { 
		  *             alert("clicked!");
		  *          }
		  *       }
		  *    }]
		  * });
		 */
		command: ICommandButton = undefined;
	};

	c1buttonfield.prototype.options = wijmo.grid.extendWidgetOptions(c1buttonbasefield.prototype.options, new c1buttonfield_options());

	wijmo.grid.registerColumnFactory((grid, column) => {
		if (c1buttonfield.test(column)) {
			return new c1buttonfield(grid, column);
		}

		return null;
	});
}


module wijmo.grid {


	var $ = jQuery;

	export class c1commandbuttonfield extends c1buttonbasefield {
		public static test(options: IColumn): boolean {
			return !!((options.showDeleteButton && options.showDeleteButton !== false) // its better to use a ("propName" in options) statement here, but there is will be no static type checking.
				|| (options.showEditButton && options.showEditButton !== false)
				|| options.cancelCommand || options.deleteCommand || options.editCommand || options.updateCommand);
		}


		/** @ignore */
		public static WIDGET_NAME = "wijmo.c1commandbuttonfield";

		public options: IC1CommandButtonFieldOptions;

		constructor(wijgrid: wijgrid, options, widgetName?: string) {
			super(wijgrid, options, widgetName || c1commandbuttonfield.WIDGET_NAME);
		}

		_provideDefaults() {
			super._provideDefaults();

			wijmo.grid.shallowMerge(this.options, c1commandbuttonfield.prototype.options);

			this.options.cancelCommand = this.options.cancelCommand || <any>{};
			wijmo.grid.shallowMerge(this.options.cancelCommand, c1commandbuttonfield.prototype.options.cancelCommand);

			this.options.deleteCommand = this.options.deleteCommand || <any>{};
			wijmo.grid.shallowMerge(this.options.deleteCommand, c1commandbuttonfield.prototype.options.deleteCommand);

			this.options.editCommand = this.options.editCommand || <any>{};
			wijmo.grid.shallowMerge(this.options.editCommand, c1commandbuttonfield.prototype.options.editCommand);

			this.options.updateCommand = this.options.updateCommand || <any>{};
			wijmo.grid.shallowMerge(this.options.updateCommand, c1commandbuttonfield.prototype.options.updateCommand);
		}

		//#region rendering
		_setupDataCell(container: JQuery, formattedValue, row: IRowInfo) {
			var opt = this.options,
				grid = this._owner();

			if (row.state & wijmo.grid.renderState.editing) {
				if (opt.showEditButton) {
					this._createCommand(container, opt.updateCommand, row, (e: JQueryEventObject, arg: ICommandClickEventArgs) => {
						grid.updateRow();
					});

					container.append(document.createTextNode("\u00A0"));

					this._createCommand(container, opt.cancelCommand, row, (e: JQueryEventObject, arg: ICommandClickEventArgs) => {
						grid.cancelRowEditing();
					});
				}
			} else {
				if (opt.showEditButton) {
					this._createCommand(container, opt.editCommand, row, (e: JQueryEventObject, arg: ICommandClickEventArgs) => {
						grid.editRow(row.dataItemIndex);
					});
				}

				if (opt.showDeleteButton) {
					if (opt.showEditButton) {
						container.append(document.createTextNode("\u00A0"));
					}

					this._createCommand(container, opt.deleteCommand, row, (e: JQueryEventObject, arg: ICommandClickEventArgs) => {
						grid.deleteRow(row.dataItemIndex);
					});
				}
			}

			container.addClass("wijmo-wijgrid-innercell-command");
		}
		//#endregion
	}


	export class c1commandbuttonfield_options extends c1buttonbasefield_options implements IC1CommandButtonFieldOptions {
		/** Gets or sets a value indicating whether a Delete button is displayed in a command column.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       showDeleteButton: true
		  *    }]
		  * });
		*/
		showDeleteButton = false;

		/** Gets or sets a value indicating whether an Edit button is displayed in a command column.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       showEditButton: true
		  *    }]
		  * });
		*/
		showEditButton = false;

		/** Represents options of a Cancel command button. 
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       cancelCommand: {
		  *          text: "Cancel!"
		  *       }
		  *    }]
		  * });
		  */
		cancelCommand: ICommandButton = {
			text: "Cancel",
			iconClass: "ui-icon-close",
			click: undefined,
			textDataKey: undefined
		};

		/** Represents options of a Delete command button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       deleteCommand: {
		  *          text: "Delete!"
		  *       }
		  *    }]
		  * });
		  */
		deleteCommand: ICommandButton = {
			text: "Delete",
			iconClass: "ui-icon-trash",
			click: undefined,
			textDataKey: undefined
		};

		/** Represents options of a Delete command button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       deleteCommand: {
		  *          text: "Edit!"
		  *       }
		  *    }]
		  * });
		  */
		editCommand: ICommandButton = {
			text: "Edit",
			iconClass: "ui-icon-pencil",
			click: undefined,
			textDataKey: undefined
		};

		/** Represents options of a Delete command button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       deleteCommand: {
		  *          text: "Update!"
		  *       }
		  *    }]
		  * });
		  */
		updateCommand: ICommandButton = {
			text: "Update",
			iconClass: "ui-icon-disk",
			click: undefined,
			textDataKey: undefined
		};
	}

	c1commandbuttonfield.prototype.options = wijmo.grid.extendWidgetOptions(c1buttonbasefield.prototype.options, new c1commandbuttonfield_options());

	wijmo.grid.registerColumnFactory((grid, column) => {
		if (c1commandbuttonfield.test(column)) {
			return new c1commandbuttonfield(grid, column);
		}

		return null;
	});
}

