/// <reference path="interfaces.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />

module wijmo.grid {


	var $ = jQuery;

	/** An object that represents selection in the grid. You do not need to create instances of this class. */
	export class selection {
		private _wijgrid: wijgrid;
		private _updates = 0;
		private __anchorCell: wijmo.grid.cellInfo;
		private _addedCells: cellInfoOrderedCollection;
		private _removedCells: cellInfoOrderedCollection;
		private _selectedCells: cellInfoOrderedCollection;
		private _addedDuringCurTransactionCells: cellInfoOrderedCollection;
		private _selectedColumns = null; // ?
		private _selectedRows: selectedRowsAccessor = null; // ?

		// n: none (0), c: extendToColumn (1), r: extendToRow (2)
		//
		//              extendMode
		// selectionMode| n | c | r
		// ------------------------
		// singlecell   | n | n | n
		// singlecolumn | c | c | c
		// singlerow    | r | r | r
		// singlerange  | n | c | r
		// multicolumn  | c | c | c
		// multirow     | r | r | r
		// multirange   | n | c | r
		private _extend_rules = {
			"singlecell": [0, 0, 0],
			"singlecolumn": [1, 1, 1],
			"singlerow": [2, 2, 2],
			"singlerange": [0, 1, 2],
			"multicolumn": [1, 1, 1],
			"multirow": [2, 2, 2],
			"multirange": [0, 1, 2]
		};

		/** Creates an object that represents selection in the grid. Normally you do not need to use this method.
		  * @example
		  * var selection = new wijmo.grid.selection(wijgrid);
		  * @param {wijmo.wijgrid} wijgrid wijgrid
		  * @returns {wijmo.grid.selection} Object that represents selection in the grid
		  */
		constructor(wijgrid: wijgrid) {

			if (!wijgrid) {
				throw "invalid arguments";
			}

			this._wijgrid = wijgrid;

			this._addedCells = new cellInfoOrderedCollection(wijgrid);
			this._removedCells = new cellInfoOrderedCollection(wijgrid);
			this._selectedCells = new cellInfoOrderedCollection(wijgrid);
			this._addedDuringCurTransactionCells = new cellInfoOrderedCollection(wijgrid);
		}

		/** Gets a read-only collection of the selected cells.
			* @example
			* var selectedCells = selectionObj.selectedCells();
			* for (var i = 0, len = selectedCells.length(); i < len; i++) {
			*	alert(selectedCells.item(i).value().toString());
			* }
			* @returns {wijmo.grid.cellInfoOrderedCollection} A read-only collection of the selected cells.
			*/
		selectedCells(): wijmo.grid.cellInfoOrderedCollection {
			return this._selectedCells;
		}

		/** Gets a read-only collection of the selected rows.
			* @example
			* alert(selectionObj.selectedRows().length());
			* @returns {wijmo.grid.selectedRowsAccessor} A read-only collection of the selected rows.
			*/
		selectedRows(): selectedRowsAccessor {
			if (!this._selectedRows) {
				this._selectedRows = new selectedRowsAccessor(this._wijgrid);
			}

			return this._selectedRows;
		}

		/** Adds a column range to the current selection.
			* Usage:
			* 1. addColumns(0)
			* 2. addColumns(0, 2)
			* @example
			* // Add the first column to the current selection.
			* selectionObj.addColumns(0);
			* @remarks
			* The result depends upon the chosen selection mode in the grid. For example, if current selection mode does not allow multiple selection the previous selection will be removed.
			* @param {Number} start The index of the first column to select.
			* @param {Number} end The index of the last column to select.
			*/
		addColumns(start: number, end?: number /* opt */): void {
			if (!end && end !== 0) {
				end = start;
			}

			this.addRange(start, 0, end, 0xFFFFFF);
		}

		/** Adds a cell range to the current selection.
		  * @example
		  * // Add a cell range (0,0)-(1,1) to the selection
		  * selectionObj.addRange(new wijmo.grid.cellInfoRange(0, 0, 1, 1));
		  * @remarks
		  * The result depends upon the chosen selection mode in the grid. For example, if current selection mode does not allow multiple selection the previous selection will be removed.
		  * @param {wijmo.grid.cellInfoRange} cellRange Cell range to select.
		  */
		addRange(cellRange: wijmo.grid.cellInfoRange): void;
		/** Adds a cell range to the current selection.
		  * @example
		  * // Add a cell range (0,0)-(1,1) to the selection
		  * selectionObj.addRange(0, 0, 1, 1);
		  * @remarks
		  * The result depends upon the chosen selection mode in the grid. For example, if current selection mode does not allow multiple selection the previous selection will be removed.
		  * @param {Number} x0 The x-coordinate that represents the top left cell of the range.
		  * @param {Number} y0 The y-coordinate that represents the top left cell of the range.
		  * @param {Number} x1 The x-coordinate that represents the bottom right cell of the range.
		  * @param {Number} y1 The y-coordinate that represents the bottom right cell of the range.
		  */
		addRange(x0: number, y0: number, x1: number, y1: number): void;
		/** @ignore */
		addRange(cellRange: any /* x0 */, y0?: number /* opt */, x1?: number /* opt */, y1?: number /* opt */): void {
			if (!cellRange && (arguments.length === 1)) {
				throw "invalid argument";
			}

			var range: wijmo.grid.cellInfoRange = (arguments.length === 4)
				? new wijmo.grid.cellInfoRange(new wijmo.grid.cellInfo(cellRange /* ie x0 */, y0), new wijmo.grid.cellInfo(x1, y1))
				: cellRange._clone();

			range._normalize();

			if (!range._isValid()) {
				throw "invalid argument";
			}

			this.beginUpdate();

			this._startNewTransaction(this._wijgrid._currentCell());
			this._selectRange(range, false, true, wijmo.grid.cellRangeExtendMode.none, null);

			this.endUpdate();
		}

		/** Adds a row range to the current selection.
		  * Usage:
		  * 1. addRows(0)
		  * 2. addRows(0, 2)
		  * @example
		  * // Add the first row to the selection.
		  * selectionObj.addRows(0);
		  * @remarks
		  * The result depends upon the chosen selection mode in the grid. For example, if current selection mode does not allow multiple selection the previous selection will be removed.
		  * @param {Number} start The index of the first row to select.
		  * @param {Number} end The index of the last row to select.
		  */
		addRows(start: number, end?: number /* opt */): void {
			if (!end && end !== 0) {
				end = start;
			}

			this.addRange(0, start, 0xFFFFFF, end);
		}

		/**
		* Removes a range of cells from the current selection.
		* @example
		* // Remove a cell range (0,0)-(1,1) from the selection
		* selectionObj.removeRange(new wijmo.grid.cellRange(0, 0, 1, 1));
		* @remarks
		* The result depends upon the chosen selection mode in the grid.
		* @param {wijmo.grid.cellInfoRange} cellRange Cell range to remove.
		*/
		removeRange(cellRange: wijmo.grid.cellInfoRange): void;
		/**
		* Removes a range of cells from the current selection.
		* @example
		* // Remove a cell range (0,0)-(1,1) from the selection
		* selectionObj.removeRange(0, 0, 1, 1);
		* @remarks
		* @param {wijmo.grid.cellInfoRange} cellRange Cell range to remove.
		* @param {Number} x0 The x-coordinate that represents the top left cell of the range.
		* @param {Number} y0 The y-coordinate that represents the top left cell of the range.
		* @param {Number} x1 The x-coordinate that represents the bottom right cell of the range.
		* @param {Number} y1 The y-coordinate that represents the bottom right cell of the range.
		*/
		removeRange(x0: number, y0: number, x1: number, y1: number): void;
		/** @ignore */
		removeRange(cellRange /* x0 */, y0?: number /* opt */, x1?: number /* opt */, y1?: number /* opt */): void {

			if (!cellRange && (arguments.length === 1)) {
				throw "invalid argument";
			}

			var range: wijmo.grid.cellInfoRange = (arguments.length === 4)
				? new wijmo.grid.cellInfoRange(new wijmo.grid.cellInfo(<number>cellRange, y0), new wijmo.grid.cellInfo(x1, y1))
				: cellRange._clone();

			range._normalize();

			if (!range._isValid()) {
				throw "invalid argument";
			}

			this.beginUpdate();

			this._startNewTransaction(this._wijgrid._currentCell());
			this._clearRange(range, wijmo.grid.cellRangeExtendMode.none);

			this.endUpdate();
		}

		/**
		* Removes a range of columns from the current selection.
		* Usage:
		* 1. removeColumns(0)
		* 2. removeColumns(0, 2)
		* @example
		* // Remove the first columm from the selection.
		* selectionObj.removeColumns(0);
		* @remarks
		* The result depends upon the chosen selection mode in the grid.
		* @param {Number} start The index of the first column to remove.
		* @param {Number} end The index of the last column to remove.
		*/
		removeColumns(start: number, end?: number /* opt */): void {
			if (!end && end !== 0) {
				end = start;
			}

			this.removeRange(start, 0, end, 0xFFFFFF);
		}

		/** Removes a range of rows from the current selection.
		* Usage:
		* 1. removeRows(0)
		* 2. removeRows(0, 2)
		* @example
		* // Remove the first row from the selection.
		* selectionObj.removeRows(0);
		* @remarks
		* The result depends upon the chosen selection mode in the grid.
		* @param {Number} start The index of the first row to remove.
		* @param {Number} end The index of the last row to remove.
		*/
		removeRows(start: number, end?: number /* opt */): void {
			if (!end && end !== 0) {
				end = start;
			}

			this.removeRange(0, start, 0xFFFFFF, end);
		}

		/**
		* Clears the selection.
		* @example
		* // Clear the selection.
		* selectionObj.clear();
		*/
		clear(): void {
			this.beginUpdate();

			this._removedCells._clear();
			this._removedCells._addFrom(this._selectedCells);

			this.endUpdate();
		}

		/**
		* Selects all the cells in a grid.
		* @example
		* selectionObj.selectAll();
		* @remarks
		* The result depends upon the chosen selection mode in the grid. For example, if the selection mode is set to "singleCell", then only the top left cell will be selected.
		*/
		selectAll(): void {
			this.beginUpdate();

			this._selectRange(this._wijgrid._getDataCellsRange(dataRowsRangeMode.sketch), false, false, wijmo.grid.cellRangeExtendMode.none, null);

			this.endUpdate();
		}

		/**
		* Begins the update. Any changes won't take effect until endUpdate() is called.
		* @example
		* selectionObj.beginUpdate();
		*/
		beginUpdate(): void {
			this._updates++;
		}

		/**
		* Ends the update. The pending changes are executed and the selectionChanged event is raised.
		* @example
		* selectionObj.endUpdate();
		* @param {JQueryEventObject} e The original event object passed through by wijgrid.
		*/
		endUpdate(e?: JQueryEventObject): void {
			if (this._updates > 0) {
				this._updates--;

				if (this._updates === 0) {
					this.doSelection(); // values must be clipped before this step

					if (this._addedCells.length() || this._removedCells.length()) {

						if (this._selectedColumns !== null) {
							this._selectedColumns.UnderlyingDataChanged(); // notify
						}

						if (this._selectedRows !== null) {
							this._selectedRows._setDirty(); // notify
						}

						this._wijgrid._trigger("selectionChanged", e, { addedCells: this._addedCells, removedCells: this._removedCells });
					}

					this._addedCells = new wijmo.grid.cellInfoOrderedCollection(this._wijgrid);
					this._removedCells._clear();
				}
			}
		}

		// * internal

		_multipleRangesAllowed() {
			var mode = this._wijgrid.options.selectionMode;
			return (mode && ((mode = mode.toLowerCase()) === "multicolumn" || mode === "multirow" || mode === "multirange"));
		}

		_multipleEntitiesAllowed() {
			var mode = this._wijgrid.options.selectionMode;
			return (mode && ((mode = mode.toLowerCase()) === "multicolumn" || mode === "multirow" || mode === "multirange"
				|| mode === "singlerange")); // entity == cell
		}

		_anchorCell(): wijmo.grid.cellInfo {
			return this.__anchorCell;
		}

		_startNewTransaction(dataCellInfo: wijmo.grid.cellInfo) {
			if (dataCellInfo) {
				this.__anchorCell = dataCellInfo._clone();
				this._addedDuringCurTransactionCells = new wijmo.grid.cellInfoOrderedCollection(this._wijgrid);
			}
		}

		_clearRange(range: wijmo.grid.cellInfoRange, extendMode: wijmo.grid.cellRangeExtendMode) {
			var selectionMode = this._wijgrid.options.selectionMode.toLowerCase(),
				dataRange = this._wijgrid._getDataCellsRange(dataRowsRangeMode.sketch);

			if (range._isValid() && (selectionMode !== "none") && (this._selectedCells.length() > 0)) {
				var rangeToClear = range._clone();

				rangeToClear._normalize();
				rangeToClear._clip(dataRange);

				if (!range._isValid()) {
					return;
				}

				// extend
				rangeToClear._extend(this._extend_rules[selectionMode][extendMode], dataRange);

				this.beginUpdate();

				// remove selected cells only, do not use doRange(rangeToClear, false) here.
				for (var i = 0, len = this._selectedCells.length(); i < len; i++) {
					var cellInfo = this._selectedCells.item(i);

					if (rangeToClear._containsCellInfo(cellInfo)) {
						this._removedCells._add(cellInfo);
					}
				}

				this.endUpdate();
			}
		}

		_selectRange(range: wijmo.grid.cellInfoRange, ctrlKey: boolean, shiftKey: boolean, extendMode: wijmo.grid.cellRangeExtendMode, endPoint: wijmo.grid.cellInfo): void {
			var selectionMode = this._wijgrid.options.selectionMode.toLowerCase(),
				rangeToSelect: wijmo.grid.cellInfoRange,
				dataRange = this._wijgrid._getDataCellsRange(dataRowsRangeMode.sketch);

			if ((selectionMode !== "none") && range._isValid()) {
				rangeToSelect = range._clone();
				rangeToSelect._normalize();
				rangeToSelect._clip(dataRange);

				if (!rangeToSelect._isValid()) {
					return;
				}

				this.beginUpdate();

				if (!this._multipleRangesAllowed()) {
					this.clear();
				}
				else {
					if (ctrlKey || shiftKey) {
						if (shiftKey) {
							this._removedCells._clear();
							this._removedCells._addFrom(this._addedDuringCurTransactionCells);
						}
					}
					else {
						this.clear();
					}
				}

				// truncate range by selectionMode
				switch (selectionMode) {
					case "singlecell":
					case "singlecolumn":
					case "singlerow":
						rangeToSelect = (endPoint === null)
						? new wijmo.grid.cellInfoRange(rangeToSelect.topLeft(), rangeToSelect.topLeft()) // top-left cell only is taken into consideration.
						: new wijmo.grid.cellInfoRange(endPoint, endPoint);
						break;
				}

				// extend
				rangeToSelect._extend(this._extend_rules[selectionMode][extendMode], dataRange);

				// do selection
				this.doRange(rangeToSelect, true);

				this.endUpdate();
			}
		}

		_ensureSelection() {
			var view = this._wijgrid._view(),
				prevRowIndex = -2,
				rowInfo: IRowInfo;

			for (var i = 0; i < this._selectedCells.length(); i++) {
				var cellInfo = this._selectedCells.item(i);

				if (cellInfo._isRendered()) {
					if (prevRowIndex !== cellInfo.rowIndex()) {
						rowInfo = cellInfo.row();
						prevRowIndex = cellInfo.rowIndex();
					}

					this.selectCell(cellInfo, rowInfo, view, true);
				}
			}
		}

		_ensureSelectionInRow(sketchRowIndex: number) {
			var view = this._wijgrid._view();

			if (view._isRowRendered(sketchRowIndex) >= 0) {
				var rowInfo = view._getRowInfoBySketchRowIndex(sketchRowIndex, false),
					selectedCells = this.selectedCells();

				if (rowInfo && selectedCells && (selectedCells.length() > 0)) {
					rowInfo.$rows.children("td, th").each((i: number, cell) => {
						var idx = selectedCells.indexOf(i, sketchRowIndex);
						if (idx >= 0) {
							this.selectCell(selectedCells.item(idx), rowInfo, view, true);
						}
					});
				}
			}
		}

		// * internal

		// * private

		private doSelection() {
			var view = this._wijgrid._view(), wijCSS = this._wijgrid.options.wijCSS,
				rowInfo: IRowInfo,
				prevRowIndex = -1;

			for (var i = 0, len = this._removedCells.length(); i < len; i++) {
				var cellInfo = this._removedCells.item(i);

				if (this._addedCells.indexOf(cellInfo) < 0) {
					if (prevRowIndex !== cellInfo.rowIndex()) {
						rowInfo = cellInfo.row();
						prevRowIndex = cellInfo.rowIndex();
					}

					this.selectCell(cellInfo, rowInfo, view, false);
					if (rowInfo && rowInfo.$rows) {
						rowInfo.$rows.removeClass(wijCSS.stateDefault + " " + wijCSS.stateHover);
					}

					this._selectedCells._remove(cellInfo);
					this._addedDuringCurTransactionCells._remove(cellInfo);
				}
				else {
					this._removedCells._removeAt(i);
					i--;
					len--;
				}
			}

			prevRowIndex = -1;

			for (var i = 0, len = this._addedCells.length(); i < len; i++) {
				var cellInfo = this._addedCells.item(i),
					index = this._selectedCells.indexOf(cellInfo);

				if (index < 0) {
					if (prevRowIndex !== cellInfo.rowIndex()) {
						rowInfo = cellInfo.row();
						prevRowIndex = cellInfo.rowIndex();
					}

					this.selectCell(cellInfo, rowInfo, view, true);

					this._selectedCells._insertUnsafe(cellInfo, ~index);
					this._addedDuringCurTransactionCells._add(cellInfo);
				}
				else {
					this._addedCells._removeAt(i);
					i--;
					len--;
				}
			}
		}

		private selectCell(cellInfo: wijmo.grid.cellInfo, rowInfo: IRowInfo, view: wijmo.grid.baseView, select: boolean) {
			if (cellInfo._isRendered()) {
				var bounds = this._wijgrid._viewPortBounds(),
					cell = view.getCell(cellInfo.cellIndexAbs(), cellInfo.rowIndexAbs());

				if (cell) {
					var $cell = $(cell),
						state = view._changeCellRenderState($cell, wijmo.grid.renderState.selected, select === true);

					this._wijgrid.mCellStyleFormatter.format($cell, cellInfo.cellIndex(), cellInfo.columnInst(), rowInfo, state);
				}
			}
		}

		private doRange(range: wijmo.grid.cellInfoRange, add: boolean) {
			var x0 = range.topLeft().cellIndex(),
				y0 = range.topLeft().rowIndex(),
				x1 = range.bottomRight().cellIndex(),
				y1 = range.bottomRight().rowIndex(),
				view = this._wijgrid._view(),
				renderBounds = this._wijgrid._viewPortBounds();

			if (add) {
				var cnt = this._addedCells.length(),
					rows = this._wijgrid._rows();

				for (var row = y0; row <= y1; row++) {
					var rowInfo = view._getRowInfoBySketchRowIndex(row);

					if (rowInfo && rowInfo.type & wijmo.grid.rowType.data) {
						for (var col = x0; col <= x1; col++) {
							var cell = new wijmo.grid.cellInfo(col, row);

							if (cnt === 0) {
								this._addedCells._appendUnsafe(cell);
							}
							else {
								this._addedCells._add(cell);
							}
						}
					}
				}
			}
			else {
				var cnt = this._removedCells.length();

				for (var row = y0; row <= y1; row++) {
					for (var col = x0; col <= x1; col++) {
						var cell = new wijmo.grid.cellInfo(col, row);

						if (cnt === 0) {
							this._removedCells._appendUnsafe(cell);
						}
						else {
							this._removedCells._add(cell);
						}
					}
				}
			}
		}

		// * private
	}

	/** An ordered read-only collection of wijmo.grid.cellInfo objects */
	export class cellInfoOrderedCollection {
		private _wijgrid: wijgrid;
		private _list: wijmo.grid.cellInfo[];

		/**
		* Creates an ordered read-only collection of wijmo.grid.cellInfo objects. Normally you do not need to use this method.
		* @example
		* var collection = new wijmo.grid.cellInfoOrderedCollection(wijgrid);
		* @param {wijmo.wijgrid} wijgrid wijgrid
		* @returns {wijmo.grid.cellInfoOrderedCollection}  An ordered read-only collection of wijmo.grid.cellInfo objects
		*/
		constructor(wijgrid: wijgrid) {
			if (!wijgrid) {
				throw "invalid arguments";
			}

			this._wijgrid = wijgrid;
			this._list = [];
		}

		// public

		/** Gets an item at the specified index.
		  * @example
		  * var cellInfoObj = collection.item(0);
		  * @param {Number} index The zero-based index of the item to get.
		  * @returns {wijmo.grid.cellInfo} The wijmo.grid.cellInfo object at the specified index.
		  */
		item(index: number): wijmo.grid.cellInfo {
			return this._list[index];
		}

		/** Gets the total number of the items in the collection.
		  * @example
		  * var len = collection.length();
		  * @returns {Number} The total number of the items in the collection.
		  */
		length(): number {
			return this._list.length;
		}

		/**
		* Returns the zero-based index of specified collection item.
		* @example
		* var index = collection.indexOf(new wijmo.grid.cellInfo(0, 0));
		* @param {wijmo.grid.cellInfo} cellInfo A cellInfo object to return the index of.
		* @returns {Number} The zero-based index of the specified object, or -1 if the specified object is not a member of the collection.
		*/
		indexOf(cellInfo: wijmo.grid.cellInfo): number;
		/**
		* Returns the zero-based index of specified collection item.
		* @example
		* var index = collection.indexOf(0, 0);
		* @param {Number} cellIndex A zero-based cellIndex component of the wijmo.grid.cellInfo object to return the index of.
		* @param {Number} rowIndex A zero-based rowIndex component of the wijmo.grid.cellInfo object to return the index of.
		* @returns {Number} The zero-based index of the specified object, or -1 if the specified object is not a member of the collection.
		*/
		indexOf(cellIndex: number, rowIndex: number): number;
		/** @ignore */
		indexOf(cellIndex, rowIndex?: number): number {
			if (arguments.length === 1) {
				rowIndex = cellIndex.rowIndex();
				cellIndex = cellIndex.cellIndex();
			}

			var lo = 0,
				hi = this._list.length - 1,
				med, current, cmp;

			while (lo <= hi) {
				med = lo + ((hi - lo) >> 1);
				current = this._list[med];

				cmp = current.rowIndex() - rowIndex;
				if (cmp === 0) {
					cmp = current.cellIndex() - cellIndex;
				}

				if (cmp < 0) {
					lo = med + 1;
				} else {
					if (cmp > 0) {
						hi = med - 1;
					} else {
						return med;
					}
				}
			}

			return ~lo;
		}

		/** @ignore */
		toString(): string {
			var val = "";

			for (var i = 0, len = this._list.length; i < len; i++) {
				val += this._list[i].toString() + "\n";
			}

			return val;
		}

		// public *

		// internal

		_add(value: wijmo.grid.cellInfo): boolean {
			var idx = this.indexOf(value);
			if (idx < 0) {
				this._list.splice(~idx, 0, value);
				value._setGridView(this._wijgrid);
				return true;
			}

			return false;
		}

		_addFrom(addFrom: cellInfoOrderedCollection) {
			if (addFrom) {
				var fromLen = addFrom.length(),
					thisLen = this._list.length,
					i;

				if (thisLen === 0) {
					this._list.length = fromLen;

					for (i = 0; i < fromLen; i++) {
						this._list[i] = addFrom.item(i);
						this._list[i]._setGridView(this._wijgrid);
					}
				} else {
					for (i = 0; i < fromLen; i++) {
						this._add(addFrom.item(i));
					}
				}
			}
		}

		_appendUnsafe(value: wijmo.grid.cellInfo) {
			this._list[this._list.length] = value;
			value._setGridView(this._wijgrid);
		}

		_insertUnsafe(value: wijmo.grid.cellInfo, index: number) {
			this._list.splice(index, 0, value);
		}

		_clear() {
			this._list.length = 0;
		}

		_remove(value: wijmo.grid.cellInfo): boolean {
			var idx = this.indexOf(value);
			if (idx >= 0) {
				this._list.splice(idx, 1);
				return true;
			}

			return false;
		}

		_removeAt(index: number) {
			this._list.splice(index, 1);
		}

		_getCellsIndicies(): number[] {
			var cells: number[] = [],
				list = this._list,
				len = list.length;

			if (len) {
				var dict: { [index: number]: number } = {};

				for (var i = 0; i < len; i++) {
					var cellIndex = list[i].cellIndex();
					dict[cellIndex] = cellIndex;
				}

				for (var key in dict) {
					if (dict.hasOwnProperty(key)) {
						cells.push(dict[key]);
					}
				}
			}

			return cells;
		}

		_getSelectedRowsIndicies(): number[] {
			var rows: number[] = [],
				list = this._list,
				len = list.length;

			if (len) {
				var dict: { [index: number]: number } = {};

				for (var i = 0; i < len; i++) {
					var rowIndex = list[i].rowIndex();
					dict[rowIndex] = rowIndex;
				}

				for (var key in dict) {
					if (dict.hasOwnProperty(key)) {
						rows.push(dict[key]);
					}
				}
			}

			return rows;
		}

		_rectangulate(): wijmo.grid.cellInfoRange {
			var x0 = 0xFFFFFFFF,
				y0 = 0xFFFFFFFF,
				x1 = 0,
				y1 = 0,
				len = this._list.length;

			if (len) {
				for (var i = 0; i < len; i++) {
					var cellInfo = this._list[i];

					x0 = Math.min(x0, cellInfo.cellIndex());
					y0 = Math.min(y0, cellInfo.rowIndex());
					x1 = Math.max(x1, cellInfo.cellIndex());
					y1 = Math.max(y1, cellInfo.rowIndex());
				}

				return new wijmo.grid.cellInfoRange(new wijmo.grid.cellInfo(x0, y0), new wijmo.grid.cellInfo(x1, y1));
			}

			return null;
		}

		// internal *
	}

	/** Provides a read-only access to rows which cells are selected. */
	export class selectedRowsAccessor {
		private mWijgrid: wijgrid;
		private mLength = 0;
		private mDirty = true;
		private mRowIndicies: number[] = [];

		/**
		* Constructor. Normally you do not need to use this method.
		* @example
		* var collection = new wijmo.grid.selectedRowsAccessor(wijgrid);
		* @param {wijmo.wijgrid} wijgrid wijgrid
		* @returns {wijmo.grid.selectedRowsAccessor} A new instance of the selectedRowsAccessor class.
		*/
		constructor(wijgrid: wijgrid) {
			this.mWijgrid = wijgrid;
		}

		/** Gets the total number of the items in the collection.
		  * @example
		  * var len = $("#demo").wijgrid("selection").selectedRows().length();
		  * @returns {Number} The total number of the items in the collection.
		  */
		public length(): number {
			this._ensure();
			return this.mLength;
		}

		/** Gets an object representing grid's row at the specified index.
		  * @example
		  * var row = $("#demo").wijgrid("selection").selectedRows().item(0);
		  * @param {Number} index The zero-based index of the item to get.
		  * @returns {wijmo.grid.IRowInfo} An object representing grid's row at the specified index.
		  */
		public item(index: number): wijmo.grid.IRowInfo {
			this._ensure();

			var rowInfo = this.mWijgrid._view()._getRowInfoBySketchRowIndex(this.mRowIndicies[index]);

			return rowInfo;
		}

		private _ensure(): void {
			if (this.mDirty) {
				this.mRowIndicies = this.mWijgrid.selection().selectedCells()._getSelectedRowsIndicies();
				this.mLength = this.mRowIndicies.length;
				this.mDirty = false;
			}
		}

		/** @ignore */
		_setDirty(): void {
			this.mDirty = true;
		}
	}
}



