/// <reference path="c1basefield.ts"/>
/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class c1groupedfield extends widgetEmulator implements IDragnDropElement {
		public static WIDGET_NAME = "wijmo.c1groupedfield";
		public options: IColumn;

		private mWijgrid: wijgrid;

		constructor(wijgrid: wijgrid, options: IColumn, element: JQuery, widgetName?: string) {
			super(options, widgetName || c1groupedfield.WIDGET_NAME);
			this.mWijgrid = wijgrid;

			this._attachToElement(element);

			this._createContent();

			if (this.mWijgrid.options.allowColMoving) {
				this.mWijgrid._UIDragndrop(true).attach(this);
			}
		}

		_destroy() {
			if (this.mWijgrid._UIDragndrop()) {
				this.mWijgrid._UIDragndrop(false).detach(this);
			}

			this.mWijgrid = null;

			super._destroy.apply(this, arguments);
		}

		_owner(): wijgrid {
			return this.mWijgrid;
		}

		_createContent() {
			var defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this.mWijgrid.options.wijCSS,
				$closeButton = $("<span class=\"" + defCSS.groupAreaButtonClose + " " + wijCSS.wijgridGroupAreaButtonClose + " " + wijCSS.stateDefault + " " + wijCSS.cornerAll + "\"><span class=\"" + wijCSS.icon + " " + wijCSS.iconClose + "\"></span></span>")
					.bind("click." + this.widgetName, this, this._onCloseClick);

			this.element
				.addClass(defCSS.groupAreaButton + " " + wijCSS.wijgridGroupAreaButton + " " + wijCSS.stateDefault + " " + wijCSS.cornerAll)
				.html(this.options.headerText || "")
				.prepend($closeButton);

			if (this._isSortableUI()) {
				this.element.bind("click." + this.widgetName, this, $.proxy(this._onSortableElementClick, this));
			}

			if (this._isSortable()) {
				// sorting icon
				var generalSortClass = defCSS.groupAreaButtonSort + " " + wijCSS.wijgridGroupAreaButtonSort + " " + wijCSS.icon;

				switch (this.options.sortDirection) {
					case "ascending":
						this.element.append($("<span class=\"" + generalSortClass + " " + wijCSS.iconArrowUp + "\"></span>"));
						break;

					case "descending":
						this.element.append($("<span class=\"" + generalSortClass + " " + wijCSS.iconArrowDown + "\"></span>"));
						break;
				}
			}

		}

		_onCloseClick(args) {
			var column: c1groupedfield = args.data;

			if (!column.options.disabled) {
				column.mWijgrid._handleUngroup(column);
			}

			return false;
		}

		// taken from the wijgridfield
		_onSortableElementClick(args: JQueryEventObject) {
			var column: c1groupedfield = args.data;

			if (column.options.disabled) {
				return false;
			}

			if (column.options.allowSort) {
				column.mWijgrid._handleSort(<c1basefield><any>column, args.ctrlKey); // dangerous casting
			}

			return false;
		}

		//#region UI actions
		_canDrag(): boolean {
			return this.options.allowMoving === true;
		}

		_canDropTo(column: c1basefield): boolean {
			return (column instanceof c1groupedfield);
		}

		_canDropToGroupArea(): boolean {
			// The rightmost column header in the the group area can't be dragged to the end of the group area again.
			return (this.options.groupedIndex !== this.mWijgrid._groupedLeaves().length - 1);
		}

		_isSortable(): boolean {
			return wijmo.grid.c1field.test(<IColumn>this.options);
		}

		_isSortableUI(): boolean {
			return this.mWijgrid.options.allowSorting && this.options.allowSort && this._isSortable();
		}

		//#endregion
	}
}