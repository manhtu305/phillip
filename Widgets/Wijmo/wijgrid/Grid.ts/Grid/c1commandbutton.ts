/// <reference path="../../../Base/jquery.wijmo.widget.ts"/>
/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export interface ICommandButtonExt extends ICommandButton {
		mobile?: boolean;
		disabled?: boolean;
	}

	/** @ignore */
	export class wijgridcommandbuttonbase extends wijmoWidget {
		options: ICommandButtonExt;

		private mBtnElement: JQuery;

		_create() {
			this.element
				.css("display", "inline")
				.addClass((<any>this.options).wijCSS.widget);
		}

		_destroy() {
			this.mBtnElement = null;
		}

		_init() {
			this.mBtnElement = this._createElement(this.options)
				.addClass("wijmo-wijgrid-commandbutton")
				.click((e: JQueryEventObject) => {
					if (!this.options.disabled && $.isFunction(this.options.click)) {
						this.options.click.call(self, e);
					}
				});

			this.element.append(this.mBtnElement);

			this._attachWidget(this.mBtnElement, this.options);
		}

		_setOption(key: string, value: any) {
			var oldValue = this.options[key];

			super._setOption.apply(this, [key, value]);

			if ((oldValue !== value) && (key === "disabled")) {
				this._onDisabledChanged(value === true);
			}
		}

		_createElement(settings: ICommandButtonExt): JQuery {
			throw "not implemented";
		}

		_attachWidget(element: JQuery, settings: ICommandButtonExt): void {
			throw "not implemented";
		}

		_btnElement(): JQuery {
			return this.mBtnElement;
		}

		_onDisabledChanged(value: boolean): void {
		}
	}

	/** @ignore */
	export class wijgridcommandbuttonbase_options implements wijmo.grid.ICommandButtonExt {
		text: string = undefined;
		click: (e: JQueryEventObject, arg: IButtonCommandEventArgs) => void = undefined;
		iconClass: string = undefined;
		disabled: boolean = false;
		textDataKey: string = undefined;
	}

	wijgridcommandbuttonbase.prototype.options = wijmo.grid.extendWidgetOptions(wijmoWidget.prototype.options, new wijgridcommandbuttonbase_options());

	$.wijmo.registerWidget("wijgridcommandbuttonbase", wijgridcommandbuttonbase.prototype);

	/** @ignore */
	export class wijgridcommandlink extends wijgridcommandbuttonbase {
		_createElement(settings: ICommandButtonExt): JQuery {
			var anchor = $("<a href=\"#\" />")
				.text(settings.text || "");

			if (settings.disabled) {
				anchor.prop("disabled", "disabled");
			}

			return anchor;
		}

		_attachWidget(element: JQuery, settings: ICommandButtonExt): void {
		}

		_onDisabledChanged(value: boolean): void {
			var btn = this._btnElement();

			if (btn) {
				btn.prop("disabled", value);
			}
		}
	}

	$.wijmo.registerWidget("wijgridcommandlink", $.wijmo.wijgridcommandbuttonbase, wijgridcommandlink.prototype);

	/** @ignore */
	export class wijgridcommandbutton extends wijgridcommandbuttonbase {
		_createElement(settings: ICommandButtonExt): JQuery {
			return $("<input type=\"button\" />")
				.val(settings.text);
		}

		_attachWidget(element: JQuery, settings: ICommandButtonExt): void {
			element
				.button({ disabled: settings.disabled });
		}

		_onDisabledChanged(value: boolean): void {
			var btn = this._btnElement();

			if (btn) {
				btn.button({ disabled: value });
			}
		}
	}

	$.wijmo.registerWidget("wijgridcommandbutton", $.wijmo.wijgridcommandbuttonbase, wijgridcommandbutton.prototype);

	/** @ignore */
	export class wijgridcommandimagebutton extends wijgridcommandbuttonbase {
		_createElement(settings: ICommandButtonExt): JQuery {
			var button: JQuery,
				hasText = !!settings.text;

			if (settings.mobile) {
				button = $("<input type=\"button\" />");

				if (hasText) {
					button.val(settings.text);
				}
			} else {
				button = $("<button />");

				if (hasText) {
					button.text(settings.text);
				} else {
					button.html("&nbsp;");
				}
			}

			return button;
		}

		_attachWidget(element: JQuery, settings: ICommandButtonExt): void {
			var hasText = !!settings.text,
				options: any = {
					disabled: settings.disabled
				};

			if (settings.mobile) {
				options.icon = settings.iconClass;

				if (!hasText) {
					options.iconpos = "notext";
				}
			} else {
				options.icons = {
					primary: settings.iconClass
				};

				options.text = hasText;
			}

			element.button(options);
		}

		_onDisabledChanged(value: boolean): void {
			var btn = this._btnElement();

			if (btn) {
				btn.button({ disabled: value });
			}
		}
	}

	$.wijmo.registerWidget("wijgridcommandimagebutton", $.wijmo.wijgridcommandbuttonbase, wijgridcommandimagebutton.prototype);

	/** @ignore */
	export class wijgridcommandimage extends wijgridcommandbuttonbase {
		_createElement(settings: ICommandButtonExt): JQuery {
			var button: JQuery;

			if (settings.mobile) {
				button = $("<input type=\"button\" />");
			} else {
				button = $("<button />").html("&nbsp;")
			}

			return button;
		}

		_attachWidget(element: JQuery, settings: ICommandButtonExt): void {
			var options: any = {
				disabled: settings.disabled
			};

			if (settings.mobile) {
				options.icon = settings.iconClass;
				options.iconpos = "notext";
			} else {
				options.icons = {
					primary: settings.iconClass
				};
				options.text = false;
			}

			element.button(options);
		}

		_onDisabledChanged(value: boolean): void {
			var btn = this._btnElement();

			if (btn) {
				btn.button({ disabled: value });
			}
		}
	}

	$.wijmo.registerWidget("wijgridcommandimage", $.wijmo.wijgridcommandbuttonbase, wijgridcommandimage.prototype);
}

interface JQuery {
	wijgridcommandlink: JQueryWidgetFunction;
	wijgridcommandbutton: JQueryWidgetFunction;
	wijgridcommandimagebutton: JQueryWidgetFunction;
	wijgridcommandimage: JQueryWidgetFunction;
}