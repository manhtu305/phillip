﻿/// <reference path="../../../data/src/dataView.ts"/>
/// <reference path="interfaces.ts"/>
/// <reference path="wijgrid.ts"/>
/// <reference path="sketchTable.ts"/>
/// <reference path="misc.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class hierarchyBuilder {
		public static build(grid: wijgrid) {
			if (grid._hasDetail()) {
				var sketch = grid.mSketchTable;

				sketch.ensureNotLazy(); // make sure all rows are created

				grid._clearDetails();

				if (!grid._isRoot()) { // make a tree
					var mi = grid._masterInfo(),
						mdi = mi.master.details()[mi.dataRowIndex];

					mdi._setDetails(grid.details());
				}

				for (var i = 0; i < sketch.count(); i++) { // process data rows
					var dataRow = <SketchDataRow>sketch.row(i),
						detailRow = this.buildDetailRow(grid),
						detail = new hierarchyNode(grid, dataRow.originalRowIndex);

					grid.details().push(detail);

					dataRow.rowType |= rowType.dataHeader;

					if (!detail.isExpanded()) {
						dataRow.extInfo.state |= renderStateEx.collapsed;
						detailRow.extInfo.state |= renderStateEx.hidden;
					}

					sketch.insert(i + 1, detailRow);
					i++;
				}
			}
		}

		private static buildDetailRow(wijgrid: wijgrid): SketchDetailRow {
			var row = new SketchDetailRow(null);

			// Add row header cells first
			$.each(wijgrid._virtualLeaves(), () => {
				row.add(HtmlCell.nbsp());
			});

			// A detail grid will be placed here
			row.add(new HtmlCell("", {
				colSpan: wijgrid._visibleLeaves().length - wijgrid._virtualLeaves().length
			}));

			return row;
		}
	}

	/** @ignore */
	export class detailInstantiator {
		static instantiateIn(container: JQuery, master: wijgrid, masterKey: IDataKeyArray, dataRowIndex: number): wijgrid {
			var table = $("<table></table>");

			container
				.empty()
				.append(table);

			var detailOptions = detailInstantiator._populateDetail(master),
				masterInfo: IMasterInfo = {
					master: master,
					dataRowIndex: dataRowIndex
				},
				args: IDetailCreatingEventArgs = {
					options: detailOptions,
					masterKey: masterKey
				};

			master._onDetailCreating(args);

			table
				.data(wijgrid.JQDATA_MASTER_INFO_KEY, masterInfo)
				.wijgrid(args.options);

			return table.wijgrid("instance");
		}

		private static _populateDetail(master: wijgrid): IDetailSettings {
			var result: IDetailSettings,
				source = master.options.detail;

			// clone original object except the .data option
			var st = wijmo.grid.DataOptionPersister.persistAndClear(source);
			result = $.extend(true, {}, source);
			wijmo.grid.DataOptionPersister.restore(source, st);
			wijmo.grid.DataOptionPersister.restore(result, st); // share same .data options

			result._isDetail = true;
			result.disabled = master.options.disabled;

			// handle the .data option
			if (!result.data || $.isArray(result.data)) {
				// result.data points to the source.data
			} else {
				if (typeof (result.data.clone) === "function") {
					result.data = (<wijmo.data.ICloneable>result.data).clone(); // clone wijdataview
				} else {
					throw "Unsupported data source.";
				}
			}

			return result;
		}
	}

	/** Provides an access to hierarchy nodes in form of tree. */
	export class hierarchyNode {
		private mOwner: wijgrid;
		private mDataRowIndex: number;
		private mIsExpanded = undefined;
		private mDetails: hierarchyNode[];
		private mDetail: wijgrid = null;

		/** @ignore */
		constructor(owner: wijgrid, dataRowIndex: number) {
			this.mOwner = owner;
			this.mDataRowIndex = dataRowIndex;
		}

		/**
		* Returns children nodes.
		* @returns {wijmo.grid.hierarchyNode[]} An array of children nodes if node is expanded, otherwise an empty array.
		*/
		public details(): hierarchyNode[] {
			if (!this.mDetails) {
				this.mDetails = [];
			}

			return this.mDetails;
		}

		/** Collapses the node. */
		public collapse(): void {
			var view = this.mOwner._view(),
				dataRow = this._masterDataRow(),
				detailRow = view.bodyRows().item(view._getRowInfo(dataRow).sectionRowIndex + 1);

			this._toggleDataRow(dataRow, false);
			this._toggleDetailRow(detailRow, false);

			var defCSS = wijmo.grid.wijgrid.CSS,
				container = rowObjToJQuery(detailRow).find("." + defCSS.detailContainerCell + ":first > ." + defCSS.cellContainer);

			container.empty();

			this._setGrid(null);
			if (this.mDetails) {
				this.mDetails.splice(0, this.mDetails.length); // clear subtree
			}

			// 1. go upstairs from the current detail to a topmost one, reset width of each traversed grid, build a path.
			var current = this.mOwner,
				path: wijgrid[] = [];

			do {
				path.unshift(current);
			} while (current = current._master());

			// 2. go backwards using path and re-size each grid.
			while (current = path.shift()) {
				current._setSizeWithoutDetails();
			}

			this.mIsExpanded = false;
		}

		/** Expands the node. */
		public expand(): void {
			var view = this.mOwner._view(),
				dataRow = this._masterDataRow(),
				detailRow = view.bodyRows().item(view._getRowInfo(dataRow).sectionRowIndex + 1);

			this._toggleDataRow(dataRow, true);
			this._toggleDetailRow(detailRow, true);

			var defCSS = wijmo.grid.wijgrid.CSS,
				container = rowObjToJQuery(detailRow).find("." + defCSS.detailContainerCell + ":first > ." + defCSS.cellContainer);

			this.mOwner.mLoadingDetails++;
			detailInstantiator.instantiateIn(container, this.mOwner, this.masterKey(), this.mDataRowIndex);

			this.mIsExpanded = true;
		}

		/**
		* Determines whether node is expanded or not.
		* @returns {Boolean} True if node is collapsed, otherwise false.
		*/
		public isExpanded(): boolean {
			if (this.mIsExpanded === undefined) {
				return this.mOwner.options.detail.startExpanded;
			}

			return this.mIsExpanded;
		}

		/**
		* Returns a wijgrid instance object which represents the detail grid related to node.
		* @returns {wijmo.grid.wijgrid} A wijgrid instance object which represents the detail grid related to node if node is expanded, otherwise null.
		*/
		public grid(): wijgrid {
			return this.mDetail;
		}

		/**
		* Returns master key related to node.
		* @returns {wijmo.grid.IDataKeyArray} Master key related to node.
		*/
		public masterKey(): IDataKeyArray {
			var result: IDataKeyArray = {},
				dataItem = this.mOwner._getDataItem(this.mDataRowIndex),
				detail = this.mOwner.options.detail;

			for (var i = 0; i < detail.relation.length; i++) {
				var r = detail.relation[i];
				result[r.masterDataKey.toLowerCase()] = this.mOwner.mDataViewWrapper.getValue(dataItem, r.masterDataKey);
			};

			return result;
		}

		/** @ignore */
		_setDetails(value: hierarchyNode[]) {
			this.mDetails = value;
		}

		/** @ignore */
		_setGrid(value: wijgrid): void {
			this.mDetail = value;
		}

		private _masterHierarchyNode(): hierarchyNode {
			if (this.mOwner) {
				var mi = this.mOwner._masterInfo();
				if (mi.master) {
					return mi.master.details()[mi.dataRowIndex];
				}
			}

			return null;
		}

		private _masterDataRowIndex(): number {
			return this.mDataRowIndex;
		}

		private _masterDataRow(): IRowObj {
			var self = this,
				dataRow = this.mOwner._rows().findRowObj(0, function (row) { return row.dataRowIndex == self.mDataRowIndex; });

			return dataRow;
		}

		private _toggleDataRow(dataRow: IRowObj, expand: boolean) {
			var view = this.mOwner._view(),
				info = view._getRowInfo(dataRow),
				sketch = this.mOwner.mSketchTable.row(info.sketchRowIndex);

			if (expand) {
				sketch.extInfo.state &= ~renderStateEx.collapsed;
				info._extInfo.state &= ~renderStateEx.collapsed;
			} else {
				info._extInfo.state |= renderStateEx.collapsed;
				sketch.extInfo.state |= renderStateEx.collapsed;
			}

			view._setRowInfo(info.$rows, info);

			this.mOwner.mRowStyleFormatter._masterFormatter(info);
		}

		private _toggleDetailRow(detailRow: IRowObj, expand: boolean) {
			var view = this.mOwner._view(),
				info = view._getRowInfo(detailRow),
				sketch = this.mOwner.mSketchTable.row(info.sketchRowIndex);

			if (expand) {
				sketch.extInfo.state &= ~renderStateEx.hidden;
				info._extInfo.state &= ~renderStateEx.hidden;
			} else {
				info._extInfo.state |= renderStateEx.hidden;
				sketch.extInfo.state |= renderStateEx.hidden;
			}

			view._setRowInfo(info.$rows, info);

			if (expand) {
				info.$rows.show();
			} else {
				info.$rows.hide();
			}
		}
	}
} 