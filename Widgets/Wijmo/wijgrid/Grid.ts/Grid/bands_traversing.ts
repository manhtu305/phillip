/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class bandProcessor {
		private _height: number;
		private _width: number;
		private _table: IColumnHeaderInfo[][];
		private _traverseList: IColumn[];
		private _shift: number;
		private _inc: number;
		private _savedXPos: number[];

		generateSpanTable(root): IColumnHeaderInfo[][] {
			this._height = this._width = this._inc = this._shift = 0;
			this._table = [];
			this._traverseList = [];
			this._savedXPos = [];

			return this._generateSpanTable(root, true);
		}

		private _generateSpanTable(root, parentVisibility: boolean): IColumnHeaderInfo[][] {
			this._height = this.getVisibleHeight(root, parentVisibility);

			this._traverseList = wijmo.grid.flatten(root);
			this._width = this._traverseList.length;

			for (var i = 0; i < this._height; i++) {
				this._table[i] = [];

				for (var j = 0; j < this._width; j++) {
					this._table[i][j] = { column: null, colSpan: 0, rowSpan: 0 };
				}
			}

			this._setTableValues(root, 0, 0);

			return this._table;
		}

		getVisibleHeight(root, parentVisibility: boolean): number {
			var result = 0;

			if ($.isArray(root)) { // columns
				for (var i = 0, len = (<IColumn[]>root).length; i < len; i++) {
					var tmp = this.getVisibleHeight(root[i], parentVisibility);
					result = Math.max(result, tmp);
				}
			} else { // column
				var rootC = <IColumn>root,
					colVis = (rootC.visible === undefined) ? true : rootC.visible;

				rootC._parentVis = colVis && parentVisibility;

				if ($.isArray(rootC.columns)) { // band
					for (var i = 0, len = rootC.columns.length; i < len; i++) {
						tmp = this.getVisibleHeight(rootC.columns[i], rootC._parentVis);
						result = Math.max(result, tmp);
					}

					if (!rootC._parentVis) {
						return result;
					}

					rootC._isLeaf = (result === 0);
					result++;
				} else { // general column
					rootC._isLeaf = true;
					if (rootC._parentVis) {
						result = 1;
					}
				}
			}

			return result;
		}

		private _getVisibleParent(column: IColumn): IColumn {
			while (column) {
				column = this._traverseList[column._parentIdx];
				if (column && (column._parentVis || column._parentVis === undefined)) {
					return column;
				}
			}

			return null;
		}

		private _setTableValues(root, y: number, x: number) {
			var tx: number, posX: number;

			if ($.isArray(root)) { //
				for (var i = 0, len = root.length; i < len; i++) {
					this._setTableValues(root[i], y, x);
				}
			} else { // column
				var rootC = <IColumn>root;

				if (rootC._travIdx === undefined) {
					throw "undefined travIdx";
				}

				tx = x + this._shift;

				if (rootC._parentVis) {
					posX = tx + this._inc;
					this._table[y][posX].column = rootC;
					this._savedXPos[rootC._travIdx] = posX;
				}

				if ($.isArray(rootC.columns)) { // band
					for (i = 0, len = rootC.columns.length; i < len; i++) {
						this._setTableValues(rootC.columns[i], y + 1, x);
					}
				}

				if (rootC._parentVis) {
					if (this._shift - tx === 0) { //root is column or band without visible nodes
						this._table[y][this._savedXPos[rootC._travIdx]].rowSpan = this._height - y;
						this._shift++;
					} else { // band with visible nodes
						this._table[y][this._savedXPos[rootC._travIdx]].colSpan = this._shift - tx;
					}
				} else {
					if (!($.isArray(rootC.columns)) && this._height > 0) {
						var visibleParent = this._getVisibleParent(rootC),
							parentIsLeaf = (visibleParent) ? visibleParent._isLeaf : false;

						if (parentIsLeaf) {
							this._inc++;
						}

						if (y >= this._height) {
							y = this._height - 1;
						}

						posX = x + this._shift + this._inc;

						this._table[y][posX].column = rootC;

						if (!parentIsLeaf) {
							if (visibleParent && (this._savedXPos[visibleParent._travIdx] === posX)) {
								this._shiftTableElements(posX, y);
							}

							this._inc++;
						}
					}
				}
			}
		}

		private _shiftTableElements(x: number, untilY: number) {
			for (var i = 0; i < untilY; i++) {
				this._table[i][x + 1] = this._table[i][x];
				this._table[i][x] = { column: null, colSpan: 0, rowSpan: 0 };

				if (this._table[i][x + 1].column) {
					this._savedXPos[this._table[i][x + 1].column._travIdx]++;
				}
			}
		}
	}

	/** @ignore */
	export function flatten(columns: IColumn[]): IColumn[] {
		var result = [];

		wijmo.grid.traverse(columns, function (column) {
			result.push(column);
		});

		return result;
	}

	/** @ignore */
	export function getTompostParent(column: c1basefield, allColumnsTraverseList: c1basefield[]): c1basefield {
		if (!column || !allColumnsTraverseList || (column.options._parentIdx === -1)) {
			return null;
		}

		var parent = allColumnsTraverseList[column.options._parentIdx];

		if (parent.options._parentIdx === -1) {
			return parent;
		}

		return wijmo.grid.getTompostParent(parent, allColumnsTraverseList);
	}

	/** @ignore */
	export function isChildOf(allColumns: c1basefield[], child: c1basefield, parent: c1basefield): boolean {
		if ($.isArray((<IColumn>parent.options).columns) && child.options._parentIdx >= 0) {
			if (child.options._parentIdx === parent.options._travIdx) {
				return true;
			}

			if (child.options._parentIdx > parent.options._travIdx) {
				while (true) {
					child = allColumns[child.options._parentIdx];

					if (child.options._travIdx === parent.options._travIdx) {
						return true;
					}

					if (child.options._parentIdx === -1) {
						break;
					}
				}
			}
		}

		return false;
	}

	/** @ignore */
	export function setTraverseIndex(columns: IColumn[]): number {
		return _setTraverseIndex(columns, 0, -1); // -> columns length
	}

	function _setTraverseIndex(columns: IColumn[], idx: number, parentIdx: number): number {
		if (columns) {
			for (var i = 0, len = columns.length; i < len; i++) {
				var column = columns[i];

				if ((<any>column).options) { // widget
					column = (<any>column).options;
				}

				column._linearIdx = i;
				column._travIdx = idx++;
				column._parentIdx = parentIdx;

				if (column.columns) {
					idx = _setTraverseIndex(column.columns, idx, idx - 1);
				}
			}
		}

		return idx;
	}

	/** @ignore */
	export function traverse(column: IColumn, callback: (column: IColumn, columns: IColumn[]) => void): void;
	/** @ignore */
	export function traverse(columns: IColumn[], callback: (column: IColumn, columns: IColumn[]) => void): void;
	/** @ignore */
	export function traverse(columns: any, callback: (column: IColumn, columns: IColumn[]) => void): void {
		if (columns && ($.isFunction(callback))) {
			for (var i = 0; i < columns.length; i++) {
				var column = columns[i];

				if (column.options) { // widget
					column = column.options;
				}

				var len = columns.length;

				callback(column, columns);

				if (columns.length !== len) { // backoff
					i--;
					continue;
				}

				if (column.columns) { // go deeper
					wijmo.grid.traverse(column.columns, callback);
				}
			}
		}
	}

	//export function getAriaHeaders(visibleLeaves: IColumn[], traverseList): string[] {
	//	var i, len, leaf, value, result = [];

	//	for (i = 0, len = visibleLeaves.length; i < len; i++) {
	//		leaf = visibleLeaves[i];
	//		value = "";

	//		do {
	//			value += (<any>window).escape(leaf.headerText) + " ";
	//		} while ((leaf = traverseList[leaf.parentIdx])/*&& leaf.parentVis*/);

	//		result[i] = $.trim(value);
	//	}

	//	return result;
	//}
}