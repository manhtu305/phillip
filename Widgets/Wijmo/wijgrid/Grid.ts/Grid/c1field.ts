/// <reference path="c1basefield.ts"/>
/// <reference path="c1groupedfield.ts"/>
/// <reference path="interfaces.ts"/>

/// <reference path="../../../wijinput/jquery.wijmo.wijinputdate.ts"/>
/// <reference path="../../../wijinput/jquery.wijmo.wijinputtext.ts"/>
/// <reference path="../../../wijinput/jquery.wijmo.wijinputnumber.ts"/>


module wijmo.grid {


	var $ = jQuery;

	export class c1field extends c1basefield {
		/** @ignore */
		public static WIDGET_NAME = "wijmo.c1field";
		public static DROPDOWN_FILTERS_MAX = 8;

		public static test(column: IColumn): boolean {
			// return column.groupInfo || column.sortDirection || column.aggregate || column.dataKey === null;
			return wijmo.grid.validDataKey(column.dataKey);
		}

		public static isGroupedColumn(column: c1basefield): boolean {
			var opt = <IColumn>column.options;
			return wijmo.grid.c1field.test(opt) && (opt.groupInfo && opt.groupInfo.position && (opt.groupInfo.position !== "none"));
		}

		public options: IC1FieldOptions;
		private mFilterEditor: JQuery;
		private mDropDownFilterList: JQuery;
		private mFilterEditorInitialized = false;

		constructor(wijgrid: wijgrid, options, widgetName?: string) {
			super(wijgrid, options, widgetName || c1field.WIDGET_NAME);
		}

		_destroy() {

			if (this.element) {
				var defCSS = this._defCSS();
				this.element.removeClass(defCSS.c1field);
			}

			try {
				if (this.mFilterEditor) {
					this.mFilterEditor
						.closest("td") // column filter cell
						.find("*")
						.unbind("." + this.widgetName);

					switch (this._getInputEditorType()) {
						case "date":
							if (this.mFilterEditor.data("wijmo-wijinputdate")) {
								this.mFilterEditor.wijinputdate("destroy");
							}
							break;

						case "text":
							if (this.mFilterEditor.data("wijmo-wijinputtext")) {
								this.mFilterEditor.wijinputtext("destroy");
							}
							break;

						case "numberCurrency":
						case "numberNumber":
						case "numberPercent":
							if (this.mFilterEditor.data("wijmo-wijinputnumber")) {
								this.mFilterEditor.wijinputnumber("destroy");
							}
							break;
					}
				}

				this._hideDropDownList();
			}
			finally {
				this.mFilterEditor = null;
				this.mDropDownFilterList = null;
			}

			super._destroy.apply(this, arguments);
		}

		_provideDefaults() {
			super._provideDefaults();

			wijmo.grid.shallowMerge(this.options, c1field.prototype.options);

			if ($.isFunction(this.options.dataParser)) {
				this.options.dataParser = new (<any>this.options.dataParser)(); // legacy, converts a function into an object intstance.
			}

			this.options.groupInfo = this.options.groupInfo || <any>{};
			wijmo.grid.shallowMerge(this.options.groupInfo, c1field.prototype.options.groupInfo);
		}

		//#region rendering
		_initializeHeaderCell(cell: JQuery, container: JQuery) {
			super._initializeHeaderCell.apply(this, arguments);

			var defCSS = this._defCSS();

			cell.addClass(defCSS.c1field);
		}

		_needToEncodeValue(row: IRowInfo): boolean {
			//  override and introduce the encodeHtml option.
			var rt = wijmo.grid.rowType,
				encode = ((row.type & rt.data) && !(row.type & rt.dataDetail)) // never encode non-data rows because it can consist of HTML markup.
				? this.options.encodeHtml
				: super._needToEncodeValue.apply(this, arguments);

			return encode;
		}

		//#endregion

		//#region UI actions
		_isSortable(): boolean {
			return true;
		}

		_isSortableUI(): boolean {
			var grid = this._owner();
			return grid.options.allowSorting && this.options.allowSort && this._isSortable();
		}

		_canDropTo(column: c1basefield) {
			if (super._canDropTo.apply(this, arguments)) {
				//the grouped column can't be dropped into group area
				if (this.options.groupedIndex !== undefined && (column instanceof c1groupedfield)) {
					return false;
				}

				return true;
			}

			return false;
		}

		_canDropToGroupArea(): boolean {
			return this.options.groupedIndex === undefined;
		}

		//#endregion

		//#region header cell
		_decorateHeaderContent($container: JQuery): void {
			var wijgrid = this._owner(),
				defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = wijgrid.options.wijCSS;

			if (!this._isSortable()) {
				super._decorateHeaderContent.apply(this, arguments); // plain text
			} else {
				var isSortableUI = this._isSortableUI();

				if (isSortableUI) {
					$container // clickable text
						.wrapInner("<a class=\"" + defCSS.headerCellText + " " + wijCSS.wijgridHeaderCellText + "\" href=\"#\" role=\"button\" />")
						.children("a")
						.bind("click." + this.widgetName, this, $.proxy(this._onSortableElementClick, this));
				} else {
					super._decorateHeaderContent.apply(this, arguments); // plain text
				}

				// icons
				var baseSortCSS = defCSS.headerCellSortIcon + " " + wijCSS.wijgridHeaderCellSortIcon + " " + wijCSS.icon,
					sortIcon: JQuery = null;

				switch (this.options.sortDirection) { // sorting icon
					case "ascending":
						$container.append(sortIcon = $("<span class=\"" + baseSortCSS + " " + wijCSS.iconArrowUp + "\"></span>"));
						break;

					case "descending":
						$container.append(sortIcon = $("<span class=\"" + baseSortCSS + " " + wijCSS.iconArrowDown + "\"></span>"));
						break;
				}

				if (sortIcon && isSortableUI) {
					sortIcon // make clickable icon (#63777)
						.css("cursor", "pointer")
						.bind("click." + this.widgetName, this, $.proxy(this._onSortableElementClick, this));
				}
			}
		}

		_onSortableElementClick(args: JQueryEventObject) {
			var column: c1field = args.data;

			if (column.options.disabled) {
				return false;
			}

			if (column.options.allowSort) {
				column._owner()._handleSort(column, args.ctrlKey);
			}

			return false;
		}
		//#endregion

		//#region filter cell

		_showFilter(): boolean {
			return this.options.showFilter && !this._isDetailRelationField(); // the detail relation field cannot be filtered
		}

		_isDetailRelationField(): boolean {
			var grid = this._owner();

			if (!grid._isRoot()) {
				var relation = (<IDetailSettings>grid.options).relation;

				if (relation) {
					for (var i = 0; i < relation.length; i++) {
						var a = relation[i].detailDataKey,
							b = this.options.dataKey;

						if (a && b && ((a + "").toLowerCase() === (b + "").toLowerCase())) {
							return true;
						}
					}
				}
			}

			return false;
		}

		_setupFilterCell(container: JQuery) {
			if (this._showFilter()) {
				var defCSS = this._defCSS(),
					wijCSS = this._wijCSS();

				container.html("<table cellPadding=\"0\" cellSpacing=\"0\" class=\"" + defCSS.filter + " " + wijCSS.wijgridFilter + " " + wijCSS.cornerAll + "\">" +
					"<tr>" +
					"<td style=\"width:100%\">" +
					"<input type=\"text\" class=\"" + defCSS.filterInput + " " + wijCSS.wijgridFilterInput + "\" style=\"width:100%\" />" +
					"</td>" +
					"<td class=\"" + defCSS.filterTrigger + " " + wijCSS.wijgridFilterTrigger + " " + wijCSS.cornerRight + " " + wijCSS.stateDefault + "\">" +
					"<span class=\"" + wijCSS.icon + " " + wijCSS.iconArrowDown + "\"></span>" +
					"</td>" +
					"</tr>" +
					"</table>");

				this._createFilterCellEditor(container);
			}
		}

		_createFilterCellEditor(container: JQuery) {

			var wijgrid = this._owner(),
				inputType = wijmo.grid.HTML5InputSupport.getDefaultInputType(wijgrid._isMobileEnv(), <IColumn>this.options),
				defCSS = this._defCSS(),
				wijCSS = this._wijCSS(),
				self = this;

			//container
			//	.bind(((<any>$.support).selectstart ? "selectstart" : "mousedown"), function (event) { //?? the problem of inputing in the filter textbox
			//		event.stopPropagation();
			//	})

			// create filter buttons
			container
				.find("." + defCSS.filterTrigger) // create filter button
				.attribute({ "role": "button", "aria-haspopup": "true" })
                .bind("mouseenter." + this.widgetName, function (e) {
                    if (self && !self.options.disabled) {
                        $(this).addClass(wijCSS.stateDefault + " " + wijCSS.stateHover);
                    }
                })
                .bind("mouseleave." + this.widgetName, function (e) {
                    if (self && !self.options.disabled) {
                        $(this).removeClass(wijCSS.stateDefault + " " + wijCSS.stateHover + " " + wijCSS.stateActive);
                    }
                })
                .bind("mouseup." + this.widgetName, this, function (e) {
                    if (self && !self.options.disabled) {
                        $(this).removeClass(wijCSS.stateDefault + " " + wijCSS.stateActive);
                    }
                })
				.bind("mousedown." + this.widgetName, $.proxy(this._onFilterBtnClick, this))
				.bind("click." + this.widgetName, function (e) { e.preventDefault(); }); // prevent # being added to url.

			var dataValue = wijgrid.parse(<IColumn>this.options, wijmo.grid.filterHelper.getSingleValue(this.options.filterValue));

			if (dataValue === null || dataValue === "undefined") { // provide default value
				switch (wijmo.grid.getDataType(<IColumn>this.options)) {
					case "boolean":
						dataValue = false;
						break;

					case "number":
					case "currency":
						dataValue = 0;
						break;

					case "datetime":
						dataValue = new Date(); // current date
						break;

					default:
						dataValue = "";
				}
			}

			var editorOptions = {
				culture: wijgrid.options.culture,
				cultureCalendar: wijgrid.options.cultureCalendar,
				disabled: wijgrid.options.disabled,
				decimalPlaces: (function (pattern) { // map decimal places specified within the dataFormatString option into the decimalPlaces option of the wijinputnumber.
					var test = /^(n|p|c){1}(\d*)$/.exec(pattern);

					if (test && test[2]) {
						return parseInt(test[2], 10);
					}

					test = /^(d){1}(\d*)$/.exec(pattern);
					if (test) {
						return 0; // left padding is not supported by wijinputnumber
					}

					return 2;
				})(this.options.dataFormatString),
				imeMode: this.options.imeMode,
				triggerMouseUp: (e) => {
					this._owner()._hideAllFilterDropDownLists();
				}
			};

			this.mFilterEditor = container
				.find("input")
				.bind("keypress." + this.widgetName, $.proxy(this._onFilterEditorKeyPress, this));

			var valListener = (args: { value: any; }) => {
				if (this.mFilterEditorInitialized) {
					this._onFilterEditorValueChanged(args);
				}
			};

			switch (this._getInputEditorType()) {
				case "date":
					if (inputType === "text") {
						this.mFilterEditor.wijinputdate($.extend(editorOptions, {
							date: dataValue,
							dateFormat: this.options.dataFormatString || undefined,
							showTrigger: true,
							dateChanged: (e, args) => { valListener({ value: args.data }); },
							calendar: wijgrid.options.calendar
						}));
					} else {
						// html5 editor
						this._createHtmlEditor(this.mFilterEditor, inputType, wijmo.grid.HTML5InputSupport.toStr(dataValue, inputType));
					}
					break;

				case "text":
					this.mFilterEditor.wijinputtext($.extend(editorOptions, {
						text: dataValue + "",
						textChanged: (e, args) => { valListener({ value: args.text }) }
					}));
					break;

				case "numberCurrency":
					this.mFilterEditor.wijinputnumber($.extend(editorOptions, {
						type: "currency",
						value: dataValue,
						valueChanged: (e, args) => { valListener({ value: args.value }) }
					}));
					break;

				case "numberNumber":
					if (inputType === "text") {
						this.mFilterEditor.wijinputnumber($.extend(editorOptions, {
							value: dataValue,
							valueChanged: (e, args) => { valListener({ value: args.value }) }
						}));
					} else {
						// html5 editor
						this._createHtmlEditor(this.mFilterEditor, inputType, wijmo.grid.HTML5InputSupport.toStr(dataValue, inputType));
					}
					break;

				case "numberPercent":
					this.mFilterEditor.wijinputnumber($.extend(editorOptions, {
						type: "percent",
						value: dataValue * 100,
						valueChanged: (e, args) => { valListener({ value: args.value / 100 }) }
					}));
					break;

				default:
					throw "Unsupported editor type";
			}

			this.mFilterEditorInitialized = true;

            // TFS: 395178
            if ($.browser.msie) {
                // TFS: 375496
                // fixing memory leak on IE browser
                self = null;
            }
		}

		_createHtmlEditor(input: JQuery, inputType: string, value: any): JQuery {
			var defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._owner().options.wijCSS;

			(<HTMLInputElement>input[0]).type = inputType; // use it instead of .attr("type", inputType) to avoid the "'type' property/attribute cannot be changed." exception.

			return input
				.addClass(wijCSS.stateDefault)
				.wrap("<span class=\"" + defCSS.filterNativeHtmlEditorWrapper + " " + wijCSS.wijgridFilterNativeHtmlEditorWrapper + "\"></span")
				.val(value);
		}

		_onFilterBtnClick(e: JQueryEventObject) {
			var wijgrid = this._owner(),
				ddflVisible = !!this.mDropDownFilterList;

			wijgrid._hideAllFilterDropDownLists();

			if (this.options.disabled || ddflVisible) {
				return false; // exit if dropdownlist of the current column was opened.
			}

			(<any>e.target).focus(); //TFS #24253: In IE9, wijgrid is distorted on opening filter drop-down in a scrollable grid

			var filterOpLowerCase = wijmo.grid.filterHelper.getSingleOperatorName(this.options.filterOperator).toLowerCase(),
				applicableFilters = wijgrid.getFilterOperatorsByDataType(wijmo.grid.getDataType(<IColumn>this.options)),
				args: IFilterOperatorsListShowingEventArgs = {
					operators: <wijmo.grid.IFilterOperator[]><any>$.extend(true, [], applicableFilters), // make a copy, so user can localize operators without affecting other controls
					column: <IColumn>this.options
				},
				items = [];

			wijgrid._onFilterOperatorsListShowing(args);

			if (args.operators) {
				$.each(args.operators, function (key, operator: IFilterOperator) {
					items.push({
						label: operator.displayName || operator.name,
						value: operator.name,
						selected: operator.name.toLowerCase() === filterOpLowerCase
					});
				});
			}

			this.mDropDownFilterList = $("<div />")
				.addClass(wijmo.grid.wijgrid.CSS.filterList + " " + wijgrid.options.wijCSS.wijgridFilterList)
				.appendTo(document.body)
				.wijlist({
					autoSize: true,
					maxItemsCount: c1field.DROPDOWN_FILTERS_MAX,
					selected: (e, data) => {
						var filterValue = this._getFilterValueFromEditor();
						this._hideDropDownList();
						wijgrid._handleFilter(this, data.item.value, filterValue);
					}
				})
				.css("z-index", wijmo.grid.getZIndex(wijgrid.mOuterDiv, 9999)) // 9999 is the default value
				.wijlist("setItems", items)
				.wijlist("renderList");

			var width = this.mDropDownFilterList.width() | 150;

			this.mDropDownFilterList
				.width(items.length > c1field.DROPDOWN_FILTERS_MAX ? width + 20 : width)
				.wijlist("refreshSuperPanel")
				.position({
					of: $(e.currentTarget),
					my: "left top",
					at: "left bottom"
				});

			(<any>this.mDropDownFilterList).$button = $(e.currentTarget);

			var eventUID = (<any>this.mDropDownFilterList).eventUID = wijmo.grid.getUID();

			$(document)
				.bind("mousedown." + this.widgetName + "." + eventUID, { column: this }, this._onDocMouseDown);
		}

		_onFilterEditorKeyPress(e: JQueryEventObject) {
			if (e && (e.which === 13)) { //if (e && (e.which === wijmo.getKeyCodeEnum().ENTER))
				var wijgrid = this._owner(),
					filterValue = this._getFilterValueFromEditor(),
					filterOperator = (this.options.filterOperator || "").toLowerCase();

				if (!filterValue) { // empty value - reset filtering
					filterValue = undefined;
					filterOperator = "nofilter";
				} else {
					if (!filterOperator || (filterOperator === "nofilter")) { // no filter operator - use default
						filterOperator = (wijmo.grid.getDataType(<IColumn>this.options) === "string")
						? "contains"
						: "equals";
					}
				}

				this._hideDropDownList();
				wijgrid._handleFilter(this, filterOperator, filterValue);
			}
		}

		_onDocMouseDown(e: JQueryEventObject) {
			var column: c1field = e.data.column,
				$target = $(e.target),
				defCSS = wijmo.grid.wijgrid.CSS,
				$filterList = $target.parents("." + defCSS.filterList + ":first"),
				$filterButton = $target.is("." + defCSS.filterTrigger)
				? $target
				: $target.parents("." + defCSS.filterTrigger + ":first");

			if (($filterButton.length && ($filterButton[0] === (<any>column.mDropDownFilterList).$button[0])) || ($filterList.length && ($filterList[0] === column.mDropDownFilterList[0]))) {
				// do nothing
			} else {
				column._hideDropDownList();
			}
		}

		_hideDropDownList() {
			if (this.mDropDownFilterList) {
				var eventUID = (<any>this.mDropDownFilterList).eventUID;

				this.mDropDownFilterList.remove();

				this.mDropDownFilterList = null;

				$(document).unbind("mousedown." + this.widgetName + "." + eventUID, this._onDocMouseDown);
			}

			if (this.mFilterEditor && (this._getInputEditorType() === "date")) {
				var inputDate: wijmo.input.wijinputdate = this.mFilterEditor.data("wijmo-wijinputdate");
				if (inputDate) {
					if (inputDate._popupVisible()) { // private API!!!
						inputDate._hidePopup();
					}
				}
			}
		}

		// "text", "date", "numberNumber", "numberPercent", "numberCurrency"
		_getInputEditorType() {
			switch (wijmo.grid.getDataType(<IColumn>this.options)) {
				case "number":
					return (this.options.dataFormatString && this.options.dataFormatString.indexOf("p") === 0)
						? "numberPercent"
						: "numberNumber";

				case "currency":
					return "numberCurrency";

				case "datetime":
					return "date";

				default:
					return "text";
			}
		}

		_getFilterValueFromEditor(): any {
			var wijgrid = this._owner(),
				inputType = wijmo.grid.HTML5InputSupport.getDefaultInputType(wijgrid._isMobileEnv(), <IColumn>this.options),
				filterValue;

			switch (this._getInputEditorType()) {
				case "date":
					if (inputType === "text") {
						filterValue = this.mFilterEditor.wijinputdate("option", "date") || new Date(); // current date
					} else {
						filterValue = wijmo.grid.HTML5InputSupport.parse(this.mFilterEditor.val(), inputType) || new Date();
					}
					break;

				case "text":
					filterValue = this.mFilterEditor.wijinputtext("option", "text");
					break;

				case "numberNumber":
					if (inputType !== "text") {
						filterValue = wijmo.grid.HTML5InputSupport.parse(this.mFilterEditor.val(), inputType) || 0;
						break;
					}
				// fall through
				case "numberCurrency":
					filterValue = this.mFilterEditor.wijinputnumber("option", "value");
					break;

				case "numberPercent":
					filterValue = this.mFilterEditor.wijinputnumber("option", "value") / 100;
					break;
			}

			return filterValue;
		}

		//#endregion filtering

		//#region footer cell
		_setupFooterCell(container: JQuery, row: IRowInfo) {
			var opt = this.options;

			if (opt.aggregate && (opt.aggregate !== "none")) {
				var totalValue = "";

				if (opt._totalsValue !== undefined) {
					totalValue = wijmo.grid.tally.getValueString(opt._totalsValue, this, this._owner());
				}

				this._updateHTML(row, container, wijmo.grid.stringFormat(opt.footerText || "{0}", totalValue || ""));
			} else {
				super._setupFooterCell.apply(this, arguments);
			}
		}
		//#endregion

		//#region properies handlers
		_postset_aggregate(value, oldValue) {
			this._owner().ensureControl(false);
		}

		_postset_allowSort(value, oldValue) {
			this._setupHeaderCell();
		}

		_postset_dataType(value, oldValue) {
			throw "read-only";
		}

		_postset_dataParser(value, oldValue) {
			this._owner().ensureControl(false);
		}

		_postset_disabled(value, oldValue) {
			if (this.mFilterEditor) {
				$.each(this.mFilterEditor.data(), function (key, widget: JQueryUIWidget) { // update the disabled option of the filter editor
					if (widget && widget.widgetName && widget.option) {
						widget.option("disabled", value);
					}
				});
			}
		}

		_postset_dataFormatString(value, oldValue) {
			this._owner().ensureControl(false);
		}

		_postset_filterOperator(value, oldValue) {
			var wijgrid = this._owner();

			wijgrid._resetDataProperitesOnFilterChanging();

			wijgrid.ensureControl(true);
		}

		_postset_filterValue(value, oldValue) {
			var wijgrid = this._owner();

			wijgrid._resetDataProperitesOnFilterChanging();

			wijgrid.ensureControl(true);
		}

		_postset_groupInfo(value, oldValue) {
			this._owner().ensureControl(true);
		}

		_postset_rowMerge(value, oldValue) {
			this._owner().ensureControl(false);
		}

		_postset_showFilter(value, oldValue) {
			this._owner().ensureControl(false);
		}

		_postset_sortDirection(value, oldValue) {
			this.options._sortOrder = 0;
			this._owner().ensureControl(true);
		}

		_postset_width(value, oldValue) {
			this._hideDropDownList();
			super._postset_width.apply(this, arguments);
		}
		//#endregion

		//#region wijinput event handlers

		_onFilterEditorValueChanged(args: { value: any; }) {
		}
		//#endregion
	}

	export class c1field_options extends c1basefield_options implements wijmo.grid.IC1FieldOptions {
		/** Causes the grid to calculate aggregate values on the column and place them in the column footer cell or group header and footer rows.
		  * Possible values are: "none", "count", "sum", "average", "min", "max", "std", "stdPop", "var", "varPop" and "custom".
		  * @example
		  * $("#element").wijgrid({ columns: [{ aggregate: "count" }]});
		  * @remarks
		  * Possible values are:
		  * "none": no aggregate is calculated or displayed.
		  * "count": count of non-empty values.
		  * "sum": sum of numerical values.
		  * "average": average of the numerical values.
		  * "min": minimum value (numerical, string, or date).
		  * "max": maximum value (numerical, string, or date).
		  * "std": standard deviation (using formula for Sample, n-1).
		  * "stdPop": standard deviation (using formula for Population, n).
		  * "var": variance (using formula for Sample, n-1).
		  * "varPop": variance (using formula for Population, n).
		  * "custom": custom value (causing grid to throw groupAggregate event).
		  *
		  * If the showFooter option is off or grid does not contain any groups, setting the "aggregate" option has no effect.
		  */
		aggregate: string = "none";

		/** A value indicating whether column can be sorted.
		  * @example
		  * $("#element").wijgrid({ columns: [{ allowSort: true }] });
		  */
		allowSort: boolean = true;

		/** A value indicating the key of the data field associated with a column.
		  * If an array of objects is used as a datasource for wijgrid, this should be string value,
		  * otherwise this should be an integer determining an index of the field in the datasource.
		  * @type {String|Number}
		  * @example
		  * $("#element").wijgrid({ columns: [{ dataKey: "ProductID" }]});
		  */
		dataKey: any = undefined;

		/** Column data type. Defines the rules, according to which column value will be formatted, defines editors types and allowed filter operators.
		  * Does not change the type of source data, besides the case when data source is HTMLTable.
		  * Possible values are: "string", "number", "datetime", "currency" and "boolean".
		  * @example
		  * $("#element").wijgrid({ columns: [{ dataType: "string" }]});
		  * @remarks
		  * Possible values are:
		  * "string": if using built-in parser any values are acceptable; "&nbsp;" considered as an empty string, nullString as null.
		  * "number": if using built-in parser only numeric values are acceptable, also "&nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
		  * "datetime": if using built-in parser only date-time values are acceptable, also "&nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
		  * "currency": if using built-in parser only numeric and currency values are acceptable, also "&nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
		  * "boolean": if using built-in parser only "true" and "false" (case-insensitive) values are acceptable, also "&nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
		  */
		dataType: string = undefined;

		/** Data converter that is able to translate values from a string representation to column data type and back.
		  * @example
		  * var myBoolParser = {
		  *		parseDOM: function (value, culture, format, nullString) {
		  *			return this.parse(value.innerHTML, culture, format, nullString);
		  *		},
		  *		parse: function (value, culture, format, nullString) {
		  *			if (typeof (value) === "boolean")  return value;
		  *
		  *			if (!value || (value === "&nbsp;") || (value === nullString)) {
		  *				return null;
		  *			}
		  *
		  *			switch (value.toLowerCase()) {
		  *				case "on": return true;
		  *				case "off": return false;
		  *			}
		  *
		  *			return NaN;
		  *		},
		  *		toStr: function (value, culture, format, nullString) {
		  *			if (value === null)  return nullString;
		  *				return (value) ? "on" : "off";
		  *			}
		  *		}
		  * }
		  *
		  * $("#element").wijgrid({ columns: [ { dataType: "boolean", dataParser: myBoolParser } ] });
		  * @remarks
		  * If undefined, than the built-in parser for supported datatypes will be used.
		  */
		dataParser: IDataParser = undefined;

		/** A pattern used for formatting and parsing column values.
		  * @example
		  * $("#element").wijgrid({
		  *		columns: [
		  *			{ dataType: "currency" }, 
		  *			{ dataType: "number" }, 
		  *			{ dataType: "number", dataFormatString: "p0" }
		  *		]
		  * });
		  * @remarks
		  * The default value is undefined ("n" pattern will be used for "number" dataType, "d" for "datetime", "c" for "currency").
		  * Please see the https://github.com/jquery/globalize for a full explanation and additional values.
		  */
		dataFormatString: string = undefined;

		/** A value indicating whether data values are HTML-encoded before they are displayed in a cell.
		  * @example
		  * $("#element").wijgrid({
		  *		data: [
		  *			[0, "<b>value</b>"],
		  *			[1, "&amp;"],
		  *		],
		  *		columns: [
		  *			{ headerText: "ID" }, 
		  *			{ headerText: "Value", encodeHtml: true }
		  *		]
		  * });
		  */
		encodeHtml: boolean = false;

		/** An operations set for filtering. Must be either one of the embedded operators or custom filter operator.
		  * Operator names are case insensitive. 
		  *
		  * @example
		  * $("#element").wijgrid({ columns: [{ dataType: "number", filterOperator: "Equals", filterValue: 0 }]});
		  * @remarks
		  * Embedded filter operators include:
		  * "NoFilter": no filter.
		  * "Contains": applicable to "string" data type.
		  * "NotContain": applicable to "string" data type.
		  * "BeginsWith": applicable to "string" data type.
		  * "EndsWith": applicable to "string" data type.
		  * "Equals": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "NotEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "Greater": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "Less": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "GreaterOrEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "LessOrEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "IsEmpty": applicable to "string".
		  * "NotIsEmpty": applicable to "string".
		  * "IsNull": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "NotIsNull": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  *
		  * Full option value is:
		  *		[filterOperartor1, ..., filterOperatorN]
		  * where each filter item is an object of the following kind:
		  *		{ name: <operatorName>, condition: "or"|"and" }
		  * where:
		  *		name: filter operator name.
		  *		condition: logical condition to other operators, "or" is by default.
		  * Example:
		  *		filterOperator: [ { name: "Equals" }, { name: "NotEqual", condition: "and" } ]
		  * It is possible to use shorthand notation, the following statements are equivalent:
		  *		filterOperator: [ { name: "Equals" }, { name: "BeginsWith" } ]
		  *		filterOperator: [ "Equals", "BeginsWith" ]
		  * In the case of a single operator option name may contain only filter operator name, the following statements are equivalent:
		  *		filterOperator: [ { name: "Equals" } ]
		  *		filterOperator: [ "Equals" ]
		  *		filterOperator: "Equals"
		  *
		  * Note: wijgrid built-in filter editors do not support multiple filter operators.
		  *
		  */
		filterOperator: any = "nofilter";

		/** A value set for filtering.
		  * @example
		  * $("#element").wijgrid({ columns: [{ dataType: "number", filterOperator: "Equals", filterValue: 0 }]});
		  * @remarks
		  * Full option value is:
		  *		[filterValue1, ..., filterValueN]
		  * where each item is a filter value for the corresponding filter operator. Example:
		  *		filterValue: [0, "a", "b"]
		  * 
		  * Built-in filter operators support array of values as an argument. Example:
		  *		filterOperator: ["Equals", "BeginsWith"]
		  *		filterValue: [[0, 1, 2], "a"]
		  * As a result of filtering all the records having 0, 1, 2, or starting with "a" will be fetched.
		  *
		  * Shorthand notation allows omitting square brackets, the following statements are equivalent:
		  *		filterValue: ["a"]
		  *		filterValue: [["a"]]
		  *		filterValue: "a"
		  *
		  * Note: wijgrid built-in filter editors do not support multiple filter values.
		  */
		filterValue: any = undefined;

		/** Used to customize the appearance and position of groups.
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { position: "header" }}]});
		  */
		groupInfo: IGroupInfo = {
			/** @ignore */
			expandInfo: [], // infrastructure
			/** @ignore */
			level: undefined, // infrastructure

			groupSingleRow: true,
			collapsedImageClass: undefined, /*$.wijmo.wijCSS.iconArrowRight*/
			expandedImageClass: undefined, /*$.wijmo.wijCSS.iconArrowRightDown*/
			position: "none",
			outlineMode: "startExpanded",
			headerText: undefined,
			footerText: undefined
		};

		/**
		* Controls the state of the input method editor for text fields.
		* Possible values are: "auto", "active", "inactive", "disabled".
		* Please refer to https://developer.mozilla.org/en-US/docs/Web/CSS/ime-mode for more info.
		* @example
		* $("#element").wijgrid({ columns: [{ imeMode: "auto" }]});
		*/
		imeMode: string = "auto";

		/**
		* Determines the type of html editor for filter and cells.
		* Possible values are: "number", "date", "datetime", "datetime-local", "month", "time", "text".
		* @example
		* $("#element").wijgrid({ columns: [{ inputType: "text" }]});
		* @remarks
		* If the value is set then input type element is used with "type" attribute set to the value. If the value is not set then:
		*  - in desktop environment a "text" input element is used as the editor.
		*  - in mobile environment a "number" input element is used for columns having "number" and "currency" dataType; for columns where dataType = "datetime" a "datetime" input element is used, otherwise a "text" input element is shown.
		*/
		inputType: string = undefined;

		/** A value indicating whether the cells in the column can be edited.
		  * @example
		  * $("#element").wijgrid({ columns: [ { readOnly: false } ] });
		  */
		readOnly: boolean = false;

		/** Determines whether rows are merged. Possible values are: "none", "free" and "restricted".
		  * @example
		  * $("#element").wijgrid({ columns: [{ rowMerge: "none" }]});
		  * @remarks
		  * Possible values are:
		  * "none": no row merging.
		  * "free": allows row with identical text to merge.
		  * "restricted": keeps rows with identical text from merging if rows in the previous column are merged.
		  */
		rowMerge: string = "none";

		/** A value indicating whether filter editor will be shown in the filter row.
		  * @example
		  * $("#element").wijgrid({ columns: [{ showFilter: true }]});
		  */
		showFilter: boolean = true;

		/** Determines the sort direction. Possible values are: "none", "ascending" and "descending".
		  * @example
		  * $("#element").wijgrid({ columns: [{ sortDirection: "none" }]});
		  * @remarks
		  * Possible values are:
		  * "none": no sorting.
		  * "ascending": sort from smallest to largest.
		  * "descending": sort from largest to smallest.
		  */
		sortDirection: string = "none";



		/** A value indicating whether null value is allowed during editing.
		  * @example
		  * $("#element").wijgrid({ columns: [{ valueRequired: false }]});
		  */
		valueRequired: boolean = false;
	};

	c1field.prototype.options = wijmo.grid.extendWidgetOptions(c1basefield.prototype.options, new c1field_options());

	wijmo.grid.registerColumnFactory((grid, column) => {
		if (c1field.test(column)) {
			return new c1field(grid, column);
		}

		return null;
	});
}

module wijmo.grid {


	var $ = jQuery;

	export class c1booleanfield extends c1field {
		public static WIDGET_NAME = "wijmo.c1booleanfield";

		constructor(wijgrid: wijgrid, options, widgetName?: string) {
			super(wijgrid, options, widgetName || c1booleanfield.WIDGET_NAME);
		}

		public static test(column: IColumn): boolean {
			return c1field.test(column) && (column.dataType === "boolean");
		}

		_setupDataCell(container: JQuery, formattedValue, row: IRowInfo) {
			var grid = this._owner(),
				opt = <IColumn>this.options,
				defCSS = this._defCSS(),
				allowClickEditing = grid._allowCellEditing() && (opt.readOnly !== true),
				$input = $("<input class=\"" + defCSS.inputMarker + "\" type=\"checkbox\" aria-label=\"checkbox\" />");

			if (!allowClickEditing) {
				$input.prop("disabled", true);
			}

			if (grid.parse(opt, grid.mDataViewWrapper.getValue(row.data, opt.dataKey)) === true) {
				$input.prop("checked", "checked");
			}

			container.empty().append($input);
		}

		_cellRendered(cell: JQuery, container: JQuery, row: IRowInfo) {
			var grid = this._owner(),
				allowClickEditing = grid._allowCellEditing() && (this.options.readOnly !== true);

			// similate single-click behaviour for checkboxes.
			// important: this code must support c1gridview.c1checkboxfield also!
			if (allowClickEditing) {
				var keyCodeEnum = wijmo.getKeyCodeEnum();

				container.children("input").bind("mousedown", function (e) {
					var targetElement = container.parent()[0],
						currentCell = grid.currentCell();

					if (currentCell.tableCell() !== targetElement) {
						grid._onClick(<JQueryEventObject>{ target: <EventTarget>targetElement });
					}
					if (!currentCell._isEdit()) {
						grid.beginEdit();
					}
				}).bind("keydown", function (e) {
						if (e.which === keyCodeEnum.ENTER) {
							grid._endEditInternal(e);
							return false;
						}
					});
			}
		}
	}

	wijmo.grid.registerColumnFactory((grid, column) => {
		if (c1booleanfield.test(column)) {
			return new c1booleanfield(grid, column);
		}

		return null;
	});
}