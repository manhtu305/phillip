/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class cellRange implements wijmo.grid.ICellRange {
		r1: number;
		r2: number;
		c1: number;
		c2: number;

		constructor(row: number, col: number);
		constructor(row1: number, col1: number, row2: number, col2: number);
		constructor(row1?: number, col1?: number, row2?: number, col2?: number) {
			switch (arguments.length) {
				case 2:
					this.r1 = this.r2 = row1;
					this.c1 = this.c2 = col1;
					break;
				case 4:
					this.r1 = row1;
					this.r2 = row2;
					this.c1 = col1;
					this.c2 = col2;
					break;
				default:
					this.r1 = 0;
					this.r2 = 0;
					this.c1 = 0;
					this.c2 = 0;
			}
		}

		isSingleCell(): boolean {
			return ((this.r1 === this.r2) && (this.c1 === this.c2));
		}
	}

	/** @ignore */
	export class merger {
		private _data: wijmo.grid.SketchTable;
		private _leaves: c1basefield[];
		private _grid: wijgrid;

		constructor() {
		}

		merge(grid: wijgrid, data: SketchTable): void {
			this._leaves = grid._visibleLeaves();
			this._data = data;

			this._merge();
		}

		private _merge() {
			var firstLeaf = true;

			for (var i = 0, len = this._leaves.length; i < len; i++) {
				if (this._leaves[i] instanceof c1field) {
					var leaf = <c1field>this._leaves[i];

					if (leaf.options.rowMerge === "free" || leaf.options.rowMerge === "restricted") {
						if (firstLeaf) {
							this._data.ensureNotLazy();
							firstLeaf = false;
						}
						this._mergeColumn(leaf);
					}
				}
			}
		}

		private _mergeColumn(column: c1field) {
			var cellIdx = column.options._leavesIdx;

			for (var i = 0, len = this._data.count(); i < len; i++) {
				var row = this._data.row(i);

				if (!row.isPureDataRow()) {
					continue;
				}

				var range = this._getCellRange(i, column);

				if (range.r1 !== range.r2) {
					var span = range.r2 - range.r1 + 1;
					this._data.row(range.r1).cell(cellIdx).ensureAttr().rowSpan = span;

					for (var spannedRow = range.r1 + 1; spannedRow <= range.r2; spannedRow++) {
						this._data.row(spannedRow).cell(cellIdx).visible(false);
					}
				}

				i = range.r2;
			}
		}

		private _getCellRange(rowIdx: number, column: c1field): wijmo.grid.cellRange {
			var cellIdx = column.options._leavesIdx,
				range = new wijmo.grid.cellRange(rowIdx, cellIdx),
				str = (this._data.valueAt(rowIdx, cellIdx) || "").toString(),
				dataLen = this._data.count();

			for (range.r2 = rowIdx; range.r2 < dataLen - 1; range.r2++) {
				var row = this._data.row(range.r2 + 1);

				if (!row.isPureDataRow() || ((row.valueCell(cellIdx).value || "").toString() !== str)) {
					break;
				}
			}

			var leafIdx = column.options._leavesIdx;

			if (leafIdx > 0 && column.options.rowMerge === "restricted") {
				var prevLeaf = this._leaves[leafIdx - 1];

				if (prevLeaf instanceof wijmo.grid.c1field) {
					var range2 = this._getCellRange(rowIdx, <c1field>prevLeaf);

					range.r1 = Math.max(range.r1, range2.r1);
					range.r2 = Math.min(range.r2, range2.r2);
				}
			}

			return range;
		}
	}
}