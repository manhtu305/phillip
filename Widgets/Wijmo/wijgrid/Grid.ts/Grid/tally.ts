/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class tally {
		static getValueString(value: any, column: c1basefield, strConverter: IParseHandler): string {
			var opt = <IColumn>column.options;

			if (typeof (value) !== "string") {
				if ((opt.aggregate === "count") && (opt.dataType !== "number")) {
					opt = <IColumn>{
						dataType: "number",
						dataFormatString: "n0"
					};
				}

				value = strConverter.toStr(opt, value);
			}

			return value;
		}

		private _sum = 0;
		private _sum2 = 0;
		private _cntNumbers = 0;
		private _cntStrings = 0;
		private _cntDates = 0;
		private _max = 0;
		private _min = 0;
		private _minString;
		private _maxString;
		private _minDate = 0;
		private _maxDate = 0;
		private _parser: IParseHandler;

		constructor(parser: IParseHandler) {
			this._parser = parser;
		}

		add(value) {
			if (value === null) {
				return;
			}

			var typeOf = (value instanceof Date) ? "datetime" : typeof (value);

			// * count strings *
			var foo = value.toString(); // value = _parseValue(value);

			if (this._cntStrings++ === 0) {
				this._minString = this._maxString = foo;
			}

			if (foo < this._minString) {
				this._minString = foo;
			}

			if (foo > this._maxString) {
				this._maxString = foo;
			}


			// * count numbers *
			var numberValue = this._parseValue(value); // a number or NaN
			if ((typeof (numberValue) === "number") && !wijmo.grid.isNaN(numberValue)) {
				value = numberValue;

				if (this._cntNumbers++ === 0) {
					this._min = this._max = value;
				}

				this._sum += value;
				this._sum2 += value * value;

				if (value < this._min) {
					this._min = value;
				}

				if (value > this._max) {
					this._max = value;
				}
			} else {
				// * count dates *
				if (typeOf === "datetime") {
					foo = value.getTime();

					if (this._cntDates++ === 0) {
						this._minDate = this._maxDate = foo;
					}

					if (foo < this._minDate) {
						this._minDate = foo;
					}

					if (foo > this._maxDate) {
						this._maxDate = foo;
					}
				}
			}
		}


		getValue(column: c1basefield): any {
			var opt = <IColumn>column.options;

			if (this._cntNumbers && ((opt.dataType === "number" || opt.dataType === "currency")
				|| (this._cntNumbers == this._cntStrings))) // suport template columns (they have a "string" dataType actually)
			{
				return this._getValue(opt.aggregate);
			}

			// we only support max/min and count for dates
			if (this._cntDates && (opt.dataType === "datetime")) {
				// we only support max/min and count for dates
				switch (opt.aggregate) {
					case "max":
						return new Date(this._maxDate);

					case "min":
						return new Date(this._minDate);

					case "count":
						return this._cntStrings;
				}
			}

			// we only support max/min and count for strings
			if (this._cntStrings) {
				switch (opt.aggregate) {
					case "max":
						return this._maxString;

					case "min":
						return this._minString;

					case "count":
						return this._cntStrings;
				}
			}

			return "";
		}

		private _parseValue(value: any): number {
			var typeOf = typeof (value);

			if (typeOf === "number") {
				return value;
			}

			if (typeOf === "string" && $.trim(value) === "") {
				return 0;
			}

			if (this._parser) {
				var testValue;

				try {
					testValue = this._parser.parse(
						<IColumn>{ dataType: "currency" },  // floating numbers parser
						value);
				}
				catch (e) {
					testValue = NaN;
				}

				if (isNaN(testValue)) {
					testValue = parseFloat(value);
				}

				if (!isNaN(testValue)) {
					return testValue;
				}
			}

			return value;
		}

		private _getValue(aggregate: string): any {
			switch (aggregate) {
				case "average":
					return (this._cntNumbers === 0)
						? 0
						: this._sum / this._cntNumbers;

				case "count":
					return this._cntStrings;

				case "max":
					return this._max;

				case "min":
					return this._min;

				case "sum":
					return this._sum;

				case "std":
					if (this._cntNumbers <= 1) {
						return 0;
					}

					return Math.sqrt(this._getValue("var"));

				case "stdPop":
					if (this._cntNumbers <= 1) {
						return 0;
					}

					return Math.sqrt(this._getValue("varPop"));

				case "var":
					if (this._cntNumbers <= 1) {
						return 0;
					}

					return this._getValue("varPop") * this._cntNumbers / (this._cntNumbers - 1);

				case "vapPop":
					if (this._cntNumbers <= 1) {
						return 0;
					}

					var tmp = this._sum / this._cntNumbers;
					return this._sum2 / this._cntNumbers - tmp * tmp;
			}

			return 0;
		}
	}
}
