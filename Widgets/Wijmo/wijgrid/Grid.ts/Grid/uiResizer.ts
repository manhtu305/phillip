/// <reference path="interfaces.ts"/>
/// <reference path="wijgrid.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class uiResizer {
		private _wijgrid: wijgrid;
		private MIN_WIDTH: number = 5;
		private _elements: c1basefield[] = [];
		private _gap = 10;
		private _step = 1;
		private _evntFormat: string;
		private _inProgress = false;
		private _hoveredColumn: c1basefield = null;
		private _docCursor: string;
		private _startLocation: IElementBounds = null;
		private _lastLocation: IElementBounds = null;
		private _proxy: JQuery = null;

		constructor(wijgrid: wijgrid) {
			this._wijgrid = wijgrid;

			this._evntFormat = "{0}." + this._wijgrid.widgetName + ".resizer";
		}

		addElement(column: c1basefield): void {
			if (column && column.element) {
				column.element
					.bind(this._eventKey("mousemove"), $.proxy(this._onMouseMove, this))
					.bind(this._eventKey("mousedown"), $.proxy(this._onMouseDown, this))
					.bind(this._eventKey("mouseout"), $.proxy(this._onMouseOut, this));

				this._elements.push(column);
			}
		}

		dispose(): void {
			$.each(this._elements, (index, field: c1basefield) => {
				field.element
					.unbind(this._eventKey("mousemove"), this._onMouseMove)
					.unbind(this._eventKey("mousedown"), this._onMouseDown)
					.unbind(this._eventKey("mouseout"), this._onMouseOut);
			});

			this._detachDocEvents();
		}

		inProgress(): boolean {
			return this._inProgress;
		}

		private _onMouseMove(e) {
			if (!this._inProgress) {
				var hoveredField = this._getFieldByPos({ x: e.pageX, y: e.pageY });
				if (hoveredField && hoveredField._canSize() && this._wijgrid._canInteract()) {
					hoveredField.element.css("cursor", "e-resize");

					this._hoveredColumn = hoveredField;

					e.stopPropagation(); // prevent frozener from taking effect
				} else {
					this._onMouseOut(e);
				}
			}
		}

		private _onMouseOut(e) {
			if (!this._inProgress) {
				if (this._hoveredColumn) {
					this._hoveredColumn.element.css("cursor", "");
					this._hoveredColumn = null;
				}
			}
		}

		private _onMouseDown(e) {
			this._hoveredColumn = this._getFieldByPos({ x: e.pageX, y: e.pageY });

			if (this._hoveredColumn && (<any>this._hoveredColumn)._canSize() && this._wijgrid._canInteract()) {
				try {
					var wijCSS = this._wijgrid.options.wijCSS,
						defCSS = wijmo.grid.wijgrid.CSS;

					this._hoveredColumn.element.css("cursor", "");

					this._docCursor = document.body.style.cursor;
					document.body.style.cursor = "e-resize";
					this._startLocation = this._lastLocation = wijmo.grid.bounds(this._hoveredColumn.element);

					this._proxy = $("<div class=\"" + defCSS.resizingHandle + " " + wijCSS.wijgridResizingHandle + " " + wijCSS.stateHighlight + "\">&nbsp;</div>");

					var visibleAreaBounds = this._wijgrid._view().getVisibleAreaBounds(true);

					this._proxy.css({
						"left": e.pageX, "top": this._startLocation.top,
						"height": visibleAreaBounds.height + visibleAreaBounds.top - this._startLocation.top
					});

					$(document.body).append(this._proxy);
				}
				finally {
					this._attachDocEvents();
					this._inProgress = true;

					e.stopPropagation(); // prevent frozener from taking effect
				}
			}
		}

		private _onDocumentMouseMove(e) {
			var deltaX = this._step * Math.round((e.pageX - this._lastLocation.left) / this._step);

			this._lastLocation = { left: this._lastLocation.left + deltaX, top: e.pageY, width: undefined, height: undefined };
			this._proxy.css("left", this._lastLocation.left);
		}

		private _onDocumentMouseUp(e) {
			try {
				document.body.style.cursor = this._docCursor;

				// destroy proxy object
				this._proxy.remove();

				if (this._startLocation !== this._lastLocation) {
					this._wijgrid._fieldResized(this._hoveredColumn, this._startLocation.width, Math.max(this._lastLocation.left - this._startLocation.left, this.MIN_WIDTH));
				}
			}
			finally {
				this._hoveredColumn = null;
				this._proxy = null;
				this._detachDocEvents();
				this._inProgress = false;
			}
		}

		private _onSelectStart(e) {
			e.preventDefault();
		}

		private _attachDocEvents() {
			if (!this._inProgress) {
				$(document)
					.bind(this._eventKey("mousemove"), $.proxy(this._onDocumentMouseMove, this))
					.bind(this._eventKey("mouseup"), $.proxy(this._onDocumentMouseUp, this));

				if ($.fn.disableSelection) {
					$(document.body).disableSelection();
				}

				if ("onselectstart" in document) { // $.support.selectstart ?
					$(document.body).bind("selectstart", this._onSelectStart);
				}
			}
		}

		private _detachDocEvents() {
			if (this._inProgress) {
				$(document)
					.unbind(this._eventKey("mousemove"), this._onDocumentMouseMove)
					.unbind(this._eventKey("mouseup"), this._onDocumentMouseUp);

				if ($.fn.enableSelection) {
					$(document.body).enableSelection();
				}

				if ("onselectstart" in document) { // $.support.selectstart ?
					$(document.body).unbind("selectstart", this._onSelectStart);
				}
			}
		}

		private _getFieldByPos(mouse): c1basefield {

			for (var i = 0, len = this._elements.length; i < len; i++) {
				var field = this._elements[i],
					bounds = wijmo.grid.bounds(field.element),
					isOver = wijmo.grid.isOver(mouse.y, mouse.x,
						bounds.top, bounds.left + bounds.width - this._gap,
						bounds.height, this._gap);

				if (isOver) {
					return field;
				}
			}

			return null;
		}

		private _eventKey(eventType) {
			var prefix = (this._wijgrid._isTouchEnv())
				? "wij" // 48214 [Win8.1, IE11] Column moving is performed instead of column sizing when resize a column.    
				: "";

			return prefix + wijmo.grid.stringFormat(this._evntFormat, eventType);
		}
	}
}
