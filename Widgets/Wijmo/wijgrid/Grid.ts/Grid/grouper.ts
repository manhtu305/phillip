/// <reference path="../../../wijutil/jquery.wijmo.wijutil.ts" />
/// <reference path="interfaces.ts"/>
/// <reference path="merger.ts"/>
/// <reference path="wijgrid.ts"/>
/// <reference path="groupHelper.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class groupRange {
		public cr = new wijmo.grid.cellRange(-1, -1);
		public isExpanded = false;
		public position = "none";

		private _sum = -1;
		private _hasHeaderOrFooter = true;

		constructor(expanded: boolean, range: wijmo.grid.cellRange, sum: number, position: string, hasHeaderOrFooter: boolean) {
			if (expanded !== undefined) {
				this.isExpanded = expanded;
			}

			if (range !== undefined) {
				this.cr = range;
			}

			if (sum !== undefined) {
				this._sum = sum;
			}

			if (position !== undefined) {
				this.position = position;
			}

			if (hasHeaderOrFooter !== undefined) {
				this._hasHeaderOrFooter = hasHeaderOrFooter;
			}
		}

		isSubRange(groupRange: wijmo.grid.groupRange): boolean {
			return ((this.cr.r1 >= groupRange.cr.r1) && (this.cr.r2 <= groupRange.cr.r2));
		}

		toString() {
			return this.cr.r1 + "-" + this.cr.r2;
		}

		collapse(grid: wijgrid, column: c1field): void {
			var groupInfo = column.options.groupInfo,
				leaves = grid._leaves();

			if (wijmo.grid.groupHelper.isParentExpanded(leaves, this.cr, groupInfo.level)) {
				if ((groupInfo.position !== "footer") && (groupInfo.outlineMode !== "none")) { // do not collapse groups with .position == "footer"
					var groupedColumnsCnt = wijmo.grid.groupHelper.getGroupedColumnsCount(leaves);

					this._collapse(grid, column, grid._rows(), leaves, this, groupedColumnsCnt, grid._allowVVirtualScrolling());
				}
			}
		}

		expand(grid: wijgrid, column: c1field, expandChildren: boolean): void {
			var groupInfo = column.options.groupInfo,
				leaves = grid._leaves();

			if (wijmo.grid.groupHelper.isParentExpanded(leaves, this.cr, groupInfo.level)) {
				var groupedColumnsCnt = wijmo.grid.groupHelper.getGroupedColumnsCount(leaves);

				this._expand(grid, column, grid._rows(), leaves, this, groupedColumnsCnt, expandChildren, true, grid._allowVVirtualScrolling());
			}
		}

		// column - an owner of the groupRange
		private _collapse(grid: wijgrid, column: c1field, rowAccessor: rowAccessor, leaves: c1basefield[], groupRange: groupRange, groupedColumnsCnt: number, virtualScrollingEnabled: boolean): void {
			var groupInfo = column.options.groupInfo,
				dataStart = groupRange.cr.r1,
				dataEnd = groupRange.cr.r2;

			switch (groupInfo.position) {
				case "header":
				case "headerAndFooter":
					this._toggleSketchRowVisibility(grid.mSketchTable.row(groupRange.cr.r1), undefined, false);

					if (!virtualScrollingEnabled) {
						this._toggleRowVisibility(grid, rowAccessor.item(groupRange.cr.r1), undefined, false);
					}

					dataStart++;
					break;
			}

			// hide child rows
			for (var i = dataStart; i <= dataEnd; i++) {
				this._toggleSketchRowVisibility(grid.mSketchTable.row(i), false, undefined);

				if (!virtualScrollingEnabled) {
					this._toggleRowVisibility(grid, rowAccessor.item(i), false, undefined);
				}
			}

			// update isExpanded property
			groupRange.isExpanded = false;

			for (var i = groupInfo.level + 1; i <= groupedColumnsCnt; i++) {
				var childRangesInfo = wijmo.grid.groupHelper.getChildGroupRanges(leaves, groupRange.cr, i - 1);

				if (childRangesInfo) {
					var childRanges = childRangesInfo.ranges;

					for (var j = 0, len = childRanges.length; j < len; j++) {
						var childRange = childRanges[j];

						childRange.isExpanded = false;

						// update groupHeader
						switch (childRangesInfo.column.options.groupInfo.position) {
							case "header":
							case "headerAndFooter":
								this._toggleSketchRowVisibility(grid.mSketchTable.row(childRange.cr.r1), false, false);

								if (!virtualScrollingEnabled) {
									this._toggleRowVisibility(grid, rowAccessor.item(childRange.cr.r1), false, false);
								}
								break;
						}
					}
				}
			}
		}

		// column - an owner of the groupRange
		private _expand(grid: wijgrid, column: c1field, rowAccessor: rowAccessor, leaves: c1basefield[], groupRange: groupRange, groupedColumnsCnt: number, expandChildren: boolean, isRoot: boolean, virtualScrollingEnabled: boolean): void {
			var groupInfo = column.options.groupInfo,
				dataStart = groupRange.cr.r1,
				dataEnd = groupRange.cr.r2;

			switch (groupInfo.position) {
				case "header":
					this._toggleSketchRowVisibility(grid.mSketchTable.row(dataStart), true, isRoot || expandChildren);

					if (!virtualScrollingEnabled) {
						this._toggleRowVisibility(grid, rowAccessor.item(dataStart), true, isRoot || expandChildren);
					}

					dataStart++;
					break;

				case "footer":
					this._toggleSketchRowVisibility(grid.mSketchTable.row(dataEnd), true, undefined);

					if (!virtualScrollingEnabled) {
						this._toggleRowVisibility(grid, rowAccessor.item(dataEnd), true, undefined);
					}

					dataEnd--;
					break;
				case "headerAndFooter":
					this._toggleSketchRowVisibility(grid.mSketchTable.row(dataStart), true, isRoot || expandChildren);

					if (!virtualScrollingEnabled) {
						this._toggleRowVisibility(grid, rowAccessor.item(dataStart), true, isRoot || expandChildren);
					}

					if (isRoot) {
						this._toggleSketchRowVisibility(grid.mSketchTable.row(dataEnd), true, undefined);

						if (!virtualScrollingEnabled) {
							this._toggleRowVisibility(grid, rowAccessor.item(dataEnd), true, undefined);
						}
					}
					dataStart++;
					dataEnd--;
					break;
			}

			if (isRoot) {
				groupRange.isExpanded = true;
			} else {
				return;
			}

			if (groupInfo.level === groupedColumnsCnt) { // show data rows
				for (var i = dataStart; i <= dataEnd; i++) {
					this._toggleSketchRowVisibility(grid.mSketchTable.row(i), true, undefined);

					if (!virtualScrollingEnabled) {
						this._toggleRowVisibility(grid, rowAccessor.item(i), true, undefined);
					}
				}
			} else {
				var childRangesInfo = wijmo.grid.groupHelper.getChildGroupRanges(leaves, groupRange.cr, groupInfo.level);

				if (childRangesInfo) {
					var childRanges = childRangesInfo.ranges,
						childColumn = childRangesInfo.column;

					if (childRanges.length && (dataStart !== childRanges[0].cr.r1)) { //
						// a space between parent groupHeader and first child range - show single rows (groupSingleRow = false)
						for (i = dataStart; i < childRanges[0].cr.r1; i++) {
							this._toggleSketchRowVisibility(grid.mSketchTable.row(i), true, undefined);

							if (!virtualScrollingEnabled) {
								this._toggleRowVisibility(grid, rowAccessor.item(i), true, undefined);
							}
						}
					}

					if (expandChildren) { // throw action deeper
						for (var i = 0, len = childRanges.length; i < len; i++) {
							var childRange = childRanges[i];

							this._expand(grid, childColumn, rowAccessor, leaves, childRange, groupedColumnsCnt, expandChildren, true, virtualScrollingEnabled);
						}
					} else { // show only headers of the child groups or fully expand child groups with .position == "footer"\ .outlineMode == "none"
						for (var i = 0, len = childRanges.length; i < len; i++) {
							var childRange = childRanges[i],
								childIsRoot = (childColumn.options.groupInfo.position === "footer" || childColumn.options.groupInfo.outlineMode === "none");

							this._expand(grid, childColumn, rowAccessor, leaves, childRange, groupedColumnsCnt, false, childIsRoot, virtualScrollingEnabled);
						}
					}
				}
			}
		}

		private _toggleRowVisibility(grid: wijgrid, rowObj: IRowObj, visible: boolean, expanded: boolean) {
			if (rowObj) {
				var rse = wijmo.grid.renderStateEx,
					view = grid._view(),
					rowInfo = view._getRowInfo(rowObj, false);

				if (visible !== undefined) {
					if (visible) {
						rowInfo._extInfo.state &= ~rse.hidden;
					} else {
						rowInfo._extInfo.state |= rse.hidden;
					}
				}

				if (expanded !== undefined) {
					if (expanded) {
						rowInfo._extInfo.state &= ~rse.collapsed;
					} else {
						rowInfo._extInfo.state |= rse.collapsed;
					}
				}

				view._setRowInfo(rowInfo.$rows, rowInfo);

				grid.mRowStyleFormatter._groupFormatter(rowInfo);

			}
		}

		private _toggleSketchRowVisibility(sketchRow: wijmo.grid.SketchRow, visible: boolean, expanded: boolean) {

			if (sketchRow) {
				var rse = wijmo.grid.renderStateEx;

				if (visible !== undefined) {
					if (visible) {
						sketchRow.extInfo.state &= ~rse.hidden;
					} else {
						sketchRow.extInfo.state |= rse.hidden;
					}
				}

				if (expanded !== undefined) {
					if (expanded) {
						sketchRow.extInfo.state &= ~rse.collapsed;
					} else {
						sketchRow.extInfo.state |= rse.collapsed;
					}
				}
			}
		}
	}

	/** @ignore */
	export class grouper {
		private mGrid: wijgrid;
		private mLeaves: c1basefield[];
		private mSketchTable: SketchTable;
		private mGroupRowIdx = 0;
		private mFirstVisbileLeafIndex = 0;

		constructor() {
		}

		group(grid: wijgrid, data: wijmo.grid.SketchTable) {
			this.mGrid = grid;
			this.mLeaves = grid._leaves();
			this.mSketchTable = data;
			this.mGroupRowIdx = 0;

			var flag = false;

			$.each(this.mLeaves, (i: number, column: c1basefield) => {
				var opt = <IColumn>column.options;

				if (opt.groupInfo) {
					delete opt.groupInfo.level;
					delete opt.groupInfo.expandInfo;
				}

				// get index of the first visible leaf (excluding row header)
				if (!flag && (flag = column._isFirstVisible())) {
					this.mFirstVisbileLeafIndex = i;
				}
			});


			this._group();
		}

		private _group() {
			var groupedLeaves = this.mGrid._groupedLeaves(true),
				level = 1;

			if (groupedLeaves.length == 0) {
				return;
			}

			// make sure all rows are created
			this.mSketchTable.ensureNotLazy();

			$.each(groupedLeaves, (i, leaf: c1field) => {
				this.mGroupRowIdx = 0;

				if (c1field.isGroupedColumn(leaf)) {
					var opt = <IColumn>leaf.options;

					opt.groupInfo.level = level;
					opt.groupInfo.expandInfo = [];

					this._processRowGroup(<c1field>leaf, level++);
				}
			});
		}

		private _processRowGroup(leaf: c1field, level: number) {
			var hasHeaderOrFooter = true,
				rse = wijmo.grid.renderStateEx;

			for (var rowIndex = 0; rowIndex < this.mSketchTable.count(); rowIndex++) {
				var row = this.mSketchTable.row(rowIndex);

				if (!row.isPureDataRow()) {
					continue;
				}

				var cellRange = this._getGroupCellRange(rowIndex, leaf, level),
					isExpanded = true,
					startCollapsed = (leaf.options.groupInfo.outlineMode === "startCollapsed"),
					header: SketchRow,
					footer: SketchRow,
					groupRange: groupRange,
					isParentCollapsed: boolean;

				if (startCollapsed || wijmo.grid.groupHelper.isParentCollapsed(this.mLeaves, cellRange, level)) {
					if ((leaf.options.groupInfo.groupSingleRow === false) && (cellRange.r1 === cellRange.r2)) {
						continue;
					}
					isExpanded = false;
				}

				// indent
				if (level && this.mGrid.options.groupIndent) {
					for (var indentRow = cellRange.r1; indentRow <= cellRange.r2; indentRow++) {
						this._addIndent(this.mSketchTable.row(indentRow).cell(this.mFirstVisbileLeafIndex), level);
					}
				}

				hasHeaderOrFooter = !(leaf.options.groupInfo.groupSingleRow === false && (cellRange.r1 === cellRange.r2));

				// insert group header/ group footer
				switch (leaf.options.groupInfo.position) {
					case "header":
						groupRange = this._addGroupRange(leaf.options.groupInfo, cellRange, isExpanded, hasHeaderOrFooter);

						for (var i = cellRange.r1; i <= cellRange.r2; i++) {
							row = this.mSketchTable.row(i);
							row.extInfo.groupLevel = level + 1;
							if (!isExpanded) {
								row.extInfo.state |= rse.hidden;
							}
						}

						if (!hasHeaderOrFooter) {
							break;
						}

						this._updateByGroupRange(groupRange, level);

						isParentCollapsed = wijmo.grid.groupHelper.isParentCollapsed(this.mLeaves, groupRange.cr, level);
						header = this._buildGroupRow(leaf, groupRange, cellRange, true, isParentCollapsed);

						this.mSketchTable.insert(cellRange.r1, header); // insert group header

						header.extInfo.groupLevel = level;
						if (!isExpanded) {
							header.extInfo.state |= rse.collapsed;
						}
						if (isParentCollapsed) {
							header.extInfo.state |= rse.hidden;
						}

						rowIndex = cellRange.r2 + 1;
						break;

					case "footer":
						groupRange = this._addGroupRange(leaf.options.groupInfo, cellRange, true, hasHeaderOrFooter);

						if (!hasHeaderOrFooter) {
							break;
						}

						this._updateByGroupRange(groupRange, level);

						footer = this._buildGroupRow(leaf, groupRange, cellRange, false, false);
						footer.extInfo.groupLevel = level;

						this.mSketchTable.insert(cellRange.r2 + 1, footer);
						rowIndex = cellRange.r2 + 1;

						isParentCollapsed = wijmo.grid.groupHelper.isParentCollapsed(this.mLeaves, groupRange.cr, level);
						if (isParentCollapsed) {
							footer.extInfo.state |= rse.hidden;
						}

						break;

					case "headerAndFooter":
						groupRange = this._addGroupRange(leaf.options.groupInfo, cellRange, isExpanded, hasHeaderOrFooter);

						for (i = cellRange.r1; i <= cellRange.r2; i++) {
							row = this.mSketchTable.row(i);
							row.extInfo.groupLevel = level + 1;
							if (!isExpanded) {
								row.extInfo.state |= rse.hidden;
							}
						}

						if (!hasHeaderOrFooter) {
							break;
						}

						this._updateByGroupRange(groupRange, level);

						isParentCollapsed = wijmo.grid.groupHelper.isParentCollapsed(this.mLeaves, groupRange.cr, level);
						header = this._buildGroupRow(leaf, groupRange, cellRange, true, isParentCollapsed);
						footer = this._buildGroupRow(leaf, groupRange, cellRange, false, false);

						this.mSketchTable.insert(cellRange.r2 + 1, footer);
						footer.extInfo.groupLevel = level;
						if (isParentCollapsed || !isExpanded) {
							footer.extInfo.state |= rse.hidden;
						}

						this.mSketchTable.insert(cellRange.r1, header);
						header.extInfo.groupLevel = level;
						if (!isExpanded) {
							header.extInfo.state |= rse.collapsed;
						}
						if (isParentCollapsed) {
							header.extInfo.state |= rse.hidden;
						}

						rowIndex = cellRange.r2 + 2;
						break;

					default:
						throw wijmo.grid.stringFormat("Unknown Position value: \"{0}\"", leaf.options.groupInfo.position);
				}

				this.mGroupRowIdx++;
			}
		}

		private _buildGroupRow(column: c1field, groupRange: groupRange, cellRange: cellRange, isHeader: boolean, isParentCollapsed: boolean): SketchGroupRow {
			//when some column is hidden, the group row is not correct.
			var colOpt = <IColumn>column.options,
				groupInfo = colOpt.groupInfo,
				sketchRow = new SketchGroupRow(isHeader, null),
				groupByValue = undefined,
				groupByText = "",
				aggregate: any = "",
				defCSS = wijmo.grid.wijgrid.CSS;

			sketchRow.extInfo.groupIndex = this.mGroupRowIdx; // to make a row ID.

			if ((groupByValue = this.mSketchTable.valueAt(cellRange.r1, colOpt._leavesIdx)) !== null) {
				if (this.mGrid.options.readAttributesFromData) {
					// Avoid sutuation when grouped cell contains inplace editors (C1GridView, #64745)
					var attrValue = this.mSketchTable.wijgridDataAttrValueAt(cellRange.r1, colOpt._leavesIdx);
					if (attrValue) {
						groupByValue = this.mGrid.parse(colOpt, attrValue);
					}
				}

				groupByText = this.mGrid.toStr(colOpt, groupByValue);
			}

			sketchRow.groupByValue = groupByValue;

			if (this.mGrid._showRowHeader()) {
				sketchRow.add(HtmlCell.nbsp());
			}

			// create the summary cell
			var cell = new HtmlCell("", null);

			sketchRow.add(cell);

			// add group header text
			if (colOpt.aggregate && (colOpt.aggregate !== "none")) {
				aggregate = this._getAggregate(cellRange, column, column, isHeader, groupByText);
			}

			var caption = (isHeader)
				? groupInfo.headerText
				: groupInfo.footerText;

			// format caption

			// The text may include up to three placeholders:
			// "{0}" is replaced with the value being grouped on and
			// "{1}" is replaced with the group's column header
			// "{2}" is replaced with the aggregate
			if (caption === "custom") {
				var args: IGroupTextEventArgs = {
					data: this.mSketchTable.getRawTable(), // data object.
					column: colOpt, // column that is being grouped.
					groupByColumn: colOpt, // column initiated grouping.
					groupText: groupByText, // text that is being grouped.
					text: "", // text that will be displayed in the groupHeader or Footer.
					groupingStart: cellRange.r1, // first index for the data being grouped.
					groupingEnd: cellRange.r2, // last index for the data being grouped.
					isGroupHeader: isHeader,
					aggregate: aggregate
				};

				if (this.mGrid._trigger("groupText", null, args)) {
					caption = args.text;
				}
			} else {
				if ((caption === undefined) || (caption === null)) { // use default formatting
					if (isHeader) {
						caption = "{1}: {0}";
					}

					if (aggregate || (aggregate === 0)) {
						caption = caption
						? caption + " {2}"
						: "{2}";
					}
				}

				caption = wijmo.grid.stringFormat(caption,
					colOpt.encodeHtml ? wijmo.htmlEncode(groupByText) : groupByText, // wijmo.htmlEncode is dangerious
					colOpt.headerText ? colOpt.headerText : "",
					this._wrapAggregateValue(aggregate));
			}

			if (!caption) {
				caption = "&nbsp;";
			}

			cell.html += "<span>" + caption + "</span>";
			this._addIndent(cell, groupInfo.level - 1);

			// summary cells span until the end of the row or the first aggregate
			var span = 1,
				col = this.mGrid._virtualLeaves().length,
				bFirst = true;

			for (; col < this.mLeaves.length; col++) {
				var tmp = <IColumn>this.mLeaves[col].options;

				if (tmp._parentVis) {
					if (bFirst) {
						bFirst = false;
						continue;
					}
					if ((tmp._dynamic !== true) && tmp.aggregate && (tmp.aggregate !== "none")) {
						break;
					}

					span++;
				}
			}

			// add aggregates (or blanks) until the end of the row
			for (; col < this.mLeaves.length; col++) {
				var tmp2 = this.mLeaves[col];

				if (tmp2.options._parentVis) {
					var agg = "";

					if (tmp2 instanceof c1field) {
						agg = this._getAggregate(cellRange, <c1field>tmp2, column, isHeader, groupByText);
					}

					agg = agg || "&nbsp;"

					sketchRow.add(new HtmlCell(this._wrapAggregateValue(agg), {
						groupInfo: {  // will be passed into the cellStyleFormatter
							leafIndex: tmp2.options._leavesIdx,
							purpose: wijmo.grid.groupRowCellPurpose.aggregateCell
						}
					}));
				}
			}

			cell.ensureAttr().colSpan = span;
			cell.ensureAttr().groupInfo = { leafIndex: colOpt._leavesIdx, purpose: wijmo.grid.groupRowCellPurpose.groupCell }; // will be passed into the cellStyleFormatter

			return sketchRow;
		}

		private _getAggregate(cellRange: wijmo.grid.cellRange, column: c1field, groupByColumn: c1basefield, isGroupHeader: boolean, groupByText: string): string {
			var result = "";

			if (!column.options.aggregate || (column.options.aggregate === "none")) {
				return result;
			}

			if (column.options.aggregate === "custom") {
				var args: IGroupAggregateEventArgs = {
					data: this.mSketchTable.getRawTable(), // data object
					column: <IColumn>column.options, // column that is being grouped.
					groupByColumn: <IColumn>groupByColumn.options, // column initiated grouping.
					groupText: groupByText, // text that is being grouped.
					text: "", // text that will be displayed in the groupHeader or groupFooter.
					groupingStart: cellRange.r1, // first index for the data being grouped.
					groupingEnd: cellRange.r2, // last index for the data being grouped.
					isGroupHeader: isGroupHeader
				};

				var tmp = column.options.dataIndex;
				try {
					column.options.dataIndex = column.options._leavesIdx; // workaround: change dataIndex since the sketchRow (args.data[i]) contains both bound and unbound cells.

					if (this.mGrid._trigger("groupAggregate", null, args)) {
						result = args.text;
					}
				} finally {
					column.options.dataIndex = tmp; // restore
				}
			} else {
				var tally = new wijmo.grid.tally(this.mGrid);

				for (var row = cellRange.r1; row <= cellRange.r2; row++) {
					tally.add(this.mSketchTable.valueAt(row, column.options._leavesIdx));
				}

				result = wijmo.grid.tally.getValueString(tally.getValue(column), column, this.mGrid);
			}

			return result;
		}

		private _getGroupCellRange(rowIndex: number, leaf: c1basefield, level: number): wijmo.grid.cellRange {
			var idx = leaf.options._leavesIdx,
				row: SketchRow,
				range = new wijmo.grid.cellRange(rowIndex, idx),
				parentRange = wijmo.grid.groupHelper.getParentGroupRange(this.mLeaves, range, level),
				value, nextValue, count;

			row = this.mSketchTable.row(rowIndex);
			if (row.isPureDataRow()) {
				value = row.valueCell(leaf.options._leavesIdx).value;

				if (value instanceof Date) {
					value = value.getTime();
				}

				for (range.r2 = rowIndex, count = this.mSketchTable.count() - 1; range.r2 < count; range.r2++) {
					if (!this.mSketchTable.row(range.r2 + 1).isPureDataRow() || (parentRange && (range.r2 + 1 > parentRange.cr.r2))) {
						break;
					}

					nextValue = this.mSketchTable.valueAt(range.r2 + 1, leaf.options._leavesIdx);

					if (nextValue instanceof Date) {
						nextValue = nextValue.getTime();
					}

					if (value !== nextValue) {
						break;
					}
				}
			}

			return range;
		}

		private _addGroupRange(groupInfo: IGroupInfo, cellRange: wijmo.grid.cellRange, isExpanded, hasHeaderOrFooter): wijmo.grid.groupRange {
			var result = null,
				idx = wijmo.grid.groupHelper.getChildGroupIndex(cellRange, groupInfo.expandInfo),
				range, expandState, r1, r2;

			if (idx >= 0 && idx < groupInfo.expandInfo.length) {
				result = groupInfo.expandInfo[idx];
			} else {
				range = new wijmo.grid.cellRange(cellRange.r1, cellRange.r1, cellRange.r2, cellRange.r2); // clone
				expandState = (groupInfo.position === "footer" || !hasHeaderOrFooter)
				? true
				: isExpanded && (groupInfo.outlineMode !== "startCollapsed");

				result = new wijmo.grid.groupRange(expandState, range, -1, groupInfo.position, hasHeaderOrFooter);

				groupInfo.expandInfo.push(result);
			}

			if (result && hasHeaderOrFooter) {
				r1 = cellRange.r1;
				r2 = cellRange.r2;

				if (groupInfo.position === "headerAndFooter") {
					r2 += 2;
				}

				if (groupInfo.position !== "headerAndFooter") {
					r2++;
				}

				result.cr.r2 = r2;
			}

			return result;
		}

		private _updateByGroupRange(groupRange: groupRange, level: number): void {
			for (var i = 0, len = this.mLeaves.length; i < len; i++) {
				var groupInfo = (<IColumn>this.mLeaves[i].options).groupInfo;

				if (groupInfo && (groupInfo.level < level)) {

					var len2 = (groupInfo.expandInfo)
						? groupInfo.expandInfo.length
						: 0;

					for (var j = 0; j < len2; j++) {
						var cur = groupInfo.expandInfo[j];
						var delta = (groupRange.position === "headerAndFooter") ? 2 : 1;

						if (cur.cr.r1 >= groupRange.cr.r1 && !((cur.cr.r1 === groupRange.cr.r1) && (cur.position === "footer"))) {
							cur.cr.r1 += delta;
						}

						if (cur.cr.r2 >= groupRange.cr.r1) {
							cur.cr.r2 += delta;
						}
					}
				}
			}
		}

		private _addIndent(cellObj: SketchCell, level: number): void {
			var indent: number;

			if (level > 0 && (indent = this.mGrid.options.groupIndent)) {
				cellObj.ensureStyle().paddingLeft = (indent * level) + "px";
			}
		}

		private _wrapAggregateValue(value: any): string {
			return "<span class='" + wijmo.grid.wijgrid.CSS.aggregateContainer + "'>" + value.toString() + "</span>";
			//return value.toString();
		}
	}
}