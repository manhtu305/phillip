/// <reference path="interfaces.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />
/// <reference path="baseView.ts" />
/// <reference path="htmlTableAccessor.ts" />

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class flatView extends baseView {
		_dataTable: wijmo.grid.htmlTableAccessor = null;
		_contentArea: JQuery = null;

		constructor(wijgrid: wijgrid, renderBounds: IRenderBounds) {
			super(wijgrid, renderBounds);
		}

		//ensureWidth(index: number, value, oldValue) {
		//	var $table = $(this._dataTable.element()),
		//		tableWidth = $table.width() + value - oldValue;

		//	super.ensureWidth(index, value, oldValue);

		//	this._setTableWidth([$table], tableWidth, value, index);
		//}

		getVisibleAreaBounds(client?: boolean): IElementBounds {
			var bounds = wijmo.grid.bounds(this._dataTable.element(), client);

			return bounds;
		}

		getVisibleContentAreaBounds(): IElementBounds {
			return this.getVisibleAreaBounds();
		}

		updateSplits(scrollValue: IScrollingState, rowsToAdjust?: cellRange) {
			var self = this,
				wijgrid = this._wijgrid,
				o = wijgrid.options,
				gridElement = wijgrid.element,
				maxWidthArray: IColumnWidth[] = [],
				minWidthArray: IColumnWidth[] = [],
				resultWidthArray: IColumnWidth[] = [],
				visibleLeaves = wijgrid._visibleLeaves(),
				outerDiv = wijgrid.mOuterDiv,
				outerWidth: number,
				expandIndex: number;

			gridElement.css({
				"table-layout": "",
				"width": "auto"
			});

			// read column widths.
			$.each(visibleLeaves, (index, leaf: c1basefield) => {
				if (leaf.options._realWidth !== undefined) { // if any column has width option, we will set the width for inner cells.
					this._setColumnWidth(leaf, leaf.options._realWidth);
				}

				maxWidthArray.push(this._getColumnWidth(index));
			});

			gridElement.css("width", "1px");

			$.each(visibleLeaves, function (index, leaf: c1basefield) {
				minWidthArray.push(self._getColumnWidth(index));
			});

			outerWidth = outerDiv.width(); // using width() instead of innerWidth() to exclude padding.
			resultWidthArray = this._adjustWidthArray(maxWidthArray, minWidthArray, outerWidth, o.ensureColumnsPxWidth);

			$.each(resultWidthArray, function (index, colWidth: IColumnWidth) {
				var leaf = visibleLeaves[index];

				if (leaf.options._realWidth !== undefined && leaf._strictWidthMode()) {
					//delete leaf.options._realWidth;
					return;
				}

				self._setColumnWidth(leaf, colWidth.width);
			});

			gridElement.css("table-layout", "fixed") // <-- The outerDiv width can change after that

			expandIndex = resultWidthArray.length - 1;
			if (expandIndex !== -1) {
				var delta = outerDiv.width() - outerWidth; // test changes
				resultWidthArray[expandIndex].width += delta;

				this._setTableWidth([gridElement],
					this._sumWidthArray(resultWidthArray, 0, expandIndex),
					resultWidthArray[expandIndex].width,
					visibleLeaves[expandIndex]);
			}
		}

		getInlineTotalWidth(): string {
			var table = this._dataTable.element(),
				width = table.style.width;

			if (width && (width !== "auto")) {
				return width;
			}

			return "";
		}
		// public **

		// ** DOMTable abstraction

		forEachColumnCell(columnIndex: number, callback, param: any): boolean {
			return this._dataTable.forEachColumnCell(columnIndex, callback, param);
		}

		forEachRowCell(rowIndex: number, callback, param: any): boolean {
			return this._dataTable.forEachRowCell(rowIndex, callback, param);
		}

		getAbsoluteCellInfo(domCell: HTMLTableCellElement, virtualize: boolean): wijmo.grid.cellInfo {
			return new wijmo.grid.cellInfo(this.getColumnIndex(domCell), (<HTMLTableRowElement>domCell.parentNode).rowIndex, this._wijgrid, true, virtualize);
		}

		getAbsoluteCellIndex(domCell: HTMLTableCellElement): number {
			return this.getColumnIndex(domCell);
		}

		getAbsoluteRowIndex(domRow: HTMLTableRowElement): number {
			return domRow.rowIndex;
		}

		getCell(absColIdx: number, absRowIdx: number): HTMLTableCellElement {
			var cellIdx = this._dataTable.getCellIdx(absColIdx, absRowIdx),
				rowObj;

			if (cellIdx >= 0) {
				rowObj = this.getJoinedRows(absRowIdx, 0);
				if (rowObj[0]) {
					return rowObj[0].cells[cellIdx];
				}
			}

			return null;
		}

		getColumnIndex(domCell: HTMLTableCellElement): number {
			return this._dataTable.getColumnIdx(domCell);
		}

		getHeaderCell(absColIdx: number): HTMLTableHeaderCellElement {
			var leaf = this._wijgrid._visibleLeaves()[absColIdx],
				headerRow;

			if (leaf && (headerRow = this._wijgrid._headerRows())) {
				return wijmo.grid.rowAccessor.getCell(headerRow.item(leaf.options._thY), leaf.options._thX);
			}

			return null;
		}

		getJoinedCols(columnIndex: number): HTMLTableColElement[] {
			var $colGroup = $(this._dataTable.element()).find("> colgroup");

			if ($colGroup.length) {
				if (columnIndex < $colGroup[0].childNodes.length) {
					return [<HTMLTableColElement>$colGroup[0].childNodes[columnIndex], null];
				}
			}

			return [null, null];
		}

		getJoinedRows(rowIndex: number, rowScope: wijmo.grid.rowScope): IRowObj {
			return <wijmo.grid.IRowObj><any>[this._dataTable.getSectionRow(rowIndex, rowScope), null];
		}

		getJoinedTables(byColumn: boolean, index: number): any[] {
			return [this._dataTable, null, index];
		}

		subTables(): wijmo.grid.htmlTableAccessor[] {
			return [this._dataTable];
		}

		// DOMTable abstraction **

		// ** private abstract

		//  ** render
		_preRender() {
			super._preRender();
			this._dataTable = new wijmo.grid.htmlTableAccessor(<HTMLTableElement>this._wijgrid.element[0], true, true, true); // skip offsets, ensure tbody + colgroup
		}

		_postRender() {
			this._wijgrid.element
				.find("> tbody").addClass(this._wijgrid.options.wijCSS.content);

			this._dataTable = new wijmo.grid.htmlTableAccessor(<HTMLTableElement>this._wijgrid.element[0]); // create with offsets

			this._wijgrid._setAttr(this._wijgrid.element, {
				role: "grid",
				cellpadding: "0",
				border: "0",
				cellspacing: "0"
			});

			this._wijgrid.element.css("border-collapse", "separate");

			super._postRender();
		}

		_insertRow(rowType: wijmo.grid.rowType, sectionRowIndex: number, domRow?: HTMLTableRowElement /* optional, used by c1gridview to clone rows of the original table */)
			: HTMLTableRowElement[] {
			var $rt = wijmo.grid.rowType,
				tableSection: HTMLTableSectionElement;

			switch (rowType) {
				case $rt.header:
				case $rt.filter:
					tableSection = this._dataTable.ensureTHead();
					break;

				case $rt.footer:
					tableSection = this._dataTable.ensureTFoot();
					break;

				default: // tbody
					tableSection = this._dataTable.ensureTBody();
			}

			if (domRow) {
				// append only
				return [<HTMLTableRowElement>tableSection.appendChild(domRow)];
			} else {

				if (sectionRowIndex > tableSection.rows.length) {
					sectionRowIndex = -1;
				}

				return [<HTMLTableRowElement>tableSection.insertRow(sectionRowIndex)];
			}
		}

		_rowRendered(rowInfo: IRowInfo, rowAttr, rowStyle) {
			var domRow = <HTMLTableRowElement>rowInfo.$rows[0];

			if (!domRow.cells.length && this._isBodyRow(rowInfo)) {
				domRow.parentNode.removeChild(domRow);
			} else {
				super._rowRendered(rowInfo, rowAttr, rowStyle);
			}
		}

		_appendCell(rowInfo: IRowInfo, cellIndex: number, $cell: JQuery) {
			rowInfo.$rows[0].appendChild($cell[0]);
			//rowInfo.$rows.append($cell);
		}

		_createCol(column: c1basefield, visibleIdx: number): HTMLTableColElement[] {
			return [<HTMLTableColElement>document.createElement("col")];
		}

		_appendCol(domCol: HTMLTableColElement[], column: c1basefield, visibleIdx: number) {
			this._dataTable.appendCol(domCol[0]);
		}

		// render **

		// private abstract **

		// ** private specific
		// private specific **
	}
}
