/// <reference path="interfaces.ts"/>
/// <reference path="filterOperators.ts"/>
/// <reference path="misc.ts"/>

/// <reference path="../../../data/src/dataView.ts"/>
/// <reference path="../../../data/src/filtering.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export interface IFilterSetting extends wijmo.data.IFilterDescriptor {
		owner: c1field;
		dataKey: any;
		customProps?: {
			originalOperator?: string;
			travIdx: number;
		};
	}

	/** @ignore */
	export interface ISortSetting {
		dataKey: any;
		sortDirection: string;
		sortOrder: number;
		customProps: {
			travIdx: number;
		};
	}

	/** @ignore */
	export interface IPageSettings {
		pageSize: number;
		pageIndex: number;
	}

	/** @ignore */
	export interface IDVSortDescriptor extends wijmo.data.ISortDescriptor {
		customProps?: {
			travIdx: number;
		};
	}

	/** @ignore */
	export interface IDVFilterDescriptor extends wijmo.data.IFilterDescriptor {
		property: string;
		customProps?: {
			originalOperator?: string;
			travIdx: number;
		};
	}

	/** @ignore */
	export class settingsManager {
		private _wijgrid: wijgrid;
		private _dataView: wijmo.data.IDataView;

		private _dvFilteringSettings: wijmo.grid.IFilterSetting[] = undefined;
		private _dvPagingSettings: IPageSettings = undefined;
		private _dvSortingSettings: wijmo.grid.ISortSetting[] = undefined;

		private _wgFilteringSettings: IFilterSetting[] = undefined;
		private _wgPagingSettings: IPageSettings = undefined;
		private _wgSortingSettings: ISortSetting[] = undefined;

		private _filterCache: wijmo.grid.filterOperatorsCache;

		constructor(wijgrid: wijgrid) {
			if (!wijgrid) {
				throw "exception";
			}

			this._wijgrid = wijgrid;
			this._dataView = wijgrid.mDataViewWrapper.dataView();
			this._filterCache = new wijmo.grid.filterOperatorsCache(wijgrid);
		}

		compareSettings(): boolean {
			var result = true,
				len: number;

			// paging
			var aPgn = this.DVPagingSettings(),
				bPgn = this.WGPagingSettings();

			result = (aPgn === bPgn);

			if (!result && aPgn && bPgn) {
				result = (aPgn.pageSize === bPgn.pageSize && aPgn.pageIndex === bPgn.pageIndex);
			}

			// sorting
			if (result) {
				var aSrt = this.DVSortingSettings();
				var bSrt = this.WGSortingSettings();

				result = (aSrt === bSrt);

				if (!result && aSrt && bSrt && ((len = aSrt.length) === bSrt.length)) {
					result = true;
					for (var i = 0; i < len && result; i++) {
						result = ((aSrt[i].dataKey === bSrt[i].dataKey) && (aSrt[i].sortDirection === bSrt[i].sortDirection));
					}
				}
			}

			// filtering
			if (result) {
				var aFltr = this.DVFilteringSettings(),
					bFltr = this.WGFilteringSettings();

				result = wijmo.grid.compareObj(aFltr, bFltr);
			}

			return result;
		}

		DVFilteringSettings(): IFilterSetting[] {
			function traverse(filter, normalizedItem) {
				var condition;

				$.each(filter, function (i, item) {
					if (typeof (item) === "string") { // "or"\ "and"
						condition = item;
					} else {
						if ($.isArray(item)) {
							traverse(item, normalizedItem);
						} else { // single filter object - { property, operator, value }
							var filterDescr = <IDVFilterDescriptor>item,
								normItem = <IFilterSetting>normalizedItem;

							normItem.value.push(filterDescr.value);

							normItem.operator.push({
								name: (filterDescr.customProps && filterDescr.customProps.originalOperator) // unwrap proxy operator
								|| filterDescr.operator,
								condition: condition
							});
						}
					}
				});
			}

			if (this._dvFilteringSettings === undefined) {

				var foo = [],
					filter: IDVFilterDescriptor[] = this._dataView.filter();

				if (filter) {
					$.each(filter, function (dataKey, item: any) {
						var normalizedItem: IFilterSetting = {
							operator: undefined,
							owner: null,
							dataKey: dataKey
						};

						if ($.isArray(item)) {
							normalizedItem.value = [];
							normalizedItem.operator = [];

							traverse(item, normalizedItem);
						} else if ($.isPlainObject(item)) {
							var filterDescriptor = <IDVFilterDescriptor>item;

							normalizedItem.value = filterDescriptor.value;
							normalizedItem.operator = (filterDescriptor.customProps && filterDescriptor.customProps.originalOperator) // unwrap proxy operator
							|| item.operator
							|| "Equals";

							normalizedItem.customProps = filterDescriptor.customProps;
						} else {
							normalizedItem.value = item;
							normalizedItem.operator = "Equals";
						}

						foo.push(normalizedItem);
					});
				}

				this._dvFilteringSettings = (foo.length)
				? foo
				: null;
			}

			return this._dvFilteringSettings;
		}

		DVPagingSettings(): IPageSettings {
			if (this._dvPagingSettings === undefined) {
				var pageableDataView = wijmo.grid.asPagedDataView(this._dataView);

				if (pageableDataView) {
					var pageSize = pageableDataView.pageSize();

					this._dvPagingSettings = (pageSize > 0)
					? {
						pageSize: pageableDataView.pageSize(),
						pageIndex: pageableDataView.pageIndex()
					}
					: null;
				}
			}

			return this._dvPagingSettings;
		}

		DVSortingSettings(): ISortSetting[] {
			if (this._dvSortingSettings === undefined) {
				var foo: ISortSetting[] = [];

				if (true/*this._dataView.canSort()*/) {
					var sortDescription: IDVSortDescriptor[] = wijmo.data.sorting.compile(this._dataView.sort()).normalized;

					if (sortDescription) {
						$.each(sortDescription, function (key, prop: IDVSortDescriptor) {
							if (prop !== null) {
								foo.push({
									dataKey: (typeof (<any>prop) === "string")
									? <any>prop
									: prop.property,

									sortDirection: prop.asc || prop.asc === undefined
									? "ascending"
									: "descending",

									sortOrder: undefined,

									customProps: prop.customProps
								});
							}
						});
					}
				}

				this._dvSortingSettings = (foo.length)
				? foo
				: null;
			}

			return this._dvSortingSettings;
		}

		WGFilteringSettings(): IFilterSetting[] {
			if (this._wgFilteringSettings === undefined) {
				var foo: IFilterSetting[] = [],
					grid = this._wijgrid,
					leaves = grid._leaves() || [],
					dataFieldsMap: { [dataKey: string]: c1field; } = {};

				// process columns
				$.each(leaves, (key, column) => {
					if (column instanceof c1field) {
						var opt = (<c1field>column).options,
							filterValue = (<any>wijmo.grid.deepExtend({}, { foo: opt.filterValue })).foo, // to avoid string values reconstruction ("abc" -> ["a", "b", "c"])
							filterOperator = (<any>wijmo.grid.deepExtend({}, { foo: opt.filterOperator })).foo,
							filter = wijmo.grid.filterHelper.verify(filterOperator, filterValue, opt.dataType, this._filterCache);

						if (filter) {
							foo.push({
								owner: <c1field>column,
								dataKey: opt.dataKey,
								value: filter.value,
								operator: filter.operator,
								customProps: {
									travIdx: opt._travIdx
								}
							});
						}

						dataFieldsMap[(opt.dataKey + "").toLowerCase()] = <c1field>column;
					}
				});

				// handle master-detail relation
				if (!grid._isRoot()) {
					var relation = (<IDetailSettings>grid.options).relation,
						masterInfo = grid._masterInfo(),
						masterDataKey = masterInfo.master.details()[masterInfo.dataRowIndex].masterKey();

					for (var i = 0; i < relation.length; i++) {
						var r = relation[i],
							column = dataFieldsMap[r.detailDataKey.toLowerCase()];

						foo.push({
							owner: column, // undefined if grid doesn't have a column bounded to the r.detailDataKey field.
							dataKey: r.detailDataKey,
							value: masterDataKey[r.masterDataKey.toLowerCase()],
							operator: "Equals"
						});
					}
				}

				this._wgFilteringSettings = (foo.length) ? foo : null;
			}

			return this._wgFilteringSettings;
		}

		WGPagingSettings(): IPageSettings {
			if (this._wgPagingSettings === undefined) {
				this._wgPagingSettings = this._wijgrid.options.allowPaging
				? {
					pageSize: this._wijgrid.options.pageSize,
					pageIndex: this._wijgrid.options.pageIndex
				}
				: null;
			}

			return this._wgPagingSettings;
		}

		WGSortingSettings(): ISortSetting[] {
			if (this._wgSortingSettings === undefined) {

				var sortDictionary: { [index: string]: ISortSetting; } = {},
					sortArray: ISortSetting[] = [],
					groupedColumns = this._wijgrid._groupedLeaves(true), // grouped columns ordered by the groupedIndex
					leaves = this._wijgrid._leaves(),
					sortOrder = 0;

				// fill the sortedDictionary with the grouped columns first
				$.each(groupedColumns, (i: number, column: c1field) => {
					if (column._isSortable()) {
						var opt = <IColumn>column.options;

						if (opt.sortDirection === "none") {
							opt.sortDirection = "ascending"; // use "ascending" for grouped columns by default
						}

						sortDictionary[opt.dataKey] = {
							dataKey: opt.dataKey,
							sortDirection: opt.sortDirection,
							sortOrder: sortOrder++,
							customProps: {
								travIdx: opt._travIdx
							}
						};
					}
				});

				sortOrder++;

				// add other columns
				$.each(leaves, (i, column: c1basefield) => {
					if ((column instanceof c1field) && (<c1field>column)._isSortable()) {
						var opt = <IColumn>column.options;

						if (opt.sortDirection === "ascending" || opt.sortDirection === "descending") {
							if (!sortDictionary[opt.dataKey]) { // skip grouped columns or columns with the same dataKey
								sortDictionary[opt.dataKey] = {
									dataKey: opt.dataKey,
									sortDirection: opt.sortDirection,
									sortOrder: (opt._sortOrder || 0) + sortOrder,
									customProps: {
										travIdx: opt._travIdx
									}
								};
							}
						}
					}
				});

				// convert {} to []
				$.each(sortDictionary, (key, value: ISortSetting) => {
					sortArray.push(value);
				});

				// sort by sortOrder
				sortArray.sort((a: ISortSetting, b: ISortSetting) => {
					return a.sortOrder - b.sortOrder;
				});

				$.each(sortArray, (i: number, item: ISortSetting) => {
					delete item.sortOrder;
				});

				this._wgSortingSettings = (sortArray.length)
				? sortArray
				: null;
			}

			return this._wgSortingSettings;
		}

		MapWGToDV(): wijmo.data.IShape {
			var result: wijmo.data.IShape = {},
				foo,
				newDVFilterOption;

			// * paging *
			if (this._mapPagingParams() && (foo = this.WGPagingSettings())) {
				result.pageIndex = foo.pageIndex;
				result.pageSize = foo.pageSize;
			} else {
				result.pageSize = -1; // cancel paging
			}

			// ** sorting
			if (this._mapSortingParams()) {
				result.sort = []; // clear sorting

				if (foo = this.WGSortingSettings()) {
					result.sort = [];
					$.each(foo, function (key, o) {
						result.sort.push(<IDVSortDescriptor>{
							property: o.dataKey,
							asc: o.sortDirection === "ascending",
							customProps: o.customProps
						});
					});
				}
			}
			// sorting **

			this._wijgrid.mDeficientFilters = {};

			// ** filtering
			if (this._mapFilteringParams()) {
				// fill the deficientFilters 
				$.each(this._wijgrid._leaves(), (key, column: c1basefield) => {
					if (column instanceof c1field) {
						var opt = (<c1field>column).options;

						if (<any>(opt.filterOperator === undefined) ^ <any>(opt.filterValue === undefined)) {
							this._wijgrid.mDeficientFilters[opt.dataKey] = {
								operator: opt.filterOperator,
								value: opt.filterValue
							};
						}
					}
				});

				result.filter = {};

				// set filtering
				if (foo = this.WGFilteringSettings()) {
					result.filter = this._convertFilterToDV(foo);
				}

				if ($.isEmptyObject(result.filter)) {
					result.filter = null; // must be null to clear filtering.
				}
			}

			// filtering **

			return result;
		}

		MapDVToWG() {
			var foo,
				leavesByDataKey: c1field[] = <any>{},
				leavesByTravIdx: c1field[] = <any>{},
				mapSortingParams = this._mapSortingParams(),
				mapPagingParams = this._mapPagingParams(),
				mapFilteringParams = this._mapFilteringParams(),
				pagedDataView = wijmo.grid.asPagedDataView(this._dataView),
				self = this;

			$.each(this._wijgrid._leaves(), function (key, column: c1basefield) {
				if (column instanceof c1field) {
					var opt = (<c1field>column).options;

					// clear sorting
					if (mapSortingParams) {
						opt._sortOrder = 0;
						opt.sortDirection = "none";
					}

					// clear filtering
					if (mapFilteringParams) {
						opt.filterOperator = "nofilter";
						opt.filterValue = undefined;
					}

					leavesByDataKey[opt.dataKey] = <c1field>column;

					if (opt._travIdx >= 0) {
						leavesByTravIdx[opt._travIdx] = <c1field>column;
					}
				}
			});

			if (mapPagingParams && pagedDataView) {
				this._wijgrid.options.pageSize = pagedDataView.pageSize();
				this._wijgrid.options.pageIndex = pagedDataView.pageIndex();
			}

			if (mapSortingParams && (foo = this.DVSortingSettings())) {
				$.each(foo, function (idx, o: ISortSetting) {
					var column: c1field;

					if (o.customProps && (o.customProps.travIdx >= 0)) {
						column = leavesByTravIdx[o.customProps.travIdx]; // to resolve issue when grid contains multiple columns with the same dataKey
					}

					if (!column || (column.options.dataKey !== o.dataKey)) {
						column = leavesByDataKey[o.dataKey];
					}

					if (column) {
						column.options.sortDirection = o.sortDirection;
						column.options._sortOrder = idx; // restore sort order
					}
				});
			}

			if (mapFilteringParams) {
				if (foo = this.DVFilteringSettings()) {
					$.each(foo, function (key, o: IFilterSetting) {
						var column: c1field;

						if (o.customProps && (o.customProps.travIdx >= 0)) {
							column = leavesByTravIdx[o.customProps.travIdx]; // to resolve issue when grid contains multiple columns with the same dataKey
						}

						if (!column || (column.options.dataKey !== o.dataKey)) {
							column = leavesByDataKey[o.dataKey];
						}

						if (column && !column._isDetailRelationField()) { // skip relation fields
							var opt = column.options;

							opt.filterValue = o.value;
							opt.filterOperator = o.operator;

							if ($.isPlainObject(opt.filterOperator)) { // custom operator, convert operator object to operator name.
								opt.filterOperator = opt.filterOperator.name;
							}

							delete self._wijgrid.mDeficientFilters[opt.dataKey]; // unary operator?
						}
					});
				}

				$.each(this._wijgrid.mDeficientFilters, (dataKey, value: wijmo.data.IFilterDescriptor) => {
					var field = leavesByDataKey[dataKey];
					if (field) {
						var opt = leavesByDataKey[dataKey].options;

						opt.filterOperator = value.operator;
						opt.filterValue = value.value;
					}
				});
			}
		}

		private _mapPagingParams(): boolean {
			return this._wijgrid.options.allowPaging &&
				!this._wijgrid._customPagingEnabled() &&
				!this._wijgrid._serverShaping(); // used by c1gridview. Disable client paging because source data are paged already and contains items of the current page only.
		}

		private _mapSortingParams(): boolean {
			return !this._wijgrid._serverShaping(); // used by c1gridview. Disable client sorting because source data are sorted already.
		}

		private _mapFilteringParams(): boolean {
			return !this._wijgrid._serverShaping(); // used by c1gridview. Disable client filtering because source data are filtered already.
		}

		private _getDateValueRequirements(column: c1field): wijmo.grid.TimeUnit {
			var opt = column.options;

			return opt.inputType
				? TimeUnitConverter.convertInputType(opt.inputType) // inputType takes precedence
				: TimeUnitConverter.convertFormatString(opt.dataFormatString || "d");
		}

		private makeProxyFilter(column: c1field, prop, name, value) {
			name = name.toLowerCase();

			var self = this,
				colOpt = column.options,
				isDateColumn = (colOpt.dataType === "datetime"),
				result: IDVFilterDescriptor = {
					property: prop,
					operator: name, // name or filter object
					value: value,
					customProps: {
						travIdx: colOpt._travIdx
					}
				};

			var internalOp = this._filterCache.getByNameInt(name);
			var builtinOp = wijmo.data.filtering.ops[name];

			if ((name !== "nofilter") && (internalOp.isCustom || isDateColumn)) { // need to create a proxy filter
				result.customProps.originalOperator = result.operator;

				if (internalOp.isCustom) {
					result.operator = $.extend(true, {}, internalOp.op);
					result.operator.apply = result.operator.operator // wijdata requires apply() function instead of operator().
				} else { // date column
					result.operator = $.extend(true, {}, builtinOp);

					var filterRequirements = this._getDateValueRequirements(column);

					result.operator.apply = (a, b) => {
						if (!(a instanceof Date)) {
							a = self._wijgrid.parse(<IColumn>colOpt, a);
						}

						if (!(b instanceof Date)) {
							b = self._wijgrid.parse(<IColumn>colOpt, b);
						}

						TimeUnitConverter.cutDate(a, filterRequirements);
						TimeUnitConverter.cutDate(b, filterRequirements);

						return builtinOp.apply(a, b);
					};
				}
			}

			return result;
		}

		// conversion from wijgrid format
		private _convertFilterToDV(normalizedFilter: IFilterSetting[]): { [key: string]: IDVFilterDescriptor; } {
			var result = {},
				manager = this;

			$.each(normalizedFilter, function (i, group: IFilterSetting) {
				var prop = group.dataKey,
					currConds = [],
					currConn = "and",
					conn, operators, values, conds

				if (!$.isPlainObject(group)) return;

				operators = group.operator;
				values = group.value;

				if (operators == null) return;

				if (!$.isArray(operators)) {
					operators = [operators];
				}

				if (!$.isArray(values)) {
					values = [values];
				}

				if (operators.length != values.length) throw "The number of filter operators must match the number of filter values";

				if (operators.length == 0) return;

				$.each(operators, function (i, operator) {
					var value;

					if (typeof operator === "string") {
						operator = {
							name: operator
						};
					}

					if (!$.isPlainObject(operator) || !operator.name) throw "Invalid filter operator";

					value = values[i];

					if (!$.isArray(value)) {
						value = [value];
					}

					conds = $.map(value, function (operand) {
						if (group.owner) {
							if ((group.owner.options.dataType === "datetime") && (operand instanceof Date)) { // truncate filterValue to fit the dataFormatString or inputType
								TimeUnitConverter.cutDate(operand, manager._getDateValueRequirements(group.owner));
							}

							return manager.makeProxyFilter(group.owner, prop, operator.name, operand);
						} else { // group.owner == null, it means that this is implicit master-detail filter, wijgrid has no column object for it.
							return <IDVFilterDescriptor> {
								property: prop,
								operator: operator.name,
								value: operand,
							};
						}
					});

					function adjustConds() {
						if (conds.length > 1) {
							conds.splice(0, 0, "or");
						} else {
							conds = conds[0];
						}
					}

					function adjustCurrConds() {
						if (currConn == null) {
							currConn = conn;
							currConds.splice(0, 0, conn);
						}
					}

					currConn = null;
					conn = operator.condition || "or";
					if (currConds.length <= 1 || currConn == conn) {
						if (conds.length == 1 || currConds.length <= 1 || currConn == "or") {
							currConds = currConds.concat(conds);
							adjustCurrConds();
						} else {
							adjustConds();
							currConds.push(conds);
							adjustCurrConds();
						}
					} else {
						adjustConds();
						currConds = [currConds, conds];
						adjustCurrConds();
					}
				});

				$.each(currConds, function (j, cond) {
					if ($.isArray(cond) && cond.length == 2)
						currConds[j] = cond[1];
				});

				if (currConds.length == 2 && typeof (currConds[0] === "string")) {
					currConds.shift();
				}

				if (currConds.length == 1) {
					currConds = currConds[0]; // unwrap single filter
				}

				result[prop] = currConds;
			}); // $.each(normalizedFilter)

			return <{ [key: string]: IDVFilterDescriptor; }>result;
		}
	}
}
