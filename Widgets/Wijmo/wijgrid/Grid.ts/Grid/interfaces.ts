/// <reference path="../../../External/declarations/jquery.d.ts"/>

/// <reference path="misc.ts"/>
/// <reference path="grouper.ts"/>

module wijmo.grid {


	/** @ignore */
	export interface wijgridCSS {
		wijgrid: string;

		root: string;
		inputMarker: string;
		outerDivMarker: string;
		editedCellMarker: string;
		spinnerMarker: string;
		table: string;
		TH: string;
		TD: string;
		cellContainer: string;
		aggregateContainer: string;

		rowHeader: string;

		currentRowHeaderCell: string;
		currentHeaderCell: string;
		currentCell: string;

		cellAlignLeft: string;
		cellAlignRight: string;
		cellAlignCenter: string;

		filterList: string;
		filter: string;
		filterInput: string;
		filterTrigger: string;
		filterNativeHtmlEditorWrapper: string;

		headerArea: string;
		footerArea: string;

		headerRow: string;
		row: string;
		dataRow: string;
		altRow: string;
		emptyDataRow: string;
		filterRow: string;
		groupHeaderRow: string;
		groupFooterRow: string;
		groupHeaderRowCollapsed: string;
		groupHeaderRowExpanded: string;
		footerRow: string;
		detailRow: string;

		loadingOverlay: string;
		loadingText: string;

		groupArea: string;
		groupAreaEmpty: string;
		groupAreaButton: string;
		groupAreaButtonSort: string;
		groupAreaButtonClose: string;
		groupToggleVisibilityButton: string;

		fixedView: string;
		scroller: string;
		scrollableContent: string;

		dndHelper: string;
		dndArrowTopContainer: string;
		dndArrowBottomContainer: string;

		freezingHandleV: string;
		freezingHandleH: string;
		freezingHandleContent: string;

		resizingHandle: string;

		headerCellSortIcon: string;
		headerCellText: string;

		detailContainerCell: string;

		c1band: string;
		c1basefield: string;
		c1field: string;
	}

	/** @ignore */
	export interface wijgridWijmoCSS extends WijmoCSS {
		wijgrid: string;
		wijgridTable: string;
		wijgridTH: string;
		wijgridTD: string;
		wijgridCellContainer: string;
		wijgridRowHeader: string;
		wijgridCurrentRowHeaderCell: string;
		wijgridCurrentHeaderCell: string;
		wijgridCurrentCell: string;
		wijgridCellAlignLeft: string;
		wijgridCellAlignRight: string;
		wijgridCellAlignCenter: string;
		wijgridFilterList: string;
		wijgridFilter: string;
		wijgridFilterInput: string;
		wijgridFilterTrigger: string;
		wijgridFilterNativeHtmlEditorWrapper: string;
		wijgridHeaderArea: string;
		wijgridFooterArea: string;

		wijgridHeaderRow: string;
		wijgridRow: string;
		wijgridDataRow: string;
		wijgridAltRow: string;
		wijgridEmptyDataRow: string;
		wijgridFilterRow: string;
		wijgridGroupHeaderRow: string;
		wijgridGroupFooterRow: string;
		wijgridGroupHeaderRowCollapsed: string;
		wijgridGroupHeaderRowExpanded: string;
		wijgridFooterRow: string;
		wijgridDetailRow: string;

		wijgridLoadingOverlay: string;
		wijgridLoadingText: string;
		wijgridGroupArea: string;
		wijgridGroupAreaButton: string;
		wijgridGroupAreaButtonSort: string;
		wijgridGroupAreaButtonClose: string;
		wijgridGroupToggleVisibilityButton: string;
		wijgridFixedView: string;
		wijgridScroller: string;
		wijgridDndHelper: string;
		wijgridDndArrowTopContainer: string;
		wijgridDndArrowBottomContainer: string;
		wijgridFreezingHandleV: string;
		wijgridFreezingHandleH: string;
		wijgridResizingHandle: string;
		wijgridHeaderCellSortIcon: string;
		wijgridHeaderCellText: string;
	}

	/** @ignore */
	export interface IScrollingState {
		x: number;
		y: number;
		index?: number;
		scrollLeft?: number;
	}

	/** @ignore */
	export interface IRenderBounds {
		start: number;
		end: number;
	}

	/** @ignore */
	export interface IRowObj extends Array<HTMLTableRowElement> {
		[index: number]: HTMLTableRowElement;
	}

	/**
	* Represents an information about a row of the wijgrid.
	*/
	export interface IRowInfo {
		/**  Associated data. */
		data?: any;

		/** Type of the row */
		type: wijmo.grid.rowType;

		/** State of the row. Only the wijmo.grid.renderState.rendering value is useful in a row context. */
		state: wijmo.grid.renderState;

		/** @ignore */
		sectionRowIndex: number;

		/** Data row index, the index of the data row inside the TBODY section of the wijgrid. */
		dataRowIndex: number;

		/** Data item index. */
		dataItemIndex: number;

		/** @ignore */
		virtualDataItemIndex: number;

		/** @ignore */
		sketchRowIndex: number;

		/** The value that is being grouped. Available for groupHeader and groupFooter rows only. */
		groupByValue?: any;

		/** jQuery object that represents associteted HTML Table rows. */
		$rows: JQuery;

		/** @ignore */
		_extInfo: IRowInfoExt;
	}

	/**
	* Infrastructure.
	* @ignore
	*/
	export interface IRowInfoExt {
		groupLevel?: number;
		groupIndex?: number;
		state?: wijmo.grid.renderStateEx;
	}

	/** @ignore */
	export interface IElementBounds {
		top: number;
		left: number;
		width: number;
		height: number;
	}

	/** @ignore */
	export interface IOwnerable {
		owner?: any;
	}

	/** @ignore */
	export interface IFieldInfo {
		name: string;
		readonly?: boolean;
		type?: string;
		format?: string;
	}

	/** @ignore */
	export interface IColumnWidth {
		width: any;
		real: boolean;
		realPercent?: boolean;
	}

	/** @ignore */
	export interface IFieldInfoCollection {
		[index: string]: IFieldInfo;
	}

	/** Provides data for the cellStyleFormatter option of the wijgrid. */
	export interface ICellStyleFormaterArgs {
		/** 
		* jQuery object that represents cell to format.
		*/
		$cell: JQuery;

		/** 
		* Options of the column to which the cell belongs.
		*/
		column: IColumn;

		/**
		* State of a cell to format.
		*/
		state: wijmo.grid.renderState;

		/** 
		* Information about associated row.
		*/
		row: IRowInfo;

		/** @ignore */
		_cellIndex: number;

		/** @ignore */
		_purpose: any;
	}

	/** @ignore */
	export interface ICurrentCellChangedAdditionalInfo {
		changeSelection: boolean;
		setFocus?: boolean;
		selectionExtendMode?: wijmo.grid.cellRangeExtendMode;
		changingEventArgs?: ICurrentCellChangingEventArgs;
		hasFocusedChild?: boolean;
	}

	/** Data converter that is able to translate values from a string representation to column data type and back. */
	export interface IDataParser {
		/** Converts the value into typed value. */
		parse: (value: any, culture: string, format: string, nullString: string) => any;
		/** Converts the value into its string representation. */
		toStr: (value: any, culture: string, format: string, nullString: string) => string;
	}

	/**
	* Represents a filter operator.
	*/
	export interface IFilterOperator {
		/** An array of datatypes to which the filter can be applied. Possible values for elements of the array are "string", "number", "datetime", "currency" and "boolean". */
		applicableTo: string[];
		/** Operator name.*/
		name: string;
		/** The display name of the operator to show in the filter dropdown.*/
		displayName?: string;
		/** The number of filter operands. Can be either 1 or 2.*/
		arity: number;
		/** Comparison operator; the number of accepted parameters depends on the arity. The first parameter is a data value, the second parameter is a filter value. */
		operator: (dataValue?: any, filterValue?: any) => boolean;
	}

	/**
	* Represents a pager settings.
	*/
	export interface IPagerSettings extends wijmo.pager.IPagerUISettings {
		/** Determines the position of the pager. Possible values are: "bottom", "top", "topAndBottom".
		* @remarks
		* Possible values are:
		* "bottom": Pager appear below the control.
		* "top": Pager appear above the control.
		* "topAndBottom": Pager appear above and below the control.
		*/
		position: string;
	}

	/**
	* Determines virtualization settings.
	*/
	export interface IVirtualizationSettings {
		/** Determines virtual scrolling mode.
		  * Possbile values are: "none", "rows", "columns", "both".
		  * @example
		  * $("#element").wijgrid({
		  *   scrollingSettings: {
		  *       virtualizationSettings: {
		  *          mode: "rows"
		  *       }					
		  *	  }
		  * })
		  * @remarks
		  * Possible values are:
		  *	"none": Virtual scrolling is not used.
		  *	"rows": Rows are rendered on demand.
		  *	"columns": Columns are rendered on demand.
		  *	"both": Both "rows" and "columns" modes are used.
		  * @remarks
		  * The "columns" mode is ignored if the grid uses grouping or band columns.
		  * The "rows" mode is ignored if the grid uses paging, columns merging or fixed rows. This mode cannot be enabled when using dynamic wijdatasource.
		  */
		mode: string;

		/** Determines the height of a rows when rows virtual scrolling is used.
		  * @example
		  * $("#element").wijgrid({
		  *   scrollingSettings: {
		  *       virtualizationSettings: {
		  *          rowHeight: 20
		  *       }					
		  *	  }
		  * })
		  * @remarks
		  * Can be set only during creation
		  */
		rowHeight: number;

		/** Determines the default width of a columns when columns virtual scrolling is used.
		  * @example
		  * $("#element").wijgrid({
		  *   scrollingSettings: {
		  *       virtualizationSettings: {
		  *          columnWidth: 100
		  *       }					
		  *	  }
		  * })
		  * @remarks
		  * Can be set only during creation
		  */
		columnWidth: number;
	}

	/**
	* Determines the scrolling settings.
	*/
	export interface IScrollingSettings {
		/** Determines which scrollbars are active and if they appear automatically based on content size.
		  * Possbile values are: "none", "auto", "horizontal", "vertical", "both".
		  * @example
		  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
		  * $("#element").wijgrid({ scrollingSettings: { mode: "both" } });
		  * @remarks
		  * Possible values are:
		  *	"none": Scrolling is not used; the staticRowIndex and staticColumnIndex values are ignored.
		  *	"auto": Scrollbars appear automatically depending upon content size.
		  *	"horizontal": The horizontal scrollbar is active.
		  *	"vertical": The vertical scrollbar is active.
		  *	"both": Both horizontal and vertical scrollbars are active.
		  */
		mode: string;

		/** Determines whether the user can change position of the static column or row by dragging the vertical or horizontal freezing handle with the mouse. Possible values are: "none", "columns", "rows", "both".
		  * @example
		  * $("#element").wijgrid({ scrollingSettings: { freezingMode: "both" }});
		  * @remarks
		  * Possible values are:
		  * "none": The freezing handle cannot be dragged.
		  * "columns": The user can drag the vertical freezing handle to change position of the static column.
		  * "rows": The user can drag the horizontal freezing handle to change position of the static row.
		  * "both": The user can drag both horizontal and vertical freezing handles.
		  */
		freezingMode: string;

		/** Indicates the index of columns that will always be shown on the left when the grid view is scrolled horizontally.
		  * Note that all columns before the static column will be automatically marked as static, too.
		  * This can only take effect when the scrollingSettings.mode option is not set to "none".
		  * It will be considered "-1" when grouping or row merging is enabled. A "-1" means there is no data column but the row header is static. A zero (0) means one data column and row header are static.
		  * @example
		  * $("#element").wijgrid({ scrollingSettings: { staticColumnIndex: -1 } });
		  */
		staticColumnIndex: number;

		/** Gets or sets the alignment of the static columns area. Possible values are "left", "right".
		* @example
		* $("#element").wijgrid({ scrollingSettings: { staticColumnsAlignment: "left" } });
		* @remarks
		* The "right" mode has limited functionality:
		*  - The showRowHeader value is ignored.
		*  - Changing staticColumnIndex at run-time by dragging the vertical bar is disabled.
		*/
		staticColumnsAlignment: string;

		/** Indicates the index of data rows that will always be shown on the top when the wijgrid is scrolled vertically.
		* Note, that all rows before the static row will be automatically marked as static, too.
		* This can only take effect when the scrollingSettings.mode option is not set to "none". This will be considered "-1" when grouping or row merging is enabled.
		* A "-1" means there is no data row but the header row is static.A zero (0) means one data row and the row header are static.
		* @example
		* $("#element").wijgrid({ scrollingSettings: { staticRowIndex: -1 } });
		*/
		staticRowIndex: number;

		/** Determines virtual scrolling settings. Use it when using large amounts of data to improve efficiency.
		  * @example
		  * $("#element").wijgrid({
		  *   scrollingSettings: {
		  *       virtualizationSettings: {
		  *          mode: "vertical"
		  *       }					
		  *	  }
		  * });
		  * @remarks
		  * Virtual scrolling has some limitations. See the virtualizationSettings.mode property for more details.
		  */
		virtualizationSettings: IVirtualizationSettings;
	}


	/** @ignore */
	export interface ICellRange {
		r1: number;
		r2: number;
		c1: number;
		c2: number;
	}

	/**
	* Used to customize the appearance and position of groups.
	*/
	export interface IGroupInfo {
		/** Determines whether the grid should insert group header and/or group footer rows for this column.  Possible values are: "none", "header", "footer", "headerAndFooter".
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { position: "header" }}] });
		  * @remarks
		  * Possible values are:
		  * "none": disables grouping for the column.
		  * "header": inserts header rows.
		  * "footer": inserts footer rows.
		  * "headerAndFooter": inserts header and footer rows.
		  */
		position: string;

		/** Determines whether the user will be able to collapse and expand the groups by clicking on the group headers, and also determines whether groups will be initially collapsed or expanded.
		  * Possible values are: "none", "startCollapsed", "startExpanded".
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { outlineMode: "startExpanded" }}] });
		  * @remarks
		  * Possible values are:
		  * "none": disables collapsing and expanding.
		  * "startCollapsed": groups are initially collapsed.
		  * "startExpanded": groups are initially expanded.
		  */
		outlineMode: string;

		/** A value indicating whether groupings containing a single row are grouped.
		  * @default true
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { groupSingleRow: true }}] });
		  */
		groupSingleRow: boolean;

		/** Determines the CSS used to show collapsed nodes on the grid.
		  * @default "ui-icon-triangle-1-e".
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { collapsedImageClass: "myClass" }}] });
		  */
		collapsedImageClass: string;

		/** Determines the CSS used to show expanded nodes on the grid.
		  * @default "ui-icon-triangle-1-se".
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { expandedImageClass: "myClass" }}] });
		  */
		expandedImageClass: string;

		/** Determines the text that is displayed in the group header rows.
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { headerText: "{1}: {0}" }}] });
		  * @remarks
		  * The text may include up to three placeholders:
		  * "{0}" is replaced with the value being grouped on.
		  * "{1}" is replaced with the group's column header.
		  * "{2}" is replaced with the aggregate
		  * The text may be set to "custom". Doing so causes the grid groupText event to be raised when processing a grouped header.
		  */
		headerText: string;

		/** Determines the text that is displayed in the group footer rows.
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { footerText: "{1}: {0}" }}] });
		  * @remarks
		  * The text may include up to three placeholders:
		  * "{0}" is replaced with the value being grouped on.
		  * "{1}" is replaced with the group's column header.
		  * "{2}" is replaced with the aggregate
		  * The text may be set to "custom". Doing so causes the grid groupText event to be raised when processing a grouped footer.
		  */
		footerText: string;

		// * infrastructure *
		/** @ignore */
		level: number;

		/** @ignore */
		expandInfo: wijmo.grid.groupRange[];
	}

	/** @ignore */
	export interface IRowAttributes {
		cellsAttributes: any;
		rowAttributes: any;
	}
	/** @ignore */
	export interface ISearchableResult<T> {
		at: number;
		val: T;
	}

	/** @ignore */
	export interface ISearchable<T> {
		find(startFrom: number, callback: (element: T) => any): T;
		findIndex(startFrom: number, callback: (element: T) => any): number;
		findEx(startFrom: number, callback: (element: T) => any): ISearchableResult<T>;
	}

	/** @ignore */
	export interface IWrappedDataItem {
		values: any;
		originalRowIndex: number;
	}

	/** @ignore */
	export interface IVirtualScrollingRequest {
		index: number;
		maxCount: number;
	}

	/** @ignore */
	export interface IUserDataArgs {
		beforeOnRendered?: Function;
		beforeRefresh?: Function;
		afterRefresh?: Function;
		virtualScrollData?: IVirtualScrollData;
		data?: any;
		forceDataLoad?: boolean;
	}

	/** @ignore */
	export interface IVirtualScrollData {
		direction?: string;
		mode?: intersectionMode;
		newBounds?: IRenderBounds;
		data?: any;
		request?: IVirtualScrollingRequest;
	}

	/** Provides data for the cellFormater option of the column. */
	export interface IC1BaseFieldCellFormatterArgs {
		/** @ignore */
		$cell: JQuery;

		/** jQuery object that represents cell container to format. */
		$container: JQuery;

		/** Callback function which is invoked after applying default formatting. */
		afterDefaultCallback: any;

		/** Options of the formatted column. */
		column: IColumn;

		/** Formatted value of the cell. */
		formattedValue: any;

		/** Information about associated row. */
		row: IRowInfo;
	}

	/** @ignore */
	export interface IDOMSource {
		items: any[];
		emptyData: any[];
	}

	/** @ignore */
	export interface IDragnDropElement {
		element: JQuery;
		_canDrag(): boolean;
		_canDropTo(element: any): boolean;
		_canDropToGroupArea(): boolean;
		options: IColumnInternalOptions;
	}

	/** @ignore */
	export interface IColumnInternalOptions extends IOwnerable {
		disabled?: boolean;

		dataIndex?: number;
		_sortOrder?: number;

		_dynamic?: boolean;
		_isLeaf?: boolean;

		groupedIndex?: number;
		_leavesIdx?: number; // index of the column inside the _leaves().
		_linearIdx?: number; // index of the column inside the owner's column collection
		_parentIdx?: number; // index (traversal) of the parent column inside the _allLeaves()
		_travIdx?: number; // index (traversal) of the column inside the _allLeaves()
		_parentVis?: boolean; // 
		_visLeavesIdx?: number; // index of the column inside the _visibleLeaves()
		_renderedIndex?: number; // index of the column among all the rendered columns. If not hv scrolling is used then it matches with the visLeavesIdx property.
		_realWidth?: number;

		_thX?: number; // maps over visible header cells only
		_thX2?: number; // maps over both visible and invisible header cells (used by C1GridView)
		_thY?: number;

		_originalDataKey?: any;
		_originalHeaderText?: string;

		_totalsValue?: any;
		_footerTextDOM?: any;
		_clientType?: string;
	}


	/** Represents options of the base class for all fields.*/
	export interface IC1BaseFieldOptions extends IColumnInternalOptions {
		/** A value indicating whether the column can be moved.
		  * @example
		  * $("#element").wijgrid({ columns: [ { allowMoving: true } ] });
		  */
		allowMoving: boolean;

		/** A value indicating whether the column can be sized.
		  * @example
		  * $("#element").wijgrid({ columns: [ { allowSizing: true } ] });
		  */
		allowSizing: boolean;

		/** Function used for changing content, style and attributes of the column cells.
		  * @example
		  * // Add an image which URL is obtained from the "Url" data field to the column cells.
		  * $("#demo").wijgrid({
		  *		data: [
		  *			{ ID: 0, Url: "/images/0.jpg" },
		  *			{ ID: 1, Url: "/images/1.jpg" }
		  *		],
		  *		columns: [
		  *			{},
		  *			{
		  *				cellFormatter: function (args) {
		  *					if (args.row.type & wijmo.grid.rowType.data) {
		  *						args.$container
		  *							.empty()
		  *							.append($("<img />")
		  *								.attr("src", args.row.data.Url));
		  *
		  *						return true;
		  *					}
		  *				}
		  *			}
		  *		]
		  * });
		  * @remarks
		  * @type {Function}
		  * Important: cellFormatter should not alter content of header and filter row cells container.
		  * @param {wijmo.grid.IC1BaseFieldCellFormatterArgs} args The data with this function.
		  * @returns {Boolean} True if container content has been changed and wijgrid should not apply the default formatting to the cell.
		  */
		cellFormatter: (args: IC1BaseFieldCellFormatterArgs) => boolean;

		/** Determines whether to use number type column width as the real width of the column.
		  * @example
		  * $("#element").wijgrid({ columns: [{ ensurePxWidth: true }]});
		  * @remarks
		  * If this option is set to true, wijgrid will use the width option of the column widget.
		  * If this option is undefined, wijgrid will refer to the ensureColumnsPxWidth option.
		  */
		ensurePxWidth: boolean;

		/** Gets or sets the footer text.
		  * The text may include a placeholder: "{0}" is replaced with the aggregate.
		  * @example
		  * $("#element").wijgrid({ columns: [{ footerText: "footer" }]});
		  * @remarks
		  * If the value is undefined the footer text will be determined automatically depending on the type of the datasource:
		  * DOM table - text in the footer cell.
		  */
		footerText: string;

		/** Gets or sets the header text.
		  * @example
		  * $("#element").wijgrid({ columns: [ { headerText: "column0" } ] });
		  * @remarks
		  * If the value is undefined the header text will be determined automatically depending on the type of the datasource:
		  * DOM table - text in the header cell.
		  * Array of objects - dataKey (name of the field associated with column).
		  * Two-dimensional array - dataKey (index of the field associated with column).
		  */
		headerText: string;

		/** Gets or sets the text alignment of data cells. Possible values are "left", "right", "canter".
		  * @example
		  * $("#element").wijgrid({ columns: [{ textAligment: "right" }]});
		  * @remarks
		  */
		textAlignment: string;

		/** A value indicating whether column is visible.
		  * @example
		  * $("#element").wijgrid({ columns: [{ visible: true }]});
		  */
		visible: boolean;

		/** Determines the width of the column.
		  * @type {String|Number}
		  * @example
		  * $("#element").wijgrid({ columns: [ { width: 150 } ] });
		  * $("#element").wijgrid({ columns: [ { width: "10%" } ]});
		  * @remarks
		  * The option could either be a number of string.
		  * Use number to specify width in pixel, use string to specify width in percentage.
		  * By default, wijgrid emulates the table element behavior when using number as width. This means wijgrid may not have the exact width specified. If exact width is needed, please set ensureColumnsPxWidth option of wijgrid to true.
		  */
		width: any;
	}

	/** Represents options of a class for for all data-bound fields.*/
	export interface IC1FieldOptions extends IC1BaseFieldOptions {
		/** Causes the grid to calculate aggregate values on the column and place them in the column footer cell or group header and footer rows.
		  * Possible values are: "none", "count", "sum", "average", "min", "max", "std", "stdPop", "var", "varPop" and "custom".
		  * @example
		  * $("#element").wijgrid({ columns: [{ aggregate: "count" }]});
		  * @remarks
		  * Possible values are:
		  * "none": no aggregate is calculated or displayed.
		  * "count": count of non-empty values.
		  * "sum": sum of numerical values.
		  * "average": average of the numerical values.
		  * "min": minimum value (numerical, string, or date).
		  * "max": maximum value (numerical, string, or date).
		  * "std": standard deviation (using formula for Sample, n-1).
		  * "stdPop": standard deviation (using formula for Population, n).
		  * "var": variance (using formula for Sample, n-1).
		  * "varPop": variance (using formula for Population, n).
		  * "custom": custom value (causing grid to throw groupAggregate event).
		  *
		  * If the showFooter option is off or grid does not contain any groups, setting the "aggregate" option has no effect.
		  */
		aggregate: string;

		/** A value indicating whether column can be sorted.
		  * @example
		  * $("#element").wijgrid({ columns: [{ allowSort: true }] });
		  */
		allowSort: boolean;
		/** A value indicating the key of the data field associated with a column.
		  * If an array of objects is used as a datasource for wijgrid, this should be string value,
		  * otherwise this should be an integer determining an index of the field in the datasource.
		  * @type {String|Number}
		  * @example
		  * $("#element").wijgrid({ columns: [{ dataKey: "ProductID" }]});
		  */
		dataKey: any;

		/** Column data type. Defines the rules, according to which column value will be formatted, defines editors types and allowed filter operators.
		  * Does not change the type of source data, besides the case when data source is HTMLTable.
		  * Possible values are: "string", "number", "datetime", "currency" and "boolean".
		  * @example
		  * $("#element").wijgrid({ columns: [{ dataType: "string" }]});
		  * @remarks
		  * Possible values are:
		  * "string": if using built-in parser any values are acceptable; "&nbsp;" considered as an empty string, nullString as null.
		  * "number": if using built-in parser only numeric values are acceptable, also "&nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
		  * "datetime": if using built-in parser only date-time values are acceptable, also "&nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
		  * "currency": if using built-in parser only numeric and currency values are acceptable, also "&nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
		  * "boolean": if using built-in parser only "true" and "false" (case-insensitive) values are acceptable, also "&nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
		  */
		dataType: string;

		/** Data converter that is able to translate values from a string representation to column data type and back.
		  * @example
		  * var myBoolParser = {
		  *		parseDOM: function (value, culture, format, nullString) {
		  *			return this.parse(value.innerHTML, culture, format, nullString);
		  *		},
		  *		parse: function (value, culture, format, nullString) {
		  *			if (typeof (value) === "boolean")  return value;
		  *
		  *			if (!value || (value === "&nbsp;") || (value === nullString)) {
		  *				return null;
		  *			}
		  *
		  *			switch (value.toLowerCase()) {
		  *				case "on": return true;
		  *				case "off": return false;
		  *			}
		  *
		  *			return NaN;
		  *		},
		  *		toStr: function (value, culture, format, nullString) {
		  *			if (value === null)  return nullString;
		  *				return (value) ? "on" : "off";
		  *			}
		  *		}
		  * }
		  *
		  * $("#element").wijgrid({ columns: [ { dataType: "boolean", dataParser: myBoolParser } ] });
		  * @remarks
		  * If undefined, than the built-in parser for supported datatypes will be used.
		  */
		dataParser: IDataParser;

		/** A pattern used for formatting and parsing column values.
		  * @example
		  * $("#element").wijgrid({
		  *		columns: [
		  *			{ dataType: "currency" }, 
		  *			{ dataType: "number" }, 
		  *			{ dataType: "number", dataFormatString: "p0" }
		  *		]
		  * });
		  * @remarks
		  * The default value is undefined ("n" pattern will be used for "number" dataType, "d" for "datetime", "c" for "currency").
		  * Please see the https://github.com/jquery/globalize for a full explanation and additional values.
		  */
		dataFormatString: string;

		/** A value indicating whether data values are HTML-encoded before they are displayed in a cell.
		  * @example
		  * $("#element").wijgrid({
		  *		data: [
		  *			[0, "<b>value</b>"],
		  *			[1, "&amp;"],
		  *		],
		  *		columns: [
		  *			{ headerText: "ID" }, 
		  *			{ headerText: "Value", encodeHtml: true }
		  *		]
		  * });
		  */
		encodeHtml: boolean;

		/** An operations set for filtering. Must be either one of the embedded operators or custom filter operator.
		  * Operator names are case insensitive. 
		  *
		  * @example
		  * $("#element").wijgrid({ columns: [{ dataType: "number", filterOperator: "Equals", filterValue: 0 }]});
		  * @remarks
		  * Embedded filter operators include:
		  * "NoFilter": no filter.
		  * "Contains": applicable to "string" data type.
		  * "NotContain": applicable to "string" data type.
		  * "BeginsWith": applicable to "string" data type.
		  * "EndsWith": applicable to "string" data type.
		  * "Equals": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "NotEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "Greater": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "Less": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "GreaterOrEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "LessOrEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "IsEmpty": applicable to "string".
		  * "NotIsEmpty": applicable to "string".
		  * "IsNull": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  * "NotIsNull": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
		  *
		  * Full option value is:
		  *		[filterOperartor1, ..., filterOperatorN]
		  * where each filter item is an object of the following kind:
		  *		{ name: <operatorName>, condition: "or"|"and" }
		  * where:
		  *		name: filter operator name.
		  *		condition: logical condition to other operators, "or" is by default.
		  * Example:
		  *		filterOperator: [ { name: "Equals" }, { name: "NotEqual", condition: "and" } ]
		  * It is possible to use shorthand notation, the following statements are equivalent:
		  *		filterOperator: [ { name: "Equals" }, { name: "BeginsWith" } ]
		  *		filterOperator: [ "Equals", "BeginsWith" ]
		  * In the case of a single operator option name may contain only filter operator name, the following statements are equivalent:
		  *		filterOperator: [ { name: "Equals" } ]
		  *		filterOperator: [ "Equals" ]
		  *		filterOperator: "Equals"
		  *
		  * Note: wijgrid built-in filter editors do not support multiple filter operators.
		  *
		  */
		filterOperator: any;

		/** A value set for filtering.
		  * @example
		  * $("#element").wijgrid({ columns: [{ dataType: "number", filterOperator: "Equals", filterValue: 0 }]});
		  * @remarks
		  * Full option value is:
		  *		[filterValue1, ..., filterValueN]
		  * where each item is a filter value for the corresponding filter operator. Example:
		  *		filterValue: [0, "a", "b"]
		  * 
		  * Built-in filter operators support array of values as an argument. Example:
		  *		filterOperator: ["Equals", "BeginsWith"]
		  *		filterValue: [[0, 1, 2], "a"]
		  * As a result of filtering all the records having 0, 1, 2, or starting with "a" will be fetched.
		  *
		  * Shorthand notation allows omitting square brackets, the following statements are equivalent:
		  *		filterValue: ["a"]
		  *		filterValue: [["a"]]
		  *		filterValue: "a"
		  *
		  * Note: wijgrid built-in filter editors do not support multiple filter values.
		  */
		filterValue: any;

		/** Used to customize the appearance and position of groups.
		  * @example
		  * $("#element").wijgrid({ columns: [{ groupInfo: { position: "header" }}]});
		  */
		groupInfo: IGroupInfo;

		/**
		* Controls the state of the input method editor for text fields.
		* Possible values are: "auto", "active", "inactive", "disabled".
		* Please refer to https://developer.mozilla.org/en-US/docs/Web/CSS/ime-mode for more info.
		* @example
		* $("#element").wijgrid({ columns: [{ imeMode: "auto" }]});
		*/
		imeMode: string;

		/**
		* Determines the type of html editor for filter and cells.
		* Possible values are: "number", "date", "datetime", "datetime-local", "month", "time", "text".
		* @example
		* $("#element").wijgrid({ columns: [{ inputType: "text" }]});
		* @remarks
		* If the value is set then input type element is used with "type" attribute set to the value. If the value is not set then:
		*  - in desktop environment a "text" input element is used as the editor.
		*  - in mobile environment a "number" input element is used for columns having "number" and "currency" dataType; for columns where dataType = "datetime" a "datetime" input element is used, otherwise a "text" input element is shown.
		*/
		inputType: string;

		/** A value indicating whether the cells in the column can be edited.
		  * @example
		  * $("#element").wijgrid({ columns: [ { readOnly: false } ] });
		  */
		readOnly: boolean;

		/** Determines whether rows are merged. Possible values are: "none", "free" and "restricted".
		  * @example
		  * $("#element").wijgrid({ columns: [{ rowMerge: "none" }]});
		  * @remarks
		  * Possible values are:
		  * "none": no row merging.
		  * "free": allows row with identical text to merge.
		  * "restricted": keeps rows with identical text from merging if rows in the previous column are merged.
		  */
		rowMerge: string;

		/** A value indicating whether filter editor will be shown in the filter row.
		  * @example
		  * $("#element").wijgrid({ columns: [{ showFilter: true }]});
		  */
		showFilter: boolean;

		/** Determines the sort direction. Possible values are: "none", "ascending" and "descending".
		  * @example
		  * $("#element").wijgrid({ columns: [{ sortDirection: "none" }]});
		  * @remarks
		  * Possible values are:
		  * "none": no sorting.
		  * "ascending": sort from smallest to largest.
		  * "descending": sort from largest to smallest.
		  */
		sortDirection: string;

		/** A value indicating whether null value is allowed during editing.
		  * @example
		  * $("#element").wijgrid({ columns: [{ valueRequired: false }]});
		  */
		valueRequired: boolean;
	}

	/** Represents options of a class which is used to create multilevel column headers.*/
	export interface IC1BandFieldOptions extends IC1BaseFieldOptions {
		/**
		* Gets a array of objects representing the band columns.
		* @example
		* $("#element").wijgrid({
		*   columns: [{
		*      headerText: "Band",
		*      columns: [
		*         { headerText: "ID" },
		*         { headerText: "Name" }
		*      ]
		*   }]
		* });
		*/
		columns: IColumn[];
	}

	/** Represents options of the base class for all button fields.*/
	export interface IC1ButtonBaseFieldOptions extends IC1BaseFieldOptions {
		/** Gets or sets the type of the button in the column. Possible values are "link", "button", "imageButton", "image".
		  * @example
		  * $("#element").wijgrid({ columns: [{ buttonType: "button" }]});
		  */
		buttonType: string;
	}

	/** Represents options of a field that displays a command button.*/
	export interface IC1ButtonFieldOptions extends IC1ButtonBaseFieldOptions {
		/** Represents options of a command button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       buttonType: "link",
		  *       command: {
		  *          text: "myCommand",
		  *          click: function (e, args) { 
		  *             alert("clicked!");
		  *          }
		  *       }
		  *    }]
		  * });
		 */
		command: ICommandButton;
	}

	/** Represents options of a field that displays command buttons to perform editing and deleting operations.*/
	export interface IC1CommandButtonFieldOptions extends IC1ButtonBaseFieldOptions {
		/** Gets or sets a value indicating whether a Delete button is displayed in a command column.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       showDeleteButton: true
		  *    }]
		  * });
		*/
		showDeleteButton: boolean;

		/** Gets or sets a value indicating whether an Edit button is displayed in a command column.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       showEditButton: true
		  *    }]
		  * });
		*/
		showEditButton: boolean;

		/** Represents options of a Cancel command button. 
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       cancelCommand: {
		  *          text: "Cancel!"
		  *       }
		  *    }]
		  * });
		  */
		cancelCommand: ICommandButton;

		/** Represents options of a Delete command button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       deleteCommand: {
		  *          text: "Delete!"
		  *       }
		  *    }]
		  * });
		  */
		deleteCommand: ICommandButton;

		/** Represents options of an Edit command button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       editCommand: {
		  *          text: "Edit!"
		  *       }
		  *    }]
		  * });
		  */
		editCommand: ICommandButton;

		/** Represents options of an Update commnd button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       updateCommand: {
		  *          text: "Update!"
		  *       }
		  *    }]
		  * });
		  */
		updateCommand: ICommandButton;
	}

	/** Represents options of the command button. Allows button look customization and track mouse click on it. */
	export interface ICommandButton {
		/** Determines the text that is displayed in the button.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       command: {
		  *          text: "MyButton"
		  *       }
		  *    }]
		  * });
		  */
		text: string;

		/** Determines the data field to bind a button text to it.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       command: {
		  *          text: "{0}",
		  *          textDataKey: "ID"
		  *       }
		  *    }]
		  * });
		  */
		textDataKey: string;

		/** The click event handler is a function that is called when a button is clicked. This event is cancellable.
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       deleteCommand: {
		  *          click: function(e, args) {
		  *             return confirm("Are you sure?");
		  *          }
		  *       }
		  *    }]
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IButtonCommandEventArgs} args The data with this event.
		  */
		click: (e: JQueryEventObject, arg: IButtonCommandEventArgs) => void;

		/** Determines the CSS class used to display an image button.
		  *	@remarks
		  *	Can be used only if the buttonType option of a button field is set to "imageButton" or "image".
		  * @example
		  * $("#element").wijgrid({
		  *    columns: [{
		  *       buttonType: "imageButton",
		  *       command: {
		  *          iconClass: "ui-icon-info",
		  *          click: function(e, args) {
		  *             alert(args.row.data.ID);
		  *          }
		  *       }
		  *    }]
		  * });
		  */
		iconClass: string;
	}

	/** Combines all of the available options */
	export interface IColumn extends IColumnInternalOptions, IC1BaseFieldOptions, IC1FieldOptions, IC1BandFieldOptions,
	IC1ButtonBaseFieldOptions, IC1ButtonFieldOptions, IC1CommandButtonFieldOptions {
	}

	/** @ignore */
	export interface IColumnHeaderInfo {
		column: IColumn;
		rowSpan: number;
		colSpan: number;
	}

	// ** event args **

	/** 
	* Provides data for the click event of a command buttons.
	*/
	export interface IButtonCommandEventArgs {
		/** The associated column. */
		column: IColumn;
		/** Information about the associated row. */
		row: IRowInfo;
	}

	/** 
	* Provides data for the editing events of the wijgrid.
	*/
	export interface IBaseCellEditEventArgs {
		/** Gets the edited cell's information. */
		cell: wijmo.grid.cellInfo;
	}

	/**
	* Provides data for the beforeCellEdit event of the wijgrid.
	*/
	export interface IBeforeCellEditEventArgs extends IBaseCellEditEventArgs {
		/** The event initiated cell editing. */
		event: JQueryEventObject;

		/** Gets or sets a value that determines how cell editing is initiated: manually or automatically.
		* If the cell contain custom controls or if you wish to provide custom editing on the front end, then the property must be set to "true".
		* The default value is "false", which means that the wijgrid will handle editing automatically.
		*/
		handled: boolean;
	}

	/**
	* Provides data for the afterCellEdit event of the wijgrid.
	*/
	export interface IAfterCellEditEventArgs extends IBaseCellEditEventArgs {
		/** The event that initiates the cell update. */
		event: JQueryEventObject;

		/** Gets or sets a value that determines how cell editing is finalized: automatically or manually.
		* The default value is "false", which means that the wijgrid will try to finalize cell editing automatically by applying the column's cell formatter to the cell.
		* If this behavior is not suitable for you, then this property must be set to "true".
		*/
		handled: boolean;
	}

	/** 
	* Provides data for the beforeCellUpdate event of the wijgrid.
	*/
	export interface IBeforeCellUpdateEventArgs extends IBaseCellEditEventArgs {
		/**  Gets the new cell value. If the property value is not changed, then the widget will try to extract the new cell value automatically.
		* If you provide custom editing on the front end, then they new cell value must be returned in this property.
		*/
		value: any;
	}

	/**
	* Provides data for the afterCellUpdate event of the wijgrid.
	*/
	export interface IAfterCellUpdateEventArgs extends IBaseCellEditEventArgs {
	}

	/**
	* Provides data for the invalidCellValue event of the wijgrid.
	*/
	export interface IInvalidCellValueEventArgs extends IBaseCellEditEventArgs {
		/** Gets the current value */
		value: any;
	}

	/**
	* Provides data for the cellClicked event of the wijgrid.
	*/
	export interface ICellClickedEventArgs {
		/** Gets the clicked cell's information. */
		cell: wijmo.grid.cellInfo;
	}

	/**
	* Provides data for the columnDragging event of the wijgrid.
	*/
	export interface IColumnDraggingEventArgs {
		/** The column being dragged. */
		drag: IColumn;
		/** The location of the column that is being dragged. The possible values are "groupArea" or "columns." */
		dragSource: string;
	}

	/**
	* Provides data for the columnDragged event of the wijgrid.
	*/
	export interface IColumnDraggedEventArgs extends IColumnDraggingEventArgs {
	}

	/**
	* Provides data for the columnDropping event of the wijgrid.
	*/
	export interface IColumnDroppingEventArgs {
		/** The column being dragged. */
		drag: IColumn;

		/** The column on which the drag source is dropped: the drop target. */
		drop: IColumn;

		/** The position at which the dragged column is dropped relative to the drop target. The possible values are "left", "right", or "center". */
		at: string;
	}

	/** Provides data for the columnDropped event of the wijgrid. */
	export interface IColumnDroppedEventArgs extends IColumnDroppingEventArgs {
	}

	/**
	* Provides data for the columnGrouping event of the wijgrid.
	*/
	export interface IColumnGroupingEventArgs {
		/** The column being dragged: the drag source. */
		drag: IColumn;

		/** The drop target, the place where the dragged column is dropped. This parameter is null if you are dropping a column into an empty group area. */
		drop: IColumn;

		/** The location of the dragged column. The possible values are "groupArea" and "columns". */
		dragSource: string;

		/** The location of the dropped column. The possible values are "groupArea" and "columns". */
		dropSource: string;

		/** The position at which the dragged column is dropped relative to the drop target. The possible values are "left", "right", or "center". The value is "left" if you're dropping the column into an empty group area. */
		at: string;
	}

	/**
	* Provides data for the columnGrouped event of the wijgrid.
	*/
	export interface IColumnGroupedEventArgs extends IColumnGroupingEventArgs {
	}

	/**
	* Provides data for the columnResizing event of the wijgrid.
	*/
	export interface IColumnResizingEventArgs {
		/** The column that is being resized. */
		column: IColumn;

		/** The width of the column before it is resized. */
		oldWidth: any;

		/** args.newWidth: The new width being set for the column. */
		newWidth: any;
	}

	/**
	* Provides data for the columnResized event of the wijgrid.
	*/
	export interface IColumnResizedEventArgs {
		/** The column that is being resized. */
		column: IColumn;
	}

	/**
	* Provides data for the columnUngrouping event of the wijgrid.
	*/
	export interface IColumnUngroupingEventArgs {
		/** The column being ungrouped */
		column: IColumn;
	}

	/**
	* Provides data for the columnUngrouped event of the wijgrid.
	*/
	export interface IColumnUngroupedEventArgs extends IColumnUngroupingEventArgs {
	}

	/**
	* Provides data for the currentCellChanging event of the wijgrid.
	*/
	export interface ICurrentCellChangingEventArgs {
		/** The new cell index. */
		cellIndex: number;

		/** The new row index. */
		rowIndex: number;

		/** The old cell index. */
		oldCellIndex: number;

		/** The old row index. */
		oldRowIndex: number;
	}

	/** Provides data for the detailCreating event of the wijgrid.*/
	export interface IDetailCreatingEventArgs {
		/** Defines create options of the detail grid. */
		options: IWijgridOptions;

		/** Returns related master key. */
		masterKey: IDataKeyArray;
	}

	/**
	* Provides data for the filterOperatorsListShowing event of the wijgrid.
	*/
	export interface IFilterOperatorsListShowingEventArgs {
		/** The associated column. */
		column: IColumn;

		/** An array of filter operators. */
		operators: IFilterOperator[];
	}

	/**
	* Provides data for the filtering event of the wijgrid.
	*/
	export interface IFilteringEventArgs {
		/** The column that is being filtered. */
		column: IColumn;

		/** The new filter operator name. */
		operator: string;

		/** The new filter value. */
		value: any;
	}

	/**
	* Provides data for the filtered event of the wijgrid.
	*/
	export interface IFilteredEventArgs {
		/** The column that is being filtered. */
		column: IColumn;
	}

	/**
	* Provides data for the groupAggregate event of the wijgrid.
	*/
	export interface IGroupAggregateEventArgs {
		/** The data object. */
		data: any;

		/** The column that is being grouped. */
		column: IColumn;

		/** The column initiated grouping. */
		groupByColumn: IColumn;

		/** The text that is being grouped. */
		groupText: string;

		/** The text that will be displayed in the group header or group footer. */
		text: string;

		/** The first index for the data being grouped. */
		groupingStart: number;

		/** The last index for the data being grouped. */
		groupingEnd: number;

		/** Specifies whether or not the row you are grouping is a group header. */
		isGroupHeader: boolean;
	}

	/**
	* Provides data for the groupText event of the wijgrid.
	*/
	export interface IGroupTextEventArgs extends IGroupAggregateEventArgs {
		/** The aggregate value. */
		aggregate: any;
	}

	/**
	* Provides data for the pageIndexChanging event of the wijgrid.
	*/
	export interface IPageIndexChangingEventArgs {
		/** The new page index. */
		newPageIndex: number;
	}

	/**
	* Provides data for the pageIndexChanged event of the wijgrid.
	*/
	export interface IPageIndexChangedEventArgs {
	}

	/**
	* Provides data for the selectionChanged event of the wijgrid.
	*/
	export interface ISelectionChangedEventArgs {
		/** The cells that have been added to the selection. */
		addedCells: wijmo.grid.cellInfoOrderedCollection;

		/** The cells that have been removed from the selection. */
		removedCells: wijmo.grid.cellInfoOrderedCollection;
	}

	/**
	* Provides data for the sorting event of the wijgrid.
	*/
	export interface ISortingEventArgs {
		/** The column that is being sorted. */
		column: IColumn;

		/** The new sort direction. */
		sortDirection: string;

		/** Combines args.column.dataKey and args.sortDirection in a shorthand notation to represent a sorting command: "&lt;dataKey&gt; &lt;asc|desc&gt;". */
		sortCommand: string;
	}

	/**
	* Provides data for the sorted event of the wijgrid.
	*/
	export interface ISortedEventArgs extends ISortingEventArgs {
	}

	// ** event args **

	/** Represents options of the wijgrid.*/
	export interface IWijgridOptions {
		/** @ignore */
		_aspNetDisabledClass?: string;
		/** @ignore */
		disabled?: boolean; // inherited
		/** @ignore */
		_htmlTrimMethod?: trimMethod; // determines how to trim values when reading them from HTMLTable
		/** @ignore */
		_isDetail?: boolean;
		/** @ignore */
		wijMobileCSS?: any;
		/** @ignore */
		wijCSS?: wijgridWijmoCSS;

		/** A value indicating whether columns can be moved.
		  * @example
		  * // Columns cannot be dragged and moved if this option is set to false
		  * $("#element").wijgrid({ allowColMoving: false });
		  * @remarks
		  * This option must be set to true in order to drag column headers to the group area.
		  */
		allowColMoving?: boolean;

		/** Determines whether the column width can be increased and decreased by dragging the sizing handle, or the edge of the column header, with the mouse.
		  * @example
		  * // The sizing handle cannot be dragged and column width cannot be changed if this option is set to false
		  * $("#element").wijgrid({ allowColSizing: false });
		  */
		allowColSizing?: boolean;

		/** Determines whether the user can make changes to cell contents in the grid.
		  * This option is obsolete. Use the editingMode option instead.
		  * @example
		  * // Users cannot change cell contents in the grid if this option is set to false
		  * $("#element").wijgrid({ allowEditing: false });
		  */
		allowEditing?: boolean;

		/** Determines whether the user can move the current cell using the arrow keys.
		  * @example
		  * // Users cannot move the selection using arrow keys if this option is set to false
		  * $("#element").wijgrid({ allowKeyboardNavigation: false });
		  */
		allowKeyboardNavigation?: boolean;

		/** Determines the action to be performed when the user presses the TAB key. 
		  * @example
		  * $("#element").wijgrid({ keyActionTab: "moveAcross" });
		  * @remarks
		  * This option is invalid when the allowKeyboardNavigation is set to false.
		  * Possible values are:
		  * "moveAcross": The focus will be kept inside the grid and current selected cell will move cyclically between grid cells when user press TAB or SHIFT+TAB key.
		  * "moveAcrossOut": The focus will be able to be moved from the grid to the next focusable element in the tab order when user press TAB key and the current selected cell is the last cell (or press SHIFT+TAB and the current selected cell is the first cell).    
		  */
		keyActionTab?: string;

		/** Determines whether the grid should display paging buttons. The number of rows on a page is determined by the pageSize option.
		  * @example
		  * // Grid displays paging buttons when allowPaging is true. The pageSize here sets 5 rows to a page.
		  * $("#element").wijgrid({ allowPaging: false, pageSize: 5 });
		  */
		allowPaging?: boolean;

		/** Determines whether the widget can be sorted by clicking the column header.
		  * @example
		  * // Sort a column by clicking its header when allowSorting is set to true
		  * $("#element").wijgrid({ allowSorting: false });
		  */
		allowSorting?: boolean;

		/** A value that indicates whether virtual scrolling is allowed. Set allowVirtualScrolling to true when using large amounts of data to improve efficiency.
		  * Obsoleted, set the scrollingSettings.virtualization.mode property to "rows" instead.
		  * @example
		  * $("#element").wijgrid({ allowVirtualScrolling: false });
		  * @remarks
		  * This option is ignored if the grid uses paging, columns merging or fixed rows. This option cannot be enabled when using dynamic wijdatasource.
		  */
		allowVirtualScrolling?: boolean;

		/** This function is called each time wijgrid needs to change cell appearence, for example, when the current cell position is changed or cell is selected.
		  * Can be used for customization of cell style depending on its state.
		  * @example
		  * // Make the text of the current cell italic.
		  * $("#element").wijgrid({
		  *		highlightCurrentCell: true,
		  *		cellStyleFormatter: function(args) {
		  *			if ((args.row.type & wijmo.grid.rowType.data)) {
		  *				if (args.state & wijmo.grid.renderState.current) {
		  *					args.$cell.css("font-style", "italic");
		  *				} else {
		  *					args.$cell.css("font-style", "normal");
		  *				}
		  *			}
		  *		}
		  * });
		  * @param {wijmo.grid.ICellStyleFormaterArgs} args The data with this function.
		  * @remarks
		  * The args.state parameters equal to wijmo.grid.renderState.rendering means that the cell is being created,
		  * at this moment you can apply general formatting to it indepentant of any particular state, like "current" or "selected".
		  */
		cellStyleFormatter?: (args: ICellStyleFormaterArgs) => void;

		/** A value that indicates calendar's options in grid. It works for calendar in inputdate.
		 * @remarks Its value is wijcalendar's option, visit 
		 * http://wijmo.com/docs/wijmo/#Wijmo~jQuery.fn.-~wijcalendar.html for more details. 
		 * @type {object}
		 * @example
		 *      $("#eventscalendar").wijgrid(
		 *      { calendar: { prevTooltip: "Previous", nextTooltip: "Next" } });
		 */
		calendar?: any;

		/** An array of column options.
		  * @example
		  * $("#element").wijgrid({ columns: [ { headerText: "column0", allowSort: false }, { headerText: "column1", dataType: "number" } ] });
		  */
		columns?: IColumn[];

		/** Determines behavior for column autogeneration. Possible values are: "none", "append", "merge".
		* @example
		* $("#element").wijgrid({ columnsAutogenerationMode: "merge" });
		* @remarks
		* Possible values are:
		* "none": Column auto-generation is turned off.
		* "append": A column will be generated for each data field and added to the end of the columns collection.
		* "merge": Each column having dataKey option not specified will be automatically bound to the first unreserved data field.For each data field not bound to any column a new column will be generated and added to the end of the columns collection.
		*
		* To prevent automatic binding of a column to a data field set its dataKey option to null.
		*
		* Note: columns autogeneration process affects the options of columns and the columns option itself.
		*/
		columnsAutogenerationMode?: string;

		/** Determines the culture ID.
		  * @example
		  * // This code sets the culture to English.
		  * $("#element").wijgrid({ culture: "en" });
		  * @remarks
		  * Please see the https://github.com/jquery/globalize for more information.
		  */
		culture?: string;

		/** A value that indicators the culture calendar to format the text.
		* This option must work with culture option.
		* @example
		* // This code sets the culture and calendar to Japanese.
		* $("#element").wijgrid({ culture: "ja-jp", cultureCalendar: "Japanese" });
		*/
		cultureCalendar?: string;

		/** An array of custom user filters. Use this option if you want to extend the default set of filter operators with your own. Custom filters will be shown in the filter dropdown.
		* @example
		* var oddFilterOp = {
		*	name: "customOperator-Odd",
		*	arity: 1,
		*	applicableTo: ["number"],
		*	operator: function(dataVal) { return (dataVal % 2 !== 0); }
		* }
		*
		* $("#element").wijgrid({ customFilterOperators: [oddFilterOp] });
		*/
		customFilterOperators?: IFilterOperator[];

		/** Determines the datasource.
		  * Possible datasources include:
		  *		1. A DOM table. This is the default datasource, used if the data option is null. Table must have no cells with rowSpan and colSpan attributes.
		  *		2. A two-dimensional array, such as [[0, "a"], [1, "b"]].
		  *		3. An array of objects, such as [{field0: 0, field1: "a"}, {field0: 1, field1: "b'}].
		  *		4. A wijdatasource.   
		  *		5. A wijdataview.
		  * @example
		  * // DOM table
		  * $("#element").wijgrid();
		  * // two-dimensional array
		  * $("#element").wijgrid({ data: [[0, "a"], [1, "b"]] });
		  */
		data?: any;

		/** @ignore */
		detail?: IDetailSettings;

		/** Determines an action to bring a cell in the editing mode when the editingMode option is set to "cell". Possible values are: "click", "doubleClick", "auto".
		  * @example
		  * $("#element").wijgrid({ editingInitOption: "auto" });
		  * @remarks
		  * Possible values are:
		  *	"click": cell is edited via a single click.
		  *	"doubleClick": cell is edited via a double click.
		  *	"auto": action is determined automatically depending upon user environment. If user has a mobile platform then "click" is used, "doubleClick" otherwise.
		  */
		editingInitOption?: string;

		/** Determines the editing mode. Possible values are: "none", "row", "cell",
		  * @example
		  * $("#element").wijgrid({
		  *    editingMode: "row",
		  *    columns: [{
		  *       showEditButton: true
		  *    }] 
		  * });
		  * @remarks
		  * Possible values are:
		  * "none": the editing ability is disabled.
		  *	"cell": a single cell can be edited via a double click.
		  *	"row": a whole row can be edited via a command column.
		  */
		editingMode?: string;

		/** Determines if the exact column width, in pixels, is used.
		  * @example
		  * $("#element").wijgrid({ ensureColumnsPxWidth: true });
		  * @remarks
		  * By default, wijgrid emulates the table element behavior when using a number as the width. This means wijgrid may not have the exact width specified. If exact width is needed, please set the ensureColumnsPxWidth option of wijgrid to true. If this option is set to true, wijgrid will not expand itself to fit the available space.Instead, it will use the width option of each column widget.
		  */
		ensureColumnsPxWidth?: boolean;

		/** Determines the order of items in the filter drop-down list.
		  * Possible values are: "none", "alphabetical", "alphabeticalCustomFirst" and "alphabeticalEmbeddedFirst"
		  * @example
		  * $("#element").wijgrid({ filterOperatorsSortMode: "alphabeticalCustomFirst" });
		  * @remarks
		  * Possible values are:
		  *	"none": Operators follow the order of addition; built-in operators appear before custom ones.
		  *	"alphabetical": Operators are sorted alphabetically.
		  *	"alphabeticalCustomFirst": Operators are sorted alphabetically with custom operators appearing before built-in ones.
		  *	"alphabeticalEmbeddedFirst": Operators are sorted alphabetically with built-in operators appearing before custom operators.
		  *
		  * "NoFilter" operator is always first.
		  */
		filterOperatorsSortMode?: string;

		/** Determines whether the user can change position of the static column or row by dragging the vertical or horizontal freezing handle with the mouse. Possible values are: "none", "columns", "rows", "both".
		  * Obsoleted, use the scrollingSettings.freezingMode property instead.
		  * @example
		  * $("#element").wijgrid({ freezingMode: "both" });
		  * @remarks
		  * Possible values are:
		  * "none": The freezing handle cannot be dragged.
		  * "columns": The user can drag the vertical freezing handle to change position of the static column.
		  * "rows": The user can drag the horizontal freezing handle to change position of the static row.
		  * "both": The user can drag both horizontal and vertical freezing handles.
		  */
		freezingMode?: string;

		/** Determines the caption of the group area.
		  * @example
		  * // Set the groupAreaCaption to a string and the text appears above the grid
		  * $("#element").wijgrid({ groupAreaCaption: "Drag a column here to group by that column." });
		  */
		groupAreaCaption?: string;

		/** Determines the indentation of the groups, in pixels.
		  * @example
		  * // Set the groupIndent option to the number of pixels to indent data when grouping.
		  * $("#element").wijgrid({ groupIndent: 15 });
		  */
		groupIndent?: number;

		/** Determines whether the position of the current cell is highlighted or not.
		  * @example
		  * $("#element").wijgrid({ highlightCurrentCell: false });
		  */
		highlightCurrentCell?: boolean;

		/** Determines whether hovered row is highlighted or not.
		  * @example
		  * $("#element").wijgrid({ highlightOnHover: true });
		  */
		highlightOnHover?: boolean;

		/** Determines the text to be displayed when the grid is loading.
		  * @example
		  * $("#element").wijgrid({ loadingText: "Loading..."});
		  */
		loadingText?: string;

		/** Cell values equal to this property value are considered null values. Use this option if you want to change default representation of null values (empty strings) with something else.
		  * @example
		  * $("#element").wijgrid({ nullString: "" });
		  * @remarks
		  * Case-sensitive for built-in parsers.
		  */
		nullString?: string;

		/** Determines the zero-based index of the current page. You can use this to access a specific page, for example, when using the paging feature.
		  * @example
		  * $("#element").wijgrid({ pageIndex: 0 });
		  */
		pageIndex?: number;

		/** Number of rows to place on a single page.
		  * The default value is 10.
		  * @example
		  * // The pageSize here sets 10 rows to a page. The allowPaging option is set to true so paging buttons appear.
		  * $("#element").wijgrid({ pageSize: 10 });
		  */
		pageSize?: number;

		/** Determines the pager settings for the grid including the mode (page buttons or next/previous buttons), number of page buttons, and position where the buttons appear.
		  * @example
		  * // Display the pager at the top of the wijgrid.
		  * $("#element").wijgrid({ pagerSettings: { position: "top" } });
		  * @remarks
		  * See the wijpager documentation for more information on pager settings.
		  */
		pagerSettings?: IPagerSettings;

		/** A value indicating whether DOM cell attributes can be passed within a data value.
		  * @example
		  * // Render the style attribute passed within the data.
		  * $("#element").wijgrid({
		  *		readAttributesFromData: false });
		  *		data: [
		  *			[ [1, { "style": "color: red" } ], a ]
		  *		]
		  * });
		  * @remarks
		  * This option allows binding collection of values to data and automatically converting them as attributes of corresponded DOM table cells during rendering.
		  * Values should be passed as an array of two items, where first item is a value of the data field, the second item is a list of values:
		  * $("#element").wijgrid({
		  *		data: [
		  *			[ [1, { "style": "color: red", "class": "myclass" } ], a ]
		  *		]
		  * });
		  *
		  * or
		  *
		  * $("#element").wijgrid({
		  *		data: [
		  *			{ col0: [1, { "style": "color: red", "class": "myclass" }], col1: "a" }
		  *		]
		  * });
		  *
		  * Note: during conversion wijgrid extracts the first item value and makes it data field value, the second item (list of values) is removed:
		  * [ { col0: 1, col1: "a" } ]
		  *
		  * If DOM table is used as a datasource then attributes belonging to the cells in tBody section of the original table will be read and applied to the new cells.
		  *
		  * rowSpan and colSpan attributes are not allowed.
		  */
		readAttributesFromData?: boolean;

		/** Determines the height of a rows when virtual scrolling is used.
		  * Obsoleted, use the scrollingSettings.virtualization.rowHeight property instead.
		  * @example
		  * $("#element").wijgrid({ rowHeight: 20 });
		  * @remarks
		  * Can be set only during creation
		  */
		rowHeight?: number;

		/** Function used for styling rows in wijgrid.
		  * @example
		  * // Make text of the alternating rows italic.
		  * $("#demo").wijgrid({
		  *		data: [
		  *			[0, "Nancy"], [1, "Susan"], [2, "Alice"], [3, "Kate"]
		  *		],
		  *		rowStyleFormatter: function (args) {
		  *			if ((args.state & wijmo.grid.renderState.rendering) && (args.type & wijmo.grid.rowType.dataAlt)) {
		  *				args.$rows.find("td").css("font-style", "italic");
		  *			}
		  *		}
		  * });
		  * @param {wijmo.grid.IRowInfo} args The data with this function.
		  */
		rowStyleFormatter?: (args: IRowInfo) => void;

		/** Determines which scrollbars are active and if they appear automatically based on content size.
		  * Possbile values are: "none", "auto", "horizontal", "vertical", "both".
		  * Obsoleted, use the scrollingSettings.mode property instead.
		  * @example
		  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
		  * $("#element").wijgrid({ scrollMode: "both" });
		  * @remarks
		  * Possible values are:
		  *	"none": Scrolling is not used; the staticRowIndex and staticColumnIndex values are ignored.
		  *	"auto": Scrollbars appear automatically depending upon content size.
		  *	"horizontal": The horizontal scrollbar is active.
		  *	"vertical": The vertical scrollbar is active.
		  *	"both": Both horizontal and vertical scrollbars are active.
		  */
		scrollMode?: string;

		/** Determines which cells, range of cells, columns, or rows can be selected at one time.
		  * Possible values are: "none", "singleCell", "singleColumn", "singleRow", "singleRange", "multiColumn", "multiRow" and "multiRange".
		  * @example
		  * // Set selectionMode to muliColumn and users can select more than one column using the CTRL or SHIFT keys.
		  * $("#element").wijgrid({ selectionMode: "multiColumn" });
		  * @remarks
		  * Possible values are: 
		  * "none": Selection is turned off.
		  * "singleCell": Only a single cell can be selected at a time.
		  * "singleColumn": Only a single column can be selected at a time.
		  * "singleRow": Only a single row can be selected at a time.
		  * "singleRange": Only a single range of cells can be selected at a time.
		  * "multiColumn": It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
		  * "multiRow": It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
		  * "multiRange": It is possible to select more than one cells range at the same time using the mouse and the CTRL or SHIFT keys.
		  */
		selectionMode?: string;

		/** A value indicating whether the filter row is visible.
		  * Filter row is used to display column filtering interface.
		  * @example
		  * // Set showFilter to true to view the filter row.
		  * $("#element").wijgrid({ showFilter: true });
		  */
		showFilter?: boolean;

		/** A value indicating whether the footer row is visible.
		  * Footer row is used for displaying of tfoot section of original table, and to show totals.
		  * @example
		  * // Set showFooter to true to view the footer row.
		  * $("#element").wijgrid({ showFooter: true });
		  */
		showFooter?: boolean;

		/** A value indicating whether group area is visible.
		  * Group area is used to display headers of groupped columns. User can drag columns from/to group area by dragging column headers with mouse, if allowColMoving option is on.
		  * @example
		  * // Set showGroupArea to true to display the group area.
		  * $("#element").wijgrid({ showGroupArea: true });
		  */
		showGroupArea?: boolean;

		/** A value indicating whether a selection will be automatically displayed at the current cell position when the wijgrid is rendered.
		  * Set this option to false if you want to prevent wijgrid from selecting the currentCell automatically.
		  * @example
		  * $("#element").wijgrid({ showSelectionOnRender: true });
		  */
		showSelectionOnRender?: boolean;

		/** A value indicating whether the row header is visible.
		  * @example
		  * $("#element").wijgrid({ showRowHeader: true });
		  */
		showRowHeader?: boolean;

		/** Indicates the index of columns that will always be shown on the left when the grid view is scrolled horizontally.
		  * Obsoleted, use the scrollingSettings.staticColumnIndex property instead.
		  * @example
		  * $("#element").wijgrid({ staticColumnIndex: -1 });
		  * @remarks
		  * Note that all columns before the static column will be automatically marked as static, too.
		  * This can only take effect when the scrollMode option is not set to "none".
		  * It will be considered "-1" when grouping or row merging is enabled. A "-1" means there is no data column but the row header is static. A zero (0) means one data column and row header are static.
		  */
		staticColumnIndex?: number;

		/** Gets or sets the alignment of the static columns area. Possible values are "left", "right".
		  * Obsoleted, use the scrollingSettings.staticColumnsAlignment property instead.
		  * @example
		  * $("#element").wijgrid({ staticColumnsAlignment: "left" });
		  * @remarks
		  * The "right" mode has limited functionality:
		  *  - The showRowHeader value is ignored.
		  *  - Changing staticColumnIndex at run-time by dragging the vertical bar is disabled.
		  */
		staticColumnsAlignment?: string;

		/** Indicates the index of data rows that will always be shown on the top when the wijgrid is scrolled vertically.
		  * Obsoleted, use the scrollingSettings.staticRowIndext property instead.
		  * @example
		  * $("#element").wijgrid({ staticRowIndex: -1 });
		  * @remarks
		  * Note, that all rows before the static row will be automatically marked as static, too.
		  * This can only take effect when the scrollMode option is not set to "none". This will be considered "-1" when grouping or row merging is enabled.
		  * A "-1" means there is no data row but the header row is static.A zero (0) means one data row and the row header are static.
		  */
		staticRowIndex?: number;

		/** Gets or sets the virtual number of items in the wijgrid and enables custom paging.
		  * Setting option to a positive value activates custom paging, the number of displayed rows and the total number of pages will be determined by the totalRows and pageSize values.
		  * @example
		  * $("#element").wijgrid({ totalRows: -1 });
		  * @remarks
		  * In custom paging mode sorting, paging and filtering are not performed automatically.
		  * This must be handled manually using the sorted, pageIndexChanged, and filtered events. Load the new portion of data there followed by the ensureControl(true) method call.
		  */
		totalRows?: number;

		/** Determines the scrolling settings.
		  * @example
		  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
		  * $("#element").wijgrid({ scrollingSettings: { mode: "both" } });
		  */
		scrollingSettings?: IScrollingSettings;

		/* --- events */

		/** The afterCellEdit event handler is a function called after cell editing is completed.
		  * This function can assist you in completing many tasks, such as in making changes once editing is completed; in tracking changes in cells, columns, or rows; or in integrating custom editing functions on the front end.
		  * @event
		  * @example
		  * // Once cell editing is complete, the function calls the destroy method to destroy the wijcombobox widget and the wijinputnumber widget which are used as the custom editors.
		  * $("#element").wijgrid({
		  *		afterCellEdit: function(e, args) {
		  *			switch (args.cell.column().dataKey) {
		  *				case "Position":
		  *					args.cell.container().find("input").wijcombobox("destroy");
		  *					break;
		  *				case "Acquired":
		  *					args.cell.container().find("input").wijinputnumber("destroy");
		  *					break;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ afterCellEdit: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridaftercelledit", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IAfterCellEditEventArgs} args The data with this event.
		  */
		afterCellEdit?: (e: JQueryEventObject, args: IAfterCellEditEventArgs) => void;

		/** The afterCellUpdate event handler is a function that is called after a cell has been updated. Among other functions, this event allows you to track and store the indices of changed rows or columns.
		  * @event
		  * @example
		  * // Once the cell has been updated, the information from the underlying data is dumped into the "#log" element.
		  * $("#element").wijgrid({
		  *		afterCellUpdate: function(e, args) {
		  *			$("#log").html(dump($("#demo").wijgrid("data")));
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ afterCellUpdate: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridaftercellupdate", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IAfterCellUpdateEventArgs} args The data with this event.
		  */
		afterCellUpdate?: (e: JQueryEventObject, args: IAfterCellUpdateEventArgs) => void;

		/** The beforeCellEdit event handler is a function that is called before a cell enters edit mode.
		  * The beforeCellEdit event handler assists you in appending a widget, data, or other item to a wijgrid's cells before the cells enter edit mode. This event is cancellable if the editigMode options is set to "cell".
		  * @event
		  * @example
		  * // Allow the user to change the price only if the product hasn't been discontinued:
		  * $("#element").wijgrid({
		  *		beforeCellEdit: function(e, args) {
		  *			return !((args.cell.column().dataKey === "Price") && args.cell.row().data.Discontinued);
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ beforeCellEdit: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridbeforecelledit", function (e, args) {
		  *		// some code here
		  * });
		  * 
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IBeforeCellEditEventArgs} args The data with this event.
		  */
		beforeCellEdit?: (e: JQueryEventObject, args: IBeforeCellEditEventArgs) => boolean;

		/** The beforeCellUpdate event handler is a function that is called before the cell is updated with new or user-entered data. This event is cancellable if the editingMode options is set to "cell".
		  * There are many instances where this event is helpful, such as when you need to check a cell's value before the update occurs or when you need to apply an alert message based on the cell's value.
		  * @event
		  * @example
		  * // In this sample, you use args.value to check the year that the user enters in the "Acquired" column.
		  * // If it's less than 1990 or greater than the current year, then the event handler will return false to cancel updating and show the user an alert message.
		  * $("#element").wijgrid({
		  *		beforeCellUpdate: function(e, args) {
		  *			switch (args.cell.column().dataKey) { 
		  *				case "Acquired":
		  *					var $editor = args.cell.container().find("input"),
		  *						value = $editor.wijinputnumber("getValue"),
		  *						curYear = new Date().getFullYear();
		  *
		  *					if (value < 1990 || value > curYear) {
		  *						$editor.addClass("ui-state-error");
		  *						alert("value must be between 1990 and " + curYear);
		  *						$editor.focus();
		  *						return false; 
		  *					}
		  *
		  *					args.value = value; 
		  *					break;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ beforeCellUpdate: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridbeforecellupdate", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IBeforeCellUpdateEventArgs} args The data with this event.
		  */
		beforeCellUpdate?: (e: JQueryEventObject, args: IBeforeCellUpdateEventArgs) => boolean;

		/** The cellClicked event handler is a function that is called when a cell is clicked. You can use this event to get the information of a clicked cell using the args parameter.
		  * @event
		  * @example
		  * // The sample uses the cellClicked event to trigger an alert when the cell is clicked.
		  * $("#element").wijgrid({
		  *		cellClicked: function (e, args) {
		  *			alert(args.cell.value());
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ cellClicked: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcellclicked", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ICellClickedEventArgs} args The data with this event.
		  */
		cellClicked?: (e: JQueryEventObject, args: ICellClickedEventArgs) => void;

		/** The columnDragging event handler is a function that is called when column dragging has been started, but before the wijgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Preventing a user from dragging a specific column
		  * $("#element").wijgrid({
		  *		columnDragging: function (e, args) {
		  *			return !(args.drag.dataKey == "ID");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnDragging: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumndragging", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnDraggingEventArgs} args The data with this event.
		  */
		columnDragging?: (e: JQueryEventObject, args: IColumnDraggingEventArgs) => boolean;

		/** The columnDragged event handler is a function that is called when column dragging has been started. You can use this event to find the column being dragged or the dragged column's location.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnDragged event:
		  * $("#element").wijgrid({
		  *		columnDragged: function (e, args) {
		  *			alert("The '" + args.drag.headerText + "' column is being dragged from the '" + args.dragSource + "' location");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnDragged: function (e, args) {
		  *		// some code here
		  * }});
		  * 
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumndragged", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnDraggedEventArgs} args The data with this event.
		  */
		columnDragged?: (e: JQueryEventObject, args: IColumnDraggedEventArgs) => void;

		/** The columnDropping event handler is a function that is called when a column is dropped into the columns area, but before wijgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Preventing user from dropping any column before the "ID" column.
		  * $("#element").wijgrid({
		  *		columnDropping: function (e, args) {
		  *			return !(args.drop.dataKey == "ID" && args.at == "left");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnDropping: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumndropping", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnDroppingEventArgs} args The data with this event.
		  */
		columnDropping?: (e: JQueryEventObject, args: IColumnDroppingEventArgs) => boolean;

		/** The columnDropped event handler is a function that is called when a column has been dropped into the columns area.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnDropped event:
		  * $("#element").wijgrid({
		  *		columnDropped: function (e, args) {
		  *			"The '" + args.drag.headerText + "' column has been dropped onto the '" + args.drop.headerText + "' column at the '" + args.at + "' position"
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnDropped: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumndropped", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnDroppedEventArgs} args The data with this event.
		  */
		columnDropped?: (e: JQueryEventObject, args: IColumnDroppedEventArgs) => void;

		/** The columnGrouping event handler is a function that is called when a column is dropped into the group area, but before the wijgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Preventing user from grouping the "UnitPrice" column.
		  * $("#element").wijgrid({
		  *		columnGrouping: function (e, args) {
		  *			return !(args.drag.headerText == "UnitPrice");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnGrouping: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumngrouping", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnGroupingEventArgs} args The data with this event.
		  */
		columnGrouping?: (e: JQueryEventObject, args: IColumnGroupingEventArgs) => boolean;

		/** The columnGrouped event handler is a function that is called when a column has been dropped into the group area.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnGrouped event:
		  * $("#element").wijgrid({
		  *		columnGrouped: function (e, args) {
		  *			alert("The '" + args.drag.headerText "' column has been grouped");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnGrouped: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumngrouped", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnGroupedEventArgs} args The data with this event.
		  */
		columnGrouped?: (e: JQueryEventObject, args: IColumnGroupedEventArgs) => void;

		/** The columnResizing event handler is called when a user resizes the column but before the wijgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Prevent setting the width of "ID" column less than 100 pixels
		  * $("#element").wijgrid({
		  *		columnResizing: function (e, args) {
		  *			if (args.column.dataKey == "ID" && args.newWidth < 100) {
		  *				args.newWidth = 100;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnResizing: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumnresizing", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnResizingEventArgs} args The data with this event.
		  */
		columnResizing?: (e: JQueryEventObject, args: IColumnResizingEventArgs) => boolean;

		/** The columnResized event handler is called when a user has changed a column's size.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnGrouped event:
		  * $("#element").wijgrid({
		  *		columnResized: function (e, args) {
		  *			alert("The '" + args.column.headerText + "' has been resized");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnResized: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumnresized", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnResizedEventArgs} args The data with this event.
		  */
		columnResized?: (e: JQueryEventObject, args: IColumnResizedEventArgs) => void;

		/** The columnUngrouping event handler is called when a column has been removed from the group area but before the wjgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Preventing user from ungrouping the "UnitPrice" column.
		  * $("#element").wijgrid({
		  *		columnUngrouping: function (e, args) {
		  *			return !(args.column.headerText == "UnitPrice");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnUngrouping: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumnungrouping", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnUngroupingEventArgs} args The data with this event.
		  */
		columnUngrouping?: (e: JQueryEventObject, args: IColumnUngroupingEventArgs) => boolean;

		/** The columnUngrouped event handler is called when a column has been removed from the group area.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnGrouped event:
		  * $("#element").wijgrid({
		  *		columnUngrouped: function (e, args) {
		  *			alert("The '" + args.column.headerText + "' has been ungrouped");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnUngrouped: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumnungrouped", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnUngroupedEventArgs} args The data with this event.
		  */
		columnUngrouped?: (e: JQueryEventObject, args: IColumnUngroupedEventArgs) => void;

		/** The currentCellChanging event handler is called before the cell is changed. You can use this event to get a selected row or column or to get a data row bound to the current cell. This event is cancellable.
		  * @event
		  * @example
		  * // Gets the data row bound to the current cell.
		  * $("#element").wijgrid({
		  *		currentCellChanging: function (e, args) {
		  *			var rowObj = $(e.target).wijgrid("currentCell").row();
		  *			if (rowObj) {		
		  *				var dataItem = rowObj.data; // current data item (before the cell is changed).
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ currentCellChanging: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcurrentcellchanging", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ICurrentCellChangingEventArgs} args The data with this event.
		  */
		currentCellChanging?: (e: JQueryEventObject, args: ICurrentCellChangingEventArgs) => boolean;

		/** The currentCellChanged event handler is called after the current cell is changed.
		  * @event
		  * @example
		  * // Gets the data row bound to the current cell.
		  * $("#element").wijgrid({
		  *		currentCellChanged: function (e, args) {
		  *			var rowObj = $(e.target).wijgrid("currentCell").row();
		  *			if (rowObj) {		
		  *				var dataItem = rowObj.data; // current data item (after the cell is changed).
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ currentCellChanged: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcurrentcellchanged", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
		currentCellChanged?: (e: JQueryEventObject) => void;

		/** The detailCreating event handler is called when wijgrid requires to create a new detail wijgrid.
		* @event
		* @remarks
		* Event receives options of a detail grid to create, which were obtained by cloning the detail option of the master grid.
		* User can alter the detail grid options here and provide a specific datasource within the args.options.data option to implement run-time hierarachy.
		* @remarks
		* You can bind to the event either by type or by name.
		* Bind to the event by name:
		* $("#element").wijgrid({ detailCreating: function (e, args) {
		*		// some code here
		* }});
		*
		* Bind to the event by type:
		* $("#element").bind("wijgriddetailcreating", function (e, args) {
		*		// some code here
		* });
		* @param {Object} e The jQuery.Event object.
		* @param {wijmo.grid.IDetailCreatingEventArgs} args The data with this event.
		*/
		detailCreating?: (e: JQueryEventObject, args: IDetailCreatingEventArgs) => void;
		//detailsLoaded?: (e: JQueryEventObject) => void;

		/** The filterOperatorsListShowing event handler is a function that is called before the filter drop-down list is shown. You can use this event to customize the list of filter operators for your users.
		  * @event
		  * @example
		  * // Limit the filters that will be shown to the "Equals" filter operator
		  * $("#element").wijgrid({
		  *		filterOperatorsListShowing: function (e, args) {
		  *			args.operators = $.grep(args.operators, function(op) {
		  *				return op.name === "Equals" || op.name === "NoFilter";
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ filterOperatorsListShowing: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridfilteroperatorslistshowing", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IFilterOperatorsListShowingEventArgs} args The data with this event.
		  */
		filterOperatorsListShowing?: (e: JQueryEventObject, args: IFilterOperatorsListShowingEventArgs) => void;

		/** The filtering event handler is a function that is called before the filtering operation is started. For example, you can use this event to change a filtering condition before a filter will be applied to the data. This event is cancellable.
		  * @event
		  * @example
		  * // Prevents filtering by negative values
		  * $("#element").wijgrid({
		  *		filtering: function (e, args) {
		  *			if (args.column.dataKey == "Price" && args.value < 0) {
		  *				args.value = 0;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ filtering: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridfiltering", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IFilteringEventArgs} args The data with this event.
		  */
		filtering?: (e: JQueryEventObject, args: IFilteringEventArgs) => boolean;

		/** The filtered event handler is a function that is called after the wijgrid is filtered.
		  * @event
		  * @example
		  * // 
		  * $("#element").wijgrid({
		  *		filtered: function (e, args) {
		  *			alert("The filtered data contains: " + $(this).wijgrid("dataView").count() + " rows");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ filtered: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridfiltered", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IFilteredEventArgs} args The data with this event.
		  */
		filtered?: (e: JQueryEventObject, args: IFilteredEventArgs) => void;

		/** The groupAggregate event handler is a function that is called when groups are being created and the column object's aggregate option has been set to "custom". This event is useful when you want to calculate custom aggregate values.
		  * @event
		  * @example
		  * // This sample demonstrates using the groupAggregate event handler to calculate an average in a custom aggregate:
		  * $("#element").wijgrid({
		  *		groupAggregate: function (e, args) {
		  *			if (args.column.dataKey == "Price") {
		  *				var aggregate = 0;
		  *
		  *				for (var i = args.groupingStart; i <= args.groupingEnd; i++) {
		  *					aggregate += args.data[i].valueCell(args.column.dataIndex).value;
		  *				}
		  *
		  *				aggregate = aggregate/ (args.groupingEnd - args.groupingStart + 1);
		  *				args.text = aggregate;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ groupAggregate: function (e, args) {
		  *		// some code here
		  * }});
		  * Bind to the event by type:
		  *
		  * $("#element").bind("wijgridgroupaggregate", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IGroupAggregateEventArgs} args The data with this event.
		  */
		groupAggregate?: (e: JQueryEventObject, args: IGroupAggregateEventArgs) => void;

		/** The groupText event handler is a function that is called when groups are being created and the groupInfo option has the groupInfo.headerText or the groupInfo.footerText options set to "custom". This event can be used to customize group headers and group footers.
		  * @event
		  * @example
		  * // The following sample sets the groupText event handler to avoid empty cells. The custom formatting applied to group headers left certain cells appearing as if they were empty. This code avoids that:
		  * $("#element").wijgrid({
		  *		groupText: function (e, args) {
		  *			if (!args.groupText) {
		  *				args.text = "null";
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ groupText: function (e, args) {
		  *		// some code here
		  * }});
		  * 
		  * Bind to the event by type:
		  * $("#element").bind("wijgridgrouptext", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IGroupTextEventArgs} args The data with this event.
		  */
		groupText?: (e: JQueryEventObject, args: IGroupTextEventArgs) => void;

		/** The invalidCellValue event handler is a function called when a cell needs to start updating but the cell value is invalid. So if the value in a wijgrid cell can't be converted to the column target type, the invalidCellValue event will fire.
		  * @event
		  * @example
		  * // Adds a style to the cell if the value entered is invalid
		  * $("#element").wijgrid({
		  *		invalidCellValue: function (e, args) {
		  *			$(args.cell.container()).addClass("ui-state-error");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ invalidCellValue: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridinvalidcellvalue", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IInvalidCellValueEventArgs} args The data with this event.
		  */
		invalidCellValue?: (e: JQueryEventObject, args: IInvalidCellValueEventArgs) => void;

		/** The pageIndexChanging event handler is a function that is called before the page index is changed. This event is cancellable.
		  * @event
		  * @example
		  * // Cancel the event by returning false
		  * $("#element").wijgrid({
		  *		pageIndexChanging: function (e, args) {
		  *			return false;
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ pageIndexChanging: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridpageindexchanging", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IPageIndexChangingEventArgs} args The data with this event.
		  */
		pageIndexChanging?: (e: JQueryEventObject, args: IPageIndexChangingEventArgs) => boolean;

		/** The pageIndexChanged event handler is a function that is called after the page index is changed, such as when you use the numeric buttons to swtich between pages or assign a new value to the pageIndex option.
		  * @event
		  * @example
		  * // Supply a callback function to handle the pageIndexChanged event:
		  * $("#element").wijgrid({
		  *		pageIndexChanged: function (e, args) {
		  *			alert("The new pageIndex is: " + args.newPageIndex);
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ pageIndexChanged: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridpageindexchanged", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IPageIndexChangedEventArgs} args The data with this event.
		  */
		pageIndexChanged?: (e: JQueryEventObject, args: IPageIndexChangedEventArgs) => void;

		/** The selectionChanged event handler is a function that is called after the selection is changed.
		  * @event
		  * @example
		  * // Get the value of the first cell of the selected row.
		  * $("#element").wijgrid({
		  *		selectionMode: "singleRow",
		  *		selectionChanged: function (e, args) {
		  *			alert(args.addedCells.item(0).value());
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ selectionChanged: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridselectionchanged", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ISelectionChangedEventArgs} args The data with this event.
		  */
		selectionChanged?: (e: JQueryEventObject, args: ISelectionChangedEventArgs) => void;

		/** The sorting event handler is a function that is called before the sorting operation is started. This event is cancellable.
		  * The allowSorting option must be set to "true" for this event to fire.
		  * @event
		  * @example
		  * // Preventing user from sorting the "ID" column.
		  * $("#element").wijgrid({
		  *		sorting: function (e, args) {
		  *			return !(args.column.headerText === "ID");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ sorting: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridsorting", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ISortingEventArgs} args The data with this event.
		  */
		sorting?: (e: JQueryEventObject, args: ISortingEventArgs) => boolean;

		/** The sorted event handler is a function that is called after the widget is sorted. The allowSorting option must be set to "true" to allow this event to fire.
		  * @event
		  * @example
		  * // The following code handles the sorted event and will give you access to the column and the sort direction
		  * $("#element").wijgrid({
		  *		sorted: function (e, args) {
		  *			alert("Column " + args.column.headerText + " sorted in " + args.sortDirection + " order");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ sorted: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridsorted", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ISortedEventArgs} args The data with this event.
		  */
		sorted?: (e: JQueryEventObject, args: ISortedEventArgs) => void;

		/* --- life-cycle events */

		/** The dataLoading event handler is a function that is called when the wijgrid loads a portion of data from the underlying datasource. This can be used for modification of data sent to server if using dynamic remote wijdatasource.
		  * @event
		  * @example
		  * // This sample allows you to set the session ID when loading a portion of data from the remote wijdatasource:
		  * $("#element").wijgrid({
		  *		data: new wijdatasource({
		  *			proxy: new wijhttpproxy({
		  *				// some code here
		  *			})
		  *		}),
		  *		dataLoading: function (e) {
		  *			var dataSource = $(this).wijgrid("option", "data");
		  *			dataSource.proxy.options.data.sessionID = getSessionID();
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ dataLoading: function (e) {
		  * // some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgriddataloading", function (e) {
		  * // some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
		dataLoading?: (e: JQueryEventObject) => void;

		/** The dataLoaded event handler is a function that is called when data is loaded.
		  * @event
		  * @example
		  * // Display the number of entries found
		  * $("#element").wijgrid({
		  *		dataLoaded: function (e) {
		  *			alert($(this).wijgrid("dataView").count());
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ dataLoaded: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgriddataloaded", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
		dataLoaded?: (e: JQueryEventObject) => void;

		/** The loading event handler is a function that is called at the beginning of the wijgrid's lifecycle. You can use this event to activate a custom load progress indicator.
		  * @event
		  * @example
		  * // Creating an indeterminate progressbar during loading
		  * $("#element").wijgrid({
		  *		loading: function (e) {
		  *			$("#progressBar").show().progressbar({ value: false });
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ loading: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridloading", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
		loading?: (e: JQueryEventObject) => void;

		/** The loaded event handler is a function that is called at the end the wijgrid's lifecycle when wijgrid is filled with data and rendered. You can use this event to manipulate the grid html content or to finish a custom load indication.
		  * @event
		  * @example
		  * // The loaded event in the sample below ensures that whatever is selected on load is cleared
		  * $("#element").wijgrid({
		  *		loaded: function (e) {
		  *			$(e.target).wijgrid("selection").clear(); // clear selection                    
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ loaded: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridloaded", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
		loaded?: (e: JQueryEventObject) => void;

		/** The rendering event handler is a function that is called when the wijgrid is about to render. Normally you do not need to use this event.
		  * @event
		  * @example
		  * $("#element").wijgrid({
		  *		rendering: function (e) {
		  *			alert("rendering");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ rendering: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridrendering", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
		rendering?: (e: JQueryEventObject) => void;

		/** The rendered event handler is a function that is called when the wijgrid is rendered. Normally you do not need to use this event.
		  * @event
		  * @example
		  * $("#element").wijgrid({
		  *		rendered: function (e) {
		  *			alert("rendered");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ rendered: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridrendered", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
		rendered?: (e: JQueryEventObject) => void;
	}

	/** Determines a master-detail relation in a hierarchy grid. */
	export interface IMasterDetailRelation {
		/** Determines the name of the master key field.*/
		detailDataKey: string;
		/** Determines the name of the detail key field.*/
		masterDataKey: string;
	}

	/** Represents options of the detail grid.*/
	export interface IDetailSettings extends IWijgridOptions {
		/** Determines an array of IMasterDetailRelation objects that represent master-detail relations in a hierarchical grid.*/
		relation?: IMasterDetailRelation[];
		/** Determines whether the hierarchy will be expanded by default or not.*/
		startExpanded?: boolean;
		/** Determines the height of the detail grid.*/
		height?: any;
		/** Determines the width of the detail grid.*/
		width?: any;
	}

	/** Represents a collection of field values of a data item in a datasource. */
	export interface IDataKeyArray {
		/** Gets or sets the value associated with the specified key. */
		[index: string]: any;
	}

	/** @ignore */
	export interface ITempDataStorage {
		data?: any;
		detail?: ITempDataStorage;
	};

	/** @ignore */
	export interface IMasterInfo {
		master: wijgrid;
		dataRowIndex: number;
	}
}
