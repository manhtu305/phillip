/// <reference path="interfaces.ts"/>
/// <reference path="wijgrid.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class uiFrozener {
		private _wijgrid: wijgrid;
		private _$hBar: JQuery;
		private _$vBar: JQuery;
		private _$outerDiv: JQuery;
		private _$proxy: JQuery;
		private _superPanel: JQueryUIWidget;
		private _docEventsUID = "wijgridfrozener" + wijmo.grid.getUID();
		private _docEventsAttached = false;
		private _newStaticIndex = -1; // depends on e.data value (staticRowIndex or staticColumnIndex)
		private _staticColumnIndex = -1;
		private _staticRowIndex = -1;
		private _staticOffsetH = 0;
		private _staticOffsetV = 0;
		private _visibleBounds: IElementBounds;
		private _inProgress = false;

		constructor(wijgrid: wijgrid) {
			this._wijgrid = wijgrid;

			this.refresh();
		}

		inProgress() {
			return this._inProgress;
		}

		refresh() {
			this.dispose();

			var freezingMode = this._wijgrid._lgGetFreezingMode();

			if (freezingMode !== "none" && (this._wijgrid._lgGetScrollMode() !== "none")) {
				this._$outerDiv = this._wijgrid.mOuterDiv.find("." + wijmo.grid.wijgrid.CSS.fixedView);
				this._superPanel = (<wijmo.grid.fixedView>this._wijgrid._view())._getSuperPanel();
				this._staticOffsetH = this._wijgrid._getStaticOffsetIndex(false);
				this._staticOffsetV = this._wijgrid._getStaticOffsetIndex(true);
				this._staticColumnIndex = this._wijgrid._getStaticIndex(false);
				this._staticRowIndex = this._wijgrid._getStaticIndex(true);
				this._visibleBounds = this._wijgrid._view().getVisibleContentAreaBounds(); //.getVisibleAreaBounds();

				var allFixedAreaBounds = wijmo.grid.bounds(this._$outerDiv.find(".wijmo-wijgrid-split-area-nw")),
					containerBounds = wijmo.grid.bounds(this._$outerDiv);

				// if staticColumnsAlignment is "right" then create vbar only when staticColumnIndex is set (vbar dragging ability is disabled in this case)
				if ((freezingMode === "both" || freezingMode === "columns") && (this._wijgrid._lgGetStaticColumnsAlignment() !== "right" || this._staticColumnIndex >= 0)) {
					this._createVBar(this._visibleBounds, allFixedAreaBounds, containerBounds);
				}

				if ((freezingMode === "both" || freezingMode === "rows") && !this._wijgrid._serverSideVirtualScrolling()) {
					this._createHBar(this._visibleBounds, allFixedAreaBounds, containerBounds);
				}
			}
		}

		ensureVBarHeight() {
			if (this._$vBar) {
				var allFixedAreaBounds = wijmo.grid.bounds(this._$outerDiv.find(".wijmo-wijgrid-split-area-nw"));

				this._visibleBounds = this._wijgrid._view().getVisibleContentAreaBounds(); //.getVisibleAreaBounds();

				this._$vBar.height(this._visibleBounds.height + this._visibleBounds.top - allFixedAreaBounds.top);
			}
		}

		dispose() {
			if (this._$hBar) {
				this._$hBar.remove();
				this._$hBar = null;
			}

			if (this._$vBar) {
				this._$vBar.remove();
				this._$vBar = null;
			}

			if (this._$proxy) {
				this._$proxy.remove();
				this._$proxy = null;
			}

			this._$outerDiv = null;
			this._superPanel = null;

			this._detachDocEvents();
		}

		private _createVBar(visibleBounds: IElementBounds, allFixedAreaBounds: IElementBounds, containerBounds: IElementBounds): void {
			var lAlign = (this._wijgrid._lgGetStaticColumnsAlignment() !== "right"),
				leftPos = lAlign
				? allFixedAreaBounds.width + allFixedAreaBounds.left
				: allFixedAreaBounds.left - 2,

				self = this,
				defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._wijgrid.options.wijCSS;

			if (leftPos <= visibleBounds.left + visibleBounds.width) {
				this._$vBar = $("<div><div></div></div>")
					.addClass(defCSS.freezingHandleV + " " + wijCSS.wijgridFreezingHandleV)
					.addClass(lAlign ? "" : "not-allowed") // remove "pointer" cursor (vbar dragging ability is disabled when staticColumnsAlignment == "right" is used)
					.css({
						left: leftPos - containerBounds.left,
						top: allFixedAreaBounds.top - containerBounds.top,
						height: visibleBounds.height + visibleBounds.top - allFixedAreaBounds.top
					})
					.bind("mousedown", function (e) {
						e.data = true; // vertical bar
						self._onBarMouseDown.apply(self, arguments);
					})
					.appendTo(this._$outerDiv);

				// content
				this._$vBar.find("div")
					.addClass(defCSS.freezingHandleContent + " " + wijCSS.header);
			}
		}

		private _createHBar(visibleBounds: IElementBounds, allFixedAreaBounds: IElementBounds, containerBounds: IElementBounds): void {
			var topPos = allFixedAreaBounds.top + allFixedAreaBounds.height,
				lAlign = (this._wijgrid._lgGetStaticColumnsAlignment() !== "right"),
				self = this,
				defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._wijgrid.options.wijCSS;

			if (topPos <= visibleBounds.top + visibleBounds.height) {
				this._$hBar = $("<div><div></div></div>")
					.addClass(defCSS.freezingHandleH + " " + wijCSS.wijgridFreezingHandleH)
					.css({
						left: lAlign
						? allFixedAreaBounds.left - containerBounds.left // 0?
						: 0,
						top: topPos - containerBounds.top,
						width: lAlign
						? visibleBounds.width + visibleBounds.left - allFixedAreaBounds.left // visibleBounds.width?
						: visibleBounds.width
					})
					.bind("mousedown", function (e) {
						e.data = false; // horizontal bar
						self._onBarMouseDown.apply(self, arguments);
					})
					.appendTo(this._$outerDiv);

				// content
				this._$hBar.find("div")
					.addClass(defCSS.freezingHandleContent + " " + wijCSS.header);
			}
		}

		// e.data: true = vertical, false = horizontal
		private _onBarMouseDown(e) {
			if (this._wijgrid.options.disabled || (this._wijgrid._lgGetStaticColumnsAlignment() === "right" && e.data)) {
				return false;
			}

			var defCSS = wijmo.grid.wijgrid.CSS,
				wijCSS = this._wijgrid.options.wijCSS;

			this._visibleBounds = this._wijgrid._view().getVisibleContentAreaBounds(); //.getVisibleAreaBounds();

			this._newStaticIndex = e.data
			? this._staticColumnIndex
			: this._staticRowIndex;

			this._$proxy = $("<div class=\"" + defCSS.resizingHandle + " " + wijCSS.wijgridResizingHandle + " " + wijCSS.header + "\"></div>")
				.appendTo(document.body);

			this._attachDocEvents(e.data);

			this._inProgress = true;

			// prevent selectionUI from taking effect
			e.stopPropagation();
		}

		private _onDocumentMouseMove(e) {
			if (e.data && this._superPanel.options.hScroller.scrollValue) {
				(<any>this._superPanel).hScrollTo(0);
			} else if (!e.data && this._superPanel.options.vScroller.scrollValue) {
				(<any>this._superPanel).vScrollTo(0);
			}

			this._showPosition(e);
		}

		private _onDocumentMouseUp(e) {
			try {
				if (this._$proxy) {
					this._$proxy.remove();
				}

				this._detachDocEvents();

				if (e.data) { // vertical bar
					if (this._newStaticIndex !== this._staticColumnIndex) {
						this._wijgrid.option("staticColumnIndex", this._newStaticIndex);
					}
				} else {  // horizontal bar
					if (this._newStaticIndex !== this._staticRowIndex) {
						this._wijgrid.option("staticRowIndex", this._newStaticIndex);
					}
				}
			}
			finally {
				this._$proxy = null;
				this._inProgress = false;
			}
		}

		private _attachDocEvents(verticalBarTouched: boolean): void {
			if (!this._docEventsAttached) {
				try {
					if ($.fn.disableSelection) {
						$(document.body).disableSelection();
					}

					this._wijgrid._view().toggleDOMSelection(false);

					$(document)
						.bind(this._docEventKey("mousemove"), verticalBarTouched, $.proxy(this._onDocumentMouseMove, this))
						.bind(this._docEventKey("mouseup"), verticalBarTouched, $.proxy(this._onDocumentMouseUp, this));
				} finally {
					this._docEventsAttached = true;
				}
			}
		}

		private _detachDocEvents() {
			if (this._docEventsAttached) {
				try {
					if ($.fn.enableSelection) {
						$(document.body).enableSelection();
					}

					this._wijgrid._view().toggleDOMSelection(true);

					$(document).unbind("." + this._docEventsUID);
				} finally {
					this._docEventsAttached = false;
				}
			}
		}

		private _docEventKey(eventName: string): string {
			return wijmo.grid.stringFormat("{0}.{1}", eventName, this._docEventsUID);
		}

		private _showPosition(e) {
			var colPosInfo: { column: c1basefield; element: JQuery; },
				elementBounds: IElementBounds,
				centerXOrY, currentIdx, prevIdx, leftOrTop,
				position: number,
				barBounds: IElementBounds,
				lAlign = (this._wijgrid._lgGetStaticColumnsAlignment() !== "right");

			if (e.data) { // vertical
				barBounds = wijmo.grid.bounds(this._$vBar);

				if (Math.abs(e.pageX - (barBounds.left + barBounds.width / 2)) < barBounds.width) {
					this._$proxy.hide();
					return;
				}


				if ((colPosInfo = this._getFieldByPos({ x: e.pageX, y: e.pageY }))) { // get column widget
					elementBounds = wijmo.grid.bounds(colPosInfo.element);
					centerXOrY = elementBounds.left + elementBounds.width / 2;
					currentIdx = colPosInfo.column.options._visLeavesIdx - this._staticOffsetV;
					prevIdx = Math.max(currentIdx - 1, -1);
					leftOrTop = e.pageX < centerXOrY ? (prevIdx !== this._staticColumnIndex) : (currentIdx === this._staticColumnIndex);
					position = leftOrTop ? elementBounds.left : elementBounds.left + elementBounds.width;

					if (!wijmo.grid.isOverAxis(position, this._visibleBounds.left - 1, this._visibleBounds.width + 3)) {
						return;
					}

					this._newStaticIndex = leftOrTop ? prevIdx : currentIdx;

					this._$proxy.show().css({
						left: position,
						top: elementBounds.top,
						width: 3,
						height: this._visibleBounds.height + this._visibleBounds.top - elementBounds.top
					});
				}

			} else { // horizontal
				barBounds = wijmo.grid.bounds(this._$hBar);

				if (Math.abs(e.pageY - (barBounds.top + barBounds.height / 2)) < barBounds.height) {
					this._$proxy.hide();
					return;
				}

				var row: HTMLTableRowElement;

				if ((row = this._getRowByPos({ x: e.pageX, y: e.pageY }))) {
					elementBounds = wijmo.grid.bounds(row);
					centerXOrY = elementBounds.top + elementBounds.height / 2;
					currentIdx = this._wijgrid._view().getAbsoluteRowIndex(row) - this._staticOffsetH;

					prevIdx = Math.max(currentIdx - 1, -1);
					leftOrTop = e.pageY < centerXOrY ? (prevIdx !== this._staticRowIndex) : (currentIdx === this._staticRowIndex);
					position = leftOrTop ? elementBounds.top : elementBounds.top + elementBounds.height;

					if (!wijmo.grid.isOverAxis(position, this._visibleBounds.top - 1, this._visibleBounds.height + 3)) {
						return;
					}

					this._newStaticIndex = leftOrTop ? prevIdx : currentIdx;

					this._$proxy.show().css({
						left: lAlign
						? elementBounds.left // this._visibleBounds.left?
						: this._visibleBounds.left,
						top: position,
						width: lAlign
						? this._visibleBounds.width + this._visibleBounds.left - elementBounds.left // this._visibleBounds.width?
						: this._visibleBounds.width,
						height: 3
					});
				}
			}
		}

		_getFieldByPos(pos: { x: number; y: number; }): { column: c1basefield; element: JQuery; } {
			var columns = this._wijgrid.columns(),
				dataRow: IRowInfo = undefined;

			for (var i = 0, len = columns.length; i < len; i++) {
				var instance = columns[i],
					opt = instance.options;

				if (opt._isLeaf) {
					var bounds: IElementBounds = null,
						element: JQuery;

					if (instance.element) {
						bounds = wijmo.grid.bounds(element = instance.element);
					} else { // no header? use first data row.
						if (dataRow == undefined) { // first time only
							dataRow = this._wijgrid._rows().find(0, (row: IRowInfo) => {
								return (row.type & rowType.data) !== 0;
							});
						}

						if (dataRow) {
							var cell = rowAccessor.getCell(<IRowObj><any>dataRow.$rows, opt._visLeavesIdx);
							if (cell) {
								bounds = wijmo.grid.bounds(element = $(cell));
							}
						}
					}

					if (bounds && wijmo.grid.isOverAxis(pos.x, bounds.left, bounds.width)) {
						return {
							column: instance,
							element: element
						};
					}
				}
			}

			return null;
		}

		_getRowByPos(pos: { x: number; y: number; }): HTMLTableRowElement {
			var rows = this._wijgrid._rows();

			for (var i = 0, len = rows.length(); i < len; i++) {
				var row = rows.item(i)[0],
					bounds = wijmo.grid.bounds($(row));

				if (wijmo.grid.isOverAxis(pos.y, bounds.top, bounds.height)) {
					return row;
				}
			}

			return null;
		}
	}
}