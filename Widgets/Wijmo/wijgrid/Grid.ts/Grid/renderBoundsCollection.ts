/// <reference path="interfaces.ts" />

module wijmo.grid {
	/** @ignore */
	export class renderableColumnsCollection {
		private mItems: IRenderBounds[] = [];

		constructor() {
		}

		public add(start: number, end: number);
		public add(value: IRenderBounds);
		public add(a: any, b?: any): void {
			var start: number, end: number;

			if (arguments.length === 1) {
				this.mItems.push(<IRenderBounds>a);
			} else {
				this.mItems.push({
					start: a,
					end: b
				});
			}
		}

		public item(index: number): IRenderBounds {
			return this.mItems[index];
		}

		public clear(): void {
			this.mItems = [];
		}

		public contains(leafIdx: number): boolean;
		public contains(column: c1basefield): boolean;
		public contains(value: any): boolean {
			if (value && (<c1basefield>value).options) {
				value = (<c1basefield>value).options._leavesIdx;
			}

			if (value || (value === 0)) {
				for (var i = 0; i < this.mItems.length; i++) {
					var item = this.mItems[i];

					if ((item.start >= 0) && (value >= item.start) && (value <= item.end)) {
						return true;
					}
				}
			}

			return false;
		}

		public equals(value: renderableColumnsCollection): boolean {
			if (this === value) {
				return true;
			}

			var len = this.length();
			if (!value || (len !== value.length())) {
				return false;
			}

			var result = true;
			for (var i = 0; i < len && result; i++) {
				var a = this.item(i),
					b = value.item(i);

				result = (a.start === b.start) && (a.end === b.end);
			}

			return result;
		}

		public length(): number {
			return this.mItems.length;
		}

		public forEachIndex(callback: (absIdx: number) => any) {
			for (var i = 0; i < this.mItems.length; i++) {
				var item = this.mItems[i];

				if (item && (item.start >= 0)) {
					for (var j = item.start; j <= item.end; j++) {
						if (callback(j) === false) {
							return;
						}
					}
				}
			}
		}
	}

	/** @ignore */
	//export class fixedRenderColumnsRange extends renderableColumnsRange {
	//	private mFix: IRenderBounds;
	//	private mScroll: IRenderBounds;

	//	constructor() {
	//		super();

	//		this.mFix = { start: -1, end: -1 };
	//		this.mScroll = { start: -1, end: -1 };

	//		this.add(this.mFix);
	//		this.add(this.mScroll);
	//	}

	//	public Fix(): IRenderBounds {
	//		return this.mFix;
	//	}

	//	public Scroll(): IRenderBounds {
	//		return this.mScroll;
	//	}
	//}

	/** @ignore */
	export class renderableRowsCollection {
		private _maxRowIndex: number;
		private _items: IRenderBounds[] = [];
		private _capacity = null;

		constructor(maxRowIndex: number) {
			this._maxRowIndex = maxRowIndex;
		}

		public add(bounds: IRenderBounds) {
			this._capacity = null;

			var len = this._items.length,
				last: IRenderBounds;

			if (!len || (last = this._items[len - 1]).end < bounds.start) {
				if (len && (bounds.start - last.end === 1)) {
					last.end = bounds.end;
				} else {
					this._items.push(bounds);
				}
			} else {
				// todo: merge adjacent bounds

				var idxS = this._binSearchByStartVal(bounds.start),
					idxE = this._binSearchByEndVal(bounds.end),
					idxSn = idxS < 0 ? ~idxS : idxS,
					idxEn = idxE < 0 ? ~idxE : idxE,
					pinL = (idxS >= 0) || (idxSn > 0 && bounds.start <= this._items[idxSn - 1].end),
					pinR = (idxE >= 0) || (idxEn < len && bounds.end <= this._items[idxEn].end && bounds.end >= this._items[idxEn].start),
					cnt: number;

				if (pinL) {
					if (pinR) {
						// 1 1
						if (idxS >= 0) {
							this._items[idxEn].start = this._items[idxSn].start;

							if (cnt = (idxEn - idxSn)) {
								this._items.splice(idxSn, cnt);
							}
						} else {
							this._items[idxEn].start = this._items[idxSn - 1].start;

							if (cnt = idxEn - idxSn + 1) {
								this._items.splice(idxSn - 1, cnt);
							}
						}
					} else {
						// 1 0
						if (idxS >= 0) {
							this._items[idxSn].end = bounds.end;

							if (cnt = (idxEn - idxSn - 1)) {
								this._items.splice(idxSn, cnt);
							}
						} else {
							this._items[idxSn - 1].end = bounds.end;

							if (cnt = (idxEn - idxSn)) {
								this._items.splice(idxSn, cnt);
							}
						}
					}
				} else { // !pinL
					if (pinR) {
						// 0 1
						this._items[idxEn].start = bounds.start;

						if (cnt = (idxEn - idxSn)) {
							this._items.splice(idxSn, cnt);
						}
					} else {
						// 0 0
						this._items.splice(idxSn, idxEn - idxSn, bounds);
					}
				}
			}
		}

		//public remove(bounds: IRenderBounds) {
		//	this._capacity = null;
		//	var len = this._items.length;

		//	if (len) {
		//		var idxS = this._binSearchByStartVal(bounds.start),
		//			idxE = this._binSearchByEndVal(bounds.end),
		//			idxSn = idxS < 0 ? ~idxS : idxS,
		//			idxEn = idxE < 0 ? ~idxE : idxE,
		//			pinL = (idxS >= 0) || (idxSn > 0 && bounds.start <= this._items[idxSn - 1].end),
		//			pinR = (idxE >= 0) || (idxEn < len && bounds.end <= this._items[idxEn].end && bounds.end >= this._items[idxEn].start);

		//		if (pinL) {
		//			if (pinR) {
		//				var boundIdxL = (idxS >= 0) ? idxS : idxSn - 1,
		//					boundIdxR = (idxE >= 0) ? idxE : idxEn;

		//				if (idxS >= 0 && idxE >= 0) {
		//					this._items.splice(idxSn, idxEn - idxSn + 1);
		//				} else {
		//					if (idxS >= 0) {
		//						this._items[idxEn].start = bounds.end + 1;
		//						this._items.splice(idxSn, idxEn - idxSn);
		//					} else {
		//						if (idxE >= 0) {
		//							if (boundIdxL === boundIdxR) { // same bound
		//								this._items[idxSn - 1].end = bounds.start - 1;
		//							} else {
		//								this._items[idxSn].end = bounds.start - 1;
		//								this._items.splice(idxSn, idxEn - idxSn + 1);
		//							}
		//						}
		//						else {
		//							if (boundIdxL === boundIdxR) { // same bound, split single bound.
		//								var tEnd = this._items[idxSn - 1].end;
		//								this._items[idxSn - 1].end = bounds.start - 1;
		//								this._items.splice(idxSn, 0, { start: bounds.end + 1, end: tEnd });
		//							} else {
		//								this._items[idxSn - 1].end = bounds.start - 1;
		//								this._items[idxEn].start = bounds.end + 1;
		//								this._items.splice(idxSn, idxEn - idxSn);
		//							}
		//						}
		//					}
		//				}
		//			} else {
		//				if (idxS >= 0) {
		//					this._items.splice(idxSn, idxEn - idxSn);
		//				} else {
		//					this._items[idxSn].end = bounds.start - 1;
		//					this._items.splice(idxSn, idxEn - idxSn - 1);
		//				}
		//			}
		//		} else {
		//			if (pinR) {
		//				if (idxE >= 0) {
		//					this._items.splice(idxSn, idxEn - idxSn + 1);
		//				} else {
		//					// !!
		//					this._items[idxEn].start = bounds.end + 1;
		//					this._items.splice(idxSn, idxEn - idxSn);
		//				}
		//			} else {
		//				this._items.splice(idxSn, idxEn - idxSn);
		//			}
		//		}
		//	}
		//}

		//public clear() {
		//	this._capacity = 0;
		//	this._items = [];
		//}

		public item(index: number): IRenderBounds {
			return this._items[index];
		}

		//public forEachIndex(callback: (idx: number) => void ) {
		//	if (callback) {
		//		for (var i = 0, len = this._items.length; i < len; i++) {
		//			var bound = this._items[i];

		//			for (var start = bound.start, end = bound.end; start <= end; start++) {
		//				callback(start);
		//			}
		//		}
		//	}
		//}

		public forEachIndex(start: number, count: number, callback: (absIdx: number) => any) {
			if (start < 0) {
				return;
			}

			var len = this._items.length,
				state = this._getIteratorStateFor(start),
				cnt = 0,
				flag = true,
				abort = false;

			if (state) {
				var j = state.j;

				for (var i = state.i; i < len && !abort; i++) {
					var item = this._items[i];

					if (!flag) {
						j = item.start;
					}

					for (j; j <= item.end && !abort; j++) {
						if ((++cnt > count) && (count !== -1)) {
							return;
						}

						abort = (callback(j) === false);
					}

					flag = false;
				}
			}
		}

		public forEachIndexBackward(start: number, count: number, callback: (absIdx: number) => any) {
			if (start < 0) {
				return;
			}

			var len = this._items.length,
				state = this._getIteratorStateFor(start),
				cnt = 0,
				flag = true,
				abort = false;

			if (state) {
				var j = state.j;

				for (var i = state.i; i >= 0 && !abort; i--) {
					var item = this._items[i];

					if (!flag) {
						j = item.end;
					}

					for (j; j >= item.start && !abort; j--) {
						if ((++cnt > count) && (count !== -1)) {
							return;
						}

						abort = (callback(j) === false);
					}

					flag = false;
				}
			}
		}

		private _getIteratorStateFor(visIndex: number): { i: number; j: number; } {
			var len = this._items.length,
				cap = 0;

			for (var i = 0; i < len; i++) {
				var item = this._items[i];

				cap += item.end - item.start + 1;

				var delta = cap - visIndex;

				if (delta >= 0) {
					return {
						i: i,
						j: item.end - delta + 1
					};
				}
			}

			return null;
		}


		// [0-5],[10-15]
		// f(6) -> 10
		// Maps visible row index to absolute row (sketch)  index
		public getAbsIndex(renderedIndex: number): number {

			for (var i = 0, len = this._items.length; i < len; i++) {
				var bound = this._items[i],
					relEndIdx = bound.end - bound.start;

				if (renderedIndex <= relEndIdx) {
					return bound.start + renderedIndex;
				} else {
					renderedIndex -= relEndIdx + 1; // -= bound length
				}
			}

			return -1;
		}

		// Maps absolute row (sketch) index to rendered row index
		public getRenderedIndex(absIndex: number): number {
			var boundIndex = this.hasAbsIndex(absIndex);

			if (boundIndex >= 0) {
				var capacity = 0;

				for (var i = 0; i < boundIndex; i++) {
					var bound = this._items[i];
					capacity += bound.end - bound.start + 1;
				}

				return capacity + (absIndex - this._items[boundIndex].start);
			}

			return -1;
		}

		// returns index of the IRenderBound item in the _items array or -1.
		public hasAbsIndex(absIndex: number): number {

			if (this._items.length) {
				var idx = this._binSearchByStartVal(absIndex);

				if (idx >= 0) {
					return idx;
				} else {
					idx = ~idx;

					if (idx > 0 && absIndex <= this._items[idx - 1].end) {
						return idx - 1;
					}
				}
			}

			return -1;
		}

		public length(): number {
			return this._items.length;
		}

		public capacity() {
			if (this._capacity === null) {
				var result = 0;

				for (var i = 0, len = this._items.length; i < len; i++) {
					var bound = this._items[i];

					result += (bound.end - bound.start) + 1;
				}

				this._capacity = result;
			}

			return this._capacity;
		}

		//public truncate(start: number, end: number) {
		//	this.truncateByStart(start);
		//	this.truncateByStart(end);
		//}

		//public truncateByCount(value: number) {
		//	var count = 0;

		//	if (value === 0) {
		//		this._items = [];
		//	} else {
		//		for (var i = 0, len = this._items.length; i < len; i++) {
		//			var bound = this._items[i];

		//			count += (bound.end - bound.start) + 1;

		//			if (count >= value) {
		//				if (count > value) {
		//					bound.end -= count - value;
		//				}

		//				this._items.splice(i + 1, this._items.length - (i + 1));

		//				break;
		//			}
		//		}
		//	}
		//}

		public truncateByStart(value: number, pinFirstRemainingBoundToValue?: boolean) {
			var idx = this._binSearchByStartVal(value),
				len = this._items.length;

			if (idx < 0) {
				idx = ~idx;

				if (idx < len) {
					if (idx > 0 && this._items[idx - 1].end >= value) {
						this._items[idx - 1].start = value
						this._items.splice(0, idx - 1);
					} else {
						if (pinFirstRemainingBoundToValue) {
							this._items[idx].start = value;
						}

						this._items.splice(0, idx);
					}
				} else {
					this._items = [];
				}
			} else {
				this._items.splice(0, idx);
			}
		}

		//public truncateByEnd(value: number, pinLastRemainingBoundToValue?: boolean) {
		//	// todo
		//	var idx = this._binSearchByEndVal(value),
		//		len = this._items.length;

		//	if (idx < 0) {
		//		idx = ~idx;

		//		if (idx < len) {
		//			this._items[idx].end = value;
		//			this._items.splice(idx + 1, this._items.length - idx + 1);
		//		} else {
		//			this._items = [];
		//		}
		//	} else {
		//		this._items.splice(idx + 1, this._items.length - idx + 1);
		//	}
		//}


		//public deleteFromTop(count: number) {
		//	var i = 0,
		//		cap = 0;

		//	for (i = 0; i < this._items.length; i++) {
		//		var item = this._items[i];

		//		cap += item.end - item.start + 1;

		//		if (cap >= count) {
		//			break;
		//		}
		//	}

		//	if (cap >= count) {
		//		this._items[i].start = cap - count + 1;
		//	}

		//	this._items.splice(0, i);
		//}

		//public deleteFromBottom(count: number) {
		//	var i = 0,
		//		cap = 0;

		//	for (i = this._items.length - 1; i >= 0; i--) {
		//		var item = this._items[i];

		//		cap += item.end - item.start + 1;

		//		if (cap >= count) {
		//			break;
		//		}
		//	}

		//	if (cap >= count) {
		//		this._items[i].start = cap - count + 1;
		//	}

		//	this._items.splice(i, this._items.length - i);

		//}

		private _binSearchByStartVal(value: number): number {
			var l = 0,
				u = this._items.length - 1;

			while (l <= u) {
				var m = (u + l) >> 1,
					cmp = this._items[m].start - value;

				if (cmp === 0) {
					return m;
				}

				if (cmp < 0) {
					l = m + 1;
				} else {
					u = m - 1;
				}
			}

			return ~l;
		}

		private _binSearchByEndVal(value: number): number {
			var l = 0,
				u = this._items.length - 1;

			while (l <= u) {
				var m = (u + l) >> 1,
					cmp = this._items[m].end - value;

				if (cmp < 0) {
					l = m + 1;
				} else {
					if (cmp) {
						u = m - 1;
					} else {
						return m;
					}
				}
			}

			return ~l;
		}
	}
}