/// <reference path="interfaces.ts"/>
/// <reference path="../../../wijutil/jquery.wijmo.wijutil.ts" />

module wijmo.grid {


	var $ = jQuery;

	export var EXPANDO = "__wijgrid";

	var rctrltrim = /^[\f\n\r\t]+|[\f\n\r\t]+$/g;

	/** 
	* Specifies the type of a row in the grid.
	*/
	export enum rowType {
		/** The header row. */
		header = 1,

		/** Data row. */
		data = 2,

		/** Alternating data row (used only as modifier of the rowType.data, not as an independent value). */
		dataAlt = 4,

		/** Filter row. */
		filter = 8,

		/** Group header row. */
		groupHeader = 16,

		/** Group footer row. */
		groupFooter = 32,

		/** Footer row. */
		footer = 64,

		/** @ignore
		* Infrastructure.
		*/
		emptyDataRow = 128,

		/** Hierarchy header row  (used only as modifier of the rowType.data, not as an independent value). */
		dataHeader = 256,

		/** @ignore
		* Infrastructure, modifer of the rowType.data
		*/
		dataDetail = 512, // used by c1gridview only. c1gridview renders detail rows on server so formally they are data rows too.

		/** Hierararchy detail row. */
		detail = 1024 // used by wijgrid only.
	}

	/**
	* Determines an object render state. This enumeration can be used with the cellStyleFormatter and rowStyleFormatter options to get a formatted object state.
	*/
	export enum renderState {
		/** This is the normal state. The object is rendered and not hovered, selected, or one of the elements determining the current position of the wijgrid. */
		none = 0,

		/** The object is being rendered. In the cellStyleFormatter, the rendered object is a table cell. In the rowStyleFormatter, the object is a table row. */
		rendering = 1,

		/** The object is one of the elements determining the current position of the wijgrid. */
		current = 2,

		/** The object is hovered over. */
		hovered = 4,

		/** The object is selected. */
		selected = 8,

		/** @ignore. */
		editing = 16
	}

	/**
	* Infrastructure.
	* @ignore
	*/
	export enum renderStateEx {
		none = 0,
		hidden = 1, // row is hidden
		collapsed = 2 // row is collapsed (groupHeaders only)
	}

	/** 
	* Infrastructure.
	* @ignore
	*/
	export enum rowScope {
		table = 0,
		head = 1,
		body = 2,
		foot = 3
	}

	/** 
	* Infrastructure.
	* @ignore
	*/
	export enum cellRangeExtendMode {
		none = 0,
		column = 1,
		row = 2,
	}

	/** 
	* Infrastructure.
	* @ignore
	*/
	export enum objectMode {
		createIfNull = 0,
		createAlways = 1,
		dispose = 2
	}

	/**
	* Determines purpose of the group row cells.
	* @ignore
	*/
	export enum groupRowCellPurpose {
		groupCell = 0,
		aggregateCell = 1
	}

	/**
	* Infrastructure.
	* @ignore
	*/
	export enum dataRowsRangeMode {
		sketch = 0,
		rendered = 1,
		renderable = 2
	}

	/**
	* Infrastructure.
	* @ignore
	*/
	export enum intersectionMode {
		none = 0,
		overlapTop = 1,
		overlapBottom = 2,
		reset = 3
	}

	/**
	* Infrastructure.
	* @ignore
	*/
	export enum trimMethod {
		none = 0,
		control = 1, // \f\n\r\t (used by C1GridView)
		all = 2 // all spaces, control characters (used by wijgrid)
	}

	export function emptyTable(table: HTMLTableElement): void {
		if (table) {
			// remove rows and cells
			while (table.rows.length) {
				var row;

				while ((<any>(row = table.rows[0])).cells.length) {
					var cell = row.cells[0];

					while (cell.firstChild) {
						cell.removeChild(cell.firstChild);
					}

					row.deleteCell(0);

					row = cell = null;
				}

				table.deleteRow(0);
			}

			// remove thead\ tbody\ tfoot elements
			while (table.firstChild) {
				table.removeChild(table.firstChild);
			}
		}

		table = null;
	}

	/** @ignore */
	export function compareObj(a: any, b: any): boolean {
		var flag: boolean;

		if ($.isArray(a) && $.isArray(b)) {
			if (a.length === b.length) {
				flag = true;

				for (var i = 0, len = a.length; i < len && flag; i++) {
					flag = wijmo.grid.compareObj(a[i], b[i]);
				}

				return flag;
			}
		} else {
			if ($.isPlainObject(a) && $.isPlainObject(b)) {
				for (var key in a) {
					if (a.hasOwnProperty(key)) {
						if (!wijmo.grid.compareObj(a[key], b[key])) {
							return false;
						}
					}
				}

				for (var key in b) {
					if (b.hasOwnProperty(key)) {
						if (!wijmo.grid.compareObj(a[key], b[key])) {
							return false;
						}
					}
				}

				return true;
			} else {
				if (a instanceof Date) {
					a = a.getTime();
				}

				if (b instanceof Date) {
					b = b.getTime();
				}
			}
		}

		return a === b;
	}

	/** @ignore */
	export function stringFormat(pattern: string, ...params: any[]): string {
		var i: number,
			len: number;

		if (!pattern) {
			return "";
		}

		for (i = 0, len = params.length; i < len; i++) {
			pattern = pattern.replace(new RegExp("\\{" + i + "\\}", "gm"), params[i]);
		}

		return pattern;
	}

	/** @ignore */
	export function validDataKey(dataKey: any): boolean {
		return (dataKey && !(dataKey < 0)) || (dataKey === 0);
	}

	/** @ignore */
	export function validDate(date: Date): boolean {
		if (date && (date instanceof Date)) {
			return !isNaN(date.getTime());
		}

		return false;
	}

	/** @ignore */
	export function getDataType(column: IColumn): string {
		return column.dataType || "string";
	}

	/** @ignore */
	export function iterateChildrenWidgets(item: JQuery, callback: (domIndex: number, dataValue: JQueryUIWidget) => void) {
		if (item && callback) {
			item.find(".ui-widget,.wijmo-wijgrid-root").each(function (index, dom) {
				$.each($(dom).data(), function (dataKey, dataValue) {
					if (dataValue.widgetName) {
						callback(index, dataValue);
					}
				});

				return true;
			});
		}
	}

	/** @ignore */
	export function remove$dataByPrefix(element: JQuery, prefix: string) {
		var data$keys = [];

		$.each(element.data(), function (key) {
			if (key.indexOf(prefix) === 0) {
				data$keys.push(key);
			}
		});


		$.each(data$keys, function (idx, key) {
			element.removeData(key);
		});
	}

	/** @ignore */
	export class domSelection {
		private _dom: HTMLInputElement;

		constructor(dom: HTMLInputElement) {
			this._dom = dom;
		}

		// The 'dom' must be an input element
		getSelection()
			: { start: number; end: number; length: number; } {
			var start = 0,
				end = 0,
				textRange;

			if (this._dom.selectionStart !== undefined) { // DOM3
				start = this._dom.selectionStart;
				end = this._dom.selectionEnd;
			} else {
				if (document.selection) { // IE
					textRange = document.selection.createRange().duplicate();
					end = textRange.text.length; // selection length
					start = Math.abs(textRange.moveStart("character", -this._dom.value.length)); // move selection to the beginning
					end += start;
				}
			}

			return { start: start, end: end, length: end - start };
		}

		// The 'dom' must be an input element
		setSelection(range: { start: number; end: number; }): void {
			if (this._dom.selectionStart !== undefined) { // DOM3
				this._dom.setSelectionRange(range.start, range.end);
			} else { // IE
				var textRange = this._dom.createTextRange();

				textRange.collapse(true);
				textRange.moveStart("character", range.start);
				textRange.moveEnd("character", range.end);
				textRange.select();
			}
		}

		toggleSelection(enable: boolean): void {
			var $dom = $(this._dom),
				useSelectStart = "onselectstart" in this._dom;

			if (enable) {
				if (useSelectStart) {
					$dom.unbind(".wijgrid-disableSelection");
				} else {
					$dom.css({ "MozUserSelect": "", "WebkitUserSelect": "" });
				}
			} else {
				if (useSelectStart) {
					$dom.bind("selectstart.wijgrid-disableSelection", function (e) {
						e.preventDefault();
					});
				} else {
					$dom.css({ "MozUserSelect": "-moz-none", "WebkitUserSelect": "none" });
				}
			}
		}
	}

	/** @ignore */
	export function createDynamicField(options: any): IColumn {
		var opt: IColumnInternalOptions = {};

		opt._dynamic = true;
		opt._isLeaf = true; // important!

		return <IColumn>$.extend(true, opt, options);
	}


	/** @ignore */
	export function bounds(element, client?: boolean): IElementBounds {
		if (element) {
			var $dom = element.nodeType ? $(element) : element,
				offset = $dom.offset();

			if (offset) {
				if (client) {
					return { top: offset.top, left: offset.left, width: $dom[0].clientWidth || 0, height: $dom[0].clientHeight || 0 };
				}

				return { top: offset.top, left: offset.left, width: $dom.outerWidth(), height: $dom.outerHeight() };
			}
		}

		return null;
	}

	/** @ignore */
	export function ensureBounds(bounds: IRenderBounds, max: number): IRenderBounds {
		if (bounds) {
			if (bounds.start < 0) {
				bounds.start = 0;
			}

			if (bounds.end < 0) {
				bounds.end = 0;
			}

			bounds.start = Math.min(bounds.start, max);

			bounds.end = Math.min(bounds.end, max);
		}

		return bounds;
	}

	// maxDepth = -1 --  iterate through all child elements
	// default value = 3
	/** @ignore */
	export function _getDOMText(dom: Element, maxDepth?: number, ignoreTextNodes?: boolean): string {
		if (!ignoreTextNodes && dom.nodeType === 3) { // text node
			var nodeValue = (dom.nodeValue || "").replace(/[\r\t\n]+/g, "");
			return nodeValue;
		}

		if (dom && maxDepth !== 0) {
			if (!ignoreTextNodes && dom.nodeType === 3) { // text node
				return dom.nodeValue;
			}

			if (dom.nodeType === 1) { // element
				switch ((<any>dom).type) {
					case "button":
					case "text":
					case "textarea":
					case "select-one":
						return (<any>dom).value;
					case "checkbox":
						return (<any>dom).checked.toString();
				}

				// go deeper
				var result = "",
					i = 0, child;

				while (child = dom.childNodes[i++]) {
					result += wijmo.grid._getDOMText(child, maxDepth - 1, ignoreTextNodes);
				}

				return result;
			}
		}

		return "";
	}

	/** @ignore */
	export function isNaN(value): boolean {
		return value !== value; // return true only if the value is NaN.
	}

	// obj, prefix, name (opt), value (opt)
	/** @ignore */
	export function dataPrefix(obj: any, prefix: string, name?: string, value?: any): any {
		var treatAsArray = (obj.jquery || $.isArray(obj)), // arrays of jQuery objects is not supported
			internalName = prefix + name;

		if (arguments.length === 3) { // getter
			if (treatAsArray) {
				return $.data(obj[0], internalName); // first item only
			}

			return $.data(obj, internalName);
		} else { // setter
			if (treatAsArray) {
				var tmp;

				for (var i = 0, len = obj.length; i < len; i++) {
					tmp = $.data(obj[i], internalName, value);
				}

				return tmp;
			}

			return $.data(obj, internalName, value);
		}
	}

	/** @ignore */
	export function shallowMerge(target: any, src: any) {
		if (src && target) {
			for (var name in src) {
				if (src.hasOwnProperty(name)) {
					var value = src[name],
						typeOf = typeof (value);

					if ((typeOf === "string" || typeOf === "boolean" || typeOf === "number") && (target[name] === undefined)) {
						target[name] = value;
					}
				}
			}
		}
	}

	/** @ignore */
	export function getAttributes(dom: HTMLElement, prevent?: (attrName: string) => boolean): any {
		if (dom) {
			var cnt = 0,
				result = {};

			for (var i = 0, len = dom.attributes.length; i < len; i++) {
				var attrName = dom.attributes[i].name;

				if (attrName && (!prevent || !prevent(attrName))) {
					var attrValue = dom.getAttribute(attrName);

					if (attrName === "style") {
						attrValue = (typeof (attrValue) === "object")
						? (<CSSStyleDeclaration><any>attrValue).cssText
						: attrValue;
					}

					if (!attrValue && attrName === "class") {
						attrValue = dom.getAttribute("className");
					}

					if (attrValue && (typeof (attrValue) !== "function")) {
						result[attrName] = attrValue;
						cnt++;
					}
				}
			}

			if (cnt) {
				return result;
			}
		}

		return null;
	}

	// unlike the jQuery.extend(true) function the deepExtend() function doesn't skips undefined values.
	/** @ignore */
	export function deepExtend(source: any, target: any): any {
		var key, src, dst, isArray, clone;

		if (source) {
			if (typeof (target) !== "object" && !$.isFunction(target)) {
				target = {};
			}

			for (key in source) {
				src = source[key];
				dst = target[dst];

				if (src === target) {
					continue;
				}

				if (src && ($.isPlainObject(src) || (isArray = $.isArray(src)))) {
					if (isArray) {
						isArray = false;
						clone = dst && $.isArray(dst) ? dst : [];
					} else {
						clone = dst && $.isPlainObject(dst) ? dst : {};
					}

					target[key] = wijmo.grid.deepExtend(src, clone);
				} else {
					target[key] = src;
				}
			}
		}

		return target;
	}

	/** @ignore */
	export function widgetName(element: any): string;
	/** @ignore */
	export function widgetName(element: any, name: string): string;
	/** @ignore */
	export function widgetName(element: any, name?: string): string {
		if (element && element.jquery) {
			element = element[0];
		}

		if (element) {
			return (arguments.length === 1)
				? <string>$.data(element, "wijgridwidgetName")
				: <string>$.data(element, "wijgridwidgetName", name);
		}

		return undefined;
	}

	/** @ignore */
	export class HTML5InputSupport {
		private static _requiresExtendedSupport = {
			"date": "",
			"datetime": "",
			"datetime-local": "",
			"month": "",
			"time": ""
		}

		private static _supportedInputTypesCache = {};

		public static isExtendSupportRequired(inputType: string): boolean {
			inputType = (inputType || "").toLowerCase();

			return (inputType in wijmo.grid.HTML5InputSupport._requiresExtendedSupport);
		}

		public static getDefaultInputType(mobileEnvironment: boolean, column: IColumn): string {
			var inputType = (column.inputType || "").toLowerCase();

			if (!inputType && mobileEnvironment) { // provide input type automatically
				switch (wijmo.grid.getDataType(column)) {
					case "number":
					case "currency":
						inputType = "number";
						break;

					case "datetime":
						inputType = "datetime";
						break;
				}
			}

			if (!inputType || ((inputType !== "text") && !HTML5InputSupport._isSupportedByBrowser(inputType))) {
				inputType = "text"; // fallback to "text"
			}

			return inputType;
		}

		public static toStr(value: any, inputType: string): string {
			var result = value;

			inputType = (inputType || "").toLowerCase();

			if (wijmo.grid.HTML5InputSupport.isExtendSupportRequired(inputType)) {
				switch (inputType) {
					case "datetime":
						result = (value)
						? result = Globalize.format(value, "yyyy-MM-ddTHH:mm:ssZ")
						: "";
						break;

					case "datetime-local":
						result = (value)
						? result = Globalize.format(value, "yyyy-MM-ddTHH:mm:ss")
						: "";
						break;

					case "date":
						result = (value)
						? result = Globalize.format(value, "yyyy-MM-dd")
						: "";
						break;

					case "month":
						result = (value)
						? result = Globalize.format(value, "yyyy-MM")
						: "";
						break;

					case "time":
						result = (value)
						? result = Globalize.format(value, "HH:mm:ss")
						: "";
						break;
				}
			} else {
				result = value + "";
			}

			return <string><any>result;
		}

		public static parse(value: string, inputType: string): any {
			var result: any,
				fallback = function (date: string): Date {
					date = <string><any>new Date(date);
					if (!wijmo.grid.validDate(<Date><any>date)) {
						date = null;
					}

					return <Date><any>date;
				};

			inputType = (inputType || "").toLowerCase();

			if (wijmo.grid.HTML5InputSupport.isExtendSupportRequired(inputType)) {
				switch (inputType) {
					case "datetime":
						result = Globalize.parseDate(value, "yyyy-MM-ddTHH:mm:ssZ") || Globalize.parseDate(value, "yyyy-MM-ddTHH:mmZ") || fallback(value);
						break;
					case "datetime-local":
						result = Globalize.parseDate(value, "yyyy-MM-ddTHH:mm:ss") || Globalize.parseDate(value, "yyyy-MM-ddTHH:mm") || fallback(value);
						break;

					case "date":
						result = Globalize.parseDate(value, "yyyy-MM-dd") || fallback(value);
						break;

					case "month":
						result = Globalize.parseDate(value, "yyyy-MM");
						break;

					case "time":
						result = Globalize.parseDate(value, "HH:mm:ss") || Globalize.parseDate(value, "HH:mm");;
						break;

					case "number":
						result = parseFloat(value);
				}
			} else {
				result = value;
			}

			return result;
		}

		public static extend(value: any, extendWith: any, inputType: string): any {
			if (!value) {
				value = extendWith;
			} else {
				inputType = (inputType || "").toLowerCase();

				switch (inputType) {
					case "date":
						value.setFullYear(extendWith.getFullYear(), extendWith.getMonth(), extendWith.getDate());
						break;

					case "month":
						value.setFullYear(extendWith.getFullYear(), extendWith.getMonth());
						break;

					case "time":
						value.setHours(extendWith.getHours());
						value.setMinutes(extendWith.getMinutes());
						value.setSeconds(extendWith.getSeconds());
						break;

					default:
						value = extendWith;
				}
			}

			return value;
		}

		private static _isSupportedByBrowser(inputType: string): boolean {
			if (inputType) {
				if (this._supportedInputTypesCache[inputType] === undefined) { // value is not tested yet
					var success: boolean;

					try {
						var $element = $("<input type='" + inputType + "' style='display:none' />");
						success = true;
					} catch (e) {
						success = false;
					}

					this._supportedInputTypesCache[inputType] = success && ((<HTMLInputElement>$element[0]).type === inputType);
				}

				return this._supportedInputTypesCache[inputType];
			}

			return false;
		}
	}

	/** @ignore */
	export function getZIndex(element: JQuery, minValue: number = 99) {
		var zIndex = 0;

		if (element && $.ui && $.fn.zIndex) {
			zIndex = element.zIndex(); // try to get zIndex of the first z-indexed ancestor. 

			if (zIndex) {
				zIndex++; // get next value
			}
		}

		return Math.max(zIndex, minValue);
	}

	// * taken from jQuery UI
	/** @ignore */
	export function isOverAxis(x: number, reference: number, size: number): boolean {
		// Determines when x coordinate is over "b" element axis
		return (x > reference) && (x < (reference + size));
	}

	/** @ignore */
	export function isOver(y: number, x: number, top: number, left: number, height: number, width: number): boolean {
		// Determines when x, y coordinates is over "b" element
		return wijmo.grid.isOverAxis(y, top, height) && wijmo.grid.isOverAxis(x, left, width);
	}
	// taken from jQuery UI *

	// ** uid
	var __uid: number = 0;
	/** @ignore */
	export function getUID(): string {
		return "uid" + __uid++;
	}
	// uid **

	/** @ignore */
	export function isMobileSafari() {
		return !!(navigator && navigator.userAgent && (navigator.userAgent.match(/Mobile.*Safari/)) !== null);
	}

	/** @ignore */
	export function isMobile() {
		if (navigator) {
			var agent = (navigator.userAgent || "").toLowerCase();
			return !!(agent.indexOf("android") >= 0 || agent.match(/iphone|ipad|ipod|mobi/i));
		}
		return false;
	}

	/** @ignore */
	export function getWindowOrientation(): number {
		return (<any>window).orientation || 0; // browser-specific. Currently used only to detect orientation changes in Android.
	}

	/** @ignore */
	export function isPercentage(value: any): boolean {
		return !!(value && (typeof (value) === "string") && (value[value.length - 1] === "%"));
	}

	/** @ignore*/
	export function rowObjToJQuery(row: IRowObj): JQuery {
		if (row) {
			if (row[1]) {
				return $(row); // both items
			}

			return $(row[0]); // first item only
		}

		return null;
	}

	var __scrollBarSize = 0;
	/** @ignore */
	export function getSuperPanelScrollBarSize(): number {
		if (!(__scrollBarSize > 0)) {
			if (document && document.body && $.support.isTouchEnabled && $.support.isTouchEnabled()) { // test for native wijsuperpanel mode
				var $div: JQuery;

				try {
					$div = $("<div></div>")
						.css({
							overflow: "scroll",
							width: 30,
							height: 30,
							position: "absolute",
							visibility: "hidden"
						})
						.append($("<div></div>").css({ width: 100, height: 100 })) // append a child
						.appendTo(document.body);

					__scrollBarSize = $div[0].offsetWidth - $div[0].clientWidth; // measure
				}
				catch (ex) {
				}
				finally {
					if ($div) {
						$div.remove();
					}
				}
			}

			if (!(__scrollBarSize > 0)) {
				__scrollBarSize = 18; // use the default size of the wijsuperpanel' scrollbars
			}
		}

		return __scrollBarSize;
	}

	/** @ignore */
	export function getContent(element: HTMLElement, decodeHTML: boolean, trim: trimMethod): string {
		var value = "";

		if (decodeHTML && element.childNodes.length === 1 && element.firstChild.nodeType === 3) { // test for text-only content to ignore server-side templates
			value = (element.textContent !== undefined) ? element.textContent : element.innerText; // IE <= 8

			if (trim) {
				value = (trim === trimMethod.all) ? $.trim(value) : trimCtrl(value);
			}

			if (value && (value.length === 1)) { // test for "&nbsp;"
				var ch = value.charCodeAt(0);

				if ((ch === 160) || ((ch === 32 /* IE < 9 */) && ($.trim(element.innerHTML) === "&nbsp;"))) {
					value = "";
				}
			}

		} else {
			value = element.innerHTML;

			if (trim) {
				value = (trim === trimMethod.all) ? $.trim(value) : trimCtrl(value);
			}

			if (value === "&nbsp;") {
				value = "";
			}
		}

		return value;
	}

	/** @ignore */
	export function setContent(element: HTMLElement, encodeHTML: boolean, value: string): void {
		if (encodeHTML) {
			if (element.textContent !== undefined) {
				element.textContent = value || "";
			} else {
				element.innerText = value || ""; // IE <= 8
			}
		} else {
			element.innerHTML = value || "&nbsp;";
		}
	}

	// removes control characters only from the beginning and the end of the string.
	/** @ignore */
	function trimCtrl(value: string): string {
		return (value === null)
			? ""
			: (value + "").replace(rctrltrim, "");
	}

	/** @ignore */
	export class DataOptionPersister {
		static DATA_STORAGE_KEY = "wijgridDataStorage";

		public static persistAndClear(options: IWijgridOptions): ITempDataStorage {
			var result: ITempDataStorage = {},
				recurse = function (o: IWijgridOptions, storage: ITempDataStorage) {
					if (o) {
						if (o.data) {
							storage.data = o.data;
							o.data = undefined; // clear
						}

						if (o.detail) {
							storage.detail = {};
							recurse(o.detail, storage.detail);
						}
					}
				};

			recurse(options, result);

			return result;
		}

		public static restore(options: IWijgridOptions, storage: ITempDataStorage) {
			var recurse = function (o: IWijgridOptions, st: ITempDataStorage) {
				if (o && st) {
					if (st.data) {
						o.data = st.data;
					}

					recurse(o.detail, st.detail);
				}
			};

			recurse(options, storage);
		}

		public static persistAndClearIntoElement(element: Element, options: IWijgridOptions) {
			$.data(element, this.DATA_STORAGE_KEY, this.persistAndClear(options));
		}

		public static restoreFromElement(element: Element, options: IWijgridOptions, removeData: boolean = false) {
			this.restore(options, $.data(element, this.DATA_STORAGE_KEY));

			if (removeData) {
				$.removeData(element, this.DATA_STORAGE_KEY);
			}
		}
	}

	// * compatibility: export members to the $.wijmo.wijgrid "namespace" *
	$.extend($.wijmo.wijgrid, {
		rowType: wijmo.grid.rowType,
		renderState: wijmo.grid.renderState,
		bounds: wijmo.grid.bounds // used by unit tests. TODO: remove
	});
}

module wijmo.grid {
	/** @ignore */
	export enum TimeUnit {
		Millisecond = 1,
		Second = 2,
		Minute = 4,
		Hour = 8,
		Day = 16,
		Month = 32,
		Year = 64,

		DATE = TimeUnit.Year | TimeUnit.Month | TimeUnit.Day,
		TIME = TimeUnit.Hour | TimeUnit.Minute | TimeUnit.Second | TimeUnit.Millisecond,
		ALL = TimeUnit.DATE | TimeUnit.TIME
	};

	/** @ignore */
	export class TimeUnitConverter {
		/**
		* @param inputType One of the HTML input type values (date-time).
		*/
		public static convertInputType(inputType: string): TimeUnit {
			var formatString = wijmo.grid.TimeUnitConverter.convertInputTypeToFormatString(inputType),
				result = wijmo.grid.TimeUnitConverter.convertFormatString(formatString);

			return result;
		}

		public static convertInputTypeToFormatString(inputType: string): string {
			// specific format values are not important here.
			switch ((inputType || "").toLowerCase()) {
				case "datetime":
				case "datetime-local":
					return "f"; // long date, short time

				case "date":
					return "d"; // short date

				case "month":
					return "Y"; // year-and-month

				case "time":
					return "t"; // short time
			}

			return "";
		}

		/**
		* @param dateFormatString
		*/
		public static convertFormatString(dateFormatString: string): TimeUnit {
			var result: TimeUnit = 0;

			if (dateFormatString) {
				// ** check one-char standard formats **

				if (dateFormatString.length === 1) {
					switch (dateFormatString[0]) {
						case "t": // short Time
							return TimeUnit.Hour | TimeUnit.Minute;

						case "T": // long Time
							return TimeUnit.TIME;

						case "d": // short Date
						case "D": // long Date
							return TimeUnit.DATE;

						case "Y": // month/year
							return TimeUnit.Month | TimeUnit.Year;

						case "M": // month/day
							return TimeUnit.Month | TimeUnit.Day;

						case "f", "F", "S":
							return TimeUnit.ALL;
					}
				}

				// ** check custom tokens **

				var quoteFirst, quoteLast;

				// remove quoted text
				if (((quoteFirst = dateFormatString.indexOf("'")) >= 0) && ((quoteLast = dateFormatString.lastIndexOf("'")) >= 0) && (quoteFirst !== quoteLast)) {
					dateFormatString = dateFormatString.substr(0, quoteFirst) + dateFormatString.substring(quoteLast + 1, dateFormatString.length - 1);
				}

				// the validness of the string is not a subject to check
				for (var i = 0, len = dateFormatString.length; i < len; i++) {
					switch (dateFormatString[i]) {
						case "d": // day: d, dd, ddd, dddd
							result |= TimeUnit.Day;
							break;

						case "M": // month: M, MM, MMM, MMMM
							result |= TimeUnit.Month;
							break;

						case "y": // year: yy, yyyy
							result |= TimeUnit.Year;
							break;

						case "m": // minute: m, mm
							result |= TimeUnit.Minute;
							break;

						case "h": // hour: h, hh
						case "H": // hour: H, HH
							result |= TimeUnit.Hour;
							break;

						case "s": // second: s, ss
							result |= TimeUnit.Second;
							break;

						case "f": // milliseconds: f, ff, fff
							result |= TimeUnit.Millisecond;
							break;
					}
				}
			}

			return result || TimeUnit.ALL;
		}

		public static cutDate(date: Date, timeUnit: TimeUnit): Date {
			if (date) {
				timeUnit = ~timeUnit;

				if (timeUnit & TimeUnit.Millisecond) {
					date.setMilliseconds(0);
				}

				if (timeUnit & TimeUnit.Second) {
					date.setSeconds(0);
				}

				if (timeUnit & TimeUnit.Minute) {
					date.setMinutes(0);
				}

				if (timeUnit & TimeUnit.Hour) {
					date.setHours(0);
				}

				if (timeUnit & TimeUnit.Day) {
					date.setDate(1);
				}

				if (timeUnit & TimeUnit.Month) {
					date.setMonth(0);
				}

				if (timeUnit & TimeUnit.Year) {
					date.setFullYear(0);
				}
			}

			return date;
		}
	}

	/** @ignore */
	export function lazy(eval: () => any, context?): () => any {
		var hasValue = false,
			value;
		return () => {
			if (!hasValue) {
				value = context ? eval.call(context) : eval();
				hasValue = true;
			}
			return value;
		};
	}
}