/// <reference path="../../../wijsuperpanel/jquery.wijmo.wijsuperpanel.ts" />
/// <reference path="../../../wijlist/jquery.wijmo.wijlist.ts" />
/// <reference path="interfaces.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />
/// <reference path="baseView.ts" />
/// <reference path="uiVirtualScroller.ts" />

module wijmo.grid {


    var $ = jQuery;

    /** @ignore */
    export class fixedView extends baseView {
        _verScrollBarSize = wijmo.grid.getSuperPanelScrollBarSize();
        _mouseWheelHandler: any;

        _viewTables: { ne: wijmo.grid.htmlTableAccessor; nw: wijmo.grid.htmlTableAccessor; se: wijmo.grid.htmlTableAccessor; sw: wijmo.grid.htmlTableAccessor; } = <any>{}; // rendered DOM tables
        _splitAreas: { ne: JQuery; nw: JQuery; se: JQuery; sw: JQuery; } = <any>{}; // rendered split areas

        _scroller: JQuery; // scrolling div
        _superPanelElementsCache: { contentWrapper: JQuery; stateContainer: JQuery; templateWrapper: JQuery; } = <any>{};

        element: JQuery; // table element
        _staticDataRowIndex: number;
        _staticRowIndex: number;
        _staticColumnIndex: number;
        _staticAllColumnIndex: number;
        vsUI: wijmo.grid.uiVirtualScroller;
        mResultWidths: IColumnWidth[];
        mRowsHeights: number[]; // stores the rows heights calculated by the _adjustRowsHeights to improve render speed during horizontal virtual scrolling if static columns are used.

        mHVSOffset: number = 0; // horizontal virtual scrolling offset

        constructor(wijgrid: wijgrid, renderBounds: IRenderBounds) {
            super(wijgrid, renderBounds);

            this.mRowsHeights = [];

            this.element = wijgrid.element; // table element

            this._staticDataRowIndex = wijgrid._getStaticIndex(true);
            this._staticRowIndex = wijgrid._getRealStaticRowIndex();
            this._staticColumnIndex = wijgrid._getRealStaticColumnIndex();
            this._staticAllColumnIndex = (this._staticColumnIndex === -1) ? -1 : wijgrid._visibleLeaves()[this._staticColumnIndex].options._leavesIdx;

            this._mouseWheelHandler = $.proxy(this._onMouseWheel, this);
        }

        dispose() {
            super.dispose();

            this.element.css({ width: "", height: "" }); // 48969
            this._wijgrid.mOuterDiv.unbind("mousewheel", this._mouseWheelHandler);
        }

        //ensureWidth(index, value, oldValue) {
        //	var wijgrid = this._wijgrid,
        //		staticColumnIndex = this._staticColumnIndex,
        //		bWest = index <= staticColumnIndex,
        //		$tableNW = $(this._viewTables.nw.element()),
        //		$tableNE = $(this._viewTables.ne.element()),
        //		$tableSW = $(this._viewTables.sw.element()),
        //		$tableSE = $(this._viewTables.se.element()),
        //		tableArray = bWest ? [$tableNW, $tableSW] : [$tableNE, $tableSE],
        //		tableWidth = (bWest ? $tableNW.width() : $tableNE.width()) + value - oldValue,
        //		scrollValue = this.getScrollValue();

        //	this._destroySuperPanel();

        //	super.ensureWidth(index, value, oldValue);

        //	this._setTableWidth(tableArray, tableWidth, value, index);

        //	try {
        //		if (staticColumnIndex >= 0) {
        //			wijgrid.mSplitDistanceX = $tableNW[0].offsetWidth;
        //		} else {
        //			wijgrid.mSplitDistanceX = 0;
        //		}
        //	} catch (ex) { }

        //	this._updateSplitAreaBounds(0);

        //	this._adjustRowsHeights();

        //	try {
        //		if (this._staticRowIndex >= 0) {
        //			wijgrid.mSplitDistanceY = Math.max($tableNW[0].offsetHeight, $tableNE[0].offsetHeight);
        //		} else {
        //			wijgrid.mSplitDistanceY = 0;
        //		}
        //	} catch (ex) { }

        //	this._updateSplitAreaBounds(1);

        //	this.refreshPanel(scrollValue);

        //	var frozener = wijgrid._UIFrozener();
        //	if (frozener) {
        //		frozener.refresh();
        //	}
        //}

        render(live: boolean) {
            if (!live) {
                this.resetRowsHeightsCache();
            }

            super.render.apply(this, arguments);
        }

        _resetWidth() {
            this._getSuperPanelContentWrapper().width("");

            super._resetWidth.apply(this, arguments);
        }

        resetRowsHeightsCache(): void {
            this.mRowsHeights = [];
        }

        ensureHeight(rowIndex?: number) {
            var wijgrid = this._wijgrid,
                $tableNW = $(this._viewTables.nw.element()),
                $tableNE = $(this._viewTables.ne.element()),
                $tableSW = $(this._viewTables.sw.element()),
                $tableSE = $(this._viewTables.se.element()),
                scrollValue = this.getScrollValue();

            this._destroySuperPanel();

            $tableSE.css("height", "");
            $tableSW.css("height", "");

            if (arguments.length > 0) {
                var rowObjsArray = this.getJoinedRows(rowIndex, 2);
                this._setRowHeight(rowObjsArray, this._getRowHeight(rowObjsArray));
            }

            var maxHeight = Math.max($tableSE.height(), $tableSW.height());

            $tableSE.height(maxHeight);
            $tableSW.height(maxHeight);

            try {
                if (this._staticRowIndex >= 0) {
                    wijgrid.mSplitDistanceY = Math.max($tableNW[0].offsetHeight, $tableNE[0].offsetHeight);
                } else {
                    wijgrid.mSplitDistanceY = 0;
                }
            } catch (ex) { }

            this._updateSplitAreaBounds(1);

            this._refreshPanel(scrollValue);

            wijgrid._UIFrozener().refresh();
        }

        getScrollValue(): IScrollingState {
            var superPanelObj = this._getSuperPanel();

            return superPanelObj
                ? { x: superPanelObj.options.hScroller.scrollValue, y: superPanelObj.options.vScroller.scrollValue }
                : null;
        }

        getVisibleAreaBounds(client?: boolean): IElementBounds {
            var bounds = this._isNativeSuperPanel()
                ? wijmo.grid.bounds(this._getSuperPanelStateContainer(), client)
                : wijmo.grid.bounds(this._getSuperPanelContentWrapper(), client);

            if (!bounds) { // .wijmo-wijsuperpanel-contentwrapper is not available -- grid is invisible.
                bounds = wijmo.grid.bounds(this._wijgrid.mOuterDiv, client);
            }

            return bounds;
        }

        getVisibleContentAreaBounds(): IElementBounds {
            var visibleBounds = this.getVisibleAreaBounds(true), // scrollable area
                b00 = wijmo.grid.bounds(this._viewTables.nw.element()) || <IElementBounds>{},
                b01 = wijmo.grid.bounds(this._viewTables.ne.element()) || <IElementBounds>{},
                b10 = wijmo.grid.bounds(this._viewTables.sw.element()) || <IElementBounds>{},
                b11 = wijmo.grid.bounds(this._viewTables.se.element()) || <IElementBounds>{},

                contentBounds: IElementBounds = { // tables area
                    top: visibleBounds.top,
                    left: visibleBounds.left,
                    width: Math.max(b00.width + b01.width, b10.width + b11.width),
                    height: Math.max(b00.height + b10.height, b01.height + b11.height)
                };

            // truncate
            contentBounds.width = Math.min(visibleBounds.width, contentBounds.width);
            contentBounds.height = Math.min(visibleBounds.height, contentBounds.height);

            return contentBounds;
        }

        _getRowAreaHeight(): number {
            var container = this._getSuperPanelContentWrapper();
            if (container.length == 0) {
                container = this._wijgrid.mOuterDiv;
            }
            var height = container.height();

            // subtract top fixed area
            var topFixedAreaHeight = this._wijgrid.mSplitDistanceY;
            if (topFixedAreaHeight) {
                height -= topFixedAreaHeight;
            }

            // subtract footer height
            if (this._wijgrid.options.showFooter) {
                var footer,
                    footerHeight = (this.isRendered() && (footer = this.footerRow()))
                        ? $(footer).height()
                        : this.getDefaultRowHeight(); // An assumption. Or we really need to link the footer height with the wijgrid.rowHeight?

                height -= footerHeight;
            }

            return height;
        }

        getVirtualPageSize(upper: boolean = true) {
            var rowHeight = this._wijgrid.mRowOuterHeight && (this._wijgrid.mRowOuterHeight > 0)
                ? this._wijgrid.mRowOuterHeight
                : this.getDefaultRowHeight();

            var vPageSize = this._getRowAreaHeight() / rowHeight;

            return upper
                ? Math.ceil(vPageSize)
                : Math.floor(vPageSize);
        }

        getFixedAreaVisibleBounds(): IElementBounds[] {
            var bounds = this.getVisibleAreaBounds(),
                neBounds = wijmo.grid.bounds(this._splitAreas.ne),
                nwBounds = wijmo.grid.bounds(this._splitAreas.nw),
                horBounds = null,
                verBounds = null;

            if (neBounds.height || nwBounds.height) {
                horBounds = {
                    left: bounds.left,
                    top: bounds.top,
                    width: bounds.width,
                    height: Math.min(neBounds.height || nwBounds.height, bounds.height)
                };
            }

            if (nwBounds.width) {
                verBounds = {
                    //left: bounds.left,
                    left: nwBounds.left,
                    top: bounds.top,
                    width: Math.min(nwBounds.width, bounds.width),
                    height: bounds.height
                };
            }

            return [horBounds, verBounds];
        }

        getDefaultRowHeight(): number {
            return this._wijgrid._lgGetRowHeight();
        }

        _refreshPanel(scrollValue?: IScrollingState) {
            var self = this,
                wijgrid = this._wijgrid,
                options = wijgrid.options,
                panelModes = this._getMappedScrollMode(),
                firstRow: IRowObj = wijgrid._rows().item(0),
                recreateSuperPanel = false,
                defCSS = wijmo.grid.wijgrid.CSS,
                neScrollableContent,
                seScrollableContent;

            wijgrid.mRowOuterHeight = firstRow && firstRow[0]
                ? $(firstRow[0]).outerHeight() // use a real height
                : this.getDefaultRowHeight();

            //clear the marginleft , make sure wijsuperpanel can get the correct width when using the HVirtualScrolling.(#120224)
            if (this.mHVSOffset && this.mHVSOffset > 0) {
                neScrollableContent = $(this._viewTables.ne.element()).closest("." + defCSS.scrollableContent)[0];
                seScrollableContent = $(this._viewTables.se.element()).closest("." + defCSS.scrollableContent)[0];
                neScrollableContent.style.marginLeft = "";
                seScrollableContent.style.marginLeft = "";
            }

            if (!this._scroller.data("wijmo-wijsuperpanel")) {
                recreateSuperPanel = true;

                if (this._wijgrid._allowVirtualScrolling()) {
                    this.vsUI = new wijmo.grid.uiVirtualScroller(wijgrid,
                        this._splitAreas.se, // content to scroll
                        wijgrid.mSplitDistanceY, // fixed area height
                        wijgrid.mRowOuterHeight
                    );
                }

                if (scrollValue && (panelModes.hScrollBarVisibility === "hidden")) { // #61398: workaround for superpanel issue (scrollValue is ignored if scrollbar is hidden).
                    scrollValue.x = 0;
                }

                this._scroller.wijsuperpanel({
                    disabled: wijgrid.options.disabled,
                    scroll: $.proxy(this._onScroll, this),                    
                    scrolling: $.proxy(function () {
                        //For TFS-440410
                        if (this._isNativeSuperPanel()) {
                            this["_isOnScroll"] = true;
                            this._splitAreas.sw.scroll();
                        }
                    }, this),
                    
                    bubbleScrollingEvent: true,
                    customScrolling: this._wijgrid._allowVVirtualScrolling(),
                    vScroller: { scrollBarVisibility: panelModes.vScrollBarVisibility, "scrollValue": scrollValue ? scrollValue.y : null },
                    hScroller: { scrollBarVisibility: panelModes.hScrollBarVisibility, "scrollValue": scrollValue ? scrollValue.x : null },
                    hScrollerActivating: $.proxy(this._onHScrollerActivating, this),
                    animationOptions: this._wijgrid._allowHVirtualScrolling()
                        ? { disabled: true } // disable animation to improve UI response
                        : undefined // use defaults
                });

                // wijsuperpanel.hScrollerActivating event is not raised if touch environment is used, simulate it.
                if (this._isNativeSuperPanel()) {
                    var container = this._getSuperPanelStateContainer();
                    if (container.length && (container[0].offsetHeight !== container[0].clientHeight)) {
                        this._onHScrollerActivating(null, { contentLength: container[0].clientHeight });
                    }
                }

                if (!this._isNativeSuperPanel()) {
                    this._getSuperPanelContentWrapper().bind("scroll", function (e: JQueryEventObject) {
                        // #50496: when a overflowing div contains focusable elements it will be scrolled automatically to fit the focused element into view.
                        // Prevent native scrolling to avoid disalignment of the fixed and unfixed areas in IE\ Chrome when partially visible cell gets focus.
                        (<Element>e.target).scrollLeft = 0;
                        (<Element>e.target).scrollTop = 0;

                        e.preventDefault();
                    });
                }

                //TFS-440410: fix issue 3 - find text (Ctrl+F) in static-area not synchronizing rows positon in Edge/IE.
                if (this._isNativeSuperPanel()) {
                    this._splitAreas.sw.bind("scroll", $.proxy(function (e: JQueryEventObject) {
                        var spInstance = <any>this._getSuperPanel(),
                            content = spInstance.getContentElement(),
                            wrapper = content.parent(),
                            scrollY = this._splitAreas.sw.scrollTop();

                        if (!this["_isOnScroll"]) {
                            if (wrapper.scrollTop() != scrollY)
                                wrapper.scrollTop(scrollY);                            
                        } else if (wrapper.scrollTop() == scrollY)
                            this["_isOnScroll"] = false;

                        if (this._splitAreas.ne.scrollLeft() != wrapper.scrollLeft())
                            this._splitAreas.ne.scrollLeft(wrapper.scrollLeft());

                        e.preventDefault();
                        e.stopPropagation();
                    }, this));

                } else {
                    this._splitAreas.sw.bind("scroll", $.proxy(function (e: JQueryEventObject) {
                        var scrollY = this._splitAreas.sw.scrollTop();
                        var vPos = this._scroller.wijsuperpanel("option", "vScroller").scrollValue;
                        vPos = this._scroller.wijsuperpanel("scrollValueToPx", vPos);

                        if (!this["_isOnScroll"]) {
                            if (scrollY != vPos)
                                this._splitAreas.sw.scrollTop(vPos);
                        } else if (scrollY == vPos)
                            this["_isOnScroll"] = false;

                        e.preventDefault();
                        e.stopPropagation();
                    }, this));
                }
                
                if (this._wijgrid._allowVirtualScrolling()) {
                    this.vsUI.attach(this._scroller);
                }
            }
            else {
                this._scroller.wijsuperpanel("paintPanel");
            }

            if (!this._wijgrid._allowVirtualScrolling()) {
                var totalRowsCount = this._splitAreas.se.find("table").find("tr").length,
                    hScrollbarHeight = this._scroller.find('.wijmo-wijsuperpanel-hbarcontainer').height(),
                    columnHeaderHeight = this._splitAreas.ne.height(),
                    rowHeight = wijgrid.mRowOuterHeight,
                    pagerHeight = (!wijgrid.mBottomPagerDiv ? 0 : wijgrid.mBottomPagerDiv.outerHeight()) +
                        (!wijgrid.mTopPagerDiv ? 0 : wijgrid.mTopPagerDiv.outerHeight()),
                    visibleRowsCount = (this._scroller.height() - columnHeaderHeight - hScrollbarHeight - pagerHeight) / rowHeight,
                    largeChange = Math.floor(visibleRowsCount),
                    smallChange = Math.floor(largeChange / 2),
                    vScroller = this._scroller.wijsuperpanel("option", "vScroller");

                vScroller.scrollLargeChange = largeChange;
                vScroller.scrollSmallChange = smallChange;
                vScroller.scrollMax = totalRowsCount;

                // [299338] restore the vertical scroll position one more time, since the scroll max is changed.
                if (scrollValue && scrollValue.y) {
                    this._scroller.wijsuperpanel({
                        vScroller: { "scrollValue": scrollValue.y }
                    });
                }

                if ($.browser.msie) {
                    this._scroller.wijsuperpanel("option", "keyDownInterval", Math.max(totalRowsCount / 5, 100));
                }
            }

            var needVBar = this._testNeedVBar(wijgrid.mOuterDiv, wijgrid.element, $(this._viewTables.ne.element()), wijgrid._lgGetScrollMode(), wijgrid.mAutoHeight),
                excludeVBarWidth = needVBar && !this._testAutohiddenScrollbars();

            var contentWidth = this.getVisibleContentAreaBounds().width;
            this._splitAreas.ne.width(contentWidth - wijgrid.mSplitDistanceX);

            if (recreateSuperPanel && (scrollValue && scrollValue.x) && this._wijgrid.element.is(":visible")) {
                // synchronize unfixed areas: NE and SE (#47277)
                var hPxValue = this._scroller.wijsuperpanel("scrollValueToPx", scrollValue.x, "h");
                this._setFixedAreaPosition(this._splitAreas.ne, "h", hPxValue, null, true);
            }

            if (wijgrid._lgGetStaticColumnsAlignment() === "right") {
                this._splitAreas.nw.css({
                    "left": "",
                    "right": excludeVBarWidth ? this._verScrollBarSize : 0
                });

                this._splitAreas.sw.css({
                    "left": "",
                    "right": excludeVBarWidth ? this._verScrollBarSize : 0
                });
            }

            this._scroller.find(".wijmo-wijsuperpanel-hbarcontainer, .wijmo-wijsuperpanel-vbarcontainer").css("zIndex", 5);

            if (this._wijgrid._allowVVirtualScrolling()) {
                this._wijgrid._handleVerticalVirtualScrolling(this._bounds.start); // re-render
            }

            //Restore the marginleft.(#120224)
            if (neScrollableContent && seScrollableContent) {
                neScrollableContent.style.marginLeft = this.mHVSOffset + "px";
                seScrollableContent.style.marginLeft = this.mHVSOffset + "px";
            }
        }

        _getLeftOffset(column: c1basefield): number {
            var offset = 0,
                leaves = this._wijgrid._leaves();

            for (var i = 0; i < leaves.length; i++) {
                var leaf = leaves[i];

                if (column.options._leavesIdx <= leaf.options._visLeavesIdx) {
                    break;
                }

                if (leaf._visible()) {
                    offset += leaf.options._realWidth;
                }
            }

            return offset;
        }

        scrollTo(cell: wijmo.grid.cellInfo, callback: () => void, info: ICurrentCellChangedAdditionalInfo) {
            var grid = this._wijgrid,
                virtualRow: number = null,
                virtualColumn: c1basefield = null;

            if (grid._allowVVirtualScrolling()) {
                var rowIndex = grid._renderableRowsRange().getRenderedIndex(cell.rowIndex()),
                    vab: IElementBounds,
                    rowsLen = grid._rows().length(),
                    $rows: JQuery,
                    handleLastRow = (rowIndex - grid._viewPortBounds().start >= rowsLen - 1); // the last row in the current view port

                // determine whether virtual scrolling is needed or not
                if (!cell._isRowRendered() || // !cell._isRendered()
                    (handleLastRow && (vab = this.getVisibleAreaBounds(true)) && ($rows = cell.row().$rows) &&
                        ($rows.offset().top + $rows.outerHeight() > vab.top + vab.height))) { // exceeded wijgrid bounds

                    virtualRow = rowIndex;

                    if (cell.rowIndex() > info.changingEventArgs.oldRowIndex) {
                        virtualRow -= rowsLen - 1;
                        virtualRow += 1;
                    }
                }
            }

            if (grid._allowHVirtualScrolling() && !cell._isRendered()) {
                virtualColumn = cell.columnInst();
            }

            var superPanelObj = this._getSuperPanel();

            if (virtualRow !== null || virtualColumn) {
                info.setFocus = true; // to listen key events
                info.hasFocusedChild = false;
            }

            if (virtualRow !== null) {
                if (superPanelObj) {
                    this.vsUI.scrollToRow(virtualRow, function () { callback(); });
                } else {
                    callback();
                }
            } else {
                var $tableCell = virtualColumn ? null : $(cell.tableCell()),
                    resultLeft: number = null,
                    resultTop: number = null;

                if (superPanelObj && (virtualColumn || $tableCell.is(":visible"))) {
                    var content: JQuery = (<any>superPanelObj).getContentElement(),
                        wrapper = content.parent(),
                        el: IElementBounds, // cell bounds
                        area: IElementBounds, // visible area bounds
                        staticRowIndex = grid._getStaticIndex(true),
                        staticColIndex = grid._getStaticIndex(false),
                        currentRowIndex = cell.rowIndex(),
                        currentCellIndex = cell.cellIndex();

                    if (this._isNativeSuperPanel()) {
                        if (virtualColumn) {  // cell is not rendered
                            el = {
                                left: this._getLeftOffset(virtualColumn), top: 0, height: 0, width: virtualColumn.options._realWidth
                            };
                        } else {
                            el = {
                                left: $tableCell[0].offsetLeft + grid.mSplitDistanceX,
                                top: $tableCell[0].offsetTop,
                                height: $tableCell.outerHeight(),
                                width: $tableCell.outerWidth()
                            };
                        }

                        area = {
                            left: wrapper[0].scrollLeft,
                            top: wrapper[0].scrollTop,
                            height: wrapper[0].clientHeight - grid.mSplitDistanceY,
                            width: wrapper[0].clientWidth - grid.mSplitDistanceX
                        };
                    } else {
                        if (virtualColumn) { // cell is not rendered
                            el = {
                                left: this._getLeftOffset(virtualColumn), top: 0, height: 0, width: virtualColumn.options._realWidth
                            };
                        } else {
                            var elementPosition = $tableCell.position();
                            
                            el = {
                                left: grid._allowHVirtualScrolling() ? this._getLeftOffset(cell.columnInst()) : elementPosition.left + wrapper.scrollLeft(),
                                top: elementPosition.top + (currentCellIndex <= staticColIndex ? this._splitAreas.sw.scrollTop() : 0),
                                height: $tableCell.outerHeight(),
                                width: $tableCell.outerWidth()
                            };
                        }

                        area = {
                            left: -(parseInt((content.css("left") + "").replace("px", ""), 10) || 0),
                            top: -(parseInt((content.css("top") + "").replace("px", ""), 10) || 0),
                            height: wrapper.outerHeight() - grid.mSplitDistanceY,
                            width: wrapper.outerWidth() - grid.mSplitDistanceX
                        };
                    }

                    if (currentRowIndex <= staticRowIndex) {
                        if (currentCellIndex <= staticColIndex) {
                            //resultLeft = 0;
                            //resultTop = 0;
                            resultLeft = null; // fixed column - do not scroll horizontally
                            resultTop = null;  // fixed row - do not scroll vertically
                        } else {
                            el.left += area.left;
                            if (el.left + el.width > area.left + area.width) {
                                area.left = resultLeft = el.left + el.width - area.width;
                            }
                            if (el.left < area.left) {
                                resultLeft = el.left;
                            }
                            //resultTop = 0;
                            resultTop = null; // fixed row - do not scroll vertically
                        }
                    } else {
                        if (currentCellIndex <= staticColIndex) {
                            // elementTop += visibleTop;
                            //el.top += this._splitAreas.sw.scrollTop();
                            
                            if (el.top + el.height > area.top + area.height) {
                                area.top = resultTop = el.top + el.height - area.height;
                            }

                            if (el.top < area.top && el.top + el.height > area.top) {
                                resultTop = el.top;
                            }

                            //resultLeft = 0;
                            resultLeft = null; // fixed column - do not scroll horizontally
                        } else {
                            el.left -= grid.mSplitDistanceX;
                            if (el.top + el.height > area.top + area.height) {
                                area.top = resultTop = el.top + el.height - area.height;
                            }

                            if (el.left + el.width > area.left + area.width) {
                                area.left = resultLeft = el.left + el.width - area.width;
                            }

                            if (el.top < area.top && el.top + el.height > area.top) {
                                resultTop = el.top;
                            }

                            if (el.left < area.left) {
                                resultLeft = el.left;
                            }
                        }
                    }

                    if (resultLeft !== null) {
                        //resultTop = null;

                        if (virtualColumn) {
                            this.vsUI.scrollToColumn(virtualColumn, resultLeft, function () { callback(); });
                            return;
                        } else {
                            (<any>superPanelObj).hScrollTo(resultLeft);
                        }
                    }

                    if (resultTop !== null) {
                        (<any>superPanelObj).vScrollTo(resultTop);
                    }
                }

                callback();
            }
        }

        updateSplits(scrollValue: IScrollingState, rowsToAdjust?: cellRange) {
            var grid = this._wijgrid,
                minWidths: IColumnWidth[] = [],
                maxWidths: IColumnWidth[] = [], // set width to top table th and bottom table td in first row.
                visibleLeaves = grid._visibleLeaves(),
                se = $(this._viewTables.se.element()),
                ne = $(this._viewTables.ne.element()),
                sw = $(this._viewTables.sw.element()),
                nw = $(this._viewTables.nw.element()),
                outerDiv = grid.mOuterDiv,
                hasDataRows = false,
                strictWidth = this._strictWidthMode(),
                live = this.isLiveUpdate(),
                defCSS = wijmo.grid.wijgrid.CSS;

            if (!live) {
                this._destroySuperPanel();

                outerDiv.unbind("mousewheel", this._mouseWheelHandler);

                this._clearNorthTablesHeight();
            }

            // * if there is no data in a table, we must enlarge the table to prevent the width from being 0
            var tBody = <HTMLTableSectionElement>((<HTMLTableElement>se[0]).tBodies && (<HTMLTableElement>se[0]).tBodies[0]);
            if (tBody) {
                for (var i = 0; i < tBody.rows.length; i++) {
                    if (!$(tBody.rows[i]).hasClass(wijmo.grid.wijgrid.CSS.groupHeaderRow)) {
                        hasDataRows = true;
                        break;
                    }
                }

                if (!hasDataRows) {
                    grid.element.css("width", "100%");
                }
            }

            $.each([se, ne, sw, nw], function (index, table) {
                table.css({ "table-layout": "", "width": "" });
            });

            $.each(visibleLeaves, (index, leaf) => {
                if (leaf.options._realWidth !== undefined) { // if any column has width option, we will set the width for inner cells.
                    this._setColumnWidth(leaf, leaf.options._realWidth);
                }

                maxWidths.push(this._getColumnWidth(index));
            });

            $.each([se, ne, sw, nw], function (index, table) {
                table.css({ "width": "1px" });
            });

            $.each(visibleLeaves, (index, leaf) => {
                minWidths.push(this._getColumnWidth(index));
            });

            var viewAreaWidth = outerDiv.width(); // using width() instead of innerWidth() to exclude padding.
            var resultWidths = this._adjustWidthArray(maxWidths, minWidths, viewAreaWidth, strictWidth);
            this._setResultWidth(resultWidths, viewAreaWidth, rowsToAdjust);

            //adjust width if showing vertical scrollbar
            if (this._testNeedVBar(grid.mOuterDiv, se, ne, grid._lgGetScrollMode(), grid.mAutoHeight)) {
                var recalculate = false;

                viewAreaWidth -= this._verScrollBarSize;

                if (strictWidth) {
                    $.each(visibleLeaves, (index: number, leaf: c1basefield) => {
                        var opt = leaf.options;

                        if (wijmo.grid.isPercentage(opt.width)) {
                            maxWidths[index].width = minWidths[index].width = viewAreaWidth * parseFloat(opt.width) / 100;
                            recalculate = true;
                        }
                    });
                } else {
                    recalculate = true;
                }

                if (recalculate) {
                    resultWidths = this._adjustWidthArray(maxWidths, minWidths, viewAreaWidth, strictWidth);
                    this._setResultWidth(resultWidths, viewAreaWidth, rowsToAdjust);
                }
            }

            this._setNorthTablesHeight();

            if (this._wijgrid._allowHVirtualScrolling()) {
                var totalScrollableWidth = this._sumWidthArray(resultWidths); // total width, icluding not rendered columns
                ne.closest("." + defCSS.scrollableContent).width(totalScrollableWidth - grid.mSplitDistanceX);
                se.closest("." + defCSS.scrollableContent).width(totalScrollableWidth - grid.mSplitDistanceX);
            }

            if (!live) {
                this._refreshPanel(scrollValue); // refresh super panel after width is set.
                outerDiv.bind("mousewheel", $.proxy(this._mouseWheelHandler, this));
            }

            this.mResultWidths = resultWidths;
        }

        updateSplitsLive() {
            var leaves = this._wijgrid._leaves(),
                visLeaves = this._wijgrid._visibleLeaves(),
                renderedColumnsWidth = 0;

            this.currentRenderableColumns().forEachIndex((i) => {
                var leaf = leaves[i];

                if (leaf._isRendered()) {
                    var width = this.mResultWidths[leaf.options._visLeavesIdx].width;

                    this._setColumnWidth(leaf, width);

                    renderedColumnsWidth += width;
                }
            });

            var ne = $(this._viewTables.ne.element()),
                se = $(this._viewTables.se.element()),
                last = this.mResultWidths.length - 1;

            this._setTableWidth([ne, se], renderedColumnsWidth, this.mResultWidths[last].width, visLeaves[last]);

            //var hPxValue = this._scroller.wijsuperpanel("scrollValueToPx", this._wijgrid.mScrollingState.x, "h");
            //this._setFixedAreaPosition(this._splitAreas.ne, "h", hPxValue, null, true);
        }

        _setResultWidth(resultWidths: IColumnWidth[], viewAreaWidth: number, rowsToAdjust?: cellRange) {
            var grid = this._wijgrid,
                visLeaves = grid._visibleLeaves(),
                staticColumnIndex = this._staticColumnIndex,
                strictWidth = this._strictWidthMode(),
                expandIndex: number,
                sum: number,
                se = $(this._viewTables.se.element()),
                ne = $(this._viewTables.ne.element()),
                sw = $(this._viewTables.sw.element()),
                nw = $(this._viewTables.nw.element()),
                live = this.isLiveUpdate();

            $.each(resultWidths, (index, colWidth) => {
                if (!colWidth.real || colWidth.realPercent) {
                    this._setColumnWidth(visLeaves[index], colWidth.width);
                }
            });

            if ((expandIndex = staticColumnIndex) >= 0) {
                sum = this._sumWidthArray(resultWidths, 0, expandIndex);
                this._setTableWidth([nw, sw], sum, resultWidths[expandIndex].width, visLeaves[expandIndex]);
            }

            if (!live) {
                //set the size of area after setting the width of column
                try {
                    grid.mSplitDistanceX = staticColumnIndex >= 0 ? nw[0].offsetWidth : 0;
                } catch (ex) { }

                this._updateSplitAreaBounds(0); //width
            }

            if (!strictWidth) {
                this._splitAreas.ne.width(viewAreaWidth - grid.mSplitDistanceX);
            }

            if ((expandIndex = resultWidths.length - 1) >= 0) {
                sum = this._sumWidthArray(resultWidths, staticColumnIndex + 1, expandIndex);
                this._setTableWidth([ne, se], sum, resultWidths[expandIndex].width, visLeaves[expandIndex]);
            }

            if (!live) {
                this._adjustRowsHeights(rowsToAdjust);

                //set the size of area after setting the width of column
                try {
                    grid.mSplitDistanceY = this._staticRowIndex >= 0 ? Math.max(nw[0].offsetHeight, ne[0].offsetHeight) : 0;
                } catch (ex) { }

                this._updateSplitAreaBounds(1); //height
            }
        }

        getInlineTotalWidth(): string {
            if (this._scroller) {
                var stateContainer = this._getSuperPanelStateContainer();

                if (stateContainer.length) {
                    var width = stateContainer[0].style.width;

                    if (width && (width !== "auto")) {
                        return width;
                    }
                }
            }

            return "";
        }

        // public **

        // ** DOMTable abstraction

        _clearBody() {
            super._clearBody();
        }

        bodyRows() {
            var accessor = super.bodyRows();
            return accessor;
        }

        forEachColumnCell(columnIndex: number, callback, param): boolean {
            var joinedTables = this.getJoinedTables(true, columnIndex),
                relIdx, callbackRes;

            if (joinedTables[0] !== null) {
                relIdx = joinedTables[2];
                callbackRes = joinedTables[0].forEachColumnCell(relIdx, callback, param);
                if (callbackRes !== true) {
                    return callbackRes;
                }

                if (joinedTables[1] !== null) {
                    callbackRes = joinedTables[1].forEachColumnCell(relIdx, callback, param);
                    if (callbackRes !== true) {
                        return callbackRes;
                    }
                }
            }

            return true;
        }

        forEachRowCell(rowIndex: number, callback, param): boolean {
            var joinedTables = this.getJoinedTables(false, rowIndex),
                table0 = joinedTables[0],
                table1 = joinedTables[1],
                relIdx, callbackResult;

            if (table0 !== null) {
                relIdx = joinedTables[2];
                if (relIdx < table0.element().rows.length) {
                    callbackResult = table0.forEachRowCell(relIdx, callback, param);
                    if (callbackResult !== true) {
                        return callbackResult;
                    }
                }

                if ((table1 !== null) && (relIdx < table1.element().rows.length)) {
                    callbackResult = table1.forEachRowCell(relIdx, callback, param);
                    if (callbackResult !== true) {
                        return callbackResult;
                    }
                }
            }

            return true;
        }

        getAbsoluteCellInfo(domCell: HTMLTableCellElement, virtualize: boolean): wijmo.grid.cellInfo {
            return new wijmo.grid.cellInfo(this.getAbsoluteCellIndex(domCell), this.getAbsoluteRowIndex(<HTMLTableRowElement>domCell.parentNode), this._wijgrid, true, virtualize);
        }

        getAbsoluteCellIndex(domCell: HTMLTableCellElement): number {
            var index = domCell.cellIndex,
                table = <HTMLTableElement>domCell.parentNode;

            while (table && table.tagName && table.tagName.toLowerCase() !== "table") {
                table = <HTMLTableElement>table.parentNode;
            }

            //order by the hit probability. 
            if (table === this._viewTables.se.element()) {
                index = this._viewTables.se.getColumnIdx(domCell) + this._staticColumnIndex + 1;
                return index;
            }

            if (table === this._viewTables.sw.element()) {
                index = this._viewTables.sw.getColumnIdx(domCell);
                return index;
            }

            if (table === this._viewTables.ne.element()) {
                index = this._viewTables.ne.getColumnIdx(domCell) + this._staticColumnIndex + 1;
                return index;
            }

            if (table === this._viewTables.nw.element()) {
                index = this._viewTables.nw.getColumnIdx(domCell);
                return index;
            }

            return index;
        }

        getAbsoluteRowIndex(domRow: HTMLTableRowElement): number {
            var index = domRow.rowIndex,
                table = <HTMLTableElement>domRow.parentNode;

            while (table && table.tagName && table.tagName.toLowerCase() !== "table") {
                table = <HTMLTableElement>table.parentNode;
            }

            return (table === this._viewTables.nw.element() || table === this._viewTables.ne.element())
                ? index
                : index + this._staticRowIndex + 1;
        }

        getCell(absColIdx: number, absRowIdx: number): HTMLTableCellElement {
            var joinedTablesRow = this.getJoinedTables(false, absRowIdx),
                joinedTablesCol, relRowIdx, relColIdx,
                table: htmlTableAccessor,
                cellIdx;

            if (joinedTablesRow[0] !== null) {
                joinedTablesCol = this.getJoinedTables(true, absColIdx);
                if (joinedTablesCol[0] !== null) {
                    relRowIdx = joinedTablesRow[2];
                    relColIdx = joinedTablesCol[2];

                    table = null;
                    if (joinedTablesRow[1] !== null) {
                        table = (absColIdx === relColIdx) ? joinedTablesRow[0] : joinedTablesRow[1];
                    }
                    else {
                        table = joinedTablesRow[0];
                    }

                    cellIdx = table.getCellIdx(relColIdx, relRowIdx);
                    if (cellIdx >= 0) {
                        return <HTMLTableCellElement>(<HTMLTableRowElement><any>table.element().rows[relRowIdx]).cells[cellIdx];
                    }
                }
            }

            return null;
        }

        getColumnIndex(domCell: HTMLTableCellElement): number {
            var owner = null,
                htmlTable = null,
                flag = false,
                colIdx;

            for (owner = domCell.parentNode; owner.tagName.toLowerCase() !== "table"; owner = owner.parentNode) {
            }

            if (owner !== null) {
                if (owner === this._viewTables.nw.element()) {
                    htmlTable = this._viewTables.nw;
                }
                else {
                    if (owner === this._viewTables.ne.element()) {
                        htmlTable = this._viewTables.ne;
                        flag = true;
                    }
                    else {
                        if (owner === this._viewTables.sw.element()) {
                            htmlTable = this._viewTables.sw;
                        }
                        else {
                            if (owner === this._viewTables.se.element()) {
                                htmlTable = this._viewTables.se;
                                flag = true;
                            }
                        }
                    }
                }

                if (htmlTable !== null) {
                    colIdx = htmlTable.getColumnIdx(domCell);
                    if (flag) {
                        colIdx += this._staticColumnIndex + 1;
                    }

                    return this._wijgrid._renderedLeaves()[colIdx].options._visLeavesIdx;
                    //return colIdx;
                }
            }

            return -1;
        }

        getHeaderCell(absColIdx: number): HTMLTableHeaderCellElement {
            var leaf = this._wijgrid._visibleLeaves()[absColIdx],
                headerRow;

            if (leaf && (headerRow = this._wijgrid._headerRows())) {
                return wijmo.grid.rowAccessor.getCell(headerRow.item(leaf.options._thY), leaf.options._thX);
            }

            return null;
        }

        getJoinedCols(columnIndex: number): HTMLTableColElement[] {
            var leaf = this._wijgrid._visibleLeaves()[columnIndex],
                result = [],
                joinedTables = this.getJoinedTables(true, /*columnIndex*/ leaf.options._renderedIndex),
                relIndex = joinedTables[2];

            joinedTables.splice(joinedTables.length - 1, 1);
            $.each(joinedTables, function (index, table) {
                result.push(table
                    ? $(table.element()).find("col")[relIndex]
                    : null);
            });

            return result;
        }

        getJoinedRows(rowIndex: number, rowScope: wijmo.grid.rowScope): IRowObj {
            var row0 = null, row1 = null,
                table0 = null, table1 = null,
                fixedRowIdx = this._staticRowIndex,
                fixedColIdx = this._staticColumnIndex,
                lastColIdx = this._wijgrid._visibleLeaves().length - 1,
                lastRowIdx = this._rowsCountRaw() - 1,
                allRowsFixed = (fixedRowIdx === lastRowIdx),
                allsRowUnfixed = (fixedRowIdx < 0),
                rowsFixedSlice = !allRowsFixed && !allsRowUnfixed,
                sectionLength = 0;

            if (allRowsFixed || rowsFixedSlice) {
                if (fixedColIdx >= 0 && fixedColIdx < lastColIdx) {
                    table0 = this._viewTables.nw;
                    table1 = this._viewTables.ne;
                }
                else {
                    table0 = (fixedColIdx < 0) ? this._viewTables.ne : this._viewTables.nw;
                }
                sectionLength = table0.getSectionLength(rowScope);
                if (rowIndex < sectionLength) {
                    row0 = table0.getSectionRow(rowIndex, rowScope);
                    if (table1 !== null) {
                        row1 = table1.getSectionRow(rowIndex, rowScope);
                    }
                }
            }

            if (allsRowUnfixed || (rowsFixedSlice && (row0 === null))) {
                if (!allsRowUnfixed) {
                    rowIndex -= sectionLength;
                }

                if (fixedColIdx >= 0 && fixedColIdx < lastColIdx) {
                    table0 = this._viewTables.sw;
                    table1 = this._viewTables.se;
                }
                else {
                    table0 = (fixedColIdx < 0) ? this._viewTables.se : this._viewTables.sw;
                }

                row0 = table0.getSectionRow(rowIndex, rowScope);

                if (table1 !== null) {
                    row1 = table1.getSectionRow(rowIndex, rowScope);
                }
            }

            return (row0 === null && row1 === null) ? null : <wijmo.grid.IRowObj><any>[row0, row1];
        }

        getJoinedTables(byColumn: boolean, index: number): any[] {
            var t0 = null,
                t1 = null,
                idx = index,
                wijgrid = this._wijgrid,
                fixedRowIdx = this._staticRowIndex,
                fixedColIdx = this._staticColumnIndex;

            if (byColumn) {
                if (index <= fixedColIdx) {
                    t0 = this._viewTables.nw;
                    t1 = this._viewTables.sw;
                }
                else {
                    t0 = this._viewTables.ne;
                    t1 = this._viewTables.se;

                    idx = idx - (fixedColIdx + 1);
                }

                if (fixedRowIdx < 0) {
                    t0 = null;
                }

                if (fixedRowIdx === this._rowsCountRaw() - 1) // fixed row is the last row
                {
                    t1 = null;
                }
            }
            else {
                if (index <= fixedRowIdx) {
                    t0 = this._viewTables.nw;
                    t1 = this._viewTables.ne;
                }
                else {
                    t0 = this._viewTables.sw;
                    t1 = this._viewTables.se;

                    idx = idx - (fixedRowIdx + 1);
                }

                if (fixedColIdx < 0) {
                    t0 = null;
                }
                if (fixedColIdx === wijgrid._leaves().length - 1) {
                    t1 = null;
                }
            }

            if (t0 === null) {
                t0 = t1;
                t1 = null;
            }
            return [t0, t1, idx];
        }

        subTables(): wijmo.grid.htmlTableAccessor[] {
            return [this._viewTables.nw, this._viewTables.ne, this._viewTables.sw, this._viewTables.se];
        }

        // DOMTable abstraction **

        // ** private abstract

        _getSuperPanel(): JQueryUIWidget {
            return this._scroller
                ? this._scroller.data("wijmo-wijsuperpanel")
                : null;
        }

        // ** render
        _ensureRenderHBounds() {
            if (this._wijgrid._allowHVirtualScrolling()) {
                var renderableColumns = this._wijgrid._renderableColumnsRange();

                // update global range and calculate left offset
                this.mHVSOffset = this._getRenderableColumnsBounds(renderableColumns, this._wijgrid.mScrollingState.scrollLeft);

                // set current range
                if (this.isLiveUpdate()) {
                    var currentRenderable = new renderableColumnsCollection();
                    currentRenderable.add(renderableColumns.item(1)); // scrollable (horizontal) area only, ne+se
                    this.currentRenderableColumns(currentRenderable);
                } else {
                    this.currentRenderableColumns(renderableColumns);
                }
            } else {
                super._ensureRenderHBounds(); // render all columns
            }
        }

        _getRenderableColumnsBounds(range: renderableColumnsCollection, offset: number): number {
            var staticLeafIdx = this._staticAllColumnIndex,
                fix: IRenderBounds = { start: -1, end: -1 }, // fixed columns
                scroll: IRenderBounds = { start: -1, end: -1 }; // scrollable columns

            range.clear();
            range.add(fix);
            range.add(scroll);

            if (staticLeafIdx >= 0) {
                fix.start = 0;
                fix.end = staticLeafIdx;
            }

            var leaves = this._wijgrid._leaves(),
                startIdxFound = false,
                offsetLeft = 0, // Total width of the completely invsible columns (from the left side)
                scrollableAreaWidth = this.getVisibleAreaBounds(true).width,
                colTotalWidth = 0;

            if (staticLeafIdx >= 0) {
                for (var i = 0; i <= staticLeafIdx; i++) {
                    var leaf = leaves[i];

                    if (leaf._visible()) {
                        scrollableAreaWidth -= leaf.options._realWidth;
                    }
                }
            }

            for (var i = staticLeafIdx + 1; i < leaves.length; i++) {
                var leaf = leaves[i];

                if (leaf._visible()) {
                    var width = leaves[i].options._realWidth;

                    colTotalWidth += width;

                    if (colTotalWidth <= offset) {
                        offsetLeft = colTotalWidth;
                    }

                    if (!startIdxFound) {
                        if (colTotalWidth - offset > 0) {
                            scroll.start = i;
                            startIdxFound = true;
                        }
                    } else {
                        //if (colTotalWidth - offset >= scrollableAreaWidth) { // overflow, stop
                        //	scroll.end = i;
                        //	break;
                        //} 
                        scroll.end = i;
                        if (colTotalWidth - offset >= scrollableAreaWidth) { // overflow, stop
                            break;
                        }
                    }
                }
            }

            if (scroll.end < scroll.start) {
                scroll.end = scroll.start;
            }

            return offsetLeft;
        }

        _ensureRenderBounds() {
            if (this._wijgrid._allowVVirtualScrolling()) {
                var virtualPageSize = this.getVirtualPageSize(),
                    b = this._bounds;

                this._wijgrid._ensureRenderableBounds(b);

                b.end = b.start + virtualPageSize - 1;

                if (this._wijgrid._serverSideVirtualScrolling()) {
                    var delta = (b.end - b.start + 1) - this._wijgrid.mSketchTable.count();

                    if (delta > 0) {
                        b.end -= delta;
                    }
                } else {
                    if (this._wijgrid.mRenderCounter == 0) {
                        b.end = b.start; // render a single row, just to calculate a real height.
                    } else {
                        this._wijgrid._ensureRenderableBounds(b);

                        if (this._isLastItemRendered()) {
                            virtualPageSize = this.getVirtualPageSize(false); // use floor, don't overlap a visible area
                        }

                        var num = Math.max(Math.min(this._wijgrid._renderableRowsCount(), virtualPageSize), 0), // how much rows can be rendered actually
                            delta = num - (b.end - b.start + 1);

                        if (delta > 0) {
                            b.start = b.start - delta;
                        }
                    }
                }

                this._wijgrid._ensureRenderableBounds(b);

                if (this._isLastItemRendered()) { // the very last items will be rendered
                    if (this._canRenderMoreItems()) {
                        virtualPageSize = this.getVirtualPageSize(false); // use floor, don't overlap a visible area
                        b.start = b.end - virtualPageSize + 1; // adjust the start position to stick the items to the bottom
                    }

                    this._wijgrid._ensureRenderableBounds(b);

                    if (b.start !== this._wijgrid.mScrollingState.index) { // the bounds.start was changed, synchronize with the scrollingState
                        this._wijgrid.mScrollingState.index = this._wijgrid.mScrollingState.y = b.start; // 
                    }
                }
            } else {
                super._ensureRenderBounds(); // render all items
            }
        }

        _isLastItemRendered(): boolean {
            return (this._bounds.start > 0) && (this._bounds.end === this._wijgrid.mSketchTable.count() - 1);
        }

        _canRenderMoreItems(): boolean {
            var virtualPageSize = this.getVirtualPageSize(false), // use floor, don't overlap a visible area
                itemsToRender = this._bounds.end - this._bounds.start + 1;

            return itemsToRender < virtualPageSize;
        }

        _preRender() {
            super._preRender();

            var defCSS = wijmo.grid.wijgrid.CSS;

            if (this.isLiveUpdate()) {
                // clear scrollable (horizontal) sections
                this._viewTables.ne.clearContent();
                this._viewTables.se.clearContent();

                // set offset to wijmo-wijgrid-scrollable-content elements.
                $(this._viewTables.ne.element()).closest("." + defCSS.scrollableContent)[0].style.marginLeft = this.mHVSOffset + "px";
                $(this._viewTables.se.element()).closest("." + defCSS.scrollableContent)[0].style.marginLeft = this.mHVSOffset + "px";
            } else {
                var docFragment = document.createDocumentFragment(),
                    HTA = wijmo.grid.htmlTableAccessor,
                    wijCSS = this._wijgrid.options.wijCSS;

                this._wijgrid.mOuterDiv.wrapInner("<div class=\"" + defCSS.fixedView + " " + wijCSS.wijgridFixedView + "\"><div class=\"" + defCSS.scroller + " " + wijCSS.wijgridScroller + "\"><div class=\"wijmo-wijgrid-split-area-se wijmo-wijgrid-content-area\"></div></div></div>");
                this._scroller = this._wijgrid.mOuterDiv.find("." + defCSS.scroller);
                this._scroller.css("padding", 0); // disable padding (inherited)

                this._scroller.after(this._splitAreas.nw = $("<div class=\"wijmo-wijgrid-split-area wijmo-wijgrid-split-area-nw\" style=\"overflow:hidden;position:absolute;z-index:4;top:0px;left:0px;\"></div>"));
                this._scroller.after(this._splitAreas.ne = $("<div class=\"wijmo-wijgrid-split-area wijmo-wijgrid-split-area-ne\" style=\"overflow:hidden;position:absolute;z-index:4;top:0px;left:0px;\"></div>"));
                this._scroller.after(this._splitAreas.sw = $("<div class=\"wijmo-wijgrid-split-area wijmo-wijgrid-split-area-sw\" style=\"overflow:hidden;position:absolute;z-index:4;top:0px;left:0px;\"></div>"));
                this._splitAreas.se = this._scroller.find(".wijmo-wijgrid-split-area-se:first");

                this._viewTables = { // skip offsets, ensure tbody + colgroup
                    nw: new HTA(<HTMLTableElement>docFragment.appendChild(document.createElement("table")), true, true, true),
                    ne: new HTA(<HTMLTableElement>docFragment.appendChild(document.createElement("table")), true, true, true),
                    sw: new HTA(<HTMLTableElement>docFragment.appendChild(document.createElement("table")), true, true, true),
                    se: new HTA(<HTMLTableElement>docFragment.appendChild(this._wijgrid.element[0]), true, true, true)
                }

                var vT = this._viewTables, sw = vT.sw.element(), se = vT.se.element(),
                    nw = vT.nw.element(), ne = vT.ne.element(),
                    allowVirtualHScrolling = this._wijgrid._allowHVirtualScrolling(),
                    defCSS = wijmo.grid.wijgrid.CSS;
                this._splitAreas.nw.empty().append(nw);
                if (allowVirtualHScrolling) {
                    this._splitAreas.ne.empty().append($("<div class=\"" + defCSS.scrollableContent + "\"></div>").append(ne));
                } else {
                    this._splitAreas.ne.empty().append(ne);
                }
                this._splitAreas.sw.empty().append(sw);
                if (allowVirtualHScrolling) {
                    this._splitAreas.se.empty().append($("<div class=\"" + defCSS.scrollableContent + "\"></div>").append(se));
                } else {
                    this._splitAreas.se.empty().append(se);
                }
            }
        }

        _postRender() {
            var t10 = this._viewTables.sw.element(),
                t11 = this._viewTables.se.element(),
                leftRows = <HTMLTableRowElement[]><any>t10.rows,
                rightRows = <HTMLTableRowElement[]><any>t11.rows;

            if (!this.isLiveUpdate()) {
                var t00 = this._viewTables.nw.element(),
                    t01 = this._viewTables.ne.element(),
                    HTA = wijmo.grid.htmlTableAccessor,
                    allowVirtualHScrolling = this._wijgrid._allowHVirtualScrolling(),
                    defCSS = wijmo.grid.wijgrid.CSS,
                    self = this;

                this._viewTables = { // rebuild with offsets
                    nw: new HTA(t00),
                    ne: new HTA(t01),
                    sw: new HTA(t10),
                    se: new HTA(t11)
                };

                $.each(this._viewTables, function (idx, hta) {
                    var $element = $(hta.element());

                    self._wijgrid._setAttr($element, {
                        role: "grid",
                        border: "0",
                        cellpadding: "0",
                        cellspacing: "0"
                    });

                    $element
                        .addClass(wijmo.grid.wijgrid.CSS.table).addClass(self._wijgrid.options.wijCSS.wijgridTable)
                        .css("border-collapse", "separate") // use separate instead of collapse to avoid a disalignment issue in chrome.
                        .find("> tbody")
                        .addClass(self._wijgrid.options.wijCSS.content);
                });

                this._setNorthTablesHeight();
            } else {
                this._rebuildOffsets();
            }

            if (leftRows != null && rightRows != null && leftRows.length > 0 && rightRows.length > 0 &&
                leftRows[0].offsetHeight !== rightRows[0].offsetHeight) {
                this._adjustRowsHeights();
            }

            super._postRender();
        }


        _setNorthTablesHeight(): void {
            $(this._viewTables.ne.element()).height("100%");
            $(this._viewTables.nw.element()).height("100%");
        }

        _clearNorthTablesHeight(clearRows = false): void {
            var tables = $([this._viewTables.ne.element(), this._viewTables.nw.element()]);

            tables.height("");

            if (clearRows) {
                tables.find("> thead > tr").height("");
            }
        }

        _rowsCountRaw() {
            var t00 = this._viewTables.nw.element(),
                t01 = this._viewTables.ne.element(),
                t10 = this._viewTables.sw.element(),
                t11 = this._viewTables.se.element(),
                res;

            res = Math.max(t00.rows.length, t01.rows.length) + Math.max(t10.rows.length, t11.rows.length);

            return res;
        }

        _createCol(column: c1basefield, visibleIdx: number): HTMLTableColElement[] {
            return [
                <HTMLTableColElement>document.createElement("col"),
                <HTMLTableColElement>document.createElement("col")
            ];
        }

        _appendCol(domCol: HTMLTableColElement[], column: c1basefield, visibleIdx: number) {
            if (visibleIdx <= this._staticColumnIndex) {
                this._viewTables.nw.appendCol(domCol[0]);
                this._viewTables.sw.appendCol(domCol[1]);
            } else {
                this._viewTables.ne.appendCol(domCol[0]);
                this._viewTables.se.appendCol(domCol[1]);
            }
        }

        _insertRow(rowType: wijmo.grid.rowType, sectionRowIndex: number, domRow?: HTMLTableRowElement /* optional, used by c1gridview to clone rows of the original table */)
            : HTMLTableRowElement[] {
            var $rt = wijmo.grid.rowType,
                leftSection, rightSection,
                vt = this._viewTables;

            switch (rowType) {
                case $rt.header:
                case $rt.filter:
                    leftSection = vt.nw.ensureTHead();
                    rightSection = vt.ne.ensureTHead();
                    break;

                case $rt.footer:
                    leftSection = vt.sw.ensureTFoot();
                    rightSection = vt.se.ensureTFoot();
                    break;

                default: // tbody
                    if (sectionRowIndex <= this._staticDataRowIndex) {
                        leftSection = vt.nw.ensureTBody();
                        rightSection = vt.ne.ensureTBody();
                    } else {
                        sectionRowIndex -= this._staticDataRowIndex + 1;  // subtracts fixed offset
                        leftSection = vt.sw.ensureTBody();
                        rightSection = vt.se.ensureTBody();
                    }
            }

            if (domRow) {
                // append only
                return [
                    leftSection.appendChild(domRow),
                    rightSection.appendChild(domRow.cloneNode(false))
                ];
            } else {
                return [
                    leftSection.insertRow(sectionRowIndex > leftSection.rows.length ? -1 : sectionRowIndex),
                    rightSection.insertRow(sectionRowIndex > rightSection.rows.length ? -1 : sectionRowIndex)
                ];
            }
        }

        _rowRendered(rowInfo: IRowInfo, rowAttr, rowStyle) {
            var leftRow = <HTMLTableRowElement>rowInfo.$rows[0],
                rightRow = <HTMLTableRowElement>rowInfo.$rows[1],
                liveUpdate = this.isLiveUpdate();


            // Do not remove empty rows from header. The number of header rows in the fixed and unfixed tables should be the same to handle unbanded columns headers correctly when the staticSolumnIndex option is used:
            //
            // row0 |   band  | |  col2  | (rowSpan = 2)
            //      |---------| |--------|
            // row1 |col0|col1| |        | <- empty row

            if (!leftRow.cells.length && (this._isBodyRow(rowInfo) || liveUpdate)) {
                leftRow.parentNode.removeChild(leftRow);
                leftRow = null;
            }

            if (!rightRow.cells.length && this._isBodyRow(rowInfo)) {
                rightRow.parentNode.removeChild(rightRow);
                rightRow = null;
            }

            if (leftRow || rightRow) {
                if (!leftRow || !rightRow) { // handle changes
                    rowInfo.$rows = leftRow
                        ? $(leftRow)
                        : $(rightRow);
                }

                if (this.mRowsHeights.length) { // horizontal virtual scrolling + static columns
                    // use results of the previous _adjustRowsHeights' call here instead of calling the _adjustRowsHeights itself later (to improve performance).
                    var globIdx = rowInfo.sectionRowIndex;

                    if (rowInfo.type === rowType.footer) {
                        globIdx = this.mRowsHeights.length - 1;
                    } else if (!(rowInfo.type === rowType.header || rowInfo.type === rowType.filter)) { // not header
                        globIdx += this._wijgrid._columnsHeadersTable().length; // skip header rows
                        globIdx += this._wijgrid.options.showFilter ? 1 : 0;
                    }

                    if (this.mRowsHeights[globIdx]) {
                        this._setRowHeight(<IRowObj><any>rowInfo.$rows, this.mRowsHeights[globIdx]);
                    }
                }

                super._rowRendered(rowInfo, rowAttr, rowStyle);
            }
        }

        _appendCell(rowInfo: IRowInfo, cellIndex: number, $cell: JQuery) {
            if (cellIndex <= this._staticAllColumnIndex) {
                rowInfo.$rows[0].appendChild($cell[0]);
            } else {
                rowInfo.$rows[1].appendChild($cell[0]);
            }
        }

        // render **
        _getRowHeight(rowObj: IRowObj, ignoreSpannedCells?: boolean) {
            if (rowObj[0] && rowObj[1]) { // static columns are used
                var lRow = rowObj[0],
                    rRow = rowObj[1],
                    $lRow = $(lRow),
                    $rRow = $(rRow),
                    lRowH, rRowH,
                    customHeight,
                    getRowHeightUsingUnspannedCells = function ($row: JQuery) {
                        var i: number,
                            domRow = <HTMLTableRowElement>$row[0],
                            domCell: HTMLTableCellElement;

                        for (i = 0; i < domRow.cells.length; i++) {
                            domCell = <HTMLTableCellElement>domRow.cells[i];

                            if (!domCell.rowSpan || domCell.rowSpan === 1) {
                                return $(domCell).outerHeight();
                            }
                        };

                        return $row.height();
                    };

                if (customHeight = $.data(lRow, "customHeight")) { // user-defined (inline) height
                    lRowH = rRowH = parseInt(customHeight);
                } else {
                    $lRow.css("height", "");
                    $rRow.css("height", "");

                    if (ignoreSpannedCells) { // used for header rows
                        lRowH = getRowHeightUsingUnspannedCells($lRow);
                        rRowH = getRowHeightUsingUnspannedCells($rRow);
                    } else {
                        lRowH = $lRow.height();
                        rRowH = $rRow.height();
                    }
                }

                return Math.max(lRowH, rRowH);
            }

            return null;
        }

        _setRowHeight(rowObj: IRowObj, maxHeight: number) {
            var isLiveUpdate = this.isLiveUpdate();

            if (rowObj[0] && (maxHeight || maxHeight === 0)) {
                if (rowObj[1] || isLiveUpdate) { // if not isLiveUpdate then set height only if static columns are used
                    var row = wijmo.grid.rowObjToJQuery(rowObj);

                    maxHeight += 3;

                    for (var i = 0; i < row.length; i++) {
                        var el = $(row[i]);

                        el.height(maxHeight);

                        if (!isLiveUpdate) { // in liveUpdate mode a single row can stretch over the whole table and its height will be abnormal large, so skip it.
                            var dif = maxHeight - el.height();
                            if (dif) {
                                el.height(maxHeight + dif);
                            }
                        }
                    }
                }
            }
        }

        _adjustRowHeight(bodyRowsToAdjust?: cellRange) {
            var wijgrid = this._wijgrid,
                fixedColIdx = this._staticColumnIndex,
                lastColIdx = wijgrid._visibleLeaves().length - 1;

            this.resetRowsHeightsCache();

            // setting row height only if grid is divided into leftern and rightern parts
            if (fixedColIdx > -1 && fixedColIdx < lastColIdx) {
                var tableNE = this._viewTables.ne.element(),
                    tableNEParent = <HTMLDivElement>tableNE.parentNode,
                    talbeNEParentScrollLeft: number,
                    tableNW = this._viewTables.nw.element(),
                    tableNWParent = <HTMLDivElement>tableNW.parentNode,
                    tableSE = this._viewTables.se.element(),
                    tableSEParent = <HTMLDivElement>tableSE.parentNode,
                    tableSW = this._viewTables.sw.element(),
                    tableSWParent = <HTMLDivElement>tableSW.parentNode,
                    leftRows: HTMLTableRowElement[],
                    rightRows: HTMLTableRowElement[],
                    fixedRowIdx = this._staticRowIndex,
                    lastRowIdx = this._rowsCountRaw() - 1;

                var totalRows = Math.max(Math.max(tableNE.rows.length, tableSE.rows.length), Math.max(tableNW.rows.length, tableSW.rows.length));

                if (bodyRowsToAdjust && (bodyRowsToAdjust.r2 - bodyRowsToAdjust.r1 + 1 > totalRows * 0.9)) {
                    bodyRowsToAdjust = null; // adjust all rows if bodyRowsToAdjust takes over 90% of all of the rows (to reduce overhead of getting joined rows).
                }

                if (bodyRowsToAdjust) { // adjust specified rows only
                    for (i = bodyRowsToAdjust.r1; i <= bodyRowsToAdjust.r2; i++) {
                        this.mRowsHeights.push(this._getRowHeight(this.getJoinedRows(i, rowScope.body)));
                    }
                } else { // adjust all rows
                    // getting the height of northern tables
                    if (fixedRowIdx > -1 && fixedRowIdx <= lastRowIdx) {
                        leftRows = <HTMLTableRowElement[]><any>tableNW.rows;
                        rightRows = <HTMLTableRowElement[]><any>tableNE.rows;

                        for (var i = 0, len = leftRows.length; i < len; i++) {
                            this.mRowsHeights.push(this._getRowHeight(<wijmo.grid.IRowObj><any>[leftRows[i], rightRows[i]], true)); // row height will be calculated using unspanned cells (TFS issue #33399).
                        }
                    }

                    // getting the height of southern tables
                    if (fixedRowIdx >= -1 && fixedRowIdx < lastRowIdx) {
                        leftRows = <HTMLTableRowElement[]><any>tableSW.rows;
                        rightRows = <HTMLTableRowElement[]><any>tableSE.rows;

                        for (var i = 0, len = leftRows.length; i < len; i++) {
                            this.mRowsHeights.push(this._getRowHeight(<wijmo.grid.IRowObj><any>[leftRows[i], rightRows[i]]));
                        }
                    }
                }

                // removing elments from dom to improve performance
                if (fixedRowIdx > -1 && fixedRowIdx <= lastRowIdx) {
                    tableNWParent.removeChild(tableNW);
                    talbeNEParentScrollLeft = tableNEParent.scrollLeft; // store value, because property will be reset on child collection changes
                    tableNEParent.style.visibility = "hidden"; // to avoid flickering in IE when scrollLeft property will be restored.
                    tableNEParent.removeChild(tableNE);
                }
                if (fixedRowIdx >= -1 && fixedRowIdx < lastRowIdx) {
                    tableSWParent.removeChild(tableSW);
                    tableSEParent.removeChild(tableSE);
                }

                if (bodyRowsToAdjust) { // adjust specified rows only
                    var j = 0;

                    for (var i = bodyRowsToAdjust.r1; i <= bodyRowsToAdjust.r2; i++ , j++) {
                        this._setRowHeight(this.getJoinedRows(i, rowScope.body), this.mRowsHeights[j]);
                    }
                } else { // adjust all rows
                    var j = 0;

                    // setting the height of northern tables
                    if (fixedRowIdx > -1 && fixedRowIdx <= lastRowIdx) {
                        leftRows = <HTMLTableRowElement[]><any>tableNW.rows;
                        rightRows = <HTMLTableRowElement[]><any>tableNE.rows;

                        for (var i = 0, len = leftRows.length, j = 0; i < len; i++ , j++) {
                            this._setRowHeight(<wijmo.grid.IRowObj><any>[leftRows[i], rightRows[i]], this.mRowsHeights[j]);
                        }
                    }
                    // setting the height of southern tables
                    if (fixedRowIdx >= -1 && fixedRowIdx < lastRowIdx) {
                        leftRows = <HTMLTableRowElement[]><any>tableSW.rows;
                        rightRows = <HTMLTableRowElement[]><any>tableSE.rows;

                        for (var i = 0, len = leftRows.length; i < len; i++) {
                            this._setRowHeight(<wijmo.grid.IRowObj><any>[leftRows[i], rightRows[i]], this.mRowsHeights[j++]);
                        }
                    }
                }

                // adding elments back to dom to improve performance
                if (fixedRowIdx > -1 && fixedRowIdx <= lastRowIdx) {
                    tableNWParent.appendChild(tableNW);
                    tableNEParent.appendChild(tableNE);

                    if (talbeNEParentScrollLeft) {
                        tableNEParent.scrollLeft = talbeNEParentScrollLeft;
                    }

                    tableNEParent.style.visibility = "visible";
                }

                if (fixedRowIdx >= -1 && fixedRowIdx < lastRowIdx) {
                    tableSWParent.appendChild(tableSW);
                    tableSEParent.appendChild(tableSE);
                }
            }
        }

        // private abstract **

        // ** private specific

        _adjustRowsHeights(rowsToAdjust?: cellRange) {
            var $tableSW = $(this._viewTables.sw.element()),
                $tableSE = $(this._viewTables.se.element()),
                height;

            $tableSE.css("height", "");
            $tableSW.css("height", "");

            this._adjustRowHeight(rowsToAdjust);

            height = Math.max($tableSE.height(), $tableSW.height());

            $tableSW.height(height);
            $tableSE.height(height);
        }

        _destroySuperPanel() {
            if (this._scroller.data("wijmo-wijsuperpanel")) {
                if (this.vsUI) {
                    this.vsUI.dispose();
                }

                this._superPanelElementsCache = <any>{};
                this._scroller.wijsuperpanel("destroy");
            }
        }

        _onScroll(e: JQueryEventObject, data: { dir: string; position: number; animationOptions: any; }) {
            var spInstance = this._getSuperPanel();

            //For TFS-440410
            if (data.animationOptions)
                this["_isOnScroll"] = true;
            
            if (this._wijgrid._allowVVirtualScrolling()) {
                if (data.dir === "h") {
                    // do horizontal scrolling
                    this._setFixedAreaPosition((<any>spInstance).getContentElement(), data.dir, data.position, data.animationOptions, false);
                    this._setFixedAreaPosition(this._splitAreas.ne, data.dir, data.position, data.animationOptions, true);
                }
            } else {
                this._setFixedAreaPosition(data.dir === "h"
                    ? this._splitAreas.ne
                    : this._splitAreas.sw,
                    data.dir, data.position, data.animationOptions, true);
                //For TFS-440410
                if (!this._isNativeSuperPanel())
                    this._splitAreas.sw.scroll();
                
            }

            spInstance && spInstance.options && this._wijgrid._trackScrollingPosition(spInstance.options.hScroller.scrollValue, spInstance.options.vScroller.scrollValue, data.dir === "h" ? data.position : null);

        }

        _onHScrollerActivating(e, args: { contentLength: number; }) {
            this._splitAreas.sw.height(args.contentLength - this._wijgrid.mSplitDistanceY);

            // auto adjusting height with hscrollbar shown
            if (this._wijgrid.mAutoHeight) {
                var diff = this._wijgrid.element.height() + this._wijgrid.mSplitDistanceY - args.contentLength;
                if (diff > 0) {
                    this._scroller.height(this._scroller.height() + diff);
                    this._scroller.wijsuperpanel("paintPanel");
                    return false;
                }
            }
        }

        _onMouseWheel(e: JQueryEventObject, delta: number) {
            // force superpanel to do scrolling when cursor is placed over then non-scrollable (fixed) areas of the wijgrid.
            
            var bounds,
                dir = (delta > 0) ? "top" : "bottom",
                isOverFixedArea = false,
                vPos;

            if (this._wijgrid._canInteract()) {
                bounds = this.getFixedAreaVisibleBounds(); // an array (horizonta area, verticalw area)

                $.each(bounds, function (i, o) {
                    if (o && wijmo.grid.isOver(e.pageY, e.pageX, o.top, o.left, o.height, o.width)) {
                        isOverFixedArea = true;
                        return false; // break
                    }
                });

                if (isOverFixedArea && this._scroller.data("wijmo-wijsuperpanel")) {
                    vPos = this._scroller.wijsuperpanel("option", "vScroller").scrollValue;
                    
                    this._scroller.wijsuperpanel("doScrolling", dir);
                    
                    // simulate wijsuperpanel behaviour: prevent window scrolling until superpanel is not scrolled to the end.
                    if (vPos !== this._scroller.wijsuperpanel("option", "vScroller").scrollValue) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }
            }
        }

        _setFixedAreaPosition(element: JQuery, direction: string, position: number, animation: any, useScrollProp: boolean) {
            var prop = {},
                key;

            if (direction === "h") {
                key = useScrollProp ? "scrollLeft" : "left";
            } else {
                key = useScrollProp ? "scrollTop" : "top";
            }

            if (!useScrollProp) {
                position = -position; // invert
            }

            if (animation) {
                prop[key] = position;
                element.animate(prop, animation);
            } else {
                if (useScrollProp) {
                    element[0][key] = position;
                } else {
                    element.css(key, position);
                }
            }
        }

        _testNeedVBar(outerDiv, gridElement, tableNE, mode, autoHeight) {
            var excludeVBarWidth = false,
                wijgrid = this._wijgrid,
                gridWidth = tableNE.width() + wijgrid.mSplitDistanceX,
                gridHeight = gridElement.height() + wijgrid.mSplitDistanceY,
                outerWidth = outerDiv.width(),
                outerHeight = outerDiv.height(),
                contentHeight, topHeight = 0, bottomHeight = 0;

            if (wijgrid.mSuperPanelHeader !== null) {
                topHeight = wijgrid.mSuperPanelHeader.outerHeight(true);
            }

            if (wijgrid.mBottomPagerDiv !== null) {
                bottomHeight = wijgrid.mBottomPagerDiv.outerHeight(true);
            }

            contentHeight = outerHeight - topHeight - bottomHeight;

            switch (mode) {
                case "both":
                case "vertical":
                    excludeVBarWidth = true;
                    break;

                case "auto":
                    // When the height needs to be auto adjusted, the vertical scrollbar should not be shown
                    excludeVBarWidth = (gridHeight > contentHeight) || (!autoHeight && gridWidth > outerWidth && gridHeight > contentHeight - this._verScrollBarSize);

                    if (!excludeVBarWidth && this._wijgrid._allowVVirtualScrolling()) { // test virtual scrolling bounds (#45894)
                        var itemsToRender = (<fixedView>wijgrid._view()).getVirtualPageSize();
                        //itemsToRender = this._bounds.end - this._bounds.start;

                        excludeVBarWidth = (itemsToRender > 0 && itemsToRender < wijgrid._totalRowsCount());
                    }

                    break;
            }

            return excludeVBarWidth;
        }

        //bSet: 0-width, 1-height, 2-all
        _updateSplitAreaBounds(bSet) {
            var wijgrid = this._wijgrid,
                controlHeight, contentHeight, topHeight = 0, bottomHeight = 0, controlRect;

            if (bSet === 0 || bSet === 2) {
                this._splitAreas.nw.width(wijgrid.mSplitDistanceX);
                this._splitAreas.sw.width(wijgrid.mSplitDistanceX);

                if (wijgrid._lgGetStaticColumnsAlignment() === "right") {
                    this._splitAreas.se.css("marginRight", wijgrid.mSplitDistanceX);
                    this._splitAreas.ne.css("marginRight", wijgrid.mSplitDistanceX);
                } else {
                    this._splitAreas.se.css("marginLeft", wijgrid.mSplitDistanceX);
                    this._splitAreas.ne.css("marginLeft", wijgrid.mSplitDistanceX);
                }
            }

            if (bSet === 1 || bSet === 2) {
                this._scroller.css("height", "");
                this._splitAreas.se.css("marginTop", 0);

                controlRect = wijgrid.mOuterDiv[0].getBoundingClientRect();
                controlHeight = Math.ceil(controlRect.height ||
                    (controlRect.bottom - controlRect.top));

                if (!wijgrid.mAutoHeight) {
                    this._scroller.height(controlHeight);
                }
                else {
                    // no height is set for outer div, we need to expand the grid.
                    this._scroller.height(controlHeight + wijgrid.mSplitDistanceY);
                    //this._noHeight = true;
                }

                this._splitAreas.nw.height(wijgrid.mSplitDistanceY);
                this._splitAreas.ne.height(wijgrid.mSplitDistanceY);

                if (wijgrid.mSuperPanelHeader !== null) {
                    topHeight = wijgrid.mSuperPanelHeader.outerHeight(true);
                }
                if (wijgrid.mBottomPagerDiv !== null) {
                    bottomHeight = wijgrid.mBottomPagerDiv.outerHeight(true);
                }
                contentHeight = controlHeight - topHeight - bottomHeight;

                if (wijgrid.mSuperPanelHeader !== null) {
                    this._splitAreas.nw.css("top", topHeight + "px");
                    this._splitAreas.ne.css("top", topHeight + "px");
                }

                if (!wijgrid.mAutoHeight) {
                    this._splitAreas.sw.height(contentHeight - wijgrid.mSplitDistanceY);
                }
                else {
                    this._splitAreas.sw.height(contentHeight);
                }

                this._splitAreas.sw.css("top", wijgrid.mSplitDistanceY + topHeight);
                this._splitAreas.se.css("marginTop", wijgrid.mSplitDistanceY);
            }
        }

        _strictWidthMode(): boolean {
            return this._wijgrid.options.ensureColumnsPxWidth || this._wijgrid._allowHVirtualScrolling();
        }

        // private specific **

        // ** wijsuperpanel specific
        _getSuperPanelContentWrapper(): JQuery {
            if (!this._superPanelElementsCache.contentWrapper || !this._superPanelElementsCache.contentWrapper.length) {
                this._superPanelElementsCache.contentWrapper = this._wijgrid.mOuterDiv.find(".wijmo-wijsuperpanel-contentwrapper:first"); // not available in native mode?
            }

            return this._superPanelElementsCache.contentWrapper;
        }

        _getSuperPanelStateContainer(): JQuery {
            if (!this._superPanelElementsCache.stateContainer || !this._superPanelElementsCache.stateContainer.length) {
                this._superPanelElementsCache.stateContainer = this._wijgrid.mOuterDiv.find(".wijmo-wijsuperpanel-statecontainer:first");
            }

            return this._superPanelElementsCache.stateContainer;
        }

        _getSuperPanelTemplateWrapper(): JQuery {
            if (!this._superPanelElementsCache.templateWrapper || !this._superPanelElementsCache.templateWrapper.length) {
                this._superPanelElementsCache.templateWrapper = this._wijgrid.mOuterDiv.find(".wijmo-wijsuperpanel-templateouterwrapper:first"); // not available in native mode?
            }

            return this._superPanelElementsCache.templateWrapper;
        }

        _isNativeSuperPanel() {
            return this._wijgrid._isTouchEnv();
        }

        _testAutohiddenScrollbars() {
            if (this._isNativeSuperPanel()) {
                var container = this._getSuperPanelStateContainer();
                if (container.length) {
                    return container[0].offsetWidth === container[0].clientWidth; // no scrollbar or scrollbar is hidden (-ms-autohiding-scrollbar)
                }
            }

            return false;
        }
        // wijsuperpanel specific **
    }
}
