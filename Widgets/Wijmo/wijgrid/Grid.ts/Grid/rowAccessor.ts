/// <reference path="interfaces.ts"/>
/// <reference path="baseView.ts"/>

module wijmo.grid {


	var $ = jQuery;

	/**
	* Class for convenient access to rows of a wijgrid.
	* @ignore
	*/
	export class rowAccessor implements ISearchable<IRowInfo> {
		private _offsetBottom: number;
		private _offsetTop: number;
		private _view: wijmo.grid.baseView;
		private _scope: wijmo.grid.rowScope;

		constructor(view: wijmo.grid.baseView, scope: wijmo.grid.rowScope, offsetTop: number, offsetBottom: number) {
			this._view = view;
			this._scope = scope;
			this._offsetBottom = offsetBottom;
			this._offsetTop = offsetTop;
		}

		/** Gets an array of the table row elements that represents a wijgrid widget row at the specified index.
		  * size of returning array is always two.
		  * @param {Number} index The zero-based index of the row to retrieve.
		  * @returns {Object[]} The array of the table row elements at the specified index.
		  * @remarks
		  */
		item(index: number): IRowObj {
			var len = this.length();

			return (index >= 0 && index < len)
				? this._view.getJoinedRows(index + this._offsetTop, this._scope)
				: null;
		}

		/**
		* Gets the total number of elements.
		* @returns {Number} The total number of elements.
		*/
		length(): number {
			var joinedTables = this._view.getJoinedTables(true, 0),
				len = 0,
				htmlAccessor: htmlTableAccessor;

			if (htmlAccessor = joinedTables[0]) {
				len = htmlAccessor.getSectionLength(this._scope);
			}

			if (htmlAccessor = joinedTables[1]) {
				len += htmlAccessor.getSectionLength(this._scope);
			}

			len -= this._offsetTop + this._offsetBottom;

			if (len < 0) {
				len = 0;
			}

			return len;
		}

		/** @ignore */
		find(startFrom: number, callback: (row: IRowInfo) => any, retrieveDataItem = false): IRowInfo {
			var f = this.findEx(startFrom, callback, retrieveDataItem);
			return f ? f.val : null;
		}

		/** @ignore */
		findIndex(startFrom: number, callback: (row: IRowInfo) => any, retrieveDataItem = false): number {
			var f = this.findEx(startFrom, callback, retrieveDataItem);
			return f ? f.at : -1;
		}

		/** @ignore */
		findRowObj(startFrom: number, callback: (row: IRowInfo) => any, retrieveDataItem = false): IRowObj {
			var f = this.findEx(startFrom, callback, retrieveDataItem);
			return f ? this.item(f.at) : null;
		}

		/** @ignore */
		findEx(startFrom: number, callback: (row: IRowInfo) => any, retrieveDataItem = false): ISearchableResult<IRowInfo> {
			var len = this.length();

			for (var i = 0; i < len; i++) {
				var rowObj = this.item(i),
					rowInfo = this._view._getRowInfo(rowObj, retrieveDataItem);

				if (callback(rowInfo) === true) {
					return {
						at: i,
						val: rowInfo
					};
				}
			}

			return null;
		}

		/** Sequentially iterates the cells in a rowObj argument.
		  * @param {Array} rowObj Array of rows to be iterated.
		  * @param {Function} callback Function that will be called each time a new cell is reached.
		  * @param {Object} param Parameter that can be handled within the callback function.
		  */
		static iterateCells(rowObj: IRowObj, callback: (cell: HTMLTableCellElement, globCellIndex: number, param: any) => boolean, param: any): void {
			if (rowObj && callback) {
				var globCellIdx = 0;

				for (var i = 0, len = rowObj.length; i < len; i++) {
					var domRow = rowObj[i];

					if (domRow) {
						for (var j = 0, cellLen = domRow.cells.length; j < cellLen; j++) {
							var result = callback(<HTMLTableCellElement>domRow.cells[j], globCellIdx++, param);
							if (result !== true) {
								return;
							}
						}
					}
				}
			}
		}

		/** Gets a cell by its global index in a row's array passed in rowObj.
		  * @example:
		  * Suppose rows is an array containing the following data: [ ["a", "b"], ["c", "d", "e"] ]
		  * "a" symbol has a global index 0.
		  * "c" symbol has a global index 2.
		  * @param {Array} rowObj Array of table row elements.
		  * @param {Number} index Zero-based global index of a cell.
		  * @returns {HTMLTableCellElement} A cell or null if a cell with provided index is not found.
		  */
		static getCell(rowObj: IRowObj, globCellIndex: number): HTMLTableCellElement {
			var domRow: HTMLTableRowElement,
				cellLen: number;

			if (rowObj && (domRow = rowObj[0])) {
				cellLen = domRow.cells.length;
				if (globCellIndex < cellLen) {
					return <HTMLTableCellElement>domRow.cells[globCellIndex];
				}

				globCellIndex -= cellLen;

				if (domRow = rowObj[1]) {
					cellLen = domRow.cells.length;
					if (globCellIndex < cellLen) {
						return <HTMLTableCellElement>domRow.cells[globCellIndex];
					}
				}
			}

			return null;
		}

		/** @ignore */
		static getCell$(row: JQuery, globCellIndex: number): JQuery {
			var domCell = wijmo.grid.rowAccessor.getCell(<any>row, globCellIndex);

			return (domCell)
				? $(domCell)
				: $([]); // an empty set
		}


		/** Gets the number of cells in a array of table row elements.
		  * @param {Array} rowObj Array of table row elements.
		  * @returns {Number} The number of cells in a array of table row elements.
		  */
		static cellsCount(rowObj: IRowObj): number;
		static cellsCount(rowObj: JQuery): number;
		/** @ignore */
		static cellsCount(rowObj: any): number {
			var res = 0,
				domRow: HTMLTableRowElement;

			if (rowObj && (domRow = rowObj[0])) {
				res = domRow.cells.length;

				if (domRow = rowObj[1]) {
					res += domRow.cells.length;
				}
			}

			return res;
		}
	}
}