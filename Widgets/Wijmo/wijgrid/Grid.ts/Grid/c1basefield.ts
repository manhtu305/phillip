/// <reference path="../../../External/declarations/jquery.d.ts"/>
/// <reference path="interfaces.ts"/>

module wijmo.grid {


	var $ = jQuery;

	export class widgetEmulator {
		public static DATA_INSTANCE_PROP = "wijemuinstance";

		public namespace: string;
		public widgetName: string;
		public widgetFullName: string;
		public element: JQuery;
		public options: any;

		private _destroyed: boolean;

		constructor(options, emulatedWidgetName: string) {
			var names = emulatedWidgetName.split(".");

			this.namespace = names[0];
			this.widgetName = names[1];
			this.widgetFullName = this.namespace + "-" + this.widgetName;

			$.fn[this.widgetName] = function (funcName) { // backward compatibility (columnHeader.c1field(...))
				var result = undefined,
					args = Array.prototype.slice.call(arguments, 1);

				if ((typeof (funcName) === "string") && this.length) { // "this" refers to the jQuery object, not the widgetEmulator instance.
					this.each(function () {
						var instance = $.data(this, widgetEmulator.DATA_INSTANCE_PROP);
						if (instance) {
							if (!$.isFunction(instance[funcName]) || funcName.charAt(0) === "_") {
								return false;
							}

							result = instance[funcName].apply(instance, args);

							if (result !== undefined) {
								return false; // break
							}
						}
					});
				}

				return result;
			}

			this.options = options || {};
			this._destroyed = false;

			this._provideDefaults();
		}

		option(key: string, value?: any): any {
			var options = key;

			if (!arguments.length) {
				return this.options;
			}

			if (typeof (key) === "string") {
				if (value === undefined) {
					return this.options[key];
				}

				options = <any>{};
				options[key] = value;

				this._setOptions(options);
			}

			return this;
		}

		enable() {
			this._setOption("disabled", false);
		}

		disable() {
			this._setOption("disabled", true);
		}

		destroy() {
			try {
				if (!this._destroyed) {
					this._destroy();

					this.element.unbind("." + this.widgetName);
					this.element.removeData(this.widgetFullName);
				}
			}
			finally {
				this._destroyed = true;
			}
		}


		widget() {
			return this.element;
		}

		_destroy() {
		}

		_setOptions(options) {
			var self = this;

			$.each(options, function (key, value) {
				self._setOption(key, value);
			});
		}

		_attachToElement(element: JQuery) {
			this.element = element;

			if (this.element) {
				if (this.options.disabled) {
					this.disable();
				}

				this.element.data(widgetEmulator.DATA_INSTANCE_PROP, this); // store class instance (used by uiDragndrop).

				this.element.bind("remove." + this.widgetName, () => {
					this.destroy();
				});
			}
		}

		_provideDefaults() {
			this.options.disabled = this.options.disabled || false;
		}

		_setOption(key, value) {
			var presetFunc = this["_preset_" + key],
				oldValue = this.options[key];

			if (presetFunc) {
				value = presetFunc.apply(this, [value, oldValue]); // .apply(this, arguments);  note: there is no dynamic linkage between the arguments and the formal parameter values when strict mode is used
			}

			if (value !== oldValue) {
				var postsetFunc = this["_postset_" + key];

				this.options[key] = value; // set option

				this._prepostsetOption(key, value);

				if (postsetFunc) {
					postsetFunc.apply(this, [value, oldValue]);
				}
			}
		}

		_prepostsetOption(key, value) {
		}

		_postset_disabled(value, oldValue) {
			//if (this.element) {
			//	this.element[value ? "addClass" : "removeClass"](this.widgetFullName + "-disabled ui-state-disabled").attr("aria-disabled", value);
			//}
		}

		_eventKey(eventName: string): string {
			return eventName + "." + this.widgetName;
		}
	}
}

module wijmo.grid {


	export interface IC1FieldFactory {
		(grid: wijgrid, column: IColumn): c1basefield;
	}

	var fieldFactories = []; // IC1FieldFactory[]

	export function registerColumnFactory(factory: IC1FieldFactory) {
		if (!$.isFunction(factory)) {
			throw "'factory' must be a function.";
		}

		fieldFactories.push(factory);
	}

	export function asColumnInstance(grid: wijgrid, column: IColumn): c1basefield {
		for (var i = fieldFactories.length - 1; i >= 0; i--) { // starting from the most specialized
			var instance = fieldFactories[i](grid, column);
			if (instance) {
				return instance;
			}
		}

		throw "Unsupported column.";
	}
}


module wijmo.grid {


	var $ = jQuery;

	export class c1basefield extends widgetEmulator implements IDragnDropElement {
		/** @ignore */
		public static WIDGET_NAME = "wijmo.c1basefield";

		public static test(column: IColumn): boolean {
			return true;
		}

		public options: IC1BaseFieldOptions; // inherited, re-define type.

		private mWijgrid: wijgrid;
		private mFirstVisible: boolean = undefined;

		constructor(wijgrid: wijgrid, options: any, widgetName?: string) {
			super(options, widgetName || c1basefield.WIDGET_NAME);

			this.mWijgrid = wijgrid;
		}

		_destroy() {
			var wijgrid = this._owner(),
				defCSS = this._defCSS(),
				wijCSS = this._wijCSS();

			if (this.element) {
				this.element.find("*").unbind("." + this.widgetName);

				this.element
					.removeClass(wijCSS.widget + " " + defCSS.c1basefield + " " + wijCSS.stateDefault)
					.html(this.element.find("." + defCSS.headerCellText).html()); // restore initial cell content
			}

			if (wijgrid && wijgrid._UIDragndrop()) {
				wijgrid._UIDragndrop(false).detach(this);
			}

			super._destroy.apply(this, arguments);
		}

		_owner(): wijgrid {
			return this.mWijgrid;
		}

		_attachToElement(element: JQuery) {
			super._attachToElement.apply(this, arguments);

			var grid = this._owner();

			if (grid.options.allowColMoving && this._draggable()) {
				grid._UIDragndrop(true).attach(this);
			}
		}

		_provideDefaults() {
			super._provideDefaults();

			wijmo.grid.shallowMerge(this.options, c1basefield.prototype.options);
		}

		_defCSS(): wijgridCSS {
			return wijmo.grid.wijgrid.CSS;
		}

		_wijCSS(): wijgridWijmoCSS {
			if (this.mWijgrid) {
				return this.mWijgrid.options.wijCSS;
			}

			return <any>{};
		}

		//#region rendering

		_initializeCell(cell: JQuery, cellIndex: number, container: JQuery, formattedValue, row: IRowInfo): void {
			var rt = wijmo.grid.rowType,
				useDefault = true,
				args: IC1BaseFieldCellFormatterArgs = {
					$cell: cell,
					$container: container,
					column: <IColumn>this.options,
					formattedValue: formattedValue,
					row: row,
					afterDefaultCallback: null
				};

			// apply rowHeight option to all tbody rows if virtual scrolling is used.
			if (((row.type & rt.data) || (row.type === rt.groupFooter) || (row.type === rt.groupHeader)) && this.mWijgrid._allowVirtualScrolling()) {
				var height = (<fixedView>this.mWijgrid._view()).getDefaultRowHeight();

				container.css({
					"overflow": "hidden",
					"height": height // apply default height if the rowHeight is not set.
				});
			}

			if ($.isFunction(this.options.cellFormatter)) {
				useDefault = !this.options.cellFormatter(args);
			}

			if (useDefault) {
				var rt = wijmo.grid.rowType;

				switch (row.type & ~(rt.dataAlt | rt.dataDetail | rt.dataHeader)) {
					case rt.data:
						this._initializeDataCell(cell, container, args.formattedValue, row);
						break;

					case rt.footer:
						this._initializeFooterCell(cell, container, row);
						break;

					case rt.header:
						this._initializeHeaderCell(cell, container);
						break;

					case rt.filter:
						this._initializeFilterCell(cell, container);
						break;

					case rt.groupHeader:
						this._initializeGroupHeaderCell(cell, cellIndex, container, args.formattedValue, row);
						break;

					case rt.detail:
						this._initializeDetailCell(cell, container, row);
						break;

					default:
						this._updateHTML(row, container, args.formattedValue);
				}

				if ($.isFunction(args.afterDefaultCallback)) {
					args.afterDefaultCallback(args);
				}
			}
		}

		_initializeDataCell(cell: JQuery, container: JQuery, formattedValue, rowInfo: IRowInfo) {
			this._setupDataCell(container, formattedValue, rowInfo);
		}

		_initializeFooterCell(cell: JQuery, container: JQuery, row: IRowInfo) {
			this._setupFooterCell(container, row);
		}

		_initializeHeaderCell(cell: JQuery, container: JQuery) {
			this._attachToElement(cell);

			var defCSS = this._defCSS(),
				wijCSS = this._wijCSS();

			cell.addClass(wijCSS.widget + " " + defCSS.c1basefield + " " + wijCSS.stateDefault)

			this._setupHeaderCell();
		}

		_initializeFilterCell(cell: JQuery, container: JQuery) {
			var defCSS = this._defCSS(),
				wijCSS = this._wijCSS();

			container.addClass(wijCSS.widget + " " + wijCSS.stateDefault);

			this._setupFilterCell(container);
		}

		_initializeGroupHeaderCell(cell: JQuery, cellIndex: number, container: JQuery, value: any, row: IRowInfo) {
			var grid = this.mWijgrid;

			// provide a toggle icon for the first cell of the groupHeader row
			if (cellIndex === this.mWijgrid._virtualLeaves().length) {
				// if grouped column is hidden then groupedColumn != this.
				var groupedColumn = this.mWijgrid._groupedLeaves()[row._extInfo.groupLevel - 1]; // groupLevel is 1-based

				if (groupedColumn) {
					var gi = groupedColumn.options.groupInfo;

					if (gi && (gi.outlineMode !== "none")) {
						var collapsed = (row._extInfo.state & wijmo.grid.renderStateEx.collapsed) != 0;

						value = this._getToggleButtonHtml(collapsed, collapsed ? gi.collapsedImageClass : gi.expandedImageClass)
						+ value;
					}
				}
			}

			this._updateHTML(row, container, value);
		}

		_initializeDetailCell(cell: JQuery, container: JQuery, row: IRowInfo) {
			cell.addClass(this._defCSS().detailContainerCell);
			this._setupDetailCell(container, row);
		}

		_getToggleButtonHtml(collapsed: boolean, priorityIconCss?: string): string {
			var defCSS = this._defCSS(),
				wijCSS = this._wijCSS(),
				icon = priorityIconCss || (collapsed
				? wijCSS.iconArrowRight
				: wijCSS.iconArrowRightDown);

			return "<div class =\"" + wijCSS.icon + " " + icon + " " + defCSS.groupToggleVisibilityButton + " " + wijCSS.wijgridGroupToggleVisibilityButton + "\">&nbsp;</div>";
		}

		_cellRendered(cell: JQuery, container: JQuery, row: IRowInfo) {
		}

		_needToEncodeValue(row: IRowInfo): boolean {
			return false;
		}

		_updateHTML(row: IRowInfo, container: JQuery, value) {
			var encode = this._needToEncodeValue(row);
			this.mWijgrid.mCellFormatter.updateHTML(container, value, encode);
		}
		//#endregion rendering

		//#region header cell
		_getHeaderContent(): string {
			return this.options.headerText || "&nbsp;"; // html(value) returns "" if value is undefined
		}

		_createHeaderContent(container: JQuery): void {
			container.html(this._getHeaderContent()); // html(value) returns "" if value is undefined
		}

		_decorateHeaderContent(container: JQuery): void {
			var defCSS = this._defCSS(),
				wijCSS = this._wijCSS();

			container.wrapInner("<span class=\"" + defCSS.headerCellText + " " + wijCSS.wijgridHeaderCellText + "\" />");
		}

		_setupHeaderCell(): void {
			var $container = this.element
				.children("." + this._defCSS().cellContainer)
				.empty();

			this._createHeaderContent($container);
			this._decorateHeaderContent($container);
		}
		//#endregion

		//#region filter cell
		_setupFilterCell(container: JQuery) {
		}
		//#endregion

		//#region data cell
		_setupDataCell(container: JQuery, formattedValue, rowInfo: IRowInfo) {
			this._updateHTML(rowInfo, container, formattedValue);
		}
		//#endregion

		//#region footer cell
		_setupFooterCell(container: JQuery, row: IRowInfo) {
			this._updateHTML(row, container, this.options.footerText || this.options._footerTextDOM);
		}
		//#endregion

		//#region detail cell
		_setupDetailCell(container: JQuery, row: IRowInfo) {
			var owner = this._owner(),
				masterRow = <SketchDataRow>owner.mSketchTable.row(row.sketchRowIndex - 1);

			var detail = owner.details()[masterRow.originalRowIndex];
			if (detail.isExpanded()) {
				owner.mLoadingDetails++;

				owner._view()._addDetailInstantiator(function () {
					detailInstantiator.instantiateIn(container, owner, detail.masterKey(), masterRow.originalRowIndex);
					container = null;
				});
			}
		}
		//#endregion

		//#region properies handlers
		_postset_headerText(value, oldValue) {
			this._setupHeaderCell();
		}

		_postset_visible(value, oldValue) {
			var self = this, wijgrid = self._owner();
			if (wijgrid._allowHVirtualScrolling()) {
				wijgrid._resetScrollState();//make sure wijgrid can get the correct renderable bound.(#123978)
			}
			wijgrid.ensureControl(false);
		}

		_postset_width(value, oldValue) {
			var flag = this._owner()._isDetail();

			try {
				if (flag) {
					this._owner()._ignoreSizing(false);
				}

				this.options.ensurePxWidth = true; // prevent auto expanding
				this.options._realWidth = value;

				this._owner().setSize(); // recalculate sizes and auto expand other columns if possible.
			} finally {
				if (flag) {
					this._owner()._ignoreSizing(true);
				}
			}
		}
		//#endregion

		//#region UI actions
		_draggable(): boolean {
			return true;
		}

		_canSize(): boolean {
			return this.options.allowSizing && this._owner().options.allowColSizing;
		}

		// drag-n-drop
		_canDrag(): boolean {
			return this.options.allowMoving === true;
		}

		_canDropTo(column: IDragnDropElement): boolean {
			// parent can't be dropped into a child
			if (wijmo.grid.isChildOf(this._owner()._allColumns(), <c1basefield>column, this)) {
				return false;
			}

			return true;
		}

		_canDropToGroupArea(): boolean {
			return false;
		}

		//#endregion

		//#region misc
		_visible() {
			return this.options._visLeavesIdx >= 0;
			//return this.options._parentVis === true;
		}

		_isFirstVisible(): boolean {
			if (this.mFirstVisible === undefined) {
				this.mFirstVisible = this.options._visLeavesIdx === this.mWijgrid._virtualLeaves().length;
			}

			return this.mFirstVisible;
		}

		_isRendered(): boolean {
			return this._visible() && this.mWijgrid._renderableColumnsRange().contains(this);
		}

		_strictWidthMode(): boolean {
			return !!(this.options.ensurePxWidth || this.mWijgrid.options.ensureColumnsPxWidth || this.mWijgrid._allowHVirtualScrolling());
		}
		//#endregion
	}

	export class c1basefield_options implements wijmo.grid.IC1BaseFieldOptions {
		/** A value indicating whether the column can be moved.
		  * @example
		  * $("#element").wijgrid({ columns: [ { allowMoving: true } ] });
		  */
		allowMoving: boolean = true;

		/** A value indicating whether the column can be sized.
		  * @example
		  * $("#element").wijgrid({ columns: [ { allowSizing: true } ] });
		  */
		allowSizing: boolean = true;

		/** This function is called each time wijgrid needs to create cell content. 
		  * This occurs when rows are being rendered or cell editing is about to finish.
		  * You can use it to customize cell content.
		  * @example
		  * // Add an image which URL is obtained from the "Url" data field to the column cells.
		  * $("#demo").wijgrid({
		  *		data: [
		  *			{ ID: 0, Url: "/images/0.jpg" },
		  *			{ ID: 1, Url: "/images/1.jpg" }
		  *		],
		  *		columns: [
		  *			{},
		  *			{
		  *				cellFormatter: function (args) {
		  *					if (args.row.type & wijmo.grid.rowType.data) {
		  *						args.$container
		  *							.empty()
		  *							.append($("<img />")
		  *								.attr("src", args.row.data.Url));
		  *
		  *						return true;
		  *					}
		  *				}
		  *			}
		  *		]
		  * });
		  * @type {Function}
		  * @param {wijmo.grid.IC1BaseFieldCellFormatterArgs} args The data with this function.
		  * @returns {Boolean} True if container content has been changed and wijgrid should not apply the default formatting to the cell.
		  * @remarks
		  * Important: cellFormatter should not alter content of header and filter row cells container.
		  */
		cellFormatter: (args: IC1BaseFieldCellFormatterArgs) => boolean = undefined;

		/** Determines whether to use number type column width as the real width of the column.
		  * @example
		  * $("#element").wijgrid({ columns: [{ ensurePxWidth: true }]});
		  * @remarks
		  * If this option is set to true, wijgrid will use the width option of the column widget.
		  * If this option is undefined, wijgrid will refer to the ensureColumnsPxWidth option.
		  */
		ensurePxWidth: boolean = undefined;

		/** Gets or sets the footer text.
		  * The text may include a placeholder: "{0}" is replaced with the aggregate.
		  * @example
		  * $("#element").wijgrid({ columns: [{ footerText: "footer" }]});
		  * @remarks
		  * If the value is undefined the footer text will be determined automatically depending on the type of the datasource:
		  * DOM table - text in the footer cell.
		  */
		footerText: string = undefined;

		/** Gets or sets the header text.
		  * @example
		  * $("#element").wijgrid({ columns: [ { headerText: "column0" } ] });
		  * @remarks
		  * If the value is undefined the header text will be determined automatically depending on the type of the datasource:
		  * DOM table - text in the header cell.
		  * Array of objects - dataKey (name of the field associated with column).
		  * Two-dimensional array - dataKey (index of the field associated with column).
		  */
		headerText: string = undefined;

		/** Gets or sets the text alignment of data cells. Possible values are "left", "right", "center".
		  * @example
		  * $("#element").wijgrid({ columns: [{ textAligment: "right" }]});
		  */
		textAlignment: string = undefined;

		/** A value indicating whether column is visible.
		  * @example
		  * $("#element").wijgrid({ columns: [{ visible: true }]});
		  */
		visible: boolean = true;

		/** Determines the width of the column.
		  * @type {String|Number}
		  * @example
		  * $("#element").wijgrid({ columns: [ { width: 150 } ] });
		  * $("#element").wijgrid({ columns: [ { width: "10%" } ]});
		  * @remarks
		  * The option could either be a number of string.
		  * Use number to specify width in pixel, use string to specify width in percentage.
		  * By default, wijgrid emulates the table element behavior when using number as width. This means wijgrid may not have the exact width specified. If exact width is needed, please set ensureColumnsPxWidth option of wijgrid to true.
		  */
		width: any = undefined;
	};

	c1basefield.prototype.options = wijmo.grid.extendWidgetOptions({}, new c1basefield_options());

	wijmo.grid.registerColumnFactory((grid, column) => {
		if (c1basefield.test(column)) {
			return new c1basefield(grid, column);
		}

		return null;
	});
}

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class c1rowheaderfield extends c1basefield {
		/** @ignore */
		public static CLIENT_TYPE = "c1rowheaderfield";
		/** @ignore */
		public static WIDGET_NAME = "wijmo.c1rowheaderfield";
		/** @ignore */
		public static COLUMN_WIDTH = 22;

		public static getInitOptions(): IColumn {
			var opt: IColumnInternalOptions = {};

			opt._clientType = c1rowheaderfield.CLIENT_TYPE;
			opt._parentVis = true;

			return <any>opt;
		}

		public static test(column: IColumn): boolean {
			return column._clientType === c1rowheaderfield.CLIENT_TYPE;
		}

		constructor(wijgrid: wijgrid, options: any, widgetName?: string) {
			super(wijgrid, options, widgetName || c1rowheaderfield.WIDGET_NAME);
		}

		_provideDefaults() {
			super._provideDefaults();

			this.options.allowMoving = false;
			this.options.allowSizing = false;
			(<IColumn>this.options).dataKey = null;
			this.options.width = c1rowheaderfield.COLUMN_WIDTH;
			this.options.ensurePxWidth = true;
		}

		_initializeCell(cell: JQuery, cellIndex: number, container: JQuery, formattedValue, rowInfo: IRowInfo): void {
			var defCSS = this._defCSS(),
				wijCSS = this._wijCSS();

			super._initializeCell.apply(this, arguments);

			cell
				.attr({ "role": "rowheader", "scope": "row" })
				.addClass(wijCSS.stateDefault + " " + wijCSS.content + " " + defCSS.rowHeader + " " + wijCSS.wijgridRowHeader);
		}

		_initializeDetailCell(cell: JQuery, container: JQuery, row: IRowInfo) {
			super._initializeDetailCell.apply(this, arguments);
			cell.removeClass(this._defCSS().detailContainerCell); // row header cell can't be used as detail grid container
		}

		_setupDetailCell(container: JQuery, row: IRowInfo) {
			this._updateHTML(row, container, ""); // row header cell can't be used as detail grid container
		}

		_draggable() {
			return false;
		}

		_isFirstVisible(): boolean {
			return false;
		}
	}

	c1rowheaderfield.prototype.options = wijmo.grid.extendWidgetOptions({}, new c1basefield_options());

	wijmo.grid.registerColumnFactory((grid, column) => {
		if (c1rowheaderfield.test(column)) {
			return new c1rowheaderfield(grid, column);
		}

		return null;
	});
}

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class c1detailrowheaderfield extends c1rowheaderfield {
		/** @ignore */
		public static CLIENT_TYPE = "c1detailrowheaderfield";
		/** @ignore */
		public static WIDGET_NAME = "wijmo.c1detailrowheaderfield";

		/** @ignore */
		public static test(column: IColumn): boolean {
			return column._clientType === c1detailrowheaderfield.CLIENT_TYPE;
		}

		/** @ignore */
		public static getInitOptions(): wijmo.grid.IColumn {
			var opts = wijmo.grid.c1rowheaderfield.getInitOptions();
			opts._clientType = c1detailrowheaderfield.CLIENT_TYPE;
			return opts;
		}

		constructor(wijgrid: wijmo.grid.wijgrid, options: any, widgetName?: string) {
			super(wijgrid, options, widgetName || c1detailrowheaderfield.WIDGET_NAME);
		}

		_setupDataCell(container: JQuery, formattedValue, row: wijmo.grid.IRowInfo) {
			if (row.type & wijmo.grid.rowType.dataDetail) {
				this._updateHTML(row, container, "&nbsp;");
			} else {
				var html = this._getToggleButtonHtml((row._extInfo.state & wijmo.grid.renderStateEx.collapsed) != 0);

				this._updateHTML(row, container, html);

				container.find("." + this._defCSS().groupToggleVisibilityButton)
					.click((e: JQueryEventObject) => {
						this._owner()._onToggleHierarchy(e, row);
						e.stopPropagation();
						return false;
					})
					.dblclick((e: JQueryEventObject) => {
						e.stopPropagation(); //#110137
						return false;
					});
			}
		}
	}

	wijmo.grid.registerColumnFactory((grid, column) => {
		if (c1detailrowheaderfield.test(column)) {
			return new c1detailrowheaderfield(grid, column);
		}

		return null;
	});
}
