/// <reference path="interfaces.ts" />
/// <reference path="wijgrid.ts" />
/// <reference path="misc.ts" />

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export enum updateCellResult {
		error = 0,
		continueEditing = 1,
		success = 2,

		// ESC key is pressed or cell in not rendered.
		cancel = 4, // used only as modifier of the success value, not as an independent value).
		notEdited = 8
	};

	/** @ignore */
	export class cellEditorHelper {
		private _timeout = 25;

		cellEditStart(grid: wijgrid, cell: cellInfo, e: JQueryEventObject) {
			var result = false,
				view = grid._view();

			if (cell._isValid() && !cell._isPreparingForEditing() && !cell._isEdit() && (cell.columnInst() instanceof wijmo.grid.c1field)) {
				var rowInfo = cell.row();

				if (rowInfo) {
					var rowType = rowInfo.type;

					if (rowType & wijmo.grid.rowType.data) {

						var args: IBeforeCellEditEventArgs = {
							cell: cell,
							event: e,
							handled: false
						};

						cell._isPreparingForEditing(true); // to avoid recursion because of changing focus\ selection and wijgrid.onfocusin event handler

						if (result = (grid._onBeforeCellEdit(args) || (grid.options.editingMode === "row"))) { // cancellable only if editingMode is "cell"
							if (!args.handled) {
								result = this._defaultBeforeCellEdit(grid, args);
							}
						}

						if (result) {
							cell._isEdit(true);
						}

						cell._isPreparingForEditing(false);
					}
				}
			}

			return result;
		}

		updateCell(grid: wijgrid, cell: cellInfo, e: JQueryEventObject, reject: boolean): updateCellResult {
			var row: IRowInfo;

			if (cell && !cell._isEdit()) {
				return updateCellResult.success | updateCellResult.notEdited;
			}

			if (!cell || !cell._isValid() || !(row = cell.row()) || !(row.type & wijmo.grid.rowType.data)) {
				return updateCellResult.error;
			}


			if (reject || !cell._isRendered()) {
				return updateCellResult.success | updateCellResult.cancel;
			}

			var result = updateCellResult.success,
				column = cell.column();

			var bcuArgs: IBeforeCellUpdateEventArgs = {
				cell: cell,
				value: undefined
			};

			if (grid._onBeforeCellUpdate(bcuArgs) || (grid.options.editingMode === "row")) { // cancellable only if editingMode is "cell"
				if (bcuArgs.value === undefined) { // user doesn't provide a new value
					bcuArgs.value = this._getCellValue(grid, cell); // get raw value from editor using  default implementation
				}

				var a = bcuArgs.value, // new value
					b = cell.value(); // old value

				try {
					var inputType = this._getHTMLInputElementType(cell);

					if (wijmo.grid.HTML5InputSupport.isExtendSupportRequired(inputType)) {
						bcuArgs.value = wijmo.grid.HTML5InputSupport.parse(bcuArgs.value, inputType);
						bcuArgs.value = wijmo.grid.HTML5InputSupport.extend(b, bcuArgs.value, inputType);
					} else {
						bcuArgs.value = grid.parse(cell.column(), bcuArgs.value); // try to parse raw value
					}

					a = bcuArgs.value;
				} catch (ex) {
					bcuArgs.value = a; // restore raw value
				}

				if (wijmo.grid.getDataType(column) === "datetime") {
					if (a instanceof Date) {
						a = a.getTime();
					}

					if (b instanceof Date) {
						b = b.getTime();
					}
				}

				if (a !== b) { // value is changed
					// update datasource
					try {
						cell.value(bcuArgs.value);
					} catch (ex) {
						var icvArgs: IInvalidCellValueEventArgs = {
							cell: cell,
							value: bcuArgs.value
						};

						result = updateCellResult.error;
						grid._onInvalidCellValue(icvArgs);
					}

					if (result & updateCellResult.success) {
						var acuArgs: IAfterCellUpdateEventArgs = {
							cell: cell
						};

						grid._onAfterCellUpdate(acuArgs);
					}
				}
			} else {
				return updateCellResult.continueEditing;
			}

			return result;
		}

		// must be called only after the updateCell().
		cellEditEnd(grid: wijgrid, cell: cellInfo, e: JQueryEventObject) {
			var rowInfo = cell.row(),
				aceArgs: IAfterCellEditEventArgs = {
					cell: cell,
					event: e,
					handled: false
				};

			cell._isEdit(false);

			if (cell._isRendered()) {
				grid._onAfterCellEdit(aceArgs);

				if (!aceArgs.handled) {
					this._defaultAfterCellEdit(grid, aceArgs);
				}
			}
		}

		// private
		private _defaultBeforeCellEdit(grid: wijgrid, args: IBeforeCellEditEventArgs) {
			var column = args.cell.column(),
				result = false,
				$container: JQuery,
				$input: JQuery;

			if (column.dataIndex >= 0) {
				var value = args.cell.value(),
					keyCodeEnum = wijmo.getKeyCodeEnum(),
					inputType = wijmo.grid.HTML5InputSupport.getDefaultInputType(grid._isMobileEnv(), column),
					allowCellEditing = grid._allowCellEditing(),
					serverSideCheckbox = false;

				result = true;

				try {
					$container = args.cell.container();

					if (wijmo.grid.getDataType(column) === "boolean") {
						var $span = $container.children("span");

						if (serverSideCheckbox = !!($span.length && ($span.prop("disabled") || (grid.options._aspNetDisabledClass && $span.hasClass(grid.options._aspNetDisabledClass))))) { // C1GridView support
							$.data($span[0], "serverSideCheckbox", true);

							$span.prop("disabled", false); // ASP.NET 3

							if (grid.options._aspNetDisabledClass) {
								$span.removeClass(grid.options._aspNetDisabledClass); // ASP.NET 4
							}
						}

						$input = $container.find(":checkbox");

						if (serverSideCheckbox || !allowCellEditing) { // c1gridview or editingMode="row"
							$input.prop("disabled", false);
						}

						if (args.event && args.event.type === "keypress") {
							$input.one("keyup", function (e) {
								if (e.which === keyCodeEnum.SPACE) {
									e.preventDefault();
									(<HTMLInputElement>$input[0]).checked = !value;
								}
							});
						}
					} else {
						$input = $("<input type='" + inputType + "' />")
							.css("ime-mode", column.imeMode || "auto").attribute("aria-label", inputType)
							.addClass(wijmo.grid.wijgrid.CSS.inputMarker).addClass("wijmo-wijinput " + grid.options.wijCSS.stateFocus)
							.bind("keydown", grid, $.proxy(this._checkBoxOrInputKeyDown, this));

						//the problem of inputing
						$input.bind(((<any>$.support).selectstart ? "selectstart" : "mousedown"), function (event: JQueryEventObject) {
							event.stopPropagation();
						});

						if (args.event && (args.event.type === "keydown") && (args.event.which !== 113)) { // 113 = F2
							// "edit on keypress", leave the editor empty.
						} else {
							switch (wijmo.grid.getDataType(column)) {
								case "currency":
								case "number":
									if (value !== null) {
										$input.val(value); // ignore formatting
										break;
									}
								case "datetime":
									if (wijmo.grid.HTML5InputSupport.isExtendSupportRequired(inputType)) {
										$input.val(wijmo.grid.HTML5InputSupport.toStr(value, inputType));
										break;
									}
								// fall through
								default:
									$input.val(grid.toStr(column, value));
									break;
							}
						}

						$container
							.empty()
							.append($input);

						if (allowCellEditing) { // don't change the focus on row editing.
							var len = $input.val().length;
							if (inputType === "text") {
								// move caret to the end of the text
								new wijmo.grid.domSelection(<HTMLInputElement>$input[0]).setSelection({ start: len, end: len });
							}
						}
					}

					if (allowCellEditing) { // don't change the focus on row editing.
						$input.focus();
						setTimeout(function () { // IE fix
							$input.focus();
						}, this._timeout * 2);
					}
				}
				catch (ex) {
					alert(ex.message);
					result = false;
				}
			}

			return result;
		}

		private _defaultAfterCellEdit(grid: wijgrid, args: IAfterCellEditEventArgs) {
			var leaf = args.cell.columnInst(),
				opt = <IColumn>leaf.options,
				result = false;

			if (opt.dataIndex >= 0) {
				result = true;

				var view = grid._view();

				try {
					var $container = args.cell.container(),
						cellValue = grid.toStr(opt, args.cell.value()),
						rowInfo = view._getRowInfoBySketchRowIndex(args.cell.rowIndex());

					if (wijmo.grid.getDataType(opt) === "boolean") {
						var $span = $container.children("span"),
							disable = $span.length && $.data($span[0], "serverSideCheckbox"),
							$input = $container.find(":checkbox");

						$input.prop("checked", cellValue === "true");

						/*if (cellValue === "true") {
							$input.attr("checked", "checked");
						}
						else {
							$input.removeAttr("checked");
						}*/

						if (disable) {
							$span.prop("disabled", true); // ASP.NET 3

							if (grid.options._aspNetDisabledClass) { // ASP.NET 4
								$span.addClass(grid.options._aspNetDisabledClass);
							}

							$input.prop("disabled", true);
						}
					}
					else {
						grid.mCellFormatter.format($(args.cell.tableCell()), args.cell.cellIndex(), $container, leaf, cellValue, rowInfo);
					}
				}
				catch (ex) {
					alert("defaultAfterCellEdit: " + ex.message);
					result = false;
				}
			}

			return result;
		}

		private _checkBoxOrInputKeyDown(args: JQueryEventObject) {
			var keyCodeEnum = wijmo.getKeyCodeEnum();

			if (args.which === keyCodeEnum.ENTER) { // stop editing when Enter key is pressed
				var grid: wijgrid = args.data;

				if (grid) {
					grid._endEditInternal(args);
					return false; // prevent submit behaviour.
				}
			}
		}

		private _getCellValue(grid: wijgrid, currentCell: wijmo.grid.cellInfo): any {
			var $input = currentCell.container().find(":input:first"),
				result = null;

			if ($input.length) {
				result = ($input.attr("type") === "checkbox")
				? (<HTMLInputElement>$input[0]).checked
				: $input.val();
			}

			return result;
		}

		private _getHTMLInputElementType(currentCell: wijmo.grid.cellInfo): string {
			return currentCell.container().find(":input:first").attr("type");
		}

		// private *
	}

	// IME support.
	/** @ignore */
	export class keyDownEventListener {
		private mHiddenElement: JQuery;
		private mFakeFocusable: JQuery;
		private mWrapper: JQuery;
		private mGrid: wijgrid;

		constructor(grid: wijgrid, container: JQuery) {
			this.mGrid = grid;

			this.mWrapper = $("<div />");

			if (this.mGrid._allowCellEditing()) {
				this.mHiddenElement = $("<input type=\"text\" />")
					.keydown($.proxy(this._onKeyDown, this));
			} else {
				this.mHiddenElement = $("<input type=\"button\" />"); // use any focusable element other than textbox (to avoid keyboard popup in mobile).
			}

			this.mHiddenElement.css({
				position: "relative",
				border: 0,
				padding: 0,
				margin: 0,
				width: 1, // if zero-dimension element is used then 1) a ghost caret will be displayed after editing in IE9. 2) IME is disabled in Chrome.
				height: 1,
				"font-size": wijmo.grid.isMobileSafari()
				? "0em" // hide caret in MobileSafari.
				: "1px" // don't use zero value otherwise the IME will be disabled in Chrome.
			});

			this.mHiddenElement.attribute("aria-label", "hiddenElement");

			this.mFakeFocusable = $("<input tabindex=\"-1\" type=\"button\" />") // special element to resolve an issue with changing the IME in IE11 .
				.css({
					position: "relative",
					width: 1,
					height: 1,
					border: 0,
					padding: 0,
					margin: 0
				});

			this.mFakeFocusable.attribute("aria-label", "fakeFocusable");

			this.mWrapper
				.append(this.mHiddenElement)
				.append(this.mFakeFocusable)
				.css({
					position: "absolute",
					width: "0px",
					height: "0px",
					overflow: "hidden",
					"z-index": 999999 // TFS issue #45934 (an element with absolute positioning affects the superpanel layout in IE).
				});

			container.append(this.mWrapper);
		}

		dispose() {
			if (this.mWrapper) {
				try {
					this.mWrapper.remove();
				}
				catch (ex) {
				}
				finally {
					this.mWrapper = null;
					this.mHiddenElement = null;
					this.mGrid = null;
				}
			}
		}

		focus(coord: JQuery, column: c1basefield): void {
			if (coord && coord.length) {
				var offset = coord.offset();

				this.mWrapper.offset({
					top: offset.top,
					left: offset.left
				});

				if ($.browser.msie) {
					this.mFakeFocusable.focus(); // move focus out of the mHiddenElement first to resolve an issue with changing the IME in IE11
                }

                var imeMode = 'auto';
                if (column && (column instanceof c1field)) {
                    var field = <c1field>column;
                    imeMode = (field.options.readOnly === true) ? 'disabled' : field.options.imeMode;
                }

                this.mHiddenElement.css("ime-mode", imeMode);
                if (imeMode !== 'disabled') {
                    this.mHiddenElement.focus();
                }
			}
		}

		isHiddenInput(element: JQuery): boolean {
			return element && ((element[0] === this.mHiddenElement[0]) || (element[0] === this.mFakeFocusable[0]));
		}

		canHandle(e: JQueryEventObject): boolean {
			return (this.mGrid._allowCellEditing() && this.isPrintableKeyCode(e));
		}

		isPrintableKeyCode(e: JQueryEventObject): boolean {
			var k = e.keyCode;

			/* ported from the SpeadJS */
			return !e.ctrlKey && !e.altKey
				&& ((k >= 65 && k <= 90) // A~Z a~z
				|| ((k >= 48 && k <= 57) || (k >= 96 && k <= 105)) // 0~9
				|| (k >= 186 && k <= 192) // Key for ; = , - . / `
				|| (k >= 220 && k <= 222 || k === 219) // Key for \ ] ' [
				|| (k >= 106 && k <= 111) // Num keyboard for * + KP_Separator - . /
				|| (k === 32) // space
				|| (k === 61) // "=" key (Firefox and Opera)
				|| (k === 173) // "-" key (Firefox)
				|| (k === 229 || e.keyCode === 0));  // IME key. 229 for IE and Chrome, 0 for Firefox.
		}

		private _onKeyDown(e: JQueryEventObject) {
			if (this.canHandle(e)) {
				//if (targetOuterDiv.length && (targetOuterDiv[0] === this.outerDiv[0])) {

				this.mGrid._beginEditInternal(e); // pass input (onKeyUp event) to the editor.
			} else {
				var keyCodeEnum = wijmo.getKeyCodeEnum();

				switch (e.keyCode) {
					case keyCodeEnum.ESCAPE:// #47873. IE workaround: if input element is inside form element then after double pressing the Esc key the whole form will be cleared.
					case keyCodeEnum.ENTER: // #69545. Prevent submit behaviour.
						e.preventDefault();
						break;
				}
			}
		}
	}
}