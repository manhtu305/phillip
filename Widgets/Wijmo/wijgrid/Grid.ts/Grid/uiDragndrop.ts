/// <reference path="interfaces.ts"/>
/// <reference path="wijgrid.ts" />
/// <reference path="c1groupedfield.ts" />

module wijmo.grid {


	var $ = jQuery;

	/** @ignore */
	export class uiDragndrop {
		private _wijgrid: wijgrid;
		private _scope_guid: string = "scope_" + wijmo.grid.getUID();
		private _wijCSS: any;
		private _$bottomArrow: JQuery;
		private _$topArrow: JQuery;
		private _droppableColumn: IDragnDropElement; // to use inside the draggable.drag event.
		private _dragEnd: boolean = false;
		private _dropTargetRedirected: boolean; // handles the situation when draggable is moved over the non-empty group area, in this case we assume the rightmost header in the group area as droppable instead of group area itself.
		private _wrapHtml: string;
		private mZIndex: number = 1000;

		constructor(wijgrid: wijgrid) {
			var defCSS = wijmo.grid.wijgrid.CSS;

			this._wijgrid = wijgrid;
			this._wijCSS = this._wijgrid.options.wijCSS;
			this._wrapHtml = "<div class=\"" + this._wijCSS.widget + " " + defCSS.wijgrid + " " + this._wijCSS.wijgrid + " " + this._wijCSS.content + " " + this._wijCSS.cornerAll + "\">" +
			"<table class=\"" + defCSS.root + " " + defCSS.table + " " + this._wijCSS.wijgridTable + "\">" +
			"<tr class=\"" + defCSS.headerRow + " " + this._wijCSS.wijgridHeaderRow + "\">" +
			"</tr>" +
			"</table>" +
			"</div>";
		}

		attachGroupArea(element: JQuery) {
			var draggedColumn: c1basefield,
				self = this;

			if (!<any>$.ui || !(<any>$.ui).droppable || !(<any>$.ui).draggable) {
				return;
			}

			element.droppable({
				scope: this._scope_guid,
				tolerance: "pointer",
				greedy: true,

				accept: function (draggable) {
					if (self._wijgrid.options.allowColMoving) {
						draggedColumn = self._getColumnInstance(draggable);

						if (draggedColumn) {
							return draggedColumn._canDropToGroupArea();
						}
					}
					return false;
				},

				drop: function (e: JQueryEventObject, ui: JQueryUI.DroppableEventUIParam) {
					if (!self._isInElement(e, ui.draggable) && (draggedColumn = self._getColumnInstance(ui.draggable))) {
						self._dragEnd = true;
					}
				},

				over: function (e: JQueryEventObject, ui: JQueryUI.DroppableEventUIParam) {
					var cnt = self._wijgrid._groupAreaColumns().length;

					self._dropTargetRedirected = (cnt > 0);
					self._droppableColumn = (cnt > 0)
					? self._wijgrid._groupAreaColumns()[cnt - 1] // use the rightmost header as a drop target
					: <any>element; // special case, the drop target is the group area itself

					element.data("thisDroppableWijField", self._droppableColumn);
				},

				out: function (e: JQueryEventObject, ui: JQueryUI.DroppableEventUIParam) {
					if (self._droppableColumn === element.data("thisDroppableWijField")) {
						self._droppableColumn = null;
					}
				}
			})
		}

		attach(column: IDragnDropElement) {
			var element: JQuery,
				draggedColumn: c1basefield,
				defCSS = wijmo.grid.wijgrid.CSS,
				self = this;

			if (!(<any>$.ui).droppable || !(<any>$.ui).draggable) {
				return;
			}

			if (!column || !(element = column.element)) {
				return;
			}

			element
				.draggable({
					helper: function (e: JQueryEventObject) {
						var result: JQuery;

						if (column instanceof c1groupedfield) {
							result = element
								.clone();
						} else {
							result = element
								.clone()
								.wrap(self._wrapHtml)
								.width(element.width())
								.height(element.height())
								.closest("." + defCSS.wijgrid);
						}

						result
							.addClass(defCSS.dndHelper + " " + self._wijCSS.wijgridDndHelper)
							.css("z-index", self.mZIndex = wijmo.grid.getZIndex(self._wijgrid.mOuterDiv, 1000)); // Update the z-index property. wijdialog increases its z-index property every time when the dialog position is changed.

						return result;
					},

					appendTo: "body",
					//cursor: "pointer",
					scope: self._scope_guid,

					drag: function (e: JQueryEventObject, ui: JQueryUI.DraggableEventUIParams) {
						self._hideArrows();

						if (self._droppableColumn && !self._isInElement(e, element)) {
							// indicate insertion position

							var $arrowsTarget = self._droppableColumn.element;
							if (!$arrowsTarget) { // _droppableWijField is the group area element
								$arrowsTarget = (<JQuery><any>self._droppableColumn);
							}

							self._showArrows($arrowsTarget, self._getPosition(column, self._droppableColumn, e, ui));
						}
					},

					start: function (e: JQueryEventObject, ui: JQueryUI.DraggableEventUIParams) {
						if (self._wijgrid._canInteract() && self._wijgrid.options.allowColMoving && (self._wijgrid._UIResizer() == null || !self._wijgrid._UIResizer().inProgress())) {

							var dragColumn = column, // default
								dragInGroup = (column instanceof c1groupedfield),
								dragSource = dragInGroup ? "groupArea" : "columns";

							if (dragInGroup) {
								dragColumn = self._wijgrid._findInstance(<IColumn>column.options);
							}

							var opt = <IColumn>dragColumn.options;

							if (column._canDrag() && self._wijgrid._trigger("columnDragging", null, <IColumnDraggingEventArgs>{ drag: opt, dragSource: dragSource })) {
								self._wijgrid._trigger("columnDragged", null, <IColumnDraggedEventArgs>{ drag: opt, dragSource: dragSource });
								return true;
							}
						}

						return false;
					},

					stop: function (e: JQueryEventObject, ui: JQueryUI.DraggableEventUIParams) {
						self._hideArrows();

						try {
							if (self._dragEnd) {
								if (!self._droppableColumn.element) { // _droppableWijField is the group area element
									self._wijgrid._handleDragnDrop(column,
										null,
										"left",
										column instanceof c1groupedfield,
										true
										);
								} else {
									self._wijgrid._handleDragnDrop(column,
										self._droppableColumn,
										self._getPosition(column, self._droppableColumn, e, ui),
										column instanceof c1groupedfield,
										self._droppableColumn instanceof c1groupedfield
										);
								}
							}
						}
						finally {
							self._droppableColumn = null;
							self._dragEnd = false;
						}
					}
				}) // ~draggable

				.droppable({
					hoverClass: self._wijCSS.stateHover,
					scope: self._scope_guid,
					tolerance: "pointer",
					greedy: true,

					accept: function (draggable: JQuery) {
						if (self._wijgrid.options.allowColMoving) {
							if (element[0] !== draggable[0]) { // different DOM elements
								draggedColumn = self._getColumnInstance(draggable); // dragged column

								if (draggedColumn) {
									return draggedColumn._canDropTo(column);
								}
							}
						}
						return false;
					},

					drop: function (e: JQueryEventObject, ui: JQueryUI.DroppableEventUIParam) {
						if (draggedColumn = self._getColumnInstance(ui.draggable)) {
							// As droppable.drop fires before draggable.stop, let draggable to finish the action.
							// Otherwise exception is thrown as during re-rendering element bound to draggable will be already deleted.
							self._dragEnd = true;

							// an alternative:
							//window.setTimeout(function () {
							//wijgrid._handleDragnDrop(draggedWijField, wijField, _getPosition(draggedWijField, wijField, e, ui));
							//}, 100);
						}
					},

					over: function (e: JQueryEventObject, ui: JQueryUI.DroppableEventUIParam) {
						self._dropTargetRedirected = false;
						self._droppableColumn = column;

						// to track when droppable.over event of other element fires before droppable.out of that element.
						element.data("thisDroppableWijField", self._droppableColumn);
					},

					out: function (e: JQueryEventObject, ui: JQueryUI.DroppableEventUIParam) {
						if (self._droppableColumn === column.element.data("thisDroppableWijField")) {
							self._droppableColumn = null;
						}
					}
				}); // ~droppable
		}

		detach(column: IDragnDropElement) {
			var element: JQuery;

			if (column && (element = column.element)) {
				if (element.data("ui-draggable")) {
					element.draggable("destroy");
				}

				if (element.data("ui-droppable")) {
					element.droppable("destroy");
				}
			}
		}

		dispose() {
			if (this._$topArrow) {
				this._$topArrow.remove();
				this._$topArrow = null;
			}

			if (this._$bottomArrow) {
				this._$bottomArrow.remove();
				this._$bottomArrow = null;
			}
		}


		// private
		private _getColumnInstance(draggable: JQuery): c1basefield {
			var instance = draggable.data(widgetEmulator.DATA_INSTANCE_PROP);

			if (!instance) {
				throw "Unable to get a column instance";
			}

			return instance;
		}

		// position: "left", "right", "center"
		private _showArrows(element: JQuery, position: string): void {
			this._topArrow()
				.css("z-index", this.mZIndex)
				.show()
				.position({
					my: "center",
					at: position + " top",
					of: element,
					collision: "none"
				});

			this._bottomArrow()
				.css("z-index", this.mZIndex)
				.show()
				.position({
					my: "center",
					at: position + " bottom",
					of: element,
					collision: "none"
				});
		}

		private _hideArrows(): void {
			this._topArrow().hide();
			this._bottomArrow().hide();
		}

		private _topArrow(): JQuery {
			if (!this._$topArrow) {
				this._$topArrow = $("<div />")
					.addClass(wijmo.grid.wijgrid.CSS.dndArrowTopContainer + " " + this._wijCSS.wijgridDndArrowTopContainer)
					.append($("<span />").addClass(this._wijCSS.icon + " " + this._wijCSS.iconArrowThickDown))
					.hide()
					.appendTo(document.body);
			}

			return this._$topArrow;
		}

		private _bottomArrow(): JQuery {
			if (!this._$bottomArrow) {
				this._$bottomArrow = $("<div />")
					.addClass(wijmo.grid.wijgrid.CSS.dndArrowBottomContainer + " " + this._wijCSS.wijgridDndArrowBottomContainer)
					.append($("<span />").addClass(this._wijCSS.icon + " " + this._wijCSS.iconArrowThickUp))
					.hide()
					.appendTo(document.body);
			}

			return this._$bottomArrow;
		}

		private _isInElement(e: JQueryEventObject, element: JQuery): boolean {
			var bounds = wijmo.grid.bounds(element, false);
			return ((e.pageX > bounds.left && e.pageX < bounds.left + bounds.width) && (e.pageY > bounds.top && e.pageY < bounds.top + bounds.height));
		}

		private _getPosition(drag: IDragnDropElement, drop: IDragnDropElement, e: JQueryEventObject, ui: JQueryUI.DraggableEventUIParams): string {
			if (!drop.element) { // drop is the group area element
				return "left";
			}

			if (this._dropTargetRedirected) {
				return "right";
			}

			var bounds = wijmo.grid.bounds(drop.element, false),
				sixth = bounds.width / 6,
				centerX = bounds.left + (bounds.width / 2),
				result = "right",
				distance;

			if (e.pageX < centerX) {
				result = "left";
			}

			if (drop instanceof c1groupedfield) { // drag is moved over a grouped column
				if (drag instanceof c1groupedfield) { // drag is a grouped column too
					distance = drop.options.groupedIndex - drag.options.groupedIndex;

					if (Math.abs(distance) === 1) {
						result = (distance < 0)
						? "left"
						: "right";
					}
				}

				return result;
			}

			// both drag and drop are non-grouped columns
			distance = drop.options._linearIdx - drag.options._linearIdx;

			if (c1bandfield.test(<IColumn>drop.options) &&
				(drag.options._parentIdx !== drop.options._travIdx) && // drag is not an immediate child of drop
				(Math.abs(e.pageX - centerX) < sixth)) {
				return "center";
			}

			// drag and drop are contiguous items of the same level
			if (drag.options._parentIdx === drop.options._parentIdx && Math.abs(distance) === 1) {
				result = (distance < 0)
				? "left"
				: "right";
			}

			return result;
		}
		// ~private
	}
}
