﻿/// <reference path="../../../Base/jquery.wijmo.widget.ts"/>
/// <reference path="../../../wijutil/jquery.wijmo.wijutil.ts"/>

/// <reference path="interfaces.ts" />
/// <reference path="bands_traversing.ts" />
/// <reference path="misc.ts"/>
/// <reference path="cellInfo.ts"/>
/// <reference path="selection.ts"/>
/// <reference path="cellEditorHelper.ts"/>
/// <reference path="cellFormatterHelper.ts"/>
/// <reference path="cellStyleFormatterHelper.ts"/>
/// <reference path="rowStyleFormatterHelper.ts"/>
/// <reference path="fixedView.ts"/>
/// <reference path="flatView.ts"/>
/// <reference path="dataViewWrapper.ts"/>
/// <reference path="filterOperators.ts"/>
/// <reference path="columnsGenerator.ts"/>
/// <reference path="rowAccessor.ts"/>
/// <reference path="grouper.ts"/>
/// <reference path="renderBoundsCollection.ts"/>

/// <reference path="uiDragndrop.ts"/>
/// <reference path="uiSelection.ts"/>
/// <reference path="uiResizer.ts"/>
/// <reference path="uiFrozener.ts"/>

/// <reference path="c1field.ts"/>
/// <reference path="c1band.ts"/>
/// <reference path="c1buttonfield.ts"/>

/// <reference path="sketchTable.ts"/>

/// <reference path="hierarchyBuilder.ts"/>

/// <reference path="../../../data/src/core.ts"/>
/// <reference path="../../../data/src/dataView.ts"/>
/// <reference path="../data/converters.ts"/>
/// <reference path="../data/koDataView.ts"/> // add reference, otherwise it will be not added to the grunt output.

/// <reference path="../../../wijpager/jquery.wijmo.wijpager.ts"/>

module wijmo.grid {


    var $ = jQuery;

    declare var wijdatasource;

    /** @ignore */
    export function extendWidgetOptions(baseOptions, newOptions) {
        var result: any = $.extend(true, {}, baseOptions, newOptions);

        delete result.constructor; // Remove the constructor because the widget.options object is a ts class now (widgetName_options).

        return result;
    }

	/** @widget
	* Represents the wijgrid widget.
	*/
    export class wijgrid extends wijmoWidget implements IParseHandler {
        static WIN_RESIZE_TIMEOUT = 200; // public property!

        /** @ignore */
        static IGNORE_WIN_RESIZE = false; // global, to avoid issue #72351.

        static JQDATA_MASTER_INFO_KEY = "masterinfo";

        /** @ignore */
        static CSS: wijmo.grid.wijgridCSS = {
            wijgrid: "wijmo-wijgrid",

            root: "wijmo-wijgrid-root",
            editedCellMarker: "wijmo-wijgrid-cell-edit",
            outerDivMarker: "wijgridouterdiv",
            inputMarker: "wijgridinput",
            spinnerMarker: "wijgridspinner",

            table: "wijmo-wijgrid-table",
            TH: "wijgridth",
            TD: "wijgridtd",
            cellContainer: "wijmo-wijgrid-innercell",
            aggregateContainer: "wijmo-wijgrid-aggregate",

            rowHeader: "wijmo-wijgrid-rowheader",

            currentRowHeaderCell: "wijmo-wijgrid-current-rowheadercell",
            currentHeaderCell: "wijmo-wijgrid-current-headercell",
            currentCell: "wijmo-wijgrid-current-cell",

            cellAlignLeft: "wijalign-left",
            cellAlignRight: "wijalign-right",
            cellAlignCenter: "wijalign-center",

            filterList: "wijmo-wijgrid-filterlist",
            filter: "wijmo-wijgrid-filter",
            filterInput: "wijmo-wijgrid-filter-input",
            filterTrigger: "wijmo-wijgrid-filter-trigger",
            filterNativeHtmlEditorWrapper: "wijgrid-input-wrapper",

            headerArea: "wijmo-wijgrid-header",
            footerArea: "wijmo-wijgrid-footer",

            headerRow: "wijmo-wijgrid-headerrow",
            row: "wijmo-wijgrid-row",
            dataRow: "wijmo-wijgrid-datarow",
            altRow: "wijmo-wijgrid-alternatingrow",
            emptyDataRow: "wijmo-wijgrid-emptydatarow",
            filterRow: "wijmo-wijgrid-filterrow",
            groupHeaderRow: "wijmo-wijgrid-groupheaderrow",
            groupFooterRow: "wijmo-wijgrid-groupfooterrow",
            groupHeaderRowCollapsed: "wijmo-wijgrid-groupheaderrow-collapsed",
            groupHeaderRowExpanded: "wijmo-wijgrid-groupheaderrow-expanded",
            footerRow: "wijmo-wijgrid-footerrow",
            detailRow: "wijmo-wijgrid-hierarchy-detailrow",

            loadingOverlay: "wijmo-wijgrid-overlay",
            loadingText: "wijmo-wijgrid-loadingtext",

            groupArea: "wijmo-wijgrid-group-area",
            groupAreaEmpty: "wijmo-wijgrid-group-area-empty",
            groupAreaButton: "wijmo-wijgrid-group-button",
            groupAreaButtonSort: "wijmo-wijgrid-group-button-sort",
            groupAreaButtonClose: "wijmo-wijgrid-group-button-close",
            groupToggleVisibilityButton: "wijmo-wijgrid-grouptogglebtn",

            fixedView: "wijmo-wijgrid-fixedview",
            scroller: "wijmo-wijgrid-scroller",
            scrollableContent: "wijmo-wijgrid-scrollable-content",

            dndHelper: "wijmo-wijgrid-dnd-helper",
            dndArrowTopContainer: "wijmo-wijgrid-dnd-arrow-top",
            dndArrowBottomContainer: "wijmo-wijgrid-dnd-arrow-bottom",

            freezingHandleV: "wijmo-wijgrid-freezing-handle-v",
            freezingHandleH: "wijmo-wijgrid-freezing-handle-h",
            freezingHandleContent: "wijmo-wijgrid-freezing-handle-content",

            resizingHandle: "wijmo-wijgrid-resizehandle",

            headerCellSortIcon: "wijmo-wijgrid-sort-icon",
            headerCellText: "wijmo-wijgrid-headertext",

            detailContainerCell: "wijgrid-detail-cell",

            c1basefield: "wijmo-c1basefield",
            c1band: "wijmo-c1band",
            c1field: "wijmo-c1field"
        };

        public options: IWijgridOptions;

        public mAdjustHeaderText: boolean; // overridden in c1gridview.
        public mCustomSortOrder: number;
        public mPageSizeKey: number;
        public mDataPrefix: string;
        public mWijDataView: wijmo.data.IDataView;
        public mOuterDiv: JQuery;
        public mSketchTable: wijmo.grid.SketchTable;
        public mCellFormatter: wijmo.grid.cellFormatterHelper;
        public mRowStyleFormatter: wijmo.grid.rowStyleFormatterHelper;
        public mCellStyleFormatter: wijmo.grid.cellStyleFormatterHelper;
        public mDataOffset: number = 0;
        public mScrollingState: IScrollingState; // h. pos, v. pos, virtual scroll index
        public mRowOuterHeight = -1; // for virtual scrolling.
        public mDestroyed: boolean;
        public mAutoHeight: boolean;
        public mResizeOnWindowResize: boolean;
        public mSuperPanelHeader: JQuery;
        public mTopPagerDiv: JQuery;
        public mBottomPagerDiv: JQuery;
        public mSplitDistanceX: number;
        public mSplitDistanceY: number;
        public mRenderCounter: number;
        public mDataViewWrapper: wijmo.grid.dataViewWrapper;
        public mDeficientFilters: { [id: string]: wijmo.data.IFilterDescriptor; }; // stores the deficient filters (one of the filterOperator\ filterValue values is unknown) during the dataView round trip by the dataKey.

        // private fields **
        private mAllowVVirtualScrolling: boolean;
        private mAllowHVirtualScrolling: boolean;
        private mViewPortBounds: IRenderBounds;
        private mRenderableColumns: renderableColumnsCollection; // the range of the columns to render, mapped over the _leaves() indicies.
        private mRenderableRows: wijmo.grid.renderableRowsCollection; // contains all visible (both rendered and not) rows
        private mView: wijmo.grid.baseView;

        private _initialized: boolean;
        private _rendered: boolean;
        private _eventUID: string;
        private _originalHtml: string;
        private _originalAttr: any;
        private _originalCssText: string;
        private _windowResizeTimer: number = 0;
        private mClosestCulture: GlobalizeCulture;
        private _prevHoveredSketchRowIndex: number;

        private _resizerui: uiResizer;
        private _frozenerui: uiFrozener;
        private _selectionui: uiSelection;
        private _dragndropui: uiDragndrop;

        private mAutoWidth: boolean;
        private _winOrientation: number; // to resolve Android issue #64466
        private _isHeightTemporaryFixed: boolean;

        private $groupArea: JQuery;
        private mUID: any;
        private _spinnerActivated: boolean;

        private mEditSkechRowIndex = -1;

        private mKeyDownEventListener: keyDownEventListener;
        private mCurrentCellLocker = false;

        private mCurrentCell: cellInfo;

        private mAllColumns: c1basefield[]; // all columns, traverse list
        private mLeaves: c1basefield[]; // all leaves 
        private mVisibleLeaves: c1basefield[]; // visible leaves only
        private mVirtualLeaves: c1basefield[]; // virtual leaves (row headers) only. A subset of the visible leaves.
        private mRenderedLeaves: c1basefield[]; // visible leaves only

        private mGroupedLeaves: c1field[];
        private mGroupAreaColumns: c1groupedfield[];

        private mSelection: wijmo.grid.selection;
        private mTFoot: any[];
        private mTHead: any[];
        private mCurrentCellPrevCycle: { x: number; y: number; dataItemIndex: number; };

        private mDetails: hierarchyNode[];
        public mLoadingDetails: number;
        private mMasterInfo: IMasterInfo;
        private mIgnoreSizing: boolean;

        // * override		
        _createWidget(options: IWijgridOptions, element: Element) {
            // A fix for the case if options.data contains a complex object leading to stack overflow when 
            // 1. $.extend is called in the widget factory 
            // 2. wijmoASPNetParseOptions is  called in the _create method.
            // Clear the data options (grid + details) and persist them within the element using $.data().
            wijmo.grid.DataOptionPersister.persistAndClearIntoElement(element, options);

            super._createWidget.apply(this, arguments);

            wijmo.grid.DataOptionPersister.restoreFromElement(this.element[0], options, true); // restore the .data option(s) back.
        }

        _create(): void {
            var self = this;

            if (!this.element.is("table")) {
                throw "invalid markup";
            }

            this.mAdjustHeaderText = true;
            this.mSplitDistanceX = 0;
            this.mSplitDistanceY = 0;
            this.mCurrentCellLocker = false;
            this._windowResizeTimer = 0;
            this.mDataOffset = 0;
            this.mScrollingState = this.mScrollingState || { x: null, y: null, index: 0, scrollLeft: 0 };
            this.mRowOuterHeight = -1;
            this._initialized = false;
            this.mDestroyed = false;
            this._rendered = false;
            this._eventUID = undefined;;
            this.mDataViewWrapper = undefined;
            this._originalHtml = undefined;
            this._originalAttr = undefined;
            this._originalCssText = undefined;
            this.mAutoHeight = undefined;
            this.mAutoWidth = undefined;
            this.mRenderCounter = 0;
            this.mSuperPanelHeader = undefined;
            this.mTopPagerDiv = undefined;
            this.mBottomPagerDiv = undefined;
            this.$groupArea = undefined;
            this.mUID = undefined;
            this.mWijDataView = undefined;
            this.mOuterDiv = undefined;
            this.mSketchTable = undefined;
            this.mCellFormatter = undefined;
            this.mRowStyleFormatter = undefined;
            this.mCellStyleFormatter = undefined;
            this._eventUID = wijmo.grid.getUID();
            this._spinnerActivated = false;
            this.mDeficientFilters = {};
            this.mRenderableColumns = new renderableColumnsCollection();

            this.mDetails = [];
            this.mLoadingDetails = 0;

            // TFS: 407417
            if (!this.mScrollingState.scrollLeft) this.mScrollingState.scrollLeft = 0;

            if (this._isDetail()) {
                this.mMasterInfo = this.element.data(wijgrid.JQDATA_MASTER_INFO_KEY);
                this.element.removeData(wijgrid.JQDATA_MASTER_INFO_KEY);
                this.mIgnoreSizing = true;
            }

            // Note: the data options (grid + details) are empty on this stage (see _createWidget() method)
            this.options = <any>$.extend(true, {}, this.options); // jQuery UI 1.9.0 fix

            if ($.isFunction(window["wijmoASPNetParseOptions"])) { // handle juice objectValue serialize
                window["wijmoASPNetParseOptions"](this.options);
            }

            wijmo.grid.DataOptionPersister.restoreFromElement(this.element[0], this.options); // restore the .data option(s)

            // enable touch support:
            if (window.wijmoApplyWijTouchUtilEvents) {
                $ = (<any>window).wijmoApplyWijTouchUtilEvents($);
            }

            this._initialized = false;
            this.mDestroyed = false;

            // culture
            this.mClosestCulture = $.wijGetCulture(this.options.culture, this.options.cultureCalendar);

            // initialize data
            this.mDataViewWrapper = new wijmo.grid.dataViewWrapper(this);

            this._originalHtml = this.element.html(); // store original html. Will be restored in the destroy() method.
            this._originalAttr = {};
            this._originalCssText = this.element[0].style.cssText;

            this.element.addClass(wijgrid.CSS.root);
            this.element.wrap("<div class=\"" + this.options.wijCSS.widget + " " + wijgrid.CSS.wijgrid + " " + this.options.wijCSS.wijgrid + " " + wijgrid.CSS.outerDivMarker + " " + this.options.wijCSS.content + " " + this.options.wijCSS.cornerAll + "\"></div>"); // outer div
            this.mOuterDiv = this.element.parent();

            var styleHeight = this.element[0].style.height,
                styleWidth = this.element[0].style.width;

            if (this._isDetail()) {
                var opt = <IDetailSettings>this.options;
                styleHeight = opt.height || (opt.height === 0) ? opt.height + "" : styleHeight;
                styleWidth = opt.width || (opt.width === 0) ? opt.width + "" : styleWidth;
            }

            if (styleHeight) {
                this.mOuterDiv.css("height", styleHeight);
            }

            if (styleWidth) {
                this.mOuterDiv.css("width", styleWidth);
            }

            this.mAutoHeight = (styleHeight == "" || styleHeight == "auto");
            this.mAutoWidth = (styleWidth == "" || styleWidth == "auto");
            this.mResizeOnWindowResize = this.mAutoHeight || ((styleHeight + "").indexOf("%") > 0) || this.mAutoWidth || ((styleWidth + "").indexOf("%") > 0);

            this.element.css({ "height": "", "width": "" });

            if (this.options.disabled) {
                this.disable();
            }

            // formatters
            this.mCellFormatter = new wijmo.grid.cellFormatterHelper();
            this.mRowStyleFormatter = new wijmo.grid.rowStyleFormatterHelper(this);
            this.mCellStyleFormatter = new wijmo.grid.cellStyleFormatterHelper(this);

            // * set bounds
            this._viewPortBounds({ start: 0, end: 0, startX: 0, endX: 0 });

            if (this._allowVVirtualScrolling()) {
                this._viewPortBounds().start = this.mScrollingState.index; // == 0 by default.

                if (this._serverSideVirtualScrolling()) {
                    this.mDataOffset = this.mScrollingState.index;
                }
            }
            // set bounds *

            // wijObservable
            if (this.element.wijAddVisibilityObserver) {
                this.element.wijAddVisibilityObserver(function (e: JQueryEventObject) {
                    if (self._initialized && !self.mDestroyed && (e.target !== self.mOuterDiv[0]) && self.element.is(":visible")) { // ignore notification triggered by baseView,
                        self.setSize();
                    }
                }, "wijgrid");
            }

            this._winOrientation = wijmo.grid.getWindowOrientation();

            this.mRenderCounter = 0;
        }

        _destroy(): void {
            var self = this;

            try {
                if (this.element.wijRemoveVisibilityObserver) {
                    this.element.wijRemoveVisibilityObserver();
                }

                this._view().dispose();

                this._detachEvents(true);

                if (this._resizerui) {
                    this._resizerui.dispose();
                    this._resizerui = null;
                }

                if (this._frozenerui) {
                    this._frozenerui.dispose();
                    this._frozenerui = null;
                }

                if (this._selectionui) {
                    this._selectionui.dispose();
                    this._selectionui = null;
                }

                if (this._dragndropui) {
                    this._dragndropui.dispose();
                    this._dragndropui = null;
                }

                this.mDataViewWrapper.dispose();

                if (this.mCellFormatter) {
                    this.mCellFormatter.dispose();
                    this.mCellFormatter = null;
                }

                // cleanup $data
                wijmo.grid.remove$dataByPrefix(this.element, this.mDataPrefix);

                // ** restore original content

                // restore content and destroy children widgets + data.
                this.element.insertBefore(this.mOuterDiv);
                this.mOuterDiv.remove();
                this.element.html(this._originalHtml);

                // restore attributes
                $.each(this._originalAttr, function (key, value) {
                    if (value === undefined) {
                        self.element.removeAttr(key);
                    } else {
                        self.element.attr(key, value);
                    }
                });

                this.element.removeClass(wijmo.grid.wijgrid.CSS.root);
                this.element[0].style.cssText = this._originalCssText; // restore style properties
                // restore original content **
            }
            finally {
                this.mDestroyed = true;
            }
        }

        _init(): void {
            this.mSuperPanelHeader = null;
            this.mTopPagerDiv = null;
            this.mBottomPagerDiv = null;
            this.$groupArea = null;

            this._prevHoveredSketchRowIndex = -1;

            // culture
            this.mClosestCulture = $.wijGetCulture(this.options.culture, this.options.cultureCalendar);

            if (!this.options.data) { // dataSource is a domTable
                if (!this.mTHead) {
                    this.mTHead = this._readTableSection(this.element, wijmo.grid.rowScope.head, this.options.readAttributesFromData);
                }

                if (!this.mTFoot) {
                    this.mTFoot = this._readTableSection(this.element, wijmo.grid.rowScope.foot, this.options.readAttributesFromData);
                }
            }

            this._initialized = this._initialized || false; // to avoid reinitialization.

            if (this._isDetail()) {
                this._validateRelation((<IDetailSettings>this.options).relation);
            }

            this.ensureControl(true);
        }

        _setOption(key, value): void {
            var presetFunc = this["_preset_" + key],
                oldValue = this.options[key],
                optionChanged, postsetFunc;

            if (presetFunc !== undefined) {
                value = presetFunc.apply(this, [value, oldValue]);
            }

            optionChanged = (value !== oldValue) ||
                (key.toLowerCase() === "data") ||
                (key.toLowerCase() === "columns"); // same reference, handle a situation when the option.columns array was modifed directly. (TFS issue #41296)

            //$.Widget.prototype._setOption.apply(this, arguments); note: there is no dynamic linkage between the arguments and the formal parameter values when strict mode is used
            super._setOption.apply(this, [key, value]); // update this.options

            if (optionChanged) {
                postsetFunc = this["_postset_" + key];
                if (postsetFunc !== undefined) {
                    postsetFunc.apply(this, [value, oldValue]);
                }
            }
        }
        // * override

        // * public
		/** Returns a one-dimensional array of widgets bound to visible column headers.
		* @example
		* var colWidgets = $("#element").wijgrid("columns");
		* @remarks
		* wijgrid columns are represented as widgets. This method returns a one-dimensional array of widgets that are bound to visible column headers.
		*
		* The column widget is initiated with values taken from the corresponding item in the wijgrid.options.columns array. However, the options of a column widget instance reference not the original object but a copy created by the widget factory. Due to that, changes to the wijgrid.options.columns options are not automatically propagated to the column widget options and vice versa.
		* To solve this issue, the wijgrid synchronized the column widget option values with the source items. This synchronization occurs inside the ensureControl() method which is automatically called at each action requiring the wijgrid to enter.
		*
		* Still, there is a drawback. For example, a user may want to filter wijgrid data from user code as in this sample:
		*
		*	$("#element").wijgrid("option", "columns")[0].filterValue = "newValue";
		*	$("#element").wijgrid("ensureControl", true); // make wijgrid re-shape data and re-render.
		*
		* In the sample above, nothing will happen since at synchronization user changes will be ignored.You need to change the filterValue of a column widget. This is what the columns() method is for:
		*
		*	$("#element").wijgrid("columns")[0].options.filterValue = "newValue";
		*	$("#element").wijgrid("ensureControl", true); // make wijgrid re-shape data and re-render.
		*
		* Here's the best way to change the filterValue:
		* 
		*	$("#element").wijgrid("columns")[0].option("filterValue", "newValue"); // column widget handles all the needful.
		*
		* @returns {Object[]} A one-dimensional array of widgets bound to visible column headers.
		*/
        columns(): c1basefield[] {
            return this.mVisibleLeaves || [];
        }

        _setColumns(value: c1basefield[]) {
            this.mVisibleLeaves = value;
        }

		/** Gets the current cell for the grid.
		* @example
		* var current = $("#element).wijgrid("currentCell");
		* @returns {wijmo.grid.cellInfo} Object that represents current cell of the grid.
		*/
        currentCell(): wijmo.grid.cellInfo; // getter
		/** Sets the current cell for the grid.
		* @example
		* $("#element).wijgrid("currentCell", new wijmo.grid.cellInfo(0, 0));
		* @remarks If you wish to hide the current cell, use (-1, -1) for the value.
		* @param {wijmo.grid.cellInfo} cellInfo Object that represents a single cell.
		* @returns {wijmo.grid.cellInfo} Object that represents current cell of the grid.
		*/
        currentCell(cellInfo: wijmo.grid.cellInfo); // setter 1
		/** Sets the current cell for the grid.
		* @example
		* $("#element).wijgrid("currentCell", 0, 0);
		* @remarks If you wish to hide the current cell, use (-1, -1) for the value.
		* @param {number} cellIndex Zero-based index of the required cell inside the corresponding row.
		* @param {number} rowIndex Zero-based index of the row that contains required cell.
		* @returns {wijmo.grid.cellInfo} Object that represents current cell of the grid.
		*/
        currentCell(cellIndex: number, rowIndex: number); // setter 2

        /** @ignore */
        currentCell(a, b, changeSelection: boolean);

        /** @ignore */
        currentCell(a?, b?, changeSelection?: boolean): any {
            var currentCell: wijmo.grid.cellInfo,
                view = this._view(),
                rows = this._rows();

            if (arguments.length === 0) { // getter
                currentCell = this._currentCell();
                if (!currentCell) {
                    this._currentCell(currentCell = wijmo.grid.cellInfo.outsideValue);
                }
                return currentCell;
            } else { // setter
                //currentCell = (arguments.length === 1)
                //	? (<wijmo.grid.cellInfo>a)._clone()
                //	: new wijmo.grid.cellInfo(<number>a, <number>b);

                currentCell = (typeof (a) !== "number")
                    ? (<wijmo.grid.cellInfo>a)._clone()
                    : new wijmo.grid.cellInfo(<number>a, <number>b);

                if (!currentCell.isEqual(wijmo.grid.cellInfo.outsideValue)) {
                    if (!currentCell._isValid()) {
                        throw "invalid arguments";
                    }

                    currentCell._clip(this._getDataCellsRange(dataRowsRangeMode.sketch));

                    if (currentCell.rowIndex() >= 0) {
                        var rowInfo = view._getRowInfoBySketchRowIndex(currentCell.rowIndex());
                        if (!rowInfo || !(rowInfo.type & wijmo.grid.rowType.data)) {
                            return;
                        }
                    }
                }

                currentCell._setGridView(this);

                this._changeCurrentCell(null, currentCell, { changeSelection: changeSelection || false, setFocus: false });

                return this._currentCell();
            }
        }

		/** Gets an array of underlying data.
		* @example
		* var data = $("#element").wijgrid("data");
		* @returns {object[]} An array of underlying data.
		*/
        data(): any[] {
            //return this._dataViewWrapper.dataView()();
            return this.mDataViewWrapper.dataView().getSource();
        }

		/** Gets an underlying wijdataview instance.
		* @example
		* var dataView = $("#element").wijgrid("dataView");
		* @returns {wijmo.data.IDataView} An underlying wijdataview instance.
		*/
        dataView(): wijmo.data.IDataView {
            return this.mDataViewWrapper.dataView();
        }

        _isRoot(): boolean {
            return !this._isDetail();
        }

        _isDetail(): boolean {
            return this.options._isDetail === true;
        }

        _clearDetails() {
            if (this.mDetails) {
                this.mDetails.splice(0, this.mDetails.length);
            }
        }

        /** Returns an array which consists of hierarchy nodes in wijgrid. */
        details(): hierarchyNode[] {
            if (!this.mDetails) {
                this.mDetails = [];
            }

            return this.mDetails;
        }

		/** Re-renders wijgrid.
		* @example
		* $("#element").wijgrid("doRefresh");
		* @param {Object} userData Infrastructure, not intended to be used by user.
		*/
        doRefresh(userData?: IUserDataArgs): void {

            if (!$.isPlainObject(userData)) {
                userData = {};
            }

            var self = this,
                virtualRefresh = userData && userData.virtualScrollData;

            if (!this._initialized) {
                try {
                    this._prepareColumns(this.options.columns, this.options.columnsAutogenerationMode, this.mDataViewWrapper.getFieldsInfo(), true, true); // prepare static and dynamic columns
                }
                catch (e) {
                    throw e;
                }
                finally {
                    //ownerise the column for bug 16936, 17079
                    this._initialized = true;
                }
            }

            if (!virtualRefresh) { // do not rebuild leaves during virtual scrolling callback
                this._rebuildLeaves(); // build leaves, visible leaves, set dataIndex etc

                var dataSlice = lazy(this.mDataViewWrapper.data, this.mDataViewWrapper),
                    dataView = this.mDataViewWrapper.dataView();

                var totalsRequest = this._prepareTotalsRequest();
                if (totalsRequest.length) {
                    $.each(this._leaves(), (i: number, column: c1basefield) => { // copy totals
                        column.options._totalsValue = (dataSlice().totals)
                            ? dataSlice().totals[(<IColumn>column.options).dataKey]
                            : undefined;
                    });
                }

                // this._setPageCount(dataSlice);

                if (dataView.count()) { // process data items
                    var leaves = this._leaves();
                    var lazyTable = new wijmo.grid.LazySketchTable({
                        count: () => dataView.count(),
                        getRange: (start: number, count: number, dest: SketchRow[], offset: number) => {
                            for (var i = 0; i < count; i++) {
                                var index = start + i;
                                var dataItem = dataView.item(index);
                                dest[offset + i] = this._buildSketchRow(this.mDataViewWrapper._wrapDataItem(dataItem, index), leaves);
                            }
                        }
                    });
                    if (this._hasGrouping() || this._hasMerging() || this._hasDetail()) {
                        lazyTable.ensureNotLazy();
                    }
                    this.mSketchTable = lazyTable;
                } else {
                    this.mSketchTable = new wijmo.grid.SketchTable();
                    var emptyData = dataSlice().emptyData;
                    if (emptyData) { // process empty data row
                        $.each(emptyData, (i, item) => {
                            self.mSketchTable.add(self._buildSketchRowEmptyDataItem(item, this._leaves(), i === emptyData.length - 1));
                        });
                    }
                }
            }

            this._onRendering(userData);

            if (!virtualRefresh) {
                this._refresh(userData);
            } else {
                this._refreshVirtual(userData);
            }

            if (userData && $.isFunction(userData.beforeOnRendered)) {
                userData.beforeOnRendered.apply(this, [userData]);
            }

            var view = this._view();
            if (this.mEditSkechRowIndex >= 0 && (view._isRowRendered(this.mEditSkechRowIndex) >= 0)) {
                var rowInfo = view._getRowInfoBySketchRowIndex(this.mEditSkechRowIndex);
                if (rowInfo && (rowInfo.state & wijmo.grid.renderState.editing)) {
                    view._makeRowEditable(rowInfo);
                }
            }

            //Fixed TFS-443329-case2
            this.correctSketchRowsIfNeeded();

            this._onRendered(userData);

            if (userData && $.isFunction(userData.afterRefresh)) {
                userData.afterRefresh.apply(this, [userData]);
            }
        }

        private correctSketchRowsIfNeeded() {
            if (this.mAllowVVirtualScrolling || this.mAllowHVirtualScrolling) {
                var _view = this._view(),
                    rows = _view.bodyRows(),
                    visited = {}, i = 0;                
                while (i < rows.length()) {
                    var ri = _view._getRowInfo(rows.item(i));
                    if (visited[ri.sketchRowIndex]) {
                        _view._removeBodyRow(i);
                    } else {
                        visited[ri.sketchRowIndex] = true;
                        i++;
                    }                    
                }
            }
        }

		/** Puts the current cell into edit mode, as long as the editingMode options is set to "cell".
		* @example
		* $("#element").wijgrid({}
		*		editingMode: "cell",
		*		currentCellChanged: function (e, args) {
		*			if ($(e.target).wijgrid("option", "isLoaded")) {
		*				window.setTimeout(function () {
		*					$(e.target).wijgrid("beginEdit");
		*				}, 100);
		*			}
		*		}
		* });
		* @returns {Boolean} True if the cell is successfully put into edit mode, otherwise false.
		*/
        beginEdit(): boolean {
            if (!this._allowCellEditing()) {
                throw "Can be used only if the editingMode option is set to \"cell\".";
            }

            return this._beginEditInternal(null);
        }

		/** Finishes editing the current cell.
		* @example
		* // endEdit is being called from within the saveChanges function
		* function saveChanges() {
		*		$("#element").wijgrid("endEdit");
		* }
		* functon cancelChanges() {
		*		$("#element").wijgrid("endEdit", true);
		* }
		* @param {Boolean} reject An optional parameter indicating whether changes will be rejected (true) or commited (false). The default value is false.
		* @returns {Boolean} True if the editing was finished successfully, othewise false.
		*/
        endEdit(reject: boolean = false): boolean {
            return this._endEditInternal(null, reject);
        }

		/** Starts editing of the specified row, can only be used when the editingMode option is set to "row".
		* @example
		* $("#element").wijgrid("editRow", 0);
		* @param {Number} dataItemIndex Determines the data item to edit.
		*/
        editRow(dataItemIndex: number): void {
            if (this.options.editingMode !== "row") {
                throw "Can be used only if the editingMode option is set to \"row\".";
            }

            if (this.mEditSkechRowIndex >= 0) {
                this.cancelRowEditing(); // only one row can be edited at a time.
            }

            if (dataItemIndex >= 0) {
                var sketchRowIndex = - 1,
                    sketchRow: SketchRow;

                for (var i = 0; i < this.mSketchTable.count() && sketchRowIndex < 0; i++) {
                    sketchRow = this.mSketchTable.row(i);

                    if (sketchRow.isDataRow() && ((sketchRow.dataItemIndex()) === dataItemIndex)) {
                        sketchRowIndex = i;
                    }
                }

                if (sketchRowIndex >= 0) {
                    var view = this._view();

                    this.mEditSkechRowIndex = sketchRowIndex;

                    sketchRow.renderState |= wijmo.grid.renderState.editing;

                    if (view._isRowRendered(this.mEditSkechRowIndex) >= 0) {
                        var row = view._getRowInfoBySketchRowIndex(this.mEditSkechRowIndex, false); // get a live IRowInfo object.

                        row.state |= wijmo.grid.renderState.editing; // ??

                        view._removeBodyRow(row.sectionRowIndex, true); // delete an exisiting row

                        var newRow = view._insertBodyRow(sketchRow, row.sectionRowIndex, row.dataItemIndex, row.virtualDataItemIndex, this.mEditSkechRowIndex); //  create and render a new one
                        newRow.dataRowIndex = row.dataRowIndex;

                        if (this._hasMerging()) {
                            this._view()._rebuildOffsets();
                        }

                        view._makeRowEditable(newRow);

                        this.selection()._ensureSelectionInRow(this.mEditSkechRowIndex);
                    }
                }
            }
        }

		/**
		* Exports the grid to a specified format. 
		* The export method only works when wijmo.exporter.gridExport's reference is on the page.
		* @remarks Possible exported types are: xls, xlsx, csv, pdf,
		* @param {string|Object} exportSettings
		* 1.The name of the exported file.
		* 2.Settings of exporting, should be conformed to wijmo.exporter.GridExportSetting
		* @param {string} type The type of the exported file.
		* @param {object} settings The export setting.
		* @param {string} serviceUrl The export service url.
		* @example
		* $("#element").wijgrid("exportGrid", "grid", "csv");
		*/
        exportGrid(exportSettings: any, type?: string, settings?: any, serviceUrl?: string) {

        }

		/** Finishes editing and updates the datasource.
		* @example
		* $("#element").wijgrid("updateRow");
		*/
        updateRow(): void {
            if (this.mEditSkechRowIndex >= 0) {
                var dataView: wijmo.data.IEditableDataView = wijmo.grid.asEditableDataView(this.mDataViewWrapper.dataView()),
                    errorOccurs = false;

                if (!dataView) {
                    throw "The provided DataView doesn't supports the editing operation.";
                }

                try {
                    var sketchRow = this.mSketchTable.row(this.mEditSkechRowIndex),
                        view = this._view();

                    if (!sketchRow || !sketchRow.isDataRow() || !(sketchRow.renderState & wijmo.grid.renderState.editing)) {
                        throw "Invalid row";
                    }

                    if (this._view()._isRowRendered(this.mEditSkechRowIndex) >= 0) {
                        var row = view._getRowInfoBySketchRowIndex(this.mEditSkechRowIndex, false); // get a live IRowInfo object.

                        // ** get the new values and update an underlying data source
                        var visibleLeaves = this._visibleLeaves(),
                            cells: cellInfo[] = [],
                            cellEditor = new wijmo.grid.cellEditorHelper();

                        for (var i = 0; i < visibleLeaves.length; i++) {
                            var column = visibleLeaves[i];

                            if ((column instanceof c1field) && !(<c1field>column).options.readOnly) {
                                var $cell = row.$rows.children("td, th").eq(i);

                                if ($cell.length) {
                                    var state = view._changeCellRenderState($cell, wijmo.grid.renderState.none, true); // dont change anything, just get a state.

                                    if (state & wijmo.grid.renderState.editing) { // make sure that cell is in edit state (the user did not cancel the beforeCellEdit event, for example)
                                        var cellInfo = view.getAbsoluteCellInfo(<HTMLTableCellElement>$cell[0], true),
                                            updateRes = cellEditor.updateCell(this, cellInfo, null, false);

                                        if (!(updateRes & updateCellResult.success)) {
                                            errorOccurs = true;
                                            return;
                                        }

                                        if (!(updateRes & updateCellResult.notEdited)) {
                                            cells.push(cellInfo);
                                        }
                                    }
                                }
                            }
                        }

                        for (var i = 0; i < cells.length; i++) {
                            cellEditor.cellEditEnd(this, cells[i], null);
                        }
                        // collect new values and update underlying data source **

                        // ** no errors, update DOM **
                        sketchRow.renderState &= ~wijmo.grid.renderState.editing;

                        row.state &= ~wijmo.grid.renderState.editing; // ??
                        view._removeBodyRow(row.sectionRowIndex, true); // delete an exisiting row

                        var newRow = view._insertBodyRow(sketchRow, row.sectionRowIndex, row.dataItemIndex, row.virtualDataItemIndex, this.mEditSkechRowIndex); //  create and render a new one
                        newRow.dataRowIndex = row.dataRowIndex;

                        this.selection()._ensureSelectionInRow(this.mEditSkechRowIndex);
                    } else {
                        sketchRow.renderState &= ~wijmo.grid.renderState.editing;
                    }
                } finally {
                    if (!errorOccurs) {
                        this.mEditSkechRowIndex = -1;
                    }
                }
            }
        }

		/** Discards changes and finishes editing of the edited row.
		* @example
		* $("#element").wijgrid("cancelRowEditing");
		*/
        cancelRowEditing(): void {
            if (this.mEditSkechRowIndex >= 0) {
                try {
                    var sketchRow = this.mSketchTable.row(this.mEditSkechRowIndex),
                        view = this._view();

                    if (!sketchRow || !sketchRow.isDataRow() || !(sketchRow.renderState & wijmo.grid.renderState.editing)) {
                        throw "Invalid row.";
                    }

                    sketchRow.renderState &= ~wijmo.grid.renderState.editing;

                    if (this._view()._isRowRendered(this.mEditSkechRowIndex) >= 0) {
                        var row = view._getRowInfoBySketchRowIndex(this.mEditSkechRowIndex, false); // get a live IRowInfo object.

                        row.state &= ~wijmo.grid.renderState.editing; // ??
                        view._removeBodyRow(row.sectionRowIndex, true); // delete an exisiting row

                        var newRow = view._insertBodyRow(sketchRow, row.sectionRowIndex, row.dataItemIndex, row.virtualDataItemIndex, this.mEditSkechRowIndex); //  create and render a new one
                        newRow.dataRowIndex = row.dataRowIndex;

                        if (this._hasMerging()) {
                            this._view()._rebuildOffsets();
                        }

                        this.selection()._ensureSelectionInRow(this.mEditSkechRowIndex);
                    }
                } finally {
                    this.mEditSkechRowIndex = -1;
                }
            }
        }

		/** Deletes the specified row.
		* @example
		* $("#element").wijgrid("cancelRowEditing");
		* @param {Number} dataItemIndex Determines the data item to edit.
		*/
        deleteRow(dataItemIndex: number): void {
            if (dataItemIndex >= 0) {
                var dataView: wijmo.data.IEditableDataView = wijmo.grid.asEditableDataView(this.mDataViewWrapper.dataView());

                if (!dataView || !dataView.canRemove()) {
                    throw "The provided DataView doesn't supports the deleting operation.";
                }

                this.mDataViewWrapper.ignoreChangeEvent(true);
                dataView.remove(dataItemIndex);
                this.mDataViewWrapper.ignoreChangeEvent(false);

                if (!this.mDataViewWrapper.isKODataView()) { // wijgrid will be refreshed automatically by the knockout.wijmo factory in case of KODataView.
                    this.ensureControl(true, { // forcibly reload a dataView.
                        forceDataLoad: true
                    });
                }
            }
        }

		/** Moves the column widget options to the wijgrid options and renders the wijgrid. Use this method when you need to re-render the wijgrid and reload remote data from the datasource.
		* @example
		* // Adds a new row to the viewModel and refreshes the wijgrid
		* var len = viewModel.data().length;
		* viewModel.data.push(new Person({ ID: len, Company: "New Company" + len, Name: "New Name" + len }));
		* $("#element").wijgrid("ensureControl", true);
		* @param {Boolean} loadData Determines if the wijgrid must load data from a linked data source before rendering.
		*/
        ensureControl(loadData: boolean, userData?: IUserDataArgs): void {
            this._loading();

            if (!$.isPlainObject(userData)) {
                userData = {
                    data: null,
                    afterRefresh: null,
                    beforeRefresh: null
                };
            }

            if (!this._initialized) {
                // this._prepareColumnOptions(false); // prepare static columns only (to make a proper sorting or filtering request to remote service)
                this._prepareColumns(this.options.columns, this.options.columnsAutogenerationMode,
                    this.mDataViewWrapper.isDataLoaded() && this.mDataViewWrapper.getFieldsInfo(),
                    this.mDataViewWrapper.isDataLoaded(),
                    false);

                if (!this.mDataViewWrapper.isOwnDataView()) {
                    // map sorting\ filtering\ paging settings from external dataView to the grid's options during initialization stage
                    (new wijmo.grid.settingsManager(this)).MapDVToWG();
                }
            }

            if (this._initialized) {
                if (userData && $.isFunction(userData.beforeRefresh)) {
                    userData.beforeRefresh.apply(this);
                }
            }

            if (!userData || !userData.virtualScrollData) {
                this.mAllowVVirtualScrolling = undefined;
                this.mEditSkechRowIndex = -1;
            }

            if (loadData === true) {
                this.mDataViewWrapper.load(userData);
            } else {
                this.doRefresh(userData);
                this._loaded();
            }
        }

		/** Gets an instance of the wijmo.grid.cellInfo class that represents the grid's specified cell.
		* @example
		* var cellInfo = $("#element").wijgrid("getCellInfo", domCell);
		* @param {Object} domCell A HTML DOM Table cell object
		* @returns {wijmo.grid.cellInfo} Object that represents a cell of the grid.
		*/
        getCellInfo(domCell: HTMLTableCellElement): wijmo.grid.cellInfo {
            var cellInfo = null;

            if (domCell && (domCell = <HTMLTableCellElement>this._findUntilOuterDiv(domCell, { td: true, th: true }))) { // test affinity
                cellInfo = this._view().getAbsoluteCellInfo(domCell, false);
            }

            return cellInfo;
        }

		/** Returns a one-dimensional array of filter operators which are applicable to the specified data type.
		* @example
		* var operators = $("#element").wijgrid("getFilterOperatorsByDataType", "string");
		* @param {String} dataType Specifies the type of data to which you apply the filter operators. Possible values are: "string", "number", "datetime", "currency" and "boolean".
		* @returns {wijmo.grid.IFilterOperator[]} A one-dimensional array of filter operators.
		*/
        getFilterOperatorsByDataType(dataType: string): IFilterOperator[] {
            return (new wijmo.grid.filterOperatorsCache(this)).getByDataType(dataType || "string");
        }

		/** Gets the number of pages.
		* @example
		* var pageCount = $("#element").wijgrid("pageCount");
		* @returns {Number} The number of pages.
		*/
        pageCount(): number {
            if (this._customPagingEnabled()) {
                return Math.ceil(this.options.totalRows / this.options.pageSize) || 1;
            }

            return this.options.allowPaging
                ? (<wijmo.data.IPagedDataView>this.mDataViewWrapper.dataView()).pageCount()
                : 1;
        }

        _closestCulture(): GlobalizeCulture {
            return this.mClosestCulture;
        }

        _serverShaping(): boolean {
            // used to support asp.net C1GridView
            return false;
        }

        _pageIndexForDataView(): number {
            /** Infrastructure */

            return this.options.pageIndex;
        }

		/** Sets the size of the grid using the width and height parameters.
		* @example
		* $("#element").wijgrid("setSize", 200, 200);
		* @param {String|Number} width Determines the width of the grid.
		* @param {String|Number} height Determines the height of the grid.
		*/
        setSize(width?: any, height?: any, rowsToAdjust?: cellRange, resizeDetails = true): void {
            if (!this._rendered || this.mIgnoreSizing) {
                return;
            }

            var view = this._view(),
                scrollValue: IScrollingState = null,
                outerDiv = this.mOuterDiv,
                visibleLeaves = this._visibleLeaves(),
                leavesWithFilter = [],
                ieLT9 = $.browser && $.browser.msie && ($.browser.version < 9);

            try {
                if (ieLT9) {
                    wijmo.grid.wijgrid.IGNORE_WIN_RESIZE = true; // #72351
                }

                if (view && view.getScrollValue) {
                    scrollValue = view.getScrollValue();
                }

                if (width || (width === 0)) {
                    this.mAutoWidth = false;
                    outerDiv.width(width);
                }

                if (height || (height === 0)) {
                    this.mAutoHeight = false;
                    outerDiv.height(height);
                }

                view._resetWidth();

                this._hideAllFilterDropDownLists();

                // recalculate sizes
                if (this._allowHVirtualScrolling()) {
                    // re-render grid completely (#106452)
                    this._resetScrollState();
                    this.doRefresh();
                } else {
                    this._setSizeInternal(scrollValue, rowsToAdjust, resizeDetails);
                }
            } finally {
                wijmo.grid.wijgrid.IGNORE_WIN_RESIZE = false;
            }
        }

        _setSizeWithoutDetails() {
            var old = this._ignoreSizing();
            this._ignoreSizing(false);
            this.setSize(undefined, undefined, undefined, false);
            this._ignoreSizing(old);
        }

        _setSizeInternal(scrollValue: IScrollingState, rowsToAdjust?: cellRange, resizeDetails = true) {
            if (!this._rendered) {
                return;
            }

            if (this._isDetail() && this.mIgnoreSizing) {
                this.element.width("100%"); // just stretch the detail grid within the parent's cell.
            }

            if (this.mIgnoreSizing) {
                return;
            }

            if (resizeDetails) {
                this._resetDetailsSize();
            }

            this._view().updateSplits(scrollValue, rowsToAdjust);
            this._UIFrozener().refresh();

            if (resizeDetails && this.mLoadingDetails === 0) { // process details
                var dtls = this.details();
                for (var i = 0; i < dtls.length; i++) {
                    var d = dtls[i].grid();

                    if (d) {
                        d._ignoreSizing(false);
                        d.setSize();
                        d._ignoreSizing(true);
                    }
                }
            }
        }

        _resetDetailsSize() {
            if (this._rendered && this.mLoadingDetails === 0) {
                var dtls = this.details();

                for (var i = 0; i < dtls.length; i++) {
                    var detailGrid = dtls[i].grid();
                    if (detailGrid) {
                        detailGrid._view()._resetWidth();
                        detailGrid._resetDetailsSize();
                    }
                }
            }
        }

		/** Gets an object that manages selection in the grid.
		* @example
		* // Use the row index to add the row to the selection object
		* var selection = $("#element").wijgrid("selection");
		* selection.addRows(2);
		* @remarks
		* See the description of the wijmo.grid.selection class for more details.
		* @returns {wijmo.grid.selection} Object that manages selection in the grid.
		*/
        selection(): wijmo.grid.selection {
            if (!this.mSelection) {
                this.mSelection = new wijmo.grid.selection(this);
            }
            return this.mSelection;
        }

        // * public
        _ignoreSizing(value?: boolean): boolean {
            if (arguments.length) {
                this.mIgnoreSizing = value;
            }

            return this.mIgnoreSizing;
        }

        _masterInfo(): IMasterInfo {
            return this.mMasterInfo;
        }

        _master(): wijgrid {
            return this.mMasterInfo ? this.mMasterInfo.master : null;
        }

        _validateRelation(r: IMasterDetailRelation[]) {
            if (!r || !r.length) {
                throw "The relation option must be defined.";
            }

            for (var i = 0; i < r.length; i++) {
                if (!r[i].detailDataKey || !r[i].masterDataKey) {
                    throw "Both detailDataKey and masterDataKey properties must be defined.";
                }
            }
        }

        _topMaster(): wijgrid {
            var master = this,
                mi: IMasterInfo;

            while (mi = master._masterInfo()) {
                master = mi.master;
            }

            return master;
        }

        _leaves(): c1basefield[] {
            if (!this.mLeaves) {
                this.mLeaves = [];
            }

            return this.mLeaves;
        }

        _visibleLeaves(): c1basefield[] {
            if (!this.mVisibleLeaves) {
                this.mVisibleLeaves = [];
            }

            return this.mVisibleLeaves;
        }

        _renderedLeaves(): c1basefield[] {
            if (!this.mRenderedLeaves) {
                this.mRenderedLeaves = [];
            }

            return this.mRenderedLeaves;
        }

        _virtualLeaves(): c1basefield[] {
            if (!this.mVirtualLeaves) {
                this.mVirtualLeaves = [];
            }

            return this.mVirtualLeaves;
        }

        _allColumns(): c1basefield[] {
            if (!this.mAllColumns) {
                this.mAllColumns = [];
            }

            return this.mAllColumns;
        }

        _findInstance(column: IColumn): c1basefield {
            if (column) {
                return this._allColumns()[column._travIdx] || null;
            }

            return null;
        }

        _currentCell(value?: cellInfo): cellInfo {
            if (!arguments.length) {
                return this.mCurrentCell;
            }

            this.mCurrentCell = value;
        }

        _groupAreaColumns(): c1groupedfield[] {
            if (!this.mGroupAreaColumns) {
                this.mGroupAreaColumns = [];
            }

            return this.mGroupAreaColumns;
        }

        _tHead(): any[] {
            return this.mTHead;
        }

        _onDataViewCurrentPositionChanged(e, args): void {
            var cellInfo = this._currentCellFromDataView(this.currentCell().cellIndex());

            // move currentCell to the new position
            cellInfo = this.currentCell(cellInfo, null, true);
        }

        _resetColumns() {
            wijmo.grid.traverse(this.options.columns, function (column, columns) {
                if (column._dynamic) {
                    // remove autogenerated columns
                    var idx = $.inArray(column, columns);
                    if (idx >= 0) {
                        columns.splice(idx, 1);
                    }
                } else {
                    // restore original values
                    column.dataKey = column._originalDataKey;
                    column.headerText = column._originalHeaderText;
                }
            });

            this._initialized = false; // to generate columns when doRefresh() or ensureControl() methods will be called
        }

        _resetDataProperitesOnFilterChanging() {
            this._resetDataProperties();
            this._resetVerticalBounds();
        }

        _resetDataProperties(): void {
            this.options.pageIndex = 0;
        }

        _resetVerticalBounds() {
            var bounds = this._viewPortBounds();
            //bounds.start = bounds.end = 0;
            bounds.start = 0; // #52058: reset the .start property only. Let wijgrid to update the .end property when the underlying data will be refreshed actually.

            this.mScrollingState.index = 0;
            this.mScrollingState.y = 0;
        }

        _resetScrollState() {
            // re-render grid completely
            if (this.mScrollingState) {
                this.mScrollingState.scrollLeft = 0;
                this.mScrollingState.x = 0;
            }
        }

        _onDataViewLoading(): void {
            this._clearDetails(); // clear subtree.
            this._activateSpinner(); // if data loading proccess was triggered outside the wijgrid.
            this._trigger("dataLoading");
        }

        _onDataViewReset(userData: IUserDataArgs, resetColumns?: boolean) {
            this.mEditSkechRowIndex = -1; // #49039

            if (this.mCurrentCellLocker) { // #77316. "currentChange" and "reset" events are firing successively if the dataView.sort() method is called outside the grid, so the changeCurrentCell callback never ends.
                if (!(this._serverSideVirtualScrolling() && userData && userData.virtualScrollData)) { //#78760 do not reset during server-side virtual scrolling request
                    this._resetVerticalBounds();
                }
                this.mCurrentCellLocker = false;
            }

            (new wijmo.grid.settingsManager(this)).MapDVToWG();

            this._trigger("dataLoaded");

            if (resetColumns) {
                this._resetColumns();
            }

            this.doRefresh(userData);

            this._loaded();
        }

        _onDataViewLoaded(): void {
        }

        _loading(): void {
            if (!this._isDetail()) {
                this._activateSpinner();
            }

            if (!this._isRoot()) {
                var master = this._masterInfo().master;
                master._onDetailLoading(this);
            }

            this._trigger("loading");
        }

        _onDetailLoading(detail: wijgrid): void {
            this._topMaster()._activateSpinner();
        }

        _loaded(): void {
            if (!(this._isRoot() && this.mLoadingDetails > 0)) { // the spinner will be hidden by _onDetailLoaded
                this._deactivateSpinner();
            }

            if (!this._isRoot()) {
                var master = this._masterInfo().master;
                master._onDetailLoaded(this);
            }

            this._trigger("loaded");
        }

        _onDetailLoaded(detail: wijgrid): void {
            if (this.mLoadingDetails > 0) {
                this.mLoadingDetails--;
            }

            // provide a detail instance to the parent's hierarchyNode
            var mi = detail._masterInfo();
            mi.master.details()[mi.dataRowIndex]._setGrid(detail);

            if (this.mLoadingDetails === 0) { // all child grids are loaded
                if (this._isRoot()) {
                    if (this._rendered) { // the detail can be rendered before the master in case of non-async datasets or if node expanded
                        this._setSizeInternal(this.mScrollingState, undefined, true); // resize all grids starting from the top.
                        this._deactivateSpinner();
                    }
                } else {
                    if (this._rendered) { // the detail can be rendered before the master in case of non-async datasets or if node is expanded
                        this._masterInfo().master._onDetailLoaded(this); // bubble
                    }
                }
            }
        }

        _buildSketchRow(wrappedDataItem: IWrappedDataItem, leaves: c1basefield[]): SketchRow {
            var meta = wijmo.grid.dataViewWrapper.getMetadata(wrappedDataItem.values),
                sketchRow = new SketchDataRow(wrappedDataItem.originalRowIndex,/*
					(dataItemIndex: number) => {
						return this._getDataItem(dataItemIndex);
					},*/
                    wijmo.grid.renderState.rendering, meta && meta.rowAttributes),
                cellAttributes = meta && meta.cellsAttributes;

            for (var i = 0, len = leaves.length; i < len; i++) {
                var leaf = leaves[i];

                if (leaf instanceof c1field) {
                    var boundField = <c1field>leaf,
                        cellAttr = cellAttributes ? cellAttributes[boundField.options.dataKey] : null,
                        value = this.mDataViewWrapper.getValue(wrappedDataItem.values, boundField.options.dataKey);

                    sketchRow.add(new ValueCell(this.parse(<IColumn>leaf.options, value), cellAttr));
                } else {
                    sketchRow.add(new ValueCell(null, {}));
                }
            }

            return sketchRow;
        }

        _buildSketchRowEmptyDataItem(dataItem, leaves: c1basefield[], isLastRow: boolean): SketchRow {
            var sketchRow = new SketchRow(wijmo.grid.rowType.emptyDataRow, wijmo.grid.renderState.rendering, null),
                leavesLen = leaves.length;

            for (var i = 0, len = dataItem.length; i < len; i++) {
                sketchRow.add(new HtmlCell(dataItem[i], {
                    colSpan: (leavesLen > 0 && isLastRow) ? this._visibleLeaves().length : 1
                }));
            }

            return sketchRow;
        }


        _prepareColumnsStage1(columns: IColumn[], dataLoaded: boolean, finalStage: boolean): void {
            new wijmo.grid.bandProcessor().getVisibleHeight(columns, true);	// set .isLeaf
        }

        _prepareColumnsStage2(columns: IColumn[], generationMode: string, fieldsInfo: IFieldInfoCollection, dataLoaded: boolean, finalState: boolean, generateInsances = true): void {
            wijmo.grid.traverse(columns, (column: IColumn) => {
                column._originalDataKey = column.dataKey;
                column._originalHeaderText = column.headerText;
            });

            if (dataLoaded) {
                wijmo.grid.columnsGenerator.generate(generationMode, fieldsInfo, columns);
            }

            wijmo.grid.setTraverseIndex(columns); // build indices (linearIdx, travIdx, parentIdx)

            if (generateInsances) {
                this._generateColumnInstancesFromOptions(columns);
            }


            // provide headerText and footerText if not specified
            if (dataLoaded && this.mDataViewWrapper && this.mAdjustHeaderText) { // mAdjustHeaderText -  wijgrid = true, c1gridview = false.
                $.each(this._leaves(), (index: number, column: c1basefield) => {
                    var boundedToDOM = this.mDataViewWrapper.isBoundedToDOM(),
                        opt = <IColumn>column.options,
                        cellIndex = (typeof (opt.dataKey) === "number") ? opt.dataKey : opt._leavesIdx;

                    // assume headerText options of the static columns only when using "merge" mode.
                    if ((opt.headerText === undefined) && (this.options.columnsAutogenerationMode === "merge" || opt._dynamic)) {
                        var headerRow = this._originalHeaderRowData();

                        if (boundedToDOM && headerRow && (cellIndex < headerRow.length)) {
                            opt.headerText = $.trim(headerRow[cellIndex]) // header text
                        } else {
                            if (column instanceof c1field) { // i.e. has a valid dataKey
                                opt.headerText = opt.dataKey + "";
                            }
                        }
                    }

                    var footerRow = this._originalFooterRowData();
                    if (boundedToDOM && footerRow && (cellIndex < footerRow.length)) {
                        opt._footerTextDOM = $.trim(footerRow[cellIndex]);
                    }
                });
            }
        }

        _prepareColumns(columns: IColumn[], generationMode: string, fieldsInfo: IFieldInfoCollection, dataLoaded: boolean, finalStage: boolean, generateInstances = true): void {
            this._prepareColumnsStage1(columns, dataLoaded, finalStage);
            this._prepareColumnsStage2(columns, generationMode, fieldsInfo, dataLoaded, finalStage, generateInstances);
        }

        _rebuildLeaves(): void {
            var tmp: IColumn[] = [];

            this._addVirtualColumns(tmp);

            $.each(this.options.columns, (index, item: IColumn) => {
                tmp.push(item); // append columns
            });

            wijmo.grid.setTraverseIndex(tmp); // build indices (linearIdx, travIdx, parentIdx)

            // generate span table and build leaves
            this._columnsHeadersTable(new wijmo.grid.bandProcessor().generateSpanTable(tmp));

            this._generateColumnInstancesFromOptions(tmp);

            this._onLeavesCreated();
        }

        _addVirtualColumns(columns: IColumn[]): void {
            if (this._showRowHeader()) { // append rowHeader
                columns.push(c1rowheaderfield.getInitOptions());
            }

            if (this._hasDetail()) { // append detail row header
                columns.push(c1detailrowheaderfield.getInitOptions());
            }
        }

        _generateColumnInstancesFromOptions(columns: IColumn[]) {
            this.mAllColumns = [];
            this.mLeaves = [];
            this.mVirtualLeaves = [];
            this.mVisibleLeaves = [];
            this.mGroupedLeaves = null; // force recreating.
            this.mRenderedLeaves = [];

            var dataIndex = 0,
                leavesIdx = 0,
                visLeavesIdx = 0;

            wijmo.grid.traverse(columns, (opt: IColumn) => {
                opt._visLeavesIdx = undefined;
                opt._leavesIdx = undefined;
                opt._renderedIndex = undefined;

                if (opt._isLeaf) {
                    opt._leavesIdx = leavesIdx++;

                    if (opt._parentVis) {
                        opt._visLeavesIdx = visLeavesIdx++;
                    }
                }

                opt.disabled = this.options.disabled === true;

                var instance = wijmo.grid.asColumnInstance(this, opt);

                this.mAllColumns.push(instance);

                if (instance instanceof c1field) {
                    opt.dataIndex = dataIndex++;
                }

                if (opt._isLeaf) {
                    this.mLeaves.push(instance);

                    if (opt._parentVis) {
                        this.mVisibleLeaves.push(instance);

                        if (instance instanceof c1rowheaderfield) {
                            this.mVirtualLeaves.push(instance);
                        }
                    }
                }
            });
        }

        _onLeavesCreated(): void {
        }

        _allowEditing(): boolean {
            return (this.options.allowEditing === true) // obsolete
                || (this.options.editingMode === "cell") || (this.options.editingMode === "row");
        }

        _allowCellEditing(): boolean {
            var editingMode = this.options.editingMode;

            if (editingMode === "row") {
                return false;
            }

            return (this.options.allowEditing === true) // obsolete
                || (editingMode === "cell");
        }

        _allowVirtualScrolling(): boolean {
            return this._allowVVirtualScrolling() || this._allowHVirtualScrolling();
        }

        _allowVVirtualScrolling(): boolean {
            if (this.mAllowVVirtualScrolling === undefined) {
                var mode = this.options.scrollingSettings.virtualizationSettings.mode,
                    allow = this.options.allowVirtualScrolling || mode === "rows" || mode === "both";

                this.mAllowVVirtualScrolling = !this.options.allowPaging && allow && (this._lgGetStaticRowIndex() < 0) && (this._lgGetScrollMode() !== "none") && !this._hasMerging() && !this._hasDetail();
            }

            return this.mAllowVVirtualScrolling;
        }

        _allowHVirtualScrolling(): boolean {
            if (this.mAllowHVirtualScrolling === undefined) {
                var mode = this.options.scrollingSettings.virtualizationSettings.mode,
                    allow = mode === "columns" || mode === "both";

                this.mAllowHVirtualScrolling = allow
                    //&& (this._lgGetStaticColumnIndex() < 0)
                    && (this._lgGetScrollMode() !== "none")
                    && !this._hasGrouping()
                    && !this._hasBands()
                    && !this._hasDetail();
            }

            return this.mAllowHVirtualScrolling;
        }


        _headerRows(): wijmo.grid.rowAccessor {
            return this._view().headerRows();
        }

        _filterRow(): IRowObj {
            return this._view().filterRow();
        }

        _rows(): wijmo.grid.rowAccessor {
            return this._view().bodyRows();
        }

        _localizeFilterOperators(locArray: { name: string; displayName: string; }[]): void {
            var self = this,
                helper = new wijmo.grid.filterOperatorsCache(this);

            $.each(locArray, function (i, o) {
                if (o.name) {
                    var fop = helper.getByName(o.name);
                    if (fop) {
                        fop.displayName = o.displayName;
                    }
                }
            });
        }

        _KeyDownEventListener(): wijmo.grid.keyDownEventListener {
            if (!this.mKeyDownEventListener) {
                var view = this._view(),
                    $fe = view && view.focusableElement();

                if ($fe) {
                    this.mKeyDownEventListener = new keyDownEventListener(this, $fe);
                }
            }

            return this.mKeyDownEventListener;
        }

        _UIDragndrop(force?: boolean): wijmo.grid.uiDragndrop {
            if (!this._dragndropui && force) {
                this._dragndropui = new wijmo.grid.uiDragndrop(this);
            }

            return this._dragndropui;
        }

        _UIFrozener(force?: boolean): wijmo.grid.uiFrozener {
            if (!this._frozenerui || force) {
                this._frozenerui = new wijmo.grid.uiFrozener(this);
            }

            return this._frozenerui;
        }

        _UIResizer(force?: boolean): wijmo.grid.uiResizer {
            if (!this._resizerui && force) {
                this._resizerui = new wijmo.grid.uiResizer(this);
            }

            return this._resizerui;
        }

        _UISelection(force?: boolean): wijmo.grid.uiSelection {
            if (!this._selectionui && force) {
                this._selectionui = new wijmo.grid.uiSelection(this);
            }

            return this._selectionui;
        }

        // * propeties (pre-\ post-)
        _postset_allowColMoving(value, oldValue): void {
            var self = this;

            $.each(this.columns(), function (idx, wijField) {
                if (value) {
                    self._UIDragndrop(true).attach(wijField);
                } else {
                    self._UIDragndrop(true).detach(wijField);
                }
            });

            var groupedWidgets = this._groupAreaColumns();

            if (groupedWidgets) {
                $.each(groupedWidgets, function (idx, wijField) {
                    if (value) {
                        self._UIDragndrop(true).attach(wijField);
                    } else {
                        self._UIDragndrop(true).detach(wijField);
                    }
                });
            }
        }


        _postset_allowEditing(value, oldValue): void { // deprecated
            this._postset_editingMode(this.options.editingMode, undefined);
        }

        _postset_allowSorting(value, oldValue): void {
            this.ensureControl(false);
        }

        _postset_columns(value, oldValue): void {
            this._initialized = false;
            this.ensureControl(true);
        }

        _postset_allowPaging(value, oldValue): void {
            this.ensureControl(true);
        }

        _postset_freezingMode(value, oldValue): void {
            if (this._frozenerui) {
                this._frozenerui.refresh();
            }
        }

        _postset_culture(value, oldValue): void {
            this.mClosestCulture = $.wijGetCulture(this.options.culture, this.options.cultureCalendar);
            this.ensureControl(false);
        }

        _postset_customFilterOperators(value, oldValue): void {
            var dataView = this.mDataViewWrapper.dataView();
        }

        _postset_data(value, oldValue): void {
            this._resetColumns();

            // this._resetDataProperties();

            if (this.mDataViewWrapper) {
                this.mDataViewWrapper.dispose();
            }

            this.mDataViewWrapper = new wijmo.grid.dataViewWrapper(this);

            this.ensureControl(true);
        }

        _postset_disabled(value, oldValue): void {
            // update children widgets
            var self = this,
                view = this._view();

            wijmo.grid.iterateChildrenWidgets(this.mOuterDiv, function (index, widget) {
                if (widget !== <JQueryUIWidget><any>self) {
                    widget.option("disabled", value);
                }
            });

            // update editors (#69699)
            if ((this.mEditSkechRowIndex >= 0) && (view._isRowRendered(this.mEditSkechRowIndex) >= 0)) {
                var row = view._getRowInfoBySketchRowIndex(this.mEditSkechRowIndex, false); // get a live IRowInfo object.
                if (row) {
                    row.$rows.find("." + wijmo.grid.wijgrid.CSS.inputMarker).prop("disabled", value);
                }
            }

            //
            if (view) {
                view.ensureDisabledState();
            }
        }

        _postset_editingMode(value, oldValue): void {
            if (this.mKeyDownEventListener) {
                this.mKeyDownEventListener.dispose();
                this.mKeyDownEventListener = null;

                this._KeyDownEventListener();
            }
        }

        _postset_groupIndent(value, oldValue): void {
            this.ensureControl(false);
        }

        _postset_groupAreaCaption(value, oldValue): void {
            var groupedColumns = this._groupedLeaves();

            if (this.$groupArea && (!groupedColumns || !groupedColumns.length)) { // update html when the group area is empty only.
                this.$groupArea.html(value || "&nbsp;");
            }
        }

        _postset_highlightCurrentCell(value, oldValue): void {
            var currentCell = this.currentCell();

            if (currentCell && currentCell._isValid()) {
                this._highlightCellPosition(currentCell, value);
            }
        }

        _preset_pageIndex(value, oldValue): any {
            if (isNaN(value)) {
                throw "out of range";
            }

            var pageCount = this.pageCount(),
                fn = function (val) {
                    if (val > pageCount - 1) {
                        val = pageCount - 1;
                    }

                    if (val < 0) {
                        val = 0;
                    }

                    return val;
                };


            value = fn(value);

            if (this.options.allowPaging && value !== oldValue) {
                var args: IPageIndexChangingEventArgs = { newPageIndex: value };

                if (!this._onPageIndexChanging(args)) {
                    value = oldValue;

                    // revert wijpager's pageIndex option
                    $.each([this.mTopPagerDiv, this.mBottomPagerDiv], (_, elem: JQuery) => {
                        if (elem && elem.length) {
                            var instance = elem.data("wijmo-wijpager");
                            if (instance) {
                                instance.options.pageIndex = value;
                            }
                        }
                    });
                } else {
                    value = fn(args.newPageIndex);
                }
            }

            return value;
        }

        _postset_pageIndex(value, oldValue): void {
            if (this.options.allowPaging) {
                var args = { newPageIndex: value };

                if (this._customPagingEnabled()) {
                    this._onPageIndexChanged(args); // Allow user the ability to load a new data and refresh the grid.
                } else {
                    this.ensureControl(true, {
                        afterRefresh: function () { this._onPageIndexChanged(args); }
                    });
                }
            }
        }

        _preset_pageSize(value, oldValue): any {
            if (isNaN(value)) {
                throw "out of range";
            }

            if (value <= 0) {
                value = 1;
            }

            return value;
        }

        _postset_pageSize(value, oldValue): void {
            this._resetDataProperties();

            if (this.options.allowPaging && !this._customPagingEnabled()) {
                this.ensureControl(true);
            }
        }

        _postset_pagerSettings(value, oldValue): void {
            this.ensureControl(false);
        }

        _postset_rowHeight(value, oldValue): void {
            this.mRowOuterHeight = -1;
            this.ensureControl(false);
        }

        _postset_scrollMode(value, oldValue): void {
            this.ensureControl(false);
        }

        _preset_scrollingSettings(value, oldValue): any {
            return $.extend(true, {}, $.wijmo.wijgrid.prototype.options.scrollingSettings, value);
        }

        _postset_selectionMode(value, oldValue): void {
            var selection = this.selection(),
                currentCell = this.currentCell(),
                hasSelection = this.selection().selectedCells().length();

            selection.beginUpdate();

            selection.clear();

            if (currentCell && currentCell._isValid() && hasSelection) {
                selection._selectRange(new wijmo.grid.cellInfoRange(currentCell, currentCell), false, false, wijmo.grid.cellRangeExtendMode.none, null);
            }

            selection.endUpdate();

            this._view().toggleDOMSelection(value === "none"); // disable or enable DOM selection
        }

        _postset_showFilter(value, oldValue): void {
            this.ensureControl(false);
        }

        _postset_showGroupArea(value, oldValue): void {
            this.ensureControl(false);
        }

        _postset_showRowHeader(value, oldValue): void {
            this.ensureControl(false);
        }

        _postset_staticRowIndex(value, oldValue): void {
            if (this._freezingAllowed()) { // staticRowIndex is ignored when scrolling is turned off.
                this.options.scrollingSettings.staticRowIndex = value;
                this.ensureControl(false);
            }
        }

        _postset_staticColumnIndex(value, oldValue): void {
            if (this._freezingAllowed()) {
                this.options.scrollingSettings.staticColumnIndex = value;
                this.ensureControl(false);
            }
        }

        _postset_allowVirtualScrolling(value, oldValue): void {
            this.ensureControl(false);
        }

        _preset_allowVirtualScrolling(value, oldValue): any {
            if (isNaN(value) || value < 0) {
                throw "out of range";
            }

            return value;
        }

        // * propeties (pre-\ post-)

        // * private

        _activateSpinner(): void {
            //For fix the issue#151745.
            //if (!this._spinnerActivated)
            if (!this._spinnerActivated && !this._topMaster()._spinnerActivated) {
                var defCSS = wijmo.grid.wijgrid.CSS,
                    wijCSS = this.options.wijCSS,

                    loadingText = this.mOuterDiv.append(
                        "<div class=\"" + defCSS.spinnerMarker + " " + defCSS.loadingOverlay + " " + wijCSS.wijgridLoadingOverlay + " " + wijCSS.overlay + "\"></div>" +
                        "<span class=\"" + defCSS.spinnerMarker + " " + defCSS.loadingText + " " + wijCSS.wijgridLoadingText + " " + wijCSS.content + " " + wijCSS.cornerAll + "\">" +
                        "<span class=\"" + defCSS.spinnerMarker + " " + wijCSS.icon + " " + wijCSS.iconClock + "\"></span>" +
                        this.options.loadingText +
                        "</span>")
                        .find("> ." + defCSS.loadingText);

                loadingText
                    .position({
                        my: "center",
                        at: "center center",
                        of: this.mOuterDiv,
                        collision: "none"
                    });

                this._spinnerActivated = true;
            }
        }

        _customPagingEnabled(): boolean {
            return this.options.allowPaging && this.options.totalRows >= 0;
        }

        _deactivateSpinner(): void {
            if (this._spinnerActivated) {
                try {
                    var defCSS = wijmo.grid.wijgrid.CSS;

                    this.mOuterDiv
                        .children("." + defCSS.spinnerMarker)
                        .remove();
                }
                finally {
                    this._spinnerActivated = false;
                }
            }
        }

        _prepareTotalsRequest(): c1field[] {
            var leaves = this._leaves(),
                result;

            if (!leaves || !this.options.showFooter) {
                return <any>[];
            }

            result = $.map((<any[]>leaves), function (column: c1basefield, index?: any) {
                var opt = <IColumn>column.options;

                if ((column instanceof c1field) && opt.aggregate && opt.aggregate !== "none") {
                    return [column];
                }

                return null;
            });

            return result;
        }

        _groupedLeaves(force?: boolean): c1field[] {
            var result: c1field[];

            force = !(result = this.mGroupedLeaves) || force;

            if (force) {
                result = [];

                var leaves = this._leaves(),
                    rebuildIndexes: boolean = false;

                $.each(leaves, (i, leaf: c1basefield) => {
                    if (c1field.isGroupedColumn(leaf)) {
                        rebuildIndexes = rebuildIndexes || (leaf.options.groupedIndex === undefined);

                        if (!rebuildIndexes) {
                            result.push(<c1field>leaf);
                        }
                    } else {
                        delete leaf.options.groupedIndex;
                    }
                });

                if (rebuildIndexes) {
                    $.each(leaves, (i, leaf: c1basefield) => {
                        if (c1field.isGroupedColumn(leaf)) {
                            leaf.options.groupedIndex = result.length;
                            result.push(<c1field>leaf);
                        }
                    });
                } else {
                    result.sort(function (a: c1basefield, b: c1basefield) {
                        return (<IColumn>a.options).groupedIndex - (<IColumn>b.options).groupedIndex;
                    });

                    $.each(result, (i, column: c1basefield) => {
                        column.options.groupedIndex = i;
                    });
                }

                this.mGroupedLeaves = result;
            }

            return result || [];
        }

        _ensureRenderableBounds(bounds: IRenderBounds): IRenderBounds {
            return wijmo.grid.ensureBounds(bounds, this._renderableRowsCount() - 1);
        }

        _ensureTotalRowsBounds(bounds: IRenderBounds): IRenderBounds {
            return wijmo.grid.ensureBounds(bounds, this._totalRowsCount() - 1);
        }

        _ensureLowerBoundVisible(bounds: IRenderBounds): IRenderBounds {
            var visibleQty = this._renderableRowsCount();

            if (bounds.end >= visibleQty) {
                var view = <fixedView>this._view(),
                    size = view.getVirtualPageSize(false); // the number of the entirely visible rows

                bounds.end = visibleQty - 1;
                bounds.start = bounds.end - size + 1;

                bounds.start = Math.max(0, bounds.start);
                bounds.end = Math.max(0, bounds.end);
            }

            return bounds;
        }

        _renderableColumnsRange() {
            return this.mRenderableColumns;
        }

        _renderableRowsRange() {
            return this.mRenderableRows;
        }

        _rebuildRenderableRowsCollection() {
            var start = -1,
                end = -1,
                rangeFound = false,
                total = this._totalRowsCount(); // <- overrided by c1gridview

            this.mRenderableRows = new wijmo.grid.renderableRowsCollection(total - 1);

            if (this._allowVVirtualScrolling() && this._serverSideVirtualScrolling()) {
                this.mRenderableRows.add({ start: 0, end: total - 1 }); // all rows, groping is disabled
            } else {
                if (!this._allowVVirtualScrolling() || !this._hasGrouping()) {
                    // assumption: if there is no grouping, then all sketch rows are visible
                    this.mRenderableRows.add({ start: 0, end: total - 1 }); // all rows
                } else {
                    // gets only visible rows from the sketchTable
                    for (var i = 0; i < this.mSketchTable.count(); i++) {
                        var sketchItem = this.mSketchTable.row(i);

                        if (sketchItem.extInfo.state & wijmo.grid.renderStateEx.hidden) {
                            if (start >= 0) {
                                this.mRenderableRows.add({ start: start, end: end });
                                rangeFound = false;
                            }

                            start = end = -1;
                        } else {
                            if (start < 0) {
                                rangeFound = true;
                                start = end = i;
                            } else {
                                end++;
                            }
                        }
                    }

                    if (rangeFound) {
                        this.mRenderableRows.add({ start: start, end: end });
                    }
                }
            }
        }

        _refresh(userData: IUserDataArgs): void {
            if (this._hasGrouping() && this._hasDetail()) {
                throw "It is not possible to use columns grouping and master-detail hierarchy simultaneously.";
            }

            if (this._hasDetail() && this._allowVirtualScrolling()) {
                throw "It is not possible to use virtual scrolling and master-detail hierarchy simultaneously.";
            }

            // build hierarchy
            hierarchyBuilder.build(this);

            // apply grouping
            new wijmo.grid.grouper().group(this, this.mSketchTable);

            // apply merging
            new wijmo.grid.merger().merge(this, this.mSketchTable);

            this._rebuildRenderableRowsCollection();

            //this._ensureViewPortBounds(this._viewPortBounds());

            // view
            if (this._lgGetScrollMode() !== "none") {
                this.mView = new wijmo.grid.fixedView(this, this._viewPortBounds());
            } else {
                this.mView = new wijmo.grid.flatView(this, this._viewPortBounds());
            }

            this._render();

            // pager
            if (this.options.allowPaging) {
                // top pager
                if (this.mTopPagerDiv) {
                    this.mTopPagerDiv.wijpager(this._pagerSettings2PagerWidgetSettings()).css("zIndex", 4);
                }

                // bottom pager
                if (this.mBottomPagerDiv) {
                    this.mBottomPagerDiv.wijpager(this._pagerSettings2PagerWidgetSettings()).css("zIndex", 4);
                }
            }
            // (re)create iternal widgets
        }

        _refreshVirtual(userData: IUserDataArgs): void {
            var scrollData = userData.virtualScrollData,
                direction = userData.virtualScrollData.direction;

            if (direction === "h") {
                this._view().render(true);
            } else {
                var diffData = {
                    top: 0,
                    bottom: 0
                };

                if (scrollData.data) {
                    diffData = this._processVirtualData(scrollData);
                }

                this._updateRowInfos(scrollData, diffData);

                this._renderVirtualIntoView(scrollData);
            }

            // debug
			/*var rows = this._view().bodyRows();
			for (var i = 0; i < rows.length(); i++) {
				var ri = this._view()._getRowInfo(rows.item(i));
				var innerDiv = ri.$rows.find("td:first ." + wijmo.grid.wijgrid.CSS.wijgridCellContainer);
				var html = innerDiv.html();

				html = "d:" + ri.dataItemIndex + " s:" + ri.sectionRowIndex + "  ||" + ri.data[0] + "|| " + html;
				innerDiv.html(html);
			}*/
            // debug

			/*if (scrollData.data && scrollData.mode === intersectionMode.reset) {
			this._view().vsUI.scrollToRow(scrollData.newBounds.start, true); // original scrollIndex could change due pageSize alignment, so we need to re-set position of the vertical scrollbar.
			}*/
        }

        _updateRowInfos(scrollData: IVirtualScrollData, diffData): void {
            var bounds: IRenderBounds = this._viewPortBounds(),
                view = this._view(),
                newBounds: IRenderBounds = scrollData.newBounds,
                rows = this._view().bodyRows(),
                relMatch: IRenderBounds,
                i: number,
                diff: number,
                rowInfo: IRowInfo;

            switch (scrollData.mode) {
                case intersectionMode.reset:
                    break;

                case intersectionMode.overlapBottom:
                    relMatch = { // zero-based
                        start: newBounds.start - bounds.start,
                        end: bounds.end - bounds.start
                    };

                    diff = newBounds.start - bounds.start;

                    for (i = relMatch.start; i <= relMatch.end; i++) {
                        rowInfo = view._getRowInfo(rows.item(i), false);

                        rowInfo.sectionRowIndex -= diff;

                        if (diffData.top !== 0) {
                            rowInfo.dataItemIndex += diffData.top;
                            rowInfo.sketchRowIndex += diffData.top;
                        }

                        view._setRowInfo(rowInfo.$rows, rowInfo);
                    }

                    break;

                case intersectionMode.overlapTop:
                    relMatch = { // zero-based
                        start: bounds.start - bounds.start,
                        end: newBounds.end - bounds.start
                    };

                    diff = bounds.start - newBounds.start;

                    for (i = relMatch.start; i <= relMatch.end; i++) {
                        rowInfo = view._getRowInfo(rows.item(i), false);

                        rowInfo.sectionRowIndex += diff;

                        if (diffData.top !== 0) {
                            rowInfo.dataItemIndex += diffData.top;
                            rowInfo.sketchRowIndex += diffData.top;
                        }

                        view._setRowInfo(rowInfo.$rows, rowInfo);
                    }

                    break;
            }
        }

        _renderVirtualIntoView(scrollData: IVirtualScrollData) {
            var bounds = this._viewPortBounds(),
                self = this,
                sketchRow: SketchRow,
                view = <fixedView>this._view(),
                match: { start: number; end: number; },
                sectionRowIndex: number,
                sketchRowIndex: number;

            view._clearNorthTablesHeight(true);
            view._ensureRenderHBounds();

            switch (scrollData.mode) {
                case intersectionMode.reset:
                    // remove all rows
                    view._clearBody();

                    // add new rows
                    var count = scrollData.newBounds.end - scrollData.newBounds.start + 1;

                    this._renderableRowsRange().forEachIndex(scrollData.newBounds.start, count, function (sketchIndex) {
                        sketchRowIndex = sketchIndex - self.mDataOffset;
                        sketchRow = self.mSketchTable.row(sketchRowIndex);
                        view._insertBodyRow(sketchRow, -1, sketchRow.dataItemIndex(), sketchRow.dataItemIndex(self.mDataOffset), sketchRowIndex);
                    });

                    view._rebuildOffsets();

                    break;

                case intersectionMode.overlapBottom:
                    match = {
                        start: scrollData.newBounds.start,
                        end: bounds.end
                    };

                    // remove rows from the top
                    for (var i = 0; i < match.start - bounds.start; i++) {
                        view._removeBodyRow(0);
                    }

                    // add new rows to the bottom
                    var count = scrollData.newBounds.end - match.end;

                    this._renderableRowsRange().forEachIndex(match.end + 1, count, function (sketchIndex) {
                        sketchRowIndex = sketchIndex - self.mDataOffset;
                        sketchRow = self.mSketchTable.row(sketchRowIndex);
                        view._insertBodyRow(sketchRow, -1, sketchRow.dataItemIndex(), sketchRow.dataItemIndex(self.mDataOffset), sketchRowIndex);
                    });

                    break;

                case intersectionMode.overlapTop:
                    match = {
                        start: bounds.start,
                        end: scrollData.newBounds.end
                    };

                    // remove rows from the bottom
                    for (var i = 0; i < bounds.end - scrollData.newBounds.end; i++) {
                        view._removeBodyRow(match.end - match.start + 1); // relative index starting from zero.
                    }

                    // add new tows to the top
                    sectionRowIndex = 0;

                    var count = bounds.start - scrollData.newBounds.start;

                    this._renderableRowsRange().forEachIndex(scrollData.newBounds.start, count, function (sketchIndex) {
                        sketchRowIndex = sketchIndex - self.mDataOffset;
                        sketchRow = self.mSketchTable.row(sketchRowIndex);
                        view._insertBodyRow(sketchRow, sectionRowIndex++, sketchRow.dataItemIndex(), sketchRow.dataItemIndex(self.mDataOffset), sketchRowIndex);
                    });

                    break;

                default: // "none", same range
                    break;
            }
        }

        _processVirtualData(scrollData: IVirtualScrollData) {
            var dvw = this.mDataViewWrapper,
                source = dvw.dataView().getSource(),
                dataItem,
                leaves = this._leaves(),
                i: number,
                alignedViewBounds,
                cachedBounds,
                exceeded = 0,
                dataDiff = {
                    top: 0,
                    bottom: 0
                },
                margin = this._serverSideVirtualScrollingMargin();

            //  * extend underlying data
            switch (scrollData.mode) {
                case intersectionMode.reset:
                    this.mSketchTable.clear();
                    dvw._unsafeSplice(0, source.length);

                    this.mDataOffset = scrollData.request.index;

                    // append
                    for (i = 0; i < scrollData.data.length; i++) {
                        dvw._unsafePush(dataItem = scrollData.data[i]); // append rows to a dataStore
                        this.mSketchTable.add(this._buildSketchRow(dvw._wrapDataItem(dataItem, i), leaves));
                    }

                    break;

                case intersectionMode.overlapBottom:
                    // append

                    for (i = 0; i < scrollData.data.length; i++) {
                        dvw._unsafePush(dataItem = scrollData.data[i]); // append rows to a dataStore
                        this.mSketchTable.add(this._buildSketchRow(dvw._wrapDataItem(dataItem, source.length - 1), leaves));
                    }

                    dataDiff.bottom = scrollData.data.length;

                    break;

                case intersectionMode.overlapTop:
                    // prepend
                    for (i = scrollData.data.length - 1; i >= 0; i--) {
                        dvw._unsafeSplice(0, 0, dataItem = scrollData.data[i]);
                        this.mSketchTable.insert(0, this._buildSketchRow(dvw._wrapDataItem(dataItem, i), leaves));
                    }

                    this.mDataOffset = scrollData.request.index;

                    dataDiff.top = scrollData.data.length;

                    break;
            }
            // extend underlying data *


            // * remove cached items exceeded cached bounds
            // [margin][pageSize = viewBounds][margin]
            alignedViewBounds = this._ensureTotalRowsBounds({
                start: scrollData.newBounds.start,
                end: scrollData.newBounds.end
            });

            cachedBounds = {
                start: this.mDataOffset,
                end: this.mDataOffset + source.length - 1
            };

            // remove items from the bottom
            exceeded = (cachedBounds.end - alignedViewBounds.end) - margin;
            if (exceeded > 0) {
                dataDiff.bottom -= exceeded;
                dvw._unsafeSplice(source.length - exceeded, exceeded)
                this.mSketchTable.removeLast(exceeded);
            }

            // remove items from the top
            exceeded = (alignedViewBounds.start - cachedBounds.start) - margin;
            if (exceeded > 0) {
                dataDiff.top -= exceeded;
                dvw._unsafeSplice(0, exceeded);
                this.mSketchTable.removeFirst(exceeded);
                this.mDataOffset += exceeded;
            }
            // remove data exceeded cached bounds *

            // * update metadata

            this.mSketchTable.updateIndexes();
            // update metadata *

            dvw._refreshSilent();

            return dataDiff;
        }

        _needToCreatePagerItem(): boolean {
            return this.options.allowPaging === true;
        }

        _isMobileEnv(): boolean {
            return this._isMobile;
        }

        _isTouchEnv(): boolean {
            return !!($.support.isTouchEnabled && $.support.isTouchEnabled());
        }

        _render() {
            var view = this._view(),
                o = this.options,
                defCSS = wijmo.grid.wijgrid.CSS,
                wijCSS = this.options.wijCSS,
                content: JQuery;

            view.render(false);

            // YK: for fixing pager is not align to top and bottom when header is fixed.
            content = this.mOuterDiv;
            if (this._lgGetScrollMode() !== "none") {
                // fixed header content
                content = this.mOuterDiv.find("div." + defCSS.scroller + ":first");
            }

            this.mSuperPanelHeader = null;

            // ** top pager (top div)
            if (this.mTopPagerDiv) {
                if (this.mTopPagerDiv.data("wijmo-wijpager")) {
                    this.mTopPagerDiv.wijpager("destroy");
                }

                this.mTopPagerDiv.remove();
            }

            this.mTopPagerDiv = null;

            if (this._needToCreatePagerItem() && ((o.pagerSettings.position === "top") || (o.pagerSettings.position === "topAndBottom"))) {
                if (!this.mTopPagerDiv) {
                    content.prepend(this.mSuperPanelHeader = $("<div class=\"wijmo-wijsuperpanel-header\"></div>"));
                    this.mSuperPanelHeader.prepend(this.mTopPagerDiv = $("<div class=\"" + defCSS.headerArea + " " + wijCSS.wijgridHeaderArea + " " + wijCSS.header + " " + wijCSS.cornerTop + "\"></div>"));
                }
            }
            // top pager **

            if (o.showGroupArea) {
                this._processGroupArea(content);
            } else {
                this.$groupArea = null;
            }

            // ** bottom pager (bottom div)
            if (this.mBottomPagerDiv) {
                if (this.mBottomPagerDiv.data("wijmo-wijpager")) {
                    this.mBottomPagerDiv.wijpager("destroy");
                }

                this.mBottomPagerDiv.remove();
            }

            this.mBottomPagerDiv = null;

            if (this._needToCreatePagerItem() && ((o.pagerSettings.position === "bottom") || (o.pagerSettings.position === "topAndBottom"))) {
                if (!this.mBottomPagerDiv) {
                    content.append(this.mBottomPagerDiv = $("<div class=\"" + defCSS.footerArea + " " + wijCSS.wijgridFooterArea + " wijmo-wijsuperpanel-footer " + wijCSS.stateDefault + " " + wijCSS.cornerBottom + "\"></div>"));
                }
            }
            // bottom pager **
        }

        _processGroupArea(content) {
            var self = this,
                groupCollection = this._groupedLeaves(),
                defCSS = wijmo.grid.wijgrid.CSS,
                wijCSS = this.options.wijCSS;

            this.mGroupAreaColumns = [];
            this.$groupArea = $("<div class=\"" + wijCSS.content + " " + wijCSS.helperClearFix + " " + defCSS.groupArea + " " + wijCSS.wijgridGroupArea + "\"></div>");

            if (groupCollection.length > 0) {
                $.each(groupCollection, (index, column: c1field) => {
                    var groupElement = $("<a href=\"#\"></a>").appendTo(self.$groupArea);
                    this.mGroupAreaColumns.push(this._createGroupAreaColumnHeader(groupElement, column));
                });
            } else {
                this.$groupArea
                    .addClass(defCSS.groupAreaEmpty)
                    .css("padding", 0) // disable padding (inherited)
                    .html(this.options.groupAreaCaption || "&nbsp;");
            }

            if (!this.mSuperPanelHeader) {
                content.prepend(this.mSuperPanelHeader = $("<div class=\"wijmo-wijsuperpanel-header\"></div>"));
            }

            this.mSuperPanelHeader.prepend(this.$groupArea);

            this._UIDragndrop(true).attachGroupArea(this.$groupArea);
        }

        _createGroupAreaColumnHeader(element: JQuery, column: c1field): c1groupedfield {
            return new c1groupedfield(this, <IColumn>column.options, element);
        }

        _pagerSettings2PagerWidgetSettings() {
            return $.extend({}, this.options.pagerSettings,
                {
                    disabled: this.options.disabled,
                    pageCount: this.pageCount(),
                    pageIndex: this.options.pageIndex,
                    pageIndexChanging: $.proxy(this._onPagerWidgetPageIndexChanging, this),
                    pageIndexChanged: $.proxy(this._onPagerWidgetPageIndexChanged, this)
                });
        }

        _showRowHeader(): boolean {
            return (this.options.showRowHeader === true) && (this._lgGetStaticColumnsAlignment() !== "right");
        }

        _attachEvents() {
            var view = this._view(),
                $fe = view.focusableElement(),
                self = this,
                k = function (eventName: string): string {
                    return eventName + "." + self.widgetName;
                };

            $fe.unbind(k("keydown")).bind(k("keydown"), $.proxy(this._onKeyDown, this));
            $fe.unbind(k("focusin")).bind(k("focusin"), $.proxy(this._onFocusIn, this));

            this._KeyDownEventListener();

            $.each(view.subTables(), function (index, element: htmlTableAccessor) {
                var domTable = element.element();
                if (domTable) {
                    if (domTable.tHead) {
                        $(domTable.tHead).unbind(k("click")).bind(k("click"), $.proxy(self._onClick, self));
                    }

                    if (domTable.tBodies.length) {
                        $(domTable.tBodies[0])
                            .unbind(k("click")).bind(k("click"), $.proxy(self._onClick, self))
                            .bind(k("dblclick")).bind(k("dblclick"), $.proxy(self._onDblClick, self))
                            .bind(k("mousemove")).bind(k("mousemove"), $.proxy(self._onMouseMove, self))
                            .bind(k("mouseout")).bind(k("mouseout"), $.proxy(self._onMouseOut, self));
                    }
                }
            });

            if (this._isRoot()) {
                $(window).bind("resize." + this.widgetName + "." + this._eventUID, $.proxy(this._onWinResize, this));
            }
        }

        _detachEvents(destroy) {
            var view = this._view(),
                self = this,
                $fe;

            if (this._windowResizeTimer > 0) {
                window.clearTimeout(this._windowResizeTimer);
                this._windowResizeTimer = 0;
            }

            $(window).unbind("resize." + this.widgetName + "." + this._eventUID);

            if (view) {
                $fe = view.focusableElement();

                $fe.unbind("." + this.widgetName);

                $.each(view.subTables(), function () {
                    var domTable = this.element(); // item (this) is a htmlTableAccessor instance 

                    if (domTable) {
                        if (domTable.tHead) {
                            $(domTable.tHead).unbind("." + self.widgetName);
                        }

                        if (domTable.tBodies.length) {
                            $(domTable.tBodies[0]).unbind("." + self.widgetName);
                        }
                    }
                });
            }
        }

        _handleSort(column: c1basefield, multiSort: boolean) {
            var colOpt = <IColumn>column.options,
                columns = this.options.columns;

            if (this.options.allowSorting) {
                var newSortDirection = ((colOpt.sortDirection === "none")
                    ? "ascending"
                    : ((colOpt.sortDirection === "ascending") ? "descending" : "ascending"));

                var sortingArgs: ISortingEventArgs = {
                    column: colOpt,
                    sortDirection: newSortDirection,
                    sortCommand: colOpt.dataKey + " " + (newSortDirection === "ascending" ? "asc" : "desc")
                };

                if (this._onColumnSorting(sortingArgs)) {
                    colOpt.sortDirection = sortingArgs.sortDirection;

                    if (multiSort) {
                        colOpt._sortOrder = this.mCustomSortOrder++;
                    } else {
                        this.mCustomSortOrder = 1000; // reset to default

                        // reset sortDirection for all columns except sorting one and grouped columns
                        $.each(this._allColumns(), function (i, item: c1basefield) {
                            item.options._sortOrder = 0;

                            if (item instanceof c1field) {
                                var itemOpt = (<c1field>item).options;

                                if (item !== column && !(itemOpt.groupInfo && itemOpt.groupInfo.position !== "none")) {
                                    itemOpt.sortDirection = "none";
                                }
                            }
                        });
                    }

                    var sortedArgs: ISortedEventArgs = {
                        column: colOpt,
                        sortDirection: colOpt.sortDirection,
                        sortCommand: colOpt.dataKey + " " + (colOpt.sortDirection === "ascending" ? "asc" : "desc")
                    };

                    if (this._customPagingEnabled()) {
                        this._onColumnSorted(sortedArgs); // Allow user the ability to load a new data and refresh the grid.
                    } else {
                        this.ensureControl(true, {
                            afterRefresh: function () { this._onColumnSorted(sortedArgs); }
                        });
                    }
                }
            }
        }

        _handleDragnDrop(dragInst: IDragnDropElement, dropInst: IDragnDropElement, at: string, dragInGroup: boolean, dropInGroup: boolean) {
            var drag = <IColumn>dragInst.options,
                drop = <IColumn>((dropInst && dropInst.options) || null), // dropInst = null if drag is dropped into the empty group area.
                dragSource = dragInGroup ? "groupArea" : "columns",
                dropSource = dropInGroup ? "groupArea" : "columns";

            if (dropInGroup) { // drag is dropped into the group area
                if (this._onColumnGrouping(<IColumnGroupingEventArgs>{ drag: drag, drop: drop, dragSource: dragSource, dropSource: dropSource, at: at })) {
                    this.ensureControl(true, {
                        beforeRefresh: function () {
                            if (!drop) { // drag is dropped into the empty group area.
                                drag.groupedIndex = 0;
                            } else {
                                switch (at) {
                                    case "left":
                                        drag.groupedIndex = drop.groupedIndex - 0.5;
                                        break;

                                    case "right":
                                        drag.groupedIndex = drop.groupedIndex + 0.5;
                                        break;
                                }
                            }

                            if (!dragInGroup) {
                                $.extend(true, drag, {
                                    groupInfo: {
                                        position: "header"
                                    }
                                });
                            }
                        },

                        afterRefresh: function () {
                            this._onColumnGrouped(<IColumnGroupedEventArgs>{ drag: drag, drop: drop, dragSource: dragSource, dropSource: dropSource, at: at });
                        }
                    });
                }
            } else {
                var traverseList = this._allColumns(),
                    dragAt = drag._parentIdx >= 0
                        ? (<c1bandfield>traverseList[drag._parentIdx]).options.columns
                        : this.options.columns,
                    dropAt = drop._parentIdx >= 0
                        ? (<c1bandfield>traverseList[drop._parentIdx]).options.columns
                        : this.options.columns,
                    dragLinearOffset = (dragAt === this.options.columns) ? this._virtualLeaves().length : 0, // calculate offsets because rowHeaderColumn is virtual
                    dropLinearOffset = (dropAt === this.options.columns) ? this._virtualLeaves().length : 0;

                if (this._onColumnDropping({ drag: drag, drop: drop, at: at })) {
                    this.ensureControl(false, {
                        beforeRefresh: function () {
                            /* modifying the wijgrid.options.columns option */
                            dragAt.splice(drag._linearIdx - dragLinearOffset, 1);

                            //because when drag is before drop, the index of drop is affected.
                            switch (at) {
                                case "left":
                                    if (dragAt === dropAt && drag._linearIdx < drop._linearIdx) {
                                        dropAt.splice(drop._linearIdx - dropLinearOffset - 1, 0, drag);
                                    } else {
                                        dropAt.splice(drop._linearIdx - dropLinearOffset, 0, drag);
                                    }
                                    break;

                                case "right":
                                    if (dragAt === dropAt && drag._linearIdx < drop._linearIdx) {
                                        dropAt.splice(drop._linearIdx - dropLinearOffset, 0, drag);
                                    } else {
                                        dropAt.splice(drop._linearIdx - dropLinearOffset + 1, 0, drag);
                                    }
                                    break;

                                case "center": // drop is a band
                                    (<IColumn>drop).columns.push(<IColumn>drag);
                                    break;
                            }
                        },

                        afterRefresh: function () {
                            this._onColumnDropped({ drag: drag, drop: drop, at: at });
                        }
                    });
                }
            }
        }

        _handleFilter(column: c1field, rawOperator, rawValue) {
            var operator = (new wijmo.grid.filterOperatorsCache(this)).getByName(rawOperator),
                value, ok;

            if (operator) {
                var opt = <IColumn>column.options;

                if (operator.arity > 1) {
                    // check value
                    if (opt.dataType === 'boolean' && rawValue.toLowerCase() !== 'true' && rawValue.toLowerCase() !== 'false') {
                        return;
                    }
                    value = this.parse(opt, rawValue);
                    ok = (value !== null && (wijmo.grid.getDataType(opt) === "string" || !isNaN(value)));
                } else {
                    ok = true;
                }

                if (ok) {
                    var args: IFilteringEventArgs = { column: opt, operator: operator.name, value: value };

                    if (this._onColumnFiltering(args)) {
                        opt.filterValue = args.value;
                        opt.filterOperator = args.operator;

                        this._resetDataProperitesOnFilterChanging();

                        if (this._customPagingEnabled()) {
                            this._onColumnFiltered({ column: opt }); // Allow user the ability to load a new data and refresh the grid.
                        } else {
                            this.ensureControl(true, {
                                afterRefresh: function () { this._onColumnFiltered({ column: opt }); }
                            });
                        }
                    }
                }
            }
        }

        _handleUngroup(column: c1groupedfield): void {
            var opt = <IColumn>column.options;

            if (this._onColumnUngrouping({ column: opt })) {

                this.ensureControl(true, {
                    beforeRefresh: function () {
                        delete opt.groupedIndex;

                        $.extend(true, opt, {
                            groupInfo: {
                                position: "none"
                            }
                        });
                    },

                    afterRefresh: function () {
                        this._onColumnUngrouped({ column: opt });
                    }
                });
            }
        }

        _onVirtualScrolling(newBounds: IRenderBounds, request: IVirtualScrollingRequest, mode: intersectionMode, scrollIndex: number, completeCallback: (scrollIndex: number) => void, data?: any /* opt*/) {
            var self = this;

            this.ensureControl(this._serverSideVirtualScrolling(), {
                virtualScrollData: {
                    direction: "v",
                    newBounds: newBounds,
                    request: request,
                    mode: mode,
                    data: data
                },
                beforeRefresh: function () {
                    (<wijmo.grid.fixedView>self._view()).resetRowsHeightsCache(); // don't use cached heights 
                },
                beforeOnRendered: function (userData) {
                    var bounds = self._viewPortBounds();
                    $.extend(bounds, userData.virtualScrollData.newBounds);
                },
                afterRefresh: function (userData) {
                    (<wijmo.grid.fixedView>self._view())._adjustRowsHeights(); // recalculate heights

                    self._UIFrozener().ensureVBarHeight();

                    if (completeCallback) {
                        completeCallback(scrollIndex);
                    }
                }
            });
        }

        _handleVerticalVirtualScrolling(scrollIndex: number, forceIntersectionMode: intersectionMode = null, completeCallback?: (scrollIndex: number) => void) {

            var view = <fixedView>this._view(),
                bounds: IRenderBounds = this._viewPortBounds(),
                newBounds = this._ensureRenderableBounds({
                    start: scrollIndex,
                    end: scrollIndex + view.getVirtualPageSize() - 1
                }),
                cachedDataBounds = this._ensureTotalRowsBounds({
                    start: this.mDataOffset,
                    end: this.mDataOffset + this.mDataViewWrapper.dataView().count() - 1
                }),
                request: IVirtualScrollingRequest = null,
                mode: intersectionMode,
                virtualPageSize = view.getVirtualPageSize();

            // check viewBounds
            if (forceIntersectionMode) {
                mode = forceIntersectionMode;
            } else {
                if (newBounds.start > bounds.end || newBounds.end < bounds.start) { // mode = "reset"
                    mode = intersectionMode.reset;
                } else {
                    if (newBounds.start > bounds.start) {
                        mode = intersectionMode.overlapBottom;
                    } else {
                        if (newBounds.start < bounds.start) {
                            mode = intersectionMode.overlapTop;
                        } else {
                            mode = newBounds.start !== bounds.start || newBounds.end !== bounds.end
                                ? intersectionMode.reset // TODO: enhance handling the case when new range is included in the old one.
                                : intersectionMode.none; // same range, "none"
                        }
                    }
                }
            }

            // quick fix 
            //mode = intersectionMode.reset;
            //

            // check dataBounds
            if (this._serverSideVirtualScrolling()) {
                switch (mode) {
                    case intersectionMode.reset: // align view bounds by pageSize
                        request = {
                            index: scrollIndex, // (scrollIndex == newBounds.start)
                            maxCount: virtualPageSize
                        };

                        break;

                    case intersectionMode.overlapBottom:
                        if (newBounds.end > cachedDataBounds.end) {
                            request = {
                                index: cachedDataBounds.end + 1,
                                maxCount: virtualPageSize
                            }
                        }

                        break;

                    case intersectionMode.overlapTop:
                        if (newBounds.start < cachedDataBounds.start) {
                            request = {
                                index: Math.max(0, cachedDataBounds.start - virtualPageSize),
                                maxCount: 0
                            };
                            request.maxCount = cachedDataBounds.start - request.index;
                        }

                        break;
                }
            }

            if (mode !== intersectionMode.none) {
                this._onVirtualScrolling(newBounds, request, mode, scrollIndex, completeCallback); // note: scrollIndex could be changed
            }
        }

        _handleHorizontalVirtualScrolling(range: renderableColumnsCollection, completeCallback?: () => void) {
            var self = this;

            this.mRenderableColumns = range;
            this.mRenderedLeaves = [];

            if (self._allowCellEditing() && self.currentCell()._isValid() && self.currentCell()._isEdit()) {
                self.endEdit(true);
            }

            this.ensureControl(false, {
                virtualScrollData: {
                    direction: "h"
                },
                afterRefresh: function (userData) {
                    //(<wijmo.grid.fixedView>self._view())._adjustRowsHeights(); // Slow. A better solution is placed in the fixedView._rowRendered method.
                    self._attachEvents();

                    if (completeCallback) {
                        completeCallback();
                    }
                }
            });
        }

        _serverSideVirtualScrolling() {
            return false;
        }

        _serverSideVirtualScrollingMargin() {
            var margin = (<fixedView>this._view()).getVirtualPageSize() * 2;

            if (margin < 20) {
                margin = 20;
            }

            return margin;
        }

        // * event handlers
        _onBeforeCellEdit(args: IBeforeCellEditEventArgs) {
            return this._trigger("beforeCellEdit", null, args);
        }

        _onBeforeCellUpdate(args: IBeforeCellUpdateEventArgs) {
            return this._trigger("beforeCellUpdate", null, args);
        }

        _onInvalidCellValue(args: IInvalidCellValueEventArgs) {
            return this._trigger("invalidCellValue", null, args);
        }

        _onAfterCellUpdate(args: IAfterCellUpdateEventArgs) {
            return this._trigger("afterCellUpdate", null, args);
        }

        _onAfterCellEdit(args: IAfterCellEditEventArgs) {
            return this._trigger("afterCellEdit", null, args);
        }

        _onColumnDropping(args: IColumnDroppingEventArgs) {
            return this._trigger("columnDropping", null, args);
        }

        _onColumnDropped(args: IColumnDroppedEventArgs) {
            this._trigger("columnDropped", null, args);
        }

        _onColumnGrouping(args: IColumnGroupingEventArgs) {
            return this._trigger("columnGrouping", null, args);
        }

        _onColumnGrouped(args: IColumnGroupedEventArgs) {
            this._trigger("columnGrouped", null, args);
        }

        _onColumnUngrouping(args: IColumnUngroupingEventArgs) {
            return this._trigger("columnUngrouping", null, args);
        }

        _onColumnUngrouped(args: IColumnUngroupedEventArgs) {
            this._trigger("columnUngrouped", null, args);
        }

        _onColumnFiltering(args: IFilteringEventArgs) {
            return this._trigger("filtering", null, args);
        }

        _onColumnFiltered(args: IFilteredEventArgs) {
            this._trigger("filtered", null, args);
        }

        _onFilterOperatorsListShowing(args: IFilterOperatorsListShowingEventArgs) {
            this._trigger("filterOperatorsListShowing", null, args);
        }

        _onColumnSorting(args: ISortingEventArgs) {
            return this._trigger("sorting", null, args);
        }

        _onColumnSorted(args: ISortedEventArgs) {
            this._trigger("sorted", null, args);
        }

        _onCurrentCellChanged(e: JQueryEventObject, info: ICurrentCellChangedAdditionalInfo) {
            var o = this.options,
                currentCell = this.currentCell(),
                rowInfo = currentCell.row(), // can be null

                completed = () => {
                    var currentCell = this.currentCell(),
                        dataRange = this._getDataCellsRange(dataRowsRangeMode.sketch);

                    if (this.options.highlightCurrentCell) {
                        var oldCell = new wijmo.grid.cellInfo(info.changingEventArgs.oldCellIndex, info.changingEventArgs.oldRowIndex, this);

                        if (oldCell._isValid && dataRange._containsCellInfo(oldCell)) {
                            this._highlightCellPosition(oldCell, false); // remove the old one
                        }

                        this._highlightCellPosition(currentCell, true); // highlight the current one
                    }

                    var domCell;
                    if (info.setFocus && (domCell = currentCell.tableCell())) {
                        this._setFocusOnCell(info.hasFocusedChild ? $(e.target) : $(domCell), info.hasFocusedChild, currentCell.columnInst());
                    }

                    this._trigger("currentCellChanged");

                    // ** set selection
                    if (info.changeSelection) {
                        this._updateSelection(e, info.selectionExtendMode);
                    }
                    // set selection **

                    // cell editing
                    var eventType = (e && (e.type || "").toLowerCase()) || "";
                    if (eventType === "click" && this._editBySingleClick()) {
                        this._beginEditInternal(e)
                    }
                };

            this.mCurrentCellLocker = true;

            // notify dataView
            this.mDataViewWrapper.currentPosition((rowInfo && (rowInfo.type & wijmo.grid.rowType.data)) ? rowInfo.dataItemIndex : -1);

            if (this._lgGetScrollMode() !== "none" && currentCell && !currentCell.isEqual(wijmo.grid.cellInfo.outsideValue)) {
                var scrollToCell = currentCell,
                    rowInfo = scrollToCell.row();

                if (rowInfo && !(rowInfo.type & wijmo.grid.rowType.data) && !scrollToCell.tableCell()) { // test for groupFoooter\ groupHeader
                    scrollToCell = currentCell._clone();
                    scrollToCell.cellIndex(0);
                }

                (<wijmo.grid.fixedView>this._view()).scrollTo(scrollToCell, () => {
                    try {
                        completed();
                    } finally {
                        this.mCurrentCellLocker = false;
                    }
                }, info);
            } else {
                try {
                    completed();
                } finally {
                    this.mCurrentCellLocker = false;
                }
            }
        }

        _onDetailCreating(args: IDetailCreatingEventArgs) {
            this._trigger("detailCreating", null, args);
        }

        _onDetailsLoaded(args: IDetailCreatingEventArgs) {
            this._trigger("detailsLoaded", null, args);
        }

        _onPageIndexChanging(args: IPageIndexChangingEventArgs) {
            return this._trigger("pageIndexChanging", null, args);
        }

        _onPageIndexChanged(args: IPageIndexChangedEventArgs) {
            this._trigger("pageIndexChanged", null, args);
        }

        _onPagerWidgetPageIndexChanging(sender, args) {
            args.handled = true; // prevent wijpager from re-drawing
        }

        _onPagerWidgetPageIndexChanged(sender, args) {
            this._setOption("pageIndex", args.newPageIndex);
        }

        _onRendering(userData: IUserDataArgs) {
            var view = this._view(),
                defCSS = wijmo.grid.wijgrid.CSS;

            this._rendered = false;
            // reset cached properties to react on options change properly (#119860)
            this.mAllowVVirtualScrolling = undefined;
            this.mAllowHVirtualScrolling = undefined;

            if (userData.virtualScrollData) {
                //this.selection().clear(); // clear selection

                //if (this.options.highlightCurrentCell) {
                //	this._highlightCellPosition(this.currentCell(), false); // remove highlighning
                //}
            } else {
                var currentCell = this._currentCell();
                this.mCurrentCellPrevCycle = currentCell
                    ? { x: currentCell.cellIndex(), y: currentCell.rowIndex(), dataItemIndex: currentCell.row() ? currentCell.row().dataItemIndex : -1 }
                    : { x: -1, y: -1, dataItemIndex: -1 };

                this._currentCell(null);

                if (view) {
                    view.dispose();
                }

                this._detachEvents(false);

                if (this.mRenderCounter === 0) {
                    wijmo.grid.emptyTable(<HTMLTableElement>this.element[0]); // IE workaround: clean original table manually during first rendering to avoid memory leak (#56601).
                }

                if (!this.mOuterDiv[0].style.height) {
                    // #64328: If height is empty then fix it temporary to avoid jumps of the page's vertical scrollbar. Height will be restored in the _onRendered method.
                    this._isHeightTemporaryFixed = true;
                    this.mOuterDiv.height(this.mOuterDiv.height());
                }

                this.element.detach();

                this.element.css({ // Chrome issue
                    "table-layout": "",
                    "width": ""
                });

                this.element.empty();

                //this.mOuterDiv.empty();
                this.mOuterDiv.children(":not(." + defCSS.spinnerMarker + ")").remove(); // remove all child nodes except the spinner.

                this.mOuterDiv.append(this.element);

                if (this._selectionui) {
                    this._selectionui.dispose();
                    this._selectionui = null;
                }

                if (this._resizerui) {
                    this._resizerui.dispose();
                    this._resizerui = null;
                }

                if (this._frozenerui) {
                    this._frozenerui.dispose();
                    this._frozenerui = null;
                }

                if (this.mKeyDownEventListener) {
                    this.mKeyDownEventListener.dispose();
                    this.mKeyDownEventListener = null;
                }
            }

            this._trigger("rendering");
        }

        _onRendered(userData: IUserDataArgs) {
            var view = this._view(),
                currentCell: wijmo.grid.cellInfo,
                hasSelection = this.selection().selectedCells().length() > 0;

            this._rendered = true;
            this.mRenderCounter++;

            if (!userData.virtualScrollData) {
                if (this._isHeightTemporaryFixed) {
                    this._isHeightTemporaryFixed = false;
                    this.mOuterDiv[0].style.height = "";
                }

                this.mSelection = null; // always recreate selection object

                // attach events
                this._attachEvents();

                // ** current cell
                this._setAttr(view.focusableElement(), "tabIndex", 0); // to handle keyboard\ focus events

                currentCell = this._currentCellFromDataView(this.mCurrentCellPrevCycle.x);

                if (!currentCell._isValid() && this.options.showSelectionOnRender) {
                    currentCell = this._getFirstDataRowCell(0);
                }

                this.currentCell(currentCell, null, true);
                // current cell *

                // selection ui
                this._UISelection(true);

                this._setSizeInternal(this.mScrollingState); // set sizes, create wijsuperpanel, restore scrolling state.
            } else {
                var currentCell = this.currentCell();
                this.currentCell(currentCell, null, false);

                if (currentCell._isEdit() && !currentCell._isRendered()) {
                    currentCell._isEdit(false); // reset editing if sketchRow or column is not available (C1GridView virtual scrolling)
                }

                var selection = this.selection();
                selection._ensureSelection();
            }

            // initialize resizer.
            if (!userData.virtualScrollData || this._allowHVirtualScrolling()) {
                this._recreateResizerUI();
            }

            this._trigger("rendered");
        }

        _recreateResizerUI() {
            if (this._resizerui) {
                this._resizerui.dispose();
            }

            this._resizerui = new wijmo.grid.uiResizer(this);

            var leaves = this._allowHVirtualScrolling() ? this.columns() : this._renderedLeaves();

            $.each(leaves, (idx, leaf) => {
                if (leaf._isRendered() && leaf.options._isLeaf) {
                    this._resizerui.addElement(leaf);
                }
            });
        }

        // note: can be called by the _onFocusIn method!
        _onClick(e: JQueryEventObject) {
            if (!this._canInteract() || !e.target) {
                return;
            }

            var view = this._view(),
                clickedCell = <HTMLTableCellElement>this._findUntilOuterDiv(<Element>e.target, { td: true, th: true }),
                $row: JQuery,
                clickedCellInfo: wijmo.grid.cellInfo,
                extendMode: wijmo.grid.cellRangeExtendMode = wijmo.grid.cellRangeExtendMode.none,
                currentCell: wijmo.grid.cellInfo,
                selection: wijmo.grid.selection,
                clickedCellChanged = false,
                clickEvent = !!(e && (e.type === "click")); // real click event?

            if (clickedCell) {
                if (clickEvent && $(e.target).hasClass(wijmo.grid.wijgrid.CSS.groupToggleVisibilityButton)) {
                    this._onGroupBtnClick(e);
                    // #29676: stop event from bubbling up to the parent grid (if available)
                    e.stopPropagation();
                    return false;
                }

                $row = $(clickedCell).closest("tr");

                if (!$row.length) {
                    return;
                }

                clickedCellInfo = view.getAbsoluteCellInfo(clickedCell, false);

                if ($row.hasClass(wijmo.grid.wijgrid.CSS.dataRow) || $row.hasClass(wijmo.grid.wijgrid.CSS.headerRow) || $row.hasClass(wijmo.grid.wijgrid.CSS.filterRow)) {
                    if (clickedCellInfo.cellIndex() < 0 || clickedCellInfo.rowIndex() < 0) { // header cell, rowheader cell or filter cell

                        if (clickedCellInfo.rowIndex() >= 0) { // rowheader cell
                            // move current cell to the first cell of the clicked row
                            if (this.options.scrollingSettings.virtualizationSettings.mode !== "none") {
                                clickedCellInfo = view.getAbsoluteCellInfo(clickedCell, true);
                            }
                            clickedCellInfo = new wijmo.grid.cellInfo(0, clickedCellInfo.rowIndex());
                            extendMode = wijmo.grid.cellRangeExtendMode.row;
                            clickedCellChanged = true;
                        } else { // header cell or filter cell
                            if (clickedCellInfo.cellIndex() >= 0) {
                                if (this._allowHVirtualScrolling()) {
                                    clickedCellInfo = view.getAbsoluteCellInfo(clickedCell, true);//get absolute cell idx (_visLeavesIdx) from rendered leaf's option when using virtualization. 
                                }
                                // move current cell to the first data cell of the clicked column
                                clickedCellInfo = this._getFirstDataRowCell(clickedCellInfo.cellIndex());
                                extendMode = wijmo.grid.cellRangeExtendMode.column;
                                clickedCellChanged = true;
                            }
                        }
                    }

                    if (!clickedCellChanged) {
                        clickedCellInfo = view.getAbsoluteCellInfo(clickedCell, true);
                    }
                    
                    // change current cell and set focus to it (if the target element is not already focused)
                    this._changeCurrentCell(e, clickedCellInfo, {
                        changeSelection: true,
                        setFocus: clickEvent,
                        selectionExtendMode: extendMode
                    });
                }

                if (clickEvent) {
                    var cellClickedArgs: ICellClickedEventArgs = { cell: clickedCellInfo };
                    this._trigger("cellClicked", null, cellClickedArgs);
                }
            }
        }

        _onDblClick(e: JQueryEventObject) {
            if (!this._editBySingleClick()
                && this._testForThis(<HTMLElement>e.target)) { // ignore parent grids
                this._beginEditInternal(e);
            }
        }

        _onGroupBtnClick(e: JQueryEventObject) {
            if (!this._canInteract()) {
                return;
            }

            var $row = $(e.target).closest("tr"),
                gh = wijmo.grid.groupHelper,
                groupInfo = gh.getGroupInfo(<HTMLTableRowElement>$row[0]),
                self = this;

            if (groupInfo) {
                var column = gh.getColumnByGroupLevel(this._leaves(), groupInfo.level),
                    rowsToAdjust: cellRange = null;

                if (column) {
                    var group = (<IColumn>column.options).groupInfo.expandInfo[groupInfo.index];

                    if (group.isExpanded) {
                        group.collapse(this, <c1field>column);
                    } else {
                        group.expand(this, <c1field>column, e.shiftKey);
                    }

                    if (this._allowVVirtualScrolling()) {
                        this._rebuildRenderableRowsCollection();
                        (<fixedView>this._view()).vsUI._changeVisibleRowsCount(this._renderableRowsRange().capacity());

                        this._ensureLowerBoundVisible(this._viewPortBounds());

                        this._handleVerticalVirtualScrolling(this._viewPortBounds().start, intersectionMode.reset);
                    } else {
                        rowsToAdjust = group.cr; // adjust heights of the rows which are belongs to the current group only (#95085, speedup group expanding\ collapsing)
                    }

                    //this._view().ensureHeight(); /*dma*/

                    this.setSize(undefined, undefined, rowsToAdjust); // recalculate sizes (#39295)
                }
            }
        }

        _onKeyDown(e: JQueryEventObject) {
            var isKeyDownListenerElement = this._KeyDownEventListener().isHiddenInput($(e.target));

            if (!this._canInteract() || (isKeyDownListenerElement && this._KeyDownEventListener().canHandle(e))) { // the focus is inside the hidden input and the printable key has been pressed.
                return true;
            }

            var tag = (<Element>e.target).tagName.toLowerCase(),
                $target = $(e.target),
                currentCell = this.currentCell(),
                keyCodeEnum = wijmo.getKeyCodeEnum(),
                $outerDiv: JQuery = undefined;

            if (!isKeyDownListenerElement
                // && $target.is(":focusable") // "unsupported pseudo: focusable" exception in IE10?
                && (tag === "input" || tag === "option" || tag === "select" || tag === "textarea")
                && ($target.closest("tr.wijmo-wijgrid-datarow").length === 0)) {
                return true;
            }

            if (this._allowCellEditing()) {
                // ESC: cancel editing, F2: finish editing
                if ((e.which === keyCodeEnum.ESCAPE || e.which === 113) && (currentCell._isValid() && currentCell._isEdit())) {
                    this._endEditInternal(e, e.which === keyCodeEnum.ESCAPE);
                    return false;
                } else {
                    if (e.which === 113) { // F2: start editing
                        this._beginEditInternal(e);
                        return false;
                    }
                }
            }

            if (!this.options.allowKeyboardNavigation) {
                return true;
            }

            var nextCell: wijmo.grid.cellInfo;

            switch (e.which) {
                case keyCodeEnum.DOWN:
                case keyCodeEnum.PAGE_DOWN:
                case keyCodeEnum.UP:
                case keyCodeEnum.PAGE_UP:
                case keyCodeEnum.LEFT:
                case keyCodeEnum.RIGHT:
                case keyCodeEnum.HOME:
                case keyCodeEnum.END:
                case keyCodeEnum.TAB:

                    if (e.which === keyCodeEnum.TAB) {
                        var visibleLeaves = this._visibleLeaves();
                        // There is no data in the grid, will bubble the keydown event out of the grid.
                        if (!visibleLeaves || !visibleLeaves.length) {
                            return true;
                        }
                    }

                    if (isKeyDownListenerElement || this._canMoveToAnotherCell(<Element>e.target, e.which)) {
                        var dataRange = this._getDataCellsRange(dataRowsRangeMode.renderable);
                        nextCell = this._getNextCurrentCell(dataRange, currentCell, e.keyCode, e.shiftKey);
                    }

                    break;
            }

            if (nextCell) {
                // change current cell and set focus to it (always set focus to change the IME mode).
                this._changeCurrentCell(e, nextCell, { changeSelection: true, setFocus: true });
                return false; // stop bubbling
            }
            else {//move the focus out of the grid when reach the TopLeft of the grid 

                if (e.which === keyCodeEnum.TAB) {
                    $outerDiv = $(this._view().focusableElement());
                    if (e.shiftKey) {
                        if ($outerDiv && e.target !== $outerDiv[0]) {
                            $outerDiv.focus();
                            return false;
                        }
                    }
                    else {
                        if (e.target === $outerDiv[0] && currentCell._isValid()) {
                            this._setFocusOnCell($(currentCell.tableCell()), false, currentCell.columnInst());
                        }
                    }
                }

            }

            return true;
        }

        _onFocusIn(e: JQueryEventObject) {
            if (this.options.allowKeyboardNavigation
                && e && e.target && $(e.target).is(":input") // optimization, test for inputs only
                && !this._KeyDownEventListener().isHiddenInput($(e.target)) // optimization, ignore IME hidden inputs
                && this._testForThis(<HTMLElement>e.target)) { // ignore parent grid

                this._onClick(e); // simulate a click
            }
        }

        _onMouseMove(e: JQueryEventObject) {
            if (!this.options.highlightOnHover || !this._canInteract() || (this._frozenerui && this._frozenerui.inProgress())) {
                return;
            }

            var hovCell = this._findUntilOuterDiv(<Element>e.target, { td: true, th: true });

            if (hovCell) {
                var hovRow = $(hovCell).closest("tr");

                if (!hovRow.length || !(hovRow.hasClass(wijmo.grid.wijgrid.CSS.dataRow) || hovRow.hasClass(wijmo.grid.wijgrid.CSS.headerRow))) {
                    return;
                }

                var hovCellInfo = this._view().getAbsoluteCellInfo(<HTMLTableCellElement>hovCell, true),
                    hovSketchIndex = hovCellInfo.rowIndex();

                if (hovSketchIndex !== this._hover()) {
                    this._hover(-1); // clear previous row
                    this._hover(hovSketchIndex); // highlight current row
                }
            }
        }

        _onToggleHierarchy(e: JQueryEventObject, row: IRowInfo) {
            if (!this._canInteract()) {
                return;
            }

            var detail = this.details()[row.dataRowIndex];

            if (detail.isExpanded()) {
                detail.collapse();
                //this.setSize();
            } else {
                detail.expand();
            }
        }

        _onMouseOut(e: JQueryEventObject) {
            if (!this._canInteract()) {
                return;
            }

            if (this._hover() >= 0) {
                var firstParentRow = this._findUntilOuterDiv(e.relatedTarget, { tr: true });

                if (!firstParentRow || !$(firstParentRow).hasClass(wijmo.grid.wijgrid.CSS.dataRow)) { // remove hovering
                    this._hover(-1); // clear
                }
            }
        }

        _onWinResize(e: JQueryEventObject) {
            var self = this;

            if (wijmo.grid.wijgrid.IGNORE_WIN_RESIZE) {
                return;
            }

            // reset timer
            if (this._windowResizeTimer > 0) {
                window.clearTimeout(this._windowResizeTimer);
                this._windowResizeTimer = 0;
            }

            if (this._windowResizeTimer !== -1) {
                this._windowResizeTimer = window.setTimeout(function () {
                    self._windowResizeTimer = -1; // lock

                    try {
                        if (!self.mDestroyed && self._initialized && self.element.parent().length && self.element.is(":visible")) {
                            var orientation = wijmo.grid.getWindowOrientation(); // browser-specific

                            // #64466: ignore Android (triggers "resize" event on virtual keyboard popup) except orientation changing.
                            var resize = self.mResizeOnWindowResize && (!wijmo.grid.isMobile() || (orientation !== self._winOrientation));

                            self._winOrientation = orientation;

                            if (resize) {
                                self._onWindowResize();
                                self.setSize();
                            }
                        }
                    }
                    finally {
                        self._windowResizeTimer = 0; // unlock
                    }
                }, wijmo.grid.wijgrid.WIN_RESIZE_TIMEOUT);
            }
        }

        _onWindowResize() {
        }

        // * event handlers

        // * resizing
        _fieldResized(column: c1basefield, oldWidth, newWidth) {
            if (oldWidth < 0) {
                oldWidth = 0;
            }

            if (newWidth <= 0) {
                newWidth = 1;
            }

            var resizingArgs: IColumnResizingEventArgs = {
                column: <IColumn>column.options,
                oldWidth: oldWidth,
                newWidth: newWidth
            };

            if (this._trigger("columnResizing", null, resizingArgs) !== false) {
                if (isNaN(resizingArgs.newWidth) || resizingArgs.newWidth < 0) {
                    resizingArgs.newWidth = 1;
                }

                column.option("width", resizingArgs.newWidth); // update widget option first (tfs issue 32108)

                if (this._allowHVirtualScrolling()) {
                    this.doRefresh();
                } else {
                    if (this._allowVVirtualScrolling() && !this._serverSideVirtualScrolling() && (this._lgGetScrollMode() === "auto")) {
                        var view = <fixedView>this._view();
                        if (view._isLastItemRendered() && view._canRenderMoreItems()) {
                            this.doRefresh(); // 82576 (virtual scrolling) - when any column is resized such that vscrollbar is visible and then the grid is scrolled down and any columm is resized again so that vscrollbar is removed then the first row is not displayed. 
                        }
                    }
                }

                var resizedArgs: IColumnResizedEventArgs = { column: <IColumn>column.options };
                this._trigger("columnResized", null, resizedArgs);
            }
        }
        // * resizing

        _setFocusOnCell(element: JQuery, focusableChild: boolean, column: c1basefield) {
            if (focusableChild) {
                //if (!element.is(":focus")) {
                //	element.focus(); // ensure that element still has focus.
                //}
            } else {
                this._KeyDownEventListener().focus(element, column);
            }
        }

        _findFocusedChild(parent: JQuery): JQuery {
            var result = parent.find(":focus");
            return result.length ? result : null;
        }

        // * currentCell
        // returns false when currentCellChanging event was cancelled, otherwise true.
        _changeCurrentCell(e: JQueryEventObject, cellInfo: wijmo.grid.cellInfo, info: ICurrentCellChangedAdditionalInfo): boolean {
            if (this.mCurrentCellLocker) { // skip until the _onCurrentCellChanged will not be completed.
                return;
            }

            var currentCell = this.currentCell(),
                dataRange = this._getDataCellsRange(dataRowsRangeMode.sketch);

            // if cellInfo has a valid value
            if ((dataRange._isValid() && dataRange._containsCellInfo(cellInfo)) || (cellInfo.isEqual(wijmo.grid.cellInfo.outsideValue))) {
                var $target = e && e.target && $(e.target),
                    domCell = cellInfo.tableCell(),
                    $container = cellInfo.container(),
                    $focusedChild: JQuery = null;

                info.hasFocusedChild = !!(domCell && $target && ($target[0] !== domCell) && ($target[0] !== $container[0])
                    && !this._KeyDownEventListener().isHiddenInput($target)
                    && /* $target.is(":focus") */ ($focusedChild = this._findFocusedChild($container))); // $focusedChild can differs from $target (#56472)

                // other cell than current cell
                if (currentCell.cellIndex() !== cellInfo.cellIndex() || currentCell.rowIndex() !== cellInfo.rowIndex()) {
                    var currentCellChangingArgs: ICurrentCellChangingEventArgs = {
                        cellIndex: cellInfo.cellIndex(),
                        rowIndex: cellInfo.rowIndex(),
                        oldCellIndex: currentCell.cellIndex(),
                        oldRowIndex: currentCell.rowIndex()
                    };
                    
                    if (this._trigger("currentCellChanging", null, currentCellChangingArgs)) {
                        var cellEditCompleted = false;

                        if (!this._allowCellEditing() || !currentCell._isEdit() || (cellEditCompleted = this._endEditInternal(null))) {
                            info.changingEventArgs = currentCellChangingArgs;

                            currentCell = cellInfo._clone();
                            currentCell._setGridView(this);
                            
                            this._currentCell(currentCell); // set currentCell
                            
                            this._onCurrentCellChanged(e, info);
                        }
                    } else {
                        return false;
                    }
                } else { // the same cell
                    if (this.options.highlightCurrentCell) {
                        this._highlightCellPosition(currentCell, true); // ensure
                    }

                    if (info.changeSelection) {
                        this._updateSelection(e, info.selectionExtendMode); // ensure selection
                    }

                    if (domCell && !currentCell._isEdit()) {
                        if (e && this._editBySingleClick()) {
                            this.mCurrentCellLocker = true; // avoid focusin-click recursion if editingInitOption="click" and currentCell is clicked.
                            this._beginEditInternal(e);
                            this.mCurrentCellLocker = false;
                        } else {
                            if (info.setFocus) {
                                this._setFocusOnCell(info.hasFocusedChild ? $focusedChild : $(domCell), info.hasFocusedChild, currentCell.columnInst());
                            }
                        }
                    }
                }
            } else { // cellInfo is invalid
                // do nothing

                // this._highlightCellPosition(currentCell, false);
                // this._field("currentCell", currentCell.outsideValue); // set currentCell
            }

            return true;
        }

        _highlightCellPosition(cellInfo: wijmo.grid.cellInfo, add: boolean) {
            if (cellInfo && cellInfo._isValid()) {

                var x = cellInfo.cellIndexAbs(),
                    y = cellInfo.rowIndexAbs(),
                    $rs = wijmo.grid.renderState,
                    view = this._view(),
                    rowInfo: IRowInfo,
                    obj, state;

                // * column header cell * - change even if the cell is not rendered.
                obj = view.getHeaderCell(x);
                if (obj) {
                    rowInfo = view._getRowInfo(this._headerRows().item(cellInfo.column()._thY));

                    obj = $(obj);
                    state = view._changeCellRenderState(obj, $rs.current, add);

                    // highlight column header cell
                    this.mCellStyleFormatter.format(obj, x, cellInfo.columnInst(), rowInfo, state);
                }

                if (cellInfo._isRendered()) {
                    // * row header cell *
                    obj = view.getJoinedRows(y, 0);
                    if (obj) {
                        // change row state
                        rowInfo = view._getRowInfo(obj);
                        view._changeRowRenderState(rowInfo, $rs.current, add);

                        // highlight row header cell
                        this.mRowStyleFormatter.format(rowInfo);
                    }

                    // * data cell *
                    obj = view.getCell(x, y);
                    if (obj) {
                        obj = $(obj);
                        state = view._changeCellRenderState(obj, $rs.current, add);

                        // highlight data cell
                        this.mCellStyleFormatter.format(obj, x, cellInfo.columnInst(), rowInfo, state);  // rowInfo is taken from the previous step
                    }
                }
            }
        }

        _hover(sketchRowIndex?: number): number {
            if (arguments.length) {
                if (sketchRowIndex >= 0) {
                    this._hoverRowCss(sketchRowIndex, true); // highlight
                    this._prevHoveredSketchRowIndex = sketchRowIndex;
                } else {
                    this._hoverRowCss(this._prevHoveredSketchRowIndex, false); // clear
                    this._prevHoveredSketchRowIndex = -1;
                }
            }

            return this._prevHoveredSketchRowIndex;
        }

        _hoverRowCss(sketchRowIndex: number, add: boolean): void {
            if (sketchRowIndex >= 0) {
                var view = this._view(),
                    row = view._getRowInfoBySketchRowIndex(sketchRowIndex);

                if (row) {
                    view._changeRowRenderState(row, wijmo.grid.renderState.hovered, add);

                    if (row.$rows) {
                        this.mRowStyleFormatter.format(row);
                    }
                }
            }
        }

        // * currentCell

        // * editing
        _beginEditInternal(e: JQueryEventObject): boolean {
            if (this._canInteract() && this._allowCellEditing()) {
                var cell = this.currentCell(),
                    column = cell.column(),
                    res: boolean;

                if (column && !column.readOnly) {
                    res = new wijmo.grid.cellEditorHelper().cellEditStart(this, cell, e);

                    //if (res) {
                    //	this._view().ensureWidth(undefined, column._visLeavesIdx);
                    //}

                    return res;
                }
            }

            return false;
        }

        _endEditInternal(e: JQueryEventObject, reject: boolean = false): boolean {
            if (this._canInteract() && this._allowCellEditing()) {
                var cell = this.currentCell(),
                    cellEditor = new wijmo.grid.cellEditorHelper();

                // Fix the TFS issue #87950
                var row = cell.row();
                if (row && !row.data) {
                    return false;
                }
                // Fix the TFS issue #87950

                var updateRes = cellEditor.updateCell(this, cell, e, reject);

                if (updateRes & updateCellResult.success) {
                    if (updateRes & updateCellResult.notEdited) {
                        return true;
                    }

                    cellEditor.cellEditEnd(this, cell, e);

                    cell = this.currentCell();

                    // set focus to listen keypress\ keydown event.
                    var domCell = cell.tableCell();
                    if (domCell) {
                        this._KeyDownEventListener().focus($(domCell), cell.columnInst());
                    } else {
                        $(this._view().focusableElement()).focus();
                    }

                    //Fix TFS-440410-issue1 cause view go to wrong position after end-edit.
                    /*
                    if (!this._allowVVirtualScrolling() && // avoid horizontal scrollbar movement.
                        !(cell.columnInst() instanceof c1booleanfield)) { // Otherwise the original event will be lost because of superpanel recreation and clicked chexbox won't be toggled (#55913)
                        this._view().ensureHeight(cell.rowIndex());
                    }
                    */
                    return true;
                }
            }

            return false;
        }
        // * editing

        // * view handlers
        _onViewInsertEmptyRow(rowType: wijmo.grid.rowType, renderState: wijmo.grid.renderState, sectionRowIndex: number, dataRowIndex: number, dataItemIndex: number, virtualDataItemIndex: number, sketchRowIndex: number, groupByValue: any): HTMLTableRowElement {
            return null; // default action
        }

        _onViewCreateEmptyCell(rowInfo: IRowInfo, dataCellIndex: number, column: c1basefield): HTMLTableCellElement {
            return null; // default action
        }

        _onViewCellRendered(cell: JQuery, container: JQuery, row: IRowInfo, cellIndex: number, column: c1basefield) {
        }

        _onViewRowRendered(rowInfo: IRowInfo, rowAttr?, rowStyle?) {
        }

        // view handlers *

        // misc
        _readTableSection(table: JQuery, scope: wijmo.grid.rowScope, readAttributes?: boolean): any[];
        _readTableSection(table: HTMLTableElement, scope: wijmo.grid.rowScope, readAttributes?: boolean): any[];
        _readTableSection(table, scope: wijmo.grid.rowScope, readAttributes?: boolean): any[] {
            var decodeHTML = (scope === wijmo.grid.rowScope.body) && (this._serverShaping()),
                trim = this.options._htmlTrimMethod;

            return wijmo.grid.readTableSection(table, scope, decodeHTML, trim, readAttributes);
        }

        _getDataParser(column: IColumn): wijmo.data.IParser {
            return <wijmo.data.IParser><any>column.dataParser ||
                wijmo.data.defaultParsers[column.dataType] ||
                wijmo.data.defaultParsers.string;
        }

        _needParseColumnDOM(column: IColumn): boolean {
            return (wijmo.grid.getDataType(column) !== "string" && wijmo.grid.validDataKey(column.dataKey));
        }

        _getDOMReaderCulture(): GlobalizeCulture {
            return null; // use user-defined culture (options.culture). c1gridview overrides this method to use invariant culture.
        }

        /** @ignore */
        parseCtx(column: IColumn, value: any, dataItem: any, cell: HTMLTableCellElement, forcedCulture?: GlobalizeCulture): any {
            return this.parse(column, value, forcedCulture);
        }

        /** @ignore */
        parseCtxFailed(column: IColumn, value: any, dataItem: any, cell: HTMLTableCellElement, forcedCulture?: GlobalizeCulture): any {
            return value;
        }

        /** @ignore */
        parse(column: IColumn, value: any, forcedCulture?: GlobalizeCulture): any {
            //// old behaviour
            //var parser = this._getDataParser(column);
            //return parser.parse(value, this._field("closestCulture"), column.dataFormatString, this.options.nullString, true);

            var fromType = wijmo.grid.getDataType(column),
                toType = wijmo.grid.getDataType(column);

            if ($.isFunction(value)) {
                value = value(); // observable
            }

            value = wijmo.data.convert(value, fromType, toType, {
                culture: forcedCulture || this.mClosestCulture,
                format: column.dataFormatString,
                nullString: this.options.nullString,
                parser: <any>column.dataParser // custom parser
            });

            return value;
        }

        /** @ignore */
        toStr(column: IColumn, value: any, forcedCulture?: GlobalizeCulture): string {
            //// old behaviour
            //var parser = this._getDataParser(column);
            //return parser.toStr(value, this._field("closestCulture"), column.dataFormatString, this.options.nullString, true);

            var dataView = this.mDataViewWrapper.dataView(),
                fromType = wijmo.grid.getDataType(column), // column._underlyingDataType || "string",
                toType = "string";

            value = wijmo.data.convert(value, fromType, toType, {
                culture: forcedCulture || this.mClosestCulture,
                format: column.dataFormatString,
                nullString: this.options.nullString,
                parser: <any>column.dataParser // custom parser
            });

            return value;
        }

        _funcOptions(): string[] {
            return ["cellStyleFormatter", "rowStyleFormatter", "afterCellEdit", "afterCellUpdate", "beforeCellEdit", "beforeCellUpdate",
                "cellClicked",
                "columnDragging", "columnDragged", "columnDropping", "columnDropped", "columnResizing", "columnResized",
                "columnGrouping", "columnGrouped", "columnUngrouping", "columnUngrouped",
                "currentCellChanging", "currentCellChanged",
                "detailCreating",
                "filtering", "filtered",
                "filterOperatorsListShowing", "groupAggregate", "groupText", "invalidCellValue", "pageIndexChanging", "pageIndexChanged",
                "selectionChanged", "sorting", "sorted", /*"ajaxError",*/"dataLoading", "dataLoaded", "loading", "loaded", "rendering", "rendered"];
        }

        _canInteract(): boolean {
            return !this.options.disabled && this.mLoadingDetails === 0; /* && this._dataViewWrapper.isLoaded();*/
        }

        _canMoveToAnotherCell(domElement: Element, keyCode: number): boolean {
            var tag = domElement.tagName.toLowerCase(),
                len, selectionRange, kc, res;

            switch (tag) {
                case "input":
                    if ($(domElement).hasClass(wijmo.grid.wijgrid.CSS.inputMarker)) {
                        var input = <HTMLInputElement>domElement;

                        if (input.type === "text") {
                            len = input.value.length;
                            selectionRange = new wijmo.grid.domSelection(input).getSelection();

                            kc = wijmo.getKeyCodeEnum();

                            res = ((keyCode === kc.TAB) ||
                                (keyCode === kc.UP || keyCode === kc.DOWN || keyCode === kc.PAGE_DOWN || keyCode === kc.PAGE_UP) ||
                                (selectionRange.length === 0 &&
                                    (
                                        (selectionRange.start === 0 && (keyCode === kc.LEFT || keyCode === kc.HOME)) ||
                                        (selectionRange.end >= len && (keyCode === kc.RIGHT || keyCode === kc.END))
                                    )
                                ));

                            return res;
                        }

                        return true;
                    }

                    return false;

                case "textarea":
                case "select":
                    return false;
            }

            return true;
        }

        _editBySingleClick(): boolean {
            var value = (this.options.editingInitOption || "").toLowerCase();

            switch (value) {
                case "click":
                case "doubleclick":
                    break;

                case "auto":
                default:
                    value = this._isMobileEnv() ? "click" : "doubleclick";
                    break;
            }

            return value === "click";
        }

        _getDataToAbsOffset(): { x: number; y: number; } {
            var x = 0,
                y = 0,
                headerRows = this._headerRows();

            x += this._virtualLeaves().length;

            if (headerRows) {
                y += headerRows.length();
            }

            if (this._filterRow()) {
                y++;
            }

            return {
                x: x,
                y: y
            };
        }

        _currentCellFromDataView(cellIndex: number): cellInfo {
            var dataViewRowIndex = this.mDataViewWrapper.currentPosition(),
                cellInfo = new wijmo.grid.cellInfo(cellIndex, this._dataViewDataRowIndexToGrid(dataViewRowIndex), null);

            // normalize
            if (cellInfo.rowIndex() < 0) {
                cellInfo.cellIndex(-1);
            } else {
                if (cellInfo.cellIndex() < 0) {
                    cellInfo.cellIndex(0);
                }
            }

            return cellInfo;
        }

        _dataViewDataRowIndexToGrid(rowIndex: number): number {

            if ((rowIndex >= 0) && this.mSketchTable) {
                for (var i = 0; i < this.mSketchTable.count(); i++) {
                    var sketchRow = this.mSketchTable.row(i);

                    if (sketchRow.isDataRow() && sketchRow.dataItemIndex() === rowIndex) {
                        return i;
                    }
                }
            }

            return -1;
        }

        _getDataCellsRange(mode: wijmo.grid.dataRowsRangeMode, lastRowEntirelyVisible = false): wijmo.grid.cellInfoRange {
            var minCol = 0,
                minRow = 0,
                maxCol = this._visibleLeaves().length - 1,
                maxRow: number;

            switch (mode) {
                case dataRowsRangeMode.rendered:
                    if (this._rendered) {
                        maxRow = this._rows().length() - 1;
                        break;
                    }
                // otherwise fallthrough to the .sketch

                case dataRowsRangeMode.sketch:
                    maxRow = this._totalRowsCount() - 1;
                    break;

                case dataRowsRangeMode.renderable:
                    maxRow = this._renderableRowsCount() - 1;
                    break;
            }

            maxCol -= this._virtualLeaves().length;

            if (lastRowEntirelyVisible && maxRow > 0) {
                maxRow--;
            }

            if (maxCol < 0 || maxRow < 0) {
                minCol = minRow = maxCol = maxRow = -1;
            }

            return new wijmo.grid.cellInfoRange(new wijmo.grid.cellInfo(minCol, minRow, null), new wijmo.grid.cellInfo(maxCol, maxRow, null));
        }

        _getDataItem(dataItemIndex: number): any {
            return this.dataView().item(dataItemIndex);
        }

        _getFirstDataRowCell(absCellIndex: number): wijmo.grid.cellInfo {
            var rowIndex, len, rowInfo,
                view = this._view(),
                rows = this._rows(),
                $rt = wijmo.grid.rowType;

            for (rowIndex = 0, len = rows.length(); rowIndex < len; rowIndex++) {
                rowInfo = view._getRowInfo(rows.item(rowIndex));

                if (rowInfo.type & $rt.data) {
                    return new wijmo.grid.cellInfo(absCellIndex, rowIndex);
                }
            }

            return wijmo.grid.cellInfo.outsideValue;
        }

        _getNextCurrentCell(dataRange: wijmo.grid.cellInfoRange, cellInfo: wijmo.grid.cellInfo, keyCode: number, shiftKeyPressed: boolean): wijmo.grid.cellInfo {
            var cellIndex = cellInfo.cellIndex(),
                rowIndex = cellInfo.rowIndex(),
                keyCodeEnum = wijmo.getKeyCodeEnum(),
                sketchTable = this.mSketchTable,
                renderedRowIndex = this._renderableRowsRange().getRenderedIndex(rowIndex),
                rbc = this._renderableRowsRange(),
                tmp: number,
                dataRowsOnly = (keyCode === keyCodeEnum.TAB),
                self = this,
                sss = self._serverSideVirtualScrolling(),
                nextCell: wijmo.grid.cellInfo = null,
                column: IColumn = null,

                findNextVisibleRow = function (further: boolean, startFrom: number, maxRowsToTouch, dataRowsOnly: boolean = false): number {
                    var iterator = further ? rbc.forEachIndex : rbc.forEachIndexBackward,
                        cnt = 0,
                        result: number = null;

                    iterator.call(rbc, startFrom, -1, (absIdx) => {
                        var sketchIdx = absIdx - self.mDataOffset;

                        if (sss && (sketchIdx < 0 || sketchIdx >= sketchTable.count())) { // exceeded sketchTable bounds
                            result = absIdx;
                            return false;
                        }

                        var sketchRow = sketchTable.row(sketchIdx), // this row is rendered, but we doesn't know whether it visible or not.
                            isDataRow = sketchRow.isDataRow();

                        // test dataType and visibility
                        if ((!dataRowsOnly || isDataRow) && (!(sketchRow.extInfo.state & wijmo.grid.renderStateEx.hidden))) {
                            result = absIdx;

                            if (++cnt >= maxRowsToTouch) {
                                return false; // stop iteration
                            }
                        }
                    });

                    return result;
                };

            switch (keyCode) {
                case keyCodeEnum.ENTER:
                case keyCodeEnum.DOWN:
                    if ((tmp = findNextVisibleRow(true, renderedRowIndex + 1, 1)) != null) {
                        rowIndex = tmp;
                    }
                    break;

                case keyCodeEnum.PAGE_DOWN:
                    if ((tmp = findNextVisibleRow(true, renderedRowIndex + 1, this.mPageSizeKey)) != null) {
                        rowIndex = tmp;
                    }
                    break

                case keyCodeEnum.UP:
                    if ((tmp = findNextVisibleRow(false, renderedRowIndex - 1, 1)) != null) {
                        rowIndex = tmp;
                    }
                    break;

                case keyCodeEnum.PAGE_UP:
                    if ((tmp = findNextVisibleRow(false, renderedRowIndex - 1, this.mPageSizeKey)) != null) {
                        rowIndex = tmp;
                    }
                    break;

                case keyCodeEnum.TAB:
                    // note: iterate through data rows only

                    if (shiftKeyPressed) {
                        cellIndex--;

                        if (cellIndex < dataRange.topLeft().cellIndex()) {

                            cellIndex = dataRange.bottomRight().cellIndex();

                            if ((tmp = findNextVisibleRow(false, renderedRowIndex - 1, 1, true)) != null) {
                                rowIndex = tmp;
                            } else { // reached the top?
                                if (self.options.keyActionTab === "moveAcross") {
                                    if ((tmp = findNextVisibleRow(false, rbc.capacity() - 1, 1, true)) != null) {
                                        rowIndex = tmp;
                                    }
                                }
                                else {
                                    rowIndex = -1;//invalid rowIndex ,then return null for bubbling ( Roadmap 2015 V2 )
                                }

                            }
                        }
                    }
                    else {
                        cellIndex++;

                        if (cellIndex > dataRange.bottomRight().cellIndex()) {
                            cellIndex = dataRange.topLeft().cellIndex();

                            if ((tmp = findNextVisibleRow(true, renderedRowIndex + 1, 1, true)) != null) {
                                rowIndex = tmp;
                            } else { // reached the bottom?
                                if (self.options.keyActionTab === "moveAcross") {
                                    if ((tmp = findNextVisibleRow(true, 0, 1, true)) != null) {
                                        rowIndex = tmp;
                                    }
                                }
                                else {
                                    rowIndex = -1; //invalid rowIndex ,then return null for bubbling 
                                }

                            }
                        }
                    }

                    break;

                case keyCodeEnum.END:
                    cellIndex = dataRange.bottomRight().cellIndex();
                    break;

                case keyCodeEnum.HOME:
                    cellIndex = dataRange.topLeft().cellIndex();

                    break;

                case keyCodeEnum.LEFT:
                    if (cellIndex > dataRange.topLeft().cellIndex()) {
                        cellIndex--;
                    }

                    break;

                case keyCodeEnum.RIGHT:
                    if (cellIndex < dataRange.bottomRight().cellIndex()) {
                        cellIndex++;
                    }

                    break;
            }

            if (rowIndex >= 0) {//if rowIndex==-1 ,return null for bubbling 
                nextCell = new wijmo.grid.cellInfo(cellIndex, rowIndex, this),
                    column = nextCell.column();

                if (column && column.rowMerge && (column.rowMerge !== "none")) {
                    var lookDown = (keyCode == keyCodeEnum.ENTER || keyCode == keyCodeEnum.DOWN || keyCode == keyCodeEnum.PAGE_DOWN),
                        test = nextCell._clone(),
                        cell: HTMLTableCellElement = null;

                    while (!(cell = test.tableCell()) && test._isRendered()) { // moves up or down until it finds a first table cell element.
                        rowIndex = (lookDown) ? rowIndex + 1 : rowIndex - 1;
                        test = new wijmo.grid.cellInfo(cellIndex, rowIndex, this);
                    }

                    if (cell) {
                        nextCell = test;
                    }
                }

            }

            return nextCell;
        }

        // test whether element is a part of this wijgrid's instance or not
        _testForThis(element: HTMLElement): boolean {
            var marker = wijgrid.CSS.outerDivMarker;

            while (element) {//Check the function "indexOf" first, because SVGElement doesn't have it(#113149).
                if (element.className && element.className.indexOf && element.className.indexOf(marker) >= 0) { // find first outerDiv. 
                    return this.mOuterDiv[0] === element;
                }

                element = <HTMLElement>element.parentNode;
            }

            return false;
        }

        _findUntilOuterDiv(start: Element, tagsToFind): Node {
            var current: Node = start,
                outerDiv: Node = this.mOuterDiv[0],
                stopper,
                nodeName,
                item = null;

            for (; current; current = current.parentNode) {
                nodeName = current.nodeName && current.nodeName.toLowerCase();

                if (nodeName) {
                    if (current === outerDiv) {
                        stopper = current;
                        break;
                    }

                    if (tagsToFind[nodeName]) {
                        item = current;
                    }
                }
            }

            return stopper
                ? item
                : null;
        }

        _freezingAllowed(): boolean {
            return this._lgGetScrollMode() !== "none" && !this._hasSpannedCells();
        }

        _getStaticIndex(bRow: boolean): number {
            if (!this._freezingAllowed()) {
                return -1; // can't use static columns\ rows
            }

            var result: number,
                dataRange = this._getDataCellsRange(dataRowsRangeMode.rendered);

            if (bRow) {
                result = Math.min(this._lgGetStaticRowIndex(), dataRange.bottomRight().rowIndex());
            } else {
                result = Math.min(this._lgGetStaticColumnIndex(), dataRange.bottomRight().cellIndex());
            }

            if (result < -1) {
                result = -1;
            }

            return result;
        }

        _getStaticOffsetIndex(isColumn: boolean): number {
            var index = 0;

            if (isColumn) {
                index += this._virtualLeaves().length; // row headers are always fixed
            } else {
                index = this._columnsHeadersTable().length; //the whole header is fixed in case of staticRowIndex >= 0.

                if (this.options.showFilter) {
                    index++; // filter row is placed inside the header, so it is fixed too.
                }
            }

            return index;
        }

        // index of the fixed leaf inside the visibleLeaves collection. rowHeader is always fixed.
        _getRealStaticColumnIndex(): number {
            var offsetStaticIndex = this._getStaticOffsetIndex(true),
                staticColumnIndex = this._getStaticIndex(false),
                realIndex = staticColumnIndex + offsetStaticIndex;

            if (staticColumnIndex >= 0) {
                var leaves = this._visibleLeaves(),
                    allColumns = this._allColumns(),
                    len = leaves.length;

                if (offsetStaticIndex > 0) {
                    allColumns = allColumns.slice(offsetStaticIndex - 1, -1); // remove rowHeader
                }

                if (realIndex < len - 1) {
                    var parent = wijmo.grid.getTompostParent(leaves[realIndex], allColumns);

                    // If child column of some band is fixed then the top and right-most column of the root band contained current column will be fixed.
                    if (parent) {
                        for (realIndex++; realIndex < len; realIndex++) {
                            var nextParent = wijmo.grid.getTompostParent(leaves[realIndex], allColumns);
                            if (nextParent !== parent) {
                                realIndex--;
                                break;
                            }
                        }
                    }
                }

                if (realIndex >= len) {
                    realIndex = len - 1;
                }
            }

            return realIndex;
        }

        // header is always fixed
        _getRealStaticRowIndex(): number {
            var offsetStaticIndex = this._getStaticOffsetIndex(false);

            return this._getStaticIndex(true) + offsetStaticIndex;
        }

        _hasMerging(): boolean {
            var leaves = this._leaves(),
                result = false;

            for (var i = 0, len = leaves.length; (i < len) && !result; i++) {
                var leafOpt = <IColumn>leaves[i].options;
                result = result || (leafOpt._parentVis && (leafOpt.rowMerge && (leafOpt.rowMerge !== "none"))); // merged visible column?
            }

            return result;
        }

        _hasBands(): boolean {
            var columns = this._allColumns();

            for (var i = 0; i < columns.length; i++) {
                if (columns[i] instanceof wijmo.grid.c1bandfield) {
                    return true;
                }
            }

            return false;
        }

        _hasDetail(): IDetailSettings {
            var d = this.options.detail;
            return d && d.relation && d.relation.length ? d : null;
        }

        _hasGrouping(): boolean {
            var leaves = this._leaves(),
                result = false;

            for (var i = 0, len = leaves.length; (i < len) && !result; i++) {
                var leafOpt = <IColumn>leaves[i].options;
                result = leafOpt.groupInfo && (leafOpt.groupInfo.position !== "none"); // grouped column?
            }

            return result;
        }

        _hasSpannedCells(): boolean {
            return this._hasGrouping() || this._hasMerging() || (this._hasDetail() !== null);
        }

        private mColumnsHeadersTable: IColumnHeaderInfo[][];
        _columnsHeadersTable(value?: any): IColumnHeaderInfo[][] {
            if (arguments.length) {
                this.mColumnsHeadersTable = value;;
            }

            return this.mColumnsHeadersTable;
        }

        _view(): wijmo.grid.baseView {
            return this.mView;
        }

        _originalFooterRowData(): any[] {
            var footer = this.mTFoot;

            return (footer && footer.length)
                ? footer[0] // first row only
                : null;
        }

        _originalHeaderRowData(): any[] {
            var header = this.mTHead;

            return (header && header.length)
                ? header[header.length - 1] // last row only
                : null;
        }

        // set one or more attribute and store original values in the this._originalAttr object if $element == this.element.
        // (key, value), (map)
        _setAttr($element, key?: any, value?: any) {
            var self = this;

            if ($element === this.element) { // store original values
                if (arguments.length === 2) { // map
                    $.each(key, function (k, v) {
                        if (!(k in self._originalAttr)) {
                            self._originalAttr[k] = $element.attr(k);
                        }
                    });

                    return $element.attr(key);
                } else { // key, value
                    if (!(key in this._originalAttr)) {
                        this._originalAttr[key] = $element.attr(key);
                    }

                    return $element.attr(key, value);
                }
            } else {
                return (arguments.length === 3)
                    ? $element.attr(key, value)
                    : $element.attr(key); // .attr(map)
            }

            return this;
        }

        // used by virtual scrolling
        _totalRowsCount(): number {
			/*if (this._dataStore.isDynamic()) {
			return this._dataStore.totalCount();
			}*/

            return this.mSketchTable.count();
        }

        _renderableRowsCount(): number {
            return this._renderableRowsRange().capacity();
        }

        _trackScrollingPosition(x: number, y: number, scrollLeft: number) {
            this.mScrollingState.x = x;
            this.mScrollingState.y = y;

            if (scrollLeft != null) {
                this.mScrollingState.scrollLeft = scrollLeft;
            }
        }

        _trackScrollingIndex(index) {
            this.mScrollingState.index = index;
        }

        _uid(): string {
            if (this.mUID === undefined) {
                this.mUID = wijmo.grid.getUID();
            }

            return "wijgrid" + this.mUID;
        }

        _updateSelection(e: JQueryEventObject, extendMode: wijmo.grid.cellRangeExtendMode): void {
            var eventType = (e && (e.type || "").toLowerCase()) || "",
                selection = this.selection(),
                currentCell = this.currentCell();

            extendMode = extendMode || wijmo.grid.cellRangeExtendMode.none;

            selection.beginUpdate();

            if (eventType) {
                if (currentCell._isValid()) {

                    if (!selection._anchorCell()) {
                        selection._startNewTransaction(currentCell); // attach selection to the current cell
                    }

                    switch (eventType) {
                        case "focusin":
                        case "click":
                            if (!e.shiftKey || !selection._multipleEntitiesAllowed()) {
                                selection._startNewTransaction(currentCell);
                            }

                            if (e.shiftKey && e.ctrlKey) {
                                selection._clearRange(new wijmo.grid.cellInfoRange(currentCell, currentCell), extendMode);
                            } else {
                                selection._selectRange(new wijmo.grid.cellInfoRange(selection._anchorCell(), currentCell), e.ctrlKey, e.shiftKey, extendMode, null);
                            }

                            break;

                        case "keydown":
                            if (!e.shiftKey || !selection._multipleEntitiesAllowed()) {
                                selection._startNewTransaction(currentCell);
                            }

                            selection._selectRange(new wijmo.grid.cellInfoRange(selection._anchorCell(), currentCell), false, e.shiftKey, wijmo.grid.cellRangeExtendMode.none, null);

                            break;
                    }
                }
            } else {
                // * move selection to the current position *
                selection.clear();

                if (currentCell._isValid()) {
                    selection._startNewTransaction(currentCell); // attach selection to the current cell
                    selection._selectRange(new wijmo.grid.cellInfoRange(currentCell, currentCell), false, false, wijmo.grid.cellRangeExtendMode.none, null);
                }
            }

            selection.endUpdate(e);
        }

        _viewPortBounds(value?: IRenderBounds): IRenderBounds {
            if (arguments.length) {
                this.mViewPortBounds = value;
            }

            return this.mViewPortBounds;
        }

        //_viewPortBoundsOfEntirelyShownRows() {
        //	var bounds = this._viewPortBounds(),
        //		isLast = false,
        //		count = bounds.end - bounds.start + 1;

        //	if (this._allowVVirtualScrolling()) {
        //		var view = <fixedView> this._view();
        //		if (count < view.getVirtualPageSize()) {
        //			isLast = true;
        //		}
        //	}

        //	if (!isLast) {
        //		bounds = <IRenderBounds> $.extend({}, bounds);
        //		bounds.end--;
        //	}

        //	return bounds;
        //}

        //#region legacy
        // to support legacy scrolling properties.
        _lgGetFreezingMode(): string {
            return this.options.freezingMode || this.options.scrollingSettings.freezingMode;
        }

        _lgGetRowHeight(): number {
            if (this.options.rowHeight !== undefined) {
                return this.options.rowHeight;
            }

            return this.options.scrollingSettings.virtualizationSettings.rowHeight;
        }

        _lgGetStaticColumnsAlignment(): string {
            return this.options.staticColumnsAlignment || this.options.scrollingSettings.staticColumnsAlignment;
        }

        _lgGetScrollMode(): string {
            return this.options.scrollMode || this.options.scrollingSettings.mode;
        }

        _lgGetStaticColumnIndex(): number {
            if (this.options.staticColumnIndex !== undefined) {
                return this.options.staticColumnIndex;
            }

            return this.options.scrollingSettings.staticColumnIndex;
        }

        _lgGetStaticRowIndex(): number {
            if (this.options.staticRowIndex !== undefined) {
                return this.options.staticRowIndex;
            }

            return this.options.scrollingSettings.staticRowIndex;
        }

        //#endregion legacy

        _hideAllFilterDropDownLists(): void {
            $.each(this._visibleLeaves(), (idx, leaf) => {
                if (leaf instanceof c1field) {
                    (<c1field>leaf)._hideDropDownList();
                }
            });
        }
    }

    wijgrid.prototype.widgetEventPrefix = "wijgrid";
    wijgrid.prototype.mDataPrefix = "wijgrid";
    wijgrid.prototype.mCustomSortOrder = 1000;
    wijgrid.prototype.mPageSizeKey = 10;

    class wijgrid_options implements IWijgridOptions {
        /** @ignore */
        _htmlTrimMethod = trimMethod.all;

        /** @ignore */
        wijMobileCSS = {
            header: "ui-header ui-bar-a",
            content: "ui-body-b",
            stateHover: "ui-btn-down-c",
            stateActive: "ui-btn-down-c",
            stateHighlight: "ui-btn-down-e"
        };

        /** @ignore */
        wijCSS: wijgridWijmoCSS = {
            wijgrid: "",
            wijgridTable: "",
            wijgridTH: "",
            wijgridTD: "",
            wijgridCellContainer: "",
            wijgridRowHeader: "",
            wijgridCurrentRowHeaderCell: "",
            wijgridCurrentHeaderCell: "",
            wijgridCurrentCell: "",
            wijgridCellAlignLeft: "",
            wijgridCellAlignRight: "",
            wijgridCellAlignCenter: "",
            wijgridFilterList: "",
            wijgridFilter: "",
            wijgridFilterInput: "",
            wijgridFilterTrigger: "",
            wijgridFilterNativeHtmlEditorWrapper: "",
            wijgridHeaderArea: "",
            wijgridFooterArea: "",

            wijgridHeaderRow: "",
            wijgridRow: "",
            wijgridDataRow: "",
            wijgridAltRow: "",
            wijgridEmptyDataRow: "",
            wijgridFilterRow: "",
            wijgridGroupHeaderRow: "",
            wijgridGroupFooterRow: "",
            wijgridGroupHeaderRowCollapsed: "",
            wijgridGroupHeaderRowExpanded: "",
            wijgridFooterRow: "",
            wijgridDetailRow: "",

            wijgridLoadingOverlay: "",
            wijgridLoadingText: "",
            wijgridGroupArea: "",
            wijgridGroupAreaButton: "",
            wijgridGroupAreaButtonSort: "",
            wijgridGroupAreaButtonClose: "",
            wijgridGroupToggleVisibilityButton: "",
            wijgridFixedView: "",
            wijgridScroller: "",
            wijgridDndHelper: "",
            wijgridDndArrowTopContainer: "",
            wijgridDndArrowBottomContainer: "",
            wijgridFreezingHandleV: "",
            wijgridFreezingHandleH: "",
            wijgridResizingHandle: "",
            wijgridHeaderCellSortIcon: "",
            wijgridHeaderCellText: ""
        };

		/** A value indicating whether columns can be moved.
		  * @example
		  * // Columns cannot be dragged and moved if this option is set to false
		  * $("#element").wijgrid({ allowColMoving: false });
		  * @remarks
		  * This option must be set to true in order to drag column headers to the group area.
		  */
        allowColMoving: boolean = false;

		/** Determines whether the column width can be increased and decreased by dragging the sizing handle, or the edge of the column header, with the mouse.
		  * @example
		  * // The sizing handle cannot be dragged and column width cannot be changed if this option is set to false
		  * $("#element").wijgrid({ allowColSizing: false });
		  */
        allowColSizing: boolean = false;

		/** Determines whether the user can make changes to cell contents in the grid.
		  * This option is obsolete. Use the editingMode option instead.
		  * @example
		  * // Users cannot change cell contents in the grid if this option is set to false
		  * $("#element").wijgrid({ allowEditing: false });
		  */
        allowEditing: boolean = false;

		/** Determines whether the user can move the current cell using the arrow keys.
		  * @example
		  * // Users cannot move the selection using arrow keys if this option is set to false
		  * $("#element").wijgrid({ allowKeyboardNavigation: false });
		  */
        allowKeyboardNavigation: boolean = true;

		/** Determines the action to be performed when the user presses the TAB key. 
		  * @example
		  * $("#element").wijgrid({ keyActionTab: "moveAcross" });
		  * @remarks
		  * This option is invalid when the allowKeyboardNavigation is set to false.
		  * Possible values are:
		  * "moveAcross": The focus will be kept inside the grid and current selected cell will move cyclically between grid cells when user press TAB or SHIFT+TAB key.
		  * "moveAcrossOut": The focus will be able to be moved from the grid to the next focusable element in the tab order when user press TAB key and the current selected cell is the last cell (or press SHIFT+TAB and the current selected cell is the first cell).    
		  */
        keyActionTab: string = "moveAcross";

		/** Determines whether the grid should display paging buttons. The number of rows on a page is determined by the pageSize option.
		  * @example
		  * // Grid displays paging buttons when allowPaging is true. The pageSize here sets 5 rows to a page.
		  * $("#element").wijgrid({ allowPaging: false, pageSize: 5 });
		  */
        allowPaging: boolean = false;

		/** Determines whether the widget can be sorted by clicking the column header.
		  * @example
		  * // Sort a column by clicking its header when allowSorting is set to true
		  * $("#element").wijgrid({ allowSorting: false });
		  */
        allowSorting: boolean = false;

		/** A value that indicates whether virtual scrolling is allowed. Set allowVirtualScrolling to true when using large amounts of data to improve efficiency.
		  * Obsoleted, set the scrollingSettings.virtualization.mode property to "rows" instead.
		  * @example
		  * $("#element").wijgrid({ allowVirtualScrolling: false });
		  * @remarks
		  * This option is ignored if the grid uses paging, columns merging or fixed rows. This option cannot be enabled when using dynamic wijdatasource.
		  */
        allowVirtualScrolling: boolean = false;

		/** A value that indicates calendar's options in grid. It works for calendar in inputdate.
		 * @remarks Its value is wijcalendar's option, visit 
		 * http://wijmo.com/docs/wijmo/#Wijmo~jQuery.fn.-~wijcalendar.html for more details. 
		 * @type {object}
		 * @example
		 *      $("#eventscalendar").wijgrid(
		 *      { calendar: { prevTooltip: "Previous", nextTooltip: "Next" } });
		 */
        calendar: any = null;

		/** This function is called each time wijgrid needs to change cell appearence, for example, when the current cell position is changed or cell is selected.
		  * Can be used for customization of cell style depending on its state.
		  * @example
		  * // Make the text of the current cell italic.
		  * $("#element").wijgrid({
		  *		highlightCurrentCell: true,
		  *		cellStyleFormatter: function(args) {
		  *			if ((args.row.type & wijmo.grid.rowType.data)) {
		  *				if (args.state & wijmo.grid.renderState.current) {
		  *					args.$cell.css("font-style", "italic");
		  *				} else {
		  *					args.$cell.css("font-style", "normal");
		  *				}
		  *			}
		  *		}
		  * });
		  * @param {wijmo.grid.ICellStyleFormaterArgs} args The data with this function.
		  * @remarks
		  * The args.state parameters equal to wijmo.grid.renderState.rendering means that the cell is being created,
		  * at this moment you can apply general formatting to it indepentant of any particular state, like "current" or "selected".
		  */
        cellStyleFormatter: (args: ICellStyleFormaterArgs) => void = undefined;

		/** An array of column options.
		  * @example
		  * $("#element").wijgrid({ columns: [ { headerText: "column0", allowSort: false }, { headerText: "column1", dataType: "number" } ] });
		  */
        columns: IColumn[] = [];

		/** Determines behavior for column autogeneration. Possible values are: "none", "append", "merge".
		* @example
		* $("#element").wijgrid({ columnsAutogenerationMode: "merge" });
		* @remarks
		* Possible values are:
		* "none": Column auto-generation is turned off.
		* "append": A column will be generated for each data field and added to the end of the columns collection.
		* "merge": Each column having dataKey option not specified will be automatically bound to the first unreserved data field.For each data field not bound to any column a new column will be generated and added to the end of the columns collection.
		*
		* To prevent automatic binding of a column to a data field set its dataKey option to null.
		*
		* Note: columns autogeneration process affects the options of columns and the columns option itself.
		*/
        columnsAutogenerationMode: string = "merge";

		/** Determines the culture ID.
		  * @example
		  * // This code sets the culture to English.
		  * $("#element").wijgrid({ culture: "en" });
		  * @remarks
		  * Please see the https://github.com/jquery/globalize for more information.
		  */
        culture: string = "";

		/** A value that indicators the culture calendar to format the text.
		* This option must work with culture option.
		* @example
		* // This code sets the culture and calendar to Japanese.
		* $("#element").wijgrid({ culture: "ja-jp", cultureCalendar: "Japanese" });
		*/
        cultureCalendar: string = "";

		/** An array of custom user filters. Use this option if you want to extend the default set of filter operators with your own. Custom filters will be shown in the filter dropdown.
		* @example
		* var oddFilterOp = {
		*	name: "customOperator-Odd",
		*	arity: 1,
		*	applicableTo: ["number"],
		*	operator: function(dataVal) { return (dataVal % 2 !== 0); }
		* }
		*
		* $("#element").wijgrid({ customFilterOperators: [oddFilterOp] });
		*/
        customFilterOperators: IFilterOperator[] = [];

		/** Determines the datasource.
		  * Possible datasources include:
		  *		1. A DOM table. This is the default datasource, used if the data option is null. Table must have no cells with rowSpan and colSpan attributes.
		  *		2. A two-dimensional array, such as [[0, "a"], [1, "b"]].
		  *		3. An array of objects, such as [{field0: 0, field1: "a"}, {field0: 1, field1: "b'}].
		  *		4. A wijdatasource.   
		  *		5. A wijdataview.
		  * @example
		  * // DOM table
		  * $("#element").wijgrid();
		  * // two-dimensional array
		  * $("#element").wijgrid({ data: [[0, "a"], [1, "b"]] });
		  */
        data: any = null;

		/**
		  * Determines the detail grid settings in a hierarchy grid, which is used as a template for populating detail grids in a hierarchical grid.
		  * @remarks
		  * Limitations:
		  * The following datasources are supported (can be used as a detail.data option): arrays, ArrayDataView, AjaxDataView, ODataView, BreezeDataView.
		  * It is not possible to use columns grouping and master-detail hierarchy simultaneously.
		  * It is not possible to use virtual scrolling and master-detail hierarchy simultaneously.
		  * In a detail grid the filtering ability is disabled for columns which are bounded to detailDataKey fields.
		  * @example
		  * $("#element").wijgrid({
		  *    data: customers,
		  *    detail: {
		  *       data: orders,
		  *       relation: [
		  *          { masterDataKey: "ID", detailDataKey: "CUST_ID" }
		  *       ]
		  *    }
		  * });
		  */
        detail: IDetailSettings = undefined;

		/** Determines an action to bring a cell in the editing mode when the editingMode option is set to "cell". Possible values are: "click", "doubleClick", "auto".
		  * @example
		  * $("#element").wijgrid({ editingInitOption: "auto" });
		  * @remarks
		  * Possible values are:
		  *	"click": cell is edited via a single click.
		  *	"doubleClick": cell is edited via a double click.
		  *	"auto": action is determined automatically depending upon user environment. If user has a mobile platform then "click" is used, "doubleClick" otherwise.
		  */
        editingInitOption: string = "auto";

		/** Determines the editing mode. Possible values are: "none", "row", "cell",
		  * @example
		  * $("#element").wijgrid({
		  *    editingMode: "row",
		  *    columns: [{
		  *       showEditButton: true
		  *    }] 
		  * });
		  * @remarks
		  * Possible values are:
		  * "none": the editing ability is disabled.
		  *	"cell": a single cell can be edited via a double click.
		  *	"row": a whole row can be edited via a command column.
		  */
        editingMode: string = "none";

		/** Determines if the exact column width, in pixels, is used.
		  * @example
		  * $("#element").wijgrid({ ensureColumnsPxWidth: true });
		  * @remarks
		  * By default, wijgrid emulates the table element behavior when using a number as the width. This means wijgrid may not have the exact width specified. If exact width is needed, please set the ensureColumnsPxWidth option of wijgrid to true. If this option is set to true, wijgrid will not expand itself to fit the available space.Instead, it will use the width option of each column widget.
		  */
        ensureColumnsPxWidth: boolean = false;

		/** Determines the order of items in the filter drop-down list.
		  * Possible values are: "none", "alphabetical", "alphabeticalCustomFirst" and "alphabeticalEmbeddedFirst"
		  * @example
		  * $("#element").wijgrid({ filterOperatorsSortMode: "alphabeticalCustomFirst" });
		  * @remarks
		  * Possible values are:
		  *	"none": Operators follow the order of addition; built-in operators appear before custom ones.
		  *	"alphabetical": Operators are sorted alphabetically.
		  *	"alphabeticalCustomFirst": Operators are sorted alphabetically with custom operators appearing before built-in ones.
		  *	"alphabeticalEmbeddedFirst": Operators are sorted alphabetically with built-in operators appearing before custom operators.
		  *
		  * "NoFilter" operator is always first.
		  */
        filterOperatorsSortMode: string = "alphabeticalCustomFirst";

		/** Determines whether the user can change position of the static column or row by dragging the vertical or horizontal freezing handle with the mouse. Possible values are: "none", "columns", "rows", "both".
		  * Obsoleted, use the scrollingSettings.freezingMode property instead.
		  * @example
		  * $("#element").wijgrid({ freezingMode: "both" });
		  * @remarks
		  * Possible values are:
		  * "none": The freezing handle cannot be dragged.
		  * "columns": The user can drag the vertical freezing handle to change position of the static column.
		  * "rows": The user can drag the horizontal freezing handle to change position of the static row.
		  * "both": The user can drag both horizontal and vertical freezing handles.
		  */
        freezingMode: string = undefined;

		/** Determines the caption of the group area.
		  * @example
		  * // Set the groupAreaCaption to a string and the text appears above the grid
		  * $("#element").wijgrid({ groupAreaCaption: "Drag a column here to group by that column." });
		  */
        groupAreaCaption: string = "Drag a column here to group by that column.";

		/** Determines the indentation of the groups, in pixels.
		  * @example
		  * // Set the groupIndent option to the number of pixels to indent data when grouping.
		  * $("#element").wijgrid({ groupIndent: 15 });
		  */
        groupIndent: number = 10;

		/** Determines whether the position of the current cell is highlighted or not.
		  * @example
		  * $("#element").wijgrid({ highlightCurrentCell: false });
		  */
        highlightCurrentCell: boolean = false;

		/** Determines whether hovered row is highlighted or not.
		  * @example
		  * $("#element").wijgrid({ highlightCurrentCell: true });
		  */
        highlightOnHover: boolean = true;

		/** Determines the text to be displayed when the grid is loading.
		  * @example
		  * $("#element").wijgrid({ loadingText: "Loading..."});
		  */
        loadingText: string = "Loading...";

		/** Cell values equal to this property value are considered null values. Use this option if you want to change default representation of null values (empty strings) with something else.
		  * @example
		  * $("#element").wijgrid({ nullString: "" });
		  * @remarks
		  * Case-sensitive for built-in parsers.
		  */
        nullString: string = "";

		/** Determines the zero-based index of the current page. You can use this to access a specific page, for example, when using the paging feature.
		  * @example
		  * $("#element").wijgrid({ pageIndex: 0 });
		  */
        pageIndex: number = 0;

		/** Number of rows to place on a single page.
		  * The default value is 10.
		  * @example
		  * // The pageSize here sets 10 rows to a page. The allowPaging option is set to true so paging buttons appear.
		  * $("#element").wijgrid({ pageSize: 10 });
		  */
        pageSize: number = 10;

		/** Determines the pager settings for the grid including the mode (page buttons or next/previous buttons), number of page buttons, and position where the buttons appear.
		  * @example
		  * // Display the pager at the top of the wijgrid.
		  * $("#element").wijgrid({ pagerSettings: { position: "top" } });
		  * @remarks
		  * See the wijpager documentation for more information on pager settings.
		  */
        pagerSettings: IPagerSettings = <any>{
            mode: "numeric",
            pageButtonCount: 10,
            position: "bottom"
        };

		/** A value indicating whether DOM cell attributes can be passed within a data value.
		  * @example
		  * // Render the style attribute passed within the data.
		  * $("#element").wijgrid({
		  *		readAttributesFromData: false });
		  *		data: [
		  *			[ [1, { "style": "color: red" } ], a ]
		  *		]
		  * });
		  * @remarks
		  * This option allows binding collection of values to data and automatically converting them as attributes of corresponded DOM table cells during rendering.
		  * Values should be passed as an array of two items, where first item is a value of the data field, the second item is a list of values:
		  * $("#element").wijgrid({
		  *		data: [
		  *			[ [1, { "style": "color: red", "class": "myclass" } ], a ]
		  *		]
		  * });
		  *
		  * or
		  *
		  * $("#element").wijgrid({
		  *		data: [
		  *			{ col0: [1, { "style": "color: red", "class": "myclass" }], col1: "a" }
		  *		]
		  * });
		  *
		  * Note: during conversion wijgrid extracts the first item value and makes it data field value, the second item (list of values) is removed:
		  * [ { col0: 1, col1: "a" } ]
		  *
		  * If DOM table is used as a datasource then attributes belonging to the cells in tBody section of the original table will be read and applied to the new cells.
		  *
		  * rowSpan and colSpan attributes are not allowed.
		  */
        readAttributesFromData: boolean = false;

		/** Determines the height of a rows when virtual scrolling is used.
		  * Obsoleted, use the scrollingSettings.virtualization.rowHeight property instead.
		  * @example
		  * $("#element").wijgrid({ rowHeight: 20 });
		  * @remarks
		  * Can be set only during creation
		  */
        rowHeight: number = undefined;

		/** Function used for styling rows in wijgrid.
		  * @example
		  * // Make text of the alternating rows italic.
		  * $("#demo").wijgrid({
		  *		data: [
		  *			[0, "Nancy"], [1, "Susan"], [2, "Alice"], [3, "Kate"]
		  *		],
		  *		rowStyleFormatter: function (args) {
		  *			if ((args.state & wijmo.grid.renderState.rendering) && (args.type & wijmo.grid.rowType.dataAlt)) {
		  *				args.$rows.find("td").css("font-style", "italic");
		  *			}
		  *		}
		  * });
		  * @param {wijmo.grid.IRowInfo} args The data with this function.
		  */
        rowStyleFormatter: (args: IRowInfo) => void = undefined;

		/** Determines which scrollbars are active and if they appear automatically based on content size.
		  * Possbile values are: "none", "auto", "horizontal", "vertical", "both".
		  * Obsoleted, use the scrollingSettings.mode property instead.
		  * @example
		  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
		  * $("#element").wijgrid({ scrollMode: "both" });
		  * @remarks
		  * Possible values are:
		  *	"none": Scrolling is not used; the staticRowIndex and staticColumnIndex values are ignored.
		  *	"auto": Scrollbars appear automatically depending upon content size.
		  *	"horizontal": The horizontal scrollbar is active.
		  *	"vertical": The vertical scrollbar is active.
		  *	"both": Both horizontal and vertical scrollbars are active.
		  */
        scrollMode: string = undefined;

		/** Determines the scrolling settings.
		  * @example
		  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
		  * $("#element").wijgrid({ scrollingSettings: { mode: "both" } });
		  */
        scrollingSettings: IScrollingSettings = {
            freezingMode: "none",
            mode: "none",
            staticColumnIndex: -1,
            staticRowIndex: -1,
            staticColumnsAlignment: "left",
            virtualizationSettings: <IVirtualizationSettings>{
                mode: "none",
                rowHeight: 19,
                columnWidth: 100
            }
        }

		/** Determines which cells, range of cells, columns, or rows can be selected at one time.
		  * Possible values are: "none", "singleCell", "singleColumn", "singleRow", "singleRange", "multiColumn", "multiRow" and "multiRange".
		  * @example
		  * // Set selectionMode to muliColumn and users can select more than one column using the CTRL or SHIFT keys.
		  * $("#element").wijgrid({ selectionMode: "multiColumn" });
		  * @remarks
		  * Possible values are: 
		  * "none": Selection is turned off.
		  * "singleCell": Only a single cell can be selected at a time.
		  * "singleColumn": Only a single column can be selected at a time.
		  * "singleRow": Only a single row can be selected at a time.
		  * "singleRange": Only a single range of cells can be selected at a time.
		  * "multiColumn": It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
		  * "multiRow": It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
		  * "multiRange": It is possible to select more than one cells range at the same time using the mouse and the CTRL or SHIFT keys.
		  */
        selectionMode: string = "singleRow";

		/** A value indicating whether the filter row is visible.
		  * Filter row is used to display column filtering interface.
		  * @example
		  * // Set showFilter to true to view the filter row.
		  * $("#element").wijgrid({ showFilter: true });
		  */
        showFilter: boolean = false;

		/** A value indicating whether the footer row is visible.
		  * Footer row is used for displaying of tfoot section of original table, and to show totals.
		  * @example
		  * // Set showFooter to true to view the footer row.
		  * $("#element").wijgrid({ showFooter: true });
		  */
        showFooter: boolean = false;

		/** A value indicating whether group area is visible.
		  * Group area is used to display headers of groupped columns. User can drag columns from/to group area by dragging column headers with mouse, if allowColMoving option is on.
		  * @example
		  * // Set showGroupArea to true to display the group area.
		  * $("#element").wijgrid({ showGroupArea: true });
		  */
        showGroupArea: boolean = false;

		/** A value indicating whether a selection will be automatically displayed at the current cell position when the wijgrid is rendered.
		  * Set this option to false if you want to prevent wijgrid from selecting the currentCell automatically.
		  * @example
		  * $("#element").wijgrid({ showSelectionOnRender: true });
		  */
        showSelectionOnRender: boolean = true;

		/** A value indicating whether the row header is visible.
		  * @example
		  * $("#element").wijgrid({ showRowHeader: true });
		  */
        showRowHeader: boolean = false;

		/** Indicates the index of columns that will always be shown on the left when the grid view is scrolled horizontally.
		  * Obsoleted, use the scrollingSettings.staticColumnIndex property instead.
		  * @example
		  * $("#element").wijgrid({ staticColumnIndex: -1 });
		  * @remarks
		  * Note that all columns before the static column will be automatically marked as static, too.
		  * This can only take effect when the scrollMode option is not set to "none".
		  * It will be considered "-1" when grouping or row merging is enabled. A "-1" means there is no data column but the row header is static. A zero (0) means one data column and row header are static.
		  */
        staticColumnIndex: number = undefined;

		/** Gets or sets the alignment of the static columns area. Possible values are "left", "right".
		  * Obsoleted, use the scrollingSettings.staticColumnsAlignment property instead.
		  * @example
		  * $("#element").wijgrid({ staticColumnsAlignment: "left" });
		  * @remarks
		  * The "right" mode has limited functionality:
		  *  - The showRowHeader value is ignored.
		  *  - Changing staticColumnIndex at run-time by dragging the vertical bar is disabled.
		  */
        staticColumnsAlignment: string = undefined;

		/** Indicates the index of data rows that will always be shown on the top when the wijgrid is scrolled vertically.
		  * Obsoleted, use the scrollingSettings.staticRowIndext property instead.
		  * @example
		  * $("#element").wijgrid({ staticRowIndex: -1 });
		  * @remarks
		  * Note, that all rows before the static row will be automatically marked as static, too.
		  * This can only take effect when the scrollMode option is not set to "none". This will be considered "-1" when grouping or row merging is enabled.
		  * A "-1" means there is no data row but the header row is static.A zero (0) means one data row and the row header are static.
		  */
        staticRowIndex: number = undefined;

		/** Gets or sets the virtual number of items in the wijgrid and enables custom paging.
		  * Setting option to a positive value activates custom paging, the number of displayed rows and the total number of pages will be determined by the totalRows and pageSize values.
		  * @example
		  * $("#element").wijgrid({ totalRows: -1 });
		  * @remarks
		  * In custom paging mode sorting, paging and filtering are not performed automatically.
		  * This must be handled manually using the sorted, pageIndexChanged, and filtered events. Load the new portion of data there followed by the ensureControl(true) method call.
		  */
        totalRows: number = -1;

        /* --- events */

		/** The afterCellEdit event handler is a function called after cell editing is completed.
		  * This function can assist you in completing many tasks, such as in making changes once editing is completed; in tracking changes in cells, columns, or rows; or in integrating custom editing functions on the front end.
		  * @event
		  * @example
		  * // Once cell editing is complete, the function calls the destroy method to destroy the wijcombobox widget and the wijinputnumber widget which are used as the custom editors.
		  * $("#element").wijgrid({
		  *		afterCellEdit: function(e, args) {
		  *			switch (args.cell.column().dataKey) {
		  *				case "Position":
		  *					args.cell.container().find("input").wijcombobox("destroy");
		  *					break;
		  *				case "Acquired":
		  *					args.cell.container().find("input").wijinputnumber("destroy");
		  *					break;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ afterCellEdit: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridaftercelledit", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IAfterCellEditEventArgs} args The data with this event.
		  */
        afterCellEdit: (e: JQueryEventObject, args: IAfterCellEditEventArgs) => void = null;

		/** The afterCellUpdate event handler is a function that is called after a cell has been updated. Among other functions, this event allows you to track and store the indices of changed rows or columns.
		  * @event
		  * @example
		  * // Once the cell has been updated, the information from the underlying data is dumped into the "#log" element.
		  * $("#element").wijgrid({
		  *		afterCellUpdate: function(e, args) {
		  *			$("#log").html(dump($("#demo").wijgrid("data")));
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ afterCellUpdate: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridaftercellupdate", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IAfterCellUpdateEventArgs} args The data with this event.
		  */
        afterCellUpdate: (e: JQueryEventObject, args: IAfterCellUpdateEventArgs) => void = null;

		/** The beforeCellEdit event handler is a function that is called before a cell enters edit mode.
		  * The beforeCellEdit event handler assists you in appending a widget, data, or other item to a wijgrid's cells before the cells enter edit mode. This event is cancellable if the editigMode options is set to "cell".
		  * @event
		  * @example
		  * // Allow the user to change the price only if the product hasn't been discontinued:
		  * $("#element").wijgrid({
		  *		beforeCellEdit: function(e, args) {
		  *			return !((args.cell.column().dataKey === "Price") && args.cell.row().data.Discontinued);
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ beforeCellEdit: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridbeforecelledit", function (e, args) {
		  *		// some code here
		  * });
		  * 
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IBeforeCellEditEventArgs} args The data with this event.
		  */
        beforeCellEdit: (e: JQueryEventObject, args: IBeforeCellEditEventArgs) => boolean = null;

		/** The beforeCellUpdate event handler is a function that is called before the cell is updated with new or user-entered data. This event is cancellable if the editingMode options is set to "cell".
		  * There are many instances where this event is helpful, such as when you need to check a cell's value before the update occurs or when you need to apply an alert message based on the cell's value.
		  * @event
		  * @example
		  * // In this sample, you use args.value to check the year that the user enters in the "Acquired" column.
		  * // If it's less than 1990 or greater than the current year, then the event handler will return false to cancel updating and show the user an alert message.
		  * $("#element").wijgrid({
		  *		beforeCellUpdate: function(e, args) {
		  *			switch (args.cell.column().dataKey) { 
		  *				case "Acquired":
		  *					var $editor = args.cell.container().find("input"),
		  *						value = $editor.wijinputnumber("getValue"),
		  *						curYear = new Date().getFullYear();
		  *
		  *					if (value < 1990 || value > curYear) {
		  *						$editor.addClass("ui-state-error");
		  *						alert("value must be between 1990 and " + curYear);
		  *						$editor.focus();
		  *						return false; 
		  *					}
		  *
		  *					args.value = value; 
		  *					break;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ beforeCellUpdate: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridbeforecellupdate", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IBeforeCellUpdateEventArgs} args The data with this event.
		  */
        beforeCellUpdate: (e: JQueryEventObject, args: IBeforeCellUpdateEventArgs) => boolean = null;

		/** The cellClicked event handler is a function that is called when a cell is clicked. You can use this event to get the information of a clicked cell using the args parameter.
		  * @event
		  * @example
		  * // The sample uses the cellClicked event to trigger an alert when the cell is clicked.
		  * $("#element").wijgrid({
		  *		cellClicked: function (e, args) {
		  *			alert(args.cell.value());
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ cellClicked: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcellclicked", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ICellClickedEventArgs} args The data with this event.
		  */
        cellClicked: (e: JQueryEventObject, args: ICellClickedEventArgs) => void = null;

		/** The columnDragging event handler is a function that is called when column dragging has been started, but before the wijgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Preventing a user from dragging a specific column
		  * $("#element").wijgrid({
		  *		columnDragging: function (e, args) {
		  *			return !(args.drag.dataKey == "ID");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnDragging: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumndragging", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnDraggingEventArgs} args The data with this event.
		  */
        columnDragging: (e: JQueryEventObject, args: IColumnDraggingEventArgs) => boolean = null;

		/** The columnDragged event handler is a function that is called when column dragging has been started. You can use this event to find the column being dragged or the dragged column's location.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnDragged event:
		  * $("#element").wijgrid({
		  *		columnDragged: function (e, args) {
		  *			alert("The '" + args.drag.headerText + "' column is being dragged from the '" + args.dragSource + "' location");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnDragged: function (e, args) {
		  *		// some code here
		  * }});
		  * 
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumndragged", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnDraggedEventArgs} args The data with this event.
		  */
        columnDragged: (e: JQueryEventObject, args: IColumnDraggedEventArgs) => void = null;

		/** The columnDropping event handler is a function that is called when a column is dropped into the columns area, but before wijgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Preventing user from dropping any column before the "ID" column.
		  * $("#element").wijgrid({
		  *		columnDropping: function (e, args) {
		  *			return !(args.drop.dataKey == "ID" && args.at == "left");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnDropping: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumndropping", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnDroppingEventArgs} args The data with this event.
		  */
        columnDropping: (e: JQueryEventObject, args: IColumnDroppingEventArgs) => boolean = null;

		/** The columnDropped event handler is a function that is called when a column has been dropped into the columns area.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnDropped event:
		  * $("#element").wijgrid({
		  *		columnDropped: function (e, args) {
		  *			"The '" + args.drag.headerText + "' column has been dropped onto the '" + args.drop.headerText + "' column at the '" + args.at + "' position"
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnDropped: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumndropped", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnDroppedEventArgs} args The data with this event.
		  */
        columnDropped: (e: JQueryEventObject, args: IColumnDroppedEventArgs) => void = null;

		/** The columnGrouping event handler is a function that is called when a column is dropped into the group area, but before the wijgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Preventing user from grouping the "UnitPrice" column.
		  * $("#element").wijgrid({
		  *		columnGrouping: function (e, args) {
		  *			return !(args.drag.headerText == "UnitPrice");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnGrouping: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumngrouping", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnGroupingEventArgs} args The data with this event.
		  */
        columnGrouping: (e: JQueryEventObject, args: IColumnGroupingEventArgs) => boolean = null;

		/** The columnGrouped event handler is a function that is called when a column has been dropped into the group area.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnGrouped event:
		  * $("#element").wijgrid({
		  *		columnGrouped: function (e, args) {
		  *			alert("The '" + args.drag.headerText "' column has been grouped");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnGrouped: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumngrouped", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnGroupedEventArgs} args The data with this event.
		  */
        columnGrouped: (e: JQueryEventObject, args: IColumnGroupedEventArgs) => void = null;

		/** The columnResizing event handler is called when a user resizes the column but before the wijgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Prevent setting the width of "ID" column less than 100 pixels
		  * $("#element").wijgrid({
		  *		columnResizing: function (e, args) {
		  *			if (args.column.dataKey == "ID" && args.newWidth < 100) {
		  *				args.newWidth = 100;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnResizing: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumnresizing", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnResizingEventArgs} args The data with this event.
		  */
        columnResizing: (e: JQueryEventObject, args: IColumnResizingEventArgs) => boolean = null;

		/** The columnResized event handler is called when a user has changed a column's size.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnGrouped event:
		  * $("#element").wijgrid({
		  *		columnResized: function (e, args) {
		  *			alert("The '" + args.column.headerText + "' has been resized");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnResized: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumnresized", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnResizedEventArgs} args The data with this event.
		  */
        columnResized: (e: JQueryEventObject, args: IColumnResizedEventArgs) => void = null;

		/** The columnUngrouping event handler is called when a column has been removed from the group area but before the wjgrid handles the operation. This event is cancellable.
		  * @event
		  * @example
		  * // Preventing user from ungrouping the "UnitPrice" column.
		  * $("#element").wijgrid({
		  *		columnUngrouping: function (e, args) {
		  *			return !(args.column.headerText == "UnitPrice");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnUngrouping: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumnungrouping", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnUngroupingEventArgs} args The data with this event.
		  */
        columnUngrouping: (e: JQueryEventObject, args: IColumnUngroupingEventArgs) => boolean = null;

		/** The columnUngrouped event handler is called when a column has been removed from the group area.
		  * @event
		  * @example
		  * // Supply a callback function to handle the columnGrouped event:
		  * $("#element").wijgrid({
		  *		columnUngrouped: function (e, args) {
		  *			alert("The '" + args.column.headerText + "' has been ungrouped");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ columnUngrouped: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcolumnungrouped", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IColumnUngroupedEventArgs} args The data with this event.
		  */
        columnUngrouped: (e: JQueryEventObject, args: IColumnUngroupedEventArgs) => void = null;

		/** The currentCellChanging event handler is called before the cell is changed. You can use this event to get a selected row or column or to get a data row bound to the current cell. This event is cancellable.
		  * @event
		  * @example
		  * // Gets the data row bound to the current cell.
		  * $("#element").wijgrid({
		  *		currentCellChanging: function (e, args) {
		  *			var rowObj = $(e.target).wijgrid("currentCell").row();
		  *			if (rowObj) {		
		  *				var dataItem = rowObj.data; // current data item (before the cell is changed).
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ currentCellChanging: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcurrentcellchanging", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ICurrentCellChangingEventArgs} args The data with this event.
		  */
        currentCellChanging: (e: JQueryEventObject, args: ICurrentCellChangingEventArgs) => boolean = null;

		/** The currentCellChanged event handler is called after the current cell is changed.
		  * @event
		  * @example
		  * // Gets the data row bound to the current cell.
		  * $("#element").wijgrid({
		  *		currentCellChanged: function (e, args) {
		  *			var rowObj = $(e.target).wijgrid("currentCell").row();
		  *			if (rowObj) {		
		  *				var dataItem = rowObj.data; // current data item (after the cell is changed).
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ currentCellChanged: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridcurrentcellchanged", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
        currentCellChanged: (e: JQueryEventObject) => void = null;

		/** The detailCreating event handler is called when wijgrid requires to create a new detail wijgrid.
		* @event
		* @remarks
		* Event receives options of a detail grid to create, which were obtained by cloning the detail option of the master grid.
		* User can alter the detail grid options here and provide a specific datasource within the args.options.data option to implement run-time hierarachy.
		* @remarks
		* You can bind to the event either by type or by name.
		* Bind to the event by name:
		* $("#element").wijgrid({ detailCreating: function (e, args) {
		*		// some code here
		* }});
		*
		* Bind to the event by type:
		* $("#element").bind("wijgriddetailcreating", function (e, args) {
		*		// some code here
		* });
		* @param {Object} e The jQuery.Event object.
		* @param {wijmo.grid.IDetailCreatingEventArgs} args The data with this event.
		*/
        detailCreating: (e: JQueryEventObject, args: IDetailCreatingEventArgs) => void = null;

		/** The filterOperatorsListShowing event handler is a function that is called before the filter drop-down list is shown. You can use this event to customize the list of filter operators for your users.
		  * @event
		  * @example
		  * // Limit the filters that will be shown to the "Equals" filter operator
		  * $("#element").wijgrid({
		  *		filterOperatorsListShowing: function (e, args) {
		  *			args.operators = $.grep(args.operators, function(op) {
		  *				return op.name === "Equals" || op.name === "NoFilter";
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ filterOperatorsListShowing: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridfilteroperatorslistshowing", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IFilterOperatorsListShowingEventArgs} args The data with this event.
		  */
        filterOperatorsListShowing: (e: JQueryEventObject, args: IFilterOperatorsListShowingEventArgs) => void = null;

		/** The filtering event handler is a function that is called before the filtering operation is started. For example, you can use this event to change a filtering condition before a filter will be applied to the data. This event is cancellable.
		  * @event
		  * @example
		  * // Prevents filtering by negative values
		  * $("#element").wijgrid({
		  *		filtering: function (e, args) {
		  *			if (args.column.dataKey == "Price" && args.value < 0) {
		  *				args.value = 0;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ filtering: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridfiltering", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IFilteringEventArgs} args The data with this event.
		  */
        filtering: (e: JQueryEventObject, args: IFilteringEventArgs) => boolean = null;

		/** The filtered event handler is a function that is called after the wijgrid is filtered.
		  * @event
		  * @example
		  * // 
		  * $("#element").wijgrid({
		  *		filtered: function (e, args) {
		  *			alert("The filtered data contains: " + $(this).wijgrid("dataView").count() + " rows");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ filtered: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridfiltered", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IFilteredEventArgs} args The data with this event.
		  */
        filtered: (e: JQueryEventObject, args: IFilteredEventArgs) => void = null;

		/** The groupAggregate event handler is a function that is called when groups are being created and the column object's aggregate option has been set to "custom". This event is useful when you want to calculate custom aggregate values.
		  * @event
		  * @example
		  * // This sample demonstrates using the groupAggregate event handler to calculate an average in a custom aggregate:
		  * $("#element").wijgrid({
		  *		groupAggregate: function (e, args) {
		  *			if (args.column.dataKey == "Price") {
		  *				var aggregate = 0;
		  *
		  *				for (var i = args.groupingStart; i <= args.groupingEnd; i++) {
		  *					aggregate += args.data[i].valueCell(args.column.dataIndex).value;
		  *				}
		  *
		  *				aggregate = aggregate/ (args.groupingEnd - args.groupingStart + 1);
		  *				args.text = aggregate;
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ groupAggregate: function (e, args) {
		  *		// some code here
		  * }});
		  * Bind to the event by type:
		  *
		  * $("#element").bind("wijgridgroupaggregate", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IGroupAggregateEventArgs} args The data with this event.
		  */
        groupAggregate: (e: JQueryEventObject, args: IGroupAggregateEventArgs) => void = null;

		/** The groupText event handler is a function that is called when groups are being created and the groupInfo option has the groupInfo.headerText or the groupInfo.footerText options set to "custom". This event can be used to customize group headers and group footers.
		  * @event
		  * @example
		  * // The following sample sets the groupText event handler to avoid empty cells. The custom formatting applied to group headers left certain cells appearing as if they were empty. This code avoids that:
		  * $("#element").wijgrid({
		  *		groupText: function (e, args) {
		  *			if (!args.groupText) {
		  *				args.text = "null";
		  *			}
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ groupText: function (e, args) {
		  *		// some code here
		  * }});
		  * 
		  * Bind to the event by type:
		  * $("#element").bind("wijgridgrouptext", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IGroupTextEventArgs} args The data with this event.
		  */
        groupText: (e: JQueryEventObject, args: IGroupTextEventArgs) => void = null;

		/** The invalidCellValue event handler is a function called when a cell needs to start updating but the cell value is invalid. So if the value in a wijgrid cell can't be converted to the column target type, the invalidCellValue event will fire.
		  * @event
		  * @example
		  * // Adds a style to the cell if the value entered is invalid
		  * $("#element").wijgrid({
		  *		invalidCellValue: function (e, args) {
		  *			$(args.cell.container()).addClass("ui-state-error");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ invalidCellValue: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridinvalidcellvalue", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IInvalidCellValueEventArgs} args The data with this event.
		  */
        invalidCellValue: (e: JQueryEventObject, args: IInvalidCellValueEventArgs) => void = null;

		/** The pageIndexChanging event handler is a function that is called before the page index is changed. This event is cancellable.
		  * @event
		  * @example
		  * // Cancel the event by returning false
		  * $("#element").wijgrid({
		  *		pageIndexChanging: function (e, args) {
		  *			return false;
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ pageIndexChanging: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridpageindexchanging", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IPageIndexChangingEventArgs} args The data with this event.
		  */
        pageIndexChanging: (e: JQueryEventObject, args: IPageIndexChangingEventArgs) => boolean = null;

		/** The pageIndexChanged event handler is a function that is called after the page index is changed, such as when you use the numeric buttons to swtich between pages or assign a new value to the pageIndex option.
		  * @event
		  * @example
		  * // Supply a callback function to handle the pageIndexChanged event:
		  * $("#element").wijgrid({
		  *		pageIndexChanged: function (e, args) {
		  *			alert("The new pageIndex is: " + args.newPageIndex);
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ pageIndexChanged: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridpageindexchanged", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.IPageIndexChangedEventArgs} args The data with this event.
		  */
        pageIndexChanged: (e: JQueryEventObject, args: IPageIndexChangedEventArgs) => void = null;

		/** The selectionChanged event handler is a function that is called after the selection is changed.
		  * @event
		  * @example
		  * // Get the value of the first cell of the selected row.
		  * $("#element").wijgrid({
		  *		selectionMode: "singleRow",
		  *		selectionChanged: function (e, args) {
		  *			alert(args.addedCells.item(0).value());
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ selectionChanged: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridselectionchanged", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ISelectionChangedEventArgs} args The data with this event.
		  */
        selectionChanged: (e: JQueryEventObject, args: ISelectionChangedEventArgs) => void = null;

		/** The sorting event handler is a function that is called before the sorting operation is started. This event is cancellable.
		  * The allowSorting option must be set to "true" for this event to fire.
		  * @event
		  * @example
		  * // Preventing user from sorting the "ID" column.
		  * $("#element").wijgrid({
		  *		sorting: function (e, args) {
		  *			return !(args.column.headerText === "ID");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ sorting: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridsorting", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ISortingEventArgs} args The data with this event.
		  */
        sorting: (e: JQueryEventObject, args: ISortingEventArgs) => boolean = null;

		/** The sorted event handler is a function that is called after the widget is sorted. The allowSorting option must be set to "true" to allow this event to fire.
		  * @event
		  * @example
		  * // The following code handles the sorted event and will give you access to the column and the sort direction
		  * $("#element").wijgrid({
		  *		sorted: function (e, args) {
		  *			alert("Column " + args.column.headerText + " sorted in " + args.sortDirection + " order");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ sorted: function (e, args) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridsorted", function (e, args) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  * @param {wijmo.grid.ISortedEventArgs} args The data with this event.
		  */
        sorted: (e: JQueryEventObject, args: ISortedEventArgs) => void = null;

        /* events --- */

        /* --- life-cycle events */

        //			/// <summary>
        //			/// The ajaxError event handler. A function called when wijgrid is bound to remote data and
        //			/// the ajax request fails.
        //			/// Default: null.
        //			/// Type: Function.
        //			/// Code example:
        //			/// Supply a callback function to handle the ajaxError event:
        //			/// $("#element").wijgrid({ ajaxError: function (e, args) { } });
        //			/// Bind to the event by type:
        //			/// $("#element").bind("wijgridajaxerror", function (e, args) { });
        //			/// </summary>
        //			/// <param name="e" type="Object">The jQuery.Event object.</param>
        //			/// <param name="args" type="Object">
        //			/// The data corresponded with this event.
        //			/// args.XMLHttpRequest: the XMLHttpRequest object.
        //			/// args.textStatus: a string describing the error type.
        //			/// args.errorThrown: an exception object.
        //			///
        //			/// Refer to the jQuery.ajax.error event documentation for more details on this arguments.
        //			/// </param>
        //			ajaxError: null,

		/** The dataLoading event handler is a function that is called when the wijgrid loads a portion of data from the underlying datasource. This can be used for modification of data sent to server if using dynamic remote wijdatasource.
		  * @event
		  * @example
		  * // This sample allows you to set the session ID when loading a portion of data from the remote wijdatasource:
		  * $("#element").wijgrid({
		  *		data: new wijdatasource({
		  *			proxy: new wijhttpproxy({
		  *				// some code here
		  *			})
		  *		}),
		  *		dataLoading: function (e) {
		  *			var dataSource = $(this).wijgrid("option", "data");
		  *			dataSource.proxy.options.data.sessionID = getSessionID();
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ dataLoading: function (e) {
		  * // some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgriddataloading", function (e) {
		  * // some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
        dataLoading: (e: JQueryEventObject) => void = null;

		/** The dataLoaded event handler is a function that is called when data is loaded.
		  * @event
		  * @example
		  * // Display the number of entries found
		  * $("#element").wijgrid({
		  *		dataLoaded: function (e) {
		  *			alert($(this).wijgrid("dataView").count());
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ dataLoaded: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgriddataloaded", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
        dataLoaded: (e: JQueryEventObject) => void = null;

		/** The loading event handler is a function that is called at the beginning of the wijgrid's lifecycle. You can use this event to activate a custom load progress indicator.
		  * @event
		  * @example
		  * // Creating an indeterminate progressbar during loading
		  * $("#element").wijgrid({
		  *		loading: function (e) {
		  *			$("#progressBar").show().progressbar({ value: false });
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ loading: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridloading", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
        loading: (e: JQueryEventObject) => void = null;

		/** The loaded event handler is a function that is called at the end the wijgrid's lifecycle when wijgrid is filled with data and rendered. You can use this event to manipulate the grid html content or to finish a custom load indication.
		  * @event
		  * @example
		  * // The loaded event in the sample below ensures that whatever is selected on load is cleared
		  * $("#element").wijgrid({
		  *		loaded: function (e) {
		  *			$(e.target).wijgrid("selection").clear(); // clear selection                    
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ loaded: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridloaded", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
        loaded: (e: JQueryEventObject) => void = null;

		/** The rendering event handler is a function that is called when the wijgrid is about to render. Normally you do not need to use this event.
		  * @event
		  * @example
		  * $("#element").wijgrid({
		  *		rendering: function (e) {
		  *			alert("rendering");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ rendering: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridrendering", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
        rendering: (e: JQueryEventObject) => void = null;

		/** The rendered event handler is a function that is called when the wijgrid is rendered. Normally you do not need to use this event.
		  * @event
		  * @example
		  * $("#element").wijgrid({
		  *		rendered: function (e) {
		  *			alert("rendered");
		  *		}
		  * });
		  * @remarks
		  * You can bind to the event either by type or by name.
		  * Bind to the event by name:
		  * $("#element").wijgrid({ rendered: function (e) {
		  *		// some code here
		  * }});
		  *
		  * Bind to the event by type:
		  * $("#element").bind("wijgridrendered", function (e) {
		  *		// some code here
		  * });
		  * @param {Object} e The jQuery.Event object.
		  */
        rendered: (e: JQueryEventObject) => void = null;
    }

    //wijgrid.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options, new wijgrid_options());
    wijgrid.prototype.options = wijmo.grid.extendWidgetOptions(wijmoWidget.prototype.options, new wijgrid_options());

    $.wijmo.registerWidget("wijgrid", wijgrid.prototype);
}

/** @ignore */
interface JQuery {
    wijgrid: JQueryWidgetFunction;
}