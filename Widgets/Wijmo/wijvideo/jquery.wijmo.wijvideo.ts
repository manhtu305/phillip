/// <reference path="../External/declarations/globalize.d.ts"/>
/// <reference path="../Base/jquery.wijmo.widget.ts" />
/// <reference path="../wijtooltip/jquery.wijmo.wijtooltip.ts"/>
/*globals jQuery,window,document*/

/*
 * Depends:
 *     jquery.ui.core.js
 *     jquery.ui.widget.js
 *     jquery.wijmo.wijtooltip.js
 */

module wijmo.video {

	var $ = jQuery,
		localCss: LocalCss = {
			widgetName: "wijvideo",
			//Classes
			wijvideoClass: "wijmo-wijvideo",
			wijvideoWrapperClass: "wijmo-wijvideo-wrapper",
			wijvideoControlsClass: "wijmo-wijvideo-controls",
			wijvideoPlayClass: "wijmo-wijvideo-play",
			wijvideoIndexClass: "wijmo-wijvideo-index",
			wijvideoIndexSliderClass: "wijmo-wijvideo-index-slider",
			wijvideoTimerClass: "wijmo-wijvideo-timer",
			wijvideoVolumeClass: "wijmo-wijvideo-volume",
			wijvideoVolumeContainerClass: "wijmo-wijvideo-volume-container",
			wijvideoVolumeSliderClass: "wijmo-wijvideo-volumeslider",
			wijvideoFullScreenClass: "wijmo-wijvideo-fullscreen",
			wijvideoContainerFullScreenClass: "wijmo-wijvideo-container-fullscreen"
		};

	/** @widget */
	export class wijvideo extends wijmoWidget {
		$video: JQuery;
		$vidParent: JQuery;
		$seekSlider: JQuery;
		$volumeBtn: JQuery;
		$volumeSlider: JQuery;
		$fullScreenBtn: JQuery;
		$disabledDiv: JQuery;
		$replacedDiv: JQuery;
		$wijvideoControl: JQuery;
		_volumnOn: boolean;
		_videoIsControls: boolean;
		_oriVidParentStyle: string;
		_oriDocOverFlow: string;
		_oriWidth: number;
		_oriHeight: number;
		_fullScreen: boolean;
		_seeking: boolean;
		_currentVolumn: number;
		options: WijVideoOptions;

		_createVideoDom(): void {
			var wijCSS: WijVideoCSS = this.options.wijCSS, space = " ",
				videoWidgetClass: string = [localCss.wijvideoClass, wijCSS.wijvideo, wijCSS.content, wijCSS.widget].join(space),
				wrapperClass: string = [localCss.wijvideoWrapperClass, wijCSS.wijvideoWrapper].join(space),
				controlClass: string = [localCss.wijvideoControlsClass, wijCSS.wijvideoControls, wijCSS.header,
					wijCSS.helperClearFix, wijCSS.helperReset].join(space),
				videoPlayClass: string = [localCss.wijvideoPlayClass, wijCSS.wijvideoPlay, wijCSS.stateDefault, wijCSS.cornerAll].join(space),
				iconClass: string = [wijCSS.icon, wijCSS.iconPlay].join(space),
				indexClass: string = [localCss.wijvideoIndexClass, wijCSS.wijvideoIndex].join(space),
				indexSliderClass: string = [localCss.wijvideoIndexSliderClass, wijCSS.wijvideoIndexSlider].join(space),
				timerClass: string = [localCss.wijvideoTimerClass, wijCSS.wijvideoTimer, wijCSS.stateDefault].join(space),
				volumeClass: string = [localCss.wijvideoVolumeClass, wijCSS.wijvideoVolume, wijCSS.stateDefault, wijCSS.cornerAll].join(space),
				volumeContainerClass: string = [localCss.wijvideoVolumeContainerClass, wijCSS.wijvideoVolumeContainer].join(space),
				volumeSliderClass: string = [localCss.wijvideoVolumeSliderClass, wijCSS.wijvideoVolumeSlider, wijCSS.stateDefault, wijCSS.cornerTop].join(space),
				volumeIconClass: string = [wijCSS.icon, wijCSS.iconVolumeOn].join(space),
				fullScreenClass: string = [localCss.wijvideoFullScreenClass, wijCSS.wijvideoFullScreen, wijCSS.stateDefault, wijCSS.cornerAll].join(space),
				diagIconClass: string = [wijCSS.icon, wijCSS.iconArrow4Diag].join(space), videoContainer, videoContent;

			videoContainer = $("<div>").addClass(videoWidgetClass);
			videoContent = $("<div>").addClass(wrapperClass)
				.append($("<ul>").addClass(controlClass)
					.append($("<li>").addClass(videoPlayClass)
						.append($("<span>").addClass(iconClass)))
					.append($("<li>").addClass(indexClass)
						.append($("<div>").addClass(indexSliderClass)))
					.append($("<li>00:00</li>").addClass(timerClass))
					.append($("<li>").addClass(volumeClass)
						.append($("<div>").addClass(volumeContainerClass)
							.append($("<div>").addClass(volumeSliderClass)))
						.append($("<span>").addClass(volumeIconClass)))
					.append($("<li>").addClass(fullScreenClass)
						.append($("<span>").addClass(diagIconClass))));
			this.$video.wrap(videoContainer).after(videoContent);
		}

		_initialSlideControl(): void {
			var self: wijvideo = this, pos: number, interval: number, wijCSS: WijVideoCSS = self.options.wijCSS,
				o: WijVideoOptions = self.options;
			self.$seekSlider = self.$vidParent.find('.' + localCss.wijvideoIndexSliderClass);

			// create the video this.seek slider
			interval = window.setInterval(function () {
				//replace the attr to prop
				if (self._getVideoAttribute("readyState")) {
					window.clearInterval(interval);
					//note: we need to adjust the size of the video in
					//this time
					self.$vidParent.width(self.$video.outerWidth()).height(self.$video.outerHeight());

					//note: if the controls is invisible, it will not 
					//get the position
					self.$wijvideoControl.show();

					pos = self.$vidParent.find('.' + localCss.wijvideoTimerClass).position().left;
					self.$seekSlider.width(pos - self.$seekSlider.position().left - 15);

					self.$seekSlider.slider({
						value: 0,
						step: 0.01,
						max: self._getVideoAttribute("duration"),
						range: 'min',
						stop: function (e, ui) {
							self._seeking = false;
							self._setVideoAttribute("currentTime", ui.value);
						},
						slide: function () {
							self._seeking = true;
						}
					}).slider("widget").addClass(o.wijCSS.stateDefault);

					self._updateTime();

					// wire up the volume
					self.$volumeSlider = self.$vidParent.find('.' + localCss.wijvideoVolumeSliderClass);
					self.$volumeSlider.slider({
						min: 0,
						max: 1,
						value: self._getVideoAttribute("volume"),
						step: 0.1,
						orientation: 'vertical',
						range: 'min',
						slide: function (e, ui) {
							self._setVideoAttribute("volume", ui.value);
							if (ui.value === 0) {
								self._volumnOn = false;
								self.$volumeBtn.find("span").removeClass(wijCSS.iconVolumeOn)
									.addClass(wijCSS.iconVolumeOff);
							} else {
								self._volumnOn = true;
								self.$volumeBtn.find("span").removeClass(wijCSS.iconVolumeOff)
									.addClass(wijCSS.iconVolumeOn);
							}
						}
					}).slider("widget").addClass(o.wijCSS.stateDefault);

					self.$wijvideoControl.hide();

					self._initToolTip();

					if (!o.showControlsOnHover) {
						self.$wijvideoControl.show();
						self.$vidParent.height(self.$video.outerHeight() + self.$wijvideoControl.height());
					}
				}
				if (self._isDisabled()) {
					if (self.$disabledDiv) {
						self.$disabledDiv.remove();
						self.$disabledDiv = null;
					}
					self._handleDisabledOption(true, self.element);
				}
			}, 200);
		}

		_bindEvents(): void {
			var self: wijvideo = this, $playbtn: JQuery, wijCSS: WijVideoCSS = self.options.wijCSS,
				o: WijVideoOptions = self.options;

			self.$video.bind("click." + self.widgetName, function () {
				self._togglePlay();
			});

			// display the bar on hover
			if (o.showControlsOnHover) {
				self.$vidParent.hover(function () {
					self.$wijvideoControl.stop(true, true).fadeIn();
				}, function () {
						self.$wijvideoControl.delay(300).fadeOut();
					});
			}
			$playbtn = self.$vidParent.find('.' + localCss.wijvideoPlayClass + ' > span');
			$playbtn.click(function () {
				self._togglePlay();
			}).parent().hover(function () {
					$(this).addClass(wijCSS.stateHover);
				}, function () {
					$(this).removeClass(wijCSS.stateHover);
				});

			self.$vidParent.find('.' + localCss.wijvideoVolumeClass).hover(function () {
				$('.' + localCss.wijvideoVolumeContainerClass)
					.stop(true, true).slideToggle();
			});

			self.$fullScreenBtn = self.$vidParent.find('.' + localCss.wijvideoFullScreenClass + ' > span');
			self.$fullScreenBtn.click(function () {
				self._toggleFullScreen();
			}).parent().hover(function () {
					$(this).addClass(wijCSS.stateHover);
				}, function () {
					$(this).removeClass(wijCSS.stateHover);
				});

			if (!self.options.fullScreenButtonVisible) {
				self.$vidParent.find('.' + localCss.wijvideoFullScreenClass).hide();
			}

			self.$volumeBtn.hover(function () {
				$(this).addClass(wijCSS.stateHover);
			}, function () {
					$(this).removeClass(wijCSS.stateHover);
				}).click(function () {
					if (self._getVideoAttribute("readyState")) {
						self._volumnOn = !self._volumnOn;
						if (!self._volumnOn) {
							self._currentVolumn = self.$volumeSlider.slider('value');
							self.$volumeSlider.slider('value', 0);
							self._setVideoAttribute('volume', 0);
							self.$volumeBtn.find("span").removeClass(wijCSS.iconVolumeOn)
								.addClass(wijCSS.iconVolumeOff);
						} else {
							//self.currentVolumn = self.currentVolumn === 0 ? 100 : self.currentVolumn;

							self.$volumeSlider.slider('value', self._currentVolumn ? self._currentVolumn : 1);
							self._setVideoAttribute('volume', self._currentVolumn ? self._currentVolumn : 1);
							self.$volumeBtn.find("span").removeClass(wijCSS.iconVolumeOff)
								.addClass(wijCSS.iconVolumeOn);
						}
					}
				});

			//move the init tooltip to interval, when the video's state
			//is ready, then init the tooltip
			//self._initialToolTip();

			self.$video.bind('play.' + self.widgetName, function () {
				$playbtn.removeClass(wijCSS.icon + " " + wijCSS.iconPlay)
					.addClass(wijCSS.icon + " " + wijCSS.iconPause);
			});

			self.$video.bind('pause.' + self.widgetName, function () {
				$playbtn.removeClass(wijCSS.icon + " " + wijCSS.iconPause)
					.addClass(wijCSS.icon + " " + wijCSS.iconPlay);
			});

			self.$video.bind('ended.' + self.widgetName, function () {
				self.pause();
			});

			self.$video.bind('timeupdate.' + self.widgetName, function () {
				self._updateTime();
			});
		}

		_layout(): void {
			var self: wijvideo = this, ele: JQuery = self.element;
			self._createVideoDom();

			self.$vidParent = self.$video.parent('.' + localCss.wijvideoClass);
			// size the div wrapper to the height and width of the controls
			self.$vidParent.width(self.$video.outerWidth())
				.height(self.$video.outerHeight());
			self.$wijvideoControl = self.$video.parent().find('.' + localCss.wijvideoControlsClass);
			//Volumn
			self._volumnOn = true;
			self.$volumeBtn = self.$vidParent.find('.' + localCss.wijvideoVolumeClass);
			self._initialSlideControl();

			//update for visibility change
			if (ele.is(":hidden") &&
				ele.wijAddVisibilityObserver) {
				ele.wijAddVisibilityObserver(function () {
					self._refresh();
					if (ele.wijRemoveVisibilityObserver) {
						ele.wijRemoveVisibilityObserver();
					}
				}, "wijvideo");
			}
		}

		_create(): void {
			var self: wijvideo = this, ele: JQuery = self.element;

			if (ele.is("video")) {
				self.$video = ele;
			} else {
				self.$video = ele.find("video");
			}

			self.$video.attribute("aria-label", "video");

			//update for fixing bug 18129 by wh at 2011/11/2
			if (!self.$video || self.$video.length === 0 ||
				($.browser.msie && parseInt($.browser.version, 10) < 9)) {
				return;
			}
			//end for fixing

			//Add for fixing bug 18204 by wh at 2011/11/7
			if (!self.$video[0]["canPlayType"]) {
				return;
			}
			//end for fixing bug 18204            
			self._layout();
			self._bindEvents();

			self._videoIsControls = false;
			if (self._getVideoAttribute("controls")) {
				self._videoIsControls = true;
			}
			self.$video.removeAttr('controls');

			super._create();
		}

		_setOption(key: string, value: any): void {
			var self: wijvideo = this, o: WijVideoOptions = self.options,
				wijvideoControl: JQuery = self.$video.parent().find('.' + localCss.wijvideoControlsClass),
				wijvideoFullScreen: JQuery = self.$video.parent().find('.' + localCss.wijvideoFullScreenClass);

			super._setOption(key, value);

			if (key === "fullScreenButtonVisible") {
				o.fullScreenButtonVisible = value;
				if (value) {
					wijvideoFullScreen.show();
				} else {
					wijvideoFullScreen.hide();
				}
			} else if (key === "showControlsOnHover") {
				if (!value) {
					self.$vidParent.unbind('mouseenter mouseleave');
					wijvideoControl.show();
					self.$vidParent.height(self.$video.outerHeight() + wijvideoControl.height());
				} else {
					this.$vidParent.height(this.$video.outerHeight());
					wijvideoControl.hide();
					self.$vidParent.hover(function () {
						wijvideoControl.stop(true, true).fadeIn();
					}, function () {
							wijvideoControl.delay(300).fadeOut();
						});
				}
			}
			//end for disabled option
		}

		_innerDisable() {
			super._innerDisable();
			if (!this._getVideoAttribute("readyState")) return;

			this._handleDisabledOption(true, this.element);
		}

		_innerEnable() {
			super._innerEnable();
			this._handleDisabledOption(false, this.element);
		}

		_handleDisabledOption(disabled: boolean, ele: JQuery): void {
			var self: wijvideo = this,
				wijvideoControl: JQuery = self.$video.parent().find('.' + localCss.wijvideoControlsClass);

			self.$vidParent.toggleClass(this.options.wijCSS.stateDisabled, disabled);
			if (disabled) {
				if (!self.$disabledDiv) {
					self.$disabledDiv = self._createDisabledDiv(ele);
				}
				self.$disabledDiv.appendTo("body");
				self.pause();
			} else {
				if (self.$disabledDiv) {
					self.$disabledDiv.remove();
					self.$disabledDiv = null;
				}
			}
		}

		_createDisabledDiv(outerEle: JQuery): JQuery {
			var ele: JQuery = this.$vidParent,
				eleOffset: JQueryCoordinates = ele.offset(),
				disabledWidth: number = ele.outerWidth(),
				disabledHeight: number = ele.outerHeight();

			return $("<div></div>")
				.addClass(this.options.wijCSS.stateDisabled)
				.css({
					"background-color": "lightgray",
					"z-index": "99999",
					position: "absolute",
					width: disabledWidth,
					height: disabledHeight,
					left: eleOffset.left,
					top: eleOffset.top
				});
		}

		_getVideoAttribute(name: string): any {
			if (name === "") {
				return null;
			}
			return this.$video.prop(name);
		}

		_setVideoAttribute(name: string, value: any): JQuery {
			if (name === "") {
				return null;
			}
			return this.$video.prop(name, value);
		}

		_initToolTip(): void {
			var self: wijvideo = this, wijCSS: WijVideoCSS = self.options.wijCSS,
				videoClass: string = [localCss.wijvideoClass, wijCSS.wijvideo].join(" ");

			//ToolTip-slider
			this.$seekSlider.wijtooltip({
				mouseTrailing: true, showCallout: false,
				position: { offset: '-60 -60' }
			});
			this.$seekSlider.bind("mousemove", function (e) {
				self._seekSliderSkimming(e);
			});

			//ToolTip-button
			this.$volumeBtn.wijtooltip({
				content: self._localizeString("volumeToolTip", "Volume"),
				showCallout: false
			});
			this.$fullScreenBtn.wijtooltip({
				content: self._localizeString("fullScreenToolTip", "Full Screen"),
				showCallout: false
			});

			//add class to prevent from overriding the origin css of tooltip.
			this.$seekSlider.wijtooltip("widget").addClass(videoClass);
			this.$volumeBtn.wijtooltip("widget").addClass(videoClass);
			this.$volumeBtn.wijtooltip("widget").addClass(videoClass);
		}

		_updateTime(): void {
			var self: wijvideo = this,
				currentTime: number = self._getVideoAttribute("currentTime");

			self.$vidParent.find('.' + localCss.wijvideoTimerClass).html(self._getTimeString(currentTime));
			if (!self._seeking && self.$seekSlider.data("ui-slider")) {
				self.$seekSlider.slider('value', currentTime);
			}
		}

		_togglePlay(): void {
			var self: wijvideo = this;

			if (!self._getVideoAttribute("readyState")) {
				return;
			}

			if (self._getVideoAttribute("paused")) {
				self.play();
			} else {
				self.pause();
			}
		}

		_toggleFullScreen(): void {
			var self: wijvideo = this,
				wijCSS: WijVideoCSS = self.options.wijCSS,
				isPaused: boolean = self._getVideoAttribute("paused"),
				offsetWidth: number,
				fWidth: number = $(window).width(),
				fHeight: number = $(window).height();

			self._fullScreen = !self._fullScreen;

			if (self._fullScreen) {
				self._oriVidParentStyle = self.$vidParent.attr("style");
				self._oriWidth = self.$video.outerWidth();
				self._oriHeight = self.$video.outerHeight();
				self._oriDocOverFlow = $(document.documentElement).css("overflow");

				$(document.documentElement).css({
					overflow: "hidden"
				});

				if (!self.$replacedDiv) {
					self.$replacedDiv = $("<div />");
				}

				self.$vidParent.after(self.$replacedDiv);
				self.$vidParent.addClass(localCss.wijvideoContainerFullScreenClass).addClass(wijCSS.wijvideoContainerFullScreen)
					.css({
						width: fWidth,
						height: fHeight
					}).appendTo($("body"));

				self.$video.attr("width", fWidth).attr("height", fHeight);

				$(window).bind("resize.wijvideo", function () {
					self._adjustFullScreen();
				});

				//for reposition the video control
				offsetWidth = fWidth - self._oriWidth;
			} else {
				$(document.documentElement).css({
					overflow: self._oriDocOverFlow
				});

				//for reposition the video control
				offsetWidth = self._oriWidth - self.$video.width();

				self.$replacedDiv.after(self.$vidParent)
					.remove();
				self.$vidParent.removeClass(localCss.wijvideoContainerFullScreenClass)
					.removeClass(wijCSS.wijvideoContainerFullScreen)
					.attr("style", self._oriVidParentStyle);

				self.$video.attr("width", self._oriWidth)
					.attr("height", self._oriHeight);

				$(window).unbind("resize.wijvideo");
			}

			self._positionControls(offsetWidth);
			self._hideToolTips();

			if (!isPaused) {
				self.play();
			} else {
				self.pause();
			}
		}

		_adjustFullScreen(): void {
			var self: wijvideo = this,
				fWidth: number = $(window).width(),
				fHeight: number = $(window).height(),
				offsetWidth: number = fWidth - self.$vidParent.width();

			self.$vidParent.css({
				width: fWidth,
				height: fHeight
			});
			self.$video.attr("width", fWidth).attr("height", fHeight);

			self._positionControls(offsetWidth);
		}

		_positionControls(offsetWidth: number): void {
			var seekSlider: JQuery = this.$vidParent.find('.' + localCss.wijvideoIndexSliderClass);

			seekSlider.width(seekSlider.width() + offsetWidth);
		}

		_seekSliderSkimming(e: JQueryEventObject): void {
			var self: wijvideo = this,
				mousePositionX: number = e.pageX,
				sliderOffset: number = self.$seekSlider.offset().left,
				sliderWidth: number = self.$seekSlider.width(),
				curWidth: number = mousePositionX - sliderOffset,
				duration: number = self._getVideoAttribute("duration"),
				skimmingTime: number = duration * (curWidth / sliderWidth);

			self.$seekSlider.wijtooltip("option", "content", self._getTimeString(skimmingTime));
		}

		_hideToolTips(): void {
			if (this.$seekSlider.data("wijmo-wijtooltip")) {
				this.$seekSlider.wijtooltip("hide");
			}
			if (this.$volumeBtn.data("wijmo-wijtooltip")) {
				this.$volumeBtn.wijtooltip("hide");
			}
			if (this.$fullScreenBtn.data("wijmo-wijtooltip")) {
				this.$fullScreenBtn.wijtooltip("hide");
			}
		}

		_localizeString(key: string, defaultValue: string): string {
			var o: WijVideoOptions = this.options;
			if (o.localization && o.localization[key]) {
				return o.localization[key];
			}
			return defaultValue;
		}

		_getTimeString(time: number): string {
			var mfmt: string = '', sfmt: string = '',
				mm: number = parseInt((time / 60).toString(), 10),
				ss: number = parseInt((time - (mm * 60)).toString(), 10);
			if (mm < 10) {
				mfmt = '0';
			}
			if (ss < 10) {
				sfmt = '0';
			}
			return mfmt + mm + ':' + sfmt + ss;
		}

		_refresh(): void {
			var pos: number, wijvideoControl: JQuery = this.$video.parent().find('.' + localCss.wijvideoControlsClass);

			wijvideoControl.show();
			pos = this.$vidParent.find('.' + localCss.wijvideoTimerClass).position().left;

			this.$seekSlider.width(pos - this.$seekSlider.position().left - 15);
			wijvideoControl.hide();
			if (!this.options.showControlsOnHover) {
				wijvideoControl.show();
				this.$vidParent.height(this.$video.outerHeight() + wijvideoControl.height());
			}
		}

		/**
		* Remove the functionality completely. This will return the element back to its pre-init state.
		*/
		destroy(): void {
			var self: wijvideo = this;
			super.destroy();

			//remove the controls
			self.$vidParent.after(self.$video).remove();
			self.$video.unbind('.' + self.widgetName);
			if (self._videoIsControls) {
				self._setVideoAttribute("controls", true);
			}
			if (self.$disabledDiv) {
				self.$disabledDiv.remove();
				self.$disabledDiv = null;
			}
		}

		/** Play the video. 
		 */
		play(): void {
			var video: HTMLVideoElement = <HTMLVideoElement>this.$video[0];
			video.play();
		}

		/** Pause the video.
		 */
		pause(): void {
			var video: HTMLVideoElement = <HTMLVideoElement>this.$video[0];
			video.pause();
		}

		/** Gets the video width in pixel.
		 */
		getWidth(): number {
			return this.$video.outerWidth();
		}

		/** Sets the video width in pixel.
		 * @param {number} width Width value in pixel.
		 * @example
		 * // Sets the video width to 600 pixel.
		 * $("#element").wijvideo("setWidth", 600);
		 */
		setWidth(width: number): void {
			var origWidth: number = this.getWidth();
			width = width || 600;
			this.$video.attr('width', width);
			this.$vidParent.width(this.$video.outerWidth());
			this._positionControls(this.getWidth() - origWidth);
		}

		/** Gets the video height in pixel.
		 */
		getHeight(): number {
			return this.$video.outerHeight();
		}

		/** Sets the video height in pixel.
		* @param {number} height Height value in pixel.
		* @example
		* // Sets the video height to 400 pixel.
		* $("#element").wijvideo("setHeight", 400);
		*/
		setHeight(height: number): void {
			height = height || 400;
			this.$video.attr('height', height);
			if (this.options.showControlsOnHover) {
				this.$vidParent.height(this.$video.outerHeight());
			} else {
				this.$vidParent.height(this.$video.outerHeight() + this.$video.parent().find('.' + localCss.wijvideoControlsClass).height());
			}
		}
	}

	/** The options of wijvideo widget. */
	export interface WijVideoOptions extends WidgetOptions {
		/** @ignore */
		wijCSS?: WijVideoCSS;

		/** A value that indicates whether to show the full screen button. */
		fullScreenButtonVisible: boolean;

		/** Determines whether to display the controls only when hovering the mouse to the video. */
		showControlsOnHover: boolean;

		/** Use the localization option in order to localize text which not depends on culture.
		* @remarks
		* The default localization: {
		*	    volumeToolTip: "Volume",
		*	    fullScreenToolTip: "Full Screen"
		*      }
		* @example 
		* $("#video").wijvideo(
		*			{ 
		*				localization: {
		*				volumeToolTip: "newVolume",
		*				fullScreenToolTip: "newFullScreen"
		*			}
		*		});
		*/
		localization: WijVideoLocalization;
	}

	/** @ignore */
	export interface WijVideoCSS extends WijmoCSS {
		iconVolumeOn: string;
		iconVolumeOff: string;
		wijvideo: string;
		wijvideoWrapper: string;
		wijvideoControls: string;
		wijvideoPlay: string;
		wijvideoIndex: string;
		wijvideoIndexSlider: string;
		wijvideoTimer: string;
		wijvideoVolume: string;
		wijvideoVolumeContainer: string;
		wijvideoVolumeSlider: string;
		wijvideoFullScreen: string;
		wijvideoContainerFullScreen: string;
	}

	/** Represents the localization settings. */
	export interface WijVideoLocalization {
		/** The tooltip text for the volumn button. */
		volumeToolTip: string;

		/** The tooltip text for the fullscreen button. */
		fullScreenToolTip: string;
	}

	/** @ignore */
	interface LocalCss {
		widgetName: string;
		//Classes
		wijvideoClass: string;
		wijvideoWrapperClass: string;
		wijvideoControlsClass: string;
		wijvideoPlayClass: string;
		wijvideoIndexClass: string;
		wijvideoIndexSliderClass: string;
		wijvideoTimerClass: string;
		wijvideoVolumeClass: string;
		wijvideoVolumeContainerClass: string;
		wijvideoVolumeSliderClass: string;
		wijvideoFullScreenClass: string;
		wijvideoContainerFullScreenClass: string;
	}
	class wijvideo_options implements WijVideoOptions {
		/** Selector option for auto self initialization. This option is internal. 
		* @ignore
		*/
		initSelector: string = ":jqmData(role='wijvideo')";
		/** All CSS classes used in widgets that use jQuery UI CSS Framework. 
		* @ignore
		*/
		wijCSS: WijVideoCSS = {
			iconVolumeOn: "ui-icon-volume-on",
			iconVolumeOff: "ui-icon-volume-off",
			wijvideo: "",
			wijvideoWrapper: "",
			wijvideoControls: "",
			wijvideoPlay: "",
			wijvideoIndex: "",
			wijvideoIndexSlider: "",
			wijvideoTimer: "",
			wijvideoVolume: "",
			wijvideoVolumeContainer: "",
			wijvideoVolumeSlider: "",
			wijvideoFullScreen: "",
			wijvideoContainerFullScreen: ""
		};
		/** A value that indicates whether to show the full screen button. */
		fullScreenButtonVisible: boolean = true;
		/** Determines whether to display the controls only when hovering the mouse to the video. */
		showControlsOnHover: boolean = true;
		/** Use the localization option in order to localize text which not depends on culture.
		* @remarks
		* The default localization: {
		*	    volumeToolTip: "Volume",
		*	    fullScreenToolTip: "Full Screen"
		*      }
		* @example 
		* $("#video").wijvideo(
		*			{ 
		*				localization: {
		*				volumeToolTip: "newVolume",
		*				fullScreenToolTip: "newFullScreen"
		*			}
		*		});
		*/
		localization: WijVideoLocalization = null;
	}

	wijvideo.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options, new wijvideo_options());

	$.wijmo.registerWidget("wijvideo", wijvideo.prototype);

}

/** @ignore */
interface JQuery {
	slider(name?, key?, value?): any;
}

/** @ignore */
interface JQuery {
	wijvideo: JQueryWidgetFunction;
}