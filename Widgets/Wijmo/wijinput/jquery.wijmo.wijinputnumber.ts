﻿/// <reference path="jquery.wijmo.wijstringinfo.ts"/>
/// <reference path="jquery.wijmo.wijinputcore.ts"/>
/// <reference path="jquery.wijmo.wijinputnumberformat.ts"/>

/*globals wijinputcore wijNumberTextProvider wijInputResult 
wijNumberFormat window jQuery*/
/*
 * Depends:
 *	jquery-1.4.2.js
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *	jquery.ui.position.js
 *	jquery.effects.core.js
 *	jquery.effects.blind.js
 *	globalize.js
 *	jquery.mousewheel.js
 *	jquery.wijmo.wijpopup.js
 *	jquery.wijmo.wijcharex.js
*	jquery.wijmo.wijstringinfo.js
 *	jquery.wijmo.wijinputcore.js
 *  jquery.wijmo.wijinputnumberformat.js
 *
 */

module wijmo.input {

	var $ = jQuery,
		jqKeyCode = wijmo.getKeyCodeEnum();


	/** @widget */
	export class wijinputnumber extends wijinputcore {
		options: any;
		_textProvider: wijNumberTextProvider;
		_initialValue: number = 0;


		_userNegativePrefix: string;
		_userNegativeSuffix: string;
		_userPositivePrefix: string;
		_userPositiveSuffix: string;
        _userCurrencySymbol: string;

        _init() {
			super._init();
			this._initUserCultureOptions();
			this._updateCultureRelatedOptions();
			this._updateText();
			this.element.data('changed', false);
			this.element.attribute({
				"role": "spinbutton",
				"aria-label": "wijinputnumber"
			});
		}

        _onKeyPress(e) {
            if (e.charCode && e.charCode !== jqKeyCode.ENTER && !e.ctrlKey) {
				// prevent invalid input
				var chr = String.fromCharCode(e.charCode);
				if (!this._isNumeric(chr)) {
                        this._fireIvalidInputEvent(); //TFS-379397
                        e.preventDefault();
                        return false;                    
				}
			}
			super._onKeyPress(e);
        }

		_isNumeric(chr): boolean {
			var isNum = (chr === this._textProvider.getDecimalSeparator()) || (chr >= '0' && chr <= '9');
			if (!isNum) {
				isNum = '+-()'.indexOf(chr) > -1;
			}
			return isNum;
		}

		_initUserCultureOptions() {
			if (this.options.negativePrefix !== null) {
				this._userNegativePrefix = this.options.negativePrefix;
			}
			if (this.options.negativeSuffix !== null) {
				this._userNegativeSuffix = this.options.negativeSuffix;
			}
			if (this.options.positivePrefix !== null) {
				this._userPositivePrefix = this.options.positivePrefix;
			}
			if (this.options.positiveSuffix !== null) {
				this._userPositiveSuffix = this.options.positiveSuffix;
			}
			if (this.options.currencySymbol !== null) {
				this._userCurrencySymbol = this.options.currencySymbol;
			}
		}

		_deleteUserCultureOptions() {
			delete this._userNegativePrefix;
			delete this._userNegativeSuffix;
			delete this._userPositivePrefix;
			delete this._userPositiveSuffix;
			delete this._userCurrencySymbol;
		}

		// should be called when 'culture', 'type' and 'currencySymbol' changes.
		_updateCultureRelatedOptions() {
			var numberFormat = this._getCulture().numberFormat,
				specificFormat = numberFormat[this.options.type] || numberFormat,
				negPattern: string = specificFormat.pattern[0],
				posPattern: string = specificFormat.pattern[1] || 'n';

			var negSplitArray = negPattern.split('n'),
				posSplitArray = posPattern.split('n'),
				percentSymbol = this._getPercentSymbol(),
				// if user has not set xxxxtiveXxxfix, it will replace all '$' to the currency symbol from culture (not from the symbol user set).
				currencySymbol = this._getCulture().numberFormat.currency.symbol; //this._getCurrencySymbol();
			if (this._userNegativePrefix === undefined) {
				this.options.negativePrefix = negSplitArray[0].replace(/%/g, percentSymbol).replace(/\$/g, currencySymbol);
			}
			if (this._userNegativeSuffix === undefined) {
				this.options.negativeSuffix = negSplitArray[1].replace(/%/g, percentSymbol).replace(/\$/g, currencySymbol);
			}
			if (this._userPositivePrefix === undefined) {
				this.options.positivePrefix = posSplitArray[0].replace(/%/g, percentSymbol).replace(/\$/g, currencySymbol);
			}
			if (this._userPositiveSuffix === undefined) {
				this.options.positiveSuffix = posSplitArray[1].replace(/%/g, percentSymbol).replace(/\$/g, currencySymbol);
			}
			if (this._userCurrencySymbol === undefined) {
				this.options.currencySymbol = numberFormat.currency.symbol;
			}
		}

		_createTextProvider() {
			if (this._textProvider) {
				return this._textProvider;
			}
			this._textProvider = new wijNumberTextProvider(this, this.options.type);
			this._textProvider.setNullValue(this.element.val() === "" && this.options.value === null);
		}

		_beginUpdate() {
			this._initialValue = 0;
			var o = this.options;
			this.element.addClass(o.wijCSS.wijinputnumber);

			this.element.data({
				defaultValue: o.value,
				preValue: o.value
			}).attribute({
					'aria-valuemin': o.minValue,
					'aria-valuemax': o.maxValue,
					'aria-valuenow': o.value || this._initialValue
				});
		}

		/** Set the focus to the widget. */
		focus() {
			this._doFocus();
		}


		_setOption(key: string, value) {
			super._setOption(key, value);

			switch (key) {
				case 'minValue':
					this.element.attribute('aria-valuemin', value);
					this._updateText();
					break;

				case 'maxValue':
					this.element.attribute('aria-valuemax', value);
					this._updateText();
					break;

				case 'value':
					this.setValue(<string>value);
					this._updateText();
					break;

				case 'showGroup':
				case 'decimalPlaces':
				case 'culture':
					//this._textProvider.updateStringFormat();
					this._updateCultureRelatedOptions();
					this._updateText();
					break;
				case 'positivePrefix':
					this._userPositivePrefix = <string>value;
					this._updateText();
					break;
				case 'negativePrefix':
					this._userNegativePrefix = <string>value;
					this._updateText();
					break;
				case 'positiveSuffix':
					this._userPositiveSuffix = <string>value;
					this._updateText();
					break;
				case 'negativeSuffix':
					this._userNegativeSuffix = <string>value;
					this._updateText();
					break;
				case 'currencySymbol':
					this._userCurrencySymbol = <string>value;
					this._updateCultureRelatedOptions();
					this._updateText();
					break;
				case 'type':
					this._deleteUserCultureOptions();
					this._updateCultureRelatedOptions();
					this._updateText();
					break;
			}
		}

		_setData(val) {
			this.setValue(val);
		}

		_resetData() {
			var val = this.element.data('defaultValue');
			if (val === 0) {
				var elementValue = this.element.data('elementValue');
				if (elementValue !== undefined && elementValue !== null && elementValue !== "") {
					val = elementValue;
				}
			}

			if (val !== this._textProvider.getValue() || (this._textProvider._nullvalue && val === 0)) {
				this.setValue(val);
			}
		}

		_validateData() {
			if (!this._textProvider.checkAndRepairBounds(true, false) && this.options.value !== null) {
				this._updateText();
			}
		}

		_raiseDataChanged() {
			var v = this.options.value,
				prevValue = this.element.data('preValue');
			this.element.data('preValue', v);
			if (prevValue !== v) {
				this.element.attribute('aria-valuenow', v);
				this._trigger('valueChanged', null, { value: v });
			}
		}

		_onChange() {
		}

		_getPrefix(): string {
			if (this._textProvider._stringFormat.isNegative()) {
				if (this._userNegativePrefix !== undefined) {
					return this._userNegativePrefix;
				}
				else {
					return this.options.negativePrefix;
				}
			}
			else {
				if (this._userPositivePrefix !== undefined) {
					return this._userPositivePrefix;
				}
				else {
					return this.options.positivePrefix;
				}
			}
		}

		_getInvertPrefix(): string {
			if (this._textProvider._stringFormat.isNegative()) {
				if (this._userPositivePrefix !== undefined) {
					return this._userPositivePrefix;
				}
				else {
					return this.options.positivePrefix;
				}
			}
			else {
				if (this._userNegativePrefix !== undefined) {
					return this._userNegativePrefix;
				}
				else {
					return this.options.negativePrefix;
				}
			}
		}

		_getSuffix(): string {
			if (this._textProvider._stringFormat.isNegative()) {
				if (this._userNegativeSuffix !== undefined) {
					return this._userNegativeSuffix;
				}
				else {
					return this.options.negativeSuffix;
				}
			}
			else {
				if (this._userPositiveSuffix !== undefined) {
					return this._userPositiveSuffix;
				}
				else {
					return this.options.positiveSuffix;
				}
			}
		}

		_getCurrencySymbol(): string {
			if (this._userCurrencySymbol !== undefined) {
				return this._userCurrencySymbol;
			}
			else {
				return this._getCulture().numberFormat.currency.symbol;
			}
		}

		_getPercentSymbol(): string {
			return this._getCulture().numberFormat.percent.symbol;
		}

		/** Gets the value. */
		getValue() {
			if (this._textProvider._nullvalue) {
				return null;
			}

			var val: any = this._textProvider.getValue();
			if (val === undefined || val === null) {
				val = this.getText();
			}
			return val;
		}

		/** Sets the value.
		  * @example
		  * // set value of a wijinputnumber to 10
		  * $(".selector").wijinputnumber("setValue", 10, true);
		  */
		setValue(val, exact = false) {
			try {
				exact = !!exact;
				if (val === null || typeof val === 'undefined' || isNaN(val)) {
					val = null;
				} else if (typeof val === 'boolean') {
					val = val ? 1 : 0;
				} else if (typeof val === 'string') {
					val = this._textProvider.tryParseValue(val);
				}

				if (this._textProvider.setValue(val)) {
					this._updateText();
				} else {
					if (exact) {
						var prevVal = this.getText();
						this.setText(val);
						var txt = this.getText().trim();
						val = val.trim();
						if (txt !== val) {
							this.setText(prevVal);
						}
					} else {
						this.setText(val);
					}
				}

				return true;
			}
			catch (e) {
				return false;
			}
		}

		_isLastValueNull() {
			return super._isLastValueNull() && !this._textProvider._nullvalue;
		}


		/** Determines whether the value is in null state. */
		isValueNull() {
			try {
				return (this._textProvider).isValueNull();
			}
			catch (e) {
				return true;
			}
		}

		/** Gets the text value when the container form is posted back to server. */
		getPostValue() {
			if (!this._isInitialized()) {
				return this.element.val();
			}
			if (super._showNullText() && this.isValueNull()) {
				return "0";
			}

			var val = this.options.value ? this.options.value : 0;
			if (this.options.type === "percent") {
				val = (val / 100).toFixed(10);
			}

			return val.toString();
		}

		_updateText(keepSelection = false) {
			if (!this._isInitialized()) {
				return;
			}

			if (this._textProvider._stringFormat.isNegative()) {
				this.element.addClass(this.options.negativeClass);
			}
			else {
				this.element.removeClass(this.options.negativeClass);
			}

			if (!this._textProvider._nullvalue) {
				this.options.value = this._textProvider.getValue();
			}
			else {
				this.options.value = null;
			}
			super._updateText(keepSelection);
			if (!this._textProvider.checkAndRepairBounds(true, false)) {
				this._trigger('valueBoundsExceeded');
				if (!this.isFocused()) {
					this._textProvider.setNullValue(false);
					this.options.value = this._textProvider.getValue();
					if (this._textProvider._stringFormat.isNegative()) {
						this.element.addClass(this.options.negativeClass);
					}
					else {
						this.element.removeClass(this.options.negativeClass);
					}
					super._updateText(keepSelection);
					return;
				}
			}

			if (!this._textProvider._nullvalue) {
				this.options.value = this._textProvider.getValue();
			}
			else {
				this.options.value = null;
			}
		}

		_doSpin(up: boolean, repeating: boolean, noFocus?: boolean): boolean {
			if (!this._allowEdit()) {
				return false;
			}
			if (repeating && this.element.data('breakSpinner')) {
				return false;
			}
			var selRange = this.element.wijtextselection(),
				rh = new wijInputResult();
			if (this.element.data('focusNotCalledFirstTime') !== -9 &&
				(new Date().getTime() - this.element.data('focusNotCalledFirstTime')) < 600) {
				this.element.data('focusNotCalledFirstTime', -9);
				this.element.data('prevSelection', { start: 0, end: 0 });
			}
			if (this.element.data('prevSelection') === null) {
				this.element.data('prevSelection', { start: selRange.start, end: selRange.start });
			} else {
				selRange.start = (this.element.data('prevSelection').start);
			}
			rh.testPosition = selRange.start;
			if (this.options.increment < 0) {
				var incrementBackup = this.options.increment;
				this.options.increment *= -1;
				this._textProvider.spinEnumPart(selRange.start, rh, this.options.increment, !up);
				this.options.increment = incrementBackup;
			}
			else {
				this._textProvider.spinEnumPart(selRange.start, rh, this.options.increment, up);
			}
			up ? this._trigger('spinUp', null) : this._trigger('spinDown', null);
			this._updateText();
			this.element.data('prevSelection', { start: rh.testPosition, end: rh.testPosition });
			if (!noFocus) {
				this.selectText(rh.testPosition, rh.testPosition);
			}
			if (repeating && !this.element.data('breakSpinner')) {
				var spintimer = window.setTimeout($.proxy(function () {
					this._doSpin(up, true);
				}, this), this._calcSpinInterval());
				this.element.data("spintimer", spintimer);
			}
			return true;
		}

		_deleteSelText(backspace = false) {
			var sel = this.element.wijtextselection(),
				everythingSelected = sel.start === 0 && sel.end === this.element.val().length,
				text: string;

			if (sel.start === sel.end) {
				// move right if "delete" key is pressed and there cursor is to left from a group/decimal separator symbol
				if (!backspace) {
					text = this._textProvider._stringFormat._currentText;
					if (text.charAt(sel.end) === this._textProvider.getGroupSeparator() || text.charAt(sel.end) === this._textProvider.getDecimalSeparator()) {
						sel.start++;
						sel.end++;
						this.element.wijtextselection(sel);
						return;
					}
				}
			}

			var previousValue = this._textProvider.getEditingValue();

			super._deleteSelText(backspace);

			// this will set value to repaired max/min value.
			//this._textProvider.checkAndRepairBounds(true, false);
			//this.options.value = this._textProvider.getValue();
			//if ((everythingSelected || parseFloat(this.element.val()) === 0) &&  this.options.value === 0){

			if (previousValue === 0 && (everythingSelected || this._textProvider.getEditingValue() === 0)) {
				// everything is selected
				this._textProvider.setNullValue(true);
				this._updateText(true);
				this.options.value = null;
			}
		}

		_processClearButton() {
			this._setOption("value", null);
		}

		_processLeftRightKey(isLeftKey: boolean) {
			if (isLeftKey) {
				if (this.element.wijtextselection().start === 0 && (this.options.blurOnLeftRightKey.toLowerCase() === "left" || this.options.blurOnLeftRightKey.toLowerCase() === "both")) {
					CoreUtility.MoveFocus(this.element.get(0), false);
					return true;
				}
			} else {
				if (this.element.wijtextselection().start === this.element.val().length && (this.options.blurOnLeftRightKey.toLowerCase() === "right" || this.options.blurOnLeftRightKey.toLowerCase() === "both")) {
					CoreUtility.MoveFocus(this.element.get(0), true);
					return true;
				}
			}
			return false;
		}

		_processTabKey(e) {
			var key = e.keyCode || e.which;
			switch (key) {
				case jqKeyCode.TAB:
					if (this.options.tabAction !== "field") {
						this._trigger('keyExit');
						break;
					}
					var dpPosition = this.element.val().indexOf('.');
					if (dpPosition === -1) {
						dpPosition = this.element.val().length;
					}
					var selRange = this.element.wijtextselection();
					if (e.shiftKey) {
						if (selRange.start > dpPosition) {
							this._toPrevField();
						}
					} else {
						if (selRange.start < dpPosition) {
							this._toNextField();
						}
					}
					return true;
			}
			return false;
		}

		_toPrevField() {
			var prefix = this._getPrefix();
			//if (this.options.type === 'currency') {
			//    prefix = '$' + prefix;
			//}
			this.element.wijtextselection(prefix.length, prefix.length);
		}

		_toNextField() {
			var dpPosition = this.element.val().indexOf('.');
			if (dpPosition === -1) {
				this._toPrevField();
			}
			else {
				this.element.wijtextselection(dpPosition + 1, dpPosition + 1);
			}
		}

		/** Performs spin up by the specified field and increment value. */
		spinUp() {
			this._doSpin(true, false, true);
		}
		/** Performs spin down by the specified field and increment value. */
		spinDown() {
			this._doSpin(false, false, true);
		}




		_getRealNegativePrefix() {
			if (this._userNegativePrefix !== undefined) {
				return this._userNegativePrefix.replace(/\$/g, this.options.currencySymbol);
			}
			else {
				if (this.options.negativePrefix === null) { // in _create()
					return "";
				}
				return this.options.negativePrefix.replace(/\$/g, this.options.currencySymbol);
			}
		}

		_getRealPositivePrefix() {
			if (this._userPositivePrefix !== undefined) {
				return this._userPositivePrefix.replace(/\$/g, this.options.currencySymbol);
			}
			else {
				if (this.options.positivePrefix === null) { // in _create()
					return "";
				}
				return this.options.positivePrefix.replace(/\$/g, this.options.currencySymbol);
			}
		}

		_getRealNegativeSuffix() {
			if (this._userNegativeSuffix !== undefined) {
				return this._userNegativeSuffix.replace(/\$/g, this.options.currencySymbol);
			}
			else {
				if (this.options.negativeSuffix === null) { // in _create()
					return "";
				}
				return this.options.negativeSuffix.replace(/\$/g, this.options.currencySymbol);
			}
		}

		_getRealPositiveSuffix() {
			if (this._userPositiveSuffix !== undefined) {
				return this._userPositiveSuffix.replace(/\$/g, this.options.currencySymbol);
			}
			else {
				if (this.options.positiveSuffix === null) { // in _create()
					return "";
				}
				return this.options.positiveSuffix.replace(/\$/g, this.options.currencySymbol);
			}
		}

		_afterFocused() {
			if (this._isNullText()) {
				super._doFocus();
			}
			else {
				this._doFocus();
			}
		}

		_doFocus() {

			super.focus();
            
			if (this.options.highlightText) {
				this.element.wijtextselection(0, this.element.val().length);
            }
            // TFS: 391973 Don't need recover the caret position
			//else {
			//	// if focused by calling this method, recover the caret position.
			//	var prevPos = this.element.data('prevSelection');
			//	if (prevPos) {
			//		this.element.wijtextselection(prevPos.start, prevPos.end);
			//	}
			//}

			this.element.data('isFocusSelecting', true);
		}
	}
	class wijinputnumber_options {
		wijCSS = {
			wijinputnumber: wijinputcore.prototype.options.wijCSS.wijinput + "-numeric",
			wijinputnumberNegative: "ui-state-negative"
		};

		/** Determines the type of the number input.
		  * Possible values are: 'numeric', 'percent', 'currency'. 
		  */
		type = 'numeric';

		/** Determines the default numeric value. 
		  */
		value: number = 0;

		/** Determines the minimal value that can be entered for 
		  * numeric/percent/currency inputs. 
		  */
		minValue = -1000000000;

		/** Determines the maximum value that can be entered for 
		  * numeric/percent/currency inputs. 
		  */
		maxValue = 1000000000;

		/** Indicates whether the thousands group separator will be 
		  * inserted between between each digital group 
		  * (number of digits in thousands group depends on the selected Culture). 
		  */
		showGroup = false;

		/** Indicates the number of decimal places to display.
		  * @remarks
		  * Possible values are integer from -2 to 8. 
		  */
		decimalPlaces = 2;

		/** Determines how much to increase/decrease the active field when performing spin on the the active field.
		  */
		increment = 1;

		/** Determine the current symbol when number type is currency.
		  * @remarks
		  * If the currencySymbol's value is null and number's type is currency, wijinputnumber will dispaly the currency symbol from the current culture, for use US culture, it is '$'.  <br />
		  * But user can change this behavior by setting the currencySymbol option, then the '$' will changed to the specified value.
		  */
		currencySymbol: string = null;

		/** Determine the class string that when the input value is negative.
		  * @remarks
		  * When the number widget's value is negative, the negativeClass will apply the number input.
		  * @example
		  *  // <style type="text/css" rel="stylesheet">
		  *  //         .negative {
		  *  //             background: yellow;
		  *  //             color: red !important;
		  *  //         }
		  *  //</style>
		  *
		  * $("#textbox1").wijinputnumber({
		  *       negativeClass: 'negative'
		  *         
		  * });
		  *
		  */
		negativeClass: string;

		/** Determine the prefix string used for negative value.
		  * The default value will depend on the wijinputnumber's type option.
		  */
		negativePrefix: string = null;

		/** Determine the suffix string used for negative value.
		   * The default value will depend on the wijinputnumber's type option.
		  */
		negativeSuffix: string = null;

		/** Determine the prefix string used for positive value.
		  * The default value will depend on the wijinputnumber's type option.
		  */
		positivePrefix: string = null;

		/** Determine the suffix stirng used for positive value.
		  * The default value will depend on the wijinputnumber's type option.
		  */
		positiveSuffix: string = null;

		/** @ignore */
		imeMode = '';

		/** Indicates whether to highlight the control's Text on receiving input focus.
		  */
		highlightText = false;

		/** The valueChanged event handler. 
		  * A function called when the value of the input is changed.
		  * @event
		  * @dataKey value The new value */
		valueChanged = null;

		/** The spinUp event handler.
		  * A function called when spin up event fired.
		  * @event
		  */
		spinUp = null;

		/** The spinDown event handler.
		  * A function called when spin down event fired.
		  * @event
		  */
		spinDown = null;

		/** The valueBoundsExceeded event handler. A function called when 
		  * the value of the input exceeds the valid range.
		  * @event
		  */
		valueBoundsExceeded = null;

		constructor() {
			this.negativeClass = this.wijCSS.wijinputnumberNegative;
		}
	}
	wijinputnumber.prototype.options = <any>$.extend(true, {}, wijinputcore.prototype.options, new wijinputnumber_options());

	$.wijmo.registerWidget("wijinputnumber", wijinputnumber.prototype);

	//==============================
	/** @ignore */
	export class wijNumberTextProvider implements IInputFormatterProvider {
		noMask: boolean;
		_type = 'numeric';
		_stringFormat: wijNumberFormat = null;
		_nullvalue = false;
		inputWidget: wijinputnumber;

		constructor(owner, type) {
			this.inputWidget = owner;
			this._type = type;
			this._stringFormat = new wijNumberFormat(/*this.inputWidget*/);
			this._stringFormat.setValueFromJSFloat(this.getValue(), this.inputWidget._getCulture(),
				this.inputWidget._getRealPositivePrefix(), this.inputWidget._getRealPositiveSuffix(),
				this.inputWidget._getRealNegativePrefix(), this.inputWidget._getRealNegativeSuffix(), this.inputWidget.options);
		}

		getEditingValue() {
			return this._stringFormat.tryParseValue(this._stringFormat._strippedText, this.inputWidget._getCulture(), this.inputWidget.options);
		}
		_getCulture() {
			return this.inputWidget._getCulture();
		}

		getDecimalSeparator() {
			return this._getCulture().numberFormat['.'];
		}

		getGroupSeparator() {
			return this._getCulture().numberFormat[","];
		}

		tryParseValue(value: string) {
			return this._stringFormat.tryParseValue(value, this.inputWidget._getCulture(), this.inputWidget.options);
		}

		toString(): string {
			if (this.inputWidget._showNullText() &&
				!this.inputWidget.isFocused() && this.isValueNull()) {
				return this.inputWidget.options.nullText;
			}

			//if (!this.inputWidget._showNullText() && this.inputWidget.isValueNull()) {
			//    if (this.inputWidget._initialValue === null || isNaN(this.inputWidget._initialValue)) {
			//        return '';
			//    }
			//}

			if (this.isValueNull()) {
				return "";
			}

			return this._stringFormat.getFormattedValue(this.inputWidget._initialValue, this.isValueNull(), this.inputWidget._getCulture(),
				this.inputWidget._getRealPositivePrefix(), this.inputWidget._getRealPositiveSuffix(),
				this.inputWidget._getRealNegativePrefix(), this.inputWidget._getRealNegativeSuffix(), this.inputWidget.options);
		}

		isValueNull() {
			var o = this.inputWidget.options;
			//    nullValue = o.minValue;
			//nullValue = Math.max(0, o.minValue);

			//return null === o.value || undefined === o.value || nullValue === o.value || this._nullvalue;
			//for fix the issue 40695
			return null === o.value || undefined === o.value || this._nullvalue;
		}

		setText(inputText: string, rh?: wijInputResult) {
			this.clear();
			this.insertAt(inputText, 0, rh);
			return true;
		}

		clear() {
			this._stringFormat.clear();
		}

		checkAndRepairBounds(chkAndRepair: boolean, onlyChkIsLessOrEqMin: boolean, allowWrap = false) {
			if (this._nullvalue) {
				return true;
			}
			var result = true, minValue, maxValue;
			if (typeof (chkAndRepair) === 'undefined') {
				chkAndRepair = false;
			}

			minValue = this.inputWidget.options.minValue;
			maxValue = this.inputWidget.options.maxValue;

			if (typeof (onlyChkIsLessOrEqMin) !== 'undefined' && onlyChkIsLessOrEqMin) {
				return this._stringFormat.checkMinValue(minValue, false, true);
			}
			var needCheckMax = true;
			if (!this._stringFormat.checkMinValue(minValue, chkAndRepair, false)) {
				result = false;
				if (allowWrap) {
					needCheckMax = false;
					this._stringFormat._currentValueInString = maxValue.toString();
				}
			}
			if (needCheckMax && !this._stringFormat.checkMaxValue(maxValue, chkAndRepair)) {
				result = false;
				if (allowWrap) {
					this._stringFormat._currentValueInString = minValue.toString();
				}
			}
			if (this.inputWidget.options.decimalPlaces >= 0) {
				this._stringFormat.checkDigitsLimits(this.inputWidget.options.decimalPlaces);
			}

			return result;
		}

		_countSubstring(txt: string, subStr: string) {
			var c = 0,
				pos = txt.indexOf(subStr);
			while (pos !== -1) {
				c++;
				pos = txt.indexOf(subStr, pos + 1);
			}
			return c;
		}

		_getAdjustedPositionFromLeft(position) {
			var currentStrippedText = this._stringFormat._strippedText, i, ch;
			for (i = 0; i < currentStrippedText.length; i++) {
				ch = currentStrippedText.charAt(i);
				if (!$.wij.charValidator.isDigit(ch) &&
					(ch !== this.getGroupSeparator() && ch !== this.getDecimalSeparator()) || ch === '0') {
					if (this._stringFormat.isZero()) {
						if (position < i) {
							position++;
						}
					} else {
						if (position <= i) {
							position++;
						}
					}
				} else {
					break;
				}
			}

			return position;
		}

		_getDecimalSeparatorPos() {
			var currentStrippedText = this._stringFormat._strippedText;
			return currentStrippedText.indexOf(this.getDecimalSeparator());
		}

		_removeLeadingZero(num: string) {
			var pos = 0;
			if (num.charAt(pos) === ' ' || num.charAt(pos) === this.inputWidget.options.currencySymbol) {
				pos++;
			}

			var cultureObj = Globalize.findClosestCulture(this.inputWidget.options.culture);
			var nf = cultureObj.numberFormat,
				specificFormat = nf[this.inputWidget.options.type] || nf;

			while (num.charAt(pos) === '0' && num.charAt(pos + 1) !== specificFormat['.']) {
				num = num.substr(0, pos) + num.substr(pos + 1);
			}

			return num;
		}

		insertAt(strInput: string, position: number, rh?: wijInputResult) {
			var nf = this._getCulture().numberFormat,
				pos: number,
				slicePos: number,
				beginText: string,
				endText: string,
				newText: string,
				oldGroupSeparatorCount: number,
				newGroupSeparatorCount: number,
				leftPrevCh: string,
				leftCh: string;

			var currentTextWithoutMinus = this._stringFormat._strippedText;

			var prefix: string = this.inputWidget._getPrefix(),
				invertPrefix: string = this.inputWidget._getInvertPrefix();

			if (position < prefix.length) {
				position = prefix.length;
			}
			else if (position > prefix.length + currentTextWithoutMinus.length) {
				position = prefix.length + currentTextWithoutMinus.length;
			}
			position -= prefix.length;


			if (!rh) {
				rh = new wijInputResult();
			}

			var isNullValueBackup = this._nullvalue;
			this.setNullValue(false);

			if (strInput.length === 1) {
				if (strInput === '+') {
					var signBefore = this._stringFormat.isNegative();
					this._stringFormat.setPositiveSign();
					this.checkAndRepairBounds(true, false);
					var isInvert = signBefore !== this._stringFormat.isNegative();
					if (isInvert) {
						rh.testPosition = invertPrefix.length + position - 1;
					}
					else {
						rh.testPosition = prefix.length + position - 1;
					}
					return true;
				}
				if (strInput === '-' || strInput === ')' || strInput === '(') {
					var isNegativeBefore = this._stringFormat.isNegative();
					this._stringFormat.invertSign(this.inputWidget._getCulture(),
						this.inputWidget._getRealPositivePrefix(), this.inputWidget._getRealPositiveSuffix(),
						this.inputWidget._getRealNegativePrefix(), this.inputWidget._getRealNegativeSuffix(), this.inputWidget.options);
					this.checkAndRepairBounds(true, false);
					if (this._stringFormat.isNegative() === isNegativeBefore) {
						rh.testPosition = prefix.length + position - 1;
						return true;
					}
					else {
						rh.testPosition = invertPrefix.length + position - 1;
						//if (this._stringFormat.isNegative()) {
						//    rh.testPosition = position;
						//}
						//else {
						//    rh.testPosition = position - 2;
						//}
						return true;
					}
				}
				if (!$.wij.charValidator.isDigit(strInput)) {
					if (strInput === this.getDecimalSeparator()) {
						pos = this._getDecimalSeparatorPos() + prefix.length;
						//if (this._stringFormat.isNegative()) {
						//    pos = pos - 1;  // _stringFormat._currentText starts with '-'.
						//}
						if (pos >= 0) {
							rh.testPosition = pos;
							return true;
						}
					}
					if (strInput !== this.getGroupSeparator() && strInput !== this.getDecimalSeparator() && strInput !== ')' &&
						strInput !== '+' && strInput !== '-' && strInput !== '(' &&
						strInput !== this.getDecimalSeparator()) {
						if (this._type === 'percent' && strInput === nf.percent.symbol) {
							rh.testPosition = position;
							return true;
						} else if (this._type === 'currency' &&
							strInput === this.inputWidget.options.currencySymbol) {
							rh.testPosition = position;
							return true;
						} else {
							this.setNullValue(isNullValueBackup);
							return false;
						}
					}
				}
			}

			position = this._getAdjustedPositionFromLeft(position);
			slicePos = position;
			if (slicePos > currentTextWithoutMinus.length) {
				slicePos = currentTextWithoutMinus.length - 1;
			}
			// if (input.length === 1) {
			// if (currentText.charAt(slicePos) === input) {
			// rh.testPosition = slicePos;
			// return true;
			// }
			// }
			beginText = this._removeLeadingZero(currentTextWithoutMinus.substring(0, slicePos));
			endText = currentTextWithoutMinus.substring(slicePos, currentTextWithoutMinus.length);
			if (this._stringFormat.isZero()) {
				endText = endText.replace(/0/, '');
			}

			var filtratedText = strInput,
				sign = '';
			while (filtratedText.charAt(0) === '+' || filtratedText.charAt(0) === '-'
				|| filtratedText.charAt(0) === ')' || filtratedText.charAt(0) === '(') {
				sign = filtratedText.charAt(0);
				filtratedText = filtratedText.substring(1);
			}
			filtratedText = filtratedText.replace(/[^0-9\.]/gi, "");
			var decimalPosition = filtratedText.indexOf('.');
			if (decimalPosition !== -1) {
				var clips = filtratedText.split('.');
				var integerString, decimalString;

				integerString = clips.shift();
				var decimalClips = clips;
				decimalString = decimalClips.join('');

				filtratedText = integerString + '.' + decimalString;
			}
			else {
				filtratedText = filtratedText;
			}
			if (sign === '+') {
				this._stringFormat.setPositiveSign();
				this.checkAndRepairBounds(true, false);
			}
			if (sign === '-' || sign === ')' || sign === '(') {
				this._stringFormat.invertSign(this.inputWidget._getCulture(),
					this.inputWidget._getRealPositivePrefix(), this.inputWidget._getRealPositiveSuffix(),
					this.inputWidget._getRealNegativePrefix(), this.inputWidget._getRealNegativeSuffix(), this.inputWidget.options);
				this.checkAndRepairBounds(true, false);
			}

			rh.testPosition = prefix.length + beginText.length + filtratedText.length - 1;
			this._stringFormat.deFormatValue((this._stringFormat.isNegative() ? "-" : "") + beginText + filtratedText + endText, this.inputWidget._getCulture(),
				this.inputWidget._getRealPositivePrefix(), this.inputWidget._getRealPositiveSuffix(),
				this.inputWidget._getRealNegativePrefix(), this.inputWidget._getRealNegativeSuffix(), this.inputWidget.options);
			newText = this._stringFormat._strippedText;

			//this.checkAndRepairBounds(true, false);
			try {
				if (this.inputWidget.options.showGroup) {
					oldGroupSeparatorCount = this._countSubstring(beginText, this._stringFormat._groupSeparator);
					newGroupSeparatorCount = this._countSubstring(newText.substring(0, beginText.length + filtratedText.length), this._stringFormat._groupSeparator);
					rh.testPosition += newGroupSeparatorCount - oldGroupSeparatorCount;
				} else if (filtratedText.length === 1) {
					leftPrevCh = beginText.charAt(beginText.length - 1);
					leftCh = newText.charAt(beginText.length - 1);
					//leftCh = newText.charAt(rh.testPosition - 1);
					if (leftCh !== leftPrevCh) {
						rh.testPosition--;
					}
				}
			}
			catch (e) {
			}

			return true;
		}

		removeAt(start: number, end: number, rh: wijInputResult = new wijInputResult(), skipCheck = false) {
			var nf = this._getCulture().numberFormat,
				curText: string,
				beginText: string,
				newText: string,
				decimalSepRemoved: boolean,
				oldGroupSeparatorCount: number,
				newGroupSeparatorCount: number;

			rh.testPosition = start;
			try {
				curText = this._stringFormat._currentText;
				if ((start === end) && curText.substring(start, end + 1) === this.getDecimalSeparator()) {
					return false;
				}
				beginText = curText.slice(0, start);

				newText = beginText;
				decimalSepRemoved = curText.slice(start, end + 1).indexOf(this.getDecimalSeparator()) >= 0;
				if (decimalSepRemoved) {
					newText += this.getDecimalSeparator();
				}
				newText += curText.slice(end + 1);
				newText = newText || "0";
				this._stringFormat.deFormatValue(newText, this.inputWidget._getCulture(),
					this.inputWidget._getRealPositivePrefix(), this.inputWidget._getRealPositiveSuffix(),
					this.inputWidget._getRealNegativePrefix(), this.inputWidget._getRealNegativeSuffix(), this.inputWidget.options);
				newText = this._stringFormat._currentText;

				this.setNullValue(false);

				if (this.inputWidget.options.showGroup) {
					if (decimalSepRemoved) {
						rh.testPosition = newText.indexOf(this.getDecimalSeparator());
					} else {
						try {
							oldGroupSeparatorCount = this._countSubstring(beginText, this._stringFormat._groupSeparator);
							newGroupSeparatorCount = this._countSubstring(newText.substring(0, Math.max(0, start - 1)), this._stringFormat._groupSeparator);
							rh.testPosition -= oldGroupSeparatorCount - newGroupSeparatorCount;
							if (curText.indexOf(this.inputWidget.options.currencySymbol) === rh.testPosition || curText.indexOf(nf.percent.symbol) === rh.testPosition) {
								rh.testPosition++;
							}
						}
						catch (e1) {
						}
					}
				}

				// if (!skipCheck){
				// this.checkAndRepairBounds(true, false);
				// }
				return true;
			}
			catch (e2) {
			}

			// if (!skipCheck){
			// this.checkAndRepairBounds(true, false);
			// }
			return true;
		}

		replaceWith(range: WijTextSelection, text: string) {
			if (text === '.' && this.inputWidget.options.decimalPlaces <= 0) {
				var result = new wijInputResult();
				result.testPosition = range.start - 1;
				return result;
			}

			return this.superReplaceWith(range, text);
		}

		superReplaceWith(range: WijRange, text: string) {
			var index = range.start;
			var result = new wijInputResult();
			if (range.start < range.end) {
				this.removeAt(range.start, range.end - 1, result, true);
				index = result.testPosition;
			}
			return this.insertAt(text, index, result) ? result : null;
		}

		_parseNumber(strippedValue: string = this._stringFormat._currentValueInString) {
			var arr = strippedValue.split(".", 2);

			var retObj = {
				integer: parseInt(arr[0], 10) || 0,
				fraction: (arr.length > 1 ? parseInt(arr[1], 10) : 0) || 0,
				isNegative: arr[0].indexOf('-') !== -1
			};

			if (retObj.integer === 0 && retObj.fraction === 0) {
				retObj.isNegative = false;
			}

			return retObj;
		}

		_doSpin(delta: number, nullValue) {    // 5, -1, 0.03, -0.08
			var num = this._parseNumber();

			var deltaDirection = delta >= 0 ? 1 : -1;
			var spinPosition = Math.abs(delta) >= 1 ? 'integer' : 'decimal';
			var deltaUnit = this.inputWidget.options.increment;
			if (deltaUnit >= Math.pow(10, this.inputWidget.options.decimalPlaces)) {
				if (spinPosition === 'integer') {
					deltaUnit = Math.abs(delta);
				}
				else {
					deltaUnit = Math.abs(delta) * Math.pow(10, this.inputWidget.options.decimalPlaces);
				}
			}
			var deltaInPosition = deltaDirection * deltaUnit;


			var signPrefix: string = '',
				integerString: string,
				decimalString: string;

			var integerResult = num.integer,
				decimalResult = num.fraction;

			if (spinPosition === 'integer') {
				integerResult = num.integer + deltaInPosition;
				if (integerResult < 0) {
					signPrefix = '-';
					integerResult = -integerResult;
				}
			}
			else if (spinPosition === 'decimal') {
				var decimalValue: number = num.isNegative ? -num.fraction : num.fraction;
				integerResult = num.integer;

				if (decimalValue < 0) {
					decimalResult = -Math.abs(decimalValue + deltaInPosition);
					if (decimalValue * (decimalValue + deltaInPosition) < 0) {
						integerResult = Math.abs(integerResult);
						signPrefix = '-';
					}
				}
				else {
					decimalResult = decimalValue + deltaInPosition;
				}


				if (decimalValue === 0 && num.isNegative) {
					decimalResult = -Math.abs(decimalResult);
					signPrefix = '-';
					if (deltaDirection === 1) {
						integerResult = Math.abs(integerResult);
					}
				}

				if (decimalResult < 0) {
					if (integerResult > 0) {
						integerResult = integerResult - 1;
						decimalResult = Math.pow(10, this.inputWidget.options.decimalPlaces) + decimalResult;
					}
					else {
						integerResult = Math.abs(num.integer);
						decimalResult = Math.abs(decimalResult);
						signPrefix = '-';
					}
				}
			}

			integerString = integerResult.toString();
			decimalString = decimalResult.toString();

			if (parseInt(this.inputWidget.options.decimalPlaces) > 0
				&& decimalString.length > parseInt(this.inputWidget.options.decimalPlaces)) {
				integerResult++;
				decimalString = decimalString.substring(1);
				integerString = integerResult.toString();
			}

			//fill leading zeros to decimal part.
			while (decimalString.length < this.inputWidget.options.decimalPlaces) {
				decimalString = '0' + decimalString;
			}

			if (integerString.indexOf('-') !== -1) {
				signPrefix = '';
			}

			if (parseInt(integerString) === 0 && parseInt(decimalString) === 0) {
				signPrefix = '';
			}
			if (nullValue) {
				if (delta >= 0) {
					this._stringFormat._currentValueInString = this.inputWidget.options.minValue.toString();
				}
				else {
					this._stringFormat._currentValueInString = this.inputWidget.options.maxValue.toString();
				}
			}
			else {
				this._stringFormat._currentValueInString = signPrefix + integerString + '.' + decimalString;
			}

		}
		spinUp(val, nullValue: boolean) {
			if (val === null || val === undefined) {
				val = 1;
			}
			this._doSpin(val, nullValue);
		}
		spinDown(val, nullValue: boolean) {
			if (val === null || val === undefined) {
				val = 1;
			}
			this._doSpin(-val, nullValue);
		}

		spinEnumPart(position: number, rh: wijInputResult, val: number, isIncrease: boolean) {
			if (!rh) {
				rh = new wijInputResult();
			}
			val = parseInt(val.toString());
			var prefixBeforeSpin = this.inputWidget._getPrefix();
			var dpPosition = this._stringFormat._currentText.indexOf(this._getCulture().numberFormat['.']);
			var valueStringWithoutMinus = this._stringFormat._currentValueInString.replace('-', '');
			var dpPositionInValueString = valueStringWithoutMinus.indexOf('.');
			var hasDecimalPoint = true;
			if (dpPosition === -1) {
				hasDecimalPoint = false;
				dpPosition = this._stringFormat._currentText.length;
			}
			if (dpPositionInValueString === 0) {    //".000"
				dpPositionInValueString = 1;
			}
			else if (dpPositionInValueString === -1) {
				dpPositionInValueString = dpPosition;
			}

			var scope = Math.pow(10, this.inputWidget.options.decimalPlaces);
			if (rh.testPosition > dpPosition && hasDecimalPoint) {
				var fullVal = val;
				val = val / scope;
				if (Math.abs(val) >= 1) {
					isIncrease ?
					this.spinUp(parseInt(<any>val), this._nullvalue) :
					this.spinDown(parseInt(<any>val), this._nullvalue);
					val = (fullVal - parseInt(<any>val) * scope) / scope; // prevent float precision problem.
				}
			}

			isIncrease ? this.spinUp(val, this._nullvalue) : this.spinDown(val, this._nullvalue);

			this.setNullValue(false);
			var allowWrap = this.inputWidget.options.allowSpinLoop;
			this.checkAndRepairBounds(true, false, allowWrap);

			var valueStringWithoutMinusAfterSpin = this._stringFormat._currentValueInString.replace('-', '');
			var dpPositionInValueStringAfterSpin = valueStringWithoutMinusAfterSpin.indexOf('.');
			if (dpPositionInValueStringAfterSpin === -1) {
				hasDecimalPoint = false;
				dpPositionInValueStringAfterSpin = valueStringWithoutMinusAfterSpin.length;
			}
			var prefixAfterSpin = this.inputWidget._getPrefix();
			if (dpPositionInValueStringAfterSpin !== dpPositionInValueString && hasDecimalPoint) {
				rh.testPosition = rh.testPosition - prefixBeforeSpin.length + prefixAfterSpin.length + dpPositionInValueStringAfterSpin - dpPositionInValueString;
			}
			else {
				rh.testPosition = rh.testPosition - prefixBeforeSpin.length + prefixAfterSpin.length;
			}

		}

		getValue() {
			return this._stringFormat.getJSFloatValue();
		}

		setValue(val) {
			try {
				//if val is null, show null value instead of 0
				if (val === null) {
					this.setNullValue(true);
					val = this.inputWidget._initialValue;
				}
				else {
					this.setNullValue(false);
				}
				this._stringFormat.setValueFromJSFloat(val, this.inputWidget._getCulture(),
					this.inputWidget._getRealPositivePrefix(), this.inputWidget._getRealPositiveSuffix(),
					this.inputWidget._getRealNegativePrefix(), this.inputWidget._getRealNegativeSuffix(), this.inputWidget.options);
				this.checkAndRepairBounds(true, false);
				return true;
			}
			catch (e) {
				return false;
			}
		}

		setNullValue(value: boolean) {
			this._nullvalue = value;
		}

		//updateStringFormat() {
		//    var t = '0';
		//    if (typeof (this._stringFormat) !== 'undefined') {
		//        t = this._stringFormat._currentValueInString;
		//    }
		//    this._stringFormat = new wijNumberFormat(this.inputWidget);
		//    this._stringFormat._currentValueInString = t;
		//}
	}

}


/** @ignore */
interface JQuery {
	wijinputnumber: JQueryWidgetFunction;
}