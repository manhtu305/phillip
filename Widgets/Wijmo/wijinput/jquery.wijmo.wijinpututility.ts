/// <reference path="../external/declarations/globalize.d.ts"/>
/// <reference path="jquery.wijmo.wijstringinfo.ts"/>
/// <reference path="../Base/jquery.wijmo.widget.ts"/>
/// <reference path="jquery.wijmo.wijcharex.ts" />
/// <reference path="jquery.wijmo.wijinputcore.ts"/>

module wijmo.input {


	/** @ignore */
	export interface ICutCopyPasteEvent extends Event {
		clipboardData: DataTransfer;
	}

	/** @ignore */
	export class Utility {

		static CheckElement: HTMLElement;
		static CurrentActiveControlId: string;
		static IsTouchMouseDown: boolean;
		static GrapeCityTimeout: boolean;
		static CutCopyPasteEventObject: ICutCopyPasteEvent;
		static LoadComplete: boolean;
		static ShouldFireOnSelectStart: boolean;
		static SavedText: string;
		static SpinTimer: number;
		static NextID: string;
		static FuncKeysPressed: any;
		static ShouldInvokeKeyPress: boolean;

		static IdCounter = 0;
		static EditFieldSuffix = "_EidtField";

		static MaskValChar = "\ufeff";
		static Hold = "g-C2";

		static DragStartElementID: string;
		static InnerSelect: boolean;
		static TouchStartTime: Date;
		static TouchEndTime: Date;
		static TouchStartEvt: Event;
		static IsOnActivate: boolean;
		static IsOnActivateControlID: string;
		static IsOnFocus: boolean;
		static FocusToBorder: boolean;
		static HasGetFocus: boolean;

		//static FireEvent(oControl, eventHandler, eArgs: any, eventName: string) {

		//    oControl.owner._trigger(eventName, null, eArgs);
		//}

		static FireEvent(oControl, eventHandler: Function, eArgs: any, eventName: string) {
			if (!eventHandler) {
				return false;
			}
			try {
				// DaryLuo 2012/10/29 fix bug 837 in IM Web 7.0.
				eventHandler.call(window, oControl, eArgs);
			}
			catch (ex) {
				return false;
			}
			return true;
		}

		static FindIMControl(id): any {
		}

		static ArrayIndexOf(array, obj) {
			if (array.indeOf) {
				return array.indexOf(obj);
			}
			else {
				for (var i = 0; i < array.length; i++) {
					if (array[i] === obj) {
						return i;
					}
				}
			}
			return -1;
		}


		static AttachEvent(element, type, handler, useCapture?) {
			if (element !== null && element !== undefined) {
				if (element.addEventListener) {
					element.addEventListener(type, handler, useCapture);
				} else if (element.attachEvent) {
					element.attachEvent("on" + type, handler);
				}
			}
		}

		static _isAttachedFocusEventOfDocument: boolean;
		static AttachFocusEventOfDocument() {
			//This guaranteen attaching FocusControlUpdate to document one times only (Fixing memory leak)
			if (Utility._isAttachedFocusEventOfDocument) return;
			Utility._isAttachedFocusEventOfDocument = true;

			if (CoreUtility.IsIE8OrBelow()) {
				Utility.AttachEvent(document, "beforeactivate", function (evt) {
					Utility.FocusControlUpdate(evt);
				}, true);
			} else {
				Utility.AttachEvent(document, "focus", function (evt) {
					Utility.FocusControlUpdate(evt);
				}, true);
			}            
		}

		static PreviousFocusControl;
		static FocusControl;
		static FocusControlUpdate(evt) {
			Utility.PreviousFocusControl = Utility.FocusControl;
			Utility.FocusControl = evt.target || evt.srcElement;
		}

		static ToString(value, length, ch?, position?) {
			var val = value + "";

			//It is same as String.PadLeft(int, char) in C#.
			if (ch != null) {
				while (val.length < length) {
					if (position) {
						val = val + ch;
					}
					else {
						val = ch + val;
					}
				}

				return val;
			}

			//add the value length times.
			while (val.length < length) {
				val += value + "";
			}

			return val;
		}

		//Added by Jeff Wu. For CursorPosition Test
		static GetSelectionEndPosition(obj) {
			// Add comments by Yang
			// For test firefox
			if (!CoreUtility.IsIE() || CoreUtility.IsIE11OrLater()) {
				//Commented by Kevin, Feb 17, 2009
				//bug#1890
				//return obj.selectionEnd;
				var endS = 0;
				endS = obj.selectionEnd;
				if (obj.value) {
					var text = obj.value.substring(0, endS);
					endS = text.GetLength();
				}

				//Commented by Kevin, Feb 18, 2009
				//bug#1894
				var startS = 0;
				startS = obj.selectionStart;
				if (obj.value) {
					var text = obj.value.substring(0, startS);
					startS = text.GetLength();
				}

				if (startS > endS) {
					endS = startS;
				}
				//end by Kevin

				return endS;
				//end by Kevin
			}
			// End by Yang 
			return Utility.GetSelectionStartPosition(obj) + document.selection.createRange().text.GetLength();
		}

		static GetSelectionStartPosition(obj) {
			if (obj.selectionStart != null) {
				var startS = 0;
				startS = obj.selectionStart;
				if (obj.value) {
					var text = obj.value.substring(0, startS);
					startS = text.GetLength();
				}

				var endS = 0;
				endS = obj.selectionEnd;
				if (obj.value) {
					var text = obj.value.substring(0, endS);
					endS = text.GetLength();
				}

				if (endS < startS) {
					startS = endS;
				}

				return startS;
			}
			try {
				var rng = obj.createTextRange();
				var sng = document.selection.createRange();
				var length = obj.value.GetLength();
				var start = 0;
				var end = length;
				var i = 0;

				while (start < end) {
					i = Math.floor((start + end) / 2);
					rng = obj.createTextRange();

					var s = i;
					var text = obj.value.Substring(0, i);
					while (1) {
						var index = text.IndexOf("\r\n");
						if (index != -1) {
							s--;
							text = text.Substring(index + 2, text.GetLength());
						}
						else {
							break;
						}
					}

					var gap = i - s;
					s = i - gap;

					rng.moveStart("character", s);

					if (rng.offsetTop > sng.offsetTop) {
						end = i;
					}
					else if (rng.offsetTop < sng.offsetTop) {
						if (start == i) {
							return end;
						}
						start = i;
					}
					else if (rng.offsetLeft > sng.offsetLeft) {
						end = i;
					}
					else if (rng.offsetLeft < sng.offsetLeft) {
						if (start == i) {
							return end;
						}
						start = i;
					}
					else {
						if (obj.value.Substring(i - 1, i) == "\r") {
							i++;
						}
						return i;
					}
				}
				return length;
			}
			catch (e) {
				return 0;
			}
		}

		static GetCursorPosition(obj, isPropertyChange?) {
			if (obj == null) {
				return -1;
			}

			// Add comments by Yang 
			// For test firefox 
			if (!CoreUtility.IsIE() || CoreUtility.IsIE11OrLater()) {
				//Commented by Kevin, Feb 17, 2009
				//bug#1890
				//return obj.selectionStart;
				var startS = 0;
				startS = obj.selectionStart;
				if (obj.value) {
					var text = obj.value.substring(0, startS);
					startS = text.GetLength();
				}

				return startS;
				//end by Kevin

			}
			// End by Yang 

			// Frank Liu fixed bug 629 at 2013/06/20.
			var caretPos = 0; // IE Support
			if (document.selection) {
				obj.focus();
				var sel = document.selection.createRange();
				sel.moveStart('character', -obj.value.length);
				caretPos = sel.text.length;
			}
			return (caretPos);


		}

		static GetPasteData(useClipboard) {
			// Add comments by Yang
			// For test firefox
			return Utility.GetDataFromClipboard(useClipboard);
		}

		static GetDataFromClipboard(useClipboard) {
			if (useClipboard == false) {
				return Utility.SavedText;
			}

			if (window.clipboardData) {
				return (window.clipboardData.getData('Text'));
			}
			else if (Utility.CutCopyPasteEventObject !== null) {
				if (Utility.CutCopyPasteEventObject.clipboardData !== undefined) {
					return Utility.CutCopyPasteEventObject.clipboardData.getData("text");
				}

			}
			//else if (window.netscape) {
			//    try {
			//        netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');
			//        var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);

			//        if (!clip) {
			//            return;
			//        }

			//        var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);

			//        if (!trans) {
			//            return;
			//        }

			//        trans.addDataFlavor('text/unicode');
			//        clip.getData(trans, clip.kGlobalClipboard);

			//        var str = new Object();
			//        var len = new Object();

			//        try {
			//            trans.getTransferData('text/unicode', str, len);
			//        }
			//        catch (error) {
			//            return null;
			//        }

			//        if (str) {
			//            if (Components.interfaces.nsISupportsWString) {
			//                str = str.value.QueryInterface(Components.interfaces.nsISupportsWString);
			//            }
			//            else if (Components.interfaces.nsISupportsString) {
			//                str = str.value.QueryInterface(Components.interfaces.nsISupportsString);
			//            }
			//            else {
			//                str = null;
			//            }
			//        }
			//        if (str) {
			//            return (str.data.substring(0, len.value / 2));
			//        }
			//    }
			//    catch (e) {
			//        window.status = "about:config signed.applets.codebase_principal_support true";
			//    }
			//}

			return null;
		}

		static ClearSelection(inputElement) {
			if (CoreUtility.IsIE() && !CoreUtility.IsIE11OrLater()) {
				if (document.selection.createRange().text != "") {
					document.selection.empty();
				}
			}
			else {
				if (Utility.GetSelectionText(inputElement) != "") {

					inputElement.selectionStart = inputElement.value.length;
					inputElement.selectionEnd = inputElement.selectionStart;
				}
			}
		}

		static GetSelectionText(obj) {
			if (obj.selectionStart != null) {
				return obj.value.substring(obj.selectionStart, obj.selectionEnd);
			}
			else if (document.selection != null) {
				return document.selection.createRange().text;
			}
			else if (document.activeElement !== null) {
				var obj: any = document.activeElement;
				//var start = Math.Min(document.activeElement.selectionStart,document.activeElement.selectionEnd);
				//var end = Math.Max(document.activeElement.selectionStart,document.activeElement.selectionEnd);
				var start = obj.selectionStart;
				var end = obj.selectionEnd;
				return obj.value.substring(start, end);
			}

			return "";
		}

		static FireSystemEvent(obj, eventName) {

			if (CoreUtility.IsIE()) {
				obj.fireEvent(eventName);
			}
			else {
				var evt = document.createEvent('HTMLEvents');

				//We must remove the eventName's first two characters "on". For example, 
				//we should remove the "onchange" to "change".
				evt.initEvent(eventName.substring(2, eventName.length), true, true);
				obj.dispatchEvent(evt);
			}

		}

		static GetTextBoxScrollWidth(inputElement, unuse?) {
			return CoreUtility.MeasureText(inputElement.value, inputElement).Width;
		}

		static CheckInt(value, min?, max?) {
			var intValue = parseInt(value);
			if (isNaN(intValue)) {
				throw "value is invalid";
			}
			if (intValue < min || intValue > max) {
				throw value + " is out of range, should between " + min + " and " + max;
			}
			return intValue;
		}


		static CheckFloat(value, min, max) {
			var intValue = parseFloat(value);
			if (isNaN(intValue)) {
				throw "value is invalid";
			}
			if (value < min || value > max) {
				throw value + " is out of range, should between " + min + " and " + max;
			}
			return intValue;
		}


		static CheckDate(value, min, max) {
			if (!(value instanceof Date)) {
				throw "Type is invalid";
			}
			if (isNaN(value)) {
				throw "Date is a invalid date";
			}

			if (max != undefined) {
				if (value < min || value > max) {
					throw value + " is out of range, should between " + min + " and " + max;
				}
			}

		}

		static CheckBool(boolValue) {
			if (typeof (boolValue) === "string") {
				if (boolValue.toLowerCase() === "true") {
					return true;
				}
			}
			return boolValue == true;
		}

		static CheckFunction(fun) {
			if (fun === undefined || fun === null) {
				return null;
			}

			if (typeof fun == "string") {
				fun = Utility.Trim(fun);
				if (fun.length == 0) {
					return null;
				}
				try {
					eval("fun =" + fun);
				}
				catch (e) {
				}
			}

			if (typeof fun == "function") {
				return fun;
			}
			else {
				throw "The value is not a valid function";
			}
		}

		static GetCheckElement() {
			if (Utility.CheckElement == null) {
				var div = document.createElement("div");
				Utility.CheckElement = div;
				div.style.display = "none";

			}
			return Utility.CheckElement;
		}

		static CheckString(str) {
			if (str == null) {
				str = "";
			}
			if (str === undefined || (typeof (str) != "string" && !(str instanceof String))) {
				throw str + " type is not a string.";
			}

			return str.toString();
		}

		static GetCSSLength(length) {
			var intValue = parseInt(length);
			if (isNaN(intValue)) {
				return 0;
			}
			return intValue;
		}

		static CheckCSSLength(length) {
			length = Utility.CheckInt(length, 0, Math.pow(2, 31));

			if (parseInt(length) == length) {
				length += "px";
			}
			Utility.GetCheckElement().style.width = "";
			Utility.GetCheckElement().style.width = length;
			return Utility.GetCheckElement().style.width;
		}

		static CheckFontSizeValue(length) {
			if (parseInt(length) == length) {
				length += "px";
			}
			Utility.GetCheckElement().style.fontSize = "";
			Utility.GetCheckElement().style.fontSize = length;
			return Utility.GetCheckElement().style.fontSize;
		}

		static CheckColor(color) {
			Utility.GetCheckElement().style.backgroundColor = "";
			Utility.GetCheckElement().style.backgroundColor = color;
			return Utility.GetCheckElement().style.backgroundColor;
		}

		static CheckImageUrl(url) {
			return Utility.CheckString(url);
		}

		static GetCssImageUrl(url) {
			if (url.startWith("url(\"")) {  //IE
				url = url.substring(5, url.length - 2);
			}
			else if (url.startWith("url(")) {   //Chrome, FF
				url = url.substring(4, url.length - 1);
			}
			return url;
		}

		static CheckCssImageUrl(url) {
			if (url.length > 0 && !url.startWith("url(")) {
				url = "url(" + url + ")";
			}
			Utility.GetCheckElement().style.backgroundImage = "";
			Utility.GetCheckElement().style.backgroundImage = url;
			return Utility.GetCheckElement().style.backgroundImage;
		}

		static CheckEnum(type, value) {
			for (var item in type) {
				if (type[item] == value || type[item] == value.toLowerCase()) {
					return type[item];
				}
			}
			throw "Enum value is invalid";
		}

		static CheckChar(value) {
			value = Utility.CheckString(value);
			// Frank Liu fixed bug 678 at 2013/06/14.
			if (value.length !== 1) {   // Frank Liu fixed bug 865 at 2013/06/26.
				throw "Invalid value";
				//value = value.Substring(0, 1);
			}
			return value;
		}


		static GetMultipleStringEnum(value) {
			var valueList = value.split(" ");
			valueList.sort();
			return valueList.join(",");
		}

		static CheckMultipleStringEnum(type, value) {
			var valueList = value.split(",");
			var result = [];
			for (var i = 0; i < valueList.length; i++) {
				result.push(Utility.CheckEnum(type, Utility.Trim(valueList[i])));
			}
			return result.join(" ");
		}

		static EncodingToHTML(text) {

			if (text.IndexOf("&") != -1) {
				var tempDispText = text;
				var tempText = "";
				while (tempDispText.IndexOf("&") != -1) {
					var findPosition = tempDispText.IndexOf("&");
					tempDispText = tempDispText.replace("&", "&amp;");
					tempText += tempDispText.Substring(0, findPosition + 5);
					tempDispText = tempDispText.Substring(findPosition + 5, tempDispText.GetLength());
				}
				if (tempDispText.IndexOf("&") == -1 && tempDispText != "") {
					tempText += tempDispText;
				}
				text = tempText;
			}

			//Modified by shenyuan at 2006-02-20 for bug #5308.
			while (text.IndexOf(' ') != -1) {
				text = text.replace(" ", "&nbsp;");
			}

			while (text.IndexOf("<") != -1) {
				text = text.replace("<", "&lt;");
			}

			while (text.IndexOf(">") != -1) {
				text = text.replace(">", "&gt;");
			}
			return text;
		}

		static DecodingFromHTML(text) {

			//Modified by shenyuan at 2006-02-23 for bug #5348.
			while (text.IndexOf("&nbsp;") != -1) {
				text = text.replace("&nbsp;", " ");
			}

			//Modified by shenyuan at 2006-02-08 for bug #5180.
			while (text.IndexOf("&lt;") != -1) {
				text = text.replace("&lt;", "<");
			}

			while (text.IndexOf("&gt;") != -1) {
				text = text.replace("&gt;", ">");
			}

			//Modified by shenyuan at 2006-02-10 for bug #5206.
			if (text.IndexOf("&amp;") != -1) {
				var tmpText = text;
				var tmpResult = "";
				while (tmpText.IndexOf("&amp;") != -1) {
					var temPos = tmpText.IndexOf("&amp;");
					tmpText = tmpText.replace("&amp;", "&");
					tmpResult += tmpText.Substring(0, temPos + 1);
					tmpText = tmpText.Substring(temPos + 1, tmpText.GetLength());
				}
				if (tmpText.IndexOf("&amp;") == -1 && tmpText != "") {
					tmpResult += tmpText;
				}
				text = tmpResult;
			}
			return text;
		}

		static IsStandCompliantModeOn() {
			//Commented by Kevin, Nov 11, 2008
			//fix bug#10414
			//return document.compatMode == "CSS1Compat";
			if (!CoreUtility.IsIE()) {
				return document.compatMode == "CSS1Compat" || document.compatMode == "BackCompat";
			}
			return document.compatMode == "CSS1Compat";
			//end by Kevin

		}

		static GetPageZoomRate() {
			// add by Sean Huang at 2008.11.13, for bug 10129, 10368 -->
			if (CoreUtility.IsIE8()) {
				return screen.deviceXDPI / screen.logicalXDPI;
			}
			// add by Sean Huang at 2008.11.13, for bug 10129, 10368 -->

			var normalPosition = document.getElementById("gcsh_standard_control_for_get_normal_position");
			if (typeof (normalPosition) != "undefined" && normalPosition != null) {
				return document.getElementById("gcsh_standard_control_for_get_normal_position").offsetLeft / 100;
			}
			else {
				var div = document.createElement("div");
				document.body.appendChild(div);
				div.id = "gcsh_standard_control_for_get_normal_position";
				div.style.visibility = "hidden";
				div.style.left = "100px";
				div.style.top = "1px";
				div.style.width = "1px";
				div.style.height = "1px";
				div.style.position = "absolute";

				return document.getElementById("gcsh_standard_control_for_get_normal_position").offsetLeft / 100;
				// return 1;
			}
		}

		static GetElementPosition(id) {
			// Frank Liu fixed bug 612 at 2013/06/09.
			// HelenLiu 2013/07/02 fix bug 742 in IM HTML5.
			if (CoreUtility.IsIE() || CoreUtility.chrome || CoreUtility.safari) {
				// change by Sean Huang at 2009.04.10, for bug 2125 -->
				//return Utility.GetElementPositionForIE(id);
				//modified by sj for bug 12220
				//if (Utility.engine == 8)
				if (CoreUtility.IsIE8OrLater())
				//end by sj
				{
					var pos1 = Utility.GetElementPositionForIE8(id);
					// DaryLuo 2012.09/10 fix bug 561 in IMWeb 7.0.
					if (pos1.Left == 0 && pos1.Top == 0) {
						var pos2 = Utility.GetElementPositionForFireFox(id);
						return pos2;
					}
					else {
						return pos1;
					}
				}
				else {
					var posIE7 = Utility.GetElementPositionForIE(id);
					//  var posIE8 = Utility.GetElementPositionForIE8(id);
					//  var posFF = Utility.GetElementPositionForFireFox(id);
					// DaryLuo 2012.09/10 fix bug 561 in IMWeb 7.0.
					//  var left = Utility.ChooseMiddle(posIE7.Left, posIE8.Left, posFF.Left);
					//  var top = Utility.ChooseMiddle(posIE7.Top, posIE8.Top, posFF.Top);
					return { Left: posIE7.Left, Top: posIE7.Top };
				}
				// end of Sean Huang <--
			}

			return Utility.GetElementPositionForFireFox(id);
		}

		static GetElementPositionForIE8(id) {
			var element = id;
			if (typeof id == "string") {
				element = document.getElementById(id);
			}

			var top: number = 0;
			var left: number = 0;

			if (element == null || element.self || element.nodeType === 9) {
				return { Left: left, Top: top };
			}

			var clientRect = element.getBoundingClientRect();
			if (!clientRect) {
				return { Left: left, Top: top };
			}
			var documentElement = element.ownerDocument.documentElement;
			left = clientRect.left + documentElement.scrollLeft;
			top = clientRect.top + documentElement.scrollTop;

			try {
				var f = element.ownerDocument.parentWindow.frameElement || null;
				if (f) {
					var offset = (f.frameBorder === "0" || f.frameBorder === "no") ? 2 : 0;
					left += offset;
					top += offset;
				}
			}
			catch (ex) {
			}

			return { Left: left, Top: top };
		}

		static GetElementPositionForIE(id) {
			var oElement = id;
			if (typeof id == "string") {
				oElement = document.getElementById(id);
			}
			// For bug 3696.
			var top = 0;//document.body.scrollTop;
			var left = 0;//document.body.scrollLeft;

			if (oElement == null) {
				return { Left: left, Top: top };
			}

			if (oElement.offsetParent) {
				while (oElement.offsetParent != null) {
					var parent = oElement.offsetParent;
					var parentTagName = parent.tagName.toLowerCase();

					if (parentTagName != "table" &&
						parentTagName != "body" &&
						parentTagName != "html" &&
						// Add comments by Yang at 14:40 Dec. 12th 2008
						// For fix the bug 1187(TP)
						//			    parentTagName != "div" &&
						// End by Yang 
						parent.clientTop &&
						parent.clientLeft) {
						left += parent.clientLeft;
						top += parent.clientTop;
					}

					// add by Sean Huang at 2008.11.12, for bug 10064
					// change by Sean Huang at 2008.11.13, for bug 10445 -->
					//if (Utility.IsIE7() && parent.style.position.toLowerCase() == "relative")
					if (CoreUtility.IsIE7() && parent.style.position.toLowerCase() == "relative")
					// end of Sean Huang <--
					{
						left += oElement.offsetLeft - oElement.scrollLeft;
						top += oElement.offsetTop;

						// add by Sean Huang at 2009.01.07, for bug 856 -->
						var zoom = Utility.GetPageZoomRate();
						if (zoom == 1)
						// end of Sean Huang <--
						{
							var offset = oElement.offsetTop;
							for (var i = 0; i < parent.children.length; i++) {
								var o = parent.children[i];
								if (o == oElement) {
									break;
								}
								else if (o.offsetTop) {
									offset = o.offsetTop;
									break;
								}
							}
							top -= offset;
						}
					}
					// end of Sean Huang <--
					// change by Sean Huang at 2008.11.13, for bug 10445 -->
					//else if (Utility.IsStandCompliantModeOn() && Utility.IsIE7()
					else if (Utility.IsStandCompliantModeOn() && CoreUtility.IsIE7()
					// end of Sean Huang <--
						&& (oElement.style.position.toLowerCase() == "absolute"
						|| oElement.style.position.toLowerCase() == "relative")) {
						// change by Sean Huang at 2009.01.07, for bug 856 -->
						// [original] -->
						//  // change by Sean Huang at 2008.12.18, for bug 896 -->
						//  //top  += (oElement.offsetTop - oElement.scrollTop) / Utility.GetPageZoomRate();
						//  //left += (oElement.offsetLeft - oElement.scrollLeft) / Utility.GetPageZoomRate();
						//  top  += (oElement.offsetTop - oElement.scrollTop);
						//  left += (oElement.offsetLeft - oElement.scrollLeft);
						//  // end of Sean Huang <--
						// <-- [original]
						var zoom = Utility.GetPageZoomRate();
						top += (oElement.offsetTop - oElement.scrollTop) / zoom;
						left += (oElement.offsetLeft - oElement.scrollLeft) / zoom;
						// end of Sean Huang, for bug 856 <--
					}
					else {
						//Add by Jiang at Dec. 10 2008
						//For fixed bug773
						if ((oElement.tagName.toLowerCase() == "input" && oElement.type.toLowerCase() == "text")
							|| oElement.tagName.toLowerCase() == "textarea") {
							top += oElement.offsetTop;
							left += oElement.offsetLeft;
						}
						else {
							top += oElement.offsetTop - oElement.scrollTop;
							left += oElement.offsetLeft - oElement.scrollLeft;
						}
						//End by Jiang Changcheng
					}

					oElement = parent;
					//end by Ryan Wu.
				}
			}
			else if (oElement.left && oElement.top) {
				left += oElement.left;
				top += oElement.top;
			}
			else {
				if (oElement.x) {
					left += oElement.x;
				}
				if (oElement.y) {
					top += oElement.y;
				}
			}

			//Add by Ryan Wu at 11:13, Nov 2 2005. For in VS2005, body has also an offset value. 
			if (oElement.style.position.toLowerCase() != "relative"
				&& oElement.style.position.toLowerCase() != "absolute"
				&& oElement.tagName.toLowerCase() == "body" && Utility.IsStandCompliantModeOn()) {
				//Add comments by Ryan Wu at 9:54 Nov. 15 2006. 
				//Fix bug#6695.
				//	    top  += oElement.offsetTop;
				//		left += oElement.offsetLeft;
				// change by Sean Huang at 2008.11.13, for bug 10445 -->
				//if (!Utility.IsIE7())
				if (!CoreUtility.IsIE7())
				// end of Sean Huang <--
				{
					top += oElement.offsetTop;
					left += oElement.offsetLeft;
				}
				else {
					// Add comments by Yang at 13:23 July 16th 2008
					// For fix the bug 9755  
					//            top  += parseInt(oElement.currentStyle.marginTop);
					//		    left += parseInt(oElement.currentStyle.marginLeft);
					var tempTop = parseInt(oElement.currentStyle.marginTop);
					var tempLeft = parseInt(oElement.currentStyle.marginLeft);
					if (isNaN(tempTop)) {
						tempTop = 0;
					}
					if (isNaN(tempLeft)) {
						tempLeft = 0;
					}
					top += tempTop;
					left += tempLeft;
					// End by Yang
				}
				//end by Ryan Wu.
			}

			return { Left: left, Top: top };
		}

		static GetElementPositionForFireFox(id) {

			var oElement = id;
			if (typeof id == "string") {
				oElement = document.getElementById(id);
			}
			// For bug 3696.
			var top = 0;//document.body.scrollTop;
			var left = 0;//document.body.scrollLeft;
			var scrollLeft = 0;
			var scrollTop = 0;

			if (oElement == null) {
				return { Left: left, Top: top };
			}

			//Gets the offsetTop and offsetLeft.
			if (oElement.offsetParent) {
				while (oElement.offsetParent != null) {
					var parentTagName = oElement.offsetParent.tagName.toLowerCase();

					if (parentTagName != "table" &&
						parentTagName != "body" &&
						parentTagName != "html" &&
						parentTagName != "div" &&
						oElement.offsetParent.clientTop &&
						oElement.offsetParent.clientLeft) {
						left += oElement.offsetParent.clientLeft;
						top += oElement.offsetParent.clientTop;
					}

					top += oElement.offsetTop;
					left += oElement.offsetLeft;

					oElement = oElement.offsetParent;
				}
			}
			else if (oElement.left && oElement.top) {
				left += oElement.left;
				top += oElement.top;
			}
			else {
				if (oElement.x) {
					left += oElement.x;
				}
				if (oElement.y) {
					top += oElement.y;
				}
			}

			//Gets the scrollTop and scrollLeft.
			oElement = id;
			if (typeof id === "string") {
				oElement = document.getElementById(id);
			}


			if (oElement.parentElement) {
				while (oElement.parentElement != null && oElement.tagName.toLowerCase() != "html") {
					scrollTop += oElement.scrollTop;
					scrollLeft += oElement.scrollLeft;

					oElement = oElement.parentElement;
				}
			}

			top -= scrollTop;
			left -= scrollLeft;

			//Add by Ryan Wu at 11:13, Nov 2 2005. For in VS2005, body has also an offset value. 
			if (oElement.style.position.toLowerCase() != "relative"
				&& oElement.style.position.toLowerCase() != "absolute"
				&& oElement.tagName.toLowerCase() == "body" && Utility.IsStandCompliantModeOn()) {
				top += oElement.offsetTop;
				left += oElement.offsetLeft;
			}

			return { Left: left, Top: top };
		}

		static GetOSDefaultFontFamily() {
			switch (CoreUtility.GetClientOS().toLowerCase()) {
				case "winxp":
					return "MS UI Gothic";
				case "vista":
					return "メイリオ";
				case "win7":
					return "メイリオ";
				case "win8":
					return "Meiryo UI";
				case "win2003":
					return "MS UI Gothic";
				case "win2000":
					return "MS UI Gothic";
			}
			return "MS UI Gothic";
		}

		static IsTouchSupport() {
			return window.navigator.userAgent.toLowerCase().indexOf("touch") != -1;
		}

		static DisabledHoldVisual(element) {
			if (element !== null && element !== undefined && Utility.IsTouchSupport() && CoreUtility.IsIE10OrLater()) {
				element.addEventListener("MSHoldVisual", function (e) {
					e.preventDefault();
				}, false);
				element.addEventListener("MSGestureHold", function (e) {
					e.preventDefault();
				}, false);
				// Disables visual
				element.addEventListener("contextmenu", function (e) {
					e.preventDefault();
				}, false);
			}
		}



		static IsFocusFromIMControl(id, evt) {
			try {
				//return event.fromElement.className.IndexOf(Utility.DefaultControlStyle) != -1;
				var src = evt.fromElement;
				while (src != null) {
					// change by Sean Huang at 2008.08.14, for bug 614 and 644 (ttp)-->
					//if (src.id.Substring(0, id.length) == id)
					//modified by sj for bug 2149
					//if (src.id == id ||  src.id == id + this.Hold + "_DropDownObj" || src.id == id + "DropDown_Container" || src.id == id+ "_ContextMenu")
					if (src.id == id || src.id == id + this.Hold + "_DropDownObj" || src.id == id + "_DropDown_Container" || src.id == id + "_ContextMenu" || src.id == id + "_HistoryList" || src.id == id + "_EditField" || src.id == id + "_BorderContainer" || src.id == id + "_DropDown_EditField")
					//end by sj
					// end of Sean huang <--
					{
						return true;
					}

					//Add comments by Ryan Wu at 11:28 Mar. 15 2007.
					//For test FireFox.
					//src = src.parentElement;
					src = src.parentNode;
					//end by Ryan Wu.
				}
				return false;
			}
			catch (e) {
				return false;
			}
		}

		static IsFocusToIMControl(id, evt) {
			try {
				//	return event.toElement.className.IndexOf(Utility.DefaultControlStyle) != -1;
				var src = evt.toElement;
				//var src = event.toElement;
				//evt.target;
				while (src != null) {
					//Add comments by Ryan Wu at 17:13 Aug. 22 2007.
					//For fix the bug8990.
					//if (src.id && src.id.substring(0, id.length) == id)

					//add by chris for 12215 (bugzilla) 2010/12/17 16:30
					//			// change by Sean Huang at 2008.08.14, for bug 614 and 644 (ttp)-->
					//			//if (src.id && src.id == id)
					//			//modified by sj for bug 2149
					//			//if (src.id == id || src.id == id + this.Hold + "_DropDownObj" || src.id == id + "DropDown_Container" || src.id == id+ "_ContextMenu")
					//			if (src.id == id || src.id == id + this.Hold + "_DropDownObj" || src.id == id + "DropDown_Container" || src.id == id + "_ContextMenu" || src.id == id + "_HistoryList")
					//			//end by sj
					//			// end of Sean Huang <--
					if (src.id == id || src.id == id + this.Hold + "_DropDownObj" || src.id == id + "_DropDown_Container" || src.id == id + "_ContextMenu" || src.id == id + "_HistoryList" || src.id == id + this.Hold + "_DropDownObj_PopupMonth" || src.id == id + "_EditField" || src.id == id + "_BorderContainer" || src.id == id + "_DropDown_EditField")
					//end by chris for 12215 (bugzilla) 2010/12/17 16:30
					{
						return true;
					}
					//end by Ryan Wu.

					//Add comments by Ryan Wu at 11:28 Mar. 15 2007.
					//For test FireFox.
					//src = src.parentElement;
					src = src.parentNode;
					//end by Ryan Wu.
				}
				return false;
			}
			catch (e) {
				return false;
			}
		}

		static Trim(value) {
			if (value == "") {
				return "";
			}

			var beginIndex = 0;
			var endIndex = 0;
			for (var i = 0; i < value.length; i++) {
				if (value.CharAt(i) != " " && value.CharAt(i) != "　") {
					beginIndex = i;
					break;
				}
			}

			for (var i = value.length - 1; i >= 0; i--) {
				if (value.CharAt(i) != " " && value.CharAt(i) != "　") {
					endIndex = i + 1;
					break;
				}
			}

			try {
				var s = value.Substring(beginIndex, endIndex);
				return s;
			}
			catch (e) {
				return value;
			}


		}

		static GetTouchPath(beginY, endY) {
			if (beginY === -1 || endY === -1) {
				return "Error";
			}

			if (beginY > endY) {
				return beginY - endY > 20 ? "ToTop" : "NotMove";
			} else if (beginY < endY) {
				return endY - beginY > 20 ? "ToBottom" : "NotMove";
			}

			return "NotMove";
		}

		static IsJapan() {
			if (navigator.userLanguage) {
				return navigator.userLanguage.indexOf('ja') != -1;
			} else if (navigator["language"]) {
				return navigator["language"].indexOf('ja') != -1;
			}

			return false;
		}

		static CreateStyleElement(id) {
			var style: any = document.createElement("style");
			style.id = id;
			//add commnets by Jason.Zhou at 14:23 November 26 2007
			//for style.type="text/css" is used in css file used by linking file type, this sytle is only in dom tree.

			style.type = "text/css";
			//end by Jason.Zhou
			return style;
		}

		static CreateClassStyle(className) {
			return Utility.CreateSelectorStyle("." + className);
		}


		static CreateSelectorStyle(selectorName) {
			var tableStyle: any = document.getElementById("tableStyle");
			if (tableStyle == null) {
				//tableStyle = GrapeCity.IM.Utility.CreateStyleElement("tableStyle");
				tableStyle = this.CreateStyleElement("tableStyle");
				document.body.appendChild(tableStyle);
			}

			var sheet = tableStyle.sheet || tableStyle.styleSheet;
			var rules = sheet.cssRules || sheet.rules;
			if (sheet.insertRule) {
				sheet.insertRule(selectorName + "{ }", rules.length);
			}
			else {
				sheet.addRule(selectorName, "{ }", rules.length);
			}

			return rules.item(rules.length - 1);
		}




		static SetSelection(element, start: number, end: number, multiLine?: boolean) {
			$(element).wijtextselection(Math.min(start, end), Math.max(start, end));
		}

		static PreventDefault(evt) {
			if (evt.preventDefault) {
				evt.preventDefault();
			} else {
				evt.returnValue = false;
			}
		}

		static CancelBubble(evt) {
			if (evt.cancelBubble !== undefined) {
				evt.cancelBubble = true;
			} else {
				evt.stopPropagation();
			}
		}

		static DragDrop(obj) {
			try {
				obj.dragDrop(true);
			}
			catch (e) {
			}
		}


		static GetMouseButton(evt) {
			var mouseButton = MouseButton.Default;
			var leftKey = CoreUtility.IsIE8OrBelow() ? 1 : 0;
			if (evt.button == leftKey) {
				mouseButton = MouseButton.Left;
			}
			else if (evt.button == 1) {
				mouseButton = MouseButton.Middle;
			}
			else if (evt.button == 2) {
				mouseButton = MouseButton.Right;
			}
			return mouseButton;
		}

		/**
		* Gets the value after mouse wheel.
		* @param value - the initial value before mousewheel.
		* @returns Returns the value after mouse wheel.
		*/
		static GetMouseWheelValue(value, evt) {
			//Add comments by Ryan Wu at 9:50 Aug. 13 2007.
			//For Firefox doesn't support the event.wheelDelta to get the mouse wheel value.
			//I don't know why the event.detail is also 3 or -3, so we must divide 3.
			if (CoreUtility.IsFireFox4OrLater()) {
				return -evt.detail / 3;
			}
			//end by Ryan Wu.

			if (evt.wheelDelta >= 120) {
				value++;
			}
			else if (evt.wheelDelta <= -120) {
				value--;
			}

			return value;
		}
		static SetCopy(text, useClipboard) {
			// Add comments by Yang
			// For test firefox	
			var selText;
			try {
				// add by Sean Huang at 2009.04.29, for bug 2209 -->
				if (text == null || text == "")
				// end of Sean Huang <--
				{
					if (document.selection) {
						selText = document.selection.createRange().text;
					}
					if (selText == "") {
						return;
					}
				}
			}
			catch (e) {
			}
			;

			if (text != null) {
				selText = text;
			}

			Utility.CopyDataToClipBoard(selText, useClipboard);
		}

		static CopyDataToClipBoard(copytext, useClipboard) {
			if (useClipboard == false) {
				Utility.SavedText = copytext;
				return;
			}

			if (window.clipboardData) {
				// change by Sean Huang at 2009.01.13, for bug 1582 -->
				//window.clipboardData.setData("Text", copytext);
				// change by Sean Huang at 2009.02.19, for bug 1903 -->
				//setTimeout('window.clipboardData.setData("Text", "' + copytext + '");', 0);
				// change by Sean Huang at 2009.05.26, for sometimes throw exception in auto test ==--> 
				//setTimeout(function () {window.clipboardData.setData("Text", copytext);}, 0);
				setTimeout(function () {
					try {
						window.clipboardData.setData("Text", copytext);
					} catch (ex) {
					}
				}, 0);
				// end of Sean Huang, for auto test <--==
				// end of Sean Huang <--
				// end of Sean Huang <--
			}
			else if (Utility.CutCopyPasteEventObject !== null) {
				if (Utility.CutCopyPasteEventObject.clipboardData !== undefined) {
					Utility.CutCopyPasteEventObject.clipboardData.setData("text", copytext);
				}
			}
			//else if (window.netscape) {
			//    try {
			//        netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');
			//        var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);

			//        if (!clip) {
			//            return;
			//        }

			//        var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);

			//        if (!trans) {
			//            return;
			//        }

			//        trans.addDataFlavor('text/unicode');

			//        var str = new Object();
			//        var len = new Object();
			//        var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);

			//        str.data = copytext;
			//        trans.setTransferData("text/unicode", str, copytext.length * 2);

			//        var clipid = Components.interfaces.nsIClipboard;

			//        if (!clip) {
			//            return false;
			//        }

			//        clip.setData(trans, null, clipid.kGlobalClipboard);

			//    } catch (e) {

			//    }

			//}
		}

		static SetZoomStyle(element, value, align?) {
			if (element === undefined || element === null) {
				return;
			}

			var zoomOrigin = "top left";
			if (align === DropDownAlign.Right) {
				zoomOrigin = "top right";
			}
			if (CoreUtility.firefox) {
				element.style.MozTransformOrigin = value === "" ? "" : zoomOrigin;
				element.style.MozTransform = value === "" ? "" : "scale(" + value + ")";
				element.setAttribute("ZoomValue", value);
			}
			else {
				if (CoreUtility.chrome || CoreUtility.safari) {
					if (value !== "") {
						element.style.MozTransform = "scale(" + value + ")";
						element.style.WebkitTransform = "scale(" + value + ")";
						element.style.webkitTransformOrigin = zoomOrigin;

					} else {
						element.style.MozTransform = "";
						element.style.WebkitTransform = "";
						element.style.WebkitTransformOrigin = "";
					}
				} else if (CoreUtility.IsIE9OrLater()) {
					if (value !== "") {
						element.style.msTransform = "scale(" + value + ")";
						element.style.msTransformOrigin = zoomOrigin;
					} else {
						element.style.msTransform = "";
						element.style.msTransformOrigin = "";
					}
				} else {
					element.style.zoom = value;
				}

			}
		}

		static FilterText(includeText, intext) {
			if (intext.GetLength() == 0)
				return "";
			var filterText = "";
			var j = 0;
			var i = 0;
			for (j = 0; j < intext.GetLength(); j++) {
				var valid = false;
				for (i = 0; i < includeText.GetLength(); i++) {
					if (intext.Substring(j, j + 1) == includeText.Substring(i, i + 1))
						valid = true;
				}
				if (valid == true)
					filterText += intext.Substring(j, j + 1);

			}
			return filterText;
		}

		static IndexOfAny(str, anyOf, startIndex) {
			if (startIndex >= str.length) {
				return -1;
			}
			for (var i = startIndex; i < str.length; i++) {
				for (var j = 0; j < anyOf.length; j++) {
					if (str.charAt(i) == anyOf[j]) {
						return i;
					}
				}
			}
			return -1;
		}
	}

	export enum FocusType {
		None = 0,
		Click = 1,
		ContextMenu = 2,
		ClientEvent = 3,
		KeyExit = 4,
		Default = 5,
		SpinButton = 6,
		DropDown = 7,
		ImeInput = 8,
		Left = 9,
		Right = 10,
		DragDrop = 11
	};

	/** @ignore */
	export enum DateCursorPosition {
		Default = 0,
		Era = 1,
		Year = 2,
		Month = 3,
		Day = 4,
		AMPM = 5,
		Hour = 6,
		Minute = 7,
		Second = 8
	};

	/**
	 * Defines the CrLf mode which describes how to process the CrLf char.
	 * @type {{NoControl: string, Filter: string, Cut: string}}
	 */
	export enum CrLfMode {
		/**
		 * Accepts all CrLf characters in copied, cut, or pasted strings.
		 */
		NoControl = 0,
		/**
		 * Removes all CrLf characters in copied, cut, or pasted strings.
		 */
		Filter = 1,
		/**
		 * Cuts the following strings from the first CrLf character in copied, cut, and pasted strings.
		 */
		Cut = 2
	};

	/**
 * Specifies how the literal in content is held in the clipboard.
 * @type {{IncludeLiterals: string, ExcludeLiterals: string}}
 */
	export enum ClipContent {
		/**
		 * Literals are included.
		 */
		IncludeLiterals = 0,
		/**
		 * Literals are excluded.
		 */
		ExcludeLiterals = 1
	};

	export enum EditMode {
		Insert = 0,
		Overwrite = 1,
		FixedInsert = 2,
		FixedOverwrite = 3
	};

	export enum ShowLiterals {
		Always = 1,
		PostDisplay = 2,
		PreDisplay = 3
	};

	export enum ExitOnLeftRightKey {
		None = 0,
		Left = 1,
		Right = 2,
		Both = 3
	};

	/**
  * Specifies the type of selection text in control.
  * @type {{None: string, Field: string, All: string}}
  */
	export enum HighlightText {
		/**
		 * No selection specified.
		 */
		None = 0,
		/**
		 * Select the specified field.
		 */
		Field = 1,
		/**
		 * Select all the text.
		 */
		All = 2
	};

	export enum DropDownAlign {
		Left = 1,
		Right = 2
	}

	export enum ScrollBarMode {
		Automatic,
		Fixed
	}

	export enum ScrollBars {
		None,
		Horizontal,
		Vertical,
		Both
	}

	export enum ControlStatus {
		Normal = 0,
		Hover = 1,
		Pressed = 2,
		Focused = 4,
		Disabled = 8
	}

	export enum ExitKeys {
		Tab = 1,
		ShiftTab = 2,
		NextControl = 3,
		PreviousControl = 4,
		Right = 5,
		Left = 6,
		CtrlRight = 7,
		CtrlLeft = 8,
		CharInput = 9
	};

	export enum TabAction {
		Control = 0,
		Field = 1
	};

	export enum Key {
		BackSpace = 8,
		Tab = 9,
		Clear = 12,
		Enter = 13,
		Shift = 16,
		Control = 17,
		Alt = 18,
		Pause = 19,
		Caps_Lock = 20,
		Escape = 27,
		Space = 32,
		PageUp = 33,
		PageDown = 34,
		End = 35,
		Home = 36,
		Left = 37,
		Up = 38,
		Right = 39,
		Down = 40,
		Select = 41,
		Print = 42,
		Execute = 43,
		Insert = 45,
		Delete = 46,
		Help = 47,
		equalbraceright = 48,
		exclamonesuperior = 49,
		quotedbltwosuperior = 50,
		sectionthreesuperior = 51,
		dollar = 52,
		percent = 53,
		ampersand = 54,
		slashbraceleft = 55,
		parenleftbracketleft = 56,
		parenrightbracketright = 57,
		A = 65,
		B = 66,
		C = 67,
		D = 68,
		E = 69,
		F = 70,
		G = 71,
		H = 72,
		I = 73,
		J = 74,
		K = 75,
		L = 76,
		M = 77,
		N = 78,
		O = 79,
		P = 80,
		Q = 81,
		R = 82,
		S = 83,
		T = 84,
		U = 85,
		V = 86,
		W = 87,
		X = 88,
		Y = 89,
		Z = 90,
		KP_0 = 96,
		KP_1 = 97,
		KP_2 = 98,
		KP_3 = 99,
		KP_4 = 100,
		KP_5 = 101,
		KP_6 = 102,
		KP_7 = 103,
		KP_8 = 104,
		KP_9 = 105,
		KP_Multiply = 106,
		KP_Add = 107,
		KP_Separator = 108,
		KP_Subtract = 109,
		KP_Decimal = 110,
		KP_Divide = 111,
		F1 = 112,
		F2 = 113,
		F3 = 114,
		F4 = 115,
		F5 = 116,
		F6 = 117,
		F7 = 118,
		F8 = 119,
		F9 = 120,
		F10 = 121,
		F11 = 122,
		F12 = 123,
		F13 = 124,
		F14 = 125,
		F15 = 126,
		F16 = 127,
		F17 = 128,
		F18 = 129,
		F19 = 130,
		F20 = 131,
		F21 = 132,
		F22 = 133,
		F23 = 134,
		F24 = 135,
		Num_Lock = 136,
		Scroll_Lock = 137
	};

	/** @ignore */
	export interface IBaseControl {

		UseClipboard: boolean;
		MultiLine: boolean;
		GetMultiLine(): boolean;
		GetUseClipboard(): boolean;
		_isSupportClipBoard(): boolean;
		AcceptsCrLf: CrLfMode;
		GetShowLiterals(): ShowLiterals;
		GetInputElement();
		GetAcceptsCrlf(): CrLfMode;
		InvalidInputEvent: any;
		KeyExitEvent: any;

	}

	/** @ignore */
	export interface IFormat {
		Fields: IFieldsCollection;
	}

	/** @ignore */
	export interface IFieldsCollection {
		SetValue(value: string, flag: boolean);
		SetText(text: string, flag: boolean);
		RollBack();
		Replace(selStart: number, selEnd: number, text: string, flag: boolean);
		MoveField(index: number, dir: boolean);
		Insert(selStart: number, text: string, flag: boolean);
		GetValue();
		GetText();
		ValueIsFull(mode: number): boolean;
		SaveOldState();
		GetLength(): number;
		GetFieldRange(index: number);
		GetNonLiteralsText(start: number, length: number);
		GetFieldIndexByPos(pos: number);
		GetFieldByIndex(index: number);
		fieldCount: number;
		Delete(selStart: number, length: number);
		ClearContent();
		GetFieldIndex(index: number);
		_focusNull: string;
		GetNonPromptText(start: number, length: number);
		formatIsNull: boolean;
	}


	/** @ignore */
	export class BaseUIProcess {
		isMulSelected = false;
		isMulSelect: boolean;
		isDblClick = false;
		isTriClick = false;
		isOverWrite = false;

		moveFocusExitOnLastChar = false;
		Format: IFormat;
		DisplayFormat: IFormat;
		Owner: IBaseControl;
		ID: string;


		GetInputElement() {
			return this.Owner.GetInputElement();
		}

		GetElementId() {
			return this.Owner.GetInputElement().id;
		}

		GetShowLiterals(): ShowLiterals {
			if (this.Owner.GetShowLiterals !== undefined) {
				return this.Owner.GetShowLiterals();
			}
			return ShowLiterals.Always;
		}

		/**
		* Get cursor start and end position according to the specified highlighttext and current text.
		*/
		SetCursorPositionAndSelection(highlightText, text: string, cursorPos?: number, startPos?: number) {
			var retInfo: any = {};

			if (highlightText == true || highlightText == HighlightText.All) {
				retInfo.SelectionStart = 0;
				retInfo.SelectionEnd = text.GetLength();
			}

			return retInfo;
		}

		/**
		* Clear the current value of the control.
		*/
		Clear() {
			return null;
		}



		/**
		* Handle the onfocus event.
		*/
		Focus(data) {
			var text = data.Text;
			var displayText = data.DisplayText;
			var focusType = data.FocusType;
			var oText = data.Element;
			var highlightText = data.HighlightText;
			var cursorPos = data.CursorPosition;
			var retInfo: any = {};

			//Add comments by Ryan Wu at 19:22 Dec 7, 2005.
			//Maybe this is a bug? for we press tab key to get focus. If the original state is
			//selection then the current state is also state, thus this.isMulSelected is true.????
			this.isMulSelected = false;

			//the focusType is used to distribute the get focus type by Left key
			// or Right key or something else.
			if (focusType == FocusType.Click) {
				retInfo.SelectionStart = Utility.GetCursorPosition(oText);
				retInfo.SelectionEnd = retInfo.SelectionStart;
			}

			//when get the focus, display the format.
			retInfo.Text = text;

			//Press tab key will set cursor start position to less than zero
			if (retInfo.SelectionStart == -1) {
				retInfo.SelectionStart = 0;
				retInfo.SelectionEnd = 0;
			}
			//for example: Format is "yy/MM/dd", displayformat is "yyyy/MM/dd"
			else if (retInfo.SelectionStart > retInfo.Text.GetLength()) {
				retInfo.SelectionStart = retInfo.Text.GetLength();
				retInfo.SelectionEnd = retInfo.Text.GetLength();
			}
			else if (retInfo.SelectionStart == displayText.GetLength()) {
				retInfo.SelectionStart = retInfo.Text.GetLength();
				retInfo.SelectionEnd = retInfo.Text.GetLength();
			}

			//the focusType is FocusType.Left it means that the focus is set by press the left key.
			// change by Sean Huang at 2008.12.05, for bug 992 -->
			//if (focusType == FocusType.Left)
			if (focusType == FocusType.Left && cursorPos == DateCursorPosition.Default)
			// end of Sean Huang <--
			{
				retInfo.SelectionStart = 0;
				retInfo.SelectionEnd = 0;
				//update by wuhao 2008-1-8 for fix bug 1362
				//return retInfo;
				//end by wuhao 2008-1-8 for fix bug 1362
			}
			// change by Sean Huang at 2008.12.05, for bug 992 -->
			//else if (focusType == FocusType.Right)
			else if (focusType == FocusType.Right && cursorPos == DateCursorPosition.Default)
			// end of Sean Huang <--
			{
				retInfo.SelectionStart = retInfo.Text.GetLength();
				retInfo.SelectionEnd = retInfo.Text.GetLength();
				//update by wuhao 2008-1-8 for fix bug 1362
				//return retInfo;
				//end by wuhao 2008-1-8 for fix bug 1362
			}

			//The selection is not determined by the HighlightText and CursorPosition property.
			if (highlightText == HighlightText.None && cursorPos == DateCursorPosition.Default) {
				return retInfo;
			}

			//Add comments by Ryan Wu at 9:23 Oct. 18 2007.
			//For fix the bug#9065.
			if (focusType == FocusType.ContextMenu) {
				return retInfo;
			}
			//end by Ryan Wu.

			//Add comments by Ryan Wu at 14:38 Oct. 11 2007.
			//For fix the bug#8998.
			//    //According to the HighlightText property and CursorPosition property to set the
			//	//selection.
			//	var ret = this.SetCursorPositionAndSelection(highlightText, retInfo.Text, cursorPos, retInfo.SelectionStart);
			var startPos = retInfo.SelectionStart == null ? data.SelectionStart : retInfo.SelectionStart;
			var ret = this.SetCursorPositionAndSelection(highlightText, retInfo.Text, cursorPos, startPos);
			//end by Ryan Wu.

			if (ret != null) {
				retInfo.SelectionStart = ret.SelectionStart;
				retInfo.SelectionEnd = ret.SelectionEnd;
				retInfo.IsSelectionDeterminedByHighlightText = true;
			}

			//Add comments by Ryan Wu at 9:13 Oct. 18 2007.
			//For removing the useless code.
			//	//the focusType is FocusType.ClientEvent it means that the focus is set by ourself.
			//	if (focusType == FocusType.ClientEvent)
			//	{
			//		return retInfo;
			//	}
			//end by Ryan Wu.

			return retInfo;
		}

		/**
		* Handle the onblur event.
		*/
		LoseFocus(data: any): any {

		}

		/**
		* Handle the onmousedown event.
		*/
		MouseDown(mouseButton: MouseButton) {
			var retInfo: any = {};

			this.isTriClick = false;

			//for triple click
			if (this.isDblClick && !Utility.GrapeCityTimeout && mouseButton == MouseButton.Left) {
				this.isTriClick = true;
				retInfo = this.SelectAll();
			}

			this.isDblClick = false;
			return retInfo;
		}

		/**
		* Handle the onmouseup event.
		*/
		// Frank Liu added the parameter "ctrlPressed" at 2013/06/27 for bug 881.
		MouseUp(obj, start: number, end: number, mouseButton: MouseButton, ctrlPressed?: boolean) {
			var retInfo: any = {};

			if (this.isTriClick) {
				Utility.SetSelection(obj, start, end);

				return null;
			}

			//Add comments by Ryan Wu at 10:13 Sep. 13 2007.
			//For fix the bug "17. Ctrl+Click(select all text) will take no effects.".
			// Frank Liu fixed bug 881 at 2013/06/27.
			//if (!Utility.IsIE() && Utility.FuncKeysPressed.Ctrl) {
			if (ctrlPressed) {
				retInfo = this.SelectAll();
				Utility.SetSelection(obj, retInfo.SelectionStart, retInfo.SelectionEnd);

				return retInfo;
			}
			//end by Ryan Wu.

			retInfo.SelectionStart = start;
			retInfo.SelectionEnd = end;

			if (mouseButton == MouseButton.Left) {
				//bug#5675
				//retInfo = GrapeCity_InputMan_GetCursorEndPos(obj, start);
				retInfo.SelectionStart = Utility.GetSelectionStartPosition(obj);
				retInfo.SelectionEnd = Utility.GetSelectionEndPosition(obj);
			}

			if (retInfo.SelectionStart != retInfo.SelectionEnd) {
				this.isMulSelected = true;
			}
			else {
				this.isMulSelected = false;
			}

			return retInfo;
		}

		/**
		* Handle the shortcut key event.
		*/
		ProcessShortcutKey(keyAction, readOnly, end, start?) {
			var retInfo: any = {};

			//ShortCuts
			switch (keyAction) {
				case "Clear":  //Clear
					if (readOnly) {
						retInfo.System = false;
						return retInfo;
					}

					return this.Clear();
				case "NextControl":    //NextControl
					var ret = this.MoveControl(this.GetInputElement(), true, false, "NextControl");

					if (ret != null) {
						retInfo.EventInfo = ret.EventInfo;
						retInfo.FocusType = ret.FocusType;
						retInfo.FocusExit = true;
					}

					retInfo.System = false;
					return retInfo;
				case "PreviousControl": //PreviousControl
					var ret = this.MoveControl(this.GetInputElement(), false, false, "PreviousControl");
					if (ret != null) {
						retInfo.EventInfo = ret.EventInfo;
						retInfo.FocusType = ret.FocusType;
						retInfo.FocusExit = true;
					}

					retInfo.System = false;
					return retInfo;
				case "NextField":       //NextField
					retInfo = this.MoveField(end, true);
					return retInfo;
				case "PreviousField":   //PreviousField
					retInfo = this.MoveField(end, false);
					return retInfo;
				case "NextField/NextControl":    //NextField/NextControl
					var retInfo = this.MoveFieldAndControl(end, true);
					return retInfo;
				case "PreviousField/PreviousControl": //PreviousField/PreviousControl
					var retInfo = this.MoveFieldAndControl(end, false);
					return retInfo;
			}
		}

		/**
		* Process char key input.
		*/
		ProcessCharKeyInput(k, start, end, isExitOnLastChar, text) {
			return null;
		}

		/**
		* Prcess navigator key input.
		*/
		ProcessNavigatorKeyInput(k, editMode, clipContent, text, start, end, exitOnLeftRightKey, isExitOnLastChar) {
			var retInfo: any = {};

			switch (k) {
				//Insert
				case 45:
					if (editMode == EditMode.FixedInsert) {
						this.isOverWrite = false;
					}
					else if (editMode == EditMode.FixedOverwrite) {
						this.isOverWrite = true;
					}
					else {
						this.isOverWrite = !this.isOverWrite;
					}

					retInfo.Overwrite = this.isOverWrite;
					retInfo.System = false;

					if (this.Format.Fields.fieldCount == 0) {
						retInfo.System = true;
					}

					return retInfo;
				//BackSpace
				case 8:
					//Add comments by Ryan Wu at 14:23 Jul. 19 2006.
					//Add text param only for number to judge whether the current text is zero.
					//retInfo = this.ProcessBackSpace(start, end);
					retInfo = this.ProcessBackSpace(start, end, text);
					//end by Ryan Wu.
					retInfo.System = false;
					break;
				//Delete
				//note: when we use the viewinbrowser mode, we can't get the del keycode,but if
				//we use the runtime mode ,we can get the del keycode. why????
				case 46:
					//Add comments by Ryan Wu at 14:23 Jul. 19 2006.
					//Add text param only for number to judge whether the current text is zero.
					//retInfo = this.ProcessDelete(start, end);
					retInfo = this.ProcessDelete(start, end, text);
					//end by Ryan Wu.
					retInfo.System = false;
					break;
				//Shift + Ctrl + End
				case 196643:
				//Shift + Ctrl + Home
				case 196644:
				//Shift + Ctrl + Left
				case 196645:
				//Shift + Ctrl + Right
				case 196647:
				//Shift + Pageup
				case 65569:
				//Shift + Ctrl + Pageup
				case 196641:
				//Shift + Pagedown
				case 65570:
				//Shift + Ctrl + Pagedown
				case 196642:
					if (k == 65569 || k == 196641) {
						k = 196644;
					}
					else if (k == 65570 || k == 196642) {
						k = 196643;
					}

					//perform the Shift+Ctrl+Left,Shift+Ctrl+Right,Shift+Ctrl+Home,Shift+Ctrl+End action
					retInfo.SelectionEnd = this.GetCaretPosition(end, k);
					this.isMulSelected = true;
					retInfo.System = false;
					break;
				//Shift + Delete
				// Shift + Backspace  DaryLuo 2012/10/16 fix bug 797 in IM Web 7.0.
				case 65582:
				case 65544:
					if (this.isMulSelected)  //same as cut Action
					{
						if (this.Owner._isSupportClipBoard()) {
							retInfo = this.Cut(clipContent, start, end);
						} else {
							retInfo.System = true;
							break;
						}
					}
					else	//the action of backspace
					{
						retInfo = this.ProcessBackSpace(start, end);
					}
					retInfo.System = false;
					break;
				//Shift + Insert  : Paste
				case 65581:
				//Ctrl + V
				case 131158:
					if (this.Owner._isSupportClipBoard()) {
						var pasteData = Utility.GetPasteData(this.Owner ? this.Owner.GetUseClipboard() : true);
						retInfo = this.Paste(start, end, pasteData, isExitOnLastChar);
						retInfo.System = false;
					} else {
						retInfo.System = true;
					}
					break;
				//Shift + End
				case 65571:
				//Shift + Home
				case 65572:
				//Shift + Left
				case 65573:
				//Shift + Right
				case 65575:
				// add by Jiang Changcheng at Aug 11 16:13, for bug#388 -->
				// Shift + Up
				case 65574:
				// Shift + Down
				case 65576:
					// end by Jiang Changcheng <--
					this.isMulSelected = true;
					//perform the Shift+Left,Shift+Right,Shift+Home,Shift+End action
					retInfo.SelectionEnd = this.GetCaretPosition(end, k);
					retInfo.System = false;
					break;
				//Ctrl + C
				case 131139:
				//Ctrl + Insert
				case 131117:
					if (this.Owner._isSupportClipBoard()) {
						this.Copy(clipContent, start, end);
						retInfo.System = false;
					} else {
						retInfo.System = true;
					}

					break;
				//Ctrl + Delete
				case 131118:
					if (!this.isMulSelected) {
						end = this.GetCaretPosition(end, k);
					}

					retInfo = this.ProcessDelete(start, end);
					retInfo.System = false;
					break;
				//Ctrl + BackSpace  Ctrl + Shift + BackSpace Key
				case 131080:
				case 196616:
					if (!this.isMulSelected) {
						end = this.GetCaretPosition(end, k);
					}

					retInfo = this.ProcessBackSpace(start, end);
					retInfo.System = false;
					break;
				//Ctrl + A
				case 131137:
					retInfo = this.SelectAll();
					retInfo.System = false;
					break;
				//Ctrl + X
				case 131160:
					if (this.Owner._isSupportClipBoard()) {
						retInfo = this.Cut(clipContent, start, end);
						retInfo.System = false;
					} else {
						retInfo.System = true;
					}

					break;
				//Ctrl + Z
				case 131162:
					//Need add undo methods by Ryan wu.
					retInfo = this.Undo();
					retInfo.System = false;
					break;
				//Ctrl + Left
				case 131109:
				//Left
				case 37:
					//Move to previous control
					if (start == 0 && (exitOnLeftRightKey == ExitOnLeftRightKey.Both || exitOnLeftRightKey == ExitOnLeftRightKey.Left)) {
						var exitType = k == 37 ? "Left" : "CtrlLeft";
						var ret = this.MoveControl(this.GetInputElement(), false, true, exitType);

						if (ret != null) {
							retInfo.EventInfo = ret.EventInfo;
							retInfo.FocusType = ret.FocusType;
							retInfo.FocusExit = true;
						}

						return retInfo;
					}
				//Ctrl + Home
				case 131108:
				// add by Sean Huang at 2009.04.16, for bug 2046 -->
				//Ctrl + Up
				case 131110:
				// end of Sean Huang <--
				//Home
				case 36:
				//PageUp
				case 33:
				//Ctrl + PageUp
				case 131105:
					if (k == 33 || k == 131105) {
						k = 131108;
					}

					retInfo = this.ProcessLeftDirection(start, end, k);
					retInfo.System = false;
					break;
				//Ctrl + Right
				case 131111:
				//Right
				case 39:
					//Move to next control
					if (start == text.GetLength() && (exitOnLeftRightKey == ExitOnLeftRightKey.Both || exitOnLeftRightKey == ExitOnLeftRightKey.Right)) {
						var exitType = k == 39 ? "Right" : "CtrlRight";
						var ret = this.MoveControl(this.GetInputElement(), true, true, exitType);
						if (ret != null) {
							retInfo.EventInfo = ret.EventInfo;
							retInfo.FocusType = ret.FocusType;
							retInfo.FocusExit = true;
						}
						return retInfo;
					}
				//Ctrl + End
				case 131107:
				// add by Sean Huang at 2009.03.30, for bug 2046 -->
				//Ctrl + Down
				case 131112:
				// end of Sean Huang <--
				//End
				case 35:
				//PageDown
				case 34:
				//Ctrl + PageDown
				case 131106:
					if (k == 34 || k == 131106) {
						k = 131107;
					}
					retInfo = this.ProcessRightDirection(start, end, k);
					retInfo.System = false;
					break;
				default:
					retInfo = null;
					break;
			}

			return retInfo;
		}
		ProcessLeftDirection(start: number, end: number, k: number) {
		}
		ProcessRightDirection(start: number, end: number, k: number) {
		}
		/**
		* Handle the onkeydown event.
		*/
		KeyDown(data) {
			var k = data.KeyCode;
			var start = data.SelectionStart;
			var end = data.SelectionEnd;
			var text = data.Text;
			var editMode = data.EditMode;
			var keyAction = data.KeyAction;
			var readOnly = data.ReadOnly;
			var clipContent = data.ClipContent;
			//var funcKeysPressed	   = data.FuncKeysPressed;
			var isExitOnLastChar = data.ExitOnLastChar;
			var exitOnLeftRightKey = data.ExitOnLeftRightKey;
			var tabAction = data.TabAction;
			var retInfo: any = {};

			switch (editMode) {
				case EditMode.Insert:
					this.isOverWrite = false;
					break;
				case EditMode.Overwrite:
					this.isOverWrite = true;
					break;
				case EditMode.FixedInsert:
					this.isOverWrite = false;
					break;
				case EditMode.FixedOverwrite:
					this.isOverWrite = true;
					break;
			}

			if (start != end) {
				this.isMulSelected = true;
			}
			else {
				this.isMulSelected = false;
			}

			//Why we must process tab key first. Because in IE we use the system's tab action and the default shortcuts has tab action.
			//So we must process tab key before processing the shortcuts
			switch (k) {
				case 9:
					retInfo = this.ProcessTabKey(end, true, tabAction);
					return retInfo;
				case 65545:
					retInfo = this.ProcessTabKey(end, false, tabAction);
					return retInfo;
			}

			//ShortCuts
			if (keyAction != null) {
				return this.ProcessShortcutKey(keyAction, readOnly, end, start);
			}

			//The ReadOnly property is set to true
			if (readOnly) {
				//When readonly is true, Escape, Alt + Up, Alt + Down, Up, Down can also take effect.
				//If return null, then in BaseInputControl will handle the Escape, Alt + Up and Alt + Down action.
				if (k == 27 || k == 262182 || k == 262184 || k == 38 || k == 40) {
					return null;
				}

				//we only let the Ctrl+C and Ctrl+Insert and ShortCut to work when we set
				//ReadOnly property to true.
				// change by Sean Huang at 2008.08.13, for bug 28 (ttp)-->
				//if (k != 131117 && k != 131139)
				if (k != 131117 && k != 131139
					&& k != 9 && k != 65545 && k != 131081 && k != 196617
				//Ctrl+A
					&& k != 131137
				//left, right, up, down
					&& k != 37 && k != 39 && k != 38 && k != 40)
				// end of Sean Huang <--
				{
					return retInfo;
				}
			}

			//the DateFormat has no Pattern property, we use the system's keydown action
			if (this.Format.Fields.fieldCount == 0) {
				//Why I move the following code here from the end part of the method? It is because
				//when there is no format we must invoke the editstatuschanged event and the ExitOnLeftRightKey
				//property also takes effect.
				switch (k) {
					//Insert
					case 45:
						if (editMode == EditMode.FixedInsert) {
							this.isOverWrite = false;
						}
						else if (editMode == EditMode.FixedOverwrite) {
							this.isOverWrite = true;
						}
						else {
							this.isOverWrite = !this.isOverWrite;

						}

						retInfo.Overwrite = this.isOverWrite;

						if (this.Format.Fields.fieldCount == 0) {
							retInfo.System = true;
						}

						return retInfo;
					//Left
					case 37:
					//Ctrl + Left
					case 131109:
						//Move to previous control
						if (start == 0 && (exitOnLeftRightKey == ExitOnLeftRightKey.Both || exitOnLeftRightKey == ExitOnLeftRightKey.Left)) {
							var exitType = k == 37 ? "Left" : "CtrlLeft";
							var ret = this.MoveControl(this.GetInputElement(), false, true, exitType);
							if (ret != null) {
								retInfo.EventInfo = ret.EventInfo;
								retInfo.FocusType = ret.FocusType;
								retInfo.FocusExit = true;
							}
							return retInfo;
						}
						break;
					//Right
					case 39:
					//Ctrl + Right
					case 131111:
						//Move to next control
						if (start == text.GetLength() && (exitOnLeftRightKey == ExitOnLeftRightKey.Both || exitOnLeftRightKey == ExitOnLeftRightKey.Right)) {
							var exitType = k == 39 ? "Right" : "CtrlRight";
							var ret = this.MoveControl(this.GetInputElement(), true, true, exitType);
							if (ret != null) {
								retInfo.EventInfo = ret.EventInfo;
								retInfo.FocusType = ret.FocusType;
								retInfo.FocusExit = true;
							}
							return retInfo;
						}
						break;
				}

				return null;
			}

			//Process char key input.
			var processInfo = this.ProcessCharKeyInput(k, start, end, isExitOnLastChar, text);

			if (processInfo != null) {
				return processInfo;
			}

			retInfo = this.ProcessNavigatorKeyInput(k, editMode, clipContent, text, start, end, exitOnLeftRightKey, isExitOnLastChar);

			return retInfo;
		}

		/**
		* Handle the onkeypress event.
		*/
		KeyPress(e) {
		}

		/**
		* Handle the onkeyup event.
		*/
		KeyUp(e) {
		}

		/**
		* Handle the oncontextmenu event.
		*/
		ShowContextMenu(oText, selText) {
			var retInfo: any = {};

			//If there's no text selected
			if (selText == "") {
				retInfo.SelectionStart = Utility.GetCursorPosition(oText);
				retInfo.SelectionEnd = retInfo.SelectionStart;
			}

			return retInfo;
		}

		/**
		* Handle the onselectstart event.
		*/
		SelectStart(obj, selText, mouseButton) {
			var retInfo: any = {};

			if (selText == "" && !this.isTriClick && !this.isDblClick && mouseButton != MouseButton.Default) {
				retInfo.SelectionStart = Utility.GetCursorPosition(obj);
				retInfo.SetFalse = true;
			}

			return retInfo;
		}

		/**
		* Handle the ondblclick event.
		*/
		DoubleClick(pos: number) {
			var retInfo: any = {};
			//Get current field range
			var fieldIndex = this.Format.Fields.GetFieldIndexByPos(pos);
			var fieldPos = this.Format.Fields.GetFieldRange(fieldIndex.index);
			retInfo.SelectionStart = fieldPos.start;
			retInfo.SelectionEnd = fieldPos.length + fieldPos.start;

			//set timer for tripple click
			Utility.GrapeCityTimeout = false;
			this.isDblClick = true;
			this.isMulSelect = true;

			setTimeout(function () { Utility.GrapeCityTimeout = true; }, 300);

			return retInfo;
		}

		/**
		* Handle the undo actions.
		*/
		Undo(): any {
		}

		/**
		* Handle the cut actions.
		* @param clipContent - The copy mode.
		* @param start - The start cursor position.
		* @param end   - The end cursor position.
		* @returns Returns the cursor position.
		*/
		Cut(clipContent: ClipContent, start: number, end: number) {
			var retInfo: any = {};

			if (start == end) {
				return retInfo;
			}

			this.FireClientEvent("OnBeforeCut");
			this.Copy(clipContent, start, end);
			retInfo = this.ProcessDelete(start, end);

			this.FireClientEvent("OnCut");
			return retInfo;
		}

		/**
		* Handle the oncopy event.
		* @param clipContent - The copy mode.
		* @param start - The start cursor position.
		* @param end   - The end cursor position.
		*/
		Copy(clipContent: ClipContent, start: number, end: number) {
			var text = null;
			var useClipboard = true;

			if (clipContent == ClipContent.ExcludeLiterals) {
				var length = Math.abs(start - end);
				var start = Math.min(start, end);
				if (length == 0) {
					return;
				}

				text = this.Format.Fields.GetNonLiteralsText(start, length);
			}
			//Add comments by Ryan Wu at 16:44 Aug. 13 2007.
			//For Firefox can't get the hightlight text using document.selection.
			else if (!CoreUtility.IsIE() || CoreUtility.IsIE11OrLater()) {
				text = Utility.GetSelectionText(this.Owner.GetInputElement());
			}
			// add by Sean Huang at 2009.04.29, for bug 2209 -->
			else {
				text = document.selection.createRange().text;
			}
			// end of Sean Huang <--
			//end by Ryan Wu.

			if (this.Owner) {
				text = BaseUIProcess.UpdateCrLfString(text, this.Owner.GetAcceptsCrlf());
				useClipboard = this.Owner.GetUseClipboard();
			}
			// change by Sean Huang at 2009.04.29, for bug 2209 -->
			//Utility.SetCopy(text);
			if (CoreUtility.IsIE()) {
				setTimeout(function () { Utility.SetCopy(text, useClipboard); });
			}
			else {
				Utility.SetCopy(text, useClipboard);
			}
			// end of Sean Huang <--
		}

		/**
		* Handle the onpaste event.
		*/
		Paste(start: number, end: number, text: string, exitonlastChar?: boolean): any {
		}

		/**
		* Select all the content. 
		* @returns Returns the cursor position.
		*/
		SelectAll() {
			var retInfo = { SelectionStart: 0, SelectionEnd: 0 };
			retInfo.SelectionStart = 0;

			//modified by sj 2008.8.13 for bug 243
			var ShowLiterals;

			if (this.ID) {
				ShowLiterals = this.GetShowLiterals();
			}

			if (ShowLiterals == 'PostDisplay' || ShowLiterals == 'PreDisplay') {
				retInfo.SelectionEnd = Utility.FindIMControl(this.ID).GetText().GetLength();
			}
			else {
				retInfo.SelectionEnd = this.Format.Fields.GetLength();
			}
			//retInfo.SelectionEnd   = this.Format.Fields.GetLength(); 
			//end by sj

			this.isMulSelected = true;

			return retInfo;
		}

		//Add comments by Ryan Wu at 9:31 Apr. 5 2007.
		//For support Aspnet Ajax 1.0.	
		///*
		//* This Function should be called when an event needs to be fired.
		//* @param oControl - the javascript object representation of our control.
		//* @param eName    - the name of the function that should handle this event.
		//* @param eArgs    - the argument of the function that should handle this event.
		//*/
		//FireEvent (oControl, eName, eArgs)
		//{
		//	//Because when we fire client event we may be invoke the lose focus event, 
		//	//so we must return the current focus type of getting focus.
		//	//No event will be fired
		//	if (eName == null || eName == "")
		//	{
		//		return null;
		//	}
		//	
		//	if (Utility.FireEvent(oControl, eName, eArgs))
		//	{
		//		return FocusType.ClientEvent;
		//	}
		//};
		/*
		* This Function should be called when an event needs to be fired.
		* @param oControl - the javascript object representation of our control.
		* @param eName    - the name of the function that should handle this event.
		* @param eArgs    - the argument of the function that should handle this event.
		*/
		FireEvent(oControl, eName, eArgs, eType?) {
			//Because when we fire client event we may be invoke the lose focus event, 
			//so we must return the current focus type of getting focus.
			//No event will be fired
			//if (eName == null || eName == "") {
			//    return null;
			//}

			if (Utility.FireEvent(oControl, eName, eArgs, eType)) {
				return FocusType.ClientEvent;
			}
		}
		//end by Ryan Wu.

		/**
		* Process the input char key action. 
		* @param start - The specified start cursor position.
		* @param end - The specified end cursor position.
		* @param charInput - The specified char will be input.
		* @returns Returns action result includes cursor position and if succeed after the process and whether we fire a client event.
		*/
		ProcessCharKey(start: number, end: number, charInput: string, isExitOnLastChar: boolean) {
			var processInfo: any = {};
			//get the selection information.
			var selectionStart = Math.min(start, end);
			var selectionLength = Math.abs(end - start);
			var retInfo: any = {};

			processInfo.start = selectionStart;
			processInfo.success = false;

			//none action.
			if (this.Format.Fields.GetFieldIndex(selectionStart).index == -1) {
				//we input an invalid char, so invoke the InvalidInput Event

				var eventInfo: any = {};
				eventInfo.Name = this.Owner.InvalidInputEvent;
				eventInfo.Args = null;
				//Add comments by Ryan Wu at 10:27 Apr. 5 2007.
				//For support Aspnet Ajax 1.0.
				eventInfo.Type = "invalidInput";
				//end by Ryan Wu.
				processInfo.EventInfo = eventInfo;


				return processInfo;
			}

			var text = charInput.toString();

			if (selectionLength == 0 && !this.isOverWrite) {
				retInfo = this.Format.Fields.Insert(selectionStart, text, false);
			}
			else if (selectionLength == 0) {
				if (selectionStart == this.Format.Fields.GetLength()) {
					retInfo = this.Format.Fields.Insert(selectionStart, text, false);
				}
				else {
					var isReplace = false;
					var posInfo = this.Format.Fields.GetFieldIndexByPos(selectionStart);
					var fieldIndex = posInfo.index;
					var fieldOffset = posInfo.offset;
					//var fieldsLength = selectionStart - fieldOffset;
					var fieldRange = this.Format.Fields.GetFieldRange(fieldIndex);

					if (this.Format.Fields.GetFieldByIndex(fieldIndex).fieldLabel == "PromptField") {
						if (selectionStart - fieldOffset + fieldRange.length == this.Format.Fields.GetLength()) {
							isReplace = false;
						}
						else {
							isReplace = true;
							selectionLength = fieldRange.length - fieldOffset + 1;
						}
					}
					else {
						isReplace = true;
						// DaryLuo 2013/07/15 fix bug 1014 in IM HTML 5.
						selectionLength = charInput.GetLength();
					}

					//none action.
					if (this.Format.Fields.GetFieldIndexByPos(selectionStart + selectionLength).index == -1) {
						//we input an invalid char, so invoke the InvalidInput Event

						var eventInfo: any = {};
						eventInfo.Name = this.Owner.InvalidInputEvent;
						eventInfo.Args = null;
						//Add comments by Ryan Wu at 10:27 Apr. 5 2007.
						//For support Aspnet Ajax 1.0.
						eventInfo.Type = "invalidInput";
						//end by Ryan Wu.

						processInfo.EventInfo = eventInfo;


						return processInfo;
					}

					if (isReplace) {
						retInfo = this.Format.Fields.Replace(selectionStart, selectionLength, text, false);
					}
					else {
						retInfo = this.Format.Fields.Insert(selectionStart, text, false);
					}
				}
			}
			else {
				retInfo = this.Format.Fields.Replace(selectionStart, selectionLength, text, false);
			}

			selectionStart = retInfo.cursorPos;

			//we input an invalid char, so invoke the InvalidInput Event
			if (retInfo.text != "") {

				var eventInfo: any = {};
				eventInfo.Name = this.Owner.InvalidInputEvent;
				eventInfo.Args = null;
				//Add comments by Ryan Wu at 10:27 Apr. 5 2007.
				//For support Aspnet Ajax 1.0.
				eventInfo.Type = "invalidInput";
				//end by Ryan Wu.
				processInfo.EventInfo = eventInfo;


				processInfo.start = selectionStart;
				processInfo.success = false;

				return processInfo;
			}

			//judge if the focus should exit on last char.
			if (isExitOnLastChar == true) {
				if (selectionStart == this.Format.Fields.GetLength()) {
					this.moveFocusExitOnLastChar = true;
				}
				else {
					var posInfo = this.Format.Fields.GetFieldIndexByPos(selectionStart);
					var fieldIndex = posInfo.index;
					var fieldOffset = posInfo.offset;

					if (fieldIndex == this.Format.Fields.fieldCount - 1 && fieldOffset == 0
						&& this.Format.Fields.GetFieldByIndex(fieldIndex).fieldLabel == "PromptField") {
						this.moveFocusExitOnLastChar = true;
					}
				}
			}

			processInfo.start = selectionStart;
			processInfo.success = true;

			return processInfo;
		}

		/**
			* Process the Delete key down action. 
			* @param start - The specified start cursor position.
			* @param end - The specified end cursor position.
			* @returns Returns the cursor position after the process.
			*/
		ProcessDeleteKey(start: number, end: number) {
			//get the selection information.
			var selectionStart = Math.min(start, end);
			var selectionLength = Math.abs(start - end);
			var retInfo: any = {};

			if (selectionStart == this.Format.Fields.GetLength() && selectionLength == 0) {
				return retInfo;
			}

			//var startFieldOffset;
			var startFieldIndex;
			var fieldPosInfo = this.Format.Fields.GetFieldIndexByPos(selectionStart);
			startFieldIndex = fieldPosInfo.index;
			//startFieldOffset = fieldPosInfo.offset;

			//none action.
			if (startFieldIndex == -1) {
				return retInfo;
			}

			//if the selectionlength = 0, do the delete action for one post char.
			if (selectionLength == 0) {
				//none action.
				if (this.Format.Fields.GetFieldByIndex(startFieldIndex).fieldLabel == "PromptField") {
					return retInfo;
				}

				selectionStart = this.Format.Fields.Delete(selectionStart, 1).cursorPos;

				retInfo.SelectionStart = selectionStart;
				retInfo.SelectionEnd = retInfo.SelectionStart;
			}
			//if the selectionlength != 0, do the delete action for the selection range..
			else {
				var endFieldOffset;
				var endFieldIndex;
				fieldPosInfo = this.Format.Fields.GetFieldIndexByPos(selectionStart + selectionLength);
				endFieldOffset = fieldPosInfo.offset;
				endFieldIndex = fieldPosInfo.index;

				//none action.
				if (endFieldIndex == -1) {
					return retInfo;
				}

				if (endFieldOffset == 0) {
					endFieldIndex--;
				}

				//none action
				if (startFieldIndex == endFieldIndex && this.Format.Fields.GetFieldByIndex(startFieldIndex).fieldLabel == "PromptField") {
					return retInfo;
				}

				var info = this.Format.Fields.Delete(selectionStart, selectionLength);
				//The same as BackSpace. ("20005[/)02/24" press delete)
				if (!info.isSucceed) {
					return retInfo;
				}

				retInfo.SelectionStart = info.cursorPos;

				if (retInfo.SelectionStart == 0 && this.Format.Fields.GetFieldByIndex(0).fieldLabel == "PromptField") {
					retInfo.SelectionStart = this.Format.Fields.GetFieldByIndex(0).GetLength();
				}

				retInfo.SelectionEnd = retInfo.SelectionStart;

				//Accordingto the changed information, generate BehaviorInfo and invoke UpdateBehavior to finsihed the correlative action.
				var currentFieldOffset;
				var currentFieldIndex;
				var currentFieldInfo = this.Format.Fields.GetFieldIndexByPos(retInfo.SelectionStart);
				currentFieldIndex = currentFieldInfo.index;
				currentFieldOffset = currentFieldInfo.offset;

				if (currentFieldOffset != 0 && this.Format.Fields.GetFieldByIndex(currentFieldIndex).fieldLabel == "PromptField") {
					retInfo.SelectionStart = retInfo.SelectionStart - currentFieldOffset + this.Format.Fields.GetFieldByIndex(currentFieldIndex).GetLength();
					retInfo.SelectionEnd = retInfo.SelectionStart;
				}
			}

			return retInfo;
		}

		/**
		* Perform the delete keydown event. 
		* @param start - The start cursor position.
		* @param end   - The start end position.
		*/
		ProcessBackSpace(start: number, end: number, text?: string): any {
		}

		/**
		* Perform the delete keydown event. 
		* @param start - The start cursor position.
		* @param end   - The start end position.
		*/
		ProcessDelete(start: number, end: number, text?: string): any {
		}

		/**
			* Get the next caret position according to the special cursor position and keycode(processType). 
			* @param cursorPos - The current cursor position.
			* @param keyCode   - The keyCode indicate the key action.
			* @returns Return the cursor position after the key action.
			*/
		GetCaretPosition(cursorPos: number, keyCode: number, startPos?: number, endPos?: number, literalFieldLabel?) {
			var fields = this.Format.Fields;
			var fieldPosInfo = fields.GetFieldIndexByPos(cursorPos);
			var fieldIndex = fieldPosInfo.index;
			var fieldOffset = fieldPosInfo.offset;
			var fieldRange;
			//var startPos;
			//var endPos;
			var i = 0;

			switch (keyCode) {
				//Home key pressed
				case 36:
				//Shift + Home
				case 65572:
				// add by Jiang Changcheng at Aug 11 16:13, for bug#388 -->
				// Shift + Up
				case 65574:
					// end by Jiang Changcheng <--
					if (cursorPos <= startPos) {
						return 0;
					}
					else {
						return startPos;
					}
				//End key pressed
				case 35:
				//Shift + End
				case 65571:
				// add by Jiang Changcheng at Aug 11 16:13, for bug#388 -->
				// Shift + Down
				case 65576:
					// end by Jiang Changcheng <--
					if (cursorPos >= endPos) {
						return fields.GetLength();
					}
					else {
						return endPos;
					}
				//Left key
				case 37:
					if (cursorPos == 0) {
						return 0;
					}

					if (fields.GetFieldByIndex(fieldIndex).fieldLabel == literalFieldLabel) {
						if (fieldOffset > 0) {
							cursorPos -= fieldOffset;
						}
						else {
							cursorPos--;
						}
					}
					else {
						//aaaggg|eebbbMMccddee
						if (fieldOffset == 0 && fields.GetFieldByIndex(fieldIndex - 1).fieldLabel == literalFieldLabel) {
							cursorPos -= fields.GetFieldRange(fieldIndex - 1).length;
						}
						else {
							cursorPos--;
						}
					}
					break;
				//Right key
				case 39:
					if (cursorPos == fields.GetLength()) {
						return cursorPos;
					}

					if (fields.GetFieldByIndex(fieldIndex).fieldLabel == literalFieldLabel) {
						fieldRange = fields.GetFieldRange(fieldIndex);
						startPos = fieldRange.start;
						endPos = startPos + fieldRange.length;

						if (cursorPos < endPos) {
							return endPos;
						}
					}
					else {
						cursorPos++;
					}
					break;
				//Ctrl + Left arrow key
				case 131109:
					if (cursorPos == 0 || fieldIndex == 0) {
						return 0;
					}

					if (fields.GetFieldByIndex(fieldIndex).fieldLabel == literalFieldLabel) {
						fieldRange = fields.GetFieldRange(fieldIndex - 1);
						cursorPos = fieldRange.start;
					}
					else {
						if (fieldOffset == 0) {
							for (i = fieldIndex - 1; i >= 0; i--) {
								//find the edit field before the current field
								if (fields.GetFieldByIndex(i).fieldLabel != literalFieldLabel) {
									fieldRange = fields.GetFieldRange(i);

									return fieldRange.start;
								}
							}

							//if the former field is PromptField, then return 0
							return 0;
						}
						else {
							cursorPos -= fieldOffset;
						}
					}
					break;
				//Ctrl + Shift + Left key
				case 196645:
					if (cursorPos == 0 || fieldIndex == 0) {
						return 0;
					}

					if (fieldOffset == 0) {
						fieldRange = fields.GetFieldRange(fieldIndex - 1);
						cursorPos = fieldRange.start;
					}
					else {
						cursorPos -= fieldOffset;
					}
					break;
				//Ctrl + Right arrow key
				case 131111:
					if (cursorPos == fields.GetLength() || fieldIndex == fields.fieldCount - 1) {
						return fields.GetLength();
					}

					if (fields.GetFieldByIndex(fieldIndex).fieldLabel == literalFieldLabel) {
						fieldRange = fields.GetFieldRange(fieldIndex + 1);
						cursorPos = fieldRange.start;
					}
					else {
						for (i = fieldIndex + 1; i < fields.fieldCount; i++) {
							//find the edit field after the current field
							if (fields.GetFieldByIndex(i).fieldLabel != literalFieldLabel) {
								fieldRange = fields.GetFieldRange(i);

								return fieldRange.start;
							}
						}

						//if the latter field is PromptField, then return fieldcollection length
						return fields.GetLength();
					}
					break;
				//Ctrl + Shift + Right key
				case 196647:
					if (cursorPos == fields.GetLength() || fieldIndex == fields.fieldCount - 1) {
						return fields.GetLength();
					}

					//if the current caret is in the last field then return the fields' length
					fieldRange = fields.GetFieldRange(fieldIndex + 1);
					cursorPos = fieldRange.start;
					break;
				//Ctrl + Delete key
				case 131118:
					if (cursorPos == fields.GetLength() || fields.GetFieldByIndex(fieldIndex).fieldLabel == literalFieldLabel) {
						return cursorPos;
					}
					else {
						fieldRange = fields.GetFieldRange(fieldIndex);

						return fieldRange.start + fieldRange.length;
					}
				//Ctrl + BackSpace key  Ctrl + Shift + BackSpace Key
				case 131080:
				case 196616:
					if (cursorPos == 0
						|| (fields.GetFieldByIndex(fieldIndex).fieldLabel == literalFieldLabel && fieldIndex == 0)) {
						return cursorPos;
					}
					else if (fields.GetFieldByIndex(fieldIndex).fieldLabel == literalFieldLabel) {
						return fields.GetFieldRange(fieldIndex - 1).start;
					}
					else {
						if (fieldOffset == 0) {
							for (i = fieldIndex - 1; i >= 0; i--) {
								//find the edit field before the current field
								if (fields.GetFieldByIndex(i).fieldLabel != literalFieldLabel) {
									fieldRange = fields.GetFieldRange(i);

									return fieldRange.start;
								}
							}

							//if there is no edit field before the current field
							return cursorPos;
						}
						else {
							cursorPos -= fieldOffset;
						}
					}
					break;
				//Ctrl + Shift + End key
				case 196643:
				//Ctrl + End key
				case 131107:
				// add by Sean Huang at 2009.03.30, for bug 2046 -->
				//Ctrl + Down arrow key
				case 131112:
					// end of Sean Huang <--
					return fields.GetLength();
				//Ctrl + Shift + Home key
				case 196644:
				//Ctrl + Home key
				case 131108:
				// add by Sean Huang at 2009.03.30, for bug 2046 -->
				//Ctrl + Up arrow key
				case 131110:
					// end of Sean Huang <--
					return 0;
				//Shift + Left key
				case 65573:
					if (cursorPos == 0) {
						return 0;
					}
					else {
						return --cursorPos;
					}
				//Shift + Right key
				case 65575:
					if (cursorPos == fields.GetLength()) {
						return cursorPos;
					}
					else {
						return ++cursorPos;
					}
			}

			return cursorPos;
		}

		/**
		* Compare the specified keycode with the shortcut array passed from server side. 
		* @param keyCode - The keyCode indicate the key action.
		* @param shortcut - The shortcut text passed from server side.
		* @returns Return true if keycode in the shortcuts array otherwise return false.
		*/
		CompareShortcut(keyCode, shortcut) {
			if (shortcut.toString().IndexOf("|") == -1) {
				if (shortcut == keyCode) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				var index = null;
				while (index != -1) {
					index = shortcut.toString().IndexOf("|");
					var length = shortcut.toString().GetLength();
					if (shortcut.Substring(0, index == -1 ? length : index) == keyCode) {
						return true;
					}
					else {
						shortcut = shortcut.Substring(index + 1, length);
					}
				}
				return false;
			}
		}

		/**
			* Move the focus from one control to another. 
			* @param elementID - The current element id.
			* @param isForward - The boolean value indicate if we move focus to the next control according to the tabindex value.
			* @param isUseLeftRightKey - The boolean value indicate if we move focus to the next control by left or right key.
			* @param exitType - The exit type.
			*/
		MoveControl(currentElement: HTMLElement, isForward: boolean, isUseLeftRightKey?: boolean, exitType?) {
			var elements = CoreUtility.GetElements();
			var ret: any = null;
			var retInfo: any = {};

			//Add by Ryan Wu at 10:24 Jan. 20 2006.
			//For fix bug#4965.
			if (elements.length < 2) {
				return null;
			}

			//Add comments by Ryan Wu at 14:08 Aug. 28 2007.
			//For the sequence of the onfocus, onblur, onkeydown event in firefox is not same as IE 
			//when we use the focus method in keydown.
			//in IE: onkeydown --> onblur --> onfocus.
			//in Firefox: onblur --> onfocus --> onkeydown.
			//So we must split the MoveFocus into two methods. Firstly, We must get the next control's id.
			//Then if we use the ExitOnLeftRightKey to move focus, we should set the next control's FocusType to FocusType.Left/FocusType.Right.
			//Lastly we can invoke the obj.focus method to set focus to the next control.
			//	var nextID = Utility.MoveFocus(elementID, elements, isForward);
			//	
			//	if (isUseLeftRightKey == true)
			//	{
			//		try
			//		{
			//		    var index = nextID.LastIndexOf("_EditField");
			//    		
			//		    if (index != -1)
			//		    {
			//		        var conID = nextID.Substring(0, index);
			//		        var nextObj = FindIMControl(conID);
			//		        nextObj.FocusType = isForward ? FocusType.Left : FocusType.Right;
			//		    }
			//		}
			//		catch(e)
			//		{}
			//    }
			var nextElement = CoreUtility.GetNextFocusableControl(currentElement, elements, isForward);

			// TODO:
			//if (isUseLeftRightKey == true) {
			//    try {
			//        var index = nextID.LastIndexOf("_EditField");

			//        if (index != -1) {
			//            var conID = nextID.Substring(0, index);
			//            var nextObj = FindIMControl(conID);
			//            nextObj.FocusType = isForward ? FocusType.Left : FocusType.Right;
			//        }
			//        else {
			//            var nextObj = FindIMControl(nextID);
			//            if (nextObj) {
			//                if (nextObj.IsjQueryControl == true) {
			//                    nextObj.FocusType = isForward ? FocusType.Left : FocusType.Right;
			//                }
			//            }
			//        }
			//    }
			//    catch (e)
			//    { }
			//}

			// change by Sean Huang at 2008.12.16, for bug 1054 -->
			//Utility.SetElementFocus(nextID);
			// change by Sean Huang at 2009.01.04, for bug 1402 -->
			//if (exitType == "CharInput")
			// change by Sean Huang at 2009.02.16, for bug 1863, 1865 -->
			//if (exitType == "CharInput" || (!isIE && isUseLeftRightKey))
			if (exitType == "CharInput" || (!CoreUtility.IsIE()))
			// end of Sean Huang <--
			// end of Sean Huang <--
			{
				Utility.NextID = nextElement;
				setTimeout(function () { CoreUtility.SetElementFocus(nextElement); }, 0);
			}
			else {
				CoreUtility.SetElementFocus(nextElement);
			}
			// end of Sean Huang <--

			// add by Sean Huang at 2008.12.09, for bug 1057, 1058 -->
			// set the cursor position of the standard text box while it is get focus 
			// by using the left right key.
			if (isUseLeftRightKey) {
				var obj: any = document.getElementById(nextElement);
				if (obj != null
					&& (obj.tagName.toLowerCase() == "textarea" || obj.type == "text")) {
					if (CoreUtility.IsIE()) {
						var range = obj.createTextRange();
						if (exitType == "Left" || exitType == "CtrlLeft") {
							// move the cursor position to the end
							range.moveStart('character', obj.value.length);
							range.select();
						}
						//else if (exitType == "Right" || exitType == "CtrlRight")
						//{
						//    // ie will move the cusor position to the begin by default
						//}
					}
					// HelenLiu 2013/06/24 fix bug 743 in IM HTML5.
					//else {
					//    if (exitType == "Left" || exitType == "CtrlLeft") {
					//        // move the cursor to the end 
					//        var len = obj.value.length;
					//        obj.setSelectionRange(len, len);
					//    }

					//else if (exitType == "Right" || exitType == "CtrlRight") {
					//     //move the cursor to the begin
					//    obj.setSelectionRange(0, 0);
					//}
					//}
				}
			}
			// end of Sean Huang, for bug 1057, 1058<--

			//end by Ryan Wu.	

			//invoke the KeyExit Event if it exit
			var eArgs = { Key: ExitKeys.CharInput };

			//Add items to the ExitKeys.
			/*if (isForward)
			{
				eArgs.Key = ExitKeys.Right;
			}
			else
			{
				eArgs.Key = ExitKeys.Left;
			}*/
			switch (exitType) {
				case "NextControl":
					eArgs.Key = ExitKeys.NextControl;
					break;
				case "PreviousControl":
					eArgs.Key = ExitKeys.PreviousControl;
					break;
				case "Right":
					eArgs.Key = ExitKeys.Right;
					break;
				case "Left":
					eArgs.Key = ExitKeys.Left;
					break;
				case "CtrlRight":
					eArgs.Key = ExitKeys.CtrlRight;
					break;
				case "CtrlLeft":
					eArgs.Key = ExitKeys.CtrlLeft;
					break;
				case "CharInput":
					eArgs.Key = ExitKeys.CharInput;
					break;
			}

			ret = {};
			ret.Name = this.Owner.KeyExitEvent;
			ret.Args = eArgs;
			//Add comments by Ryan Wu at 10:27 Apr. 5 2007.
			//For support Aspnet Ajax 1.0.
			ret.Type = "KeyExit";
			//end by Ryan Wu.


			if (ret != null) {
				retInfo.EventInfo = ret;
			}

			return retInfo;
		}

		/**
			* Move the caret from one field to another in the control. 
			* @param pos - The current caret position.
			* @param isForward - The boolean value indicate if we move caret to the next field or previous field.
			*/
		MoveField(pos: number, isForward: boolean) {
			var nextPos = this.Format.Fields.MoveField(pos, isForward);
			var retInfo: any = {};

			if (nextPos == -1) {
				retInfo.NextPos = nextPos;
				return retInfo;
			}

			retInfo.SelectionStart = nextPos;
			retInfo.SelectionEnd = nextPos;
			return retInfo;
		}

		/**
			* Process Tab key press event. 
			* @param isForward - The boolean value indicate if we move caret to the forward or backward.
			*/
		ProcessTabKey(pos: number, isForward: boolean, tabAction: TabAction) {
			//invoke the KeyExit Event if it exit
			var eArgs = { Key: ExitKeys.Tab };
			var retInfo: any = {};
			if (isForward) {
				eArgs.Key = ExitKeys.Tab;
			}
			else {
				eArgs.Key = ExitKeys.ShiftTab;
			}

			var eventInfo: any = {};
			eventInfo.Name = this.Owner.KeyExitEvent;
			eventInfo.Args = eArgs;
			//Add comments by Ryan Wu at 10:27 Apr. 5 2007.
			//For support Aspnet Ajax 1.0.
			eventInfo.Type = "KeyExit";
			//end by Ryan Wu.

			retInfo.EventInfo = eventInfo;


			retInfo.FocusType = FocusType.KeyExit;
			retInfo.System = true;

			return retInfo;
		}

		/**
			* Move caret between fields then move focus to the next control if caret is in the edge 
			* of the control. 
			* @param isForward - The boolean value indicate if we move caret to the forward or backward.
			*/
		MoveFieldAndControl(pos: number, isForward: boolean) {
			var retInfo = this.MoveField(pos, isForward);

			//retInfo.NextPos == -1 indicate that we have move the caret to the edge of the control.
			if (retInfo.NextPos != -1) {
				return retInfo;
			}


			var exitType = isForward ? "NextControl" : "PreviousControl";
			var ret = this.MoveControl(this.GetInputElement(), isForward, false, exitType);

			if (ret != null) {
				retInfo.EventInfo = ret.EventInfo;
				retInfo.FocusType = ret.FocusType;
				retInfo.FocusExit = true;
			}

			return retInfo;
		}

		/**
		* Handle the ondragstart event.
		*/
		DragStart() {
		}

		/**
		* Handle the ondragend event.
		*/
		DragEnd() {
		}

		/**
		* Handle the ondrop event.
		*/
		Drop() {
		}

		/**
		* Handle the ondragover event.
		*/
		DragOver() {
		}

		/**
		* Parse the shortcutsString and judge whether the key user pressed
		* is a shortcutkey defined by deveploper.
		* not replace indexOf, substring method, because autotest is slow
		* @param keyCode - The specified keyCode will be checked.
		* @param strShortcut - The shortcuts string passed from the server side.
		* @returns Returns the shortcut's keyAction Name if contains the specified
		*          keyCode action; else return null.
		*/
		GetKeyActionName(keyCode, strShortcut) {
			if (strShortcut == null) {
				return null;
			}

			var s = strShortcut;
			var shortcuts = new Array();
			var index = s.IndexOf(",");
			var i = 0;

			if (strShortcut != "") {
				while (index != -1) {
					shortcuts[i++] = s.Substring(0, index);
					s = s.Substring(index + 1, s.GetLength());
					index = s.IndexOf(",");
				}
				shortcuts[i++] = s;

				for (var j = 0; j < i; j = j + 2) {
					if (this.IsKeyCodeContained(keyCode, shortcuts[j + 1])) {
						return shortcuts[j];
					}
				}
			}
			return null;
		}

		/**
		* Judge whether the keyCode is contained in the shortcuts's item.
		* @param keyCode - The specified keyCode will be checked.
		* @param shortcut - The shortcuts's item.
		* @returns Returns true if contains the specified keyCode action; else return false.
		*/
		IsKeyCodeContained(keyCode, shortcut: any) {
			var s = shortcut;
			var index = s.IndexOf("|");

			while (index != -1) {
				if (s.Substring(0, index) == keyCode) {
					return true;
				}
				s = s.Substring(index + 1, s.GetLength());
				index = s.IndexOf("|");
			}

			if (s == keyCode) {
				return true;
			}

			return false;
		}

		/**
		 *fire event
		 */
		FireClientEvent(evenType) {
			// TODO:
		}

		PerformSpin(curpos: number, increment: number, wrap: boolean): any {
		}


		/**
		 * update crlf string for AcceptsCrLf property
		 */
		static UpdateCrLfString(text: string, crlfMode: CrLfMode) {
			var ret = text;

			if (text) {
				if (crlfMode == CrLfMode.Filter) {
					ret = text.replace(new RegExp("[\r\n]", "g"), "");
				}
				else if (crlfMode == CrLfMode.Cut) {   // Frank Liu fixed bug 569 at 2013/06/05.
					var splits = text.split(new RegExp("[\r\n]", "g"));
					if (splits.length > 0) {
						ret = splits[0];
					}
					else {
						ret = "";
					}
				}
			}

			return ret;
		}

		static FilterReturnChar(text: string) {
			if (text != null) {
				text = text.replace(new RegExp("[\r]", "g"), "");
			}
			return text;
		}


	}

	/** @ignore */
	export interface IControl {
		GetInputElement(): HTMLInputElement;
		KeyDown(evt: KeyboardEvent);
		ImeMode: boolean;
	}

	/** @ignore */
	export class InputUIUpdate {

		InputElement: HTMLInputElement;
		Owner: IControl;

		constructor(owner) {
			this.Owner = owner;
		}

		SetLastClientValues(text) {
			//var obj = document.getElementById(this.ID + Utility.LastClientValuesID);

			//if (obj != null) {
			//    obj.value = text;
			//}
			//this.lastclientvalues = text;
		}

		GetText() {
			if (this.Owner.GetInputElement() != null) {
				return this.Owner.GetInputElement().value;
			}

		}

		SetText(text) {
			if (this.GetText() == null) {
				return;
			}

			if (this.GetText().replace(/\r\n/g, "\n") == text.replace(/\r\n/g, "\n")) {
				return;
			}

			if (this.Owner.GetInputElement() != null) {
				this.Owner.GetInputElement().value = text;
			}


		}

		SetFocus() {
			try {
				if (this.Owner.GetInputElement() != null) {
					this.Owner.GetInputElement().focus();
				}

			}
			catch (e) {
			}
		}

		GetTextHAlign() {
			if (this.Owner.GetInputElement() !== null) {
				return this.Owner.GetInputElement().style.textAlign;
			}
			return "";
		}

		SetTextHAlign(value) {
			if (this.Owner.GetInputElement() !== null) {
				this.Owner.GetInputElement().style.textAlign = value;


				if (CoreUtility.IsIE()) {
					// Change the width to trigger the layout, to make this property take affect immediately.
					var old = this.Owner.GetInputElement().style.width;
					var length = parseInt(old);
					if (isNaN(length)) {
						length = 120;
					}
					this.Owner.GetInputElement().style.width = length + 1 + "px";
					var self = this;
					setTimeout(function () {
						self.InputElement.style.width = old;
					}, 0);
				}
			}
		}

		SetForeColor(foreColor) {
			if (this.Owner.GetInputElement() != null) {
				this.Owner.GetInputElement().style.color = foreColor;
			}
		}


		WriteCssStyle(style) {
			try {
				var styleContainer: any = document.getElementById('gcsh_InputManWeb_Style_Container');

				if (CoreUtility.IsIE()) {
					styleContainer.styleSheet.cssText = style;
				}
				else {
					var sheet = styleContainer.sheet;

					for (var i = sheet.cssRules.length - 1; i >= 0; i--) {
						sheet.deleteRule(i);
					}

					var ruleLines = style.split('}');

					for (var j = 0; j < ruleLines.length; j++) {
						var rule = ruleLines[j];
						var index = rule.indexOf('{');

						if (index == -1) {
							continue;
						}

						var style = rule.substring(index + 1);

						if (style.length != 0) {
							var selector = rule.substring(0, index);
							sheet.insertRule(selector + '{' + style + '}', sheet.cssRules.length);
						}
					}
				}
			}
			catch (e) {
			}
		}

		ClearCssStyle() {
		}

	}

	/** @ignore */
	export class GlobalEventHandler {
		static OnKeyDown(control: IControl, evt, forShortcutExtender?) {
			if (control.ImeMode === true && !CoreUtility.IsIE8OrBelow()) {
				return;
			}
			// debugger;
			var k = evt.keyCode;


			//Add comments by Jiang Changcheng at Sep. 9 2008
			//Add the fake key for Shortcut Extender
			if (forShortcutExtender) {
				k |= 524288;
			}
			//End by Jiang Changcheng

			var funcKeysPressed: any = {};
			funcKeysPressed.Shift = false;
			funcKeysPressed.Ctrl = false;
			funcKeysPressed.Alt = false;
			if (evt.shiftKey) {
				funcKeysPressed.Shift = true;
			}

			if (evt.ctrlKey) {
				funcKeysPressed.Ctrl = true;
			}

			if (evt.altKey) {
				funcKeysPressed.Alt = true;
			}
			var useSystem = null;

			//Add comments by Ryan Wu at 16:55 Sep. 11 2007.
			//For fix the bug "17. Ctrl+Click(select all text) will take no effects(firefox).".
			Utility.FuncKeysPressed = funcKeysPressed;
			//end by Ryan Wu.   

			try {
				useSystem = control.KeyDown(evt);
			}
			catch (e) { }

			//Added by Jeff for Edit
			if (useSystem != null && useSystem.KeyCode != null) {
				// Add comments by Yang at 11:44 Sep. 5th 2007
				// For event.keyCode is readonly in firefox.
				// Firefox doesn't support some shortcuts.
				//event.keyCode = useSystem.KeyCode;
				if (CoreUtility.IsIE()) {
					evt.keyCode = useSystem.KeyCode;
				}
				// End by Yang

				//Add comments by Ryan Wu at 15:35 Aug. 13 2007.
				//For in firefox, even if we set the event.returnValue = false in keydown event,
				//the keypress event will also be invoked while in IE will not.
				if (!CoreUtility.IsIE()) {
					Utility.ShouldInvokeKeyPress = false;
				}
				//end by Ryan Wu.

				return;
			}


			//Add comments by Ryan Wu at 15:35 Aug. 13 2007.
			//For in firefox, even if we set the event.returnValue = false in keydown event,
			//the keypress event will also be invoked while in IE will not.
			//	if (!useSystem)
			//	{
			//		event.returnValue = false;
			//	    //event.cancelBubble = true;
			//	}
			if (!useSystem) {
				Utility.PreventDefault(evt);
				//event.cancelBubble = true;

				if (!CoreUtility.IsIE()) {
					Utility.ShouldInvokeKeyPress = false;
				}
			}
			else {
				if (!CoreUtility.IsIE()) {
					Utility.ShouldInvokeKeyPress = true;
				}
				else if (evt != null) {
					evt.returnValue = true;
				}
			}
		}

		static OnKeyPress(control, evt: KeyboardEvent) {
			// Ctrl + X, Ctrl + V, Ctrl + C.
			if ((evt.charCode === 118 || evt.charCode === 120 || evt.charCode === 99) && evt.ctrlKey) {
				// Fire fox 's paste behavior will run at here.
				return;
			}

			if (CoreUtility.IPadAndIPhone && evt.charCode > 256) {
				// DaryLuo 2013/05/22 fix bug 414, on the ipad, when the charCode is greater than 256, the compoistion event will fired, so here we don't process.
				return;
			}


			if (evt.keyCode == 46 && evt.shiftKey) {
				// FireFox Shift + Delete will run at here.  Cut operation.
				return;
			}

			if (control.ImeMode === true) {
				return;
			}
			var obj = control;


			if (!CoreUtility.IsIE() && (evt.charCode == 0 || !Utility.ShouldInvokeKeyPress)) {
				if (!Utility.ShouldInvokeKeyPress || ((evt.keyCode == 38 || evt.keyCode == 40) && obj.Type != "Edit")) {
					Utility.ShouldInvokeKeyPress = true;
					Utility.PreventDefault(evt);

					// Add comments by Yang at 20:15 October 15th 2007
					// For fix the bug 9047
					if (obj != null && obj.Type == "Edit" && obj.DropDownObj != null && obj.DropDownObj.IsKeyFromDropDown) {
						obj.DropDownObj.IsKeyFromDropDown = false;
					}
					// End by Yang

					return;
				}
				// Add comments by Yang at 16:08 Sep. 12th 2007
				// For fix the bug 8805
				else if (!(evt.keyCode == 13 && obj.Type == "Edit"))
				//else if (!(event.keyCode == 13 && event.ctrlKey && obj.Type == "Edit"))
				// End by Yang
				{
					return;
				}
			}

			var keyCode = evt.keyCode || evt.charCode;


			if (keyCode != 13 || obj.Type == "Edit")
			// End by Yang
			{
				var str = String.fromCharCode(keyCode);

				try {


					if (CharProcess.CharEx.IsSurrogate(str.charAt(0))) {
						// DaryLuo 2012/09/17 fix bug 630 in IM Web 7.0, do it in keyup.
						obj.IsSurrogateKeyPressing = true;
						return;
					}
				}
				catch (e) {

				}
				if (str != null) {
					var useSystem = null;

					try {
						useSystem = GlobalEventHandler.CallKeyPress(control, str, evt);
					}
					catch (e) { }
				}
				if (!useSystem) {
					Utility.PreventDefault(evt);
					//event.cancelBubble = true;
					return;
				}

			}
		}

		static CallKeyPress(control, str, evt) {
			if (CoreUtility.IsPad()) {
				control.HasInput = true;
				return true;
			} else {
				var useSystem = control.KeyPress(str, evt);
				return useSystem;
			}

		}

		static OnKeyUp(control, evt) {

			if (control.ImeMode === true && !CoreUtility.IsIE8OrBelow()) {
				return;
			}
			try {
				var imControl = control;
				if (imControl.IsSurrogateKeyPressing) {
					// DaryLuo 2012/09/17 fix bug 630 in IM Web 7.0.
					try {
						if (imControl.InputElement != null) {
							var value = imControl.InputElement.value;
							var selectionStart = imControl.InputElement.selectionStart;
							var str = value.substr(selectionStart - 2, selectionStart);
							imControl.KeyPress(str, evt);
						}
					}
					finally {
						imControl.IsSurrogateKeyPressing = false;
					}
				}

				//Add comments by Ryan Wu at 16:55 Sep. 11 2007.
				//For fix the bug "17. Ctrl+Click(select all text) will take no effects(firefox).".
				Utility.FuncKeysPressed = { Shift: evt.shiftKey, Ctrl: evt.ctrlKey, Alt: evt.altKey };
				//end by Ryan Wu.

				imControl.KeyUp(evt);
				//event.cancelBubble = true;
			}
			catch (e) { }
		}

		static OnCompositionStart(control, evt) {
			try {

				if (CoreUtility.IsPad()) {
					control.ImeMode = true;
					if (CoreUtility.IsAndroid()) {
						// initialize _compositionEndFired
						control._compositionEndFired = false;
					} else {
						control.HasInput = true;
					}
					
					return;
				}
				control.CompositionStart(evt);
			} catch (e) {

			}
		}

		static OnCompositionUpdate(control, evt) {
			try {
				if (CoreUtility.IsPad()) {
					if (CoreUtility.IPadAndIPhone) {
						control.HasInput = true;
						control.ImeMode = true;
					}
					return;
				}
				control.CompositionUpdate(evt);
			} catch (e) {

			}
		}

		static OnCompositionEnd(control, evt) {
			try {

				if (CoreUtility.IsPad()) {
					if (CoreUtility.IsAndroid()) {
						control.HasInput = true;
						// In chrome 61(android), when we input the direct text, the keycode is always 229,
						// 1. if the text correction is open in ime:
						//		keydown, compostionStart, compostionUpdate, input, keyup,...,compositionEnd, keydown, textinput, input, keyup
						//	In this case, we need process the input text in the input event after textinput, because we can get the correct text in this event.
						// 2. if the text correction is not open:
						//		keydown, textinput, keyup
						//	In this case, we need process the input text in the textinput event.
						// 3. no matter whether text correction is open, the delete, backspace key should be processed in keyup event. It cannot be processed in
						// the textinput event and the input event as the focus process is complex and we use the same behavior in the non-mobile browser.
						control._compositionEndFired = true; // if compositionEnd is fired, it is the case 1.
					} else {
						if (control.ImeMode) {
							control.CompositionEnd(evt);
						}
						control.HasInput = false;
					}
					control.ImeMode = false;
					return;
				}
				control.CompositionEnd(evt);
			} catch (e) {

			}
		}

		static OnTextInput(control, evt) {
			if (control.ImeMode && control._compositionEndFired !== true) { // if it is the case 2
				control.HasTextInput = false;
				control.TextInput(evt);
				Utility.PreventDefault(evt); // forbid firing the input event.
			} else {
				// if it is the case 1, 
				control.HasTextInput = true;
			}

			if (control._compositionEndFired === true) {
				control._compositionEndFired = false;
			}
		}

		static OnInput(control, evt) {
			try {
				if (CoreUtility.IsAndroid() && control.ImeMode && !control.HasTextInput) {
					control.UIProcess.keyupResponse = true;
					control.UIProcess.systemEdit = true;
					control.ImeMode = false;
					return;
				}

				if (control.HasTextInput !== true && (control.ImeMode === true || control.HasInput !== true)) {
					return;
				}

				control.ImeMode = true;
				control.HasInput = false;
				control.HasTextInput = false;
				control.Input(evt);
			} catch (e) {

			}
		}

		static OnMouseOver(control, evt) {
			try {
				if (control.MouseOver) {
					control.MouseOver();
				}
			} catch (e) {

			}
		}

		static OnMouseOut(control, evt) {
			try {
				control.MouseOut();
			} catch (e) {

			}
		}

		static OnMouseMove(control, evt) {
			try {
				if (control.MouseMove) {
					control.MouseMove(evt);
				}
			} catch (e) {

			}
		}

		static OnMouseDown(control, evt) {
			if (control.ImeMode && control.ImeMode === true && !CoreUtility.IsIE8OrBelow()) {
				return;
			}
			try {
				control.MouseDown(evt);

				if (!CoreUtility.IsIE()) {
					Utility.DragStartElementID = control;
				}
			}
			catch (e) { }
		}

		static OnMouseUp(imControl, evt) {
			if (imControl.ImeMode && imControl.ImeMode === true && !CoreUtility.IsIE8OrBelow()) {
				return;
			}
			try {
				// DaryLuo 2013/07/12 fix bug 933, 934 in IM HTML 5.
				if (imControl.MouseUpPointerType != null && imControl.MouseUpPointerType !== 4 && imControl.MouseUpPointerType !== "mouse") {
					var evtClone = {};
					for (var item in evt) {
						evtClone[item] = evt[item];
					}
					setTimeout(function (parameters) {
						imControl.MouseUp(evtClone);
					}, 300);
				}
				else {
					imControl.MouseUp(evt);
				}

				if (!CoreUtility.IsIE()) {
					Utility.DragStartElementID = "";
				}
			}
			catch (e) { }
		}

		static OnSelectStart(control, evt) {
			if (Utility.InnerSelect === true && Utility.ShouldFireOnSelectStart == false) {
				Utility.CancelBubble(evt);
			}

			var selText = Utility.GetSelectionText(control.GetInputElement());

			if (typeof (selText) == "undefined" || selText == null) {
				selText = "";
			}
			var useSystem = null;

			try {
				useSystem = control.SelectStart(selText);
			}
			catch (e) { }

			if (useSystem == false) {
				Utility.PreventDefault(evt);
			}
		}

		static OnDblClick(control, evt) {
			var useSystem = null;
			try {
				useSystem = control.DoubleClick();
			}
			catch (e) { }

			if (!useSystem) {
				Utility.PreventDefault(evt);
			}
		}

		static OnHTML5BeforeCopy(control, evt) {
			try {
				Utility.CutCopyPasteEventObject = evt ? evt.originalEvent : evt;
				control.Copy(evt);
			} catch (e) {

			} finally {
				Utility.CutCopyPasteEventObject = null;
			}
		}

		static OnHTML5Cut(control, evt) {
			try {
				var inputElement = control.GetInputElement();
				var text = inputElement.value;
				var selStart = inputElement.selectionStart;
				var selEnd = inputElement.selectionEnd;
				setTimeout(function () {
					inputElement.value = text;
					inputElement.selectionStart = selStart;
					inputElement.selectionEnd = selEnd;
					control.Cut(evt);
				}, 0);

				// Let browser do it.

			} catch (e) {

			}
		}

		static OnHTML5Paste(control, evt) {
            try {
                var inputElement = control.GetInputElement();
                if (inputElement && inputElement.disabled){
                    wijmo.input.Utility.PreventDefault(evt);
                    return;
                }
				// DaryLuo 2013/05/21, the paste operation doesn't take effect on the android chrome.
				if (CoreUtility.chrome && !CoreUtility.IsAndroid()) {
					Utility.CutCopyPasteEventObject = evt ? evt.originalEvent : evt;

					control.Paste(Utility.GetDataFromClipboard(true));
					Utility.PreventDefault(evt);
				} else {
					var selStart = control.SelectionStart;
					var selEnd = control.SelectionEnd;
					setTimeout(function () {

						if (CoreUtility.IsAndroid()) {
							// DaryLuo 2013/05/27 fix bug 371 in IM HTML5.0.
							control.SelectionStart = selStart;
							control.SelectionEnd = selEnd;
						}

						control.isPasting = true;

						// Firefox & android chrome will run at here.
						control.ImeInput("DirectInput");

					}, 0);

				}

			} catch (e) {

			}
			finally {
				Utility.CutCopyPasteEventObject = null;
			}
		}

		static OnMouseWheel(control, evt) {
			try {
				control.MouseWheel(evt);
				if (control.ShouldCancelMouseWheelDefaultBehavior()) {
					Utility.PreventDefault(evt);
				}
			}
			catch (e) {
			}
		}

		static OnDragStart(control, evt) {
			try {
				control.DragStart();
			}
			catch (e) { }
		}

		static OnDragEnd(control, evt) {
			try {
				control.DragEnd(evt);
			}
			catch (e) { }
		}

		static OnDrop(control, evt) {
			try {
				var text = evt.originalEvent.dataTransfer.getData("Text");
				control.DragDrop(text, evt);
			}
			catch (e) { }
		}

		static OnTouchStart(control, evt) {
			Utility.TouchStartTime = new Date();
			Utility.TouchStartEvt = evt;
		}

		static OnTouchEnd(control, evt) {
			if (Utility.TouchStartTime !== undefined) {
				Utility.TouchEndTime = new Date();
				var offset = Utility.TouchEndTime.valueOf() - Utility.TouchStartTime.valueOf();
				if (offset > 1000) {
					var text = "";
					try {
						text = Utility.GetSelectionText(control.GetInputElement());
					}
					catch (e)
					{ }
					control.ShowContextMenu(text, Utility.TouchStartEvt);
					if (control.GetEnabled()) {
						Utility.PreventDefault(evt);
					}
				}
				Utility.TouchStartTime = undefined;
				Utility.TouchEndTime = undefined;
			}
		}

		static OnDragEnter(control, evt) {
			try {
				control.DragEnter();
			}
			catch (e) { }
		}

		static OnDragLeave(control, evt) {
			try {
				control.DragLeave();
			}
			catch (e) { }
		}

		static OnSelect(control, evt) {
			try {
				if (CoreUtility.IsPad() && control.ImeMode) {
					return;
				}
				if (control && control.Focused) {
					control.Select();
				}
			}
			catch (e) {

			}
		}

		static OnPropertyChanged(control, evt) {
			try {
				control.PropertyChange(evt);
			}
			catch (e) { }
		}

		static OnMSPointerUp(control, evt) {
			control.MSPointerUp(evt);
		}

		static OnMSPointerDown(control, evt) {
			try {
				control.MSPointerDown(evt);
			}
			catch (e) { }
		}


		static OnMSGestureTap(control, evt) {
			try {
				control.MSGestureTap(evt);
			}
			catch (e) { }
		}

		static OnEditFieldFocus(control, evt) {
			if (Utility.IsFocusFromIMControl(control._id, evt)) {
				if (CoreUtility.IsIE() || Utility.HasGetFocus) {
					return;
				}
			}
			//if (!Utility.firefox) {
			//    if (Utility.IsOnFocus) {
			//        if (GrapeCity.IM.Utility.IsIE() && Utility.IsOnActivate || Utility.HasGetFocus) {
			//            return;
			//        }
			//    }
			//}

			Utility.IsOnFocus = true;
			if (!CoreUtility.IsIE()) {
				Utility.HasGetFocus = true;
			}
			this.OnFocus(control, evt);
			//onFocus(id);
			//Utility.FireEvent(document.getElementById(id), eventName, null);
			// end of Sean Huang <--
		}

		static OnEditFieldLoseFocus(control, evt) {
			try {
				//function gcsh_InputManWeb_onLoseFocus(eventName, id) {
				var obj: any = control;

				if (obj != null && obj.Type == "Edit" && obj.IsFocusToDropDownEdit) {
					obj.IsFocusToDropDownEdit = false;
					return;
				}

				// Temporarily comment at 2013/08/13.
				//if (!Utility.IsIE() && obj != null && obj.IMControlType == "Date" && obj.DropDownObj != null) {
				//    if (Utility.IsFireFox4OrLater()) {
				//        if (obj.DropDownObj.IsNavigateMouseDown) {
				//            obj.DropDownObj.IsNavigateMouseDown = null;
				//            return;
				//        }
				//        if (obj.DropDownObj.IsZoomButtonMouseDown) {
				//            obj.DropDownObj.IsZoomButtonMouseDown = null;
				//            return;
				//        }
				//    }
				//    if (obj.DropDownObj.IsHeaderMouseDown) {
				//        obj.DropDownObj.IsHeaderMouseDown = null;
				//        return;
				//    }
				//}

				if (Utility.IsFocusToIMControl(control._id, evt)) {
					return;
				}

				if (CoreUtility.firefox) {
					if (Utility.IsOnFocus && CoreUtility.IsIE() && Utility.IsOnActivate && Utility.IsOnActivateControlID == control._id) {
						if (Utility.FocusToBorder != null && Utility.FocusToBorder == true) {
							Utility.FocusToBorder = false;
							obj.SetInnerFocus();
						}

						return;
					}

					Utility.FocusToBorder = false;

					if (!CoreUtility.IsIE()) {
						Utility.HasGetFocus = false;
					} else {
						Utility.IsOnFocus = false;
					}
				}

				GlobalEventHandler.OnLostFocus(control, evt);

				Utility.FuncKeysPressed = { Shift: false, Ctrl: false, Alt: false };
				//Utility.FireEvent(control, "LoseFocus", null);
			} catch (e) {
			}
		}

		static OnFocusOut(control, evt) {
			Utility.FocusToBorder = false;

			if (CoreUtility.IsIE()) {
				if (evt.toElement) {
					var toID = evt.toElement.id;
					if (toID == control._id + "_Inside_Div_Container") {
						Utility.FocusToBorder = true;
					}
				}
			}

			if (Utility.IsFocusToIMControl(control._id, evt)) {
				if (CoreUtility.IsIE9OrLater()) {
					Utility.IsOnActivate = true;
					Utility.IsOnActivateControlID = control._id;
					Utility.IsOnFocus = true;
				}
				return;
			}
			if (CoreUtility.IsIE() && Utility.IsOnActivate) {
				Utility.IsOnActivate = false;
				Utility.IsOnActivateControlID = "";
			}
			if (Utility.IsOnFocus) {
				Utility.IsOnFocus = false;
			}
		}

		static OnActivate(control, evt) {
			Utility.IsOnActivate = true;
			Utility.IsOnActivateControlID = control._id;

			if (Utility.IsFocusFromIMControl(control._id, evt)) {
				return;
			}

			Utility.IsOnActivate = true;
			Utility.IsOnActivateControlID = control._id;

		}

		static OnDeActivate(control, evt) {
			if (Utility.IsFocusToIMControl(control._id, evt)) {
				return;
			}

			Utility.IsOnActivate = false;
			Utility.IsOnActivateControlID = "";

			Utility.IsOnFocus = false;

		}

		static OnFocus(control, evt) {
			try {
				var handler = function () {
					control.Focus();
					control.Focused = true;

					if (CoreUtility.IsPad()) {
						// DaryLuo 2013/05/27 fix bug 412 in IM HTML 5.0.
						control.IPadSelectionRefreshTimer = setInterval(function (evt) {
							GlobalEventHandler.OnSelect(control, evt);
						}, 400);
					}

				};
				if (CoreUtility.IsIE()) {
					handler.call(this);
				}
				else {
					setTimeout(handler, 0);
				}
			}
			catch (e) { }
		}

		static OnLostFocus(control, evt) {
			try {
				control.LoseFocus(evt);
				control.Focused = false;

				if (CoreUtility.IsPad()) {
					clearInterval(control.IPadSelectionRefreshTimer);
				}
			} catch (e) {

			}
		}
	}

}

/** @ignore */
interface Window {
	escape(charString: string): string;
	unescape(charString): string;
}

